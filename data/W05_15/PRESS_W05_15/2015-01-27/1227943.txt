TITRE: Fidel Castro sort de son silence sur le rapprochement avec les Etats-Unis | JOL Journalism Online Press
DATE: 27-01-2015
URL: http://www.jolpress.com/fidel-castro-sort-de-son-silence-sur-le-rapprochement-avec-les-etats-unis-article-829371.html
PRINCIPAL: 1227941
TEXT:
Fidel Castro sort de son silence sur le rapprochement avec les Etats-Unis
publi� le 27/01/2015 � 09h09
Commenter
L'ancien dirigeant cubain Fidel Castro s'est exprim� pour la premi�re fois lundi dans un communiqu� sur les pourparlers engag�s entre Cuba et les Etats-Unis.
Alors que les rumeurs le disaient mort, Fidel Castro a donn� son avis sur le rapprochement avec les Etats-Unis. (Cr�dit : This image)
Fidel Castro est bel et bien vivant. Celui qu�on annon�ait mort a fait lire lundi, � la t�l�vision d'Etat, une lettre pour �voquer pour la premi�re fois le d�gel des relations entre Cuba et les Etats-Unis, initi� mi-d�cembre.
Il a paru soutenir le processus de rapprochement entre les deux pays. Fidel Castro, qui a c�d� les r�nes du pouvoir � son fr�re Raul, a expliqu� ne pas faire confiance aux autorit�s am�ricaines mais a dit ne pas vouloir rejeter une solution pacifique � un conflit.
A RETROUVER AUSSI >> Cuba/Etats-Unis : retour sur un conflit surann�
Amiti� avec tous les peuples
"Toute solution pacifique et n�goci�e aux probl�mes entre les Etats-Unis et les peuples, n'importe quel peuple d'Am�rique latine, qui n'implique pas la force ou le recours � la force doit �tre abord�e conform�ment aux r�gles et aux principes internationaux", indique le communiqu� de l'ex-pr�sident cubain.
"Nous soutiendrons toujours la coop�ration et l'amiti� avec tous les peuples du monde, et parmi eux nos adversaires politiques", poursuit le texte.
Barack Obama et Raul Castro ont annonc� le 17 d�cembre qu'ils entendaient engager les Etats-Unis et Cuba sur la voie d'une normalisation des relations apr�s plus d'un demi-si�cle de conflit larv�.
Source Reuters
