TITRE: l'Acad�mie Fran�aise. Michel Houellebecq pense �tre un candidat valable
DATE: 27-01-2015
URL: http://www.ouest-france.fr/lacademie-francaise-michel-houellebecq-pense-etre-un-candidat-valable-3145411
PRINCIPAL: 1228477
TEXT:
l'Acad�mie Fran�aise. Michel Houellebecq pense �tre un candidat valable
France -
Achetez votre journal num�rique
Michel Houellebecq, auteur du controvers� � Soumission �, a fait part de son int�r�t pour entrer � l'Acad�mie fran�aise, estimant qu'il pourrait � �tre utile � � l'institution
Alors qu'on l'interrogeait sur le soutien d'H�l�ne Carr�re d'Encausse, secr�taire perp�tuel de l'Acad�mie fran�aise, Michel Houellebecq a r�pondu: ��l'Acad�mie fran�aise oui, c'est gentil. En m�me temps, ils ont d�j� eu des probl�mes avec (Alain) Finkielkraut. Il y a eu des pol�miques, je ne sais pas si je vais leur imposer �a tout de suite��.
��Je pense que je pourrais �tre utile��
��Je pense que je pourrais �tre utile sur certains mots. J'aurais des opinions. Oui, �ventuellement, je pourrais �tre un candidat valable��, a ajout� l'auteur.  Alain Finkielkraut, auteur en 2013 d'un essai � succ�s sur l'identit� nationale et l'immigration, ��L'identit� malheureuse��, a �t� �lu le 10 avril 2014 � l'Acad�mie fran�aise au fauteuil de F�licien Marceau, d�s le premier tour, en d�pit de la pol�mique qui avait pr�c�d� le scrutin.
La candidature du pol�miste anticonformiste, tax� de r�actionnaire par ses d�tracteurs, avait divis� le petit monde feutr� du Quai de Conti: personnalit� ��trop clivante��, jugeaient en coulisses les acad�miciens oppos�s � son �lection, certains allant jusqu'� �voquer l'entr�e � l'Acad�mie du Front national.
Mercredi dernier, lors d'un entretien � la t�l�vision suisse RTS H�l�ne Carr�re d'Encausse s'�tait montr�e enthousiaste � l'id�e d'une candidature de Michel Houellebecq. ��Je trouverais �a bien. Je le soutiendrais, simplement je ne sais pas si cela correspond � son d�sir��, avait-elle d�clar�.
��Un sociologue de notre temps��
��Houellebecq est un grand �crivain (...) il nous raconte notre temps. C'est un sociologue de notre temps (...) En grossissant le trait, il voit les grandes tendances. C'est ce qui rend son livre extr�mement int�ressant��, avait ajout� le secr�taire perp�tuel de l'Acad�mie fran�aise. L'institution fond�e en 1635 par Richelieu a fait savoir que ses 40 fauteuils �taient ��tous pourvus��. ��Un fauteuil doit �tre vacant pour recevoir officiellement les candidatures��, a-t-on ajout� de m�me source.
L'Acad�mie est charg�e de veiller au respect de la langue fran�aise et d'en composer le dictionnaire. Elle attribue aussi des prix, dont le Grand prix du roman, d�cern� cette ann�e le 30 octobre.  Elle d�crypte aussi en ligne les pi�ges du fran�ais sur sa page ��Dire, ne pas dire�� et r�pond aux questions des internautes.�
Tags :
