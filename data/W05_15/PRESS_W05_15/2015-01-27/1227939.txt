TITRE: Bagdad : un avion de FlyDubai touch� par un tir avant l'atterrissage - SudOuest.fr
DATE: 27-01-2015
URL: http://www.sudouest.fr/2015/01/27/bagdad-un-avion-de-flydubai-touche-par-un-tir-avant-l-atterrissage-1810844-4803.php
PRINCIPAL: 1227934
TEXT:
Un avion de la compagnie a�rienne FlyDubai a �t� la cible d'un tir de petit calibre � son arriv�e � Bagdad
Un avion de flydubai a �t� pris pour cible avant son atterrissage � Bagdad � Photo
AFP HO
Publicit�
E
tre pris pour cible : voil� l'une des craintes principales des compagnies a�riennes survolant les zones de conflit. Alors qu'en juillet un appareil de la Malaysia Airlines a �t� abattu au dessus de l'Ukraine , un avion de la compagnie � bas co�ts FlyDubai a �t� touch� par un tir avant son atterrissage lundi � l'a�roport international de Bagdad, a annonc� mardi un porte-parole de la compagnie a�rienne �miratie.
Publicit�
Le fuselage de l'appareil assurant le vol FZ215 a �t� touch� par une "balle de petit calibre" mais tous les passagers ont pu d�barquer sains et saufs, a pr�cis� le porte-parole.
"L'avion a pu se poser normalement", a pour sa part indiqu� un officier des services de s�curit� de l'a�roport de Bagdad.
Vols suspendus
Apr�s cet incident, l'autorit� de l'aviation civile aux Emirats arabes unis a d�cid� de suspendre les vols vers Bagdad des quatre compagnies du pays : FlyDubai, Emirates, Etihad et Air Arabia.
Emirates, la plus grosse compagnie �miratie, a confirm� dans un communiqu� avoir suspendu ses vols sur Bagdad "pour des raisons op�rationnelles".
De son c�t�, Etihad a indiqu� avoir suspendu ses vols en direction de la capitale irakienne "jusqu'� nouvel ordre".
L'incident a aussi entra�n� des retards dans les vols d'autres compagnies, comme Turkish Airlines et Royal Jordanian.
"Les horaires des vols ne sont pas clairs. Les directeurs des op�rations sont actuellement en r�union", a d�clar� un porte-parole de la compagnie turque � Ankara.
Les grosses compagnies a�riennes survolant le territoire irakien redoublent de pr�cautions par crainte que des membres du groupe Etat islamique (EI) puissent acqu�rir des armes capables de toucher leurs appareils.
L'a�roport de Bagdad est situ� � l'ouest de Bagdad, pr�s de la province d'Al-Anbar, largement contr�l�e par les jihadistes de l'EI.
