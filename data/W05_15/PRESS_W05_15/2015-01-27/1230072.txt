TITRE: Nouveau sur Twitter: discussions en groupe et publication de vid�os de maximum 30 secondes - L'Avenir Mobile
DATE: 27-01-2015
URL: http://www.lavenir.net/article/detail.aspx?articleid%3Ddmf20150127_00592903
PRINCIPAL: 1230065
TEXT:
Nouveau sur Twitter: discussions en groupe et publication de vid�os de maximum 30 secondes
27-01-2015 21:23
- AFP
Le r�seau social Twitter a annonc� ce mardi que ses utilisateurs pouvaient d�sormais avoir des discussions de groupe, comme sur Facebook, et partager des vid�os sur sa plateforme.
�La plupart d�entre vous utilise les messages directs pour discuter. La possibilit� d��changer en priv� avec plusieurs personnes vous offre davantage de possibilit�s sur comment et avec qui vous souhaitez communiquer�, a indiqu� Jinen Kamdar, un responsable produit de Twitter, sur le blog officiel de l�entreprise.
Cette nouvelle option permet de d�marrer une conversation avec 20 personnes, �qui n�ont pas besoin de toutes se suivre pour discuter�, pr�cise le blog.
Twitter a annonc� une seconde nouveaut� sur sa plateforme: la possibilit� de �r�aliser, �diter et partager des vid�os� qui peuvent durer jusqu�� 30 secondes sur le r�seau social. Une configuration sp�ciale pour la vid�o sera d�sormais disponible ainsi qu�une fonctionnalit� �d��dition interne�.
�Les utilisateurs de Twitter sur iPhone pourront �galement t�l�charger des vid�os depuis leur appareil, une option qui sera bient�t disponible sur notre application pour Android�, a ajout� Twitter.
Pour contrer des r�sultats d�cevants?
Le r�seau social avait d�j� ajout� une nouvelle fonctionnalit� � sa plateforme la semaine derni�re, en proposant de r�capituler pour ses utilisateurs les publications qu�ils ont manqu�es quand ils ne sont pas connect�s.
Avec ce genre de nouveaut�s, Twitter s�efforce de se rendre plus int�ressant pour ses membres, dont la croissance et les interactions avec sa plateforme sont jug�es d�cevantes par certains analystes.
Le nombre d�utilisateurs mensuels, une variable cl� pour les investisseurs, avait ainsi augment� de seulement 13 millions au troisi�me trimestre pour atteindre 284 millions fin septembre, selon le dernier pointage fait par la soci�t�.
