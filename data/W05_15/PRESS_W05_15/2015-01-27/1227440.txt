TITRE: Routiers en colère. Un médiateur pour renouer le dialogue - Rennes - Le Télégramme, quotidien de la Bretagne
DATE: 27-01-2015
URL: http://www.letelegramme.fr/ille-et-vilaine/rennes/rocade-de-rennes-nouvelle-operation-escargot-des-routiers-27-01-2015-10506693.php
PRINCIPAL: 1227438
TEXT:
Routiers en colère. Un médiateur pour renouer le dialogue
27 janvier 2015 à 18h38
Dès 6 h, ce mardi, les routiers en colère ont repris leur action, notamment dans l'ouest. Le patronat a annoncé en milieu d'après-midi qu'il acceptait le principe d'une médiation.
18H35. Probable barrage filtrant demain sur la RN12 à Plounévez-Moëdec (22). ( Plus d'infos ).
16H33. Le patronat routier accepte le principe d'une médiation sous conditions
Les fédérations patronales du transport routier (FNTR, TLF et UNOSTRA) se sont dites prêtes ce mardi à accepter la nomination d'un médiateur pour renouer le dialogue avec les syndicats, à condition que ces derniers renoncent "à toute pression revendicative en cours de discussions". Ils devront aussi accepter d'élargir le champ des discussions à l'ensemble des "chantiers professionnels" (protection sociale, classifications métiers, formation professionnelle, etc.) de la branche, sans se focaliser sur les seules négociations salariales, selon elles.
13H10. Seule une opération escargot perdure dans le sens Rennes-Le Mans...
... C'est ce que nous indique le Cricr, le Centre régional d'information et de coordination routières. Les manifestants se dirigent vers le péage de la Gravelle.
10H30. Bouchons sur la N157
En milieu de matinée, les routiers poursuivaient leur action. Les manifestants sont actuellement à l'arrêt de Châteaubourg à Servon-sur-Vilaine, ce qui crée, bien sûr, des bouchons : bouchon de 2 km sur deux voies, sur la N157, dans le sens Rennes-Le Mans (sens ouest-est), de Servon-sur-Vilaine à Châteaubourg; bouchon de 1,1 km sur une voie, sur la N157, dans le sens Le Mans-Rennes (sens est-ouest), à Châteaubourg. Informations qui nous ont été données par le Cricr, le Centre régional d'information et de coordination routières.
7H00. Début d'une opération escargot
A 6 h, ce mardi, les routiers en colère se sont lancés dans une nouvelle opération escargot sur la rocade de Rennes, dans le sens Rennes-Le Mans. Vers 7 h, ils ont fait un arrêt devant les Transports STG, à Noyal-sur-Vilaine, pour "distribuer des tracts et bloquer quelques camions", d'après une source syndicale. Dans la journée, ils devraient ainsi alterner opérations escargots et barrages filtrants.
Une trentaine de routiers zone industrielle de Noyal sur Vilaine #routiers pic.twitter.com/mPycVmUtin
