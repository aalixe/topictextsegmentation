TITRE: Mexique : Florence Cassez r�clame en justice 36 millions de dollars
DATE: 27-01-2015
URL: http://www.lemonde.fr/ameriques/article/2015/01/27/mexique-action-en-justice-de-florence-cassez_4564170_3222.html
PRINCIPAL: 1228563
TEXT:
Mexique : Florence Cassez r�clame en justice 36 millions de dollars
Le Monde |
� Mis � jour le
27.01.2015 � 14h00
Florence Cassez, emprisonn�e plus de sept ans au Mexique et lib�r�e il y a deux ans par la Cour supr�me mexicaine , a entam� une action en justice pour obtenir des dommages et int�r�ts d'un montant de 36 millions de dollars (31,86 millions d'euros).
Son avocat mexicain, Me Jos� Pati�o Hurtado, a indiqu� sur radio MVS que cette action en justice, lanc�e vendredi 23 janvier, visait l'ex-pr�sident mexicain Felipe Calderon (2006-2012), son ancien secr�taire particulier, l'actuel s�nateur Roberto Gil, ainsi que les anciens ministres de la s�curit� publique, Genaro Garcia Luna, et de la justice, Daniel Cabeza de Vaca.
Lire : Pour la presse mexicaine, l'ex-ministre Garcia Luna devra rendre des comptes
��Nous pr�sentons une plainte pour dommage moral envers Florence Cassez, atteinte dans ses sentiments, sa r�putation et son honneur. Ils ont tu� sa vie��, a dit l'avocat. Il a estim� que l'ancien pr�sident Calderon, comme les autres personnalit�s vis�es, ���tait en charge et n'a pas emp�ch� que soit commis l'illicite�� contre Florence Cassez.
La plainte vise aussi la cha�ne mexicaine Televisa et l'un de ses pr�sentateurs vedettes, Carlos Loret de Mola, accus�s d'avoir pr�sent� comme une arrestation en direct une mise en sc�ne de la police.
Voir : Florence Cassez : ��le plus difficile �tait l'injustice��, pas la prison
CRISE DIPLOMATIQUE
Florence Cassez, actuellement �g�e de 40 ans, avait �t� arr�t�e le 8 d�cembre 2005 en compagnie de son ex-compagnon mexicain, Israel Vallarta, sur une route du sud de Mexico. Le lendemain matin � l'aube, la police avait organis�, devant les cam�ras de t�l�vision, une simulation de l'arrestation dans un ranch o� avaient �t� lib�r�s trois otages pr�sum�s.
Condamn�e � soixante ans de prison pour enl�vements, Florence Cassez a �t� lib�r�e le 23 janvier 2013, la Cour supr�me ayant jug� que n'avaient pas �t� respect�es les conditions d'un proc�s �quitable avec, au d�part, une ��mise en sc�ne contraire � la r�alit頻. L'affaire, qui avait provoqu� en 2011 une crise diplomatique entre la France et le Mexique, avait �t� mise en avant par l'ex-pr�sident Calderon comme embl�matique de sa lutte contre la criminalit�.
