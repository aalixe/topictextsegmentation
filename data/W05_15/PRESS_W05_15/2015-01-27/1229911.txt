TITRE: VIDEO. H�rault : cinq personnes en garde � vue lors d'une op�ration anti-jihadistes � Lunel
DATE: 27-01-2015
URL: http://www.francetvinfo.fr/france/jihadistes-francais/video-herault-cinq-personnes-en-garde-a-vue-lors-d-une-operation-anti-jihadiste-a-lunel_808727.html
PRINCIPAL: 0
TEXT:
VIDEO. H�rault : cinq personnes en garde � vue lors d'une op�ration anti-jihadistes � Lunel
L'intervention du Raid a �t� men�e mardi 27 janvier.
(PEGGY MAUGER - FRANCE 2)
Par Francetv info avec AFP
Mis � jour le
, publi� le
27/01/2015 | 20:27
Selon des �lus de Lunel,�commune de plus de 26�000�habitants�dans l'H�rault,�une vingtaine de jeunes entre 18�ans et 30�ans sont partis combattre en Syrie�depuis le mois d'octobre.�Six d'entre eux y ont trouv� la mort, selon les autorit�s fran�aises. Cinq personnes proches de jihadistes �partis en Syrie ont �t� interpell�es et plac�es en garde � vue �mardi 27 janvier, lors d'une op�ration du Raid. Elles sont soup�onn�es de les avoir aid� � partir.
Des proches morts en Syrie
Les personnes interpell�es sont �g�es de 44�ans, 35�ans, 31�ans et 26�ans pour deux d'entre elles. Deux sont �galement soup�onn�es d'�tre d�j� parties en Syrie alors que les trois autres seraient des candidats au jihad. Deux des interpell�s ont perdu un proche dans les combats en Syrie.
L'op�ration du Raid, men�e dans le cadre d'une information judiciaire dont sont saisis des magistrats antiterroristes � Paris, s'est poursuivie aussi � Caussiniojouls (H�rault), au nord de B�ziers, et � Aimargues (Gard).�
Le JT
Les autres sujets du JT
1
