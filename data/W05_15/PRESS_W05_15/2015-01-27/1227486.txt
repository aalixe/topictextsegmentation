TITRE: � Facebook Lite � : la nouvelle version de Facebook pour les pays �mergeants
DATE: 27-01-2015
URL: http://www.infos-mobiles.com/facebook/facebook-lite-nouvelle-version-facebook-les-pays-emergeants/88748
PRINCIPAL: 1227448
TEXT:
Accueil � � Facebook Lite � : la nouvelle version de Facebook pour les pays �mergeants
� Facebook Lite � : la nouvelle version de Facebook pour les pays �mergeants
Thomas le 27.01.2015 � 8:17 Commentaires ferm�s
Apr�s avoir lanc� une version professionnelle de son site � Facebook at Work �, le g�ant am�ricain des r�seaux sociaux vient de tester une nouvelle version de son application. Eh oui, une nouvelle version baptis�e � Facebook Lite � qui serait destin�e aux habitants des pays �mergents.
Une version conf�rant un chargement du fil d�actualit� et des photos rapides
� Facebook Lite � : la nouvelle version de Facebook pour les pays �mergeants
Le grand r�seau social am�ricain veut vraiment que tout le monde se r�unisse sur son r�seau. Mark Zuckerberg teste donc actuellement une version � Lite � de son r�seau pour les pays �mergents. Nous savons bien que dans ces pays la connexion est encore limit�e � la 2G mais aussi que la majorit�, d�o� il est difficile de charger les pages plus lourds � citer le fil d�actualit�, les messages des fois et m�me les photos. Si l�on en croit bien au propos de Facebook , m�me les gens habitant dans les zones o� l�acc�s � internet est assez limit� peuvent profiter de cette nouvelle version de son site. Bien s�r cette version sera donc compatible avec le syst�me d�exploitation du g�ant am�ricain Google : Android, qui occupe la majorit� des smartphones utilis�s par les gens vivant dans ces pays. Ce sont nos confr�res du site TechCrunch qui nous ont tenu part de cette grande nouvelle donc, a eu de rajouter que le lancement de cette cette nouvelle mouture de Facebook a d�but� ce week-end dans quelques pays de l�Afrique mais aussi de l�Asie � citer le Bangladesh, le N�pal, le Nigeria, l�Afrique du Sud, le Sri Lanka, le Soudan, le Vietnam et le Zimbabwe.
Une initiation � � internet.org �
Facebook veut vraiment connecter tout le monde sur son r�seau, lui qui dispose de 1,35 milliard d�utilisateurs actuellement veut encore agrandir son champ d�audience. C�est donc dans les pays �mergents qu�il compte conqu�rir du monde actuellement, � rappeler que �a ne date pas d�aujourd�hui que Facebook lance une version � Lite � de son application. Mais cette prise en charge des pays ayant une difficult� d�acc�s � internet fait aussi partie de son projet pour permettre � l�internet pour tous � : � Internet.org �.
