TITRE: Facebook d�ment toute cyberattaque apr�s une importante panne
DATE: 27-01-2015
URL: http://www.latribune.fr/technos-medias/20150127trib8e64fc16d/facebook-instagram-et-tinder-confrontes-a-une-importante-panne.html
PRINCIPAL: 1227824
TEXT:
OR 1 184,9$ -1,03                 %
Facebook d�ment toute cyberattaque apr�s une importante panne
En 2010, le r�seau social avait subi une coupure durant deux heures et demie, en raison de probl�mes techniques, rappelle Le Figaro. (Cr�dits : � Dado Ruvic / Reuters)
latribune.fr (avec Reuters et AFP) �|�
27/01/2015, 10:39
�-� 265 �mots
La panne de 50 minute s'est produite "apr�s l'introduction d'un changement qui a affect� nos syst�mes de configuration", assure le r�seau social.
sur le m�me sujet
Abonnez-vous � partir de 1�
Publi� le 27/01/2015 � 08:39. mis � jour le 27/01/2015 � 10:39.
L'acc�s aux applications de rencontres Tinder ainsi qu'au r�seau social�Facebook ont �t� perturb�s le matin du mardi 27 janvier par une panne, emp�chant les utilisateurs d'acc�der au service en ligne pendant 50 minutes, entre 7 heures et 8 heures.
"Beaucoup de personnes ont eu du mal � acc�der � Facebook�et Instagram. Cela ne r�sultait pas d'une attaque de tiers mais s'est produit apr�s l'introduction d'un changement qui a affect� nos syst�mes de configuration", a indiqu� le groupe dans un communiqu�.
"Nous avons rapidement r�solu le probl�me et les deux services fonctionnent � nouveau � 100% pour tout le monde", a assur� Facebook.
Un groupe de hackers revendique une attaque sur le r�seau social
Quand le probl�me a commenc� � �tre r�percut� sur les r�seaux sociaux, Facebook�avait indiqu� dans un tr�s court message: "Nous savons que de nombreuses personnes peinent actuellement � acc�der �Facebook�et Instagram. Nous travaillons � un retour � la normale aussi rapidement que possible."
L'attaque a �t� revendiqu�e sur un compte Twitter cens� appartenir � "Lizard Squad", un groupe de pirates informatiques associ� � d'autres r�centes cyberattaques, contre le serveur Xbox Live de Microsoft et le PlayStation Network de Sony en d�cembre dernier notamment.
En 2010, le r�seau social avait subi une coupure durant deux heures et demie, en raison de probl�mes techniques, rappelle Le Figaro.
Avec 1,35 milliard d'utilisateurs revendiqu�s � la fin septembre 2014, le site de Mark Zuckerberg poss�de le plus important r�seau social au monde. Facebook a rachet� Instagram en 2012 pour 1 milliard de dollars .
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Votre email ne sera pas affich� publiquement
Tous les champs sont obligatoires
�
Merci pour votre commentaire. Il sera visible prochainement sous r�serve de validation.
�a le � :
