TITRE: La Nordiste Florence Cassez r�clame 36 millions de dollars de dommages et int�r�ts au Mexique - R�gion - Nord Eclair
DATE: 27-01-2015
URL: http://www.nordeclair.fr/info-locale/la-nordiste-florence-cassez-reclame-36-millions-de-dollars-jna60b0n614843
PRINCIPAL: 1228976
TEXT:
La Nordiste Florence Cassez r�clame 36 millions de dollars de dommages et int�r�ts au Mexique
Par la r�daction pour Nord Eclair , Publi� le 27/01/2015 - Mis � jour le 27/01/2015 � 12 : 42
Nord Eclair
- A +
La fran�aise Florence Cassez, emprisonn�e plus de 7 ans au Mexique et lib�r�e il y a deux ans par la Cour supr�me, a entam� une action en justice pour obtenir des dommages et int�r�ts d�un montant de 36 millions de dollars.
L�avocat mexicain de Florence Cassez, Me Jos� Pati�o Hurtado, a indiqu� sur radio MVS que cette action en justice, entam�e vendredi, visait l�ex-pr�sident mexicain Felipe Calderon (2006-2012), son ancien secr�taire particulier, l�actuel s�nateur Roberto Gil, ainsi que les anciens ministres de la S�curit� publique, Genaro Garcia Luna, et de la Justice, Daniel Cabeza de Vaca.
Plainte pour �� Dommage moral� �
�� Nous pr�sentons une plainte pour dommage moral envers Florence Cassez, atteinte � ses sentiments, � sa r�putation et � son honneur. Ils ont tu� sa vie ��, a dit l�avocat. Il a estim� que l�ancien pr�sident Calderon, comme les autres personnalit�s vis�es, � ��tait en charge et n�a pas emp�ch� que soit commis l�illicite �� contre Florence Cassez. La plainte vise aussi la cha�ne mexicaine Televisa et l�un de ses pr�sentateurs vedettes, Carlos Loret de Mola, accus�s d�avoir pr�sent� comme une arrestation en direct une mise en sc�ne de la police.
Florence Cassez, actuellement �g�e de 40 ans, avait �t� arr�t�e le 8 d�cembre 2005 en compagnie de son ex-compagnon mexicain, Israel Vallarta, sur une route du sud de Mexico. Le lendemain matin � l�aube, la police avait organis� devant les cam�ras de t�l�vision, une simulation de l�arrestation dans un ranch o� avaient �t� lib�r�s trois otages pr�sum�s.
Condamn�e � 60 ans de prison pour enl�vements, Florence Cassez a �t� lib�r�e le 23 janvier 2013 , la Cour supr�me ayant jug� que n�avaient pas �t� respect�es les conditions d�un proc�s �quitable, avec, au d�part, une ��mise en sc�ne contraire � la r�alit頻.
L�affaire, qui avait provoqu� en 2011 une crise diplomatique entre la France et le Mexique, avait �t� mise en avant par l�ex-pr�sident Calderon comme embl�matique de sa lutte contre la criminalit�
