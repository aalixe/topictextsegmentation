TITRE: Tom Cruise aurait fait espionner Nicole Kidman apr�s leur divorce - telestar.fr
DATE: 27-01-2015
URL: http://www.telestar.fr/2015/articles/tom-cruise-aurait-fait-espionner-nicole-kidman-apres-leur-divorce-73339
PRINCIPAL: 1229232
TEXT:
Tom Cruise aurait fait espionner Nicole Kidman apr�s leur divorce
Par Thomas Janua Le 27 janvier 2015 � 16h46
Dans un documentaire sur la scientologie, r�cemment montr� au festival de Sundance, le r�alisateur Alex Gibney affirme que la star Tom Cruise, aurait �t� pouss� � quitter Nicole Kidman par la secte ainsi qu'� mettre son ex sur �coute apr�s leur divorce.
Partagez
Commentaire(s) : 1
� Mano
Avec "Going clear�: scientology and the prison of belief" du r�alisateur oscaris� Alex Gibney a fait son petit effet lors de sa projection le 25 janvier 2015 au festival du film de Sundance . Gibney a expliqu� les classiques pressions exerc�es par la Scientologie pour freiner son enqu�te, produite par la cha�ne HBO. Non pour simplement stopper un nouveau document � charge, mais bien parce que le film �corne l'image de la figure de proue m�diatique de la scientologie�: la star de cin�ma Tom Cruise . Dans le film de Gibney, un ancien membre de haut rang de la secte, Mark Rathbun, r�v�le que la scientologie avait fait mettre sur �coute le mobile de Nicole Kidman , juste apr�s sa s�paration d'avec Tom Cruise. Selon la m�me source, ce sont Tom Cruise en personne et David Miscavige , le gourou de la secte, qui auraient demand� cet espionnage.
Les faits auraient eu lieu, car Tom Cruise s'�tait semble-t-il "dangereusement" �loign� de la secte durant l'interminable tournage (deux ans!) en Angleterre du dernier film de Stanley Kubrick " Eyes wide shut ". La secte, pour r�cup�rer sa star, aurait convaincu Tom Cruise de divorcer de Nicole Kidman. Pire, elle aurait aussi r�ussi � convaincre l'acteur que Nicole Kidman pouvait lui nuire, m�me apr�s leur s�paration. C'est pour v�rifier la chose que le t�l�phone de l'actrice australienne aurait �t� �cout�. Le film d'Alex Gibney explique, par ailleurs, que la secte, une fois la rupture totalement consomm�e entre les deux stars, aurait vainement entrepris de "r��duquer" les enfants du couple, Connor et Isabella pour que ceux-ci tournent le dos � leur m�re.
�R�agissez
