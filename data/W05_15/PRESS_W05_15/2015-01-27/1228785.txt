TITRE: Un homme touch� par balles dans le quartier de la Castellane � metronews
DATE: 27-01-2015
URL: http://www.metronews.fr/marseille/un-homme-touche-par-balles-dans-le-quartier-de-la-castellane/moaA!yvBb3KmKvKXQI/
PRINCIPAL: 1228783
TEXT:
Cr�� : 27-01-2015 14:32
Un homme touch� par balles dans le quartier de la Castellane
FAIT DIVERS - Un homme de 26 ans a �t� bless� par balles mardi dans une cit� des quartiers Nord de Marseille, a-t-on appris de source proche du dossier.
Tweet
�
L'nequ�te pourrait �tre confi�e � la police judiciaire de Marseille en charge de dossiers similaires. Photo :�G�rard Julien / AFP
Nouveau coup de feu dans le quartier de la Castellane. Mardi, vers 12h30, un homme de 26 ans a �t� bless� par balles dans cette cit� marseillaise du 16�me arrondissement.
Touch� au niveau des jambes selon La Provence , ses jours ne seraient pas en danger. Il a �t� imm�diatement pris en charge par les marins-pompiers de Marseille.
Une deuxi�me victime en deux semaines
On ignore encore pour le moment les raisons de ces coups de feu indique une source proche du dossier.
C'est la deuxi�me personne touch�e par balles en moins de deux semaines dans le m�me secteur. Le 15 janvier dernier, un homme avait �t� abattu d'une balle dans la t�te dans ce quartier connu pour son trafic de drogue.�
Eric Miguet
