TITRE: Faits divers | Le nombre d�??actes antisémites a doublé en 2014
DATE: 27-01-2015
URL: http://www.lejsl.com/faits-divers/2015/01/27/le-nombre-d-actes-antisemites-a-double-en-2014
PRINCIPAL: 1228171
TEXT:
Le nombre d�??actes antisémites a doublé en 2014
- Publié le 27/01/2015
SOCI�?T�? Le nombre d�??actes antisémites a doublé en 2014
Le nombre des actes antisémites a doublé (+101%) en 2014 par rapport à 2013 en France, avec même une augmentation de 130% des actes avec violences physiques, a annoncé mardi le Conseil représentatif des institutions juives de France (Crif), qui estime que "le point critique a largement été dépassé".
Le président du Crif, Roger Cukierman. Photo AFP/Dominique FAGET
Selon le Crif, qui cite des chiffres du Service de protection de la communauté juive (SPCJ) basés sur des données du ministère de l�??Intérieur, 851 actes antisémites ont été recensés en 2014, contre 423 en 2013.
"Ces actes antisémites représentent 51% des actes racistes commis en France, alors que les Juifs ne sont que moins de 1% de la population française", indique le Crif dans un communiqué. La communauté juive de France - la première en Europe et la troisième dans le monde après Israël et les Etats-Unis - est estimée à 500 000 à 600 000 personnes.
Fait particulièrement notable, le nombre d�??actes avec violences physiques est passé de 105 en 2013 à 241 en 2014, soit une augmentation de 130%. "Il résulte de ces chiffres un accroissement très important et très préoccupant de la violence des actes antisémites", note l�??organe de représentation politique de la communauté juive de France, qui estime que "cette tendance confirme bien malheureusement la persistance, voir le renforcement des préjuges antisémites en France, parfois leur radicalité croissante qui fait qu�??on passe de l�??insulte à la violence, de la violence au terrorisme".
Le président du Crif, Roger Cukierman, cité dans le communiqué, "espère que des mesures puissantes et fortes seront prises dans les domaines de la prévention, la protection et l�??éducation" pour endiguer la hausse de ces actes antisémites.
AFP
Poster un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
