TITRE: Logements neufs. 200 000 chantiers fant�mes
DATE: 27-01-2015
URL: http://www.ouest-france.fr/logements-neufs-200-000-chantiers-fantomes-3145450
PRINCIPAL: 1229173
TEXT:
Logements neufs. 200 000 chantiers fant�mes
France -
Achetez votre journal num�rique
500 000 logements neufs devaient �tre construits en 2014 pour r�pondre � l'objectif du gouvernement. 200 000 constructions manquent � l'appel.
L'ann�e 2014 a �t� tr�s mauvaise pour la construction de logements neufs en France, le nombre de mises en chantier passant en-de�� de la barre symbolique des 300 000 logements pour la premi�re fois depuis 17 ans, alors qu'un petit rebond est esp�r� cette ann�e.�
200 000 logements manquants
Apr�s un recul de 4,2�% en 2013, les mises en chantier ont chut� de 10,3�% l'an dernier pour s'afficher � 297 532, contre un objectif gouvernemental de 500 000, a annonc� mardi le minist�re du Logement.
��Nous nous attendions � une tr�s mauvaise ann�e, elle est conforme � ce que nous redoutions : nous sommes pass�s sous la barre des 300 000 logements��, commente Fran�ois Payelle, le pr�sident de la F�d�ration des promoteurs immobiliers (FPI).
��Il manque des centaines de milliers de logements en France, et non seulement on ne comble pas le retard, mais on l'aggrave��, d�plore-t-il.
Lire : Logements neufs : des constructions en baisse �
Les plus mauvais chiffres depuis 1997
Ce chiffre est aussi nettement inf�rieur � la moyenne des dix derni�res ann�es -- de l'ordre de 347 000 logements -- et il faut remonter 17 ans en arri�re, � 1997 pour retrouver un niveau aussi faible (287 104 mises en chantier).
��La baisse est assez marqu�e cette ann�e, la construction a suivi la correction assez forte des ventes de logements neufs��, observe Olivier Elu�re, �conomiste de Cr�dit Agricole SA. Par rapport au plus haut de 2007, les ventes ont ainsi recul� de 37 � 40�% tandis que les mises en chantier ont baiss� de 36�%, calcule-t-il.
De 3 300�euros � 3 900�euros le m2
��Du c�t� des m�nages acc�dants classiques, un cumul de facteurs n�gatifs a jou� en 2014 : la hausse du ch�mage, les perspectives jug�es tr�s incertaines, le poids des normes (qui rench�rissent les co�ts de construction, ndlr) et le niveau �lev� des prix�� de l'immobilier neuf, �num�re-t-il.
Ainsi le prix d'un appartement neuf est-il en moyenne de 3 900�euros le m2 en France contre environ 3 300�euros le m2 pour un appartement ancien, d'o� un arbitrage des m�nages ��en faveur de l'ancien��.
Lire : La vente de logements neufs � la peine sur le littoral breton
Le march� du neuf sur�valu� de 25%
��Les prix sont toujours sur�valu�s��, rench�rit Sophie Tahiri, �conomiste de Standard and Poor's. ��Depuis le pic de 2011, ils n'ont baiss� que de 5�%, alors qu'ils avaient grimp� de 125�% depuis le d�but des ann�es 2000��.
Si l'on rapporte cette envol�e � la faible croissance des revenus des m�nages, sur une moyenne de long terme, ��le march� est sur�valu� de 25�%��.
Hausse de la fiscalit�
De fait, loin d'�tre g�n�ralis�e, la correction du march� immobilier a �t� plus forte dans le neuf tandis que l'ancien conservait des niveaux d'activit� �lev�s (quelque 700 000 transactions annuelles).
En outre la chute des taux des cr�dits immobiliers � leur plus bas depuis les ann�es 1940, et l'allongement de la dur�e moyenne des pr�ts a solvabilis� une partie des m�nages primo-acc�dants.�
Du c�t� des investisseurs, qui ach�tent un logement neuf pour le louer, ��la hausse de la fiscalit� sur les plus-values et les aspects plus contraignants du dispositif "Duflot" , par rapport au "Scellier" , ont fait chuter les ventes�� de logements neufs, rappelle M. Elu�re.
�Lire : Le dispositif Duflot inqui�te les promoteurs de Bretagne
Mesures de relance
Pour soutenir l'activit� du secteur, le gouvernement a pris des mesures de relance, ax�es notamment sur l'am�lioration du Pr�t � taux z�ro (PTZ), l'am�nagement du dispositif fiscal ��Pinel�� (ex ��Duflot��) pour l'investissement locatif et la simplification des normes de construction.
Petit rebond esp�r�
Ces mesures adopt�es dans le budget 2 015 ��corrigent un peu tous les facteurs n�gatifs qui ont pes� sur la construction en 2014, avec notamment des dispositions fiscales, et d'autres visant � faire baisser les co�ts et � r�duire les d�lais��, note Mme Tahiri.
M. Elu�re en esp�re ��un petit rebond du nombre de ventes de logements neufs promoteurs (commercialis�s par ces derniers, ndlr), de l'ordre de 5�%, cette ann�e��.
��Mais cela ne repr�sente qu'un tiers des mises en chantier. Si de leur c�t� les ventes de maisons individuelles restent stables, on pourrait avoir une petite hausse de 3�% des mises en chantier��, pronostique-t-il.
Simplification des normes
Toutefois le nombre de permis de construire a d�gringol� de 12�% pour s'�tablir � 381 075 l'an dernier, ce qui ne laisse pas augurer d'am�lioration dans les tout prochains mois.
A la FPI, M. Payelle esp�re une am�lioration des ventes au premier semestre 2015, et juge qu'��il faut aller beaucoup plus loin�� dans la simplification des normes.
��Mais ce qui nous inqui�te fortement, c'est la volont� profonde d'un certain nombre de collectivit�s locales, partout en France, de ralentir, voire de stopper tr�s fortement la construction de logements��, souligne-t-il, appelant l'Etat � ��reprendre la main�� en modulant ses dotations en fonction de ce crit�re.
Tags :
