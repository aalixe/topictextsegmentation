TITRE: L'histoire : Le jour o� Elisabeth II a terroris� le roi Abdallah. Info - Saint-Brieuc.maville.com
DATE: 27-01-2015
URL: http://www.saint-brieuc.maville.com/actu/actudet_-l-histoire-le-jour-ou-elisabeth-ii-a-terrorise-le-roi-abdallah_54028-2703342_actu.Htm
PRINCIPAL: 0
TEXT:
Google +
On ne plaisante pas avec Elisabeth II...� Photo Reuters.
Le roi Abdallah d'Arabie Saoudite est d�c�d� vendredi dernier. L'occasion pour un diplomate anglais de raconter une anecdote savoureuse, survenue en 1998...
Sir Sherard Cowper-Coles, ambassadeur du Royaume-Uni en Arabie Saoudite en 2003, fait le r�cit du dr�le de tour que la reine d'Angleterre a jou� au roi Abdallah d'Arabie Saoudite. Il tient l'histoire d'Elisabeth II elle-m�me...
En 1998, le roi Abdallah est re�u par la reine dans son domaine de Balmoral en Ecosse. Apr�s le repas, Elisabeth II propose � son invit� de faire le tour de la propri�t�. H�sitant dans un premier temps, Abdallah accepte l'invitation, sous la pression de ses diplomates.
F�ministe...
Des Land Rovers sont gar�s devant le palais, et conform�ment aux instructions, Abdallah s'installe dans le v�hicule de t�te, sur le fauteuil passager. Son interpr�te prend place sur la banquette arri�re. C'est l� que le cours normal des choses fl�chit : � la surprise g�n�rale des Saoudiens, la reine d'Angleterre s'installe au volant, et met le contact...
Il faut rappeler qu'en Arabie Saoudite, les femmes n'ont pas le droit de conduire. Abdallah n'est pas habitu� � �tre conduit par une femme. La f�ministe Elisabeth II a trouv� ainsi une mani�re tr�s subtile de faire passer un message ! Qui d'autre que la Reine d'Angleterre pouvait se permettre cela ?
et pilote chevronn�e !
Et ce n'est pas tout... Cherry on the cake, il s'av�re qu'Elisabeth II se d�brouille derri�re un volant. Pour m�moire, � la fin de la Seconde guerre mondiale, elle a re�u un entra�nement militaire � la conduite de camions et autres ambulances :
La reine en 1945, en tenue militaire.�|�Photo DR
C'est donc avec une conduite sportive qu'Elisabeth II a emmen� son invit� sur les petites routes �cossaises de Balmoral, tout en n'arr�tant pas de parler. Abdallah p�trifi�, a demand� � son interpr�te d'inviter la Reine � ralentir et se concentrer sur sa conduite...
F�ministe et pilote, Elisabeth II a fait passer un message...�|�Photo Reuters.
Ouest-France��
