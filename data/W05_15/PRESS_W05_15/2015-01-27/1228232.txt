TITRE: [M�J]Internet fixe�: option TV obligatoire et payante chez SFR
DATE: 27-01-2015
URL: http://www.lesnumeriques.com/internet-fixe-option-tv-obligatoire-payante-chez-sfr-n38921.html
PRINCIPAL: 1228227
TEXT:
Publi� le 26 janvier 2015 14:16
[M�J]Internet fixe�: option TV obligatoire et payante chez SFR
Tout est dans l'aplomb avec lequel on ose
SFR, fra�chement aval� par Numericable, a r�cemment proc�d� � une modification de sa brochure tarifaire � destination des clients du fixe. Curieusement, l'op�rateur a notamment introduit une option payante et obligatoire...
�
Mise � jour
26/01/2015 � 15:15
SFR nous fait savoir que "cette nouveaut� fait suite � un changement d'offres" et assure que "cela n'influe pas sur la redevance TV pay�e par les clients". On sait, n�anmoins, que les FAI peuvent avoir � fournir le d�tail de leurs abonn�s pour �valuer une demande d'exon�ration de redevance audiovisuelle. De m�me, cela ne change rien � la situation de nombreux abonn�s�: ils se retrouvent avec une option payante non sollicit�e.
�
Le Dual Play est une chose du pass�, chez SFR. En effet, les clients qui ne b�n�ficient ni d'une offre comprenant la t�l�vision (Triple Play), ni d'une option compensant cette absence�(2�� pour la "TV classique", 3�� pour la "TV��volution" ou "TV SFR avec Google Play")�se voient d�sormais impos� de "souscrire" � une option�TV. Baptis�e�"TV sur smartphone, tablette et PC", celle-ci est factur�e 1�� par mois et est dor�navant�"obligatoire pour les clients non d�tenteurs d'une option TV".�En clair, quoi qu'il fasse, l'abonn� doit payer pour la t�l�vision���et donc, par ricochet, la redevance.
� en croire la fiche tarifaire, la facturation de cette "option" devait d�marrer le 17 mars prochain pour les nouveaux clients. L'op�rateur, n�anmoins, semble avoir d�cid� de prendre les devants avec sa client�le existante. En effet, de nombreux abonn�s se plaignent d'avoir re�u d�s janvier une facture gonfl�e d'un euro ���et ce sans aucune sollicitation, naturellement.�La bonne nouvelle, toutefois, est qu'il s'agit d'une modification manifeste des conditions contractuelles, ce qui devrait donc permettre aux clients de r�silier sans frais.
