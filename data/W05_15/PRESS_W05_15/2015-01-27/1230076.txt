TITRE: Comment avoir l�assistant vocal Cortana sur la version fran�aise de Windows 10
DATE: 27-01-2015
URL: http://www.01net.com/editorial/643152/comment-avoir-l-assistant-vocal-cortana-sur-la-version-francaise-de-windows-10/
PRINCIPAL: 1230075
TEXT:
agrandir la photo
Vous avez install� la version fran�aise de Windows 10 et �tes d��u que Cortana ne fonctionne pas ? Rassurez-vous, il est assez facile, si vous connaissez un peu la langue de Shakespeare, de tester l�assistant vocal de Microsoft tout de m�me. Seule chose � faire�: basculer votre version de Windows 10 provisoirement en Anglais.
Pour cela, rendez-vous simplement dans ��Settings�� depuis le menu d�marrer. Cliquez alors sur ��heure et langue��. Modifiez d�abord votre fuseau horaire dans date et heure pour vous caler sur un horaire am�ricain. Rendez-vous ensuite sur ��R�gion et langue��, feignez d��tre aux Etats-Unis dans la rubrique ��Pays�� puis choisissez et ajoutez English�: United States dans ��langues��.
L�, il vous faudra patienter un petit peu, le temps que Windows t�l�charge le module linguistique correspondant (qui contient notamment les donn�es de reconnaissance et de synth�se vocale). Cela fait, il faut encore que vous d�finissiez l�Anglais comme langue principale, toujours dans la m�me fen�tre.
Derni�re �tape�: red�marrez votre machine. Si tout va bien, Cortana devrait appara�tre dans l�espace de recherche, par�e pour r�pondre � vos questions. Et pour �pater vos amis, rien de mieux que de commencer en lui demandant de vous chanter une chanson. Cliquez sur l�ic�ne ��micro��, dites ��sing me a song�� de votre plus bel accent anglais, et �coutez le r�sultat�!
envoyer
