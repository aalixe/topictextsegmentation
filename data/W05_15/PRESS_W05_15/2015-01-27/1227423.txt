TITRE: France/Monde | Facebook, victime d�une panne mondiale d�ampleur, d�ment toute cyberattaque
DATE: 27-01-2015
URL: http://www.ledauphine.com/france-monde/2015/01/27/facebook-actuellement-inaccessible-pour-de-nombreux-utilisateurs
PRINCIPAL: 0
TEXT:
INTERNET Facebook, victime d�une panne mondiale d�ampleur, d�ment toute cyberattaque
-
Le r�seau social Facebook, qui revendique plus d�1,3 milliard d�abonn�s dans le monde, a connu mardi une panne d�ampleur, longue de cinquante minutes, mais a d�menti toute attaque de la part de pirates informatiques.
Entre 7 heures 20 et 8 heures 05 environ, Facebook et sa filiale Instagram ont connu de tr�s importants probl�mes de connexion, des utilisateurs du monde entier relayant imm�diatement l�information sur Twitter.
"Beaucoup de personnes ont eu du mal � acc�der � Facebook et Instagram. Cela ne r�sultait pas d�une attaque de tiers mais s�est produit apr�s l�introduction d�un changement qui a affect� nos syst�mes de configuration. Nous avons rapidement r�solu le probl�me et les deux services fonctionnent � nouveau � 100% pour tout le monde", a affirm� Facebook dans un communiqu�, sans donner de d�tails.
Il a ainsi voulu faire taire les rumeurs circulant sur internet au sujet d�un �ventuel piratage de ses syst�mes, notamment apr�s un tweet ambigu d�un groupe de hackers, Lizard Squad, que certains m�dias ont fait passer pour une revendication: "Facebook, Instagram, Tinder, AIM, Hipchat #offline #LizardSquad".
"Ce groupe est tr�s visible ces temps-ci, et leur responsabilit� est notamment av�r�e dans le piratage de Playstation et Xbox � No�l", a comment� G�r�me Billois, expert en cybercriminalit� au cabinet Solucom. "Ils ont donc tout int�r�t � revendiquer la panne de Facebook m�me si ce n�est pas vrai, car cela leur offre un formidable coup de communication dans les m�dias et sur internet. Mais habituellement, les grands acteurs de l�internet ont tendance � le dire lorsqu�ils sont victimes d�une attaque en d�ni de service" par des pirates informatiques qui bombardent de demandes un site jusqu�� ce qu�il tombe, juge M. Billois.
"Droit dans ses bottes"
"Si Facebook d�ment une attaque informatique, c�est qu�il est droit dans ses bottes, il ne prendrait pas le risque de d�mentir une telle information", rench�rit J�r�me Robert, directeur marketing de la soci�t� Lexsi.
"Quant � Lizard Squad, leur tweet n�est pas vraiment une revendication. Ils ont certes un tableau de chasse mais rien du niveau de Facebook. Mais � la question Facebook peut-il �tre hack� ?, la r�ponse est oui, m�me si en cas d�attaque de d�ni de service le groupe est capable de basculer tr�s rapidement" ses infrastructures pour les pr�server, ajoute M. Robert.
Un op�rateur mondial de r�seaux a indiqu� de son c�t� avoir constat� "d�s 7 heures 05 que Facebook n�envoyait plus de trafic sur certains points d��change, dont celui de Palo Alto (en Californie, ndlr), qui est le principal point d��change internet de la Silicon Valley" et �galement un des principaux noeuds d�interconnexion de trafic au niveau mondial.
Une des possiblit�s est que Facebook ait proc�d� � des changements internes de configuration sur un ou plusieurs de ses routeurs (qui constituent des �quipements cl�s pour tout r�seau informatique), et qu�un probl�me technique s�est alors produit et a "plant�" l��mission de trafic, avance-t-il.
En mars 2012, la toute premi�re panne d�ampleur du r�seau social depuis sa cr�ation avait priv� ses utilisateurs de connection pendant environ deux heures. Plus r�cemment, en juin dernier, Facebook avait subi une importante baisse de trafic pendant une vingtaine de minutes.
MORE: Facebook, Instagram outage affecting users in U.S., Asia, Australia: http://t.co/GAHuxLOaqB
; The Associated Press (@AP) 27 Janvier 2015
La liste s�allonge : AIM, Tinder, Hipchat viennent s�ajouter � Facebook et Instagram #down pic.twitter.com/on4dgHGxTo
; Breaking 3.0 (@Breaking3zero) 27 Janvier 2015
Message publi� sur le compte Twitter @instagram, filiale de Facebook�: "Nous sommes conscients qu'une panne affecte Instagram et nous travaillons sur un correctif. Nous vous remercions de votre patience."
We're aware of an outage affecting Instagram and are working on a fix. Thank you for your patience.
Par AFP | Publi� le 27/01/2015 � 08:11
Vos commentaires
