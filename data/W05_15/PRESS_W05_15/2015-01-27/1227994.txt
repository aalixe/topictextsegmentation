TITRE: Michel Sapin apr�s la victoire de Syriza en Gr�ce : "Jean-Luc M�lenchon se fait des illusions" - leJDD.fr
DATE: 27-01-2015
URL: http://www.lejdd.fr/Politique/Michel-Sapin-apres-la-victoire-de-Syriza-en-Grece-Jean-Luc-Melenchon-se-fait-des-illusions-715008
PRINCIPAL: 1227993
TEXT:
27 janvier 2015 �|� Mise � jour le 28 janvier 2015
6 Commentaires
Michel Sapin : "Jean-Luc M�lenchon se fait des illusions"
L'EXPRESSO POLITIQUE -�LeJDD.fr�vous propose un condens� de l'info politique de mardi matin.
Pour Michel Sapin, Jean-Luc M�lenchon est encore loin d'Alexis Tsipras. (Sipa)
L�attaque
Michel Sapin refuse de comparer Syriza en Gr�ce au Front de gauche en France . "Jean-Luc M�lenchon se fait des illusions ou il est dans l'illusion. Syriza est composite (...) C'est une alliance qui est beaucoup plus large", explique le ministre des Finances dans Lib�ration . "Je vois une tr�s grande diff�rence entre (Alexis Tsipras) qui parle de ren�gociation et M�lenchon qui claironne qu'il veut une annulation de la dette. L'annulation est une aberration, la ren�gociation est sur la table", d�clare le socialiste.
Lire aussi : Ce que les politiques fran�ais disent de Syriza, et la r�alit�
Le pronostic
"En politique, la d�ception est aussi souvent � gauche qu'� droite". Sur RTL, Jean-Pierre Raffarin s'est montr� pessimiste sur l'arriv�e au pouvoir de Syriza en Gr�ce .�"(Alexis) Tsipras a-t-il les moyens de sa politique? Je ne pense pas", a affirm� l'ancien Premier ministre, affirmant que le nouveau gouvernement ne pourra n�gocier qu'"� la marge" sur sa dette et cela provoquera "une forte d�ception de l'opinion".
La phrase
"La France s'est battue � l'�t� 2012 pour que la Gr�ce ne sorte pas de la zone euro. J'�tais bien plac� � l'�poque pour le voir : sans Fran�ois Hollande et son action r�solue, la Gr�ce serait vraisemblablement sortie de l'euro", a affirm� sur Europe 1 le ministre de l'Economie Emmanuel Macron, qui a d�fendu la place du pays dans la zone euro.
Lire aussi : L'Europe retient son souffle face � Syriza
La position
Le vote de�Fran�ois Fillon en faveur de la loi Macron "d�pendra de l'attitude du gouvernement et de la majorit� dans ce d�bat". Mais l'ancien Premier ministre, qui a port� pour l'UMP la contradiction au ministre de l'Economie lundi � l'Assembl�e nationale , a jug� sur France Info que ce dernier propose des rem�des "hom�opathique". Le d�put� de Paris veut que gauche et droite soient "capables de faire preuve d'un peu d'intelligence" pour s'entendre sur un programme �conomique commun.
Lire aussi : Emmanuel Macron, le "chouchou" face aux frondeurs
La pr�cision
"On ne touche pas au droit du licenciement, contrairement � ce que laisse entendre la CGT", assure dans Le Parisien le rapporteur PS de la loi Macron, l'ancien "frondeur" Denys Robiliard. Les syndicats estiment en effet que le projet de loi favorisera le licenciement �conomique. "C'est bien de critiquer, mais il faut �tre de bonne foi", r�pond le d�put� du Loir-et-Cher, qui assure que "plusieurs amendements ont �t� pris � la suite des inqui�tudes formul�es par les syndicats".
Le sondage
Le barom�tre Odoxa-Orange/Presse r�gionale/France Inter/L'Express confirme � son tour un bond de popularit� du tandem ex�cutif. Selon cette enqu�te, r�alis�e par Internet les 22 et 23 janvier, la cote de popularit� de Fran�ois Hollande a grimp� de 10 points en janvier, atteignant 31%, et celle de Manuel Valls gagn� encore 8 points, � 53%. Dimanche selon notre barom�tre Ifop (r�alis� du 16 au 24 janvier) qui date du d�but de la Ve R�publique, le Pr�sident gagnait 12 points (29% de satisfaits) et son Premier ministre, 18 (53% de satisfaits).
Lire aussi :
Fran�ois Hollande sur son gain de popularit� : "Le regard des Fran�ais change"
La visite
Nicolas Sarkozy sera en "immersion" jeudi � Tourcoing, dans la ville de son ancien porte-parole G�rald Darmanin, indique Le Parisien . Ce sera la premi�re visite de terrain du pr�sident de l'UMP depuis son �lection. L'ex-Pr�sident y rencontrera "une quinzaine de Fran�ais", pr�cise le quotidien.
Arnaud Focraud - leJDD.fr
