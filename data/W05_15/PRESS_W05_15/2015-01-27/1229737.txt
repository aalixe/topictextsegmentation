TITRE: S�gol�ne Royal d�sign�e "ministre de l'ann�e" par le Trombinoscope
DATE: 27-01-2015
URL: http://www.francetvinfo.fr/politique/segolene-royal-designee-ministre-de-l-annee-par-le-trombinoscope_808507.html
PRINCIPAL: 1229730
TEXT:
S�gol�ne Royal d�sign�e "ministre de l'ann�e" par le Trombinoscope
Cet annuaire du monde politique d�cerne chaque ann�e des prix aux personnalit�s qui ont marqu� l'actualit�.
La ministre de l'Ecologie, S�gol�ne Royal, le 15 janvier 2015, lors de sa c�r�monie de v�ux � la presse organis�e � Paris. (  MAXPPP)
Par Francetv info avec AFP
Mis � jour le
, publi� le
27/01/2015 | 17:44
De tous les membres du gouvernement, c'est sans doute celle qui se d�marque le plus. S�gol�ne Royal a �t� d�sign�e, lundi 26 janvier, "ministre de l'ann�e" dans le�palmar�s annuel du�Trombinoscope. Cet�annuaire du monde politique d�cerne chaque ann�e des prix aux personnalit�s qui ont marqu� l'actualit�.
C'est qu'en 2014,�la ministre de l'Ecologie s'est souvent d�marqu�e du reste de ses coll�gues, quitte � faire des propositions surprenantes au regard de son poste. Outre l'enterrement fracassant de l'�cotaxe poids lourds , elle a propos� en octobre la gratuit� des autoroutes le week-end , avant de batailler deux mois plus tard contre�l'arr�t� pr�fectoral interdisant les feux de chemin�e � foyer ouvert en�Ile-de-France, malgr� les �tudes indiquant qu'il s'agit d'une source importante de pollution.
Une "v�ritable super ministre"
Un style que l'ancienne candidate � la pr�sidentielle de 2012 n'a pas h�sit� � revendiquer dans une interview � Paris Match parue mi-mai.�"Ceux qui veulent me museler se trompent. Oui, je parle. C'est ma libert� et je la garderai quoi qu'il arrive", avait-elle alors lanc�.�De quoi en faire, selon la journaliste Arlette Chabot, membre du Trombinoscope, une "v�ritable super ministre".�
Sur le m�me sujet
