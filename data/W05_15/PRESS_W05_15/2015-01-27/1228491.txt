TITRE: BERLINALE 2015: le jury au complet ! :: FilmDeCulte
DATE: 27-01-2015
URL: http://www.filmdeculte.com/cinema/actualite/BERLINALE-2015-le-jury-au-complet-20856.html
PRINCIPAL: 1228488
TEXT:
Publi� le 27/01/2015
BERLINALE 2015: le jury au complet !
Le jury de la prochaine Berlinale est d�sormais au complet. On savait d�j� que le r�alisateur am�ricain Darren Aronofsky allait pr�sider ce jury.
Il sera entour� du r�alisateur cor�en Bong Joon-Ho, de l'actrice fran�aise Audrey Tautou, de l'acteur allemand Daniel Br�hl, de la productrice am�ricaine Martha de Laurentiis (co-fondatrice avec Dino De Laurentiis de la Dino De Laurentiis Company, productrice de nombreuses s�ries B dans les ann�es 80 et 90 et plus r�cemment de la s�rie Hannibal), du sc�nariste et producteur am�ricain Matthew Weiner (Les Sopranos, Mad Men) et de la r�alisatrice p�ruvienne Claudia Llosa (qui a sign� l'Ours en carton Fausta et �tait � nouveau en comp�tition l'an pass� avec le catastrophique Aloft ).
La Berlinale aura lieu du 5 au 15 f�vrier et sera � suivre en direct sur FilmDeCulte. Qui succ�dera au Chinois Black Coal, qui fut ensuite un gros succ�s en Chine et un succ�s surprise en France ? Retrouvez la s�lection compl�te en cliquant ici .
Source: Berlinale
Et pour ne rien manquer de nos news, dossiers, critiques et entretiens consacr�s � la Berlinale, rejoignez-nous sur Facebook et Twitter !
