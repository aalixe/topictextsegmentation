TITRE: Volvo Ocean Race: Dongfeng triomphe � domicile - Voile - Sports.fr
DATE: 27-01-2015
URL: http://www.sports.fr/voile/scans/volvo-ocean-race-dongfeng-triomphe-a-domicile-1174583/
PRINCIPAL: 1228082
TEXT:
27 janvier 2015 � 08h50
Mis à jour le
27 janvier 2015 � 08h51
Ils l’ont fait ! Charles Caudrelier et ses hommes, parmi eux quatre Français (Thomas Rouxel, Pascal Bidégorry, Kevin Escoffier et Eric Péron) mais aussi deux Chinois surnommés « Kit » et « Black », ont remporté la troisième étape de la Volvo Ocean Race entre Abu Dhabi (Emirats Arabes Unis) et Sanya (Chine). Dongfeng Race Team a franchi la ligne d’arrivée ce mardi matin à 00h31 (heure française) après 23 jours, 13 heures et 31 minutes de mer.
Cette victoire, qui s’est dessinée dès les premières heures de course, fera date en Chine. C’est la première fois en effet qu’un concurrent chinois remporte une étape de ce tour du monde en équipage et avec escales. Déjà présent lors la précédente édition sous les couleurs de Sanya, mais avec un seul Chinois à bord, n’avait jamais joué aux avant-postes sous les ordres de Mike Sanderson. Charles Caudrelier, vainqueur de la précédente édition à bord de Groupama (skippé par Franck Cammas), a donc réussi le tour de force d’imposer ses hommes devant des équipages plus rompus à la navigation au large après avoir déjoué les pièges du détroit de Malacca (entre la Malaisie et l’Indonésie) puis résisté au retour de la concurrence le long de la côte vietnamienne.
Mieux, Dongfeng Race Team, deuxième des deux premières étapes, s’empare par la même occasion du classement général provisoire de la course qui s’achèvera fin juin à Göteborg (Suède). L’équipage sino-français (5 points) devance Abu Dhabi Ocean Racing (Ian Walker), deuxième à Sanya et donné favori pour la victoire finale.
