TITRE: L'astéroïde qui a frôlé la terre a sa propre lune - 7SUR7.be
DATE: 27-01-2015
URL: http://www.7sur7.be/7s7/fr/1506/Sciences/article/detail/2196512/2015/01/27/L-asteroide-qui-a-frole-la-terre-a-sa-propre-lune.dhtml
PRINCIPAL: 1228817
TEXT:
27/01/15 - 14h12��Source: afp, NASA
� Capture d'�cran.
vid�o Un ast�ro�de a fr�l� la Terre lundi � une distance jamais vue depuis deux si�cles. D'apr�s une vid�o diffus�e par la Nasa, il dispose de sa propre lune.
Cet ast�ro�de d'un demi-kilom�tre de largeur, baptis� 2004 BL86, a crois� la Terre au plus pr�s � 16H19 GMT � 1,2 million de kilom�tres, soit une distance repr�sentant trois fois celle s�parant notre plan�te de la Lune. C'est le plus gros objet c�leste � passer aussi pr�s de notre plan�te depuis deux si�cles. Il n'a repr�sent� aucune menace pour la terre mais s'�tant approch� relativement pr�s, a donn� une occasion unique de l'observer et d'en apprendre davantage.
"Cet ast�ro�de ne repr�sente aucune menace pour la Terre dans le futur mais �tant donn� qu'il s'approchera relativement pr�s il donnera une occasion unique d'observer un objet de cette taille et d'en apprendre davantage", a expliqu� Don Yeomans, directeur du bureau de la Nasa au Jet Propulsion Laboratory (JPL) qui traque les objets dont les orbites passent pr�s de notre plan�te, le "Near Earth Object Program Office".
"Nous ne savons presque rien de cet ast�ro�de ce qui fait que nous pourrions avoir des surprises", indiquait Lance Benner, un scientifique du JPL responsable des observations de cet ast�ro�de.
Le jeu en valait la chandelle puisque d'apr�s les fabuleuses images diffus�es par la NASA (voir vid�o ci-dessous), il s'av�re que l'ast�ro�de a sa propore lune. En effet, on peut apercevoir que l'ast�ro�de 2004 BL86, �tait suivie sur sa trajectoire d'un autre petit satellite d'environ 70 m�tres, indique la NASA sur son site .
Lire aussi
