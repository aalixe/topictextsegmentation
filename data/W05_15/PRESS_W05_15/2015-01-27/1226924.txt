TITRE: Le match � ne pas rater - Tennis - Sports.fr
DATE: 27-01-2015
URL: http://www.sports.fr/tennis/open-d-australie/articles/open-d-australie-berdych-defie-nadal-1174466/?features2tennis
PRINCIPAL: 1226923
TEXT:
26 janvier 2015 � 14h24
Mis à jour le
26 janvier 2015 � 14h33
Seulement quatre matches à Melbourne mardi mais un attire notre attention davantage que les autres. Berdych-Nadal, c’est le choc à ne pas manquer. 
Tomas Berdych – Rafael Nadal
Il fut un temps où Tomas Berdych était l’une des (rares) bêtes noires de Rafael Nadal . Ce temps est révolu. Après avoir battu l’Espagnol trois fois de suite en 2005 et 2006, le Tchèque s’est ramassé les seize fois qui ont suivi. Alors, à l’heure de retrouver l’Espagnol à Melbourne, en quarts de finale comme en 2012, le 7e joueur mondial n’est forcément pas très confiant, pourtant il a des raisons d’y croire.
Avant de parler de son adversaire, il faut déjà regarder le parcours de Berdych en Australie. Falla, Melzer, Troicki et Tomic ont déjà fait les frais de la forme du Tchèque. Sur une surface rapide qui lui sied très bien, l’ancien boyfriend de Lucie Safarova n’a pas perdu un set en quatre matches. Déjà bien en jambes au début du mois à Doha , où il avait seulement calé en finale face à Ferrer,  il propose un jeu très offensif et solide depuis le début de la quinzaine.
On ne peut pas en dire autant de Nadal, passé tout près du crash au deuxième tour. S’il s’est montré meilleur après s’être extirpé des griffes de Smyczek, le Majorquin, à la recherche de sa forme optimale, n’a pas encore donné l’impression de pouvoir aller au bout, comme il l’a fait une seule fois ici, en 2009. Face aux puissants coups de Berdych, Nadal ne pourra pas s’en sortir s’il ne retrouve pas, notamment, sa longueur de balle. Il aura toutefois un avantage considérable en sa faveur, celui, psychologique, qui découle de leur bilan (17-3).
Rod Laver Arena
A 11h (1h en France)
Ekaterina Makarova (RUS/n°10) – Simona Halep (ROU/n°3)
Pas avant 12h30 (2h30 en France)
Eugenie Bouchard (CAN/n°7) – Maria Sharapova (RUS/n°2)
Tomas Berdych (RTC/n°7) – Rafael Nadal (ESP/n°3)
Pas avant 19h15 (9h15 en France)
Andy Murray (GBR/n°6) – Nick Kyrgios (AUS)
