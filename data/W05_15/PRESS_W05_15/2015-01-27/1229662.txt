TITRE: Schladming-Slalom (M): Khoroshilov devant, les Bleus � la tra�ne
DATE: 27-01-2015
URL: http://www.beinsports.fr/news/title/schladming-slalom-m-khoroshilov-devant-les-bleus-a-la-traine/article/1i6bx8ona08wx1xbyfhpom011q
PRINCIPAL: 1229659
TEXT:
Schladming-Slalom (M): Khoroshilov devant, les Bleus � la tra�ne
Publi� le 27 Janvier 2015, � 18h46
A 35 ans, Julien Lizeroux prouve qu'il est capable aujourd'hui de se reconstruire � un �ge avanc�.
Vainqueur du Super-combin� de Kitzb�hel il y a quatre jours, Alexis Pinturault n'a que peu d'espoirs de r��diter une telle performance apr�s la premi�re manche du slalom de Schladming. Le leader de l'�quipe de France, parti avec le dossard 15, termine � 2.93 secondes du vainqueur, le Russe Alexander Koroshilov, pour terminer � la 23e place.
L'Allemand Felix Neureuther, qui domine le classement de la sp�cialit�, pointe lui au 2e rang � 0.79, alors que l'Autrichien Marcel Hirscher est 4e derri�re son compatriote, Fritz Dopfer, � 1.34. Les autres Fran�ais, Julien Lizeroux, Jean-Baptiste Grange, Victor Muffat-Jeandet et Steve Missillier font un tir group� devant le skieur de Mo�tiers, pour prendre les 17e, 18e, 19e et 26e places.
