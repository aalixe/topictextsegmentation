TITRE: ��Grim Fandango��, un classique du jeu vid�o revient d'entre les morts
DATE: 27-01-2015
URL: http://www.lemonde.fr/pixels/article/2015/01/27/grim-fandango-un-classique-du-jeu-video-revient-d-entre-les-morts_4564315_4408996.html
PRINCIPAL: 0
TEXT:
��Grim Fandango��, un classique du jeu vid�o revient d'entre les morts
Le Monde |
27.01.2015 � 18h45
| Par William Audureau
��Mythique��, ��une ambiance unique��, ��joie, rire, bonheur et jazz��� Plus de quinze ans apr�s sa sortie, en�1998, Grim Fandango continue de hanter la m�moire de ceux qui s'y sont essay�, avec son univers d�cal� � la Tim Burton, son humour caustique et ses dialogues spirituels.
Il ressort mardi 27 janvier sur PlayStation�4, PSVita, Mac, PC Windows et Linux dans une version remasteris�e. L'image est restaur�e en haute d�finition, le format ajustable � un �cran 16:9, et langues et sous-titres sont personnalisables. Seul b�mol�: le fichier est tr�s lourd (pr�s de 5�Go). N�anmoins, le jeu vid�o a�rarement aussi bien racont� son histoire�: bonus rare et pr�cieux, des commentaires de d�veloppeurs accompagnent d�sormais la partie du joueur s'il le souhaite.
Cette r��dition pousse le vice jusqu'� r�server un prestigieux troph�e � qui finira l'aventure avec le p�nible syst�me de commande d'origine, affectueusement surnomm� ��mode tank��, tandis que de nouveaux contr�les bien plus naturels sont d�sormais propos�s.
Le charme des jeux LucasArt
Manette en main, le charme du jeu original op�re toujours. Le joueur incarne Manny Calavera, commercial de seconde zone dans une compagnie de voyages sp�cialis�e dans les offres pour les d�funts de fra�che date. Accessoirement, Manny est un squelette, qui appartient au monde de l'au-del�, et c'est d�guis� en Camarde avec une capuche et une faux qu'il vient r�colter ses ��clients�� dans le monde des vivants.
��Il est normalement interdit de faire peur aux vivants, mais on le fait tous��, savoure le h�ros au d�tour d'une sc�ne chez les non-morts, avec ce ton d�licieusement flegmatique qui est le sien. Petit bijou d'humour � la fois noir et absurde, l'aventure poss�de le charme des jeux LucasArts, ancien studio sp�cialis� dans les jeux d'enqu�te loufoques et alambiqu�s. Son auteur, Tim Schaffer (Maniac Mansion, Monkey Island, Full Throttle�), en reste d'ailleurs vingt ans plus tard le ma�tre incontest�.
Des �nigmes volontiers alambiqu�es
Grim Fandango propose ainsi la quintessence du ��point & click��, ce genre � la logique interne si capillotract�e, que pour avancer il est souvent bien utile d'avoir la solution du jeu sur les genoux ��ou un tube d'aspirine.
Par exemple, pour intercepter l'ordre de mission d'un coll�gue un peu trop dou�, il vous faudra aller discuter avec un clown, lui demander de vous offrir deux ballons gonflables en forme de ver de terre, les remplir avec des fluides mortuaires, les ins�rer dans un tube � message, trouer un jeu de cartes avec une perforatrice et l'utiliser pour acc�der � la salle des machines.
En 1998 d�j�, le jeu �tait r�put� pour ses �nigmes infernales. En�2015, bonne nouvelle�: de nombreux guides pour les r�soudre sont disponibles sur Internet. Quant � ceux qui souhaiteraient revivre les prises de t�te d'antan quitte � y passer des heures, pas de souci : au pays des morts, Manny Calavera a tout son temps�
William Audureau
