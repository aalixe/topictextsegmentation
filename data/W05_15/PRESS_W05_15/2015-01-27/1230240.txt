TITRE: Calendrier CAN 2015 : le programme des matchs de la Coupe d'Afrique - Linternaute
DATE: 27-01-2015
URL: http://www.linternaute.com/sport/foot/1211181-can-2015-calendrier-date-resultat-coupe-d-afrique/
PRINCIPAL: 0
TEXT:
Toute l'actualit� Sport
Calendrier, groupes... D�couvrez le programme des matchs de la Coupe d'Afrique des nations 2015, du samedi 17 janvier au dimanche 8 f�vrier. Le Ghana et la C�te d'Ivoire sont au rendez-vous de la finale.
Le calendrier des matchs de la CAN 2015 s'�tend sur trois semaines, la premi�re quinzaine de comp�tition �tant consacr�e aux matchs de poule et la derni�re semaine aux rencontres de la phase finale (quarts de finale, demi-finale, finale). Seize �quipes s'affrontent dans cette comp�tition qui a lieu en Guin�e �quatoriale, apr�s le d�sistement du Maroc en tant que pays organisateur. Ce dernier, inquiet de l'ampleur prise le virus Ebola, avait demand� le report de la comp�tition avant d'�tre destitu�e de l'organisation. L'�quipe marocaine a �t� �galement disqualifi�e du tournoi, au profit de la R�publique d�mocratique du Congo, qui avait termin�e meilleur troisi�me de la phase de qualifications.
Cette Coupe d'Afrique 2015 s'annonce tr�s ouverte en l'absence du tenant du titre, le Nig�ria, qui n'a pas r�ussi � se qualifier pour la comp�tition. Parmi les favoris, difficile de ne pas citer l'Alg�rie, apr�s son beau parcours lors de la derni�re Coupe du monde (�limin�e de justesse en 8e de finale par l'Allemagne, futur vainqueur). La s�lection d�sormais entra�n�e par Christian Gourcuff peut notamment compter sur un trio offensif de tout premier plan avec Brahimi, Feghouli et Slimani. Pour la victoire finale, il faudra �galement compter avec le Cameroun, le Ghana, le S�n�gal ou encore la C�te d'Ivoire qui figurent parmi les �quipes les plus r�guli�res sur le continent africain.
La Guin�e �quatoriale, qui organise la comp�tition, a cr�� la surprise du premier tour. Sa victoire face au Gabon, lors du 3e match de poule, lui a permis de se qualifier pour les quarts de finale. Dans ce m�me groupe A, le Congo, entra�n� par Claude Le Roy s'est �galement qualifi�, gr�ce � sa victoire face au Burkina Faso. Dans le groupe B, la Tunisie, avec une victoire et un match nul est bien partie pour poursuivre l'aventure, en compagnie de la RD Congo, du Cap Vert ou de la Zambie. Dans le Groupe C, c'est pour le moment le S�n�gal qui fait la course en t�te, suivi de pr�s par le Ghana et l'Alg�rie. Le groupe D est le plus ind�cis puisque toutes les rencontres se sont pour le moment termin�es sur le m�me score : 1-1.
Groupe A
17 janvier : Guin�e �quatoriale - Congo : 1-1
17 janvier : Burkina Faso - Gabon : 0-2
21 janvier : Guin�e �quatoriale - Burkina Faso - 0-0
21 janvier : Gabon - Congo : 0-1
25 janvier (19h) : Congo - Burkina Faso : 2-1
25 janvier (19h) : Gabon - Guin�e �quatoriale : 0-2
Congo et Guin�e �quatoriale qualifi�es
Groupe B
18 janvier : Zambie - RD Congo : 1-1
18 janvier : Tunisie - Cap Vert : 1-1
22 janvier (17h) : Zambie - Tunisie : 1-2
22 janvier (20h) : Cap Vert - RD Congo : 0-0
26 janvier (19h) : RD Congo - Tunisie : 1-1
26 janvier (19h) : Cap vert - Zambie : 0-0
Tunisie et RD Congo qualifi�es
Groupe C
19 janvier : Ghana - S�n�gal : 1-2
19 janvier : Alg�rie - Afrique du Sud : 3-1
23 janvier (17h) : Ghana - Alg�rie : 1-0
23 janvier (20h) : Afrique du Sud - S�n�gal : 1-1
27 janvier (19h) : Afrique du Sud - Ghana : 1-2
27 janvier (19h) : S�n�gal - Alg�rie : 0-2
Ghana et Alg�rie qualifi�s
20 janvier : C�te d'Ivoire - Guin�e : 1-1
20 janvier : Mali - Cameroun : 1-1
24 janvier (17h) : C�te d'Ivoire - Mali : 1-1
24 janvier (20h) : Cameroun - Guin�ee : 1-1
28 janvier (19h) : Cameroun - C�te d'Ivoire : 0-1
28 janvier (19h) : Guin�e - Mali : 1-1
C�te d'Ivoire et Guin�e qualifi�es.
Les quarts de finale
31 janvier : Congo - RD Congo : 2-4
31 janvier : Tunisie - Guin�e �quatoriale : 1-2
1er f�vrier : Ghana - Guin�e : 3-0
1er f�vrier : C�te d'Ivoire - Alg�rie : 3-1
Les demi-finales
4 f�vrier (20h) : RD Congo - C�te d'Ivoire : 1-3
5 f�vrier (20h) : Guin�e �quatoriale - Ghana : 0-3
La finale
Dimanche 8 f�vrier (20h) : C�te d'Ivoire - Ghana
EN VIDEO - L'Alg�rie figurait parmi les favoris de cette CAN 2015.
Autour du m�me sujet
