TITRE: Maternit�s menac�es : l'exemple de Saint-Girons : Allodocteurs.fr
DATE: 27-01-2015
URL: http://www.allodocteurs.fr/actualite-sante-maternites-menacees-l-exemple-de-saint-girons-15418.asp?1%3D1
PRINCIPAL: 1229672
TEXT:
Accueil ��>�� Actualit�s ��>��Maternit�s menac�es : l'exemple de Saint-Girons
Maternit�s menac�es : l'exemple de Saint-Girons
Par La r�daction de Allodocteurs.fr
r�dig� le 27 janvier 2015, mis � jour le 27 janvier 2015
Dans un rapport publi� le 23 janvier 2015, la Cour des comptes pr�conise de fermer treize maternit�s. Le nombre d'accouchements qu'elles pratiquent est jug� trop faible pour garantir la s�curit� des futures m�res et de leurs b�b�s. Exemple avec la maternit� du Centre hospitalier Ari�ge-Couserans, situ�e � Saint-Girons.
Maternit�s menac�es : l'exemple de Saint-Girons
Maternit�s menac�es : l'exemple de Saint-Girons
La petite ville de Saint-Girons et ses vall�es sont encaiss�es aux pieds des Pyr�n�es. Alors pour les 30.000 habitants de la r�gion, le Centre hospitalier et sa maternit� sont des services publics auxquels ils sont attach�s.
La maternit� compte deux salles de naissance, quatre lits et la Cour des comptes comptabilise 219 naissances annuelles, en dessous du seuil minimal d'activit� de 300 accouchements par an fix� par d�cret en 1998 pour toutes les maternit�s. C'est pour cela que cet �tablissement est dans le viseur des Sages qui d�noncent le manque de visites de conformit� depuis 2001. Pourtant, l'Agence r�gionale de sant� (ARS) a renouvel� l'autorisation du service maternit� et obst�trique en 2012, pour une dur�e de cinq ans.
La maternit� b�n�ficie d'une d�rogation depuis 2001, qui lui permet de rester ouverte par n�cessit� g�ographique � cause de l'�loignement. Mais pour la Cour des Comptes c'est la s�curit� des patients au sein de ces petits �tablissements qui est en jeu... un argument r�fut� par les m�decins pour qui les grossesses � risques sont orient�es en amont vers les maternit�s comp�tentes.
La pr�vention passe par la formation continue des m�decins. Et face aux �v�nements impr�vus l'�tablissement� dispose de l'ensemble du personnel n�cessaire.� Reste la question de la rentabilit�...
En France, ces quarante derni�res ann�es deux maternit�s sur trois ont ferm� leurs portes. Avec ce rapport, la Cour souhaite donc proc�der � une nouvelle �tape de restructuration.
VOIR AUSSI
