TITRE: Si vous voulez �tre mince, mangez sur 9 � 12 heures, pas plus ! | Medisite
DATE: 27-01-2015
URL: http://www.medisite.fr/a-la-une-si-vous-voulez-etre-mince-mangez-sur-9-a-12-heures-pas-plus.784227.2035.html
PRINCIPAL: 0
TEXT:
� Si vous voulez �tre mince, mangez sur 9 � 12 heures, pas plus !
Si vous voulez �tre mince, mangez sur 9 � 12 heures, pas plus !
Des chercheurs am�ricains d�montrent que pour garder la ligne, il faut �tre autant s�rieux sur l'heure des repas que sur ce que l'on met dans son assiette.
Publicit�
Pour rester mince, vous �vitez de manger trop gras ou trop sucr�. C'est bien, mais vous pouvez faire mieux ou r�duire cette rigueur si vous surveillez � quelle heure vous prenez vos repas. Selon les scientifiques de l'Institut Salk en Californie (Etats-Unis), il faudrait limiter sa prise alimentaire sur 8 � 12 heures maximum, pour garder la ligne , contr�ler son diab�te , son cholest�rol , et m�me pr�venir l'ob�sit�. En clair, si vous prenez votre petit-d�jeuner � 7 heures le matin, il faut prendre votre dernier repas le soir avant 19 heures maximum.
Des g�nes mieux synchronis�s
C'est apr�s avoir adiministr� diff�rents r�gimes alimentaires (riches en sucres, en graisses, ou all�g�s), � pr�s de 400 souris qu'ils ont fait cette d�couverte. Ils ont remarqu� que celles prenant un repas riche en graisses autoris�es � ne manger que pendant 8 heures �taient minces et en meilleure sant�, en comparaison � celles ayant ing�r� le m�me nombre de calories mais autoris�es � manger sur toute une journ�e, sans restriction d'horaires. Pour les chercheurs, avoir des repas r�guliers mais limit�s dans le temps aiderait � synchroniser les fonctions de centaines de g�nes. Leur d�couverte devrait permettre aux personnes n'ayant pas acc�s � une alimentation saine de malgr� tout prot�ger leur sant� en surveillant leurs heures de repas.
Publicit�
Source : Hatori M, Vollmers C, Zarrinpar A, DiTacchio L, Bushong E, Gill S, Leblanc M, Chaix A, Joens M, Fitzpatrick J, Ellisman M, Panda S. Time-restricted feeding without reducing caloric intake prevents metabolic diseases in mice fed a high-fat diet. Cell Metabolism.
Publi� par R�daction le Jeudi 22 Janvier 2015 : 11h36
Vid�os
