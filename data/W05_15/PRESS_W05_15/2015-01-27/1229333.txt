TITRE: 20 Minutes Online - Un avion touch� par un tir avant son atterrissage - Monde
DATE: 27-01-2015
URL: http://www.20min.ch/ro/news/monde/story/22839451
PRINCIPAL: 1229331
TEXT:
Irak
Un avion touch� par un tir avant son atterrissage
Un appareil de la compagnie a�rienne FlyDuba� a �t� touch� par un tir au moment o� il rejoignait l'a�roport de Bagdad. Selon le Wall-Street Journal, l'avion a pu atterrir sans faire de victime.
Un avion de la compagnie � bas co�ts flydubai a �t� touch� par un tir avant son atterrissage lundi � l'a�roport international de Bagdad, a annonc� mardi un porte-parole de la compagnie a�rienne �miratie.
Le fuselage de l'appareil assurant le vol FZ215 a �t� touch� par une �balle de petit calibre� mais tous les passagers ont pu d�barquer sains et saufs, a pr�cis� le porte-parole.
�L'avion a pu se poser normalement�, a pour sa part indiqu� un officier des services de s�curit� de l'a�roport de Bagdad.
Vols suspendus
Apr�s cet incident, l'autorit� de l'aviation civile aux Emirats arabes unis a d�cid� de suspendre les vols vers Bagdad des quatre compagnies du pays: flydubai, Emirates, Etihad et Air Arabia.
Emirates, la plus grosse compagnie �miratie, a confirm� dans un communiqu� avoir suspendu ses vols sur Bagdad �pour des raisons op�rationnelles�.
De son c�t�, Etihad a indiqu� avoir suspendu ses vols en direction de la capitale irakienne �jusqu'� nouvel ordre�.
Retards en vue
L'incident a aussi entra�n� des retards dans les vols d'autres compagnies, comme Turkish Airlines et Royal Jordanian.
�Les horaires des vols ne sont pas clairs. Les directeurs des op�rations sont actuellement en r�union�, a d�clar� � l'AFP un porte-parole de la compagnie turque � Ankara.
A Beyrouth, le Pdg de la Middle East Airlines (MEA), Mohammad al-Hout, a indiqu� � l'AFP que le vol quotidien Beyrouth-Bagdad de mardi a �t� annul�.
�On a annul� le vol d'aujourd'hui. Pour celui de demain, nous allons �valuer la situation et voir quelles mesures vont �tre prises apr�s l'incident. Tous les autres vols de la MEA � d'autres destinations en Irak sont maintenus�, a-t-il ajout�.
Les grosses compagnies a�riennes survolant le territoire irakien redoublent de pr�cautions par crainte que des membres du groupe Etat islamique (EI) puissent acqu�rir des armes capables de toucher leurs appareils.
L'a�roport de Bagdad est situ� � l'ouest de Bagdad, pr�s de la province d'Al-Anbar, largement contr�l�e par les jihadistes de l'EI.
(afp)
