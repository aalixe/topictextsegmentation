TITRE: Noir et majordome � la Maison Blanche sous sept pr�sidents
DATE: 27-01-2015
URL: http://www.lemonde.fr/televisions-radio/article/2015/01/27/noir-et-majordome-a-la-maison-blanche-sous-sept-presidents_4564381_1655027.html
PRINCIPAL: 1229038
TEXT:
Noir et majordome � la Maison Blanche sous sept pr�sidents
Le Monde |
27.01.2015 � 12h12
| Par Sandrine Marques
Sign� Lee Daniels, le film, inspir� du parcours d�Eugene Allen, a �mu aux larmes Barack Obama (mardi 27 janvier � 20 h 55 sur Canal+)
Pour �chapper � son pass� douloureux dans une exploitation de coton, le jeune Cecil Gaines fuit le sud des Etats-Unis en�1926. Sur les routes, sans emploi, Gaines brise la vitrine d�un salon de th�. Ce geste va modifier radicalement le cours de son existence. Plut�t que de le sanctionner, on l�embauche. Il apprend � servir et � rester � sa place, en se tenant �loign� de toute pens�e politique. Son fils a�n�, lui, choisit le militantisme.
�uvrant au c�t� de Martin Luther King, il est r�guli�rement arr�t� et battu. Ses activit�s lui valent l�incompr�hension et la d�sapprobation de son p�re, entr� dans l�intervalle � la Maison Blanche comme majordome. Le point de rupture entre les deux hommes est atteint quand le jeune gar�on se laisse tenter par le radicalisme des Black Panthers. Mais Cecil prend peu � peu conscience qu�il s�est tromp�.
Avec Le Majordome, Lee Daniels s�attaquait � un vrai d�fi�: transcender l�exercice toujours compliqu� du biopic, tout en mettant en sc�ne une fresque s��tirant sur plus de trente ann�es. Au moyen d�images d�archives bien exploit�es, il retrace l�histoire de la condition noire aux Etats-Unis. Des zones �taient r�serv�es aux Noirs dans les lieux publics ou les transports. Quant aux �tablissements scolaires, ils �taient ferm�s � toute mixit� raciale. Pour protester, des militants ont appel� � la d�sob�issance civile. Depuis le bureau Ovale, des d�cisions majeures vont �tre prises en faveur des Noirs, Gaines, passivement, assiste � ces transformations avant d�en saisir l�importance.
Une retenue bienvenue
C�est l��veil de cette conscience que montre Lee Daniels dans un film qui n��chappe pas � des simplifications grossi�res, dans un but p�dagogique manifeste. La volont� de f�d�rer les spectateurs de tous bords autour de son r�cit �difiant prime de toute �vidence. Sa mani�re d�arrondir les angles vaut � ce film, classique dans sa r�alisation, de s�offrir � nous sans asp�rit�s, en d�pit des conflits m�nag�s pour les besoins du sc�nario.
La retenue dont fait preuve Daniels, fascin� par la violence et les d�tails glauques, est bienvenue. Elle se met au service d�une �pop�e qui rend sensible la destin�e d�un homme aveugl� presque toute sa vie.
��Le Majordome��, de Lee Daniels. Avec Forest Whitaker, Oprah Winfrey, David Oyelowo (E.-U., 2013, 125�min). Mardi 27�janvier �20�h�55 sur Canal+
Sandrine Marques
