TITRE: Mercato | Mercato - Barcelone : Quand Guardiola a pr�f�r� un autre joueur � Griezmann�
DATE: 27-01-2015
URL: http://www.le10sport.com/football/mercato/mercato-barcelone-quand-guardiola-a-prefere-un-autre-joueur-a-griezmann178441
PRINCIPAL: 1228303
TEXT:
�
GUARDIOLA A PR�F�R� SANCHEZ � GRIEZMANN
Selon les informations de Sport , Antoine Griezmann �tait dans le viseur du FC Barcelone � deux reprises. Si le Fran�ais �tait d�j� surveill� par le club Blaugrana lors de sa p�riode de formation, il figurait �galement en bonne position sur une short-list � un an du d�part de Pep Guardiola selon le quotidien catalan. Seulement le technicien espagnol lui a pr�f�r� un certain Alexis Sanchez.
�
��C�EST QUAND M�ME LE CHAMPION D�ESPAGNE !��
��Pourquoi l�Espagne et pas ailleurs�? C�est mon Championnat, ma maison. Je suis bien ici. Et l�Atletico est un tr�s grand club. Les dirigeants �taient pr�ts � mettre 30M� pour m�avoir. J�avais eu le boss au t�l�phone, il m�avait motiv� et dit que le club me voulait vraiment. Je savais qu�en venant ici j�allais apprendre. Bizarre quand le�PSG�et�Arsenal��taient int�ress�s�? C�est quand m�me le champion d�Espagne ! J�avais besoin d�un club comme �a et d�un entra�neur comme�Diego Simeone�pour progresser. Il met de l�intensit� � tous les entra�nements. C�est mon style, �a me pla�t. Le capitaine de l��quipe m�a aussi appel� pour me demander de venir et me dire que je me sentirais bien ici, avec un vestiaire tr�s familial��, a confi� Antoine Griezmann apr�s s��tre engag� en faveur de l�Atl�tico Madrid.
R�daction
Retrouvez toute l'actualit� du mercato
Retrouvez toutes les vid�os
