TITRE: Proc�s Bettencourt: la d�fense tire � boulets rouges sur l'instruction - LExpress.fr
DATE: 27-01-2015
URL: http://www.lexpress.fr/actualites/1/actualite/le-proces-bettencourt-suit-son-cours-et-espere-le-temoignage-de-claire-thibout_1645106.html
PRINCIPAL: 1228779
TEXT:
Zoom moins
Zoom plus
Patrice de Maistre, l'un des pr�venus du proc�s Bettencourt, arrive au palais de justice de Bordeaux, le 27 janvier 2015
afp.com/Mehdi Fedouach
Malgr� le rejet par le tribunal correctionnel d'une question prioritaire de constitutionnalit� (QPC), les avocats de la d�fense n'ont pas d�sarm� et se sont lanc�s dans une nouvelle salve de demandes visant � faire annuler ou reporter ce proc�s-fleuve, pr�vu pour durer cinq semaines. Le tribunal doit juger dix hommes, dont le d�put� UMP et ex-ministre Eric Woerth, soup�onn�s d'avoir profit� entre 2006 et 2011 de la vuln�rabilit� de la femme la plus riche de France. Aujourd'hui �g�e de 92 ans et sous tutelle, Liliane Bettencourt est la grande absente du proc�s.�
Pierre Cornut-Gentille, l'un des avocats du photographe Fran�ois-Marie Banier, confident de la milliardaire, a ainsi �gren� les "�l�ments � d�charge" qui auraient selon lui �t� d�lib�r�ment ignor�s par les juges d'instruction.�
L'avocat a notamment cit� quelques-unes des 2.000 � 3.000 lettres que son client aurait �chang�es avec Mme Bettencourt. "C'est ma marge de libert� que j'entends pr�server", aurait notamment �crit Mme Bettencourt en 1999 � propos des donations faites � son ami. "M. Banier ne m'a jamais demand� d'argent", assurerait-elle encore selon Me Cornut-Gentille.�
- 'Apr�s le show, le froid' -�
Poursuivi pour "abus de faiblesse" et "blanchiment", Fran�ois-Marie Banier est � lui seul accus� d'avoir per�u plus de 400 millions d'euros de la part de la milliardaire, sans compter les autres cadeaux consentis au compagnon du photographe, Martin d'Orgeval, lui aussi pr�sent sur le banc des pr�venus.�
M�me strat�gie du c�t� de la d�fense de Patrice de Maistre, ancien gestionnaire de la fortune de la milliardaire, dont les avocats ont d�nonc� une instruction en forme de "r�quisitoire". Le juge d'instruction Jean-Michel Gentil - qui avait notamment mis en examen Nicolas Sarkozy, au printemps 2013, avant que l'ex-pr�sident ne b�n�ficie d'un non lieu - a �t� nomm�ment pris � partie.�
"La d�fense a voulu se positionner pour fustiger l'instruction, c'est ce qu'elle fait depuis le d�part", a temp�r� Me Arnaud Dupin, l'un des avocats de Mme Bettencourt, partie civile. "Mais apr�s le +show+ va venir le froid, va venir la r�alit� du dossier", a-t-il pr�venu. Dons manuels ou "lib�ralit�s" par dizaines de millions d'euros, faramineux contrats d'assurance-vie, oeuvres d'art: c'est dans un monde d'ultra-riches que le proc�s pourrait plonger d�s mercredi pour tenter de d�m�ler les cadeaux li�s � "l'amiti�" des pr�sum�s "abus de faiblesse".�
Me Patricia Laffont, autre avocate de Patrice de Maistre, a pris pour cible Claire Thibout, ancienne comptable de Liliane Bettencourt et "t�moin central" dans les accusations contre MM. Banier et de Maistre.�
"On a l'impression qu'elle est encore plus accusatrice que le r�quisitoire de M. le Procureur", a lanc� l'avocate, d�plorant la "place d�mesur�e" faite aux d�clarations de Mme Thibout, "cit�e 144 fois" dans le dossier.�
- T�moignage par visio-conf�rence ' -�
Ce t�moin-clef viendra-t-il d�poser � Bordeaux pour faire la lumi�re sur les pratiques qui avaient cours dans l'h�tel particulier de la 11e fortune mondiale (30 milliards d'euros selon le magazine Forbes) � Neuilly-sur-Seine' �
Lundi, l'avocat de Mme Thibout a produit le certificat m�dical d'un psychiatre �voquant un �tat "incompatible" avec le fait de venir t�moigner, "pour une dur�e ind�termin�e", au grand dam de la d�fense.�
Car Claire Thibout a �t� mise en examen fin 2014 � Paris, dans une proc�dure distincte, pour "faux t�moignages" � la suite d'une plainte de Fran�ois-Marie Banier et Patrice de Maistre. Et leurs avocats comptent bien en profiter pour demander la suspension du proc�s jusqu'� la fin de la proc�dure visant l'ex-comptable de 56 ans.�
D�s la reprise de l'audience mardi, le pr�sident Denis Roucou a demand� qu'une expertise m�dicale soit r�alis�e d'ici le 1er f�vrier sur Mme Thibout afin de d�terminer si elle peut venir t�moigner � la barre ou, � d�faut, par visio-conf�rence.�
Mme Thibout accuse depuis le d�but Fran�ois-Marie Banier et Patrice de Maistre d'avoir profit� de la fragilit� de sa patronne. Elle a notamment affirm� avoir remis � M. de Maistre 50.000 euros en liquide, qui auraient �t� destin�s � Eric Woerth, poursuivi pour "recel". Ce que MM. de Maistre et Woerth nient.�
Lundi, le proc�s avait d�but� sur un coup de th��tre qui avait glac� le tribunal: l'annonce de la tentative de suicide d'Alain Thurin, ancien infirmier de Liliane Bettencourt, poursuivi pour "abus de faiblesse". �
Mardi, le procureur a inform� le tribunal qu'Alain Thurin, 64 ans, avait envoy� une lettre au Parquet de Bordeaux, la veille de sa pendaison en r�gion parisienne, sans en divulguer le contenu.�
L'infirmier se trouvait lundi soir � l'h�pital entre la vie et la mort.�
Par
