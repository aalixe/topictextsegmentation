TITRE: Facebook et Instagram victimes d�une panne mondiale ce mardi matin - La Voix du Nord
DATE: 27-01-2015
URL: http://www.lavoixdunord.fr/france-monde/facebook-et-instagram-victimes-d-une-panne-mondiale-ce-ia0b0n2625244
PRINCIPAL: 1228759
TEXT:
France-Monde
Facebook et Instagram victimes d�une panne mondiale ce mardi matin
Par la r�daction pour La Voix du Nord , Publi� le 27/01/2015 - Mis � jour le 27/01/2015 � 14 : 53
AVEC AFP.
Le journal du jour � partir de 0,79 �
Ce mardi matin, le monde entier s�est demand� ce qu�il se passait  : de 7h15 � 8h05, heure fran�aise, le r�seau social Facebook et sa filiale Instagram ont �t� inaccessibles. Une panne mondiale dont l�ampleur et l�origine restent ind�termin�es.
Ce mardi, le r�seau social Facebook et sa filiale Instagram ont �t� inaccessibles pendant pr�s d�une heure. CAPTURE ECRAN.
- A +
Surprise t�t ce mardi matin en allumant smartphones, tablettes et autres ordinateurs  : impossible d�acc�der � Facebook et Instagram. Le c�l�bre r�seau social de Mark Zuckerberg et sa filiale ont �t� victimes d�une panne mondiale pendant une cinquantaine de minutes. Le service a �t� r�tabli aux environs de 8h05.
Facebook d�ment toute cyberattaque
�Beaucoup de personnes ont eu du mal � acc�der � Facebook et Instagram. Cela ne r�sultait pas d�une attaque de tiers mais s�est produit apr�s l�introduction d�un changement qui a affect� nos syst�mes de configuration. Nous avons rapidement r�solu le probl�me et les deux services fonctionnent � nouveau � 100% pour tout le monde�, a affirm� Facebook dans un communiqu�, sans donner de d�tails.
Facebook a ainsi voulu faire taire les rumeurs circulant sur Internet au sujet d�un �ventuel piratage de ses syst�mes, notamment apr�s un tweet ambigu d�un groupe de hackers, Lizard Squad (voir ci-dessous).
Lizard Squad on Twitter
