TITRE: Septante ans apr�s la lib�ration du camp d'Auschwitz
DATE: 27-01-2015
URL: http://www.romandie.com/news/Septante-ans-apres-la-liberation-du-camp-dAuschwitz_RP/559434.rom
PRINCIPAL: 1229092
TEXT:
Tweet
Septante ans apr�s la lib�ration du camp d'Auschwitz
Survivants de l'Holocauste, chefs d'Etat et t�tes couronn�es se r�unissaient mardi � Auschwitz pour lancer un nouveau "Plus jamais �a", 70 ans apr�s la lib�ration du camp d'extermination nazi sur fond de craintes de mont�e de l'antis�mitisme en Europe. La pr�sidente de la Conf�d�ration Simonetta Sommaruga sera sur place.
Un des premiers � s'�tre exprim� officiellement, le pr�sident allemand Joachim Gauck a estim� mardi matin qu'il n'y avait "pas d'identit� allemande sans Auschwitz". Il a assign� � son pays le devoir de "prot�ger les droits de chaque �tre humain".
"La m�moire de l'Holocauste demeure l'affaire de tous les citoyens qui vivent en Allemagne", a martel� M. Gauck lors d'un discours d'hommage aux victimes du nazisme devant les d�put�s du Bundestag, en pr�sence de la chanceli�re Angela Merkel.
"Ici en Allemagne, o� nous longeons chaque jour des maisons depuis lesquelles des Juifs ont �t� d�port�s; ici en Allemagne, o� leur annihilation a �t� pr�vue et organis�e. Ici, l'horreur pass�e est plus proche et la responsabilit� (...) plus grande et imp�rative qu'ailleurs", a-t-il insist�.
"Une communaut� de responsabilit�"
Retra�ant la tumultueuse �mergence d'une m�moire collective de la Shoah dans l'Allemagne d'apr�s-guerre, M. Gauck a d�peint un pays d�sormais transform� par l'immigration et devenu "une communaut� de responsabilit�". M�me les jeunes qui n'ont pas connu la guerre, "m�me les gens dont les racines ne sont pas allemandes se sentent touch�s lorsqu'ils d�couvrent le nom de leurs anciens propri�taires parmi les victimes d'Auschwitz", a poursuivi le pr�sident allemand.
Pour M. Gauck, "le devoir moral" de l'Allemagne "ne r�side pas uniquement dans le souvenir" mais assigne au pays "une mission". "Il nous dit: prot�ge et pr�serve l'humanit�. Prot�ge et pr�serve les droits de chaque �tre humain", a-t-il d�clar�.
Poutine absent
Outre Joachim Gauck, les pr�sidents fran�ais Fran�ois Hollande et ukrainien Petro Porochenko, le secr�taire am�ricain au Tr�sor Jack Lew ainsi que les familles royales belge et n�erlandaise, notamment, doivent assister � la c�r�monie principale � Auschwitz mardi apr�s-midi.
La Russie doit �tre repr�sent�e par le chef de l'administration pr�sidentielle Sergue� Ivanov. Le pr�sident Vladimir Poutine n'a pas souhait� se d�placer - alors qu'il l'avait fait en 2005 - n'ayant pas �t� officiellement invit�.
C'est l'arm�e sovi�tique qui a lib�r� en 1945 le camp d'Auschwitz-Birkenau, o� quelque 1,1 million de personnes avaient �t� extermin�es, un million de Juifs de diff�rents pays d'Europe, des Polonais, des Tsiganes et des Russes, notamment.
Si l'extermination organis�e comme une industrie par les nazis s'est d�roul�e essentiellement en Pologne occup�e, l'Holocauste avait touch� plusieurs autres pays europ�ens o� les Juifs ont �t� arr�t�s pour �tre d�port�s vers les camps de la mort.
"La France est votre patrie"
Fran�ois Hollande s'est lui aussi d�j� exprim� en matin�e � Paris. "La France est votre patrie", a-t-il assur� aux juifs de France, alors que le nombre d'actes antis�mites a doubl� sur un an en 2014 dans le pays qui abrite la premi�re communaut� juive d'Europe.
Dans un discours au M�morial de la Shoah, le chef de l'Etat a d�nonc� dans la mont�e des actes antis�mites "une r�alit� insupportable". Il a annonc� un renforcement des sanctions contre le racisme.
Le "fl�au" de l'antis�mitisme "conduit certains Juifs � s'interroger sur leur pr�sence en France. Vous, Fran�ais de confession juive, votre place est ici", a d�clar� le pr�sident fran�ais. Avec 500'000 � 600'000 personnes, la France abrite la premi�re communaut� juive d'Europe et la troisi�me dans le monde apr�s Isra�l et les Etats-Unis.
M. Hollande a �galement affirm� que la France "n'oubliera jamais" les victimes de la Shoah, et rendu hommage aux 76'000 Juifs de France d�port�s sous le r�gime collaborationniste de Vichy pendant la Seconde Guerre mondiale.
(ats / 27.01.2015 12h20)
