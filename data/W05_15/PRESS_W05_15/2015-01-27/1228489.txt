TITRE: Berlinale 2015 : Audrey Tautou, Daniel Br�hl et le cr�ateur de Mad Men dans le jury de Darren Aronofsky
DATE: 27-01-2015
URL: http://www.premiere.fr/Cinema/Photos/Reportages/Berlinale-2015-Audrey-Tautou-Daniel-Bruehl-et-le-createur-de-Mad-Men-dans-le-jury-de-Darren-Aronofsky-4122503
PRINCIPAL: 1228488
TEXT:
Personnalit�s
(7)
Berlinale 2015 : Audrey Tautou, Daniel Br�hl et le cr�ateur de Mad Men dans le jury de Darren Aronofsky
27/01/2015 - 12h32
Suiv >>
Le jury de la Berlinale 2015 sera compos� de...
>>> 50 Nuances de Grey sera d�voil� en avant-premi�re au festival de Berlin
L'actrice fran�aise Audrey Tautou
L'interpr�te d' Am�lie Poulain , qui a re�u le C�sar du meilleur espoir f�minin en 2000 pour V�nus Beaut� ��paulera Darren Aronofsky �du 5 au 15 f�vrier.
L'acteur allemand Daniel Br�hl
Vainqueur du prix de la r�v�lation � la Berlinale�gr�ce � Goodbye, Lenin ! en 2003, Daniel Br�hl �a depuis pris ses marques � Hollywood. Son interpr�tation de Niki Lauda dans Rush , de Ron Howard , �tait notamment remarquable, et il vient de rejoindre l'�curie Marvel en acceptant ?un r�le myst�rieux? dans Captain America 3 .
Le r�alisateur sud-cor�en Bong Joon-ho
Rep�r� au d�but des ann�es 2000 gr�ce � Barking Dog et� Memories of Murder , Bong Joon-ho �s'essaye au cin�ma de monstres avec The Host �en 2006 et signe l'excellent Mother �trois ans plus tard. Son dernier film, Snowpiercer , est une adaptation brillante de la BD de Jacques Lob et Jean-Marc Rochette. C'est aussi son premier projet tourn� en anglais.
La r�alisatrice p�ruvienne Claudia Llosa
Claudia Llosa �a remport� en 2009 l'Ours d'or du meilleur film � la Berlinale pour Fausta , qui s'inspire du traumatisme caus� par le conflit p�ruvien entre 1980 et 1992.
Le producteur am�ricain Matthew Weiner
Co-sc�nariste et co-producteur des saisons 5 et 6 des Soprano , Matthew Weiner a depuis connu un �norme succ�s en cr�ant Mad Men , sa s�rie d�voilant les coulisses d'une agence publicitaire dans les ann�es 1960.
La productrice italienne Martha De Laurentiis
Depuis qu'elle a �pous� Dino de Laurentiis ,�Martha Schumacher a produit une quarantaine de films et de s�ries avec lui, dont la franchise li�e � Hannibal Lecter (qui se d�cline depuis peu en s�rie). Le producteur est mort en 2010, mais elle continue � financer Hannibal �avec succ�s.
Et le r�alisateur am�ricain Darren Aronofsky sera le pr�sident
"Darren Aronofsky�s'est distingu� en tant qu'auteur de cin�ma, justifiait�le directeur Dieter Kosslick en annon�ant que le r�alisateur de Requiem for a Dream �et Black Swan �serait pr�sident du 65e festival de Berlin.�Gr�ce � son approche artistique originale, il utilise le langage cin�matographique avec le plus d'esth�tique possible. Je suis ravi de l'accueillir en tant que pr�sident du jury de la Berlinale 2015."
L'actrice ukrainienne Olga Kurylenko sera dans le jury "sp�cial premier film"
