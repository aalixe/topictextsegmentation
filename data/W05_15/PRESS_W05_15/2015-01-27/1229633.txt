TITRE: 20 Minutes Online - Le tome 4 de �Mill�nium� para�tra cet �t� - Stories
DATE: 27-01-2015
URL: http://www.20min.ch/ro/community/stories/story/Le-tome-4-de--Millenium--para-tra-cet-ete-25829711
PRINCIPAL: 1229632
TEXT:
Le nouvel ouvrage comportera de multiples intrigues. Il sortira le 27 ao�t prochain.    (photo: AFP)
27 janvier 2015 16:21
Saga � succ�s
Le tome 4 de �Mill�nium� para�tra cet �t�
Le quatri�me tome de la c�l�bre saga de romans policiers su�dois s'intitulera �Ce qui ne nous tue pas�, annonce l'auteur qui a repris le flambeau de Stieg Larsson, d�c�d� en 2004.
L'ouvrage sign� David Lagercrantz, co-auteur de l'autobiographie �Moi, Zlatan Ibrahimovic�, est achev� et doit para�tre le 27 ao�t dans 35 pays et compter 500 pages. Interrog� par le quotidien Dagens Nyheter sur le sens de ce titre, l'�crivain a laiss� planer le doute. �Un bon titre doit toujours avoir plusieurs significations et ne pas se laisser enfermer, dit-on d'habitude�, a-t-il r�pondu.
Il n'a pas livr� d'�l�ment sur le contenu, sinon qu'il resterait fid�le � la profusion des pistes que lan�aient et des sujets que couvraient les trois tomes qu'a finis Stieg Larsson, lequel projetait d'en �crire beaucoup plus au moment o� il a �t� terrass� par une crise cardiaque � 50 ans.
�Ce n'est pas du tout un but en soi d'�crire aussi long, mais j'ai voulu avoir autant d'intrigues dans le livre que Larsson en avait dans les siens�, a dit David Lagercrantz.
Pour la maison d'�dition su�doise Nordstedts, le processus de publication simultan�e dans une multitude de langues devrait ressembler � celui des livres de l'Am�ricain Dan Brown, traduits dans le plus grand secret pour �viter les fuites. En France, c'est toujours Actes Sud qui conserve les droits, comme sur les trois tomes sign�s Stieg Larsson.
L'id�e de faire poursuivre Mill�nium par un autre romancier a �t� vivement critiqu�e par la compagne de l'auteur original, Eva Gabrielsson, qui s'est toujours refus�e � laisser voir les �bauches du quatri�me tome, et qui n'a pas touch� un centime des ventes prodigieuses des trois romans apr�s la mort de leur auteur.
Que pensez-vous de la sortie d'un 4e tome, �crit par un autre romancier que Stieg Larsson? Pensez-vous le lire? La saga devait-elle lui survivre? Exprimez-vous dans notre espace commentaires!
(afp)
