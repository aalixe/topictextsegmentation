TITRE: Facebook Lite�: application all�g�e, messagerie incluse
DATE: 27-01-2015
URL: http://www.lesnumeriques.com/facebook-lite-application-allegee-messagerie-incluse-n38947.html
PRINCIPAL: 0
TEXT:
Publi� le 27 janvier 2015 09:29
Facebook Lite�: application all�g�e, messagerie incluse
Mais disponibilit� (presque) limit�e
Une fois n'est pas coutume, c'est en toute discr�tion que Facebook a lanc�, il y a quelques jours, une nouvelle d�clinaison de son application Android. Nomm�e Facebook Lite, elle regroupe toutes les fonctions du r�seau social, mais affiche un poids �tonnamment l�ger.
En effet, Facebook Lite permet de suivre l'actualit� de ses amis, de poster des photos, de recevoir des notifications, de commenter et dispose m�me de la messagerie retir�e � l'application principale. Pourtant, le fichier apk � installer est incroyablement l�ger�: 252 Ko, selon le Play Store . Un petit miracle�? Non, en r�alit�, Facebook a bas� son application sur Snaptu, le client Java du r�seau social pour les feature phones. Il en r�sulte une interface un brin grossi�re... mais un gain de place extr�mement cons�quent.
Naturellement, cette mouture ne vise par les possesseurs de LG G3 , Samsung Galaxy Note 4 ou autres Sony Xperia Z3 . Elle est sp�cialement con�ue pour les terminaux modestes, voire dat�s. Ainsi, on remarque par exemple sa compatibilit� qui remonte jusqu'� Android 2.2 (Froyo, pour les intimes). De m�me, on note que l'application est "con�ue pour les r�seaux 2G et les zones d'acc�s r�seau limit�es".
Sans surprise, Facebook Lite a donc �t� lanc� en Afrique du Sud, au Bangladesh, au N�pal, au Nigeria, au Soudan, au Sri Lanka et au Zimbabwe . H�las, pour les mobinautes se trouvant dans d'autres pays, le Play Store ne permet pas de t�l�charger l'application. Android oblige, il est n�anmoins facile de contourner cet obstacle. Si vous utilisez encore un smartphone datant de l'�poque Froyo/Gingerbread, potentiellement affubl� de 512�Mo de m�moire interne, il est relativement simple de remplacer la rondelette application Facebook par Facebook Lite. Il suffit de trouver l'apk du second sur le Net , de l'installer, puis de d�sinstaller la version "poids lourd".
TAGS
