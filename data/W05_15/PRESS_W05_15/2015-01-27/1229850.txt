TITRE: Parents s�par�s : La garde altern�e a doubl� en 10 ans. Info - Toulon.maville.com
DATE: 27-01-2015
URL: http://www.toulon.maville.com/actu/actudet_-parents-separes-la-garde-alternee-a-double-en-10-ans_54135-2704033_actu.Htm
PRINCIPAL: 0
TEXT:
Twitter
Google +
En cas de s�paration, la garde des enfants reste prioritairement confi�e � la m�re.� Ouest France
Le minist�re de la Justice publie mardi, une �tude sur l'�volution des d�cisions du juge aux affaires familiales, concernant les enfants de couple s�par�s.
Les d�cisions des juges en faveur de la garde altern�e des enfants de parents s�par�s ont doubl� en 10 ans, m�me si la r�sidence chez la m�re reste largement majoritaire, selon une �tude publi�e mardi par le minist�re de la Justice.�
D'apr�s ce document d'Infostat Justice, dont les principaux enseignements ont �t� diffus�s par La Croix dans son �dition de mardi, un enfant de parents divorc�s sur cinq vit selon ce mode de garde.
Lire : Dix ans de garde altern�e et toujours de vifs d�bats
Le nombre de gardes altern�es a doubl� en dix ans
Alors que la garde altern�e ne concernait que 12% des d�cisions des juges aux affaires familiales (JAF) en 2003 en cas de divorce, ce chiffre s'�levait � 21% en 2012.
Lorsque les parents se s�parent sans avoir �t� mari�s, la proportion s'�l�ve � 11%. �Au total 17% des enfants de couples divorc�s ou s�par�s vivent en garde altern�e.
Il demeure toutefois une zone floue car les couples non mari�s ne sont pas oblig�s de faire appel au juge pour organiser la vie de leur enfant mineur.
R�sidence chez la m�re
En 2012, environ 126 000 d�cisions sur la garde de mineurs ont �t� rendues, concernant quelque 200 000 enfants. Un peu plus de la moiti� de ces d�cisions sont des divorces, le reste a �t� rendu sur saisine de couples non mari�s.
Mais la r�sidence chez la m�re reste prononc�e dans 69% des divorces, indique l'�tude, m�me si elle repr�sentait 78% des d�cisions en 2003.
Cette �volution est li�e � l'augmentation de la garde altern�e, la r�sidence chez le p�re restant stable, de 7% � 2003 � 6% en 2012.
La r�sidence altern�e demeure beaucoup plus fr�quentes lors de divorces gracieux (30%) qu'� l'occasion de divorces contentieux (13%) ou de la s�paration de couples non mari�s (11%).
La r�sidence altern�e tr�s rare pour les enfants de moins de 2 ans
Le choix du mode de r�sidence varie sensiblement selon l'�ge des enfants. Ainsi, la r�sidence altern�e est tr�s rare avant 2 ans (moins de 5% des enfants), mais elle d�passe les 10% d�s cet �ge franchi. Elle atteint un pic chez les 6-10 ans (21% des cas) avant de baisser pour ne plus concerner que 15% des 15-17 ans.
Inversement, la proportion des d�cisions fixant la r�sidence habituelle chez la m�re est plus forte pour les enfants de moins de 6 ans (82%) et tend � diminuer lorsque l'enfant grandit (69% pour les 15-17 ans).
Baisse du nombre de pensions alimentaires
L'�tude r�v�le �galement que le nombre de pensions alimentaires fix�es par le juge ne cesse de baisser.
Les juges n'a fix� de pension au titre de la contribution � l'entretien et � l'�ducation de l'enfant (CEEE) que pour 68% des mineurs en 2012, contre plus de 78% dans les cas de divorce et 73% pour les s�parations de parents non mari�s en 2003.
Selon l'�tude, le versement d'une pension alimentaire est beaucoup plus fr�quente en cas de r�sidence habituelle chez la m�re (82%) et les pensions vers�es par les p�res sont g�n�ralement plus �lev�es que celles vers�es par les m�res (172 euros contre 118 euros mensuels).
En outre, ces montants sont en baisse par rapport � 2003. D'apr�s les chiffres du minist�re, si on s'en tient � la configuration la plus fr�quente, � savoir celle d'une pension CEEE vers�e par le p�re � la m�re qui a la garde habituelle (86%), les montants ont diminu� de 10%.
Ouest-France��
