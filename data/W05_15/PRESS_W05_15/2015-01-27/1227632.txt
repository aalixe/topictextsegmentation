TITRE: Alba Ventura : "L'apprentissage politique d'Emmanuel Macron"
DATE: 27-01-2015
URL: http://www.rtl.fr/actu/politique/alba-ventura-l-apprentissage-politique-d-emmanuel-macron-7776348915
PRINCIPAL: 1227630
TEXT:
Accueil Actu Politique Alba Ventura : "L'apprentissage politique d'Emmanuel Macron"
Alba Ventura : "L'apprentissage politique d'Emmanuel Macron"
REPLAY / �DITO - D�cid�ment, Emmanuel Macron continue d'intriguer. Un jeune homme, sur-dipl�m�, ancien banquier et sympa : on se dit que c'est un peu suspect. On croise pourtant assez peu de gens pour le critiquer.
Alba Ventura : "L'apprentissage politique d'Emmanuel Macron" Cr�dit Image : Elodie Gr�goire |                                 Cr�dits M�dia : RTL |                                 Dur�e :
03:41
publi� le 27/01/2015 � 09:05
mis � jour le 27/01/2015 � 10:28
Partager
Commenter
Imprimer
C'�tait donc l'�preuve du feu lundi 26 janvier pour Emmanuel Macron. Le ministre de l'�conomie a d�fendu sa loi devant l'Assembl�e nationale. A-t-il �t� � la hauteur de la situation. En tout cas, il a fait tout ce qu'il fallait. Il est appliqu�, l'�l�ve Macron.
La semaine derni�re en commission, il n'a pas m�nag� sa peine. Ses amis de Bercy racontent qu'il n'a pas l�ch� le morceau de 8 heures � 3 heures du matin. Visiblement, c'est ce qui bluffe tout le monde : sa r�sistance physique. C'est vrai qu'il n'a que 37 ans.
Il ma�trise d�j� l'art du compromis
Mais il n'y pas que cela, �videmment. Il ma�trise d�j� � la perfection l'art du compromis. Il a r�ussi la semaine derni�re � se mettre dans la poche des proches de Martine Aubry, et m�me quelques UMP. Au point, parfois, de vider certains articles. Preuve en est le volet sur le travail le dimanche : finalement, ce sont les maires qui auront toute latitude pour ouvrir ou fermer .
Reste que le compromis est une grande qualit� en politique. Surtout quand on pr�sente une loi importante, en tout cas sur laquelle le Pr�sident compte beaucoup. C'est une sacr�e charge.
Ce n'est pas la m�me chose de d�fendre sa loi en commission et devant 577 d�put�s
Alba Ventura
Facebook Twitter Linkedin
M�me si cela fait cinq mois qu'Emmanuel Macron se pr�pare � pr�senter cette loi devant le Parlement, ce n'est pas la m�me chose de la d�fendre en commission et devant 577 d�put�s. C'est impressionnant le Palais-Bourbon, quand on n'est pas un professionnel de la politique.
Histoire de faire tomber la pression le moment venu, le pr�sident de l'Assembl�e nationale, Claude Bartolone, avait invit� le tout jeune ministre de l'�conomie, un lundi soir, � venir visiter l'h�micycle vide.
Pour ne pas se laisser impressionner, Emmanuel Macron avait demand� � Claude Bartolone quelles avaient �t� les questions qui l'avaient le plus marqu�, et quelles avaient �t� les r�ponse du gouvernement. Studieuse, la visite.
Pas un "meilleur d'entre nous"
Alors, est-ce pour autant un nouveau "meilleur d'entre nous" ? C'est amusant, parce que justement non. En tout cas, lorsqu'on discute avec ceux qui le fr�quentent ou qui l'ont crois�, m�me certains de ses adversaires politiques, tout le monde est d'accord pour dire qu'il n'est ni un Jupp�, ni un Fabius, dont on dit toujours que ce sont de belles m�caniques intellectuelles.
Tout le monde insiste pour dire qu'Emmanuel Macron n'a pas le c�t� froid, hautain ou impersonnel de ses a�n�s. Il y en m�me � droite, des anciens, qui le comparent � Pompidou. On entend parler d'un Macron humaniste, empathique, d'humeur constante, pas dogmatique, etc.
Aussi incroyable que cela puisse para�tre, la mission qui consiste � trouver quelqu'un qui critique sa personne et son caract�re est impossible.
Bourdes � r�p�tition
Il n'aurait donc pas de d�fauts ? Si, mais ce sont plut�t des faux-pas politiques. Il a fait quelques bourdes. Ainsi, � la veille de sa nomination, il explique dans Le Point qu'il fallait autoriser les entreprises � d�roger aux 35 heures. Le recadrage de Matignon a �t� imm�diat. Pour sa d�fense, il a accord� cet entretien alors qu'il ne savait pas qu'il allait �tre� nomm� � Bercy .
On se souvient aussi des ouvri�res de Gad et de ce mot malheureux ( "illettr�es" ) qu'il prononce � la radio. Dans cette affaire, on l'a vu pousser l'abn�gation au point d'aller rendre visite la semaine derni�re aux salari�s de Gad, de� s'excuser �et d'accueillir le cadeau qu'on lui a fait (des p�tes alphabet).
� droite, certains comparent Macron � Pompidou
Alba Ventura
Facebook Twitter Linkedin
Derni�re bourde en date : dans une interview aux Echos, en parlant de l��conomie du Net, le ministre avoue qu'il aimerait bien que les jeunes aient envie de devenir� "milliardaires" . C'est s�r que pour un homme politique de gauche, �a�manque un peu de nuance. "Millionnaire", c'est d�j� pas mal !
Comme dit l'un de ses proches : "Le probl�me d'Emmanuel Macron, c'est qu' il n'est pas encore dans la vraie vie politique". C'est vrai, mais il apprend vite.
La r�daction vous recommande
