TITRE: R�ponse mardi ou mercredi pour Ben Arfa - Ligue 1 - Football - Sport.fr
DATE: 27-01-2015
URL: http://www.sport.fr/football/ligue-1-reponse-mardi-ou-mercredi-pour-ben-arfa-368396.shtm
PRINCIPAL: 1227889
TEXT:
Football - Ligue 1 Mardi 27 janvier 2015 - 9:47
R�ponse mardi ou mercredi pour Ben Arfa
Tweet
Le dossier d'Hatem Ben Arfa devrait connaître son épilogue dans la journée de mardi ou mercredi, à en croire nos confrères de L'Equipe. La FIFA , qui avait annoncé avoir bien reçu le dossier de l'international français serait proche de rendre une décision. Pour rappel, le président de la Commission juridique de la LFP André Soulier avait déclaré attendre une réponse claire de l'instance dirigeante du football mondial, qui avait dans un premier temps donné un avis consultatif négatif. Soulier en avait aussi profité pour faire la moral à Ben Arfa.
