TITRE: Basket - NBA : Memphis reste ma�tre chez lui - Sports - Nord Eclair
DATE: 27-01-2015
URL: http://www.nordeclair.fr/sports/basket-nba-memphis-reste-maitre-chez-lui-jna0b0n614657
PRINCIPAL: 1228097
TEXT:
- A +
Les Grizzlies ont battu les Orlando Magic (103-94) � Memphis, en prenant le large d�s la premi�re moiti� du match, et conservent la deuxi�me place de la Conf�rence Ouest � l�issue des matches du Championnat nord-am�ricain de basket disput�s lundi.
Les Grizzlies ont creus� l��cart dans la premi�re moiti� du match et comptaient dix-neuf longueurs d�avance � la mi-temps (67-48). Dans cette partie, Zach Randolph a r�ussi son quizi�me double-double cons�cutif, son 25e de la saison, en inscrivant 24 points et en d�livrant dix passes d�cisives.
Le Fran�ais Evan Fournier a marqu� 5 points pour Orlando en 16 minutes de jeu, avec seulement 2 paniers r�ussis sur six tentatives.
A Los Angeles, les Clippers, qui comptaient sept longueurs de retard apr�s le troisi�me quart-temps (69-76), ont renvers� la vapeur dans le dernier pour arracher la victoire (102-98) aux Denver Nuggets. Ils chipent � Houston la 4e place de la Conf�rence Ouest. Jamal Crawford a sonn� la charge pour les Clippers en inscrivant 19 des 31 points de son �quipe dans cet ultime quart-temps.
Dans les trois autres rencontres au programme de cette journ�e, opposant des �quipes de seconde moiti� de tableau, les P�licans de la Nouvelle-Orl�ans se sont distingu�s en mettant 25 points dans la vue aux Philadephia 76ers (99-74). Anthony Davis a �t� le h�ros de la soir�e, en enquillant 32 points dont cinq dunks rageurs, pour un total de 32 points et 10 rebonds.
�� On s�est tous amus�s, on a fait tourner le ballon, peu importe qui marque ��, a d�clar� Anthony Davis apr�s la victoire. �� On a tous jou� collectif, on s�est retrouss� les manches, et c�est tout ce que le coach demande ��.
Apr�s cette d�monstration, la Nouvelle-Orl�ans est 9e de la conf�rence Ouest, tout pr�s de Phoenix, et peut croire aux play-offs.
Lundi:
LA Clippers � Denver 102 � 98
Utah � Boston 90 � 99
La Nouvelle-Orleans � Philadelphie 99 � 74
Oklahoma City � Minnesota 92 � 84
