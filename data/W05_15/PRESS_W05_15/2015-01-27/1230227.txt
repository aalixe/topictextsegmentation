TITRE: Ibra, un rapport embarrassant - Football - Sports.fr
DATE: 27-01-2015
URL: http://www.sports.fr/football/ligue-1/articles/lyon-psg-sans-lacazette-ni-ibra-1175060/?sitemap
PRINCIPAL: 1230226
TEXT:
Zlatan Ibrahimovic et Maxime Gonalons (Reuters)
Par Thomas Siniecki
27 janvier 2015 � 21h10
Mis à jour le
27 janvier 2015 � 22h48
Zlatan Ibrahimovic pourrait manquer le rendez-vous du 8 février prochain à Gerland, entre l'Olympique Lyonnais et le Paris Saint-Germain. L'attaquant suédois du club de la capitale est dans le collimateur de la Commission de discipline pour un geste répréhensible commis contre le Stéphanois Romain Hamouma dimanche dernier.
Zlatan Ibrahimovic pourrait manquer le choc entre l'Olympique Lyonnais et le Paris Saint-Germain (24e journée de Ligue 1) dans moins de deux semaines, selon Le Parisien. Comme Alexandre Lacazette mais pour d'autres raisons, l'ancien Milanais pourrait donc être privé de cette affiche du championnat de France entre deux sérieux prétendants au titre. Et un tel match sans le meilleur buteur de la compétition ni sa grande star perdrait inévitablement en saveur...
Mais le Suédois sera peut-être bien suspendu après l'étude du rapport complémentaire dressé par Ruddy Buquet, l'arbitre de Saint-Etienne -Paris dimanche (0-1, 22e journée). Ibra s'en était sorti avec un simple carton jaune après son agression caractérisée sur le genou de Romain Hamouma dans les derniers instants du match.
A noter que l'attaquant parisien manquera quoi qu'il arrive le déplacement de la semaine prochaine à Lille en demi-finale Coupe de la Ligue, pour avoir reçu trois cartons jaunes en moins de 10 rencontres. Reste à voir quel timing adoptera la Commission de discipline de la LFP et si, par conséquent, la probable suspension supplémentaire inclura ou non la période du match à Lyon .
#Football @Ibra_official aura sans doute des nouvelles de la Commission de visionnage... https://t.co/36nWnc8HJF
