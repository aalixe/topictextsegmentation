TITRE: Marseille : un jeune homme bless� par balles dans les quartiers Nord
DATE: 27-01-2015
URL: http://www.24matins.fr/marseille-un-jeune-homme-blesse-par-balles-dans-les-quartiers-nord-151090
PRINCIPAL: 1229173
TEXT:
>        Marseille : un jeune homme bless� par balles dans les quartiers Nord
Faits Divers
Photo d'illustration
Ce mardi, un homme de 26 ans a �t� bless� par balles dans les quartiers Nord de Marseille. Son pronostic vital n'est cependant pas engag�.
L'information nous provient directement de La Provence , le journal phoc�en rapportant plusieurs coups de feu ayant �t� tir�s ce mardi aux alentours de 12h30 dans les quartiers Nord de Marseille . La victime, un homme de 26 ans ayant �t� lourdement touch� aux jambes.
Il �tait pr�vu que le jeune homme soit hospitalis� apr�s avoir �t� soign� sur place par les marins pompiers. La Provence pr�cise toutefois que les jours de la victime ne sont pas en danger, en ajoutant que la Police judiciaire sera vraisemblablement d�sign�e pour enqu�ter sur cette affaire. Et ce pour notamment d�terminer le ou les auteur(s) de l'attaque de m�me que les circonstances de cette derni�re.
Bless� par balles � Marseille : un nouveau r�glement de comptes ?
Ce fait nous en rappelle un autre du m�me type apparent survenu il y a quelques jours en Seine-Saint-Denis. Mais la cit� o� ont eu lieu les tirs de mardi a �galement �t� le th��tre d'un d�c�s le 14 janvier dernier, apr�s qu'un homme de 25 ans ait re�u une balle dans la t�te . Le site 20minutes.fr nous rappelle que la victime �tait "un peu" connue des services policiers et qu'il s'agissait l� du premier r�glement de comptes de l'ann�e � Marseille.
Cr�dits photos : � Pixabay / Public Domain
Partager cet article
