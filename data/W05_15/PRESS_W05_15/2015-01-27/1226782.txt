TITRE: �gypte : 516 arrestations apr�s les heurts de dimanche | Site mobile Le Point
DATE: 27-01-2015
URL: http://www.lepoint.fr/monde/egypte-516-arrestations-apres-les-heurts-de-dimanche-26-01-2015-1899780_24.php
PRINCIPAL: 1226777
TEXT:
26/01/15 � 18h44
�gypte : 516 arrestations apr�s les heurts de dimanche
Les autorit�s r�priment f�rocement la confr�rie dont est issu l'ancien chef de l'�tat Mohamed Morsi, depuis que ce dernier a �t� �vinc� en juillet 2011.
Des heurts ont eu lieu au Caire le dimanche 25 janvier, lors d'une manifestation pour c�l�brer le quatri�me anniversaire de la r�volution �gyptienne. ALAIN COUTURIER / AFP
Source AFP
La police a arr�t� 516 partisans des Fr�res musulmans, la confr�rie islamiste interdite, lors des heurts dimanche entre police et manifestants rassembl�s � l'occasion de l'anniversaire de la r�volte de 2011, a annonc� lundi le ministre de l'Int�rieur. "Hier (dimanche), nous avons arr�t� 516 individus li�s aux Fr�res musulmans, soup�onn�s d'avoir tir� des munitions, pos� des bombes et fait exploser certains locaux", a d�clar� le ministre Mohamed Ibrahim devant la presse au Caire.
Selon Mohamed Ibrahim, 20 personnes, dont deux policiers, ont �t� tu�es dans les affrontements ayant �clat� entre des manifestants majoritairement islamistes et les forces de s�curit�. Selon lui, la plupart des victimes ont trouv� la mort dans un quartier du nord du Caire, Matareya, o� les heurts ont dur� plus de douze heures. Le minist�re de la Sant� a de son c�t� fait �tat lundi d'un bilan de 20 morts, dont un policier.
Les autorit�s �gyptiennes r�priment f�rocement la confr�rie dont est issu l'ancien chef de l'�tat Mohamed Morsi, depuis que ce dernier a �t� �vinc� en juillet 2013 par Abdel Fattah al-Sissi, qui dirigeait alors l'arm�e et a depuis �t� �lu pr�sident. Des organisations de d�fense des droits de l'homme, telles que Human Rights Watch, ont d�nonc� � maintes reprises "l'usage excessif de la force" par la police en �gypte "contre des manifestations pacifiques".
