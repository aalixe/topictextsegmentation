TITRE: AUTOhebdo.fr | Magazine
DATE: 27-01-2015
URL: http://www.autohebdo.fr/magazine-449-2-wrc_monte_carlo_entretien_sebastien_ogier
PRINCIPAL: 1227513
TEXT:
WEC ? Porsche hausse le rythme
��AUTOhebdo
Apr�s une premi�re saison pleine de promesses, r�compens�e par  une victoire au Br�sil, la firme de Stuttgart aff�te les nouveaux arguments qu?elle opposera � Toyota et Audi cette ann�e en LM P1. Avec un double objectif�: s?imposer au Mans et d�crocher le titre mondial.
Lanc�e � plus de 330 km/h, la 919 Hybrid avale les 1800 m�tres de la ligne droite du Mistral, sur le circuit Paul-Ricard, avec une voracit� sid�rante. Qui pourrait croire, face � un tel spectacle, que son moteur est un modeste V4 turbo essence de 2.0 litres de cylindr�e�? L?info n?est pas nouvelle, mais l?effet reste le m�me�: cette voiture est un audacieux pari technologique. Un d�tail surprend malgr� tout. La Porsche �met une sonorit� puissante quand, l?an dernier pour ses d�buts, le souffle de sa m�canique �tait �teint. L� encore, sans �tre inform� en amont, il serait impossible � l?oreille de reconna�tre les caract�ristiques du c?ur battant dans la 919.
(...)
