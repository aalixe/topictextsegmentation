TITRE: Argentine : dissolution du service de renseignement apr�s la mort d�un procureur | Mediapart
DATE: 27-01-2015
URL: http://www.mediapart.fr/journal/international/270115/argentine-dissolution-du-service-de-renseignement-apres-la-mort-d-un-procureur
PRINCIPAL: 1228382
TEXT:
La lecture des articles est r�serv�e aux abonn�s.
La pr�sident argentine Cristina Kirchner est intervenue � la t�l�vision, lundi 26 janvier, pour annoncer la dissolution du SI, le principal service de renseignements du pays, peut-on lire sur le site du Monde.Elle avait pr�c�demment mis en cause plusieurs de ses membres dans la mort suspecte du magistrat Alberto Nisman, en charge notamment depuis dix ans de l'enqu�te sur l'attentat de la mutuelle juive AMIA en 1994. Il avait accus�, peu avant sa mort, Mme Kirchner d'avoir entrav� l'enqu�te pour prot�ger l'Iran de toute mise en cause et n�gocier des contrats commerciaux.
Lire aussi     Argentine: la mort myst�rieuse d�un procureur ravive de vilains souvenirs     Par Lamia Oualalou
Selon les premiers �l�ments de l'enqu�te, ce dernier s'est tir� une balle ...
