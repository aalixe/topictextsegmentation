TITRE: Anticoagulants: Les nouveaux produits n'ont pas la m�me efficacit� - 20minutes.fr
DATE: 27-01-2015
URL: http://www.20minutes.fr/sante/1526747-20150127-anticoagulants-tous-produits-efficacite
PRINCIPAL: 0
TEXT:
Sant�
Ils se nomment Eliquis, Xarelto et Pradaxa, et sont les trois NACO (nouveaux anticoagulants oraux) mis sur le march� voici plusieurs ann�es. Tr�s diff�rents dans leur composition, leur maniement est de fait rendu difficile et n�cessite une surveillance renforc�e. La Commission de la transparence de la haute autorit� de sant� (HAS) suit ainsi de pr�s ces trois produits, et vient d'ailleurs d'en r��valuer les r�sultats afin de les hi�rarchiser, dans la strat�gie de soins, selon leur efficacit�.
Selon la HAS, il s'av�re que ��le service m�dical rendu par les NACO diff�re d'une mol�cule � l'autre:�il reste important pour l'Eliquis et le Xarelto, mais est mod�r� pour le Pradaxa. L'am�lioration du service m�dical rendu par l'Eliquis est en outre mineure par rapport aux antivitamines K (ou AVK, les anticoagulants ancienne g�n�ration, NDLR), et il n'y a pas d'am�lioration du service m�dical rendu par rapport aux AVK pour le Pradaxa et le�Xarelto�.
De nouvelles conclusions dans un an
Les NACO doivent ainsi �tre r�serv�s, selon les recommandations de la HAS, �aux patients pour lesquels le maintien de l'INR (examen biologique qui mesure le niveau d'anticoagulation, NDLR) d�sir� dans la zone cible n'est pas assur�, et aux patients pour lesquels les AVK sont contre-indiqu�s ou mal tol�r�s ou qui acceptent mal les contraintes li�es � ce m�dicament�. La HAS devrait �tablir de nouvelles conclusions dans un an, apr�s une r��valuation de l'ensemble de ces m�dicaments.
Pour rappel, les NACO sont arriv�s sur le march� en France en 2008. Initialement prescrits dans la pr�vention de l'embolie lors des op�rations de chirurgie de la hanche et du genou, leurs indications ont �t� �largies quatre ans plus tard � la pr�vention des AVC (accidents vasculaires c�r�braux) et des embolies chez les personnes souffrant de troubles du rythme cardiaque, entrant ainsi en concurrence avec les traditionnels AVK.
