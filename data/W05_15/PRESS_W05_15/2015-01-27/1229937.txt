TITRE: 3G et 4G : les premières lignes du métro et du RER couvertes d'ici fin 2015
DATE: 27-01-2015
URL: http://www.numerama.com/magazine/32026-3g-et-4g-les-premieres-lignes-du-metro-et-du-rer-couvertes-d-ici-fin-2015.html
PRINCIPAL: 1229935
TEXT:
Publi� par Julien L., le Mardi 27 Janvier 2015
3G et 4G : les premi�res lignes du m�tro et du RER couvertes d'ici fin 2015
La RATP annonce le d�ploiement de la 3G et de la 4G sur les lignes 1 et A du m�tro et du RER d'ici la fin de l'ann�e. L'ensemble de son r�seau sera quant � lui couvert d'ici fin 2017.
2
L'acc�s � l'Internet mobile dans le RER et le m�tro sera bient�t de meilleure qualit�. C'est en effet � partir de cette ann�e que la RATP va �tendre la couverture 3G et 4G � l'ensemble du r�seau ferroviaire d'�le-de-France. Ce d�ploiement progressif : dans un premier temps, deux voies seront enti�rement �quip�es d'ici d�cembre, � savoir la ligne 1 du m�tro de Paris et la ligne A du RER.
En 2015, les lignes 1 & A seront les 1�res lignes �quip�es en totalit� en #3G et #4G . Emmanuel Pitron secr�taire g�n�ral du Groupe #RATP 2/2
� Groupe RATP (@GroupeRATP) 27 Janvier 2015
L'objectif que se fixe la RATP est de couvrir l'ensemble des lignes d'ici la fin 2017. Cela laisse donc deux ans � la r�gie pour s'occuper des seize lignes du m�tro, dont la longueur cumul�e atteint 220 kilom�tres, et des lignes A et B du RER (� l'exception de certains tron�ons), dont la longueur atteint 115 kilom�tres. Les autres portions et les lignes C, D et E sont g�r�es par la SNCF.
#RATP Le d�ploiement de la #3G / #4G dans son int�gralit� sur le r�seau #m�tro et #RER est pr�vu pour fin 2017. 1/2
� Groupe RATP (@GroupeRATP) 27 Janvier 2015
L'arriv�e de la 3G et de la 4G est un sujet vieux de quelques ann�es chez la RATP, consciente que la 2G, lorsqu'elle est disponible, n'est plus suffisante . Un premier accord avait �t� sign� en juillet 2012 avec SFR , puis peu de temps apr�s avec Bouygues Telecom . Orange s'est lanc� dans l'aventure en d�cembre 2013. De son c�t�, Free Mobile a sign� un partenariat le mois dernier.
