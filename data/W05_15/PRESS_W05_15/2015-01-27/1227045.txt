TITRE: Fiona: aggravation des poursuites contre la m�re de la fillette | France | Nice-Matin
DATE: 27-01-2015
URL: http://www.nicematin.com/france/affaire-fiona-aggravation-des-poursuites-contre-la-mere-de-la-fillette.2079365.html
PRINCIPAL: 1227044
TEXT:
Tweet
Clermont-Ferrand (AFP)
La m�re de la petite Fiona, fillette disparue en mai 2013 � Clermont-Ferrand et dont le corps n'a jamais �t� retrouv�, s'est vu notifier lundi une mise en examen suppl�tive pour des violences commises "en r�union" sur l'enfant, a-t-on appris aupr�s de son avocat.
Son compagnon Berkane Makhlouf convoqu� comme elle devant le juge d'instruction lundi, a lui aussi vu s'alourdir les charges pesant contre lui "apr�s un interrogatoire synth�tique", selon son avocat, confirmant une information du Parisien.
Incarc�r�s depuis octobre 2013, C�cile Bourgeon et Berkane Makhlouf �taient tous deux mis en examen, � ce stade, pour des "violences volontaires sur mineur de 15 ans par ascendant ayant entra�n� la mort sans l'intention de la donner".
Pour l'avocat de la m�re, Me Renaud Portejoie, cette troisi�me circonstance aggravante - le fait que ces violences pr�sum�es aient �t� commises "en r�union" - "ne modifie en rien la peine encourue" par la jeune femme, soit 30 ans de r�clusion criminelle.
"C'est un aveu de faiblesse de l'accusation qui, en fin d'instruction, cherche la responsabilit� de C�cile Bourgeon d�sormais plus sur un terrain collectif qu'� titre individuel", a-t-il d�clar�.
Apr�s plus de quatre mois de mensonges sur le sort de la fillette de cinq ans, port�e disparue le 12 mai 2013 dans un parc de la capitale auvergnate, le couple Bourgeon-Makhlouf avait avou� fin septembre qu'elle �tait morte et que tous deux l'avaient enterr�e � la lisi�re d'une for�t.
Depuis, chacun se renvoie la responsabilit� des coups port�s � la fillette, qui auraient pu causer sa mort.
"Ils se sont accord�s � enterrer ensemble le corps mais l'incrimination n'est pas conforme aux �l�ments criminels du dossier", estime pour sa part le conseil de Berkane Makhlouf, Me Mohamed Khanifar.
"Avec cette mise en examen suppl�tive, on a plus de certitudes de condamner les deux plut�t qu'un seul, sachant que le complice encourt les m�mes peines que le coauteur", a-t-il pr�cis�.
"C'est vraiment pour l'accusation (un moyen) d'obtenir plus facilement une condamnation lors du proc�s. Ils ont commis des violences, on ne sait pas qui a fait quoi, donc on envisage de les condamner tous les deux", a insist� Me Portejoie, qui a par ailleurs annonc� qu'il allait saisir la chambre de l'instruction de la cour d'appel de Riom (Puy-de-D�me) dans le but de faire annuler cette mise en examen suppl�tive.
Ce recours va retarder encore le proc�s de l'affaire qui ne devrait pas se tenir avant fin 2015 ou d�but 2016, bien que l'instruction soit � ce jour quasiment termin�e, selon Me Portejoie. Une pr�c�dente requalification des chefs d'accusation visant la m�re, initialement poursuivie pour recel de cadavre et non-assistance � personne en danger, a fait l'objet d'un pourvoi en cassation.
Le corps de la petite Fiona n'a jamais �t� retrouv� malgr� trois op�rations de recherches autour du lac d'Aydat (Puy-de-D�me), � une vingtaine de kilom�tres au sud de Clermont-Ferrand.
Tweet
Tous droits de reproduction et de repr�sentation r�serv�s. � 2012 Agence France-Presse.
Toutes les informations (texte, photo, vid�o, infographie fixe ou anim�e, contenu sonore ou multim�dia) reproduites dans cette rubrique (ou sur cette page selon le cas) sont prot�g�es par la l�gislation en vigueur sur les droits de propri�t� intellectuelle. �Par cons�quent, toute reproduction, repr�sentation, modification, traduction, exploitation commerciale ou r�utilisation de quelque mani�re que ce soit est interdite sans l�accord pr�alable �crit de l�AFP, � l�exception de l�usage non commercial personnel. �L�AFP ne pourra �tre tenue pour responsable des retards, erreurs, omissions qui ne peuvent �tre exclus dans le domaine des informations de presse, ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
AFP et son logo sont des marques d�pos�es.
"Source AFP" ? 2015 AFP
France
Le d�lai de 15 jours au-del� duquel il n'est plus possible de contribuer � l'article est d�sormais atteint.
Vous pouvez poursuivre votre discussion dans les forums de discussion du site. Si aucun d�bat ne correspond � votre discussion, vous pouvez solliciter le mod�rateur pour la cr�ation d'un nouveau forum : moderateur@nicematin.com
