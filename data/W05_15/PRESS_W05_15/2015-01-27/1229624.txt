TITRE: TV : ce soir, on découvre l’univers de la maison Dior - Elle
DATE: 27-01-2015
URL: http://www.elle.fr/Loisirs/Sorties/News/TV-ce-soir-on-decouvre-l-univers-de-la-maison-Dior-2881792
PRINCIPAL: 0
TEXT:
Quel programme choisir ce soir à la télé�? Une immersion dans une grande maison de couture, une romance britannique ou un documentaire écologique�: voici ce que l’on vous suggère de regarder.
«�Dior et moi�», documentaire, à 23h05 sur Canal+
Le 9 avril 2012, le couturier belge Raf Simons reprenait la direction artistique des collections Dior. Durant 1h30, le réalisateur Frédéric Tcheng, à qui l’on doit également le long-métrage sur Diana Vreeland «�The Eye Has to Travel�», nous fait découvrir l’univers de la légendaire maison de couture et le parcours du jeune créateur. Un «�très beau documentaire�» à ne rater sous aucun prétexte .�
«�The Edge of Love�», un film de John Maybury, à 23h sur Chérie 25
Dix ans après avoir vécu une belle histoire d’amour, Vera, une chanteuse de cabaret et Dylan Thomas, un des plus grands poètes du XXe siècle, se retrouvent par hasard. Lui est devenu célèbre et s’est marié avec Caitlin. Celle-ci ne voit pas d’un très bon œil le retour de Vera dans la vie de son époux. Une rivalité va naître, avant de se transformer en une amitié inébranlable. Un scénario bien rodé avec l’irrésistible Keira Knightley, la cover girl du magazine ELLE en kiosque cette semaine.
«�La Fin du poisson à foison ?�», reportage, à 20h50 sur Arte
Ce n’est plus un secret, la pêche est pratiquée de manière excessive, et les océans se vident. Afin de lutter contre ces ravages, Maria Damanaki, commissaire européenne aux Affaires maritimes et à la Pêche, a mis en place une réforme contre la surexploitation. Ce reportage de la journaliste Jutta Pinzler revient sur ce combat et sur celui des ONG qui dénoncent les conditions de travail de nombreux pêcheurs, notamment en Thaïlande.
�
