TITRE: CM (Hommes) -         Khoroshilov le plus rapide
DATE: 27-01-2015
URL: http://www.lequipe.fr/Ski/Actualites/Khorosilov-mene-la-danse/531762
PRINCIPAL: 1229659
TEXT:
a+ a- imprimer RSS
Alexander Khoroshilov est en t�te � l'issue de la premi�re manche. (Reuters)
C'est parti tr�s vite, mardi sur les pentes de Schladming. Dossard n�6, Alexander Khoroshilov a sign� le meilleur chrono de la premi�re manche . Il a repouss� � 0''79 Felix Neureuther, vainqueur cette saison des slaloms de Madonna di Campiglio et Wengen . Lequel avait d�j� devanc� Marcel Hirscher de 0''55. L'Autrichien,� champion du monde de la sp�cialit� et le leader du classement g�n�ral de la Coupe du monde , se trouve � 1''34 de Khoroshilov, alors que la seconde manche est programm�e � 20h45. Les skieurs allemands ont, eux, r�ussi un beau tir group�, Felix Dopfer se classant troisi�me (+1''08).
Les Fran�ais, de leur c�t�, n'ont pas brill� sur la neige de Schladming. Avant m�me le d�part de la deuxi�me manche, ils sont loin au classement. A 2''65 d'Alexander Khoroshilov, Julien Lizeroux, l'ancien vice-champion du monde (en 2009 � Val-d'Is�re) est le mieux loti des Tricolores. Jean-Baptiste Grange (+2''68), Victor Muffat-Jeandet (+2''83) et Alexis Pinturault (+2''93), vainqueur la semaine derni�re du super-combin� de Kitzb�hel, le suivent au classement. Steve Missilier (+3''08) est encore plus loin.
O.P.
