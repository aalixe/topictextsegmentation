TITRE: Mondial-2022: la Fifa doit revoter, selon Conseil de l'Europe | Site mobile Le Point
DATE: 27-01-2015
URL: http://www.lepoint.fr/sport/foot-fifa-doit-revoter-pour-le-mondial-2022-selon-conseil-de-l-europe-27-01-2015-1899962_26.php
PRINCIPAL: 1229971
TEXT:
27/01/15 � 11h37
Mondial-2022: la Fifa doit revoter, selon Conseil de l'Europe
Le pr�sident de la Fifa Sepp Blatter d�voile le bulletin de l'attribution de l'organisation du Mondial-2022 au Qatar, le 2 d�cembre 2010 � Zurich AFP/Archives -
Un rapport du Conseil de l'Europe, adopt� mardi, demande � la Fifa de proc�der � un nouveau vote pour attribuer l'organisation du Mondial-2022 de football, apr�s la proc�dure "profond�ment entach�e d'ill�galit�" emport�e par le Qatar .
Apr�s des r�v�lations sur cette d�cision "radicalement vici�e", "la Fifa ne saurait se d�rober de son obligation de proc�der � un nouveau vote", selon ce rapport de la commission de l'Assembl�e parlementaire du Conseil de l'Europe (APCE).
Les parlementaires y regrettent que l'enqu�te de la chambre d'instruction du comit� d'�thique de la Fifa, qui a mis au jour des "pratiques extr�mement douteuses", n'ait pas �t� enti�rement publi�e. Et ils fustigent la "facilit�" avec laquelle la Fifa a confirm� la d�signation du Qatar malgr� cette enqu�te.
Le d�put� travailliste britannique Michael Connarty, rapporteur de ce projet de r�solution parlementaire, d�plore sans d�tours une "farce" et "une tentative d'�touffer l'affaire" de la part des instances mondiales du football.
Dans son expos� des motifs, il dit avoir consult� des documents r�v�l�s par le Sunday Times , prouvant qu'un membre qatari du Comit� ex�cutif de la Fifa, Mohamed Bin Hammam, avait vers� d'importantes sommes d'argent pour s'attirer le soutien de f�d�rations africaines de football lors de l'attribution du Mondial-2022.
"L'affirmation concernant l'absence d'une responsabilit� directe du Qatar pour les agissements de M. Bin Hammam ne saurait rendre valide une proc�dure si profond�ment entach�e d'ill�galit�", insistent les parlementaires europ�ens.
Ils demandent aussi au Qatar, vivement critiqu� au sujet des conditions de travail des immigr�s employ�s sur les chantiers du Mondial, "de prendre sans d�lai toutes les mesures n�cessaires pour garantir le respect des droits fondamentaux" de ces migrants.
Le texte et la r�solution qui l'accompagne doivent �tre soumis en avril en session pl�ni�re � l'APCE, qui regroupe � Strasbourg plus de 300 parlementaires des 47 �tats membres du Conseil de l'Europe. Une r�solution de cette assembl�e n'a pas de valeur contraignante, mais constitue une pression politique.
Depuis l'attribution en 2010 du Mondial-2022 au Qatar, ce pays et la Fifa sont au centre de nombreuses pol�miques. Mais le pr�sident de la f�d�ration internationale, Joseph Blatter, a pr�venu en d�cembre qu'il "faudrait vraiment qu'il y ait un s�isme" pour retirer l'organisation du Mondial au Qatar.
27/01/2015 17:35:31 - Strasbourg (AFP) - � 2015 AFP
