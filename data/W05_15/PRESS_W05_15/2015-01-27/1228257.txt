TITRE: Michel Houel�le�becq: ��Il y aura d'autres atten�tats�� - Gala
DATE: 27-01-2015
URL: http://www.gala.fr/l_actu/news_de_stars/michel_houellebecq_il_y_aura_d_autres_attentats_334305
PRINCIPAL: 1228253
TEXT:
char�lie hebdo
Partager
Michel Houel�le�becq �tait invit� ce matin sur RTL. Pour la premi�re fois depuis les atten�tats contre Char�lie Hebdo et l'Hyper Cacher, le roman�cier est revenu en France s'expri�mer � propos de son livre pol�mique Soumis�sion et des attaques terro�ristes dans lesquelles il a perdu des amis.
��Pour ne pas les faire se d�pla�cer pour rien, �a oblige � faire des listes de courses. � part �a, �a ne change rien. Je ne trouve pas �a g�nant.�� Sur RTL Michel Houel�le�becq en rit, mais il sait bien la gravit� de la chose�: l'�cri�vain est actuel�le�ment sous protec�tion poli�ci�re. Une mesure indis�pen�sable depuis les atten�tats meur�triers de d�but janvier, alors que son livre Soumis�sion est accus� d'isla�mo�pho�bie ���bien que l'auteur s'en soit souvent d�fendu. ��Ce qui fait peur dans le livre c'est la premi�re partie, estime-t-il. Des gens non repr�sen�t�s, qui sentent que la d�mo�cra�tie ne marche plus, se radi�ca�lisent.��
�
Michel Houel�le�becq regrette que la libert� d'expres�sion soit syst�ma�tique�ment remise en cause lorsqu'il s'agit de reli�gion . ��On pratique le parti de ceux qui disent�: 'La libert� d'expres�sion c'est tr�s bien mais il ne faut pas blas�ph�mer, il ne faut pas insul�ter les reli�gions'. Et il a re�u un soutien de poids avec le Pape, d�plore-t-il.�Les enne�mis de la libert� d'expres�sion sont toujours l�.��
�
��C'est quand m�me une grosse victoire pour les djiha�distes, pour�suit l'�cri�vain.�La France �tait une de leurs cibles privi�l�gi�es. Char�lie Hebdo �tait leur cible la plus c�l�bre. Ils ont remport� une vraie victoire. J'�tais rest� sur l'id�e qu'il �tait assez diffi�cile de se procu�rer des armes de guerre en France. Appa�rem�ment pas tant que �a. Ces gens ont pour projet de d�clen�cher une guerre civile en France. La seule chose qui leur manque, c'est des adver�saires. Je pense qu'il y aura d'autres atten�tats avec la cible perma�nente, les juifs.�� Une pr�dic�tion qui rejoint les pr�c�dentes d�cla�ra�tions de Michel Houel�le�becq, qui confiait il y a pr�s de deux semaines avoir ��peur�� de poten�tielles nouvelles attaques.
Cr�dits photos : Abaca
