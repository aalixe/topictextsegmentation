TITRE: Gr�ce: la bourse d'Ath�nes chute de plus de 5% apr�s l'annonce du gouvernement
DATE: 27-01-2015
URL: http://www.romandie.com/news/Grece-la-bourse-dAthenes-chute-de-plus-de-5-apres-lannonce-du-gouvernement_RP/559614.rom
PRINCIPAL: 1229273
TEXT:
Tweet
Gr�ce: la bourse d'Ath�nes chute de plus de 5% apr�s l'annonce du gouvernement
Ath�nes - L'indice g�n�ral de la Bourse d'Ath�nes (Athex) a chut� de plus 5% mardi alors qu'�tait annonc�e la composition du gouvernement grec du Premier ministre de gauche radicale Alexis Tsipras.
L'indice g�n�ral qui avait ouvert dans le rouge � 813 points est descendu jusqu'� 761 points soit -6,39% en milieu d'apr�s-midi, � peu pr�s au moment o� �tait annonc�e la liste du nouveau gouvernement grec men� par la gauche radicale Syriza.
L'Athex est ensuite l�g�rement remont� � 776 points vers 14h00 GMT (-4,59%)
L'universitaire Yanis Varoufakis, 53 ans, �conomiste pourfendeur de la dette odieuse et partisan de la fin des mesures d'aust�rit�, prend le portefeuille central des Finances, et sera ainsi en charge des n�gociations sur la  dette avec les cr�anciers du pays.
La r�duction de la dette publique et le programme d'aide de l'UE et du FMI � la Gr�ce sont parmi les sujets chauds � g�rer par le nouveau gouvernement d'Alexis Tsipras, et seront d'ores au d�j� au centre de la discussion pr�vue vendredi � Ath�nes avec Jeroen Dijsselbloem, le chef de l'Eurogroupe.
(�AFP / 27 janvier 2015 15h22)
�
