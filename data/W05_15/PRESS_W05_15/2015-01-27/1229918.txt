TITRE: Aides agricoles. La France va devoir rendre � l'UE un milliard d'euros
DATE: 27-01-2015
URL: http://www.ouest-france.fr/aides-agricoles-la-france-va-devoir-rendre-lue-un-milliard-deuros-3145470
PRINCIPAL: 0
TEXT:
Aides agricoles. La France va devoir rendre � l'UE un milliard d'euros
France -
La France va devoir rendre � l'UE plus de 1 milliard d'euros d'aides agricoles vers�es � tort, pour cause de fraudes et d'erreurs, pour la p�riode 2008-2012.�|�Cr�dit photo : Jo�l Le Gall
Facebook
La France va devoir rendre � l'UE plus de 1 milliard d'euros d'aides agricoles vers�es � tort, pour cause de fraudes et d'erreurs, pour la p�riode 2008-2012.
C'est ce qu'a indiqu� la Commission europ�enne ce mardi.
Cette somme, correspondant � environ 2% des quelque 40 milliards d'euros per�us par la France pour cette p�riode dans le cadre de la Politique agricole commune (PAC), devra �tre rembours�e en trois tranches jusqu'en 2017, a pr�cis� une source europ�enne.
Tags :
