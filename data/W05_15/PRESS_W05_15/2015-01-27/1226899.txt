TITRE: Standard & Poor's : La Russie rel�gu�e dans la cat�gorie "sp�culative". Info - Draguignan.maville.com
DATE: 27-01-2015
URL: http://www.draguignan.maville.com/actu/actudet_-standard-%26-poor-s-la-russie-releguee-dans-la-categorie-speculative_54135-2703709_actu.Htm
PRINCIPAL: 1226897
TEXT:
Twitter
Google +
L'agence de notation Standard's & Poor's a abaiss� la note de la dette russe, la faisant passer dans la cat�gorie "sp�culative".� AFP
La Russie a subi un camouflet de la part de l'agence de notation Standard's & Poor's, qui a abaiss� la note de sa dette, la faisant passer dans la cat�gorie "sp�culative".
Cette cat�gorie est boud�e par les investisseurs, ce qui risque d'aggraver la pire crise financi�re travers�e par ce pays en 15 ans de pouvoir de Vladimir Poutine.�
Le ministre russe des Finances Anton Silouanov a aussit�t d�nonc� un ��pessimisme excessif��, ne voyant ��pas de raisons de dramatiser�� cette d�cision, pourtant accueillie sur les march�s par un plongeon de plus de 6�% du rouble.�La note d'endettement � long terme de la Russie a �t� abaiss�e d'un cran, � ��BB+��, et S&P a indiqu� ne pas exclure de la faire encore reculer.�
La Russie isol�e comme jamais
Cette d�cision intervient apr�s une ann�e qui aura vu la Russie post-sovi�tique s'isoler comme jamais des Occidentaux en raison de l'annexion de la Crim�e et de son soutien - y compris militaire selon Kiev - aux s�paratistes de l'est de l'Ukraine, puis subir de plein fouet les effets de la chute des cours du p�trole.�
S&P a mis en avant la d�t�rioration de la flexibilit� mon�taire du pays ainsi que la d�gradation de ses perspectives de croissance, r�sultat des sanctions �conomiques europ�ennes et am�ricaines introduites en liaisson avec la crise ukrainienne.
Dette souveraine "pourrie"
Chez Standard & Poor's, la Russie est le seul �tat du groupe des pays �mergents dit des BRICS (Br�sil, Russie, Inde, Chine et Afrique du Sud) � voir la note de sa dette � long terme ainsi plac�e dans la cat�gorie des dettes souveraines ��pourries�� en jargon financier.�
Le ministre russe de l'�conomie, Alexe� Ouliouka�ev, avait jug� r�cemment ��assez forte�� la probabilit� de voir la note de la Russie abaiss�e dans la cat�gorie ��sp�culative��, au m�me niveau que des pays bien moins puissants comme la Bulgarie et l'Indon�sie.�
Il avait aussi pr�venu qu'une telle r�trogradation, qui �carte automatiquement la dette russe du portefeuille de certains investisseurs, aurait des ��cons�quences mat�rielles�� pour Moscou, qui pourrait se trouver contraint de rembourser de mani�re anticip�e jusqu'� 30�milliards de dollars de dette.�
Autrement dit, l'�conomie russe s'expose � un nouveau coup de boutoir, apr�s avoir subi en 2014 des fuites de capitaux record depuis la chute de l'URSS en 2014, de plus de 150 milliards de dollars.�
Effondrement du rouble
L'effondrement du rouble (-41�% face au dollar en 2014) s'est traduit par une envol�e des prix, le taux annuel d'inflation d�passant d�j� 11�%.�
Pour 2015, en raison des sanctions �conomiques occidentales et de la chute des cours du p�trole, premi�re source de revenus de la Russie, les autorit�s pr�voient une r�cession qui pourrait atteindre 5�%, si l'��or noir�� reste � ses niveaux actuels.�
��La d�cision qui a �t� prise d�montre un pessimisme excessif et ne prend pas en compte toute une s�rie de facteurs qui font la force de l'�conomie russe��, a tent� de rassurer lundi soir le ministre Anton Silouanov, citant les importantes r�serves de devises accumul�es par Moscou ainsi que le faible niveau de sa dette ext�rieure.�
��Il n'y a pas mati�re � parler de raisons objectives pour les investisseurs �trangers de se d�faire des actifs russes��, a-t-il martel�, notant que S&P �tait la seule agence � donner � la Russie une si mauvaise note.�
Investisseurs alarmistes
Le g�ant p�trolier public Rosneft, tr�s endett�, a assur� se sentir ��s�r de lui�� pour fonctionner sans probl�me, selon l'agence de presse Ria-Novosti.�
Les investisseurs ont sembl� plus alarmistes et lundi soir le rouble, d�j� malmen� par le regain des violences en Ukraine et le spectre de nouvelles sanctions, a de nouveau d�viss�.�
La devise a chut� jusqu'� 69,29 roubles pour un dollar et 77,89 roubles pour un euro � la Bourse de Moscou, soit � son plus bas depuis les jours les plus sombres de mi-d�cembre. Le mouvement de panique des march�s avait alors gagn� la population, qui avait retir� en masse des fonds des comptes bancaires.�
Des banques vont dispara�tre
Malgr� un plan d'aide massif au secteur, les autorit�s ne cachent pas qu'il faut s'attendre � voir des banques dispara�tre dans les mois � venir.�
Selon le num�ro deux du gouvernement russe, Igor Chouvalov, la situation �conomique de la Russie est plus difficile que pendant la crise de 2008-2009, mais la confrontation avec les Occidentaux offre une bonne occasion de proc�der des r�formes structurelles.�
Standard & Poor's a apport� une r�ponse cinglante � cet �gard lundi, disant ne pas ��s'attendre � ce que le gouvernement se montre capable de surmonter les obstacles structurels connus de longue date��.
Ouest-France��
