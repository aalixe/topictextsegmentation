TITRE: Egypte: HRW d�nonce l'usage excessif de la force par la police face aux manifestants
DATE: 27-01-2015
URL: http://www.romandie.com/news/Egypte-HRW-denonce-lusage-excessif-de-la-force-par-la-police-face_RP/559050.rom
PRINCIPAL: 1226882
TEXT:
Tweet
Egypte: HRW d�nonce l'usage excessif de la force par la police face aux manifestants
Le Caire - Human Rights Watch a d�nonc� lundi l'usage excessif de la force par la police en Egypte contre des manifestations pacifiques, apr�s la mort d'au moins 19 personnes dimanche dans des rassemblements marquant le quatri�me anniversaire de la r�volte de 2011.
Quatre ans apr�s la r�volution, la police tue toujours r�guli�rement des manifestants, a d�nonc� dans un communiqu� la directrice pour le Moyen-Orient de Human Rights Watch (HRW), Sarah Leah Whiston.
Les abus de la police �taient d�j� l'une des causes principales du soul�vement populaire qui a chass� le pr�sident Hosni Moubarak du pouvoir.
Dimanche, au moins 19 civils, pour la plupart des manifestants islamistes, ont �t� tu�s lors de heurts avec les forces de s�curit�, dans des rassemblements organis�s par les partisans du pr�sident islamiste Mohamed Morsi, destitu� par l'arm�e en 2013.
Au moment o� le pr�sident Abdel Fattah al-Sissi �tait � Davos pour soigner son image internationale, ses forces de s�curit� avaient recours � la violence contre des Egyptiens participant � des manifestations pacifiques, ass�ne HRW.
Samedi, Sha�maa al-Sabbagh, une manifestante de 34 ans d'un parti la�c de gauche, avait �t� tu�e par un tir de chevrotine lors de heurts avec la police, durant un rassemblement pour comm�morer la r�volte du 25 janvier 2011.
Des manifestants assurent que la jeune femme, m�re d'un enfant de 5 ans, a �t� tu�e par la police, ce que le minist�re de l'Int�rieur nie. Le parquet a ouvert une enqu�te.
Le rapport m�dico-l�gal affirme que la manifestante a �t� touch�e dans le dos, selon HRW, qui pr�cise que m�me si les vid�os et les photos ne montrent pas quand et comment elle a �t� tu�e, elles montrent que certains membres des forces de s�curit� �taient arm�s de fusils.
R�agissant au communiqu� de HRW, le ministre de l'Int�rieur Mohamed Ibrahim a estim� lors d'une conf�rence de presse que l'organisation n'�tait jamais objective dans ses rapports, accusant la confr�rie des Fr�res musulmans de M. Morsi d'�tre responsable des violences de dimanche.
Il a indiqu� que deux policiers �taient mort dimanche dans ces violences.
M. Sissi, �lu pr�sident apr�s avoir destitu� M. Morsi, est r�guli�rement accus� d'avoir instaur� un r�gime encore plus r�pressif que celui de M. Moubarak. Il jouit cependant d'une forte popularit� aupr�s d'une grande partie de l'opinion publique.
Depuis l'�viction de M. Morsi, soldats et policiers ont tu� plus de 1.400 manifestants pro-Morsi, dont 700 en quelques heures lorsque les forces de s�curit� ont dispers� des rassemblements islamistes le 14 ao�t 2013 au Caire.
(�AFP / 26 janvier 2015 16h27)
�
