TITRE: Maternité: petites villes et Cour des comptes en désaccord
DATE: 27-01-2015
URL: http://www.lefigaro.fr/flash-eco/2015/01/27/97002-20150127FILWWW00413-maternite-les-petites-villes-en-desaccord-avec-la-cour-des-comptes.php
PRINCIPAL: 0
TEXT:
le 27/01/2015 à 18:40
Publicité
L'Association des petites villes de France (APVF) a critiqué aujourd'hui le rapport de la Cour des comptes recommandant la fermeture des petites maternités n'offrant pas toutes les garanties de sécurité, estimant qu'appliquer ce rapport risquait d'"aggraver les inégalités territoriales en termes d'accès aux soins".
Dans ce document publié vendredi, la Cour des comptes affirme que les petites maternités doivent faire l'objet d'un contrôle accru et "fermer sans délai" dans le cas où elles ne pourraient respecter les normes de sécurité. Les magistrats jugent "inévitable" une "nouvelle phase de réorganisation". Une quinzaine d'établissements sont visés en particulier.
Selon un communiqué, "l'APVF ne remet pas en cause, bien au contraire, la nécessité de renforcer la sécurité de ces structures mais s'oppose à des recommandations qui pourraient aggraver les inégalités territoriales en termes d'accès aux soins".
LIRE AUSSI :
