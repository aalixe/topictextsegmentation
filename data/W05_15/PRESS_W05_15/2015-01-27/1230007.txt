TITRE: CAN - Can 2015: l'Alg�rie �limine le S�n�gal | francetv sport
DATE: 27-01-2015
URL: http://www.francetvsport.fr/football/can/can-2015-l-algerie-elimine-le-senegal-263445
PRINCIPAL: 1230003
TEXT:
Francetv sport Football CAN Can 2015: L'Alg�rie �limine Le S�n�gal
CAN
Can 2015: l'Alg�rie �limine le S�n�gal
Madjid Bouguerra (Alg�rie) � la lutte avec Biram Diouf (S�n�gal) (ISSOUF SANOGO / AFP)
Par Gr�gory Jouin
Publi� le 27/01/2015 | 20:57, mis � jour le 27/01/2015 | 22:15
L'Alg�rie et le Ghana se sont qualifi�s pour les quarts de finale de la CAN en battant respectivement le S�n�gal (2-0) et l'Afrique du Sud (2-1) lors de la 3e et derni�re journ�e du groupe C. Les Alg�riens ont fait la diff�rence gr�ce � deux buts sign�s Mahrez (11e) et Bentaleb (82e) � Malabo. Le Ghana a renvers� la vapeur face aux Bafana Bafana qui menaient 1-0 (Masango 17e) et �taient un moment virtuellement qualifi�s. Mais Boye (73e) et Andre Ayew (83e) ont permis aux Ghan�ens de s'imposer.
L�Alg�rie s�est cr��e la premi�re grosse occasion du match d�entr�e de jeu. Papy Djilobodji assurait mal une passe en retrait et l�interception profitait aux Verts : lanc� dans l�espace, Sofiane Feghouli se pr�sentait seul dans la surface mais il tergiversait trop et permettait � Coundoul de gagner son face � face (3e).
L'Alg�rie prend les devants
Quelques minutes plus tard, les Fennecs trouvaient la faille. Sur un long coup-franc tir� de la droite, Riyad Mahrez, esseul�, contr�lait du pied gauche avant d�encha�ner par un tir crois� du pied droit � ras de terre qui trompait Coundoul, impuissant (1-0, 11e). Le portier s�n�galais sauvait toutefois les siens dix minutes plus tard sur une tentative sign�e Hillel Soudani.
Les Lions de la Teranga r�pliquaient enfin, par Sadio Man� qui d�clenchait un tir puissant du pied droit, trop enlev� (22e). Les duels �taient �pres entre les joueurs des deux �quipes. Sur l�un d�eux, un duel a�rien entre A�ssa Mandi et Cheikh M�Bengu�, le S�n�galais prenait un coup. Saignant beaucoup, il �tait contraint de sortir quelques instants plus tard, remplac� par Pape Souar� (29e).
Juste apr�s la demi-heure, un tir de Djilobodji du gauche passait tout pr�s du poteau gauche alg�rien. A la pause, les deux pays �taient qualifi�s puisque l�Afrique du Sud menait 1-0 contre le Ghana dans l�autre rencontre.
Penalty refus� au S�n�gal
Le match se durcissait encore en seconde p�riode, � l�image du carton jaune inflig� � Saphir Ta�der pour une succession de fautes (51e). A l�heure de jeu, la partie s�emballait carr�ment. Les Lions manquaient l��galisation : sur un centre � ras de terre de Kara M�Bodji, Sadio Man� ne parvenait pas � cadrer. Puis l�arbitre mauritanien refusait un penalty pourtant �vident pour une faute de Carl Medjani sur Mame Biram Diouf dans la surface, le S�n�galais se relevant avec un maillot compl�tement d�chir� (64e).
Mais ce sont les hommes de Christian Gourcuff qui terminaient les plus forts, d�abord sur une action tr�s chaude � l�entame du dernier quart d�heure : sur un centre venu de la gauche et de Mahrez, Bouna Coundoul renvoyait des deux poings dans l�axe. Ta�der reprenait du droit mais M�Bodji repoussait sur sa ligne avec les deux genoux.
Puis en tuant le suspense � la 82e minute : sollicit� c�t� droit, Feghouli s�appuyait dans l�axe sur Nabil Bentaleb. A l�entr�e de la surface, le milieu alg�rien effectuait un encha�nement parfait contr�le rapide et frappe pr�cise qui prenait Coundoul � contre-pied (2-0). La messe �tait dite. L�Alg�rie file en quarts de finale tandis que le S�n�gal laisse sa place au Ghana, finalement vainqueur de l�Afsud (2-1).
R�actions
Christian Gourcuff� (s�lectionneur de l'Alg�rie): "C'�tait un match excessivement physique, avec� beaucoup d'impact, un combat avec des ballons a�riens et des duels. On a fait� preuve de beaucoup de discipline sur le plan tactique. On leur a pos� des� probl�mes par notre technique. On aurait m�me pu doubler la mise. C'�tait une� opposition de style. Nous, on a des petits gabarits tr�s techniques alors que� le S�n�gal mise sur ses gabarits et son physique. Mais on n'�tait pas les plus� mauvais du monde apr�s le match contre le Ghana (d�faite 1-0, ndlr) et on n'est� pas les champions maintenant.
���
Sofiane Feghouli (attaquant de Valence): "Sur la globalit� du match,� l'�quipe (d'Alg�rie) a fait preuve de maturit�. Elle ne s'est pas mise en� danger. On aurait pu marquer plus de buts, on leur a pos� beaucoup de soucis.� On s'est bien adapt� � son jeu direct, bas� sur de longs ballons. Il faut aussi� f�liciter les rempla�ants qui ont apport� du sang neuf."
