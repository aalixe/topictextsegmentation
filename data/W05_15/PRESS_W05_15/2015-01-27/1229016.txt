TITRE: Actualit� > L'ast�ro�de qui nous a fr�l� est double
DATE: 27-01-2015
URL: http://www.futura-sciences.com/magazines/espace/infos/actu/d/asteroides-asteroide-nous-frole-double-56941/
PRINCIPAL: 1229012
TEXT:
image radar
L'ast�ro�de qui nous a fr�l� est double
Surprise : l�ast�ro�de 2004 BL86 qui a crois� notre orbite le 26 janvier � quelque 1,2 million de km de la Terre est double. Les donn�es recueillies avec le radar de Goldstone, en Californie, montrent en effet deux objets de respectivement 325 et 70 m de longueur.
Le 27/01/2015 � 15:38           - Par Xavier Demeersman, Futura-Sciences
9 commentaires R�agir
Le g�ocroiseur 2004 BL86, qui est pass� le 26 janvier � 16 h 19 TU � 1,2 million de km de la Terre, est en r�alit� double. Les images prises avec la grande antenne de Goldstone, en Californie montrent que l�objet principal mesure environ 325 m et que son satellite serait long de quelque 70 m. � Nasa, JPL-Caltech
L'ast�ro�de qui nous a fr�l� est double - 1 Photo
PDF
Le g�ocroiseur 2004 BL86 (d�sign� aussi 357439 car, en 2004, il �tait le 357.439e ast�ro�de d�couvert) a beaucoup fait parler de lui, ce lundi 26 janvier. D�une taille estim�e sup�rieure � 500 m, il est en effet sagement pass� � 16 h 19 TU, soit 17 h 19 en France m�tropolitaine, � 1,2 million de km de la Terre (soit 3,1 fois la distance moyenne Terre-Lune), sans pour autant nous menacer.
C�est la premi�re fois, au cours de l��re moderne, qu�un corps de cette dimension passe aussi pr�s de nous. Dans son cas, cela ne se reproduira pas avant deux si�cles environ, assurent les chercheurs de la Nasa qui, � la faveur de cette proximit�, en ont profit� pour affiner les calculs de sa trajectoire. Toutefois, notre prochain rendez-vous avec un ast�ro�de de cette classe est pr�vu pour le 7 ao�t 2027. Ce jour-l�, 1999 AN10 (137108), dont la taille est comprise entre 800 et 1.800 m, passera � 388.960 km de notre Plan�te. En avril 2029, ce sera le tour d�Apophis (99942) de passer � seulement 30.000 km environ au-dessus de nos t�tes, sans danger.
Le g�ocroiseur 2004 BL86 (357439) est pass� pr�s de la Terre le 26 janvier. Cette animation a �t� cr��e � partir des vingt images radar r�alis�es avec la grande antenne du Deep Space Network de Goldstone en Californie. La r�solution est de 4 m�tres par pixel. � Nasa, JPL-Caltech
Un ast�ro�de binaire
� l�occasion de cette visite rapide de 2004 BL86, de nombreux astronomes amateurs et autres curieux �quip�s de jumelles, lunette ou t�lescope ont tent� l�exp�rience de l�observer et de le filmer. Il apparaissait tel un petit point lumineux se d�pla�ant devant le fond du ciel �toil�. Bien entendu, les professionnels n�ont pas manqu� l��v�nement et l�ont salu� en cueillant au passage un maximum de donn�es physiques � son sujet.
La vingtaine d�images radar obtenues avec la grande antenne Deep Space Network de Goldstone (70 m de diam�tre), en Californie, ont, � cet �gard, montr� qu�il s�agit d�un ast�ro�de binaire, � l�instar d�environ 16 % des g�ocroiseurs connus . Le corps principal mesure approximativement 325 m et le secondaire, son satellite, environ 70 m, comme l'indique un communiqu� du Jet Propulsion Laboratory (JPL). Les chercheurs continuent de traiter les r�sultats sur leurs rotations, caract�ristiques de surface, alb�do , densit�, etc.
L�agence spatiale am�ricaine (Nasa) ne cache pas son grand int�r�t pour les ast�ro�des . D�une part, sur le plan astrophysique , car ils sont des t�moins des origines de notre Syst�me solaire et auraient pu, par ailleurs, abreuver la Terre primitive en eau voire l�ensemencer de mol�cules pr�biotiques , et aussi, d�autre part, parce qu�ils pourraient servir, d�s 2020, de base d�entrainement pour les futures missions habit�es vers Mars. Osiris-Rex , dont le lancement est pr�vu en 2016, s�int�ressera quant � elle de tr�s pr�s � l�ast�ro�de potentiellement dangereux de 500 m de long, Bennu (101955). La sonde spatiale devrait nous rapporter en 2023 des �chantillons rocheux pr�lev�s � sa surface.
