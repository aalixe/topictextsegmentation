TITRE: Sean Penn recevra un César d’honneur, Culture
DATE: 27-01-2015
URL: http://www.lesechos.fr/week-end/culture/0204111761482-sean-penn-recevra-un-cesar-dhonneur-1087343.php
PRINCIPAL: 1228492
TEXT:
Le 27/01 � 11:42, mis � jour � 11:57
AFP PHOTO / ANNE-CHRISTINE POUJOULAT
1 / 1
L�acteur et r�alisateur am�ricain sera honor� lors de la c�r�monie au Th��tre du Ch�telet.
L�Acad�mie des Arts et Techniques du Cin�ma d�cernera le 20 f�vrier prochain un C�sar d�honneur au com�dien Sean Penn. A l�occasion de la 40�me �dition de la remise de prix, l�acteur et r�alisateur am�ricain sera distingu� pour ses choix de carri�re, sa sensibilit� et son engagement. Grand habitu� des r�compenses, Sean Penn a re�u d�s 1997, alors �g� de 37 ans, le prix d�interpr�tation masculine au Festival de Cannes pour son r�le dans le film de Nick Cassavetes � She�s So Lovely �, avant de se voir d�cerner, en 2004, son premier Oscar pour � Mystic River � de Clint Eastwood. Il obtient �galement cinq ans plus tard l�Oscar du Meilleur Acteur pour sa performance dans � Harvey Milk � de Gus Van Sant.
Prim� dans les trois plus grands festivals du monde, Sean Penn a d�croch� ces derni�res ann�es l�Ours d�Argent � Berlin du Meilleur Acteur pour � La Derni�re Marche � de Tim Robbins et deux coupes Volpi � Venise pour � Hollywood Sunrise � d�Anthony Drazan et � 21 Grammes � d�Alejandro Gonz�les I�arritu. Egalement cin�aste, il a r�alis� en 1991 son premier long m�trage � The Indian Runner �, suivi de quatre autres films dont le tr�s m�diatis� � Into The Wild �. Le C�sar d�honneur avait �t� attribu� l�ann�e derni�re � la com�dienne Scarlett Johansson, qui recevait son prix des mains de Quentin Tarantino pour l�ensemble de sa jeune carri�re.
C�cilia Delporte
