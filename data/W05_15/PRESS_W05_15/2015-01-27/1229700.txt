TITRE: Poutine et la guerre des mots | La-Croix.com - Editos
DATE: 27-01-2015
URL: http://www.la-croix.com/Editos/Poutine-et-la-guerre-des-mots-2015-01-27-1273515
PRINCIPAL: 1229696
TEXT:
Poutine et la guerre des mots
Il accuse l�Occident d�hyst�rie antirusse.
27/1/15 - 18 H 00
En Ukraine, le processus de paix toujours dans l�impasse
Vladimir Poutine s�est lanc� dans une ��guerre des mots�� contre les Occidentaux, l�Otan et les �tats-Unis r�unis. Tout lui est bon pour d�noncer l�influence am�ricaine. Ainsi, les notations de l�agence Standard & Poor�s, classant la Russie dans la cat�gorie des dettes souveraines sp�culatives � c�est-�-dire ��pourries�� �, auraient �t� prises ��sur ordre direct de Washington��. L�arm�e ukrainienne ne serait qu�une ��L�gion �trang�re de l�Otan��. Jusqu�� la comm�moration de la lib�ration d�Auschwitz qui lui donne l�occasion d�une nouvelle diatribe contre un Occident qui, selon lui, falsifie l�histoire, minimise l�action de l�Arm�e rouge, voulant ainsi dissimuler ��sa propre honte, la honte de sa l�chet�, hypocrisie et trahison�� et ��cacher sa complicit� tacite, passive ou active avec les nazis��. Poutine, dans son souci de v�rit� historique, aurait pu se souvenir d�un certain pacte germano-sovi�tique de non-agression, sign� en ao�t�1939.
Le pr�sident russe fait un parall�le os� avec le sort des populations civiles du Donbass, o� l�arm�e ukrainienne se bat contre des rebelles soutenus par des soldats russes. Vladimir Poutine est-il v�ritablement convaincu de ce qu�il �nonce, ou mobilise-t-il ce vocabulaire patriotique pour d�tourner la population des difficult�s �conomiques dans lesquelles s�enfonce son pays, en raison de la baisse du prix du p�trole et des sanctions ? Peu importe : la tension s�accro�t et toute perspective de cessez-le-feu s��loigne, malgr� les menaces de nouvelles mesures de r�torsion, encore en discussion.
Quand chaque camp se renvoie la responsabilit� des crimes de guerre commis, quand s�affrontent des versions diam�tralement oppos�es (qui est l�agresseur, qui sont les victimes ?), le chemin diplomatique para�t ardu. Est-ce une s�quence paroxystique, o� le ma�tre du Kremlin s�emploie � effrayer un Occident occup� sur d�autres fronts, et peu tent� de se lancer dans des op�rations militaires ? Vladimir Poutine, apr�s avoir suffisamment bomb� le torse pour se sentir en position de force, pourrait entrer en n�gociations. Esp�rons que telle est sa strat�gie.
Dominique Quinio
