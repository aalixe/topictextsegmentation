TITRE: Emma Watson annonce qu’elle va incarner Belle au cinéma - Elle
DATE: 27-01-2015
URL: http://www.elle.fr/Loisirs/Cinema/News/Emma-Watson-annonce-qu-elle-va-incarner-Belle-au-cinema-2880978
PRINCIPAL: 1228047
TEXT:
Emma Watson annonce qu’elle va incarner Belle au cinéma
Créé le 27/01/2015 à 10h26
© Visual
Envoyer à un ami
«�Je suis enfin autorisée à vous le dire…�» Emma Watson a annoncé elle-même la nouvelle, lundi, sur son compte Facebook�: elle s’apprête à jouer le rôle principal dans «�La Belle et la Bête�». «�Je vais incarner Belle dans la nouvelle adaptation faite par Disney�! Elle a été tellement présente dans mon enfance, ça paraît presque surréaliste de me dire que je vais danser et chanter sur ses chansons�», a expliqué Emma Watson sur Facebook. Le film sera réalisé par Bill Condon, le metteur en scène des deux derniers épisodes de «�Twilight�». Lundi, les studios Disney ont également annoncé que la production du long-métrage débuterait cette année.
«�Il est temps de commencer les leçons de chant�»
«�La petite fille de 6 ans qui est en moi saute au plafond, le cœur battant�», a confié Emma Watson dans son message posté sur Facebook. Enthousiaste à l’idée d’incarner Belle, l’actrice de 24 ans a déjà envie de se mettre au travail. «�Il est temps de commencer les leçons de chants. J’ai tellement hâte que vous voyiez ça�», a-t-elle ainsi écrit à ses fans.
�
Cela sera la deuxième fois, après le dessin animé sorti en 1991, que «�La Belle et la Bête�» sera adapté par Disney. Un film avec Léa Seydoux et Vincent Cassel a également été réalisé par Christophe Gans en 2014. La plus célèbre version du conte de Gabrielle-Suzanne de Villeneuve est signée Jean Cocteau, elle est sortie en 1946.
