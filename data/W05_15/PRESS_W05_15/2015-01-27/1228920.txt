TITRE: VIDEO. Libye: neuf morts, dont un Fran�ais dans l'attaque d'un h�tel de Tripoli - L'Express
DATE: 27-01-2015
URL: http://www.lexpress.fr/actualite/monde/afrique/libye-neuf-morts-dans-l-attaque-d-un-hotel-de-tripoli-revendiquee-par-daesh_1645186.html
PRINCIPAL: 1228916
TEXT:
VIDEO. Libye: neuf morts, dont un Fran�ais dans l'attaque d'un h�tel de Tripoli
Par L'Express.fr avec AFP,           publi� le
27/01/2015 � 15:30
, mis � jour �
21:04
La branche libyenne du groupe djihadiste Etat islamique a revendiqu� l'attaque d'un h�tel de la capitale libyenne. L'attaque a fait neuf morts dont un Am�ricain, un Fran�ais, 2 Philippins et un Sud-Cor�en.
Zoom moins
?
� � � � �
L'h�tel Corinthia de Tripoli a �t� attaqu� par des hommes arm�s qui se sont fait exploser. Un Fran�ais figure parmi les victimes.
Reuters/Ismail Zitouny
Des hommes arm�s ont pris d'assaut un h�tel de la capitale libyenne , ce mardi, apr�s avoir fait exploser une voiture pi�g�e devant l'�tablissement. Neuf personnes ont �t� tu�es dans l'attaque, revendiqu�e par le groupe djihadiste Etat islamique. Les assaillants se sont fait exploser dans l'�tablissement. �
Au moins cinq �trangers, dont un Am�ricain, un Fran�ais, 2 Philippins et un Sud-Cor�en ont �t� tu�s par balles, selon la m�me source. Outre ces cinq �trangers, une "personne prise en otage" par les assaillants et dont la nationalit� n'�tait pas connue a p�ri lorsque les hommes arm�s se sont fait exploser, a pr�cis� le porte-parole. �
En milieu de matin�e, selon le porte-parole des op�rations de s�curit� � Tripoli, Issam al-Naass, quatre hommes ont fait exploser une voiture pi�g�e devant l'h�tel Corinthia puis ont p�n�tr� dans les lieux, tuant par balle deux membres des forces de s�curit�. Un troisi�me membre de la s�curit� est mort dans l'explosion de la voiture pi�g�e.�
�
Cern�s, ils se sont fait exploser
"Pourchass�s et encercl�s au 24e �tage de l'h�tel par les forces de s�curit�, les assaillants ont d�clench� les ceintures explosives qu'ils portaient", a pr�cis� Issam al-Naass. �
L'h�tel Corinthia connu pour accueillir des diplomates et des responsables, avait un temps abrit� les si�ges de plusieurs missions diplomatiques et celui du gouvernement libyen. Le 24e �tage de l'h�tel est normalement r�serv� � la mission diplomatique du Qatar mais aucun responsable ou diplomate ne s'y trouvait au moment de l'attaque, selon une source de s�curit�.�
�
Le chef du gouvernement auto-proclam�, Omar al-Hassi, se trouvait � l'int�rieur de l'h�tel
La branche libyenne du groupe djihadiste Etat islamique a, selon le centre am�ricain de surveillance des sites islamistes SITE, revendiqu� l'assaut.�
Le chef du gouvernement auto-proclam� en Libye, Omar al-Hassi, se trouvait � l'int�rieur de l'h�tel au moment de l'assaut mais il a �t� �vacu� sains et sauf, selon le porte-parole.�
La capitale libyenne est contr�l�e par Fajr Libya, une puissante coalition de milices, notamment islamistes, qui a install� un gouvernement parall�le � Tripoli apr�s en avoir chass� le gouvernement reconnu par la communaut� internationale.�
�
