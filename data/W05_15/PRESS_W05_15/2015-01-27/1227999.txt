TITRE: France-Le proc�s Bettencourt reprend mais un report reste possible- 27 janvier 2015 - Challenges.fr
DATE: 27-01-2015
URL: http://www.challenges.fr/economie/20150127.REU7677/france-le-proces-bettencourt-reprend-mais-un-report-reste-possible.html
PRINCIPAL: 1227998
TEXT:
Challenges �>� Economie �>�France-Le proc�s Bettencourt reprend mais un report reste possible
France-Le proc�s Bettencourt reprend mais un report reste possible
Publi� le� 27-01-2015 � 10h35
* Rejet d'une Question prioritaire de constitutionnalit�
* La mise en examen de Claire Thibout pourrait freiner le proc�s
BORDEAUX, 27 janvier ( Reuters ) - Le proc�s des dix personnes accus�es d'abus de faiblesse au d�triment de la milliardaire de L'Or�al Liliane Bettencourt a repris mardi � Bordeaux apr�s le rejet d'une requ�te d'une partie de la d�fense mais un report n'est pas exclu.
Le tribunal correctionnel de Bordeaux a rejet� mardi la question prioritaire de constitutionnalit� (QPC) soulev�e lundi par les avocats de deux pr�venus, Patrice de Maistre, l'ancien gestionnaire de fortune de Liliane Bettencourt, et Fran�ois-Marie Banier, le photographe et ami de la milliardaire.
Ils estimaient qu'on ne peut poursuivre une seule et m�me personne � la fois pour des abus de faiblesse et pour le blanchiment du produit de ces abus. Le parquet et les parties civiles s'�taient oppos�s � cette interpr�tation.
Le minist�re public n'a par ailleurs pas pu donner d'information sur l'�tat de sant� d' Alain Thurin , un ancien infirmier de Liliane Bettencourt qui aurait profit� de son �tat de faiblesse pour b�n�ficier de lib�ralit�s.
L'homme a tent� dimanche de mettre fin � ses jours dans un bois proche de son domicile dans l' Essonne .
Lundi, deux acteurs importants des d�bats manquaient aussi � l'appel pour raisons de sant�. Il s'agit de Carlos Cassina-Vejarano, l'ancien gestionnaire de l'�le d'Arros aux Seychelles , propri�t� de Liliane Bettencourt , et de Claire Thibout, l'ancienne comptable de la milliardaire.
Cette derni�re affirme avoir pr�par� des enveloppes d'argent au profit de l'ancien ministre Eric Woerth pour financer la campagne de Nicolas Sarkozy en 2007. L'ancien pr�sident, qui avait �t� mis en examen, a b�n�fici� d'un non-lieu.
VERS UNE DEMANDE DE REPORT
Claire Thibout ayant �t� mise en examen le 27 novembre � Paris par le juge Roger Le Loire pour faux t�moignage apr�s une plainte de Fran�ois-Marie Banier et de Patrice de Maistre, les deux avocats devraient poursuivre mardi leur combat proc�dural.
Ils devraient ainsi d�poser une demande de report du proc�s en demandant le renvoi de la proc�dure au parquet pour inclure les �l�ments � d�charge mis en �vidence par cette mise en examen, et un sursis � statuer le temps que l'instruction du juge Le Loire qui concerne encore cinq autres t�moins de l'affaire soit men�e � son terme.
Si le tribunal ne retient pas les arguments des deux avocats, le proc�s pourrait entrer dans la phase d'examen des personnalit�s puis des faits reproch�s des mercredi.
Dans ce dossier, sur douze personnes mises en examen, dix ont �t� renvoy�es devant le tribunal dont Fran�ois-Marie Banier , accus� d'avoir obtenu de la milliardaire plusieurs centaines de millions d'euros et son compagnon, Martin Le Barrois d'Orgeval.
Patrice de Maistre, doit r�pondre de plusieurs accusations, notamment celle d'avoir b�n�fici� de lib�ralit�s pour un montant de plus de 8 millions d'euros et d'avoir retir� de fortes sommes d'argent des comptes en Suisse de la milliardaire dont une partie aurait �t� revers�e � Eric Woerth .
L'homme d'affaires St�phane Courbit, qui avait obtenu de la milliardaire un investissement 143,7 millions d'euros dans son groupe Lov Group Industrie de paris en ligne, a trouv� un accord de restitution de cette somme avec la famille Bettencourt � la veille du proc�s.
L'avocat Pascal Wilhelm, successeur de Patrice de Maistre en tant que gestionnaire de fortune, ainsi que deux notaires, Patrice Bonduelle et Jean-Michel Normand, comparaissent �galement.   (Claude Canellas, �dit� par Yves Clarisse)
Partager
