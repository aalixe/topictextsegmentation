TITRE: Les logements neufs au plus bas depuis 1997
DATE: 27-01-2015
URL: http://www.lefigaro.fr/flash-eco/2015/01/27/97002-20150127FILWWW00081-les-logements-neufs-au-plus-bas-depuis-1997.php
PRINCIPAL: 1227630
TEXT:
le 27/01/2015 à 08:47
Publicité
Un total de 297.532 logements ont été mis en chantier l'an passé en France, soit un plus bas depuis 1997, selon les chiffres publiés aujourd'hui par le ministère de l'Ecologie et du Développement durable. Les permis de construite délivrés ont atteint 381.075, soit le plus faible niveau depuis 1999.
Par rapport à 2013, les mises en chantier accusent un recul de 10,3% et les permis de construire de 12,0%. Dans la construction neuve, qui représente près de 88% de l'offre de nouveaux logements, les mises en chantier baissent de 9,5% et les permis de construire de 11,1%.
Sur le seul quatrième trimestre, les mises en chantiers ont reculé de 1,7% en données corrigées des variations saisonnières par rapport au troisième trimestre et les permis de construire accordés de 0,8%.
