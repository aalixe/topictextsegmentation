TITRE: Don de sang : l�EFS en gr�ve pour d�fendre le monopole de la collecte - Pourquoi Docteur ?
DATE: 27-01-2015
URL: http://www.pourquoidocteur.fr/Don-de-sang---l-EFS-perd-son-monopole--9624.html
PRINCIPAL: 0
TEXT:
Don de sang : l�EFS en gr�ve pour d�fendre le monopole de la collecte
Publi� le 28 Janvier 2015
Les salari�s de l�Etablissement Fran�ais du Sang sont en gr�ve pour d�noncer l�arr�t obligatoire de la fabrication de plasma th�rapeutique. Ils craignent aussi de perdre le monopole de la collecte.
GELEBART/20 MINUTES/SIPA
� Don du sang : les stocks � un niveau critique �
Le sang attire bien des convoitises. En gr�ve, les salari�s de l�Etablissement Fran�ais du Sang (EFS) en font l�am�re exp�rience. Dans un march� mondial estim� � douze milliards d�euros, le statut du seul et unique fournisseur de sang en France semble quelque peu compromis.
Le plasma th�rapeutique, fabriqu� par des groupes priv�s
De fait, le mod�le h�g�monique de l�EFS a d�j� perdu en vitesse - raison pour laquelle les salari�s manifestent ce mardi. En juillet 2014, le Conseil d�Etat a port� un coup de massue � l��tablissement en mettant fin � son droit exclusif de fabriquer et de commercialiser du plasma th�rapeutique. D�s le 1er f�vrier, l�EFS devra donc abandonner cette partie de son activit�, pour la c�der � des entreprises priv�es, et notamment au groupe suisse Octapharma.
Le laboratoire helv�tique est � l�origine de la proc�dure qui a mis fin au monopole. En effet, l�EFS �tait, jusqu�ici, la seule structure autoris�e � produire du plasma SD en France. Le plasma correspond � la partie liquide du sang, qui contient des prot�ines d'un int�r�t th�rapeutique majeur. Pour inactiver les virus potentiellement pr�sents, l��tablissement utilise un solvant-d�tergent (SD). Selon le Conseil d�Etat, ce proc�d� constitue une technique industrielle qui fait du plasma un m�dicament � part enti�re.
R�sultat�: l�EFS, qui n�a pas le statut d��tablissement pharmaceutique, perd le droit de produire le plasma SD. Pour la structure publique, cela signifie la destruction de plusieurs dizaines d�emplois.
Un rapport qui suscite des craintes
Un autre point inqui�te davantage les salari�s de l�EFS. Un rapport de l�IGAS sur la fili�re plasma, command� par le minist�re de la Sant�, �met des recommandations qui n�augurent rien de bon pour l��tablissement. Selon les syndicats CGT, CFDT, CFE-CGC et FO, ses auteurs proposent en effet de casser le monopole de la collecte et de permettre � une autre structure publique de pr�lever le plasma des donneurs.
Ainsi, le Laboratoire fran�ais de fractionnement et des biotechnologies (LFB), qui commercialise des m�dicaments � base de sang, pourrait lui aussi obtenir le droit de collecte. Selon l�IGAS, une telle ouverture du monopole devrait lui permettre de gonfler ses stocks, puisque le LFB ne peut se fournir en plasma qu�aupr�s de l�ESF.
��Qu�en est-il des principes s�curitaires, instaur�s � la suite de l�affaire du sang contamin�, de s�parer le collecteur du fractionneur ?��, s�interrogent les syndicats dans un communiqu�. De fait, la s�paration des deux organismes a �t� d�cid�e en 1993 pour �viter tout conflit d�int�r�t.
��Nous ne comprenons pas de telles d�cisions, si ce n�est une volont� de limiter consid�rablement le monopole et le r�le de l�EFS, en favorisant les lobbies pharmaceutiques��, d�noncent encore les syndicats, qui craignent, � terme, de voir dispara�tre le mod�le fran�ais du don de sang, fond� sur la gratuit�.
Dans la m�me rubrique
