TITRE: Le S�nat vote le texte sur les comp�tences des collectivit�s - Capital.fr
DATE: 27-01-2015
URL: http://www.capital.fr/a-la-une/actualites/le-senat-vote-le-texte-sur-les-competences-des-collectivites-1008466
PRINCIPAL: 1229517
TEXT:
Le S�nat vote le texte sur les comp�tences des collectivit�s
Source : Reuters 27/01/15 � 17:45
Mis � jour le 27/01/15 � 21:48
Temps de lecture : 1 minute
Partager
R�agir
0 R�action
Le S�nat fran�ais a adopt� en premi�re lecture le projet de loi relatif � la nouvelle organisation territoriale de la R�publique (dite "loi NOTRe") qui red�finit les comp�tences des nouvelles r�gions, ainsi que des d�partements et communes. /Photo d'archives/REUTERS/Vincent Kessler
Le S�nat fran�ais a adopt� mardi en premi�re lecture le projet de loi relatif � la nouvelle organisation territoriale de la R�publique (dite "loi NOTRe") qui red�finit les comp�tences des nouvelles r�gions, ainsi que des d�partements et communes.
Les groupes UMP et UDI (centriste) ont vot� pour ainsi que la majorit� du groupe RDSE (centre droit et centre gauche). Les groupes PS, Front de gauche et des �cologistes se sont abstenus.
Le texte a �t� adopt� par 192 voix contre 11.
Ce texte, qui compl�te la loi ramenant de 22 � 13 le nombre des r�gions m�tropolitaines et celle relative aux m�tropoles, a �t� modifi� par le S�nat o� la droite est redevenue majoritaire en septembre dernier.
Il supprime la comp�tence g�n�rale pour les d�partements et les r�gions qui leur donne une capacit� d'intervention dans tous les domaines, sans qu'il soit n�cessaire de les �num�rer.
La droite s�natoriale a �galement refus� de relever le seuil d�mographique des intercommunalit�s de 5.000 � 20.000 habitants.
Ce projet de loi, que le S�nat a examin� pendant plusieurs semaines, sera d�battu en s�ance publique par les d�put�s � partir du mardi 17 f�vrier, soit avant les �lections d�partementales pr�vues les 22 et 29 mars.
(Emile Picy, �dit� par Yves Clarisse)
� 2015 Reuters - Tous droits de reproduction r�serv�s par Reuters.
Partager
