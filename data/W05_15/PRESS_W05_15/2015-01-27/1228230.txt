TITRE: Quand SFR impose une option payante � ses clients
DATE: 27-01-2015
URL: http://www.latribune.fr/technos-medias/telecoms/20150126trib19de5bc57/quand-sfr-impose-une-option-obligatoire-a-ses-clients.html
PRINCIPAL: 1228227
TEXT:
OR 1 184,8$ -1,04                 %
Quand SFR impose une option payante � ses clients
� SFR ne propose pas d'options � valeur aujourd'hui � avait d�plor� Patrick Drahi en pr�sentant son projet de rachat de SFR. (Cr�dits : reuters.com)
Delphine Cuny �|�
26/01/2015, 17:55
�-� 675 �mots
L�op�rateur vient de changer ses conditions de vente, obligeant ses abonn�s ADSL n�ayant pas souscrit d�option TV � payer 1 euro de plus. Ou � r�silier sans frais. Une d�cision unilat�rale qui fait grincer des dents.
sur le m�me sujet
Abonnez-vous � partir de 1�
Patrick Drahi avait pr�venu : en rachetant SFR, il comptait bien faire remonter les marges de SFR. Le premier actionnaire de Numericable avait d�plor� que l'op�rateur � ne propose pas d'option � valeur � , � la diff�rence du � c�blo. � Mise en pratique d�s janvier, deux mois apr�s son acquisition effective : SFR vient de d�cider d'appliquer une option TV payante � obligatoire � � tous ses abonn�s, comme l'a remarqu� le site sp�cialis� Nextinpact . En effet, SFR a ajout� dans sa nouvelle brochure de tarifs valables au 20 janvier 2015, pour tous ses abonn�s ADSL (en zone d�group�e ou non), une rubrique � TV sur smartphone, tablette et PC : +1 � / mois, obligatoire pour les clients non d�tenteurs d'une option TV �, en plus des 29,99 euros par mois ( voir page 4 de la brochure ). Une hausse d�cr�t�e unilat�ralement qui a provoqu� des r�actions acerbes et outr�es de certains abonn�s sur les r�seaux sociaux.
� Cette nouveaut� fait suite � un changement d'offres. D�sormais, toutes les offres int�grent un service de t�l�vision � indique SFR, qui assure avoir respect� � le d�lai de pr�venance d'un mois pour en informer les clients. �
Une option TV � anti-taxe �
Interrog�, SFR ne communique pas la part des abonn�s concern�s mais indique � La Tribune que seule � une minorit� � de ses clients sont touch�s, sur ses 5 millions d'abonn�s ADSL, qui ont pour la plupart une offre � triple-play � incluant d�j� une option TV. Pour m�moire, SFR avait instaur� en septembre 2012 une � option TV � payante (3 euros), copiant Free qui avait initi� le mouvement en lan�ant sa Freebox Revolution fin 2011. Une astuce destin�e officiellement � ne pas faire payer la t�l�vision aux abonn�s n'ayant pas le d�bit suffisant pour l'IP TV, permettant surtout de contourner la taxe sur les services de t�l�vision appel�e � TST �, qui alimente le Centre national du cin�ma (CNC). Toutefois, une r�forme de cette taxe, principale ressource du CNC, avait mis fin � ce contournement au 1er janvier 2014.
Droit � r�siliation sans frais
Il pourrait tout de m�me s'agir de pr�s d'un million de clients concern�s, avance un analyste. Le num�ro deux fran�ais des t�l�coms pourrait alors augmenter m�caniquement son � ARPU �, son revenu moyen par abonn�, de 1 euro par mois, donc son chiffre d'affaires de l'ordre d'une douzaine de millions d'euros sur douze mois� - un coup de pouce modeste par rapport aux plus de 11,5 milliards d'euros annuels de Numericable-SFR, mais toujours appr�ciable pour l'"Ebitda", l'exc�dent brut d'exploitation.
A moins que les abonn�s, m�contents, d�cident de quitter l'op�rateur, puisque ce changement ouvre le droit � la r�siliation sans frais - sans les 49 euros de fermeture de ligne ADSL indiqu�s dans le contrat - tel que l e pr�voit le Code de la Consommation . Ils ont quatre mois pour le faire. Comme lorsque SFR et Orange avaient d�cid� de r�percuter la hausse de la TVA sur leurs forfaits mobiles en janvier 2011, avant de renoncer, face au toll� suscit� chez leurs abonn�s et � Bouygues Telecom faisant cavalier seul et gardant ses tarifs inchang�s.... Dans le fixe, les abonnements sont sans engagement mais le taux de r�siliation (dit "churn" dans le jargon) est moins �lev�.
Privil�gier les clients plus rentables
D'ailleurs, les supporters de Free se frottaient les mains sur la Toile, imaginant que les r�siliations pourraient profiter au fournisseur d'acc�s contr�l� par Xavier Niel. Bouygues Telecom, qui casse les prix dans le fixe depuis mars dernier, pourrait �galement r�cup�rer ces clients d��us, que Numericable-SFR pr�f�re peut-�tre voir partir au profit d'abonn�s plus rentables. En effet, le groupe pr�sid� par Patrick Drahi met d�sormais en avant dans le fixe les seules offres tr�s haut d�bit de Numericable et sa box, estampill�e du logo SFR, qui rapportent en moyenne 42 euros (hors promotion), contre 32 euros chez SFR.
L'auteur
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Votre email ne sera pas affich� publiquement
Tous les champs sont obligatoires
Commentaires
Sa Facture Racket a �crit le 28/01/2015 � 15:18 :
SFR = Sa Facture Racket, les financiers de num�ricables ont pris l'ascendant sur l'op�rateur t�l�com, c'est comme free, juste un financier
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
gpro a �crit le 27/01/2015 � 21:03 :
contrat sign� le 15 d�c 14. "d�couverte" au 15/01/15....
pas que ca � faire � me bagarrer sur des lignes client.
ni � �crire au service clients 94098 CRETEIL
puis, "si vous n' �tes pas satisfait de la r�ponse du service client" au service consommateurs (SFR toujours) 62978 ARRAS cedex 9 (page 2 de ma facture t�l�charg�e)
je vais sans doute aller au bout de cette ann�e d' engagemen
t... sans faire de publicit� pour autan. "formule carr�e"... ironique...
Signaler un contenu abusif
@ gpro �a r�pondu le 27/01/2015                                                 � 21:16:
Vous avez envoyez une autorisation de pr�l�vement a votre banque faite apposition et faites march� la concurrence. Si SFR n'a jamais cherch� � garder ces clients box et portable, quand ils recoivent votre demande de portabilit�, l� ils font plein de cadeau... Pour ma part je les ais quitt� il y a 4 ans suite a une panne jamais r�par� de ma connexion box. Ils en avaient rien a faire et tous les appels �taient payant et long; puis en ayant eut raz la casquette j'ai �t� chez un de leur nombreux concurrents et bizarrement quelques jours apr�s, c'est eux qui m'appelait, me proposait le remboursement et une r�duction de ma facture... En gros vous restez vous payez, vous les quittez ils s'inqui�tent.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Arf a �crit le 27/01/2015 � 18:16 :
apr�s 15 ans de fid�lit� chez SFR (d'abord 9 telecom) dans l'ADSL, j'ai r�sili� il y a un mois pour passer chez Orange Fibre. Le tarif chez SFR n'�tait pas tr�s comp�titif mais ce sont surtout les nombreuses coupures qui m'ont fait suspendre mon contrat chez eux.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Hel�ne a �crit le 27/01/2015 � 11:14 :
Service SFR digne de la RDA : Impossible de renouveler une option par internet, "ces options sont momentan�ment hors de service" et 3 h environ au t�l�phone pour obtenir quelqu'un... Le nouveau gadget de SFR : "Presser la touche 3" lorsque vous voulez parler � un conseiller, puis lorsque vous vous ex�cutez, "nous n'avons pas compris votre choix, nous vous demandons de s�lectionner une touche" puis au 2�me essai, "vous n'avez pas donn� votre choix, au revoir"... Et voil� comment SFR vous plante ! Honteux ! faute de conseillers, SFR a imagin� un syst�me diabolique....
Chich �a r�pondu le 27/01/2015                                                 � 12:12:
Votre comparaison avec la RDA est tr�s limite....
moimeme �a r�pondu le 27/01/2015                                                 � 17:31:
C'est vrai, c'est insultant pour la RDA...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
seb a �crit le 27/01/2015 � 10:41 :
je lis vos coms et je vais prendre le contre pied de tout ca moi qui suit a la fois chez sfr pour le gsm et numericable pour l internet... tout d abord je voudrais dire qu aussi bien sfr que numericable ont un service impeccable ( je suis sur marseille intra muros ) je capte partout avec sfr et je n 'ai jamais de coupure avec numericable...de plus numericable m a fait une ristourne de 10 euros/mois sur la fibre parceque je voulais changer pour un operateur moins cher (jusqua present aucun operateur m avait proposer de me faire une ristourne sans meme en avoir fait la demande...) apres vous dites c est 1 euros de plus c est de la vente forc�e oui et non puisque vs avez la possiblit� de resilier sans frais... de plus je voudrais rajouter qu aussi sfr que numericable de mon jamais factur� plus que le prix de mon forfait alors qu avec bouygues mon ancien operateur j avais tous les mois des hors forfait de folie pour de soit disant appels a l etranger que je n avais jamais pass�s alors a bon entendeur.......
Signaler un contenu abusif
aq �a r�pondu le 27/01/2015                                                 � 10:50:
@ seb: service apr�s-vente de SFR nul: il faut se battre pour avoir une nouvelle box pour remplacer celle en panne, accueil dans les boutiques d�plorable digne de l'ex URSS, pas de r�ponse aux mail, ni aux questions pos�es, ... Quant � la qualit� du service internet c'est � peu pr�s OK.
Signaler un contenu abusif
Chich �a r�pondu le 27/01/2015                                                 � 12:14:
@aq : vous �tes la m�me personne que H�l�ne au dessus qui compare le SAV de SFR avec la RDA ? Non parce que l� il faut arreter de sortir des conneries pareilles !!
Signaler un contenu abusif
CRC32 �a r�pondu le 27/01/2015                                                 � 17:04:
A Marseille il est de notori�t� publique que le client est roi, d'ailleurs il r�alise lui m�me son branchement au c�ble... (v�ridique)
Signaler un contenu abusif
@ Chich �a r�pondu le 27/01/2015                                                 � 21:19:
J'ai eu affaire a leur service apr�s vente. Pour r�soudre le probl�me j'ai �t� oblig� de changer de fournisseurs... Quand a comparer leur service � la RDA c'est absurde quand on a leur SAV en ligne on a aucun doute de leur pays d'origine et ils n'auraient jamais pus atteindre la RDA...
Signaler un contenu abusif
ah bon ? �a r�pondu le 28/01/2015                                                 � 15:16:
les r�seaux cabl�s mal entretenus, d�faillants, et factur�s aux syndics, le service FTTLA qui est un monopole il�gal avec 6 millions de prises � 100M sur lesquelles le client n'a pas acc�s aux autres FAI, le fait que SFR arr�te la couverture FTTH qui ouvrirait le r�seau, donc il coucoone son monopole, tout c'est n'est pas clean
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Fran�ois a �crit le 27/01/2015 � 10:28 :
Bon, cela va �tre l'occasion de partir Bouyges Telecom, qui �tait d�j� beaucoup moins cher, m�me sans tenir compte de la vente forc�e de SFR.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Toto a �crit le 27/01/2015 � 9:56 :
N'oubliez pas �galement que c'est 1� de plus par mois.
Mais que ceux qui n'avaient pas cette option TV n'�tait pas oblig� (sauf s'il avait une TV) de payer la taxe audiovisuelle.
Par cette option impos�e, ils seront l�galement oblig� de la payer.
Admettons que 10% de ces 1 millions d'abonn�s n'avaient pas pris cette option pour �viter l�galement la redevance �a fera � ces 10% de personnes 12 + 131 soit 143 � par an en plus a payer.
Et pour l'Etat �a ferait 100 000*131=13 100 000� dans les caisses tous les ans ...
Signaler un contenu abusif
basco �a r�pondu le 27/01/2015                                                 � 12:42:
La taxe audiovisuelle doit �tre pay�e d�s lors que vous d�tenez un t�l�viseur � votre domicile m�me si si vous avez l'option t�l� dans votre abonnement.
Signaler un contenu abusif
@ basco �a r�pondu le 20/02/2015                                                 � 20:39:
Dans ce cas, que l'�tat d�duise ces 1 euros/mois a ceux qui payent d�j� la redevance TV et aussi � ceux qui en sont exon�r�e!
Kcarn �a r�pondu le 24/02/2015                                                 � 23:40:
Je paye d�j� ma redevance TV, j'vois pas pourquoi j'payerais pour d'autre.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Logique a �crit le 27/01/2015 � 9:20 :
Logique quand on sait que, lors de changement de FAI, le nouveau remborse en g�n�ral les fais de r�siliation du pr�c�dent ...
Signaler un contenu abusif
@ Logique �a r�pondu le 27/01/2015                                                 � 21:21:
En cas de changement des CGU ou en cas de panne il n'y a aucun frais de r�siliation ni de temps de r�siliation. L� ils changent de CGU soit vous acceptez soit vous les quittez et allez ailleurs sans frais de r�siliations.
Signaler un contenu abusif
@ Logique �a r�pondu le 27/01/2015                                                 � 21:23:
Et j'oubliais si ils vous demande de renvoyez la box, attendez leur lettre T si elle ne viens pas faites opposition sur votre compte pour pas qu'ils vous la facture!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
carpatrick a �crit le 27/01/2015 � 9:16 :
NUMERICABLE entreprise de truands. Prix facial int�ressant, mais bonjour le prix des options et des d�passements! Ma fille �tudiante les avait choisis, mais en lieu et place des quelques 30 � d'abonnement, ses factures avoisinaient plut�t les 100 �, et parfois plus! Donc, r�siliation, et pas question d'y retourner!
J'ai �t� chez SFR pendant quelque temps, m�me combat! R�sultat, depuis, tous nos contrats "communication" sont chez ORANGE. Pas le moins cher, mais le service est l�!
Signaler un contenu abusif
Chich �a r�pondu le 27/01/2015                                                 � 12:15:
Ba bien sur ce n'est pas la faute de votre fille mais celle de Num�ricable. Un peu trop facile non ?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Thiery a �crit le 27/01/2015 � 9:10 :
Moi qui ai d�j� quitt� sfr mobile par sa mauvaise qualit� de service.
Je pense qui si cette vente forc�e est mise en place je quitterais aussi sfr et ces probl�mes.
Ces magouilles de sfr devraient �tre corrig�es par la DGCCRF pour �viter que d'autres personnes se fassent arnaquer.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
rien � voir circulez a �crit le 27/01/2015 � 8:25 :
Cons�quence directe du capitalisme de connivence. Quand on voit le parcours de num�ricable, constell� de litiges incroyables, on se dit qu'il y a vraiment deux poids deux mesures.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
aq a �crit le 27/01/2015 � 8:15 :
A mon avis, il s'agit de vente forc�e, punie par la loi. Qu'en disent les juristes ?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
ml a �crit le 27/01/2015 � 7:56 :
Cela ne m'�tonne plus venant de SFR....Pr�s de deux mois apr�s ma r�silliation plus ou moins forc�e si je voulais internet (deux mois sans internet factur� alors que  d�branch� sans justification, migration jamais effectu�e et service client qui envoie des sms diffamatoire pr�tendant avoir cherch� � te joindre sans succ�s (mais aucuns appels en absences ni sur le r�pondeur etc...mais qui au final ne foutent rien t'invitant � les recontacter pour qu'on ne soit pas foutu de te dire pourquoi)......�a m'�tonne encore que des gens s'abonnent chez eux.....  Des pures voleurs qui me doivent du pognon (et comme par hasard quand eux on leurs doit du pognon c'est int�r�t, menaces etc.....mais quand une soci�t� te pr�l�ve du pognon sans te fournir le service (ce qui constitue un vol) ben la c'est diff�rent t'as limite qu'a attendre, rien a dire et t'as pas le droit d'appliquer des int�r�t pour de l'argent qu'on t'as pris pour rien et dont tu ne peux pas te servir alors qu'il est a toi........soci�t� a deux vitesses de toutes fa�on..
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Lennart a �crit le 27/01/2015 � 7:00 :
Cela dit c'est aussi un bon moyen de faire basculer un "client" vers une offre plus lucrative ou se d�barrasser d'un client qui lui coute plus qu'il ne rapporte puisque celui-ci a 4 mois apr�s la r�ception des nouveaux tarifs pour r�silier son abonnement avant �ch�ance sans frais.
Raven �a r�pondu le 27/01/2015                                                 � 7:32:
Qui lui co�te ? A part les frais de gestion, qui lui co�te quoi?
Lennart �a r�pondu le 27/01/2015                                                 � 16:16:
Un client qui ne rapporte pas est un client qui coute.
On peut faire des promos pour engranger des clients, mais � terme il faut le faire �voluer vers des services qui rapportent.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
bazy a �crit le 26/01/2015 � 23:57 :
L'offre Open d'orange inclut la TV. Sauf qu'en milieu rural, elle n'est pas disponible, sauf � se faire installer une antenne...payante. Sans parler du d�bit ADSL, plus de 4 fois inf�rieur au moins au d�bit urbain..pour le m�me prix. Sans parler non plus des connexions t�l�phones portables ou smartphones tr�s souvent...absentes. Tous fournisseurs confondus.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Numericable a �crit le 26/01/2015 � 23:56 :
Numericable je l'aurais fait un proc�s pour non portabilit� du num�ro, quatre mois apr�s ils m'ont mis ce num�ro en place peur de se voir en  proc�s,fermer d�s que mon contrat arrivera � expiration je the je change et je pars chez free. N'allait pas chez Numericable .
Numericable ne tient pas ses promessestient
Signaler un contenu abusif
Gun �a r�pondu le 27/01/2015                                                 � 7:08:
Numeric able a aussi change ses conditions et augmente les tarifs, VOUS pouvez partir maintenant sans payer de frais. N'attendez plus, fonez!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
pm11 a �crit le 26/01/2015 � 23:42 :
Merci pour l'article,
oui, on peut se demander si le but n'est pas de "sortir" les abonn�s � faible rendement, c'est � dire ceux qui ne sont qu'en Double Play ou pire juste Internet seul (mon cas depuis Club-Internet) car impossible de trouver maintenant ces 2 offres sur le nouveau portail...
Ou alors je suis un manche...
PS: ces offres existent pourtant toujours pages 18/19 de la brochure du 201/01/2015...
Signaler un contenu abusif
Albe �a r�pondu le 27/01/2015                                                 � 12:59:
Vous n'�tes pas un manche, j'ai �t� confront� au m�me probl�me, impossible de trouver l'offre sur leur site.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Tonton D a �crit le 26/01/2015 � 22:56 :
Juste une question : � partir du moment ou cette option quantifi�e et payante est impos�e, ne peut-on pas parler de vente forc�e et saisir la DGCCRF?
Signaler un contenu abusif
lolo �a r�pondu le 28/01/2015                                                 � 11:28:
Puisqu'on vous dit que vous pouvez r�silier votre abonnement sans frais ce n'est donc pas une vente forc�e, rien ne vous oblige � rester chez cet op�rateur, c'est pourtant simple � comprendre, la seule chose � saisir c'est votre stylo ou votre t�l�phone si vous refusez cette option.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
indhyra g a �crit le 26/01/2015 � 21:44 :
domage je me casse pour free ya longtemps que l on ma dit beaucoup moin chere
et deux euro a partir d une deuxieme ou troisieme ligne mobile ya pas ca chez sfr
en plus sa ram  donc un abonner en moins
Chich �a r�pondu le 27/01/2015                                                 � 12:10:
Le minimum serait d'�crire votre commentaire dans un francais correct et avec une vraie syntaxe.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Carlier a �crit le 26/01/2015 � 20:37 :
Y en a marre de toutes ces magouilles
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
tacata a �crit le 26/01/2015 � 20:05 :
je propose un boycott national de sfr
si on le faisait tous on les mettrait au pas en deux mois comme l'on fait les annonceurs du bon coin qui ont obtenu la r�ductions des frais pr�vus pour les annonces
Signaler un contenu abusif
@ tacata �a r�pondu le 20/02/2015                                                 � 20:44:
La m�me chose pourrait �tre faite avec le gouvernement mais bon y a toujours personne dans les rues...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Testdes 46754 a �crit le 26/01/2015 � 20:05 :
Compteur qui evolue des resiliations en ligne
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
CRC32 a �crit le 26/01/2015 � 19:02 :
C'est Bouygues Telecom qui va se r�galer... en m�me temps lorsqu'on s'endette � un niveau record pour acqu�rir SFR et prochainement Portugal Telecom, il n'y a gu�re d'illusions � se faire sur le comportement commercial des soci�t�s acquises par le champion de la sp�culation: Patrick DRAHI.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
pomme a �crit le 26/01/2015 � 18:36 :
Il n'y a pas de petit b�n�fice... un sous par ci, un sous par l� et voila comme on rach�te SFR et autres...bringbaleries...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Comparez avec Free ? a �crit le 26/01/2015 � 18:34 :
Chez Free, c'est aussi une option et c'est pas moins cher...
pomme �a r�pondu le 26/01/2015                                                 � 18:40:
... sauf qu'une option par d�finition n'est pas impos�e... sauf chez SFR, Num�ricable et autres saloperies...
Signaler un contenu abusif
Bazy �a r�pondu le 27/01/2015                                                 � 0:01:
Faux; Ils font tous LA M�ME CHOSE. Forfait Orange avec TV obligatoire, mais non accessible en milieu rural.
Signaler un contenu abusif
@ Comparez avec Free ? �a r�pondu le 27/01/2015                                                 � 21:25:
Mais chez free c'est une "option" et c'est NOUS qui d�cidons de l'activer et la payer ou de ne pas l'activer et de e pas la payer...
Gigi �a r�pondu le 11/02/2015 � 14:40:
Bnjour tyout le monde
H� bien moi �tant client de tres longue date  chez sfr c'est la premiere augmentation que je subit et d�guiser en soit disant une option  tv obligatoire  je pense aussi me diriger ver free car je pense que ce n'est qu'un debut malheureusement
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
�
Merci pour votre commentaire. Il sera visible prochainement sous r�serve de validation.
�a le � :
