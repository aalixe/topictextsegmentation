TITRE: Un match nul qui arrange tout le monde, Football
DATE: 27-01-2015
URL: http://www.lesechos.fr/sport/football/sports-705855-un-match-nul-qui-arrange-tout-le-monde-1087081.php
PRINCIPAL: 0
TEXT:
Un match nul qui arrange tout le monde
Le 26/01 � 21:02
Un match nul qui arrange tout le monde
1 / 1
Au terme d'un match engag� et o� personne n'a calcul�, la RD Congo et la Tunisie se quittent sur un r�sultat nul qui fait les affaires des deux s�lections, qualifi�es de concert pour les quarts de finale.
Un r�sultat qui arrange tout le monde, sans forc�ment l'avoir voulu. Ce groupe B �tait totalement ind�cis au d�but de cette derni�re journ�e avec la possibilit� d'avoir recours � un tirage au sort pour d�signer le deuxi�me qualifi�. Mais, pour un seul petit but de plus marqu� par rapport au Cap Vert, la RD Congo n'a pas eu besoin d'avoir recours � une telle extr�mit� pour s'assurer de son billet pour les quarts de finale de la CAN. Alors que tous les calculs �taient permis en fonction de l'�volution du score de l'autre match du groupe, les deux �quipes ont jou� pour l'emporter et �viter toute mauvaise surprise de derni�re minute. A ce petit jeu, c'est la Tunisie qui avait le mieux d�marr� la rencontre avec une pression intense sur les buts d'un Kidiaba pas forc�ment brillant mais bien suppl�� par sa d�fense. La troisi�me occasion franche �tait la bonne pour la Tunisie qui s'�vitait de douter et de vivre une situation comme le Gabon ce dimanche, qui avait son destin en main et avait craqu� au moment de conclure.
Apr�s cette ouverture du score, les Tunisiens ont un peu baiss� de pied, se montrant moins pressants. Ce qui a permis � la RDC de revenir dans le match. Remont�s par leur s�lectionneur Florent Ibenge � la mi-temps, les joueurs de la RD Congo sont revenus des vestiaires avec d'autres ambitions, car �galiser �tait une n�cessit� pour reprendre la deuxi�me place au Cap Vert. Mais, sans l'inefficacit� d'Akaichi puis de Khazri devant le but de Kidiaba, la RDC aurait �t� en plus grande difficult�. Et c'est bien la Tunisie qui se mettait la pression pour la fin de match en laissant les Congolais revenir � hauteur et reprendre le deuxi�me billet pour les quarts de finale. Alors que le r�sultat faisait les affaires des deux �quipes, la peur d'une �volution du score lors de l'autre match du groupe incitait les deux �quipes � toujours pousser, mais de mani�re trop brouillonne malgr� l'entr�e en jeu d'�l�ments offensifs suppl�mentaires. La RDC a eu plusieurs fois la balle de match mais n'est pas parvenue � battre un A.Mathlouthi en grande forme. Au coup de sifflet final, s�lectionneurs et joueurs des deux pays pouvaient se jeter dans les bras les un des autres pour f�ter leur qualification pour les quarts de finale. La Tunisie a rendez-vous avec le pays organisateur, la Guin�e Equatoriale, alors que la RD Congo aura un duel fratricide face au Congo.
10eme minute
Chikhaoui lance parfaitement Akaichi sur l'aile droite qui entre dans la surface apr�s avoir effac� Kasusula. L'attaquant tunisien tente de dribbler Kidiaba puis retrouve en retrait Sassi, seul face au but. Mais les d�fenseurs congolais Issama Mpeko et Kimwaki se sont mis sur leur ligne et ce dernier d�gage le danger.
20eme minute
Jeu � trois parfaitement men� par les Tunisiens sur l'aile gauche avec Khazri, Sassi et Chikhaoui. Le capitaine tunisien peut entrer dans la surface et se mettre sur son pied droit. Il tente une frappe tendue crois�e qui file au ras du poteau mais Kidiaba se couche pour sortir le ballon du bout des doigts de la main gauche.
28eme minute
Sur coup franc, Kasusula prend sa chance d'une frappe enroul�e du pied gauche qui passe de peu au-dessus du mur tunisien. Le ballon retombe dans le but mais A.Mathlouthi est sur la trajectoire pour d�gager le danger en corner.
31eme minute (0-1)
Suite � une longue s�quence de possession sur l'aile gauche avec Khazri et Sassi, le ballon arrive sur Chikhaoui qui contr�le et cherche � frapper mais Mbemba intervient pour d�gager le danger face au capitaine tunisien. La frappe contr�e se transforme en passe en profondeur dans le dos de la d�fense congolaise pour Akaichi, � la limite du hors-jeu, qui place une t�te plongeante pour battre Kidiaba.
48eme minute
Mbemba acc�l�re sur l'aile gauche et trouve Mbokani � l'entr�e de la surface dans l'axe. L'attaquant congolais se met sur son pied droit et place une frappe �cras�e � ras-de-terre qui trouve le cadre mais A.Mathlouthi capte tr�s facilement le ballon.
56eme minute
Suite � une ouverture de Mubele dans le dos de la d�fense tunisienne, Mbokani prend la profondeur et va se pr�senter seul face � A.Mathlouthi... mais le gardien tunisien fait une sortie lointaine, hors de la surface pour d�gager le danger. Il semble qu'il touche le ballon du bras sur cette action mais l'arbitre ne dit rien.
61eme minute
Chikhaoui acc�l�re sur l'aile droite et prend de vitesse Kasusula. Le capitaine tunisien centre en retrait pour Akaichi qui contr�le et a Sassi sur sa gauche, totalement d�marqu�. Mais l'attaquant tunisien choisit la solution personnelle et place une frappe crois�e du pied gauche largement � gauche du but de Kidiaba.
62eme minute
Issama Mpeko apporte le surnombre sur l'aile droite et centre au point de penalty pour la t�te de Mbokani. L'attaquant congolais prend le meilleur sur Abdennour et Ben Youssef mais il ne parvient pas � rabattre le ballon vers la cage de A.Mathlouthi. Le ballon passe au-dessus.
65eme minute
Chikhaoui acc�l�re sur l'aile gauche et efface Issama Mpeko d'un passement de jambe parfaitement r�alis�. Le capitaine tunisien centre en retrait pour Khazri dos au but. Ce dernier contr�le et se retourne pour placer une frappe crois�e du pied droit mais il enl�ve trop sa tentative, qui passe au-dessus de la cage de Kidiaba.
66eme minute (1-1)
Sur une longue relance d'Issama Mpeko sur l'aile droite vers l'entr�e de la surface, Bokila est trop court. Le ballon rebondit au sol et Mbokani est � la retomb�e du ballon pour le d�vier dans le dos de la d�fense. Bokila est vigilant et, pour son premier ballon, contr�le et fait preuve de sang-froid face � A.Mathlouthi en encha�nant avec une frappe du pied gauche pour �galiser.
75eme minute
Maaloul est bien lanc� sur l'aile gauche par Sassi qui centre fort devant le but au premier poteau. Younes est � la r�ception de ce centre et met le plat du pied gauche. L'attaquant tunisien trouve le cadre mais Kidiaba plonge parfaitement dans ses pieds pour conc�der le corner.
83eme minute
Kasusula apporte le surnombre sur l'aile gauche et centre au deuxi�me poteau. Bokila r�cup�re le ballon et centre � son tour devant le but d'A.Mathlouthi mais la d�fense tunisienne d�gage sur Kasusula, qui prend sa chance d'une frappe tendue du pied gauche qui passe de peu � gauche du but tunisien.
RD Congo
KIDIABA a sans doute permis � la RD Congo de se qualifier pour les quarts de finale. Le gardien congolais, qui ne peut rien faire sur le but d'Akaichi, a fait tous les arr�ts qu'il fallait pour �viter une d�faite qui aurait signifi� � la RDC son �limination. En d�fense, MONGONGU a tout donn�, m�me plus que tout car il a �t� contraint de laisser sa place en fin de match � ZAKUANI en raison de crampes. A ses c�t�s, KIMWAKI a su intervenir proprement et avec sang-froid, comme en d�but de match o� il sort un ballon de but sur sa ligne. Sur les c�t�s, tant MPEKO que KASUSULA ont pris leurs responsabilit�s, surtout en fin de match, pour apporter le surnombre tout en �tant solides d�fensivement. Devant, BOLASIE a beaucoup boug� pour d�stabiliser la d�fense tunisienne mais sans grande r�ussite alors que MBOKANI n'a pas su prendre le meilleur sur la d�fense centrale des Aigles de Carthage. D�cevant. Derri�re lui, MABWATI a fait un match en demi-teinte, o� il n'a pas su apporter le soutien n�cessaire � ses attaquants et a �t� logiquement remplac� par BOKILA, d�cisif sur son premier ballon puis pesant sur la d�fense tunisienne. Sur l'aile droite, MUBELE a fait un match correct, sans �clat. Enfin, devant la d�fense, le duo MBEMBA- MAKIADI a fait un match solide tant � la r�cup�ration qu'� la relance.
Tunisie
AKAICHI a fait un match entre ombre et lumi�re. L'attaquant tunisien a �t� opportuniste sur son but, qui a facilit� la t�che de la Tunisie dans cette rencontre. Mais il a souvent pris de mauvaises d�cisions alors que les Aigles de Carthage avaient de bonnes situations devant le but de Kidiaba. Il a �t� remplac� au d�but du dernier quart d'heure par YOUNES, qui s'est montr� dangereux sur son premier ballon avant d'avoir moins d'influence. Devant un A.MATHLOUTHI solide et inspir� dans son but, la d�fense centrale ABDENNOUR-BEN YOUSSEF a fait un match de grande qualit�, seulement prise � revers sur le but de la RD Congo. Devant elle, YAKOUBI et RAGUED ont fait un match tout juste correct, avec des soucis de pr�cision dans la relance et la r�cup�ration. KHAZRI a �t� mobile mais sans r�ellement cr�er d'espaces pour CHIKHAOUI, qui a �t� de tous les bons coups, sans faire preuve de r�ussite avant cette passe d�cisive un peu heureuse pour la t�te d' AKAICHI, d�j� buteur contre la Zambie. Entr� pour le dernier quart d'heure � la place de Khazri, ALI MONCER n'a pas eu suffisamment de temps pour s'exprimer. En soutien de l'attaquant, SASSI a �t� pr�sent pour mener le jeu tunisien avec qualit� et efficacit�. Enfin, sur les c�t�s, H.MATHLOUTHI et MAALOUL ont r�alis� une performance propre, efficaces d�fensivement mais pas forc�ment suffisamment pr�sents en soutien sur les phases offensives.
M.Gassama a fait un match solide lors d'une rencontre engag�e mais pas tendue. Il n'a sorti qu'� une seule occasion le carton jaune en d�but de match pour assurer son autorit� et n'en a plus eu besoin ensuite face � des d�bats corrects.
- Cette opposition entre les deux s�lections �tait la quatri�me lors d'une phase finale de la CAN. La Tunisie s'�tait impos�e � deux reprises, dont la derni�re en 2004 (3-0), pour un seul match nul en 1994 (1-1).
- La RD Congo est une des rares �quipes � ne pas avoir re�u de carton jaune durant les deux premiers matchs.
- Wahbi Khazri �tait le seul joueur tunisien sous la menace d'une suspension lors de l'�ventuel quart de finale, suite � son carton jaune re�u lors de la premi�re rencontre face au Cap Vert.
Estadio de Bata (10 000 spectateurs environ)
Temps chaud et humide - Pelouse correcte
Arbitre : M.Gassama ( 6)
Buts : Bokila (66eme) pour la RD Congo � Akaichi (31eme) pour la Tunisie
Avertissements : Ali Yakoubi (9eme) pour la Tunisie
Expulsion : Aucune
RD Congo
Kidiaba (cap) ( 6) � Issama Mpeko ( 5), Mongongu ( 6) puis Zakuani (87eme), Kimwaki ( 6), Kasusula ( 5) � Mbemba ( 5), Makiadi ( 5) - Mubele ( 4) puis Kabananga (78eme), Mabwati ( 4), Bolasie ( 4) puis Bokila (60eme) � Mbokani ( 4)
N'ont pas particip� : Kudimbana (g), P.Mandanda (g), Oualembo, Muganga, Kage, Kebano, Bawaka, Mabidi
Entra�neur : F.Ibenge
Tunisie
A.Mathlouthi ( 6) � H.Mathlouthi ( 5), Abdennour ( 6), Ben Youssef ( 6), Maaloul ( 5) � Ali Yakoubi ( 4), Ragued ( 4) puis Nater (87eme) - Khazri ( 5) puis Ali Moncer (77eme), Sassi ( 6), Chikhaoui (cap) ( 5) � Akaichi ( 5) puis Younes (74eme)
N'ont pas particip� : Ben Mustapha (g), Ben Cherifia (g), Mohsni, Bedoui, Msakni, Rjaibi, Chermiti, Saihi, Ben Djemla
Entra�neur : G.Leekens
L1, L2, National, CFA, CFA2, Allemagne, Angleterre, Espagne, Italie... Tous les r�sultats et les classements en un coup d'oeil
Football
