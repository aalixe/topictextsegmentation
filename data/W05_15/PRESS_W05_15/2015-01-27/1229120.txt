TITRE: Auschwitz: 'Plus jamais ça', lance Obama
DATE: 27-01-2015
URL: http://www.lefigaro.fr/flash-actu/2015/01/27/97001-20150127FILWWW00336-auschwitz-plus-jamais-ca-lance-obama.php
PRINCIPAL: 1229092
TEXT:
le 27/01/2015 à 16:53
Publicité
Le président américain Barack Obama a marqué aujourd'hui le 70e anniversaire de la libération d'Auschwitz en mettant en garde contre une résurgence de l'antisémitisme et en exhortant la communauté internationale à faire en sorte qu'un tel génocide ne se reproduise "plus jamais". Jurant de "ne jamais oublier" la mort des six millions de juifs et les millions d'autres personnes assassinées par le régime nazi, M. Obama a appelé la communauté internationale à faire en sorte que ce génocide "ne se reproduise plus jamais".
Mardi, pour commémorer les 70 ans de la libération d'Auschwitz, survivants de l'Holocauste et chefs d'Etat se sont retrouvés sur le site de l'ancien camp d'extermination nazi, en Pologne.
Cet anniversaire a une acuité particulière, vingt jours après les attentats meurtriers de jihadistes français contre le journal Charlie Hebdo et un magasin casher à Paris. "Les récentes attaques terroristes survenues à Paris nous rappellent de façon douloureuse notre devoir de condamner et de combattre la résurgence de l'antisémitisme sous toutes ses formes, que ce soit par la négation ou la banalisation de l'Holocauste", a déclaré le président américain dans un communiqué.
"Aujourd'hui, nous nous rassemblons et nous nous engageons auprès des millions de personnes assassinées et des survivants à faire en sorte que cela ne se reproduise plus jamais", a-t-il conclu.
