TITRE: FC Barcelone : deux belles erreurs commises avec Griezmann
DATE: 27-01-2015
URL: http://www.butfootballclub.fr/1601993-fc-barcelone-les-deux-boulettes-du-club-avec-griezmann/
PRINCIPAL: 1228303
TEXT:
FC Barcelone : les deux boulettes du club avec Griezmann
Post� le 27 janvier 2015
1 commentaire
��
Le FC Barcelone a loup� deux belles occasions de compter Antoine Griezmann, qui flambe avec l�Atl�tico Madrid, dans ses rangs.
C�est le quotidien catalan Sport qui, ce matin, d�voile les regrets que peuvent avoir les dirigeants du FC Barcelone en observant les performances magistrales de Griezmann � Madrid. Par deux fois, le Bar�a aurait pu le faire venir. La premi�re occasion, raconte le journal, �tait pendant son cursus de formation. Alors que le club catalan cherchait justement des jeunes joueurs de son profil, le Bar�a aurait vu Griezmann lui passer sous le nez avant que celui-ci ne file � la Real Sociedad.
Quelques ann�es plus tard, c�est Pep Guardiola, lors de sa derni�re ann�e au club, qui aurait coch� son nom pour se renforcer offensivement. Sauf qu�� l��poque, la priorit� se nommait Alexis Sanchez, que le FC Barcelone a finalement r�ussi � recruter� Avant de le vendre par la suite � Arsenal o� Sanchez flambe d�sormais.
