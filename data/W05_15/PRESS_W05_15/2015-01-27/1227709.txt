TITRE: Assurance maladie: des d�penses en hausse dues � de nouveaux m�dicaments on�reux
DATE: 27-01-2015
URL: http://www.latribune.fr/actualites/economie/france/20150127trib82274f860/assurance-maladie-des-depenses-en-hausse-dues-a-de-nouveaux-medicaments-onereux.html
PRINCIPAL: 0
TEXT:
Abonnez-vous � partir de 1�
La loi pour le financement de la s�curit� sociale vot�e en 2013 avait fix� un objectif national de d�penses de l'assurance maladie pour l'ensemble des r�gimes � 178,3 milliards d'euros en 2014. Celui-ci devrait �tre respect�, indique-t-on du c�t� du minist�re de la Sant�.
Ainsi, en 2014, "les remboursements de soins du r�gime g�n�ral ont progress� de 3,1%, dont 3,6% pour les remboursements de soins de ville", a indiqu� la Caisse nationale d'assurance maladie (Cnam), lundi 26 janvier. Elle note en particulier "une croissance tr�s forte des remboursements des produits de�sant�" (m�dicaments et dispositifs m�dicaux): +4,9% en 2014, apr�s +1,1% en 2013.
Le Solvadi a creus� les "d�penses m�dicamenteuses"
Dans le d�tail, si "les remboursements de m�dicaments d�livr�s en ville d�croissent faiblement" en 2014 (-0,6%), les remboursements de m�dicaments prescrits en ville et d�livr�s � l'h�pital augmentent de pr�s de 70%. Cette hausse est "int�gralement li�e � l'arriv�e de nouveaux m�dicaments efficaces et on�reux" contre l'h�patite C, d�but �2014, assure la Cnam. Le prix de l'un d'entre eux, le Sovaldi, du laboratoire am�ricain Gilead, a �t� ren�goci� par le gouvernement en novembre dernier, pour atteindre 41.000 euros pour 12 semaines de traitement, contre 57.000 auparavant.
Mais la facture finale li�e � l'h�patite C, notamment, devrait baisser "significativement" pour l'assurance maladie, souligne-t-on au minist�re de la�Sant�. Les laboratoires doivent en effet reverser la diff�rence entre les prix provisoires initialement pratiqu�s et le tarif finalement fix�.�Par ailleurs, dans le cadre d'un m�canisme de r�gulation d�j� existant, renforc� pour ces traitements, les laboratoires doivent aussi reverser certaines sommes d�s que les d�penses de l'assurance maladie d�passent un certain seuil. Au total, cela repr�sente plusieurs centaines de millions d'euros, selon Les Echos.
Les soins de m�decine g�n�rale en augmentation
Les remboursements des autres produits de�sant�, comme les dispositifs m�dicaux, enregistrent "une progression encore soutenue" (+6%, contre +6,9% en 2013).
En outre, les d�penses de soins de�m�decine g�n�rale ont augment� de 3% (contre 2,8% en 2013), en raison notamment de "versements plus importants" qu'en 2013 au titre de la r�mun�ration sur objectif de sant�publique ou de la majoration de la consultation pour les personnes �g�es. Mais "les remboursements des seules consultations" chez le g�n�raliste ont, elles, baiss� de 1,3%, souligne la Cnam.
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Votre email ne sera pas affich� publiquement
Tous les champs sont obligatoires
Commentaires
rockit a �crit le 29/01/2015 � 9:08 :
et les cures thermales pour les b�n�ficiaires de l'AME
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
aaa a �crit le 27/01/2015 � 18:04 :
Avant que l'�tat fasse e genre de reproche il faudrait d�j� que la s�cunous rembourse... Or a leur actuelle et vu les remboursements, mieux vaut avoir une mutuelle plut�t qu'une s�cu... Sas parler des 1 euro/doc et 0.5�/boite...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Patrickb a �crit le 27/01/2015 � 13:15 :
Les m�dicaments sont (plus ou moins) rembours�s par la s�cu, c'est-�-dire par les cotisations des contribuables. L'�tat �tant donc partie prenante dans les r�sultats des entreprises pharmaceutiques, je ne comprends pas pourquoi l'�tat ne participerait pas aux b�n�fices ? Dans le cas contraire, l'�tat pourrait rembourser beaucoup moins, ce qui ferait forc�ment chuter les prix des m�dicaments. Dans les deux cas, le contribuable s'y retrouverait :-)
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
mirou13 a �crit le 27/01/2015 � 13:07 :
le monde m�dical a envahi la politique...il f
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
paradoxe a �crit le 27/01/2015 � 10:58 :
l'arriv� des g�n�riques augmente le prix des nouveaux m�dicaments car d�sormais la marge du laboratoire repose uniquement sur eux et pour un temps tr�s court.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
papy a �crit le 27/01/2015 � 9:46 :
Le prix du m�dicament est d�termin� en fonction du service suppos� rendu et non du cout de fabrication!!!!peut-on esp�rer une fin � cette escroquerie organis�e?
@papy �a r�pondu le 27/01/2015                                                 � 10:55:
gr�ce � votre crit�re des m�dicaments sont retir�s du march� car non rentable.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
�
Merci pour votre commentaire. Il sera visible prochainement sous r�serve de validation.
�a le � :
