TITRE: Une starlette de Manchester United recrut�e pour 0 � par le PSG�?
DATE: 27-01-2015
URL: http://www.footmercato.net/premier-league/transferts/une-starlette-de-manchester-united-recrute-pour-0-eur-par-le-psg_147011
PRINCIPAL: 1228075
TEXT:
Accueil > Premier League > Transferts > Une starlette de Manchester United recrut�e pour 0 � par le PSG�(...)
Une starlette de Manchester United recrut�e pour 0 � par le PSG�?
Publi� le :
S�bastien DENIS Suivre @footmercato_net
� la recherche d�opportunit�s, le PSG songerait � recruter une p�pite de Manchester United libre en juin prochain.
Andreas Pereira futur joueur du PSG ?
�Maxppp
Paul Pogba a laiss� un souvenir amer � Manchester United. L�international tricolore, qui brille aujourd�hui � la Juventus Turin, n�a rien rapport� aux Red Devils, qui doivent avoir du mal � avaler la pilule. Mais malgr� ce douloureux �pisode, MU n�a semble-t-il pas retenu la le�on puisque plusieurs talents du club sont d�ores et d�j� libres de n�gocier avec le club de leur choix. Parmi ces p�pites disponibles � z�ro euro, figure Andreas Pereira.
� 19 ans, le milieu offensif belge est tr�s demand�. Fils de l�ancien footballeur professionnel br�silien Marcos Pereira, il est n� en Belgique lorsque son p�re �voluait dans la premi�re division locale. Form� au PSV Eindhoven, il a rejoint Manchester United en 2011 et a connu les s�lections belges U15, U16, U17 avant de f�ter sa premi�re cape avec le Br�sil U20. Jug� tr�s prometteur, Andreas Pereira a disput� son premier match pro avec MU en ao�t dernier en League Cup. Conscient de son potentiel, MU lui a bien propos� une offre de prolongation de contrat, mais le joueur l�aurait repouss� d�un revers de la main.
Une situation qui a �veill� l�int�r�t du PSG comme le rapporte le Daily Mail. Le tablo�d anglais indique que le club de la capitale songerait � proposer un contrat au milieu de terrain en vue de le recruter libre en fin de saison. Mais le club de la capitale ne serait pas seul sur la belle affaire, puisque l�AS Rome et le Milan AC seraient �galement sur le dossier. La bataille s�annonce donc f�roce, mais le PSG a bien des arguments � faire valoir.
