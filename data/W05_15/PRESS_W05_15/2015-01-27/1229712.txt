TITRE: Attaque � Tripoli : un Fran�ais parmi les victimes - Lib�ration
DATE: 27-01-2015
URL: http://www.liberation.fr/monde/2015/01/27/attentat-jihadiste-dans-un-hotel-de-tripoli_1190070
PRINCIPAL: 1229709
TEXT:
Lire sur le readerMode zen
Un Fran�ais, un Am�ricain, deux Philippines et un Sud-Cor�en, font partie, selon la s�curit� lybienne, des neuf personnes tu�es lors de l�assaut de plusieurs heures lanc� mardi contre le Grand H�tel Corinthia de Tripoli,�connu pour accueillir des diplomates, des responsables libyens et des �trangers. L�assaut a �t� men� par des hommes arm�s qui, au final, se sont fait exploser. Au�moment o� l�attaque �tait en cours, la branche libyenne du groupe jihadiste Etat islamique l�a revendiqu�e, selon le Centre am�ricain de surveillance des sites islamistes (Site Intelligence Group). Photo AP
Recevoir la newsletter
