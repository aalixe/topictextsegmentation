TITRE: Emma Watson h�ro�ne du prochain Disney
DATE: 27-01-2015
URL: http://www.bfmtv.com/culture/emma-watson-heroine-du-prochain-disney-859785.html
PRINCIPAL: 0
TEXT:
Imprimer
Emma Watson a d�voil� sur Facebook qu'elle serait � l'affiche du prochain Disney, La belle et la b�te.
Entre l'Onu et Davos, Emma Watson a encore le temps de tourner. L'actrice britannique, jeune femme engag�e en faveur de l'�galit� des sexes, sera l'h�ro�ne du prochain film de Disney, La belle et la b�te.
Elle l'a annonc� sur Facebook lundi: "Je jouerai Belle dans le prochain film de Disney La belle et la b�te. J'ai grandi avec ce film et cela semble presque irr�el de danser sur Be our guest , et de chanter Something There" .
"J'ai h�te que vous voyiez �a"
"Mon 'moi' de six ans est surexcit� et mon coeur explose. Il est temps de commencer � prendre des cours de chant. J'ai h�te que vous voyiez �a!", exulte-t-elle encore.
L'actrice de Harry Potter d�voile donc une information de taille: le film sera chantant, comme l'�tait le dessin anim� de 1991. On n'en sait pour l'instant pas plus sur le casting ni sur la date de sortie. Bill Condon (Twilight, Dreamgirls) devrait r�aliser le film.
Cassel et Seydoux
Une Belle et la b�te fran�aise et beaucoup plus sombre sign� Christophe Gans, avec Vincent Cassel et L�a Seydoux, est sorti en 2014.
Les studios Disney vont par ailleurs livrer en mars 2015 une version film�e de Cendrillon, dirig�e par Kenneth Branagh.�
