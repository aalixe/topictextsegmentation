TITRE: Lyon : un homme arr�t� pour avoir t�l�charg� 2 millions d�images p�dophiles en 8 mois - 27/01/2015 - LaDepeche.fr
DATE: 27-01-2015
URL: http://www.ladepeche.fr/article/2015/01/27/2037823-lyon-homme-arrete-avoir-telecharge-2-millions-images-pedophiles-8.html
PRINCIPAL: 1229115
TEXT:
Lyon : un homme arr�t� pour avoir t�l�charg� 2 millions d�images p�dophiles en 8 mois
Publi� le 27/01/2015 � 16:21
,
Mis � jour le 27/01/2015 � 21:40
Faits divers
Un homme regardant des images p�dopornographiques sur son ordinateurs./Photo DDM, illustration.
Un homme a �t� interpell� lundi � son domicile lyonnais apr�s avoir t�l�charg� plus de deux millions d�images p�dopornographiques en huit mois.
�g� de 28 ans, le suspect habite dans les beaux quartiers de la pr�fecture du Rh�ne. Il avait fait l�objet d�un signalement de la plateforme de veille sur internet du centre technique de la gendarmerie nationale de Rosny-sous-Bois. Celle-ci avait rep�r� les t�l�chargements ill�gaux r�alis�s en ��peer-to-peer��. ��On a exploit� son ordinateur et on a calcul� qu'il avait t�l�charg� un peu plus de deux millions d'images��, entre juillet 2013 et f�vrier 2014, soit plus de 8300 par jour de moyenne, a indiqu� la police.
Inconnu des services de police, l�homme s�est justifi� en disant ��qu�il avait une certaine app�tence pour ce genre de choses��, a-t-elle ajout�. Le jeune homme a �t� convoqu� devant le tribunal correctionnel le 25 juin. Il encourt un maximum de cinq ans de prison.
LaDepeche.fr
