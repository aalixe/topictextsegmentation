TITRE: Alexis Tsipras d�voile son gouvernement en Gr�ce
DATE: 27-01-2015
URL: http://www.romandie.com/news/Alexis-Tsipras-devoile-son-gouvernement-en-Grece/559561.rom
PRINCIPAL: 1228997
TEXT:
Tweet
Alexis Tsipras d�voile son gouvernement en Gr�ce
Le nouveau Premier ministre grec Alexis Tsipras a d�voil� son gouvernement mardi � Ath�nes. Il a nomm� l'�conomiste Yanis Varoufakis au poste de ministre des Finances.
Le portefeuille de la D�fense a �t� confi� � Panos Kammenos. Ce dernier est le chef de file des Grecs ind�pendants, formation de droite souverainiste alli�e au parti de gauche radicale Syriza.
Yanis Varoufakis est un universitaire de 53 ans. Il poss�de la double nationalit� grecque et australienne et se d�finit comme un "�conomiste par accident".
Il combat la gestion de la crise �conomique par l'Europe qui, dit-il, risque de saper les fondements d�mocratiques du continent et de faire �clater la zone euro. Il s'en prend � la th�orie �conomique conventionnelle qui pr�ne la rigueur budg�taire et les r�formes de comp�titivit� comme des r�ponses � la crise.
La r�duction de la dette publique et le programme d'aide de l'UE et du FMI � la Gr�ce sont parmi les sujets chauds � g�rer par le nouveau gouvernement d'Alexis Tsipras, et seront d�j� au centre de la discussion pr�vue vendredi � Ath�nes avec Jeroen Dijsselbloem, le chef de l'Eurogroupe.
Influence sur la Bourse
L'indice g�n�ral de la Bourse d'Ath�nes (Athex) a chut� de plus 5% alors qu'�tait annonc�e la composition du gouvernement grec.
L'indice g�n�ral qui avait ouvert dans le rouge � 813 points est descendu jusqu'� 761 points soit -6,39% en milieu d'apr�s-midi, � peu pr�s au moment o� �tait annonc�e la liste du nouveau gouvernement men� par la gauche radicale Syriza. L'Athex est ensuite l�g�rement remont� � 776 points vers 15h00 (-4,59%).
(ats / 27.01.2015 15h40)
