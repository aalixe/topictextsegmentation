TITRE: Libye : 9 morts dans l'attaque terroriste d'un h�tel � Tripoli - Afrik.com : l'actualit� de l'Afrique noire et du Maghreb - Le quotidien panafricain
DATE: 27-01-2015
URL: http://www.afrik.com/libye-attaque-terroriste-dans-un-hotel-de-luxe-a-tripoli-9-morts
PRINCIPAL: 1229709
TEXT:
mardi 27 janvier 2015 / par Kardiatou Traor�
L�h�tel Corinthia en Libye/ Cr�dit photo�: corinthia.com
Un h�tel de luxe en Libye est, en ce moment-m�me, le th��tre d�une attaque terroriste revendiqu�e par des combattants du groupe extr�miste Etat islamique dans le pays. L�annonce a �t� faite via le r�seau social Twitter. Au moins neuf morts sont � d�plorer.
Tripoli, la capitale libyenne, a �t� la cible d�une attaque terroriste, ce mardi 27 janvier 2015, devant l�h�tel de luxe Corinthia. Une attaque imm�diatement revendiqu�e par la branche libyenne de l�organisation Etat islamique, qui aurait, selon plusieurs sources, caus� la mort de neuf personnes et fait plusieurs bless�s.
L�h�tel Corinthia assi�g�
Cet h�tel de luxe, connu pour avoir abrit� des diplomates et des responsables de gouvernement, est assi�g� par les forces de s�curit� � la suite de l�attaque. En effet, � l�issue de l�explosion de la voiture, plusieurs hommes arm�s ont fait irruption dans l�h�tel obligeant les forces de l�ordre � encercler le lieu afin d�interpeller les combattants et s�curiser la zone. Selon le minist�re de l�Int�rieur, un des tireurs a �t� arr�t� par les forces de l�ordre. Les autres tireurs se seraient retranch�s au 16�me �tage de l�h�tel, selon un journaliste pr�sent � Tripoli. Des r�sidents de l�h�tel pourraient �tre retenus en otage par les combattants.
Selon certaines sources, l�h�tel avait re�u, quelques jours avant l�attaque, une menace anonyme invitant les r�sidents des lieux ��� �vacuer le b�timent��. En l�espace d�un mois, Tripoli a �t� la cible de trois attentats commis par les combattants du groupe extr�miste Etat islamique. Des actes qui inqui�tent les observateurs, qui craignent de nouvelles attaques prochainement.
� la une
