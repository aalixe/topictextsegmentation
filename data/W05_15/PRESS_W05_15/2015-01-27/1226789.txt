TITRE: Les �couteurs et oreillettes bient�t interdits au volant
DATE: 27-01-2015
URL: http://www.nerienlouper.fr/73550-les-ecouteurs-et-oreillettes-bientot-interdits-au-volant/
PRINCIPAL: 1226787
TEXT:
Tweet
� Giuseppe Porzani � Fotolia.com
Les �couteurs, oreillettes, casques et autres kits mains libres seront tr�s bient�t interdits au volant en France. Le ministre de l�Int�rieur, Bernard Cazeneuve, a d�voil� lundi 26 janvier de nouvelles mesures pour tenter de diminuer le nombre de tu�s sur les routes. En effet, les chiffres de la mortalit� sur les routes de France pour l�ann�e 2014 ont �t� d�voil�s. Malheureusement, le nombre de tu�s sur les routes en 2014 a �t� sup�rieur � celui de 2013. Au cours de l�ann�e derni�re, 3 388 personnes ont trouv� la mort sur les routes de France. Un bilan sup�rieur � celui de l�ann�e 2013 avec 120 tu�s en plus. Une hausse de la mortalit� notamment chez les pi�tons, (+8%) cyclistes(+8%)�et cyclomotoristes(+6%). �Une note positive toutefois, le taux de mortalit� du mois de d�cembre 2014 en baisse de 8,9% par rapport � d�cembre 2013. 23 vies ont ainsi �t� �pargn�es sur les routes de France en d�cembre dernier par rapport au d�cembre de l�ann�e pr�c�dente. Toutefois, cela n��tant pas suffisant, le mauvais bilan de 2014�entra�ne toute une s�rie de mesure afin que l�ann�e 2015 ne soit pas encore endeuill�e par de tels chiffres.
Au total, pas moins de 26 mesures ont �t� pr�sent�es et sont � d�couvrir ci-dessous. Parmi celles qui font d�j� parler, l�interdiction de porter tout syst�me de type �couteurs, oreillettes, casques audio� susceptibles de limiter aussi bien l�attention que l�audition des conducteurs.
A noter �galement le d�ploiement de radars double-face pour permettre aux enqu�teurs de mieux identifier les infractions ainsi que l�abaissement du taux l�gal d�alcool�mie de 0,5 g/l � 0,2 g/l pour les nouveaux conducteurs (dur�e de 3 ans apr�s le passage du permis, 2 ans si conduite accompagn�e).
