TITRE: Philip Glass composera la musique des �Quatre Fantastiques� - 20minutes.fr
DATE: 27-01-2015
URL: http://www.20minutes.fr/cinema/1527127-20150127-philip-glass-composera-musique-quatre-fantastiques
PRINCIPAL: 0
TEXT:
Cin�ma
Le r�alisateur du reboot de The Fantastic Four, Josh Trank, a confirm� la nouvelle:� Philip Glass composera la�musique de son film . Il faut se pincer et relire deux ou trois fois l�information pour bien l�assimiler. Philip Glass, compositeur minimaliste, est en charge de la musique de l�un des blockbusters les plus attendus de l�ann�e.
Un an de travail
Josh Trank s�en est expliqu� en racontant �tre un fan de longue date de Philip Glass:��J�ai eu son num�ro gr�ce � son manager. Je lui ai envoy� mon pr�c�dent film, Chronicle, puis je l�ai appel�. C��tait le coup de fil le plus cool de ma vie parce que, putain, c�est Philip Glass et qu�il avait regard� mon film.�
Le r�alisateur et le compositeur, qui a �t� charm� par le travail �tr�s philosophique� de Josh Trank ,�ont travaill� ensemble durant un an. Il s�agira de la premi�re musique de film de super-h�ros pour Philip Glass, plus habitu� depuis ses d�buts, dans les ann�es 1950, aux op�ras et spectacles de danse contemporaine .�Il a tout de m�me �t� nomm� plusieurs fois aux Oscars et a m�me re�u une statuette pour sa partition dans The Truman Show.
