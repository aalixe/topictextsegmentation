TITRE: 8emes de finale : La leçon des Experts, Tous les sports
DATE: 27-01-2015
URL: http://www.lesechos.fr/sport/omnisport/sports-705853-8emes-de-finale-la-lecon-des-experts-1087080.php
PRINCIPAL: 1228864
TEXT:
8emes de finale : La le�on des Experts
Le 26/01 � 20:30
8emes de finale : La le�on des Experts
1 / 1
Faciles vainqueurs de l'Argentine, 33-20 lundi soir, les Fran�ais se sont qualifi�s sans trembler pour les quarts de finale du championnat du Monde de handball. A l'issue d'une v�ritable d�monstration de force, la France lance v�ritablement sa comp�tition, au d�triment des Argentins.
Les joueurs de Claude Onesta ne pouvaient pas r�ver meilleure mani�re de lever les doutes et de rentrer de plein pied dans cette deuxi�me phase de comp�tition. Face � des Argentins totalement d�pass�s, les Experts n'ont pas fait dans le d�tail, en livrant la performance la plus accomplie de leur Mondial. Un moyen id�al de pr�parer au mieux le quart de finale qui se profile contre la Slov�nie.�
Une d�fense retrouv�e
En difficult�s sur les phases d�fensives depuis leur entr�e dans la comp�tition, la France s'est cette fois-ci montr�e parfaite dans ce domaine. Emmen�s par un Thierry Omeyer imp�rial (9 arr�ts sur les 15 tirs argentins en premi�re p�riode), les Bleus ont �touff� l'attaque argentine (seulement 6 buts inscrits en 30 minutes par les Gladiadores) gr�ce � une organisation bien huil�e et une agressivit� retrouv�e. Une solidit� en d�fense qui permet rapidement aux partenaires de Valentin Porte (homme du match avec 6 buts) de se d�tacher. La France m�ne 4-1 apr�s moins de 10 minutes de jeu, une avance qui ne va faire que s'accentuer au fil du match. En manque d'inspiration et visiblement entam�s physiquement, les Argentins se r�v�lent impuissants face � la machine fran�aise. A l'issue d'une premi�re p�riode parfaitement ma�tris�e, les Experts m�nent de 10 buts, alors que l'�cart aurait pu �tre encore plus important. 16-6 � la pause pour les Tricolores.
Un r�cital collectif
Dans un match presque d�j� pli�, les Fran�ais continuent de d�rouler leur handball en seconde p�riode. Claude Onesta peut m�me effectuer une large revue d'effectif dans le deuxi�me acte. Des rotations payantes, puisque tous les joueurs de champ de l'�quipe de France ont au moins inscrit un but. Pendant que les cadres soufflent, les rempla�ants font le boulot de fort belle mani�re et l'�cart se creuse inexorablement. M�me ceux qui n'avaient pratiquement pas eu de temps de jeu, comme Igor Anic ou Mathieu Gr�bille, apportent leur pierre � l'�difice. L'Argentine, elle, a toujours autant de mal, m�me si les Sud-Am�ricains ont un peu plus de r�ussite au tir apr�s la pause (la sortie de Thierry Omeyer aidant). Score final 33-20 au terme d'un cavalier seul de 60 minutes. S�chement battus et �limin�s, les Argentins pourront tout de m�me se consoler en se rappelant que ce huiti�me de finale repr�sente le meilleur r�sultat de l'histoire de leur nation en championnat du Monde.
Place � la Slov�nie
Apr�s une 13eme victoire en 13 confrontations contre l'Argentine, les Bleus vont maintenant devoir se concentrer sur leurs adversaires en quarts de finale, la Slov�nie. Une rencontre qui s'annonce d�j� plus compliqu�e, face � des Slov�nes qui connaissent tr�s bien les Fran�ais. Mais, s'ils conservent la m�me intensit� et la m�me application que lors de ce huiti�me de finale, les Experts ont �videmment toutes leurs chances de se qualifier pour les demies de ce Mondial au Qatar. R�ponse mercredi, � 19h.
�
�
CHAMPIONNAT DU MONDE DE HANDBALL 2015 (H)
Du 15 janvier au 1er f�vrier au Qatar (tous les matchs se jouent � Doha, dans trois salles diff�rentes)
HUITIEMES DE FINALE
