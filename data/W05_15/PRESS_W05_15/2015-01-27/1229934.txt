TITRE: Accus� du viol de ses enfants handicap�s mentaux, un homme est acquitt� - DH.be
DATE: 27-01-2015
URL: http://www.dhnet.be/actu/faits/accuse-du-viol-de-ses-enfants-handicapes-mentaux-un-homme-est-acquitte-54c7d00935701001a165fa66
PRINCIPAL: 1229929
TEXT:
Accus� du viol de ses enfants handicap�s mentaux, un homme est acquitt�
Belga Publi� le
mardi 27 janvier 2015 � 18h53
- Mis � jour le
mardi 27 janvier 2015 � 18h55
...
Faits divers
Le proc�s avait lieu � Louvain.
Le tribunal correctionnel de Louvain a acquitt� mardi un homme de 49 ans de Kessel-Lo poursuivi pour le viol de ses enfants. L'homme �tait accus� par son ex-femme du viol de ses deux fils durant plusieurs ann�es.�
Les faits se seraient produits entre 2006 et 2011. Le pr�venu avait clam� son innocence lors d'une pr�c�dente audience. "J'avais un bon mariage, jusqu'� ce que mes deux beaux-fr�res viennent en Belgique. Ils disaient partout que j'�tais une mauvaise personne. Je n'ai plus de contact avec mes enfants depuis 2011. Le plus �g� souffre d'autisme, les deux autres ont des probl�mes mentaux", a-t-il pr�cis�.
L'homme a �t� acquitt� par manque de preuves.
Sur le m�me sujet :
