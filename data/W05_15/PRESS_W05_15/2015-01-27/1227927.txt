TITRE: ESPAGNE. ??La chute d'un F-16 grec fait onze morts | Courrier international
DATE: 27-01-2015
URL: http://www.courrierinternational.com/une/2015/01/27/la-chute-d-un-f-16-grec-fait-onze-morts
PRINCIPAL: 1227920
TEXT:
Economie. L�accord entre l�Eurogroupe et la Gr�ce r�sum� en cinq points
"La plus grande trag�die a�rienne militaire depuis trente ans". C'est ainsi que le quotidien conservateur ABC qualifie l'accident d'avion qui a co�t� la vie � deux pilotes grecs et neuf soldats fran�ais pr�s d'Albacete, dans le sud-est de l'Espagne.
L'�paisse colonne de fum�e qui se d�gage de la base militaire de l'Otan apr�s la chute de l'avion de chasse F-16 lors d'un exercice fait la une de presque toute la presse espagnole qui s'inqui�te du sort de la vingtaine de bless�s, dont quatre se trouvent dans un �tat critique.
Source
