TITRE: Cinq arrestations au cours d'une op�ration anti-djihadistes en France - rts.ch - Monde
DATE: 27-01-2015
URL: http://www.rts.ch/info/monde/6492548-cinq-arrestations-au-cours-d-une-operation-anti-djihadistes-en-france.html
PRINCIPAL: 1228125
TEXT:
le 27 janvier 2015
Une op�ration anti-djihadistes s'est d�roul�e t�t mardi au centre de Lunel, dans le sud de la France. Cinq personnes, soup�onn�es de recruter des jeunes, ont �t� interpell�es, selon la police.
Le Raid et le GIPN ont d�clench� une op�ration anti-djihadistes depuis 6h30 � Lunel, dans l'H�rault, a fait savoir mardi la police, confirmant une information du Midi Libre .
Cinq personnes impliqu�es dans des fili�res de recrutement ont �t� plac�es en garde � vue, dont deux revenaient de Syrie. L'op�ration est termin�e mais plusieurs quartiers restaient boucl�s mardi matin.
Une vingtaine de jeunes de cette ville de 26'000 habitants sont partis depuis l'�t� dernier pour faire le djihad en Syrie et en Irak. Six d'entre eux, �g�s de 18 ans � 30 ans, sont morts depuis octobre.
Le maire de Lunel a r�agi sur France Bleu H�rault.
"Avec cette op�ration anti-jihad, c'est un d�but de r�ponse sur les d�parts de jihadistes que l'on aura" Claude Arnaud, le maire de #Lunel
� France Bleu H�rault (@bleuherault) January 27, 2015
Op�ration pr�vue d�but janvier
Ce coup de filet �tait pr�vu depuis d�but janvier mais il avait �t� report� en raison des attentats de Paris, poursuit le Midi Libre.
Une femme qui se tenait derri�re la porte lors de la perquisition de la police a �t� bless�e, selon la m�me source. Elle a �t� emmen�e aux urgences.
vtom
Trois interpellations en Belgique
Trois personnes de la mouvance djihadiste ont �t� interpell�es dans la nuit de lundi � mardi dans la r�gion de Courtrai, en Belgique.
Ces interpellations ont eu lieu � Harelbeke (ouest), � une vingtaine de kilom�tres de la fronti�re fran�aise, a pr�cis� le parquet de Courtrai, cit� par l'agence Belga.
Aucune information sur l'identit� des personnes arr�t�es ou leurs liens �ventuels avec d'autres djihadistes pr�sum�s n'ont �t� rendus publics.
Lunel, dans le sud de la France
A consulter �galement
