TITRE: D�couvrez Rib�ry sans cicatrice - Football - Sports.fr
DATE: 27-01-2015
URL: http://www.sports.fr/football/allemagne/articles/ribery-sans-cicatrice-c-est-possible-!-1174903/?features3home
PRINCIPAL: 1230092
TEXT:
Franck Ribéry photoshopé. (Youtube)
Par La rédaction
27 janvier 2015 � 15h40
Mis à jour le
27 janvier 2015 � 20h16
Franck Ribéry sans cicatrice, un photographe professionnel curieux de découvrir le visage du joueur du Bayern sans les stigmates de son accident a choisi d’en faire la démonstration via photoshop.
Victime d’un grave accident de la route quand il était enfant, Franck Ribéry porte sur son visage les stigmates de ce drame.
Des cicatrices que le photographe Robson Lami Brito s’est chargé d’estomper via le célèbre logiciel de retouche Photoshop. Pour un rendu bluffant !
  
