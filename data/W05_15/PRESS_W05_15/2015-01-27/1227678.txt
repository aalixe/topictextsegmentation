TITRE: Une fess�e qui fait du bien - Handball - Sports.fr
DATE: 27-01-2015
URL: http://www.sports.fr/handball/mondial/articles/les-bleus-impitoyables-!-1174624/
PRINCIPAL: 1227674
TEXT:
26 janvier 2015 � 20h22
Mis à jour le
27 janvier 2015 � 09h00
Après une phase de poules en demi-teinte, l'équipe de France a passé la vitesse supérieure en huitièmes de finale du championnat du monde. Les Bleus ont étripé l'Argentine (33-20), dans une rencontre pliée dès la pause. Ils retrouveront la Slovénie en quarts de finale.
"C’est une équipe bien plus faible que nous. Je n’ose même pas imaginer une seconde qu’on puisse perdre ce soir. Perdre ce soir ne serait pas une erreur, ce ne serait pas une faute, ce serait une grossièreté." Lors de l’entraînement du matin, Claude Onesta avait décidé de mettre ses joueurs devant leurs responsabilités. Après un premier tour mal maîtrisé, les Bleus se devaient d’entrer dans la phase éliminatoires en marquant le coup, face à une équipe d’Argentine battue 33-19 en phase de préparation. Vu la fessée infligée à la troupe de Diego Simonet (33-20), le message est semble-t-il bien passé. 
D’entrée, les "Gladiateurs" se sont retrouvés la tête dans le seau, incapables de bousculer la défense tricolore, et encore moins de tromper un Thierry Omeyer de gala (8-3, 17e). De l’autre côté, les Tricolores offrent un récital, avec (enfin) de jolis mouvements collectifs. A la pause, l’écart est du coup déjà abyssal (16-6, 30e), et les Argentins ont le moral dans les chaussettes. Après un tournoi franchement surprenant au sein d’une poule difficile (Danemark, Allemagne, Pologne, Russie), les Argentins ne ressemblent alors plus qu’à de jeunes pousses recevant leur leçon. 
Une leçon qui va s’éterniser une période de plus puisque contrairement au premier tour, l’équipe de France avait décidé de jouer soixante minutes sur le même ton. Profitant des errances défensives adverses, les Tricolores se régalent et font grossir l’écart (25-11, 45e). De quoi se permettre de lancer les seconds couteaux dans de bonnes conditions, et d’ainsi regonfler leur moral, à l’image de Mathieu Grebille (2 buts), dans une rencontre où tous les joueurs de champ ont marqué. Le curseur confiance est désormais au plus haut, et les Bleus lancés dans leur tournoi. La Slovénie, adversaire des Tricolores en quarts de finale, est prévenue…
