TITRE: Peugeot RCZ R Bimota : 304 ch
DATE: 27-01-2015
URL: http://www.turbo.fr/actualite-automobile/751365-peugeot-rcz-bimota-304-ch/
PRINCIPAL: 1226985
TEXT:
> Peugeot RCZ R Bimota : 304 ch
Peugeot RCZ R Bimota : 304 ch
Voir plus de photos
Dot� de 270 ch de s�rie, le Peugeot RCZ R franchit le cap des 300 ch via l?�dition sp�ciale d�velopp�e par la marque de motos Bimota.
Peugeot s'est associ� au constructeur italien de motos Bimota pour donner naissance � une �dition sp�ciale du RCZ R , baptis�e PB104 en r�f�rence aux initiales des deux entit�s. De s�rie, le coup� sportif au Lion d�veloppe 270 chevaux pour un 0 � 100 km/h abattu en 5,9 secondes.
Le mod�le d�velopp� par Peugeot et Bimota  revendique pas moins de 304 chevaux gr�ce � une reprogrammation de l'ECU du 1.6 THP et l'introduction d'un nouvel �chappement. Ses performances n'ont pas communiqu�es, mais elles promettent avec un freinage optimis� et des suspensions retravaill�es.
D�voil� lors de la Motor Bike Expo de V�rone, le RCZ R PB104 attire le regard avec sa teinte de carrosserie partag�e entre blanc et rouge � l'exception du toit et des r�troviseurs qui restent noirs. Au sein de l'habitacle,  on rel�ve la pr�sence massive d'Alcantara, sur la sellerie et la planche de bord ainsi que l'arriv�e d'un syst�me de navigation TomTom et de deux cam�ras GoPro.
Vid�o : le 0 � 100 km/h � bord du Peugeot RCZ R
SUR LE M�ME TH�ME :
