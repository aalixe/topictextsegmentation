TITRE: 150 millions de dollars pour les six principaux dirigeants d'Apple
DATE: 27-01-2015
URL: http://www.linformatique.org/150-millions-de-dollars-pour-les-six-principaux-dirigeants-dapple/
PRINCIPAL: 0
TEXT:
Accueil / Technologie / 150 millions de dollars pour les six principaux dirigeants d�Apple
150 millions de dollars pour les six principaux dirigeants d�Apple
Emilie Dubois 27/01/2015 Technologie
Par le biais d�un document transmis au r�gulateur am�ricain de la finance, on apprend que l�ensemble des r�mun�rations des six principaux dirigeants d�Apple s�est mont� � 150 millions de dollars en 2014.
C�est par l�interm�diaire d�un document Cupertino transmis au r�gulateur am�ricain de la finance par Apple que l�on apprend que les r�mun�rations des principaux dirigeants de la marque � la pomme ont explos� en 2014. Pour l�ensemble des six principaux cadres de l�entreprise, la firme leur a vers� 150 millions de dollars l�ann�e derni�re.
Par rapport � sa r�mun�ration de 4 252 727 dollars touch�e en 2013, Tim Cook a re�u 9 222 638 dollars en 2014, soit plus du double. Sa r�mun�ration se compose de 1 748 462 dollars de salaire de base auquel il faut notamment ajouter un bonus de 6 700 000 dollars de bonus.
La r�mun�ration de Tim Cook n�est pas la plus haute d�Apple vu qu�Angela Ahrendts a pour sa part re�u 73 351 124 dollars. Si son salaire de base n�est que de 411 538 dollars, ce sont les 70 001 196 dollars d�actions du groupe qui font exploser son salaire.
Luca Maestri, le directeur financier d�Apple, a �galement �t� mieux r�mun�r� que Tim Cook. Il a touch� 14 002 801 dollars, dont 11 335 043 en actions. Il en va de m�me pour Eddy Cue et Jeff Williams, respectivement vice-pr�sident responsable d�internet et vice-pr�sident responsable des op�rations, qui ont touch� 24 445 739 dollars et 24 403 235 dollars.
