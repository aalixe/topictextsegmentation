TITRE: Jacques Julliard : ��Les Fran�ais ne vont pas imiter les Grecs��
DATE: 27-01-2015
URL: http://www.lemonde.fr/politique/article/2015/01/27/jacques-julliard-les-francais-ne-vont-pas-imiter-les-grecs_4564265_823448.html
PRINCIPAL: 0
TEXT:
| Propos recueillis par Nicolas Chapuis
Le dimanche �lectoral en Gr�ce, qui a vu la large victoire du parti de gauche radicale Syriza, a rebattu les cartes sur la sc�ne europ�enne et fran�aise. Le nouveau premier ministre grec, Alexis Tsipras, s�est entretenu d�s lundi 26 janvier avec Fran�ois Hollande pour discuter de ses projets de ren�gociation de la dette de son pays. L�historien Jacques Julliard, qui a publi� en 2012 Les Gauches fran�aises : 1762-2012 : Histoire, politique et imaginaire (Flammarion), analyse les cons�quences potentielles en France de la perc�e de Syriza.
Quels effets la victoire de Syriza peut-elle avoir sur le rapport de forces � gauche en France ?
En termes �lectoraux, je ne crois pas qu�il y aura de cons�quences directes. Il est tr�s rare qu�une �lection en France soit d�termin�e par des questions de politique ext�rieure. Il faudra surveiller les r�sultats de l��lection l�gislative partielle � Montb�liard, le week-end prochain. Mais je ne crois pas que les Fran�ais vont se prendre soudainement � imiter les Grecs.
La gauche radicale en France peut-elle esp�rer tirer profit de la victoire de Syriza ?
Pour qu�il y ait ce que Jean-Luc M�lenchon appelle un ��effet domino��, il faudrait qu�il y ait des situations comparables. Or, si la France a souffert de la crise, c�est dans des proportions qui n�ont rien � voir avec la Gr�ce. La France, en dehors de ses ch�meurs, n�a pas p�ti de l�aust�rit� sous la forme d�une diminution brutale de son niveau de vie.
En revanche, sur un plan strat�gique, avant m�me le succ�s...
L�acc�s � la totalit� de l�article est prot�g� D�j� abonn� ? Identifiez-vous
Jacques Julliard : ��Les Fran�ais ne vont pas imiter les Grecs��
Il vous reste 76% de l'article � lire
