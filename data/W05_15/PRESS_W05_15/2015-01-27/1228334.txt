TITRE: 700 médicaments génériques bientôt retirés de la vente ?
DATE: 27-01-2015
URL: http://www.pratique.fr/actu/700-medicaments-generiques-bientot-retires-vente-101478.html
PRINCIPAL: 1228331
TEXT:
700 médicaments génériques bientôt retirés de la vente ?
Article mis à jour le
27 janvier 2015
A-
Imprimer
Plusieurs centaines de m�dicaments g�n�riques seraient sur le point d��tre interdits � la vente en Europe, suite � une recommandation de l�Agence europ�enne du m�dicament (EMA). Une d�cision qui d�coulerait d�une inspection men�e par l�Agence fran�aise de s�curit� du m�dicament (ANSM) au sein des laboratoires GVK Biosciences � Hyderabad (Inde).
Pr�s de 700 m�dicaments g�n�riques pourraient prochainement se voir retir�s de la vente en Europe, comme l�a d�cid� l�EMA. En cause�: une inspection r�alis�e par l�ANSM dans les laboratoires indiens de la soci�t� GVK Biosciences ayant mis au jour des manipulations de donn�es. � noter que les m�dicaments concern�s ne mettraient pas la sant� du patient en danger.
Des donn�es d��lectrocardiogrammes modifi�es
Ainsi, l�Agence fran�aise de s�curit� du m�dicament (ANSM) aurait rep�r� des manipulations de donn�es d��lectrocardiogrammes au cours d��tudes portant sur des m�dicaments g�n�riques. Des irr�gularit�s pratiqu�es pendant au moins cinq ans, selon l�Agence europ�enne du m�dicament (EMA). Rappelons que ces b�vues avaient d�abord �t� r�v�l�es en d�cembre 2014. Ce qui avait alors entra�n� la mise en �uvre d�une proc�dure de suspension de 25 m�dicaments g�n�riques propos�s en France , dont l�Ibuprof�ne.
La s�curit� des m�dicaments concern�s ne serait pas en cause
Dor�navant, l�ANSM souligne que les tests en question ne sont pas indispensables pour d�montrer que l�action th�rapeutique d�un g�n�rique est �quivalente � celle du m�dicament de r�f�rence. N�anmoins, les anomalies relev�es sont la preuve du non respect des pratiques cliniques recommand�es. Pour cette raison, les analyses et �tudes men�es par GVK Bio ne seront plus prises en compte.
Ainsi, si la s�curit� et l�efficacit� des m�dicaments en question ne se posent pas, 700 g�n�riques sont touch�s par la recommandation europ�enne. Nombre d�entre eux seront donc retir�s de la vente, exception faite de ceux indispensables aux patients et pour lesquels il n�existe pas assez d�alternatives.
Comme l�a indiqu� l�EMA, ce sera aux �tats membres de l�UE de d�terminer la r�ponse � donner au probl�me. De son c�t�, l�ANSM n�avait pas attendu la d�cision de la Commission pour suspendre des m�dicaments en janvier. Et depuis vendredi, huit autres sont venus s�ajouter � la liste, dont le Ropinorol (substance prescrite notamment pour lutter contre la maladie de Parkinson) ou encore un antiviral nomm� Aciclovir. La liste compl�te est disponible ici .
