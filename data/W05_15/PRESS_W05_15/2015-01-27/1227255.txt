TITRE: Un chewing-gum peut �liminer 100 000 bact�ries dans la bouche | Medisite
DATE: 27-01-2015
URL: http://www.medisite.fr/a-la-une-un-chewing-gum-peut-eliminer-100-000-bacteries-dans-la-bouche.786778.2035.html
PRINCIPAL: 1227254
TEXT:
� Un chewing-gum peut �liminer 100 000 bact�ries dans la bouche
Un chewing-gum peut �liminer 100 000 bact�ries dans la bouche
Selon une �tude hollandaise un chewing-gum peut retenir 100 000 bact�ries issues de la bouche, � condition de ne pas le m�cher trop longtemps !�
Publicit�
Le chewing-gum peut-il remplacer les bains de bouche ? Selon une �tude hollandaise, m�cher un chewing-gum pendant 10 minutes peut nettoyer la bouche de 100 000 bact�ries. Une estimation d�couverte en analysant le contenu de gommes m�ch�es par cinq volontaires. Attention ! Selon les chercheurs, seuls les chewing-gums sans sucre ont cet effet, autrement le sucre nourrit les bact�ries qui risquent de prolif�rer.
M�ch� trop longtemps il ne retient plus les bact�ries
Publicit�
Au cours de leur �tude, les chercheurs ont remarqu� que plus le chewing-gum est m�ch� plus il perd de son efficacit� antibact�rienne. "A force sa structure change et les bact�ries qui �taient retenues dans la gomme retournent dans la bouche ", expliquent les chercheurs. Maintenant, le groupe d'�tude esp�re cr�er des chewing-gums qui pourraient retenir certains types de bact�ries responsables d'infections de la bouche.
Publi� par B�n�dicte Demmer, r�dactrice sant� le Lundi 26 Janvier 2015 : 15h34
Vid�os
