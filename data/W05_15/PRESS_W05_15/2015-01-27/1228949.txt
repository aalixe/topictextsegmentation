TITRE: La communauté kurde en liesse après la défaite de l'État islamique à Kobané
DATE: 27-01-2015
URL: http://www.lefigaro.fr/photos/2015/01/27/01013-20150127ARTFIG00292-la-communaute-kurde-en-liesse-apres-la-defaite-de-l-etat-islamique-a-kobane.php
PRINCIPAL: 0
TEXT:
Envoyer par mail
La communauté kurde en liesse après la défaite de l'État islamique à Kobané
EN IMAGES - Dans plusieurs villes de Turquie et de Syrie, les Kurdes sont descendus dans la rue pour célébrer la victoire des leurs à Kobané, où ils ont mis en déroute les combattants de Daech.
< Envoyer cet article par e-mail
X
Séparez les adresses e-mail de vos contacts par des virgules.
De la part de :
EN IMAGES - Dans plusieurs villes de Turquie et de Syrie, les Kurdes sont descendus dans la rue pour célébrer la victoire des leurs à Kobané, où ils ont mis en déroute les combattants de Daech.
J'accepte de recevoir la newsletter quotidienne du Figaro.fr
Oui
