TITRE: D�couverte des plus vieilles plan�tes de notre galaxie - Loisirs et culture - Le Nouvelliste Online - Quotidien Valaisan
DATE: 27-01-2015
URL: http://www.lenouvelliste.ch/fr/societe/loisirs-et-culture/decouverte-des-plus-vieilles-planetes-de-notre-galaxie-475-1406179
PRINCIPAL: 1230018
TEXT:
27.01.2015, 20:40 - Loisirs et culture
Actualis� le 27.01.15, 20:39
D�couverte des plus vieilles plan�tes de notre galaxie
Espace
Cette nouvelle d�couverte va permettre aux chercheurs d'avancer sur la compr�hension de notre galaxie et sur la possibilit� de trouver une plan�te similaire � la Terre.
Cr�dit: KEYSTONE
Tous les commentaires (0)
Des astronautes internationales ont d�couvert le plus vieux syst�me plan�taire connu � ce jour dans notre galaxie. Ces 5 plan�tes comparables � la taille de la Terre sont �g�es de 11,2 milliards d'ann�es.
"Nous n'avons jamais rien vu de semblable: une �toile aussi vieille avec un grand nombre de petites plan�tes qui la rend tr�s sp�ciale", a soulign� Daniel Huber de l'Universit� de Birmingham au Royaume-Uni, un des co-auteurs de cette d�couverte faite avec le t�lescope Kepler et publi�e mardi dans la revue am�ricaine "The Astrophysical Journal".
Ce syst�me plan�taire est baptis� Kepler-444. Son �toile est une "naine orange", 25% plus petite que notre Soleil et donc moins chaude. Elle compte cinq plan�tes dont le diam�tre varie de 0,4 � 0,7 fois celui de la Terre, soit des tailles allant de celle de Mercure � celle de V�nus.
Proches de leur �toile
Kepler-444 se situe � 117 ann�es-lumi�re de notre plan�te (une ann�e-lumi�re �quivaut � 9.461 milliards de kilom�tres) et il s'agit du plus ancien syst�me plan�taire de notre galaxie, la Voie Lact�e, connu � ce jour.
Les plan�tes sont tr�s proches de leur �toile. Ainsi Kepler-444f, la plus �loign�e, en fait le tour en 9,7 jours et la plus proche, Kepler-444b, en 3,6 jours. A ces distances, les exoplan�tes sont toutes beaucoup plus chaudes que Mercure et ne sont pas habitables, soulignent les astronomes.
Vie possible?
"Nous voyons que des plan�tes de taille terrestre se sont form�es pendant quasiment toute l'histoire de l'Univers n� il y a 13,8 milliards d'ann�es, ouvrant la possibilit� de l'existence de vie ancienne dans la galaxie", a comment� Tiago Campante, de l'Universit� de Birmingham, le principal auteur de ces travaux.
"Cette d�couverte ouvre la voie � une plus grande compr�hension de la formation des premi�res plan�tes dans la galaxie (...) ce qui nous rapproche du Graal des astronomes, � savoir d�couvrir une exo-plan�te de m�me taille que celle de la Terre en orbite d'une ann�e autour d'une �toile jumelle de notre Soleil", a comment� le professeur Huber.
Source: ATS
