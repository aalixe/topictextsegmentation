TITRE: Am�riques - En images : une temp�te de neige transforme New York en ville fant�me - France 24
DATE: 27-01-2015
URL: http://www.france24.com/fr/20150127-images-new-york-neige-ville-fantome-alerte-blizzard-diaporama-tempete/
PRINCIPAL: 1228589
TEXT:
Texte par FRANCE 24 Suivre france24_fr sur twitter
Derni�re modification : 27/01/2015
Une temp�te de neige s'abat depuis lundi sur la ville de New York, transform�e en ville fant�me. La circulation a �t� interdite et les habitants de la ville ont �t� pri�s de rester chez eux.
New york, la ville qui ne dort jamais, �tait �trangement calme mardi 27 janvier. Au deuxi�me jour d' une temp�te de neige , qui a paralys� la Big Apple et d�autres grandes villes de la c�t� est, des millions d'Am�ricains sont rest�s calfeutr�s chez eux, tandis que des milliers de vols ont �t� annul�s.
New York sous la neige
La neige a transform� certaines art�res de New York en ville fant�me.
� Jewel Samad, AFP
Le m�tro new-yorkais d�sert� par ses usagers mardi matin.
� Jewel Samad, AFP
Un important dispositif a �t� d�ploy� dans les rues de la ville pour d�gager les voies d'acc�s.
� Alex Trautwig, AFP
Certains New-yorkais ont bien �t� oblig�s de braver les intemp�ries pour aller travailler.
� Jewel Samad, AFP
De jeunes Texans profitent de la m�t�o pour faire une bataille de neige dans les rue de la ville.
� Jewel Samad, AFP
La neige tombe sur la statue de George M. Cohan situ�e � Times Square.
� Jewel Samad, AFP
Un homme a chauss� une paire de ski pour se d�placer � Times Square.
� Alex Trautwig, AFP
Des passagers consultent, inquiets, les bulletins m�t�o pour suivre l'�tat du trafic a�rien.
� Bruce Bennett, AFP
L'accumulation de neige semblait, � l'approche de l'aube, un peu plus l�g�re que pr�vue dans certaines zones, selon les m�dias locaux, mais la temp�te �tait pr�vue pour durer encore toute la journ�e de mardi.
� New York, la circulation a �t� interdite par les autorit�s qui ont recouru aux grands moyens. Des centaines de chasse-neige et employ�s municipaux ont �t� mobilis�s, et le maire Bill de Blasio a appel� la population � sortir le moins possible. Il a annonc� que les �coles resteraient ferm�es mardi. "Ce pourrait �tre une question de vie ou de mort, et ce n'est pas dramatiser que de le dire, il faut �tre prudent", a de son c�t� d�clar� le gouverneur de l'�tat de New York, Andrew Cuomo.
Lundi soir, les ponts et tunnels ont �t� ferm�s, le m�tro et les bus arr�t�s, et plus personne n'avait le droit de circuler dans la ville, sauf les v�hicules d'urgence, comme les ambulances ou les chasse-neige.
L'�tat d'urgence a �t� d�cr�t� dans sept �tats du nord-est et plus de 7 100 vols ont �t� annul�s lundi et mardi, les chutes de neige s'accompagnant de bourrasques de vent pouvant d�passer les 110 km/h.
Avec AFP
