TITRE: Enqu�te sur le crash d'un F-16 grec qui a fait onze morts en Espagne | Site mobile Le Point
DATE: 27-01-2015
URL: http://www.lepoint.fr/monde/crash-d-un-f-16-grec-en-espagne-huit-francais-et-deux-grecs-tues-27-01-2015-1899852_24.php
PRINCIPAL: 1227259
TEXT:
27/01/15 � 06h24
Enqu�te sur le crash d'un F-16 grec qui a fait onze morts en Espagne
Les ministres de la d�fense fran�ais Jean-Yves Le Drian (g) et espagnol Pedro Morenes (d) en conf�rence de presse � la base militaire d'Albacete le 27 janvier 2015 AFP - Jose Jordan
L'enqu�te a commenc� mardi pour d�terminer les causes de l'accident d'un F-16 sur une base militaire du sud-est de l'Espagne au cours duquel sont morts neuf Fran�ais et deux Grecs, pendant des manoeuvres multinationales.
Les ministres fran�ais et espagnol de la D�fense Jean-Yves Le Drian et Pedro Morenes se sont recueillis mardi soir sur le site de l'accident  de l'avion de combat, peu avant 19h00 (18H00 GMT).
Ils sont rest�s debout, silencieux, entre deux hangars de la base de Los Llanos, � quelque 250 km au sud-est de Madrid, non loin du site o� se trouvaient toujours les d�pouilles des victimes.
"Dans la vie d'un ministre de la D�fense, il y a certaines journ�es qui marquent de mani�re d�finitive", a d�clar� M. Le Drian devant quelque 70 militaires fran�ais aux visages ferm�s, en pr�sence de son homologue espagnol et du chef d'�tat-major de l'arm�e de l'Air Denis Mercier.
Jean-Yves Le Drian �tait arriv� deux heures plus t�t, soit 24 heures apr�s le drame qui a tu� les aviateurs fran�ais et deux pilotes grecs, et a fait 21 bless�s (neuf Fran�ais et douze Italiens).
Les militaires survivants sont "secou�s, traumatis�s, tristes, je voulais leur t�moigner de mon soutien", a-t-il dit � la presse sur place.
Sept des victimes fran�aises sur neuf venaient de la base 133 de Nancy, dans l'est de la France, et suivaient un cours de perfectionnement. La BA 133, o� une cellule de crise a �t� mise en place, d�ploie actuellement des avions au Niger, pour des op�rations au Sahel, et en Jordanie, pour des op�rations en Irak.
Au d�collage, le F-16 "a d�vi� de sa route, tr�s nettement, de 90 degr�s, a percut� les avions fran�ais qui s'appr�taient � d�coller", a indiqu� une source proche du ministre fran�ais en �cartant cependant toute cause "ext�rieure", alors que la France est encore traumatis�e par les attaques jihadistes qui ont fait 17 morts � Paris entre les 7 et 9 janvier.
"Les avions �taient pleins de k�ros�ne et il y avait beaucoup de monde autour", ce qui explique le bilan tr�s �lev� de l'accident.
Au total, l'arm�e fran�aise a vu quatre de ses appareils d�truits ou endommag�s: deux Mirage 2000D et deux Alpha Jet.
Une femme capitaine et navigatrice figure parmi les morts, selon le minist�re fran�ais.
Selon un dernier bilan fourni par le ministre espagnol, cinq fran�ais sont encore hospitalis�s, sur les neuf bless�s. Deux pourraient rentrer en France d�s mercredi. Trois militaires bless�s plus graves restent hospitalis�s � Madrid.
Sur les douze Italiens bless�s, un reste hospitalis� et doit subir une intervention mardi.
En Gr�ce, un deuil a �t� d�cr�t� au sein des forces arm�es.
- Substance toxique -
Sur la base, des enqu�tes technique et judiciaire ont commenc� afin de d�terminer comment le F-16 grec a soudainement perdu de la puissance au d�collage lundi � 15h16 (14H16 GMT).
Cet avion de combat a ensuite heurt�, outre les quatre appareils fran�ais, deux AMX italiens et endeuill� les militaires des sept pays (France, Italie, Gr�ce, Etats-Unis, Allemagne, Royaume-Uni, Espagne) participant � une session de formation.
L'accident s'est produit en plein entra�nement dans ce centre de formation de pilotes d'�lite de dix nationalit�s, o� est mis en oeuvre le "Training Leadership Programme" de l'OTAN, "une des formations les plus r�put�es et les plus exigeantes du monde" selon le minist�re de la D�fense en France.
Sur le plan technique, une commission espagnole d'enqu�te sur les accidents d'a�ronefs militaires devra notamment �tudier les bo�tes noires et analyser les conversations, a pr�cis� un responsable du minist�re de la D�fense.
Deux enqu�teurs fran�ais du Bureau enqu�tes accidents d�fense (BEAD Air) sont �galement sur la base.
L'accident a suscit� une vive �motion en France, o� une minute de silence a �t� observ�e � l'Assembl�e nationale et au S�nat.
C'est pour l'arm�e fran�aise un des bilans les plus lourds en une seule journ�e, depuis une embuscade dans la passe d'Uzbin (est de l'Afghanistan) en 2008, qui avait fait dix morts.
Mardi soir, on pouvait entre-apercevoir un magma de ferraille fondu, les restes d'un Alpha Jet Fran�ais, et un Mirage 2000 � la queue calcin�e.
En fin de journ�e une partie de la base restait encore interdite d'acc�s, selon une journaliste de l'AFP qui a pu aussi observer des hommes s'activant, non loin du quartier g�n�ral du centre de formation, en blouse blanche et prot�g�s par des masques pour �viter une substance tr�s toxique d�gag�e par le F-16 accident�.
27/01/2015 21:54:43 - BASE MILITAIRE DE LOS LLANOS (Espagne) (AFP) - Par Val�rie LEROUX, Anna CUENCA, avec Val�rie Leroux � Paris - � 2015 AFP
