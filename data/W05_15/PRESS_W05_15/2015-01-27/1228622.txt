TITRE: IRFM : quand des d�put�s se font un patrimoine avec leurs frais de mandat | Planet
DATE: 27-01-2015
URL: http://www.planet.fr/politique-irfm-quand-des-deputes-se-font-un-patrimoine-avec-leurs-frais-de-mandat.787336.29334.html
PRINCIPAL: 1228619
TEXT:
Selon les informations de France Tv Info, plusieurs parlementaires se sont servis de leur indemnit� de repr�sentativit� de frais de mandat pour devenir propri�taire. Explications.
� Capture Facebook
En toute opacit�
L�indemnit� repr�sentative de frais de mandat (IRFM), dont l�usage par les �lus et marqu� par le sceau de l�opacit� n�en finit pas d�interroger. Cette enveloppe mensuelle s��l�ve � 6412 euros par mois.
Ce mardi 27 janvier, France tv info sort une enqu�te pointant ces d�put�s soup�onn�s de s��tre constitu� un patrimoine avec cette indemnit� initialement destin�e � "faire face aux diverses d�penses li�es � l'exercice de leur mandat". Si cela n�a pour le moment rien d�ill�gal, certaines pratiques posent des questions d�ontologiques voire morales. Voici donc les parlementaires �pingl�s par nos confr�res.
Fran�ois Sauvadet (UDI)
Publicit�
D�put� centriste de la C�te-d�Or, Fran�ois Sauvadet (ci-dessus) a achet� sa permanence avec l�IRFM plut�t que de la louer. Ce faisant, il se retrouve propri�taire de celle-ci sans avoir eu recours � son indemnit� parlementaire (son salaire en somme). Selon ce dernier, cela profite � la communaut� depuis que le bien est pay� dans la mesure o� cela "permet � [son] IRFM de servir � d�autres usages au service de [ses] concitoyens". Soit.
1
