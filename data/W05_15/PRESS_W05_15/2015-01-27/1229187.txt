TITRE: La France va devoir rendre � l'UE un milliard d'euros d'aides agricoles  - Lib�ration
DATE: 27-01-2015
URL: http://www.liberation.fr/economie/2015/01/27/la-france-va-devoir-rendre-a-l-ue-un-milliard-d-euros-d-aides-agricoles_1189817
PRINCIPAL: 1229173
TEXT:
Un champ de bl�, le 25 juillet 2013 � Civray-sur-Esves (Indre-et-Loire). (Photo Jean-Fran�ois Monier. AFP)
Le remboursement de ces aides est justifi� �pour cause de fraudes et d'erreurs� d'apr�s la Commission europ�enne.
La France va devoir rendre � l�Union europ�enne plus d�un milliard d�euros d�aides agricoles vers�es � tort entre 2008 et 2012 pour cause de fraudes et d�erreurs, a-t-on appris mardi aupr�s de la Commission europ�enne.
Cette somme repr�sente environ 2% des quelque 40 milliards d�euros per�us par la France pour cette p�riode dans le cadre de la Politique agricole commune (PAC). Elle devra �tre rembours�e en trois tranches jusqu�en 2017, a pr�cis� une source europ�enne.
Ce montant a �t� finalis� � l�issue d�une longue p�riode de n�gociations entre Paris et Bruxelles. Au d�part, la Commission europ�enne n�avait pas exclu de r�clamer jusqu�� 1,8 milliard d�euros. Quinze �tats membres sont concern�s par cette demande de remboursement, mais la France �ponge le plus gros de l�ardoise, qui s��tablit au total � 1,45 milliard, selon le d�tail des chiffres publi�s dans le Journal officiel de l�UE.
La France est ainsi p�nalis�e pour ne pas assez v�rifier le bien-fond� des aides vers�es aux agriculteurs, notamment en mati�re d��co-conditionnalit� et de calculs des surfaces agricoles �ligibles. La gestion des fonds agricoles europ�ens et le contr�le de l��ligibilit� de leurs b�n�ficiaires sont r�guli�rement critiqu�s par la Cour des comptes europ�enne. La Commission s��tait engag�e l�ann�e derni�re � redoubler de vigilance.
