TITRE: Quand Geneviève de Fontenay tombe sur des djihadistes - 7SUR7.be
DATE: 27-01-2015
URL: http://www.7sur7.be/7s7/fr/1540/TV/article/detail/2196352/2015/01/27/Quand-Genevieve-de-Fontenay-tombe-sur-des-djihadistes.dhtml
PRINCIPAL: 1228851
TEXT:
27/01/15 - 10h43
� Capture d'�cran.
vid�o Genevi�ve de Fontenay fait l'objet de nombreuses moqueries sur la toile apr�s avoir annul� la participation de ses Miss � un �v�nement par crainte de tomber sur des "djihadistes".
L'ancienne pr�sidente du comit� Miss France, qui a d�sormais son propre concours de beaut� Miss Prestige nationale, �tait attendue avec ses Miss � un d�ner � la mairie de Schiltigheim en Alsace mardi dernier. Mais madame chapeau a subitement tout annul� et a pr�f�r� emmener les jeunes femmes... au McDo.
Dans l'�mission "66 Minutes" diffus�e ce dimanche sur M6, on apprend les raisons de ce changement de programme. Genevi�ve de Fontenay y explique en effet avoir pr�f�r� changer ses plans � cause de "djihadistes". "On a �t� oblig� d'annuler ce repas parce qu'il y a des djihadistes l�-bas", a-t-elle lanc�. "On a vu des djihadistes, on a pr�f�r� ne pas y aller".
Il semblerait en fait que Genevi�ve de Fontenay ait �t� effray�e par un article paru quelques jours plus t�t dans "Les Derni�res Nouvelles d'Alsace" et selon lequel un ancien employ� de la mairie en question �tait parti combattre en Syrie. Cette s�quence pour le moins insolite de "66 Minutes" est d�sormais fortement partag�e sur les r�seaux sociaux.
Lire aussi
