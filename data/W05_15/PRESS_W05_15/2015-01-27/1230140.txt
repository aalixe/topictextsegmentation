TITRE: La justice mexicaine a acquis la certitude de la mort des �tudiants disparus
DATE: 27-01-2015
URL: http://www.romandie.com/news/La-justice-mexicaine-a-acquis-la-certitude-de-la-mort-des-etudiants-disparus/559733.rom
PRINCIPAL: 1230137
TEXT:
Tweet
La justice mexicaine a acquis la certitude de la mort des �tudiants disparus
Mexico - Les autorit�s mexicaines ont acquis la certitude que les 43 �tudiants disparus en septembre dans le sud du Mexique ont �t� assassin�s par des tueurs du crime organis�, a annonc� mardi le ministre de la Justice, Jesus Murillo Karam.
L'enqu�te, qui inclut les d�clarations de pr�s d'une centaine de d�tenus et des �l�ments mat�riels, nous donne la certitude l�gale que les �l�ves enseignants ont �t� tu�s dans les circonstances d�crites, � savoir leur enl�vement par des policiers locaux et leur meurtre par des membres d'un cartel, a d�clar� le ministre au cours d'une conf�rence de presse.
Les aveux de d�tenus et les expertises nous ont permis de faire une analyse logique des causes et de parvenir, sans aucun doute possible, � la conclusion que les �tudiants ont �t� priv�s de leur libert�, puis qu'on leur a �t� la vie, avant de les incin�rer et de les jeter dans la rivi�re San Juan, dans cet ordre, a dit le ministre.
C'est cela la v�rit� historique, a-t-il affirm�.
Jusqu'� pr�sent, les autorit�s judiciaires consid�raient comme disparus les �l�ves enseignants de l'�cole normale d'Ayotzinapa, victimes d'une attaque arm�e de la part de policiers corrompus d'Iguala, ville de l'Etat de Guerrero, puis remis � des tueurs du cartel de trafiquants de drogue des Guerreros Unidos.
Les parents des victimes, qui ont  manifest� lundi avec plusieurs milliers de personnes � Mexico, refusent d'accepter le sc�nario pr�sent� par les autorit�s judiciaires et craignent que le gouvernement ne d�clare clos le dossier de l'affaire qui a horrifi� le Mexique et la communaut� internationale.
Pour le moment, les experts du laboratoire autrichien d'Innsbruck n'ont pu identifier les restes que d'un seul des 43 �tudiants et les proches des victimes affirment qu'ils ont encore l'espoir de retrouver les 42 autres.
(�AFP / 27 janvier 2015 22h10)
�
