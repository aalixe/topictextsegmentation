TITRE: Facebook indisponible: �Plus de panne que de mal�  - Technologies - RFI
DATE: 27-01-2015
URL: http://www.rfi.fr/technologies/20150127-facebook-indisponible-plus-panne-mal-lizard-squad-piratage/?ns_campaign%3Dgoogle_choix_redactions%26ns_fee%3D0%26ns_linkname%3Dtechnologies.20150127-facebook-indisponible-plus-panne-mal-lizard-squad-piratage%26ns_mch
PRINCIPAL: 0
TEXT:
Modifi� le 28-01-2015 � 12:23
Facebook indisponible: �Plus de panne que de mal�
Facebook et sa filiale de partage de photos sont devenus indisponibles vers 6H00 TU, ce mardi 27 janvier, et ce pendant 50 minutes.
REUTERS/Dado Ruvic/Files
Pendant cinquante minutes ce mardi matin, Facebook a �t� inaccessible dans plusieurs pays. Le r�seau social a d�menti toute attaque de cyberpirate, en �voquant un probl�me technique.
�
D�s le d�but de l�incident, de nombreux messages sur Twitter en fran�ais, en anglais, en hindi et en espagnol relayaient la panne, en signalant que le r�seau social, son syst�me de partage de photos Instagram et l�application Tinder, qui permet depuis un Smartphone de r�aliser des rencontres, �taient inaccessibles, non seulement en Europe mais aussi aux Etats-Unis et en Asie.
Facebook dans un communiqu� a voulu faire taire les rumeurs qui ont circul� sur la Toile au sujet d'un �ventuel piratage de ses syst�mes, une attaque qui aurait �t� orchestr�e par le groupe de pirates Lizard Squad. Selon J�r�me Robert, du cabinet de conseil en cybers�curit� Lexsi, le g�ant des r�seaux sociaux ne prendrait pas le risque de nier ce type d�information sans subir en repr�sailles les assauts des hackers du monde entier. ��Le groupe de hacker Lizard Squad n�a pas explicitement revendiqu� le hack, souligne-t-il. On a plut�t une d�claration assez laconique qui constate que le site est "off line". Donc moi j�aurai tendance � croire Facebook, s�ils d�mentent, c�est qu�ils sont assez droits dans leurs bottes.��
� Plus de panne que de mal � donc comme l�indique le communiqu� du r�seau social qui �voque, sans plus de d�tails, un probl�me technique. Cinquante minutes apr�s l�incident, Facebook reconnectait 1,3 milliard d'abonn�s � ses services web dans le monde.
