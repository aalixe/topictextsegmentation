TITRE: La Bourse de Paris reprend son souffle apr�s huit s�ances de hausse (-1,09%) | Site mobile Le Point
DATE: 27-01-2015
URL: http://www.lepoint.fr/bourse/la-bourse-de-paris-reprend-son-souffle-apres-huit-seances-de-hausse-1-09-27-01-2015-1900081_81.php
PRINCIPAL: 1229602
TEXT:
27/01/15 � 18h27
La Bourse de Paris reprend son souffle apr�s huit s�ances de hausse (-1,09%)
La Bourse de Paris a termin� en nette baisse mardi (-1,09%), en raison de prises de b�n�fices apr�s avoir sign� une s�rie de huit s�ances de hausse cons�cutives principalement gr�ce la BCE.
L'indice CAC 40 a perdu 50,92 points � 4.624,21 points, dans un volume d'�changes nourri de 4,5 milliards d'euros. La veille, il avait pris 0,74%.
Parmi les autres march�s europ�ens, Francfort a l�ch� 0,60% et Londres 1,57%. Par ailleurs, l'Eurostoxx 50 a perdu 1,22%.
"Le march� baisse en raison de prises de profits qui sont assez l�gitimes apr�s une s�rie de hausses quasi-historique pour un mois de janvier", observe Renaud Murail, g�rant chez Barclays Bourse.
Le CAC 40 a eu besoin de reprendre son souffle, apr�s avoir bondi de 10,7% lors des huit s�ances pr�c�dentes capitalisant sur les annonces spectaculaires de la BCE.
Ce recul "n'est pas un retour de b�ton ni un changement d'orientation", pr�vient M. Murail.
Le march� parisien s'est montr� h�sitant en d�but de s�ance, avant de se replier nettement, p�nalis� par le recul de Wall Street, qui �tait affect� par des r�sultats d'entreprises d�cevants.
L'annonce par la Banque centrale europ�enne (BCE) d'un vaste programme de rachat d'actifs a m�me permis aux march�s d'accueillir dans le plus grand calme la victoire du parti de gauche radicale Syriza en Gr�ce dimanche.
"Les propos apaisants tenus par plusieurs responsables europ�ens laissent entrevoir une solution sur le dossier grec", le pays pr�voyant de ren�gocier sa dette, remarquent les strat�gistes chez Cr�dit Mutuel-CIC.
"Si une ligne rouge a �t� trac�e pour exclure une r�duction de dette, un r��chelonnement et un all�gement du programme d'aust�rit� paraissent acceptables", selon eux.
La prudence du march� a �galement pu �tre nourrie par la r�union de la banque centrale am�ricaine (Fed) qui se tient mardi et mercredi.
Les investisseurs seront � l'aff�t de d�tails concernant une possible remont�e des taux de la Fed, pr�vue en 2015 compte tenu de la vigueur de l'�conomie am�ricaine, mais qui pourrait �tre perturb�e par la hausse du dollar et la baisse des prix du p�trole.
"Tous ces �l�ments pourraient plaider pour un report de la normalisation de la politique mon�taire de la Fed, plut�t pour la fin de l'ann�e, ce que le march� commence d�j� � int�grer", note M. Murail.
Parmi les valeurs, Tarkett a chut� (-6,82% � 18,92 euros) en raison de son exposition � la Russie, dont la note a �t� rel�gu�e en cat�gorie "sp�culative" par l'agence de notation Standard & Poor's.
Eiffage (-6,34% � 44,60 euros) et Vinci (-3,53% � 47,78 euros) ont fortement recul� alors que le gouvernement a suspendu la hausse des p�ages autoroutiers pr�vue le 1er f�vrier.
Les valeurs industrielles et bancaires ont p�ti des prises de b�n�fices des investisseurs apr�s avoir progress� ces derniers jours.
BNP Paribas a perdu 0,98% � 48,85 euros, Cr�dit Agricole 1,48% � 10,99 euros et Soci�t� G�n�rale 2,30% � 37,23 euros. Par ailleurs, Alcatel-Lucent a l�ch� 1,83% � 3,05 euros et Saint-Gobain 1,54% � 37,73 euros.
GTT (-2,82% � 48,20 euros) a souffert, le fonds H&F Luxembourg ayant annonc� ne plus d�tenir aucune participation dans l'entreprise fran�aise.
Soitec (+1,11% � 0,91 euro) a progress� nettement apr�s avoir pr�sent� un produit haut de gamme destin� aux applications de radiofr�quence.
Arkema a �t� p�nalis� (-3,43% � 60,57 euros) par un abaissement de recommandation par Goldman Sachs.
Metabolic Explorer s'est envol� (+20,77% � 6,28 euros) apr�s avoir obtenu l'autorisation de la FDA, l'autorit� am�ricaine de l'alimentation et du m�dicament, de commercialiser un acide amin� utilis� dans l'alimentation animale.
Enfin, Nextradiotv (+2,44% � 26,00 euros) a �t� galvanis� par une progression de 12% de son activit� au quatri�me trimestre.
jbo/fka/els
