TITRE: Crash d'un F-16 en Espagne: une enqu�te ouverte � Paris | Site mobile Le Point
DATE: 27-01-2015
URL: http://www.lepoint.fr/societe/crash-d-un-f-16-en-espagne-une-enquete-ouverte-a-paris-27-01-2015-1900015_23.php
PRINCIPAL: 1228916
TEXT:
27/01/15 � 14h47
Crash d'un F-16 en Espagne: une enqu�te ouverte � Paris
Colonne de fum�e sur la base militaire de Los Llanos apr�s le crash d'un F16, le 26 janvier 2015 � Albacete en Espagne AFP - Josema Moreno
Le parquet de Paris a ouvert une enqu�te sur les causes de l'accident d'un avion de combat F-16 qui a tu� onze militaires, neuf Fran�ais et deux Grecs, sur une base du sud-est de l' Espagne , et des gendarmes fran�ais devaient se rendre sur place mardi, a-t-on appris de source judiciaire.
M�me si les faits ont eu lieu � l'�tranger, l'ouverture d'une enqu�te en France est une proc�dure classique lorsque des nationaux comptent parmi les victimes. En Espagne, une enqu�te a �galement �t� ouverte et confi�e � un juge de Valence avec la garde civile. Une commission d'enqu�te technique est aussi � l'oeuvre.
A Paris, le parquet a saisi la Direction g�n�rale de la gendarmerie nationale (DGGN) d'une enqu�te pour homicides et blessures involontaires, a indiqu� � l'AFP une source judiciaire.
Des officiers de police judiciaire de la section de recherches de la gendarmerie de l'air et des militaires de l'IRCGN (Institut de recherche criminelle de la gendarmerie) devaient se rendre sur place mardi, dans le cadre d'une demande d'entraide p�nale internationale transmise aux autorit�s judiciaires espagnoles, a pr�cis� cette source.
Neuf Fran�ais et deux Grecs ont �t� tu�s dans l'accident lundi d'un F-16 de l'arm�e de l'air grecque sur la base de Los Llanos, situ�e pr�s d'Albacete (sud-est de l'Espagne) et qui accueille un centre de formation de l'Otan. Neuf autres Fran�ais et onze Italiens ont �t� bless�s dans l'accident.
Apr�s son d�collage, l'avion a enregistr� une perte de puissance, virant l�g�rement sur sa droite avant de percuter plusieurs autres avions de chasse. Le choc a entra�n� un violent incendie.
Deux AMX italiens, deux Alfa Jet fran�ais, un Mirage 2000 fran�ais ont �t� touch�s.
Au moins sept des neuf morts fran�ais appartenaient � la base a�rienne 133 de Nancy-Ochey (Meurthe-et-Moselle), o� les drapeaux sont en berne.
Le ministre fran�ais de la D�fense Jean-Yves le Drian �tait attendu sur la base de Los Llanos en fin d'apr�s-midi.
27/01/2015 14:46:28 - Paris (AFP) - � 2015 AFP
