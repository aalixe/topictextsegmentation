TITRE: L'Emprise, avec Odile Vuillemin, a �cras� la concurrence sur TF1 Audiences  - T�l� 2 Semaines
DATE: 27-01-2015
URL: http://www.programme.tv/news/audiences/129469-lemprise-ecrase-la-concurrence-demarrage-tout-juste-correct-pour-top-chef/
PRINCIPAL: 1227857
TEXT:
L�Emprise, avec Odile Vuillemin, a �cras� la concurrence sur TF1
Cr�dit : Julien Cauvin / Leonis / TF1
1/1
L�Emprise, avec Odile Vuillemin, a �cras� la concurrence sur TF1
Mardi 27 janvier 2015 � 09h28 - par T�l� 2 Semaines
Tweeter
La soir�e du lundi 26 janvier 2015.
Auditop : Sur TF1, le t�l�film choc L�Emprise, avec Odile Vuillemin et Fred Testot, a r�uni 8,5 millions de t�l�spectateurs (33% de part d�audience).
�a passe : Les deux premiers �pisodes de la nouvelle s�rie de France 2, Secrets and Lies , ont �t� suivis par 3,5 millions de personnes en moyenne (13 % de PDA).
Sur M6, le retour de Top chef sur M6 a attir� 3,3 millions de gourmets (13,7 % de PDA). Un d�marrage dans la lign�e de celui de la saison pr�c�dente (3,14 millions de fans et 14,8% de PDA).
Audiflop : Sur France 3, le documentaire V�ronique Sanson, Pierre Palmade, dr�le de vie a s�duit seulement 1,7 million de curieux (6,6% de PDA).
TNT Top : Le film Le Dernier Samaritain a attir� 1,15 million de cin�philes sur TMC (4,4% de PDA). Le thriller Limitless en a captiv� 922 000 (3,5% de PDA) sur W9. D8 n�est pas loin, avec 845 000 personnes devant Le Dernier Samoura� (3,7% de PDA).
Audizoom : Sur Arte, le polar de Jean-Pierre Melville Le Doulos, avec Jean-Pierre Belmondo et Serge Reggiani, a r�alis� un joli score avec 915 000 t�l�spectateurs (3,4% de PDA).
Source : M�diam�trie.
TAGS : L'emprise - Le Dernier Samaritain - Secrets and Lies - Top chef - V�ronique Sanson
Derni�res news Audiences
Dany Boon et Sophie Marceau (France 2) ont bien r�sist� aux Bleus (TF1) Audiences -  Lundi 30 mars 2015 � 09h17 La soir�e du dimanche 29 mars 2015. Auditop : Sur TF1, la victoire de l'�quipe de France de football contre...
The Voice, ind�tr�nable, devant Commissaire Magellan Audiences -  Dimanche 29 mars 2015 � 09h24 La soir�e du samedi 28 mars 2015. Auditop : Un peu plus de 6 millions de fid�les ont regard� le...
Jean-Pierre Foucault bouscul� par NCIS, superbe remont�e pour Thalassa Audiences -  Samedi 28 mars 2015 � 09h25 La soir�e du vendredi 27 mars 2015. Auditop : TF1 et M6 �taient au coude-�-coude. 4,14 millions de t�l�spectateurs ont...
Commentez
