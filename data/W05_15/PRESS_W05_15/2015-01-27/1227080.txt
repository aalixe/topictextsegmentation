TITRE: La Bourse d'Athènes chute de 5,5% - 7SUR7.be
DATE: 27-01-2015
URL: http://www.7sur7.be/7s7/fr/1536/Economie/article/detail/2195026/2015/01/26/La-Bourse-d-Athenes-chute-de-5-5.dhtml
PRINCIPAL: 1227073
TEXT:
26/01/15 - 11h06��Source: Belga
� reuters.
L'indice g�n�ral de la Bourse d'Ath�nes (Athex) chutait de 5,5% dans les premiers �changes lundi, refl�tant les inqui�tudes des investisseurs au lendemain de la victoire de la gauche anti-aust�rit� Syriza aux �lections l�gislatives anticip�es.
L'indice g�n�ral affichait 793 points (-5,5%), dix minutes apr�s le d�but de la s�ance qui avait commenc� sur une baisse de 2,04%.
Le parti Syriza, dirig� par Alexis Tsipras, pr�ne la fin de la politique d'aust�rit� en Gr�ce et veut n�gocier avec les cr�anciers UE et FMI la r�duction de la dette grecque.
Votre impression sur cet article?
