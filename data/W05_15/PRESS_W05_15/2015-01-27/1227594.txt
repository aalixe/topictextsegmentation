TITRE: A la Une | Dans une lettre, Fidel Castro assure "ne pas faire confiance aux �tats-Unis"
DATE: 27-01-2015
URL: http://www.dna.fr/actualite/2015/01/27/dans-une-lettre-fidel-castro-assure-ne-pas-faire-confiance-aux-etats-unis
PRINCIPAL: 1227592
TEXT:
Dans une lettre, Fidel Castro assure "ne pas faire confiance aux �tats-Unis"
CUBA Dans une lettre, Fidel Castro assure "ne pas faire confiance aux �tats-Unis"
L�ex-pr�sident cubain Fidel Castro, qui n�est pas apparu en public depuis plus d�un an, a �crit lundi dans une lettre qu�il ne faisait pas confiance aux Etats-Unis mais qu�il ne rejetait pas pour autant le rapprochement avec Washington.
Fidel Castro. Photo AFP
Politique
"Je n�ai pas confiance dans la politique des Etats-Unis, et je n�ai �chang� aucun mot avec eux, mais cela ne signifie � aucun moment un rejet d�une solution pacifique aux conflits", a d�clar� l�ex-chef d�Etat de 88 ans dans un courrier adress� � une f�d�ration �tudiante lu sur l�antenne de la t�l�vision d�Etat.
Le p�re de la r�volution cubaine, qui a c�d� le pouvoir � son fr�re Raul � partir de 2006 pour raisons de sant�, a tenu � manifester son appui � la politique de son successeur envers Washington, apr�s l�annonce historique le 17 d�cembre d�une normalisation progressive entre les deux pays.
"Le pr�sident de Cuba a pris les mesures pertinentes au regard de ses pr�rogatives (...) Nous d�fendrons toujours la coop�ration et l�amiti� entre tous les peuples du monde, y compris nos adversaires politiques", a d�clar� Fidel Castro, rompant un silence qui durait depuis plusieurs mois.
L��tat de sant� de Fidel Castro, qui n�est pas apparu en public depuis le 8 janvier 2014, a fait l�objet de nombreuses sp�culations ces derni�res semaines. Ces rumeurs, provenant principalement de journaux et sites internet de Cubains expatri�s, ont �t� aliment�es mi-d�cembre par l�absence remarqu�e du "Comandante" au moment de l�annonce du d�gel avec les �tats-Unis et du retour au pays d�agents cubains lib�r�s par Washington.
Le 12 janvier dernier, il avait rompu une premi�re fois un silence de plusieurs mois en adressant une lettre � son vieil ami, le footballeur argentin Diego Maradona, alors en tournage sur l��le communiste.
Vos commentaires
