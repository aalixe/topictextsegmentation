TITRE: Une Land Rover, la reine Elisabeth II et la trouille du roi Abdallah - Auto - LeVif.be
DATE: 27-01-2015
URL: http://www.levif.be/actualite/auto/une-land-rover-la-reine-elisabeth-ii-et-la-trouille-du-roi-abdallah/article-normal-363503.html
PRINCIPAL: 1229146
TEXT:
Imprimer
La reine Elisabeth II (ici jou�e par Helen Mirren dans The Queen) et sa Land Rover. � DR
Nous sommes en 1998. Abdallah ben Abdelaziz al-Saoud est alors r�gent d'Arabie Saoudite, endossant le r�le de son fr�re Fahd, malade. En septembre, il est de passage en �cosse, au ch�teau de Balmoral, r�sidence de la famille royale britannique dans laquelle la reine aime passer du temps. Au cours du d�jeuner, celle-ci propose au prince h�ritier de l'emmener faire un tour en voiture: la propri�t� qui s'�tend sur 200 km� vaut le d�tour.
D'abord h�sitant, Abdallah se fait sermonner par son ministre des affaires �trang�res et finit par accepter. Monte dans la Land Rover royale, s'installe du c�t� passager alors que son traducteur monte � l'arri�re. Chris Doyle, diplomate britannique proche de la reine � l'�poque raconte la suite de la sc�ne dans ses m�moires: "� sa grande surprise, la reine est mont�e sur le si�ge conducteur, allume le moteur et part. Les femmes n'ont pas -encore- le droit de conduire en Arabie Saoudite, et Abdallah n'avait pas l'habitude d'�tre emmen�e par une femme, encore moins par une reine.
Il poursuit: "Sa nervosit� va alors cro�tre alors que la reine, une conductrice de l'arm�e lors de la Seconde Guerre mondiale (elle avait re�u � l'�poque un entrainement en m�canique, comme le rappelle Konbini ), a commenc� � aller plus vite, avec le Land Rover, le long des routes �cossaises �troites, n'arr�tant pas de parler. Bien que le traducteur et le prince h�riter ont implor� la reine de ralentir et de se concentrer sur la route."
Comme le rappelait r�cemment Sophia Aram dans une chronique sur France Inter , si les femmes ont le droit de vote en Arabie Saoudite depuis 2011, elles ne sont toujours pas autoris�es � conduire. On se souvient en outre d'une sc�ne similaire dans le film The Queen de Stephen Frears, mais sans le roi Abdallah... La s�quence ci-dessous montre quant � elle la vraie Elisabeth II au volant de sa Land Rover, dans le documentaire Elizabeth R de la BBC (1992).
En savoir plus sur:
