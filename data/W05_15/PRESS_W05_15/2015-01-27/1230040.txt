TITRE: Meeting de Valls � Audincourt : la CGT coupe l'�lectricit� | France info
DATE: 27-01-2015
URL: http://www.franceinfo.fr/actu/politique/article/meeting-de-valls-audincourt-la-cgt-coupe-l-electricite-636761
PRINCIPAL: 1230038
TEXT:
Meeting de Valls � Audincourt : la CGT coupe l'�lectricit�
par Elise Del�ve mardi 27 janvier 2015 21:44
Manuel Valls lors de son arriv�e au meeting d'Audincourt � Maxppp
Le meeting du Premier ministre � Audincourt (Doubs) a d�but� avec du retard ce mardi soir en raison d'une coupure de courant revendiqu�e... par la CGT Mines-�nergie.
Pas de courant, pas de discours. Manuel Valls a d� attendre la lumi�re ce mardi soir � Audincourt dans le Doubs. Une coupure d'�lectricit� a emp�ch� le Premier ministre de d�buter son meeting de soutien au candidat PS � la l�gislative partielle de dimanche. Elle a �t� revendiqu�e par la CGT Mines-Energie et a eu lieu pendant la conf�rence de presse. La lumi�re s'est �teinte au moment o� Manuel Valls d�clarait : "Le seul qui est capable de battre la candidate du Front national est Fr�d�ric Barbier, voil� pourquoi je suis venu le..." (soutenir). Les micros ont �t� coup�s. Surprise du Premier ministre et des journalistes pr�sents.�
La CGT coupe le courant au meeting de Valls � Audicourt, dans le Doubs. Marc Pautot, de la CGT Mines-Energie de France-Comt�
Manuel Valls a patient� environ une heure dans la salle de presse. Il se tenait au courant de la situation par textos et par t�l�phone, montrant quelques signes d'impatience. Les r�parateurs ont r�ussi � r�tablir le courant vers 20h50. La panne a affect� tout le quartier o� se d�roule le meeting. La CGT Mines-Energie proteste contre la loi de transition �nerg�tique.�
par Elise Del�ve mardi 27 janvier 2015 21:44
commentaires 4
salsaye
Aveux d'�chec de la part de la CGT ?  Quand on a pas de s�rieux arguments a faire valoir , on en vient a cette extr�mit�.   La cgt emp�che la libert� d'expression , emp�che le d�bat d�mocratique oui . LA fin justifie toujours les moyens avec eux ! C'est navrant de voir ce spectacle . Quel triste exemple pour la jeunesse ! Pour obtenir des droits , obtenons les par la force .
r�pondre
Martinet
La CGT d�voile son vrai visage. On sait que les  salari�s sont les derniers de ses soucis, que la priorit� va � la satisfaction de ses apparatchiks (salaires, indemnit�s, logements). Pour esp�rer conserver cette position avantageuse il ne faut pas que certaines v�rit�s �clatent. Il faut donc tout faire pour limiter la libert� d'expression. Ce qui s'est pass� hier n'est pas un incident. La signification en est tr�s importante. Esp�rons que les journalistes accorderont la place qu'il m�rite � cet �v�nement grave.
