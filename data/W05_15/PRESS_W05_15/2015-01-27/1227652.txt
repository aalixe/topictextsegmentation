TITRE: Facebook, Instagram et Tinder touch�s par une panne
DATE: 27-01-2015
URL: http://www.lemonde.fr/pixels/article/2015/01/27/facebook-instagram-et-tinder-touches-par-une-importante-panne_4563985_4408996.html
PRINCIPAL: 1227559
TEXT:
Facebook, Instagram et Tinder touch�s par une importante panne
Le Monde |
� Mis � jour le
27.01.2015 � 10h31
Facebook, Instagram et Tinder ont connu une panne quasi-simultan�e ce mardi 27 janvier au matin, qui a lieu dans tous les pays pendant une heure. Le site Web de Tinder est rest� accessible, mais de nombreux t�moignages en ligne ont �voqu� une panne importante de l'application.
Instagram a �t� rachet� par Facebook en 2012 pour 1�milliard de dollars. L'entreprise n'a pas encore expliqu� les raisons de la panne qui a touch� les deux services. L'application populaire de rencontres Tinder, qui est un service autonome, s'appuie cependant tr�s largement sur Facebook pour l'identification de ses utilisateurs. Les pannes de Facebook affectent g�n�ralement de tr�s nombreux services utilisant la technologie d'authentification permettant � l'utilisateur de Facebook de se connecter.
It's not very interesting that Facebook is down. But it's really interesting that Facebook log-in services all over the web go down with it.
� Trevor Timm (@trevortimm)
Peu de temps apr�s le d�but de la panne, un groupe de hackeurs baptis� ��Lizard Squad�� a laiss� entendre qu'il �tait � l'origine du blocage. Une hypoth�se extr�mement improbable, et d�mentie en milieu de matin�e par Facebook qui a confirm� que la panne � n'�tait pas la cons�quence d'une attaque mais s'est produite apr�s une modification [du service] qui a eu un effet sur notre syst�me de configuration �.
Le groupe Lizard Squad s'�tait fait conna�tre en bloquant l'acc�s aux services de jeu en ligne de Microsoft et Sony. Un membre pr�sum� du groupe a �t� arr�t� en Grande-Bretagne � la mi-janvier , pour son implication pr�sum�e dans les attaques contre le PlayStation Network et le Xbox Live.
R�seaux sociaux
