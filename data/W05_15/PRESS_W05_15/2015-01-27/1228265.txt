TITRE: Lupita Nyong’o et Jared Leto, plus proches que jamais ? - Elle
DATE: 27-01-2015
URL: http://www.elle.fr/People/La-vie-des-people/News/Lupita-Nyong-o-et-Jared-Leto-plus-proches-que-jamais-2880996
PRINCIPAL: 0
TEXT:
Lupita Nyong’o et Jared Leto, plus proches que jamais ?
Créé le 27/01/2015 à 12h03
© Sipa
Envoyer à un ami
Les rumeurs sur leur relation circulent depuis plus d’un an �! Lupita Nyong’o et Jared Leto les ont relancées de plus belle lors des SAG Awards , dimanche soir. Les deux acteurs sont en effet apparus très proches tout au long de la soirée et les médias américains et britanniques les disent déjà en couple. Après la cérémonie officielle, Lupita Nyong’o a rejoint la soirée organisée par Harvey Weinsten… suivie de près par Jared Leto. «�Ils étaient confortablement installés et discutaient dans un coin… très complices », a raconté un témoin à «�People�» .
 
A photo posted by JARED LETO (@jaredleto) on
Jan 26, 2015 at 2:27pm PST
Une relation ambiguë depuis longtemps
Ne pouvant ignorer les rumeurs, Jared Leto avait fait un clin d’œil à Lupita Nyong’o lorsqu’il était monté sur la scène des Independent Spirit Awards, en mars dernier. «�Je veux remercier toutes les femmes avec qui j’ai été et toutes celles qui pensent avoir été avec moi un jour. Et également ma future ex-femme�: je m’adresse à toi Lupita Nyong’o�!�», avait alors plaisanté l’acteur oscarisé. Le même mois, ils avaient été aperçus ensemble à la Fashion Week de Paris. L’actrice aurait par ailleurs quitté récemment le rappeur K’naan, avec qui elle était en couple depuis quelques mois . �
