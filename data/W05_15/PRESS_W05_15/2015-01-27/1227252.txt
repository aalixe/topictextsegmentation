TITRE: Flash 04h33 Dopage-Armstrong : "Je le referais probablement" - News - Cyclisme - Sport.fr
DATE: 27-01-2015
URL: http://www.sport.fr/cyclisme/flash-04h33-dopage-armstrong-je-le-referais-probablement-368676.shtm
PRINCIPAL: 0
TEXT:
Cyclisme - News Mardi 27 janvier 2015 - 4:33
Flash 04h33 Dopage-Armstrong : "Je le referais probablement"
Tweet
Lance Armstrong a révélé dans un entretien accordé à la BBC qu'il se doperait encore, s'il courait dans les années 90. "Si je courais en 2015, non. Je ne le ferais pas à nouveau car je ne pense pas que j'en aurais besoin. Mais, si vous me ramenez en 1995, quand le dopage était complètement présent, je l'aurais probablement encore fait", a expliqué le cycliste américain, banni à vie du monde du cyclisme.
