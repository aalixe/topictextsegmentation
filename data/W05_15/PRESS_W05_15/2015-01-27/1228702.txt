TITRE: NBA - Kobe Bryant va se faire op�rer | francetv sport
DATE: 27-01-2015
URL: http://www.francetvsport.fr/basket/nba/kobe-bryant-va-se-faire-operer-263341
PRINCIPAL: 1228699
TEXT:
Francetv sport Basket NBA Kobe Bryant Va Se Faire Op�rer
NBA
Kobe Bryant va se faire op�rer
Kobe Bryant (LA Lakers) (EZRA SHAW / GETTY IMAGES NORTH AMERICA / AFP)
Par Romain Bonte
Publi� le 27/01/2015 | 08:37, mis � jour le 27/01/2015 | 08:51
Kobe Bryant va subir une intervention chirurgicale ce mercredi pour soigner son �paule droite. La vedette des Lakers conna�tra son indiponibilit� apr�s l'op�ration. Il serait question d'environ six mois d'arr�t pour le joueur de 36 ans, qui n'a toutefois pas encore parl� de retraite. Son coach parle d'un retour "t�t ou tard".
Depuis deux ans, les gros p�pins physiques s'encha�nent pour le "Black Mamba". Apr�s ses blessures au tendon d'Achille en 2013, puis au genou la saison derni�re (il n'avait alors jou� que six rencontres), le MVP de la saison 2008 va vraisemblablement rater la fin de la saison. Contractuellement, il restera bien une saison suppl�mentaire � Bryant�(avec un salaire de 25 millions de dollars), mais encore faut-il que le mental suive. Selon le manager des Lakers, Byron Scott, le joueur �tait plut�t confiant. "Nous esp�rons bien �videmment que l'op�ration sera un succ�s, et apr�s, son retour se fera t�t ou tard", a-t-il affirm� devant de nombreux journalistes. Quoi qu'il arrive, la franchise californienne devra composer sans sa star, alors qu'elle pointe � l'avant-derni�re place de la Conf�rence Ouest avec seulement 12 victoires pour 33 d�faites.
Byron Scott reacts to the news that @kobebryant will have surgery Wednesday. VIDEO: http://t.co/4YfWBvm6gH pic.twitter.com/eH4V9E7CL7
