TITRE: QATAR > L'Europe met la pression sur la FIFA - Coupe du monde 2022 - Football - Sport.fr
DATE: 27-01-2015
URL: http://www.sport.fr/football/qatar-l-europe-met-la-pression-sur-la-fifa-368715.shtm
PRINCIPAL: 1229075
TEXT:
Football - Coupe du monde 2022 Mardi 27 janvier 2015 - 15:57
QATAR > L'Europe met la pression sur la FIFA
Le conseil de l'Europe a demand� dans un rapport � la FIFA de revoir sa copie concernant l'attribution de la Coupe du monde 2022 au Qatar.
Tweet
La FIFA n'a pas terminé d'entendre parler de la Coupe du monde au Qatar et des irrégularités qui auraient entaché le vote. Mardi, un rapport du conseil de l'Europe, adopté à l'unanimité a demandé à l'instance dirigeante du football mondial de revoir sa copie concernant l'attribution du Mondial 2022. Pour cette institution, "la Fifa ne saurait se dérober de son obligation de procéder à un nouveau vote".
Ce rapport de la commission de l'Assemblée parlementaire du Conseil de l'Europe n'hésite par à qualifier de "farce" le processus d'attribution. Au delà de cette attribution controversée, il est aussi demandé à la FIFA de faire pression sur le Qatar quant au traitement des ouvriers étrangers présents sur les chantiers des stades.
De quoi probablement agacer Sepp Blatter . Car si le conseil de l'Europe n'a évidemment aucun pouvoir décisionnaires, c'est une pression politique supplémentaire qui s'abat sur les épaules du président de la FIFA.
