TITRE: Espagne: neuf Fran�ais tu�s dans le crash d'un avion de chasse grec - Europe - RFI
DATE: 27-01-2015
URL: http://www.rfi.fr/europe/2min/20150126-espagne-huit-francais-tues-crash-avion-chasse-f-16/
PRINCIPAL: 0
TEXT:
Modifi� le 27-01-2015 � 11:29
Espagne: neuf Fran�ais tu�s dans le crash d'un avion de chasse grec
Par RFI
De la fum�e s'�l�ve du centre d'entra�nement  de la base militaire � Albacete, apr�s le crash du F-16.
AFP PHOTO / JOSEMA MORENO
Neuf militaires Fran�ais ainsi que deux Grecs ont p�ri dans l�accident d�un avion de combat F-16 grec, sur une base a�rienne de l'Otan, dans le sud de l'Espagne.�C'est le chef du gouvernement espagnol Mariano Rajoy qui l'a annonc� lundi soir. Ce drame est survenu au cours d'un exercice r�serv� aux pilotes de chasse les plus exp�riment�s. Le ministre fran�ais de la D�fense, Jean-Yves Le Drian se rendra sur place ce mardi apr�s-midi.
L�accident a eu lieu lundi en d�but d�apr�s-midi sur une base de l�Otan pr�s d�Albacete, � environ 250 kilom�tres au sud-est de Madrid. Sur les dix personnes qui ont �t� tu�es, huit sont de nationalit� fran�aise, a annonc� le chef du gouvernement espagnol, Mariano Rajoy. Une information confirm�e par le minist�re fran�ais de la D�fense. Un bilan donn� par la suite faisait par ailleurs �tat d'au moins six bless�s. Enfin, ce mardi matin, un neuvi�me militaire fran�ais est d�c�d� des suites de ses blessures, a indiqu� le minist�re espagnol de la D�fense.
Les enqu�teurs de l�Otan sont arriv�s sur les lieux.
L�accident a eu lieu juste apr�s le d�collage d�un avion de combat grec de type F-16, man�uvr� par des pilotes grecs. Selon un communiqu� du minist�re fran�ais de la D�fense, qui doit arriver sur place dans l'apr�s-midi ce mardi, l�appareil aurait heurt� plusieurs appareils qui se trouvaient sur l'un des parkings de la base a�rienne.�Parmi les appareils, deux Mirage et deux Rafale fran�ais.
Coup dur pour l'arm�e de l'air fran�aise
L'arm�e de l'air fran�aise n�avait pas connu un tel bilan depuis l'accident d�un avion de transport en Ari�ge, en 2003, dans le sud-ouest de la France. L�, c�est la communaut� des pilotes de chasse qui est touch�e, particuli�rement celle de la base de Nancy, dans l�est du pays.
Parmi les victimes fran�aises, il y a du personnel au sol, des m�caniciens, mais aussi  des pilotes. L�accident a eu lieu au moment o� les appareils fran�ais �taient en train d��tre pr�par�s pour partir en mission. Certains �quipages avaient d�j� rejoint leurs avions quand ils ont �t� percut�s pas le F-16 grec en perdition�; une boule feu, car l�avion �tait rempli de carburant au moment du d�collage. Les deux pilotes grecs ont �galement trouv� la mort dans l�accident.
Selon des t�moins espagnols, l�avion de chasse aurait perdu de la vitesse au moment du d�collage et aurait bascul� vers la droite. Ensuite, il aurait, dans sa chute, percut� un hangar o� au moins cinq avions, attendant l�ordre de d�collage.
Exercice d�entrainement de haut niveau
Les militaires effectuaient l�exercice dit �TLP� : Tactical Leadership Program. Il s�agit d�un programme de tout premier plan pour les pilotes de l�Otan. La base d�Albacete, en Espagne, accueille des pilotes venus des principaux pays de l�Alliance. Ces jours-ci, 750 militaires �taient pr�sents. La France avait envoy� une centaine de personnels.
Le TLP est un stage de haut niveau qui permet aux pilotes d�acqu�rir le statut de chef de mission. Ce qui permet ensuite de diriger des missions complexes en coalition, comme en Libye en 2011 ou encore aujourd�hui au-dessus de l�Irak. C�est une formation exigeante, fatigante qui allie des simulations de raids, des bombardements, une mission d�entrainement � la d�fense a�rienne avec des sorties a�riennes qui peuvent impliquer, dans certains sc�narios, pr�s d�une vingtaine d�avions simultan�ment.
Chronologie et chiffres cl�s
