TITRE: Mercato � FC Nantes : Serge Gakp� au Genoa, Torino ou Crystal Palace ??
DATE: 27-01-2015
URL: http://www.foot-sur7.fr/207568-mercato-fc-nantes-serge-gakp-au-genoa-torino-ou-crystal-palace
PRINCIPAL: 1227896
TEXT:
Mercato � FC Nantes : Serge Gakp� au Genoa, Torino ou Crystal Palace ??
Auteur : Gary SLM / Publi� le : 27 janvier 2015
Serge Gakp� n�est pas une priorit� pour son entra�neur Michel Der Zakarian cette saison. Il pourrait rejoindre le Genoa, Torino ou Crystal Palace, trois clubs int�ress�s par ses services.
Serge Gakp� n�a d�but� que 12 matchs de Ligue 1 cette saison sur les 20 affich�s � son compteur. Retourn� 8 fois sur le banc des rempla�ants, l�attaquant togolais de 27 ans souffre d�un manque de temps de jeu. Forcement perturb� par sa situation puisqu�il n�a scor� qu�une fois en championnat contre 8 la saison derni�re, il pourrait c�der � l�appel de la Genoa en Italie pour relancer sa carri�re qui est en train de se gripper � petit feu au FC Nantes. Selon le quotidien L��quipe , ses dirigeants voudraient bien le c�der au Genoa m�me si ce club renforc� par Mbaye Niang semble de moins en moins int�ress�. Mais en fin de contrat en juin prochain avec Nantes, Gakp� pourrait assister � un regain d�int�r�t de l��curie italienne. Et si cette piste n�aboutissait pas, Serge Gakp� ne devrait pas d�primer trop longtemps puisqu�il est sur les tablettes du Torino d�apr�s la m�me source. Crystal Palace en Angleterre surveillerait aussi sa situation pour le m�me besoin de renforcer son secteur offensif
550 vote(s)
Ocampos, une bonne recrue pour l'OM
Oui
