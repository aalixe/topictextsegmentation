TITRE: Comment donner un coup de pouce � ses petits enfants�?
DATE: 27-01-2015
URL: http://www.lemonde.fr/argent/article/2015/01/26/comment-donner-un-coup-de-pouce-a-ses-petits-enfants_4563288_1657007.html
PRINCIPAL: 0
TEXT:
Comment donner un coup de pouce � ses petits enfants�?
Le Monde |
26.01.2015 � 16h21
| Par Jean Dugor (notaire � Auray - Morbihan)
Les possibilit�s ne manquent pas. La plus commun�ment utilis�e reste le don manuel. Il est en effet possible de donner une somme d�argent (ch�que, virement, esp�ces�) sans payer de taxe si le montant ne d�passe pas 31�865�euros.
Pour b�n�ficier de cette exon�ration, les grands-parents doivent �tre �g�s de moins de 80 ans et les petits-enfants �tre majeurs (ou mineurs �mancip�s). Ces dons manuels seront pris en compte au moment du r�glement de la succession du donateur, ce qui n�est pas le cas des pr�sents d�usage, ces cadeaux r�alis�s lors d�une grande occasion (mariage, anniversaire�).
Ces derniers peuvent �tre importants (voiture, somme d�argent�), mais toujours dans une proportion raisonnable par rapport � la ��richesse�� de celui qui donne.
L�assurance-vie un instrument tr�s efficace
L�assurance-vie est aussi un instrument tr�s efficace pour transmettre un capital. Premi�re solution�: le ou les grands-parents d�signent comme b�n�ficiaire(s) de leur contrat le ou les petits-enfants (m�me mineurs).
Mais le versement des sommes �tant li� au d�c�s du souscripteur, les petits-enfants ne savent pas quand ils pourront en profiter. Une autre piste consiste alors � combiner donation et assurance-vie.
Les grands-parents donnent une somme d�argent qui est obligatoirement plac�e dans un contrat d�assurance-vie souscrit au nom des petits-enfants. La donation pr�cise la dur�e pendant laquelle les sommes restent indisponibles (vingt-cinq ans maximum). A la date convenue, le ou les petits-enfants pourront proc�der � des retraits r�guliers sur leurs contrats dans des conditions fiscales avantageuses.
