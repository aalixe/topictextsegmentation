TITRE: La critique du film "Snow Therapy" : Avalanche et boules de fiel - leJDD.fr
DATE: 27-01-2015
URL: http://www.lejdd.fr/Culture/Cinema/La-critique-du-film-Snow-Therapy-Avalanche-et-boules-de-fiel-714464
PRINCIPAL: 0
TEXT:
25 janvier 2015 �|� Mise � jour le 27 janvier 2015
R�agissez !
Tweet
Snow Therapy : Avalanche et boules de fiel
T�moin d�une coul�e de neige spectaculaire lors de ses vacances au ski, un couple se l�zarde. En cause : la l�chet� du mari. Corrosif.
Paru dans leJDD
Un ph�nom�ne visuel et naturel qui va mettre une famille au bord de la crise de nerfs. (Plattform Produktion / Essential Film)
Amoureux de la montagne et fervent skieur, le Su�dois Ruben �stlund a fait ses classes de cin�aste dans les ann�es 1990 avec un documentaire d'�tudes : le divorce de ses propres parents� Plus port�s sur les contradictions du comportement humain que sur la glisse en altitude, ses films suivants (Happy Sweden, Play) lui ont permis de fa�onner un style personnel, reconnu pour son utilisation novatrice des cam�ras num�riques � haute d�finition, et surtout mordant dans sa fa�on d'observer ces dr�les d'animaux que sont les humains.
Dans Snow Therapy, com�die conjugale corrosive o� toute une famille bascule soudain dans la d�prime existentielle, �stlund rabiboche avec brio ses deux dadas. Tourn� aux Arcs, c�l�bre station de ski des Alpes, ce film dont le titre original est Turist tire tout d'abord le meilleur parti de l'environnement immacul� des pentes enneig�es. Car au passage, il r�v�le leur �tranget� qui se trouve aussi �tre celle des hommes qui les colonisent et les contr�lent au point de d�clencher eux-m�mes les avalanches.
La fin d'un mythe
"Ce genre d'endroit cr�� de toutes pi�ces dans les ann�es 1950, explique �stlund, est typique du confort standardis� qu'on vend aux classes moyennes occidentales. Les vacances au ski symbolisent pour moi le sentiment de ma�trise compl�te de sa propre vie. C'est le moment o� le p�re "redonne" � sa famille, il peut compenser son absence, se rapprocher des siens."
Un id�al assez banal mais auquel Tomas, l'(anti)-h�ros de Snow Therapy, n'a pas acc�s. La faute � cette avalanche superbement film�e qui va soudain le mettre � nu devant les siens. "L'histoire, poursuit le cin�aste, est celle d'un contrat bris� impliquant de nouvelles r�gles. Elle me vient d'une anecdote r�elle arriv�e � des amis qui se sont fait braquer par un gang arm� en Am�rique du Sud. Terroris�, le mari avait laiss� son �pouse en plan. Par la suite, elle n'arr�tait plus de le torturer avec ce souvenir peu glorieux." En fouillant le sujet, �stlund trouve, par ailleurs, des �tudes scientifiques d�montrant que ces couples soumis � des situations extr�mes de catastrophe ou de grande frayeur sortent rarement indemnes et souvent divorcent.
"J'ai trouv� ce constat passionnant et appris au passage que l'�re du Titanic a v�cu. Dans le naufrage de l'Estonia, en 1994, il y a eu peu de survivants femmes ou enfants. Sans aucun doute, c'est le "chacun pour soi" qui a pr�valu et qui a b�n�fici� � des hommes. Et je ne vous parle m�me pas du capitaine du Concordia, le l�che absolu!"
Autant de naufrages qui, en fin de compte, sont ceux d'un mythe, d'une virilit� brave et protectrice? "Oui, il y a un mythe du r�le de l'homme qui confine � l'imposture g�n�ralis�e. On joue ce qu'on attend de nous�" Jusqu'au jour o� le masque tombe.�
Alexis Campion - Le Journal du Dimanche
dimanche 25 janvier 2015
