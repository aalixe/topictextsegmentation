TITRE: Le billet de 20 euros fait exploser la fausse monnaie | www.directmatin.fr
DATE: 27-01-2015
URL: http://www.directmatin.fr/economie/2015-01-27/le-billet-de-20-euros-fait-exploser-la-fausse-monnaie-698532
PRINCIPAL: 0
TEXT:
Le billet de 20 euros fait exploser la fausse monnaie
Par Direct Matin, publi� le
27 Janvier 2015 � 16:25
Des exemplaires du billet de 20 euros actuellement en circulation.[Denis Charlet / AFP/Archives]
En compl�ment
Ils veulent payer leurs courses avec un vrai billet de 500 euros et se font arr�ter
Le nombre de faux billets saisis dans la zone Euro a explos� en 2014 � en croire les derniers chiffres publi�s par la Banque centrale europ�enne (BCE). L'ann�e derni�re, 838.000 fausses coupures ont �t� retir�es de la circulation contre seulement 670.000 en 2013. Une hausse qui m�rite quelques explications.
�
2014, une ann�e faste pour les faussaires en Europe. L'ann�e derni�re ce sont en effet pas moins de 838.000 faux billets qui ont �t� retir�s de la circulation dont 507.000 pour le seul second semestre. Soit une hausse de 25% sur un an� et de 44% sur le dernier semestre.
Cela ne veut pas dire pour autant que fabriquer des faux billets en euros soit devenu plus simple. En r�alit�, deux �v�nements ont permis de multiplier les saisies. D'une part, la mise en circulation de quantit� importante de nouvelles coupures par la BCE.�
Songez qu'au second semestre 2014, 16 milliards d'euros ont �t� mis en circulation dans la zone Euro. Ce qui fait dire � la BCE que�"le nombre de billets contrefaits demeure toutefois tr�s faible par rapport au nombre total, en augmentation, des billets authentiques en circulation".
�
Le billet de 20 euros, la star des faussaires
D'autre part la fin annonc�e du billet de 20 euros sous sa forme actuelle. �En effet, est de loin le billet pr�f�r� des faussaires car il est bien plus simple � mettre en circulation que les coupures de 100 ou 500 euros dans la mesure o� il est utilis� au quotidien par les habitants.
Et de fait, la coupure de 20 euros repr�sente 60% des faux billets saisis en 2014, loin devant celles de 50 euros (26%) de 5 euros (1%) ou de 500 euros (0,5%). Selon certains sp�cialistes, l'annonce de son renouvellement prochain entra�ne un ph�nom�ne de "d�stockage" chez les faussaires. �
La BCE a lanc� une nouvelle s�rie de billets en euros baptis�e "Europe" cens�e �tre plus difficile � contrefaire. Apr�s le nouveau billet de 5 euros en 2013 et celui de 10 euros en 2014, elle d�voilera le 24 f�vrier prochain le nouveau billet de 20 euros.
�
