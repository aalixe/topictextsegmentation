TITRE: Marseille: un homme blessé par balles
DATE: 27-01-2015
URL: http://www.lefigaro.fr/flash-actu/2015/01/27/97001-20150127FILWWW00235-marseille-un-homme-blesse-par-balles.php
PRINCIPAL: 0
TEXT:
le 27/01/2015 à 13:54
Publicité
Un homme de 26 ans a été blessé par balles aujourd'hui dans une cité des quartiers Nord de Marseille, a-t-on appris de source proche du dossier.
L'homme a été touché de plusieurs coups de feu dans la cité de la Castellane (16e arrondissement). Ses jours ne sont pas en danger, a précisé cette source qui ignorait les motifs de l'agression et le type d'arme utilisé. Les marins-pompiers se sont rendus sur place pour le soigner.
