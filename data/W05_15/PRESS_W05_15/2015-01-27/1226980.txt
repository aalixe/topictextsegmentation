TITRE: Mattel : change de PDG face � la baisse continue des ventes | Zone bourse
DATE: 27-01-2015
URL: http://www.zonebourse.com/MATTEL-INC-13462/actualite/Mattel--change-de-PDG-face-a-la-baisse-continue-des-ventes-19764030/
PRINCIPAL: 1226940
TEXT:
(Vous pouvez saisir plusieurs adresses mails en les s�parant par des points virgules)
Message personnel :
*Champs obligatoires
Mattel, le fabricant des poup�es Barbie et des jouets Fisher-Price, a annonc� lundi le d�part de son PDG Bryan Stockton et pr�venu que son chiffre d'affaires avait baiss� pour le cinqui�me trimestre cons�cutif.
L'action Mattel a perdu jusqu'� 11% en d�but de s�ance pour tomber � son plus bas niveau depuis trois ans avant de regagner la majeure partie du terrain c�d�. Vers 16h35 GMT, le titre n'abandonnait plus que 3,3% � 27,1150 dollars.
Bryan Stockton sera remplac� � titre provisoire par Christopher Sinclair, un ancien cadre dirigeant de PepsiCo  qui si�geait d�j� � son conseil d'administration.
Mattel souffre depuis plusieurs ann�es de la d�saffection croissante des enfants pour la poup�e Barbie, de plus en plus concurrenc�e par les jouets et les jeux �lectroniques, mais aussi par les produits d�riv�s des succ�s de Walt Disney , entre autres "La Reine des neiges" ("Frozen").
Bryan Stockton �tait directeur g�n�ral du groupe depuis janvier 2012 et pr�sident depuis 2013.
"Nous ne nous attendions pas � ce changement mais nous pensons qu'il a �t� d�cid� apr�s deux ann�es cons�cutives de r�sultats inf�rieurs aux attentes", ont comment� les analystes de Stifel, Nicolaus & Co dans une note.
Les ventes de Barbie baissent depuis pr�s de trois ans et les autres marques de poup�es du groupe, comme Monster High ou American Girl, n'ont pas r�ussi � compenser ce recul.
Alors que Barbie d�tenait en 2009 plus d'un quart du march� des poup�es et produits associ�s aux Etats-Unis, elle n'en repr�sentait plus que 19,6% en 2013.
Mattel estime que ses ventes mondiales ont baiss� de 6% au cours du trimestre clos le 31 d�cembre, � 1,99 milliard de dollars, en raison entre autres de l'appr�ciation du dollar. Son b�n�fice net a parall�lement chut� de pr�s de 60% � 149,9 millions de dollars, soit 44 cents par action.
La marge brute du groupe a diminu� de 4,1 points � 50,4% des ventes nettes, principalement � cause du rachat du canadien Mega Brands l'an dernier.    (Yashaswini Swamynathan, Marc Angrand pour le service fran�ais)
0
Derni�res actualit�s  sur MATTEL, INC.
Publicit�
