TITRE: VIDEO. Crash d'un F-16 en Espagne: Dix morts, dont huit Fran�ais, et 21 bless�s - 20minutes.fr
DATE: 27-01-2015
URL: http://www.20minutes.fr/monde/1526215-20150126-video-espagne-huit-francais-morts-crash-avion-grec
PRINCIPAL: 1227350
TEXT:
accident
Huit aviateurs Fran�ais ont trouv� la mort dans le crash d'un avion de combat grec de type F 16 � Albacete en Espagne, selon le pr�sident du gouvernement espagnol Mariano Rajoy, un terrible chiffre confirm� par l'Elys�e � 22h40.
L'appareil s'est �cras� alors qu'il tentait de d�coller sur la base a�rienne militaire de Los Llanos, faisant en tout dix morts et vingt-et-un bless�s (dix Fran�ais et onze Italiens), selon le minist�re espagnol de la D�fense.
Sept des huit victimes fran�aises appartenaient � la base militaire de Nancy
Trois capitaines et un lieutenant feraient partie des victimes fran�aises.�Sept des huit victimes fran�aises appartenaient � la base a�rienne 133 de Nancy-Ochey (Meurthe-et-Moselle), a indiqu� mardi le commandant de la base, o� les drapeaux ont �t� mis en berne.
Selon Mariano Rajoy, interrog� en direct sur la cha�ne priv�e espagnole Telecinco, deux Grecs ont �galement �t� tu�s dans l'accident. D'apr�s le minist�re de la D�fense � Paris, l'un des bless�s fran�ais �est en situation d'extr�me urgence. Cinq autres sont en r�animation dont deux plac�s en coma artificiel. Trois autres bless�s l�gers sont en sortie d'h�pital.� �Une enqu�te sera conjointement men�e par la Gr�ce et l'Espagne�, selon la source au minist�re de la D�fense contact�e par 20 Minutes, qui ajoute que la th�se de l'accident est privil�gi�e pour l'heure.
Jean-Yves Le Drian sur place ce mardi
Le minist�re de la D�fense pr�cise qu'�� 15h30 sur la base a�rienne d�Albacete en Espagne, un F-16 biplace des forces a�riennes grecques s�est �cras� sur un des parkings de la base o� �taient stationn�s des avions de chasse de plusieurs nationalit�s, dont deux Alpha Jet, deux Mirage 2000D et deux Rafale fran�ais�. Jean-Yves Le Drian se rendra sur place mardi apr�s-midi. Le ministre de la D�fense adresse �ses plus sinc�res condol�ances aux proches des victimes et exprime sa compassion aux bless�s de cette trag�die�.
Selon l'�tat-major italien, parmi les bless�s figurent neuf Italiens, dont seulement deux sont gri�vement touch�s.
Sur Twitter, le chef du gouvernement Mariano Rajoy a exprim� son �motion �apr�s le tragique accident sur la base a�rienne de Los Llanos�, pr�s d'Albacete (environ 250 km au sud-est de Madrid), avant de partager sa �tristesse� pour les victimes.
Conmovido por el tr�gico accidente en la base a�rea de Los Llanos. Mi pesar por los fallecidos y mi deseo de recuperaci�n de los heridos. MR
� Mariano Rajoy Brey (@marianorajoy) January 26, 2015
�
Un incendie important apr�s le crash
L'avion de combat de la force a�rienne grecque devait effectuer des man�uvres dans le cadre d'un entra�nement organis� par l'OTAN, le Tactical Leadership Programme (TLP), et s'est �cras� au d�collage, selon un communiqu� diffus� plus t�t par le minist�re de la D�fense. Il s'est apparemment �cras� sur le tarmac, heurtant d'autres a�ronefs et tuant d'autres personnes qui s'y trouvaient. Lundi apr�s-midi des cha�nes de t�l�vision espagnoles ont diffus� quelques secondes d'images o� on aper�oit un avion en feu, d'o� s'�chappent d'importantes volutes de fum�e noire. Les �quipes de secours ont d� venir � bout de l'incendie entra�n� par le crash sur l'aire de stationnement avant de pouvoir d�terminer le nombre de victimes.
