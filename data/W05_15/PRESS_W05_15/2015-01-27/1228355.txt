TITRE: Ukraine: 9 soldats tués ces dernières 24 heures
DATE: 27-01-2015
URL: http://www.lefigaro.fr/flash-actu/2015/01/27/97001-20150127FILWWW00082-ukraine-9-soldats-tues-ces-dernieres-24-heures.php
PRINCIPAL: 1228353
TEXT:
Ukraine: 9 soldats tués ces dernières 24 heures
< Envoyer cet article par e-mail
X
Séparez les adresses e-mail de vos contacts par des virgules.
De la part de :
J'accepte de recevoir la newsletter quotidienne du Figaro.fr
Oui
Réagir à cet article
Publicité
La résolution du 29 juin 2006 des NATIONS UNIS
LE PEUPLE RUSSOPHONE DE L UKRAINE DOIT DECIDER DE SON AVENIR
Déclaration des Nations Unies sur les droits des peuples autochtones
Résolution adoptée par l’Assemblée générale résolution du 29 juin 2006
,Affirmant que les peuples autochtones sont égaux à tous les autres peuples, tout en reconnaissant le droit de tous les peuples d’être différents, de s’estimer différents et d’être respectés en tant que tels,
Affirmant également que tous les peuples contribuent à la diversité  et à la richesse des civilisations et des cultures, qui constituent le patrimoine commun de l’humanité,
Consciente de la nécessité urgente de respecter et de promouvoir les droits intrinsèques des peuples autochtones, qui découlent de leurs structures politiques, économiques et sociales et de leur culture, de leurs traditions spirituelles, de leur histoire et de leur philosophie, en particulier leurs droits à leurs terres, territoires et ressources,
Considérant que le respect des savoirs, des cultures et des pratiques traditionnelles autochtones contribue à une mise en valeur durable et équitable de l’environnement et à sa bonne gestion,
la présente Déclaration encouragera des relations harmonieuse et de coopération entre les États et les peuples autochtones, fondées sur les principes de justice, de démocratie, de respect des droits de l’hommeEncourageant les États à respecter et à mettre en oeuvre effectiveme toutes leurs obligations
Le 27/01/2015 à 21:53
antitotalitaires
comme le dit le président Poutine, l'armée ukrainienne est une légion étrangère de l'OTAN qui exécute des ordres géostratégiques qui n'ont rien à voir avec le bien et la défense des ukrainiens,
Le 27/01/2015 à 19:11
Yajean
Les autorités séparatistes ont  affirmé leur volonté d'attaquer les lignes ennemies pour déplacer le front à quelques kilomètres de distance des principales villes, Donetsk en particulier, de manière à éviter que les tirs d'artillerie des gouvernementaux ukrainiens ne continuent à massacrer des civils.
Le 27/01/2015 à 11:35
