TITRE: Facebook d�ment toute cyberattaque apr�s une panne de 50 minutes
DATE: 27-01-2015
URL: http://www.romandie.com/news/Facebook-dement-toute-cyberattaque-apres-une-panne-de-50-minutes_RP/559387.rom
PRINCIPAL: 0
TEXT:
Tweet
Facebook d�ment toute cyberattaque apr�s une panne de 50 minutes
Washington - Le r�seau social Facebook, qui a �t� fortement perturb� au niveau mondial pendant une cinquantaine de minutes mardi, a d�menti toute attaque de ses syst�mes par des pirates informatiques, contrairement � des rumeurs circulant sur internet.
Beaucoup de personnes ont eu du mal � acc�der � Facebook et Instagram. Cela ne r�sultait pas d'une attaque de tiers mais s'est produit apr�s l'introduction d'un changement qui a affect� nos syst�mes de configuration, a affirm� Facebook dans un communiqu�.
Un groupe de hackers, Lizard Squad, a publi� sur Twitter un message ambig�: Facebook, Instagram, Tinder, AIM, Hipchat #offline #LizardSquad.
Nous avons rapidement r�solu le probl�me, et les deux services fonctionnent � nouveau � 100% pour tout le monde, a ajout� Facebook.
Le r�seau social aux 1,3 milliard d'utilisateurs (revendiqu�s fin septembre) n'a donn� aucun d�tail sur l'ampleur de la panne qui avait d�but� vers 06H15 GMT pour se terminer peu apr�s 07H00 GMT.
Facebook avait alors publi� un court communiqu�: Nous savons que de nombreuses personnes peinent actuellement � acc�der � Facebook et Instagram. Nous travaillons � un retour � la normale aussi rapidement que possible.
kd-jum/fpo/bpi/lma
(�AFP / 27 janvier 2015 10h52)
�
