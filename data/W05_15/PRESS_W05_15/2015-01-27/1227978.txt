TITRE: Cuba: Fidel Castro n'a toujours pas confiance dans les Etats-Unis - L'Express
DATE: 27-01-2015
URL: http://www.lexpress.fr/actualite/monde/amerique-nord/cuba-fidel-castro-n-a-toujours-pas-confiance-dans-les-etats-unis_1644991.html
PRINCIPAL: 1227975
TEXT:
Cuba: Fidel Castro n'a toujours pas confiance dans les Etats-Unis
Par L'Express.fr avec AFP,           publi� le
27/01/2015 � 10:24
, mis � jour �
10:30
Dans une lettre, l'ancien chef d'Etat cubain a exprim� sa m�fiance vis-�-vis des Etats-Unis sans d�savouer le rapprochement entrepris par son fr�re, Raul Castro.
Zoom moins
?
� � � � �
Fidel Castro assis � c�t� de son fr�re Raul, pr�sident de Cuba, lors d'une r�union de l'Assembl�e nationale le 24 f�vrier 2013 � La Havane
afp.com/Ismael Francisco
"El Commandante" est vivant. L'ex-pr�sident cubain, qui n'est pas apparu en public depuis plus d'un an, a �crit lundi dans une lettre son avis sur les pourparlers entre Cuba et les Etats-Unis . "Je n'ai pas confiance dans la politique des Etats-Unis, et je n'ai �chang� aucun mot avec eux" a-t-il d�clar� moins d'une semaine apr�s le d�but des n�gociations . Toutefois, il ne rejette pas l'actuel rapprochement entre la Havane et Washington .�
Apr�s la lettre qu'il avait envoy�e � Maradona, ce message tend � prouver que Fidel Castro est, malgr� les rumeurs , bien vivant. Le p�re de la r�volution cubaine, fonci�rement oppos� aux Etats-Unis, a apport� son soutien au rapprochement entrepris par son fr�re Raul Castro avec ceux-ci. "Nous d�fendrons toujours la coop�ration et l'amiti� entre tous les peuples du monde, y compris nos adversaires politiques." L'ex-chef d'Etat, �g� de 88 ans, n'est pas apparu en public depuis le 8 janvier 2014.�
Avec
