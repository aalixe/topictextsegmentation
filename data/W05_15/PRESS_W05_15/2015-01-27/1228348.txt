TITRE: Flamb�e des violences antis�mites en France en 2014 - LExpress.fr
DATE: 27-01-2015
URL: http://www.lexpress.fr/actualites/1/actualite/le-nombre-d-actes-antisemites-a-double-en-2014-selon-le-crif_1644947.html
PRINCIPAL: 0
TEXT:
Flamb�e des violences antis�mites en France en 2014
Par AFP, publi� le
27/01/2015 � 08:28
, mis � jour � 12:58
Paris - Le nombre d'actes antis�mites a doubl� en France en 2014 par rapport � 2013, avec une hausse des violences plus marqu�e encore que celle des injures: les instances juives s'en alarment et attendent des "mesures puissantes et fortes".
�
Des Juifs prient dans une synagogue pr�s de Toulouse, le 8 janvier 2009
afp.com/Eric Cabanis
Le Service de protection de la communaut� juive (SPCJ), organisme communautaire travaillant en lien avec le minist�re de l'Int�rieur, a publi� mardi son rapport 2014 sur l'antis�mitisme, r�alis� avec le soutien du M�morial de la Shoah, 70 ans apr�s la lib�ration du camp nazi d'Auschwitz.�
Selon le SPCJ, 851 actes antis�mites (actions et menaces) ont �t� recens�s l'an dernier, contre 423 en 2013, soit une hausse de 101%, atteignant un plus haut depuis 2004 (974 actes).�
Fait notable, les actions (violences, incendies, d�gradations, vandalisme...), en progression de 130% avec 241 faits contre 105 en 2013, se sont intensifi�es par rapport aux menaces (propos, gestes, tracts, inscriptions, courriers injurieux, +92%).�
Ces chiffres n'int�grent pas la prise d'otages de l'Hyper Cacher, qui a fait quatre morts le 9 janvier. Le rapport est d�di� � leur m�moire ainsi qu'� celle des 13 autres victimes des attentats jihadistes parisiens.�
"Le point critique a �t� largement d�pass�", commente le Conseil repr�sentatif des institutions juives de France (Crif). �
Selon le SPCJ, "51% des actes racistes commis en France en 2014 sont dirig�s contre des Juifs", alors m�me que la premi�re communaut� juive d'Europe repr�sente moins de 1% de la population fran�aise, soit entre 500.000 et 600.000 personnes. L'organisme rel�ve que la progression de 30% des actes racistes recens�e en France l'an dernier "est constitu�e exclusivement par la hausse des actes antis�mites".�
Les actes antimusulmans (133 dont 55 actions violentes) ont eux connu une baisse de 41,1% en 2014, m�me si l'Observatoire national contre l'islamophobie juge que ces chiffres "ne refl�tent pas la r�alit�" et anticipe une nette hausse cette ann�e, dans la foul�e d'un mois de janvier tr�s agit� contre les mosqu�es.�
- 'Il est urgent d'agir' -�
Si "le fait antis�mite est pr�pond�rant quasiment sans discontinuer" sur 2014, le SPCJ rel�ve plusieurs pics de menaces et violences. En janvier, lors de l'affaire Dieudonn� et de la manifestation "Jour de col�re", o� des cris "Mort aux Juifs" ont �t� prof�r�s. En juillet (208 actes recens�s, presque un quart du total), lors d'incidents visant des synagogues et commerces casher en marge de manifestations pro-Hamas. Enfin en d�cembre avec la s�questration � Cr�teil d'un couple vis� pour la jud�it� du jeune homme, sur fond de pr�jug�s sur les Juifs et l'argent: une affaire qui "n'est pas sans rappeler le rapt, la torture et le meurtre d'Ilan Halimi" en 2006, estime le SPCJ.�
Cr�teil, d'ailleurs, avec 13 faits recens�s, a �t� l'une des neuf villes les plus touch�es en 2014, selon le SPCJ. Les huit autres, o� vivent aussi d'importantes communaut�s juives, sont Paris (154 actes), Marseille (36), Lyon (30), Toulouse (27), Sarcelles (19), Villeurbanne et Nice (17), Strasbourg (14).�
En �cho � la d�nonciation d'un "nouvel antis�mitisme" issu des banlieues et quartiers populaires, le pr�sident du SPCJ, Eric de Rothschild, invite � combattre "cette approche fanatique d'un islam barbare et r�trograde dont nous souffrons tous, que nous soyons musulman, chr�tien, juif ou ath�e".�
Le pr�sident du Crif, Roger Cukierman, a lui esp�r� "que des mesures puissantes et fortes seront prises dans les domaines de la pr�vention, la protection et l'�ducation", quelques heures avant l'annonce par Fran�ois Hollande qu'un "plan global de lutte contre le racisme et l'antis�mitisme" serait pr�sent� d'ici fin f�vrier.�
"Pendant trop longtemps, la communaut� juive de France s'est sentie isol�e dans son combat contre l'antis�mitisme. Si les pouvoirs publics ont toujours �t� � nos c�t�s, l'indiff�rence de la soci�t� nous a profond�ment affect�s", a r�agi � l'AFP le grand rabbin de France Ha�m Korsia, qui esp�re que la "mobilisation historique du 11 janvier" contre le terrorisme perdure et "ouvre la voie � des actions de grande ampleur". "Il est d�sormais urgent d'agir."�
Par
