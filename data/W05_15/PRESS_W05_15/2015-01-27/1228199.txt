TITRE: RPT-France-Hausse des 2% des cr�ations d'entreprises en 2014- 27 janvier 2015 - Challenges.fr
DATE: 27-01-2015
URL: http://www.challenges.fr/finance-et-marche/20150127.REU7680/france-hausse-des-2-des-creations-d-entreprises-en-2014.html
PRINCIPAL: 1228196
TEXT:
Challenges �>� Finance et march� �>�RPT-France-Hausse des 2% des cr�ations d'entreprises en 2014
RPT-France-Hausse des 2% des cr�ations d'entreprises en 2014
Publi� le� 27-01-2015 � 10h35
Mis � jour � 10h51
A+ A-
(R�p�tition date)
PARIS, 27  janvier ( Reuters ) - Le nombre de cr�ations d'entreprises a augment� de 2,0% l'an pass� en France pour atteindre 550.700, selon les donn�es publi�es mardi par l'Insee.
En excluant les entreprises individuelles, dont les auto-entrepreneurs, la hausse est de 4,0%, � 165.700.
La hausse de 2014 compense une baisse de m�me ampleur en 2013, l'Insee soulignant que le nombre de cr�ations d'entreprises fluctue autour de 550.000 depuis 2011, apr�s le pic provoqu� par la mise en place en 2009 du r�gime de l'auto-entrepreneur.
Les immatriculations d'auto-entreprises, qui repr�sentent 74% des cr�ations d'entreprises individuelles, ont progress� l'an pass� de 3%, � 283.400.
Apr�s le repli quasi g�n�ral de 2013, les cr�ations d'entreprise ont augment� l'an pass� dans une majorit� de secteurs, plus particuli�rement dans les activit�s financi�res et d' assurance (+8%), le segment enseignement, sant� humaine et action sociale (+6%) et l'industrie (+5%).
Ce dernier secteur a b�n�fici� d'un regain d'immatriculations d'auto-entreprises (+11% apr�s -18% en 2013) li� tout particuli�rement � trois activit�s : la pr�paration � l'�dition, la fabrication d'articles de bijouterie fantaisie et la fabrication de v�tements de dessus.
Le d�veloppement des entreprises de voitures de transport avec chauffeurs (VTC) li� � l'entr�e en vigueur de la loi Th�venoud en octobre se traduit par une acc�l�ration des cr�ations dans le secteur transport et entreposage (+35% apr�s +12%).
Les rares baisses de 2014 ont concern� la construction (-3%), les "autres services aux m�nages" (-2%) et le commerce (-2%).
L'an pass�, seules 5% des entreprises �taient employeuses au moment de leur cr�ation (10% si l'on exclut les auto-entrepreneurs), avec une moyenne de 2,7 salari�s.
Pour l'�tude compl�te de l'Insee : http://www.insee.fr/fr/themes/document.asp?ref_id=ip1534
Les indicateurs fran�ais en temps r�el
Les indicateurs de la zone euro en temps r�el
LE POINT sur la conjoncture fran�aise             (Yann Le Guernigou, �dit� par Yves Clarisse)
Partager
