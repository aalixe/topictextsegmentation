TITRE: Attaque contre un h�tel � Tripoli : Paris confirme la mort d'un Fran�ais
DATE: 27-01-2015
URL: http://www.romandie.com/news/Attaque-contre-un-hotel-a-Tripoli--Paris-confirme-la-mort-dun-Francais/559746.rom
PRINCIPAL: 0
TEXT:
Tweet
Attaque contre un h�tel � Tripoli : Paris confirme la mort d'un Fran�ais
Paris - Un ressortissant fran�ais figure parmi les victimes de l'assaut lanc� mardi contre un h�tel de Tripoli par des hommes arm�s, qui a caus� au total la mort de neuf personnes, a confirm� le minist�re fran�ais des Affaires �trang�res.
La France condamne l'attaque men�e aujourd'hui � l'h�tel Corinthia de Tripoli, qui a caus� la mort de neuf personnes, dont un Fran�ais, a d�clar� le porte-parole du minist�re dans un communiqu�, sans pr�ciser l'identit� de la victime.
Neuf personnes, dont cinq �trangers, ont �t� tu�es dans un assaut de plusieurs heures lanc� contre cet h�tel par des hommes arm�s qui se sont ensuite fait exploser, avait annonc� plus t�t dans la journ�e Issam Al-Naass, porte-parole des op�rations de s�curit� dans la capitale libyenne.
Les cinq �trangers (...) �taient un Am�ricain, un Fran�ais, deux femmes de nationalit� philippine et un Sud-Cor�en, avait-il pr�cis�, ajoutant qu'ils avaient �t� tu�s par balle.
Au moment o� l'attaque �tait en cours, dans le centre de la capitale, elle a �t� revendiqu�e par la branche libyenne du groupe jihadiste Etat islamique, selon le centre am�ricain de surveillance des sites islamistes SITE.
La capitale libyenne est contr�l�e par Fajr Libya, une puissante coalition de milices notamment islamistes, qui a install� un gouvernement parall�le � Tripoli apr�s en avoir chass� le gouvernement reconnu par la communaut� internationale.
L'assaut s'est achev� en milieu d'apr�s-midi avec la mort des trois assaillants qui ont fait exploser les ceintures explosives qu'ils transportaient, alors qu'ils �taient encercl�s au 24e �tage de l'h�tel, selon Issam Al-Naass.
Alors que le repr�sentant sp�cial du secr�taire g�n�ral des Nations unies a r�uni depuis hier les parties � Gen�ve pour relancer le processus politique et faire respecter une tr�ve, nous lui apportons � nouveau notre plein soutien, a encore d�clar� le porte-parole du minist�re fran�ais des Affaires �trang�res.
(�AFP / 27 janvier 2015 23h01)
�
