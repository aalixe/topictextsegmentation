TITRE: Into the Woods de Rob Marshall: critique | CineChronicle
DATE: 27-01-2015
URL: http://www.cinechronicle.com/2015/01/into-the-woods-de-rob-marshall-critique-93069/
PRINCIPAL: 1229947
TEXT:
Into the Woods de Rob Marshall: critique
Publi� par Julie Braun le 27 janvier 2015
Synopsis�: Les intrigues de plusieurs contes de f�es bien connus se croisent afin d�explorer les d�sirs, les r�ves et les qu�tes de tous les personnages. Cendrillon, le Petit Chaperon rouge, Jack et le haricot magique et Raiponce. Tous sont r�unis dans un r�cit o� interviennent �galement un boulanger et sa femme qui esp�rent fonder une famille, mais � qui une sorci�re a jet� un mauvais sort�
�
�
Into the Woods � affiche
Rob Marshall est devenu depuis Chicago et Nine une certaine r�f�rence dans la r�alisation de com�dies musicales. Il nous le prouve une nouvelle fois en adaptant Into the Woods. Cette �uvre � succ�s, �crite par James Lapine et Stephen Sondheim, et jou�e � Broadway depuis 1986, est un m�lange de contes de f�es de notre enfance. Ainsi, Cendrillon (Ana Kendrick), Blanche Neige (Lila Crawford), Jack (Daniel Huttlestone) et Raiponce (MacKenzie Mauzy) se retrouvent dans les bois pour vivre des aventures qui vont les rapprocher, avec�au�centre de l�intrigue un boulanger et son �pouse (James Corden et Emily Blunt). L�adaptation cin�matographique, qui reprend la structure narrative du musical originel, transcende les histoires et personnages en les entrem�lant avec habilet� pour former un r�cit unique et coh�rent. Construit comme une sorte de diptyque, il propose deux parties bien distinctes, rappelant �galement l�architecture des contes de Perrault et des fr�res Grimm. Dans un premier temps, il nous d�voile des personnages classiques, �go�stes et centr�s sur leur propre existence. Mais le fil des �v�nements va bien s�r forcer les protagonistes � s�unir et � s�affirmer, laissant derri�re eux leur existence pass�e. Ce tableau est mis en �vidence gr�ce au montage impeccable de Wyatt Smith. Si � la premi�re lecture, on retrouve le manich�isme des �uvres de Disney, cette vision traditionnelle impose rapidement ses couleurs gr�ce � l�ensemble des dispositifs artistiques mis en �uvre. Tout est habilement exploit�. La photographie intense comme les d�cors surr�alistes apportent leur force, �tincelant ce pays imaginaire haut en couleurs et cette for�t aux lumi�res sombres et inqui�tantes. Mais lorsqu�on d�couvre les habitats des villageois, simples et inscrits dans une certaine r�alit�, on y per�oit d�s lors tout l�exercice m�ticuleux qui r�v�le d�mesure et technicit�.
�
James Corden, Emily Blunt et Meryl Streep dans Into the Woods de Rob Marshall
�
On appr�cie �galement d�embl�e l�humour d�cal�, ajout� au contexte volontairement dramatique, notamment dans une sc�ne dans laquelle deux fr�res, les princes charmants de Cendrillon et de Raiponce se livrent � un combat de coqs pour savoir qui est le plus beau et le plus fort. Chris Pine, vu r�cemment dans COMMENT TUER SON BOSS 2 (notre critique) , et Billy Magnussen, jouent savoureusement de leur sex-appeal se r�appropriant avec une parfaite d�sinvolture ces princes pr�cieux et ridicules. L�ajout de la morale explicite dans le d�nouement, � la mani�re des fables de La Fontaine, renforce�la dimension philosophique de Into the Woods et rappelle que les contes de f�es ont droit aussi � plusieurs degr�s de lecture. Rob Marshall parviendrait ainsi presque � nous faire oublier qu�il s�agit d�une com�die musicale, tant les dialogues sont int�gr�s aux personnages et aux situations avec simplicit� et naturel. Mention sp�ciale � Meryl Streep, qui interpr�te cette m�chante sorci�re, fausse m�re de Raiponce, revendiquant par son jeu l�inhumanit� et l�aigreur de son personnage classique. Into the Woods commet finalement la seule erreur de nous proposer dans ce tableau deux actes un peu in�gaux. Car si la premi�re partie introduit les personnages avec rythme et fra�cheur, la seconde moiti� s��tend souvent en longueur, explicitant la fin de leurs aventures. Into the woods reste cependant�une com�die musicale revigorante qui fait voyager les adultes au pays de leur �me d�enfant, et les plus jeunes dans un univers f�erique et philosophique. Un conte qui plaira ainsi tout naturellement aux petits comme aux grands�
�
�
�
INTO THE WOODS : PROMENONS NOUS DANS LES BOIS r�alis� par Rob Marshall en salles le 28 Janvier 2015.
Avec : Meryl Streep, James Corden, Emily Blunt, Anna Kendrick, Chris Pine, Johnny Depp, MacKenzie Mauzy, Christine Baranski,�Lila Crawford, Daniel Huttlestone,�Billy Magnussen�
Sc�nario�: James Lapine d�apr�s la com�die musicale Into the Woods de Stephen Sondheim et James Lapine
Production�: John DeLuca, Rob Marshall, Callum McDougall, Marc Platt
Photographie�: Dion Beebe
