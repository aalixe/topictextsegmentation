TITRE: Mattel sort une Barbie super-h�ro�ne
DATE: 27-01-2015
URL: http://www.madmoizelle.com/barbie-super-heroine-princess-power-316897
PRINCIPAL: 1228799
TEXT:
Mattel sort une Barbie super-h�ro�ne
27 janvier 2015
par Lea Bucci 16 Commentaires
Barbie in Princess Power est une super-h�ro�ne� vraiment tr�s tr�s rose.
144
Partage�sur�Facebook�! Partage�sur�Twitter�!
Chez les fabricants de super-h�ro�nes, il y avait Marvel et DC Comics. Maintenant, il va falloir compter avec Barbie. Mattel a en effet�d�voil� Barbie in Princess Power �(� Barbie en princesse pouvoir �), un nouveau mod�le de poup�e qui, comme son nom le laisse fortement deviner, est une princesse qui se d�couvre des super-pouvoirs.
� lire aussi : � My first book of Girl Power � met les super-h�ro�nes � l�honneur
Les premi�res traces du produit sur�Twitter remontent au mois de septembre, et Barbie in Princess Power a �t� pr�sent�e le 27 janvier au Salon du Jouet de Nuremberg, selon le Huffington Post. �Sur le principe, une Barbie super-h�ro�ne semble �tre une bonne id�e. Mais comme�le souligne le magazine, elle ne va pas vraiment � l�encontre des st�r�otypes de genre.
Pour commencer, les�tenues de Barbie in Princess Power�sont assez d�sesp�rantes. Il s�agit d�un jouet � pour filles �, alors en toute logique bien r�trograde, elle porte une robe rose, avec une tiare et un collier qui brillent, eux aussi roses. Lorsqu�on appuie sur le bouton de son corsage pour la transformer en super-h�ro�ne, son costume se compose d�un bustier et mini-jupe (roses), d�accessoires eux aussi roses et d�une cape dans les tons de� rose (quelle surprise).
Mais le cas de Barbie Princess in Power ne s�arr�te pas l� : Mattel la met �galement en sc�ne dans un film d�animation qui sortira au printemps 2015. Et l�histoire semble assez terrifiante de niaiserie. Barbie incarne Kara, une princesse qui se fait embrasser par un papillon magique� et paf, voil� qu�elle se d�couvre des super-pouvoirs :
� Elle devient alors Super Sparkle (Super �tincelle) et combat la criminalit� aux alentours de son royaume. Jalouse, sa cousine se transforme en son alter ego m�chant. Toutes deux deviennent alors rivales, jusqu�� ce qu�elles d�couvrent que le plus grand des pouvoirs n�est autre que l�amiti�. �
Bref, cette super h�ro�ne n�a malheureusement pas l�air d�avoir grand-chose de novateur.�Un pas en avant�
Les autres papiers parlant de Barbie , Marketing genr� , Super-h�ros
Big up
Lea Bucci Tous ses articles
L�a (pas celle qui fait des vid�os, l'autre) tra�ne son clavier sur madmoiZelle depuis la fin de l'an 2011. Elle a commenc� par collectionner les WTF mode, avant de revenir en 2015 pour causer culture, soci�t� et gens qui font des trucs tellement cools que �a ne pouvait pas rester un secret.
a lire �galement
(attention, tu dois �tre connect�e pour participer �� tu peux nous rejoindre ici !)
CaraNougat, Le mercredi 28 janvier 2015 � 15h06
Moi je trouve �a cool !
Je vais peut �tre m�me acheter le film d'ailleurs
Je ne vois pas o� est le probl�me ? C'est vrai que �a fait bcp de rose (m�me si perso j'aime beaucoup cette couleur) mais c'est sa couleur, les filles qui n'aiment pas ne l'ach�terons pas et puis basta...
Pourquoi devrait-elle s�parer le fait d'avoir des super pouvoirs et son addiction au rose et aux paillettes (qui sont devenu une marque du perso d'ailleurs) ? �a me semble un peu r�ducteur m�me
Apr�s je suis d'accord que ce n'est pas tr�s original, et je comprend que �a puisse sembler inesth�tique a certain(e)s mais c'est tout simplement Barbie
Ysia, Le mercredi 28 janvier 2015 � 15h19
@CaraNougat Y'a rose et rose, le rose c'est bien sa marque de fabrique et �a aurait �t� bizarre de ne pas en mettre, je suis d'accord. Mais l�.. euh non trop c'est trop. Si la fashion police passait dans les parages, elle serait embarqu�e direct
Spoiler
Fanncy, Le mercredi 28 janvier 2015 � 15h43
@CaraNougat : Je ne sais pas pour les autres, mais �a me d�range pas que Barbie porte du rose hein ? J'�tais moi-m�me une grosse consommatrice de la poup�e dans les ann�e 90 (mais c'est peut-�tre diff�rent de maintenant).
C'est juste que le total look rose, �a va tr�s loin et c'est dommage. Ca d�note un peu avec le c�t� suer-h�ros. Je saurais pas bien comment l'expliquer sans m'emm�ler les pinceaux et traduire ce sentiment. Qu'il y ait du rose dans son costume, oui, pourquoi pas, c'est Barbie.
Mais qu'au nom de Barbie, on lui mette tout les atouts de la princesse + ceux des super-h�ros, c'est bizarre et je comprends pas pourquoi
J'trouve que �a fait un peu euh ... pas classe, pas coh�rent.
Sans parler de r�alisme parce que c'est un dessin anim�, je trouve dommage, vraiment, qu'on r�duise syst�matiquement Barbie � une princesse (il y a m�me la princesse chanteuse
d�sol�e, �a m'attriste)  . Pour moi �a sous-entend qu'elle attend son prince charmant et que c'est un but � sa vie. Alors qu'une Barbie qui est une fille lamba, qui deviens super-h�ro�ne et qui n'a pas le total look rose, �a aurait �t� plus agr�able et moins "violent".
C'est triste qu'une petite fille doive se priver de Barbie (apr�s tout si elle aime) si elle n'aime pas le rose.
On reste dans le clich� "les filles aiment le rose/violet et les trucs qui brillent", celles qui n'aiment pas sont automatiquement exclues de cet univers.
Quand je jouais � la Barbie, j'ai pas eu le souvenir que toutes mes poup�es �taient des princesses. Y'en a m�me qui n'avaient rien de sp�cial, aucun m�tier ou position sociale �tablie par le constructeur
Mais c'�tait les ann�es 90, y'avait pas encore les dessins anim�s Barbies ou une super grosse comp�tition sur le march� de la poup�e (enfin � travers mes souvenirs d'enfant). Et puis elles n'�taient pas syst�matiquement en total look rose, y'avait plus de couleur et de diversit�.
J'suis un peu d�cousue dans mes propos parce que j'ai plein de choses qui me viennent et j'essaye de tout mettre en ordre. Je critique pas le fait que tu puisse aimer ce dessin anim� en particulier, vraiment pas
Je pense juste qu'il y a un meilleur moyen d'exploiter Barbie et son univers, sans abuser du rose et de la "princessitude"
Steffie38, Le mercredi 28 janvier 2015 � 16h12
Alors oui Barbie = rose, c'est son truc ok !! Mais est-ce qu'on peut parler du costume sans parler de la couleur... C'est quoi ce costume ??????
Et la cape, s�rieux elle est immonde, elle ne ressemble � rien !!! Ils auraient pu faire des efforts et essayer de lui faire une tenue de super-h�ro�ne plus sympa quand m�me, parce que l� pour moi c'est juste Barbie qui va en soir�e, et qui a mis un truc bizarre dans son dos (cape? ailes de papillon en tissu?...). Dommage l'id�e aurait pu vraiment �tre sympa...
CaraNougat, Le mercredi 28 janvier 2015 � 16h44
@Ysia : Oui, c'est aussi ce que j'ai �crit et je comprends que tu aies cette opinion ^^
Cependant je pense que ses utilisatrices s'y feront bien vite x)
Je commence moi m�me a m'y habituer
Et, depuis quelque temps au moins, j'ai l'impression que sa marque de fabrique n'est pas simplement le rose
mais aussi l'omnipr�sence de cette couleur :/
@Fanncy : Oui, je suis d'accord que c'est s�rement "trop" de rose et ca ne semble pas forc�ment super coh�rent mais apr�s; elle est tr�s souvent comme �a, c'est �a l'univers Barbie maintenant... :/
Moi aussi �a me g�ne un peu qu'elle soit presque syst�matiquement une princesse. Mais ce n'est pas toujours le cas et je ne pense pas que �a ait � avoir avec le message du prince charmant. Pour avoir vu le film correspondant a la poup�e dont tu parles, il est quasiment absent du film et c'est comme �a dans bien d'autres :/
Je pense que c'est juste li� � l'id�e qu'une princesse est droite, gracieuse et peut porter des v�tements styl�s qui feront vendre la poup�e
Mais pour en revenir a la poup�e et a l'article, je trouve juste �a un peu d�plac� de reprocher a un film Barbie de sembler niais (enfin... Barbie quoi
) et de reprocher a cette poup�e de ne pas aller assez a l'encontre des st�r�otypes de genre; parce que tout ce que je vois c'est Barbie fid�le a elle-m�me (rose a l'infini et paillet�e) sauver le monde~!
C'est bien non, qu'on n'ait pas a renoncer a notre c�t� girly pour sauver le monde ? x)
M�me si son "elle-m�me" outre les fashionistas. Je ne trouve pas �a cool de faire la morale a une super h�ro�ne parce qu'elle porte une robe de princesse ou parce qu'elle porte plus que la moyenne le rose :/
Ceci �tant dit, je ne pense pas non plus que ce costume soit g�nial mais je ne rapporterais pas �a au f�minisme x)
J'ai l'impression qu'on en attend trop de cette poup�e et qu'on la descend trop facilement en g�n�ral :/
C'est juste une actrice accro au fuchsia qui a bien r�ussi
Lilimaow, Le mercredi 28 janvier 2015 � 19h37
Elle est nulle, m�me pas articul�e pour lui faire faire la bagarre ou monter sa super licorne magique.... Pi d'abord les super-h�ros ils portent des collants moulants avec leur slip par dessus!!! sisisi!!!
Poulpichon, Le mercredi 28 janvier 2015 � 21h08
Mais elle est carr�ment fam�lique Barbie dans le Clip!!
Qu'on apporte un sandwich � cette poup�e!!
Olimeli, Le jeudi 29 janvier 2015 � 11h47
la marque de fabrique de barbie c est d etre blanche blonde et en rose donc ca ne me choque pas... je trouve l id�e de la cape et du personnage de super heroine sympa, dommage qu il faille attendre 2015... n oublions pas qu il y a barbie business woman ou veterinaire ou encore fait de l equitation y a ps que la danse ou la sirene elle est multi facette...
Solstice, Le jeudi 29 janvier 2015 � 13h58
� Elle devient alors Super Sparkle (Super �tincelle) et combat la criminalit� aux alentours de son royaume. Jalouse, sa cousine se transforme en son alter ego m�chant. Toutes deux deviennent alors rivales, jusqu�� ce qu�elles d�couvrent que le plus grand des pouvoirs n�est autre que l�amiti�. �
Ho le rire nerveux que j'ai eu en lisant �a (et en voyant les images (et en regardant les clips)). Bon je passe outre la panoplie rose et tout hein (
) mais j'avoue que �a me les brise assez joyeusement qu'on prenne les enfants pour des idiots en leur vendant une soupe pareille. Je vois pas comment on peut prendre les enfants pour autre chose que des vaches � laits avec un sc�nario tel que celui-ci. Ils essayent M�ME PAS quoi c'est dingue. On retire tout ce qui fait le sel et l'int�r�t des super-h�ros.h�ro�nes (ben oui �a pourrait choquer nos petits ch�rubins vous pensez) pour vendre un truc ultra format�. Bravo bravo.
(Je suis sans doute jamais contente mais �a me tape aussi sur le syst�me aussi ces histoires de princesses. Evidemment c'est la fille p�t�e de thunes qui chope tous les pouvoirs. J'attends "Barbie utopie marxiste" avec impatience.
)
French-scarlet, Le jeudi 29 janvier 2015 � 17h55
Ah oui quand m�me, c'est Pink-land l� quand m�me!
Je suis sure cependant que les petites filles vont l'adorer avec sa grande cape (rose). Mais ils ne se sont pas beaucoup creus� la t�te niveau look, un peu de parme ou de rouge � la Wonder Woman ne lui aurait pas fait de mal � Barbie!
Non mais oh les gars, on se r�veille et on re-design �a fissa!
(attention, tu dois �tre connect�e pour participer �� tu peux nous rejoindre ici !)
Les dossiers
