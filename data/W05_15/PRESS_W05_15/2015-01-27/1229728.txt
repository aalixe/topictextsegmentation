TITRE: Plus de 80 bless�s au Kosovo lors d'une manifestation contre un ministre serbe - rts.ch - Monde
DATE: 27-01-2015
URL: http://www.rts.ch/info/monde/6494349-plus-de-80-blesses-au-kosovo-lors-d-une-manifestation-contre-un-ministre-serbe.html
PRINCIPAL: 1229726
TEXT:
Mise � jour le 27 janvier 2015
Plus de 80 bless�s au Kosovo lors d'une manifestation contre un ministre serbe
Plusieurs bless�s lors de manifestations au Kosovo Le Journal en continu / 1 min. / Le 27 janvier 2015
Vid�os et audio
le 27 janvier 2015
De violents heurts � Pristina, capitale du Kosovo, ont fait plus de 80 bless�s mardi lors d'une manifestation contre un ministre serbe accus� d'avoir insult� les Albanais kosovars.
Des incidents ont fait mardi au moins 80 bless�s, plus de 50 dans les rangs des forces de l'ordre kosovares lors d'accrochages avec plusieurs milliers de manifestants kosovars qui r�clamaient la d�mission d'un ministre serbe accus� d'avoir insult� les Albanais kosovars.
Quelques-uns des 10'000 manifestants ont jet� des pierres contre la police qui a ripost� en utilisant du gaz lacrymog�ne lors de ces heurts devant le si�ge du gouvernement � Pristina.
Deuxi�me manifestation en quatre jours
Il s'agit de la deuxi�me manifestation depuis samedi � Pristina pour r�clamer la d�mission du serbe kosovar, Aleksandar Jablanovic, ministre du Travail et� l'un des trois ministres serbes du cabinet du Premier ministre Isa Mustafa.
"Jablanovic dehors" et "� bas le gouvernement", scandaient les manifestants pour qui le ministre a insult� la majorit� albanaise du Kosovo, territoire qui a proclam� son ind�pendance de la Serbie en 2008.
afp/hend
Albanais trait�s de "sauvages"
Aleksandar Jablanovic a provoqu� la col�re des Albanais lorsqu'il a qualifi� de "sauvages" des manifestants albanais qui avaient emp�ch�, il y a deux semaines, un groupe de Serbes de visiter un monast�re � l'occasion du No�l orthodoxe dans l'ouest du Kosovo sous le pr�texte que des "criminels de guerre" se trouvaient parmi les p�lerins.
Le ministre s'est publiquement excus�, mais ses propos ont n�anmoins provoqu� plusieurs manifestations d'Albanais, qui repr�sentent plus de 90% des 1,8 millions d'habitants du Kosovo.
120'000 Serbes rest�s au Kosovo
Sur quelque 120'000 Serbes rest�s au Kosovo, environ 40'000 vivent dans le nord limitrophe de la Serbie et 80'000 dans des enclaves �parpill�es sur le territoire peupl� essentiellement d'Albanais.
Les �changes entre Belgrade et Pristina se sont multipli�s depuis l'accord conclu en 2013 visant � la normalisation des relations bilat�rales, parrain� par l'Union europ�enne.
Pristina, capitale du Kosovo
