TITRE: Logements neufs : les mises en chantier au plus bas depuis 1997 - Capital.fr
DATE: 27-01-2015
URL: http://www.capital.fr/immobilier/actualites/logements-neufs-les-mises-en-chantier-au-plus-bas-depuis-1997-1008335
PRINCIPAL: 1228201
TEXT:
Les services recommand�s par Capital.fr
Immobilier neuf et d�fiscalisation
...Voir tous nos services
2014 a �t� une ann�e catastrophique pour la construction de logements neufs. Pour le gouvernement, le d�fi est d�sormais d'arriver � relancer un secteur en panne s�che.
Les grues sont � l'arr�t. L�an pass� seuls 297.532 logements ont �t� mis en chantier en France, soit un plus bas depuis 1997, selon les chiffres publi�s mardi par le minist�re de l'Ecologie et du D�veloppement durable.
Pas mieux du c�t� des permis de construite d�livr�s, qui ont atteint p�niblement les 381.075. Soit le plus faible niveau depuis 1999.
Par rapport � 2013, les mises en chantier accusent un recul de 10,3% et les permis de construire de 12,0%.
Dans ce contexte, l�ann�e 2015 s�annonce comme celle de tous les d�fis pour le gouvernement. Plusieurs mesures ont bien �t� annonc�es pour relancer la machine. Ainsi, le nouveau dispositif Pinel , vot�e en fin d�ann�e, doit redonner un �lan aux ventes aux investisseurs. Mais certaines promesses tardent � entrer en vigueur : les professionnels d�noncent notamment les retards pris sur les fameuses simplifications des normes de construction. Sur les 50 all�gements de normes que le couple Valls-Pinel avait promis en fin d�ann�e, � peine 10 sont entr�s en vigueur : limitation des normes parasismiques, pr�vention contre les termites...�
� Capital.fr (avec Reuters)
