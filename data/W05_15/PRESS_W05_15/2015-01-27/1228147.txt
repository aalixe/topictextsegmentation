TITRE: Succ�s kurde � Koban�: Erdogan ne veut pas d'un Kurdistan en Syrie
DATE: 27-01-2015
URL: http://www.romandie.com/news/Succes-kurde-a-Kobane-Erdogan-ne-veut-pas-dun-Kurdistan-en-Syrie/559366.rom
PRINCIPAL: 1228141
TEXT:
Tweet
Succ�s kurde � Koban�: Erdogan ne veut pas d'un Kurdistan en Syrie
La Turquie ne veut pas en Syrie d'une zone kurde autonome comme celle existant en Irak, a d�clar� Recep Tayyip Erdogan. Le pr�sident turc est cit� mardi par la presse, au lendemain de la victoire des Kurdes syriens qui ont affirm� avoir chass� de Koban� les djihadistes de l'EI.
"Nous ne voulons pas une (r�p�tition) de la situation en Irak (...) c'�tait le nord de l'Irak. Nous ne pouvons accepter la naissance maintenant du nord de la Syrie", a d�clar� le pr�sident turc � un groupe de journalistes. Il s'est exprim� dans l'avion le ramenant � Ankara au terme d'une tourn�e en Afrique.
"Nous devons conserver notre position � ce sujet, sinon ce sera un nord de la Syrie comme un nord de l'Irak. Cette entit� est source de gros ennuis dans l'avenir", a dit l'homme fort de Turquie, cit� par le quotidien "H�rriyet".
Les combattants kurdes syriens ont annonc� lundi avoir obtenu une victoire d�cisive. Ils ont affirm� avoir chass� de Koban� les djihadistes du groupe Etat islamique (EI), lequel occupait cette ville syrienne depuis septembre dernier.
Depuis le d�but de la bataille, les combats pour la ville ont fait plus de 1800 morts, dont plus de 1000 dans les rangs djihadistes. Ce bilan a �t� transmis par l'OSDH (Observatoire syrien des droits de l'Homme), une ONG bas�e � Londres disposant d'un large r�seau de sources civiles, militaires et m�dicales en Syrie.
Position ambigu�
La Turquie a adopt� une position ambigu�. Ankara a refus� de participer � la coalition internationale men�e par les Etats-Unis contre les jihadistes en Irak et Syrie et de renforcer le camp des Kurdes de Syrie.
Recep Tayyip Erdogan avait qualifi� de "terroriste" le principal parti kurde de Syrie (PYD), � la pointe du combat contre l'EI. Il utilise le m�me qualificatif pour le mouvement fr�re du Parti des travailleurs du Kurdistan (PKK) qui m�ne depuis 1984 la gu�rilla sur le sol turc.
Inqui�tude turque
Press� par ses alli�s d'intervenir, le r�gime islamo-conservateur d'Ankara a finalement fait un geste. A fin octobre, il avait autoris� le passage par son territoire d'un contingent de kurdes irakiens pour renforcer la d�fense de Koban�.
Ankara redoute qu'une victoire des Kurdes � Koban� par le soutien des rais a�riens des avions de la coalition soit synonyme d'une ind�pendance de la partie kurde de la Syrie, aux fronti�res de son pays.
"Zone d'exclusion a�rienne"
M. Erdogan a une nouvelle fois d�fendu devant les journalistes sa th�se d'une "zone d'exclusion a�rienne" et d'une "zone de s�curit�" � la fronti�re syrienne. Il a r�affirm� sa farouche hostilit� au r�gime du pr�sident Bechar al-Assad ainsi qu'� l'administration de cantons mis en place par les Kurdes syriens.
La d�faite de l'EI � Koban� a provoqu� un mouvement de liesse dans le sud-est turc, frontalier de l'Irak et de la Syrie et peupl� majoritairement de Kurdes. Des milliers de personnes sont descendues dans les rues des principales villes comme Diyarbakir et Hakkari mais aussi � Istanbul pour f�ter la victoire, ont rapport� les m�dias.
(ats / 27.01.2015 10h25)
