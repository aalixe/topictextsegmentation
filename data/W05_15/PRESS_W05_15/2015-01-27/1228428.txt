TITRE: La croissance de l'e-commerce port�e par les "places de march�"
DATE: 27-01-2015
URL: http://www.latribune.fr/entreprises-finance/services/distribution/20150127trib7554eeee5/la-croissance-de-l-e-commerce-porte-par-les-places-de-marche.html
PRINCIPAL: 0
TEXT:
OR 1 185,3$ -1,00                 %
La croissance de l'e-commerce port�e par les "places de march�"
Le chiffre d'affaires de l'e-commerce ne repr�sente "que" 9% de celui du commerce de d�tail.�A titre de comparaison, la Banque de France signale une progression de seulement 1% du commerce de d�tail dans l'Hexagone. (Cr�dits : reuters.com)
Marina Torre �|�
27/01/2015, 12:36
�-� 489 �mots
Le chiffre d'affaires du commerce �lectronique en France a atteint quasiment 57 milliards d'euros en 2014, soit 11% de plus qu'en 2013 et plus que les 56 milliards initialement anticip�s. Une croissance au rythme relativement moins soutenu que les ann�es pr�c�dentes, port�e notamment par les ventes sur les places de march� virtuelles.
sur le m�me sujet
Abonnez-vous � partir de 1�
Certes, le rythme de la croissance s'essouffle dans l'e-commerce. Mais il reste largement plus soutenu que celui du commerce de d�tail. L'an dernier, les cyber-marchands ont g�n�r� en France un chiffre d'affaires de 56,8 milliards d'euros,�selon l'enqu�te�de la F�d�ration e-commerce et vente � distance (Fevad)�publi�e ce 27 janvier et qui porte sur les donn�es de 157.000 entreprises.�Le chiffre d'affaires de l'e-commerce ne repr�sente "que" 9% de celui du commerce de d�tail. A titre de comparaison, la Banque de France signale une progression de seulement 1% du commerce de d�tail dans l'Hexagone.
Dans l'e-commerce, le chiffre d'affaires a augment� de 11% par rapport � 2014. Entre 2012 et 2013, l'augmentation du chiffre d'affaires �tait encore plus �lev�e: 13,5%. Autre signe d'une forme d'arriv�e � "maturit�" du march� : le panier moyen par transaction se r�duit depuis quatre ans pour atteindre environ 80 euros. Mais ce mouvement est compens� par une augmentation du�nombre de transactions. Celles-ci augmentent de 15% entre 2013 et 2014, passant � 700 millions.
No�l dynamique
En septembre, la Fevad tablait sur une croissance de 56 milliards d'euros en 2014. Entre-temps, une saison de No�l particuli�rement dynamique pour les sites marchands a�contribu� � ce que ces attentes soient d�pass�es. Ce, en d�pit de craintes de retards qui poussent � r�aliser de vastes op�rations de promotions import�s des pays anglo-saxons comme les "black friday" et "cyber monday".
En effet, les ventes r�alis�es � cette p�riode, qui repr�sentent plus de 20% du chiffre d'affaires totales des e-commer�ants, ont augment� plus rapidement que le reste de l'ann�e (13%).
Point notable: les ventes sur les "places de march�", ces plateformes d�di�es et d�sormais ces espaces mis � disposition d'autres commer�ants par de�grands sites marchands pour mon�tiser leur trafic ont cr� de 53%. Ils repr�sentent d�sormais pr�s d'un quart du volume d'affaires total des sites qui louent ce type d'espaces. D'ailleurs Amazon a m�me d�pass� eBay, pourtant plateforme uniquement d�di�e � cette activit�, comme premi�re place de march� de France.
62 milliards pr�vus en 2015
Enfin, m�me si la croissance se tasse et que le panier moyen par achat diminue, les perspectives restent plut�t positives pour le secteur. Notamment parce que les commer�ants "connect�s", sont plus nombreux que les autres � d�velopper leur activit� � l'�tranger.
"Il se cr�� encore un site marchand toutes les demi-heures en France", rappelle Marc Lolivier, d�l�gu� g�n�ral de la�F�d�ration e-commerce et vente � distance. Pour 2015, la barre des 60 milliards d'euros de chiffre d'affaires pourrait �tre d�pass�e. Surtout si, comme le note Carole Delga, la secr�taire d'Etat au Commerce, "les habitudes reprennent pour des Fran�ais qui se sont montr�s�plus citoyens que consommateurs" juste apr�s�les attentats � Paris d�but janvier. Ev�nements qui ont apparemment� surtout d�tourn� des acheteurs �des magasins physiques.
L'auteur
