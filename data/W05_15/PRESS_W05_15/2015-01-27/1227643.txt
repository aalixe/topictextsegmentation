TITRE: Macron: 'La place de la Grèce est dans la zone euro'
DATE: 27-01-2015
URL: http://www.lefigaro.fr/flash-eco/2015/01/27/97002-20150127FILWWW00065-macron-la-place-de-la-grece-est-dans-la-zone-euro.php
PRINCIPAL: 1227642
TEXT:
le 27/01/2015 à 08:19
Publicité
Emmanuel Macron , invité de la matinale sur Europe 1, revient sur les suites de l'élection en Grèce , qui a consacré la gauche radicale dimanche . Pour lui, "la place de la Grèce est dans la zone euro (...). Tsipras (le nouveau premier ministre grec, NDLR) a été élu dans un pays ou plus des trois quarts des Grecs ont rappelé leur attachement à l'euro". "Comment en est-on arrivé là ? Pendant des décennies, les gouvernements ont menti à leur peuple, ont protégé des grandes familles, des intérêts acquis. La nouvelle génération est plus libre avec ces intérêts acquis", estime le ministre de l'Économie français.
Désormais, selon lui, "les engagements pris par la Grèce en tant que nation souveraine doivent être tenus en tant que nation souveraine", ajoute-t-il, dans le même message de fermeté soufflé par les pays autres membres de la zone euro.
Quant à la France, "elle n'a rien demandé à Bruxelles. La France est dans un dialogue constructif avec Bruxelles pour l'intérêt européen". Il explique : "On aurait pu faire un plan de relance français mais ce n'est pas possible au regard de nos comptes publics. D'abord, on restaure la confiance, et on demande aux Allemands d'investir davantage". Il appelle l'Europe à mener des "politiques macroéconomiques plus adaptées et une politique d'investissement ambitieuse (...). Le plan Juncker est un bon début."
 
