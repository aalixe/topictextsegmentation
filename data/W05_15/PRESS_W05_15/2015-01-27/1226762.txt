TITRE: "Nutella", "Fraise" et autres pr�noms loufoques � �viter | Site mobile Le Point
DATE: 27-01-2015
URL: http://www.lepoint.fr/justice/nutella-fraise-et-autres-prenoms-loufoques-a-eviter-26-01-2015-1899809_2386.php
PRINCIPAL: 1226759
TEXT:
27/01/15 � 06h29
"Nutella", "Fraise" et autres pr�noms loufoques � �viter
Un juge des affaires familiales a rejet� l'attribution d'un pr�nom � un b�b�, estimant qu'il portait atteinte aux int�r�ts de l'enfant.
Un couple souhaitait pr�nommer son enfant "Nutella". JUSTIN SULLIVAN / GETTY IMAGES NORTH AMERICA / AFP
Source AFP
Ces derni�res semaines, des magistrats du palais de justice de Valenciennes (Nord) ont rejet� l'attribution des pr�noms "Nutella" et "Fraise" � des nouveau-n�s, estimant qu'ils portaient atteinte aux int�r�ts de l'enfant, selon une source judiciaire.
Un officier de l'�tat civil aurait alert� l'autorit� judiciaire qu'un couple souhaitait baptiser son enfant, n� le 24 septembre, du nom de la c�l�bre p�te � tartiner au chocolat et � la noisette, selon le parquet de Valenciennes, confirmant une information de La Voix du Nord.
Estimant que le pr�nom choisi n'�tait "pas conforme" � l'int�r�t de l'enfant, l'officier d'�tat civil a saisi le procureur de Valenciennes, lequel a saisi un juge des affaires familiales pour que le pr�nom soit supprim� des registres de l'�tat civil, a ajout� le parquet.
"Ram�ne ta fraise"
Lors d'une audience � laquelle les parents n'ont pas assist�, le juge a choisi de renommer l'enfant Ella.
Un second couple avait donn� naissance � une petite fille qu'il souhaitait pr�nommer Fraise. Devant l'avis contraire du tribunal, qui a craint r�cemment en rendant son d�lib�r� les "moqueries" susceptibles d'�tre occasionn�es par ce patronyme, citant notamment l'expression "ram�ne ta fraise", et les "r�percussions n�fastes" sur l'enfant, les parents ont finalement opt� pour Fraisine, un pr�nom ancien, donn� au XIXe si�cle.
