TITRE: Benedict Cumberbatch s'excuse apr�s ses propos pol�miques - Closermag.fr
DATE: 27-01-2015
URL: http://www.closermag.fr/people/people-anglo-saxons/benedict-cumberbatch-s-excuse-apres-ses-propos-polemiques-460608
PRINCIPAL: 1229807
TEXT:
Accueil > Actu People > People Anglo-saxons > Benedict Cumberbatch s'excuse apr�s ses propos pol�miques
Benedict Cumberbatch s'excuse apr�s ses propos pol�miques
News publi�e le  27/01/2015 � 18h23
Benedict Cumberbatch lors de la c�r�monie des Oscars 2014
� Copyright  A.M.P.A.S./ RICHARD HARBAUGH
Benedict Cumberbatch et Jennifer Garner lors de la c�r�monie des Oscars 2014
� Copyright  A.M.P.A.S. / MICHAEL YADA
Benedict Cumberbatch � l'affiche du film Imitation Game
� Copyright  FILMNATION ENTERTAINMENT / JACK ENGLISH
Agrandir
Imprimer la page
Alors qu'il est en course afin de remporter l'Oscar du meilleur acteur, Benedict Cumberbatch est aujourd'hui sous le feu des critiques. En cause, des propos que beaucoup ont jug�s racistes et dont le com�dien s'est depuis excus�.
C'est une pol�mique dont Benedict Cumberbatch se serait bien pass�. En lice afin de remporter l'oscar du meilleur acteur pour son r�le dans Imitation Game, le com�dien s'est attir� les foudres des t�l�spectateurs de PBS. En effet, invit� sur le plateau de l'animateur Tavis Smiley, Benedict Cumberbatch a d�clar� que les acteurs "de couleur" avaient plus de chance de percer aux Etats-Unis qu'en Angleterre. En utilisant le terme "de couleur" afin de d�signer les acteurs noirs, le com�dien s'est attir� les foudres des internautes qui ont estim� que ses propos �taient racistes.
Sous le feu des critiques, Benedict Cumberbatch a tenu � pr�senter ses excuses. Interrog� par le magazine People, le com�dien s'est dit "d�vast�" d'avoir offens� des gens en utilisant ce terme qu'il qualifie de "d�suet". "Je n'ai aucune excuse pour avoir �t� aussi idiot. Le mal est fait", poursuit l'acteur britannique. Et d'ajouter : "Je me sens compl�tement idiot et je suis d�sol� d'avoir offens� des gens. Je vous assure que j'ai appris de mes erreurs. Je m'excuse une nouvelle fois envers tous ceux que j'ai offens�s en utilisant un terme inappropri� afin de parler d'un sujet qui affecte mes amis et qui me tient r�ellement � c�ur". Le message est pass�. Il ne reste plus � esp�rer pour l'acteur que cette pol�mique n'aura aucune cons�quence dans la course aux Oscars .
