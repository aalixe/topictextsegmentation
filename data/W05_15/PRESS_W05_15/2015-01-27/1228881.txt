TITRE: Le CII appelle � la fin de la stigmatisation de la l�pre
DATE: 27-01-2015
URL: http://www.infirmiers.com/profession-infirmiere/competences-infirmiere/mettre-fin-a-la-stigmatisation-de-la-lepre.html
PRINCIPAL: 1228880
TEXT:
Suivant
A l'occasion de la Journ�e mondiale des l�preux 2015, le Conseil international des infirmi�res (CII) appelle � la fin de la stigmatisation de la l�pre qui entra�ne un rejet social des personnes affect�es, m�me apr�s leur gu�rison.
Cr�dit Nippon Foundation - Principal groupe de professionnels de sant�, les infirmi�res s'engagent, sans parti-pris, � rem�dier aux souffrances physiques et mentales de tous les patients et parmi eux, les personnes atteintes de la l�pre.
La Journ�e mondiale des l�preux est traditionnellement organis�e le dernier week-end de janvier, soit pour 2015 les 24 et 25 janvier. L�Organisation mondiale de la Sant� indique que pr�s de 190 000 personnes sont touch�es par la l�pre, une maladie chronique qui affecte essentiellement la peau, les nerfs p�riph�riques, la muqueuse des voies respiratoires sup�rieures et les yeux. Selon l'OMS, l'Inde reste le pays le plus touch� (pr�s de 127.000 nouveaux cas en 2013), devant le Br�sil (31.000 cas), l'Indon�sie (pr�s de 17.000 cas) et deux pays africains, l'Ethiopie et la R�publique d�mocratique du Congo (entre 3.500 et 4.500 nouveaux cas).
Gu�rissable, la l�pre a �t� cependant �radiqu�e dans 119 pays sur 122. Pourtant, mythes et pr�jug�s continuent de pr�valoir au sujet de cette maladie. Or la l�pre n�est pas h�r�ditaire, et c�est une maladie � incubation longue provoqu�e par le bacille Mycobacterium leprae, qui est acido-r�sistant et de forme allong�e. La maladie touche principalement la peau, les nerfs p�riph�riques, la muqueuse des voies respiratoires sup�rieures ainsi que les yeux. Cependant, rappelons-le, la l�pre est une maladie gu�rissable et un traitement pr�coce permet d��viter les incapacit�s. La polychimioth�rapie (PCT), traitement que l�Organisation mondiale de la Sant� (OMS) met gratuitement � la disposition de tous les sujets atteints dans le monde depuis 1995, repr�sente un moyen curatif aussi simple qu�efficace pour tous les types de l�pre.
190 000 personnes aujourd'hui sont touch�es par la l�pre de par le monde. Gu�rissable, la l�pre a �t� �radiqu�e dans 119 pays sur 122.
A l'occasion de cette journ�e mondiale des l�preux, le Conseil international des infirmi�res (CII)1 annonce s��tre associ� �
l�Appel mondial 2015
pour faire cesser la stigmatisation et la discrimination des personnes atteintes de la l�pre ; Appel lanc� lors d'une c�r�monie organis�e par la Nippon Foundation , � Tokyo. Cet Appel a pour objet, d�une part, de mieux faire comprendre aux 16 millions d�infirmi�res dans le monde, qui travaillent en contact �troit avec des personnes affect�es par la l�pre, ce qu�est cette maladie ; et, d�autre part, de les encourager � participer aux initiatives contre la stigmatisation et la discrimination associ�es � la l�pre. Comme l'a rappel� la Pr�sidente du CII, le Dr Judith Shamian dans un communiqu�,
cette stigmatisation entra�ne le rejet social des personnes affect�es par la l�pre, m�me apr�s leur gu�rison : des familles enti�res sont ainsi marginalis�es, un probl�me aux cons�quences parfois d�sastreuses. En tant que principal groupe de professionnels de sant�, les infirmi�res s�engagent, sans parti-pris, � rem�dier aux souffrances physiques et mentales des patients ainsi qu�� promouvoir la sant� des populations.
En s�associant � l�Appel, le CII affirme le droit des personnes affect�es par la l�pre de recevoir un traitement et des soins et demande que l�on mette fin � la discrimination dont elles sont victimes, ainsi que leurs familles. Le CII d�fend en effet le droit de toute personne affect�e par la l�pre de vivre dans la dignit� en tant que membre � part enti�re de la communaut�, jouissant d�un acc�s �quitable � tous ses droits fondamentaux. La c�r�monie a �t� suivie d�un colloque international traitant des soins infirmiers et m�dicaux contre la l�pre, des r�ponses soci�tales � historiques et contemporaines � � la maladie, et de la dimension historique du probl�me. Des personnes atteintes de la l�pre, ainsi que des pr�sidents des associations d'infirmi�res de plusieurs pays et des infirmi�res et d'autres professionnels engag�s dans les soins aux victimes de cette maladie y ont activement particip�.
Note
1- Le Conseil international des infirmi�res (CII) est une f�d�ration de plus de 130 associations nationales d�infirmi�res, repr�sentant plusieurs millions d�infirmi�res dans le monde entier. G�r� par des infirmi�res et � l�avant-garde de la profession au niveau international, le CII �uvre � promouvoir des soins de qualit� pour tous et de solides politiques de sant� dans le monde.
Bernadette FABREGASR�dactrice en chef Infirmiers.com
Cette adresse e-mail est prot�g�e contre les robots spammeurs. Vous devez activer le JavaScript pour la visualiser.
