TITRE: Neuf soldats ukrainiens tu�s en un jour
DATE: 27-01-2015
URL: http://www.romandie.com/news/Neuf-soldats-ukrainiens-tues-en-un-jour/559331.rom
PRINCIPAL: 1228353
TEXT:
Tweet
Neuf soldats ukrainiens tu�s en un jour
Neuf soldats ukrainiens ont �t� tu�s et 29 autres bless�s au cours des derni�res 24 heures, a annonc� mardi matin le porte-parole de l'arm�e ukrainienne Vladislav Selezniov. Ces militaires �taient engag�s dans des combats contre les forces s�paratistes pro-russes dans l'est du pays.
Les affrontements en cours en Ukraine sont les plus violents depuis la signature d'un cessez-le-feu en septembre dernier. Ils ensanglantent en particulier le secteur de Debaltseve, une ville situ�e au nord-est du bastion rebelle de Donetsk.
Le soul�vement s�paratiste dans l'est de l'Ukraine a d�but� en avril dernier. Il a fait plus de 5000 morts selon le plus r�cent bilan des Nations unies.
(ats / 27.01.2015 09h38)
