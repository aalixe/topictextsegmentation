TITRE: 'Facebook Lite' : une version pour les pays �mergents, et la 2G
DATE: 27-01-2015
URL: http://www.zdnet.fr/actualites/facebook-lite-une-version-pour-les-pays-emergents-et-la-2g-39813648.htm
PRINCIPAL: 0
TEXT:
RSS
'Facebook Lite' : une version pour les pays �mergents, et la 2G
Application : Tr�s l�g�re, cette application qui int�gre Messenger, est disponible pour le moment dans certains pays d'Afrique et d'Asie. Objectif : offrir (� nouveau) l'exp�rience du r�seau social en bas d�bit. Car Facebook Lite existait d�j� en 2009...
Par Olivier Chicheportiche |
Mardi 27 Janvier 2015
Facebook a toujours cherch� � convertir les mobinautes des pays �mergents avec des versions all�g�es de son application, permettant de fonctionner avec des r�seaux bas d�bit. En 2009, le g�ant lan�ait aux Etats-Unis et en Inde, Facebook Lite qui mettait met l'accent sur la simplicit� en ne conservant que  l'essentiel�: profil, messages, moteur de recherche, �v�nements.
Un an plus tard , le roi des r�seaux social stoppait l'exp�rience, sans expliquer son choix, pr�f�rant miser sur sa version mobile , all�g�e �galement, et fortement utilis�e dans les pays �mergents o� le haut d�bit fixe est quasiment inexistant.
Sans oublier Facebook Zero version texte du service qui permet un  acc�s plus rapide depuis les t�l�phones mobiles et, notamment, des mod�les ne poss�dant pas de grands �crans.
Mais aujourd'hui, Facebook relance Lite pour les mobiles. L� encore, il s'agit de proposer une application Android l�g�re (250 ko) offrant les fonctions principales, et Messenger (qui rappelons-le est d�sormais s�par� du Facebook classique sur smartphone), ainsi que des notifications.
Afin de fonctionner sur les r�seaux 2G, l'application est optimis�e pour consommer moins de donn�es sans d�grader l'exp�rience et l'ergonomie, finalement assez proche de la version classique. Elle est bas�e sur Snaptu, un client multi-services en Java support� par la grande majorit� des t�l�phones.
Facebook Lite est pour le moment disponible que dans certains pays d'Asie et d'Afrique (Bangladesh, N�pal, Nig�ria, Afrique du Sud, Soudan, Sri Lanka, Vietnam, Zimbabwe).
