TITRE: Un avion de flydubai touch� par un tir avant son atterrissage � Bagdad
DATE: 27-01-2015
URL: http://www.romandie.com/news/Un-avion-de-flydubai-touche-par-un-tir-avant-son-atterrissage-a_RP/559489.rom
PRINCIPAL: 0
TEXT:
Tweet
Un avion de flydubai touch� par un tir avant son atterrissage � Bagdad
Duba� - Un avion de la compagnie � bas co�ts flydubai a �t� touch� par un tir avant son atterrissage � l'a�roport de Bagdad, poussant plusieurs compagnies a�riennes � suspendre leurs vols vers la capitale irakienne, selon des responsables mardi.
Le fuselage de l'appareil assurant le vol FZ215 a �t� touch� lundi par une balle de petit calibre mais tous les passagers ont pu d�barquer sains et saufs, a pr�cis� le porte-parole de flydubai.
L'avion a pu se poser normalement, a indiqu� un officier des services de s�curit� de l'a�roport international de Bagdad. Il n'�tait pas possible dans l'imm�diat de pr�ciser l'origine du tir.
Apr�s cet incident, l'autorit� de l'aviation civile aux Emirats arabes unis a d�cid� de suspendre les vols vers Bagdad des quatre compagnies du pays: flydubai, Emirates, Etihad et Air Arabia.
Emirates, la plus grosse compagnie �miratie, a confirm� dans un communiqu� avoir suspendu ses vols sur Bagdad pour des raisons op�rationnelles.
De son c�t�, Etihad a indiqu� avoir suspendu ses vols en direction de la capitale irakienne jusqu'� nouvel ordre.
Un responsable de la Turkish Airlines a indiqu� � l'AFP que la compagnie avait suspendu ses vols sur Bagdad mardi pour des raisons op�rationnelles.
Les responsables de la Royal Jordanian �taient r�unis � Amman pour �valuer la situation.
A Beyrouth, le Pdg de la Middle East Airlines (MEA), Mohammad al-Hout, a indiqu� � l'AFP que le vol quotidien Beyrouth-Bagdad de mardi a �t� annul�.
Pour le vol de mercredi nous allons �valuer la situation et voir quelles mesures vont �tre prises apr�s l'incident. Tous les autres vols de la MEA � destination d'autre villes en Irak sont maintenus, a-t-il ajout�.
Les grosses compagnies a�riennes survolant le territoire irakien redoublent de pr�cautions par crainte que des membres du groupe Etat islamique (EI) aient acquis des armes capables de toucher leurs appareils.
L'a�roport de Bagdad est situ� � l'ouest de la ville, pr�s de la province d'Al-Anbar, largement contr�l�e par les jihadistes de l'EI.
(�AFP / 27 janvier 2015 13h01)
�
