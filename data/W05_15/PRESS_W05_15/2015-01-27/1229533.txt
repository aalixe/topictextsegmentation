TITRE: RD Congo-Tunisie: 1-1 - Football - Sports.fr
DATE: 27-01-2015
URL: http://www.sports.fr/football/can/articles/rd-congo-tunisie-0-1-1174592/?sitemap
PRINCIPAL: 1229530
TEXT:
26 janvier 2015 � 19h01
Mis à jour le
26 janvier 2015 � 23h55
Suivez le déroulement de la rencontre du groupe B de la CAN 2015 entre le RD Congo et la Tunisie.
90' C'est terminé, la Tunisie et la RDC se qualifient au détriment du Cap-Vert et de la Zambie. Les deux Congo s'affronteront en quarts de finale, alors que la Tunisie retrouvera le pays hôte, la Guinée Equatoriale.
90' Ce match nul satisfait les deux équipes qui se qualifient si le score reste à égalité entre le Cap-Vert et la Zambie (0-0).
90' A la suite d'un bon travail de Bolasie, Mbemba frappe fort depuis l'entrée de la surface, mais c'est au-dessus.
88' Bon coup franc obtenu par la RDC, mais le tir de Makiadi est repoussé par le mur.
87' Ragued est remplacé par Nater pour la Tunisie.
86' Zakuani remplace Mongongu pour la RDC.
85' Sur un dégagement lointain de Mathlouthi, Mongongu est mis en difficulté dans sa surface mais parvient à éloigner le danger in extremis.
82' Frappe puissante de Kasusula des 20 m, mais c'est à côté.
81' Mathlouthi a repris sa place sur le terrain.
79' Gros tacle de Mongongu face à Mathlouthi qui pénétrait dans la surface. Le défenseur d'Evian TG a touché la cheville du Tunisien qui sort sur civière.
78' Kabananga remplace Mubele pour la RDC.
76' Nouveau changement pour la Tunisie avec l'entrée de Moncer à la place de Khazri.
74' Arrêt décisif de Kidiaba
Chikhaoui décale parfaitement Maaloul sur la gauche de la surface, le centre de ce dernier est repris à bout portant par Younes, mais Kidiaba dévie en corner.
72' Le buteur tunisien, Akaichi, est remplacé par Younes.
71' Sur un contre, Akaichi pénètre dans la surface mais s'empale sur Kimwaki.
70' Frappe très lointaine de Mbemba après un petit pont sur Ragued, mais c'est au-dessus.
69' Bolasie s'échappe côté gauche et centre devant le but, mais Ben Youssef est là pour dégager.
66' BUT de Bokila pour RD Congo !
BUT de Bokila pour RD Congo ! Sur une très longue relance de Mpeko, Mbokani s'impose dans les airs à l'entrée de la surface et dévie pour le nouvel entrant, qui s'emmène le ballon de la cuisse et trompe Mathlouthi. La RDC est provisoirement qualifiée (1-1) !
65' Khazri au-dessus
Nouveau festival de Chikhaoui qui se joue de Mpeko sur la gauche de la surface. Le capitaine tunisien centre pour Khazri, qui se retourne et frappe en pivot, mais c'est au-dessus.
64' Bien décalé côté gauche, Bolasie centre au point de penalty, mais aucun Congolais ne parvient à couper la trajectoire.
62' Tête de Mbokani
Sur un centre venu du côté droit de Mubele, Mbokani s'impose dans les airs face à Ben Youssef mais sa tête passe de peu au-dessus.
60' Chikhaoui s'échappe côté droit et centre en retrait pour Akaichi qui oublie Sassi sur l'aile opposée et frappe à côté.
59' Très discret, Mabwati cède sa place à Bokila pour la RDC.
57' Corner de Khazri dégagé au premier poteau par la tête de Mbokani.
56' Mpeko est servi côté droit et centre, mais c'est trop profond pour Mbokani.
53' Sur un ballon dans la profondeur, Mbokani échappe à la vigilance de la défense tunisenne, mais Mathlouthi veille et sort loin de son but pour dégager.
50' Les Congolais ont du mal à se procurer des occasions, en raison notamment d'un manque de justesse technique.
48' Mbokani tente sa chance à ras de terre, mais c'est dans les gants de Mathlouthi.
46' Chikhaoui frappe de loin et trompe Kidiaba qui dévie le ballon dans son propre but, mais l'arbitre assistant soulève son drapeau et signale un hors-jeu.
46' C'est parti pour la seconde période.
45' C'est la fin d'une première période plutôt équilibrée, mais les Tunisiens ont su se montrer réalistes par rapport à la RD Congo, qui devra accélérer pour ne pas être éliminée. Un but suffirait à leur bonheur.
45' Mubele et Mpeko combinent sur la droite de la surface, mais la défense tunisienne ne se laisse pas piéger sur le centre en retrait de ce dernier.
44' Bon travail de Mabwati sur la droite de la surface. Ce dernier centre en retrait pour Mbokani, repris par la défense tunisienne.
42' Les Congolais ont le pied sur le ballon, mais s'exposent aux contres tunisiens.
41' Mbokani est trouvé dans la surface mais Abdennour veille au grain et dégage en corner.
40' Gros tacle de Mpeko sur Khazri au milieu de terrain. Le joueur de la RD Congo aurait pu écoper d'un avertissement.
38' Bon coup franc pour la RD Congo, mais la passe en profondeur de Bolasie pour un coéquipier sur le côté gauche est trop profonde et file direct en sortie de but.
35' Coup franc lointain de Maaloul, mais c'est hors-cadre.
33' Ballon en profondeur de Maaloul pour Chikhaoui dont la reprise de volée transperce les filets de Kidiaba. Mais le meilleur joueur sur la pelouse jusqu'à présent était hors-jeu.
31' BUT de Akaichi pour la Tunisie !
BUT de Akaichi pour la Tunisie ! A la suite d'un mauvais dégagement de la défense congolaise, Ragued trouve Ben Youssef sur la gauche de la surface. Ce dernier décale Maaloul, qui trouve Chikhaoui. Après un slalom dans la zone de vérité, le joueur du FC Zurich adresse une frappe enroulée, reprise d'une tête plongeante par Akaichi.
28' Coup franc de Kasusula
Grosse faute d'Abdennour à l'entrée de la surface, Kasusula frappe en force, mais Mathlouthi plonge parfaitement au pied de son poteau droit.
25' Après un début de match compliqué, les Congolais sortent la tête de l'eau et apportent le danger aux alentours de la surface tunisienne.
23' Bon corner de Bolasie pour la tête de Mbokani, mais ce dernier ne parvient pas à redresser la course du ballon.
20' Tir cadré de Chikhaoui
Superbe action à une touche de balle de la Tunisie. Khazri lance côté gauche Chikhaoui, qui repique dans l'axe et adresse une frappe enroulée à ras de terre sauvée de justesse par Kidiaba.
18' Nouveau corner de Khazri, Kidiaba dégage tant bien que mal du poing.
16' Coup franc lointain joué par Khazri qui trouve la tête de Chikhaoui, dont la remise dans la surface ne trouve pas preneur.
15' Chikhaoui s'infiltre tout seul dans l'axe et tente sa chance de plus de 20 mètres, mais son tir est complètement manqué.
10' Kimwaki sauve sur la ligne
Grosse occasion pour la Tunisie. Akaichi lance dans la profondeur Chikhaoui, qui fait la différence et trouve finalement en retrait Sassi, dont la frappe est contrée sur la ligne par Kimwaki. La défense congolaise peine à se dégager et Khazri tente sa chance de l'entrée de la surface, mais Kidiaba dévie et Chikhaoui, à l'affût, ne peut dévier dans le but vide.
8' Jaune pour Yaakoubi
Yaakoubi est averti pour un excès d'engagement.
5' Coup franc dangereux pour la RD Congo, mais le Monégasque Abdennour s'impose dans les airs.
3' Corner pour la Tunisie tiré par Khazri, Chikhaoui prend le ballon de la tête, mais ce n'est pas cadré. Mbokani dégage.
1' C'est parti entre la RD Congo et la Tunisie.
0' De leur côté, les Aigles de Carthage, leaders de la poule avec quatre points, n'ont besoin que d'un nul.
0' Après deux matches nuls face à la Zambie (1-1) et le Cap-Vert (0-0), la République démocratique du Congo a encore son destin entre les mains, mais il lui faudra battre la Tunisie ce lundi pour accéder aux quarts de finale.
0' Le coup d'envoi de la rencontre RD Congo-Tunisie sera donné à 19h00.
