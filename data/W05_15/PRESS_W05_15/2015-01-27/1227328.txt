TITRE: Nadal hilare à cause d'un ramasseur de balles - 7SUR7.be
DATE: 27-01-2015
URL: http://www.7sur7.be/7s7/fr/9099/Hors-jeu/article/detail/2194960/2015/01/26/Nadal-hilare-a-cause-d-un-ramasseur-de-balles.dhtml
PRINCIPAL: 1227322
TEXT:
Par: r�daction
26/01/15 - 09h07
vid�o Rafael Nadal s'est bien amus� en constatant qu'un ramasseur de balles �tait (vraiment) aux petits soins pour lui.
Lors de son match contre Kevin Anderson, Rafael Nadal a �t� interrompu par un ramasseur de balles, qui a repositionn� sa bouteille d'eau qui �tait tomb�e. L'Espagnol n'a pas pu s'emp�cher de rigoler en constatant que le jeune gar�on connaissait parfaitement ses petites manies, � savoir mettre l'�tiquette de la marque vers le terrain.
"En fait, cela ne m'importe pas du tout de savoir dans quel sens la bouteille est positionn�e", a expliqu� Nadal par apr�s. "C'est juste une de ces manies d�biles que vous attrapez quand vous faites du sport de haut niveau depuis des ann�es."
Votre impression sur cet article?
