TITRE: Audaincourt (Doubs) - L�gislatives partielles : Manuel Valls plong� dans le noir par la CGT � metronews
DATE: 27-01-2015
URL: http://www.metronews.fr/info/audaincourt-doubs-legislatives-partielles-manuel-valls-plonge-dans-le-noir-par-la-cgt/moaA!WA6ycDq4vdbTs/
PRINCIPAL: 1230126
TEXT:
Cr�� : 27-01-2015 22:08
Quand la CGT plonge Manuel Valls dans le noir
DOUBS � Un meeting de campagne auquel devait participer Manuel Valls a �t� retard� de 45 minutes apr�s que la CGT a plong� la salle dans le noir.
Tweet
�
Manuel Valls a du attendre 45 minutes le retour de la lumi�re lors d'un meeting � Audaincourt, dans le Doubs. Photo :�AFP
Ce devait �tre un meeting sans histoire. Mardi soir, Manuel Valls se rendait � Audaincourt, dans le Doubs, pour apporter son soutien � Fr�d�ric Barbier. Celui-ci est en effet le candidat PS � la l�gislative partielle de la 4e circonscription du d�partement, qui doit permettre d'�lire le rempla�ant de Pierre Moscovici � l'Assembl�e nationale
Et soudain... le noir
La visite du Premier ministre �tait d'autant plus importante que, depuis deux ans, le Parti socialiste encha�ne les d�faites �lectorales et que, cette fois encore, le candidat du PS est loin d'�tre favori. Le soutien affich� du chef du gouvernement, tr�s populaire dans les sondages, ne pouvait qu'�tre b�n�fique.
EN SAVOIR +
>> Le PS fait les yeux doux � Th�venoud pour sauver sa majorit�
Malheureusement pour Manuel Valls, rien ne s'est pass� comme pr�vu. Quelques minutes apr�s le d�but du meeting, toute la salle s'est retrouv�e plong�e dans le noir. Sans �lectricit�, le Premier ministre et son poulain n'avaient d'autre choix que d'attendre. Au bout de 45 minutes, le courant est finalement revenu.
Manuel Valls attend le retour de la lumi�re pour reprendre ... #gene #Doubs #Circo2504 pic.twitter.com/BzAhXXLoQi
� Lucile Br�haut (@LucileBrhaut) 27 Janvier 2015
La CGT d�nonce la loi de transition �nerg�tique
Il n'aura pas fallu longtemps pour trouver d'o� venait la panne�: la  CGT mines-�nergie a en effet rapidement envoy� un communiqu� � l' Est  R�publicain afin de revendiquer le m�fait, d�non�ant la loi de  transition �nerg�tique, "une bonne id�e, mais avec de tr�s mauvaises  solutions".
�
