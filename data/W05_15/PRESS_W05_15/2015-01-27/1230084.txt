TITRE: Benedict Cumberbatch : "Turing avait un esprit brillant" | www.directmatin.fr
DATE: 27-01-2015
URL: http://www.directmatin.fr/cine/2015-01-27/benedict-cumberbatch-turing-avait-un-esprit-brillant-698251
PRINCIPAL: 1230080
TEXT:
Le casting du prochain X-Men r�v�le des surprises
Favori des Oscars pour le prix du meilleur acteur, Benedict Cumberbatch incarne le math�maticien Alan Turing dans "Imitation Game". L�occasion pour l�acteur britannique qu�Hollywood r�clame � cor et � cri de faire conna�tre la vie de ce scientifique qui collabora avec les Alli�s au d�cryptage du code nazi Enigma et qui fut mis au ban de la soci�t� apr�s le conflit � cause de son homosexualit�.�
�
Quel genre de d�fi repr�sentait l�incarnation d�Alan Turing ?
C��tait imprudent de ne voir en Alan Turing qu�un esprit brillant. Certes c��tait un �tre singulier. Mais m�me quelqu�un qui �labore un ordinateur doit collaborer avec des �tres humains au final. Le rendre humain n�a pas �t� difficile � partir du moment o� j�ai commenc� � faire des recherches sur lui. C��tait quelqu�un de tr�s sensible qui a subi un grand traumatisme durant son enfance.
�
Etiez-vous au courant du contexte d�homophobie de l��poque?
Des centaines de gays ont �t� pers�cut�s � cette �poque pour les m�mes faits qu�Alan. C��tait une �poque terrible. Ce que je ne savais pas en revanche, c��tait qu�on pratiquait la castration chimique. Que l�on pratique d�ailleurs encore dans d�autres parties du globe. Et, bien qu�on soit devenu plus tol�rant � cet �gard, c�est encore un sujet sur lequel on doive progresser en termes de droits.
�
Existe-t-il des similarit�s entre le personnage de Sherlock Holmes et Alan Turing�?�
Outre que ce sont tous les deux des �tres extr�mement intelligents�?� Non�! Ils sont tr�s diff�rents. Le premier est un personnage de fiction, tr�s th��tral, qui parle vite et se sert de son intelligence pour coincer les autres et faire son autopromotion. L�autre a vraiment exist� et �tait atteint parfois de b�gayements� Il avait un esprit brillant et s�en est servi pour accomplir de grandes choses avec d�autres personnes.�
�
Vous incarnez un g�nie. Dans votre m�tier, croyez-vous davantage au g�nie qu�au travail?
Les deux ne s�excluent pas. Il faut s�rement poss�der quelque chose au d�part et ensuite le faire fructifier. Ce qui a fait de Turing un g�nie, c�est qu�il a r�ellement construit ce qu�il avait imagin�.�
�
Vous avez d�j� incarn� pas mal de personnages historiques. Y�a-t-il une personnalit� que vous r�vez d�interpr�ter�?
Je suis assez nerveux quand je r�ponds � ce genre de questions parce que les producteurs s�empressent tout de suite de m�envoyer des tonnes de sc�narios. Mais oui bien s�r qu�il y a des personnages, de fiction ou non, que j�aimerais interpr�ter. Sinon je serai d�j� � la retraite. (rires) �
�
Dans le cadre du m�tier d�acteur, avez-vous peur de quelque chose�?
Faire ce m�tier avec obsession�?!... Comme pour tout projet, c�est parfois terrifiant de se lancer pour la premi�re fois. Mais je suis vraiment chanceux de faire un m�tier dans lequel je ne suis pas oblig� de me r�p�ter sans cesse.
�
"Imitation Game", de Morten Tyldum, avec Benedict Cumberbatch, Keira Knightley et Matthew Goode. En salles, le mercredi 28 janvier 2015.
�
La bande-annonce de "Imitation Game" :
�
