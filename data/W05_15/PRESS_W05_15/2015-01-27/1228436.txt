TITRE: GB: croissance meilleure qu'en Allemagne
DATE: 27-01-2015
URL: http://www.lefigaro.fr/flash-eco/2015/01/27/97002-20150127FILWWW00164-gb-24-de-croissance-en-2014.php
PRINCIPAL: 1228435
TEXT:
le 27/01/2015 à 11:38
Publicité
La croissance au Royaume-Uni a ralenti au quatrième trimestre, à 0,5% par rapport au troisième. Mais elle a atteint 2,6% pour l'ensemble de l'année 2014, bien plus que celle des principaux voisins européens du pays, a annoncé mardi l'Office des statistiques nationales (ONS).
L'activité dans les services, qui comprend notamment le puissant secteur financier de la City de Londres, est restée dynamique en fin d'année, mais celle de la construction a diminué, ce qui a un peu ralenti la vigueur de la croissance lors des derniers mois de 2014.
Les économistes s'attendaient en moyenne à une progression un peu plus rapide du Produit intérieur brut (PIB), de 0,6% au quatrième trimestre et de 3,0% pour 2014, d'après un consensus établi par FactSet.
La croissance du PIB britannique a atteint un niveau confortable, bien supérieur à celui de l'Allemagne (1,5% en 2014) et à celui prévu pour la France (0,4%). Des estimations préliminaires de la Commission européenne, datant de novembre, ont affirmé que le Royaume-Uni avait repris à la France la cinquième place économique mondiale à la faveur de cette accélération.
Lire aussi:
