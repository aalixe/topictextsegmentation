TITRE: Lutte contre la pauvret� : De nouvelles mesures en mars. Info - Toulon.maville.com
DATE: 27-01-2015
URL: http://www.toulon.maville.com/actu/actudet_-lutte-contre-la-pauvrete-de-nouvelles-mesures-en-mars_54135-2703660_actu.Htm
PRINCIPAL: 1226759
TEXT:
Google +
Les familles pauvres, notamment monoparentales devraient b�n�ficier d'un effort accru.� Ouest France
Le gouvernement doit pr�senter en mars de nouvelles actions de lutte contre la pauvret�. Des efforts sont attendus pour les familles monoparentales et sur l'acc�s au logement.
Le gouvernement pr�sentera d�but mars ��une feuille de route actualis�e�� de son plan de lutte contre la pauvret�, a indiqu� lundi Matignon apr�s la remise d'un rapport d'�tape par l'ancien leader de la CFDT Fran�ois Ch�r�que.�
��Le gouvernement s'appuiera sur ce rapport pour �laborer et pr�senter au d�but du mois prochain (d�but mars, a ensuite pr�cis� Matignon) une feuille de route actualis�e de mise en oeuvre du plan pluriannuel contre la pauvret� et pour l'inclusion sociale��, a indiqu� le cabinet de Manuel Valls dans un communiqu�.
��Le Premier ministre r�affirme son engagement total et la mobilisation de l'ensemble du gouvernement dans la lutte contre la pauvret� et la mise en oeuvre renforc�e du plan pluriannuel��, ajoute-t-on de m�me source.
Familles pauvres et monoparentales
Charg� du suivi du plan quinquennal de lutte contre la pauvret� lanc� il y a deux ans, M. Ch�r�que recommande dans son rapport un effort accru � l'�gard des familles pauvres, notamment monoparentales.
Il propose �galement d'acc�l�rer la construction de logements sociaux et de faire un ��plan d'urgence�� visant � r�duire le recours aux h�tels pour l'h�bergement d'urgence, a-t-il r�sum� � quelques journalistes sur le perron de Matignon, m�me si ��globalement le gouvernement respecte ses engagements�� en mati�re de pauvret�, avec notamment la hausse des minimas sociaux.
Conf�rence sur la pauvret�
Selon l'ancien syndicaliste, le gouvernement envisage d'organiser une conf�rence sur la pauvret�. ��Il y aura un �v�nement mais le format n'est pas encore pr�cis頻, a indiqu� Matignon.
Pr�sentant ce lundi son propre bilan de ce plan quinquennal lanc� en 2013, le collectif Alerte, qui regroupe plusieurs associations dont le Secours catholique, M�decins du Monde et ATD Quart Monde, a d�nonc� des ��zones de pauvret頻 install�es de fa�on p�renne en France. Il a repris les termes d'��apartheid�� et de ��ghetto�sation�� �voqu�s la semaine derni�re par Manuel Valls.
Le collectif sera d'ailleurs re�u prochainement par le Premier ministre, a indiqu� Matignon lundi.
Ouest-France��
