TITRE: Nord: un an de prison ferme pour avoir abandonn� son nouveau-n� dans des toilettes
DATE: 27-01-2015
URL: http://www.bfmtv.com/societe/nord-un-an-de-prison-ferme-pour-avoir-abandonne-son-nouveau-ne-dans-des-toilettes-859829.html
PRINCIPAL: 1229370
TEXT:
Nord: un an de prison ferme pour avoir abandonn� son nouveau-n� dans des toilettes
27/01/2015 � 15h52
Imprimer
La jeune femme de 18 ans a expliqu� au cours de l'audience qu'elle savait qu'elle �tait enceinte mais qu'elle avait peur d'�tre exclue par sa famille.
Le drame avait �t� �vit� de peu. Une jeune femme de 18 ans a �t� condamn�e mardi � Valenciennes, dans le Nord, � quatre ans de prison, dont trois avec sursis, pour avoir abandonn� son nouveau-n� vivant dans une cuvette de toilettes de salle des f�tes en juin 2013 et qui avait surv�cu.
Le tribunal a ordonn� le retrait de l'autorit� parentale et a assorti la p�riode de sursis d'une mise � l'�preuve de deux ans, contre trois ans requis par le parquet � l'audience du 7 novembre. Une obligation de soins, de travailler ou de se former et une obligation d'indemnisation avec une provision de 5.000 euros en attendant les r�sultats de l'enfant.�
Une temp�rature de 26 degr�s
Ce 22 juin 2013, la jeune m�re participait � la f�te de l'�cole de sa petite soeur � la salle des f�tes de Raismes, quand elle a mis au monde un petit gar�on dans les toilettes.�
Le b�b�, dont la temp�rature corporelle atteignait � peine 26 degr�s et qui a surv�cu, avait �t� d�couvert par un employ� municipal et imm�diatement hospitalis�. Interpell�e quelques jours apr�s cette naissance et la d�couverte de l'enfant, sa m�re avait reconnu les faits.
Agi "par crainte"
La jeune femme, poursuivie pour tentative d'homicide volontaire sur mineur de 15 ans, avait expliqu� � l'audience avoir agi par crainte d'�tre "�ject�e de la cellule familiale", selon son avocat. "Elle savait qu'elle �tait enceinte mais cela ne se voyait pas", a-t-il soulign�. "Elle n'a jamais os� avouer qu'elle �tait enceinte".
"Cette d�cision m'appara�t juste et empreinte d'humanit�", a comment� l'avocat de la jeune femme, Me Eric Tiry. "Cette d�cision va �tre accept�e par ma cliente qui a compris aujourd'hui la mesure de la gravit� de son geste", a-t-il d�clar�.
Par J.C. avec AFP
