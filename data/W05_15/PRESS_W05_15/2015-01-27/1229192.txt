TITRE: Les valeurs � suivre � la mi-s�ance � la Bourse de Paris
DATE: 27-01-2015
URL: http://www.boursorama.com/actualites/les-valeurs-a-suivre-a-la-mi-seance-a-la-bourse-de-paris-d22144ff5928e25df002297718ccc282
PRINCIPAL: 0
TEXT:
Les valeurs � suivre � la mi-s�ance � la Bourse de Paris
Reuters le
Tweet 0
LA BOURSE DE PARIS DANS LE ROUGE � LA MI-JOURN�E
Les valeurs � suivre mardi � la Bourse de Paris o� l'indice CAC 40 creuse ses pertes � la mi-s�ance (-1,21% � 4.618,49 points vers 13h00) dans un march� qui, comme les autres places europ�ennes, marque une pause apr�s huit s�ances cons�cutives de hausse.
* Les BANCAIRES (-1,34%) accusent une des plus fortes baisses sectorielles en Europe, l'impact du programme de rachats d'actifs de la BCE sur les banques �tant difficile � pr�voir � ce stade. Avec les deux poids lourds de l'indice, TOTAL (-1,17%) et SANOFI (-1,13%), elles p�sent sur la cote. SOCI�T� G�N�RALE (-3,4%) accuse  la plus forte baisse du CAC 40, BNP PARIBAS perd 2,31% et CR�DIT AGRICOLE 1,93%.
* A l'inverse de TOTAL, TECHNIP gagne 0,38%. Hors CAC 40, CGG s'adjuge 3,16% et poursuit son redressement.
* TECHNIP a annonc� mardi avoir remport� aupr�s du groupe Stone Energy Corporation deux contrats dans le golfe du Mexique pour la conception, la fabrication et l'installation d�une conduite flexible et indique que le projet sera achev� au cours du second semestre 2015, sans toutefois pr�ciser le montant des contrats.
* GEMALTO (+2,91%) signe la plus forte hausse du CAC 40. Le sp�cialiste de la s�curit� num�rique a annonc� mardi avoir fourni � la banque br�silienne Banco do Estado Grande do Sul sa solution "Ezio" pour s�curiser ses services de banque mobile.
* Les VALEURS fortement cycliques comme l'AUTOMOBILE (-1,8%) comptent parmi les plus fortes baisses sectorielles en Europe. RENAULT perd 2,7%, LEGRAND 2%.
* CAP GEMINI, qui �volue depuis le 12 novembre au-dessus de sa moyenne mobile 20 jours et depuis le 15 janvier au-dessus de sa MM 50 jours, perd 2,06%.
* VINCI perd 2,59% et EIFFAGE 2,32%,   l'association "40 millions d'automobilistes" ayant d�clar� � Reuters dans un communiqu� que le gouvernement allait geler les tarifs autoroutiers au 1er f�vrier 2015, une information ensuite confirm�e par Matignon.
* ARKEMA (-4,4%) accuse une des plus fortes baisses du SBF 120, Goldman Sachs �tant pass� � vendre contre neutre sur le titre du groupe chimique.
* TARKETT, plus forte baisse du SBF 120, perd 4,43%, le titre ayant atteint un niveau technique de "surachat"(+8,85% � 20,3 euros) a acc�l�r� un rebond technique apr�s avoir franchi en s�ance sa moyenne mobile 50 jours (19,219 euros). Depuis un plus bas le 7 janvier, le titre engrange pr�s de�%.
* GTT perd 3,23%. Le fonds d'investissement H&F Luxembourg a annonc� avoir vendu un bloc de 4,35% du capital du num�ro un mondial des syst�mes de confinement � membrane pour le transport du gaz naturel liqu�fi� (GNL).
* SOITEC (+4,44% � 0,94 euro) signe la plus forte hausse du SBF 120 apr�s l'annonce par le fabricant de mat�riaux semi-conducteurs de haute performance d'un nouveau substrat qui, dit-il, "repoussent les limites des communications mobiles".
* NEXT RADIO TV (+4,02%) a annonc� lundi soir une hausse de 12% de son chiffre d'affaires sur l'ann�e (TV +23%, radio +4%, digital -3%).
* METABOLIC EXPLORER s'envole de 30%. Le groupe de chimie biologique a annonc� avoir obtenu le feu vert de la FDA am�ricaine pour sa L-M�thionine, une forme naturelle d�un acide amin� essentiel pour la nutrition des animaux.
(Raoul Sachs, �dit� par Jean-Michel B�lot)
� 2015 Thomson Reuters. All rights reserved.
Reuters content is the intellectual property of Thomson Reuters or its third party content providers. Any copying, republication or redistribution of Reuters content, including by framing or similar means, is expressly prohibited without the prior written consent of Thomson Reuters. Thomson Reuters shall not be liable for any errors or delays in content, or for any actions taken in reliance thereon. "Reuters" and the Reuters Logo are trademarks of Thomson Reuters and its affiliated companies.
