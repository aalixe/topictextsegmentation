TITRE: Alain Duhamel�: "La m�moire de la Shoah est toujours aussi vive"
DATE: 27-01-2015
URL: http://www.rtl.fr/actu/societe-faits-divers/alain-duhamel-la-memoire-de-la-shoah-est-toujours-aussi-vive-7776356907
PRINCIPAL: 1229730
TEXT:
Accueil Actu Soci�t� et faits divers Alain Duhamel�: "La m�moire de la Shoah est toujours aussi vive"
Alain Duhamel�: "La m�moire de la Shoah est toujours aussi vive"
REPLAY/�DITO � Le 70�me anniversaire de la lib�ration du camp nazi d'Auschwitz par l'Arm�e rouge a �t� c�l�br� ce mardi 27 janvier, en Pologne. Aujourd'hui, la m�moire de la Shoah est rest�e dans les esprits.
La page de l'�mission : L'Edito d'Alain Duhamel
>
Alain Duhamel�: "La m�moire de la Shoah est toujours aussi vive" Cr�dit Image : Damien Rigondeaud |                                 Cr�dits M�dia : RTL |                                 Dur�e :
03:15
publi� le 27/01/2015 � 18:57
mis � jour le 27/01/2015 � 22:34
Partager
Commenter
Imprimer
Le 70�me anniversaire de la lib�ration du camp nazi d'Auschwitz a �t� c�l�br� ce mardi 27 janvier, en Pologne, en pr�sence de plusieurs chefs d'�tat, dont Fran�ois Hollande. On a pu constater que la m�moire de la Shoah est toujours tr�s vive aujourd'hui, comme l'illustre cette c�r�monie tr�s impressionnante.
Par ailleurs, une �tude publi�e par la Fondation pour la M�moire de la Shoah a r�v�l� que sur 31 pays, la France est celui qui est le plus sensible � celle-ci. Elle se retrouve devant les �tats-Unis, qui a pourtant tendance � donner des conseils, voire m�me des critiques.
Malgr� les difficult�s rencontr�es dans certains �tablissements scolaires, la transmission de la m�moire de la Shoah se fait couramment en France, � travers le cin�ma, la t�l�vision et l'enseignement.
Alain Duhamel
Facebook Twitter Linkedin
N�anmoins, le nombre d'actes antis�mites a doubl� en 2014, selon le Conseil repr�sentatif des institutions juives de France (Crif). En cons�quence, le nombre de Juifs qui quittent l'Hexagone pour rejoindre Isra�l a �galement doubl� l'an dernier.
Ce flux migratoire s'explique en grande partie par un sentiment d'ins�curit�, renforc� par les �v�nements qui ont eu lieu au supermarch� casher de Vincennes. C'est un cr�ve-c�ur pour la France, mais Fran�ois Hollande a annonc� des plans de lutte contre l'antis�mitisme pour le mois prochain, qui peuvent susciter l'espoir.
Les d�parts massifs des Juifs de France pour Isra�l ont contrari� l'id�e d'int�gration dont on �tait si fier.
Alain Duhamel
