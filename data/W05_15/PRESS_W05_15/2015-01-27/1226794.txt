TITRE: Un ancien chef de la LRA face � la Cour p�nale internationale
DATE: 27-01-2015
URL: http://www.lemonde.fr/afrique/article/2015/01/26/dominic-ongwen-chef-de-la-lra-face-a-la-cour-penale-internationale_4563857_3212.html
PRINCIPAL: 1226787
TEXT:
Dominic Ongwen, chef de la LRA, face � la Cour p�nale internationale
Le Monde.fr avec AFP
�
Mis � jour le 26.01.2015 � 18h29
Dominic Ongwen a comparu pour la premi�re fois devant la CPI lundi 26 janvier. Il est accus� de crimes contre l'humanit� et crimes de guerre. Cr�dits : REUTERS
��Mon nom est Dominic Ongwen et je suis un citoyen de l�Ouganda, du nord de l�Ouganda��. Interrog� par la juge Ekaterina Trendafilova qui lui demandait de confirmer son identit�, Dominic Ongwen, chef de la sanguinaire r�bellion ougandaise de la LRA ( Arm�e de r�sistance du Seigneur) a comparu pour la premi�re fois, lundi 26�janvier, devant la Cour p�nale internationale (CPI), � la Haye (Pays-Bas). V�tu d�un costume bleu sur une chemise blanche et d�une cravate � carreaux, cet ancien enfant soldat devenu le num�ro trois de la LRA est soup�onn� par la CPI de crimes contre l�humanit� et crimes de guerre.
Calme et attentif durant sa comparution, les traits tir�s par la fatigue, il s�est exprim� en acholi, la langue utilis�e par les combattants de la LRA. Il a rappel� que lui-m�me avait �t� enlev� par des soldats du chef de la LRA, Joseph Kony, et emmen� dans la brousse. Il avait 14 ans. Et de pr�ciser qu�il a �t� un combattant ��jusqu�� son arriv�e � la cour��.
Recherch� depuis dix ans, sa t�te mise � prix � 5�millions de dollars, Dominic Ongwen a �t� captur� en Centrafrique d�but janvier par des �l�ments de l�ex-S�l�ka, la r�bellion centrafricaine, puis remis aux soldats am�ricains. Washington puis l�arm�e ougandaise ont par la suite d�clar� que le leader de la LRA s��tait rendu volontairement. Il est arriv� � la prison de La�Haye le 21�janvier.
Accus� de sept crimes contre l�humanit� et crimes de guerre
Command�e depuis sa cr�ation il y a une trentaine d�ann�es par Joseph Kony, la LRA a sem� la terreur dans plusieurs pays d�Afrique centrale. M.Ongwen est le premier membre de la LRA � compara�tre devant la CPI. ��Je voudrais remercier Dieu pour avoir cr�� le Paradis et la Terre, avec tous ceux qui sont sur la Terre��, a-t-il pr�cis�.
Lire aussi�: Un chef rebelle ougandais captur� en Centrafrique
M.�Ongwen fait l�objet de sept chefs d�accusation de crimes contre l�humanit� et crimes de guerre, notamment pour meurtre, r�duction en esclavage et traitements cruels. Des Organisations non gouvernementales (ONG) ont n�anmoins estim� que le pass� de Dominic Ongwen, lui-m�me enlev� par la LRA, pourrait constituer des circonstances att�nuantes, s�il devait �tre reconnu coupable.
���La CPI l�accuse en partie des m�mes crimes qui ont �t� commis � son encontre��, a soulign� le chercheur Ledio Cakaj. Mais pour Victor Ochen, ancien r�fugi� ayant fui les violences de la LRA qui dirige aujourd�hui une ONG dans le nord du pays, les victimes attendent que justice soit rendue. ��Ceux qui sont sans nez, sans l�vres ou sans visage veulent que Dominic Ongwen soit jug� en tant que membre du commandement de la LRA, et non en tant qu�ancien enfant soldat��, a-t-il d�clar� � l�AFP. La prochaine comparution de Dominic Ongwen doit avoir lieu le 24�ao�t, pour des audiences destin�es � �valuer les preuves rassembl�es par le procureur et d�cider si un proc�s doit �tre tenu.
Cr��e aux alentours de 1987, la LRA op�rait alors dans le nord de l�Ouganda o� elle a multipli� les exactions qui ont fait sa sinistre r�putation�: rapts d�enfants transform�s en soldats et en esclaves, mutilations et massacres de civils� Elle en a �t� chass�e au milieu des ann�es 2000 par l�arm�e ougandaise, avant de s��parpiller dans les for�ts �quatoriales des pays alentour, dont la Centrafrique. Selon l�ONU, la r�bellion a, depuis sa cr�ation, tu� plus de 100�000 personnes en Afrique centrale et enlev� plus de 60�000 enfants.
�
