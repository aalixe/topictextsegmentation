TITRE: France: les ventes en ligne ont progress� de 11% en 2014
DATE: 27-01-2015
URL: http://www.romandie.com/news/France-les-ventes-en-ligne-ont-progresse-de-11-en-2014/559454.rom
PRINCIPAL: 1228428
TEXT:
Tweet
France: les ventes en ligne ont progress� de 11% en 2014
Paris (awp/afp) - Le commerce en ligne a continu� � progresser en France en 2014, en d�pit du contexte g�n�ral de baisse de la consommation, avec une hausse de 11% de son chiffre d'affaires, a annonc� mardi la f�d�ration du secteur (Fevad).
En 2014, "au total, les Fran�ais auront d�pens� 57 milliards d'euros sur internet", soit un peu plus que les 56,5 milliards d'euros que pr�voyait la Fevad en septembre, gr�ce � une solide progression lors des f�tes de fin d'ann�e (+13%).
Si le panier moyen a poursuivi son recul pour la quatri�me ann�e cons�cutive (en baisse de 4% par rapport � 2013, � 81 euros), l'arriv�e de nouveaux acheteurs, l'augmentation de la fr�quence d'achat et la cr�ation de nouveaux sites marchands ont plus que compens� cette tendance, a expliqu� la Fevad lors d'une conf�rence de presse.
La f�d�ration a en particulier mis en lumi�re la forte hausse des ventes sur mobile (+60%), port�es par un taux d'�quipement plus important en smartphones, une meilleure connexion (4G) et des �crans plus grands (tablettes et "phablets", les t�l�phones mobiles grand format).
La consommation des m�nages en biens dans son ensemble a elle recul� de 1,1% en novembre sur un an, selon les derniers chiffres publi�s par l'Insee.
Pour 2015, la Fevad pr�voit une progression comparable � 2014: elle estime que les ventes en ligne devraient progresser de 10%, pour atteindre 62,4 milliards d'euros.
afp/jq
