TITRE: ASSE | ASSE : Ce joueur qui manque beaucoup aux Verts�
DATE: 27-01-2015
URL: http://www.le10sport.com/football/ligue1/asse/asse-ce-joueur-qui-manque-beaucoup-aux-verts-178332
PRINCIPAL: 1227157
TEXT:
�
L�ASSE A MANQU� DE FOLIE
Romain Hamouma, Yohan Mollo, Ricky Van Wolfswinkel et Mevl�t Erding �taient notamment en charge de l�animation offensive. Mais ils n�ont jamais r�ussi � d�stabiliser l�arri�re garde parisienne. Clairement, il manquait de la folie dimanche soir c�t� st�phanois. Le jeune Allan Saint-Maximin, 17 ans, a apport� de la fra�cheur et du dynamisme en deuxi�me p�riode. Mais c��tait d�j� presque trop tard.
�
GRADEL, LE SAUVEUR�?
Un joueur a manqu� dimanche soir aux Verts. Ce joueur, c�est Max-Alain Gradel, l�international ivroirien. Gradel, si inconstant par le pass�, a �t� l�homme fort de l�ASSE lors de la premi�re partie de saison en attaque. C�est lui le d�tonateur des attaques st�phanois d�sormais. Et quand il n�est pas l�, il manque. Clairement. La C�te-d�Ivoire joue son avenir � la CAN mercredi face au Cameroun. Gradel pourrait tr�s vite retrouver les pelouses de L1. Au grand bonheur de l�ASSE�?
R�daction
D�couvrez les produits officiels de la Boutique ASSE
Retrouvez toute l'actualit� du mercato
Retrouvez toutes les vid�os
