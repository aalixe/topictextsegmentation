TITRE: CAN: Tunisie-RD Congo, un nul pour deux qualifi�s
DATE: 27-01-2015
URL: http://www.leparisien.fr/flash-actualite-sports/can-tunisie-rd-congo-un-nul-pour-deux-qualifies-26-01-2015-4480897.php
PRINCIPAL: 1228570
TEXT:
CAN: Tunisie-RD Congo, un nul pour deux qualifi�s
26 Janv. 2015, 20h51 | MAJ : 26 Janv. 2015, 20h51
r�agir
L'attaquant Ahmed Akaichi, buteur pour la Tunisie contre la R�publique d�mocratique du Congo dans le Groupe B de la CAN, le 26 janvier 2015 � Bata
Un point, c'est tout: la Tunisie et la RD Congo, dos � dos lundi � Bata (1-1), se sont qualifi�s pour les quarts de finale de la CAN -2015 o� ils affronteront respectivement la Guin�e �quatoriale et l'autre Congo.
Les Aigles de Carthage terminent en t�te de ce groupe A (5 points) devant la RDC (3), qui devance seulement au nombre de buts marqu�s le Cap-Vert, �galement auteur d'un nul face � la Zambie parall�lement � Ebebiyin, mais vierge (0-0).
Le but �galisateur de Bokila (66e) apr�s l'ouverture du score tunisienne d'Akaichi (31e) p�se lourd, et convoque donc une cocasse affiche congolo-congolaise dans le top 8.
Sur un terrain rendu glissant par la pluie, les Tunisiens ont g�r� le match sans d�pense d'�nergie superflue, avec une d�fense � cinq, eux que seul un sc�nario tr�s particulier �liminait (une d�faite combin�e � une victoire cap-verdienne).
Chikhaoui a rayonn�: le capitaine tunisien, pr�cieux en point de fixation, posait de gros probl�mes � la d�fense adverse. C'est lui qui, d'une glissade tacl�e, offrait une passe d�cisive pour la t�te opportuniste d'Akaichi (31e).
Le N�9 avait auparavant rat� de justesse le cadre dans la foul�e d'une frappe trop molle de Sassi (10e) et contraint Kidiaba � une belle parade (20e). Apr�s un gros travail c�t� gauche, il offrait aussi une balle de but � Khazri qui vendangeait (65e).
Mais Chikhaoui et les autres attaquants (Akaichi, Khazri) se montraient aussi gourmands et individualistes, comme si, consid�rant la qualification acquise, ils voulaient chacun briller.
La premi�re p�riode �tait ainsi exclusivement tunisienne, alors que les L�opards peinaient � construire jusqu'� la surface adverse et manquaient de promptitude au moment du dernier geste. Ils n'avaient gu�re qu'un coup franc de Kasusula bien d�tourn� par Mathlouthi � se mettre sous la dent (28e).
Ils haussaient le ton apr�s la pause gr�ce � Mbokani, qui perdait son duel face � Mathlouthi bien sorti de sa surface (54e) et pla�ait une t�te au dessus (62e) avant de d�vier encore de la t�te pour Bokila qui cette fois �galisait (66e).
Les Congolais ont donc d�croch� le point de la qualification en construisant patiemment plut�t que par esprit de r�volte. A la fin, � Bata, tout le monde �tait content. A Ebebiyin, beaucoup moins.
