TITRE: Soci�t�. Le nombre d'actes antis�mites a doubl� en 2014
DATE: 27-01-2015
URL: http://www.ouest-france.fr/societe-le-nombre-dactes-antisemites-double-en-2014-3145362
PRINCIPAL: 1227539
TEXT:
Soci�t�. Le nombre d'actes antis�mites a doubl� en 2014
France -
Le 9 janvier Am�dy Coulibaly est tu� apr�s une prise d'otages meurtri�re, visant des juifs, dans une �picerie casher, porte de Vincennes � Paris.�|�Reuters.
Facebook
Achetez votre journal num�rique
Selon le Crif 851 actes antis�mites ont �t� recens�s en 2014, contre 423 en 2013.
Le nombre des actes antis�mites a doubl� (+101%) en 2014 par rapport � 2013 en France, avec m�me une augmentation de 130% des actes avec violences physiques, a annonc� mardi le Conseil repr�sentatif des institutions juives de France (Crif), qui estime que ��le point critique a largement �t� d�pass頻.
Pr�occupant
Selon le Crif, qui cite des chiffres du Service de protection de la communaut� juive (SPCJ) bas�s sur des donn�es du minist�re de l'Int�rieur, 851 actes antis�mites ont �t� recens�s en 2014, contre 423 en 2013. Le nombre des actes avec violences physiques a �t� de 241 l'an pass�, contre 105 en 2013. ��Il r�sulte de ces chiffres un accroissement important et tr�s pr�occupant de la violence des actes antis�mites��, note le Crif dans un communiqu�.
Tags :
