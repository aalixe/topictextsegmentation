TITRE: Lucas Moura: "Le PSG devient meilleur au fil des matchs" - Sambafoot.com, toute l'actualit� du football br�silien
DATE: 27-01-2015
URL: http://www.sambafoot.com/fr/informations/68474_lucas_moura___le_psg_devient_meilleur_au_fil_des_matchs_.html
PRINCIPAL: 1227053
TEXT:
Lucas Moura: "Le PSG devient meilleur au fil des matchs"
Par  Fr�d�ric Fausser 2015-01-26 14:54:00 20150126
Action Images
L'attaquant du Paris Saint-Germain, Lucas Moura, �tait doublement satisfait apr�s la victoire de son club face � Saint-Etienne hier � Geoffroy-Guichard (0-1).
A titre collectif, son �quipe reste au contact du leader Lyon qui pointe � quatre points. D'un point de vue individuel, le Br�silien a disput� son 100e match sous les couleurs du PSG.
"C�est toujours difficile de jouer ici, le plus important c�est la victoire. Je pense qu�on devient meilleur au fil des matches, et puis surtout on reste au contact de Lyon", a-t-il reconnu.
A lire aussi:
