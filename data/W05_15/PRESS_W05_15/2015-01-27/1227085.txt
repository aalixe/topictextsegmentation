TITRE: Des armes et des gilets pare-balles pour les policiers municipaux | France | Nice-Matin
DATE: 27-01-2015
URL: http://www.nicematin.com/france/des-armes-et-des-gilets-pare-balles-pour-les-policiers-municipaux.2079453.html
PRINCIPAL: 1227082
TEXT:
Tweet
Paris (AFP)
Le gouvernement va mettre � disposition des collectivit�s plus de 4.000 revolvers pour les policiers municipaux, et va les aider � financer l'achat de 8.000 gilets pare-balles, a annonc� lundi le ministre de l'Int�rieur � l'issue d'une r�union avec les syndicats de policiers municipaux.
Cette r�union, � laquelle participaient le pr�sident de l'association des maires de France (AMF), Fran�ois Baroin, et le pr�sident de la Commission consultative des polices municipales, Christian Estrosi, �tait destin�e � "am�liorer les conditions de travail et de protection des personnels, dans le respect du principe de libre administration des collectivit�s territoriales", affirme Bernard Cazeneuve dans un communiqu�.
Elle fait suite aux attentats de Paris des 7, 8 et 9 janvier, au cours desquels 17 personnes ont �t� tu�es, dont deux policiers et une polici�re municipale.
"L'Etat mettra gracieusement � disposition des collectivit�s qui souhaiteraient armer leur police municipale, et qui seront autoris�es � le faire � cadre juridique constant, des armes op�rationnelles (revolvers), dans la limite des stocks disponibles (plus de 4.000 armes)", souligne le ministre.
Cette mesure "� cadre juridique constant" signifie qu'il n'est "pas question d'armer l'ensemble des policiers municipaux", a de son c�t� fait valoir l'AMF.
Il s'agira de revolvers Manhurin, a pr�cis� � l'AFP Fran�ois Baroin, pr�sident de l'Association des maires de France, qui s'est d�clar� "satisfait des modalit�s des annonces et du calendrier".
De m�me, afin d'aider "les communes et les �tablissements publics de coop�ration intercommunale (EPCI) � financer le renforcement de la protection des policiers municipaux, notamment en gilets pare-balles, le gouvernement a d�cid� un accroissement de deux millions d?euros des ressources du Fonds interminist�riel de pr�vention de la d�linquance, repr�sentant une aide - pouvant aller jusqu?� 50% - � l?acquisition de 8.000 gilets", ajoute le minist�re.
Afin d'am�liorer l'interop�rabilit� des r�seaux de communication entre forces de s�curit� nationales et municipales, l?Etat va �galement subventionner les communes et EPCI � hauteur de 30% pour l?acquisition de postes de radio, pour "une meilleure efficacit� op�rationnelle et une s�curit� accrue des policiers municipaux", via "le d�clenchement d'alertes g�n�rales en cas d?agression".
Enfin, "un meilleur acc�s des polices municipales aux fichiers de la police et de la gendarmerie nationale sera favoris�, dans le respect du cadre fix� par la loi", ajoute le ministre.
Dans un communiqu�, le Syndicat de d�fense des policiers municipaux s'est dit "totalement insatisfait des pr�tendues +annonces+", dont une partie avait d�j� �t� �nonc�e en 2010 apr�s la mort d'Aur�lie Fouquet, une polici�re municipale tu�e dans le Val-de-Marne.
"Les maires resteront libres de ne pas armer les policiers municipaux et sont confort�s dans cette position par l'AMF", regrette-t-il encore.
Tweet
Tous droits de reproduction et de repr�sentation r�serv�s. � 2012 Agence France-Presse.
Toutes les informations (texte, photo, vid�o, infographie fixe ou anim�e, contenu sonore ou multim�dia) reproduites dans cette rubrique (ou sur cette page selon le cas) sont prot�g�es par la l�gislation en vigueur sur les droits de propri�t� intellectuelle. �Par cons�quent, toute reproduction, repr�sentation, modification, traduction, exploitation commerciale ou r�utilisation de quelque mani�re que ce soit est interdite sans l�accord pr�alable �crit de l�AFP, � l�exception de l�usage non commercial personnel. �L�AFP ne pourra �tre tenue pour responsable des retards, erreurs, omissions qui ne peuvent �tre exclus dans le domaine des informations de presse, ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
AFP et son logo sont des marques d�pos�es.
"Source AFP" ? 2015 AFP
France
Le d�lai de 15 jours au-del� duquel il n'est plus possible de contribuer � l'article est d�sormais atteint.
Vous pouvez poursuivre votre discussion dans les forums de discussion du site. Si aucun d�bat ne correspond � votre discussion, vous pouvez solliciter le mod�rateur pour la cr�ation d'un nouveau forum : moderateur@nicematin.com
