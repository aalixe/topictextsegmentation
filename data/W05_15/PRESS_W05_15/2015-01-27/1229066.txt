TITRE: Euro 2016: �Le risque terroriste est devenu tangible�, selon le pr�sident du comit� d'organisation Jacques Lambert - 20minutes.fr
DATE: 27-01-2015
URL: http://www.20minutes.fr/sport/1526923-20150127-euro-2016-risque-terroriste-devenu-tangible-selon-president-comite-organisation-jacques-lambert
PRINCIPAL: 1229065
TEXT:
euro 2016
Jacques Lambert , pr�sident du comit� d'organisation de l'Euro 2016 , a expliqu� mardi dans un entretien � l'AFP que les attentats des 7 et 9 janvier � Paris avaient rendu �tangible� un risque terroriste qui �tait �pr�sent d�s le d�but� de la pr�paration du tournoi. �Le risque terroriste �tait pr�sent d�s le d�but. Quand j'ai r�dig� le dossier de candidature en 2009, la menace terroriste faisait partie des 12 risques identifi�s comme les risques majeurs lors d'un �v�nement comme celui-l�, a d�clar� M. Lambert.
�Cela ajoute une intensit� particuli�re�
�Ce qu'ont chang� les �v�nements de janvier, c'est que d'un risque th�orique, on est pass� � un risque tangible, palpable, puisqu'il y a eu passage � l'acte�, a-t-il ajout�. Selon lui, ��a ne change sans doute pas grand-chose pour les professionnels de la s�curit� en ce qui concerne la pr�paration de l'�v�nement�.
�Mais on voit bien que, pour tout le monde, l'opinion publique, les m�dias, les �quipes, cela ajoute une intensit� particuli�re�, a-t-il ajout�. �S'agissant de la s�curit�, on voit combien, dans le contexte international et national, tout d�pendra jusqu'� la derni�re heure du dernier jour de comp�tition de bien des choses qui vont se passer partout dans le monde�, a encore estim� l'ancien pr�fet. L'Euro aura lieu en France du 10 juin au 10 juillet 2016 , dans dix stades diff�rents, dont le Stade de France (o� aura lieu la finale).
