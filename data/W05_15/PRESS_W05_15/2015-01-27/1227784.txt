TITRE: Le nombre d'actes antis�mites a doubl� en France en 2014
DATE: 27-01-2015
URL: http://www.boursorama.com/actualites/le-nombre-d-actes-antisemites-a-double-en-france-en-2014-0dd2c9fc6bf61ece3eda467d5dd8da88
PRINCIPAL: 1227742
TEXT:
Le nombre d'actes antis�mites a doubl� en France en 2014
Reuters le
Tweet 0
PARIS, 27 janvier (Reuters) - Le nombre d'actes antis�mites  a doubl� en 2014 en France, accusant une hausse de 101% par  rapport � 2013, annonce mardi le Conseil repr�sentatif des  institutions juives de France (Crif) dans un communiqu�.       Quelque 851 actes antis�mites ont �t� enregistr�s en 2014,  contre 423 en 2013,, indique le Crif, qui cite les chiffres du  service de protection de la communaut� juive (SPCJ) �tablis � partir de donn�es du minist�re de l'Int�rieur.      Le nombre des actes avec violences physiques est quant � lui  pass� de 105 en 2013 � 241 en 2014, soit une augmentation de 130  %. "Ces actes antis�mites repr�sentent 51% des actes racistes  commis en France, alors que les juifs ne sont que moins de 1% de  la population fran�aise", �crit le Crif qui estime que le "point  critique a �t� d�pass�". "Cette tendance confirme bien malheureusement la  persistance, voir le renforcement des pr�jug�s antis�mites en  France, parfois leur radicalit� croissante qui fait qu'on passe  de l'insulte � la violence, de la violence au terrorisme",  ajoute-t-il.       (Marine Pennetier, �dit� par Yves Clarisse)
� 2015 Thomson Reuters. All rights reserved.
Reuters content is the intellectual property of Thomson Reuters or its third party content providers. Any copying, republication or redistribution of Reuters content, including by framing or similar means, is expressly prohibited without the prior written consent of Thomson Reuters. Thomson Reuters shall not be liable for any errors or delays in content, or for any actions taken in reliance thereon. "Reuters" and the Reuters Logo are trademarks of Thomson Reuters and its affiliated companies.
