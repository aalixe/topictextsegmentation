TITRE: PSG - Mercato : David Luiz convoit� par le Real Madrid ?
DATE: 27-01-2015
URL: http://www.butfootballclub.fr/1602053-psg-mercato-un-nouveau-joueur-convoite-par-le-real-madrid/
PRINCIPAL: 1229833
TEXT:
PSG � Mercato : un autre grand nom convoit� par le Real Madrid ?
Post� le 27 janvier 2015
Commentez cet article
��
S�il semble attir� par l�id�e de recruter Marco Verratti, Carlo Ancelotti pourrait bien �tre aussi tent� de chiper David Luiz au Paris Saint-Germain.
C�est le Daily Express qui lance la rumeur : Carlo Ancelotti aurait coch� le nom de David Luiz en cas de d�part pr�matur� de Pepe. En pleine phase de n�gociations pour une prolongation de contrat au Real Madrid, Pepe pourrait en effet voir ses 32 ans jouer en sa d�faveur, ce qui pourrait pousser le club merengue � lui trouver un rempla�ant.
Rapha�l Varane semblait le joueur tout d�sign� pour remplir ce r�le mais nos confr�res britanniques misent plus sur le d�fenseur du PSG, qui a l�avantage de pouvoir jouer au milieu du terrain. L�hypoth�se semble tout de m�me un brin farfelue.
