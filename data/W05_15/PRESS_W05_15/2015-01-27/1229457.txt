TITRE: Ben Arfa reste en salle d'attente - Football - Sports.fr
DATE: 27-01-2015
URL: http://www.sports.fr/football/ligue-1/articles/ben-arfa-va-encore-devoir-patienter-1174945/
PRINCIPAL: 0
TEXT:
27 janvier 2015 � 16h58
Mis à jour le
27 janvier 2015 � 17h46
Hatem Ben Arfa va devoir s’armer de patience. La Fifa, dont on attendait la décision à son sujet ce mardi, n’a pas tranché.
Et le feuilleton se prolonge. Toujours interdit de jouer avec Nice qu’il a rejoint durant la trêve hivernale, Hatem Ben Arfa , qui a porté les couleurs de Hull City cette saison mais aussi de Newcastle United lors d’une seule rencontre comptant l’été dernier pour un tournoi inter centres de formation, est désormais suspendu au bon vouloir de la Fifa, à qui la Ligue de football professionnel a demandé un avis consultatif. Mais la grande instance mondiale du football n’a pas l’air pressé puisque sa décision, attendue ce mardi, ne sera pas rendue publique dans les prochaines heures, mais éventuellement "avant la fin de la semaine", selon une information de Nice-Matin.
Autant dire que l'international tricolore, écœuré par la situation qui le prive d’exercer son métier, ne sera pas fixé très vite, d’autant que c’est la Commission juridique de la LFP, présidée par André Soulier, avocat proche de l’Olympique Lyonnais et de Jean-Michel Aulas, qui prendra la décision finale. Soulier, faut-il le rappeler, qui n’était pas apparu attendri par la situation de Ben Arfa voilà quelques jours lors de son passage dans l’émission Luis Attaque sur RMC. "Je pense qu’il a les moyens de subvenir à ses besoins pendant cette période de ''chômage''. (…) Je voudrais que tous les chômeurs de France aient un contrat en poche." Un Ben Arfa qui ne demande qu'à pouvoir jouer au foot. 
