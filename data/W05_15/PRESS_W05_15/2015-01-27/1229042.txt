TITRE: cin�ma: Sean Penn, roi de la prochaine C�r�monie des C�sar -   Culture - lematin.ch
DATE: 27-01-2015
URL: http://www.lematin.ch/loisirs/cinema/sean-penn-roi-prochaine-ceremonie-cesar/story/25689629
PRINCIPAL: 1229038
TEXT:
Sean Penn, roi de la prochaine C�r�monie des C�sar
cin�ma
�
L'acteur et r�alisateur am�ricain recevra un prix sp�cial couronnant l'ensemble de sa carri�re, le vendredi 20 f�vrier prochain, � Paris.
Par Cover Media. Mis � jour le 27.01.2015 1 Commentaire
Sean Penn, en compagnie de la com�dienne Charlotte Gainsbourg, lors de la C�r�monie des C�sar 2009
Image: AFP
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
La France aime Sean Penn et entend le faire savoir. C'est pourquoi il sera honor� lors de la 40e C�r�monie des C�sar, diffus�e en direct et en clair le 20 f�vrier prochain, sur Canal+.
A cette occasion, le r�alisateur et com�dien �tasunien recevra un prix sp�cial couronnant l�ensemble de sa carri�re. �Acteur mythique, personnalit� engag�e, cin�aste d'exception, Sean Penn est une ic�ne � part dans le cin�ma am�ricain. Une l�gende de son vivant�, pr�cise un communiqu� de l�Acad�mie des C�sar.
Sean Penn est un habitu� des manifestations fran�aises qui le mettent r�guli�rement � l�honneur. En 2008, il avait, par exemple, pr�sid� le Festival de Cannes. L'ann�e suivante, la star avait particip� une premi�re fois aux C�sar. On se souvient, notamment, de la surprise qu'il avait r�serv�e � l'humoriste Florence Foresti, en montant la rejoindre sur sc�ne, durant un sketch.
Un acteur multi-r�compens�
Ce C�sar d�honneur viendra couronner une carri�re riche, au cours de laquelle l�acteur s�est aussi illustr� derri�re la cam�ra et a tourn� avec les plus grands, de Clint Eastwood � Gus Van Sant.
Rappelons que Sean Penn a d�j� re�u deux Oscars du Meilleur acteur (pour �Harvey Milk� et �Mystic River�), un Golden Globe (�galement pour �Mystic River�) et l'Ours d�Argent, pour son r�le de condamn� � mort dans �La derni�re marche�.
Gage que l�acteur a vraiment un faible pour l�Hexagone, il sera bient�t � l�affiche de �Gunman�, une adaptation d�un polar du romancier fran�ais Jean-Patrick Manchette. (Le Matin)
Cr��: 27.01.2015, 14h17
