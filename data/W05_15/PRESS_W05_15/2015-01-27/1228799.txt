TITRE: En Gr�ce, un gouvernement construit pour redresser l'�conomie
DATE: 27-01-2015
URL: http://www.lemonde.fr/europe/article/2015/01/27/le-nouveau-premier-ministre-grec-s-apprete-a-devoiler-son-gouvernement_4564356_3214.html
PRINCIPAL: 0
TEXT:
En Gr�ce, un gouvernement construit pour redresser l'�conomie
Le Monde |
| Par Ad�a Guillot (Ath�nes, correspondance)
L'essentiel
Yanis Varoufakis, pourfendeur de � la dette odieuse �, devient ministre des finances.
Panos Kammenos, membre du parti de droite populiste des Grecs ind�pendants, et alli� de Syriza, est nomm� ministre de la d�fense.
Le gouvernement comporte une dizaine de portefeuilles minist�riels, regroupant une quarantaine de membres.
Le premier gouvernement de M. Alexis Tsipras a �t� d�voil�, mardi�27�janvier. Resserr� autour d'une dizaine de portefeuilles, il comporte notamment quatre ��hyperminist�res�� tourn�s vers les r�formes �conomiques et attribu�s aux personnalit�s charg�es ces derni�res ann�es de mettre en place le programme �conomique de Syriza. En incluant les vice-ministres et secr�taires d'Etat, il compte une quarantaine de membres, dont sept femmes.
Lire le d�cryptage : Les questions que vous vous posez apr�s la victoire de Syriza en Gr�ce
Yanis Varoufakis � la t�te des finances
Avant m�me l'annonce officielle, l'universitaire polyglotte Yanis Varoufakis avait annonc� sur son blog qu'il �tait nomm� ministre des finances . Ag� de 53 ans, ce professeur d'�conomie � l'universit� d'Austin, au Texas, est �tiquet� comme un des ��radicaux�� de Syriza, dont il s'est rapproch� r�cemment.
Pourfendeur de � la dette odieuse �, il est un fervent partisan de la fin des mesures d'aust�rit�, qui ont provoqu�, selon lui, � une crise humanitaire �. Tr�s actif sur les r�seaux sociaux et dans les m�dias, il a conseill� Georges Papandr�ou, de�2004 �2006, quand ce dernier �tait pr�sident du Pasok. Il est �galement connu pour son travail statistique sur les jeux vid�o .
M. Varoufakis, qui avait vivement critiqu� les plans de sauvetage de la Gr�ce, a expliqu� vouloir mettre en place de profondes r�formes pour l'�conomie grecque, ��ind�pendamment de ce que [les] cr�diteurs demandent��, et sans souscrire un autre pr�t :
��Notre Etat doit vivre par ses propres moyens dans le proche futur. Nous sommes pr�ts � mener une vie aust�re, ce qui est diff�rent de l'aust�rit� ! �
Lire son portrait (�dition abonn�s) : Yanis Varoufakis, la nouvelle t�te des finances grecques
Yannis Dragasakis, vice-premier ministre
Yannis Dragasakis, 68 ans, devrait superviser l'ensemble de l'action �conomique ainsi que les n�gociations avec les cr�anciers du pays. Transfuge du Parti communiste et d�put� d'une circonscription d'Ath�nes, il dirige depuis deux ans un comit� interne � Syriza r�fl�chissant � un mod�le de d�veloppement pour la Gr�ce.
Un autre ��hyperminist�re�� de l'�conomie, des infrastructures, de la marine marchande et du tourisme revient � l'�conomiste Georges Stathakis. Celui de la restructuration de la production, de l'environnement et de l'�nergie revient � Panayotis Lafazanis, repr�sentant de la plate-forme de gauche, courant contestataire interne � Syriza auquel M. Tsipras devait donner des gages. Le quatri�me gros minist�re, attribu� � Aristides Baltas, regroupe la culture, l'�ducation et les affaires religieuses.
Panos Kammenos � la d�fense
Fruit de l'alliance entre la Syriza et les Grecs ind�pendants (ANEL), Panos Kammenos, membre du parti de droite populiste, obtient le portefeuille qu'il convoitait. La concession est importante de la part de Syriza : tenant d'une ligne nationaliste dure vis-�-vis de la Turquie ou encore de l'ARYM, pays auquel il refuse de reconnaitre l'appellation de Mac�doine, la t�te de file des souverainistes de droite se situe � l'oppos� des positions jusqu'ici d�fendues par Syriza sur ces questions.
L'universitaire et ancien conseiller de l'ex-premier ministre socialiste Georges Papandr�ou, Nikos Kotzias, a �t� nomm� ministre des affaires �trang�res. Les minist�res de la sant�, de l'emploi et de la justice ont �t� respectivement r�partis entre Panayotis Kouroublis, Panos Skourletis et Nikos Paraskevopoulos.
Les quarante membres du gouvernement pr�teront serment � 17 heures lors d'une c�r�monie la�que pour tous ceux qui ne souhaitent pas jurer sur la Bible, comme l'a fait hier Alexis Tsipras lors de sa prestation de serment en tant que premier ministre. Une b�n�diction religieuse sera parall�lement pr�vue pour ceux, comme Panos Kamm�nos, qui souhaitent la b�n�diction de l'Eglise orthodoxe.
Lire le portrait : Panos Kammenos, l�encombrant alli� d�Alexis Tsipras
La Bourse d'Ath�nes chute de pr�s de 4 %
L'Athex� a chut� au moment o� �tait annonc�e la composition du nouveau gouvernement. L'indice g�n�ral qui avait ouvert dans le rouge � 813 points est descendu jusqu'� 761 points, soit ��6,39% en milieu d'apr�s-midi. L'Athex est ensuite l�g�rement remont� �et a cl�tur� � -3,69 % (793 points).
