TITRE: Voile: Dongfeng attendu en vainqueur de la 3e �tape � Sanya, en Chine- 26 janvier 2015 - Challenges.fr
DATE: 27-01-2015
URL: http://www.challenges.fr/sport/20150126.AFP6616/voile-dongfeng-attendu-en-vainqueur-de-la-3e-etape.html
PRINCIPAL: 1229468
TEXT:
Challenges �>� Sport �>�Voile: Dongfeng attendu en vainqueur de la 3e �tape � Sanya, en Chine
Voile: Dongfeng attendu en vainqueur de la 3e �tape � Sanya, en Chine
Publi� le� 26-01-2015 � 15h36
Mis � jour � 23h57
A+ A-
Le voilier chinois Dongfeng lors du d�part de la 2e �tape de la Volvo Ocean Race du Cap vers Abou Dhabi, le 19 novembre 2014 en Afrique du Sud  (c) Afp
PARIS ( AFP ) - Le voilier chinois Dongfeng, skipp� par le Fran�ais Charles Caudrelier, �tait attendu en vainqueur mardi � Sanya (Chine), terme de la 3e �tape de la Volvo Ocean Race, la course autour du monde en �quipage avec escales.
Lundi � 15h40 GMT (16h40 heure fran�aise), Dongfeng n'�tait plus qu'� 87,1 milles (161 km) de Sanya, une ville touristique situ�e sur l'�le tropicale de Hainan, au sud de la Chine.
Le bateau chinois -dont cinq des neuf membres d'�quipage sont... fran�ais- devan�ait son plus proche adversaire, le bateau des Emirats Arabes Unis Azzam (Ian Walker), de 41,9 milles.
Aussi pr�s du but, il faudrait une avarie majeure ou une collision pour que Azzam rattrape Dongfeng, qui a domin� outrageusement cette 3e manche oc�anique.
Apr�s avoir envisag� une arriv�e de Dongfeng lundi, les organisateurs de la course tablaient plut�t sur un franchissement de la ligne dans la nuit de lundi � mardi, les vents ayant molli en mer de Chine.
Jamais un voilier chinois n'a remport� une �preuve (�tape, en l'occurrence) de course au large en �quipage. Et � domicile, qui plus est! Caudrelier et son �quipage vont donc sans doute �crire une belle page de l'histoire maritime dans une �tape longue de quelque 4700 milles (environ 8700 km), tr�s complexe, avec des passages � hauts risques comme le d�troit de Malacca et le louvoyage le long des c�tes vietnamiennes.
Une chose est s�re: Dongfeng m�rite de remporter cette 3e �tape tant sa domination a �t� �crasante depuis le d�part d'Abou Dhabi, le 3 janvier.
S'il gagne, le bateau chinois -� �galit� de points avec Azzam et le N�erlandais Brunel (Bouwe Bekking) avant le d�part de cette manche- prendra la t�te du classement g�n�ral provisoire. La course est encore longue jusqu'au finish le 27 juin � Goeteborg (Su�de) mais belle d�monstration!
Apr�s l'escale de Sanya, les six VOR65 -des monocoques monotypes de 20 m de long- se rendront � Auckland. C'est dans cette Mecque de la voile et sur ce m�me parcours Chine - Nouvelle-Z�lande qu'un autre Fran�ais, Franck Cammas , avait gagn� sa premi�re �tape lors de la pr�c�dente Volvo Ocean Race.
Une victoire de bon augure puisque Cammas et ses hommes avaient remport� l'ensemble de la course sur Groupama 4, en juillet 2012 � Galway ( Irlande ) quelques semaines plus tard.
Classement lundi � 13h40 heure fran�aise (12h40 GMT):
1. Dongfeng Race Team (CHN/Charles Caudrelier) � 87,1 milles de l'arriv�e
2. Abu Dhabi Ocean Racing (EAU/Ian Walker) � 41,9 milles du premier
3. Team Alvimedica ( USA -TUR/Charlie Enright) � 52
4. Mapfre (ESP/Xabi Fernandez) � 58,5
5. Team Brunel (NED/Bouwe Bekking) � 61,1
6. Team SCA (SWE/Samantha Davies) � 141,2
7. Team Vestas Wind (DEN/Chris Nicholson) -- n'a pas pris le d�part (endommag� lors de son �chouement dans la 2e �tape)
Partager
