TITRE: Open d'Australie: Murray et Sharapova surfent sur la nouvelle vague | Site mobile Le Point
DATE: 27-01-2015
URL: http://www.lepoint.fr/sport/open-d-australie-nadal-martyrise-par-son-souffre-douleur-berdych-27-01-2015-1899884_26.php
PRINCIPAL: 0
TEXT:
Open d'Australie: Murray et Sharapova surfent sur la nouvelle vague
William West AFP
Andy Murray et Maria Sharapova ont ma�tris� la rel�ve du tennis pour voguer vers les demi-finales de l'Open d' Australie qui se d�rouleront sans Rafael Nadal, submerg� par le "raz-de-mar�e" Tomas Berdych, mardi � Melbourne.
En engloutissant Nadal en trois sets 6-2, 6-0, 7-6 (7/5), le Tch�que, 7e mondial, a cr�� des remous dans le tournoi des Antipodes, d�j� priv� d'un autre gros poisson, Roger Federer, depuis le troisi�me tour.
Son succ�s est des plus inattendus car il restait sur dix-sept d�faites d'affil�e contre le Majorquin, qu'il n'avait plus battu depuis 2006 � Madrid. Jamais dans l'histoire de l'�re Open (depuis 1968), un tennisman n'avait subi autant d'�checs successifs contre un m�me joueur.
"J'�tais pr�t pour �a et j'ai tr�s bien appliqu� mon plan de jeu pendant trois sets", a expliqu� Berdych, battu l'an pass� en demi-finales par le Suisse Stan Wawrinka.
A l'intersaison, le Tch�que avait d�cid� de changer d'entra�neur, pour franchir un cap en remportant enfin un tournoi majeur, lui qui avait �chou� en finale de Wimbledon en 2010 face � Nadal.
�conduit par le l�gendaire Ivan Lendl, le Tch�que avait jet� son d�volu sur le V�n�zu�lien Dani Vallverdu, ancien coach assistant d'Andy Murray. �a tombe bien, l'�cossais, qui a douch� la fougue du jeune Australien Nick Kyrgios (6-3, 7-6 (7/5), 6-3), 19 ans, sera son prochain adversaire.
Mais avoir son ancien entra�neur dans de camp d'en face n'est pas n�cessairement un handicap selon le Britannique. "Je sais aussi ce que Dani pense du jeu de Tomas, donc cela marche dans les deux sens."
Berdych semble avoir r�colt�, en acc�l�r�, les fruits du travail entrepris avec son nouvel entra�neur. Rel�ch� et d�complex�, il a inond� de coups gagnants Nadal qui n'a refait surface que dans le troisi�me set.
Un z�ro point� dans une manche, cela n'�tait arriv� que deux fois � Nadal en Grand Chelem, contre Roger Federer � Wimbledon en 2006 (finale) et face � Andy Roddick en 2004 � l'US Open (deuxi�me tour).
"Avant ce set, je n'avais pas la bonne attitude, je jouais trop court et je lui ai rendu les choses faciles", a estim� le Majorquin, conscient de devoir encore "travailler dur" pour retrouver son meilleur niveau.
- Sharapova sans piti� -
Min� par des probl�mes physiques (poignet) et une op�ration de l'appendicite, il n'avait disput� que sept matches lors de la deuxi�me moiti� de la saison 2014.
Sa d�faite d�s son entr�e en lice � Doha d�but janvier pour sa reprise, face au modeste allemand Michael Berrer, ne l'avait pas rassur�. Et son parcours � Melbourne a �t� fait "de hauts et de bas", avec un sauvetage de justesse au deuxi�me tour.
Pass� tout pr�s de la noyade au m�me stade, o� elle avait d� sauver deux balles de match, Maria Sharapova navigue depuis sur des mers paisibles, que la jeune et ambitieuse Canadienne Eugenie Bouchard n'a pas r�ussi � troubler.
A l'image de Murray contre Kyrgios, la "Tsarine" a fait parler son exp�rience pour l'emporter 6-3, 6-2 et s'offrir une dix-neuvi�me demi-finale de Grand Chelem.
- La surprise Makarova -
Sa concurrente qu�b�coise, incarnation � 20 ans de l'avenir du tennis, lui avait donn� du fil � retordre � Roland-Garros mais cette fois-ci "Masha" s'est montr�e sans piti�.
"Elle jouait vraiment tr�s bien jusqu'ici, en �tant en pleine confiance et agressive. Mais aujourd'hui, j'ai fait du tr�s bon boulot en r�ussissant � la faire douter", s'est f�licit�e Sharapova, qui d�tr�nera l'Am�ricaine Serena Williams du sommet mondial si elle remporte un sixi�me troph�e majeur � Melbourne.
Elle partira favorite en demies contre sa compatriote Ekaterina Makarova, 11e mondiale, qu'elle a toujours battue en cinq matches.
Sharapova devra toutefois se m�fier. Car Makarova a frapp� fort en coulant la N.3 mondiale Simona Halep en deux sets (6-4, 6-0), confirmant les progr�s accomplis, en 2014, ann�e de sa premi�re demi-finale majeure, � l'US Open.
27/01/2015 16:56:20 - Melbourne (AFP) - Par Ludovic LUPPINO - � 2015 AFP
