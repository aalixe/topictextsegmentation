TITRE: Loire-Atlantique : une polici�re bless�e d'un coup de barre de fer
DATE: 27-01-2015
URL: http://www.rtl.fr/actu/societe-faits-divers/loire-atlantique-une-policiere-blessee-d-un-coup-de-barre-de-fer-7776352548
PRINCIPAL: 1228628
TEXT:
Accueil Actu Soci�t� et faits divers Loire-Atlantique : une polici�re bless�e d'un coup de barre de fer
Loire-Atlantique : une polici�re bless�e d'un coup de barre de fer
Une jeune polici�re a �t� bless�e � coups de barre de fer par un automobiliste qu'elle tentait d'interpeller.
Cr�dit : PATRICK KOVARIK / AFP
publi� le 27/01/2015 � 14:02
Partager
Commenter
Imprimer
Une polici�re a �t� bless�e d'un coup de barre de fer alors qu'elle tentait d'interpeller un automobiliste en fuite dans la nuit de lundi � mardi 27 janvier � Montoir-de-Bretagne (Loire-Atlantique), a-t-on appris de source polici�re. L'homme est toujours en fuite.
Le suspect avait un peu plus t�t forc� un barrage de police mis en place dans la cadre d'une op�ration de d�pistage syst�matique d'alcool�mie � Saint-Nazaire.�Pris en chasse par un v�hicule de police, l'automobiliste avait fini par abandonner son v�hicule � Montoir, avant de s'enfuir � pied, poursuivi par deux gardiens de la paix.
Le suspect, identifi�, est toujours en fuite
Alors que ces derniers tentaient de l'interpeller, il avait r�ussi � prendre la fuite apr�s avoir ass�n� un coup de barre de fer � une polici�re de 34 ans, selon la police.�La jeune femme a �t� conduite au centre hospitalier de Saint-Nazaire et�plac�e en observation pour la nuit, selon la m�me source.
Une polici�re frapp�e cette nuit avec une barre de fer lors d'un contr�le routier � @VilleStNazaire . L'auteur r�ussit � prendre la fuite.
� Police Nationale 44 (@PNationale44) 27 Janvier 2015
Rapidement identifi�, l'auteur pr�sum� des faits, �g� de 35 ans et d�j� connu des services de police, �tait toujours recherch� mardi en milieu de journ�e.
La r�daction vous recommande
par La r�daction num�rique de RTL
Suivez La r�daction num�rique de RTL sur :
VOUS AIMEREZ AUSSI
