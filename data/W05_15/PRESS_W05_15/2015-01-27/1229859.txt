TITRE: Neuf morts dont cinq �trangers dans une attaque contre un h�tel de Tripoli
DATE: 27-01-2015
URL: http://www.romandie.com/news/Neuf-morts-dont-cinq-etrangers-dans-une-attaque-contre-un-hotel-de-Tripoli_RP/559699.rom
PRINCIPAL: 0
TEXT:
Tweet
Neuf morts dont cinq �trangers dans une attaque contre un h�tel de Tripoli
Tripoli - Neuf personnes dont cinq �trangers ont �t� tu�es mardi dans un assaut de plusieurs heures lanc� contre un h�tel de Tripoli par des hommes arm�s qui au final se sont fait exploser, nouvel exemple du chaos r�gnant en Libye.
Au moment o� l'attaque �tait en cours contre le grand h�tel Corinthia dans le centre de la capitale, la branche libyenne du groupe jihadiste Etat islamique l'a revendiqu�e, selon le centre am�ricain de surveillance des sites islamistes SITE.
On ignorait dans l'imm�diat la nationalit� des �trangers qui ont �t� tu�s par balles par les assaillants, mais un porte-parole des services de s�curit� a pr�cis� que deux femmes figuraient parmi eux.
La capitale libyenne est contr�l�e par Fajr Libya, une puissante coalition de milices notamment islamistes, qui a install� un gouvernement parall�le � Tripoli apr�s en avoir chass� le gouvernement reconnu par la communaut� internationale.
L'assaut contre le Corinthia, connu pour accueillir des diplomates, des responsables libyens et des �trangers, a commenc� le matin par l'explosion d'une voiture pi�g�e devant l'�tablissement.
Aussit�t apr�s, trois hommes arm�s ont p�n�tr� dans l'h�tel, avant d'�tre pourchass�s par les forces de s�curit� relevant du gouvernement pro-Fajr Libya, qui ont �galement assi�g� le Corinthia.
L'assaut s'est achev� en milieu d'apr�s-midi avec la mort des trois assaillants qui ont d�ton� les ceintures explosives qu'ils transportaient, alors qu'ils �taient encercl�s au 24e �tage, selon Issam Al-Naass, le porte-parole des op�rations de s�curit� � Tripoli.
Bilan: neuf morts. Outre les cinq �trangers, une sixi�me personne prise en otage par les assaillants et dont la nationalit� n'�tait pas connue a p�ri lorsque les hommes arm�s se sont fait exploser, a-t-il pr�cis�.
Trois membres des services de s�curit� ont �galement �t� tu�s, et cinq personnes bless�es.
Le 24e �tage de l'h�tel est normalement r�serv� � la mission diplomatique du Qatar mais aucun diplomate ne s'y trouvait au moment de l'attaque, selon une source de s�curit�.
- un coup aux efforts de paix -
Le chef du gouvernement auto-proclam� en Libye, Omar al-Hassi, se trouvait � l'int�rieur de l'h�tel au moment de l'assaut mais il a �t� �vacu� sain et sauf, selon M. Naass.
Dans un communiqu�, le gouvernement parall�le a affirm� que les auteurs de l'attaque voulaient tuer le Premier ministre. Ils ont imput� cette tentative d'attentat au criminel de guerre Khalifa Haftar, un g�n�ral controvers� qui a lanc� ces derniers mois une op�ration pour reprendre Benghazi aux groupes arm�s islamistes qui contr�lent la ville.
La branche libyenne du groupe jihadiste EI qui s�vit notamment en Irak et en Syrie, et a d�j� revendiqu� plusieurs attaques en Libye a, dans un message sur Twitter, affirm� que ses membres avaient pris d'assaut l'h�tel, selon SITE.
Dans une premi�re r�action � l'attaque contre le Corinthia, la repr�sentante de la diplomatie europ�enne Federica Mogherini a condamn� un acte de terrorisme r�pr�hensible qui porte un coup aux efforts destin�s � r�tablir la paix et la stabilit� dans le pays, en allusion aux n�gociations en cours � Gen�ve.
Depuis la chute de Mouammar Kadhafi au terme de huit mois de r�volte en 2011, les autorit�s de transition n'ont pas r�ussi � imposer leur pouvoir sur les nombreuses milices form�s d'ex-rebelles.
Le gouvernement et le Parlement reconnus par la communaut� internationale, chass�s de Tripoli, si�gent dans l'est de la Libye. Les milices rivales pro et antigouvernementales continuent � se disputer les territoires et la manne p�troli�re au prix de combats meurtriers.
Malgr� la tr�ve annonc�e par les milices en vertu d'un accord conclu � Gen�ve en janvier, les combats meurtriers ont continu� notamment � Benghazi (est) o� le g�n�ral Haftar, appuy� par les forces gouvernementales, tente de reprendre la ville. Les violences ont fait 18 morts et 44 bless�s � Benghazi en 24 heures, a affirm� lundi soir une source de s�curit�.
Lundi �galement, Fajr Libya a tir� trois missiles sur des r�servoirs du terminal p�trolier d'Al-Sidra, dans le croissant p�trolier, une r�gion dont elle cherche � s'emparer.
(�AFP / 27 janvier 2015 19h40)
�
