TITRE: Autoroutes: le gouvernement suspend la hausse des tarifs des péages
DATE: 27-01-2015
URL: http://www.lefigaro.fr/flash-eco/2015/01/27/97002-20150127FILWWW00203-autoroute-vers-un-gel-des-tarifs.php
PRINCIPAL: 0
TEXT:
Autoroutes: le gouvernement suspend la hausse des tarifs des péages
< Envoyer cet article par e-mail
X
Séparez les adresses e-mail de vos contacts par des virgules.
De la part de :
J'accepte de recevoir la newsletter quotidienne du Figaro.fr
Oui
Réagir à cet article
Publicité
C'est vraiment n'importe quoi. Pour être pragmatique, il faut prendre des décisions en tant qu'actionnaires et donc racheter des autoroutes. Sur ce site, ils ont déjà réunis plus d'1 million d'euros d'intentions en quelques semaines https://hoolders.com/rachetonslesautoroutes/
C'est selon moi la position la plus efficace
Le 02/02/2015 à 12:48
patrice R.
Quel lamentable exemple que celui donné par l'état en ne respectant pas les termes d'un contrat (certe négocié et signé par l'ump)!!!
Il faut de plus noter que les tarifs autoroutiers augmente moins vite depuis la privatisation des autoroutes par rapport à leur évolution avant celle ci.
Enfin, personne n'est obligé d'emprunter les autoroutes!
Le 28/01/2015 à 13:23
jeanluc85
Comme les sociétés d'autoroute vont attaquer l'état devant les tribunaux, comme l'état va perdre, les sociétés d'autoroute augmenterons un peu plus pour rattraper le retard et avec de la chance l'état sera condamné à verser des réparations donc comme d'habitude avec ce gouvernement combien va nous coûter cette petite plaisanterie. De plus bloquer le prix des péages, ce n'est pas cela qui va redonner du pouvoir d'achat à la majorité des Français mais ce gouvernement faisant toujours n'importe quoi, n'importe comment à et /ou avec n'importe qui continu dans la médiocrité des ses mesurettes
Le 28/01/2015 à 09:04
