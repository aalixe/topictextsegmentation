TITRE: 3 morts dans une attaque de l'EI à Tripoli
DATE: 27-01-2015
URL: http://www.lefigaro.fr/flash-actu/2015/01/27/97001-20150127FILWWW00180-attentat-a-tripoli-revendications-de-l-ei.php
PRINCIPAL: 1228356
TEXT:
le 27/01/2015 à 12:05
Publicité
Des hommes armés ont attaqué aujourd'hui un hôtel de luxe de Tripoli où résident souvent des responsables gouvernementaux et des délégations étrangères. Le groupe djihadiste Etat Islamique a revendiqué l'assaut qui a commencé par l'explosion d'une voiture piégée. La déflagration a tué trois gardes.
Trois djihadistes au moins se sont ensuite engouffrés dans l'hôtel, affrontant les services de sécurité. Les forces de sécurité évacuent les clients étage par étage. Il y a des tirs entre les assaillants et les gardes", a dit le porte-parole du bureau de sécurité de Tripoli.
Un assaillant a été arrêté, a-t-il déclaré, et l'hôtel est encerclé par les forces de sécurité. Selon lui, "il est plus que probable qu'il y ait des otages détenus par les hommes armés au 23e étage". Une chaîne de télévision, basée à Tripoli, a déclaré que de "hauts responsables" se trouvaient dans l'hôtel.
LIRE AUSSI :
