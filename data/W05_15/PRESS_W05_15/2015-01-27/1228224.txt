TITRE: Nantes. La maison De Ligonn�s est toujours � vendre � 296 900 � | Courrier de l'Ouest
DATE: 27-01-2015
URL: http://www.courrierdelouest.fr/actualite/nantes-la-maison-de-ligonnes-est-toujours-a-vendre-a-296-900-27-01-2015-205773
PRINCIPAL: 0
TEXT:
Lundi encore, les volets de la maison de Nantes restaient clos.
Photo Presse Oc�an
#Nantes
L�annonce est volontairement discr�te. Plusieurs acheteurs potentiels se sont d�j� manifest�s.
"� vendre sur Nantes, rond-point de Rennes, maison r�nov�e, comprenant salon s�jour de 30 m2, 5 chambres, un bureau, terrain de 300m2 �.�
Pour illustrer l�annonce, trois photos, d�une pi�ce de vie sur parquet, d�une chambre mansard�e et du jardin arbor�.
Mais aucune photo de la fa�ade, ni aucune autre pr�cision sur l�emplacement exact de cette maison, dont le prix affich� est nettement inf�rieur aux prix du march� nantais. Pour une surface habitable de 122 m2, le bien est vendu � 296 900 �.
Renseignements pris, ce type de produit pourrait �tre affich� � 420 000 �, au moins, selon un connaisseur.
Impossible � deviner d�embl�e : cette propri�t� est situ�e au� 55 boulevard Schumann, � Nantes. Une adresse tristement c�l�bre � Nantes.
C�est ici, en effet, que Xavier Dupont De Ligonn�s est soup�onn� d�avoir ex�cut� sa femme et ses enfants. C��tait en avril 2011. Il y a bient�t quatre ans...
