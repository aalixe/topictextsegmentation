TITRE: Demis Rous�sos�: sa famille avait d�cid� de lui cacher la gravit� de sa mala�die - Voici
DATE: 27-01-2015
URL: http://www.voici.fr/news-people/actu-people/demis-roussos-sa-famille-avait-decide-de-lui-cacher-la-gravite-sa-maladie-551910
PRINCIPAL: 1229030
TEXT:
Publi� le mardi 27 janvier 2015 � 09:25� par Anthony Martin
Demis Rous�sos�: sa famille avait d�cid� de lui cacher la gravit� de sa mala�die
� Il igno�rait qu�il avait un cancer �
Demis Rous�sos nous a quit�t�s dans la nuit de samedi � dimanche d�un cancer de l�es�to�mac. Au lende�main du drame, sa fille Emily t�moigne et nous apprend que le chan�teur n��tait pas au courant de �la gravit� de sa mala�die.
Apr�s le choc de l�an�nonce, les t�moi�gnages. Disparu dans la nuit de samedi � dimanche des suites d�un cancer de l�es�to�mac, le chan�teur Demis Rous�sos laisse derri�re lui des fans et une grande famille en deuil. Sa fille, Emily, inter�ve�nait hier dans les colonnes du Figaro �:���Mon p�re est d�c�d� dimanche � 9h30 du matin des suites d'un cancer de l'esto�mac, du pancr�as et du foie. Mon fr�re, ma belle-m�re et moi �tions � son chevet jusqu'� ses derni�res heures. Il m'a sembl� normal de pr�ve�nir Nikos Alia�gas , un ami proche de la famille depuis tr�s long�temps. Il aimait �nor�m�ment mon p�re et r�ci�proque�ment. Je lui ai donc donn� l'auto�ri�sa�tion de trans�mettre cette terrible nouvelle, � la France, un pays tr�s cher � mon p�re.��
Dans Paris Match , Emily en dit plus sur les circons�tances de la dispa�ri�tion de son p�re. Son cancer (d�j� � un stade tr�s avanc�) a �t� d�cel� en avril dernier, suite � une visite de routine chez le m�de�cin�:���Connais�sant sa peur panique concer�nant la mala�die et la mort, ma belle-m�re, mon fr�re, son prati�cien et moi, d�ci�dons de ne rien lui dire��. Pour qu�il accepte de suivre une chimio�th�ra�pie, ses proches lui parlent de quelques cellules canc�reuses � suppri�mer rapi�de�ment.
Aujourd��hui disparu, Demis Rous�sos laisse un grand vide derri�re lui�: ��Il vient de nous quit�ter et je persiste � voir sa grande pr�sence d�am�bu�ler dans la maison��. Ses obs�ques auront lieu vendredi midi dans le grand cime�ti�re d�Ath�nes�: ��Je tiens � ce qu'elles soient ouvertes � tous�: aux jour�na�listes, aux fans, aux gens� Des artistes et amis de mon p�re seront aussi pr�sents. Il y a beau�coup de chan�teurs, de musi�ciens qui le respec�taient, et que lui respec�tait aussi.�� Gageons qu�il y aura foule.
Mots-cl�s :
