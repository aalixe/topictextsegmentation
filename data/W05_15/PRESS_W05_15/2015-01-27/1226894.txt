TITRE: VIDEO. Affaire Bettencourt : d�but du proc�s
DATE: 27-01-2015
URL: http://www.francetvinfo.fr/replay-jt/france-3/19-20/video-affaire-bettencourt-debut-du-proces_807831.html
PRINCIPAL: 1226859
TEXT:
/ 19/20
VIDEO. Affaire Bettencourt : d�but du proc�s
Ce lundi 26 janvier s'ouvre au tribunal correctionnel de Bordeaux le proc�s de l'affaire Bettencourt. Dix personnes sont sur le banc des pr�venus, soup�onn�es d'avoir soutir� de l'argent � la milliardaire Liliane Bettencourt.
(FRANCE 3)
, publi� le
27/01/2015 | 00:33
La premi�re journ�e du proc�s de l'affaire Bettencourt a �t� tr�s vite perturb�e. Le procureur a annonc� que l'un des dix pr�venus, l'ex-infirmier de la milliardaire, avait tent� de se suicider durant le week-end. Il est aujourd'hui entre la vie et la mort.
Fran�ois-Marie Banier lui aurait soutir� 400 millions d'euros
Outre l'ex-infirmier, on compte parmi les dix pr�venus Fran�ois-Marie Banier, le photographe qui aurait soutir� 400�millions d'euros � Liliane Bettencourt � une p�riode o� la milliardaire n'�tait plus, selon ses m�decins, en pleine possession de ses moyens. Le photographe est la principale cible de�la fille de la victime, Fran�oise Meyers�: " Pas tellement pour l'argent, mais surtout pour la fa�on dont il a su, � l'image de certains gourous, comme un pr�dateur, diviser la famille", explique ma�tre Beno�t Ducos-Ader, avocat de Liliane Bettencourt, au micro de France�3. Parmi les autres pr�venus, �ric Woerth, l'ancien ministre de Nicolas Sarkozy, est quant � lui accus� de recel�: il aurait per�u des sommes en esp�ces qui auraient servi � financer la campagne pr�sidentielle de 2007. Il a toujours ni� les faits.
Le JT
Les autres sujets du JT
1
