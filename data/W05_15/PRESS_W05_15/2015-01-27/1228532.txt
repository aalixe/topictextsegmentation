TITRE: www.larep.fr - Sant� - Beaut� - L'infection nosocomiale toujours int�gralement indemnis�e
DATE: 27-01-2015
URL: http://www.larep.fr/france-monde/actualites/societe/sante-beaute/2015/01/25/l-infection-nosocomiale-toujours-integralement-indemnisee_11303984.html
PRINCIPAL: 1228530
TEXT:
(cliquez sur le code s'il n'est pas lisible)
Tous les articles
Le r�gime m�diterran�en est aussi plus �cologique
Lu 153 fois
(Relaxnews) - Alors que les bienfait sur la sant� du r�gime m�diterran�en ne sont plus � prouver, une �quipe de scientifiques s'est int�ress�e � l'empreinte carbone de ce type d'alimentation.Pour cette �tude, une �quipe espagnole a analys� l'empreinte carbone de menus quotidiens se...
Marre de vos coll�gues r�leurs ? Vous n'�tes pas seul
(AFP) - Avoir un coll�gue qui r�le tout le temps ou qui d�gage des effluves d�sagr�ables est le pire cauchemar des salari�s, selon un sondage OpinionWay pour Monster publi� lundi.Interrog�s sur les d�fauts les plus p�nibles que peuvent avoir leurs coll�gues, 36% des salari�s citent...
Sidaction 2015 : 4.250.000 euros r�colt�s cette ann�e
Lu 7 fois
(Relaxnews) - Les promesses de dons sont en baisse cette ann�e, par rapport aux 5 millions d'euros de 2014.Pendant les trois jours de manifestation, 22 m�dias partenaires, ainsi que des artistes ont port� l'�v�nement. Les fonds recueillis seront utilis�s pour la recherche, la pr�vention...
Recette : Japchae, vermicelles saut�s � la viande et aux l�gumes
Lu 1 fois
(Relaxnews) - Pour 4 personnes - Pr�paration : 2 heures - Cuisson : 27 min     50g de b�uf (culotte)2 champignons parfum�s3g de champignons noirsSauce d'assaisonnement 1 : une demi C�S de sauce soja, une demi C�S de sucre, une demi C�S de ciboule hach�, une demi C�S de graines de...
Le lien social, la cl� du moral des s�niors
Lu 190 fois
(Relaxnews) - Les Fran�ais �g�s de 55 ans et plus ont le moral, pointe une �tude r�alis�e par TNS Sofres pour Cogedim Club. En 2015, 85% s'estiment satisfaits de leur quotidien.Les seniors semblent avoir trouv� la cl� du bonheur. La moiti� des personnes interrog�es estiment que le...
Un week-end pour prendre soin de son coeur
Lu 86 fois
(Relaxnews) - Les 28 et 29 mars se tient la 40e �dition des Parcours du Coeur, avec plus de 800 �v�nements partout en France. Ce week-end de pr�vention contre les accidents et maladies cardiovasculaires est organis� par la F�d�ration fran�aise de cardiologie (FFC).Durant ces deux...
Google et Johnson & Johnson s'allient dans les robots chirurgicaux
Lu 57 fois
(AFP) - Le groupe pharmaceutique am�ricain Johnson & Johnson (J&J) et le g�ant de l'internet Google vont fabriquer ensemble des robots chirurgicaux, un cr�neau en plein boom, afin d'aider les praticiens lors d'op�rations, a annonc� vendredi le premier.Ce partenariat strat�gique,...
R�duire sa consommation de graisse pour se sentir plus serein ?
Lu 6319 fois
(Relaxnews) - Les r�gimes trop riches en graisse peuvent entra�ner une prise de poids et des probl�mes cardiovasculaires, mais aussi la d�pression et des changements de comportement, selon une �tude publi�e par l'universit� am�ricaine de l'�tat de Louisiane aux �tats-Unis.Ces f�cheuses...
