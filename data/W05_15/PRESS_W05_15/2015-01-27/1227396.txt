TITRE: M�t�o: La neige transforme New York en ville fant�me -   Monde - tdg.ch
DATE: 27-01-2015
URL: http://www.tdg.ch/monde/tempete-neige-paralyse-nordest-usa/story/28573108
PRINCIPAL: 1227392
TEXT:
Images
La neige transforme New York en ville fant�me
M�t�oPonts et tunnels ferm�s, transports en communs arr�t�s, circulation interdite, New York, la ville qui ne dort jamais, connaissait un d�but de nuit �trangement calme, en raison d'une temp�te de neige.
27.01.2015
Vous voulez communiquer un renseignement ou vous avez rep�r� une erreur ?
Orthographe
Description de l'erreur*
Veuillez SVP entrez une adresse e-mail valide
Un avis de blizzard �tait en place jusqu'� mercredi dans sept Etats du nord-est, dont celui de New York, affectant des millions d'Am�ricains, qui se sont pr�cipit�s lundi pour rentrer t�t chez eux, afin d'�viter le pire des chutes de neige, annonc�es avec des vents violents pour la nuit de lundi � mardi.
La neige tombait drue lundi soir � New York o� des centaines de chasse-neige �taient mobilis�s contre le blizzard. Le maire Bill de Blasio a appel� la population � sortir le moins possible, et a interdit la circulation de tous les v�hicules - sauf v�hicules d'urgence - � partir de 23 heures, une mesure rarissime.
La neige est tomb�e drue lundi apr�s-midi � New York, et s'est arr�t�e en soir�e. Elle a repris quelques heures plus tard.
Des centaines de chasse-neige et employ�s municipaux ont �t� mobilis�s, et le maire Bill de Blasio a appel� la population � sortir le moins possible. Il a annonc� que les �coles resteraient ferm�es mardi.
Vid�o AFP: Le blizzard r�duit la grouillante New York au silence
�D'habitude, je rentre chez moi vers 18h ou 18h30. Mais aujourd'hui, je suis parti deux heures plus t�t�, confiait � la gare de Grand Central quasi-d�serte en milieu d'apr�s-midi Joseph Burke, un avocat vivant en banlieue.
M�tro ferm�
Selon la m�t�o nationale, il devait tomber durant la nuit de lundi � mardi 24 cm de neige � New York, avec des bourrasques de vent allant jusqu'� 69 km/heure, et encore 21 cm dans la journ�e de mardi, s'ajoutant aux 11 cm tomb�s lundi � Central Park.
�La temp�te va s'aggraver durant la nuit�, a d�clar� lundi en fin d'apr�s-midi le gouverneur de l'Etat de New York Andrew Cuomo, annon�ant la fermeture du m�tro, qui fonctionne en principe 24 heures sur 24, pour une dur�e ind�termin�e.
Les trains de banlieue, les trains reliant Boston � New York, les tunnels, les ponts, les parcs ont �galement �t� tous �t� arr�t�s ou ferm�s pour une dur�e ind�termin�e.
Broadway a annul� lundi soir ses c�l�bres spectacles, le si�ge de l'ONU devait rester ferm� mardi, mais la Bourse sera ouverte. Les fonctionnaires non essentiels de l'Etat ont �galement �t� pri�s de rester chez eux.
Des milliers de vols annul�s
Dans la perspective du blizzard, que la m�t�o nationale a d�crit comme �potentiellement historique�, une interdiction de circuler a �t� d�cr�t�e dans plusieurs Etats du nord-est, dont New York, Connecticut, New Jersey et Massachusetts.
Plus de 2700 vols ont �t� annul�s lundi, et plus de 3700 l'�taient pour mardi.
�Ce n'est pas une temp�te typique�, a insist� lundi le maire de New York. �Rentrez t�t chez vous�, a-t-il ajout�, r�p�tant que le blizzard devrait �tre �l'un des plus importants de l'histoire de la ville� de 8,4 millions d'habitants.
� The Weather Network (@weathernetwork) January 26, 2015
Le record pour une temp�te de neige � New York date de 2006, quand 68,32 cm de neige avaient �t� enregistr�s � Central Park les 11 et 12 f�vrier.
Pannes d'�lectricit�
La temp�te actuelle, qui concerne une bande de plus de 450 km de longueur, allant de Philadelphie � l'Etat du Maine, devrait s'�loigner progressivement mercredi.
�Les New-Yorkais doivent se pr�parer � de possibles pannes d'�lectricit�, dues aux vents violents qui pourraient faire tomber les lignes et les arbres�, a aussi averti le gouverneur de l'Etat de New York.
Les New-Yorkais inquiets ont dimanche et lundi d�valis� les supermarch�s pour faire des provisions, patientant parfois dans le froid avant de pouvoir y entrer acheter pain, lait, fruits et l�gumes.
The #blizzardof2015 will come in waves & up to 58M people could be put into the deep freeze: http://t.co/1j5ahCpzyi pic.twitter.com/Mli0emM4qn
� The Lead CNN (@TheLeadCNN) January 26, 2015
50 � 76 cm de neige attendus � Boston
Dans la r�gion de Boston, o� de 50 � 76 cm de neige sont attendus, les bourrasques de vent pourraient d�passer 100 km/heure.
Les �coles y seront aussi ferm�es mardi, selon les autorit�s et une interdiction de circuler � partir de lundi soir a �t� impos�e dans tout l'Etat du Massachusetts, comme dans l'Etat proche du Connecticut.
La s�lection du jury dans le proc�s de Djokhar Tsarnaev, accus� des attentats de Boston en 2013, a �galement �t� suspendue, le tribunal annon�ant qu'il resterait ferm� mardi.
This is the scene outside the @WholeFoods in Union Square, NYC right now. Chaos. (photo by @acpants ) #blizzardof2015 pic.twitter.com/Kg7rDLaOVG
� Brian Ries (@moneyries) January 26, 2015
Boston Mayor: City preparing 'for the worst', we're expecting 24-30 inches: http://t.co/xEKulRW4E6 #blizzardof2015 pic.twitter.com/OlPKqak5tl
� The Situation Room (@CNNSitRoom) January 26, 2015
(afp/Newsnet)
27.01.2015
Galerie photo
Sur Twitter, les Am�ricains pr�f�rent rire de la temp�te Alors que le blizzard fait les gros titres de la presse internationale, les Am�ricains pr�f�rent en rire sur Twitter.
Articles en relation
