TITRE: JIM.fr - Identification
DATE: 27-01-2015
URL: http://www.jim.fr/medecin/actualites/flash/e-docs/les_personnels_de_letablissement_francais_du_sang_appeles_a_la_greve_demain__150038/document_flash.phtml
PRINCIPAL: 1227908
TEXT:
Déjà inscrit au site ? Saisissez vos codes d’accès.
Identifiant *
Mémoriser mon login et mot de passe
Se connecter
Si vous avez oublié votre mot de passe, cliquez ici
.
Certaines de ces rubriques sont libres d’accès pour tous les professionnels de santé inscrits,
d’autres, et toutes les archives, requièrent un abonnement Premium.
Pour des raisons réglementaires, l’inscription individuelle des professionnels de santé est indispensable pour accéder au site.
ATTENTION :
Il est indispensable, pour vous identifier :
que votre ordinateur soit à l'heure (ainsi qu'à la bonne date)
que votre navigateur (Internet Explorer, Firefox, Chrome, Safari...) accepte les cookies.
Vous pouvez consulter cette page si vous avez besoin d'aide : Comment activer les cookies
Héritier du Journal International de Médecine
publié sous sa version papier d’octobre 1979 à juillet 1998, JIM.fr est aujourd’hui l'un des premiers sites médicaux francophones selon toutes les enquêtes de lecture.
Mis à jour 7 jour sur 7, JIM comporte des rubriques :
d’Information :
actualités médicales (presse internationale et congrès) et professionnelles, grandes études cliniques, JIM+...
de FMC :
mises au point, base de données iconographiques, diagnostics, bibliothèque et vidéothèque...
de Communication :
Débats, Dossiers, Sondages, Réactions, carrefour des sites...
et un moteur de recherche comportant plus de 70 000 textes
Dossier du JIM
