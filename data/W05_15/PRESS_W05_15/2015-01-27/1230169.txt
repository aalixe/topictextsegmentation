TITRE: Castro prend acte du rapprochement avec les Etats-Unis
DATE: 27-01-2015
URL: http://www.boursorama.com/actualites/castro-prend-acte-du-rapprochement-avec-les-etats-unis-d55bed02c418281cb659c529e4723589
PRINCIPAL: 1230166
TEXT:
Castro prend acte du rapprochement avec les Etats-Unis
AFP Video le
L'ex-pr�sident cubain Fidel Castro a rompu lundi un silence de plusieurs mois en confiant dans une lettre qu'il ne faisait pas confiance aux �tats-Unis mais qu'il ne rejetait pas pour autant le rapprochement r�cemment engag� avec Washington. Dur�e 00:53
Copyright � 2015 AFP. Tous droits de reproduction et de repr�sentation r�serv�s.
Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par l'AFP. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, transmise, rediffus�e, traduite, vendue, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de l'AFP. l'AFP ne pourra �tre tenue pour responsable des d�lais, erreurs, omissions, qui ne peuvent �tre exclus ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
