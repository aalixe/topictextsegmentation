TITRE: Auschwitz, la lib�ration - Le Document d'iTELE � iTELE
DATE: 27-01-2015
URL: http://www.itele.fr/magazines/le-document-itele/auschwitz-la-liberation-109279
PRINCIPAL: 0
TEXT:
Google+
Le 27 janvier 2015, plusieurs pays comm�morent la lib�ration du camp d'Auschwitz, et de tous les camps Nazis, il y a 70 ans. Les survivants des camps sont aujourd'hui de moins en moins nombreux � pouvoir t�moigner de l'horreur de la d�portation. Ida, Ivan, Zdzislawa, trois destins entrecrois�s, nous racontent comment se sont d�roul�s ces �v�nements. De la marche de la mort � l'ouverture du camp, de la lib�ration au retour � la vie.
De la marche de la mort � l�ouverture du camp, Le Document retrace la journ�e du 27 janvier 1945 autour des t�moignages de deux rescap�es ainsi que de l�un des premiers officiers Russe � avoir p�n�tr� � Auschwitz.
27 janvier 1945 : les Russes d�couvrent le camp d'Auschwitz-Birkenau
Le 27 janvier 1945, plus de 6 mois apr�s le d�barquement alli� en Normandie, 5 mois apr�s la lib�ration de Paris, les Russes � l�Est et les Anglo-saxons � l�Ouest accentuent leur pouss�e sur les flancs de l�Allemagne nazie. Sur la trajectoire de leurs combats, les Sovi�tiques d�couvrent le camp de Majdanek, d�abord, puis en Haute-Sil�sie, le plus grand syst�me concentrationnaire et d�extermination �labor� par les Allemands : Auschwitz-Birkenau. Le site o� 1 100 000 victimes sont mortes pendant la Seconde Guerre mondiale.
C�est  l�ouverture du camp. Mais ce n�est pas synonyme de lib�ration pour tous les d�port�s. Car la grande majorit� d�entre eux a �t� �vacu�e de force par les nazis quelques jours avant l�arriv�e des Russes. Pour eux, la fin du calvaire prendra encore plusieurs mois.
Des derniers soubresauts du camp d�Auschwitz-Birkenau, ils sont de moins en moins nombreux � pouvoir t�moigner. Les survivants ont 80 ans, 90 ans. Certains font encore le voyage sur le site de l�ancien camp avec des lyc�ens, pour transmettre ce que fut leur r�alit� pendant des mois ; pour qu�� leur tour, les �l�ves deviennent les t�moins des t�moins, quand ceux-ci s��teindront.
