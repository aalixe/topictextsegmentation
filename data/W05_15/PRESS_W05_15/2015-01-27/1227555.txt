TITRE: Cuba: Fidel Castro sort du silence et prend acte du rapprochement avec les Etats-Unis | La-Croix.com - Monde
DATE: 27-01-2015
URL: http://www.la-croix.com/Actualite/Monde/Cuba-Fidel-Castro-sort-du-silence-et-prend-acte-du-rapprochement-avec-les-Etats-Unis-2015-01-27-1273206
PRINCIPAL: 1227553
TEXT:
Pourquoi Cuba a publi� des photos de Fidel Castro
L'ex-pr�sident cubain Fidel Castro a rompu lundi un silence de plusieurs mois en confiant dans une lettre qu'il ne faisait pas confiance aux Etats-Unis, mais qu'il ne rejetait pas pour autant le rapprochement r�cemment engag� avec Washington.
"Je n'ai pas confiance dans la politique des Etats-Unis, et je n'ai �chang� aucun mot avec eux, mais cela ne signifie � aucun moment un rejet d'une solution pacifique aux conflits", a d�clar� l'ex-chef d'Etat de 88 ans dans un courrier lu sur l'antenne de la t�l�vision d'Etat.
Ces affirmations viennent lever le myst�re sur sa vision de la normalisation avec l'ennemi am�ricain, que Fidel Castro n'a cess� de fustiger pendant des d�cennies. Ce courrier est �galement d�voil� au moment o� l'Etat de sant� du "Comandante", qui n'est pas apparu en public depuis plus d'un an, fait l'objet de rumeurs r�currentes.
Son silence remarqu� au moment de l'annonce du d�gel avec les �tats-Unis le 17 d�cembre, puis lors du retour au pays d'agents cubains lib�r�s par Washington, avait nourri ces sp�culations, principalement relay�es par des journaux et sites internet de Cubains expatri�s.
Dans cette lettre adress�e � une f�d�ration estudiantine, le p�re de la r�volution cubaine - qui a c�d� le pouvoir � son fr�re Raul � partir de 2006 pour raisons de sant� - n'a pas comment� ces bruits mais a tenu � manifester son appui � la politique de son successeur vis � vis de Washington.
"Le pr�sident de Cuba a pris les mesures pertinentes au regard de ses pr�rogatives (...) Nous d�fendrons toujours la coop�ration et l'amiti� entre tous les peuples du monde, y compris nos adversaires politiques", a d�clar� Fidel Castro dans ce long message dat� de lundi.
Lue sur l'antenne de la t�l�vision nationale par Randy Perdomo, pr�sident de la F�d�ration estudiantine universitaire, le courrier �voque divers th�mes, allant des in�galit�s dans la Gr�ce antique aux campagnes militaires cubaines en Afrique dans les ann�es 1970 et 1980, avant de conclure sur les relations avec le vieil ennemi am�ricain.
Le long de son r�cit, Fidel Castro revient aussi sur ses ann�es d'�tudiant et de r�volutionnaire jusqu'� son arriv�e au pouvoir en 1959. "J'ai lutt� et je continuerai � lutter (pour la dignit� humaine) jusqu'� mon dernier souffle", a-t-il conclu.
- Plus d'un an d'absence -
La publication de la missive survient quatre jours apr�s des premi�res discussions officielles de haut niveau entre les deux pays depuis plusieurs d�cennies. Ces pourparlers ont ouvert la voie au r�tablissement des relations diplomatiques rompues en 1961 entre les deux pays.
La derni�re sortie en public du Lider Maximo remonte � plus d'un an. Le 8 janvier 2014, il s'�tait rendu � l'inauguration de la galerie d'un ami de longue date. Cette br�ve apparition avait donn� lieu aux derni�res images film�es du "Comandante". Il �tait alors apparu vo�t� et semblait rencontrer des difficult�s pour se d�placer, s'aidant d'une canne et du bras de son m�decin personnel.
Depuis, les images du p�re de la r�volution cubaine se sont faites tr�s rares. Un comble pour cet "animal politique" r�put� pour ses discours fleuves, qui adorait fustiger "l'ennemi imp�rialiste am�ricain" et c�toyer les m�dias lorsqu'il �tait au pouvoir.
En juillet, il avait rencontr� les pr�sidents chinois Xi Jinping et russe Vladimir Poutine � son domicile, mais hors cam�ras. Seuls quelques clich�s avaient immortalis� ces t�te-�-t�te. En ao�t, Fidel Castro avait aussi re�u un jeune admirateur de 8 ans, mais l� encore, seules des photos avaient t�moign� de leurs entretiens.
Depuis, aucune image du "camarade Fidel" n'a �t� diffus�e. Et ses fameuses "r�flexions", auparavant publi�es avec r�gularit� dans la presse d��tat cubaine, se font de plus en plus rares. Ses derniers billets datent d'octobre.
Le 12 janvier dernier toutefois, il avait rappel� le monde � son existence en adressant une lettre � son vieil ami, le footballeur argentin Diego Maradona, alors en tournage sur l'�le communiste.
Mais le contenu de ce courrier dactylographi� n'avait pas �t� d�voil� par les m�dias d'Etat.
AFP
