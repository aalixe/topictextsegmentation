TITRE: Le groupe EI chass� de Koban�, les Kurdes de Syrie en liesse - La Voix du Nord
DATE: 27-01-2015
URL: http://www.lavoixdunord.fr/france-monde/le-groupe-ei-chasse-de-kobane-les-kurdes-de-syrie-en-liesse-ia0b0n2625203
PRINCIPAL: 1227723
TEXT:
- A +
Une atmosph�re de liesse r�gnait lundi dans les r�gions kurdes syriennes apr�s l'�viction du groupe jihadiste Etat islamique (EI) de la ville de Koban�, sa d�faite la plus cuisante en Syrie.
Cet �chec intervient le jour m�me o� un responsable militaire en Irak annon�ait que la province de Diyala, dans l'est du pays, �tait aussi lib�r�e du groupe extr�miste.
"Koban� lib�r�, f�licitations � l'Humanit�, au Kurdistan et au peuple de Koban�", a tweet� dans l'apr�s-midi Polat Can, un porte-parole des YPG (Unit�s de protection du peuple kurde), la milice qui d�fend la ville.
Plus t�t, l'Observatoire syrien des droits de l'Homme (OSDH) avait affirm� que les Kurdes contr�laient "totalement" Koban�, cette petite ville frontali�re de la Turquie devenue le symbole de la r�sistance � l'organisation EI depuis que les jihadistes y ont lanc� un vaste assaut le 16 septembre.
Dans les r�gions � majorit� kurde en Syrie, des foules sont descendues dans les rues pour c�l�brer cette victoire, certains dansant, d'autres tirant en l'air en signe de joie, rapporte l'OSDH.
A Paris, quelque 300 personnes se sont rassembl�es place de la R�publique, d�ployant un immense drapeau aux couleurs kurdes sur fond de danse traditionnelle et feux d'artifice.
- Jihadistes en fuite -
Les YPG "ont chass� tous les combattants de l'EI", a pr�cis� l'OSDH qui dispose d'un large r�seau en Syrie. "Les jihadistes se sont repli�s dans les environs de Koban�", a pr�cis� � l'AFP son directeur Rami Abdel Rahmane.
Le d�partement d'Etat am�ricain est rest� prudent une bonne partie de la journ�e, estimant que "les forces anti-EI contr�laient approximativement 70% du territoire � Koban� et pr�s de Koban�".
Mais un peu plus tard, le commandement militaire am�ricain au Moyen-Orient (Centcom) a estim� que les forces kurdes avaient repris "� peu pr�s 90% de la ville de Koban�".
"La guerre contre le groupe Etat islamique est loin d'�tre termin�e, mais son �chec � Koban� prive l'EI de l'un de ses objectifs strat�giques", s'est f�licit� Centcom.
L'�pilogue de la bataille � Koban� (A�n al-Arab en arabe) fait suite � plus de quatre mois de violents combats men�s par les forces kurdes avec le soutien pr�pond�rant des frappes quotidiennes de la coalition internationale.
Mustefa Ebdi, militant kurde de Koban�, a affirm� � l'AFP que "les combats ont cess�" � Koban� et que la bataille visait d�sormais � "lib�rer les environs de la ville", o� l'EI contr�le encore plusieurs dizaines de villages.
A l'extr�mit� est de la ville, les forces kurdes avan�aient "prudemment (...) par peur des mines et des voitures pi�g�es", selon le militant.
Les combats ont fait plus de 1.800 morts, dont plus de 1.000 dans les rangs jihadistes depuis la mi-septembre, selon un nouveau bilan de l'OSDH.
Le revers � Koban� porte un coup d'arr�t � l'expansion territoriale que le groupe EI m�ne en Syrie depuis son apparition dans le conflit en 2013, estiment des experts.
"C'est un coup dur pour l'EI et ses projets" d'expansion, a soulign� Mutlu Civiroglu, sp�cialiste de la question kurde bas� � Washington. "Malgr� toutes leurs armes sophistiqu�es et leurs combattants, ils n'ont pas pu prendre la ville".
Les forces kurdes, au d�part sous-�quip�es, ont r�ussi � prendre l'avantage gr�ce � l'appui crucial de la coalition internationale dirig�e par les Etats-Unis, qui a fait de Koban� une priorit� depuis le d�but des frappes a�riennes en Syrie le 23 septembre.
Dix-sept frappes y ont �t� encore men�es entre dimanche soir et lundi matin sur des positions jihadistes, selon le Commandement de la coalition.
- L'EI chass� de Diyala -
En Irak, les forces arm�es contr�lent d�sormais totalement toutes les villes, districts et cantons de la province de Diyala, dans l'est, a indiqu� le g�n�ral Abdelamir al-Za�di.
Gr�ce � sa campagne de frappes men�e depuis ao�t, la coalition estime avoir stopp� l'avanc�e du groupe EI dans ce pays, mais les jihadistes conservent pour l'instant l'essentiel de leurs positions, notamment Mossoul, la deuxi�me ville du pays.
Par ailleurs, le pr�sident syrien Bachar al-Assad a d�nonc� dans un entretien � une revue am�ricaine le plan des Etats-Unis d'entra�ner ses ennemis rebelles pour combattre l'EI, estimant qu'il s'agissait d'une chim�re.
Pour Assad, ces rebelles sont une force "ill�gale" et seront trait�s par l'arm�e comme les autres insurg�s, qualifi�s de "terroristes" depuis le d�but de la r�volte en 2011 contre son r�gime.
Washington, qui soutient l'opposition syrienne depuis le d�but de la r�volte il y a quatre ans, entend former au Qatar, en Arabie saoudite et en Turquie plus de 5.000 rebelles tri�s sur le volet afin de combattre le groupe EI.
0partage
