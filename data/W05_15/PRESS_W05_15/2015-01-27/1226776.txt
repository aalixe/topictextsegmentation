TITRE: Football: La RD Congo et la Tunisie de Chikhaoui en quarts -  News Sports: Football - lematin.ch
DATE: 27-01-2015
URL: http://www.lematin.ch/sports/football/rd-congo-tunisie-chikhaoui-quarts/story/20366977
PRINCIPAL: 1226775
TEXT:
La RD Congo et la Tunisie de Chikhaoui en quarts
Football
�
La Tunisie de Chikhaoui (FC Zurich) et la RD Congo, qui ne sont pas parvenues � se d�partager (1-1), ont obtenu leurs billets pour les quarts de finale de la CAN 2015.
Mis � jour le 26.01.2015
Yassine Chikhaoui (� dr.) � la lutte avec le Congolais Cedric Mongongu.
Image: AFP
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Les Aigles de Carthage, qui ont termin� en t�te du groupe B, affronteront au prochain tour la Guin�e �quatoriale, le pays organisateur, alors que la RDC sera oppos�e au Congo de Claude Le Roy, samedi, dans un duel fratricide.
La pr�sence de la Tunisie dans le top 8 n'est pas une surprise. Apr�s avoir boucl� les qualifications invaincue, elle faisait partie des �quipes � surveiller dans cette Coupe d'Afrique. Son parcours aura �t� assez mitig� avec 2 nuls et une seule victoire mais l'essentiel a �t� assur�.
Les troupes de Georges Leekens ont de nouveau altern� le bon et le moins bon face � la la RD Congo, ouvrant le score gr�ce � Akaichi (31e) avant de se faire rejoindre sur une r�alisation de Bokila (71e). Un but capital pour la RDC.
Le sacre est encore loin pour la Tunisie, dont l'unique troph�e date de 2004 et qui sort d'une longue p�riode d'instabilit� (10 s�lectionneurs depuis 2008). Mais elle peut profiter de son tirage au sort favorable pour s'en approcher.
La d�ception zambienne
Pour la RDC, le dernier quart de finale de CAN datait de 2006 en Egypte sous les ordres de Claude Le Roy. C'est donc un v�ritable retour au premier plan qu'ont op�r� les L�opards m�me s'ils n'ont pas r�ussi � remporter la moindre rencontre (3 nuls).
La grosse d�ception de la poule est venue de la Zambie, tenue en �chec par le Cap Vert (0-0) sous le d�luge d'Ebebiyin. Le titre de champion d'Afrique en 2012 n'est plus qu'un lointain souvenir et les Chipolopolos n'en finissent pas de r�trograder dans la hi�rarchie continentale. Ils viennent ainsi d'encha�ner une 2e �limination pr�coce � la CAN apr�s celle de 2013 en Afrique du Sud. (SI/Le Matin)
Cr��: 26.01.2015, 22h15
