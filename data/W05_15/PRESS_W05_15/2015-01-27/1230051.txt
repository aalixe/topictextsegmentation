TITRE: France: le ch�mage en hausse de 0,2% en d�cembre - Economie - RFI
DATE: 27-01-2015
URL: http://www.rfi.fr/economie/20150127-france-le-chomage-hausse-decembre/?ns_campaign%3Dgoogle_choix_redactions%26ns_fee%3D0%26ns_linkname%3Deconomie.20150127-france-le-chomage-hausse-decembre%26ns_mchannel%3Deditors_picks%26ns_source%3Dgoogle_actualite
PRINCIPAL: 1230050
TEXT:
Modifi� le 27-01-2015 � 21:54
France: le ch�mage en hausse de 0,2% en d�cembre
Sur l'ann�e, les principales victimes du ch�mage restent les plus de 50 ans.
REUTERS/Eric Gaillard
Les chiffres de P�le emploi sont toujours dans le rouge en d�cembre 2014. 8�100  demandeurs d�emploi en plus en cat�gorie A. Ces chiffres cl�turent une ann�e de records sur le front du ch�mage.
Le ch�mage est en hausse de plus 0,2 % en d�cembre pour les demandeurs d'emploi en cat�gorie A. C'est-�-dire ceux qui n'ont pas travaill� du tout le mois dernier. Au total, pour les cat�gories A, B et C, qui prennent en compte les personnes qui ont effectu� des petits boulots, la France, Dom-Tom compris, totalise 5�521�200 ch�meurs.
Cette hausse du ch�mage, due � une croissance atone, frappe les jeunes de moins de 25 ans. Plus 0,5 % en d�cembre, malgr� le recours aux contrats aid�s. Le tableau est sombre pour les seniors, les plus de 50�ans�: 1% de ch�meurs en plus le mois dernier. Et le nombre de demandeurs d�emploi de longue dur�e augmente de 1,3 %.
Cette hausse g�n�rale conclut une ann�e noire . Le ch�mage n�aura baiss� qu�une seule fois en 2014. L�ex�cutif se dit toutefois optimiste pour 2015, gr�ce au Pacte de responsabilit� cens� relancer la croissance. Mais pour g�n�rer des emplois, les �conomistes s'accordent � dire qu'il faut 1,5 % de croissance du PIB. Or la France n�en pr�voit que 1�% cette ann�e.
�
