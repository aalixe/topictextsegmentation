TITRE: Barbie enfile sa cape de Wonderwoman - lesoir.be
DATE: 27-01-2015
URL: http://www.lesoir.be/769619/article/victoire/air-du-temps/2015-01-27/barbie-enfile-sa-cape-wonderwoman
PRINCIPAL: 1229173
TEXT:
Barbie enfile sa cape de Wonderwoman
Julien Bosseler
Mis en ligne
mardi 27 janvier 2015, 16 h 48
La poup�e d�barque en version ��superh�ro�ne�� pour amuser les petites filles. Et pour sauver Mattel de la d�b�cle financi�re.
La sage bimbo blonde se rebiffe�: manifestement lass�e de passer pour une potiche, dont les exploits se r�sumaient jusqu�ici � griller sa carte de cr�dit en shopping, spas et sorties entre girls, elle enfile la tenue d�une superh�ro�ne super-gentille, avec masque rose, m�che rebelle assortie et la cape �toil�e de rigueur.
Son plus grand pouvoir�? L�amiti�! Pr�sent�e ce mardi 27 janvier au salon du Jouet de Nuremberg, cette Barbie in Princess Power (Barbie en princesse puissante), s�accompagne de la sortie d�une histoire anim�e dans laquelle la blonde au sourire kryptonite finit par devenir la meilleure amie de sa rivale revenue � de bons sentiments. Mattel la vend comme une possibilit� pour les petites filles de jouer aux superh�ros, comme les gar�ons, et de balayer au passage les st�r�otypes qui font rage dans l�univers super genr�s des jouets. Noble intention.
Mais le g�ant californien de la poup�e esp�re surtout que la nouvelle miss contribuera, avec ses superpouvoirs, � stopper l�h�morragie financi�re du groupe. Les trois derniers mois de l�an dernier, ses b�n�fices nets ont recul� de 59�%, cons�quence in�luctable du d�clin des jouets classiques en raison de la concurrence super-muscl�e des divertissements num�riques et des produits d�riv�s de l�ogre Disney. Un drame qui a d�ailleurs pouss� Bryan Stockton, PDG de Mattel, � d�missionner pas plus tard que ce lundi.
