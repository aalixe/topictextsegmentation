TITRE: 8emes de finale : Onesta, " on a v�rifi� ce qu'il fallait faire "
DATE: 27-01-2015
URL: http://www.boursorama.com/actualites/8emes-de-finale--onesta--on-a-verifie-ce-qu-il-fallait-faire-acfa75e737331c0dc644646be967459e
PRINCIPAL: 1228864
TEXT:
Tweet 0
8emes de finale : Onesta, " on a v�rifi� ce qu'il fallait faire "
Tr�s exigeant avec ses joueurs ces derniers jours, Claude Onesta s'est f�licit� de la victoire des Experts, lundi soir face � l'Argentine (33-20), au terme d'un match o� ses hommes ont su �lever leur niveau de jeu. Mais le s�lectionneur tricolore reste prudent quant � la suite de la comp�tition.
Claude Onesta, s�lectionneur de l��quipe de France
� Franchement je n�ai rien � leur reprocher ce soir, je leur ai d�j� beaucoup reproch� avant. C�est s�rement le parfum des matchs � �limination directe qui ram�ne tout le monde � l�essentiel. On a suffisamment pataug� dans la comp�tition pour comprendre que c�est en faisant des choses simples, en s�appliquant, en se concentrant et en �tant exigeant qu�on fait de belles choses. On a vu ce qu�il ne fallait pas faire, on a v�rifi� ce qu�il fallait faire. �a donne une meilleure vision pour la suite. C�est bien pour se lancer dans le sprint final, mais on ne va pas se reposer pour autant. On a vu dans les matchs pr�c�dents qu�il fallait faire attention. �
Copyright � 2015 Sport365.fr
