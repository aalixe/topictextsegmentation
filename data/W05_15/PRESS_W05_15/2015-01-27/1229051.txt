TITRE: Les 4 Fantastiques d�voilent une premi�re bande-annonce t�n�breuse - Actualit� Film - EcranLarge.com
DATE: 27-01-2015
URL: http://www.ecranlarge.com/films/news/935302-4-fantastiques-la-bande-annonce-du-reboot-est-enfin-la
PRINCIPAL: 1229049
TEXT:
23
Commentaires
Le reboot des� 4 Fantastiques r�alis� par� Josh Trank aura beaucoup inqui�t� la communaut� des fans. Mais cette premi�re bande-annonce devrait largement les rassurer.
Bien s�r, on pourra s�agacer de voir un nouveau blockbuster h�ro�que nous la jouer ��sombre��, avec sa photographie un peu dark, ses h�ros tortur�s et son atmosph�re de fin du monde.
Mais bon sang, cela faisait longtemps qu�on n�avait pas vu un produit Marvel se d�tacher aussi franchement de son �uvre originale pour nous en proposer une r�interpr�tation. On se r�jouit de voir la place qui semble �tre celle de Miles Teller , aux antipodes du super-h�ros classique, tout comme Kate Mara , loin des clich�s de la combattante poumonn�e.
Ajoutons � cela un univers qui s�annonce plut�t original et un soin �vident apport� � la direction artistique et on se dira que Josh Trank est peut-�tre sur le point de r�it�rer le miracle Chronicle.
Esp�rons simplement que les importants reshoots d�cid�s par le studio ne soient pas synonyme de transformation de derni�re minute, ou d�irr�cup�rables d�fauts de conception.
Enfin, ce trailer des 4 Fantastiques a la bonne id�e de ne nous d�voiler que tr�s peu de choses. Ainsi c�est � peine si l�on aper�oit La Torche, tandis que La Chose fait une extr�mement br�ve apparition et que Fatalis est rel�gu� au hors champ.
Bref, le film en a encore beaucoup sous le pied.
commentaires
Intriguant...
Touco 27/01/2015
L'avantage de ce dernier, c'est qu'il passe apr�s de navets de premier ordre. Et je ne parle pas de la version hilarante de 1994 qui a titre, p�dagogique  (ou anthropologique) vaut son pesant de cacahuete (je vous laisse faire les recherches sur youtube).
Il pourra difficilement faire pire... Bref, ca ne peut �tre qu'une bonne surprise (sinon c'est que la franchise est d�finitivement maudite)
louig 27/01/2015
" par le studio qui vous a pr�sent� XMEN DAY OF FUTURE PAST"
ah ouais carr�ment ^^
en tout cas �'a l'air int�ressant ...ce qui est difficilement concevable avec les FF
diez 27/01/2015
Il semble y avoir une vraie recherche sur la plan visuel, je suis �tonn� et maintenant curieux.
Le c�t� gamins peut �tre rebutant, mais apr�s la BA, le film commence � m�int�resser.
Will Wayne 27/01/2015
La Fox s'est fait sp�cialiste dans la destruction de ses licences Marvel... C'est pas la bouse qu'est le dernier X-Men qui va m encourager � l optimisme concernant ce nouveau FF.
REA 27/01/2015
R�ticent � un reboot. Vaguement int�ress� lors de l'annonce de Michael B. Jordan. Intrigu� avec cette bande-annonce.
Entre le prochain Terminator et les FF, le dernier a l'air plus solide et cr�dible que le 1er.
WAIT AND SEE
JET JET 27/01/2015
Pour ma part, ils pourront compter sur moi pour d�couvrir la nouvelle �uvre de Josh Trank en salle...
Je fais pourtant parti des gens us�s par les films de super h�ros, mais l�, il y a une dose de myst�re, une autre d'�motion, un visuel et des acteurs qui me font compl�tement adh�rer au projet.
Quand en plus le r�alisateur nous fait part de sa note d'intention et cite "La Mouche" & "Scanners" pour un reboot des 4F, je suis totalement acquis � la cause.
Et dire qu'Ecran Large crachait sur le visuel des costumes le matin m�me pour se rendre compte quelques heures plus tard, image � l'appui, que finalement, �a � l'air pas mal.
OMG !
Mmarvin 28/01/2015
A ce qu'ils semble, ils ne se basent pas sur la s�rie classique mais la d�clinaison " Ultimate ", bien plus sombre et cr�dible.
Autant dans la s�rie classique cela d�gouline de bons sentiments et d'h�ro�sme pur, autant la version Ultimate est plus Bad Ass, avec des h�ros bien plus occup�s � tenter de r�parer les cons�quences de leurs choix qu'� sauver du chaton coinc� dans un arbre.
Et quand on sait que les FF Ultimate finissent mal, tous les espoirs sont permis !
Kilian 28/01/2015
Pour ceux qui voudrait voir la VF :
http://www.videoty.com/les-4-fantastiques-la-premiere-bande-annonce-du-reboot
Ardeau 28/01/2015
@Mmarvin
S'ils se basent sur la version Ultimate, esp�rons que �a ne finisse pas comme le d�lire qu'�tait Ultimate Future Foundation : "Miles Morhames", l'arrogance de Doom prenant des proportions comiques, la "castration" de Reed, le "b�b� magique" de Susan. �a m'a bien fait rire, mais je devine que les vrais fans �taient outr�s.
