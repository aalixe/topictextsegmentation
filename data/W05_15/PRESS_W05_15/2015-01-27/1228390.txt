TITRE: Angela Merkel souhaite �force et succ�s� � Al�xis Ts�pras - Lib�ration
DATE: 27-01-2015
URL: http://www.liberation.fr/monde/2015/01/27/angela-merkel-souhaite-force-et-succes-a-alexis-tsipras_1189697
PRINCIPAL: 1228387
TEXT:
La chanceli�re Angela Merkel au Bundestag � Berlin, le 15 janvier 2015 (Photo Odd Andersen. AFP)
Al�xis Ts�pras doit annoncer ce mardi la composition de son gouvernement.
Trente-six heures apr�s la victoire de Syriza aux �lections l�gilsatives en Gr�ce, la chanceli�re allemande, Angela Merkel, est sortie de son silence ce mardi en souhaitant, par le biais d�un message �crit, �force et succ�s� au nouveau premier ministre, Al�xis�Ts�pras.
Un �succ�s� qui d�pendra notamment d�Angela Merkel elle-m�me, la chanceli�re �tant l�un des interlocuteurs avec qui Al�xis�Ts�pras devra n�gocier pour r�gler le probl�me de la dette grecque. La suite du message transmis par la chancellerie informe d�ailleurs l�imp�trant qu�il prend ses fonctions �dans une p�riode difficile, dans laquelle [il va] �tre confront� � une grande responsabilit�. Une fa�on de le rappeler aux engagements de ses pr�d�cesseurs, que Berlin veut voir appliqu�s.
A lire aussi :� Gr�ce�: des doutes autour d�une dette
Le gouvernement annonc� aujourd�hui
Al�xis Ts�pras doit annoncer ce mardi la composition de son gouvernement. L��conomiste�Yannis Varoufakis, pourfendeur de �la dette odieuse�, a d'ores et d�j� �t� confirm� au minist�re strat�gique des Finances. Le pr�sident de l�Eurogroupe, Jeroen Dijsselbloem, s�est d�ailleurs entretenu avec lui par t�l�phone lundi.
Le nouveau gouvernement d�Alexis Tsipras devra �galement trouver un �quilibre avec son encombrant alli�, le parti souverainiste des Grecs Ind�pendants (Anel), dont le pr�sident Panos Kammenos est pressenti pour le minist�re de la D�fense, selon la presse grecque.
