TITRE: La RATP promet la 3G et la 4G sur les lignes�1 et A courant�2015
DATE: 27-01-2015
URL: http://www.begeek.fr/la-ratp-promet-la-3g-et-la-4g-sur-les-lignes-1-et-courant-2015-159221
PRINCIPAL: 1230065
TEXT:
>
La RATP promet la 3G et la 4G sur les lignes�1 et A courant�2015
Le secr�taire g�n�ral de la RATP a annonc� que la 3�G et la 4�G devraient �tre disponible sur les lignes�1 et A courant�2015.
L�annonce faite aujourd�hui par la RATP risque de sonner comme une lib�ration pour les utilisateurs du m�tro et du RER parisien ! La r�gie de transport a en effet annonc� que la couverture�3G et 4G allait conna�tre un bon coup d�acc�l�rateur.
Si l�ensemble du r�seau devait �tre couvert pour 2017, la ligne�1 du m�tro et la ligne A du RER devraient offrir le pr�cieux haut d�bit mobile dans les transports au cours de l�ann�e�2015.
Les lignes�1 et A enfin dot�es de la 3G et 4G
Si Paris est pour beaucoup la plus belle ville du monde, la capitale fran�aise n�en demeure pas moins tr�s en retard en ce qui concerne l�acc�s � la data haut d�bit dans le m�tro. Avec de la chance, on arrive � capter une bonne vieille 2G qui tombe � genou d�s que l�on veut charger son fil Facebook .
Suite � la convention sign�e avec les op�rateurs mobiles fran�ais, la RATP n�avait d�autre choix que de faire avancer les infrastructures assez rapidement. Lors des v�ux � la presse d�Emmanuel Pitron, secr�taire g�n�ral de la RATP a promis qu���En 2015, les lignes�1 (du m�tro) et A (du RER) seront les premi�res lignes �quip�es en totalit� en 3�G et 4�G��. Si on est loin de la couverture de l�ensemble du r�seau, on ne peut que saluer la nouvelle.
En 2015, les lignes 1 & A seront les 1�res lignes �quip�es en totalit� en #3G et #4G . Emmanuel Pitron secr�taire g�n�ral du Groupe #RATP 2/2
� Groupe RATP (@GroupeRATP) 27 Janvier 2015
Couverture int�grale en 2017 pour la RATP
Apr�s l�appel d�offres de la RATP, c�est la soci�t� Sogetrel qui a �t� choisie pour assurer ce chantier tr�s important pour la r�gie de transport mais �galement pour la ville de Paris. Ce ne sont pas moins de 300 stations de m�tro et 65 gares RER qu�il faut �quiper et pour cela, les cr�neaux horaires permettant de r�aliser les travaux sont tr�s restreints car il faut attendre que les rames�ne circulent plus.
Des contraintes qui, conjugu�es � l� entr�e tardive de Free dans la convention entre la RATP et les op�rateurs, ont d�ores et d�j� repouss� la fin des travaux permettant une couverture totale pr�vue pour 2016 � la fin de l�ann�e�2017.
Vous avez aim� cet article ?
Topic Associ�s
