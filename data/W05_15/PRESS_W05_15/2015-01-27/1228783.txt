TITRE: Marseille: Un homme bless� par balles � la Castellane - 20minutes.fr
DATE: 27-01-2015
URL: http://www.20minutes.fr/marseille/1526791-20150127-marseille-homme-blesse-balles-castellane
PRINCIPAL: 0
TEXT:
faits-divers
Un homme de 26 ans a �t� bless� par balles mardi � la Castellane (16e), a-t-on appris de source proche du dossier. Il a �t� touch� de plusieurs coups de feu. Il a �t� pris en charge par les marins-pompiers et ses jours ne sont pas en danger. Pour le moment, les motifs de l'agression et le type d'arme utilis� ne sont pas connus.
Dans la nuit du 14 au 15 janvier, un homme de 25 ans a �t� abattu dans la cit� d'une balle dans la t�te. Des �tuis de calibre 9 mm ont �t� trouv�s sur le sol. L'homme �tait �un peu� connu des services de police.
15 r�glements de compte en 2014
C'�tait le premier r�glement de comptes de l'ann�e � Marseille apr�s six mois d'accalmie. Quinze r�glements de compte ont eu lieu � Marseille intra-muros en 2014, contre 14 en 2013.
Lors de la rentr�e solennelle du tribunal de grande instance de Marseille, le procureur de la R�publique s'�tait f�licit� du nombre d'armes saisies en 2014 par rapport � 2013. �Le nombre d'armes saisies augmente de 37%, soit 609 armes saisies, 324 armes longues, dont 22 kalachnikovs et 285 armes de poing�, avait-il indiqu�.
