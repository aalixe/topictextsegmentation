TITRE: Cinq arrestations dans une op�ration anti-djihadiste � Lunel
DATE: 27-01-2015
URL: http://www.europe1.fr/faits-divers/quatre-arrestations-dans-une-operation-antiterroriste-a-lunel-2355123
PRINCIPAL: 1227532
TEXT:
Cinq arrestations dans une op�ration anti-djihadiste � Lunel
Europe 1Alain Acco et Walid Berrissoul avec Chlo� Pilorget-Rezzouk
Publi� � 08h51, le 27         janvier  2015
, Modifi� � 07h25, le 28         janvier  2015
Dossiers :
Des membres du GIPN (photo d'illsutration)  � SICOP
Par Alain Acco et Walid Berrissoul avec Chlo� Pilorget-Rezzouk
0
0
2
Une op�ration antiterroriste de grande envergure a �t� lanc�e mardi matin, � Lunel et dans ses environs, dans l'H�rault et le Gard. Cinq hommes ont �t� arr�t�s.
L'info. Une op�ration antiterroriste de grande envergure a �t� lanc�e tr�s t�t mardi matin dans les milieux djihadistes, � Lunel et dans ses environs, dans l'H�rault et le Gard. Cinq personnes ont �t� interpell�es et plusieurs perquisitions effectu�es.
>> LIRE AUSSI - 4 � 10 ans de prison pour trois Fran�ais candidats au djihad
"Une fili�re particuli�rement dangereuse". Ce coup de filet a permis d'arr�ter cinq personnes, �g�es de 44, 35, 31 ans et de 26 ans pour deux d'entre elles. Ils ont �t� plac�s en garde � vue. Ces hommes "sont suspect�s de participation active � une fili�re dijhadiste dont les membres ont �t� recrut�s et endoctrin�s, et ont �galement recrut� et endoctrin� plusieurs jeunes Fran�ais originaires de Lunel", a pr�cis�, mardi matin, lors d'une conf�rence au minist�re de l'Int�rieur, Bernard Cazeneuve. Deux d'entre eux sont revenus de Syrie o� ils avaient effectu� un court s�jour. Les deux autres leur avaient fourni une aide logistique. Deux ont �t� arr�t�s a Lunel, deux autres � Aimargues (Gard) et Caussiniojouls.
C'est "une fili�re particuli�rement dangereuse et organis�e [...] qui aura �t� d�mantel�e ce matin", a ajout� le ministre de l'Int�rieur, avant de rappeler "la mobilisation enti�re et la d�termination totale des pouvoirs publics � lutter sans tr�ve � l'int�rieur comme � l'ext�rieur contre le terrorisme".
>> LIRE AUSSI - Lutter contre le djihad : ce qui est possible et ce qui ne l'est pas
D�manteler une fili�re djihadiste. Lanc�e depuis 6h30 par la direction g�n�rale de la s�curit� int�rieure (DGSI) et la sous-direction anti-terroriste (SDAT), et avec le soutien du Raid et du GIPN, cette op�ration a �t� men�e sur commission rogatoire d'un juge antiterroriste. Elle visait le d�mant�lement d'une fili�re de recrutement djihadiste pour la Syrie. Les rues du vieux centre-ville de Lunel ont �t� investies par les policiers. Le vieux centre a �t� compl�tement boucl�, ainsi que deux villages, Caussiniojouls, dans l'H�rault, et Aimargues, dans le Gard.
Une intervention pr�vue depuis longtemps. C'est une op�ration pr�par�e de longue date, comme l'a indiqu� le d�put� socialiste de Lunel, Patrick Vignal , au micro d'Europe 1 : "on avait dit qu'on ferait du m�nage et qu'on irait fort. On savait qu'il y avait un terreau de gens malsains et donc ce matin, c'est une grosse op�ration, �a faisait longtemps que c��tait pr�par�... On voulait essayer de trouver ces gens malsains qui envoient nos enfants � la mort et qui servent de chair � canon. On fait le maximum pour arr�ter les gens qui pourrissent notre ville."
>> LIRE AUSSI - 2014, la France d�couvre les chemins du djihad
Six jeunes Lunellois morts en Syrie. Car cette ville de 26.000 habitants, en effet, poss�de un passif tr�s lourd . Depuis un an, une vingtaine de jeunes auraient quitt� Lunel pour la Syrie, afin de rejoindre les rangs de l'Etat islamique. En l'espace de quelques mois pas moins de six Lunellois , de 18 � 30 ans, sont ainsi morts au front en Syrie ou en Irak. Tous ont moins de 25 ans, tous faisaient partie du m�me groupe d'amis. Certains fr�quentaient d'ailleurs tr�s assid�ment la mosqu�e locale , qui n'a pas �t� vis�e par les perquisitions, mais dont le pr�sident avait refus� de condamner le choix de ces jeunes candidats au djihad partant en Syrie.
En juin dernier, le pr�fet de l'H�rault avait indiqu� qu'une cinquantaine de personnes au total avaient quitt� la r�gion Languedoc-Roussillon au cours des derniers mois pour la Syrie et l'Irak.
Tous les articles faits-divers
