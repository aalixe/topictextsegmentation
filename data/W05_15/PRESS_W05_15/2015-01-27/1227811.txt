TITRE: Logement. Le nombre de mises en chantier au plus bas depuis 1997
DATE: 27-01-2015
URL: http://www.ouest-france.fr/logement-le-nombre-de-mises-en-chantier-au-plus-bas-depuis-1997-3145368
PRINCIPAL: 1227809
TEXT:
Logement. Le nombre de mises en chantier au plus bas depuis 1997
France -
Achetez votre journal num�rique
Les mises en chantier de logements neufs en France ont chut� de 10,3% sur l'ann�e 2014 pour s'afficher � 297 532, soit leur plus bas niveau depuis l'ann�e 1997.
D'apr�s les chiffres publi�s mardi par le minist�re du Logement, les mises en chantier de logements neufs, qui sont de 297 532 pour 2014, ont ainsi atteint leur plus bas niveau depuis l'ann�e 1997.
L'an dernier, le nombre des permis de construire accord�s pour des logements neufs a aussi d�gringol� de 12% par rapport � 2013, pour s'�tablir � 381 075, a pr�cis� le minist�re.
Tags :
