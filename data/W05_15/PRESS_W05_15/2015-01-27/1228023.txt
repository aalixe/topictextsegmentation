TITRE: Logement : les mises en chantier tombent au plus bas depuis 1997 !
DATE: 27-01-2015
URL: http://www.boursier.com/actualites/economie/logement-les-mises-en-chantier-au-plus-bas-depuis-1997-26887.html
PRINCIPAL: 1228021
TEXT:
Cr�dit photo ��Reuters
(Boursier.com) � C'est un plus bas depuis 1997 !... Un total de 297.532 logements ont �t� mis en chantier l'an pass� en France, selon les chiffres publi�s mardi par le minist�re de l'Ecologie et du D�veloppement durable.�Les permis de construite d�livr�s ont atteint 381.075, soit le plus faible niveau depuis 1999.
Baisse des permis de construire
En comparaison avec 2013, les mises en chantier accusent un recul de 10,3% et les permis de construire de 12,0%. Dans la construction neuve, qui repr�sente pr�s de 88% de l'offre de nouveaux logements, les mises en chantier baissent de 9,5% et les permis de construire de 11,1%.�Sur le seul quatri�me trimestre, les mises en chantiers ont recul� de 1,7% en donn�es corrig�es des variations saisonni�res par rapport au troisi�me trimestre et les permis de construire accord�s de 0,8%...
C.L. avec Reuters � �2015, Boursier.com
Nombre de caract�res autoris� : 500
