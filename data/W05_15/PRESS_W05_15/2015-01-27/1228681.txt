TITRE: Griezmann recal� deux fois par le Bar�a ! - Football - Sports.fr
DATE: 27-01-2015
URL: http://www.sports.fr/football/espagne/articles/griezmann-recale-deux-fois-par-barcelone-!-1174787/?coverhome
PRINCIPAL: 0
TEXT:
27 janvier 2015 � 11h21
Mis à jour le
27 janvier 2015 � 12h09
S’il fait aujourd’hui le bonheur de l’Atletico Madrid, après avoir explosé en Espagne sous le maillot de la Real Sociedad, Antoine Griezmann aurait pu rallier le FC Barcelone à deux reprises. En 2009 puis en 2011, il s’en fallu de peu pour que l’enfant de Mâcon ne devienne blaugrana.
Rétrospectivement, le Barça s’en mord peut-être les doigts. Après avoir fait les beaux jours de la Real Sociedad cinq saisons durant, Antoine Griezmann s’est engagé l’été dernier en faveur du champion d’Espagne en titre, l’Atletico Madrid. Un transfert heureux puisqu’après des débuts mitigés, et une charge virile à son encontre de Diego Simeone, son nouvel entraîneur, l’intéressé s’est parfaitement adapté au collectif madrilène. Ses stats à mi-saison en témoignent. En 31 matches toutes compétitions confondues, le natif de Mâcon a déjà frappé à 12 reprises. Prometteur !
Là où le FC Barcelone peut avoir des regrets au vu de ces chiffres, c’est parce que le fleuron catalan a eu deux fois l’occasion de recruter l’attaquant français par le passé. En 2009 d’abord, alors qu’il achevait sa formation du côté de Saint-Sébastien. Mais alors les pisteurs blaugranas l’avaient estimé trop petit et trop frêle – un comble quand on sait l’archétype du joueur du Barça à cette époque-là, incarné par les Messi, Xavi et autre Iniesta…
Deux ans plus tard, tandis qu’Antoine Griezmann prend toujours plus d’envergure avec la Real Sociedad, Pep Guardiola s’intéresse de près à son profil, appréciant sa vitesse, ses qualités de percussion et son adresse devant le but. Au final cependant, le coach catalan optera pour l’attaquant chilien de l’Udinese Alexis Sanchez . Tant pis pour Griezmann, qui aurait pu devenir champion d’Espagne sous les couleurs barcelonaises et même remporter la Coupe du Roi. Tant pis, surtout, pour le Barça !   
¿Sabías que el Barça dijo no a Griezmann? http://t.co/aQJzIT4PLq   (by @didacpeyret ) pic.twitter.com/GRBzsejGAS
