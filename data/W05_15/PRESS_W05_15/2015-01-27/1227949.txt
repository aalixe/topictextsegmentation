TITRE: VU DE RUSSIE. Poutine brillera par son absence � Auschwitz | Courrier international
DATE: 27-01-2015
URL: http://www.courrierinternational.com/revue-de-presse/2015/01/27/poutine-brillera-par-son-absence-a-auschwitz
PRINCIPAL: 0
TEXT:
Economie. L�accord entre l�Eurogroupe et la Gr�ce r�sum� en cinq points
Un visiteur dans le camp d'Auschwitz, le 25 janvier 2015 - AFP/Joel Saget
La comm�moration du 70e anniversaire de la lib�ration du camp de la mort nazi se d�roulera sur fond de conflit dans le Donbass, et dans une atmosph�re empoisonn�e par les propos du chef de la diplomatie polonaise jug�s r�visionnistes � Moscou.
Le 27 janvier est le jour de la comm�moration du 70e anniversaire de la lib�ration du camp d'Auschwitz-Birkenau, ultime entreprise d'extermination nazie o� furent tu�es 1,1 million de personnes, dont 960 000 Juifs, entre 1940 et 1945. Il est situ� en Pologne, � 60 kilom�tres de Cracovie. De nombreux chefs d'Etat seront pr�sents, parmi lesquels Fran�ois Hollande, le pr�sident de l'Allemagne Joachim Gauck, ainsi que le pr�sident de l'Ukraine Petro Porochenko.
Vladimir Poutine, pr�sent en 2005 � l'occasion du 60e anniversaire, brillera par son absence. Selon son porte-parole, "aucune invitation ne lui a �t� adress�e", et le pr�sident russe, ce jour-l�, f�tera l'�v�nement en Russie. Il se
rendra au Mus�e juif et centre de tol�rance, ouvert � Moscou en
novembre 2012, dans lequel une c�r�monie en m�moire des victimes de l'Holocauste
aura lieu sous la houlette du grand rabbin de Russie, Berl Lazar. C'est
en principe le chef de l'administration pr�sidentielle Sergue� Ivanov
qui repr�sentera la Russie en Pologne, nous pr�cise Gazeta.ru .
Subtilit�s diplomatiques
Comme le raconte en d�tail le quotidien moscovite anglophone Moscow Times , selon les organisateurs de la c�r�monie en Pologne, � savoir le Mus�e d'Etat Auschwitz-Birkenau et le Conseil international d'Auschwitz, aucune invitation officielle nominale n'a �t� adress�e aux chefs d'Etat. Les ambassades des pays donateurs sont cens�es pr�ciser elles-m�mes qui elles souhaitent envoyer pour repr�senter leur Etat.
Le ministre des Affaires �trang�res russe, Sergue� Lavrov, a confirm� avec d�dain, lors de sa conf�rence annuelle du 21 janvier, que l'ambassade de Russie � Varsovie avait en effet re�u une notification qui disait en substance : "Vous pouvez venir si vous voulez. Si vous ne le souhaitez pas, dites-nous qui est cens� se pr�senter. Vous n'�tes pas oblig�s de r�pondre � cette invitation." "Cependant, commente le titre, au Kremlin, une invitation formul�e de la sorte n'a aucune chance d'atteindre le sommet du pouvoir."
"L'ineptie" du ministre polonais
le ministre des Affaires �trang�res polonais, Grzegorz Schetyna, "a ajout�
l'insulte au pr�judice", poursuit The Moscow Times, en d�clarant dans une
interview � la radio polonaise que le camp d'Auschwitz avait �t� lib�r� par
les Ukrainiens, faisant allusion, comme s'�vertuent � le pr�ciser les officiels et la presse russes, scandalis�s, aux "unit�s du premier front ukrainien de l'Arm�e rouge".
Le minist�re des Affaires �trang�res russe a imm�diatement
rappel� que "des individus en provenance de toute l'ex-URSS avaient combattu dans
l'Arm�e rouge qui a lib�r� Auschwitz". Le pr�sident de la Douma [le Parlement], Sergue�
Narychkine, a pour sa part comment� : "Du
point de vue historique, c'est une falsification, une ineptie pure et simple.
Mais par ailleurs il s'agit, je pense, d'un d�sir de r�crire l'histoire, �
des fins politiques."
Pravda , rapporte que m�me en Pologne on est un peu g�n�. Un journaliste du
quotidien de Varsovie Dziennik a
ainsi sugg�r� au chef de la diplomatie polonaise de ne pas se d�placer sans
antis�ches, qu'il serait ravi de r�diger pour lui.
Une journaliste du quotidien Moskovski Komosomets rappelle que les unit�s de l'arm�e sovi�tique n'�taient pas constitu�es
selon le principe ethnique ou national, ce qui aurait �t� contraire �
l'esprit sovi�tique. Selon les recherches de Grigori Krivocheev, sur 100 soldats sovi�tiques parvenus aux portes du camp d'Auschwitz, 16 �taient ukrainiens. Si l'on veut pr�ciser, ajoute-t-elle, il faut signaler que l'officier qui a� ouvert ces portes �tait juif. Il s'agit du commandant Anatoli Shapiro.
Unit�s ukrainiennes dans la Wehrmacht
Cependant, la journaliste va plus loin. Elle d�busque, dans les propos du ministre polonais, un "lapsus freudien" et tente une interpr�tation : "En disant que le 'front ukrainien' �tait compos� d'Ukrainiens, le ministre avait en t�te une autre arm�e, que visiblement il conna�t mieux, � savoir la Wehrmacht. En effet, aux c�t�s des nazis combattaient des troupes form�es sur le principe des nationalit�s, regroupant les habitants d'un m�me pays occup�. Y compris la Pologne.
En haute Sil�sie, d'o� est originaire M. Schetyna, selon les calculs des historiens, un homme sur quatre s'est battu sous l'uniforme allemand. On comprend alors d'o� lui viennent ces notions d'"unit�s ukrainiennes". Car il y a bien eu des unit�s ukrainiennes pendant la Deuxi�me Guerre mondiale, mais elles se battaient du c�t� de Hitler�.
"J'�tais tr�s fier d'�tre en premi�re ligne des lib�rateurs, pas tant parce que j'�tais un Juif en train de lib�rer ce camp, mais parce nous, l'Arm�e rouge, l'avions lib�r�, avait d�clar� Shapiro au New York Daily News, peu avant sa mort en 2005, conclut le Moscow Times.
Source
