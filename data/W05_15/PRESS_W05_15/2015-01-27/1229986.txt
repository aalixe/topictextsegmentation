TITRE: Des chercheurs r�ussissent � �d�cuire� un �uf dur - 20minutes.fr
DATE: 27-01-2015
URL: http://www.20minutes.fr/sciences/1526283-20150126-chercheurs-reussissent-decuire-uf-dur
PRINCIPAL: 1229984
TEXT:
high-tech
L'exploit fait sourire, mais les applications sont bien concr�tes. Des chercheurs de l'Universit� d'Irvine, en Californie, ont r�ussi � �d�cuire� un �uf dur. Traduction:�ils ont invers� le processus de transformation des prot�ines pour que le blanc redevienne liquide en quelques minutes, ce pourrait permettre de faire baisser les co�ts de fabrication de certains m�dicaments.
Les sportifs le savent bien, du blanc d��uf, c'est plus ou moins des prot�ines liquides. En cuisant, la chaleur casse les liaisons les plus faibles de ces cha�nes d'acides amin�s (d�naturation), et la prot�ine se d�roule. Ensuite, de nouveaux liens plus solides se cr�ent (coagulation). L'eau qui entoure ces prot�ines est chass�e et pi�g�e dans une gaine:�l'oeuf est �dur�.
Proc�d� chimique et m�canique
Malheureusement, le processus ne peut pas s'inverser en agissant sur la temp�rature. L'�quipe du professeur Weiss a donc utilis� une approche combinant deux techniques, chimique et m�canique. Un d�riv� de l'ur�e �mange� les liaisons et liqu�fie le blanc, qui est ensuite plac� dans une centrifugeuse pour d�rouler les prot�ines dans leur �tat initial.
Le but n'est �videmment pas de �d�cuire� (� quand dans le Larousse?) des �ufs. Mais �tre capable de manipuler des prot�ines en quelques minutes pourrait permettre d'acc�l�rer le processus de fabrication de certains m�dicaments utilis�s pour traiter des cancers. Des applications industrielles li�es � la manipulation de bact�ries ou de levures, dans la fabrication du fromage, notamment, pourraient �galement en profiter. Bon app�tit!
