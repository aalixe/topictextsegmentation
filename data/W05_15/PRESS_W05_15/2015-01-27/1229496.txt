TITRE: France/Monde | 2014 année noire : il n'y a jamais eu autant de chômeurs en France
DATE: 27-01-2015
URL: http://www.leprogres.fr/france-monde/2015/01/27/2014-annee-noire-il-n-y-a-jamais-autant-de-chomeurs-en-france
PRINCIPAL: 1229494
TEXT:
� > �2014 année noire : il n'y a jamais eu autant de chômeurs en France
- Publié le 27/01/2015
ECONOMIE 2014 année noire : il n'y a jamais eu autant de chômeurs en France
France : le chômage a dépassé le pic 1997
Photo AFP/Philippe HUGUEN
Previous Next
Le nombre de demandeurs d�??emploi sans aucune activité a atteint en décembre 3,496 millions en métropole, soit 8 100 personnes de plus en un mois et 189 100 de plus sur l�??année 2014, selon les statistiques de Pôle emploi publiées mardi.
La hausse est de 0,2% sur un mois et de 5,7% sur un an. En tenant compte des chômeurs ayant une activité réduite, 5,21 millions de personnes étaient inscrites sur les listes en métropole, et 5,52 millions avec l�??Outre-mer.
En tenant compte des chômeurs ayant une activité réduite, 5,21 millions de personnes étaient inscrites fin décembre sur les listes de Pôle emploi en métropole, et 5,52 millions avec l�??Outre-mer.
En décembre, si la progression est contenue pour les demandeurs d�??emploi sans aucune activité (+8.100), elle est en revanche importante pour les personnes en activité réduite (+41.900).
Sur l�??année, les principales victimes du chômage restent les plus de 50 ans. En 2014, leur nombre a augmenté de 10,4% pour dépasser 820.000.
Le nombre de demandeurs d�??emploi de moins de 25 ans connaît, lui, une hausse modérée de 1,7%. Il a même légèrement baissé en décembre (-0,2%).
Le chômage de longue durée a continué sa course en 2014. Fin décembre, plus de 2,2 millions de personnes étaient inscrites à Pôle emploi depuis plus d�??un an, 9,7% de plus en un an. En moyenne, les demandeurs d�??emploi étaient inscrits depuis 539 jours.
Dans un communiqué, le ministère souligne que «le gouvernement a mobilisé la politique de l�??emploi tout au long de 2014, en particulier en faveur de ceux qui sont les plus exposés au risque d�??exclusion du marché du travail», citant «près de 97.000 emplois d�??avenir pour les jeunes souvent peu qualifiés», «près de 310.000 contrats aidés non marchands et 48.000 marchands pour des chômeurs de longue durée ou éloignés de l�??emploi».
Il estime que «le plein déploiement du Pacte de responsabilité et de solidarité et une amélioration de l�??environnement économique dynamiseront l�??emploi en 2015».
Le ministre François Rebsamen doit en outre présenter «en février les résultats des travaux sur la lutte contre le chômage de longue durée», rappelle le ministère dans ce communiqué.
Articles Associ�s
