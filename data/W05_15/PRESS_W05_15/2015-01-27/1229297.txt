TITRE: Twitter fait son nid entre vid�o et conversations de groupe
DATE: 27-01-2015
URL: http://www.itespresso.fr/twitter-nid-video-conversations-groupe-86896.html
PRINCIPAL: 0
TEXT:
8 32 1 7 Donnez votre avis
Twitter supportera bient�t les conversations de groupe, ainsi que la capture, l��dition et la publication de vid�os directement dans l�application mobile.
D�but janvier, dans le cadre du CES 2015, Twitter �tait revenu sur ses ambitions autour du format vid�o. Le r�seau social avait annonc� la mise en place imminente d�un service ��natif�� qui permettrait aux utilisateurs de publier directement leurs contenus, sans passer par des plates-formes tierces comme YouTube.
Une premi�re �tape en ce sens sera bient�t franchie avec le lancement � dans les tout prochains jours � d�une fonctionnalit� �� Mobile Video Camera �� au sein de l�application mobile pour Android et iOS. Les twittos pourront capturer des s�quences durant jusqu�� 30 secondes, puis les �diter et les partager sans avoir � quitter le site de micro-blogging.
L�application iPhone permettra aussi de charger des vid�os depuis la galerie (Android sera concern� dans un second temps). On notera que le premier tweet exploitant cette fonction ��Mobile Video Camera�� a �t� post� par l�acteur Neil Patrick Harris, connu notamment pour son r�le dans la s�rie How I Met Your Mother.
Twitter aura davantage de contr�le sur ces vid�os qu�il h�bergera sur ses propres serveurs� et qui pourront par exemple �tre lanc�es automatiquement. Il est ainsi question d�en afficher, � terme, un aper�u de 6 secondes � l�issue desquelles l�utilisateur pourra choisir de lancer la vid�o dans son int�gralit�. Pendant la lecture, un extrait d�une autre vid�o serait mis en avant dans une optique de r�tention des twittos.
Une deuxi�me fonctionnalit� va faire son apparition sur Twitter dans les prochains jours : les conversations priv�es de groupe. Elles exploiteront la fonction DM (��Direct Messages�� ou ��Messages�� en fran�ais), symbolis�e par une ic�ne repr�sentant une enveloppe. Plusieurs personnes pourront rejoindre ces discussions en cercle restreint afin de partager des tweets, des images, des liens et des �motic�nes.
Le sch�ma est le suivant : l�utilisateur peut lancer une conversation avec n�importe lequel de ses followers ; ces derniers n�ont pas besoin de se suivre mutuellement pour participer � la discussion, qui r�unira un maximum de 20 participants. Quiconque est ajout� � un groupe re�oit une notification (voir la d�monstration vid�o ci-dessous).
Twitter poursuit l� l�enrichissement de son service de microblogage apr�s avoir lanc� la traduction automatique des tweets dans une quarantaine de langues en partenariat avec Bing de Microsoft� et un syst�me de r�capitulatif �� Pendant que vous �tiez parti �� qui permet � chacun de lire, en un coup d�oeil, les messages publi�s depuis sa derni�re connexion.
�� A voir aussi ��
