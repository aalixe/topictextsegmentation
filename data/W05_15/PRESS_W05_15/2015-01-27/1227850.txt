TITRE: Sean Penn rece�vra un C�sar d�hon�neur lors de la prochaine c�r�mo�nie - Voici
DATE: 27-01-2015
URL: http://www.voici.fr/news-people/actu-people/sean-penn-recevra-un-cesar-d-honneur-lors-de-la-prochaine-ceremonie-551915
PRINCIPAL: 1227846
TEXT:
Publi� le mardi 27 janvier 2015 � 09:38� par Marie Astrid KUNERTH
Sean Penn rece�vra un C�sar d�hon�neur lors de la prochaine c�r�mo�nie
Une l�gende r�com�pen�s�e
L�ac�teur et r�ali�sa�teur am�ri�cain Sean Penn va rece�voir un prix pour l�en�semble de son �uvre lors de la 40�me c�r�mo�nie des C�sar.�
Il y avait eu Scar�lett Johans�son en 2014 , c�est son ex qui sera r�com�pens� cette ann�e. Lors de la 40�me c�r�mo�nie des C�sar qui se d�rou�lera le 20 f�vrier prochain au th��tre du Ch�te�let, Sean Penn se verra remettre un C�sar d�hon�neur. ��Acteur mythique, person�na�lit� enga�g�e, r�ali�sa�teur d�ex�cep�tion, Sean Penn est une ic�ne � part enti�re dans le cin�ma am�ri�cain. Une l�gende de son vivant��, a expliqu� l�Aca�d�mie des arts et tech�niques du cin�ma dans un commu�niqu� publi� lundi soir.
Sa toute premi�re r�com�pense, Sean Penn l�a re�ue en France, lors du Festi�val de Cannes de 1997, o� il a d�cro�ch� le prix d�in�ter�pr�ta�tion mascu�line pour She�s so lovely de Nick Cassa�vetes. Sept ans plus tard, il �tait aur�ol� de son premier Oscar de meilleur acteur pour Mystic River de Clint East�wood . En 2009, il a � nouveau d�cro�ch� la pr�cieuse la statuette pour son excellent r�le de poli�ti�cien mili�tant dans le long-m�trage de Gus Van Sant, Harvey Milk.
Excellent devant la cam�ra, il a prouv� qu�il pouvait tout aussi bien l��tre derri�re. Apr�s un premier essai en 1991 avec�The Indian runner, il a acc�d� � la recon�nais�sance inter�na�tio�nale gr�ce au succ�s reten�tis�sant d�Into the Wild en 2007. Un an plus tard, il �tait choisi pour pr�si�der le Festi�val de Cannes.
Le 20 f�vrier prochain, on esp�re que Sean Penn vien�dra accom�pa�gn� de Char�lize Theron , la com�dienne avec laquelle il se serait fianc� � Paris . Une belle occa�sion de reve�nir l� o� ils auraient d�cid� de se dire oui avec, pourquoi pas, une jolie bague � l�an�nu�laire gauche de la future Madame Penn.�
Mots-cl�s :
