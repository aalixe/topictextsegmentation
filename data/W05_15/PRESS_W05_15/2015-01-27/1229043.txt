TITRE: Nicole Kidman et Tom Cruise ont divorc� � cause de la scientologie... Le message de PPDA � sa fille disparue... - 20minutes.fr
DATE: 27-01-2015
URL: http://www.20minutes.fr/people/1526639-20150127-nicole-kidman-tom-cruise-divorce-cause-scientologie-message-ppda-fille-disparue
PRINCIPAL: 0
TEXT:
People
PEOPLE Retrouvez toute l'actualit� des stars sur �20 Minutes�...
Nicole Kidman et Tom Cruise ont divorc� � cause de la scientologie... Le message de PPDA � sa fille disparue...
Envoyer
Tom Cruise et Nicole Kidman 30 millions d'euros Apr�s 9 ans de mariage, le couple le plus glamour d'Hollywood entame une proc�dure de divorce en f�vrier 2001. Un accord � l'amiable est trouv� dix mois plus tard. Tom Cruise conserve la propri�t� du Colorado et les avions. Nicole Kidman r�cup�re la maison de L.A. et celle de Sydney. - REUTERS
B.C.
Mardi 27 janvier 2015
Un documentaire accuse la scientologie
Si Tom Cruise et Nicole Kidman ont divorc� en 2001, c�est la faute � la scientologie. Cette th�se est d�fendue dans le documentaire Going Clear:�Scientology and the Prison of Belief pr�sent� au festival de Sundance. L�enqu�te d�Alex Gibney se base sur le livre de Lawrence Wright et sera diffus�e sur HBO prochainement. Les t�l�spectateurs d�couvriront comment la secte s�est organis�e pour �loigner Nicole Kidman, jug�e �n�faste� � l�organisation. Des d�tectives priv�s auraient ainsi �t� engag�s pour chercher des noises � Nicole. Apr�s le divorce, la secte s�est ensuite mobilis�e pour trouver une nouvelle femme � Tom Cruise. Mais aussi pour r�nover sa maison, entre autres petits services, afin que l�acteur revienne dans le giron de la scientologie. L�organisation crie d�j� au complot et aux mensonges. HBO, de son c�t�, a mis 160 avocats sur le dossier pour se prot�ger .
Irina Shayk accuse Cristiano Ronaldo de tromperie
Le top model russe commence � l�cher quelques infos sur les r�elles raisons de sa s�paration avec Cristiano Ronaldo. Si le couple a communiqu� sur la question de leur rupture apr�s cinq ans de relation, des �proches� d�Irina Shayk ont affirm� au Daily Mail que la belle en avait surtout assez de jouer la petite amie officielle alors que le footballeur multipliait les conqu�tes. Le couple officiel ne se voyait quasiment jamais.
>> Toute l�actualit� des people pris sur le vif est dans notre diaporama Rep�r�s!
Kim Kardashian va jouer au Superbowl
La starlette a annonc� sur Twitter qu�elle appara�trait dans une des fameuses publicit�s diffus�es � la mi-temps du Superbowl. �Trop excit�e�, Kim Kardashian n�en dit pas plus si ce n�est la marque qui l�a engag�e. La photo de tournage qui accompagne le message est �galement tr�s sobre.
So excited!�Going to be in a @TMobile ad!�Only sad part was no @JohnLegere sighting. See it on @ConanOBrien tonight!� pic.twitter.com/W3tATov24C
� Kim Kardashian West (@KimKardashian) January 26, 2015
L��mouvant message de Patrick Poivre d�Arvor � sa fille disparue
Vingt ans jour pour jour apr�s le suicide de sa quatri�me fille, Solenn, Patrick Poivre d�Arvor a publi� un touchant message sur Twitter.
Il y a tout juste vingt ans,jour pour jour,Solenn quittait ce monde.Vingt ans avec elle,vingt ans sans elle.Mais tellement pr�sente!
� Poivre d'Arvor (@PPDA) January 27, 2015
Le 27 janvier 1995, Solenn s��tait jet�e sous un m�tro parisien. Dans un message � ses parents , qui l�aidaient depuis des ann�es � lutter contre l�anorexie et la boulimie, elle �crivait:��Je vous aime mais je n�aime pas la vie.�
