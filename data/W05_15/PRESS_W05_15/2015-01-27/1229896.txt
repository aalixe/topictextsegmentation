TITRE: Violente manifestation antigouvernementale � Pristina: 80 bless�s
DATE: 27-01-2015
URL: http://www.romandie.com/news/Violente-manifestation-antigouvernementale-a-Pristina-80-blesses/559706.rom
PRINCIPAL: 1229874
TEXT:
Tweet
Violente manifestation antigouvernementale � Pristina: 80 bless�s
De violents incidents ont fait mardi plus de 80 bless�s lors d'accrochages entre la police et plusieurs milliers de manifestants kosovars qui r�clamaient la d�mission d'un ministre serbe accus� d'avoir insult� les Albanais kosovars. Plus d'une centaine de manifestants ont �t� interpell�s, selon la police.
Des manifestants ont jet� des pierres contre la police, qui a ripost� en utilisant du gaz lacrymog�ne lors de ces incidents devant le si�ge du gouvernement � Pristina, a rapport� un correspondant de l'AFP.
Il s'agit de la deuxi�me manifestation depuis samedi � Pristina pour r�clamer la d�mission du Serbe kosovar Aleksandar Jablanovic, ministre du Travail et l'un des trois ministres serbes du cabinet du Premier ministre Isa Mustafa.
"Jablanovic dehors" et "� bas le gouvernement", scandaient les manifestants, pour qui ce ministre a insult� la majorit� albanaise du Kosovo, territoire qui a proclam� son ind�pendance de la Serbie en 2008.
"Sauvages"
Il y a deux semaines, M. Jablanovic avait provoqu� la col�re des Albanais lorsqu'il avait qualifi� de "sauvages" des manifestants albanais qui avaient emp�ch� un groupe de Serbes de visiter un monast�re � l'occasion du No�l orthodoxe dans l'ouest du Kosovo sous le pr�texte que des "criminels de guerre" se trouvaient parmi les p�lerins.
Le ministre s'est publiquement excus�, mais ses propos ont provoqu� plusieurs manifestations d'Albanais. Ceux-ci repr�sentent plus de 90% des 1,8 million d'habitants du Kosovo.
Mardi, les �chauffour�es ont �clat� lorsque plusieurs milliers de personnes, r�unies � l'appel du mouvement nationaliste kosovar Autod�termination (Vetvendosje, opposition) ont tent� de p�n�trer dans le b�timent du gouvernement kosovar, dans le centre de Pristina.
Reprise du dialogue en f�vrier
Le gouvernement de M. Mustafa, r�cemment form�, compte trois ministres serbes, signe d'une volont� d'apaisement des relations entre les Albanais et les Serbes du Kosovo mais aussi avec la Serbie. Le dialogue entre Pristina et Belgrade doit reprendre d�but f�vrier � Bruxelles.
(ats / 27.01.2015 20h40)
