TITRE: Loi santé : Marisol Touraine rencontre les patrons de cliniques | www.directmatin.fr
DATE: 26-01-2015
URL: http://www.directmatin.fr/france/2015-01-26/loi-sante-marisol-touraine-rencontre-les-patrons-de-cliniques-698402
PRINCIPAL: 0
TEXT:
La loi sant� d�voil�e dans ses grandes lignes
Sur tous les fronts. Alors que des discussions ont d�j� �t� lanc�es avec les urgentistes et les m�decins, Marisol Touraine doit d�sormais composer avec les cliniques priv�es pour tenter de trouver des compromis sur le projet de loi sant�, vivement critiqu�.
�
Plusieurs repr�sentants sont attendus ce lundi au minist�re pour discuter de l��pineux dossier des d�passements d�honoraires. Car le nouveau texte change la donne : il pr�voit qu'un �tablissement puisse �tre consid�r� comme un service public hospitalier, dot� de cr�dits financiers, seulement si aucun de ses m�decins ne pratique de d�passement d�honoraire.
Une mesure qui fait bondir les patrons de clinique, dont les m�decins, notamment les chirurgiens, pratiquent des tarifs libres.
Marisol Touraine avait d�j� l�ch� du lest sur ce point en d�cembre, acceptant de r�fl�chir � "des d�rogations limit�es" face � la menace de gr�ve illimit�e du secteur priv�. �
�
