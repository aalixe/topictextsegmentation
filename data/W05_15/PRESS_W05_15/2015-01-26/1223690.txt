TITRE: Miss Univers. La Colombienne Pauline Vega sacr�e Miss Univers 2014
DATE: 26-01-2015
URL: http://www.ouest-france.fr/miss-univers-la-colombienne-pauline-vega-sacree-miss-univers-2014-3144357
PRINCIPAL: 1223686
TEXT:
Miss Univers. La Colombienne Pauline Vega sacr�e Miss Univers 2014
Monde -
Miss Colombie a �t� sacr�e Miss Univers�|�Photo Reuters
Miss Colombie a �t� sacr�e Miss Univers�|�Photo EPA.
Miss Colombie a �t� sacr�e Miss Univers�|�Photo AFP
Achetez votre journal num�rique
Miss Colombie, Paulina Vega, a �t� sacr�e dimanche Miss Univers 2014, lors de la 63e �dition du concours organis�e � Doral pr�s de Miami.
La mannequin et �tudiante en commerce de 22 ans, qui �tait en comp�tition face � 87 autres femmes du monde entier, succ�de � la V�n�zu�lienne Gabriela Isler.
Pauline Vega a re�u sa couronne les larmes aux yeux et v�tue d'une longue robe argent�e �tincelante, au d�collet� profond.
Elle est la seconde Colombienne � remporter le titre, apr�s Luz Marina Zuluaga, en 1956.
Quatorze autres pays s'�taient hiss�s parmi les finalistes : Argentine, Australie, Br�sil, Espagne, Etats-Unis, France, Inde, Indon�sie, Italie, Jama�que, Pays-Bas, Philippines, Ukraine, Venezuela.
Les cinq derni�res pr�tendantes en lice �taient Pauline Vega, Miss Etats-Unis, Miss Jama�que, Miss Ukraine et Miss Pays-Bas.
Tags :
