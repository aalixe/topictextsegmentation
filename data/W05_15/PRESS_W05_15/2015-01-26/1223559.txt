TITRE: France/Monde | Claudia Priest: l�ex-otage de retour hier en France
DATE: 26-01-2015
URL: http://www.republicain-lorrain.fr/france-monde/2015/01/26/l-ex-otage-de-retour-hier-en-france
PRINCIPAL: 1223543
TEXT:
� > �Claudia Priest: l�ex-otage de retour hier en France
CENTRAFRIQUE Claudia Priest: l�ex-otage de retour hier en France
le 26/01/2015 � 05:00 par A Villacoublay, Patrice BARR�RE.
Claudia Priest a �t� accueillie par sa famille et le ministre  des Affaires �trang�res.  Photo AFP
Claudia Priest, enlev�e pendant cinq jours en Centrafrique, est arriv�e hier en France.
�Je suis contente de retrouver le sol fran�ais, m�me si le sol centrafricain est aussi un peu ma patrie �. L�humanitaire de 67 ans Claudia Priest, enlev�e pendant cinq jours en Centrafrique par des miliciens chr�tiens anti-balaka, est arriv�e hier peu apr�s 18 h � l�a�roport de Villacoublay, pr�s de Paris. Armand, son mari, Florent et B�reng�re, ses enfants, ont �t� les premiers � la serrer dans leurs bras � sa descente du Falcon estampill� R�publique fran�aise. Laurent Fabius, le ministre des Affaires �trang�res, repr�sentant le gouvernement, a dit son � bonheur � de la voir libre.
Cheveux gris courts, en baskets, pantalon beige et petit sac a dos, elle a remerci� bri�vement toutes les personnes qui sont intervenues pour aider � sa lib�ration. La voix cass�e, les traits tir�s, elle est apparue en relative bonne sant�.
Des roses blanches en cadeau
L��motion a �t� � son comble quand ses cinq petits enfants se sont pr�cipit�s vers elle avec des bouquets de roses blanches. Les yeux des enfants, �g�s de 5 � 11 ans, �taient embu�s. Le plus grand n�a pas pu contenir ses larmes. � Le retour de la mamie �tait d�sir�, a expliqu� son mari Armand. Nous sommes une famille unie. Nous sommes tr�s proches les uns des autres. Nous avons v�cu ce moment douloureux ensemble. � Dans le salon d�honneur de l�a�roport, � l�abri des cam�ras et appareils photos, les petits enfants se sont accapar�s leur grand-m�re. � � part quelques bisous, je n�ai pas pu beaucoup lui parler �, expliquait Armand Priest devant les journalistes. Avant une visite m�dicale qui aura lieu aujourd�hui, Armand Priest a estim� la sant� de sa femme � relativement bonne �. Il est revenu sur les conditions de la lib�ration et a r�fut� l�id�e du versement d�une ran�on. Armand Priest a surtout salu� l�intervention de l�archev�que de Bangui dans les n�gociations avec les ravisseurs. � Ni moi, ni mon �pouse n�en voulons aux ravisseurs. Ce sont des jeunes qui n�ont pas eu d��ducation et qui n�ont aucun avenir �, a-t-il ajout�. Ce qu�ils font � pour gagner quelques francs CFA, je peux presque l�excuser �.
Le couple a dit son d�sir de revenir travailler en Afrique. � Cette histoire ne remet pas en cause les activit�s de notre association humanitaire. Les Centrafricains sont dans nos c�urs. Les groupuscules incontr�l�s qui ont enlev� ma femme ne refl�tent pas la mentalit� de ce peuple �. Toutefois, Armand Priest, avec le sourire, a indiqu� qu�il demandera l�avis de ses enfants avant de repartir.
A Villacoublay, Patrice BARR�RE.
