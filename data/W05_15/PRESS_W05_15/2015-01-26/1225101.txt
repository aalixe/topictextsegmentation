TITRE: VIDEO. Etats-Unis: Plus de 2.000 vols annul�s avant une temp�te de neige exceptionnelle - 20minutes.fr
DATE: 26-01-2015
URL: http://www.20minutes.fr/monde/1525847-20150126-etats-unis-plus-2000-vols-annules-avant-tempete-neige-exceptionnelle
PRINCIPAL: 1225097
TEXT:
New York
Quelque 2.009 vols ont �t� annul�s lundi en provenance ou � destination des Etats-Unis, o� le trafic a�rien �tait affect� par 271 retards en d�but de matin�e, a pr�cis� le site sp�cialis� Flightaware. La m�t�o nationale a �mis un avis de blizzard pour New York et Boston, jusqu'� la fronti�re canadienne.
>> Racontez la temp�te de neige aux Etats-Unis
De 5 � 10 cm de neige attendus lundi
De 5 � 10 centim�tres de neige sont attendus lundi dans le nord-est, qui seront suivis par un autre �pisode neigeux lundi soir et mardi avec de �lourdes� pr�cipitations, notamment � Boston et New York, selon le service national de m�t�orologie (NWS). A u total, 45 � 60 cm de neige pourraient s'accumuler de New York � Boston, voire pr�s de 80 cm dans l'est du Massachusetts. Davantage de neige n'est pas � exclure dans d'autres r�gions isol�es, selon le NWS.
Le pr�sident am�ricain Barack Obama, actuellement en Inde, a �t� inform� des tr�s fortes chutes de neige attendues dans le nord-est des Etats-Unis, � New York et Boston notamment, a indiqu� lundi la Maison Blanche. �Des responsables de la Maison Blanche ont �t� en contact avec des responsables locaux le long de la c�te Est pour s'assurer qu'ils disposaient des ressources n�cessaires pour se pr�parer et r�agir imm�diatement � la temp�te�, a d�clar� Josh Earnest, porte-parole de l'ex�cutif am�ricain, lors d'un point de presse � New Delhi o� Barack Obama effectue une visite de trois jours.
Plus de 50 millions de personnes pourraient �tre affect�es
Le maire de New York Bill de Blasio a mis en garde dimanche contre l'arriv�e de cette temp�te qui pourrait �tre �tr�s probablement l'une des temp�tes de neige les plus importantes de l'histoire de cette ville�.
Plus de 50 millions de personnes dans le nord-est pourraient �tre affect�es par cette temp�te de neige qui risque de paralyser les transports. D�s dimanche, plusieurs compagnies a�riennes ont propos� aux voyageurs de changer gratuitement leurs vols et des centaines de vols ont �t� annul�s. A Boston, le maire Martin Walsh a appel� ses concitoyens � s'occuper de leurs voisins les plus vuln�rables.
