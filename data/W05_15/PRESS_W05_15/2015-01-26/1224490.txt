TITRE: Le 4e anniversaire de la r�volution marqu� par des violences
DATE: 26-01-2015
URL: http://www.romandie.com/news/Le-4e-anniversaire-de-la-revolution-marque-par-des-violences_RP/558739.rom
PRINCIPAL: 1224486
TEXT:
Tweet
Le 4e anniversaire de la r�volution marqu� par des violences
Manifestants islamistes et forces de l'ordre se sont affront�s ce week-end en Egypte � l'occasion du quatri�me anniversaire de la r�volte de 2011 qui a chass� Hosni Moubarak du pouvoir. Au moins 19 personnes, dont un policier, ont �t� tu�es, selon des responsables.
Dans le nord du Caire, douze manifestants islamistes ont �t� tu�s dans des affrontements avec la police au Caire, a dit un responsable du minist�re de la Sant�. Un policier a aussi �t� tu� et 11 ont �t� bless�s, selon le minist�re de l'Int�rieur. Au total, 150 personnes ont �t� arr�t�es en marge des rassemblements.
Six autres personnes ont �t� tu�es � Alexandrie, la deuxi�me ville du pays, dans le gouvernorat de Gizeh, pr�s du Caire, et dans la province de Baheira, dans le delta du Nil, ont soulign� les forces de s�curit�. L'an dernier, des dizaines de personnes ont p�ri lors des manifestations marquant cet anniversaire.
Photos de Mohamed Morsi
Pour marquer l'anniversaire du soul�vement populaire de 2011, les partisans de l'ex-pr�sident islamiste Mohamed Morsi avaient appel� � manifester contre l'actuel chef de l'Etat, Abdel Fattah al-Sissi. M. Morsi avait �t� renvers� par les militaires en juillet 2013.
Les manifestants ont brandi des photos de M. Morsi, a constat� un journaliste de Reuters sur place. Les forces de s�curit� sont rapidement intervenues et les ont interpell�s. Samedi, une manifestante avait d�j� �t� tu�e dans la capitale �gyptienne.
Selon son parti, l'Alliance socialiste populaire, elle a �t� tu�e par les forces de s�curit�. Un millier de personnes ont assist� dimanche � ses fun�railles.
(ats / 25.01.2015 20h53)
