TITRE: Miss Univers : la Colombienne Paulina Vega sacr�e, Camille Cerf dans le top 15 | Site mobile Le Point
DATE: 26-01-2015
URL: http://www.lepoint.fr/societe/miss-univers-la-colombienne-paulina-vega-sacree-camille-cerf-dans-le-top-15-26-01-2015-1899552_23.php
PRINCIPAL: 1223824
TEXT:
26/01/15 � 07h53
Miss Univers : la Colombienne Paulina Vega sacr�e, Camille Cerf dans le top 15
Pour la 63e �dition du concours, Pauline Vega a succ�d� � la V�n�zu�lienne Maria Gabriela Isler. Elle devient la seconde Colombienne � remporter le titre.
Pauline Vega vient tout juste de f�ter ses 22 ans, elle est la petite-fille du t�nor Gaston Vega. Aelxander Tamargo / Getty Images/AFP
Source AFP
Lors de la 63e �dition du concours organis�e � Doral pr�s de Miami, c'est la repr�sentante de la Colombie , Paulina Vega, qui a �t� sacr�e dimanche Miss Univers 2014. La mannequin et �tudiante en commerce de 22 ans, qui �tait en comp�tition face � 87 autres femmes du monde entier, succ�de � la V�n�zu�lienne Gabriela Isler.
Pauline Vega a re�u sa couronne avec des larmes dans les yeux et v�tue d'une longue robe argent�e �tincelante. Elle est la seconde Colombienne � remporter le titre, apr�s Luz Marina Zuluaga en 1956. Miss France , la nordiste Camille Cerf, a, elle, termin� dans les 14 autres pays finalistes : Argentine , Australie, Br�sil, Espagne, �tats-Unis, France, Inde, Indon�sie, Italie, Jama�que, Pays-Bas, Philippines, Ukraine, Venezuela.
Les cinq derni�res pr�tendantes en lice �taient Pauline Vega, Miss �tats-Unis, Miss Jama�que, Miss Ukraine et Miss Pays-Bas.
