TITRE: Incendie accidentel dans la maison de Jean-Marie Le Pen, l�g�rement bless�
DATE: 26-01-2015
URL: http://www.ladepeche.pf/Incendie-accidentel-dans-la-maison-de-Jean-Marie-Le-Pen-legerement-blesse_a2910.html?com
PRINCIPAL: 1226528
TEXT:
Incendie accidentel dans la maison de Jean-Marie Le Pen, l�g�rement bless�
Lundi 26 Janvier 2015
(Photo: AFP)
Un incendie s'est d�clar� lundi dans la maison de Jean-Marie Le Pen, qui a �t� l�g�rement bless� au visage en voulant �chapper aux flammes, a-t-on appris aupr�s de son entourage. "Il s'agit d'un d�part de feu accidentel qui a pris vraisemblablement dans le conduit de la chemin�e sur laquelle travaillait un ouvrier", � la mi-journ�e dans la maison de M. Le Pen � Rueil-Malmaison (Hauts-de-Seine), a relat� � l'AFP une source polici�re, confirmant une information de LCI. "La maison a br�l�, mon p�re �tait � l'int�rieur. Il est bless� au visage, mais il n'y a rien de grave, rien d'inqui�tant", a d�clar� la pr�sidente du Front national (FN), Marine Le Pen, � l'AFP. M. Le Pen "s'est refugi� sur la terrasse" et "est tomb� en voulant �chapper aux flammes", a expliqu� une autre source.
Lui et son �pouse Jany ont �t� pris en charge par les pompiers, mais leur �tat ne n�cessitait pas une hospitalisation, a-t-on pr�cis� dans l'entourage du pr�sident d'honneur du FN.
�
le Lundi 26 Janvier 2015 � 10:42 | Lu 12191 fois
Nouveau commentaire :
