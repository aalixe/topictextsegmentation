TITRE: Assurance-maladie�: les d�penses ont d�rap� en�2014 | Le Quotidien du M�decin
DATE: 26-01-2015
URL: http://www.lequotidiendumedecin.fr/actualite/securite-sociale/assurance-maladie-les-depenses-ont-derape-en-2014
PRINCIPAL: 1225885
TEXT:
une alerte
Les d�penses du r�gime g�n�ral de l�Assurance-maladie ont augment� de 3,1�% en 2014, un chiffre sup�rieur � l�objectif fix� par le gouvernement, selon les derniers chiffres de la Caisse nationale d�assurance-maladie�(CNAM).
��En 2014, les remboursements de soins du r�gime g�n�ral ont progress� de 3,1�% dont 3,6�% pour les remboursements de soins de ville�� (hors h�pital), indique la Caisse dans un communiqu�. Qui pr�cise qu���en donn�es corrig�es des jours ouvr�s les �volutions sont respectivement de 3,3�% et de 3,8�%��.
La LFSS (loi pour le financement de la S�curit� sociale) vot�e en�2013 avait fix� un objectif national de d�penses de l�assurance-maladie (ONDAM) de 2,4�% pour l�ann�e 2014.
Dans le d�tail, ��les remboursements de m�dicaments d�livr�s en ville d�croissent faiblement en�2014 (-�0,6�% apr�s, - 0,1�% en 2013 et -�1,6�% en 2012)��, mais ��les remboursements dits de r�trocession hospitali�re (m�dicaments prescrits en ville mais d�livr�s � l�h�pital) augmentent de pr�s de 70�%��.
Le poids des nouveaux m�dicaments on�reux
��Cette forte hausse est int�gralement li�e � l�arriv�e de nouveaux m�dicaments efficaces et on�reux�� contre l�h�patite�C, ��en d�but d�ann�e 2014, dans le circuit de la r�trocession hospitali�re��, pr�cise la CNAM.
Le prix de l�un d�entre eux, le Sovaldi, du laboratoire am�ricain Gilead, a �t� ren�goci� par le gouvernement en novembre dernier, pour atteindre 41�000�euros pour 12 semaines de traitement, contre 57�000 auparavant.
Les remboursements des autres produits de sant�, comme les dispositifs m�dicaux, enregistre ��une progression encore soutenue�� (+�6�%, contre +�6,9�% en 2013).
Avec AFP
