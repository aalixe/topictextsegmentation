TITRE: Maggi plaque le groupe PS - Lib�ration
DATE: 26-01-2015
URL: http://www.liberation.fr/politiques/2015/01/26/maggi-plaque-le-groupe-ps_1189312
PRINCIPAL: 0
TEXT:
Maggi plaque le groupe PS
26 janvier 2015 � 21:36
Lire sur le readerMode zen
Coup de sang . � l�Assembl�e Nationale
Avant m�me la l�gislative partielle dans le Doubs, dont le premier tour doit se tenir ce dimanche (lire pages�10-11), le groupe PS a d�j� perdu sa majorit� absolue � l�Assembl�e. Comme il l�avait annonc�, le gu�riniste Jean-Pierre Maggi, d�put� de�la�8e�circonscription des Bouches-du-Rh�ne, a quitt� le groupe socialiste et a adh�r� au groupe RRDP (� majorit� radicaux de gauche). Le groupe PS comprenait 289�membres (sur�577�d�put�s au total), soit tout juste la majorit� absolue. Mais au-del� de la symbolique, cette d�cision ne devrait a�priori pas avoir de cons�quence sur la capacit� de l�ex�cutif � faire voter ses principaux textes au Palais Bourbon. Au groupe RRDP, on fait toutefois valoir qu��il sera [�] d�autant plus utile au gouvernement d�obtenir [son] accord�[�], pour l�adoption de tel ou tel texte de loi�.
Recevoir la newsletter
