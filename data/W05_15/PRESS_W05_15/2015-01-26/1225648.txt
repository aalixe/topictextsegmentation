TITRE: Open d'Australie : Djokovic avec patience et autorit� | France info
DATE: 26-01-2015
URL: http://www.franceinfo.fr/sports/sports/article/open-d-australie-djokovic-avec-patience-et-autorite-636207
PRINCIPAL: 1225645
TEXT:
Open d'Australie : Djokovic avec patience et autorit�
Tennis par media365.fr lundi 26 janvier 2015 16:26
Oppos� � Gilles Muller lundi, Novak Djokovic a su acc�l�rer aux moments cl�s pour s'imposer face � un bon adversaire. Parfois en difficult� sur son service, le Serbe a finalement r�ussi � retourner la situation avant de prendre le service du Luxembourgeois en toute fin de deuxi�me et troisi�me manche pour �viter le tie break. Il se qualifie pour les quarts de finale en trois sets face � un Muller qui a pourtant fait mieux que r�sister (6-4, 7-5, 7-5). Djokovic disputera son 23eme quart de finale en Grand Chelem de rang face � Milos Raonic.
OPEN D�AUSTRALIE (Melbourne, Grand Chelem, dur, 12 571 000�)
Tenant du titre : Stan Wawrinka (SUI)
Quarts de finale
Djokovic (SER, n�1) -�Raonic (CAN, n�8)
Wawrinka (SUI, n�4) -�Nishikori (JAP, n�5)�
Nadal (ESP, n�3) -�Berdych (RTC, n�7)
Murray (GBR, n�6) -�Kyrgios (AUS)
Huiti�mes de finale
Djokovic (SER, n�1) bat Muller (LUX) : 6-4, 7-5, 7-5
Raonic (CAN, n�8) bat�Lopez (ESP, n�12) : 6-4, 4-6, 6-3, 6-7, 6-3
Wawrinka (SUI, n�4) bat Garcia-Lopez (ESP) : 7-6, 6-4, 4-6, 7-6
Nishikori (JAP, n�5) bat�Ferrer (ESP, n�9) : 6-3, 6-3, 6-3
Nadal (ESP, n�3)�bat�Anderson (AFS, n�14) :�7-5, 6-1, 6-4
Berdych (RTC, n�7)�bat�Tomic (AUS) :�6-2, 7-6, 6-2
Murray (GBR, n�6)�bat Dimitrov (BUL, n�10) :�6-4, 6-7, 6-3, 7-5
Kyrgios (AUS) bat Seppi (ITA) :�5-7, 4-6, 6-3, 7-6, 8-6
3eme tour
Djokovic (SER, n�1) bat Verdasco (ESP, n�31) :�7-6, 6-3, 6-4
Muller (LUX) bat�Isner (USA, n�19) :�7-6, 7-6, 6-4
Lopez (ESP, n�12) bat Janowicz (POL) : 7-6, 6-4, 7-6
Raonic (CAN, n�8) bat�Becker (ALL) : 6-4, 6-3, 6-3
Wawrinka (SUI, n�4)�bat Nieminen (FIN) : 6-4, 6-2, 6-4
Garcia-Lopez (ESP) bat�Pospisil (CAN) : 6-2, 6-4, 6-4
Ferrer (ESP, n�9) bat�Simon (FRA, n�18)�:�6-2, 7-5, 5-7, 7-6
Nishikori (JAP, n�5) bat�Johnson (USA) :�6-7 6-1 6-2 6-3
Berdych (RTC, n�7) bat Troicki (SER) : 6-4, 6-3, 6-4�
Tomic (AUS) bat Groth (AUS) : 6-4, 7-6, 6-3�
Anderson (AFS, n�14) bat�Gasquet (FRA, n�24)�: 6-4, 7-6, 7-6
Nadal (ESP, n�3) bat Sela (ISR) : 6-1, 6-0, 7-5�
Murray (GBR, n�6) bat Sousa (POR) : 6-1, 6-1, 7-5�
Dimitrov (BUL, n�10) bat Baghdatis (CHY) : 4-6, 6-3, 3-6, 6-3, 6-3
Kyrgios (AUS) bat�Jaziri (TUN) : 6-3, 7-6, 6-1
Seppi (ITA) bat Federer (SUI, n�2) : 6-4, 7-6, 4-6, 7-6
2eme tour
Djokovic (SER, n�1) bat Kuznetsov (RUS) : 6-0, 6-1, 6-4�
Verdasco (ESP, n�31) bat Soeda (JAP) : 6-3, 6-2, 7-6
Isner (USA, n�19) bat Haider-Maurer (AUT) : 6-4, 7-6, 4-6, 6-4
Muller (LUX) bat Bautista Agut (ESP, n�13) : 7-6, 1-6, 7-5, 6-1
Lopez (ESP, n�12) bat�Mannarino (FRA) : 4-6, 4-6, 7-6, 4-0 abandon
Janowicz (POL) bat�Monfils (FRA, n�17) : 6-4, 1-6, 6-7, 6-3, 6-3
Becker (ALL) bat Hewitt (AUS) : 2-6, 1-6, 6-3, 6-4, 6-2
Raonic (CAN, n�8) bat Young (USA) : 6-4, 7-6, 6-3�
Wawrinka (SUI, n�4) bat Copil (ROU, Q) : 7-6, 7-6, 6-3
Nieminen (FIN) bat Bachinger (ALL, Q) : 7-6, 7-5, 7-5
Pospisil (CAN) bat Lorenzi (ITA) : 6-7, 7-6, 6-3, 6-4
Garcia-Lopez (ESP) bat Gonzalez (COL) : 6-1, 6-3, 6-3
Ferrer (ESP, n�9) bat Stakhovsky (UKR) : 5-7, 6-3, 6-4, 6-2
Simon (FRA, n�18) bat�Granollers (ESP) : 7-6, 6-2, 6-4�
Johnson (USA) bat Giraldo (COL, n�30) : 6-3, 6-4, 6-2�
Nishikori (JAP, n�5) bat Dodig (CRO) : 4-6, 7-5, 6-2, 7-6
Berdych (RTC, n�7) bat Melzer (AUT, Q) : 7-6, 6-2, 6-2�
Troicki (SER) bat Mayer (ARG, n�26) : 6-4, 4-6, 6-4, 6-0
Tomic (AUS) bat�Kohlschreiber (ALL, n�22) : 6-7, 6-4, 7-6, 7-6
Groth (AUS) bat Kokkinakis (AUS, WC) : 3-6, 6-3, 7-5, 3-6, 6-1
Anderson (AFS, n�14) bat Berankis (LIT) : 6-2, 6-2, 7-6
Gasquet (FRA, n�24)�bat�Duckworth (AUS, WC) : 6-2, 6-3, 7-5
Sela (ISR)�bat�Rosol (RTC, n�28) : 7-6, 5-7, 7-5, 6-3
Nadal (ESP, n�3) bat�Smyczek (USA, Q) : 6-2, 3-6, 6-7, 6-3, 7-5
Murray (GBR, n�6) bat Matosevic (AUS) : 6-1, 6-3, 6-2
Sousa (POR) bat Klizan (SLQ, n�32) : 4-6, 7-6, 6-4, 1-0 ab
Baghdatis (CHY) bat�Goffin (BEL, n�20) : 6-1, 4-6, 6-4, 6-0
Dimitrov (BUL, n�10) bat�Lacko (SLQ) : 6-3, 6-7, 6-3, 6-3
Jaziri (TUN) bat�Roger-Vasselin (FRA) : 1-6, 6-3, 6-4, 1-6, 6-3
Kyrgios (AUS)�bat Karlovic (CRO, n�23) : 7-6, 6-4, 5-7, 6-4
Seppi (ITA)�bat�Chardy (FRA, n�29) : 7-5, 3-6, 6-2, 6-1
Federer (SUI, n�2) bat�Bolelli (ITA) : 3-6, 6-3, 6-2, 6-2
1er tour
Djokovic (SER, n�1) bat Bedene (SLO, Q) : 6-3, 6-2, 6-4
Kuznetsov (RUS) bat Ramos-Vinolas (ESP) : 6-1, 3-6, 6-3, 7-6
Soeda (JAP) bat Ymer, (SUE, Q) : 1-6, 6-4, 4-6, 6-3, 6-3
Verdasco (ESP, n�31) bat�Ward (GBR) : 2-6, 6-0, 7-6, 6-3
Isner (USA, n�19) bat Wang (TPE, Q) : 7-6, 6-4, 6-4
Haider-Maurer (AUT) bat�Lokoli (FRA, Q)�: 6-4, 7-5, 4-6, 6-3
Muller (LUX) bat�Carreno Busta (ESP) : 6-4, 7-6, 7-6
Bautista Agut (ESP, n�13) bat�Thiem (AUT) : 4-6, 6-2, 6-3, 7-6
Lopez (ESP, n�12) bat Kudla (USA, WC) : 3-6, 6-2, 4-6, 6-2, 10-8
Mannarino (FRA) bat�Rola (SLO) : 7-6, 6-3, 6-2
Janowicz (POL) bat�Moriya (JAP, LL) : 7-6, 2-6, 6-3, 7-5
Monfils (FRA, n�17) bat�Pouille (FRA, WC)�: 6-7, 3-6, 6-4, 6-1, 6-4
Becker (ALL) bat�Benneteau (FRA, n�25)�: 7-5, 5-7, 6-2, 6-4
Hewitt (AUS) bat Zhang (CHN, WC) : 6-3, 1-6, 6-0, 6-4
Young (USA) bat Puetz (ALL, Q) : 6-4, 4-6, 6-3, 6-2
Raonic (CAN, n�8) bat�Marchenko (UKR, Q) : 7-6, 7-6, 6-3
Wawrinka (SUI, n�4) bat Ilhan (TUR) : 6-1, 6-4, 6-2
Copil (ROU, Q) bat Andujar (ESP) : 6-2, 6-2, 7-5
Nieminen (FIN) bat Golubev (KAZ) : 6-1, 6-2, 7-6
Bachinger (ALL, Q) bat Cuevas (URU, n�27) : 7-6, 6-3, 6-1
Lorenzi (ITA) bat�Dolgopolov (UKR, n�21) : 6-4, 6-3, 6-2
Pospisil (CAN) bat�Querrey (USA) : 6-3, 6-7, 2-6, 6-4, 6-4
Garcia-Lopez (ESP) bat Gojowczyk (ALL) : 6-7, 7-5, 6-4, 1-0 ab
Gonzalez (COL) bat Fognini (ITA, n�16) : 4-6, 6-2, 6-3, 6-4
Ferrer (ESP, n�9) bat Bellucci (BRE) : 6-7, 6-2, 6-0, 6-3
Stakhovsky (UKR) bat Lajovic (SER) : 6-3, 4-6, 6-4, 6-7, 6-4
Granollers (ESP) bat�Robert (FRA) : 6-3, 6-4, 6-4
Simon (FRA, n�18) bat�Haase (PBS) : 6-1, 6-3, 6-4
Giraldo (COL, n�30) bat Hernych (RTC, Q) : 6-3, 6-2, 6-2
Johnson (USA) bat Edmund (GBR, Q) : 6-4, 6-4, 6-3
Dodig (CRO) bat Souza (BRE) : 6-4, 7-5, 6-4
Nishikori (JAP, n�5) bat�Almagro (ESP)�: 6-4, 7-6, 6-2
Berdych (RTC, n�7) bat Falla (COL) : 6-3, 7-6, 6-3
Melzer (AUT, Q) bat Estrella Burgos (DOM) : 6-1, 6-4, 6-2
Troicki (SER) bat�Vesely (RTC) : 6-3, 3-6, 6-2, 6-3
Mayer (ARG, n�26) bat�Millman (AUS, WC) : 6-3, 6-3, 6-2
Kohlschreiber (ALL, n�22) bat�Mathieu (FRA) : 6-2, 6-2, 6-1
Tomic (AUS) bat�Kamke (ALL) : 7-5, 6-7, 6-3, 6-2
Groth (AUS) bat Krajinovic (SER) : 6-3, 7-6, 6-4
Kokkinakis (AUS, WC) bat Gulbis (LET, n�11) :�5-7, 6-0, 1-6, 7-6, 8-6
Anderson (AFS, n�14) bat Schwartzman (ARG) : 7-6, 7-5, 5-7, 6-4
Berankis (LIT) bat Sijsling (PBS) : 2-6, 6-4, 7-6, 6-7, 6-4
Duckworth (AUS, WC) bat�Kavcic (SLO) : 6-2, 5-7, 7-6, 3-6, 6-2
Gasquet (FRA, n�24) bat�Berlocq (ARG)�: 6-1, 6-3, 6-1
Rosol (RTC, n�28) bat�De Schepper (FRA) : 4-6, 6-2, 6-7, 6-3, 6-4
Sela (ISR) bat�Struff (ALL) : 6-4, 4-6, 3-6, 6-3, 7-5
Smyczek (USA, Q) bat�Saville (AUS, WC) : 7-6, 7-5, 6-4
Nadal (ESP, n�3) bat�Youzhny (RUS) : 6-3, 6-2, 6-2
Murray (GBR, n�6) bat Bhambri (IND, Q) : 6-3, 6-4, 7-6
Matosevic (AUS) bat�Kudryavtsev (RUS, Q) : 6-4, 6-7, 4-6, 7-5, 6-3
Sousa (POR) bat�Thompson (AUS, WC) : 6-4, 7-6, 6-4
Klizan (SLQ, n�32) bat�Ito (JAP) : 7-6, 6-2, 6-4
Goffin (BEL, n�20) bat Russell, (USA, Q) : 6-3, 6-3, 5-7, 6-0
Baghdatis (CHY) bat Gabashvili (RUS) : 6-2, 6-7, 3-6, 6-4, 6-4
Lacko (SLQ) bat�Gonzalez (ARG) : 4-6, 6-2, 7-5, 6-7, 6-1
Dimitrov (BUL, n�10) bat�Brown (ALL) : 6-2, 6-3, 6-2
Roger-Vasselin (FRA) bat�Robredo (ESP, n�15) : 2-3 ab
Jaziri (TUN) bat Kukushkin (KAZ) : 6-2, 6-3, 2-6, 7-6
Kyrgios (AUS) bat�Delbonis (ARG) : 7-6, 3-6, 6-3, 6-7, 6-3
Karlovic (CRO, n�23) bat�Bemelmans (BEL, Q) : 6-4, 6-2, 6-4
Chardy (FRA, n�29)�bat Coric (CRO) : 3-6, 6-4, 7-5, 6-4
Seppi (ITA) bat Istomin (OUZ) : 5-7, 6-3, 2-6, 6-1, 6-4
Bolelli (ITA) bat�Monaco (ARG) : 6-3, 3-6, 6-3, 6-1
Federer (SUI, n�2) bat�Lu (TPE) : 6-4, 6-2, 7-5
par media365.fr lundi 26 janvier 2015 16:26
