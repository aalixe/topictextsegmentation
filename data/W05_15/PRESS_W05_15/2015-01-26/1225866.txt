TITRE: Can 2015- Le Congo en quarts de finales � Rewmi.com - actualit� au s�n�gal
DATE: 26-01-2015
URL: http://www.rewmi.com/can-2015-congo-en-quarts-finales.html
PRINCIPAL: 1225862
TEXT:
Accueil / SPORT / Can 2015- Le Congo en quarts de finales
Can 2015- Le Congo en quarts de finales
Partager !
tweet
�Vainqueur du Burkina Faso, le Congo, dirig� par le s�lectionneur fran�ais Claude Le Roy, a d�croch� une qualification historique pour les quarts de finale de la CAN-2015. La Guin�e �quatoriale a �galement valid� son billet.
Quatre jours apr�s avoir �crit une belle page de son histoire en s�imposant dans un match de Coupe d�Afrique pour la premi�re fois depuis 41 ans , le Congo a r�cidiv�, dimanche 25 janvier, � Ebebiyin.
Au terme d�une rencontre anim�e, les hommes du s�lectionneur fran�ais Claude Le Roy ont dispos� du Burkina Faso (2-1) et ainsi valid� leur billet pour les quarts de finale de la CAN-2015. Une premi�re depuis 1992 ! Face au Burkina, les Diables rouges ont d�marr� tambour battant, multipliant les initiatives sur le front de l�attaque.
Bifouma, d�j� excellent face au Gabon en d�but de semaine, a confirm� qu�il �tait bien devenu le moteur offensif de sa s�lection. Peu avant la demi-heure de jeu, c�est lui qui s�est procur� la premi�re occasion du match. Ndinga, lanc� dans l�axe, a d�cal� son attaquant c�t� gauche. Bifouma, � l�entr�e de la surface, a d�coch� une superbe frappe du gauche qui est pass�e tout pr�s des cages de Sanou.
Les Diables rouges plus inspir�s que les �talons
Apr�s une premi�re mi-temps anim�e dans l�entrejeu mais assez pauvre en occasion franches, les Congolais sont revenus sur le terrain avec d�excellentes intentions. Cinq minutes ont suffi pour que Bifouma, encore lui, ouvre enfin le score.
Sur un centre venu du c�t� droit et sign� Fod� Dor�, l�attaquant d�Almeria a fusill� Sanou en catapultant le cuir d�un tacle rageur (51e, 1-0).
Derri�re, les �talons ont bien tent� de r�agir, mais sans parvenir � se montrer dangereux. Pitroipa, peu inspir� sur le front de l�attaque, a multipli� les positions de hors jeu face � une d�fense congolaise bien align�e.
En toute fin de match, les Burkinab� ont bien cru revenir au score, mais le but de Bertrand Traor�, cons�cutif � un ballon perdu de Mafoumbi dans sa surface, a �t� logiquement refus� pour une faute sur le portier des Diables rouges (84e).
Une fin de match �pique
Deux minutes plus tard, sur leur deuxi�me tir cadr� du match, les �talons sont finalement revenus au score par Banc�, bien lanc� dans l�axe par Ou�draogo (1-1, 86e). L�attaquant du HJK Helsinki, rempla�ant depuis le d�but de la comp�tition, a �galis� d�une reprise du plat du pied � hauteur du point de p�nalty.
Mais le suspense a finalement �t� de courte dur�e. Sur l�action suivante, le Congo a assur� sa qualification gr�ce � Ondama. Sur un coup franc lointain, Sanou a totalement loup� sa sortie et a d�vi� le ballon sur la t�te du Congolais, tout heureux de voir le cuir filer dans les buts vides (2-1, 88e).
Issiaka Tour� envoy� sp�cial
