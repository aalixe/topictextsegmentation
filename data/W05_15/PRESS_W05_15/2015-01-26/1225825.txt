TITRE: Popcorn time : un concurrent potentiel de Netflix
DATE: 26-01-2015
URL: http://www.infos-mobiles.com/popcorn-time/popcorn-time-concurrent-potentiel-netflix/88714
PRINCIPAL: 1225824
TEXT:
Accueil � Popcorn time : un concurrent potentiel de Netflix
Popcorn time : un concurrent potentiel de Netflix
Henri le 26.01.2015 � 17:16 Commentaires ferm�s
Popcorn Time , l�application pour les appareils Mac et iOS jailbreak�s qui vous permet de diffuser des torrents de cin�ma et de t�l�vision � partir du Cloud, a souvent �t� salu� comme ��Netflix pour les pirates�� gr�ce � son interface facile � utiliser et son grand choix de contenu. Il s�av�re que c�est assez pour Netflix � consid�rer Popcorn Time comme un concurrent direct.
Il est facile de voir pourquoi Netflix est inquiet
Popcorn time : un concurrent potentiel de Netflix
Dans une lettre envoy�e aux actionnaires hier, Netflix a d�clar� que la piraterie �tait un de ses principaux concurrents et a plus particuli�rement soulign� Popcorn Time comme un rival possible. N�importe qui peut t�l�charger l�application Popcorn Time pour facilement diffuser des films et des �missions de t�l�vision sans rien conna�tre du soi-disant torrent. Le piratage sur Bittorrent n�est pas quelque chose de nouveau, mais il n�a jamais �t� aussi facile que Popcorn Time le rend.
L�application vous permet de parcourir un catalogue iTunes, des films et �missions de t�l�vision par leurs affiches, s�lectionnez l�un pour plus d�informations, puis commenc�es � diffuser apr�s une courte p�riode de tampon. C�est tellement simplifi� que n�importe qui devrait �tre capable de tout ramasser et de commencer � regarder.
Pour Netflix, c�est pr�occupant
Netflix pointe vers un graphique Google Trends pour comparer la popularit� des recherches pour Netflix, HBO, et Popcorn Time aux Pays-Bas. Le graphique montre la popularit� de Popcorn Time en forte hausse au cours des six derniers mois, de sorte que c�est maintenant � �galit� avec Netflix et bien au-dessus du HBO. Netflix appelle la popularit� de Popcorn time comme un donn� ��� r�fl�chir.�� Bien s�r, ce n�est que des donn�es dans le Pays-Bas, mais en regardant les donn�es globales, le graphique est loin d��tre diff�rente. C�est un signe que Popcorn Time gagne en importance. Netflix est une grande force de dissuasion du piratage parce que ce n�est pas cher et facile, mais Popcorn Time est en train de faire le piratage trop facile, trop.
Essentiellement, les services comme Netflix et Spotify parviennent � r�ussir en faisant du contenu en streaming sur Internet. Ce dernier est plus facile que le piratage. Mais qu�est-ce qui arrive quand le piratage de contenu est plus facile que d�utiliser Netflix?
