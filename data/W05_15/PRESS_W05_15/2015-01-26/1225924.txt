TITRE: J�r�me Cahuzac aurait utilis� les comptes de sa m�re pour dissimuler des revenus | Atlantico.fr
DATE: 26-01-2015
URL: http://www.atlantico.fr/pepites/jerome-cahuzac-aurait-utilise-comptes-mere-pour-dissimuler-revenus-1974451.html
PRINCIPAL: 1225922
TEXT:
Le Briefing Atlantico Business CULTURE Elections d�partementales 2015 La guerre contre l'Etat Islamique Fran�ois Hollande, "lui" pr�sident Primaire UMP 2016 Mort de Muffat, Arthaud et Vastine Les chiffres du ch�mage en France Les chiffres de l'�conomie en France La loi Macron Salon de l'agriculture 2015 D�mission de Thierry Lepaon Les id�es de 2014 Ceux qui voulaient changer le monde : les id�es pour 2015
J�r�me Cahuzac aurait utilis� les comptes de sa m�re pour dissimuler des revenus
Selon l'AFP, le parquet national financier (PNF) estime que 210 000 euros ont �t� transf�r�s sur des comptes appartenant � la m�re de J�r�me Cahuzac entre 2003 et 2010.
Soup�ons
J�r�me Cahuzac�
Cr�dit Reuters
D'apr�s une source judiciaire cit�e par l'AFP, l'ancien ministre du budget, J�r�me Cahuzac, serait soup�onn� d'avoir dissimul� certains de ses revenus � l'administration fiscale en les transf�rant sur les comptes bancaires de sa m�re. L'ancien ministre �tait d�j� poursuivi pour l'existence d'un compte cach� et Suisse puis � Singapour.
Les premiers soup�ons contre J�r�me Cahuzac avaient �t� d�clench�s apr�s un signalement de Tracfin, la cellule antiblanchiment de Bercy, � propos de nombreux ch�ques enregistr�s sur des comptes de la m�re de l'ancien ministre entre 2003 et 2010.
D'apr�s Bercy, ces ch�ques pouvaient correspondre aux b�n�fices du cabinet d'implants capillaires de J�r�me et Patricia Cahuzac.
Une autre source cit�e par l'AFP assure que l'ancien ministre n'a pas contest� ces faits, mais que la m�re de J�r�me Cahuzac affirme ignorer l'existence de ces mouvements sur ses comptes bancaires.�
Selon l'AFP, le parquet national financier (PNF), estime que 210 000 euros ont ainsi �t� transf�r�s sur des comptes appartenant � la m�re de J�r�me Cahuzac entre 2003 et 2010, et ce � l'insu de la principale int�ress�e.
Apr�s plusieurs semaines de tourmente m�diatique, J�r�me Cahuzac avait finalement reconnu en avril 2013 l'ouverture d'un compte en Suisse � la banque UBS en 1992. La somme de 600 000 euros vers�e sur ce compte avait �t� transf�r�e sur un autre compte � Singapour avant de finalement revenir en France quand l'affaire a �t� rendue publique.�
Lire ou relire plus tard
Pour classer cet article et le retrouver dans votre compte :
Besoin de vous concentrer
Saisissez votre pseudonyme ou votre email pour Atlantico.
Mot de passe *
Lire ou relire plus tard
Pour classer cet article et le retrouver dans votre compte :
Besoin de vous concentrer
Saisissez votre pseudonyme ou votre email pour Atlantico.
Mot de passe *
Commentaires
Nos articles sont ouverts aux commentaires sur une p�riode de 7 jours.
Face � certains abus et d�rives, nous vous rappelons que cet espace a vocation � partager vos avis sur nos contenus et � d�battre mais en aucun cas � prof�rer des propos calomnieux, violents ou injurieux. Nous vous rappelons �galement que nous mod�rons ces commentaires et que nous pouvons �tre amen�s � bloquer les comptes qui contreviendraient de fa�on r�currente � nos conditions d'utilisation.
Par
- 26/01/2015 - 19:55 - Signaler un abus Madame M�re ....Cahuzac
Voil� qui est bien vilain. Etait elle consentante?
Par
Texas
- 26/01/2015 - 19:55 - Signaler un abus Une m�thode..
...utilis�e par Mr Papandreou ( Premier Ministre Socialiste Grec )dont la m�re cumulait 550 Millions d' Euros sur un compte Suisse . Il va falloir �tendre les d�clarations d' actifs des hommes politiques aux mamans et belle-mamans ...!
Par
- 26/01/2015 - 20:39 - Signaler un abus Le revoil�
Cahuzac, �a faisait bien longtemps. Pourquoi n'est-il pas en prison?
Par
- 26/01/2015 - 23:38 - Signaler un abus Le bon socialo...g�n�reux avec sa vieille m�re...
Il ne pourra plus la regarder les yeux dans les yeux!
Pour commenter :
Depuis son lancement Atlantico avait fait le choix de laisser ouvert � tous la possibilit� de commenter ses articles avec un syst�me de mod�ration a posteriori. Sous couvert d'anonymat, une minorit� d'internautes a trop souvent d�tourn� l�esprit constructif et respectueux de cet espace d��changes. Suite aux nombreuses remarques de nos lecteurs, nous avons d�cid� de r�server les commentaires � notre communaut� d�abonn�s.
Cambad�lis (PS) croit � un "plafond de verre" qui arr�tera le FN
09h40
La Salton Sea, bombe � retardement �cologique dans le d�sert de Californie
09h40
�ducation : vers une meilleure exploitation des "intelligences multiples"
09h40
Y�men: les forces hostiles au pr�sident s'emparent d'une base proche d'Aden
09h40
Crash A320: l'hypoth�se terroriste "pas privil�gi�e" selon Cazeneuve
09h40
Crash A320: la bo�te noire "est arriv�e � Paris"
09h36
Herm�s offre un dividende exceptionnel apr�s un solide exercice
09h36
Le fonds 3G Capital n�gocie le rachat de Kraft Foods
09h36
ACCOR perd 3,8% apr�s les cessions d'EURAZEO et Colony
09h36
Kenya : un restaurant chinois ferm� apr�s avoir interdit l�acc�s aux noirs le soir
09h35
Le climat des affaires s'am�liore en mars
09h35
Le directeur d'�cole is�rois reconna�t avoir viol� ses �l�ves
09h33
Crash A320: la piste terroriste "pas privil�gi�e" selon Royal
09h33
H�catombe de thons dans un populaire aquarium de Tokyo
09h33
Y�men: le pr�sident Hadi exhorte l'ONU � faire stopper l'avanc�e des Houthis sur Aden
Toutes les d�p�ches
Je quitte la S�cu Partie 1 : Un parcours du combattant
Laurent C
Je quitte la S�cu  Partie 2 : Pourquoi c'est possible
Laurent C
Comment r�v�ler votre charisme Partie 2 : Votre corps parle pour vous : Apprenez � le ma�triser en toutes circonstances
Chilina Hills
Je cr�e mon entreprise � l'�tranger Les cl�s du succ�s
Ingrid Labuzan
Devenir un pro de la n�gociation Partie 2 : Prendre le contr�le �motionnel de la n�gociation
Alexis Kyprianou
La France handicap�e du calcul Vaincre l'innum�risme pour sortir du ch�mage
Michel Vigier
L'id�e qui tue ! Comment vendre une id�e r�volutionnaire
Nicolas Bordas
Culturellement incorrect : De l'ill�gitimit� de l'intervention publique en mati�re culturelle
Bertrand Allamel
Comment r�v�ler votre charisme Partie 1 : Ce que vous gagnerez en le cultivant, et pas seulement si vous recherchez la c�l�brit�
Chilina Hills
Sortir l'Europe de la crise : le mod�le japonais
Nicolas Goetzmann
