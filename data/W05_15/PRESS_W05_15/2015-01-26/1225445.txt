TITRE: Mondial de Handball 2015 : Huiti�mes de finale France-Argentine ce soir | melty.fr
DATE: 26-01-2015
URL: http://www.melty.fr/mondial-de-handball-2015-huitiemes-de-finale-france-argentine-ce-soir-a374873.html
PRINCIPAL: 1225442
TEXT:
Mondial de Handball 2015 : Huiti�mes de finale France-Argentine ce soir
3
Publi� il y a 58 jours par Garbine1
Publi� le 26 janv. 2015 09:35:04
Les Bleus affrontent, ce soir, les Argentins en huiti�mes de finale du mondial de handball. Un adversaire, sur le papier, largement � la port�e des Bleus.
L��quipe de France a termin� premi�re du groupe C apr�s s��tre impos�e, non sans difficult�, face � la Su�de (27-25) samedi soir. Un succ�s qui lui permet d�h�ritier du quatri�me du groupe D soit l�Argentine. En gagnant cette ultime rencontre de poules, Nikola Karabatic, pi�ce ma�tresse des Bleus , et ses co�quipiers ont ainsi �vit� le Danemark, vice-champion d�Europe en titre. Toutefois, ils devront se m�fier de l��quipe de Diego Simonet, co�quipier notamment de Micha�l Guigou et Mathieu Gr�bille � Montpellier, qui a montr� un beau visage lors des matches de poules. L'Argentine, nation montante dans le jeu � sept, s�est offert le luxe d��liminer les Russes (30-27) lors de son dernier match de poules samedi soir. Les Sud-am�ricains atteignent ce stade de la comp�tition pour la quatri�me fois en dix participations. Un adversaire qui r�ussit plut�t bien � l��quipe de France � en croire l�ailier gauche Micha�l Guigou.
Les Bleus se doivent de leur porter pour poursuivre leur aventure au Qatar
M�me si l�Argentine appara�t comme un adversaire largement � la port�e des Bleus, les hommes de Claude Onesta ne devront pas prendre cette rencontre � la l�g�re. Le dernier match de poules contre la Su�de devait leur permettre de se rassurer. Il n'en a rien �t�, en tout cas lors des 50 premi�res minutes de jeu. Apr�s avoir �t� men�s pendant presque toute la rencontre, les Tricolores ont su se serrer les coudes et faire parler leur classe pour finalement faire basculer le match de leur c�t�. On reconnait bien l� la marque de fabrique des meilleurs joueurs mais il ne faudra pas jouer avec le feu � chaque fois. Thierry Omeyer qui a encore r�alis� la bonne parade au moment face aux Su�dois monte en gamme au fil des rencontres. Il devrait donc ne pas laisser passer beaucoup de ballons ce soir. La malchance laissera peut-�tre tranquille Daniel Narcisse en manque d�efficacit� incroyable face � la Su�de. En tout cas l��quipe de France le sait elle a d�sormais pas le droit � l�erreur. Une d�faite serait synonyme d��limination et de retour pr�cipit� � la maison ! Les Bleus vont-ils faire qu'une bouch�e des Argentins ?
Cr�dit photos / vid�os : Pixsell/abaca
