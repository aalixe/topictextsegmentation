TITRE: Centrafrique: un ministre et ex-chef rebelle enlev� a Bangui
DATE: 26-01-2015
URL: http://www.romandie.com/news/Centrafrique-un-ministre-et-exchef-rebelle-enleve-a-Bangui_RP/558660.rom
PRINCIPAL: 0
TEXT:
Tweet
Centrafrique: un ministre et ex-chef rebelle enlev� a Bangui
Bangui - Le ministre centrafricain de la Jeunesse et des Sports, l'ex-chef rebelle Armel Ningatoloum Sayo, a �t� enlev� dimanche par des hommes arm�s, a-t-on appris aupr�s de son �pouse qui a assist� au kidnapping.
C'est la premi�re fois qu'un membre du gouvernement en fonction est kidnapp� en Centrafrique.
Le rapt n'a pas �t� revendiqu�. Mais depuis quelques jours, les enl�vements se multiplient � Bangui. Une humanitaire fran�aise, Claudia Priest, a �t� kidnapp�e lundi dernier par des miliciens chr�tiens anti-balaka et lib�r�e vendredi. Elle avait �t� enlev�e en compagnie d'un Centrafricain travaillant pour la m�me ONG, et lui aussi lib�r�.
Une employ�e expatri�e de la Minusca (Mission des Nations unies en Centrafrique) avait �t� kidnapp�e mardi et lib�r�e apr�s avoir �t� retenue quelques heures par des anti-balaka.
Ces milices, principalement chr�tiennes, se sont form�es pour lutter contre les rebelles, essentiellement musulmans, de la coalition S�l�ka qui avait pris le pouvoir en Centrafrique en mars 2013 avant d'en �tre chass�e en janvier 2014. Les deux camps sont accus�s d'avoir commis de graves exactions.
Mon mari (a �t�) effectivement enlev� au quartier Galabadja dans le 8e arrondissement (nord de Bangui). C'�tait ce matin aux environs de 09H00 (08H00 GMT) alors qu'il se trouvait au volant de sa voiture de commandement. Nous �tions trois dans le v�hicule en train de rentrer de l'�glise. Les ravisseurs sont arriv�s � bord d'un taxi non num�rot�, ni immatricul�, a racont� � l'AFP son �pouse Nicaise Danielle Sayo.
Selon une source proche de la famille, le ministre et son �pouse rentraient du culte � l'�glise protestante de Galabadja, quand deux v�hicules ont fonc� sur sa voiture et bloqu� sa progression.
Quatre hommes arm�s �taient � bord. Trois sont sortis et ont tir� des coups de feu en l'air. Le ministre leur a demand� +Quel est le probl�me?+ Mais ils lui ont intim� l'ordre de monter � bord du taxi. Il est donc mont�, ils ont fouill� la voiture et pris le sac de son �pouse. Puis ils sont partis en direction du 4e arrondissement (quartier de Boy-Rabe), a expliqu� ce proche du ministre.
Armel Ningatoloum Sayo �tait le chef de la r�bellion Mouvement R�volution Justice bas�e dans le nord-ouest du pays.
Il est entr� au gouvernement du Premier ministre Mahamat Kamoun, � la suite de la signature de l'accord de cessation des hostilit�s du 23 juillet 2014 � Brazzaville, au Congo.
(�AFP / 25 janvier 2015 16h23)
�
