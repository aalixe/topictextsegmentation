TITRE: Open d�Australie : les Fran�ais(es) d�j� en d�route - Lib�ration
DATE: 26-01-2015
URL: http://www.liberation.fr/sports/2015/01/25/open-d-australie-les-francaises-deja-en-deroute_1188576
PRINCIPAL: 0
TEXT:
Lire sur le readerMode zen
Trois matchs gagn�s : tel��tait le seuil de comp�tence des joueurs et joueuses de tennis fran�ais � l�Open d�Australie, premi�re lev�e du Grand Chelem de la saison. C�t� filles, Aliz�e Cornet (5-7, 2-6 contre la Slovaque Dominika Cibulkov�) a�ferm� le ban samedi, quelques heures avant que Gilles Simon (photo) rende les armes apr�s, et, il faut lui en �tre gr�, vu le contexte, un match �pique (2-6, 5-7, 7-5, 6-7 [4]) contre l�Espagnol David Ferrer, qui est plus fort que le Ni�ois. Une tendance lourde : d�j� en�2011, aucun Fran�ais n�avait atteint la seconde semaine et ces derni�res ann�es, c�est souvent Jo-Wilfried Tsonga (absent � Melbourne) qui avait cach� la mis�re. Photo AFP
Recevoir la newsletter
