TITRE: Y�men: des miliciens chiites tirent en l'air pour disperser des manifestants � Sanaa
DATE: 26-01-2015
URL: http://www.romandie.com/news/Yemen-des-miliciens-chiites-tirent-en-lair-pour-disperser-des/558576.rom
PRINCIPAL: 0
TEXT:
Tweet
Y�men: des miliciens chiites tirent en l'air pour disperser des manifestants � Sanaa
Sanaa - Des miliciens chiites ont tir� en l'air pour disperser dimanche un d�but de manifestation hostile devant l'Universit� de Sanaa, au lendemain d'une grande marche de protestation contre leur pr�sence dans la capitale, ont rapport� des t�moins.
Certains t�moins ont �voqu� des bless�s parmi les manifestants, d'autres ont fait �tat de plusieurs arrestations op�r�es par ces miliciens chiites, appel�s Houthis. Aucun bilan pr�cis de ces incidents n'a pu �tre obtenu imm�diatement.
Les miliciens s'en sont pris �galement � des journalistes, les emp�chant de filmer la fuite de dizaines de manifestants, selon des t�moins.
Ils ont renforc� leur pr�sence arm�e dans le secteur de l'Universit� de Sanaa pour emp�cher toute nouvelle tentative de rassemblement, de m�me source.
La prise par ces miliciens de positions-cl�s dans la capitale -o� ils sont entr�s le 21 septembre- ont conduit le pr�sident Abd Rabbo Mansour Hadi � d�missionner cette semaine.
Samedi, des Houthis avaient d�j� tent� d'emp�cher une marche � Sanaa, avant de renoncer devant le nombre important de participants � cette manifestation qui a �t� d�crite comme la plus imposante depuis leur entr�e dans la capitale.
En outre, une r�union du Parlement pr�vue dimanche, apr�s la d�mission du gouvernement et du pr�sident Hadi, a de nouveau �t� report�e � une date non pr�cis�e, selon l'agence officielle Saba. Cette r�union avait dans un premier temps �t� annonc�e pour vendredi.
M. Hadi, 69 ans, au pouvoir depuis 2012, a jet� l'�ponge jeudi soir apr�s avoir constat� que le Y�men �tait dans une impasse totale suite � la prise du palais pr�sidentiel par les Houthis qui ont renforc� leur emprise sur Sanaa.
(�AFP / 25 janvier 2015 09h05)
�
