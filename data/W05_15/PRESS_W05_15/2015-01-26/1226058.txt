TITRE: LORACTU.fr - VIDEO. Odile Vuillemin dans le r�le d'une femme battue sur TF1
DATE: 26-01-2015
URL: http://www.loractu.fr/metz/9135-video-odile-vuillemin-dans-le-role-d-une-femme-battue-sur-tf1.html
PRINCIPAL: 1226053
TEXT:
La R�daction
Culture
TF1 diffuse, ce soir � 20 h 55, L'Emprise. Une histoire vraie d'une femme battue pendant 14 ans qui se retrouve aux assises pour le meurtre de son mari. L'actrice qui incarne Alexandra Lange se nomme Odile Vuillemin, elle  a pass� son enfance et son adolescence � Metz (Moselle). PHOTO : TF1
TF1 diffuse, ce soir � 20 h 55, L'Emprise. Une histoire vraie d'une femme battue pendant 14 ans qui se retrouve aux assises pour le meurtre de son (...)
TF1 diffuse, ce soir � 20 h 55, L�Emprise. Une histoire vraie d�une femme battue pendant 14 ans qui se retrouve aux assises pour le meurtre de son mari. L�actrice qui incarne Alexandra Lange se nomme Odile Vuillemin, elle �a pass� son enfance et son adolescence � Metz (Moselle)
�
Une m�re de famille de quatre enfants, s�est retrouv�e en mars 2012 dans le box des accus�s des assises de Douai, pour le meurtre de son mari apr�s avoir souffert pendant quatorze ans de ses coups.
�
Comment une femme va jusqu�� tuer le p�re de ses enfants d�un coup de couteau ? Le temps des trois jours du proc�s, ce r�cit haletant nous plonge dans le quotidien d�Alexandra, une m�re que la soci�t� n�a pas su prot�ger, ni elle, ni ses enfants, de l�emprise d�un monstre qu�elle a pourtant aim�.
�
"J'ai jou� ce r�le avec mes tripes"�
�
Dans Metro News, la messine est revenue sur les raisons pour lesquelles elle a accept� ce r�le : �On m�a propos� ce sc�nario, tr�s joliment �crit. C�est une histoire bouleversante, un r�le bouleversant... J�ai pens� que je ne pourrais jamais faire �a et puis je me suis dit que, si on me l�avait propos�, c�est que je devais en �tre capable. Mais je ne pouvais pas le faire � moiti�. Le sujet est trop grave pour �tre trait� � la l�g�re.�
�
�J�ai rencontr� Alexandra Lange pour la sentir physiquement, pour l�interpr�ter au plus juste. J�ai lu son livre [Acquitt�e, ndlr] ainsi que des livres sur la psychologie. J�ai fait aussi un gros travail de pr�paration physique pour apprendre � donner des coups et � en recevoir. Apr�s, c�est l�instinct qui parle��
�
A travers ce fait divers qui a d�fray� la chronique et boulevers� l�opinion publique, il s�agit d�un t�l�film qui pose la question de la balance de la justice. Sera-t-elle entendue ? R�ponse ce soir � 20h55 sur TF1.
�
