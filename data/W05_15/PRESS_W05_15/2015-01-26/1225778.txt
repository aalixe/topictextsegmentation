TITRE: Routiers en col�re. Ils l�vent une partie des barrages
DATE: 26-01-2015
URL: http://www.ouest-france.fr/routiers-en-colere-ils-levent-une-partie-des-barrages-3144529
PRINCIPAL: 1225777
TEXT:
Routiers en col�re. Ils l�vent une partie des barrages
Nantes -
Les routiers en col�re ont bloqu� la zone industrielle de l'a�roport Nantes-Atlantique, une partie de la journ�e�|�Franck Dubray
Facebook
Achetez votre journal num�rique
Les routiers ont lib�r� vers 16 h 30 l'acc�s � la zone industrielle de l'a�roport Nantes-Atlantique. Partiellement.
� 16 h 30, les routiers en col�re qui bloquaient depuis l'aube l'acc�s des camions � la zone industrielle de l'a�roport Nantes-Atlantique, ont lev� les barrages. Mais pas tous. "Nous restons cette nuit au niveau de la soci�t� Baudron, filiale du groupe Charles Andr� qui refuse de n�gocier",�affirment les manifestants.
Le blocage impacte �galement la centrale d'achats de Syst�me U.
L'intersyndicale des routiers (CGT, FO, CFTC et CGC) a d�cid� de poursuivre le mouvement de protestation entam� la semaine derni�re. "Les n�gociations nationales n'aboutissent pas". Les syndicats continuent d'exiger une hausse des salaires de 5�%, les organisations patronales proposent entre 1 et 2�%.
Tags :
