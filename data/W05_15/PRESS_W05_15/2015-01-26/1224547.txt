TITRE: LEAD 1-Allemagne/Ifo-Le climat des affaires � un pic de six mois- 26 janvier 2015 - Challenges.fr
DATE: 26-01-2015
URL: http://www.challenges.fr/finance-et-marche/20150126.REU7527/lead-1-allemagne-ifo-le-climat-des-affaires-a-un-pic-de-six-mois.html
PRINCIPAL: 0
TEXT:
Challenges �>� Finance et march� �>�LEAD 1-Allemagne/Ifo-Le climat des affaires � un pic de six mois
LEAD 1-Allemagne/Ifo-Le climat des affaires � un pic de six mois
Publi� le� 26-01-2015 � 11h47
(Actualis� avec pr�cisions, contexte, commentaires)
par Michelle Martin
BERLIN, 26 janvier (Reuters) - Le climat des affaires en Allemagne , en hausse pour le troisi�me mois d'affil�e, a atteint en janvier son plus haut niveau en six mois, selon l'enqu�te mensuelle de l'institut Ifo publi�e lundi.
Il confirme ainsi un bon d�marrage de d'ann�e pour la premi�re �conomie d' Europe , qui profite de la baisse des cours du p�trole et de taux de change favorables.
L'indice Ifo, calcul� sur la base d'une enqu�te aupr�s d'environ 7.000 entreprises, est ressorti � 106,7, au plus haut depuis juillet, contre 105,5 en d�cembre. Les �conomistes interrog�s par Reuters pr�voyaient en moyenne un chiffre de 106,3.
"Globalement presque tout est l� en vue d'une nouvelle ann�e vigoureuse pour l'�conomie allemande. En tout cas dans un monde parfait et lin�aire", dit l'�conomiste d'ING Carsten Breszki.
"Toutefois, les retomb�es des �lections grecques vont nous prouver que ces mondes parfaits et lin�aires n'existent pas."
Alexis Tsipras, le leader du parti de gauche radicale Syriza, grand vainqueur des �lections l�gislatives anticip�es, a promis dimanche soir de mettre fin � cinq ann�es d'aust�rit�, "d'humiliation et de souffrance" impos�es par les cr�anciers internationaux de la Gr�ce.
Les dirigeants politiques allemands ont appel� la Gr�ce � respecter ses engagements et le pr�sident de la Bundesbank Jens Weidmann a soulign� dimanche qu'il �tait �galement dans l'int�r�t du gouvernement grec d'appliquer les r�formes.
L'enqu�te Ifo montre que les entreprises ont une vision plus positive de la situation actuelle par rapport au mois de d�cembre et qu'elle sont �galement devenues l�g�rement plus optimistes concernant les perspectives des six prochains mois.
Le sous-indice mesurant le sentiment sur la situation actuelle s'est am�lior�, � 111,7 contre 109,8 (r�vis� de 110,0), d�passant le consensus qui �tait � 110,7. Celui sur les perspectives, en l�g�re hausse � 102,0 contre 101,3 (r�vis� de 101,1), est toutefois ressorti inf�rieur aux attentes (102,5).
Klaus Wohlrabe, �conomiste d'Ifo, a pr�cis� que l'industrie allemande b�n�ficiait de la faiblesse de l'euro et des cours du p�trole et il a dit ne pas voir de "nuage noir" � l'horizon.
Il ajout� que l'impact du vaste plan de rachats d'actifs annonc� la semaine derni�re par la Banque centrale europ�enne ( BCE ) pour tenter de doper la croissance et pr�venir tout risque de d�flation ne se refl�tait pas encore dans l'enqu�te Ifo.
Certaines entreprises ont d�j� r�cemment fait des annonces positives, notamment l'�quipementier sportif Adidas et Beiersdorf, propri�taire de la marque Nivea, qui ont tous deux pu publi� des chiffres d'affaires meilleurs que pr�vu sur 2014.
L'enqu�te Ifo montre une am�lioration sur presque tous les segments de l'�conomie - industrie, services et commerce - � l'exception du BTP qui conna�t une l�g�re d�t�rioration.
Les indicateurs publi�s la semaine derni�res ont d�j� montr� une am�lioration du moral des investisseurs et une acc�l�ration de la croissance dans le secteur priv� dans le pays.
L'�conomie allemande a enregistr� une croissance de 1,5% l'an dernier, tir�e par la consommation priv�e et le commerce ext�rieur et, selon des sources gouvernementales, Berlin pourrait revoir en hausse sa pr�vision pour 2015, de 1,3 � 1,5%.
Tableau de l'enqu�te
Les indicateurs allemands en temps r�el    (Michelle Martin, V�ronique Tison pour le service fran�ais)
Partager
