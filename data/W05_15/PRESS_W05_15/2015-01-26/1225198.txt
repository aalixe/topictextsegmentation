TITRE: Open d'Australie: Novak Djokovic en quarts de finale - LExpress.fr
DATE: 26-01-2015
URL: http://www.lexpress.fr/actualites/1/sport/open-d-australie-novak-djokovic-en-quarts-de-finale_1644718.html
PRINCIPAL: 1225196
TEXT:
Zoom moins
Zoom plus
Le Serbe Novak Djokovic c�l�bre sa qualification pour les quarts de finale de l'Open d'Australie apr�s sa victoire face au Luxembourgeois Gilles Muller, le 26 janvier 2015 � Melbourne
afp.com/Paul Crock
Ce sera le vingt-troisi�me quart de finale d'affil�e en Grand Chelem pour Le Serbe, quatre fois titr� en Australie (en 2008 et de 2011 � 2013).�
Djokovic affrontera au prochain tour le Canadien Milos Raonic, t�te de s�rie N.8.�
Par
