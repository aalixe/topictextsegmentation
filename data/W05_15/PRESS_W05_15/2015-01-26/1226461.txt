TITRE: Les cabines de bronzage d�nonc�es aussi par les dermatologues !
DATE: 26-01-2015
URL: http://www.lesnewseco.fr/les-cabines-de-bronzage-denoncees-aussi-par-les-dermatologues-08513.html
PRINCIPAL: 1226453
TEXT:
Les cabines de bronzage d�nonc�es aussi par les dermatologues !
Cl�ment Pessaux
Lutte contre le tabac : La consommation de tabac a�
Une voix de plus qui s��l�ve contre les cabines de bronzage, c�est maintenant au tour du Syndicat national des dermatologues (SNDV) qui a demand� lundi aux pouvoirs publics de lancer des campagnes de pr�vention contre les dangers des cabines de bronzage et de prendre des mesures pouvant aller jusqu�� leur interdiction � moyen terme.
��Le Syndicat national des dermatologues attend des pouvoirs publics des mesures efficaces telles que celles prises par exemple au Br�sil ou plus r�cemment en Australie avec l�interdiction des UV artificiels�� indique le SNDV dans un communiqu�.
Lire aussi : Cancer de la peau: les cabines � UV dans le collimateur du gouvernement
Le communiqu� fait suite � la publication la semaine derni�re d�un article de la revue 60 millions de consommateurs reprochant aux professionnels du secteur de ne pas respecter la r�glementation existante, ��alors que les risques li�s aux UV artificiels sont av�r�s��, selon le SNDV.
Le syndicat demande �galement � ce que des campagnes de pr�vention soient mises en place ��pour lutter contre le risque sanitaire que repr�sentent les UV naturels ou artificiels, sources de cancers cutan�s et notamment du m�lanome, dont les cons�quences m�dicales sont co�teuses en mati�re de sant� publique��.
En attendant le syndicat recommande � la population ��de ne pas avoir recours aux pratiques addictives des UV, notamment artificiels��. Selon l�Institut de veille sanitaire (InVS), jusqu�� 350 cas de m�lanome et 76 d�c�s pourraient �tre attribu�s chaque ann�e aux cabines de bronzage.
Commentaires
