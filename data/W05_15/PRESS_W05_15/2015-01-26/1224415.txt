TITRE: Top Chef 2015 (M6) : 5 raisons de d�guster le nouveau Top Chef ! - News T�l� 7 Jours
DATE: 26-01-2015
URL: http://www.programme-television.org/news-tv/Top-Chef-M6-5-raisons-de-deguster-le-nouveau-Top-Chef-4120397
PRINCIPAL: 0
TEXT:
Top Chef 2015 (M6) : 5 raisons de d�guster le nouveau Top Chef !
Top Chef 2015 (M6) : 5 raisons de d�guster le nouveau Top Chef !
26/01/2015 - 09h44
Partager :
Pour sa sixi�me �dition, Top Chef fait le plein de nouveaut�s. Du jury aux �preuves, le changement, c�est maintenant ! Passage en revue des ingr�dients qui vont pimenter cette �dition.
� PIERRE OLIVIER/M6
Un jury sucr�-sal�
Si Top Chef 2015 compte toujours parmi ses jur�s Jean-Fran�ois Pi�ge, trois autres chefs prennent la suite de Ghislaine Arabian, Thierry Marx et Christian Constant. Leurs successeurs sont H�l�ne Darroze, Philippe Etchebest et Michel Sarran. Si on devait les qualifier en un mot, ce serait � maternel � pour la premi�re, � rigueur � pour le deuxi�me et � sensibilit� � pour le dernier. � eux quatre, ils cumulent huit �toiles au Guide Michelin !
Des candidats � point
Looks loufoques ou parcours atypiques, parmi les quinze pr�tendants, certains devraient marquer les esprits. Comme Olivier Streiff, un virtuose de la cuisine qui compte d�j� deux Toques au Gault & Millau. Un dandy citant Oscar Wilde et maquillant ses yeux de noir. Dans son ancien restaurant, Nicolas Pourcheresse, lui, comptait d�j� trois Toques au Gault & Millau. Avec sa crini�re de feu, celui qui fut le plus jeune �toil� de France en 2005, a tout plaqu� pour barouder aux quatre coins du monde. Quant aux femmes (elles ne sont que trois cette saison), elles ne se laisseront pas faire. � l�image de Vanessa Robuschi, 32 ans, qui �l�ve seule sa fille et g�re son restaurant � Marseille. Ou encore Fatimata Amadou, 21 ans, la plus jeune femme du concours. Stagiaire � l�H�tel de Crillon, elle aimerait ouvrir son restaurant.
La cuisine en vedette
Cette ann�e, chaque prime time durera 110 mn contre 150 la saison derni�re. Selon Matthieu Bayle, directeur des programmes de Studio 89, il s�agit d��viter de lasser le t�l�spectateur et de revenir � certains fondamentaux : � Nous nous recentrons sur la cuisine en tant que telle. Termin�es les �preuves en haut d�une nacelle. Idem pour les d�fis lanc�s aux cuisiniers alors qu�ils sont d�j� en pleine �preuve. Nous avons eu des retours des candidats
des saisons pr�c�dentes et tenu compte de l�avis du public. On a rectifi� le tir. � Par ailleurs, si l��preuve avec les proches des candidats est maintenue, exit la venue de c�l�brit�s.
Des d�fis savoureux
Les jur�s seront davantage pr�sents aux c�t�s des candidats qu�� l�accoutum�e. Le but ? �tre plus dans le conseil, le coup de main et la proposition. Si certains cuisiniers font fausse route, ils seront
gentiment invit�s � se remettre en question avant la fin du chrono. Et de nouvelles �preuves permettront aux participants d�affronter les chefs ! Ne loupez pas le d�fi relev� par six d�entre eux, qui ont d� r�aliser un dessert avec pour adversaire Philippe Etchebest : un r�gal !
Un chef amateur croustillant
Grand vainqueur d�Objectif Top Chef diffus� � la fin de l�ann�e,
Xavier Koenig participe � cette sixi�me �dition. Du haut de ses 19 ans, le Meilleur Apprenti de France 2012 est pr�t � en d�coudre avec ses petits camarades. D�abord peu m�fiants, ses adversaires vont rapidement se raviser et se m�fier de ce jeune prodige de la cuisine qui travaille d�ores et d�j� dans un restaurant �toil� en Alsace. Dot� d�un solide mental et passionn� de concours, il pourrait bien �tre la r�v�lation de cette saison.
Adeline Quittot
Les derni�res news t�l�
