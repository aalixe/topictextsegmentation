TITRE: Egypte: 516 arrestations apr�s les heurts de dimanche - DH.be
DATE: 26-01-2015
URL: http://www.dhnet.be/dernieres-depeches/afp/egypte-516-arrestations-apres-les-heurts-de-dimanche-54c69ccd3570af82d516607e
PRINCIPAL: 1226657
TEXT:
Vous �tes ici: Accueil > Derni�res d�p�ches
Egypte: 516 arrestations apr�s les heurts de dimanche
Publi� le
26 janvier 2015 � 20h59
Le Caire (AFP)
Les autorit�s �gyptiennes ont annonc� lundi avoir arr�t� plus de 500 islamistes lors des heurts meurtriers la veille entre police et manifestants rassembl�s � l'occasion de l'anniversaire de la r�volte de 2011.
Ces violences, les plus meurtri�res depuis des mois, ont fait 20 morts et �clat� lors de rassemblements organis�es � l'appel de manifestants islamistes contre le pr�sident Abdel Fattah al-Sissi, quatre ans jour pour jour apr�s le d�but du soul�vement qui chassa Hosni Moubarak du pouvoir.
Les partisans du successeur de Moubarak, l'islamiste Mohamed Morsi, affrontent r�guli�rement les forces de s�curit� depuis la destitution en juillet 2013 de M. Morsi, premier pr�sident librement �lu d'Egypte, par M. Sissi, qui �tait alors chef de l'arm�e.
La confr�rie des Fr�res musulmans dont est issu M. Morsi a �t� interdite et f�rocement r�prim�e, mais la vague d'arrestations de dimanche est la plus importante depuis l'accession au pouvoir de M. Sissi en mai.
"Hier (dimanche), nous avons arr�t� 516 individus li�s aux Fr�res musulmans, soup�onn�s d'avoir tir� des munitions, pos� des bombes et fait exploser certains locaux", a d�clar� le ministre de l'Int�rieur Mohamed Ibrahim devant la presse au Caire.
Selon M. Ibrahim, 20 personnes, dont deux policiers, ont �t� tu�es dans les affrontements ayant �clat� entre des manifestants majoritairement islamistes et les forces de s�curit�.
La plupart des victimes ont trouv� la mort dans un quartier du nord du Caire, Matareya, o� les heurts ont dur� plus de douze heures, a-t-il pr�cis�.
Le minist�re de la Sant� a de son c�t� fait �tat lundi d'un bilan de 20 morts, dont un policier.
- 'La police tue toujours' -
Des organisations de d�fense des droits de l'Homme, telles que Human Rights Watch, ont d�nonc� � maintes reprises "l'usage excessif de la force" par la police en Egypte "contre des manifestations pacifiques".
"Quatre ans apr�s la r�volution, la police tue toujours r�guli�rement des manifestants", a d�nonc� dans un communiqu� la directrice pour le Moyen-Orient de HRW, Sarah Leah Whiston.
Les abus de la police �taient d�j� l'une des causes principales du soul�vement populaire qui a chass� le pr�sident Hosni Moubarak du pouvoir il y a quatre ans.
Samedi, Sha�maa al-Sabbagh, une manifestante de 34 ans d'un parti la�c de gauche, avait �t� tu�e par un tir de chevrotine lors de heurts avec la police, durant un rassemblement comm�morant la r�volte de 2011.
Des manifestants assurent que la jeune femme a �t� tu�e par la police, ce que le minist�re de l'Int�rieur nie. Le parquet a ouvert une enqu�te.
Le ministre de l'Int�rieur a accus� Human Rights Watch de n'�tre "jamais objective dans ses rapports", accusant les Fr�res musulmans d'�tre responsables des violences de dimanche.
Le pr�sident Sissi est r�guli�rement accus� d'avoir instaur� un r�gime encore plus r�pressif que celui de M. Moubarak. Il jouit cependant d'une forte popularit� aupr�s d'une grande partie de l'opinion publique.
Mais des groupes jihadistes, disant agir en repr�sailles � la sanglante r�pression qui s'est abattue sur les pro-Morsi, ont multipli� les attentats.
Lundi, Ansar Be�t al-Maqdess, le principal groupe jihadiste �gyptien qui a pr�t� all�geance � l'organisation Etat islamique (EI), a revendiqu� la mort d'un officier de police, enlev� dans le nord de la p�ninsule du Sina� et retrouv� abattu d'une balle dans la t�te.
Et trois insurg�s pr�sum�s ont �t� tu�s par l'explosion accidentelle d'engins explosifs qu'ils tentaient de poser dans la r�gion du delta du Nil.
Symbole de l'ancien pouvoir, les deux fils de l'ancien pr�sident �gyptien Hosni Moubarak, qui doivent �tre rejug�s dans une affaire de corruption, ont �t� remis en libert�, a affirm� lundi le ministre de l'Int�rieur.
La lib�ration des deux fils, qui avaient atteint la p�riode maximale l�gale de d�tention provisoire, avait �t� annonc�e vendredi, mais des responsables des autorit�s carc�rales avaient ensuite indiqu� que leur remise en libert� avait �t� retard�e, pour ne pas exacerber la col�re de l'opposition � la veille de l'anniversaire de la r�volte.
� 2015 AFP. Tous droits de reproduction et de repr�sentation r�serv�s. Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par l'AFP. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, rediffus�e, traduite, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de l'AFP.
Derni�res d�p�ches
