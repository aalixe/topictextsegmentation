TITRE: Mill�sime Bio : Trois�jours pour�mesurer la�dynamique des�vins�bio - Actualit�s -  - La Vigne magazine - Viticulture et vin
DATE: 26-01-2015
URL: http://www.lavigne-mag.fr/actualites/millesime-bio-trois-jours-pour-mesurer-la-dynamique-des-vins-bio-99311.html
PRINCIPAL: 1225128
TEXT:
salon
Premier salon de l�ann�e, la 22e �dition de Mill�sime Bio ouvre ses portes lundi 26?janvier, au parc des expositions de Montpellier (H�rault). Forum de rencontres et d�affaires incontournable pour les vins bio, ce salon refl�te l��tat d�esprit de la fili�re.
Mill�sime Bio est le salon incontournable des vins bio. �G.LEFRANCQ
En 2015, le moral des op�rateurs en bio est � l'optimisme : �?Vin bio, une fili�re qui ne conna�t pas la crise?�, clame ainsi la conf�rence inaugurant le salon. En 2013, en effet, un million d�hectolitres de vin bio ont �t� commercialis�s pour 64 600 hectares de vignes, soit 8,2?% de la superficie nationale.
Les surfaces en conversion semblent avoir atteint un palier aujourd�hui et Patrick Guiraud, pr�sident de l�association interprofessionnelle SudVinBio (organisatrice du salon), rappelle que rien n�a �t� simple. �?Pendant dix ans, les surfaces en conversion �taient importantes, on a craint un d�s�quilibre avec les march�s. Mais on s�aper�oit que l�on est en p�nurie et que des consommateurs sollicitent les vins bio?�, affirme-t-il, persuad� que la prochaine phase de conversion massive sera d�clench�e par la demande.
�?UN ACTE MILITANT?�
Pour l�amplifier, la fili�re demande, via l�Agence Bio, une communication des pouvoirs publics sur les modalit�s de production des vins bio aupr�s du grand public. �?Consommer un produit labellis� bio est un acte militant pour toute la cha�ne de production et de transformation, avance Patrick Guiraud, que ce soit en termes de protection de l�environnement ou d�emploi?�.
En 2014, le salon avait accueilli 4 250 visiteurs professionnels (+2?% par rapport � 2013). Cette ann�e, son objectif est d�en accueillir plus de 4 500. Mill�sime Bio re�oit 800 stands de producteurs, dont 610 Fran�ais (repr�sentant 42?% des volumes bio nationaux). � noter que, d�s l�an prochain, SudVinBio souhaite accueillir une centaine de domaines suppl�mentaires, soit le nombre actuellement inscrit sur la liste d�attente.
Alexandre Abellan
