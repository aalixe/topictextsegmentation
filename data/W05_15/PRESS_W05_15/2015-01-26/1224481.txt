TITRE: Vers un accord de gouvernement entre Syriza et la droite souverainiste ?
DATE: 26-01-2015
URL: http://www.lemonde.fr/europe/article/2015/01/26/vers-un-accord-de-gouvernement-entre-syriza-et-la-droite-souverainiste_4563410_3214.html
PRINCIPAL: 1224480
TEXT:
Vers un accord de gouvernement entre Syriza et la droite souverainiste ?
Le Monde |
26.01.2015 � 15h18
L'essentiel
Syriza, qui conteste l'aust�rit� impos�e par l'UE, a remport� nettement les l�gislatives dimanche, avec 36,3�% des voix.
Le parti obtiendrait 149�si�ges au Parlement, � 2 si�ges de la majorit� absolue.
Son leader charismatique, Alexis Tsipras, qui a �t� officiellement nomm� premier ministre � 15 heures, devra mettre en place des alliances ou une coalition.
Syriza, le parti de la gauche radicale grecque qui a largement emport� les �lections l�gislatives, dimanche 25 janvier , devrait cependant manquer, � deux si�ges pr�s, la majorit� absolue (151 d�put�s). Il doit donc former un gouvernement de coalition et ce pourrait �tre avec l'appui du parti des Grecs ind�pendants, a annonc� lundi matin le chef de file de la petite formation de droite AN.EL.
Lire :� Gr�ce : quel est le rapport de force des partis politiques ?
Ce dernier, Panos Kammenos, 49�ans, s'est entretenu dans la matin�e avec le leader du parti de la gauche radicale Alexis Tsipras, qui a �t� officiellement nomm� premier ministre � 15�heures. ��Nous allons donner un vote de confiance au nouveau premier ministre Alexis Tsipras��, a-t-il pr�cis� ajoutant�: ��Le premier ministre verra dans la journ�e le pr�sident pour sa prestation de serment et annoncera la composition du gouvernement auquel les Grecs ind�pendants participeront��, a pr�cis� Kammenos.
Avec 13 �lus au Parlement, la formation de Kammenos issu d'une scission dans les rangs de Nouvelle d�mocratie, le parti du premier ministre sortant Antonis Samaras, lui servira d'appoint. Bien que clairement marqu� � droite et tr�s souverainiste, l'AN.EL., fond�e en f�vrier 2012, a objectivement aid�, au mois de d�cembre, Syriza � obtenir l'�lection l�gislative anticip�e qui l'a port� dimanche au pouvoir, en s'abstenant de voter pour le candidat � la pr�sidentielle pr�sent� par M. Samaras. Ce blocage avait entra�n� la dissolution du Parlement.
Les deux partis ont des points de vue divergents sur de nombreux sujets de soci�t� mais partagent un m�me rejet des conditions fix�es en contrepartie du plan d'aide financi�re accord� � la Gr�ce par ses partenaires europ�ens et le Fonds mon�taire international.
Une autre alliance �tait envisageable avec les pro-europ�ens de centre gauche du To Potami, cr�� en avril 2014 seulement. Apr�s seulement un mois d'existence, le parti pro-europ�en, qui veut repr�senter la voix du bon sens en Gr�ce, a r�ussi � faire entrer deux d�put�s (sur 21 pour l'ensemble du pays) au Parlement europ�en lors des �lections de mai. Dimanche, le parti talonnait la formation d'inspiration n�onazie Aube dor�e pour la troisi�me place, obtenant, comme elle, 17 si�ges.
Les Grecs ind�pendants et les�pro-europ�ens de centre gauche ont pr�venu qu'ils refuseraient de participer � un gouvernement dont l'autre fait partie.
