TITRE: Gr�ce: Victorieux, Alexis Tsipras annonce qu'il va �n�gocier� avec les cr�anciers du pays - 20minutes.fr
DATE: 26-01-2015
URL: http://www.20minutes.fr/monde/1525283-20150125-grece-victorieux-alexis-tsipras-annonce-va-negocier-creanciers-pays
PRINCIPAL: 0
TEXT:
Gr�ce
Alexis Tsipras, le dirigeant de la gauche grecque Syriza victorieuse aux l�gislatives de dimanche , a d�clar� vouloir �n�gocier� avec les cr�anciers du pays une �nouvelle solution viable qui b�n�ficie � tous�.
�Le nouveau gouvernement sera pr�t � collaborer et n�gocier pour la premi�re fois avec nos partenaires une solution juste, viable, durable, qui b�n�ficie � tous�, a d�clar� Tsipras, affirmant qu'il n'y aura pas �d'affrontement� et que la Gr�ce �d�cevra tous les Cassandre � l'int�rieur et l'ext�rieur du pays�, misant sur un �chec.
�Ni vainqueurs ni vaincus�
�La Gr�ce avance avec optimisme dans une Europe qui change�, a lanc� Alexis Tsipras devant une foule des milliers des personnes brandissant des drapeaux rouges. Selon Syriza, plus de 8.000 personnes se sont rassembl�s devant l'Universit� dans le centre d'Ath�nes pour assister au premier discours d'Alexis Tsipras. �Nous sommes conscients que les Grecs ne nous ont pas donn� un ch�que en blanc (...) nous avons devant nous une occasion importante pour la Gr�ce et l'Europe�, a-t-il dit.
Sur le plan des n�gociations cruciales avec les cr�anciers du pays, UE et FMI, le chef du Syriza a indiqu� que �le nouveau gouvernement grec serait pr�t � proc�der � un dialogue sinc�re� et � soumettre �un plan national et un plan sur la dette�. Parmi ses principaux points, le programme �conomique du Syriza comprend la fin des mesures d'aust�rit� et la ren�gociation de l'�norme dette publique du pays, � 175% du Produit int�rieur brut.
�Aujourd'hui, il n'y a ni vainqueurs ni vaincus. Notre priorit� est de faire face aux blessures de la crise, rendre justice, faire une rupture avec les oligarques, l'establishment et la corruption�, a affirm� Alexis Tsipras. La Gr�ce attend le d�blocage de la derni�re tranche de pr�ts accord�s au pays d'ici fin f�vrier mais � condition que le pays respecte ses engagements vis-�-vis ses cr�anciers sur la poursuite des r�formes. Depuis 2010, la tro�ka des cr�anciers s'est engag�e � accorder quelque 240 milliards d'euros de pr�ts � la Gr�ce.
