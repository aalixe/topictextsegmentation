TITRE: Ils voulaient appeler leur enfant Nutella: la justice refuse - SWI     swissinfo.ch
DATE: 26-01-2015
URL: http://www.swissinfo.ch/fre/toute-l-actu-en-bref/ils-voulaient-appeler-leur-enfant-nutella--la-justice-refuse/41238248
PRINCIPAL: 1226362
TEXT:
Imprimer Envoyez cet article
26. janvier 2015 - 20:29
"Nutella", "Fraise" : un tribunal du nord de la France a rejet� ces derni�res semaines l'attribution de ces pr�noms insolites � des nouveau-n�s, a-t-on appris lundi de source judiciaire. Les juges ont estim� que ces pr�noms portaient atteinte aux int�r�ts de l'enfant.
C'est un responsable de l'�tat-civil qui a alert� la justice lorsqu'un couple a voulu baptiser son enfant n� le 24 septembre du nom de la c�l�bre p�te � tartiner � la noisette et au cacao, a-t-on appris aupr�s du parquet de Valenciennes (nord).
Estimant que le pr�nom choisi n'�tait "pas conforme" � l'int�r�t de l'enfant, l'officier d'�tat-civil a saisi le procureur de la ville pour que le pr�nom soit supprim� des registres, a ajout� le parquet. Lors d'une audience,� laquelle les parents n'ont pas assist�, un juge a choisi de renommer l'enfant "Ella".
Une seconde affaire a provoqu� des remous lorsqu'un couple a dit vouloir pr�nommer sa petite fille "Fraise".
Devant l'avis contraire du m�me tribunal, qui a fait valoir le risque de "moqueries" d�coulant de ce patronyme, en citant notamment l'expression en langage populaire "ram�ne ta fraise" et les "r�percussions n�fastes" sur l'enfant, les parents ont finalement opt� pour "Fraisine", un pr�nom donn� au XIXe si�cle en France.
sda-ats
