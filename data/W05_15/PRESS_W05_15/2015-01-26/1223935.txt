TITRE: Loi Macron. Richard Ferrand (PS) sort de l'anonymat
DATE: 26-01-2015
URL: http://www.breizh-info.com/22126/actualite-politique/loi-macron-richard-ferrand-ps-sort-de-lanonymat/
PRINCIPAL: 1223875
TEXT:
La Bretagne en photos
Loi Macron. Richard Ferrand (PS) sort de l�anonymat
26/01/2015 � 09h00�Ch�teaulin (Breizh-info.com) ��Pour l�examen des 106 articles de la Loi Macron, l�Assembl�e nationale a cr�e une commission sp�ciale dont le rapporteur g�n�ral est Richard Ferrand (PS), d�put� de Ch�teaulin-Carhaix.
Proche d�Henri Emmanuelli, ce dernier est class� � gauche du parti. Il s�est abstenu lors des votes de la loi de s�curisation de l�emploi et du plan de 50 milliards d��conomie. Il �tait donc tr�s bien plac� pour piloter au Palais-Bourbon un texte d�inspiration lib�rale ! A�lui d��tre � la hauteur de la t�che car � cette lois va cr�er plusieurs dizaines de milliers d�emplois d�ici � dix-huit mois �, souligne Emmanuel Macron, ministre de l�Economie (Le Figaro, 13 janvier 2015).
Ce faisant, le gouvernement respecte une r�gle de la politique. Pour faire passer un projet de loi jug� difficile, le plus efficace consiste � utiliser les services de quelqu�un que l�on sait devoir y �tre hostile.�Et comme tout parlementaire r�ve, � d�faut de devenir ministre, de s�illustrer en devenant le rapporteur d�un texte important, il suffit de d�baucher un quidam. En un tour de main, son hostilit� � la politique du gouvernement se transformera en un soutien sans faille au texte qu�il aura l�honneur de pr�senter dans son assembl�e ; on peut compter alors sur son ardeur pour que le gouvernement l�emporte.
C�est le sort enviable de Richard Ferrand que l�on entend en ce moment sur les radios nationales. Sans la loi Macron, les journalistes parisiens continueraient d�ignorer l�existence de ce dernier�
Photo :�DR
[ cc ] Breizh-info.com, 2015, d�p�ches libres de copie et diffusion sous r�serve de mention de la source d�origine
