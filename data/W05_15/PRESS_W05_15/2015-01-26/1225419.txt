TITRE: Ils nomment leur nourrisson "Fraise" et "Nutella", le juge conteste | Planet
DATE: 26-01-2015
URL: http://www.planet.fr/societe-ils-nomment-leurs-nourrissons-fraise-et-nutella-le-juge-conteste.786712.29336.html
PRINCIPAL: 1225410
TEXT:
Ils nomment leur nourrisson "Fraise" et "Nutella", le juge conteste
Vous �tes ici
Publi� par Maxime Beaufils le Lundi 26 Janvier 2015 : 14h10
�Getty Images (illustration)
Envoyer par courriel
Participer (16 commentaires)
�Le juge des affaires familiales du tribunal de Valenciennes a refus� � deux couples de parents d'appeler leur nouveau-n� "Fraise" et "Nutella", au nom de l'int�r�t des enfants.
Ce sont des pr�noms pour le moins originaux que ces deux couples de parents pr�destinaient � leur enfant. Souhaitant les baptiser "Nutella" pour l'un et "Fraise" pour l'autre, le caract�re fantaisiste de ces pr�noms n'a pas emp�ch� le juge des affaires familiales (JAF) de Valenciennes de les refuser, rapporte La Voix du Nord .
Des pr�noms singuliers, mais censur�s
"Tout pr�nom inscrit dans l'acte de naissance peut �tre choisi comme pr�nom usuel", ces parents le savaient. Le 24 septembre 2014 � Valenciennes, un couple a appliqu� la loi � la lettre en pr�nommant le sien "Nutella". Seulement, lors de l'inscription � l'�tat civil, l'officier a estim� que "le pr�nom de l'enfant n'est pas conforme � son int�r�t". Il a d�s lors averti le procureur de la R�publique, comme la proc�dure le pr�voit, qui a lui m�me saisi le JAF. La suppression sur les registres de l'�tat civil effectu�e, le juge a renomm� l'enfant "Ella" lors d'une audience � laquelle les parents ne se sont pas rendus.
En ce qui concerne "Fraise", n�e le 17 octobre, la m�me proc�dure a �t� appliqu�e, avec pour motif : "le pr�nom de Fraise (�) sera n�cessairement � l'origine de moquerie notamment l'utilisation de l'expression ram�ne ta fraise, ce qui ne peut qu'avoir des r�percussions n�fastes sur l'enfant". Par chance, les parents, conscients que leur premier choix pourrait �tre refus�, ont propos� le pr�nom "Fraisine", accept� cette fois-ci.
Publicit�
Une libert� non-absolue
L'article 57 du code civil stipule que les parents peuvent choisir en toute libert� les pr�noms de leurs enfants. En effet, si aucune liste de pr�noms admissibles n'est en vigueur, l'article pr�cise cependant que l'officier d'�tat civil contr�le le pr�nom lors de la d�claration de naissance. L'int�r�t de l'enfant peut alors �tre invoqu�, le juge des affaires familiales sollicit� et le pr�nom refus�.
Vid�o sur le m�me th�me - Tha�lande : mobilisation pour Gammy, le b�b� trisomique abandonn� � sa m�re porteuse
Publi� par Maxime Beaufils le Lundi 26 Janvier 2015 : 14h10
Ailleurs sur le web
