TITRE: Open d'Australie: Stan Wawrinka avait un compte � r�gler -  News Sports: Tennis - lematin.ch
DATE: 26-01-2015
URL: http://www.lematin.ch/sports/tennis/stan-wawrinka-regler/story/17095745
PRINCIPAL: 1223709
TEXT:
Stan Wawrinka avait un compte � r�gler
Open d'Australie
� Imprimer
1/20 Indian Wells, USA
Stanislas Wawrinka a tr�buch� au 2e tour � Indian Wells face au N�erlandais Robin Haase (15 mars 2015).
Image: Keystone
� �
Serena Williams bouscul�e
Cette nuit, Serena Williams a d� puiser dans ses ressources mentales pour venir � bout de l'Espagnole Garbine Muguruza, t�te de s�rie N.24, en trois sets 2-6 6-3 6-2 et acc�der aux quarts de finale de l'Open d'Australie.
L'Am�ricaine, N.1 mondiale, restait sur deux �liminations successives en huiti�mes � Melbourne o� elle s'est impos�e � cinq reprises (2003, 2005, 2007, 2009, 2010).
De son c�t�, la Slovaque Dominika Cibulkov a mis un terme aux espoirs de la B�larusse Victoria Azarenka, domin�e en trois sets 6-2 3-6 6-3 lundi � Melbourne.
C'est la sixi�me fois de sa carri�re que la bondissante Slovaque de 25 ans, finaliste l'an pass�, atteint ce niveau en Grand Chelem.
Galerie photo
Retrouvez les meilleures moments de la vie du Vaudois en images.
Articles en relation
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
�a sentait la revanche � plein nez sur le Margaret Court. Stan Wawrinka retrouvait Guillermo Garcia Lopez, son bourreau lors du premier tour de Roland-Garros l'an dernier. �Pour le battre, je n'ai pas le choix: je dois faire le jeu�, disait le Vaudois au sujet de l'Espagnol avant leur huiti�me rendez-vous (4-3 pour Wawrinka).
Dans un premier set � combien d�terminant, Stan Wawrinka ne s'est pas affol�. Break� � 3-2 puis 5-4, le Vaudois a r�agi par deux fois en champion en reprenant le service de son adversaire en alignant deux jeux blancs. Dans la foul�e, il r�alisait un tie-break taille patron avec deux aces pour conclure, dont l'un sur deuxi�me balle.
Se demandant s�rement comment il avait pu perdre cette premi�re manche, Guillermo Garcia Lopez l�chait son service d�s le d�but du deuxi�me set. Solide sur sa premi�re balle de service lundi (60%), le Vaudois ne laissait par la suite aucune possibilit� � l'Espagnol de refaire son retard, 6-4.
Le d�but de la troisi�me manche �tait une r�plique de la pr�c�dente. Stan Wawrinka faisait le break pour mener 2-1. Le match bien en mains, il menait 4-3 avant de perdre son service par deux fois face � un Garcia-Lopez au d�chet minime (4 fautes directes dans cette manche). Le Vaudois venait d'�garer son premier set de la quinzaine.
A nouveau conqu�rant, Garcia-Lopez �voluait en pleine confiance. Le mano-�-mano entre les deux joueurs filait jusqu'au tie-break, o�, bien mal embarqu�, Stan Wawrinka sauvait quatre balles de set avant d'inscrire six poins cons�cutifs. Il remportait finalement son quatri�me jeu d�cisif du tournoi sur sa deuxi�me balle de match et �vitait un cinqui�me set d�s plus p�rilleux... (Newsnet)
Cr��: 26.01.2015, 06h50
