TITRE: Lepetitjournal.com - 404
DATE: 26-01-2015
URL: http://www.lepetitjournal.com/international/france-monde/actualite/205644-routiers-les-blocages-cibles-se-mettent-en-place
PRINCIPAL: 1224051
TEXT:
En direct de nos �ditions locales
Milan - actualit� italie
C�est une OPA qui risque de faire parler en Italie. Pirelli, la soci�t� mythique italienne de pneumatiques sera d�sormais g�r�e par un�
istanbul - Actu Turquie
Le bilan des victimes de la grippe dite �porcine� (H1N1) continue de s�aggraver. Le ministre de la Sant� Mehmet M�ezzino?lu a annonc� que�
Hong Kong Interview et Portraits
Les sc�nes de rue, Xyza Cruz Bacani les fige en noir et blanc mais le monde de cette jeune domestique philippine �prise de photographie�
Actualit� Espagne
Cette ann�e, la Semaine Sainte se d�roulera du 28 mars au 5 avril. Ces quelques jours sont c�l�br�s partout en Espagne, ce qui fait de�
Le Cap - Actualit� Afrique du Sud
Les repr�sentants des �tudiants de UCT (University of Cape Town) demandent le retrait de la statue � l�image du colonisateur de l�Empire�
Alger - Actualit�
Le pr�sident malien, Ibrahim Boubacar Ke�ta, a estim� que sa visite d�Etat de trois jours en Alg�rie a atteint �tous ses objectifs�.
Sydney - Actualit� Australie
L�attaque de la jeune Masa, poignard�e mardi dernier dans un parc � Melbourne a provoqu� une pol�mique au sein de la soci�t� australienne�
Les troph�es
TROPH�ES DES FRAN�AIS DE L'�TRANGER - Une soir�e de gala au Quai d�Orsay pour les laur�ats
Pour la troisi�me �dition des Troph�es des Fran�ais de l��tranger, plus de 300 personnes �taient r�unies mardi 17 mars au Quai d�Orsay pour r�compenser les laur�ats. Ces sept expatri�s aux parcours exceptionnels, faisant rayonner la France au-del� de ses fronti�res gr�ce � leurs projets vari�s, �taient � l�honneur. Aux c�t�s du Secr�taire d�Etat charg� des Fran�ais de l��tranger, Matthias Fekl, s�nateurs, d�put�s et partenaires �taient pr�sents pour la c�r�monie.
TROPH�ES DES FRAN�AIS DE L��TRANGER � D�couvrez les laur�ats 2015
lepetitjournal.com met � l�honneur nos compatriotes avec les Troph�es des Fran�ais de l'�tranger. D�couvrez les laur�ats de cette troidi�me �dition. Ils sont luthier au Portugal, entrepreneur au Vietnam, artiste scupteur en Tha�lande, apprenti forgeron au Japon, ils ont fond� une �cole au Danemark, une ONG au Cambodge ou vou� leur vie � l'agro-�cologie en Afrique...Tous ont en commun un engagement et un parcours exceptionnels. Ils ont �t� r�compens� hier soir au Quai d'Orsay, lors d'une soir�e de gala
Expat
LEPETITJOURNAL.COM ET L�AEFE � Un partenariat sous le signe de la francophonie...
Herv� Heyraud, pr�sident du site lepetitjournal.com, et H�l�ne Farnaud-Defromont, directrice de l�Agence pour l'enseignement fran�ais � l'�tranger, ont sign� une convention de partenariat. L�objectif ? Am�liorer la visibilit� des �v�nements mis en place par l�AEFE en France et dans le monde, et contribuer ainsi � la promotion et au rayonnement de la culture et de la langue fran�aise � l��tranger
FRAN�AIS D'EXCEPTION - S�bastien Perret, l'urgence au coeur
Instituteur et pompier volontaire, S�bastien Perret a eu l�id�e folle de monter de toutes pi�ces un service de secours d�urgence pour les victimes d�accident � Vientiane au Laos. Malgr� des moyens d�risoires, gr�ce � l�aide de volontaires, ce service b�n�vole fait des miracles et sauve des vies
SALON - "S'expatrier mode d'emploi" le 27 mars � Paris
�
Gr�ce au salon � S�expatrier, mode d�emploi � qui se tient le vendredi 27 mars, les candidats au d�part peuvent se renseigner sur toutes les d�marches � r�aliser avant de partir en expatriation (imp�ts, retraite, d�marches juridiques, protection sociale, emploi, assurance ch�mage etc.). Seront pr�sents l�Assurance retraite, la CFE (Caisse des Fran�ais de l�Etranger), le CLEISS (Centre des Liaisons Europ�ennes et Internationales d...
TROPH�ES DES FRAN�AIS DE L'�TRANGER - Une soir�e de gala au Quai...
Pour la troisi�me �dition des Troph�es des Fran�ais de l��tranger, plus de 300 personnes �taient r�unies mardi 17 mars au Quai d�Orsay pour r�compenser les laur�ats. Ces sept expatri�s aux parcours exceptionnels, faisant rayonner la France au-del� de ses fronti�res gr�ce � leurs projets vari�s, �taient � l�honneur. Aux c�t�s du Secr�taire d�Etat charg� des Fran�ais de l��tranger, Matthias Fekl,� s�nateurs, d�put�s et partenaires �taient pr�sents ...
TROPH�ES DES FRAN�AIS DE L��TRANGER � D�couvrez les laur�ats 2015
lepetitjournal.com met � l�honneur nos compatriotes avec les Troph�es des Fran�ais de l'�tranger. D�couvrez les laur�ats de cette troidi�me �dition. Ils sont luthier au Portugal, entrepreneur au Vietnam, artiste scupteur en Tha�lande, apprenti forgeron au Japon, ils ont fond� une �cole au Danemark, une ONG au Cambodge ou vou� leur vie � l'agro-�cologie en Afrique...Tous ont en commun un engagement et un parcours exceptionnels. Ils ont �t� r�compe...
MARIE-CHRISTINE SARAGOSSE - "Nous sommes fid�les � nos valeurs"
Rencontre avec Marie-Christine Saragosse, Pr�sidente-directrice g�n�rale de France M�dias Monde, qui regroupe France 24, RFI et MCD, depuis 2012. Elle revient sur les suites des attentats de Paris et �voque son premier bilan � la t�te du groupe, ainsi que ses projets de d�veloppement � l�international.
ENQU�TE - Votre couple et l'expatriation
Vous avez particip� au grand sondage que nous avons lanc� avec lepetitjournal et femmexpat sur l�impact de l�expatriation pour les couples ? Alix Carnot vous livre, avant m�me la publication de son livre qui sortira cet �t�, la substantifique mo�lle de l'�tude en 10 points. C�est du dense, du lourd, du sensible, du tr�s pr�cieux�!
Les deux tiers des expatri�s fran�ais n�envisagent pas un retour en France dans les cinq ans � venir. Pourquoi cette r�ticence ? La protection sociale "� la fran�aise" peut-elle les faire changer d'avis ?
Expat - Emploi
FRANCOPHONIE � Le fran�ais, la langue morte des entreprises ?
� l�heure de la mondialisation, l�anglais a un statut h�g�monique dans le monde de l�entreprise. Qu�en est-il du fran�ais�? Peut-il encore �tre consid�r� comme une langue de travail�? Yves Montenay, directeur de l�Institut de culture, �conomie et g�opolitique, et ancien chef d�entreprise, estime que c�est une valeur essentielle � la sant� des entreprises francophones.
COACHING - Peur de rentrer en France
Apr�s 25 ans en Australie � travailler dans l'h�tellerie Jos�phine h�site � rentrer. Le retour en France est-il simple apr�s une si longue expatriation ? Afin de surmonter son appr�hension elle fait appel � un coach. Nicolas Serres Cousin� nous fait part de cette exp�rience
Expat - Politique
FRANCOPHONIE - Fleur Pellerin lance la 20e �dition de la semaine de la langue Fran�aise
"La langue fran�aise est langue d�accueil " c�est le th�me de la 20e �dition de la semaine de langue fran�aise, lanc�e par la ministre de la culture et de la communication Fleur Pellerin ce mercredi 11 mars � Paris. Avec les �v�nements de ce d�but d�ann�e, la langue fran�aise devient un outil strat�gique d�unification et de tol�rance pour les francophones.
MATTHIAS FEKL � Premier forum pour pousser les  PME � l�export
Pour aider les PME � exporter plus, Matthias Fekl, secr�taire d�Etat charg� du Commerce ext�rieur, du Tourisme et des Fran�ais de l��tranger, pr�sentait un nouveau plan d�action lors du premier forum des PME � l�international dont il est l'initiateur.
Magazine
TRISTAN LUCAS � "Le one man show n��tait pas une �vidence"
Apr�s avoir tourn� avec "En douce!" pendant un an de Paris au festival de Royan o� il vient de remporter le Prix de la presse, Tristan Lucas et son "one man show� dr�le" s'est produit � Hong Kong. Une parenth�se chinoise pour cet habitu� des matchs d�improvisation � l'humour "cynique et bienveillant", d�j� distingu� par le Point virgule et le Jamel Comedy Club.
GASTRONOMIE FRAN�AISE - A table la plan�te !
Grand succ�s jeudi 19 mars partout dans le monde pour l�op�ration "Go�t de France / Good France", lanc�e par Laurent Fabius, Ministre des affaires �trang�res et du d�veloppement international, sous le haut patronage du chef Alain Ducasse. Le Commissaire g�n�ral du Pavillon de la France � l�Exposition universelle 2015,�Alain Berger, �tait � Milan � cette occasion pour y lancer Le Caf� des chefs, le restaurant du pavillon dont le concept reposera �...
