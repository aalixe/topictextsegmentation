TITRE: Euthanasie: des milliers de militants "pour la vie" d�filent � Paris | Site mobile Le Point
DATE: 26-01-2015
URL: http://www.lepoint.fr/societe/paris-des-milliers-de-militants-pour-la-vie-defilent-contre-l-euthanasie-25-01-2015-1899458_23.php
PRINCIPAL: 1223889
TEXT:
25/01/15 � 16h36
Euthanasie: des milliers de militants "pour la vie" d�filent � Paris
Des milliers de manifestants munis de ballons noirs et d'affichettes "Je suis Vincent Lambert" ont d�fil� dimanche, � Paris, pour d�noncer, lors d'une "Marche pour la vie", "la menace de l'euthanasie � l'Assembl�e nationale", a constat� une journaliste de l'AFP.
Le d�fil� entre les places Denfert-Rochereau et Vauban a r�uni entre 11.000 selon la police et 45.000 personnes d'apr�s les organisateurs, venues de toute la France.
Pour sa 10e �dition, la Marche pour la vie, traditionnellement d�di�e � la d�nonciation de l'avortement, a choisi de s'�lever contre "la l�galisation de gestes euthanasiques". Elle s'est tenue quatre jours apr�s un d�bat sur la fin de vie � l'Assembl�e nationale autour d'une proposition d�pos�e par Jean Leonetti (UMP) et Alain Claeys (PS), qui sera examin�e en mars.
Derri�re la banderole de t�te "Je suis Vincent Lambert", avaient pris place Viviane Lambert, sa m�re, l'avocat de cette derni�re, Me J�r�me Triomphe et, en blouse blanche et st�thoscope, le neurologue Xavier Ducros, conseiller m�dical des parents de Vincent Lambert.
Viviane Lambert est venue "se battre" pour son fils, victime d'un grave accident de la route en 2008, atteint de l�sions c�r�brales irr�versibles et dont elle refuse l'arr�t des soins.
"On se bat pour Vincent mais aussi pour la soci�t� (...) Il y a une porte qui s'est ouverte.  Aujourd'hui, c'est Vincent. Il n'est pas le premier et il ne sera pas le dernier", a-t-elle assur� � la presse avant le d�but de la manifestation.
"La loi Leonetti permet l'euthanasie; c'est grave de l'introduire dans le droit sans la nommer", a rench�ri le Pr Ducros.
Les organisateurs de la Marche font un lien direct entre l'interruption volontaire de grossesse (IVG) et "le droit � mourir dans la dignit�" qui permettrait aux malades incurables, de "b�n�ficier d'une s�dation profonde et continue",  comme propos� par Jean Leonetti et Alain Claeys.
"L'avortement est un permis de tuer, mis dans la loi sous couvert de la libert� des femmes, qui se poursuit aujourd'hui" avec les textes en d�bat, a dit C�cile Edel, pr�sidente de Choisir la vie, un des mouvements fondateurs de la Marche.
"Ce n'est pas � nous de d�cider qui doit vivre ou mourir", explique Isabelle, une ancienne infirmi�re venue d�filer en famille, dont sa fille trisomique de 21 ans.
"Cela commence ainsi mais o� s'arr�te t-on ? Ce qu'on a fait dans les camps de concentration, on risque de le faire dans les h�pitaux", lance-t-elle.
La marche, anim�e par une sono et encadr�e par une centaine de jeunes volontaires, s'est fig�e lors d'une minute de silence "pour les neuf millions de vie qui n'ont pas pu na�tre".
Jean-Paul, un badaud, estime qu'il assiste � "un m�lange des genres".
"On croit que les revendications portent sur la sant� puis on voit des drapeaux � fleur de lys, des symboles de la chouannerie", constate-t-il, "�a d�rive sur un mouvement droitier, d'apr�s moi, �a les dessert".
Le mouvement, qui dit avoir re�u le soutien du pape Fran�ois, a r�uni l'an dernier � Paris plusieurs dizaines de milliers de personnes contre l'interruption volontaire de grossesse.
La Cour europ�enne des droits de l'Homme a �t� saisie de l'affaire Vincent Lambert par ses parents notamment qui refusent une d�cision du conseil d'Etat en faveur de l'arr�t de l'alimentation et hydratation artificielles de cet homme de 38 ans, t�trapl�gique et en �tat v�g�tatif.
25/01/2015 18:56:37 - Paris (AFP) - � 2015 AFP
