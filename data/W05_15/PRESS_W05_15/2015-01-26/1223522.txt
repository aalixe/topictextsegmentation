TITRE: 04h51 Caen sait voyager - Ligue 1 - Football - Sport.fr
DATE: 26-01-2015
URL: http://www.sport.fr/football/04h51-caen-sait-voyager-368571.shtm
PRINCIPAL: 0
TEXT:
Football - Ligue 1 Lundi 26 janvier 2015 - 4:51
04h51 Caen sait voyager
Le Stade Malherbe de Caen s'est impos� � Rennes (1-4), dimanche, lors de la 22e journ�e de Ligue 1. En lutte pour le maintien et toujours dans la zone rouge malgr� cette victoire, le club normand affiche un bilant �loquent en d�placement depuis le d�but de la saison : en 11 matches hors de ses bases il n'a subi que 5 d�faites et totalise d�sormais 2 victoires ! Pour Rennes, ce revers annonce le retour vers le ventre mou du classement.
