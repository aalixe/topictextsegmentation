TITRE: L1 (J22) / RENNES - CAEN : 1-4 - Caen assomme Rennes et se relance en beaut�
DATE: 26-01-2015
URL: http://www.football365.fr/france/ligue-1/caen-assomme-rennes-et-se-relance-en-beaute-1203454.shtml
PRINCIPAL: 1223522
TEXT:
L1 (J22) / RENNES - CAEN : 1-4 - Publi� le 25/01/2015 � 19h10 -  Mis � jour le : 25/01/2015 � 19h35
Caen assomme Rennes et se relance en beaut�
Gr�ce � des buts de Privat, Nangis, F�ret et Da Silva, Caen a glan� un succ�s pr�cieux sur la pelouse de Rennes, qui continue de plonger. Avec cette deuxi�me victoire de rang en championnat, une premi�re cette saison, le Stade Malherbe revient � une longueur du premier non-rel�gable. Le tout avec la mani�re !
Le debrief
Apr�s une entame de match d�cevante et une ouverture du score de Sloan Privat (4eme), Rennes est progressivement entr� dans son match � l�occasion de la r�ception de Caen. Positionn�s en 4-2-3-1, les Rouge et Noir ont logiquement �galis� par l�interm�diaire de Benjamin Andr� (15eme) et auraient pu passer devant avant la pause, mais le score de parit� n��tait pas forc�ment d�m�rit�. Et le sc�nario s�est r�p�t� au retour des vestiaires. Les co�quipiers de Nicolas Seube se sont montr�s beaucoup plus tranchants que les Bretons et c�est Lenny Nangis (50eme) qui s�est charg� de leur redonner l�avantage. On pensait alors que les troupes de Philippe Montanier avaient les armes pour revenir une nouvelle fois. Mais c��tait sans compter sur la maladresse et les limites actuelles du Stade Rennais, qui n�a plus gagn� en championnat depuis le 3 d�cembre dernier (2-1 � Nice ). Il n�en fallait pas plus pour que les Normands profitent des espaces en contre et fassent le break. Auteur d�une prestation XXL pour son retour au Stade de la Route de Lorient, Julien F�ret (3 passes d�cisives et un but) et Damien Da Silva se sont charg�s de corser l�addition. Avec cette deuxi�me victoire de rang en championnat (deux fois 4-1), une premi�re cette saison, le Stade Malherbe revient � une longueur du premier non-rel�gable. De quoi respirer un peu mieux avant la prochaine journ�e (r�ception de l'ASSE). On ne peut pas en dire autant des Rennais, qui sont au bord de la crise...
Le film du match
4eme minute (0-1)
F�ret se charge d�un corner c�t� droit. Alors que son centre a �t� repouss� par la d�fense rennaise, le capitaine r�mois r�cup�re, contr�le et adresse un nouveau centre en direction de Privat. � l'entr�e de la surface de but, l�ancien Sochalien s'impose dans les airs et trompe Costil d'une t�te d�crois�e.
15eme minute (1-1)
Toivonen r�cup�re le ballon � l'entr�e de la surface. Sa passe est contr�e mais profite � Br�ls qui d�borde c�t� droit et centre. Pedro Henrique ne peut pas reprendre mais B.Andr� est � l�aff�t et conclut du plat du pied.
42eme minute
Sur le cot� droit, Prcic centre en direction de Toivonen qui passe devant son adversaire direct et reprend du pied droit apr�s un rebond. Mais sa tentative termine sur Vercoutre.
44eme minute
Nouvelle occasion pour le Stade Rennais ! S.Armand r�cup�re la ballon dans son camp et remonte tout le terrain. Tr�s l�g�rement d�cal� c�t� droit, l�ancien Parisien essaie de servir un partenaire mais le ballon lui revient dessus. Le d�fenseur central rennais tente alors sa chance et h�rite une nouvelle fois du ballon sur une d�viation. Son centre-tir du pied droit est repouss� par Vercoutre.
50eme minute (1-2)
F�ret lance parfaitement Nangis qui part entre Mexer et Danz�. Le Caennais s�arrache pour �liminer Costil et redresse le ballon dans le but vide. Le Stade Malherbe repasse devant.
76eme minute
Sur un long service d'Armand, Danz� est trouv� seul sur le c�t� droit de la surface caennaise. Le capitaine rennais se met sur son pied gauche et tente sa chance, mais Vercoutre est � la parade.
84eme minute
Vercoutre est sauv� par sa barre transversale sur une t�te de l�Autrichien Hosiner. Servi par Armand, l'ancien joueur de l'Austria Vienne a profit� d�une sortie h�sitante du gardien caennais pour placer une reprise lob�e.
85eme minute (1-3)
Sur un contre, F�ret s'�chappe seul et va se pr�senter seul face � Beno�t Costil. Le capitaine caennais multiplie les feintes de frappe avant de tromper le gardien rennais d'un tir crois� du pied droit.
47eme minute (1-4)
Contre-attaque pour le Stade Malherbe. En une touche, F�ret lance Da Silva sur le c�t� gauche de la surface rennaise. L�ancien Clermontois s�emm�ne le ballon d�une aile de pigeon et arme un missile du pied gauche qui termine dans la lucarne oppos�e de Costil.
Les joueurs � la loupe
Rennes
Alors que COSTIL n�a pas forc�ment grand-chose � se reprocher compte tenu des boulevards laiss�s par ses co�quipiers, ARMAND et MEXER n�ont pas �t� souverains en d�fense centrale. Loin de l�... M�me son de cloche concernant DIAGNE et DANZE, trop f�briles sur les flancs. Dans l�entrejeu, G.FERNANDES et B.ANDRE ont souffl� le chaud et le froid. Les deux hommes ont parfois manqu� de soutien pour contenir les offensives du Stade Malherbe. Alors que Doucour� a commenc� le match sur le banc et en l�absence de Ntep, Philippe Montanier a d�cid� de privil�gier l�attaque ce dimanche. En alignant le trio BRULS-PRCIC-PEDRO HENRIQUE, le technicien breton a fait le choix de l�offensive. Apr�s une entame de match discr�te, le Belge, le Bosnien et le Br�silien sont mont�s en puissance, notamment avec des permutations qui ont pos� des probl�mes aux Caennais, mais ce sursaut a �t� trop bref et sans suffisamment d�impact dans le jeu. Passeur d�cisif pour le 1-1, l�ancien joueur du FC Zurich n�a pas d�m�rit�, mais son coach a rapidement d�cid� de le remplacer par HABIBOU (59eme), tr�s discret. Confirm� en pointe, TOIVONEN a confirm� ses limites du moment. Le Su�dois s�est procur� quelques occasions ici et l�, mais on reste invariablement sur notre faim. L�ancien joueur du PSV Eindhoven peut et doit faire davantage, m�me s�il est aussi victime des limites actuelles du jeu propos� par les Rouge et Noir.
Caen
Impuissant sur le but de B.Andr�, VERCOUTRE a livr� une prestation convaincante ce dimanche. Devant lui, la charni�re compos�e de DA SILVA et de YAHIA a parfois souffert, mais les deux hommes ont fait mieux que limiter la casse face � Toivonen. Sur les c�t�s, APPIAH et IMOROU ont fait dans la sobri�t� pour contenir les assauts de Prcic, Pedro Henrique et Br�ls. Au milieu de terrain, SEUBE a permis de stabilit� le bloc �quipe. A ses c�t�s, KANTE et FERET ont �t� pr�cieux. Alors que le premier a effectu� un gros travail � la r�cup�ration et quelques perc�es rageuses, le second a �t� d�cisif � la cr�ation. Avec trois � assists � et un but, le capitaine caennais n�a pas rat� ses retrouvailles avec son ancien club. Une prestation XXL. Devant, PRIVAT a livr� un gros combat avec la charni�re rennaise. Buteur en d�but de match, l�ex-Sochalien a confirm� sa bonne forme du moment. Sur les couloirs, KOITA et NANGIS (buteur pour le 2-1) ont su alterner travail d�fensif et percussion.
Monsieur l�arbitre au rapport
Un match plein de M.Moreira qui �tait souvent tr�s pr�s des actions. L�arbitre de la rencontre a notamment eu raison de ne pas siffler de penalty � la suite d�une � faute � sur Koita en d�but de seconde p�riode (50eme). Il n�y avait rien et il ne s�est pas fait abuser.
�a s�est pass� en coulisses�
- Philippe Montanier, l�entra�neur rennais, �tait priv� de Grosicki (reprise), Ntep (cuisse), Lenjani (hanche), Konradsen (mollet) et M�Bengue (CAN).
- Chelsea ou encore Hull City avaient d�p�ch� des �missaires pour assister � cette rencontre comptant pour la 22eme journ�e de Ligue 1.
- Parti disputer la CAN 2015, le d�fenseur central gabonais de Caen, Yrondu Musavu-King, s�est bless� � une cheville et sera indisponible pour une dur�e estim�e � un mois par le staff m�dical des Panth�res.
La feuille de match
L1 (22eme journ�e) / RENNES - CAEN : 1-4
Stade de la Route de Lorient (18 091 spectateurs)
Temps froid - Pelouse moyenne
Arbitre : M.Moreira (6)
Buts : B.Andr� (15eme) pour Rennes - Privat (4eme), Nangis (50eme), F�ret (85eme) et Da Silva (89eme) pour Caen
Avertissement : Aucun
Expulsion : Aucune
Rennes
Costil (4) - Danz� (cap) (4), Mexer (4), S.Armand (4), Diagne (4) - G.Fernandes (4), B.Andr� (5) puis Hosiner (77eme) - Prcic (4), P.Henrique (5) puis Habibou (59eme), Br�ls (4) puis Doucour� (65eme) - Toivonen (4)
N'ont pas particip� : Sorin (g), S.Moreira, Zajkov, Pajot
Entra�neur : P.Montanier
Caen
Vercoutre (5) - Appiah (4), A.Yahia (5), Da Silva (6), Imorou (5) - Seube (5) puis Saad (88eme), N.Kant� (6), F�ret (cap) (8) - Koita (4) puis Ad�oti (82eme), Privat (6) puis Duhamel (86eme), Nangis (6)
N'ont pas particip� : Perquis (g), Calv�, Lemar, Bazile
Entra�neur : P.Garande
R�dig� par R�daction Football365 Suivre @toto
R�agir � cet article
