TITRE: 20 Minutes Online - Un F-16 s �crase au d�collage: 10 morts - Faits divers
DATE: 26-01-2015
URL: http://www.20min.ch/ro/news/faits_divers/story/Un-F-16-s-ecrase-au-decollage--10-morts-29189450
PRINCIPAL: 1226283
TEXT:
Voir le diaporama en grand �
29.01 Les d�pouilles des neuf soldats fran�ais tu�s lors du crash du F-16 ont �t� rapatri�es jeudi en France. Lors d'un point presse, le chef de l'arm�e de l'air fran�aise a d�taill� les circonstances du drame: �C'est une succession de malchances�, a d�plor� le g�n�ral Mercier. �L'�quipage du F-16, assez rapidement, pratiquement apr�s le d�collage, a essay� de s'�jecter, donc cela confirme bien cette panne�, a-t-il estim�. Image: AFP/Jose Jordan
29.01 L'avion de combat grec de type F-16 qui s'est �cras� lundi sur la base espagnole d'Albacete, faisant onze morts dont neuf militaires fran�ais, a eu une panne au d�collage, et les deux pilotes ont tent� de s'�jecter. Image: Keystone/AP/Daniel Ochoa de Olza
26 janvier 2015 18:22
Espagne
Un F-16 s'�crase au d�collage: 10 morts
Un avion de combat grec s'est �cras� lundi lors d'un entra�nement sur la base a�rienne de Los Llanos, dans le sud-est de l'Espagne. L'accident a fait dix morts et treize bless�s.
Le crash au d�collage d'un avion de combat grec de type F-16, lundi apr�s-midi dans le sud-est de l'Espagne, a fait dix morts et 13 bless�s, a-t-on appris aupr�s d'un porte-parole du minist�re de la D�fense espagnol. Cinq personnes se trouvaient dans un �tat grave.
�Pour le moment nous avons dix personnes mortes et 13 bless�es�, a d�clar� ce porte-parole en pr�cisant que parmi les personnes bless�es lors de cet accident survenu sur la base de Los Llanos dans la province d'Albacete (sud-est) sept sont tr�s gri�vement touch�es. Plus tard, le chef du gouvernement espagnol Mariano Rajoy a pr�cis� que huit Fran�ais et deux Grecs avaient trouv� la mort.
Cinq autres se trouvaient lundi vers 19h00 dans un �tat grave tandis qu'une a d�j� pu quitter l'�tablissement o� elle �tait soign�e, a indiqu� le porte-parole, qui n'�tait pas en mesure de pr�ciser la nationalit� des victimes.
L'avion de combat de la force a�rienne grecque devait effectuer des manoeuvres dans le cadre d'un entra�nement organis� par l'OTAN, le Tactical leadership Programme (TLP), et s'est �cras� au d�collage, selon un communiqu� diffus� plus t�t par le minist�re de la D�fense.
Incendie
Il s'est apparemment �cras� sur le tarmac, heurtant d'autre a�ronefs et tuant d'autres personnes qui s'y trouvaient. Lundi apr�s-midi des cha�nes de t�l�vision espagnoles ont diffus� quelques secondes d'images o� on aper�oit un avion en feu, d'o� s'�chappent d'importantes volutes de fum�e noire.
Les �quipes de secours ont d� venir � bout de l'incendie entra�n� par le crash sur l'aire de stationnement avant de pouvoir d�terminer le nombre de victimes.
(ats)
