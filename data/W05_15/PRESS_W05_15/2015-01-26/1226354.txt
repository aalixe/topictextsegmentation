TITRE: Stups : Saisie d�ampleur dans la r�gion toulousaine - Lib�ration
DATE: 26-01-2015
URL: http://www.liberation.fr/societe/2015/01/26/stups-saisie-d-ampleur-dans-la-region-toulousaine_1189317
PRINCIPAL: 1226353
TEXT:
Lire sur le readerMode zen
HISTOIRE
Plus d�une tonne de r�sine de cannabis - �valu�e � 2�millions d�euros - a �t� saisie en fin de semaine dans la r�gion toulousaine. Les gendarmes ont �galement interpell� 4�personnes, toutes fran�aises, dont l�une est consid�r�e comme la t�te d�un r�seau d�importation de drogue depuis l�Espagne. Les arrestations se sont d�roul�es dans la ville de l�Union (Haute-Garonne) ainsi qu�� Toulouse. Au cours des perquisitions, les enqu�teurs ont mis la main sur cinq v�hicules et d�couvert 7 500�euros au domicile d�un des hommes impliqu�s. Il s�agit d�une saisie �d�une rare ampleur sur la place toulousaine�, a d�clar� une source proche du dossier. Les premi�res pr�sentations aux juges d�instruction devraient avoir lieu � partir de ce mardi. La section de gendarmerie de Midi-Pyr�n�es a pr�cis� dans un communiqu� que l�op�ration a �t� men�e par la section de recherches (SR) de Toulouse dans le cadre d�une enqu�te de plusieurs semaines diligent�e par deux juges d�instruction de�la juridiction interr�gionale sp�cialis�e de Bordeaux (JIRS).
Recevoir la newsletter
