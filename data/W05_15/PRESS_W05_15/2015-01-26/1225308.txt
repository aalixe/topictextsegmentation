TITRE: Les autres affaires dans l'affaire Bettencourt
DATE: 26-01-2015
URL: http://www.bfmtv.com/societe/les-autres-affaires-dans-l-affaire-bettencourt-859563.html
PRINCIPAL: 1225247
TEXT:
Imprimer
Au-del� du volet "abus de faiblesse", quatre autres affaires s'inscrivent dans le spectre de l'affaire Bettencourt, pour "trafic d'influence", "atteinte � l'intimit� de la vie priv�e", "violation du secret professionnel" et "faux t�moignages". Rappel.
Quatre affaires dans l'affaire. Outre le volet "abus de faiblesse" , jug� � partir de ce lundi, l' affaire Bettencourt , instruite � Bordeaux, comprend trois autres volets, pour "trafic d'influence", "atteinte � l'intimit� de la vie priv�e" et "violation du secret professionnel". Une proc�dure distincte est en cours � Paris apr�s des plaintes de Fran�ois-Marie Banier et Patrice de Maistre en 2012, contre l'ex-comptable de Liliane Bettencourt, Claire Thibout, principal t�moin � charge dans le volet abus de faiblesse.
> Trafic d'influence
Ce volet concerne uniquement Patrice de Maistre, l'ex-gestionnaire de fortune de la milliardaire, mis en examen le 12 juin 2012 pour trafic d'influence actif, et l'ex-ministre UMP Eric Woerth, mis en examen le 8 f�vrier 2012 pour trafic d'influence passif.
Patrice de Maistre est soup�onn� d'avoir fourni un travail � l'�pouse d'Eric Woerth dans la soci�t� Clym�ne de Liliane Bettencourt, en �change d'une L�gion d'honneur.
Ce dossier sera jug� par le Tribunal correctionnel de Bordeaux du 23 au 25 mars.
> Atteinte � l'intimit� de la vie priv�e �
Six personnes sont renvoy�es dans ce dossier concernant les enregistrements clandestins qui ont pr�cipit� l'affaire. Tout d'abord l'ex-majordome de Liliane Bettencourt, Pascal Bonnefoy, auteur des enregistrements.
Puis cinq journalistes du Point et de Mediapart: Franz-Olivier Giesbert, directeur du Point � l'�poque, Herv� Gattegno, r�dacteur en chef du Point, Edwy Plenel, directeur de Mediapart, Fabrice Arfi, journaliste � Mediapart, Fabrice Lhomme, ancien journaliste � Mediapart. Le dossier a �t� renvoy� devant le Tribunal correctionnel de Bordeaux, mais aucune date n'a encore �t� fix�e.
Parall�lement, dans un dossier encore � l'instruction, Pascal Bonnefoy a �t� mis en examen une deuxi�me fois, le 21 f�vrier 2014, pour avoir enregistr� des conversations chez l'h�riti�re de L'Or�al, cette fois au d�triment de Fran�ois-Marie Banier et Patrice de Maistre. � � �
> Violation du secret professionnel�
Ce volet concerne l'annonce dans Le Monde, le 1er septembre 2010, d'une perquisition men�e chez Liliane Bettencourt le jour-m�me.�La juge d'instruction de Nanterre, Isabelle Pr�vost-Desprez, qui avait men� cette perquisition, est soup�onn�e d'avoir donn� cette information � des journalistes du quotidien. Elle sera jug�e, � ce titre, pour "violation du secret professionnel". Le Tribunal correctionnel de Bordeaux �voquera ce volet les 8 et 9 juin prochains.
> Enqu�te � Paris pour faux t�moignage � � � � � � � ��
Le 27 novembre dernier, Claire Thibout, ex-comptable de Liliane Bettencourt de 1995 � 2008, a �t� mise en examen par le juge parisien Roger Le Loire pour "faux t�moignages" et "attestation mensong�re". Le juge enqu�te � la suite de plaintes d�pos�es par Fran�ois Marie-Banier et Patrice de Maistre en juillet 2012.
Par A.S. avec AFP
A.S. avec AFP 26/01/2015 � 15:55
A lire aussi
