TITRE: Les anticoagulants Eliquis, Pradaxa et Xarelto ne doivent �tre prescrits qu'en deuxi�me choix | PsychoM�dia
DATE: 26-01-2015
URL: http://www.psychomedia.qc.ca/sante-cardiovasculaire/2015-01-26/anticoagulants-nacos-has
PRINCIPAL: 1225881
TEXT:
Les anticoagulants Eliquis, Pradaxa et Xarelto ne doivent �tre prescrits qu'en deuxi�me choix
Soumis par Gestion le 26 janvier 2015
Sant�
Les trois nouveaux anticoagulants oraux (NACO) apixaban (Eliquis), dabigatran (Pradaxa (1)) et rivaroxaban (Xarelto) ne doivent pas �tre prescrits en premi�re intention pour pr�venir les accidents vasculaires c�r�braux (AVC) et les embolies chez les personnes souffrant de fibrillation atriale non valvulaire (trouble du rythme cardiaque le plus fr�quent), rappelle la Haute autorit� fran�aise de sant� (HAS).
Les antivitamines K (AVK), tels que la warfarine (Coumadine, Coumadin), restent le traitement de r�f�rence (2).
Ces 3 NACO, dits anticoagulants d'action directe n'ont pas tous la m�me efficacit�, �value la HAS:
le service m�dical rendu reste important pour Eliquis et Xarelto;
il est mod�r� pour Pradaxa;
l�am�lioration du service m�dical rendu d�Eliquis est mineure par rapport aux AVK, mais il n�y a pas d�am�lioration du service m�dical rendu par rapport aux AVK pour Pradaxa et Xarelto.
Les NACO doivent, dans l��tat actuel des connaissances, �tre prescrits en deuxi�me intention, compte tenu de l�absence d�antidote (contre les h�morragies) et l�absence de possibilit� de mesurer en pratique courante le niveau d�anticoagulation.
Ils sont � r�server :
aux patients sous AVK pour lesquels le maintien de l�INR (International Normalized Ratio) d�sir� dans la zone cible n�est pas assur� malgr� une observance correcte;
les patients pour lesquels les AVK sont contre-indiqu�s ou mal tol�r�s ou qui acceptent mal les contraintes li�es � la surveillance de l�INR.
"
La HAS rappelle la n�cessit� d�une prise r�guli�re de ces traitements, en raison de la sensibilit� particuli�re des anticoagulants non vitamine K � l�oubli d�une prise.
"
La HAS actualisera et diffusera � l�attention des prescripteurs une fiche de bon usage de ces m�dicaments.
Voyez �galement:
