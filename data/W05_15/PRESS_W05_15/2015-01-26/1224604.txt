TITRE: Mort de l'Allemand Froese, pionnier de la musique �lectronique
DATE: 26-01-2015
URL: http://www.boursorama.com/actualites/mort-de-l-allemand-froese-pionnier-de-la-musique-electronique-295a5240002df728636eece78f72664f
PRINCIPAL: 1224595
TEXT:
Mort de l'Allemand Froese, pionnier de la musique �lectronique
Le Monde le
Tweet 0
Le musicien allemand Edgar Froese est d�c�d� � l'�ge de 70 ans, a annonc� son fils vendredi 23 janvier.
Il �tait membre du groupe Tangerine Dream, form� dans les ann�es 1970 et connu pour ses m�lodies futuristes. Ag� de 70 ans, il a succomb� � une embolie pulmonaire.
Le musicien allemand Edgar Froese, dont le groupe Tangerine Dream avait cr�� un son futuriste qui allait marquer des g�n�rations d'artistes �lectroniques, est d�c�d� � l'�ge de 70 ans, a annonc� son fils vendredi 23 janvier.
Ce dernier, qui avait rejoint son p�re au sein de la formation musicale, a pr�cis� qu'il �tait mort subitement, mardi, � Vienne (Autriche) d'une embolie pulmonaire.
��Un jour Edgar a dit�: �Il n'y a pas de mort, il y a juste un changement de notre adresse cosmique�. Edgar, cela nous r�conforte un tout petit peu��, ont d�clar� les autres membres du groupe dans un communiqu�.
Froese avait choqu� le monde musical au d�but des ann�es 1970 en utilisant des synth�tiseurs pour g�n�rer une atmosph�re de transe minimaliste. Celle-ci n'avait que peu � voir avec le rock qui dominait alors les ondes.
SURR�ALISME
N� le 6 juin 1944 dans ce qui est aujourd'hui la ville russe de Sovetsk, il avait �tudi� � Berlin-ouest. Sa vie a bascul� quand il a �t� invit� � jouer en Espagne, dans la villa du peintre Salvador Dali, un de ses h�ros, en 1967.
Cette rencontre l'a pouss� � transposer une sorte de surr�alisme dans sa musique. Dans une interview donn�e des ann�es apr�s, Froese avait racont� que Dali lui avait dit�: ��Presque tout est possible dans l'art tant que tu crois fermement en ce que tu fais��.
Froese s'�tait ensuite li� � plusieurs musiciens berlinois pour former Tangerine Dream, un groupe qui a d�coll� apr�s avoir attir� l'attention du pr�sentateur radio bri...
