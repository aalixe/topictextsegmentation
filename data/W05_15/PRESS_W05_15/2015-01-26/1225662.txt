TITRE: Ligue 1 : le bon coup du PSG | www.directmatin.fr
DATE: 26-01-2015
URL: http://www.directmatin.fr/foot/2015-01-25/ligue-1-le-bon-coup-du-psg-698424
PRINCIPAL: 1225659
TEXT:
Ligue 1 : le bon coup du PSG
Par Direct Matin, publi� le
26 Janvier 2015 � 19:51
Victorieux � Saint-Etienne (0-1) en cl�ture de la 22e journ�e de Ligue 1, dimanche, le PSG en a profit� pour revenir � la deuxi�me place, � hauteur de son rival marseillais.[[PHILIPPE DESMAZES / AFP]]
En compl�ment
Petit � petit, ils refont leur retard. Victorieux � Saint-Etienne (0-1) en cl�ture de la 22e journ�e de Ligue 1, dimanche, le PSG en a profit� pour revenir � la deuxi�me place, � hauteur de son rival marseillais.
�
Dans une rencontre o� la premi�re p�riode a �t� totalement vierge d�occasion, la d�cision s�est fait sur un penalty transform� par Zlatan Ibrahimovic � l�heure de jeu (60e). Si les Parisiens n�ont pas encore montr� un visage s�duisant que l�on attend depuis le d�but de saison, les hommes de Laurent Blanc se sont attel�s � �tre rigoureux et appliqu�s. Bien leur en a fait car avec ce succ�s le PSG �limine un adversaire direct et r�alise la bonne op�ration du weekend en ne laissant pas le leader lyonnais s��chapper en t�te du championnat.�
�
Lyon maintient la cadence
Car l�OL a poursuivi sa superbe s�rie plus t�t dans la journ�e avec une septi�me victoire d�affil�e en Championnat en battant Metz (2-0). Un succ�s qui a toutefois �tait terni par la blessure d�Alexandre Lacazette qui venait d�ouvrir le score sur un penalty (son 21e but de la saison). Le meilleur buteur de L1 souffrirait d�une l�sion musculaire d�une cuisse et passera des analyses dans la journ�e. �Il y a de l'inqui�tude car c�est notre leader d�attaque avec les deux �ch�ances qui nous attendent contre Monaco et le PSG�, a d�clar� Hubert Fournier. Il est vrai que sans lui, Lyon pourrait �tre tr�s handicap�.�
�
