TITRE: VIDEOS. New York, d�j� sous la neige, se pr�pare � une temp�te historique
DATE: 26-01-2015
URL: http://www.leparisien.fr/international/new-york-se-prepare-a-une-tempete-de-neige-historique-26-01-2015-4479775.php
PRINCIPAL: 1224458
TEXT:
VIDEOS. New York, d�j� sous la neige, se pr�pare � une temp�te historique
26 Janv. 2015, 08h25 | MAJ : 27 Janv. 2015, 01h11
1/2 r�agir
25
New York (Etats-Unis), lundi 26 janvier 2015. En fin d'apr�s-midi la neige a commenc� � blanchir Manhattan.�Bill de Blasio, le maire de la ville, a appel� les habitants � la plus grande prudence pour mardi et mercredi.
AFP / Getty Images / Spencer Platt
Le maire de New York  Bill de Blasio�a mis en garde contre l'arriv�e d'une temp�te de neige qui pourrait �tre selon lui l'une des �plus importantes de l'histoire de cette ville�. Cette temp�te accompagn�e de vents violents va provoquer d'importantes chutes de neige dans le nord-est des Etats-Unis lundi en fin de journ�e et mardi, a annonc� la m�t�o nationale qui a �mis un avis de blizzard pour New York et Boston, jusqu'� la fronti�re canadienne.
Sur le m�me sujet
Pol�mique � New  York apr�s une temp�te de neige moins grave que pr�vu
La neige tombait d�j� fortement lundi soir � New York o� des centaines de chasse-neige �taient mobilis�s contre le blizzard. Le maire Bill de Blasio a appel� la population � sortir le moins possible, et a interdit la circulation de tous les v�hicules --sauf v�hicules d'urgence-- � partir de 23 heures (5 heures du matin en France mardi), une mesure rarissime.
VIDEO. Suivez l'�volution de la situation en direct
Selon la m�t�o nationale, la neige pourrait tomber durant la nuit au rythme de 5 � 10 cm par heure, avec une accumulation attendue de 60 cm � New York, voire plus en certains endroits, et des bourrasques de vent de 72 � 88 km/heure. �La temp�te va s'aggraver durant la nuit�, a annonc� en fin d'apr�s-midi le gouverneur de l'Etat de New York Andrew Cuomo. Il a annonc� la fermeture du m�tro, qui fonctionne � New York 24 heures sur 24, � partir de 23 heures pour une dur�e ind�termin�e. M. Cuomo a �galement annonc� que les trains de banlieue s'arr�teraient aussi � 23 heures, l� encore pour une dur�e ind�termin�e, et a �tendu � tout l'Etat de New York l'interdiction de circuler, sauf pour les v�hicules d'urgence. Les parcs new-yorkais ont ferm� � 18 heures. Les �coles resteront closes mardi, a annonc� �galement le maire. Le si�ge de l'ONU n'ouvrira pas non plus. Les spectacles de la c�l�bre Broadway ont �t� annul�s lundi soir. Mais la Bourse sera ouverte mardi.
Le pr�sident am�ricain Barack Obama , actuellement en Inde, a �t� inform� de tr�s fortes chutes de neige attendues dans le nord-est des Etats-Unis, � New York et Boston notamment, a indiqu� lundi la Maison Blanche. �Des responsables de la Maison Blanche ont �t� en contact avec des responsables locaux le long de la c�te Est pour s'assurer qu'ils disposaient des ressources n�cessaires pour se pr�parer et r�agir imm�diatement � la temp�te�, a d�clar� Josh Earnest, porte-parole de l'ex�cutif am�ricain, lors d'un point de presse � New Delhi o� M. Obama effectue une visite de trois jours.
VIDEO. Blizzard au Etats-Unis : transports publics limit�s � New York
�Ce n'est pas une temp�te typique�, a insist� le maire, �rentrez t�t chez vous�, a-t-il ajout�, r�p�tant que le blizzard devrait �tre �l'un des plus importants de l'histoire de la ville� de 8,4 millions d'habitants. Cette temp�te, qui touche une bande de plus de 450 km de longueur, allant de Philadelphie � l'Etat du Maine, devrait s'�loigner progressivement mercredi. Prenant leur maire au mot, beaucoup de New-Yorkais ont quitt� t�t le travail. En milieu d'apr�s-midi, de nombreux embouteillages congestionnaient les rues de la plus grande ville am�ricaine. �Les New-Yorkais doivent se pr�parer � de possibles pannes d'�lectricit�, dues aux vents violents qui pourraient faire tomber les lignes et les arbres�, a aussi averti le gouverneur.
Il a pr�cis� que l'Etat de New York disposait d'au moins 1.806 chasse-neige et de plus de 126.000 tonnes de sel pour lutter contre le mauvais temps. Tous les bus urbains devaient selon lui �tre �quip�s de pneus neige ou de cha�nes.
Ecoles ferm�es mardi
Les New-Yorkais inquiets avaient d�s dimanche d�valis� les supermarch�s pour faire des provisions. Beaucoup ont fait de m�me lundi, patientant parfois dans le froid avant de pouvoir entrer, pour faire des r�serves de pain, lait, fruits et l�gumes. Dans la r�gion de Boston, o� de 50 � 76 cm de neige sont attendus, les bourrasques de vent pourraient d�passer 100 km/heure. Les �coles y seront aussi ferm�es mardi, selon les autorit�s et une interdiction de circuler � partir de lundi soir a �t� impos�e dans tout l'Etat du Massachusetts, comme dans l'Etat proche du Connecticut.
VIDEO. �Ne sous-estimez pas cette temp�te� avertit le maire de New-York.
La s�lection du jury dans le proc�s de Djokhar Tsarnaev, accus� des attentats de Boston en 2013, a �galement �t� suspendue, le tribunal annon�ant qu'il resterait ferm� mardi. Le manque de visibilit� associ� � la neige et au vent rend la circulation particuli�rement dangereuse, ont insist� les autorit�s dans tous les Etats touch�s par la temp�te, dont plusieurs ont d�clar� l'�tat d'urgence.
�De nombreuses routes seront impraticables�, a soulign� la m�t�o nationale. Les d�placements seront �extr�mement dangereux en raison des fortes chutes de neige et des vents violents�. Lundi, plus de 2.700 vols ont �t� annul�s de ou vers les Etats-Unis, et plus de 3.700 l'�taient d�j� pour mardi, selon le site sp�cialis� Flightaware.com. Selon les autorit�s, la moiti� des vols ont notamment �t� annul�s � l'a�roport international John F. Kennedy � New York.
#USA 3200 vols annul�s lundi en raison du #blizzard qui va frapper le Nord-Est. pic.twitter.com/w2VBdD3Y90
� Breaking 3.0 (@Breaking3zero) 26 Janvier 2015
Quelques donn�es de r�f�rence avant le #blizzardof2015 pic.twitter.com/9c9avBqERJ
� Richard H�tu (@richardhetu) 26 Janvier 2015
La compagnie United a annonc� qu'elle avait l'intention d'annuler tous ses vols mardi dans les trois a�roports new-yorkais de Newark, LaGuardia et JFK, ainsi qu'� Boston et Philadelphie.
Dimanche soir, de nombreux supermarch�s new-yorkais ont �t� pris d'assaut par des habitants inquiets. Dans le quartier de Chelsea, au centre de Manhattan, une file d'attente s'�tait form�e sur le trottoir pour pouvoir entrer dans le magasin Trader Joe's sur la 6e avenue. Plus de 50 millions de personnes dans le nord-est du pays pourraient �tre affect�es par cette temp�te de neige qui risque de paralyser les transports.
Avant la temp�te de neige historique attendue lundi � New York : longues files d'attente et rayons d�valis�s. #AFP pic.twitter.com/uXoo8Hs5Iw
� Brigitte Dusseau (@BrigitteDusseau) 26 Janvier 2015
Sur la c�te est des Etats-Unis, les habitants de la ville d'York, en Pennsylvanie, pr�voient d'�tre bloqu�s plusieurs jours. Au Walmart Supercenter, les rayons ont litt�ralement �t� d�valis�s dans l'attente de la temp�te historique annonc�e. Idem � New-York, o� les magasins ont �t� pris d'assaut d�s leur ouverture au petit matin.
� Briana Mendenhall (@bmendenhall2) 26 Janvier 2015
Au #supermarche pour faire le plein d'eau et de conserves. Jamais vu autant de monde a 7 heures du mat. #blizzard #NewYork #blizzardof2015
� sandrine  Kukurudz (@sandrineMehrezK) 26 Janvier 2015
VIDEO. M�t�o : un blizzard attendu sur la c�te Est des Etats-Unis
