TITRE: Bleus : Onesta sort les griffes, Tous les sports
DATE: 26-01-2015
URL: http://www.lesechos.fr/sport/omnisport/sports-705776-bleus-onesta-sort-les-griffes-1086955.php
PRINCIPAL: 1225442
TEXT:
Bleus : Onesta sort les griffes
Le 26/01 � 11:08
Bleus : Onesta sort les griffes
1 / 1
Alors que l'�quipe de France affronte l'Argentine en huiti�me de finale du Championnat du monde, lundi (19h), Claude Onesta s'est adress� � son groupe avec autorit�, lundi matin. Tout autre r�sultat qu'une victoire serait � une grossi�ret� � selon le s�lectionneur tricolore.
Claude Onesta n'est pas satisfait du niveau de jeu affich� par son �quipe depuis le d�but du Mondial au Qatar. Et s'ils ne le savaient pas encore, les joueurs sont d�sormais au courant. Alors que se pr�sente le huiti�me de finale face � l'Argentine lundi soir (19h), le s�lectionneur tricolore a adress� un discours �pic� � son groupe.
� Ceux qui ne sont pas capables d'�tre � la hauteur de l'�v�nement qui leur est propos� vont finir la comp�tition dans les tribunes en regardant les autres. C'est peut-�tre l'endroit o� ils sont le mieux. (...) Pour moi c'est termin� ! A partir d'aujourd'hui, ceux qui n'auront pas su prendre le train en marche, qui ne montreront pas ce qu'ils peuvent apporter � l'aventure, vont quitter l'aventure, a pr�venu Claude Onesta lors d'une causerie, mercredi matin. Je n'ose m�me pas imaginer une seule seconde que l'on puisse perdre ce soir. Cela ne serait pas une erreur, �a ne serait pas une faute, ce serait une grossi�ret� dans le contexte et dans l'histoire de cette �quipe. �a ne rel�ve pas du niveau de jeu mais du niveau d'exigence que chacun va y mettre. Vous avez un potentiel qui est bien au-del� du potentiel n�cessaire pour dominer ce match-l� et ramener � la maison une �quipe qui a bien moins de talent que vous n'en avez. Sauf qu'� un moment donn�, ce potentiel, il va falloir le mettre sur le terrain. �
Les joueurs sont pr�venus. Battre l'Argentine n'est pas un objectif, mais un ordre.
Tous les sports
