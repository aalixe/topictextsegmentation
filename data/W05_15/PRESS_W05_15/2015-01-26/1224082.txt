TITRE: Arabie saoudite Tous aux fun�railles du roi Abdallah, mais sans critique aucune           | L'Humanit�
DATE: 26-01-2015
URL: http://www.humanite.fr/arabie-saoudite-tous-aux-funerailles-du-roi-abdallah-mais-sans-critique-aucune-563647
PRINCIPAL: 1224077
TEXT:
Arabie saoudite Tous aux fun�railles du roi Abdallah, mais sans critique aucune
Lundi, 26 Janvier, 2015
L'Humanit�
Le Britannique David Cameron, l��gyptien Abdel Fattah Al-Sissi, le Fran�ais Fran�ois Hollande�
Le Britannique David Cameron, l��gyptien Abdel Fattah Al-Sissi, le Fran�ais Fran�ois Hollande� De nombreux chefs d��tat et de gouvernement ont fait le d�placement samedi en Arabie saoudite pour pr�senter leurs condol�ances au nouveau roi Salmane, successeur de son demi-fr�re  Abdallah, d�c�d� dans la nuit de jeudi � vendredi. �?L�Arabie saoudite est un partenaire, � la fois sur le plan �conomique et politique?�, a soulign� au forum de Davos, vendredi, Fran�ois Hollande. Le roi Abdallah fait l�objet de louanges depuis vendredi. Ainsi, Christine Lagarde a vu en lui un �?grand d�fenseur des femmes?�, bien que le pays ne brille pas en la mati�re : elles n�y ont, par exemple, pas le droit de conduire. Amnesty International a d�nonc� un r�gime �?insensible aux droits de l�homme?�. Un jeune blogueur, Raif Badawi, a commenc� et va continuer � subir dans les prochains mois une peine de mille coups de fouets pour �?avoir insult� l�islam dans ses �crits?�.
�
Connectez-vous ou inscrivez-vous pour publier un commentaire
� la Une
D�partementales. La gauche veut faire barrage � la droite antisociale
25 mar 2015
Une exigence commune monte largement � gauche en vue du second tour : battre la droite et l�extr�me droite. Un appel � la mobilisation qui n�a rien d�un ch�que en blanc pour le gouvernement,  mais qui vise � prot�ger les politiques publiques locales pour les populations.
Le�la Shahid :��Forcer Isra�l � respecter le droit international�: c�est urgent��
24 mar 2015
Infatigable combattante palestinienne, diplomate hors pair, celle qui pendant longtemps a repr�sent� le visage de la Palestine en France puis aupr�s de l�Union europ�enne, Le�la Shahid, cessera ses activit�s officielles � la fin du mois d�avril. L�occasion de retrouver cette amie de longue date de "l�Humanit�", d��voquer sa carri�re et de tirer les le�ons du pass� pour arriver � construire l�avenir de la Palestine et des Palestiniens.
Changer de disque
24 mar 2015
L'�ditorial de Maud Vergnol : "S�rieusement d�savou� dimanche, le gouvernement d�chante et entonne l�air de � la division de la gauche"
Crash du vol Germanwings : �La descente rectiligne de l�appareil et l�absence de message de d�tresse interrogent�
24 mar 2015
Expert suisse en accidentologie a�rienne, auteur de � Accidents d�avions � (Favre, 2008), Ronan Hubert a cr�� en 1990 un site r�pertoriant l�ensemble des catastrophes a�riennes dans le monde, le Bureau d�archives des accidents d�avions (www.baaa-acro.com). Cet expert analyse pour l�Humanit� les premiers �l�ments disponibles sur le crash du vol Barcelone-Dusseldorf de la Germanwings, survenu mardi matin dans les Alpes.
Crash d'un Airbus A320 dans les Alpes-de-Haute-Provence
24 mar 2015
Un Airbus A320 de la compagnie Germanwings transportant 150 passagers dont six membres d'�quipage s'est �cras� pr�s de Barcelonnette. Selon le gouvernement et les secours,"'il n'y a aucun survivant". En vid�o, les premi�res images tourn�es sur le lieu du crash.
Jean-Paul Dufr�gne : ��Dimanche, soit la justice sociale, soit la droite��
25 mar 2015
Les communistes et leurs alli�s sont au coude-�-coude avec la droite dans l�Allier. Le pr�sident Jean-Paul Dufr�gne (PCF) revient sur son bilan, son projet et le risque de basculement.
