TITRE: Les Bleus prennent leur quart tranquillement - Championnat du monde de handball 2015 - Handball - Sport.fr
DATE: 26-01-2015
URL: http://www.sport.fr/handball/championnat-du-monde-de-handball-2015-les-bleus-prennent-leur-quart-tranqui-368684.shtm
PRINCIPAL: 1226409
TEXT:
Handball - Championnat du monde de handball 2015 Lundi 26 janvier 2015 - 20:34
Les Bleus prennent leur quart tranquillement
Contre l'Argentine, l'�quipe de France n'a pas fait de quartier. Vainqueurs facilement (33-20), les Bleus d�fieront la Slov�nie lors du prochain tour.
Tweet
Il y a des matches comme cela ou rien ne semble pouvoir vous arriver... Les hommes de Claude Onesta ont surement dû ressentir cela après une première période dominée de bout en bout (16-6), notamment grâce à un très grand Thierry Omeyer dans les cages, auteur de 9 arrêts dans la première demi-heure de jeu, avec un taux de réussite à 60%. A partir de ce moment là, les Français ont facillement pu enchaîner les contres, face à une vaillante équipe d'Argentine, cependant trop limitée pour pouvoir inquiéter les champions d'Europe en titre.
Les choses sérieuses commenceront donc en quart de finale, avec une rencontre face à la Slovénie. Les Slovènes se sont de leur côté défaits de la Macédoine (30-28) pour en arriver là. Ils constitueront un vrai test pour les coéquipiers de Nikola Karabatic . Mais selon toute vraisemblance, l'équipe de France devrait pouvoir, avec toutes ses forces vives, se défaire des Baltes tranquillement. Il ne restera plus ensuite que deux matches pour qu'elle puisse un peu plus entrer dans la légende de ce sport.
