TITRE: S�curit� routi�re: les nouvelles mesures d�voil�es par Bernard Cazeneuve | www.francesoir.fr
DATE: 26-01-2015
URL: http://www.francesoir.fr/societe-transport/securite-routiere-les-nouvelles-mesures-devoilees-par-bernard-cazeneuve
PRINCIPAL: 1225900
TEXT:
S�curit� routi�re: les nouvelles mesures d�voil�es par Bernard Cazeneuve
Ecouteurs, oreillette ou casque audio interdits
Publi� le :  Lundi 26 Janvier 2015 - 18:26
Derni�re mise � jour :  Jeudi 12 F�vrier 2015 - 15:51
S�curit� routi�re: les nouvelles mesures d�voil�es par Bernard Cazeneuve
Le ministre de l'Int�rieur Bernard Cazeneuve a d�voil� ce lundi les nouvelles mesures qui vont �tre prises afin de r�duire la mortalit� sur les routes de France. Le plan d'action du gouvernement s'articule autour de 4 axes regroupant 26 mesures diff�rentes.
�Capture d'�cran Daylimotion/LCP
Le ministre de l'Int�rieur a d�voil� ce lundi ses mesures pour lutter contre la mortalit� routi�re.
Bernard Cazeneuve veut faire � nouveau baisser la courbe des morts sur la route, en hausse en 2014� pour la premi�re fois depuis 12 ans . Lors d'une conf�rence de presse ce lundi 26, le ministre de l'Int�rieur a annonc� une s�rie de mesures, apr�s avoir officialis� les chiffres de la mortalit� routi�re: en 2014, ce sont 3.388 personnes qui ont trouv� la mort sur les routes de France, soit 120 de plus (3,7 %) qu'en 2013. Par ailleurs, le nombre d'accident corporels a vu une augmentation de 1,7 %.
Bernard Cazeneuve a affirm� son ambition de passer sous la barre des 2.000 morts par an sur les routes � l'horizon 2020.�Les� axes de travail propos�s par le ministre de l'Int�rieur �sont les suivants: "sensibiliser, pr�venir et former", "prot�ger les plus vuln�rables", "lutter contre les infractions graves" et "am�liorer la s�curit� des infrastructures".
La lutte contre l'alcoolisation des jeunes est le cheval de bataille de la sensibilisation. Apr�s s'�tre prononc� en faveur de la conduite accompagn�e � partir de 15 ans et du passage de l'examen du permis de conduire � 17 ans et demi, Bernard Cazeneuve a tranch� pour un abaissement du taux d'alcool�mie de 0,5g/l � 0,2g/l pour les jeunes conducteurs (soit un verre d'alcool maximum autoris�, au lieu de deux). De plus, les m�decins seront form�s la au d�pistage de l'alcoolisme et de la consommation de stup�fiants, alors que les g�rants de d�bits de boisson n'offrant pas d'�thylotest seront syst�matiquement sanctionn�s.
Une autre mesure vise � interdire de porter "tout syst�me de type �couteurs, oreillette, casque, susceptible de limiter tant l'attention que l'audition des conducteurs". Cela entra�nera notamment l'interdiction des kits mains libres, actuellement autoris�s. Seuls les dispositifs bluetooth et apparent�s resteront l�gaux --ce qui peut para�tre difficile � comprendre alors que les conducteurs qui t�l�phonent en conduisant semblent de plus en plus nombreux ces derni�res ann�es.�
En ville, le stationnement des v�hicules sera interdit � 5 m�tres des passages pi�tons "pour am�liorer la visibilit� entre pi�tons et conducteurs". Le stationnement dangereux sera �galement plus s�v�rement sanctionn�.
Les maires de certaines villes pourront baisser les limitations de vitesse sur certains axes ou dans toute la municipalit�. Sur certains tron�ons de route � double sens identifi�s comme particuli�rement accidentog�nes, on appliquera �une diminution de la vitesse maximale autoris�e de 90 � 80km/h.�
Le gouvernement entend �galement continuer la modernisation des 4.200 radars fixes d�ploy�s sur le territoire fran�ais (radars chantiers pour la s�curit� des personnels, radars mobile de nouvelle g�n�ration).�Enfin, autre mesure phare compte tenu de� la recrudescence de ce type d'accident :�promouvoir l'installation de panneaux sens interdit sur les bretelles d'autoroute pour �viter les contresens.�
Auteur : La r�daction de FranceSoir.fr
Mots Cl�s:�
