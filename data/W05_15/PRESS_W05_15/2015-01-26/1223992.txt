TITRE: Ligue 1 -                 22e j. -         Le PSG n'est pas l�ch�
DATE: 26-01-2015
URL: http://www.lequipe.fr/Football/Actualites/Le-psg-n-est-pas-lache/531331
PRINCIPAL: 1223985
TEXT:
a+ a- imprimer RSS
Le match : 0-1
Tr�s appliqu� d�s le d�but de la rencontre, le PSG a pos� sa patte sur le match et l'a logiquement emport� 1-0 . Saint-Etienne n�a quasiment pas jou� et n�a pu b�n�ficier que de 28% de possession de balle avant la pause. Rarement dangereux, les Verts ont craqu� � l�heure de jeu sur un penalty (voir par ailleurs). Et ils ont vu le lob de Cavani �chouer sur leur barre (76e). L'ASSE n�a plus gagn� depuis huit rencontres face aux Parisiens, qui ont d�ailleurs remport� les cinq derniers duels toutes comp�titions confondues. Troisi�me � quatre points de Lyon, le PSG a retrouv� la bonne carburation depuis une semaine.
Ibrahimovic, qui sera suspendu � Lille pour une accumulation d�avertissements, semble retrouver le niveau qui �tait le sien il y a plusieurs mois. Profitant du fait que l�ASSE n�ait pas align� trois r�cup�rateurs, il a souvent recul� sur le terrain pour organiser le jeu et a pos� des probl�mes � ses adversaires. Id�alement servi deux fois par Verratti, il a cependant �t� mis en �chec � chaque reprise par Ruffier. En fin de match, il aurait pu �tre expuls� pour une grosse semelle sur Hamouma.
L'homme du match : Marquinhos marque encore des points
Barr� en charni�re centrale par le tandem Luiz-Silva, Marquinhos a �t� titularis� pour la deuxi�me fois de la semaine sur le c�t� droit. L�ancien joueur de la Roma a �t� tr�s int�ressant d�fensivement (il a sorti un ballon tr�s chaud � la 80e) ou offensivement (90% de passes r�ussies, deux centres dangereux en premi�re p�riode). Son activit� a �t� incessante (83 ballons jou�s, troisi�me total parisien). Il a �quilibr� la d�fense 100% br�silienne du PSG, au sein de laquelle Silva a �t� excellent. Van der Wiel bless�, Aurier � la CAN : Marquinhos a le champ libre pour montrer � Laurent Blanc qu�il peut faire mieux que d�panner � ce poste.
Le tournant du match : le penalty contre Cl�ment
J�r�my Cl�ment a fait une belle faveur � son ancien club du PSG. Alors que sa d�fense, la meilleure du Championnat, tenait le coup contre le champion de France en titre, il a intentionnellement stopp� un ballon qui tra�nait dans sa surface de l��paule droite. M.Buquet n�a pas h�sit� et a accord� un penalty que Zlatan Ibrahimovic a transform�. En trois matches face aux Verts cette saison toutes comp�titions confondues, le Su�dois a marqu� � cinq reprises !
Paris sur le bon chemin :
Cyril OLIV�S-BERTHET
