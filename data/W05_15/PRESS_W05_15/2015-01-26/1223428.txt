TITRE: Yémen. La crise politique s'aggrave - Monde - Le Télégramme, quotidien de la Bretagne
DATE: 26-01-2015
URL: http://www.letelegramme.fr/monde/yemen-la-crise-politique-s-aggrave-26-01-2015-10504247.php
PRINCIPAL: 1223427
TEXT:
Yémen. La crise politique s'aggrave
26 janvier 2015
La crise politique s'est encore exacerbée, hier, au Yémen, où une réunion cruciale du Parlement a été reportée. Les nouveaux maîtres de Sanaa, les miliciens chiites (appelés Houthis), ont tiré à balles réelles et s'en sont pris à des journalistes, les empêchant de filmer la fuite de dizaines de manifestants. Une escalade de violence qui intervient au lendemain d'une grande manifestation contre la présence des Houthis dans la capitale, où ils sont entrés le 21 septembre. Dans ce contexte, Barack Obama a affirmé que les États-Unis étaient plus que jamais déterminés à y combattre al-Qaïda. Il a toutefois reconnu que la tâche s'annonçait ardue, notamment après les démissions, jeudi, du Président et du gouvernement de ce pays allié des États-Unis.
