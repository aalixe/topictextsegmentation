TITRE: Inside man et les Experts en t�te des audiences sur TF1 - Le zapping du PAF
DATE: 26-01-2015
URL: http://www.lezappingdupaf.com/2015/01/inside-man-et-les-experts-en-tete-des-audiences-sur-tf1.html
PRINCIPAL: 0
TEXT:
Inside man et les Experts en t�te des audiences sur TF1
Le Zapping du PAF
TF1
Cr�dit photo : Sonja Flemming / CBS
Le film Indide man de Spike Lee avec Denzel Washington, Clive Owen et Jodie Foster a s�duit 6.3 millions de t�l�spectateurs et 26% de pda, 32% de pda sur les femmes de moins de 50 ans RdA et 30% de pda sur les 15-34 ans.
A 23h20, l'�pisode "Maudite maison" des Experts a r�uni 1.8 million de t�l�spectateurs.
En moyenne les 4 �pisodes diffus�s hier soir ont rassembl� 1.1 million de t�l�spectateurs et 23% de pda, 25% de pda sur les femmes de moins de 50 ans RdA.
Source : TF1 / M�diamat M�diam�trie
