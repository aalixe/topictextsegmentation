TITRE: Drogue : deux policiers aux fronti�res interpell�s � Roissy avec 40 kilos de drogue
DATE: 26-01-2015
URL: http://www.rtl.fr/actu/societe-faits-divers/drogue-deux-policiers-aux-frontieres-interpelles-a-roissy-7776334853
PRINCIPAL: 1224532
TEXT:
Accueil Actu Soci�t� et faits divers Drogue : deux policiers aux fronti�res interpell�s � Roissy avec 40 kilos de drogue
Drogue : deux policiers aux fronti�res interpell�s � Roissy avec 40 kilos de drogue
Deux agents de la police aux fronti�res ont �t� interpell�s � Roissy dans le cadre d'une enqu�te sur un trafic de drogue.
Cr�dit : PIERRE VERDY / AFP
publi� le 25/01/2015 � 19:22
mis � jour le 25/01/2015 � 19:53
Partager
Commenter
Imprimer
Deux fonctionnaires de la police aux fronti�res (PAF) ont �t� interpell�s dimanche 25 janvier � l'a�roport de Roissy, en possession de 40 kilos de coca�ne, a-t-on appris de source proche de l'enqu�te.�Ils sont soup�onn�s d'avoir facilit� le passage � la douane d'un trafiquant de drogue.
Les deux policiers ont �t� interpell�s dans le cadre d'une commission rogatoire d�livr�e par un juge d'instruction parisien, alors qu'ils contr�laient des passagers en provenance de R�publique dominicaine, a ajout� cette source, confirmant une information de France 3.
Arr�t�s en possession de valises contenant de la coca�ne
Ils sont soup�onn�s d'avoir ferm� les yeux sur le passage de valises contenant de la coca�ne, transport�es par des mules, a pr�cis� la source proche de l'enqu�te.�Selon France 3, qui a r�v�l� l'information, les deux fonctionnaires ont �t� arr�t�s en possession de deux valises qu'ils venaient de r�cup�rer, contenant chacune 20 kilos de coca�ne.
D'apr�s la source proche de l'enqu�te, leur interpellation s'inscrit dans un coup de filet plus vaste, dans le cadre duquel une dizaine de personnes ont �t� arr�t�es et plac�es en garde � vue.�Parmi les personnes interpell�es figure notamment le commanditaire pr�sum� du trafic, interpell� dans les Hauts-de-Seine, selon France 3.
La r�daction vous recommande
