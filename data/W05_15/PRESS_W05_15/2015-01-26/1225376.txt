TITRE: Netflix n'est qu'à 2,5 % de son objectif en France
DATE: 26-01-2015
URL: http://www.numerama.com/magazine/32009-netflix-n-est-qu-a-25-de-son-objectif-en-france.html
PRINCIPAL: 1225375
TEXT:
Publi� par Guillaume Champeau, le Lundi 26 Janvier 2015
Netflix n'est qu'� 2,5 % de son objectif en France
Avec 250 000 abonn�s maximum selon les estimations d'un cabinet sp�cialis�, Netflix est encore extr�mement loin de l'objectif qu'il s'est fix� pour les prochaines ann�es. Pourra-t-il rattraper son retard ?
42
Est-il trop t�t pour parler d'�chec ? Lors de l'annonce officielle de l'arriv�e de Netflix en France, son patron Reed Hastings n'avait pas h�sit� � d�voiler de grandes ambitions, en confiant� au Figaro qu'il voulait "s�duire globalement un tiers des foyers d'ici cinq � dix ans", ce qui est l'objectif dans tous les pays o� le service de SVOD s'implante. Quatre mois apr�s son lancement le 15 septembre dernier, selon des donn�es de Digital TV Research rapport�s par Les Echos , Netflix "n�aurait recrut� que 200.000 � 250.000 abonn�s en France, dont la plupart passent par Internet (PC, tablettes...)" plut�t que par les box des op�rateurs.
Il reste donc beaucoup, beaucoup de chemin � parcourir avant que Netflix ne r�ussisse � atteindre ses objectifs en France. Selon les projections de l'INSEE (.pdf) , la France comptera�29,5 millions de foyers en 2020. S'il veut �tre raccord avec ses ambitions, le service en ligne am�ricain a donc cinq ans pour convaincre 9,8 millions d'abonn�s. Or si les estimations de Digital TV Research sont exactes, Netflix est au mieux � 2,5 % de cet objectif final.
Si Netflix n'arrive pas � d�coller rapidement, la question de la continuit� du service en France pourrait m�me se poser. Netflix estime que son seuil de rentabilit�, au dessus duquel il arr�te de perdre de l'argent, se situe � 10 % des foyers d'un pays o� il s'implante. Or pour le moment Netflix n'a convaincu que 0,9 % des foyers fran�ais. Il faut donc qu'il d�cuple sa base d'abonn�s pour commencer � rembourser ses investissements.�
Pour ce faire, le service de SVOD mise sur de prochaines productions, en particulier des s�ries TV fran�aises de qualit� disponibles exclusivement sur Netflix, pour convaincre les foules. Mais il devra aussi r�ussir � faire sauter le verrou de la chronologie des m�dias pour pouvoir proposer des films plus r�cents, et surtout �tendre son catalogue de s�ries TV am�ricaines, �tonnamment pauvre en France .
