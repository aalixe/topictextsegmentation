TITRE: Grève des routiers : les blocages ciblés se mettent en place, Tourisme - Transport
DATE: 26-01-2015
URL: http://www.lesechos.fr/industrie-services/tourisme-transport/0204108222260-greve-des-routiers-les-blocages-cibles-se-mettent-en-place-1086917.php
PRINCIPAL: 1224348
TEXT:
Gr�ve des routiers : les blocages cibl�s se mettent en place
Les Echos |
Le 26/01 � 09:32, mis � jour � 13:08
Lundi matin, les routiers bloquaient � nouveau des plates-formes logistiques dans toute la France - AFP/Lionel Bonaventure
1 / 1
Comme annonc�, les syndicats de routiers bloquent de nouveaux de sites industriels lundi dans toute la France. Cibles prioritaires �: les sites logistiques des grands noms du secteur.
Comme annonc� ces derniers jours par l'intersyndicale , les routiers, en gr�ve depuis le 18 janvier, bloquaient de nouveaux sites lundi dans toute la France. "Ca se met en place un peu partout", a ainsi d�clar� J�r�me V�rit� (CGT), ce que confirme Thierry Douine (CFTC). Les actions visent � p�naliser les patrons qui refusent la n�gociation salariale, et non les particuliers. Elles concernent des sites logistiques et les grosses entreprises adh�rentes des organisations patronales. "On bloque les zones pour emp�cher les camions de sortir et m�me de rentrer", a indiqu� M. V�rit�.
Autour de Dunkerque, les acc�s � l'ancienne raffinerie de Mardyck et au D�p�t de p�trole c�tier (DPC) de Saint-Pol-sur-Mer, sont rest�s ferm�s, selon FO.
Pr�s de l'a�roport de Nantes-Atlantique, une cinquantaine de militants syndicaux-chauffeurs routiers sont ainsi post�s sur la zone industrielle de Saint-Aignan Grandlieu depuis 5h00, selon Thierry Mayer (CGT). "Vous pouvez rassurer les usagers, tout le monde passe sauf les camions," a-t-il d�clar�. Le but est de "toucher au porte-monnaie deux acteurs principaux (ndlr, Charles Andr� et Norbert Dentressangle) de la FNTR [ organisation patronale ] qui n'appliquent que les minima sociaux et freinent les discussions pour l'am�lioration de la convention collective", a-t-il ajout�.
A Rennes, les chauffeurs routiers doivent se r�unir lundi apr�s-midi pour d�cider d'une �ventuelle action mardi matin qui ciblerait de grosses entreprises, selon la CGT.
