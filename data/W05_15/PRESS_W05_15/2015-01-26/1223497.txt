TITRE: Les march�s s�enflamment pour le rem�de du ��Dottore Draghi��
DATE: 26-01-2015
URL: http://www.lemonde.fr/economie/article/2015/01/24/les-marches-s-enflamment-pour-le-remede-du-dottore-draghi_4562861_3234.html
PRINCIPAL: 0
TEXT:
| Par Audrey Tonnelier
Tr�s fort. D�cid�ment tr�s fort. Une fois de plus, Mario Draghi a montr� qu�en mati�re de communication son talent confine � l�art. En annon�ant, jeudi 22�janvier, un programme de rachat de dettes de plus de 1�100�milliards d�euros (quantitative easing ou QE), le patron de la Banque centrale europ�enne (BCE) a r�ussi le tour de force de contenter, d�un coup d�un seul, �conomistes, classe politique � � l�exception notable des orthodoxes mon�taires allemands � et march�s.
Jeudi soir, toutes les places boursi�res ont termin� dans le vert, les pays d�Europe du Sud en t�te � la Bourse de Milan a gagn� 2,44�% et celle de Madrid, 1,7�%. Le CAC 40 a pass� la barre des 4�500 points jeudi, inscrivant un plus haut depuis la mi-juin, et des 4�600 points vendredi�
On peut l�gitimement se demander s�il est normal que les Bourses soient, sinon les seules, du moins les principales b�n�ficiaires � court terme d�un programme mon�taire tout de m�me prioritairement destin� � relancer le cr�dit et, partant, l��conomie europ�enne tout enti�re.
Le risque �tait grand
Mais, au-del� de ces querelles pro ou antimarch�s, une certitude demeure�: le bon accueil r�serv� par les investisseurs � ��Super Mario�� n�avait rien d��vident. D�abord parce que la plan�te finance, d�humeur tr�s volatile, est actuellement difficile � satisfaire�: elle passe en quelques heures de l�euphorie � la d�prime, et vice versa. Ensuite, parce que les march�s avaient d�j� largement anticip� les annonces...
L�acc�s � la totalit� de l�article est prot�g� D�j� abonn� ? Identifiez-vous
Les march�s s�enflamment pour le rem�de du��Dottore Draghi��
Il vous reste 71% de l'article � lire
