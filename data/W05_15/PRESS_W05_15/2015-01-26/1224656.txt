TITRE: Les Verts d�noncent � le cadeau de l'arbitre �
DATE: 26-01-2015
URL: http://www.leparisien.fr/psg-foot-paris-saint-germain/les-verts-denoncent-le-cadeau-de-l-arbitre-26-01-2015-4479677.php
PRINCIPAL: 1224655
TEXT:
Les Verts d�noncent � le cadeau de l'arbitre �
A Saint-Etienne (Loire) Yves Leroy |  26 Janv. 2015, 07h00 | MAJ : 26 Janv. 2015, 12h51
12
Stade Geoffroy-Guichard, hier soir. St�phane Ruffier lors du p�nalty. (Icon Sport/Jean-Paul Thomas.)
Le p�nalty accord� par Ruddy Buquet � la 60e minute a beaucoup fait jaser les St�phanois, hier. Du moins, ceux qui ont accept� de s'exprimer � ce sujet, puisque la plupart des Verts ne sont pas pass�s en zone mixte, hier. La sanction fait suite � un ballon repouss� sur sa ligne par J�r�my Cl�ment.
Sur le m�me sujet
Football : Ibrahimovic et Baysse se chauffent
Le joueur avait pris la peine de mettre la main dans le dos, mais il s'est lanc� en direction du ballon et a bien touch� le ballon de l'�paule, ce qu'il reconna�t�: � Pour moi, il n'y a pas main. Je suis persuad� de mettre l'�paule. Or, pour moi, l'�paule ne fait pas partie de la main. Si j'avais pens� le contraire, j'aurais laiss� passer le ballon. �
L'aveu du joueur d�note une certaine m�connaissance du r�glement. Les lois du jeu stipulent en effet que le bras comprend �galement l'�paule et qu'un contact avec le ballon entra�ne une sanction. Laurent Blanc , lui-m�me, a d'abord reconnu sur Canal+ que le p�nalty �tait � s�v�re, comme celui sur Van der Wiel � Bastia �. L'entra�neur parisien a ensuite minimis� en conf�rence de presse : � Du banc, on ne voit rien de ce qui se passe dans la surface. �
Le vestiaire sur les nerfs
L'entra�neur st�phanois, Christophe Galtier, furieux sur la pelouse, a esquiv� � deux reprises la question : � L'arbitre a pris deux secondes pour d�cider, on ne peut pas revenir dessus. Je lui ai dit ce que j'en pensais. C'est comme �a. � Le milieu de terrain Fabien Lemoine a quant � lui d�voil� beaucoup plus clairement la vision d'un vestiaire st�phanois sur les nerfs : ��L'arbitre a offert un cadeau aux Parisiens. Tant mieux pour eux. Bien s�r que c'est un cadeau ! On est nombreux � avoir revu les images et tout le monde dit que c'est l'�paule. Certains arbitres nous disent que l'�paule fait partie du bras, d'autres pr�tendent le contraire. Il faudrait s'accorder ! On est vraiment �nerv�s, mais on ne va pas pol�miquer, sinon on va se faire attraper par la patrouille. �
Nul doute que la s�quence sera longuement diss�qu�e cette semaine au centre d'entra�nement de L'Etrat.
Votre email * (ne sera pas visible)
Votre r�action *
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France*
* champs obligatoires
Bastien75015 a publi� le 27 Janvier 2015 � 15:53
Je ne comprends m�me pas qu'il puisse y avoir pol�mique. Dans les r�glements, "faire main" inclus la main, le bras ET l'�paule. Point barre.  La m�connaissance du r�glement par les st�phanois est criante de v�rit�. Ils devraient plut�t s'estimer heureux de ne pas avoir r�colt� de carton rouge pour cette �paule "clairement volontaire" ! C'est fou cette mauvaise foi st�phanoise...
Signaler un abus R�agir
Dicaz a publi� le 27 Janvier 2015 � 13:50
Ok donc main derri�re le dos   y touche pas de la main met de l �paule penalty donc si ont reste dans la logique pourquoi pas de carton rouge ?? rien  maintenant les arbitres donne leurs propre r�gle c'est du n'importe quoi tous le monde dis les sp�cialiste et j en passe y a clairement pas penalty met faut encore la mauvaise fois du parisiens c �nervant !! Dans le cas contraire vous seriez les 1 er a passer votre haine sur le mauvais arbitrage je suis presser de voir votre �quipe ce ramasser a Chelsea avec votre club de l�gende avec un palmar�s a mourir de rire
Signaler un abus R�agir
Haramisgood a publi� le 27 Janvier 2015 � 10:02
"Les lois du jeu stipulent en effet que le bras comprend �galement l'�paule et qu'un contact avec le ballon entra�ne une sanction." Voil�, donc rien � ajouter c'est la r�gle et ceux qui n'aiment pas les r�gles du foot n'ont qu'� faire un autre sport.
Signaler un abus R�agir
Croixnivert a publi� le 26 Janvier 2015 � 21:49
Dans le r�glement, le haut de l'�paule = main...En plus le geste est volontaire, donc p�no et le joueur n'a qu'� s'en prendre qu'� lui-m�me !
Signaler un abus R�agir
75bez13 a publi� le 26 Janvier 2015 � 15:36
Nous ,on n'a rien dit quand l'arbitre accorde des coups francs sur Hamouma sur de grossi�res simulations sur ce match et en coupe de la ligue.
Anne o'n�mes a publi� le 26 Janvier 2015 � 14:19
Il ne savait pas quoi ? Rien dans le r�glement ne mentionne l'�paule.
Signaler un abus R�agir
Uncoco a publi� le 26 Janvier 2015 � 12:43
et le coup de pied de Tavanou sur Lucas qui merite un carton rouge c'est quoi un scandale?
Signaler un abus R�agir
Goupi a publi� le 26 Janvier 2015 � 11:53
Le joueur l'avoue lui-m�me, il prends volontairement le ballon avec l'�paule . A partir de l�, je ne vois pas comment l'arbitre pourrait ne pas siffler penalty ! C'est la premi�re fois que j'entends un joueur sortir une excuse pareille ! Il ne savait pas, lol !
Pascal a publi� le 26 Janvier 2015 � 11:06
Pourtant les poteaux n'�taient pas carr�s.
Signaler un abus R�agir
Pierre mais nes a publi� le 26 Janvier 2015 � 10:41
C'est hilarant d'entendre des journalistes expliquer que le r�glement dit que l'�paule fait partie du bras, puis s'�tonner que les joueurs ne connaissent pas la r�gle, alors qu'eux-m�mes n'ont pas v�rifi� l'info !  Que dit le r�glement ? -> "Il y a main lorsqu�il y a contact d�lib�r� entre le ballon et la main ou le bras."  Il ne dit donc rien sur l'�paule. Ce qui ne veut pas dire que l'�paule ne fait pas partie du bras. Mais le contraire non plus.  Bref, avant de critiquer l'ignorance des autres, il faut v�rifier ses propres connaissances...
