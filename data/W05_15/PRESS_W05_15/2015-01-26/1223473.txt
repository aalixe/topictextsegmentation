TITRE: Une collecte pour aider l'imprimerie victime des fr�res Kouachi - France - RFI
DATE: 26-01-2015
URL: http://www.rfi.fr/france/20150126-imprimerie-retranches-freres-kouachi-dammartin-goele-collecte-gign/
PRINCIPAL: 0
TEXT:
Modifi� le 26-01-2015 � 05:27
Une collecte pour aider l'imprimerie victime des fr�res Kouachi
Le site de l'imprimerie � Dammartin-en-Go�le, lors de l'assaut du GIGN, le vendredi 9 janvier 2015.
AFP PHOTO / JOEL SAGET
Apr�s l'assaut lanc� par les forces du GIGN contre les deux jihadistes retranch�s dans l'imprimerie de Dammartin-en-Go�le, le bilan est lourd. L'entreprise est � l'arr�t. Une collecte a �t� lanc�e pour remettre sur pied cette entreprise. Il leur faudrait atteindre la somme de 100�000 euros.
Impacts de balles sur la fa�ade, machines d'impression d�truites dans leur totalit�, mobilier saccag� Apr�s la tuerie de Charlie Hebdo, les fr�res Kouachi en cavale s'�taient retranch�s dans cette entreprise de Dammartin-en-Go�le. L'imprimerie CTD (Cr�ation Tendance D�couverte) n'a pas r�sist� � l'assaut lanc� par les forces du GIGN.
Pour relancer au plus vite l'activit� de cette entreprise, encore � l'arr�t aujourd�hui, l'association des commer�ants de la r�gion a eu une id�e originale�:�faire appel � la solidarit� de chacun. Pour collecter de l'argent, elle a lanc� un appel sur internet. Mise en ligne sur le site de cagnottes leetchi.com, ce fonds a d�j� d�pass� les 40�000 euros. Un d�but prometteur et encourageant, mais encore insuffisant pour couvrir tous les frais.
En attendant la suite, � savoir l'assurance qui devrait prendre en charge les co�ts des travaux de r�paration.
Chronologie et chiffres cl�s
