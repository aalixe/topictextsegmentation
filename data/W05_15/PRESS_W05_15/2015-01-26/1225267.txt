TITRE: Alexis Tsipras : «Je servirai toujours la Grèce et l'intérêt du peuple grec»
DATE: 26-01-2015
URL: http://www.lefigaro.fr/international/2015/01/26/01003-20150126ARTFIG00234-grece-le-leader-de-syriza-alexis-tsipras-va-former-son-gouvernement.php
PRINCIPAL: 0
TEXT:
Publié
le 26/01/2015 à 14:39
«Je servirai toujours la Grèce et l'intérêt du peuple grec», a déclaré Alexis Tsipras lors d'une prestation de serment. Crédits photo : ANGELOS TZORTZINIS/AFP
VIDÉO - Le président de Syriza a été désigné lundi premier ministre grec après s'être allié avec un parti de droite souverainiste. Il devrait annoncer d'ici mardi la composition de son gouvernement.
Publicité
� Alexis Tsipras nommé premier ministre
«Je servirai toujours la Grèce et l'intérêt du peuple grec.» En costume bleu, et comme à son habitude sans cravate, Alexis Tsipras a prêté serment lundi auprès du président de la République Carolos Papoulias. Cette prestation de serment civile est une première en Grèce, pays orthodoxe, où cette cérémonie revêt d'ordinaire un caractère religieux.
Le président de Syriza s'est allié lundi matin avec la formation de droite des Grecs indépendants . Les deux formations vont cumuler une majorité de 162 sièges sur 300 sans que la répartition des postes au sein du futur gouvernement ne soit encore connue. La composition devrait être dévoilée mardi. L'économiste Yanis Varoufakis devrait être chargé du portefeuille des Finances, ont déclaré à Reuters trois membres du parti de la gauche radicale.
� Une alliance gouvernementale contre l'austérité
Tout comme Syriza, les Grecs indépendants développent une rhétorique anti-austérité très ferme. Mais ils défendent par ailleurs des positions nationalistes et conservatrices sur le plan des m�urs. Ce parti a été formé après l'éclosion de la crise grecque en 2010. À sa tête: Panos Kammenos, un dissident issu de la droite Nouvelle-Démocratie. Lors des précédentes législatives de 2012, le parti est entré pour la première fois au Parlement en obtenant 20 sièges et 7,5% des voix. Les Grecs indépendants sont aujourd'hui la sixième force du Parlement.
Avant les élections, le parti souverainiste avait déjà annoncé son intention de s'allier avec Alexis Tsipras en vue de mettre fin «aux mesures de rigueur». C'est la ligne la plus ferme vis à vis des créanciers UE et FMI qui semble donc l'emporter aujourd'hui. Le nouvel homme fort de la Grèce douche les attentes de ses partenaires européens. Ces derniers souhaitaient voir la gauche radicale assouplir ses positions sur la dette et l'austérité, comme cela aurait été le cas s'il s'était allié avec une formation pro-européenne plus conciliante.
� Les Européens prêts à négocier à rééchelonnement
Les ministres des Finances de la zone euro se réunissaient lundi après-midi à Bruxelles. Au c�ur des discussions: les conséquences de l'alternance au pouvoir à Athènes. Pour l'heure, les Européens se disent prêts à accorder plus de temps à Athènes pour rembourser ses dettes, mais il n'est pas question de négocier une réduction de la dette comme entend le faire Syriza. Angela Merkel a ainsi rappelé la Grèce à ses engagements. «Faire partie de la zone euro signifie qu'il faut respecter l'ensemble des accords déjà passés», a de son côté déclaré le président de l'Eurogroupe, Jeroen Dijsselbloem. «Sur cette base, nous sommes prêts à travailler avec eux», ajoute-t-il. Le programme en cours d'assistance financière expire en l'état le 28 février.
