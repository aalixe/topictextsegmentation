TITRE: VIDEO. Mort du chanteur Demis Roussos
DATE: 26-01-2015
URL: http://www.francetvinfo.fr/culture/musique/video-mort-du-chanteur-demis-roussos_807787.html
PRINCIPAL: 1226694
TEXT:
/ Musiques
VIDEO. Mort du chanteur Demis Roussos
L'artiste grec, dont la voix et la barbe �taient connues dans le monde entier, s'est �teint � 68 ans.
(FRANCE 2)
, publi� le
26/01/2015 | 22:38
Art�mios Ventouris Rousos, plus connu sous le nom de Demis Roussos, est mort � l'�ge de 68�ans . Le Grec, ancien membre du groupe de rock progressif Aphrodite's Child, se fait conna�tre avec la sortie de Rain and Tears, enregistr� � Paris en mai 1968, dont les larmes font �cho � celles des protestataires aux yeux rougis par le gaz lacrymog�ne. Le titre devient l'un des slows de l'�t� les plus diffus�s en Europe et conna�tra un succ�s interplan�taire.
"La musique, c'est une philosophie"
� partir de 1971, Demis Roussos se lance dans une carri�re solo et sort de nombreux autres tubes. Il prend l'apparence d'un colosse hirsute, d'un homme au physique atypique. "La musique, c'est une philosophie", aimait-il � dire. Demis l'oriental a ensuite fait un d�tour par la musique new age. Depuis 2009, la maladie l'avait contraint � r�duire son activit�, et il s'�tait r�fugi� � Ath�nes, o� il s'est �teint.
Le JT
Les autres sujets du JT
1
