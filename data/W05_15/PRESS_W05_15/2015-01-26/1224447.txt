TITRE: Sill�-le-Guillaume.  Grippe saisonni�re. Point de situation et rappel des r�gles d�hygi�ne � Article � Les Alpes Mancelles
DATE: 26-01-2015
URL: http://www.lesalpesmancelles.fr/2015/01/26/grippe-saisonniere-point-de-situation-et-rappel-des-regles-d%25E2%2580%2599hygiene/
PRINCIPAL: 1224445
TEXT:
Accueil
Sill�-le-Guillaume Grippe saisonni�re. Point de situation et rappel des r�gles d�hygi�ne
La surveillance �pid�miologique de la grippe saisonni�re indique une activit� forte en m�tropole, la semaine 3 de 2015 �tant la premi�re semaine de d�passement du seuil �pid�mique.
26/01/2015 � 11:09 par chafik
Partages Facebook Twitter Google + Email
Les cas de grippe recens�s � ce jour sont principalement dus � la circulation de la souche A(H3N2), qui peut entra�ner des complications s�v�res chez les personnes fragiles et particuli�rement chez les personnes �g�es.
Pour les personnes identifi�es � risque de complication et pour lesquelles la vaccination contre la grippe est recommand�e, la Direction g�n�rale de la Sant� rappelle l�int�r�t de la vaccination, m�me en d�but de phase �pid�mique.
Les professionnels de sant�, notamment ceux en contact r�gulier avec des personnes �g�es r�sidant en collectivit�, ont �t� incit�s � la plus grande vigilance.
La vaccination reste la meilleure protection
M�me si la vaccination reste la meilleure protection contre la grippe, en particulier chez les professionnels de sant� et les personnes � risque, il existe des mesures d�hygi�ne simples qui contribuent � limiter la transmission de la maladie de personne � personne.
Il est recommand� aux personnes malades, d�s le d�but des sympt�mes, de :
- limiter les contacts avec d�autres personnes et en particulier avec les personnes � risque ou fragiles et �ventuellement de porter un masque chirurgical en leur pr�sence ;
- se laver r�guli�rement les mains � l�eau et au savon, ou les d�sinfecter par friction avec une solution hydroalcoolique ;
- se couvrir la bouche et le nez � chaque fois qu�elles toussent ou �ternuent ;
- se moucher dans des mouchoirs en papier � usage unique.
Il est recommand� aux personnes de l�entourage du malade ainsi qu�aux professionnels de sant�
- d��viter les contacts rapproch�s avec ces derni�res, si elles sont � risque ;
- de se laver r�guli�rement les mains et particuli�rement apr�s tout contact avec le malade ou le mat�riel utilis� par le malade ;
- de nettoyer les objets couramment utilis�s par celui-ci.
InVS : Bulletins �pid�miologiques grippe : http://www.invs.sante.fr/Dossiers-thematiques/Maladies-infectieuses/Maladies-a-prevention-vaccinale/Grippe/Grippe-generalites/Donnees-de-surveillance/Bulletin-epidemiologique-grippe.-Point-au-21-janvier-2015
