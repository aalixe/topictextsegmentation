TITRE: Rapatriement du corps de Herv� Gourdel - Lib�ration
DATE: 26-01-2015
URL: http://www.liberation.fr/societe/2015/01/26/rapatriement-du-corps-de-herve-gourdel_1189319
PRINCIPAL: 0
TEXT:
Lire sur le readerMode zen
Le corps du Fran�ais Herv� Gourdel, enlev� puis d�capit� en septembre par des jihadistes alg�riens, a �t� rapatri� lundi en France, onze jours apr�s sa d�couverte en Kabylie. La d�pouille du guide de haute montagne est arriv�e � l�a�roport de Roissy-Charles-de-Gaulle, � bord d�un vol Air France en provenance d�Alger.
Une courte c�r�monie de recueillement s�est tenue, � l��cart des journalistes mais en pr�sence du ministre des Affaires �trang�res, Laurent Fabius, et de Christian Estrosi, d�put� UMP des Alpes-maritimes, d�partement d�o� �tait originaire Herv� Gourdel. Ces derniers comme les proches du d�funt sont repartis sans faire de d�clarations. Selon une source a�roportuaire, le corps du guide de haute montagne devait �tre transport� � l�institut m�dico-l�gal de Paris.
Herv� Gourdel, 55�ans, avait �t� kidnapp� le 21�septembre par le groupe jihadiste Jund al-Khilafa, qui a affirm� l�avoir ex�cut� en repr�sailles � l�engagement de la France aux c�t�s des Etats-Unis dans les frappes contre l�Etat islamique (EI) en Irak et en Syrie. Arriv� en Alg�rie deux jours plus t�t pour faire du trekking dans le massif du Djurdjura, il avait �t� enlev� pr�s du sommet Lalla Khedidja. Les cinq compagnons alg�riens avec qui il se trouvait avaient �t� rel�ch�s apr�s quatorze heures de s�questration.
