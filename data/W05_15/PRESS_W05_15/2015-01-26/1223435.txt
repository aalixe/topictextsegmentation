TITRE: 1er tour des municipales d'Ajaccio : r�action de Francis Nadizi - Alta Frequenza
DATE: 26-01-2015
URL: http://www.alta-frequenza.com/l_info/l_actu/1er_tour_des_municipales_d_ajaccio_reaction_de_francis_nadizi_73031
PRINCIPAL: 1223434
TEXT:
Bienvenue sur alta-frequenza.com, la radio Corse !
Derni�re mise � jour de notre site le 21 f�vrier � 8h45
Info corse � L'actu �1er tour des municipales d'Ajaccio : r�action de Francis Nadizi
1er tour des municipales d'Ajaccio : r�action de Francis Nadizi
Publi� le 25/01/2015, 22h53
Tweet
(Alex Bertocchini - Alta Frequenza) -�Premier tour des municipales d'Ajaccio : avec 1510 voix, le Front National ne parvient pas � r�aliser le score de la pr�c�dente �lection o� il avait totalis� 8,28% des voix contre 6,82% aujourd�hui. Ajaccio Bleu Marine, et un peu contre toute attente vu le contexte international, n�a pas r�ussi � franchir cette fameuse barre des 10% qui lui aurait permis de se maintenir au second tour. Il se situe tout de m�me dans la fourchette qui lui permettrait de fusionner, ce qui semble, vu le positionnement du mouvement sur le plan national, assez peu probable, y compris pour une municipale partielle.
Ecoutez Francis Nadizi, leader d�Ajaccio Bleu Marine.
