TITRE: A la Une | Taux d'alcool�mie � 0,2g/l pour les jeunes et fin des oreillettes au volant
DATE: 26-01-2015
URL: http://www.dna.fr/actualite/2015/01/26/3-388-morts-sur-les-routes-en-2014-3-7-de-plus-qu-en-2013
PRINCIPAL: 1226509
TEXT:
Taux d'alcool�mie � 0,2g/l pour les jeunes et fin des oreillettes au volant
S�CURIT� ROUTI�RE Taux d'alcool�mie � 0,2g/l pour les jeunes et fin des oreillettes au volant
Faits divers
16h50 : Bernard Cazeneuve a pr�sent� de nouvelles mesures pour lutter contre la mortalit� routi�re, suite � l'officialisation des mauvais chiffres de 2014 :
- Abaisser le taux l�gal d�alcool�mie de 0,5 gramme par litre de sang � 0,2 g/l pour les conducteurs novices (trois ans apr�s le permis, ou deux ans s�il a �t� pr�c�d� d�un apprentissage par conduite accompagn�e).
- Interdire le stationnement des v�hicules (hors deux-roues) cinq m�tres avant les passages pi�tons pour am�liorer la visibilit�.
- Interdire de porter �couteurs, oreillettes, casques... susceptibles de limiter l�attention et l�audition des conducteurs.
- Permettre aux maires d�abaisser la vitesse sur de grandes parties, voire sur la totalit� de l�agglom�ration.
- Exp�rimenter, sur certains tron�ons de route � double sens identifi�s comme particuli�rement accidentog�nes, une diminution de la vitesse maximale autoris�e de 90 � 80 km/h.
- Renforcer les sanctions pour les conducteurs qui, stationnant sur les passages pi�tons, trottoirs ou pistes cyclables, mettent en danger les pi�tons en les obligeant � les contourner.
- Exp�rimenter dans onze d�partements le d�pistage de stup�fiants par le double pr�l�vement salivaire.
- Relancer le d�ploiement des radars feux rouges et leur associer syst�matiquement un module de contr�le de la vitesse, notamment en agglom�ration.
- Poursuivre la modernisation des 4.200 radars (radars chantiers pour la s�curit� des personnels, radars mobile de nouvelle g�n�ration).
- D�ployer des radars double-face pour mieux identifier les auteurs des infractions. - Uniformiser la taille et le format des plaques d�immatriculation des deux-roues motoris�s, pour faciliter les contr�les.
- Rendre obligatoire pour les usagers de deux-roues motoris�s le port du gilet de s�curit� en cas d�arr�t d�urgence.
- Exiger, lors de la demande de certification d�immatriculation, la d�signation d�une personne titulaire du permis de conduire responsable en cas d�infraction si le conducteur n�a pas pu �tre identifi�.
- Moderniser l�enseignement de la conduite.
- G�n�raliser un module de sensibilisation � la s�curit� routi�re en classe de seconde � la rentr�e 2015, ainsi que lors des journ�es de d�fense et de citoyennet�.
- D�velopper des op�rations de sensibilisation aux risques li�s aux pratiques addictives et � l�utilisation du t�l�phone portable au volant, et lancement en f�vrier d�une campagne d�information sur les bless�s de la route.
- R�duire les risques de contresens sur autoroute par l�installation de panneaux �sens interdit� sur fond r�tror�fl�chissant sur les bretelles de sortie.
- Former tous les m�decins agr��s au d�pistage pr�coce des probl�mes d�alcool et de stup�fiants.
16h24 : Le nombre de morts sur les routes fran�aises a augment� de 3,7% en 2014, premi�re ann�e de hausse depuis douze ans, a annonc� lundi le ministre de l�Int�rieur Bernard Cazeneuve. Il y a eu 3.388 personnes tu�es sur les routes en 2014, soit 120 de plus qu�en 2013, qui avait marqu� un record � la baisse depuis 1948, ann�e des premi�res statistiques.
Ces chiffres ne sont �pas � la hauteur des objectifs que nous nous �tions fix�s�, a d�clar� le ministre, qui �maintient� l�objectif fix� l�an dernier de faire baisser � 2.000 le nombre de morts sur les routes en 2020. �2014 n�est pas aussi sombre qu�on pourrait le croire dans la mesure o� c�est la deuxi�me moins mauvaise ann�e� depuis que des statistiques sont �tablies, a soulign� M. Cazeneuve.
Le nombre de morts sur les routes est en baisse r�guli�re depuis 1973. Cette ann�e-l�, les autorit�s avaient recens� plus de 18.000 morts en France. La mortalit� a ainsi �t� divis�e par cinq en un peu plus de quarante ans en France.
Vos commentaires
