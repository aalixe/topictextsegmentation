TITRE: OL : Riolo voit deux probl�mes � venir pour Lyon
DATE: 26-01-2015
URL: http://www.butfootballclub.fr/1601879-ol-riolo-voit-deux-problemes-a-venir/
PRINCIPAL: 1225032
TEXT:
OL : Riolo voit deux probl�mes � venir pour Lyon
Post� le 27 janvier 2015
Commentez cet article
��
Si l�Olympique Lyonnais est toujours leader du championnat de France, la blessure d�Alexandre Lacazette est pr�occupante pour l�avenir. Pour Daniel Riolo, le fait de croiser le fer avec l�AS Monaco ce week-end est �galement emb�tante�
� Devant, l�OL est toujours leader, apr�s sa victoire face � Metz (2-0). Franchement qui envisageait un faux pas Lyonnais ? R�ussite, confiance et m�me les faits de jeu, tout tourne. La pression de la premi�re place n�existe pas. Mais pour la premi�re fois depuis plusieurs semaines, il y a un � mais �, commente le consultant de RMC sur son blog. L�OL est en effet suspendu � la blessure de Lacazette. Le caillou dans la pompe qu�il fallait �viter. �a, plus le d�placement � Monaco dimanche prochain, �a suffit � faire entrer le doute �.
