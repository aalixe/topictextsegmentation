TITRE: La Chine, bient�t premier march� mondial de l'iPhone d'Apple ?
DATE: 26-01-2015
URL: http://www.boursier.com/actualites/economie/la-chine-1er-marche-mondial-de-l-iphone-d-apple-26872.html
PRINCIPAL: 1224595
TEXT:
Cr�dit photo ��Reuters
(Boursier.com) � L'offensive commerciale d'Apple sur la Chine semble porter ses fruits... Selon des analystes cit�s par le 'Financial Times' du jour, le march� chinois aurait ainsi d�pass� pour la premi�re fois celui des Etats-Unis pour les ventes d'iPhones. Selon UBS, la Chine aurait ainsi repr�sent� 36% des iPhones vendus par la firme de Cupertino au 4�me trimestre 2014, contre 24% pour les Etats-Unis et 40% pour le reste du monde. A titre de comparaison, en 2013, la Chine avait repr�sent� 22% des ventes et les Etats-Unis 29%, ajoute UBS...
Accord avec China Mobile et succ�s de l'iPhone 6
Cette avanc�e historique a pu �tre r�alis�e gr�ce � la signature d'un accord, en d�cembre 2013, entre Apple et China Mobile, le premier op�rateur mobile chinois, pour commercialiser l'iPhone en Chine. Le succ�s de l'iPhone 6, lanc� en octobre dernier en Chine, a aussi permis de faire bondir les ventes.
Tim Cook, le directeur g�n�ral d'Apple, avait indiqu� en 2013 qu'il s'attendait � ce que la Chine d�passe � terme les Etats-Unis comme source de revenus pour le groupe, mais il n'avait pas donn� de calendrier... Selon le 'FT', Apple devrait confirmer officiellement cette semaine cette avanc�e d�cisive en Chine, au moment o� son grand rival Samsung (no1 mondial des smartphones) y perdu des parts de march�, notamment en raison de la concurrence du constructeur low-cost chinois Xiaomi, qui voit ses ventes s'envoler depuis plusieurs trimestres.
Les smartphones Android restent en t�te en Chine
Pour la firme � la pomme, les perspectives de croissance devraient continuer � �tre sup�rieures en Chine, march� en expansion, qu'aux Etats-Unis, march� d�sormais mature. Le groupe pourrait notamment s�duire encore davantage les clients chinois cette ann�e, avec la sortie de sa montre connect�e Apple Watch, estiment les analystes.
Le g�ant am�ricain continuera cependant de faire face � une forte concurrence des smartphones et autres appareils mobiles fonctionnant sous Android (le syst�me d'exploitation de Google), qui dominent � ce jour le march� chinois...
Victoria Adam � �2015, Boursier.com
