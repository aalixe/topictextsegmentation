TITRE: Libye:L'envoy� sp�cial de l'ONU plaide pour une force de maintien de la paix
DATE: 26-01-2015
URL: http://www.maghrebemergent.com/actualite/breves/fil-maghreb/44789-libye-l-envoye-special-de-l-onu-plaide-pour-une-force-de-maintien-de-la-paix.html
PRINCIPAL: 1225106
TEXT:
lundi 26 janvier 2015 11:50
Libye:L'envoy� sp�cial de l'ONU plaide pour une force de maintien de la paix
Soyez le premier � commenter!
L'envoy� sp�cial des Nations unies en Libye, Bernardino Leon, a soulign� la n�cessit� de la pr�sence d'une force internationale de maintien de la paix en Libye pour pr�server la stabilit� dans le pays, m�me si les n�gociations de Gen�ve qui doivent reprendre ce lundi r�ussissent � mettre fin � la guerre civile.
Des consultations ont d�j� �t� men�es avec les Etats-Unis, la France, la Grande-Bretagne et d'autres pays sur la n�cessit� de la pr�sence d'observateurs militaires et civils en Libye et � ses fronti�res en cas d'accord, a r�v�l� M. Leon lors d'une une interview accord�e au journal britannique 'Financial Times' et reprise dimanche par des m�dias libyens.
Il a soulign� l'importance pour les parties libyennes de soutenir le travail des observateurs.
M. Leon a de nouveau soulign� l'importance du dialogue en Libye, assurant que l'objectif principal du dialogue est de parvenir � la stabilit� et la formation d'un gouvernement d'unit� nationale, qui exige un cessez-le-feu et emp�che la circulation des armes.
'Les Nations unies auront besoin, dans ce cadre, de la surveillance de tous les ports et les a�roports et les entr�es du pays', a ajout� l'�missaire onusien, pr�cisant: 'nous allons demander aux groupes arm�s d'�tablir un calendrier pour quitter toutes les villes, abandonner les a�roports et les installations importantes, ce qui exigera une surveillance internationale'.
Evoquant le probl�me d'ins�curit�, l'envoy� de l'ONU a d�clar� que 'le terrorisme �tait un probl�me majeur en Libye et qu'il n'�tait pas limit� � la r�gion de l'Est uniquement, mais se d�veloppe dans le Sud et l'Ouest, o� il peut atteindre jusqu'� l'Alg�rie et la Tunisie, vu que ils (groupes terroristes) ont des camps d'entra�nement dans le Sud et ils ont des racines l�-bas, ainsi que des activit�s terroristes � Tripoli.
Jusqu'� pr�sent, le nombre de ces groupes n'est pas consid�rable, mais cette situation pourrait changer en quantit� et en qualit� dans le cas ou les efforts pour parvenir � un accord �choueraient'.
Il a reconnu qu'il n'�tait pas difficile de parvenir � un accord entre les diff�rentes parties parce que les diff�rences entre elles ne sont pas substantielles.
Pana
