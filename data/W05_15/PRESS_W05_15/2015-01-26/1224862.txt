TITRE: SOS M�decins poursuit son mouvement lundi jusqu'� 20 heures
DATE: 26-01-2015
URL: http://www.bfmtv.com/societe/sos-medecins-poursuit-son-mouvement-lundi-jusqu-a-20-heures-859574.html
PRINCIPAL: 1224858
TEXT:
Imprimer
L'association SOS M�decins a annonc� lundi qu'elle poursuivait jusqu'� lundi 20 heures son mouvement de "suspension d'activit�".
"En cause, l'absence totale de proposition de la part du gouvernement si ce n'est une +�pid�mie+ de r�quisitions tous azimuts envers nos m�decins et nos structures", explique-t-elle dans un communiqu�.
L'association qui regroupe environ 1.100 m�decins dans 63 structures avait suspendu dimanche ses activit�s pour 24 heures.
Son appel a �t� massivement suivi, engendrant d'importantes perturbations malgr� les r�quisitions, ont indiqu� dimanche ses responsables.
Par D. N. avec AFP
D. N. avec AFP
