TITRE: Bettencourt: l'un des pr�venus entre la vie et la mort apr�s une tentative de suicide | Site mobile Le Point
DATE: 26-01-2015
URL: http://www.lepoint.fr/societe/proces-bettencourt-les-principaux-protagonistes-sont-arrives-au-tribunal-26-01-2015-1899588_23.php
PRINCIPAL: 0
TEXT:
26/01/15 � 09h58
Bettencourt: l'un des pr�venus entre la vie et la mort apr�s une tentative de suicide
Avant m�me d'entrer dans le vif du sujet, le premier proc�s de "l'affaire Bettencourt" s'est ouvert lundi � Bordeaux sur un coup de th��tre: l'annonce de la tentative de suicide d'un des pr�venus, Alain Thurin, l'ancien infirmier de la milliardaire Liliane Bettencourt, entre la vie et la mort � l'h�pital.
La veille de l'audience o� il devait �tre jug� pour "abus de faiblesse" au d�triment de la milliardaire, Alain Thurin, 64 ans, entr� au service des Bettencourt en 2007, "aurait tent� d'attenter � ses jours" en se pendant dans un bois pr�s de son domicile, en r�gion parisienne, a annonc� le procureur G�rard Aldig�.
"Nous ne savons pas s'il est mort ou vivant", a pr�cis� le pr�sident du tribunal correctionnel, Denis Roucou. L'ex-infirmier n'avait pas d'avocat pour le repr�senter au proc�s.
Alain Thurin "est � l'h�pital entre la vie et la mort" apr�s avoir �t� d�couvert par un passant dans un parc de Br�tigny-sur-Orge (Essonne), a pr�cis� une source polici�re.
Un des avocats de Liliane Bettencourt, Me Beno�t Ducos-Ader, a d�plor� un �v�nement "particuli�rement troublant et grave, car cet homme a �t� aux c�t�s de Mme Bettencourt pendant tr�s longtemps". "C'est quelqu'un qu'on ne voyait pas, il est venu s'expliquer devant le juge, mais mes clients n'avaient absolument aucune nouvelle de lui ", a-t-il ajout�.
Au total, dix personnes sont poursuivies dans le premier volet de ce dossier tentaculaire, pour la plupart soup�onn�es d'avoir profit� de la vuln�rabilit� de la richissime nonag�naire h�riti�re du groupe de cosm�tiques L'Or�al.
Sur le banc des pr�venus s'alignaient lundi les costumes sombres, � la coupe impeccable, devant les rangs tr�s fournis des avocats. Non loin l'un de l'autre, deux des principaux pr�venus, Patrice de Maistre, gestionnaire de fortune de Mme Bettencourt, et le photographe Fran�ois-Marie Banier, confident de la milliardaire.
Tous deux sont poursuivis pour "abus de faiblesse" et "blanchiment" au d�triment de la vieille dame, aujourd'hui �g�e de 92 ans, alors qu'elle souffrait de s�nilit� depuis septembre 2006. Des d�lits passibles chacun de trois ans d'emprisonnement et de 375.000 euros d'amende.
Sur le m�me banc, l'ex-ministre UMP Eric Woerth. Le d�put� de l'Oise est poursuivi pour "recel" d'une somme qu'il aurait re�ue de Patrice de Maistre, alors qu'il �tait tr�sorier de la campagne pr�sidentielle de Nicolas Sarkozy, ce que nient les deux hommes. Mis en examen au printemps 2013, l'ex-pr�sident a depuis b�n�fici� d'un non-lieu.
- Centaines de millions d'euros -
Dons manuels ou lib�ralit�s par millions d'euros, faramineux contrats d'assurance-vie, donations d'oeuvres d'art : le proc�s, sur cinq semaines, devrait braquer ses projecteurs sur les coulisses de l'h�tel particulier de la 11e fortune mondiale � Neuilly-sur-Seine (Hauts-de-Seine), ses usages et ses rivalit�s internes.
A lui seul, Fran�ois-Marie Banier est accus� d'avoir per�u plus de 400 millions d'euros de la part de Liliane Bettencourt, sans compter les autres cadeaux consentis au compagnon du photographe, Martin d'Orgeval, lui aussi poursuivi.
Des montants � la fois �normes et d�risoires au regard de la fortune de l'h�riti�re de l'empire L'Or�al (plus de 30 milliards d'euros selon le magazine Forbes).
"La dimension de ce proc�s ne doit pas faire oublier les actes", a temp�r� Me Arnaud Dupin, autre avocat de Liliane Bettencourt. "Cette femme de 92 ans a droit � ce que justice lui soit rendue face � tant d'abus, tant de d�lits commis a son encontre", a-t-il d�clar�.
La milliardaire, sous tutelle, "plus qu'affaiblie sur le plan psychique", mais "vivant � quelques m�tres de sa fille" selon ses avocats, sera la grande absente du proc�s.
- Absence d'un t�moin capital ? -
Autre absence remarqu�e lundi, celle de Claire Thibout, ex-comptable de Liliane Bettencourt et principal t�moin � charge dans ce volet d'abus de faiblesse. Certificat m�dical � l'appui, Mme Thibout a fait savoir par le biais de son avocat que son �tat de sant� ne lui permettait pas de se pr�senter devant le tribunal pour "une dur�e ind�termin�e".
Absence d'autant plus g�nante que, parmi une kyrielle d'incidents de proc�dures d�j� annonc�e, plusieurs avocats de la d�fense ont l'intention de demander un report du proc�s en invoquant la mise en examen � Paris de l'ex-comptable pour "faux t�moignages".
L'avocate de Patrice de Maistre, Jacqueline Laffont, a aussit�t demand� au tribunal "des pr�cisions sur cette impossibilit�" d'entendre un "t�moin capital" dans cette affaire.
Le tribunal "verra ce qu'il fera", a r�torqu� le pr�sident Roucou, suspendant l'audience jusqu'� mardi � 9H15 pour d�lib�rer sur une demande de la d�fense concernant une question prioritaire de constitutionnalit� (QPC) portant sur la notion de "blanchiment". Au cas o� le tribunal jugerait que cette QPC m�rite d'�tre transmise � la Cour de cassation, les d�bats seraient suspendus.
Hormis Alain Thurin, seul un pr�venu manquait � l'appel: l'ex-gestionnaire de l'�le seychelloise de Mme Bettencourt, Carlos Cassina Vejarano.
Partie civile au proc�s, la fille unique de Liliane Bettencourt, Fran�oise Bettencourt-Meyers, �tait �galement pr�sente lundi. C'est par elle, et une plainte visant Fran�ois-Marie Banier, que l'affaire avait d�but� fin 2007.
26/01/2015 17:44:51 - Bordeaux (AFP) - Par Philippe BERNES-LASSERRE, Jordane BERTRAND - � 2015 AFP
