TITRE: Pogba humilie 4 adversaires et marque ! - Football - Sports.fr
DATE: 26-01-2015
URL: http://www.sports.fr/football/italie/articles/pogba-impressionne-la-planete-foot-1174008/
PRINCIPAL: 1223143
TEXT:
25 janvier 2015 � 16h52
Mis à jour le
25 janvier 2015 � 21h21
Paul Pogba a encore sorti le grand jeu dimanche, à l'occasion de la victoire de la Juventus Turin face au Chievo Verone (2-0) pour le compte de la 20e journée de Serie A. Le milieu de terrain français a fait étalage de toute sa classe technique avant de marquer un but venu d'ailleurs.
Si pour la Juventus Turin , Paul Pogba représentait jusqu'alors une valeur marchande flirtant avec les 100 millions d'euros, ses prétendants vont sans doute apprendre que son prix a encore été revu à la hausse après sa prestation de dimanche face au Chievo Verone, un match comptant pour la 20e journée de Serie A (2-0). Si les leaders du championnat d'Italie ont mis le temps avant de trouver leur rythme de croisière et la faille dans la défense adverse, c'est le jeune Tricolore qui a sorti les siens du piège.
Encore une fois, l'ancien Red Devil de Manchester a sorti le grand jeu, mais avec plus de succès que face à l' Inter Milan il y a peu (1-1).
Le Bianconero a d'abord humilié tranquillement quatre adversaires dans l'entrejeu pour ressortir tranquillement le cuir de la nasse, et a continué son show en signant un but brillant du pied gauche après avoir éliminé son vis-à-vis d'un geste technique qui avait fait la réputation du grand Romario au FC Barcelone . Il aurait même pu réussir un autre but étonnant, si Albano Bizzarri n'avait pas sorti une belle parade dont a su profiter Stephan Lichtsteiner . Admirez...
“ @FootyAccums : POGBA PLEASE STOP! https://t.co/GkTCXRTsxv �?
� Ayo Ogunmoyin (@Ayobamii_O) 25 Janvier 2015
" @VineFootFrance : Le but de Paul Pogba avec la Juve ! https://t.co/wRbVcsdkR5 " ce type vient d'une autre planète
� Thomas Dufant (@justbeliketom) 25 Janvier 2015
Le but de Lichtsteiner. Si Pogba marque juste avant................ #JuveChievo https://t.co/swvbYwe9sk
