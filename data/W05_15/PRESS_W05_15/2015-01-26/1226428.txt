TITRE: CAN 2015: La d�ception du Burkina Faso, l'exploit du Congo et la sensation de la Guin�e �quatoriale
DATE: 26-01-2015
URL: http://www.linfodrome.com/sport/19011-can-2015-la-deception-du-burkina-faso-l-exploit-du-congo-et-la-sensation-de-la-guinee-equatoriale
PRINCIPAL: 1226426
TEXT:
Football
CAN 2015: La d�ception du Burkina Faso, l'exploit du Congo et la sensation de la Guin�e �quatoriale
par Kouakou N'dri | Publi� le 26/1/2015 � 10:15 dans Sport | source : Linfodrome
ses articles
(Photo d'archives)
La Guin�e �quatoriale et le Congo se sont qualifi�s pour les quarts de finale de la Coupe d'Afrique des Nations (CAN) 2015, au d�triment du Gabon et du Burkina Faso, �limin�s.
La Guin�e �quatoriale, pays-h�te de la CAN �dition 2015, victorieux du Gabon d'Aubameyang (2-0) et le Congo de Claude Le Roy, qui a domin� le Burkina Faso (2-1) sont les deux premiers pays � avoir d�croch� leur qualification pour les quarts de finale de la CAN-2015, dimanche 25 janvier 2015. Terminant � la t�te du groupe A avec 7 points+2, les congolais affronteront en quart de finale le 2�me de la poule B, tandis que les �quato-guin�ens, deuxi�me avec 5 points+2 seront oppos�s au vainqueur de ce m�me groupe, le 31 janvier 2015.
Grand exploit pour le Congo
L'exploit est �norme pour les Diables Rouges du Congo et leur s�lectionneur Claude Le Roy, qui depuis 1992 n'ont plus connu les joies du Top 8 continental. Le technicien fran�ais de 66 ans a encore fait des miracles pour sa 8�me Coupe d'Afrique des Nations en franchissant l'obstacle du premier tour pour la 7�me fois de sa carri�re. Un record dont il est le seul � d�tenir le secret. Les h�ros congolais avec leurs buteurs, Bifouma � la 51minute, auteur de son 2e but de la comp�tition et Nguessi � la 87 minute, alors que Banc� avait entre-temps r�ussi � �galiser � la minute 86, sont les outsiders de ce tournoi qui ont fini en ordre les premiers tours. Mais attention, ils doivent rester vigilants et concentrer pour la suite, s'ils souhaitent atteindre le sommet.
Finaliste de la derni�re �dition, le Burkina n'a que d��u
Le Burkina Faso, finaliste de la derni�re �dition, quitte en revanche le tournoi la t�te basse et aura �tal� trop de lacunes pour esp�rer faire mieux qu'une derni�re position dans sa poule. Les �talons n'ont que d��u! Dans l'ensemble, sur trois match, les vice-champions n'ont pus scorer qu'une seule fois. Eux qui, � l'absence du Nigeria, champion en titre �taient beaucoup attendus dans cette comp�tition, apr�s leur brillante participation en 2013. Oui! C'est �a la comp�tition de haut niveau. Les ann�es passent et ne se ressemblent pas. Le football d'hier n'est forcement celui d'aujourd'hui et celui d'aujourd'hui ne peut pas ressembler � celui d'autrefois. Si l'�limination des Burkinab�s �tait pr�visible, la sensation de la soir�e est venue de Bata o� la Guin�e �quatoriale a surpris les L�opards gabonais pour s'inviter en quart de finale gr�ce � un penalty de Balboa (55�me) et une 2�me r�alisation d'Iban (88�me).
La Guin�e �quatoriale fait sensation
L�histoire s'est donc r�p�t�e pour l'organisateur de la CAN, qui avait �t� � pareille f�te en 2012 lors d'une �preuve co-organis�e avec le Gabon. Comme il y a 3 ans, le Nzalang Nacional s'est appuy� sur des joueurs r�cup�r�s aux quatre coins de la plan�te et a chang� de s�lectionneur juste avant le coup d'envoi de la Coupe d'Afrique. Et dire que le pays avait �t� disqualifi� au tour pr�liminaire pour avoir align� un joueur non �ligible avant d'�tre rep�ch� en novembre 2014 avec le retrait de l'organisation au Maroc par la Conf�d�ration Africaine de Football. En contrario, la d�sillusion est immense pour le Gabon, qui avait tout mis� sur sa star Pierre-Emerick Aubameyang. L'attaquant de Dortmund ambitionnait de devenir l'�gal des deux l�gendes Samuel Eto'o et Didier Drogba, qui ont pris leur retraite internationale. Il doit largement d�chanter et n'aura trouv� qu'une seule fois le chemin des filets en trois rencontres.
Kouakou N'dri
Sauf autorisation de la r�daction ou partenariat pr�-�tabli, la reprise des articles de linfodrome.com, m�me partielle, est strictement interdite. Tout contrevenant s�expose � des poursuites
�
