TITRE: Open d'Australie (Hommes) -     Kei Nishikori a d�roul� et jouera Wawrinka en quarts
DATE: 26-01-2015
URL: http://www.lequipe.fr/Tennis/Actualites/Nishikori-a-deroule/531374
PRINCIPAL: 1223997
TEXT:
a+ a- imprimer RSS
Kei Nishikori retrouvera mercredi Stan Wawrinka en quarts de finale.   (Reuters)
Oui, il n'y a aucun Fran�ais qualifi� pour la deuxi�me semaine de l'Open d'Australie. Mais l'un d'eux a indirectement pes�, lundi sur le huiti�me de finale entre Kei Nishikori (5e) et David Ferrer (10e), remport� par le premier ( 6-3, 6-3, 6-3 , en 2h01). Samedi, Gilles Simon avait pouss� l'Espagnol � disputer quatre sets et, surtout, � livrer une intense bataille du fond du court pendant 3h37.
Au bord des crampes lors de la poign�e de main, Ferrer n'a visiblement pas eu assez de 48 heures pour r�cup�rer physiquement : sur la Rod Laver Arena, il a �t� �touff� du premier au dernier point, incapable de diriger les �changes avec son coup droit. � maintenant 32 ans, la mobylette espagnole n'a plus le m�me moteur qu'il y a quelques ann�es. C'est le troisi�me Grand Chelem d'affil�e dans lequel Ferrer ne rallie les quarts de finale.�
A neat score & neat performance from #Nishikori , who advances to his 2nd #ausopen q/f w. a 6-3 6-3 6-3 win v #Ferrer pic.twitter.com/PJAiZnX66F
� Australian Open (@AustralianOpen) January 26, 2015
Nishikori-Wawrinka, un sommet du dernier US Open
Pour Kei Nishikori, la dynamique est inverse et l'heure reste � l'ascension : �finaliste du dernier US Open, le Japonais (25 ans) se qualifie pour la troisi�me fois en quart de finale d'un Majeur. Bouscul� au deuxi�me et troisi�me tour (un set abandonn� � Dodig et Johnson), le 5e mondial est mont� en puissance lundi, r�ussissant 42 coups gagnants et 5 breaks et gagnant presque la moiti� des points sur le service adverse (48%, 47 sur 98). Apr�s avoir domin� pour la cinqui�me fois d'affil�e Ferrer, Nishikori retrouvera mercredi Stan Wawrinka en quarts de finale. Leur dernier affrontement avait �t� un sommet du dernier US Open (succ�s de Nishikori, 6-4 au 5e set).�
F.M.
