TITRE: Actustar.com - Ryan Reynolds : � j�ai essay� l�allaitement �
DATE: 26-01-2015
URL: http://www.actustar.com/74135/ryan-reynolds-jai-essaye-lallaitement/
PRINCIPAL: 1225387
TEXT:
Ryan Reynolds : � j�ai essay� l�allaitement �
+
Accueillir un nouvel enfant rend fou les stars. Certaines se mettent � manger du placenta, d'autres se mettent � allaiter. Quoi de plus normal, nous diriez-vous, que d'allaiter son enfant�? Pas lorsque la star en question est un homme�!
Ryan Reynolds�: un papa d�vou�
Ryan Reynolds, qui a recement acceuilli son premier enfant avec Blake Lively, a en effet profit� du Festival du film de Sundance pour faire quelques r�v�lations sur son r�le de p�re. Nous apprenons ainsi que c'est un papa d�vou�, pr�t � tout pour son enfant� y compris � allaiter�! Il confie ainsi�:���C'est bizarre � quel point on peut �tre fatigu� et heureux � la fois� J'ai fait toute sorte de choses. J'ai m�me essay� d'allaiter. C'�tait frustrant pour le b�b� et d�rangeant pour moi� Les choses ne se sont pas bien pass�es.�� Pauvre b�b�!
L'acteur qui a fait toute sorte de confidences, ne nous a malheureusement pas r�v�l� le pr�nom de sa petite princesse.
Nous finirons bien par le savoir Ryan�
