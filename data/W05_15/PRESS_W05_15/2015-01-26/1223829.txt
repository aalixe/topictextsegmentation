TITRE: VIDEOS. La Colombienne Paulina Vega �lue Miss Univers 2014
DATE: 26-01-2015
URL: http://www.leparisien.fr/laparisienne/miss-univers/videos-la-colombienne-paulina-vega-elue-miss-univers-2014-26-01-2015-4479621.php
PRINCIPAL: 1223824
TEXT:
VIDEOS. La Colombienne Paulina Vega �lue Miss Univers 2014
26 Janv. 2015, 06h55 | MAJ : 26 Janv. 2015, 18h07
29
AFP
La repr�sentante de la Colombie, Paulina Vega, a �t� sacr�e dimanche Miss Univers 2014, lors de la 63e �dition du concours organis�e � Doral pr�s de Miami, aux Etats-Unis.
La mannequin et �tudiante en commerce de 22 ans, qui �tait en comp�tition face � 87 autres femmes du monde entier, succ�de � la V�n�zu�lienne Gabriela Isler.
Sur le m�me sujet
Miss Univers�: le coup de gueule de Sylvie Tellier
Pauline Vega a re�u sa couronne les larmes aux yeux et v�tue d'une longue robe argent�e �tincelante, au d�collet� profond. Elle est la seconde Colombienne � remporter le titre, apr�s Luz Marina Zuluaga, en 1958.
Quatorze autres pays s'�taient hiss�s parmi les finalistes : Argentine, Australie, Br�sil, Espagne, Etats-Unis, Inde, Indon�sie, Italie, Jama�que, Pays-Bas, Philippines, Ukraine, Venezuela et France . Mais la Fran�aise Camille Cerf, �g�e de 20 ans, n'a pas r�ussi a int�grer le groupe des finalistes malgr� sa belle prestation. Les cinq derni�res pr�tendantes en lice �taient donc Pauline Vega, Miss Etats-Unis, Miss Jama�que, Miss Ukraine et Miss Pays-Bas.
VIDEO.�Paulina Vega sacr�e Miss Univers 2014
VIDEO. Paulina Vega durant la comp�tition
Miss Univers a �t� cr�� en 1952
Aujourd'hui propri�t� du milliardaire Donald Trump, Miss Univers a �t� cr�� en 1952 par la compagnie californienne de v�tements Pacifique Mills pour riposter au concours de beaut� international Miss Monde, fond� l'ann�e pr�c�dente au Royaume-Uni par un entrepreneur britannique. En d�cembre dernier, le titre de Miss Monde a �t� d�croch� par l'Afrique du Sud. C'est Flora Coquerel, Miss France 2014, qui avait d�fendu les couleurs tricolores.
Sur ce concours Miss Univers 2014, un incident diplomatique a �t� �vit� de justesse lorsque Miss Isra�l a r�alis� un selfie avec la concurrente du Liban . Le clich� a enflamm� les r�seaux sociaux. Consid�r�es comme faisant partie des favorites du concours, les deux brunettes dont les pays sont officiellement en guerre, ont eu un surcro�t de tension. La controverse a m�me r�ussi � �clipser celle du financement � hauteur de 2,5 millions de dollars du concours par la ville organisatrice.
VIDEO. Gabriela Isler sacr�e Miss Univers 2013 l'an dernier � Moscou.
Votre email * (ne sera pas visible)
Votre r�action *
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France*
* champs obligatoires
Glagny91 a publi� le 26 Janvier 2015 � 23:17
En r�ponse @Briana. Beaut�, perfection, �a ne veut rien dire. Ce sont des crit�res fluctuants. 10 ans plus tard ou plus t�t, le classement aurait �t� autre. Et puis, il y a beaucoup de contingences de politique internationale qui influent sur le r�sultat. Il n'y a pas que la beaut� plastique, toute relative. Mettons un autre jury et les premiers seront les derniers. Je le r�p�te, c'est une mascarade juteuse.
Signaler un abus R�agir
Rabangab a publi� le 26 Janvier 2015 � 14:44
Les dauphines de Camille Cerf: Miss Tahiti Douche ou Miss C�te d'Azur auraient fait un bien meilleur classement...
Signaler un abus R�agir
Menerlach a publi� le 26 Janvier 2015 � 14:24
Le titre de miss univers est un tantinet pr�tentieux dans la mesure o� l'univers est peut-�tre infini et en tout cas beaucoup plus grand que ce que l'on peut observer actuellement dans le ciel...
Signaler un abus R�agir
Briana a publi� le 26 Janvier 2015 � 14:11
Ca ne m etonne pas que ce soit encore une sud americaine qui gagne. Elles sont de loin les plus belles. Elles ont tout : un corps avec des formes parfaites, une chevelure sublime et un visage de deesse. Miss France, meme si elle est belle, fait pale figure a cote des miss sud americaines.
Signaler un abus R�agir
Rabangab a publi� le 26 Janvier 2015 � 14:10
D��u de ce r�sultat: la Jama�que et l'Ukraine m�ritait tout autant ce titre que Miss Colombie qui s'est content�e de r�pondre par des banalit�s en espagnol aux questions pos�es... Elles �taient aussi belles et peut-�tre plus naturelles. Bref, faut arr�ter avec Camille Cerf. Elle est peut-�tre " jolie ", mais elle avait vraiment l'air de se faire ch... et a d�fil� avec un r�el manque de motivation durant toutes les phases du concours!
Mamz a publi� le 26 Janvier 2015 � 13:23
Elle est  plus belle que Camille Cerf .
Signaler un abus R�agir
Glagny91 a publi� le 26 Janvier 2015 � 13:02
En r�ponse @W93. On dit "Miss Pays-Bas", car la Hollande n'est qu'une r�gion de cet �tat. Disons que tu as fait de l'humour !!! Passons...
Signaler un abus R�agir
Glagny91 a publi� le 26 Janvier 2015 � 13:00
Ce que j'aime beaucoup, c'est de voir la gagnante pleurer et les perdantes sourire jusqu'aux oreilles, alors qu'elles ont les boules ! Et je me demande aussi quelles sensations l'unique pr�sentateur (masculin) peut-il avoir. La sensation d'�tre le sultan du harem ?
W93 a publi� le 26 Janvier 2015 � 12:53
Miss Hollande........WHAOOOOWwww
Glagny91 a publi� le 26 Janvier 2015 � 12:53
En r�ponse @Gouja. Elle ne l'est plus ?
