TITRE: Galtier tacle Erding - Football - Sports.fr
DATE: 26-01-2015
URL: http://www.sports.fr/football/ligue-1/articles/galtier-n-a-pas-aime-erding-1174286/
PRINCIPAL: 1224415
TEXT:
26 janvier 2015 � 08h26
Mis à jour le
26 janvier 2015 � 11h02
Déjà battu par Paris en quarts de finale de la Coupe de la Ligue (0-1), Saint-Etienne a connu le même sort dimanche soir pour le compte de la 22e journée de Ligue 1. Un résultat décevant pour son entraîneur Christophe Galtier, mécontent du comportement de certains de ses joueurs, dont Mevlüt Erding, nommément cité.
Les oreilles d’Erding ont sifflé dimanche soir peu après la nouvelle défaite à domicile de Saint-Etienne face au PSG , venu s’imposer 1-0 à Geoffroy-Guichard pour la deuxième fois en moins de quinze jours. Interrogé sur la prestation de ses joueurs en conférence de presse, Christophe Galtier a d’abord regretté leur passivité d’ensemble: "En première mi-temps, nous avons été beaucoup trop timides pour inquiéter cette équipe parisienne. Nous avons manqué d’enthousiasme et de générosité. Nous n’avons joué qu’à partir du moment où nous avons été menés au score. Nous nous sommes enfin lâchés. Nous avons pu espérer jusqu’au bout mais nous ne nous sommes pas procuré d’occasions."
Le coach de l’ASSE s’est ensuite montré agacé de la prestation individuelle de certains, supposés être des cadres de l’équipe: "Ce soir, je suis déçu du manque de caractère et d’investissement de certains joueurs. Quand on a la chance de jouer un grand match face à une équipe brillante, on doit avoir un autre investissement. Je prends acte de ce que j’ai vu ce soir pour la prochaine composition d’équipe." Qui visait-il ? Au regard du match, les regards se tournent inévitablement vers les attaquants, Van Wolfswinkel et Erding, passés à travers, incapables de faire peser la moindre menace sur le but d’un Sirigu qui a finalement eu très peu de travail dans le Chaudron.
Je l'ai trouvé très en dedans
Christophe Galtier
Mais c’est bien le second qu’il a nommément cité, lâchant "Mevlüt a été très très moyen", avant de préciser: "Il avait l’opportunité de jouer un grand match, d’être associé à un autre attaquant, je l’ai trouvé très en dedans." Et Galtier de rejeter l’excuse du manque de fraîcheur chez un joueur qui a été écarté des terrains quasiment deux mois en fin d’année dernière: "On peut être en dedans car on manque de rythme, mais ça fait un certain moment qu’il joue. Après, il y a la détermination que l’on doit avoir sur certaines situations. C’est là où je suis déçu le concernant." Comment l’international turc va-t-il prendre ses critiques ? Bien selon Galtier qui a terminé ainsi son intervention à charge: "Je peux me permettre de vous le dire car il le sait…" S’il le sait…
