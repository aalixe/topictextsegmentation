TITRE: St�phane Lhopiteau nomm� directeur financier d'Areva- 26 janvier 2015 - Challenges.fr
DATE: 26-01-2015
URL: http://www.challenges.fr/finance-et-marche/20150126.REU7496/stephane-lhopiteau-nomme-directeur-financier-d-areva.html
PRINCIPAL: 1224145
TEXT:
Challenges �>� Finance et march� �>�St�phane Lhopiteau nomm� directeur financier d'Areva
St�phane Lhopiteau nomm� directeur financier d'Areva
Publi� le� 26-01-2015 � 09h23
PARIS, 26 janvier ( Reuters ) - Areva a annonc� lundi la nomination de St�phane Lhopiteau comme directeur financier du sp�cialiste fran�ais du nucl�aire � compter du 1er mars.
Membre du comit� ex�cutif d' Areva , il est plac� sous la responsabilit� du directeur g�n�ral Philippe Knoche.
A 45 ans, St�phane Lhopiteau pilotait jusqu'ici le programme de performance et les services partag�s du groupe Thales , dont il a �galement �t� directeur financier adjoint puis CFO � titre int�rimaire.
* Le communiqu� :
http://bit.ly/1ELa42U   (Gilles Guillaume, �dit� par Dominique Rodriguez)
Partager
