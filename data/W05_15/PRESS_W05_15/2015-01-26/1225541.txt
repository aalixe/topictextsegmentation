TITRE: Incendie au domicile de Jean-Marie Le Pen: "Peur? Je n'ai jamais peur!"
DATE: 26-01-2015
URL: http://www.bfmtv.com/societe/incendie-au-domicile-de-jean-marie-le-pen-peur-je-n-ai-jamais-peur-859648.html
PRINCIPAL: 0
TEXT:
Imprimer
La maison de Jean-Marie Le Pen a �t� d�truite lundi dans un incendie. Le leader frontiste, l�g�rement bless�, s'est confi� � Paris-Match.
Jean-Marie Le Pen �tait seul, sans son �pouse, lundi en milieu de journ�e lorsqu'un incendie a �clat� dans son pavillon cossu � Rueil-Malmaison, dans les Hauts-de-Seine, o� il habitait depuis 1991. Selon une source polici�re, "la maison a �t� d�truite".
Un constat confirm� par l'ancien leader frontiste, joint par t�l�phone par Paris Match . "Presque tout" a br�l�, explique-t-il.
L�g�rement bless� sur le haut de la pommette dans sa fuite, il n'a pas �t� hospitalis� pour autant. "J'ai r�ussi � sortir par la fen�tre mais dans ma h�te, j'ai tr�buch� et je suis tomb� sur le sol de la terrasse. Rien de grave", confie-t-il.
Origine accidentelle?
Jean-Marie Le Pen raconte avoir vu "des flammes hautes". "Les d�g�ts sont impressionnants", dit-il.
Pour autant, le pr�sident d'honneur du Front national tient � se montrer fort. A la question de savoir s'il a eu peur, il r�pond, interloqu�: "Peur? Je n'ai jamais peur! A part d'une seule chose que je r�p�te souvent: que le ciel ne me tombe sur la t�te! C'est mon c�t� breton..."
L'origine du sinistre n'a pas �t� clairement identifi�e pour le moment.
Selon une source polici�re, il pourrait s'agir "d'un d�part de feu accidentel, qui a pris vraisemblablement dans le conduit de la chemin�e sur laquelle travaillait un ouvrier".
Le parquet de Nanterre a d�cid� d'ouvrir une enqu�te pour d�terminer les causes de l'incendie.
Par A. G.
A. G. 26/01/2015 � 16:41
Votre opinion
