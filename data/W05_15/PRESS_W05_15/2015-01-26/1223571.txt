TITRE: Economie et Finance | Loi Macron : deux semaines pour faire ses preuves � l'Assembl�e
DATE: 26-01-2015
URL: http://www.ledauphine.com/economie-et-finance/2015/01/25/la-loi-macron-devant-les-deputes
PRINCIPAL: 0
TEXT:
�CONOMIE Loi Macron : deux semaines pour faire ses preuves � l'Assembl�e
Emmanuel Macron d�fendra son projet de loi durant deux semaines   � l�Assembl�e nationale.   Photo AFP
Deux semaines de d�bats sont pr�vues pour adopter la centaine d�articles de la loi pour la croissance et l�activit�, port�e par le ministre de l�Economie.
On y est� Apr�s avoir �t� adopt� et amend� en commission sp�ciale, le projet de loi Macron, du nom du ministre de l�Economie et des Finances qui le porte, arrive aujourd�hui dans l�h�micycle de l�Assembl�e nationale. Deux semaines de d�bats sont pr�vues.
Un texte phare
Fran�ois Hollande attend beaucoup de ce projet de loi qui est au centre des discussions politiques depuis des mois. Son but est simple : booster la croissance et l�activit� pour cr�er de l�emploi. Pour cela, il s�appuie sur trois piliers qui regroupent chacun une pl�iade de mesures : lib�rer, investir et travailler.
Le texte rassemble en tout une centaine d�articles tr�s diff�rents les uns des autres qui ont pour but de lib�rer le potentiel inexploit� de croissance en levant les blocages identifi�s dans les secteurs de l��conomie (transports, commerce, etc.).
Une disposition phare
Le d�bat s�est focalis� sur une seule mesure : l�extension du travail le dimanche dans les commerces. Initialement, le projet de loi pr�voyait l�ouverture possible des commerces non alimentaires cinq dimanches par an sur simple demande et jusqu�� douze dimanches avec l�accord du maire. En commission sp�ciale, les d�put�s ont modifi� le texte. L�ouverture des cinq dimanches par an de plein droit est supprim�e et les �lus ont le choix d�autoriser une ouverture jusqu�� douze dimanches par an. Le d�put� PS de la Manche, St�phane Travert, qui a eu l�id�e de ce compromis, assure que � dans la grande majorit� des communes, les besoins commerciaux sont inf�rieurs � cinq dimanches par an �.
� gauche, certains ne sont pas d�accord, estimant comme la socialiste Karine Berger (Hautes-Alpes) qu�ouvrir 12 dimanches ne rel�ve plus de l�exception mais de l�habitude.
Un ministre phare
Les d�put�s de la commission sp�ciale vantent l��coute d�Emmanuel Macron, sa volont� de trouver un consensus. Julien Aubert, d�put� UMP du Vaucluse et camarade de promotion de l�ENA, l�a trouv� � solaire �, � pr�sent et humble �.
� tout juste 37 ans, le jeune et ambitieux ministre n�a pas le droit � l�erreur. �narque (inspecteur des finances), ex-banquier de chez Rothschild, ancien secr�taire g�n�ral adjoint de l��lys�e, il a le CV impeccable et pla�t � ses opposants politiques, s�duits par sa ligne tr�s lib�rale.
Mais Emmanuel Macron a d� aussi composer. Bien que sa loi puisse ressembler � un catalogue � la Pr�vert � cause de ses nombreux articles, il aurait aim� qu�il y en ait davantage, notamment sur l�assouplissement des 35 heures. Politiquement, c��tait difficile, d�autant qu�il n�est pas �lu et qu�il doit plus que d�autres, convaincre. Il jouera la carte de l�opinion qui lui est favorable, notamment sur l�ouverture des magasins le dimanche.
Par Nathalie MAURET | Publi� le 26/01/2015 � 06:06
Vos commentaires
Connectez-vous pour laisser un commentaire
Se connecter
Pour acc�der � votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
