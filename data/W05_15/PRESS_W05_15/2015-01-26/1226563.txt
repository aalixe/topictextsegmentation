TITRE: iPhone 6s : sa puce sera majoritairement produite par... Samsung
DATE: 26-01-2015
URL: http://www.fredzone.org/iphone-6s-sa-puce-sera-majoritairement-produite-par-samsung-393
PRINCIPAL: 1226553
TEXT:
| 26 janvier, 2015 at 08:14
55
0
Apple et Samsung ont toujours eu du mal � s�entendre, mais cela ne les a jamais emp�ch� de faire des affaires ensemble. En r�alit�, le g�ant sud-cor�en a produit de nombreux composants utilis�s dans les derniers t�l�phones et dans les derni�res tablettes tactiles de notre bonne vieille Pomme Croqu�e. La tendance n�est d�ailleurs pas pr�te de s�inverser car la puce de l�iPhone 6s sera majoritairement produite par ses soins. Du moins selon les derniers bruits de couloir en date.
Tout est parti d�une rumeur colport� par Digitimes au d�but du mois. Citant des sources in�vitablement proches de l�industrie et des chaines de production, la publication a effectivement pondu un article enti�rement d�di� � la puce A9.
Samsung aura un r�le � jouer dans la production de l�iPhone 6s.
D�apr�s lui, Samsung et Globalfoundries �taient ainsi cens�s�prendre en charge 70% de la production, les 30% restants �tant confi�s � TSMC. Quelques jours plus tard, trois analystes travaillant pour USB en ont rajout� une couche,�avec des chiffres diff�rents puisque TSMC devait alors produire 50% des puces A9.
Apple et Samsung n�ont pas fini de travailler ensemble
Ce matin, le Maeil Business Newspaper a produit de nouveaux chiffres encore plus impressionnants. Selon ses sources, la puce A9 pourrait ainsi �tre produite � 75% par Samsung. Pas mal, non ? Certes et le meilleur reste � venir car l�usine charg�e de cette mission serait celle bas�e � Austin, au Texas.
La prudence est �videmment de mise, d�autant que ces chiffres ont la f�cheuse tendance � changer toutes les semaines. Ce qui est s�r, en tout cas, c�est que le g�ant sud-cor�en aura un r�le � jouer dans la production de l�iPhone 6s.�Apple n�est donc�pas pr�t de se d�barrasser de lui.
Sinon, et pour celles et ceux qui seraient pass�s � c�t�, l�iPhone 6s devrait se d�cliner lui aussi en deux mod�les, et il serait tr�s proche du mod�le pr�c�dent. Avec deux ou trois petites bricoles en plus comme une puce A9, 2 Go de m�moire vive et un nouveau capteur.
