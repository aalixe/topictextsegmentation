TITRE: Box-office USA : American Sniper toujours au top ! (VIDEO) - news t�l�
DATE: 26-01-2015
URL: http://www.programme-tv.net/news/cinema/61987-box-office-usa-american-sniper-toujours-au-top/
PRINCIPAL: 1225403
TEXT:
Box-office USA : American Sniper toujours au top ! (VIDEO)
Box-office USA : American Sniper toujours au top ! (VIDEO)
Plurimedia lundi 26 Janvier 2015 � 13:05
� Keith Bernstein - 2014 Warner Bros. / WV Films IV
American Sniper de Clint Eastwood a conserv� sa place de leader au box-office am�ricain ce week-end, devant Un voisin trop parfait et Paddington.
Il y a parfois des choses qui ne changent pas. La preuve avec le box-office USA du week-end. En effet, apr�s un d�marrage en trombe la semaine derni�re, American Sniper , le nouveau film de Clint Eastwood, reste en t�te. Emmen� par Bradley Cooper (nomm� � l'Oscar du meilleur acteur pour la troisi�me fois de sa carri�re), le long m�trage a cumul� 64,3 millions de dollars de recettes. Ce qui pousse son chiffre d'affaires global � 200,1 millions de dollars et permet au film d'enregistrer le huiti�me meilleur deuxi�me week-end de tous les temps derri�re Spider-Man (71,4 millions en 2002).
C'est Jennifer Lopez qui s'offre la deuxi�me marche du podium avec Un voisin trop parfait. Le film, thriller sexy dans lequel elle donne la r�plique � Ryan Guzman, a r�colt� pas moins de 15 millions de dollars de recettes. Pas mal quand on sait que le long m�trage n'a co�t� que 4 millions de dollars.
Enfin, c'est Paddington qui cl�t le trio de t�te avec 12,3 millions de dollars au compteur. Le film de Paul King tient bon la barre pour son deuxi�me week-end d'exploitation puisqu'il enregistre une toute petite baisse (34,3%) par rapport � la semaine derni�re.
Plus loin, on retrouve T�moin � louer qui descend de deux places (11,6 millions) et Taken 3 avec 7,6 millions.
A lire aussi
