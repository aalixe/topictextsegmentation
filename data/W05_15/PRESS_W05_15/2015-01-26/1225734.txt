TITRE: Poutine accuse l'arm�e ukrainienne d'�tre une L�gion �trang�re de l'Otan
DATE: 26-01-2015
URL: http://www.romandie.com/news/Poutine-accuse-larmee-ukrainienne-detre-une-Legion-etrangere-de-lOtan/559000.rom
PRINCIPAL: 1225733
TEXT:
Tweet
Poutine accuse l'arm�e ukrainienne d'�tre une L�gion �trang�re de l'Otan
Moscou - Le pr�sident russe Vladimir Poutine a accus� lundi l'arm�e ukrainienne, engag�e dans des combats avec les rebelles prorusses dans l'est de l'Ukraine, d'�tre la L�gion �trang�re de l'Otan utilis�e par les Occidentaux pour contenir la Russie.
On dit souvent: l'arm�e ukrainienne, l'arm�e ukrainienne. En r�alit�, qui combat dans ses rangs' Il y a des unit�s r�guli�res des forces arm�es, et en grande partie des soi-disants +bataillons de volontaires nationalistes+, a d�clar� M. Poutine � Saint-P�tersbourg, selon des images de la t�l�vision publique russe.
De fait, il ne s'agit pas d'une arm�e mais d'une L�gion �trang�re, dans le cas pr�sent, une L�gion �trang�re de l'Otan qui n'a pas pour but la d�fense des int�r�ts nationaux de l'Ukraine, a ajout� le pr�sident russe.
Il s'agit d'un autre but g�opolitique: contenir la Russie, ce qui ne correspond pas du tout avec les int�r�ts nationaux du peuple ukrainien, a accus� le chef de l'Etat russe.
La d�claration muscl�e de Vladimir Poutine est intervenue � l'approche d'une r�union extraordinaire de l'Otan sur la situation en Ukraine, convoqu�e � la demande de Kiev.
La commission Otan-Ukraine (COU) a �t� cr��e dans le cadre des accords de partenariat entre l'Alliance et l'Ukraine, sign�s en 1997. Elle offre un cadre pour la coop�ration politique et pratique entre les deux partenaires.
Le secr�taire g�n�ral, Jens Stoltenberg, doit tenir un point de presse vers 16H00 (15H00 GMT) apr�s la r�union.
L'Union europ�enne a �galement d�cid� de convoquer une r�union extraordinaire de ses ministres des Affaires �trang�res jeudi � Bruxelles avec l'objectif de sanctionner cette nouvelle offensive.
La chanceli�re allemande Angela Merkel a appel� dimanche le pr�sident russe Vladimir Poutine � faire pression sur les s�paratistes. Le ministre russe des Affaires �trang�res, Sergue� Lavrov, a annonc� lundi que la Russie faciliterait des contacts entre Kiev et les rebelles dans les prochains jours.
Le conflit de neuf mois dans l'Est s�paratiste prorusse a connu un nouveau tournant samedi avec le bombardement du port strat�gique de Marioupol, qui a fait 30 morts et une centaine de bless�s civils.
(�AFP / 26 janvier 2015 14h58)
�
