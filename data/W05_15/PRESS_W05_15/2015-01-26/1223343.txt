TITRE: Hospitalisation : une pneumonie augmente le risque d'infarctus - Pourquoi Docteur ?
DATE: 26-01-2015
URL: http://www.pourquoidocteur.fr/Hospitalisation---une-pneumonie-augmente-le-risque-d-infarctus-9593.html
PRINCIPAL: 0
TEXT:
� Un infarctus en heures creuses augmente de 13 % le risque de d�c�s �
Hypertension, tabagisme, diab�te, ob�sit�les facteurs de troubles cardiovasculaires sont multiples. Mais selon des chercheurs canadiens qui ont r�cemment publi� une �tude dans la revue Journal of the American Medical Association (JAMA) , �tre hospitalis� pour une pneumonie accro�t �galement fortement les risques de faire un infarctus.
Pour parvenir � ces r�sultats, les scientifiques ont �tudi� les dossiers m�dicaux de pr�s de 3800 personnes, r�parties en deux groupes d��ge. Le premier avait entre 45 et 65 ans et le second, 65 ans et plus.
Dans chaque cohorte, la moiti� d�entre eux a fait un s�jour � l�h�pital pour traiter une pneumonie. Le Dr Vicente Corrales-Medina, infectiologue et auteur principal de l��tude, a ensuite compar� leurs dossiers avec ceux de 2500 autres personnes qui n�ont jamais contract� de pneumonie. Et pour ce sp�cialiste, les r�sultats sont sans �quivoque�: toutes les personnes victimes de pneumonie ont vu leur risque de d�velopper des troubles cardiovasculaires augmenter apr�s leur s�jour � l�h�pital.
Des risques quadrupl�s chez les plus de 65 ans
Cette augmentation diff�re selon les personnes selon leur �ge, leur mode de vie et bien s�r leurs ant�c�dents cardiovasculaires. Ainsi, une femme �g�e de 72 ans fumeuse et souffrant d�hypertension, a vu ses risques de faire un �pisode cardiaque tripler apr�s son hospitalisation. Pour cette cat�gorie d��ge, les risques peuvent quadrupler au cours des deux premi�res ann�es et perdurent jusqu�� la dixi�me ann�e, m�me s�ils sont deux fois moins �lev�s deux ans apr�s. En revanche, chez les 45-65 ans, les chercheurs n�ont pas observ� d�augmentation de risque au-del� des deux premi�res ann�es.
Premi�re �tude � faire lien entre hospitalisation pour pneumonie et infarctus, le personnel de sant� hospitalier, mais aussi les patients sont fortement encourag�s � se prot�ger. ��Ces r�sultats montrent l'importance de tout faire pour pr�venir une pneumonie, surtout par la vaccination et une bonne hygi�ne de base, comme le fait de se laver les mains r�guli�rement��, conlucent les auteurs de l'�tude.
maladies sur le meme sujet
