TITRE: Ils se sont frott�s � Zlatan - Football - Sports.fr
DATE: 26-01-2015
URL: http://www.sports.fr/football/ligue-1/articles/zlatan-des-annees-de-provoc-et-d-altercations-1174547/
PRINCIPAL: 0
TEXT:
Zlatan Ibrahimovic, du temps de sa période Inter Milan. (Reuters)
Par Olivier Chauvet
26 janvier 2015 � 17h41
Mis à jour le
26 janvier 2015 � 18h01
S’il a trouvé du répondant en la personne de Paul Baysse, dimanche lors de Saint-Etienne-PSG (0-1), Zlatan Ibrahimovic a eu souvent maille à partir avec un adversaire au cours de sa carrière. Voici un florilège de quelques accrochages impliquant le Suédois. 
Rio Mavuba, PSG - Lille (2-2) 22 décembre 2013
Habituellement plutôt cool sur le terrain, le capitaine lillois Rio Mavuba avait cette fois craqué face aux provocations de Zlatan Ibrahimovic et les esprits s’étaient échauffés des deux côtés avec un Florent Blamont très remonté. Mais l’arbitre avait calmé les esprits en adressant un carton jaune à chacun. Rapport de cause à effet ou pas, le milieu de terrain nordiste avait égalisé dans la foulée après l’ouverture du score du géant suédois.
Joey Barton, PSG-OM (2-0) 27 février 2013
Entre Zlatan et Joey Barton , il ne pouvait y avoir que des étincelles. Alors qu’il n’y avait pas vraiment eu photo entre Parisiens et Marseillais, à l’occasion des huitièmes de finale de la Coupe de France, avec un doublé du Suédois en faveur du club de la capitale, le milieu de terrain anglais s’était permis de le chambrer sur la taille de son nez en toute fin de match.
Philippe Mexès, Inter-Roma (2-1) 21 janvier 2009
Ce quart de finale de Coupe d’Italie entre l’Inter Milan et la Roma avait donné lieu à un échange plutôt musclé entre l’attaquant scandinave et le défenseur français, auteur d’un tacle il est vrai dangereux sur son vis-à-vis. L’arbitre avait finalement été clément en ne sortant pas un deuxième carton jaune pour le joueur de la Louve, qui avait déjà été averti quelques minutes plus tôt.
Marco Materrazzi, Inter- AC Milan (0-1) 14 novembre 2010
Pour ce derby milanais très musclé, le duel entre Ibra et Marco Materazzi est au centre des débats. Après un penalty provoqué par le défenseur nerazzurro sur le Suédois, qui se fait justice lui-même en début de match, l’attaquant rossonero passe la vitesse supérieure après l’heure de jeu, en adressant un high-kick dont il a le secret dans la poitrine du Transalpin. Un geste qui lui vaudra un simple carton jaune, mais les images ont fait le tour de la planète. Après Zidane, Materazzi est aussi devenu une victime de Zlatan.   
Le natif de Malmö n’a pas attendu d’être au PSG pour se faire des amis sur les terrains. Avec l’âge, ce dernier semble même plutôt avoir tendance à se calmer. La preuve, du temps de sa période Inter, Ibra n’hésitait pas à jouer des coudes...
Ou à s’en prendre à Giorgio Chiellini , à la suite d’un duel savoureux entre les deux hommes:
Dans le genre grande gueule, Mark van Bommel n’est pas mal non plus. Mais le Néerlandais avait cette fois réussi à garder son calme face au bouillant Suédois. A moins que l’échange d’amabilités se soit poursuivi dans le chemin menant aux vestiaires.
