TITRE: CAN - La Tunisie et la RD Congo se partagent les points et la qualification | francetv sport
DATE: 26-01-2015
URL: http://www.francetvsport.fr/football/can/la-tunisie-et-le-rd-congo-se-partagent-les-points-et-la-qualification-263317
PRINCIPAL: 1226322
TEXT:
Vous �tes ici
Francetv sport Football CAN La Tunisie Et La RD Congo Se Partagent Les Points Et La Qualification
CAN
La Tunisie et la RD Congo se partagent les points et la qualification
La joie des Tunisiens � la CAN 2015 (KHALED DESOUKI / AFP)
Par francetv sport
Publi� le 26/01/2015 | 20:51, mis � jour le 26/01/2015 | 21:29
Malgr� leur nul 1-1, la Tunisie et la RD Congo se sont qualifi�s pour les quarts de finale de la Coupe d'Afrique des Nations 2015. Les Aigles de Carthage terminent � la premi�re place du groupe B devant les Congolais, qualifi� gr�ce � une meilleure attaque aux d�pens du Cap-Vert, auteur du nul 0-0 contre la Zambie.
Dans une CAN peu prolixe, marquer c'est presque gagner. En position tr�s favorable avant le dernier match du groupe B, la Tunisie n'a pas choisi l'attentisme face � la�RD Congo, rival potentiel. Pendant que le Cap-Vert et la Zambie se neutralisaient, les Aigles de Carthage ont pris leur destin en main. Il a fallu s'y reprendre � plusieurs fois devant le but de Kidiaba pour inscrire le but lib�rateur. Apr�s une tentative avort�e de Sassi (10e), Chikhaoui a �t� le plus actif. Maladroit au d�but (10e, 15e), l'attaquant du FC Zurich a ensuite but� sur le portier congolais malgr� une belle s�quence (20e). Finalement, c'est dans le r�le du passeur qu'on l'a vu plus efficace. Bien que son tacle ne ressemblait ni � un tir ni � une passe, Akaichi a parfaitement coup� la trajectoire du ballon pour ouvrir le score (1-0, 31e). Une r�compense logique pour la Tunisie plus technique face � un Congo timor� o� seul Kasusula a test� les gants de Mathlouthi. Et encore, c'�tait sur un coup franc enroul� (28e).
Le RD�Congo�passe par un trou de souris
Apr�s la pause, la Tunisie g�rait son avance et reculait d'un cran. Des occasions de contre s'offraient aux Tunisiens mais �a p�chait dans la derni�re passe comme lorsqu'Akaichi oubliait Sassi � sa gauche pour tirer � c�t� (61e). Kahzri avait la balle du 2-0 mais il frappait au dessus de la barre (65e). Toujours vivant, le Congo saisissait sa chance sur un long d�gagement. Mbokani d�vait pour Bokila qui crucifiait le gardien tunisien (1-1, 66e). Ce but ne changeait rien � la premi�re place du groupe, d�di�e � la Tunisie. En revanche, il qualifiait le Congo gr�ce � une meilleure attaque que le Cap-Vert. Les Tunisiens repartaient � l'attaque. Lanc� par Chikhaoui, Maaloul centre en premi�re intention pour Younes. Sans un Kidiaba vigilant, c'�tait le but assur�. Les derni�res minutes �taient stressantes pour les deux �quipes qui ne savaient plus quoi faire. Attaquer ou d�fendre ? Les Congolais, qui avaient le plus � perdre, essayaient de se mettre � l'abri. Ils �taient finalement soulag� de voir que le Cap-Vert et la Zambie�se quittaient eux aussi sur un nul mais sans but. La�RD Congo passe par un trou de souris mais l'important est de passer.
