TITRE: Odile Vuillemin (L'Emprise sur TF1) : "J'ai jou� ce r�le avec mes tripes" (VIDEO) - news t�l�
DATE: 26-01-2015
URL: http://www.programme-tv.net/news/series-tv/61873-odile-vuillemin-l-emprise-tf1-joue-tripes-video/
PRINCIPAL: 0
TEXT:
Odile Vuillemin (L'Emprise sur TF1) : "J'ai jou� ce r�le avec mes tripes" (VIDEO)
Odile Vuillemin (L'Emprise sur TF1) : "J'ai jou� ce r�le avec mes tripes" (VIDEO)
David Peyrat lundi 26 Janvier 2015 � 12:58
Apr�s avoir annonc� son d�part de Profilage, Odile Vuillemin joue ce lundi soir (20h55), sur TF1, une femme battue dans le t�l�film L�Emprise. Rencontre avec une com�dienne qui ose.
Odile Vuillemin est bouleversante dans L�Emprise . Dans ce drame, elle joue le r�le d'une m�re de quatre enfants qui se retrouve devant la justice pour le meurtre de son mari ( Fred Testot ), un homme qui l�a violent�e pendant dix-sept ans. Un personnage tr�s lourd � porter pour la com�dienne de 38 ans. "Je me suis mis beaucoup de pression pour ce r�le car, en tant que femme, c��tait aussi un devoir moral de l�accepter".
Plusieurs moments sont tr�s violents dans L'Emprise, surtout quand Alexandra se fait frapper par son mari. "On a r�p�t� les sc�nes de lutte avec un r�gleur de cascades qui nous a appris � donner et � recevoir des coups sans se faire mal. Prise dans l�action, je me suis bless�e une fois en me cognant contre un meuble. J�ai aussi regard� des documentaires sur les violences conjugales et rencontr� la vraie Alexandra Lange, puisque L�Emprise est l�adaptation de son livre. J�avais besoin de savoir physiquement o� se trouvait la peur."
Les habitu�s de Chlo� Saint-Laurent, de Profilage, risquent d'�tre perturb�s par la transformation physique d'Odile Vuillemin. "J�ai pris du poids car je voulais trouver un �tat de mollesse qui me permettait de mieux appr�hender ce r�le de femme battue, que la vie abandonne. Et puis, je ne me voyais pas jouer en rousse. Alexandra n�est pas la bondissante Chlo� Saint-Laurent de Profilage�! Mon travail de com�dienne passe par le physique et me met dans des situations extr�mes. J�ai dit au producteur de L�Emprise qu�avec moi, on plonge sans filet�! J�ai beaucoup pleur� pendant le tournage car j�ai jou� ce r�le avec mes tripes. J�ai pr�f�r� pr�venir tout le monde que j�aurais besoin d��vacuer toute l�horreur de cette histoire."
Et pour Profilage ? "J�ai fait le tour du personnage. Elle est super, Chlo�, mais j�ai besoin de passer � autre chose. Cette d�cision n�a pas �t� prise sur un coup de t�te. Elle a �t� m�rement r�fl�chie et je ne reviendrai pas dessus."
A lire aussi
