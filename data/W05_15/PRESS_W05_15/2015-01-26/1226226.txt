TITRE: L’iPhone, plus vendu en Chine qu'aux Etats-Unis, High tech
DATE: 26-01-2015
URL: http://www.lesechos.fr/tech-medias/hightech/0204110428533-iphone-plus-vendu-en-chine-quau-etats-unis-1087052.php
PRINCIPAL: 0
TEXT:
Fabienne Schmitt / Chef de service adjoint |
Le 26/01 � 19:04, mis � jour � 19:43
Les chinois raffolent des smartphones � grande taille - AFP
1 / 1
L�empire du Milieu est aujourd�hui le troisi�me march� d� Apple .
Tim Cook l�avait pr�dit il y a tr�s pr�cis�ment deux ans. � La Chine va devenir le premier march� d�Apple, j�en suis persuad� �, avait dit le PDG d�Apple, sans donner de calendrier. Cela serait en passe de devenir une r�alit� : selon des analystes cit�s par le � Financial Times �, pour la premi�re fois de son histoire, la firme � la pomme aurait vendu plus d�iPhone en Chine qu�aux Etats-Unis. Elle pourrait confirmer la nouvelle lors de la pr�sentation de ses r�sultats demain soir. d�apr�s UBS, au dernier trimestre 2014, 36 % des ventes d�iPhone se sont faites dans l�empire du Milieu, contre 24 % pour celui de l�Oncle Sam. Un an plus t�t, c��tait l�inverse : 29 % des ventes �taient r�alis�es aux Etats-Unis et 22 % en Chine.
Potentiel monstrueux
Cette derni�re repr�sente un potentiel monstrueux : c�est le premier march� mondial des smartphones. Au dernier trimestre, Apple y a r�alis� 14 % de son chiffre d�affaires. C�est son troisi�me march� aujourd�hui apr�s les Etats-Unis et l�Europe. Il a commenc� � d�coller apr�s la signature d�un accord de commercialisation des iPhone, en d�cembre�2013, avec China Mobile, le premier op�rateur mobile chinois. Les iPhone n��taient alors propos�s que par de petits op�rateurs � China Unicom et China Telecom. � L�iPhone 6 a l�avantage d�avoir une grande taille, ce que le march� chinois r�clame beaucoup �, indique Leslie Griffe de Malval, g�rant chez Fourpoints.
Cette bonne nouvelle pour Apple intervient alors que son grand rival, Samsung, est � la peine. En Chine, la croissance du march� des smartphones est surtout tir�e par les appareils d�entr�e et de milieu de gamme. En t�moigne le succ�s de la nouvelle star des mobiles : Xiaomi, troisi�me vendeur mondial de smartphones depuis peu � derri�re Samsung et Apple � et premier en Chine. Ses appareils co�tent 50 % du prix de l�iPhone d�Apple, qui demeure un produit de luxe. � Apple va rester sur ce positionnement haut de gamme. Xiaomi, lui, se situe plut�t sur le milieu de gamme, l� o� c�est le bain de sang en termes de marge �, reprend Leslie Griffe de Malval.
L�iPhone 6 fait des ravages
En attendant, l�iPhone�6 fait des ravages. Il pourrait d�passer les 70�millions d�appareils �coul�s selon certains analystes�et devenir l�iPhone le plus vendu d�Apple, malgr� son prix �lev�. V�ritable vache � lait d�Apple, les ventes d�iPhone repr�sentent la moiti� de ses 171�milliards de dollars de revenus et la majeure partie de ses profits.
Tim Cook, dont le salaire est li� aux b�n�fices r�alis�s par l�entreprise gr�ce aux ventes d�iPhone, peut se r�jouir. Ses revenus ont doubl� entre�2013 et�2014, � 9,2�millions de dollars. Mais il n�est pas le mieux pay�. Chez Apple, c�est en effet Angela Ahrendts, la patronne des Apple Store (qu�il a fallu d�baucher chez Burberry), qui surclasse tout le monde avec une r�mun�ration de 73�millions de dollars.
Fabienne Schmitt
