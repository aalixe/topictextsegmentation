TITRE: Tour de Normandie 2015 �tape 2 en direct live streaming | Racing-1.com
DATE: 26-01-2015
URL: http://www.racing-1.com/2015/01/26/zidane-passe-en-tete/
PRINCIPAL: 1225656
TEXT:
by Cyrille Mounier � mars 25, 2015
Ce mercredi a lieu la troisi�me �tape du Tour de Normandie qui promet d�assurer un show formidable pour tous les passionn�s de la petite reine. Les coureurs s��lanceront depuis la ville de Duclair pour rejoindre au terme de 166,5 kilom�tres la ville d�Elbeuf sur Seine.
Un beau programme en perspective pour cette troisi�me �tape qui devrait enchanter les spectateurs venus en masse sur le long du parcours. Au classement g�n�ral c�est Thomas Scully qui m�ne la danse devant Olivier Pardini. Un peu plus loin on retrouve Lucas Destang et Jan Diteren. Nathan Van Hooydonck compl�te le top cinq provisoire.
Une course � suivre en direct � partir de 12h00 et en live par streaming l�gal sur Internet.
