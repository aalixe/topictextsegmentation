TITRE: Windows 10 arrivera sur la 'majorit�' des smartphones Lumia | BlogNT
DATE: 26-01-2015
URL: http://www.blog-nouvelles-technologies.fr/60021/windows-10-lumia/
PRINCIPAL: 1224576
TEXT:
26 janvier, 2015
dans Smartphone/Tablette avec 0 commentaire
Microsoft va lancer une version preview de Windows 10 pour les smartphones au mois de f�vrier, avec une version d�finitive qui devrait �tre livr�e � tous les consommateurs � la fin de l�ann�e. Alors qu�est-ce que cela signifie pour les gens ayant un smartphone sous Windows Phone 8.1 ?
Selon Chris Weber de Microsoft, ��la majorit� des smartphones Lumia�� qui ex�cutent d�j� Windows 8 ou Windows 8.1, ces derniers vont recevoir une mise � jour pour ex�cuter le tout dernier syst�me d�exploitation, Windows 10.
Weber pr�cise que la derni�re version de Windows a �t� con�ue pour offrir un support aux smartphones Lumia existants, quelles que soient la forme, la taille et la performance intrins�que du dispositif. Il y a des mod�les d�entr�e de gamme avec des �crans � faible r�solution, et de petites quantit�s de m�moire vive, ainsi que des dispositifs haut de gamme de la taille d�une phablette avec un mat�riel plus rapide et de meilleurs composants.
Windows 10 presque pour tous�
Cela sugg�re que certaines fonctionnalit�s de Windows 10 peuvent ne pas �tre disponibles sur tous les smartphones, et certaines pourraient rencontrer quelques difficult�s avec des p�riph�riques d�ores et d�j� commercialis�s. Mais fondamentalement, Microsoft pr�cise que si vous achetez un smartphone Lumia d�s aujourd�hui, il y a de fortes chances que celui-ci soit en mesure d�ex�cuter Windows 10 quand il sera disponible.
Les nouvelles fonctionnalit�s dans Windows 10 pour les smartphones comprennent une application Param�tres mise � jour, le support des images d�arri�re-plans personnalis�es sur l��cran d�accueil, et de nouvelles versions des applications de Microsoft Office, qui ont �t� optimis�es pour le tactile. On va �galement b�n�ficier d�une nouvelle application Outlook.
Les smartphones ex�cutant Windows 10 seront �galement en mesure de synchroniser les donn�es avec les tablettes, les ordinateurs portables, ou autres PC sous Windows 10 par le service de stockage sur le cloud de Microsoft, OneDrive. De plus, ils vont m�me utiliser certaines des m�mes applications. Par exemple, Outlook est essentiellement la m�me application, qu�elle soit install�e sur un smartphone avec une puce bas�e sur l�architecture ARM, ou un PC de bureau avec un processeur Intel ou AMD.
Microsoft encourage �galement les d�veloppeurs tiers � cr�er ce qu�elle appelle des applications universelles, Universal Windows Apps, qui seront en mesure de fonctionner � travers diff�rents types de dispositifs.
Tags : Lumia , Windows 10
Yohann Poiron
J'ai fond� le BlogNT en 2010. Autodidacte en matie?re de de?veloppement de sites en PHP, j�ai toujours pousse? ma curiosite? sur les sujets et les actualite?s du Web. Je suis actuellement engage? en tant qu'architecte interop�rabilit�.
