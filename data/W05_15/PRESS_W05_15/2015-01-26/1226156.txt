TITRE: Foot - Coupe d�Afrique des Nations (Groupe C) : Alg�rie, S�n�gal, Ghana, qui va sauter ?  - La Voix du Nord
DATE: 26-01-2015
URL: http://www.lavoixdunord.fr/sports/foot-coupe-d-afrique-des-nations-groupe-c-algerie-ia182b200n2623524
PRINCIPAL: 1226153
TEXT:
Le journal du jour � partir de 0,79 �
La derni�re journ�e du groupe C de la CAN-2015 sera forc�ment fatale � un grand nom du continent, mardi, avec deux chocs br�lants, Alg�rie-S�n�gal et Ghana-Afrique du Sud.
Sc�ne tendue entre les Alg�riens et les Ghan�ens lors du deuxi�me match de ce groupe C. PHOTO AFP
- A +
La poule est pour le moment domin�e par le S�n�gal (4 points), avec une petite longueur d�avance sur le duo Alg�rie-Ghana et trois sur l�Afrique du Sud, mais aucune �quipe ne poss�de de garanties pour la suite des �v�nements.
Les Lions de la Teranga abordent cet ultime rendez-vous du 1er tour dans les meilleures conditions et n�ont besoin que d�un nul pour se hisser en quarts de finale. La situation est en revanche beaucoup plus d�licate pour les Fennecs alg�riens, oblig�s de l�emporter. A moins de se contenter d�un score de parit� et de miser sur un succ�s des Bafana Bafana contre les Ghan�ens.
Autant dire que la tension est extr�me dans le camp alg�rien et les nerfs � vif apr�s deux premi�res sorties gu�re convaincantes, loin des attentes g�n�r�es par le magnifique parcours au Mondial-2014 (8e de finale) et un rang de premi�re nation africaine au classement Fifa (18e).
� Aucune nervosit�, aucune �
Violemment critiqu� par la presse alg�rienne qui a notamment �voqu� des conflits d�ego entre joueurs, Christian Gourcuff s�est m�me senti oblig� d�effectuer une mise au point publique avant l�entra�nement de dimanche � Malabo. � Il y a beaucoup d�unit� et de solidarit� et cela est inestimable, a d�clar� le s�lectionneur fran�ais. Maintenant, il y a des choses totalement fausses qui se disent �� et l�, et j�en laisse l�enti�re responsabilit� � leurs auteurs. On a surtout colport� des mensonges et cela est inadmissible �.
� Il n�y a vraiment aucun souci. Entre nous, il n�y a avait aucune nervosit�, aucune, il y a une bonne ambiance �, a �galement assur� le d�fenseur Madjid Bougherra.
Le S�n�gal est serein
C�t� s�n�galais, l�ambiance est tout autre et rien n�est jusqu�ici venu perturber la qui�tude des Lions, qui esp�rent retrouver le Top-8 pour la premi�re fois depuis 2006. Pas m�me l�incendie qui a eu lieu dimanche tout pr�s de leur lieu d�entra�nement, le stade de la Paz de la capitale de la Guin�e �quatoriale, dont les flammes �taient visibles depuis les tribunes.
Dans l�autre rencontre, le Ghana, quadruple vainqueur de la CAN et demi-finaliste des quatre derni�res �ditions, est en ballottage favorable, puisqu�une victoire ou un nul (si l�Alg�rie ne bat pas le S�n�gal) suffiront � son bonheur alors que l�Afrique du Sud n�a qu�une chance infime de se qualifier.
Apr�s une entame mitig�e (d�faite face au S�n�gal 2-1), les Black Stars se sont bien repris en battant l�Alg�rie dans les arr�ts de jeu (1-0) gr�ce � Asamoah Gyan, remis de sa crise de paludisme, et paraissent confiants avant ce match couperet. � Ce sera une tr�s dure bataille, les attentes sont tr�s grandes au pays mais le Ghana est habitu� � g�rer la pression �, a expliqu� le milieu de Marseille Andr� Ayew.
15partages
