TITRE: Mise en examen d'un p�diatre st�phanois suspect� de p�do-pornographie - LExpress.fr
DATE: 26-01-2015
URL: http://www.lexpress.fr/actualites/1/societe/mise-en-examen-d-un-pediatre-stephanois-suspecte-de-pedo-pornographie_1644736.html
PRINCIPAL: 1226113
TEXT:
Zoom moins
Zoom plus
Un p�diatre st�phanois a �t� mis en examen apr�s la d�couverte d'images � caract�re p�do-pornographique sur l'ordinateur de son cabinet m�dical, et laiss� en libert� sous contr�le judiciaire
afp.com/Philippe Huguen
L'homme, mis en examen le 12 janvier, a �t� laiss� en libert� sous contr�le judiciaire. �
L'alerte a �t� donn�e � la police par une m�re de famille, dont l'enfant �tait en consultation chez ce praticien, apr�s qu'elle eut aper�u des images suspectes sur l'�cran de l'ordinateur.�
Dans le cadre de l'enqu�te, un juge a ordonn� une perquisition au cabinet et au domicile du p�diatre, o� ses �quipements num�riques ont �t� saisis pour en v�rifier le contenu.�
Contact� par l'AFP, l'Ordre des m�decins de la Loire a r�agi lundi apr�s-midi en d�clarant avoir "assist� � une perquisition au cabinet de ce confr�re, dans le cadre de la proc�dure l�gale".�
"Le procureur de la R�publique de Saint-Etienne, � la suite de cette perquisition, a d�cid� de (le) soumettre � un contr�le judiciaire interdisant de se livrer � l'activit� professionnelle de m�decin p�diatre", a ajout� un porte-parole du conseil d�partemental de l'Ordre des m�decins de la Loire.�
Par
