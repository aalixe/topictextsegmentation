TITRE: Open d'Australie: Un jour � Melbourne, �pisode 7 -   Sports: Toute l'actu sports - tdg.ch
DATE: 26-01-2015
URL: http://www.tdg.ch/sports/actu/jour-melbourne-episode-7/story/20388625
PRINCIPAL: 1223328
TEXT:
Un jour � Melbourne, �pisode 7
Open d'AustralieVoici ce qu'il faut retenir de la septi�me journ�e de l'Open d'Australie.
25.01.2015
Vous voulez communiquer un renseignement ou vous avez rep�r� une erreur ?
Orthographe
Veuillez SVP entrez une adresse e-mail valide
L'EXPLOIT
Les Australiens l'attendaient et Nick Kyrgios l'a fait. Depuis 2005 et la finale perdue par Lleyton Hewitt face � Marat Safin, aucun joueur local n'�tait all� jusqu'en quarts de finale. Le �Kid� de Canberra, 19 ans a r�ussi cette performance en venant � bout de l'Italien Andreas Seppi. Kyrgios a invers� le cours du jeu apr�s avoir �t� men� 2 sets 0 et d� sauver une balle de match.
Il atteint pour la deuxi�me fois ce stade en Grand Chelem apr�s un premier quart disput� � Wimbledon l'an pass�. Il est aussi le plus jeune joueur � se hisser � ce niveau � Melbourne depuis le Russe Andrey Cherkasov en 1990. La nation �Aussie� r�ve maintenant d'un nouvel exploit.
LE FAIT DE MATCH:
� Australian Open (@AustralianOpen) 25 Janvier 2015
LE FLOP
Pr�sent� comme un potentiel vainqueur de Grand Chelem, Dimitrov devra encore attendre. Le Bulgare, longtemps surnomm� �Baby Federer� pour une gestuelle quasi identique � celle du Suisse, s'est arr�t� au stade des huiti�mes de finale, dompt� par l'Ecossais Andy Murray.
Dimitrov avait domin� Murray sur sa pelouse sacr�e de Wimbledon en quarts de finale. Dimanche, le Britannique s'est veng� et Dimitrov, qui servait � 5-3 dans le quatri�me set, a laiss� passer une opportunit� de prolonger les d�bats.
LES PHRASES
- �Je n'ai jamais rencontr� quelqu'un portant le m�me pr�nom que moi donc cela n'arrivera jamais.� De la Canadienne Eugenie Bouchard � qui on demandait s'il y aurait un jour un match entre deux �Eugenie� apr�s celui opposant lundi deux �Madison� (Keys et Brengle).
- �Je ne sais pas qui est en charge de la programmation cette ann�e, mais elle est un peu absurde. Et je ne suis pas le seul � �tre de cet avis. Sur certains matches, je me suis dit: 'Les gars, qu'est-ce qui vous prend?'�. De l'Australien Bernard Tomic, d��u de ne pas avoir disput� un seul de ses matches dans la Rod Laver Arena, le court central de Melbourne.
LES CHIFFRES
- 3: le nombre d'ann�es cons�cutives que le Tch�que Tomas Berdych n'a pas l�ch� un set avant les quarts de finale.
- 16: le nombre de quarts de finale d'affil�e en Grand Chelem pour l'Ecossais Andy Murray (absent de Roland-Garros 2013 � cause de son dos).
- 17: le nombre de victoires d'affil�e -s�rie en cours- de Rafael Nadal sur Berdych, son prochain adversaire.
L'HISTOIRE
Le tennis aux Etats-Unis marche � deux vitesses. Pour la premi�re fois depuis 2003, le pays de la banni�re �toil�e pourrait avoir trois repr�sentantes en quarts de finale de l'Open d'Australie. Tout d�pendra des r�sultats des soeurs Williams, Serena et Venus, oppos�es � Garbine Muguruza et Agnieszka Radwanska, alors que les deux Madison, Keys et Brengle, s'affronteront.
C�t� messieurs, en revanche, c'est le d�sert. Plus aucun joueur ne s'est hiss� en huiti�mes de finale depuis la d�faite � ce niveau de comp�tition en 2011 d'Andy Roddick qui a pris sa retraite un an plus tard. Il est le dernier Am�ricain � avoir remport� un �Majeur�, l'US Open en 2003. Douze ans apr�s, les Etats-Unis se cherchent toujours une rel�ve.
(afp/Newsnet)
