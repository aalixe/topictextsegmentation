TITRE: R�gime strict pour George Clooney - Amal met son �poux � la di�te
DATE: 26-01-2015
URL: http://www.parismatch.com/People/Cinema/Encourage-par-Amal-George-Clooney-se-met-au-regime-697290
PRINCIPAL: 1225839
TEXT:
Amal met son �poux � la di�te
R�gime strict pour George Clooney
Amal met son �poux � la di�te
Amal et George Clooney aux Golden Globes Awards le 11 janvier dernier � D.Long / Globe / ZUMA / VISUAL Press Agency
Le 26 janvier 2015 | Mise � jour le 26 janvier 2015
Sarah Louaguef
Tweeter
Adieu les grands plats, finies les grasses festivit�s: apr�s des mois de c�l�bration, George Clooney se met officiellement au r�gime. Selon les informations rapport�es par le �Daily Star�, sa jeune �pouse, Amal, serait pour beaucoup dans cette nouvelle r�solution.
2015 d�bute en fanfare pour George Clooney. Honor� pour l'ensemble de sa carri�re le 11 janvier aux Golden Globes Awards , l'acteur de 53 ans a �galement profit� de quelques vacances reposantes sous le soleil du Mexique aux c�t�s de sa nouvelle �pouse, Amal. Couple incontournable de la sph�re hollywoodienne depuis leur mariage en Italie le 27 septembre dernier, le com�dien am�ricain et la brillante avocate de 36 ans n'en finissent plus d'intriguer les m�dias du monde entier. Ce dimanche, le � Daily Star � rapporte ainsi que le r�alisateur de �Monuments Men� aurait �t� somm� par sa femme de se mettre au r�gime pour se d�barrasser du poids r�colt� depuis leur lune de miel.�
Bien d�termin�e � surveiller la ligne de son �poux, la juriste britanno-libanaise lui aurait donc conseill� de perdre environ cinq kilos avant le 22 f�vrier, date de la prochaine c�r�monie des Oscars. Apr�s avoir d�marr� la promotion du film �Tomorrowland� en novembre, George Clooney devrait finalement reprendre un rythme plus soutenu en mars avec le tournage de la nouvelle com�die des fr�res Coen, �Hail, Caesar?!�. Un agenda charg� qui n�cessiterait une petite remise en forme, selon Amal.
"Il n'y a rien que George aime davantage que d�vorer quelques tacos"
�Elle m'a dit qu'il y avait cinq r�gles dans le nouveau r�gime de George. Cela devrait lui faire retrou�ver la ligne tr�s rapi�de�ment: pas d'alcool, pas de viande grasse, moins de produits laitiers et de glucides, et des tests r�gu�liers de son taux de choles�t�rol�, d�clare un proche du couple au journal anglais. Selon les propos rapport�s, l'�g�rie Nespresso est quelqu'un de tr�s gourmand et voue un v�ritable culte aux sp�cialit�s mexicaines. �George est peut-�tre disci�plin� sur les tour�nages, mais malgr� les direc�tives strictes d'Amal, il devrait quand m�me faire quelques entorses. Il adore la nour�ri�ture mexi�caine et se rend au fast food plusieurs fois par semaine � Los Angeles�, ajoute-t-on.
Grand �picurien, George Clooney est �galement � la t�te de sa propre vari�t� de tequila, Casamigos Tequila, fond�e il y a deux ans en association avec son ami Rande Gerber, c�l�bre �poux de Cindy Crawford. �Rande et Cindy ont promis � Amal qu'ils feront tout ce qui est en leur pouvoir pour l'aider � retrouver la ligne. Ce sera quand m�me difficile, car il n'y a rien que George aime davantage que de d�vorer quelques tacos au poisson �pic� suivis d'un ou deux shots de sa tequila�, conclut-on. Les paris sont lanc�s.
Les personnalit�s li�es
