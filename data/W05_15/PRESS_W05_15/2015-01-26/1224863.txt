TITRE: Meaux. SOS M�decins en gr�ve lundi 26 janvier � Article � La Marne
DATE: 26-01-2015
URL: http://www.journallamarne.fr/2015/01/26/sos-medecins-en-greve-lundi-26-janvier/
PRINCIPAL: 1224858
TEXT:
Accueil
Meaux SOS M�decins en gr�ve lundi 26 janvier
L'antenne de Meaux de SOS M�decins poursuit son mouvement de gr�ve ce lundi 26 janvier jusqu'� 20 h.
26/01/2015 � 12:18 par Carine Babec
Partages Facebook Twitter Google + Email
SOS M�decins France vient de d�cider, pour le moment, de poursuivre son mouvement de suspension d�activit� pendant la journ�e du lundi 26 janvier jusqu�� 20h. En cause : �l�absence totale de proposition de la part du gouvernement si ce n�est une � �pid�mie � de r�quisitions tous azimuts envers nos m�decins et nos structures�.
Et les m�decins d�annoncer que les prochaines semaines risquent d��tre de plus en plus tendues. C�est pourquoi SOS M�decins a d�cid� d�engager une grande campagne d�information des patients afin qu�ils puissent mesurer que �les enjeux de ce combat les concernent au premier chef et qu�ils ont leur mot � dire. Nous en ferons part � la ministre de la sant�, Madame Marisol Touraine, qui a fini par nous proposer un rendez-vous le 2 f�vrier, en esp�rant qu�elle saura comprendre l�urgence d�engager un v�ritable dialogue pour construire avec les m�decins lib�raux et non contre eux le syst�me de sant� de demain�.
