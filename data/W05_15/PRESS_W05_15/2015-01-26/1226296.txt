TITRE: Hand: la France tape du poing face � l'Argentine
DATE: 26-01-2015
URL: http://www.leparisien.fr/flash-actualite-sports/hand-la-france-se-joue-de-l-argentine-pour-aller-en-quarts-26-01-2015-4480869.php
PRINCIPAL: 1226289
TEXT:
Hand: la France tape du poing face � l'Argentine
26 Janv. 2015, 20h35 | MAJ : 26 Janv. 2015, 20h35
1/2 r�agir
1
La joie des joueurs de l'�quipe de France qualifi�s pour les quarts de finale du Mondial de handball aux d�pens de l'Argentine, le 26 janvier 2015 � Doha
La France a attendu son premier grand rendez-vous du Mondial-2015 messieurs de handball pour r�ussir enfin un match complet, qui lui a permis de surclasser une �quipe d'Argentine vite impuissante (33-20) en huiti�mes de finale, lundi � Doha.
L'ensemble tricolore avait �t� un peu brinquebalant pendant les matches de poules. Port� par une d�fense extraordinaire devant un Thierry Omeyer de gala, il a plus que rassur� avec ce match plein de bout en bout.
"C'est un premier match sans accroc, qu'on a r�ussi � jouer sans faire revenir l'adversaire, avec beaucoup de lucidit�, d'application, d'exigence et suffisamment d'efficacit�", a appr�ci� Claude Onesta , le s�lectionneur fran�ais.
Excit�s par la perspective de se rapprocher des JO-2016, leur premier objectif dans ce Mondial, les Bleus ont �t� tr�s convaincants. Ce succ�s leur garantit quasiment une place dans un Tournoi de qualification olympique (TQO), qui devrait ensuite n'�tre qu'une formalit� pour eux.
Il permet aussi � la France de confirmer sa permanence au plus haut niveau. Depuis sa premi�re m�daille internationale aux JO-1992 (bronze), elle a toujours termin� dans les huit premi�res de toutes les comp�titions majeures auxquelles elle a pris part, � l'exception de l'Euro-2012 en Serbie (11e).
Les Bleus ont encore fait preuve de leur capacit� � g�rer les rendez-vous importants. Depuis 2006, ils ont remport� 20 des 24 matches � �limination directe qu'ils ont jou�s sur les trois comp�titions majeures (JO, Mondial, Euro).
Avec ce succ�s, ils ont aussi signifi� � tous leurs adversaires que, cette ann�e encore, il faudrait compter avec eux pour le titre. La Slov�nie qu'ils affronteront mercredi (19h00) en quarts, est avertie.
Les Fran�ais se m�fieront toutefois de cette nation, 4e de la pr�c�dente �dition en 2013 et qui compte dans ses rangs huit joueurs �voluant en France, dont l'ailier droit de Montpellier Dragan Gajic, le meilleur buteur du Mondial avant cette journ�e (52 buts en six matches).
- Monstrueux en d�fense -
La France, qui avait gagn� ses 12 confrontations avec l'Argentine jusque-l�, n'a gu�re souffert pour s'offrir la 13e. Les Sud-Am�ricains, qui avaient d�tonn� en sortant d'un groupe tr�s relev� en phase de poules, ont �t� pris � la gorge d'entr�e et se sont � peine d�battus.
Apr�s leur entame de match rat�e contre la Su�de (25-27), les Bleus ont rectifi� le tir, avec emphase. Omeyer (13 arr�ts, dont 9 � la pause) les a mis en confiance en �cartant les quatre premiers tirs argentins.
Monstrueux en d�fense, les Fran�ais ont d'abord connu un peu de d�chet aux tirs, ce qui les a emp�ch�s d'imm�diatement s'envoler (5-2, 11e). Mais au fil des minutes, les Argentins sont apparus de plus en plus d�sempar�s.
Le petit g�nie Diego Simonet mis sous l'�teignoir, ils n'ont jamais pu prendre � revers la d�fense fran�aise, m�me en sup�riorit� num�rique. Omeyer a continu� � lire comme dans un livre ouvert leurs tirs et l'�cart a enfl� (11-4, 24e).
Onesta a acc�l�r� le rythme de ses rotations. D'une interception conclue de l'autre c�t� du terrain, Luka Karabatic a symbolis� cette premi�re p�riode, marqu�e par la totale sup�riorit� des Bleus (16-6, 30e).
En marquant trois buts en quelques minutes, Valentin Porte a donn� une claire indication � l'Argentine que les Bleus n'avaient pas l'intention de desserrer l'�treinte (20-8, 37e).
Mathieu Gr�bille, que son s�lectionneur, tenait absolument � relancer apr�s un d�but de Mondial compliqu�, l'a rassur� en montrant toute la palette de son talent avec deux buts rapides, l'un de l'aile, l'autre � 9 m (26-11, 42e).
Malgr� les rotations innombrables, les Fran�ais ont continu� � se montrer tr�s s�rieux jusqu'� la derni�re seconde, sans oublier de s'amuser, tous finissant par marquer.
