TITRE: Génériques. Huit nouveaux médicaments suspendus - France - Le Télégramme, quotidien de la Bretagne
DATE: 26-01-2015
URL: http://www.letelegramme.fr/france/huit-nouveaux-medicaments-generiques-suspendus-26-01-2015-10504239.php
PRINCIPAL: 1223413
TEXT:
Génériques. Huit nouveaux médicaments suspendus
26 janvier 2015 à 06h38
La suspicion qui entoure les médicaments génériques est alimentée par la décision prise par l'Agence française du médicament (ANSM) de suspendre, à compter du 5 février, les autorisations de mise sur le marché de huit d'entre eux. Motif invoqué : des « manipulations » de données opérées par une société indienne chargée d'en certifier la qualité. Le Ropinorol, prescrit pour traiter la maladie de Parkinson et le syndrome des jambes sans repos, ainsi que l'antiviral Aciclovir, rejoignent ainsi une liste de 25 médicaments génériques (dont l'Ibuprofène) déjà temporairement interdits par l'ANSM, en décembre. Tandis qu'au niveau européen, plusieurs dizaines de génériques sont suspendus « à titre de précaution ». « Ce qui ne signifie pas qu'ils présentent un risque pour la santé humaine », rassure l'ANSM. (Photo François Destoc)
