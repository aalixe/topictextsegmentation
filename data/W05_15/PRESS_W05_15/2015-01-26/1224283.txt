TITRE: New York va subir la temp�te "la plus importante de son histoire"
DATE: 26-01-2015
URL: http://www.24matins.fr/new-york-va-subir-la-tempete-de-neige-la-plus-importante-de-son-histoire-150686
PRINCIPAL: 1224280
TEXT:
>       New York va subir la temp�te "la plus importante de son histoire"
Nature
New York sous la neige
Lundi et mardi, une temp�te de neige historique va s'abattre sur la c�te nord-est des �tats-Unis. Le maire de New York se veut alarmiste.
Si vous �tes amateurs de films catastrophes d�anticipation, vous avez sans doute vu le film Le Jour d�Apr�s de Roland Emmerich o� la ville enti�re de New York se retrouvait pi�g�e par la neige et la glace suite � une temp�te exceptionnelle.
Cette fois, c�est bien dans la vie r�elle que les habitants de New York s�appr�tent � vivre une temp�te de neige qui pourrait bien devenir ��la plus importante de l�histoire de la ville��.
New York menac� par une temp�te de neige
Bien entendu, nous ne sommes pas en train de vous dire que la fiction va rattraper la r�alit� ici, mais le maire de New York, Bill de Blasio a tenu � mettre les New Yorkais en garde, Big Apple serait sur le point de subir ��l�une des plus importantes temp�tes de l�histoire de la ville��.
Ce dernier se veut alarmiste en invitant les habitants de la ville � se�pr�parer ��� quelque chose de pire que ce que nous avons vu jusqu�� pr�sent�� en ajoutant���Ne sous-estimez pas cette temp�te��. Bill de Blasio invite tous les habitants de la ville � rester chez eux si possible tout leur demandant de ne pas prendre la route. Les �coles devraient �tre ferm�es mardi selon des d�clarations relay�es par Reuters .
90�cm de neige dans les rues de New York
Cette annonce alarmiste a cr�� un petit vent de panique chez la population qui s�est ru�e vers les supermarch�s, on pouvait y voir de tr�s longues files d�attente de personnes cherchant � se ravitailler pour affronter l��v�nement.
La temp�te, accompagn�e de vents violents, va provoquer d�abondantes chutes de neige dans le nord-est des �tats-Unis en d�but de semaine. � New York, on pr�voit entre 60 et 90�cm de neige et 50 millions de personnes pourraient �tre affect�es par le ph�nom�ne. D�s hier, de nombreuses compagnies a�riennes ont d�cid� d�annuler des centaines de vols.
Cr�dits photos : robert cicchetti/Shutterstock
