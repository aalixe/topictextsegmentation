TITRE: Plus de 2000 vols annul�s aux Etats-Unis avant une temp�te de neige  - rts.ch - Monde
DATE: 26-01-2015
URL: http://www.rts.ch/info/monde/6489198-plus-de-2000-vols-annules-aux-etats-unis-avant-une-tempete-de-neige.html
PRINCIPAL: 1225261
TEXT:
le 26 janvier 2015
Le nord-est des Etats-Unis se pr�parait lundi � affronter une temp�te de neige exceptionnelle qui a d�j� provoqu� l'annulation de plus de 2000 vols.
Plus de 2000 vols ont �t� annul�s en provenance ou � destination des Etats-Unis lundi � l'approche d'un blizzard sur le nord-est du pays, o� l'�paisseur du tapis blanc pourrait approcher 1 m�tre.
La m�t�o nationale a �mis un avis de blizzard pour New York et Boston, jusqu'� la fronti�re canadienne.
Pr�s de 1 m�tre de neige est attendue sur le nord-est des Etats-Unis. [Brian Snyder - reu]
Entre lundi et mardi, de 45 � 60 cm de neige pourraient s'accumuler de New York � Boston, voire pr�s de 80 cm dans l'est du Massachusetts. Davantage de neige n'est pas � exclure dans d'autres r�gions isol�es, selon le service national de m�t�orologie (NWS).
L'une des plus importantes de l'histoire
Le maire de New York Bill de Blasio a mis en garde dimanche contre l'arriv�e de cette temp�te qui pourrait �tre "tr�s probablement l'une des temp�tes de neige les plus importantes de l'histoire de cette ville".�
Dimanche soir, de nombreux supermarch�s new-yorkais avaient �t� pris d'assaut par des habitants inquiets.
agences/fb
Swiss annule une dizaine de vols
La compagnie d'aviation Swiss annule une dizaine de vols en direction et en provenance de la c�te Est des Etats-Unis, en raison de la temp�te.
Six vols ont �t� annul�s depuis la Suisse vers New-York et Boston ainsi que dans l'autre sens. Mardi, quatre annulations sont pr�vues. D'apr�s les pr�visions m�t�o, le trafic a�rien devrait �tre normal d�s mercredi, selon la compagnie a�rienne.
A consulter �galement
