TITRE: M�t�o: Les USA se pr�parent � un blizzard historique -   Monde: Am�riques - lematin.ch
DATE: 26-01-2015
URL: http://www.lematin.ch/monde/ameriques/Les-USA-se-preparent-a-une-tempete-de-neige-historique/story/11998368
PRINCIPAL: 0
TEXT:
Les USA se pr�parent � un blizzard historique
M�t�o
�
Le nord-est des �tats-Unis attend l'une des pires temp�tes de son histoire, accompagn�e de plus d'un m�tre de neige par endroits.
Mis � jour le 26.01.2015 1 Commentaire
1/17 New York a �t� �pargn�e par la temp�te de neige qui s'est abattue dans le nord-est des Etats-Unis. (27 janvier 2015)
Image: Keystone
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Des milliers de vols ont �t� annul�s lundi 26 janvier de ou vers les Etats-Unis en pr�paration d'un blizzard historique � New York et dans le nord-est du pays, qui devrait se traduire par de fortes chutes de neige et des vents violents.
La neige tombait d�j� drue lundi en d�but d'apr�s-midi � New York, o� des centaines de chasse-neige �taient mobilis�s. Les chutes de neige les plus fortes sont attendues en fin d'apr�s-midi et durant la nuit.
Selon la m�t�o nationale (National Weather Service), la neige pourrait alors tomber au rythme de 5 � 10 cm par heure, avec une accumulation attendue de 45 � 60 cm, voire plus en certains endroits, et des bourrasques de vent de 72 � 88 km/heure. La temp�te devrait s'�loigner progressivement mercredi.
MORE: Also confirmed State of Emergency in Massachusetts, as well as statewide travel ban at midnight #blizzardof2015 pic.twitter.com/5KnIpnQWZ6
� The Situation Room (@CNNSitRoom) January 26, 2015
Aucune voiture autoris�e apr�s 23 heures
Le gouverneur de l'Etat de New York, Andrew Cuomo, a annonc� que les trains de banlieue s'arr�teraient � 23 heures (5 heures en Suisse mardi), et que le m�tro serait �limit� � partir de 19 heures ou 20 heures (1 heure ou 2 heures en Suisse mardi).
Le maire de la ville, Bill de Blasio, a une fois encore mis en garde les habitants, leur demandant de rester � l'int�rieur. Il a pr�cis� que les parcs allaient fermer � 18 heures (minuit en Suisse), et qu'aucune voiture, hormis les v�hicules d'urgence, ne serait autoris�e � circuler apr�s 23 heures. Les �coles seront ferm�es mardi, a-t-il ajout�.
�Ce n'est pas une temp�te typique�
�Ce n'est pas une temp�te typique�, a-t-il insist�. �Rentrez t�t chez vous�, a-t-il ajout�, r�p�tant que le blizzard pourrait �tre l'une des temp�tes de neige les plus importantes � frapper la ville de 8,4 millions d'habitants.
�Les New-Yorkais doivent se pr�parer � de possibles pannes d'�lectricit�, dues aux vents violents qui pourraient faire tomber les lignes et les arbres�, a aussi averti le gouverneur Andrew Cuomo.
Il a pr�cis� que l'Etat disposait d'au moins 1'806 chasse-neige et de plus de 126'000 tonnes de sel � r�partir pour lutter contre le mauvais temps. Tous les bus urbains devaient �tre �quip�s de pneus neige ou de cha�nes d'ici lundi apr�s-midi selon le gouverneur, qui a �galement pr�cis� que des �quipes suppl�mentaires avaient �t� mobilis�es pour assurer la s�curit� des quais et des escaliers de m�tro.
50 � 76 cm de neige attendus � Boston
Les New-Yorkais ont suivi � la lettre les conseils des autorit�s, et se sont pr�par�s, patientant parfois jusque sur le trottoir pour faire des provisions dans des supermarch�s d�valis�s, qui n'avaient pour certains plus de pain ou de lait dimanche soir.
So there's 2 feet of snow coming to the NY/NJ area and this is what people do to the store shelves? Huh? pic.twitter.com/ybOLHqwyhj
Dans la r�gion de Boston (Massachusetts), o� de 50 � 76 cm de neige sont attendus, les bourrasques de vent pourraient d�passer 100 km/heure. Les �coles devraient aussi rester ferm�es mardi, selon les autorit�s.
Des milliers de vols annul�s
Le manque de visibilit� associ� � la neige et au vent pourrait cr�er la pagaille dans les transports, et affecter plus de 50 millions de personnes dans le secteur touch�, allant de Philadelphie (Pennsylvanie) au Maine.
Lundi, plus de 2600 vols ont �t� annul�s de ou vers les Etats-Unis, et plus de 3300 l'�taient d�j� pour mardi, selon le site sp�cialis� Flightaware.com. Selon les autorit�s, la moiti� des vols ont notamment �t� annul�s � l'a�roport international John F. Kennedy � New York.
La compagnie United a annonc� qu'elle avait l'intention d'annuler tous ses vols mardi dans les trois a�roports new-yorkais de Newark, LaGuardia et JFK, ainsi qu'� Boston et Philadelphie.
�Imp�ratif d'avoir un plan pour rentrer chez vous�
A Philadelphie, les �coles devaient fermer lundi � la mi-journ�e. L'Etat du Connecticut a annonc� une mesure similaire et le gouverneur Dannel Malloy a annonc� une interdiction de voyager dans tout l'Etat � partir de 21 heures (3 heures en Suisse mardi).
�Il est imp�ratif que vous ayez un plan en place pour rentrer chez vous en toute s�curit� ce soir, avant que ne commencent les fortes chutes de neige�, a-t-il d�clar�.
Le National Weather Service a mis en garde contre tout d�placement inutile.
�De nombreuses routes seront impraticables�, a soulign� la m�t�o nationale. Les d�placements seront �extr�mement dangereux en raison des fortes chutes de neige et des vents violents�. (ats/afp/Newsnet)
Cr��: 26.01.2015, 16h48
