TITRE: Trafic de drogue : dix personnes dont deux policiers interpell�s � Roissy / France Bleu
DATE: 26-01-2015
URL: http://www.francebleu.fr/infos/trafic/trafic-de-drogue-dix-personnes-dont-deux-policiers-interpelles-roissy-2097489
PRINCIPAL: 1224532
TEXT:
Trafic de drogue : dix personnes dont deux policiers interpell�s � Roissy
Lundi 26 janvier 2015 � 11h48
0 commentaire
Deux agents de la police de l'air et des fronti�res ont �t� interpell�s � Roissy dans le cadre d'une op�ration de grande envergure pour arr�ter le commanditaire d'un trafic de drogue. Au total, une dizaine de personnes est en garde-�-vue, dont les deux policiers qui sont soup�onn� d'avoir facilit� le passage de mules transportant d'importantes quantit�s de coca�ne.
Les deux agents de la PAF ont �t� interpell�s alors qu'ils venaient de r�cup�rer deux valises pleines de coca�ne�- photo d'illustration � MaxPPP
Deux agents de la police de l'air et des fronti�res (PAF) ont �t� interpell�s ce dimanche � Roissy. Selon une source judiciaire, les deux hommes, qui ont �t� appr�hend�s alors qu'ils venaient de r�cup�rer aupr�s de passagers contr�l�s deux valises transportant chacune 20 kilos de coca�ne. Ils sont suspect�s d'avoir ferm� les yeux sur le passage de mules, munies de valises pleines de coca�ne.
L'enqu�te, men�e par l'Office central pour la r�pression du trafic illicite de stup�fiants (Octris), avait d�but� fin 2012. Apr�s de nombreuses filatures et �coutes t�l�phoniques, ce sont plus de dix personnes qui ont �t� interpell�es ce week-end, dont le commanditaire pr�sum� du trafic, mais aussi des proches du "cerveau" du trafic et des deux policiers, notamment les compagnes des trois hommes. Ils sont tous en garde-�-vue � Nanterre et Versailles.
Les policiers faisaient "franchir la douane" aux mules
Dans cette affaire, les deux policiers auraient pu avoir un "r�le crucial", allant chercher les passagers directement sur le "tarmac de l'a�roport", "en voiture s�rigraphi�e" et "leur faisant franchir la douane". Ni vu ni connu, les mules auraient ainsi pu acheminer leur cargaison � destination sans risquer les contr�les de police.
Ce n'est pas la premi�re fois que la police est mise en cause dans des affaires de trafic de stup�fiants ces derniers mois. En juillet 2014, la disparition de plus de 52 kilos de coca�ne des scell�s d�tenus au 36 Quai des Orf�vres avait fait grand bruit au sein de la DRPJ. Un policier de la brigade des Stups avait �t� mis en examen et �crou�, tout comme trois de ses proches, dont deux policiers, mi-janvier.
En juin 2012, � Roissy, sept policiers avaient �t� mis en examen pour vol en bande organis�e, blanchiment et association de malfaiteurs. Ils �taient soup�onn�s de voler des millions d'euros en argent liquide, en piochant dans les valises des trafiquants de drogue pendant plus de 20 ans.
Des cadres ont �galement �t� condamn�s. Le commissaire Michel Neyret, n�2 de la police judiciaire de Lyon, a �t� r�voqu� de la police nationale apr�s sa mise en examen en octobre 2011 dans une enqu�te de corruption et trafic international de stup�fiants. Et fin novembre 2008, Fran�ois Stuber, num�ro 2 de la police judiciaire de Strasbourg, et sa ma�tresse, une greffi�re au tribunal de grande instance, ont �t� condamn�s � 10 et 9 ans de prison pour trafic d'h�ro�ne, coca�ne, cannabis et Subutex.
