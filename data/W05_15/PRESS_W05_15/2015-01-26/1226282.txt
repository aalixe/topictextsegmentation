TITRE: S�curit� routi�re : les associations convaincues par Cazeneuve � metronews
DATE: 26-01-2015
URL: http://www.metronews.fr/info/securite-routiere-les-associations-convaincues-par-cazeneuve/moaz!UiLGexSzhiEM/
PRINCIPAL: 0
TEXT:
Mis � jour  : 26-01-2015 22:45
- Cr�� : 26-01-2015 19:43
S�curit� routi�re : les associations convaincues par Cazeneuve
PR�VENTION � Pour mettre un coup de frein � la hausse de la mortalit� sur les routes constat�e en 2014, le ministre de l�Int�rieur, Bernard Cazeneuve, a annonc� une s�rie de mesures ce lundi. R�pressives mais adapt�es, les mesures font consensus chez les associations d'automobilistes... et de pr�vention.
Tweet
�
Le gouvernement a abaiss� � 0,2 g/l de sang le seuil d'alcool�mie pour les jeunes conducteurs, contre 0,5 actuellement. Photo :�FRED TANNEAU / AFP
Les annonces de Bernard Cazeneuve plaisent. Ce lundi, face � la hausse de la mortalit� sur les routes en 2014 � une premi�re depuis douze ans �, le ministre de l�Int�rieur a d�taill� 26 mesures pour enrayer cette augmentation, annonces qui satisfont autant les associations pro qu�anti-voiture.�"C�est la premi�re fois que nous avons des annonces qui ne sont pas focalis�es sur les radars et la vitesse, et qu�un ministre �coute les automobilistes", se r�jouit Pierre Chasseray, le d�l�gu� g�n�ral de l�association pro-voiture� 40 Millions d�automobilistes , qui estime que les mesures vont dans la bonne direction.�M�me ligne pour Jean-Yves Sala�n, le d�l�gu� g�n�ral de l�association� Pr�vention routi�re , �galement satisfait.�Une convergence de vues assez rare pour �tre soulign�e. Illustration avec trois exemples concrets.
LIRE AUSSI >> S�curit� routi�re : les principales annonces du minist�re
? Abaissement du seuil d'alcool�mie � 0,2�g pour les nouveaux conducteurs
"Comment pourrait-on �tre contre cette mesure quand on sait que l�alcool est la premi�re cause de mortalit� au volant chez les jeunes", explique le d�l�gu� g�n�ral de 40 Millions d�automobilistes, qui d�plore toutefois "une stigmatisation", la r�gle n��tant pas la m�me pour tout le monde. Un argument balay� par Jean-Yves Sala�n : "C�est une mesure pour sauver la vie des jeunes et leur permettre de prendre tr�s t�t le r�flexe de ne pas boire."
?�Interdiction des oreillettes, casques et �couteurs
"�a va dans le bon sens", assure Pierre Chasseray, qui loue la possibilit� laiss�e aux automobilistes de t�l�phoner gr�ce � un syst�me embarqu� dans le v�hicule. "On offre une alternative intelligente et plus s�curisante." Pour Jean-Yves Sala�n, ce "nouveau pas" dans la lutte contre l'utilisation du t�l�phone au volant, et sa manipulation, est une excellente nouvelle. Le d�l�gu� g�n�ral regrette toutefois que le probl�me de l�attention ne soit pas pleinement r�solu. "T�l�phoner en conduisant, c�est dangereux !" mart�le-t-il.
?�Interdiction de se garer avant les passages pi�tons
L� encore, la mesure fait l�unanimit�. "C�est une mesure de bon sens", assure l�association de la Pr�vention routi�re. Idem pour 40 Millions d�automobilistes, qui lie toutefois sa pleine acceptation par les conducteurs au remplacement des places de stationnement perdues. Si des divergences de forme sont l�, sur le fond, les deux responsables associatifs pointent la justesse globale des mesures.
Geoffrey Bonnefoy
