TITRE: Mattel : d�part du CEO et r�sultats sous les attentes
DATE: 26-01-2015
URL: http://www.boursier.com/actions/actualites/news/mattel-depart-du-ceo-et-resultats-sous-les-attentes-613065.html?sitemap
PRINCIPAL: 1225588
TEXT:
Mon compte Je suis d�j� client
D�couvrir Je souhaite recevoir une documentation gratuite
Mattel : d�part du CEO et r�sultats sous les attentes
Abonnez-vous pour
moins de 1� par jour !
Le 26/01/2015 � 15h38
(Boursier.com) � Le titre Mattel plonge de plus de 8% en d�but de s�ance � Wall Street, � 25,7$, alors que le g�ant am�ricain du jouet vient d'annoncer la d�mission de son CEO et Chairman Bryan G. Stockton. Il est remplac� imm�diatement par Christopher A. Sinclair � la t�te du Groupe. Ce dernier est membre du conseil d'administration de Mattel depuis 1996, et a notamment occup� plusieurs postes de direction au sein de PepsiCo .
Mattel d�voile par ailleurs ses r�sultats pr�liminaires du quatri�me trimestre. Plomb�s par un dollar fort, les b�n�fices sont de 149,9 M$ (0,44$ par action), contre 369,2 M$ (1,07$ par action) un an avant. Les ventes baissent de 6% � 1,99 Md$. Les analystes anticipaient en moyenne un bpa trimestriel de 0,88$, pour des revenus de 2,14 Mds$.
La publication officielle des r�sultats du quatri�me trimestre est pr�vue pour le 30 janvier.
Damien Mezinis � �2015, Boursier.com
