TITRE: Bordeaux: Laurent Marti pr�t � �lib�rer sur le champ Rapha�l Ibanez� pour le XV de France - 20minutes.fr
DATE: 26-01-2015
URL: http://www.20minutes.fr/bordeaux/1525723-20150126-bordeaux-laurent-marti-pret-liberer-champ-raphael-ibanez-xv-france
PRINCIPAL: 0
TEXT:
Philippe Saint-Andr�
D�marrer sa carri�re d�entra�neur � Bordeaux et quitter la Gironde pour rejoindre l��quipe de France. Voici un parcours qui a d�j� �t� test�il y a cinq ans par un certain Laurent Blanc en football. Rapha�l Ibanez, le manageur de l�UBB , pourrait �tre tent� de faire pareil. Depuis quelques jours, son nom circule de plus en plus au sujet d�une �ventuelle succession de Philippe Saint-Andr� apr�s la Coupe du monde qui aura lieu l�automne prochain.
Il y a quelques semaines, Ibanez refusait d��voquer le sujet . Mais depuis,�il a revu sa position, puisque dimanche, sur le plateau de France 2, il n�a pas vraiment bott� en touche lorsque lui a �t� �voqu�e la possibilit� d��tre bient�t � la t�te des Bleus. ��a reste une rumeur... Ce n'est pas � l'ordre du jour mais, bien s�r, tout ce qui s'est dit ne m'a pas laiss� indiff�rent.�
�C�est maintenant, pas au mois de juin�
Si l�actuel manageur de l�UBB venait � �tre sollicit� par la f�d�ration, le pr�sident bordelais, Laurent Marti, ne s�y opposera pas, comme il l�a affirm� ce lundi � Midi Olympique . �S�il devait �tre appel� � la t�te de l��quipe de France, je le lib�rerais sur le champ et sans demander d�indemnit�s � la FFR pour la bonne et simple raison que, selon moi, l��quipe de France passe avant tout.�
Cependant, pas question pour autant de faire n�importe quoi et d�attendre gentiment que les choses se passent. �C�est maintenant que je dois me pr�parer, ce n�est pas au mois de juin, ni m�me l�ann�e prochaine, pr�vient Marti. Je ne veux pas que l�on m�explique que le futur manager du XV de France prendra ses fonctions apr�s le Tournoi 2016. J�ai entendu parler de l�id�e qui circule � la FFR.�
En clair, Ibanez devra �tre nomm� rapidement sinon son pr�sident pourrait se montrer beaucoup moins coop�ratif.
