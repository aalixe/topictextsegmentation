TITRE: Pop, glitter, rétro-futuriste, c'est la haute couture de Dior ! - Elle
DATE: 26-01-2015
URL: http://www.elle.fr/Mode/Les-defiles-de-mode/Haute-Couture-Printemps-Ete-2015/Femme/Paris/Christian-Dior/Pop-glitter-retro-futuriste-c-est-la-haute-couture-de-Dior-2880468
PRINCIPAL: 1226246
TEXT:
Ophélie Meunier @opheliemeunier
[Tous ses articles]�
Comme dans un rêve éveillé, on pénètre dans cette tente des jardins du musée Rodin et on se retrouve dans un octogone rose recouvert de miroirs, avec une galerie à l'étage, comme un manège pour enfants, ou une navette spatiale. C'est un peu des deux dont il s'agit ici pour la collection haute couture printemps-été 2015 de Raf Simons. Moonage Daydream, c'est le nom de la collection, et aussi celui d'une chanson de David Bowie, qui rythme le show avec "Space Invader" ou "Rock'N'Roll Suicide". L'espace est concrétisé, ici, tel qu'on l'imaginait dans les années 60 ou 70, argenté, pop, dématérialisé, dépassant les limites encore inconquises de la technique. Et Raf Simons s'amuse. Il les repousse les techniques de ses ateliers, avec des capes en plastique rebrodées de cristaux, des dentelles incroyables qui reprennent l'esthétique historique de la maison Dior. Du cuir vinyle habille des bottes carrées, monte sur la jambe comme une deuxième peau. Les combinaisons shorts ou pantalons sont rebrodées de sequins multicolores, et prennent alors un visage plus seventies. Les fleurs, véritables emblèmes de la maison, sont libérées, chamboulées, elles évoluent à travers des dentelles incrustées ou des imprimés bourgeons hyperréalistes. « Je voulais une vision plus sauvage, plus sexuée, plus étrange et sans doute plus libérée », dit Raf Simons dans sa note d'intention du défilé. « Oh You Pretty Things » aurait dit David Bowie.
 
