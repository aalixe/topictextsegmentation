TITRE: 26 ans de prison requis contre l'ex-commandant du Concordia- 26 janvier 2015 - Challenges.fr
DATE: 26-01-2015
URL: http://www.challenges.fr/monde/20150126.REU7579/26-ans-de-prison-requis-contre-l-ex-commandant-du-concordia.html
PRINCIPAL: 1225724
TEXT:
Challenges �>� Monde �>�26 ans de prison requis contre l'ex-commandant du Concordia
26 ans de prison requis contre l'ex-commandant du Concordia
Publi� le� 26-01-2015 � 16h46
A+ A-
FLORENCE, Italie , 26 janvier ( Reuters ) - L'accusation a requis lundi une peine de 26 ans et trois mois de prison � l'encontre du capitaine du Costa Concordia, Francesco Schettino, pour son r�le dans le naufrage du paquebot qui a fait 32 morts au large de l'�le du Giglio il y a trois ans en Toscane.
Devant le tribunal de Grosseto, o� se d�roule le proc�s de l'ex-commandant du navire, la procureure Maria Navarro a r�clam� 14 ans de prison pour homicide et blessures, neuf ans pour avoir caus� un naufrage, trois ans pour abandon de navire et trois mois pour faux t�moignage.
Le verdict sera mis en d�lib�r� le mois prochain.
Plus de 4.000 passagers et membres d'�quipage se trouvaient sur le Costa Concordia quand il a chavir� au soir du 13 janvier 2012 apr�s avoir heurt� un rocher.
La r�gion de Toscane et l'�le du Giglio r�clament 220 millions d'euros de dommages � la soci�t� Costa Croisi�res, filiale du groupe am�ricain Carnival Corp., en raison de l'impact n�gatif de la catastrophe sur l'industrie touristique locale.
L'�pave partiellement immerg�e du navire est rest�e pendant plus de deux ans � quelques dizaines de m�tres du port de Giglio avant d'�tre remorqu�e vers le port de G�nes en juillet dernier.
Costa Croisi�res a �chapp� � un proc�s au p�nal en acceptant de verser un million d'euros � l'Etat italien l'an dernier et en promettant de verser des milliers d'euros aux rescap�s.
Francesco Schettino est accus� d'avoir trop rapproch� le Concordia du rivage pour effectuer "l'inchino", un salut toutes lumi�res allum�es aux habitants de l'�le, et d'avoir quitt� le navire avant l'�vacuation de tous les survivants.
La phrase enregistr�e d'un officier des gardes-c�tes le sommant de remonter � bord ("Remontez � bord, bordel!") est devenue virale sur internet et les tee-shirts des Italiens.   (Silvia Ognibene; Jean-St�phane Brosse pour le service fran�ais)
Partager
