TITRE: D�fil�s Haute Couture : Christian Dior - 26/01/2015 - LaDepeche.fr
DATE: 26-01-2015
URL: http://www.ladepeche.fr/article/2015/01/26/2036662-defiles-haute-couture-christian-dior.html
PRINCIPAL: 1226246
TEXT:
D�fil�s Haute Couture : Christian Dior
Publi� le 26/01/2015 � 17:44
Mode - Accessoires - Shopping
Christian Dior pr�sente sa collection haute couture printemps-�t� 2015. RelaxNews �/� AFP PHOTO / FRANCOIS GUILLOT
5
Incontournable, le d�fil� Dior s'est d�roul� ce lundi 26 janvier au coeur des jardins du mus�e Rodin (Paris 7e) devant un parterre de c�l�brit�s. Pour ce nouvel opus, Raf Simons livre une collection r�tro tout en d�licatesse.
Les mannequins, v�tues des nouvelles pi�ces de la collection printemps-�t� 2015 de Christian Dior, d�ambulaient au beau milieu d'un �chafaudage g�ant, pr�sentant des pi�ces r�tro tout droit sorties des sixties.
Le cr�ateur a jou� avec les volumes, notamment au niveau des trench/imperm�ables transparents longs et amples superpos�s sur des mini-robes brod�es, mais �galement au niveau des robes. La maison a en effet propos� une large panoplie de silhouettes de danseuses, �voluant dans des robes pourvues de jupons XXL. Le tout parsem� de fines rayures ultra-color�es.
Tr�s pr�sents, les imprim�s se veulent graphiques, que ce soit sur les combinaisons-pantalons (rayures ou motifs g�om�triques) ou les robes. Bien �videmment, la fleur, l'une des signatures de la maison Dior, �tait de la partie, en imprim� ou d�licatement brod�e.
Autre d�tail r�tro : une multitude de petites robes d'inspiration 60's, port�es - d�tail ultra-chic - avec des cuissardes ou des bottines en latex, en version noir, vert, rouge et m�me orange.
RelaxNews
