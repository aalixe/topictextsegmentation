TITRE: Le caf�, nouvelle arme contre le cancer de la peau ?
DATE: 26-01-2015
URL: http://www.24matins.fr/le-cafe-nouvelle-arme-contre-le-cancer-de-la-peau-150859
PRINCIPAL: 1225681
TEXT:
>        Le caf�, nouvelle arme contre le cancer de la peau ?
Sant�
Un expresso
Une �tude am�ricaine lie une forte consommation de caf� � la diminution de d�velopper un m�lanome, forme agressive du cancer de la peau.
Bienfaits ou m�faits de la consommation de caf� ? Parue dans le Journal of The National Cancer Institute, une nouvelle �tude vient relancer le d�bat.
Men�e par l'Institut national du cancer, elle r�v�le qu'une forte consommation de caf� permettrait de r�duire l'apparition du m�lanome, qui est la forme la plus dangereuse de cancer de la peau.
Caf� contre cancer de la peau : plus de 4 tasses par jour ?
Pour mener � bien leur �tude, les chercheurs ont�suivi un gros panel de 447.000 individus blancs (amplitude d'�ge 51 � 70 ans), dont aucun n'�tait touch� par un cancer au d�but des 10 ans de suivi. A l'issue de la d�cennie, 2.900 d'entre eux avaient d�velopp� un m�lanome malin, et 1.900 autres un autre cancer de la peau.
Apr�s avoir analys� leur lieu de r�sidence (li�e � l'exposition aux ultra-violets), leur sexe et leur indice de masse corporelle, ils se sont pench�s sur leurs habitudes de consommation. C'est ainsi que les scientifiques ont observ� que les personnes buvant plus de 4 caf�s quotidiennement voyaient leur risque de d�velopper un m�lanome malin diminuer�de 20%. Ses composantes r�duiraient entre autres les outrages inflig�s � l'ADN.
Une �tude qui doit �tre poursuivie
Pr�cision importante : cet effet b�n�fique du caf� sur le cancer de la peau n'a �t� not� que sur des m�lanomes au stade avanc�, et non sur les m�lanomes diagnostiqu�s tr�s t�t.
Concernant les liens entre caf� et cancer, cette �tude n'est pas la premi�re. Une pr�c�dente avait d�j� conclu � ses b�n�fices chez la population f�minine, tandis que d'autres voyaient des effets positifs de cette boisson sur�la maladie d'Alzheimer , le diab�te ou encore les facult�s cognitives. Attention cependant, trop boire de caf� n'est pas indiqu� pour autant, notamment pour les plus jeunes.
Cr�dits photos : Marcelo_Krelling/Shuttestock.com
