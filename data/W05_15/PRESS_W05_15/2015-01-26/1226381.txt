TITRE: D�tecteurs de fum�e : trois appareils �non conformes� selon l�UFC-Que Choisir - La Voix du Nord
DATE: 26-01-2015
URL: http://www.lavoixdunord.fr/economie/detecteurs-de-fumee-trois-appareils-non-conformes-ia0b0n2623757
PRINCIPAL: 1226350
TEXT:
Gilles C.
01/02/2015 � 14 h 55
Antoine B., l'article L129-8 du code de la construction et de l'habitation impose � TOUT propri�taire d'installer chez lui, � compter du 8 mars 2015, un d�tecteur de fum�e et, vous, tout seul, vous dites que c'est faux ???? Lisez donc la l�gislation au lieu de nier l'�vidence. Marcel F., j'installe des d�tecteurs de fum�e depuis plusieurs d�cennies et ceci, sans le moindre probl�me. Si vous achetez un d�tecteur HOMOLOGUE, et que vous suivez la notice, il ne se d�clenchera pas pour un oui ou pour un non. Quant � votre odorat, il ne d�tectera pas forc�ment les fum�es si vous dormez. Vous vous asphyxierez sans vous en rendre compte. Quant � ceux qui disent que c'est une connerie et qui ne connaissent rien � la s�curit�, ils devraient r�fl�chir un peu plus.
Gilles C.
01/02/2015 � 12 h 57
Aucune r�action � cet article ??? http://www.lavoixdunord.fr/region/iwuy-une-famille-sauvee-grace-au-detec...
Marcel f.
26/01/2015 � 18 h 38
Antoine B change de bagnole il est grand temps ! Humour bien sur !!!
Freud S.
26/01/2015 � 18 h 16
L(installation des d�tecteurs de fum�e est obligatoire � partir du 08 mars 2015 " Arr�t� du 5 f�vrier 2013 relatif � l'application des articles R. 129-12 � R. 129-15 du code de la construction et de l'habitation" Comparer cela � l'�thylotest est aberrant et  limite criminel ;0(
Antoine B.
26/01/2015 � 17 h 42
Encore une belle connerie comme les �thylotests qui avaient une dur�e de vie limit� et donc ne pouvaient trop s�journer dans une bagnole !
Marcel f.
26/01/2015 � 16 h 45
je vais me r�p�ter mais il y a 1 ans j'ai pos� un appareil  pay� environs 15 euros et qui se d�clanchait tout seul et sans fum�e ! Si c'est pour nous imposer ce genre de gadget qui ne sont pas tous fiables autant se fier uniquement � notre odorat !
Antoine B.
26/01/2015 � 16 h 43
Faux, on n'est pas oblig� de mettre ces machins dans sa maison si on est propri�taire, le principal est d'�tre assur� contre l'incendie, les assureurs ne peuvent refuser le remboursement pour absence de ces d�tecteurs... Par contre le propri�taire d'un logement qu'il loue a int�r�t � en mettre afin d'�viter que le locataire qui viendrait � perdre un membre de sa famille dans l'incendie du logement ne se retourne contre lui.
