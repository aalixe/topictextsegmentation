TITRE: L'ex-otage en Centrafrique, Claudia Priest, est rentr�e en France | France | Nice-Matin
DATE: 26-01-2015
URL: http://www.nicematin.com/france/lex-otage-en-centrafrique-claudia-priest-est-rentree-en-france.2077996.html
PRINCIPAL: 1223201
TEXT:
Tweet
V�lizy-Villacoublay (France) (AFP)
La Fran�aise Claudia Priest, enlev�e pendant cinq jours en Centrafrique par des miliciens chr�tiens anti-balaka, est arriv�e dimanche peu apr�s 18H00 � l'a�roport militaire de Villacoublay, pr�s de Paris, ont constat� des journalistes de l'AFP.
L'humanitaire de 67 ans a �t� accueillie � sa descente de l'avion, un Falcon estampill� R�publique fran�aise, par son mari Armand, ses enfants B�rang�re et Florent, ainsi que le ministre des Affaires �trang�res Laurent Fabius.
Cheveux gris courts, en baskets, pantalon beige et petit sac a dos, elle a remerci� bri�vement toutes les personnes qui sont intervenues pour aider � sa lib�ration. "Je suis contente de retrouver le sol fran�ais, m�me si le sol centrafricain est aussi un peu ma patrie", a-t-elle bredouill�, la voix cass�e.
"Une prise d'otages, c'est toujours extr�mement violent, a d�clar� ensuite le ministre, mais quand en plus il s'agit d'une personne qui donne une grande partie de sa vie aux Centrafricains, c'est encore plus violent."
Cinq petits enfants de Mme Priest, originaire de Pont-de-Veyle (Ain), lui ont ensuite remis des roses blanches, une chacun. Elle les a embrass�s en sanglotant.
"Sa sant� est relativement bonne et son moral aussi", a assur� son mari Armand apr�s s'�tre entretenu avec elle.
Arriv�e en Centrafrique le 6 janvier pour une mission de deux semaines pour le compte de l'ONG m�dicale catholique Codis (Coordination dioc�saine de la sant�), fond�e par son mari, elle avait �t� enlev�e lundi, puis lib�r�e vendredi.
Un employ� centrafricain de l'ONG, kidnapp� en m�me temps qu'elle, a lui aussi �t� lib�r�.
Enlev�s sur une des avenues principales au nord de la capitale alors qu'ils rentraient d'une mission � 70 km de Bangui, ils avaient �t� emmen�s dans le quartier Boy-Rabe, fief des anti-balaka, puis � une quinzaine de km dans la brousse.
- Aucune ran�on -
"Ils �taient vraiment tr�s mena�ants, ils avaient des armes, ils avaient des poignards, des machettes, et ils me disaient: +on va te tuer, on va t'�gorger, on va te tuer+", avait-elle racont� samedi lors d'un entretien � l'AFP � Bangui.
"Ils m'ont frapp�e, ils m'ont tra�n�e (...), emmen�e jusqu'� une carri�re un peu loin sur la colline. Ensuite nous avons march�, sur au moins 15 km, (...) ils m'ont mis quelque chose pour qu'on ne reconnaisse pas que j'�tais fran�aise", avait ajout� Mme Priest.
La lib�ration a �t� "un tr�s gros soulagement", a-t-elle confi�. "Je n'y croyais pas du tout. C'est simplement quand j'ai vu Monseigneur Nzapalainga (l'archev�que de Bangui), (...) je l'ai pris dans mes bras, il m'a pris dans ses bras, l� j'ai dit: +c'est bon+".
L'archev�que de Bangui "�tait en t�te des n�gociations", selon Armand Priest, qui assure qu'aucune ran�on n'a �t� vers�e.
"Ni moi, ni mon �pouse n'en voulons aux ravisseurs. Ce sont des jeunes qui n'ont pas eu d'�ducation et qui n'ont aucun avenir", a-t-il ajout�. Ce qu'ils font "pour gagner quelques francs CFA, je peux presque l'excuser".
Les anti-balaka sont des milices principalement chr�tiennes qui se sont form�es pour lutter contre les rebelles, essentiellement musulmans, de la coalition S�l�ka qui avait pris le pouvoir en Centrafrique en mars 2013 avant d'en �tre chass�e en janvier 2014. Les deux camps sont accus�s d'avoir commis de graves exactions.
Les auteurs de l'enl�vement de la Fran�aise, le premier en Centrafrique depuis le d�but de l'intervention militaire fran�aise "Sangaris" dans ce pays en d�cembre 2013, protestaient contre l'arrestation de Rodrigue Nga�bona, dit "g�n�ral Andjilo", l'un de leurs chefs, soup�onn� d'�tre l'un des meneurs de massacres de musulmans dans la capitale centrafricaine.
Tweet
Tous droits de reproduction et de repr�sentation r�serv�s. � 2012 Agence France-Presse.
Toutes les informations (texte, photo, vid�o, infographie fixe ou anim�e, contenu sonore ou multim�dia) reproduites dans cette rubrique (ou sur cette page selon le cas) sont prot�g�es par la l�gislation en vigueur sur les droits de propri�t� intellectuelle. �Par cons�quent, toute reproduction, repr�sentation, modification, traduction, exploitation commerciale ou r�utilisation de quelque mani�re que ce soit est interdite sans l�accord pr�alable �crit de l�AFP, � l�exception de l�usage non commercial personnel. �L�AFP ne pourra �tre tenue pour responsable des retards, erreurs, omissions qui ne peuvent �tre exclus dans le domaine des informations de presse, ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
AFP et son logo sont des marques d�pos�es.
"Source AFP" ? 2015 AFP
France
Le d�lai de 15 jours au-del� duquel il n'est plus possible de contribuer � l'article est d�sormais atteint.
Vous pouvez poursuivre votre discussion dans les forums de discussion du site. Si aucun d�bat ne correspond � votre discussion, vous pouvez solliciter le mod�rateur pour la cr�ation d'un nouveau forum : moderateur@nicematin.com
