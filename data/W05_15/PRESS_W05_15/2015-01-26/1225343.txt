TITRE: Nouvelle semaine de mobilisation pour les transporteurs routiers
DATE: 26-01-2015
URL: http://www.boursier.com/actualites/economie/une-nouvelle-semaine-de-mobilisation-pour-les-transporteurs-routiers-26878.html?sitemap
PRINCIPAL: 1225336
TEXT:
Nouvelle semaine de mobilisation pour les transporteurs routiers
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � Nouvelle semaine de mobilisation pour les transporteurs routiers, qui se pr�parent � mener des actions dans une vingtaine de villes de France apr�s une pause ce week-end, dans la foul�e de l'�chec des n�gociations salariales avec le patronat. Ils�ont repris leur mouvement notamment dans le Nord, � Caen, � Angers, Bordeaux, Marseille, Vitrolles ou encore Chamb�ry. A Nantes, ils ont bloqu� la zone industrielle pr�s de l'actuel a�roport Nantes-Atlantique.
"L'objectif �tait de cibler les zones logistiques et les grands groupes, c'est mission accomplie", a d�clar� � l'agence de presse Reuters J�r�me V�rit� de la CGT Transport. "On ne peut que se f�liciter, tout le monde misait sur l'essoufflement du mouvement et ce n'est vraiment pas le cas."�La n�gociation annuelle obligatoire s'est conclue la semaine derni�re par un �chec...
La CFDT de retour
Les syndicats, qui d�noncent une "smicardisation rampante" de la profession, r�clament une "revalorisation salariale de 5%" pour tous les salari�s. Mais face � eux, les patrons estiment ne pas pouvoir�s'engager au-del� de "1 � 2 % de hausse". Ils �voquent les difficult�s �conomiques des transporteurs, le contexte de faible croissance et les concurrents venus d'Europe de l'Est.
Nouveau d�part
Le mouvement syndical pourrait prendre un nouveau d�part avec l'appel � la mobilisation lanc� par la CFDT , premier syndicat de transport routier, � partir du milieu de cette semaine.�Jusqu'� pr�sent, la CFDT ne s'�tait pas associ�e � la mobilisation conduite par l'intersyndicale CGT, FO, CFTC et CFE-CGC.�"Je pense que le patronat commence � s'inqui�ter", a indiqu� J�r�me V�rit�.
Claire Lemaitre � �2015, Boursier.com
