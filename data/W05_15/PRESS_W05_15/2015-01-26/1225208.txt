TITRE: Saint-Etienne - Paris SG (0-1) : Ibra éteint les Verts - Autres sports - Le Télégramme, quotidien de la Bretagne
DATE: 26-01-2015
URL: http://www.letelegramme.fr/autres-sports/saint-etienne-paris-sg-0-1-ibra-eteint-les-verts-26-01-2015-10505252.php
PRINCIPAL: 1225207
TEXT:
Saint-Etienne - Paris SG (0-1) : Ibra éteint les Verts
26 janvier 2015
Il a fallu une main, ou plutôt une épaule, du milieu stéphanois Jérémy Clément dans sa surface suite à un corner de Thiago Motta pour que le Paris SG réussisse à prendre l'avantage sur les Verts, hier à Geoffroy-Guichard. Bien entendu, c'est Zlatan Ibrahimovic qui a pris ses responsabilités et a transformé le penalty à la 70e. Grâce à ce succès (1-0) face au 4e, en clôture de la 22e journée de Ligue 1, le PSG, 3e, rejoint Marseille qui compte le même nombre de points désormais (44). (Photo AFP)
