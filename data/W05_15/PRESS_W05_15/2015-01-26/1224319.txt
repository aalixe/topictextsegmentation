TITRE: Le corps d'une britannique retrouvé dans le 74
DATE: 26-01-2015
URL: http://www.lefigaro.fr/flash-actu/2015/01/25/97001-20150125FILWWW00186-le-corps-d-une-britannique-retrouve-dans-le-74.php
PRINCIPAL: 0
TEXT:
le 25/01/2015 à 18:35
Publicité
Le corps sans vie d'une ressortissante britannique, âgée de 55 ans, a été retrouvé dimanche, peu après minuit, à proximité de la barre rocheuse d'Ardent à Montriond en Haute-Savoie, a-t-on appris auprès des secours en montagne. 
Partie seule samedi matin randonner depuis la télécabine d'Ardent, la victime n'avait plus donné signe de vie. Ce sont ses amis anglais, avec qui elle devait déjeuner le samedi midi qui ont donné l'alerte.
"Elle s'est écarté d'un sentier, a fait une glissade puis a chuté du haut d'une barre rocheuse. Elle est vraisemblablement morte sur le coup", a indiqué dimanche le peloton de gendarmerie de haute montagne (PGHM) de Chamonix.
Les gendarmes, appuyés de Groupement montagne sapeurs pompiers (GMSP) de Haute-Savoie ainsi que des membres du personnel des pistes de Montriond, ont retrouvé la victime peu après minuit.
