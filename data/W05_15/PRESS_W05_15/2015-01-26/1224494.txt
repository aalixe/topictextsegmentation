TITRE: Une autre ville du Nord-Est aux mains de Boko Haram | Le Devoir
DATE: 26-01-2015
URL: http://www.ledevoir.com/international/actualites-internationales/429950/nigeria-une-autre-ville-du-nord-est-aux-mains-de-boko-haram
PRINCIPAL: 0
TEXT:
Une autre ville du Nord-Est aux mains de Boko Haram
Taille du texte
26 janvier 2015 | Agence France-Presse | Actualit�s internationales
Photo:  Ali Kaya Agence France-Presse Des soldats de l�arm�e tchadienne, en route pour affronter Boko Haram
Boko Haram a pris dimanche une ville strat�gique du nord-est du Nigeria et sa base militaire, au moment o� le secr�taire d��tat am�ricain John Kerry promettait un soutien accru des �tats-Unis contre les insurg�s islamistes.
Le groupe arm� s�est empar� de la ville de Monguno, � quelque 130 km au nord-est de Maiduguri, la capitale de l��tat de Borno, elle-m�me frapp�e par une attaque simultan�e dimanche avant l�aube.
�Monguno est tomb�e, Monguno est tomb�e �, a d�clar� � l�AFP un haut responsable militaire sous couvert d�anonymat. � Nous les avons affront�s toute la nuit mais ils ont r�ussi � prendre la ville, y compris les casernes militaires�.
L�arm�e nig�riane a affirm� que de � nombreux � combattants islamistes avaient �t� tu�s durant les combats � Maiduguri et dans la ville voisine de Konduga, o� les assauts de Boko Haram ont �t� repouss�s.
Mais, � Monguno, les forces a�riennes sont pass�es � l�action, � apr�s que les troupes ont d� battre en retraite �, a indiqu� le porte-parole du minist�re de la D�fense, Chris Olukolade.
La chute de cette ville, que Boko Haram a d�j� essay� de prendre deux fois par le pass�, pourrait avoir de lourdes cons�quences.
En effet, les islamistes se sont ainsi empar�s de la derni�re base militaire avant Maiduguri � qui fut leur fief historique � depuis les zones recul�es du nord-est pass�es ces derniers mois sous leur contr�le.
Kerry en visite
C�est dans ce lourd contexte que John Kerry a effectu� une visite de quelques heures � Lagos, capitale �conomique du pays le plus peupl� d�Afrique, au moment o� la multiplication des violences islamistes fait planer une ombre sur la pr�sidentielle du 14 f�vrier.
Des centaines de milliers de personnes ont d� fuir � cause des tueries dans le nord-est et pourraient ne pas �tre en mesure de voter, ce qui a conduit certains � appeler � un report du scrutin.
Il est �imp�ratif que les �lections se tiennent � la date pr�vue�, a cependant lanc� M. Kerry.
Face � Boko Haram, M. Kerry a affirm� que les �tats-Unis �taient pr�ts � accro�tre leur aide � leur partenaire nig�rian.
�Nous sommes pr�ts � faire plus�, a-t-il dit, tout en soulignant que cela d�pendait notamment du caract�re �transparent et pacifique� des prochaines �lections.
Les �tats-Unis partagent des renseignements sur le groupe islamiste avec le Nigeria et ont d�p�ch� l�an dernier des conseillers militaires et civils pour tenter de retrouver les plus de 200 lyc�ennes enlev�es par Boko Haram � Chibok (nord-est).
�
�
�dition abonn�
La version longue de certains articles (environ 1 article sur 5) est r�serv�e aux abonn�s du Devoir. Ils sont signal�s par le symbole suivant :
�
