TITRE: Miss France : � J�avais peur d��tre �lue Miss Univers � (VID�O) - La Voix du Nord
DATE: 26-01-2015
URL: http://www.lavoixdunord.fr/region/miss-france-j-avais-peur-d-etre-elue-miss-univers-ia0b0n2622871
PRINCIPAL: 0
TEXT:
En images : Miami, lieu paradisiaque pour l��lection de Miss Univers
- Camille, vous ne parvenez pas au top 10�
� C�est d�j� une tr�s grande fiert� pour moi d��tre dans les quinze plus belles femmes de l�univers� selon ce concours ! Je f�licite ma copine Paulina (Miss Colombie, d�sormais d�tentrice de la couronne de Miss Univers). Je suis contente pour elle. �
- Comment s�est pass� ce concours  ?
� Il a n�cessit� beaucoup d�endurance, il y a eu beaucoup de fatigue vers la fin. Avec l��lection de Miss France en d�cembre et les quinze jours de marathon m�diatique qui ont suivi, j�ai accumul� beaucoup de fatigue. J�aurais quand m�me souhait� faire un top 10. Mais ce n�est pas grave �
- C�est donc la d�ception que vous �prouvez � cet instant�
� Oui, un peu, je me dis que j�aurais m�rit� ma place dans le top 10. Mais je n�ai pas de regret. J�ai fait ce que je voulais faire, et tout ce que je savais faire. En m�me temps, j�avais peur d��tre �lue Miss Univers. Je n�ai m�me pas encore v�cu mon ann�e de Miss France. J�avais peur de passer � c�t� et d��tre directement projet�e vers les �tats-Unis. On a pu rencontrer Gabriela Isler, Miss Univers sortante. Elle n�a pas l�air tr�s heureuse de son ann�e. Elle semble assez triste. Les contraintes sont tellement lourdes que �a doit emp�cher de profiter de l�exp�rience. �
- Cette �lection n�a rien � voir avec celle de Miss France�
� Non, c�est d�ailleurs tr�s �trange. Pendant les r�p�titions, la coach ne cessait de nous r�p�ter qu�il ne s�agissait pas d��lire seulement une plastique. Mais on se rend compte qu�en fait, il s�agit bien de �a. D�j�, un concours de beaut� o� la chirurgie esth�tique est autoris�e, �a n�est pas tout � fait normal. Dans ce cas, cela revient � �lire celle qui a le meilleur chirurgien. �
- Beaucoup de candidates ont eu recours � la chirurgie esth�tique  ?
� Oui� Plus de la moiti�. Pas forc�ment de tr�s grosses op�rations, mais c�est un peu de la triche. �
- Quels ont �t� vos atouts et vos handicaps ?
� Mes atouts, je pense, �taient ma d�marche et ma spontan�it�. Ensuite, ce qui a d� me desservir, c�est de venir d�un pays comme la France, de ne pas �tre d�Am�rique latine. Cela joue. Je ne voudrais pas �tre m�chante, mais les miss V�n�zuela, Colombie, Argentine et Br�sil sont syst�matiquement dans le top 15 chaque ann�e, m�me si elles ne le m�ritent pas forc�ment. Miss Univers, c�est quand m�me quelque chose de tr�s politique. �
- Auriez-vous �t� heureuse avec cette couronne  ?
� J�aurais �t� fi�re de repr�senter mon pays. En cela, le r�sultat est une d�ception. L�organisation de Miss France comptait sur moi. Mais c�est vrai que �a n�est pas quelque chose qui me repr�sente. Je n�ai pas envie de dire aux petites filles que pour �tre belle, il faut �tre tr�s maquill�e, tr�s coiff�e, il faut mettre des talons hauts et des robes courtes. Je ne veux pas que les petites filles aient envie d��tre comme �a. J�ai envie qu�elles veuillent �tre naturelles, intelligentes, spontan�es, et qu�elles sachent parler. �
- Vous retirez quand m�me du positif de cette exp�rience  ?
� J�ai v�cu un mois extraordinaire. Je n�ai pas vraiment profit� de Miami, mais j�ai rencontr� des filles super que je vais essayer de revoir bient�t puisqu�elles font toutes un peu du mannequinat sur Paris. J�ai cr�� des amiti�s � travers le monde. Avec Miss Australie par exemple. �a n��tait pas le cas au d�but. On se regardait un peu du coin de l��il. Nous �tions blondes, nous savions que ce serait soit l�une, soit l�autre au bout d�un moment� Mais en fait, on s�est pris d�affection. C�est devenu une comp�tition bon enfant. J�aime aussi beaucoup Miss Finlande ; Miss Gabon, ma voisine de chambre ; Miss Ha�ti ; et �le Maurice, Nouvelle-Z�lande� Autre aspect positif, j�ai pratiqu� mon anglais et mon espagnol, �a n�est pas n�gligeable. �
- Votre r�gion vous manque-t-elle  ?
� Oui, ma famille me manque �norm�ment. Ma vie aussi. J�adorais aller en cours. J�adorais aller l� o� je travaillais, dans une boutique de pr�t-�-porter d�Euralille. Et j�ai h�te de d�couvrir ce qui m�attend en tant que Miss France. �
- Un mot pour les habitants du Nord-Pas-de-Calais  ?
� Je voudrais vraiment les remercier. J�esp�re qu�ils sont fiers de moi, parce que moi, je suis fi�re d�avoir repr�sent� le Nord-Pas-de-Calais � Miami ! �
Cet article vous a int�ress� ? Poursuivez votre lecture sur ce(s) sujet(s) :
