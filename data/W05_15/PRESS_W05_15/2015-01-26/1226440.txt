TITRE: Un nouveau candidat � la pr�sidence de la FIFA - Goal.com
DATE: 26-01-2015
URL: http://www.goal.com/fr/news/28/main/2015/01/26/8333042/un-nouveau-candidat-%25C3%25A0-la-pr%25C3%25A9sidence-de-la-fifa
PRINCIPAL: 1226434
TEXT:
Un nouveau candidat � la pr�sidence de la FIFA
Getty Images
0
26 janv. 2015 20:03:53
Michael Van Praag (67 ans), pr�sident de la F�d�ration n�erlandaise, a pr�sent� sa candidature � la pr�sidence de la FIFA.
Il y a donc un quatri�me candidat. Le pr�sident de la F�d�ration n�erlandaise de football, Michael Van Praag, s'est d�clar� candidat � la pr�sidence de la FIFA. Il affrontera le Suisse, Sepp Blatter, le prince de Jordanie, Ali Ben Al-Hussein, le Fran�ais, J�r�me Champagne, et peut-�tre David Ginola.
�
