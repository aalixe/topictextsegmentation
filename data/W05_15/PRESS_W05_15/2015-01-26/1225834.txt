TITRE: Les ma�tres de la BD publient un album en hommage � Charlie Hebdo - L'Express
DATE: 26-01-2015
URL: http://www.lexpress.fr/culture/livre/les-maitres-de-la-bd-publient-un-album-en-hommage-a-charlie-hebdo_1644792.html
PRINCIPAL: 1225832
TEXT:
Les ma�tres de la BD publient un album en hommage � Charlie Hebdo
Par LEXPRESS.fr avec AFP, publi� le
26/01/2015 � 17:48
L'ouvrage collectif rassemble 173 signatures de dessinateurs de presse et auteurs de bandes dessin�es dont Zep, Martin Vidberg ou Manu Larcenet. Il sera disponible en librairie d�s le 5 f�vrier.�
Zoom moins
"La BD est Charlie" para�tra le 5 f�vrier prochain.
La BD est Charlie / Editions Gl�nat
Un album multi-�diteurs en hommage � Charlie Hebdo et aux victimes des attentats du 7 janvier, r�alis� par 173 dessinateurs de presse et auteurs de bandes dessin�es, sortira le 5 f�vrier, a annonc� lundi le Syndicat national de l'�dition (SNE). �
Guy Delisle, Philippe Geluck: de prestigieux participants
Intitul� La BD est Charlie, l'ouvrage collectif r�alis� � l'initiative du groupe BD du SNE, rassemble 183 dessins. L'int�gralit� des b�n�fices de sa commercialisation sera revers�e aux familles des victimes.�
Parmi les auteurs ayant particip� � l'album figurent Blutch , Robert Crumb , Guy Delisle , Philippe Geluck , Manu Larcenet , Ren� P�tillon , Lo�c S�cheresse, Lewis Trondheim , Martin Vidberg ou encore Zep . De nombreux �diteurs sp�cialis�s prennent part � l'op�ration, dont l'ensemble des marques de Madrigall (Castermab, Fluide Glacial , Futuropolis, Deno�l Graphic).�
Un tirage � 100.000 exemplaires
L'album, qui para�tra le 5 f�vrier, sera tir� � 100.000 exemplaires. Il sera vendu 10 euros. Il sera disponible en avant-premi�re au 42e Festival international de la bande dessin�e d'Angoul�me , qui s'ouvre jeudi.�
"Dessinateurs, �diteurs et diffuseurs ont offert leurs droits et leurs services; imprimeurs, papetiers et distributeurs ont travaill� � prix co�tant; les libraires, enfin, ont tous re�u la m�me remise, minor�e", ajoute le SNE.�
Avec
