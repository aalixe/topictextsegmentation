TITRE: Offrez-vous le maillot de David Beckham, le pull de Beyonc� ou le ballon de PSG-Barcelone � metronews
DATE: 26-01-2015
URL: http://www.metronews.fr/sport/offrez-vous-le-maillot-de-david-beckham-le-pull-de-beyonce-ou-le-ballon-de-psg-barcelone/moaz!pflywsdBEZ15A/
PRINCIPAL: 0
TEXT:
Cr�� : 26-01-2015 14:48
Offrez-vous le maillot de Beckham, le pull de Beyonc� ou le ballon de PSG-Barcelone
FOOTBALL � Le Paris Saint-Germain organise mardi soir une vente aux ench�res au profit de sa fondation qui f�te ses quinze ann�es d'existence. Des articles in�dits seront ainsi propos�s, comme un maillot de David Beckham, le ballon du match PSG-Barcelone ou encore le pull de Beyonc�...
Tweet
�
David Beckham lors de son premier match disput� sous les couleurs du PSG en f�vrier 2013. Photo :�ALEXIS REAU/SIPA
Si les supporters du Paris Saint-Germain ou les fans de football en r�gle g�n�rale souhaitent se faire un petit plaisir en achetant un objet phare de leur club pr�f�r�, ils devront mettre des sous de c�t� en pr�vision d'une vente aux ench�res pr�vue mardi soir. La fondation PSG qui f�te son quinzi�me anniversaire, a d�cid� de mettre en vente des lots de prestige afin de r�colter un maximum d'argent lors de cette soir�e organis�e au Pavillon Gabriel.
Tous les articles propos�s seront mis � prix 500 euros, et les amateurs de ballon rond, de sport et m�me de musique pourront y trouver leur bonheur. Ainsi, le premier maillot port� par l'Anglais David Beckham en 2013 lors de PSG-OM est pr�sent� sur le site Artcurial , tout comme celui de Lucas, d�dicac� par le Br�silien.
Beyonce, Rafael Nadal, Roger Federer �galement mis � l'honneur
Vous pourrez �galement vous offrir le ballon de l a victoire des Parisiens face au FC Barcelone en septembre dernier, sign� par l'ensemble de l'effectif parisien, ou bien celui du troph�e des champions. Dans le genre lots in�dits, sont �galement propos�s, le coup d'envoi d'un match du PSG, les raquettes de Rafael Nadal, Roger Federer ou encore la r�plique du pull brod� port� par Beyonc� lors de sa venue au Parc des Princes au c�t� de Jay-Z pour PSG-Barcelone .
L�ensemble des fonds r�colt�s lors de cette vente aux ench�res sera revers� � la Fondation PSG. Depuis 15 ans, environ 150 000 enfants ont profit� des programmes mis en oeuvre par la fondation. L�ann�e derni�re, la vente aux ench�res avait permis de r�unir pr�s de 200 000 euros. L'objectif sera bien �videmment de d�passer cette somme, � l'occasion du quinzi�me anniversaire.
Adrien Chantegrelet
