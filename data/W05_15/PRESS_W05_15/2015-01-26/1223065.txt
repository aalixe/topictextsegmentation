TITRE: VIDEO. PSG: Chambr� par Zlatan Ibrahimovic, Paul Baysse vanne le Su�dois - 20minutes.fr
DATE: 26-01-2015
URL: http://www.20minutes.fr/sport/football/1525291-20150125-saint-etienne-psg-zlatan-ibrahimovic-chambre-paul-baysse-rend-pareille
PRINCIPAL: 0
TEXT:
R�gles de mod�ration des commentaires
Afin d'�tre publi�e, votre note :
- Doit se conformer � la l�gislation en vigueur. En particulier et de mani�re non exhaustive sont proscrits : l'incitation � la haine raciale et � la discrimination, l'appel � la violence ; la diffamation, l'injure, l'insulte et la calomnie ; l'incitation au suicide, � l'anorexie, l'incitation � commettre des faits contraires � la loi ; les collages de textes soumis  au droit d'auteur ou au copyright ; les sous-entendus racistes, homophobes, sexistes ainsi que les blagues stigmatisantes.
- De plus, votre message doit respecter les r�gles de biens�ance : �tre respectueux des internautes comme des journalistes de 20Minutes, ne pas �tre hors-sujet et ne pas tomber dans la vulgarit�.
- D'autre part, les messages publicitaires, post�s en plusieurs exemplaires, r�dig�s en majuscules, contenant des liens vers des sites autres que 20Minutes ou trop longs seront supprim�s. De m�me, 20Minutes.fr respecte tous les engagements de ses lecteurs, mais n'autorise pas le pros�lytisme. Les commentaires ne doivent pas appeler au vote pour un parti, un syndicat ou un candidat quel qu'il soit.
Fermer
Fermer
Votre commentaire a bien �t� pris en compte. Il sera visible sur le site tr�s prochainement s'il respecte la charte 20 Minutes.
Fermer
