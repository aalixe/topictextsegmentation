TITRE: Plainte contre quatre fabricants de cigarettes | Mediapart
DATE: 26-01-2015
URL: http://www.mediapart.fr/journal/france/250115/plainte-contre-quatre-fabricants-de-cigarettes
PRINCIPAL: 0
TEXT:
La lecture des articles est r�serv�e aux abonn�s.
Comme l'a r�v�l� Le Journal du dimanche, le Comit� national contre le tabagisme (CNCT) a d�pos� plainte contre les fabricants de cigarettes Marlboro (Philip Morris), Camel (Japan Tobacco), Lucky Strike (British American Tobacco) et Gauloises (Imperial Tobacco-Seita) pour entente illicite sur les prix, a confirm� dimanche Yves Martinet, son pr�sident.
��Nous avons d�pos� plainte aupr�s du parquet financier de Paris, a pr�cis� Yves Martinet. Les quatre grands industriels du tabac travaillent en cartel, ils font tout pour que les prix augmentent de fa�on mod�r�e, pour que la consommation ne diminue pas.�� Selon lui, les industriels vis�s par la plainte ��se limitent � de petites augmentations qui sont en fait des rattrapages ...
