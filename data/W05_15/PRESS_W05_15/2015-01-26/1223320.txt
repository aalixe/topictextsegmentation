TITRE: Haute-couture: Versace ouvre le bal en c�l�brant les courbes f�minines - LExpress.fr
DATE: 26-01-2015
URL: http://www.lexpress.fr/actualites/1/actualite/haute-couture-versace-ouvre-le-bal-en-celebrant-les-courbes-feminines_1644464.html
PRINCIPAL: 0
TEXT:
Zoom moins
Zoom plus
Les mannequins Amber Valetta (� d.) et Eva Herzigova (� g.) pr�sentent les cr�ations de la styliste Donatella Versace, le 25 janvier 2015 � Paris
afp.com/Miguel Medina
Dans les salons de la Chambre de commerce et d'Industrie de Paris, les actrices am�ricaines Goldie Hawn, sa fille Kate Hudson, Michelle Rodriguez, la chanteuse britannique Ellie Goulding avaient pris place au premier rang du d�fil� Atelier Versace.�
Asym�triques, des robes longues en soie noires, rouges, blanches, bleu �lectrique �pousent le corps de pr�s, se fendent, se d�coupent en volutes, comme peintes sur la peau. Fid�les � l'esth�tique exub�rante de la griffe italienne, des mod�les brod�s de perles scintillent, des dentelles se font toiles d'araign�es, les robes sir�ne et � tra�ne se succ�dent.�
Eva Herzigova et Amber Valletta, mannequins stars des ann�es 1990, ont cl�tur� sous les applaudissements du public ce show qui donnait le coup d'envoi des d�fil�s haute couture. Ce rendez-vous parisien de prestige, auquel participent 24 maisons jusqu'� jeudi, a pris la rel�ve des d�fil�s de mode masculine, qui se sont achev�s dimanche soir avec Saint Laurent.�
A la diff�rence des Semaines du pr�t-�-porter qui se tiennent aussi � New York, Londres et Milan, Paris est la seule capitale de la mode � accueillir les pr�sentations de haute couture, deux fois par an, en janvier et juillet. �
Cette sp�cificit� parisienne b�n�ficie d'une appellation juridiquement prot�g�e, attribu�e par une commission du minist�re de l'Industrie en fonction de crit�res pr�cis, r�compensant notamment un travail � la main et sur-mesure r�alis� dans les ateliers du couturier.�
D'autres maisons pr�sentent leur d�fil� dans ce calendrier de la haute couture en tant que "membres correspondants" ou "membres invit�s", comme Atelier Versace, Valentino, Viktor&Rolf, ou encore Yiqing Yin et Schiaparelli.�
- Margiela, absent notable -�
Cette maison l�gendaire fond�e dans les ann�es 1930 par Elsa Schiaparelli, grande rivale de Coco Chanel, pr�sentera lundi matin un d�fil� malgr� le d�part surprise en novembre dernier de son directeur de la cr�ation, l'Italien Marco Zanini, qui l'avait relanc�e.�
Endormie pendant 60 ans, la griffe avait fait son retour sur les podiums parisiens de haute couture en janvier 2014, sous l'impulsion de son propri�taire, l'homme d'affaires italien Diego Della Valle (groupe Tod's).�
Si Schiaparelli n'a pas encore annonc� le nom du directeur artistique qui remplacera Marco Zanini, la porte-parole de la maison, l'ancien mannequin Farida Khelfa, a indiqu� au site sp�cialis� WWD que le d�cor du d�fil� serait sign� du photographe Jean-Paul Goude.�
Outre les incontournables d�fil�s des maisons Dior et Chanel lundi et mardi, les yeux seront aussi tourn�s vers le show de Jean Paul Gaultier, qui avait annonc� peu avant le d�but de la Fashion Week en septembre qu'il arr�tait le pr�t-�-porter pour se concentrer sur la haute couture et d'autres projets.�
Absent notable du calendrier, la Maison Margiela, o� officie d�sormais John Galliano, pr�sentera sa collection "artisanale" sur rendez-vous, dans ses locaux. Le couturier a pr�f�r� Londres pour faire son retour sur les podiums, pr�s de quatre ans apr�s son fracassant licenciement par Dior � cause des propos antis�mites qu'il avait tenus, ivre, dans un bar parisien. Il avait alors �t� exclus du monde de la mode. �
Cette collaboration a suscit� une forte curiosit� en raison des esth�tiques radicalement diff�rentes de la maison au style minimaliste et du cr�ateur flamboyant. Le d�fil�, qui alliait les codes de Margiela - le blanc, la d�construction - � la th��tralit� de Galliano -spectaculaire robe rouge, tra�nes, chaussures baroques -, a �t� globalement salu� par la critique comme un retour prometteur.�
Par
