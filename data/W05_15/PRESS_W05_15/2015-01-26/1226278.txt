TITRE: A Ta�wan, les enfants n'ont l�galement plus le droit de passer trop de temps devant un �cran | Slate.fr
DATE: 26-01-2015
URL: http://www.slate.fr/story/97249/taiwan-enfants-ecran
PRINCIPAL: 1226277
TEXT:
A Ta�wan, les enfants n'ont l�galement plus le droit de passer trop de temps devant un �cran
[220/365] Ipad Petras Gagilas via Flickr CC License by
Vous trouvez que vos enfants passent trop de temps sur Internet? Si vous habitez Ta�wan vous pouvez d�sormais l�galement les emp�cher d�utiliser une tablette ou un t�l�phone portable trop longtemps.
Comme l�explique le site Quartz, le l�gislateur ta�wanais vient de d�cider que les mineurs n�auraient d�sormais plus le droit �d�utiliser constamment des produits �lectroniques pour une p�riode de temps non raisonnable�.
Ta�wan a en fait �tendu une loi d�j� en place, qui interdit au moins de 18 ans de boire, fumer, regarder du porno ou m�me m�cher des noix de b�tel . Les parents encourent d�sormais une amende de pr�s de 1.400 euros s�ils ne respectent pas la r�glementation.
La loi risque cependant d��tre compliqu�e � appliquer, puisqu�elle ne sp�cifie pas � partir de combien d�heures l'utilisation continue d�une tablette ou d�un smartphone n�est �pas raisonnable�.
PChang Hsiu-yuan, le directeur du d�partement des services de protection au minist�re de la Sant� de Ta�wan, explique:
�30 minutes serait consid�r� comme une p�riode de temps raisonnable pour l�usage de dispositifs �lectroniques par des enfants de plus de deux ans.�
Selon les sp�cialistes, le temps pass� par les mineurs devant des �crans est un vrai probl�me. D�apr�s l�Acad�mie am�ricaine des p�diatres, un Am�ricain de 8 ans passe en moyenne 8 heures par jour devant un �cran alors qu�elle ne recommande que 2 heures d�utilisation maximum.
En France, les jeunes passent pr�s de 1.200 heures par an , soit 3,30 heures par jour, devant un ordinateur ou la t�l�vision.
A Ta�wan, 81% des moins de 15 ans sont myopes
Cette utilisation des nouvelles technologies a un impact n�gatif sur la sant� et l�attention des enfants. Comme nous vous l'expliquions sur Slate , � Ta�wan, le nombre d�enfants de moins de 15 ans myopes a atteint 81%. L�utilisation de smartphones aurait aussi un impact sur le sommeil des enfants et poserait�m�me�un risque d'empoisonnement .
Comme l�explique Quartz, Ta�wan n�est pas le seul pays � voir pris des dispositions pour lutter contre l�addiction aux �crans. La Chine limite depuis 2005 l'usage de jeux vid�o en ligne � 5 heures.
Pour l�instant, la France pr�f�re miser sur la sensibilisation et l��ducation. C�est ce qu�expliquait le psychologue clinicien Jean-Pierre Couteron au Monde en octobre 2014:
�On a du mal � faire entendre que les �ducateurs ont un r�le � jouer pour montrer que ces objets sont une ouverture au monde (�) mais qu�il est n�cessaire d�en organiser la rencontre, de mettre en garde sur les dangers de certains contenus et de poser des limites (heures, lieux, dur�e�).�
Le CSA avait notamment lanc�, en novembre 2014, une campagne de sensibilisation sur le sujet.
Partagez cet article
