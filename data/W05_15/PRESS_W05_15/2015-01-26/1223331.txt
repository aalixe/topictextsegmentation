TITRE: Serie A: La Juventus chausse les bottes de Sept Lieues - DH.be
DATE: 26-01-2015
URL: http://www.dhnet.be/sports/football/europe/championnatsetrangers/serie-a-la-juventus-chausse-les-bottes-de-sept-lieues-54c56b0a35701f35435af051
PRINCIPAL: 1223328
TEXT:
Calcio: la Lazio enfonce l'AC Milan
Championnats �trangers
La Juventus et Paul Pogba, vainqueurs du Chievo V�rone (2-0), s'envolent sept longueurs devant l'AS Rome, frein�e � la Fiorentina (1-1), dimanche pour la 20e journ�e du Championnat d'Italie.
En deux gestes de classe pure, Paul Pogba a encore une fois lib�r� la Juventus, qui calait contre le rel�gable Chievo. Il a sign� son quatri�me but sur les quatre derniers matches et offert le second � Stephan Lichtsteiner.
D'une superbe frappe du gauche, qui n'est pas son meilleur pied, il est venu � bout de la r�sistance d'Albano Bizzarri, le gardien v�ronais, qui repoussait depuis une heure les assauts de la "Vieille Dame".�Pogba a cosign� le second but, le gardien repoussant sur Lichtsteiner sa vol�e du droit, apr�s un contr�le a�rien digne d'un num�ro de cirque.
Ces nouveaux gestes g�niaux, et d�cisifs, illustrent la prise de pouvoir de Pogba sur le jeu de la Juve. Andrea Pirlo, bless�, �tait forfait, laissant au jeune fran�ais (21 ans) le r�le d'aimant � ballons des "Bianconeri".
Avec cette troisi�me victoire convaincante de rang, plus le 6-1 pass� � V�rone en Coupe d'Italie, la patronne a retrouv� toute sa ma�trise dans le sillage de son jeune g�nie.
La Roma a limit� les d�g�ts � Florence, mais elle se tra�ne depuis un mois avec ce quatri�me nul en cinq rencontres. Avant cette s�rie, elle �tait revenue � une seule longueur de la Juve.
Si l'�quipe de Rudi Garcia a laiss� six points en route � sa rivale pour le "scudetto", elle le doit � la baisse de son pouvoir offensif (5 buts sur ces cinq matches) et � son inconstance, soulign�e par l'entra�neur fran�ais lui-m�me.
Contre la Fiorentina, la Roma a encore une fois compl�tement laiss� filer sa premi�re p�riode. �Domin�e, elle s'est inclin�e sur le troisi�me but de la semaine de Mario Gomez, enfin r�veill�.
Apr�s la pause, la Roma s'est r�veill� et a �galis� par Adem Ljajic, ex de la Fiorentina, sur une grande action de Juan Iturbe, qui a r�sist� � la charge du d�fenseur dans la surfacer pour servir le Serbe.
Le match vivant, entre deux �quipes joueuses, aurait pu basculer d'un c�t� comme de l'autre, mais n'a finalement pas donn� de vainqueur.
L'Inter Milan s'est fait surprendre � la derni�re seconde par le Torino (1-0) et s'enfonce dans la crise, comme son cousin de l'AC Milan, battu samedi � la Lazio Rome (3-1) pour la troisi�me fois sur les quatre derni�res journ�es.
Emiliano Moretti (90+4), oubli� par la d�fense int�riste, a offert de la t�te une victoire chez l'Inter que le "Toro" attendait depuis 1988.
Incapable de secouer le Torino, le club "nerazzurro" a r��dit� la pi�tre performance d'Empoli (0-0) une semaine plus t�t, avec une conclusion pire.
Lukas Podolski et Xherdan Shaqiri, ses deux recrues vedettes, sont rest�s assez transparents. Cette d�faite renvoie en arri�re l'Inter Milan de Roberto Mancini, apr�s avoir battu la "Samp" en 8e de finale de Coupe d'Italie, mercredi (2-0), avec le premier but du Suisse Shaqiri.
Sur le m�me sujet :
