TITRE: Le corps d'Herv� Gourdel attendu � 13h30 � Roissy  | Faits Divers | Var-Matin
DATE: 26-01-2015
URL: http://www.varmatin.com/faits-divers/le-corps-dherve-gourdel-attendu-a-13h30-a-roissy.2017140.html
PRINCIPAL: 1223906
TEXT:
Tweet
Le corps de l'otage ni�ois Herv� Gourdel, enlev� puis d�capit� fin septembre par des jihadistes alg�riens, doit �tre transf�r� lundi vers la France, douze jours apr�s sa d�couverte en Kabylie (est d'Alger), a appris dimanche l'AFP de source proche du dossier.
Deux m�decins l�gistes fran�ais et la soeur du guide de haute montagne, enlev� le 21 septembre au coeur du massif du Djurdjura o� il �tait all� faire de la randonn�e, se sont rendus en Alg�rie pour confirmer son identification, selon cette source.
Selon des sources a�roportuaires fran�aises, la d�pouille d'Herv� Gourdel doit arriver � Roissy-Charles-de-Gaulle lundi vers 13h30. Une c�r�monie devrait �tre organis�e au pavillon de r�ception de l'a�roport.
Des tests g�n�tiques ont confirm� cette semaine que le corps exhum� en Kabylie �tait bien celui du guide, les m�decins l�gistes ayant compar� l'ADN du touriste fran�ais � celui de sa soeur.
>> RELIRE. Le corps du Ni�ois Herv� Gourdel retrouv� en Alg�rie
Le transfert de la d�pouille n�cessite la d�livrance d'un acte de d�c�s mentionnant la date de sa mort, que les m�decins l�gistes sont parvenus � d�terminer, a ajout� la source proche du dossier, sans plus de pr�cision.
Herv� Gourdel, guide de haute montagne de 55 ans originaire du sud de la France, avait �t� enlev� le 21 septembre par le groupe arm� Jund al-Khilafa, qui a affirm� l'avoir ex�cut� en repr�sailles � l'engagement de la France aux c�t�s des Etats-Unis dans les frappes contre le groupe Etat islamique en Irak.
Arriv� en Alg�rie le 19 septembre pour faire du trekking dans le massif du Djurdjura, il avait avait �t� enlev� pr�s du sommet Lala Khedidja.
Ses cinq compagnons alg�riens avaient �t� rel�ch�s apr�s 14 heures de s�questration. L'arm�e alg�rienne, qui avait mobilis� 3.000 soldats, a retrouv� le corps d'Herv� Gourdel le 15 janvier, enterr� dans une for�t de ch�nes et d'oliviers tout pr�s du village Takheldjit, � une vingtaine de km du lieu de son enl�vement.
Sa d�pouille a �t� d�couverte sur les indications d'un jihadiste captur� par l'arm�e, selon les informations officielles.
Tweet
Tous droits de reproduction et de repr�sentation r�serv�s. � 2012 Agence France-Presse.
Toutes les informations (texte, photo, vid�o, infographie fixe ou anim�e, contenu sonore ou multim�dia) reproduites dans cette rubrique (ou sur cette page selon le cas) sont prot�g�es par la l�gislation en vigueur sur les droits de propri�t� intellectuelle. �Par cons�quent, toute reproduction, repr�sentation, modification, traduction, exploitation commerciale ou r�utilisation de quelque mani�re que ce soit est interdite sans l�accord pr�alable �crit de l�AFP, � l�exception de l�usage non commercial personnel. �L�AFP ne pourra �tre tenue pour responsable des retards, erreurs, omissions qui ne peuvent �tre exclus dans le domaine des informations de presse, ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
AFP et son logo sont des marques d�pos�es.
Avec AFP
Faits Divers , Derni�re minute , France , Monde
Le d�lai de 15 jours au-del� duquel il n'est plus possible de contribuer � l'article est d�sormais atteint.
Vous pouvez poursuivre votre discussion dans les forums de discussion du site. Si aucun d�bat ne correspond � votre discussion, vous pouvez solliciter le mod�rateur pour la cr�ation d'un nouveau forum : moderateur@nicematin.com
Vos derniers commentaires
