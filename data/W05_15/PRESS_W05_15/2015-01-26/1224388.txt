TITRE: Antarctique : La fonte du plus grand glacier a commenc�. Info - Redon.maville.com
DATE: 26-01-2015
URL: http://www.redon.maville.com/actu/actudet_-antarctique-la-fonte-du-plus-grand-glacier-a-commence_54135-2703284_actu.Htm
PRINCIPAL: 1224386
TEXT:
Twitter
Google +
La fonte du plus grand glacier de l'Arctique pourrait engendrer une hausse de six m�tres du niveau de la mer.� Photo: Reuters
Le plus grand glacier de l'Antarctique oriental, dont la disparition pourrait engendrer une hausse de 6 m�tres du niveau des mers, a commenc� � fondre � cause du r�chauffement
Le glacier Totten, qui fait 120 kilom�tres de long sur plus de 30�km de large, �tait jusque-l� consid�r� comme �tant situ� dans une zone �pargn�e par les courants chauds, o� la glace est donc tr�s stable et peu sujette aux variations.�
Le glacier fond par en-dessous
De retour d'une exp�dition sur place, des scientifiques australiens ont cependant indiqu� que l'eau autour du glacier s'�tait av�r�e plus chaude qu'attendue et que le glacier risquait de fondre par en dessous.���Nous savions gr�ce � des donn�es satellitaires que le glacier perdait de l'�paisseur mais nous ignorions pourquoi��, a d�clar� Steve Rintoul, chef de l'exp�dition.La temp�rature des eaux autour du glacier �tait environ 1,5 degr� plus �lev�e que dans d'autres zones explor�es � l'occasion de ce s�jour dans l'Antarctique pendant l'�t� austral, a-t-il ajout�.
Pas de mont�e brutale
��Le fait que des eaux chaudes puissent atteindre ce glacier t�moigne que l'Est Antarctique est potentiellement plus vuln�rable � l'impact du r�chauffement global qu'on ne le pensait jusqu'alors��.Le glacier Totten ne va pas fondre du jour au lendemain et provoquer une mont�e brutale du niveau des mers, a dit le chercheur. Mais il a soulign� que cette d�couverte �tait importante pour mieux cerner l'impact du changement de la temp�rature de l'oc�an sur la couverture glaciaire.Le rythme de la fonte des glaciers dans la zone de l'Antarctique la plus expos�e � ce ph�nom�ne a tripl� au cours de la derni�re d�cennie, selon une �tude publi�e le mois dernier et portant sur les 21 derni�res ann�es.
Ouest-France��
