TITRE: LEAD 5-Crash d'un chasseur en Espagne: 10 morts dont 8 Fran�ais- 26 janvier 2015 - Challenges.fr
DATE: 26-01-2015
URL: http://www.challenges.fr/monde/20150126.REU7615/lead-1-un-avion-de-chasse-grec-s-ecrase-en-espagne-dix-morts.html
PRINCIPAL: 0
TEXT:
Challenges �>� Monde �>�LEAD 5-Crash d'un chasseur en Espagne: 10 morts dont 8 Fran�ais
LEAD 5-Crash d'un chasseur en Espagne: 10 morts dont 8 Fran�ais
Publi� le� 26-01-2015 � 20h32
Mis � jour � 23h36
A+ A-
(Avec communiqu� de l'Elys�e)
MADRID, 26 janvier ( Reuters ) - Huit Fran�ais et les deux pilotes grecs d'un chasseur F-16 ont �t� tu�s lorsque l'appareil qui participait � des manoeuvres de l'Otan s'est �cras� lundi au d�collage � Albacete, en Espagne , ont annonc� les autorit�s espagnoles et fran�aises.
"Il semble que deux des personnes qui sont mortes sont de nationalit� grecque et que les huit autres sont fran�ais", a d�clar� � la t�l�vision le pr�sident du gouvernement espagnol Mariano Rajoy qui a �t� tenu inform� de la situation par son ministre de la D�fense.
Le minist�re fran�ais de la D�fense a confirm� dans un communiqu� publi� en milieu de soir�e la mort de "plusieurs Fran�ais".
"Selon les premiers �l�ments fournis par les autorit�s espagnoles, nous d�plorons la mort de huit Fran�ais", dit le minist�re.
Le chasseur F-16 s'est �cras� � 15h20 (14h20 GMT), peu apr�s avoir d�coll� d'un centre d'entra�nement � Albacete, � 260 km environ au sud-est de Madrid .
Il est tomb� sur un des parkings de la base "o� �taient stationn�s des avions de chasse de plusieurs nationalit�s, dont deux Alpha Jet, deux Mirage 2000D et deux Rafale fran�ais", a indiqu� le minist�re fran�ais de la D�fense.
"Un bless� est en situation d'extr�me urgence. Cinq autres sont en r�animation dont deux plac�s en coma artificiel", ajoute le communiqu�, pr�cisant que "trois autres bless�s l�gers sont en sortie d'h�pital".
Le pr�sident Fran�ois Hollande a exprim� dans un communiqu� sa "tr�s grande �motion" et "son profond respect pour l'engagement de ces personnels, officiers et sous-officiers, qui se pr�paraient aux missions op�rationnelles de l'arm�e de l'air".
Le ministre de la D�fense Jean-Yves Le Drian se rendra mardi apr�s-midi � Albacete, pr�cise son communiqu�.
Le secr�taire g�n�ral de l'Alliance atlantique Jens Stoltenberg a fait part dans un communiqu� de sa "profonde tristesse". "C'est une trag�die qui touche toute la famille de l'Otan", a-t-il dit.   (Inmaculada Sanz et Emma Pinedo; Eric Faye, G�rard Bon et Tangi Sala�n pour le service fran�ais)
Partager
