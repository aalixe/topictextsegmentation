TITRE: Syrie : les kurdes ont repris Koban� - leJDD.fr
DATE: 26-01-2015
URL: http://www.lejdd.fr/International/Moyen-Orient/Syrie-les-kurdes-ont-repris-Kobane-714880
PRINCIPAL: 1224689
TEXT:
Tweet
Syrie : les kurdes ont repris Koban�
D�faite importante pour l'Etat islamique en Syrie. Les djihadistes ont �t� chass� lundi de la ville de Koban� par les forces kurdes apr�s ds mois de combats.
Les forces kurdes ont chass� le groupe Etat islamique (EI) de la ville syrienne de Koban�, une d�faite cuisante pour les djihadistes apr�s plus de quatre mois de combats, rapporte lundi une organisation non gouvernementale.�"La milice des YPG (Unit�s de protection du peuple kurde) a chass� l'EI de Koban� et contr�le quasi-totalement la ville", a indiqu� lundi l'Observatoire syrien des droits de l'Homme (OSDH), pr�cisant que les djihadistes se sont repli�s dans les environs de Koban�.
"Certains djihadistes combattent encore � l'extr�mit�-est de Koban�, notamment � la p�riph�rie du quartier Maqtala", pr�cise l'ONG.�Il s'agit de la plus importante d�faite de l'EI en Syrie depuis l'apparition de ce groupe dans le conflit en 2013 et, selon les analystes, cet �chec porte un coup d'arr�t � son expansion territoriale.
1.600 morts
L'EI a perdu plus de 1.000 djihadistes depuis le d�but de son offensive contre Koban� le 16 septembre.�Les combats, qui ont fait au total plus de 1.600 morts, et la d�termination de l'EI � conqu�rir cette ville frontali�re de la Turquie ont transform� Koban� en un symbole de la lutte contre ce groupe extr�miste qui contr�le de larges territoires en Syrie et en Irak.
Les forces kurdes, au d�part sous-�quip�s, ont b�n�fici� de l'appui crucial des frappes a�riennes men�es par une coalition internationale dirig�e par Washington depuis le 23 septembre.
Vivien Vergnaud (avec AFP) - leJDD.fr
lundi 26 janvier 2015
La ville de Koban� sous les bombardements fin d�cembre. (Reuters)
Et aussi
