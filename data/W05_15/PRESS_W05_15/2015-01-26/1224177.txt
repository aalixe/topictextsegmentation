TITRE: Programme TV - "L'Emprise", sur TF1 - Odile Vuillemin : "Ce film m�a donn� une force inou�e" � metronews
DATE: 26-01-2015
URL: http://www.metronews.fr/culture/odile-vuillemin-dans-l-emprise-ce-film-m-a-donne-une-force-inouie/moay!e1AQtiB22bm3/
PRINCIPAL: 1224175
TEXT:
PUBLICIT�
Karine, 50 ans, infirmi�re. Comment avez-vous �t� amen�e�� jouer ce r�le ?�
Odile Vuillemin : On m�a propos� ce sc�nario, tr�s joliment �crit. C�est une histoire bouleversante, un r�le bouleversant... J�ai pens� que je ne pourrais jamais faire �a et puis je me suis dit que, si on me l�avait propos�, c�est que je devais en �tre capable. Mais je ne pouvais pas le faire � moiti�. Le sujet est trop grave pour �tre trait� � la l�g�re.�
Elodie, 26 ans, conseill�re en parfumerie. Quelle a �t�votre pr�paration�?�
O. V. : J�ai rencontr� Alexandra Lange pour la sentir physiquement, pour l�interpr�ter au plus juste. J�ai lu son livre [Acquitt�e, ndlr] ainsi que des livres sur la psychologie. J�ai fait aussi un gros travail de pr�paration physique pour apprendre � donner des coups et � en recevoir. Apr�s, c�est l�instinct qui parle...
EN SAVOIR +
>> "L'Emprise", l'histoire d'Alexandra Lange, femme battue acquitt�e apr�s le meurtre de son mari
Philippe, 50 ans, conseiller client�le. Comment s�est�pass�e la rencontre ?�
O. V. : C�est d�licat de parler de �a. Elle avait le trac, j�avais le trac. Mais c�est notre jardin secret.
"Pour me d�tendre, j'avais besoin de pleurer"
Karine. Le tournage a-t-il �t� difficile ?�
O. V. : Pour me d�tendre et �vacuer, j�avais besoin de pleurer, et Fred [Testot] avait, lui, besoin de d�lirer. L��tat de concentration est tr�s difficile � atteindre et je pr�f�rais rester dans cette concentration.�
Fran�oise, 57 ans, demandeuse d�emploi. Et avec les�quatre enfants ? �
O. V. : On leur a expliqu� que c��tait du maquillage, j�ai essay� de les mettre en confiance. Certains �taient dans le jeu naturellement, pour d�autres c��tait d�licat. Celle qui jouait ma fille a pos� beaucoup de questions.�
Jean-Gabriel, 54 ans, responsable d�un service courrier. Comment avez-vous tourn� la page�?�
O. V. : Je pensais avoir du mal � en sortir car j��tais boulevers�e pendant le tournage. Pourtant, d�s le lendemain, je p�tais le feu. Jouer m�a donn� une force inou�e, cela a un effet cathartique et transcendant. Ce film m�a donn� quatre fois plus d��nergie.�
"J'esp�re que ce film aidera � lib�rer la parole"
Karine : Alexandra Lange a-t-elle vu le film ?�
O. V. : Oui et elle a �t� tr�s touch�e, tr�s �mue et tr�s fi�re du r�sultat. Elle a dit que si ce film pouvait n�aider qu�une seule personne, il aurait servi � quelque chose.
Karine. Et vous, comment�l�avez-vous regard� ?�
O. V. : Stress�e et �mue. J�ai �t� bluff�e car j�ai vu encore plus que ce que je croyais avoir donn�.
Elodie. Pensez-vous que le film�aura des retomb�es�?�
O. V. : J�esp�re qu�il aidera � lib�rer la parole. Il a �t� r�alis� avec l�espoir d�apporter une pierre � l��difice.
Jean-Gabriel. Comment�expliquez-vous ce silence�des femmes battues ?�
O. V. : La femme est amoureuse et il y a cette relation toxique qui la d�poss�de de sa personnalit�. Il y a une d�valorisation permanente et, une fois qu�on a perdu confiance en soi, on ne peut plus partir. C�est tellement incroyable que le cerveau tombe dans le d�ni.
L'Emprise, lundi soir � 20 h 55 sur TF1.�
Christophe Joly
