TITRE: La France est dos au mur, dit Macron en pr�sentant sa loi- 26 janvier 2015 - Challenges.fr
DATE: 26-01-2015
URL: http://www.challenges.fr/economie/20150126.REU7589/la-france-est-dos-au-mur-dit-macron-en-presentant-sa-loi.html
PRINCIPAL: 1225992
TEXT:
Challenges �>� Economie �>�La France est dos au mur, dit Macron en pr�sentant sa loi
La France est dos au mur, dit Macron en pr�sentant sa loi
Publi� le� 26-01-2015 � 17h33
* "Nous transformons l'or en plomb", dit Macron
* Evolution des positions chez les centristes et les �cologistes
PARIS, 26 janvier ( Reuters ) - La France doit se moderniser pour arr�ter de "transformer l'or en plomb", a d�clar� lundi Emmanuel Macron en pr�sentant aux d�put�s fran�ais son projet de loi pour la croissance et l'activit� qui divise la majorit� comme l'opposition.
"Notre pays est dos au mur et le statu quo n'est plus une option", a dit le ministre de l' Economie devant un h�micycle quasiment d�sert, fait �tonnant pour un texte qui suscite tant de passions. "Nous avons besoin d'un nouveau souffle."
Apr�s examen par une commission sp�ciale, son projet de loi comprend d�sormais quelque 200 articles, contre 106 initialement, sur lesquels ont �t� d�pos�s 3.000 amendements.
Emmanuel Macron a fait r�f�rence aux attentats djihadistes qui ont frapp� la France d�but janvier pour estimer que les dysfonctionnements et les injustices pouvaient "engendrer les drames humains les plus terribles".
"C'est pourquoi cette loi ne fait pas myst�re des orientations qu'elle porte", a-t-il poursuivi. "Plus de vitalit�, plus de justice et de transparence, des droits aux Fran�ais et en particulier aux plus jeunes".
Mais il n'a pas cach� qu'il s'agissait aussi de montrer � l'Union europ�enne, � laquelle la France demande un nouveau d�lai pour r�duire ses d�ficits, que "nous sommes capables de bouger, que nous ne sommes prisonniers d'aucun dogme".
"Comment leur demander plus, si nous ne faisons pas nos r�formes nous-m�mes ?", a-t-il demand�.
Emmanuel Macron a m�me revendiqu� le c�t� "fourre-tout" de sa loi, une des critiques de l'opposition, en expliquant que, les blocages �tant "partout", il fallait toucher de nombreux domaines, y compris dans ce qui est "trivial".
Le texte pr�voit ainsi une extension du travail dominical , laissant aux maires la possibilit� de fixer le nombre de dimanches travaill�s "entre 0 et 12" mais aussi une lib�ralisation du transport par autocar.
LE GOUVERNEMENT PEUT �TRE CONFIANT
La commission sp�ciale a modifi� les dispositions relatives � l'installation des professions juridiques r�glement�es. Elle a ainsi ent�rin� la libert� d'installation "contr�l�e" des notaires, huissiers et commissaires-priseurs, et a fix� une limite d'�ge (70 ans) pour ces professions.
Elle a supprim� le dispositif qui permet aux clercs asserment�s de recevoir certains actes � la place du notaire et abrog� la disposition du texte qui proposait la cr�ation d'une statut d'avocat en entreprise pour trouver un compromis.
Le texte comporte �galement des mesures acc�l�rant la justice prud'homale, propose la cr�ation de bourses r�gionales dans les capitales des treize nouvelles r�gions et introduit la notion de "secret des affaires" dans le code du commerce.
Enfin, il autorise le gouvernement � l�gif�rer par ordonnances pour permettre la mise en place de la liaison ferroviaire expresse entre Paris et l'a�roport Roissy-Charles de Gaulle et pr�voit d'harmoniser les d�lais de recours sur les projets d'installation d'�oliennes.
Le gouvernement devrait faire voter son texte sans avoir � recourir � des mesures contraignantes, comme le vote bloqu�.
Si le Front de gauche dans son ensemble, une partie des "frondeurs" du PS et l'UMP dans sa majorit� restent hostiles au texte, les positions de l'UDI (centriste) et des �cologistes semblent avoir �volu� lors de l'examen en commission.
Le groupe UDI n'exclut plus un vote positif si certains de ses amendements sont retenus en s�ance publique et certains d�put�s UMP devraient les rejoindre.
Chez les �cologistes, il n'est plus exclu qu'une partie d'entre eux s'abstiennent, malgr� l'opposition frontale de l'ancienne ministre C�cile Duflot .
"Les choses ont un peu �volu�, plut�t dans le bon sens", estime Fran�ois de Rugy, le co-pr�sident du groupe.
Le gouvernement peut compter sur le gros des troupes socialistes et sur les radicaux de gauche.
L'Assembl�e se prononcera le 10 f�vrier par un vote solennel sur l'ensemble du projet de loi que le S�nat devrait examiner � son tour vers la fin avril.   (Emile Picy, �dit� par Yves Clarisse)
Partager
