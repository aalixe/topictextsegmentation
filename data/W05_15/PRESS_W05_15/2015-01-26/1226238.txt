TITRE: Mort de Demis Roussos : 10 choses que vous ne saviez pas sur le chanteur disparu
DATE: 26-01-2015
URL: http://www.purepeople.com/article/mort-de-demis-roussos-10-choses-que-vous-ne-saviez-pas-sur-le-chanteur-disparu_a154137/1
PRINCIPAL: 1226226
TEXT:
Mort de Demis Roussos : 10 choses que vous ne saviez pas sur le chanteur disparu
1/6
News publi�e  le Lundi 26 Janvier 2015 � 20:08
Lancer le diaporama
Demis Roussos le 20 novembre 1978 sur le plateau d'une �mission de t�l�. �Bestimage        Dans cette photo : Demis Roussos
Recevez tout l'actu des stars
OU
Suivez l'actualit� de Purepeople.com sur Facebook
Star de la vari�t� dans les ann�es 70 et 80, Demis Roussos nous a quitt�s dans la nuit de samedi � dimanche 25 janvier 2015, � l'�ge de 68 ans. Ce Grec parmi les plus connus au monde aura men� une vie et une carri�re pour le moins atypiques. Fils d'�migr�s grecs, ce chanteur au physique imposant, aux poils l�gendaires et � la voix puissante se fait conna�tre gr�ce � un groupe de rock. La cons�cration viendra ensuite avec des chansons d'amour en fran�ais comme Quand je t'aime ou Mourir aupr�s de mon amour qui feront vibrer les habitants de l'Hexagone et du monde entier. D�couvrez dix facettes m�connues de la vie de cet artiste prolifique et d�j� tant regrett�.
1. Demis Roussos n'est pas n� en Gr�ce...
Art�mios Vento�ris Ro�sos dit Demis Roussos est n� et a grandi � Alexandrie, en Egypte, o� vivaient et travaillaient ses parents et ses grands-parents d'origine grecque.
2. Il a appris la musique... � l'�glise
Enfant, Demis Roussos a �t� enfant de coeur. Sa famille �tait tr�s impliqu�e dans la communaut� orthodoxe de la 2e plus grande ville d'Egypte, aussi, la future star a-t-elle �t� pendant cinq ans soliste du Choeur de l'Eglise orthodoxe d'Alexandrie.
3. Une courte et difficile adolescence en Gr�ce
En 1961, Demis et sa famille quittent l'�gypte pour la Gr�ce, ruin�s apr�s la crise du Canal de Suez. � Ath�nes, il suit des cours dans l'une des meilleures �coles de la capitale, sa m�re d�sirant le voir travailler dans l'h�tellerie et parler plusieurs langues. Mais Demis ne pense qu'� la musique et joue le soir dans des clubs et des caf�s, au sein de diff�rents groupes qu'il cr�e avec des connaissances, qui conna�tront aussi, pour certaines, de brillantes carri�res. Ils reprennent les standards du rock'n'roll, alors naissant, et Demis va alors tout investir dans cette passion d�vorante.
4. Un chanteur peu s�r de lui
Demis Roussos ne se destinait pas � une carri�re de chanteur mais � une carri�re d'instrumentiste. Il d�marre en apprenant la trompette jazz puis, dans les groupes qu'il forme � Ath�nes, il est guitariste et bassiste et ne revendique absolument pas un r�le de chanteur. Ce n'est qu'apr�s avoir �t� oblig� de remplacer, au pied lev�, le chanteur de son premier groupe, The Idols, qu'il commencera � s'affirmer comme interpr�te vocal.
5. Une installation en France totalement impr�vue
Demis Roussos quitte Ath�nes en mars 1968 avec ses partenaires musicaux Vangelis Papathanassiou (qui deviendra un grand compositeur de musique new age et �lectronique et de musiques de films oscaris�) et Lucas Sideras (batteur qui se sp�cialisera dans le rock et sera producteur). Si les trois gar�ons se retrouvent � Paris et y connaissent la gloire, ce n'est, � l'origine, pas du tout leur destination.
Les musiciens voulaient en fait se rendre � Londres, qui �tait alors consid�r�e comme la capitale des nouvelles musiques. Mais arriv�s � Douvres pour embarquer vers le Royaume-Uni, les trois amis sont refus�s par la douane britannique et rebroussent chemin vers Paris. Bloqu�s dans la capitale par les �v�nements de Mai-68 et quasiment � court d'argent, ils parviennent � signer un contrat avec la maison Phonogram pour leur groupe Aphrodite's Child. Ils composent et enregistrent alors en quelques semaines leur premier album de rock progressif, End of The World.
6. Un succ�s explosif qui d�sint�gre son groupe
� sa sortie, le premier album des Aphrodite's Child fait un tabac et se classe num�ro un des ventes. Inconnu jusqu'alors, le groupe se retrouve propuls� en premi�re partie de Sylvie Vartan � l'Olympia puis part en tourn�e � travers l'Europe. Le groupe multiplie les passages en t�l�vision, plus de 200 en l'espace de deux ans. Un deuxi�me album sort d�s 1969 (It's Five O'Clock) mais les tensions commencent � poindre. Vangelis, le compositeur, souhaite en effet �voluer vers un registre plus pointu et moins pop.
Le groupe passe trois mois en studio pour enregistrer l'album mythique 666, inspir� par le texte sacr� de l'Apocalypse de Saint-Jean, qui confronte les genre de l'�poque (rock progressif, pop, �lectro, hard rock). � l'issue de l'enregistrement, en 1970, le groupe se s�pare et Vangelis termine seul le double album, qui sort en 1971, et est unanimement applaudi. Demis Roussos entame alors une carri�re en solo, et retravaillera plus tard individuellement avec les membres des Aphrodite's Child.
7. Un globe-trotteur accro au travail
M�me s'il ne composait plus de nouveaux titres � la fin de sa carri�re, � l'exception d'un dernier album en 2009 apr�s plusieurs ann�es de silence, Demis Roussos n'a jamais cess� de monter sur sc�ne. Les ann�es pr�c�dant sa mort, il donnait encore pr�s de 80 concerts par an partout dans le monde. Mais le rythme �tait encore plus effr�n� au sommet de sa carri�re. En 1974, il confiait avoir, en trois ans, donn� 380 concerts dans 18 pays, enregistr� 40 chansons et particip� � 300 �missions de t�l� et radio.
8. Une unique apparition au cin�ma
Chanteur star, Demis Roussos n'a jamais �t� tent� par une carri�re au cin�ma. On ne l'a aper�u qu'une seule fois dans les salles obscures, pour un petit r�le en forme de clin d'oeil dans la com�die Tu honoreras ta m�re et ta m�re, de Brigitte Ro�an, sorti en 2013 et tourn� pendant l'�t� 2011. Il y jouait le pope orthodoxe d'une famille en crise men�e par Nicole Garcia et compos�e de Eric Caravaca, Gaspard Ulliel , Elisa Tovati ...
9. Un amateur de bonne ch�re
Demis Roussos a toujours eu une carrure massive. Son physique imposant (il a pes� jusqu'� 150 kilos) et sa barbe fournie reconnaissable entre mille ont �t� entretenus par son rythme de vie effr�n� et par un amour pour les bons produits, tant et si bien qu'� la fin de sa vie, il fabriquait sa propre huile d'olive gr�ce aux arbres plant�s sur l'�le en face d'Ath�nes qu'il avait achet�e pour y couler des jours tranquilles.
10. Une star des charts
Depuis 1968, Demis Roussos a �coul� plus de 60 millions d'albums et donn� des milliers de concerts. C'est colossal mais toutefois moins que son amie grecque Nana Mouskouri , tr�s affect�e par son d�c�s, qui a vendu, elle, plus de 300 millions de disques � travers la plan�te.
Alvin Haddad�ne
