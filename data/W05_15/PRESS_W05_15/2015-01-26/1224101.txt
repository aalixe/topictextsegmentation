TITRE: A�roport de Roissy : deux policiers en garde � vue pour trafic de drogue | Planet
DATE: 26-01-2015
URL: http://www.planet.fr/societe-aeroport-de-roissy-deux-policiers-en-garde-a-vue-pour-trafic-de-drogue.786450.29336.html
PRINCIPAL: 1224094
TEXT:
Envoyer par courriel
Participer (2 commentaires)
Deux fonctionnaires de la police aux fronti�res ont �t� interpell�s et plac�s en garde vue dimanche. Ils sont soup�onn�s d�avoir laiss� passer des bagages contenant de la drogue � l�a�roport de Roissy-Charles de Gaulle en r�gion parisienne.
Interpellation de policiers � l�a�roport de Roissy-Charles de Gaulle. Dimanche, deux fonctionnaires de la police aux fronti�res en poste dans cet a�roport de la r�gion parisienne ont �t� arr�t�s et plac�s en garde � vue dans le cadre d�une enqu�te sur un trafic de drogue, a r�v�le France 3 . Selon une source proche de l�affaire, tous les deux sont soup�onn�s d�avoir pris part � ce trafic en facilitant l�arriv�e de drogue sur le territoire fran�ais. Plus pr�cis�ment, les deux hommes auraient ferm� les yeux sur le passage de deux valises en provenance de la R�publique dominicaine et contenant de la coca�ne.
Ils aidaient les passeurs � franchir la douane
Confi�e � l�Office central pour la r�pression du trafic illicite de stup�fiants (Octris), l�enqu�te aurait par ailleurs r�v�l� que les deux hommes n�h�sitaient pas � aller chercher les personnes transportant les valises pleines de drogue "sur le tarmac de l�a�roport, en voiture s�rigraphi�e" et "en uniforme" pour leur faire "franchir la douane".
Publicit�
Un vaste coup de filet
Selon les informations de la Trois, l�arrestation aurait eu lieu alors que les deux policiers �taient en possession des deux valises, lesquelles contenaient chacune pr�s de 20 kilos de poudre blanche. En tout, une dizaine de personnes auraient �t� interpell�es et plac�es en garde en vue dans le cadre de cette enqu�te. L�une d�entre elles aurait �t� bless�e par balle, � Saint-Cloud, rapport� �galement France 3, sans toutefois pr�ciser la gravit� de ses blessures.
�
Publi� le Lundi 26 Janvier 2015 : 09h33
Ailleurs sur le web
