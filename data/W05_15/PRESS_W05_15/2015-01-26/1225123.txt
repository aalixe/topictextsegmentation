TITRE: Obama � Ryad mais pas � Paris? �C�est diff�rent�, r�pond la Maison Blanche | JDM
DATE: 26-01-2015
URL: http://www.journaldemontreal.com/2015/01/26/obama-a-ryad-mais-pas-a-paris-cest-different-repond-la-maison-blanche
PRINCIPAL: 1225120
TEXT:
Obama � Ryad mais pas � Paris? �C�est diff�rent�, r�pond la Maison Blanche
Autres
REUTERS Barack Obama avec le premier ministre indien.
REUTERS Barack Obama avec le premier ministre indien.
AFP
Lundi, 26 janvier 2015 08:00
MISE � JOUR
Lundi, 26 janvier 2015 08:20
Exclusif aux membres
Coup d'oeil sur cet article
NEW DELHI - �C�est diff�rent�: la Maison Blanche a rejet� lundi les critiques de ceux qui s�interrogent sur le d�placement annonc� du pr�sident Barack Obama � Ryad alors qu�il ne s��tait pas d�plac� � Paris lors de la manifestation contre le terrorisme.
�Les circonstances sont diff�rentes�, a soulign� Ben Rhodes, conseiller de M. Obama, lors d�un point de presse � New Delhi o� le pr�sident am�ricain effectue une visite de trois jours.
�Il y a une p�riode durant laquelle diff�rents dirigeants peuvent se rendre en Arabie Saoudite pour exprimer leurs condol�ances (apr�s le d�c�s du roi Abdallah) et rencontrer le nouveau roi�, a-t-il expliqu�.
�Il y a donc une diff�rence m�me si cela ne change rien au fait que nous avons clairement indiqu� que nous aurions d� envoyer quelqu�un de plus haut niveau�, � la marche de Paris, a-t-il poursuivi.
L�absence de M. Obama, ou de son vice-pr�sident ou de l�un de ses ministres lors de la marche contre le terrorisme qui a rassembl� des centaines de milliers de personnes et une cinquantaine de dirigeants du monde entier le 11 janvier � Paris a provoqu� une avalanche de critiques aux �tats-Unis. Fait rare, la Maison Blanche a fait son mea culpa le lendemain.
Certains m�dias am�ricains ont ironis� ces derniers jours sur le fait que M. Obama ait d�cid� de se rendre � Ryad mais n�ait pas envisag�, � l�inverse, le d�placement � Paris dans la foul�e des attentats meurtriers qui ont frapp� la capitale fran�aise.
Interrog� sur l�objectif de cette visite � Ryad, M. Rhodes a indiqu� que M. Obama entendait d�abord �pr�senter ses condol�ances (...) et rencontrer le nouveau roi Salmane�.
�Ils �voqueront les principaux sujets sur lesquels nous collaborons tr�s �troitement avec l�Arabie Saoudite�, a-t-il ajout�, citant en particulier la campagne contre l�organisation �tat islamique �dans laquelle les Saoudiens ont particip� aux op�rations militaires� et la situation au Y�men.
�Il est tr�s probable que l�Iran soit �galement �voqu�, a-t-il ajout�.
M. Obama a d�cid� d��courter sa visite en Inde et d�annuler la visite qu�il devait effectuer mardi avec sa femme Michelle mardi au Taj Mahal, haut-lieu touristique indien, pour se rendre en Arabie Saoudite.
Les plus populaires
