TITRE: Jean-Marie Le Pen l�g�rement bless� dans l'incendie de sa maison | Atlantico.fr
DATE: 26-01-2015
URL: http://www.atlantico.fr/pepites/jean-marie-pen-legerement-blesse-dans-incendie-maison-1974063.html
PRINCIPAL: 1225292
TEXT:
Jean-Marie Le Pen l�g�rement bless� dans l'incendie de sa maison
L'origine du feu au domicile de l'ex-pr�sident du Front national reste ind�termin�e.
Au feu !
Jean-Marie Le Pen�
Cr�dit Reuters
Un incendie s'est d�clar� lundi en d�but d'apr�s-midi au domicile de Jean-Marie Le Pen, � Rueil-Malmaison, dans les Hauts-de-Seine. Selon les premiers �l�ments, r�v�l�s par BFMTV, d'importants d�g�ts mat�riels sont � noter. Un feu de chemin�e pourrait �tre � l'origine du sinistre.
L'ancien leader du Front national a lui �t� pris en charge par les pompiers, en raison de blessures au visage. Il pr�senterait un h�matome sous l'oeil, indique Le Parisien . Il pourrait �tre hospitalis� pour plus de s�curit�. "La maison a br�l�, mon p�re �tait � l'int�rieur. Il est bless� au visage, mais il n'y a rien de grave, rien d'inqui�tant", a assur� sa fille, Marine Le Pen.
Jean-Marie Le Pen se serait r�fugi� sur la terrasse pour �chapper aux flammes et serait tomb�, explique une autre source, cit�e par Le Figaro .
La roue tourne RT @pierrezidi : @BFMTV premi�res images de l'incendie de la maison de Jean marie lepen il y a 20 min pic.twitter.com/0GOAPdChjT "
� Dorian (@DorianHNZ) 26 Janvier 2015
�
L'ancien pr�sident frontiste r�side � Rueil-Malmaison depuis le d�but des ann�es 90 mais poss�de toujours la propri�t� familiale de Montretout, � Saint-Cloud. Le si�ge du FN se situe lui dans une autre commune voisine � Nanterre.
Lire ou relire plus tard
Pour classer cet article et le retrouver dans votre compte :
Besoin de vous concentrer
Saisissez votre pseudonyme ou votre email pour Atlantico.
Mot de passe *
Lire ou relire plus tard
Pour classer cet article et le retrouver dans votre compte :
Besoin de vous concentrer
Saisissez votre pseudonyme ou votre email pour Atlantico.
Mot de passe *
Commentaires
Nos articles sont ouverts aux commentaires sur une p�riode de 7 jours.
Face � certains abus et d�rives, nous vous rappelons que cet espace a vocation � partager vos avis sur nos contenus et � d�battre mais en aucun cas � prof�rer des propos calomnieux, violents ou injurieux. Nous vous rappelons �galement que nous mod�rons ces commentaires et que nous pouvons �tre amen�s � bloquer les comptes qui contreviendraient de fa�on r�currente � nos conditions d'utilisation.
Par
- 26/01/2015 - 16:03 - Signaler un abus Bon r�tablissement
� Jean-Marie Le Pen, un phare dans la nuit!
Par
- 26/01/2015 - 16:38 - Signaler un abus Encore un complot!
bjorn borg: en effet un phare qui marche � la g�g�ne !
Pour commenter :
Depuis son lancement Atlantico avait fait le choix de laisser ouvert � tous la possibilit� de commenter ses articles avec un syst�me de mod�ration a posteriori. Sous couvert d'anonymat, une minorit� d'internautes a trop souvent d�tourn� l�esprit constructif et respectueux de cet espace d��changes. Suite aux nombreuses remarques de nos lecteurs, nous avons d�cid� de r�server les commentaires � notre communaut� d�abonn�s.
Cambad�lis (PS) croit � un "plafond de verre" qui arr�tera le FN
09h40
La Salton Sea, bombe � retardement �cologique dans le d�sert de Californie
09h40
�ducation : vers une meilleure exploitation des "intelligences multiples"
09h40
Y�men: les forces hostiles au pr�sident s'emparent d'une base proche d'Aden
09h40
Crash A320: l'hypoth�se terroriste "pas privil�gi�e" selon Cazeneuve
09h40
Crash A320: la bo�te noire "est arriv�e � Paris"
09h36
Herm�s offre un dividende exceptionnel apr�s un solide exercice
09h36
Le fonds 3G Capital n�gocie le rachat de Kraft Foods
09h36
ACCOR perd 3,8% apr�s les cessions d'EURAZEO et Colony
09h36
Kenya : un restaurant chinois ferm� apr�s avoir interdit l�acc�s aux noirs le soir
09h35
Le climat des affaires s'am�liore en mars
09h35
Le directeur d'�cole is�rois reconna�t avoir viol� ses �l�ves
09h33
Crash A320: la piste terroriste "pas privil�gi�e" selon Royal
09h33
H�catombe de thons dans un populaire aquarium de Tokyo
09h33
Y�men: le pr�sident Hadi exhorte l'ONU � faire stopper l'avanc�e des Houthis sur Aden
Toutes les d�p�ches
Je quitte la S�cu Partie 1 : Un parcours du combattant
Laurent C
Je quitte la S�cu  Partie 2 : Pourquoi c'est possible
Laurent C
Comment r�v�ler votre charisme Partie 2 : Votre corps parle pour vous : Apprenez � le ma�triser en toutes circonstances
Chilina Hills
Je cr�e mon entreprise � l'�tranger Les cl�s du succ�s
Ingrid Labuzan
Devenir un pro de la n�gociation Partie 2 : Prendre le contr�le �motionnel de la n�gociation
Alexis Kyprianou
La France handicap�e du calcul Vaincre l'innum�risme pour sortir du ch�mage
Michel Vigier
L'id�e qui tue ! Comment vendre une id�e r�volutionnaire
Nicolas Bordas
Culturellement incorrect : De l'ill�gitimit� de l'intervention publique en mati�re culturelle
Bertrand Allamel
Comment r�v�ler votre charisme Partie 1 : Ce que vous gagnerez en le cultivant, et pas seulement si vous recherchez la c�l�brit�
Chilina Hills
Sortir l'Europe de la crise : le mod�le japonais
Nicolas Goetzmann
