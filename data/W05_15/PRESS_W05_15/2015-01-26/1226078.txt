TITRE: Pogba est plus cher que Messi et Cristiano Ronaldo - Goal.com
DATE: 26-01-2015
URL: http://www.goal.com/fr/news/33/transferts/2015/01/26/8328642/pogba-est-plus-cher-que-messi-et-cristiano-ronaldo
PRINCIPAL: 1226074
TEXT:
Fermeture du march� des transferts
Pogba est plus cher que Messi et Cristiano Ronaldo
Getty Images
0
26 janv. 2015 17:57:00
Selon son agent, seulement huit clubs peuvent s'offrir Paul Pogba, le milieu fran�ais de la Juventus.
Paul Pogba �est�courtis� par les plus grands clubs europ�ens. Tous souhaitent s'attacher les services de l'international fran�ais. Du coup, il pourrait bien quitter l'Italie � la fin de la saison. Pas mal�de clubs anglais sont fortement int�ress�s. Selon la presse anglaise, Jose Mourinho aurait pr�par� une�offre de plus de 50 millions d'euros pour s'offrir ses services. Le club londonien devrait�m�me proposer, en plus de l'argent,�Ramires et��Andre Schurrle en��change.
Paul Pogba est �galement dans le radar de Man United (qui aurait�transmis une offre d'environ 80 millions d'euros)�et de City. Selon les rumeurs en Espagne, le Real Madrid�se dit pr�t � mettre 100 millions d'euros pour s'attacher les services du�milieu de terrain de la� Juventus , auteur de 5 buts en 16 matches de Serie A cette saison,�
Sauf que Mino Raiola, son agent, a affirm� que les clubs � pouvoir l'acheter ne sont pas nombreux.�� Pogba est le joueur le plus cher du monde, il co�te plus cher que Cristiano Ronaldo et Lionel Messi. Il y a seulement huit clubs qui peuvent se le payer. Il a un contrat de cinq ans � la Juve, mais nous verrons ce qui se passera en juin et en juillet � , a confi� l'agent � Sky Italia.�
Dimanche, tenue en �chec par le Chievo V�rone, la Juventus s�en est remise � son g�nie fran�ais Paul Pogba pour s�imposer et consolider son rang de leader.�
Relatifs
