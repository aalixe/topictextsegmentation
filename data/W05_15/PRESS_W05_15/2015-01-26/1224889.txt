TITRE: L'incendie cause d'importants d�g�ts
DATE: 26-01-2015
URL: http://www.leparisien.fr/espace-premium/hauts-de-seine-92/l-incendie-cause-d-importants-degats-26-01-2015-4478501.php
PRINCIPAL: 1224887
TEXT:
r�agir
Un incendie s'est d�clar� samedi peu apr�s 15�heures dans un logement situ� au dernier �tage d'un petit immeuble HLM de Rueil-Malmaison, 36,�rue Eug�ne-Labiche.
Le feu a pris en l'absence du locataire et s'est rapidement propag� au toit du b�timent, construit sur deux niveaux de trois et quatre �tages. Les secours arriv�s sur place ont demand� � la quarantaine d'habitants pr�sents d'�vacuer l'immeuble pendant l'intervention. Personne n'a souffert des �manations de fum�e et aucun bless� n'est � d�plorer. En revanche, les d�g�ts sont importants car les pompiers ont d� d�molir une partie du toit, qui mena�ait de s'effondrer. Et au moins trois familles de cette r�sidence sociale, qui n'ont pas pu regagner leur appartement, doivent �tre relog�es par la mairie.
