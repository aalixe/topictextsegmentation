TITRE: Mort de Demis Roussos: Cinq choses que vous ne saviez (probablement) pas sur la star - 20minutes.fr
DATE: 26-01-2015
URL: http://www.20minutes.fr/culture/musique/1525719-20150126-cinq-choses-saviez-probablement-demis-roussos
PRINCIPAL: 0
TEXT:
D�c�s
Demis Roussos, c�est une voix, une silhouette, plusieurs looks et quelques tubes. Au-del� de ce que l�on sait du plus c�l�bre chanteur grec des derni�res d�cennies , la biographie de l�artiste d�c�d� dans la nuit de samedi � dimanche, � 68 ans, rec�le quelques informations insolites.
Un pr�nom commun
Art�mios Vento�ris Ro�sos de son nom complet doit son pr�nom � son grand-p�re paternel, comme le veut la tradition grecque. Demis est l�un des diminutifs d�Art�mios particuli�rement courant.
Culture grecque, mais pas que
Demis Roussos n�a pas grandi en Gr�ce mais en Egypte, jusqu�� l��ge de 15 ans. Ses parents �taient install�s � Alexandrie o� ils avaient une soci�t� de construction. �Le petit Demis a grandi au sein d�une communaut� grecque orthodoxe mais dans un pays de culture musulmane.
Avant �Aphrodite�
Demis Roussos a connu une adolescence musicale avant de fonder Aphrodite�s Child , le groupe qui lui fit acc�der au succ�s plan�taire. Encore enfant, il chantait dans le ch�ur de l�Eglise orthodoxe d�Alexandrie. Apr�s son arriv�e en Gr�ce, il fonde un petit groupe pop, The Idols.
Le souffle de Demis
On l�a vu au piano, beaucoup, surtout dans les ann�es 1980, et aussi un peu � la guitare. Mais son premier instrument fut la trompette . Demis Roussos racontait�souvent que la pratique de cet instrument lui avait permis d�acqu�rir un bon souffle.
Les �v�nements originels
Au d�but de l�ann�e 1968, Demis Roussos et l'un de ses amis veulent rejoindre Vangelis � Londres pour se lancer dans la musique. Refoul�s par la douane britannique faute de visa, ils se retrouvent � Paris. Les �v�nements de mai 1968 retardent les formalit�s administratives. A cours d�argent, Demis Roussos se r�sout � proposer sa musique � un label fran�ais alors qu�il r�vait de signer en Angleterre. Phonogram accepte tout de suite. Le groupe Aphrodite�s Child est cr��. Leur premier tube, Rain and Tears, est enregistr� puis press� � la veille de la fermeture de l�usine � cause de la gr�ve g�n�rale.
�
