TITRE: Les Kurdes ont chass� les djihadistes de l'EI de Koban� - 20minutes.fr
DATE: 26-01-2015
URL: http://www.20minutes.fr/monde/1525707-20150126-kurdes-chasse-djihadistes-ei-kobane
PRINCIPAL: 1225090
TEXT:
SYRIE Une d�faite cuisante pour l�Etat islamique apr�s quatre mois de combats�
Les Kurdes ont chass� les djihadistes de l'EI de Koban�
Des jihadistes � c�t� d'un drapeau du groupe Etat Islamique au sommet d'une colline surplombant la ville de Koban�, le 6 octobre 2014 en Syrie - Aris Messinis AFP
20 Minutes avec AFP
Etat islamique en Irak et au Levant
Une atmosph�re de liesse r�gnait lundi dans les r�gions kurdes syriennes apr�s l'�viction du groupe djihadiste de l'Etat islamique (EI) de la ville de Koban� , sa d�faite la plus cuisante en Syrie.
Cet �chec intervient le jour m�me o� un responsable militaire en Irak annon�ait que la province de Diyala, dans l'est du pays, �tait aussi lib�r�e du groupe extr�miste.
300 personnes rassembl�es � Paris
�Koban� lib�r�, f�licitations � l'Humanit�, au Kurdistan et au peuple de Koban�, a tweet� dans l'apr�s-midi Polat Can, un porte-parole des YPG (Unit�s de protection du peuple kurde) , la milice qui d�fend la ville. Plus t�t, l'Observatoire syrien des droits de l'Homme (OSDH) avait affirm� que les Kurdes contr�laient �totalement� Koban�, cette petite ville frontali�re de la Turquie devenue le symbole de la r�sistance � l'organisation EI depuis que les jihadistes y ont lanc� un vaste assaut le 16 septembre.
Dans les r�gions � majorit� kurde en Syrie, des foules sont descendues dans les rues pour c�l�brer cette victoire, certains dansant, d'autres tirant en l'air en signe de joie, rapporte l'OSDH. A Paris, quelque 300 personnes se sont rassembl�es place de la R�publique, d�ployant un immense drapeau aux couleurs kurdes sur fond de danse traditionnelle et feux d'artifice.
90% de la ville contr�l�e, selon Washington
Le d�partement d'Etat am�ricain est rest� prudent une bonne partie de la journ�e, estimant que �les forces anti-EI contr�laient approximativement 70% du territoire � Koban� et pr�s de Koban�. Mais un peu plus tard, le commandement militaire am�ricain au Moyen-Orient (Centcom) a estim� que les forces kurdes avaient repris �� peu pr�s 90% de la ville de Koban�.
�La guerre contre le groupe Etat islamique est loin d'�tre termin�e, mais son �chec � Koban� prive l'EI de l'un de ses objectifs strat�giques�, s'est f�licit� Centcom.
