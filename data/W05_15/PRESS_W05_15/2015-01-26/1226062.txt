TITRE: Quentin Tarantino d�bute le tournage de "The Hateful Eight" | GQ
DATE: 26-01-2015
URL: http://www.gqmagazine.fr/pop-culture/news/articles/the-hateful-eight-le-tournage-a-commence/23752
PRINCIPAL: 1226060
TEXT:
POP CULTURE / ACTU CULTURE - LUNDI, 26 JANVIER 2015
QUENTIN TARANTINO D�BUTE LE TOURNAGE DE "THE HATEFUL EIGHT"
PAR MARINE DELCAMBRE
Apr�s plusieurs rebondissements, le tournage de "The Hateful Height", le nouveau western de Tarantino, a bel et bien commenc�.
� � AFP
Cette fois-ci c'est la bonne. Le tournage du nouveau western de Quentin Tarantino vient de debuter � Telluride, dans le Colorado. Apr�s la fuite du sc�nario dans la presse il y a un an, le r�alisateur a finalement d�cid� de faire le film The Hateful Eight. Le pitch ? Apr�s la Guerre de S�cession, huit voyageurs se retrouvent coinc�s dans un refuge au milieu des montagnes. Alors qu'ils s'appr�tent � rejoindre Red Rock, une temp�te s'abat au-dessus du massif... Les voyageurs sont incarn�s par Kurt Russell, Jennifer Jason Leigh, Samuel L. Jackson , Walton Goggins, Bruce Dern, Demian Bichir, Tim Roth et Michael Madsen. Au casting �galement, Channing Tatum, qui sera bient�t � l'affiche de X-Men : Apocalypse .
Le film devrait sortir d�s novembre prochain aux Etats-Unis.
