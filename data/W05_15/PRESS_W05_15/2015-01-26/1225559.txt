TITRE: Le groupe Etat islamique appelle � de nouvelles attaques apr�s Charlie Hebdo
DATE: 26-01-2015
URL: http://www.bfmtv.com/international/ei-appelle-a-de-nouvelles-attaques-apres-charlie-hebdo-859650.html
PRINCIPAL: 1225556
TEXT:
Imprimer
Le groupe extr�miste appelle � de nouvelles attaques contre les pays occidentaux. "Vous n'avez encore rien vu", dit le porte-parole de l'EI dans un enregistrement audio.
Le groupe extr�miste Etat islamique (EI) a appel� lundi � mener de nouvelles attaques contre les pays occidentaux. Il salue aussi les attentats ex�cut�s par des jihadistes, notamment en France contre la r�daction de Charlie Hebdo.
�� �
"Nous appelons les musulmans en Europe et dans l'occident infid�le � attaquer les crois�s o� qu'ils soient (...) nous promettons aux bastions chr�tiens qu'ils continueront de vivre dans un �tat d'alerte, de terreur, de peur et d'ins�curit� (...) Vous n'avez encore rien vu", a affirm� Mohammad al-Adnani, porte-parole de l'EI dans un message audio diffus� sur internet.
Pour cela, il appelle � se servir de toute arme, "que ce soit une bombe artisanale, des balles, un couteau, une voiture pi�g�e ou le poing".
17 morts en France
L'homme ajoute que l'EI consid�re comme "ennemis" les musulmans qui peuvent s'en prendre aux "crois�s" et ne le feront pas. Il salue les attaques men�es par les "fr�res" jihadistes en "France, en Australie et en Belgique".
Le massacre perp�tr� dans la r�daction de Charlie Hebdo le 7 janvier, a fait 12 morts. Il a �t� men� par les fr�res Kouachi, deux jihadistes fran�ais se revendiquant de la branche d'Al-Qa�da en p�ninsule arabique (AQPA). Le lendemain, quatre morts ont �t� abattus dans un supermarch� casher � Paris par Amedy Coulibaly, un jihadiste se revendiquant de l'EI.
Les menaces du groupe sur les pays occidentaux sont diffus�es alors que l'El est mis en difficult� dans la ville de Koban�, dans les r�gions kurdes de Syrie. La ville, devenue le symbole de la r�sistance � l'EI depuis l'assaut des jihadistes en septembre, a �t� reprise par les Kurdes. Le groupe essuie ainsi sa d�faite la plus cuisante en Syrie.
Prise d'otage � Dammartin, le r�cit en images
