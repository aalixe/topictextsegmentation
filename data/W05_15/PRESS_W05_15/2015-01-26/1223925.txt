TITRE: Mortalit� routi�re en hausse, Cazeneuve attendu au tournant - L'Express
DATE: 26-01-2015
URL: http://www.lexpress.fr/actualite/societe/mortalite-routiere-en-hausse-cazeneuve-attendu-au-tournant_1644483.html
PRINCIPAL: 1223922
TEXT:
Mortalit� routi�re en hausse, Cazeneuve attendu au tournant
Par LEXPRESS.fr avec AFP, publi� le
26/01/2015 � 07:24
Bernard Cazeneuve va annoncer ce lundi une augmentation du nombre des morts sur les routes fran�aises en 2014. Une premi�re depuis douze ans. Il pr�sentera une nouvelle s�rie de mesures pour lutter contre ce ph�nom�ne.�
Zoom moins
12
� � � � �
Des sapeurs pompiers sur les lieux d'un accident de la route, le 14 janvier 2014 pr�s de Roubaix
afp.com/Philippe Huguen
Une hausse dont le gouvernement se serait bien pass�e. Le minist�re de l'Int�rieur devrait annoncer ce lundi les mauvais chiffres de la s�curit� routi�re . Entre 120 et 140 morts de plus ont �t� comptabilis�s en 2014 par rapport � 2013, selon trois sources concordantes proches de la s�curit� routi�re. Il s'agit de la premi�re hausse annuelle depuis 2001 .�
Le nombre de morts sur les routes avait recul� de 10,5% en 2013 atteignant un nouveau record � la baisse depuis 1948, l'ann�e des premi�res statistiques. Il y avait eu 3.268 tu�s en 2013 soit 403 vies �pargn�es en un an. Le gouvernement avait affich� l'an dernier un objectif de diminuer � 2000 le nombre de morts par an sur les routes en 2020 .�
A quelques rares exceptions, comme l'ann�e 2001, le nombre de morts sur les routes est en baisse constante depuis 1973. Cette ann�e-l�, les autorit�s avaient recens� plus de 18 000 morts en France. La mortalit� a ainsi �t� divis�e par cinq en un peu plus de quarante ans en France.�
Des mesures pour faire diminuer la mortalit� routi�re
"Le ministre va nous annoncer une remont�e de la mortalit� apr�s 12 ans de baisse. Cette remont�e est d'autant plus inacceptable qu'elle �tait �vitable", a r�agi aupr�s  Chantal Perrichon, la pr�sidente de la Ligue contre la violence routi�re. "Cette remont�e est la responsabilit� de la puissance publique qui n'a annonc� aucune nouvelle mesure depuis mars 2013", et la mise en place de radars de la troisi�me g�n�ration dans les voitures banalis�es, selon Chantel Perrichon.�
Pour enrayer cette hausse de la mortalit�, le ministre doit pr�senter lundi une nouvelle s�rie de mesures, a-t-on appris au minist�re. L'une d'elles concerne l'alcool�mie au volant , d'apr�s plusieurs sources proches concordantes.�
Le ministre a d'ores et d�j� r�p�t� qu'une baisse g�n�rale de la limitation de vitesse de 90 km/h � 80 km/h sur les routes secondaires bidirectionnelles, recommand�e par le Conseil national de la s�curit� routi�re (CNSR), �tait exclue. Cette limitation ne sera exp�riment�e que sur quelques tron�ons.�
Avec
