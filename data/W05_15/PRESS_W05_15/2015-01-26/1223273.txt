TITRE: Italie - Serie A: La Roma cale et l�che du terrain sur la Juventus | francetv sport
DATE: 26-01-2015
URL: http://www.francetvsport.fr/football/italie/serie-a-la-roma-cale-et-lache-du-terrain-sur-la-juventus-263147
PRINCIPAL: 1223269
TEXT:
Serie A: La Roma cale et l�che du terrain sur la Juventus
Francesco Totti n'a pas trouv� de solution face � la Fiorentina (TIZIANA FABI / AFP)
Par francetv sport
Publi� le 25/01/2015 | 22:44, mis � jour le 25/01/2015 | 23:03
Quelques heures apr�s la victoire de la Juve sur le Chievo V�rone (2-0), la Roma n'a pu faire mieux qu'un match nul sur la pelouse de la Fiorentina (1-1) dimanche. Les Gialorossi, men�s apr�s l'ouverture du score de Mario Gomez (19e), ont r�ussi � recoller juste apr�s la pause par l'interm�diaire de Ljajic (49e). Ils n'ont toutefois pas pu renverser la marque. Au classement, la Louve coince � sept unit�s de la Juventus, leader de Serie A.
La Roma a limit� les d�g�ts � Florence (1-1), mais elle se tra�ne depuis un mois avec ce quatri�me nul en cinq rencontres. Avant cette s�rie, elle �tait revenue � une seule longueur de la Juve.�Si l'�quipe de Rudi Garcia a laiss� six points en route � sa rivale pour le "Scudetto", elle le doit � la baisse de son pouvoir offensif (5 buts sur ces cinq matches) et � son inconstance, soulign�e par l'entra�neur fran�ais lui-m�me.�Contre la Fiorentina, la Roma a encore une fois compl�tement laiss� filer sa premi�re p�riode.�Domin�e, elle s'est inclin�e sur le troisi�me but de la semaine de Mario�Gomez, enfin r�veill�.
Apr�s la pause, la Roma s'est r�veill� et a �galis� par Adem Ljajic, ex de la Fiorentina, sur une grande action de Juan Iturbe, qui a r�sist� � la charge du d�fenseur dans la surfacer pour servir le Serbe.�Le match vivant, entre deux �quipes joueuses, aurait pu basculer d'un c�t� comme de l'autre, mais n'a finalement pas donn� de vainqueur.
