TITRE: Actualité | Un astéroïde va frôler la terre ce soir  - Le Bien Public
DATE: 26-01-2015
URL: http://www.bienpublic.com/actualite/2015/01/26/un-asteroide-va-froler-la-terre-ce-soir
PRINCIPAL: 1224243
TEXT:
Envoyer à un ami
L'astéroïde "2004 BL86", qui mesure 500 mètres de diamètre, va frôler notre planète ce lundi soir. Ne représentant aucun danger pour la terre, il passera à 1,2 million de km, le "caillou" est tout de même le premier à passer aussi près.
"Ce sera l'astéroïde le plus près de la Terre pour au moins les 200 prochaines années", a ainsi indiqué Don Yeomans, scientifique à la Nasa.
Une bonne paire de jumelles ou une lunette astronomique permettront d'observer ce rare phénomène.  
#Asteroid to fly by Earth safely tomorrow. Latest details & facts: http://t.co/mfbf0oTpY4 @AsteroidWatch pic.twitter.com/PALQHPt38K
�?? NASA (@NASA) 25 Janvier 2015
DIRECT : un astéroïde de belle dimension va croiser la Terre aujourd'hui #2004BL86 http://t.co/rpS9OdDlEu pic.twitter.com/VbZYZx5ZbB
�?? Guillaume Cannat (@GuillaumeCannat) 26 Janvier 2015
Vos commentaires
