TITRE: Ligue 2. Revivez Angers - Brest
DATE: 26-01-2015
URL: http://www.ouest-france.fr/ligue-2-angers-veut-continuer-dy-croire-face-brest-3144422
PRINCIPAL: 1226448
TEXT:
Ligue 2. Revivez Angers - Brest
Angers -
Les Angevins esp�rent jouer un bien vilain tour aux Brestois.�|�Photo : Philippe Renault/Ouest-France.
Facebook
Achetez votre journal num�rique
Sur une excellente dynamique avec quatre matches sans d�faite, les Angevins ont �t� men�s 0-1 par Brest sur un but de Belghazouani (23'). Egalisation de Kodjia (58' sp).
En match d�cal� de la 21e journ�e, les Angevins accueillent les Brestois avec l'ambition de se rapprocher du podium et de se m�ler � la lutte pour l'accession.
Les �quipes
ANGERS :�Butelle - Angoulan, Guillon, Thomas, Manceau - Diers, Auriac (cap), Frikeche - Ngando, Kodjia,
Cl�mence.�Entra�neur�:�St�phane Moulin.
Rempla�ants : Letellier (g), Ke�ta, Ben Othman, Pessalli, Ayari.�
BREST :�Th�baux - Tritz, Traor�, Martial, Moimb� - Ramar� (cap), Tour�, Doumbia - Belghazouani, Alphonse,
Laborde.�Entra�neur�:�Alex Dupont.
Rempla�ants : Chardonnet, Ranneaud, Missilou, Hamdi, Sea.�
Tags :
