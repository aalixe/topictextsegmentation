TITRE: Alg�rie : avant le rapatriement du corps d'Herv� Gourdel, l'Alg�rie lui rend hommage
DATE: 26-01-2015
URL: http://www.rtl.fr/actu/international/algerie-avant-le-rapatriement-du-corps-d-herve-gourdel-l-algerie-lui-rend-hommage-7776339002
PRINCIPAL: 1225261
TEXT:
Accueil Actu International Alg�rie : avant le rapatriement du corps d'Herv� Gourdel, l'Alg�rie lui rend hommage
Alg�rie : avant le rapatriement du corps d'Herv� Gourdel, l'Alg�rie lui rend hommage
Une hommage a �t� rendu � Alger avant le rapatriement du corps d'Herv� Gourdel.
Cr�dit : AFP
Herv� Gourdel avait �t� enlev� le 21 septembre avant d'�tre abattu quelques jours plus tard par le groupe islamiste Jund al-Khilafa (Archives)
publi� le 26/01/2015 � 10:29
Partager
Commenter
Imprimer
Une c�r�monie s'est tenue lundi matin � l'a�roport international d'Alger en hommage au touriste fran�ais Herv� Gourdel, avant le transfert de son corps en France.
Cette c�r�monie est le "dernier adieu � Herv� Gourdel en terre alg�rienne", a d�clar� l'ambassadeur de France � Alger, Bernard Emie.
Le corps du guide de montagne, enlev� puis d�capit� fin septembre par des djihadistes alg�riens, est rapatri� en France apr�s avoir �t� retrouv� le 15 janvier par l'arm�e alg�rienne en Kabylie (est d'Alger).
Son cercueil d�pos� au milieu de la chambre fun�raire de l'a�roport aux murs tapiss�s de versets du Coran et de photos de La Mecque �tait surmont� d'une couronne de fleurs ceinte d'une �charpe tricolore et du sac de voyage du guide de montagne.
Une feuille coll�e au sac portait la liste de son mat�riel retrouv�: un sac de couchage, un b�ton de marche, deux ceinture d'escalade, un porte-feuille vide, des lunettes de soleil et un guide de voyage notamment.
Avant le d�but de la c�r�monie, le cercueil a �t� inspect� par des d�mineurs.
Herv� Gourdel voulait d�couvrir "les magnifiques paysages de Kabylie"
L'ambassadeur de France a rappel� que le guide fran�ais �tait parti en Alg�rie en raison de "sa passion pour la montagne" et pour d�couvrir "les magnifiques paysages de Kabylie".
Il a salu� "la mobilisation de l'arm�e alg�rienne" pour traquer ses assassins et retrouver son corps. Il s'est �galement f�licit� de l'"excellente coop�ration judiciaire" entre l'Alg�rie et la France dans cette affaire.
Du c�t� alg�rien, de hauts responsables du minist�re des Affaires �trang�res �taient pr�sents � la c�r�monie.
Herv� Gourdel, �g� de 55 ans et originaire du sud de la France, avait �t� enlev� par le groupe arm� Jund al-Khilafa, qui a affirm� l'avoir ex�cut� en repr�sailles � l'engagement de la France aux c�t�s des Etats-Unis dans les frappes contre le groupe Etat islamique en Irak.
La r�daction vous recommande
