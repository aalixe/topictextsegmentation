TITRE: Le futur SUV de Renault se nommera Kadjar - NEWS
DATE: 26-01-2015
URL: http://www.planeterenault.com/1-gamme/1597-actualite-automobile/4286-futur-suv-renault-se-nommera-kadjar/
PRINCIPAL: 1224766
TEXT:
Par Team Plan�te Renault le 26/01/2015
�
�Les ventes mondiales de Renault en hausse L'accord Mitsubishi-Renault sans avenir...
Le futur SUV de Renault se nommera Kadjar
Ca y est, c'est officiel, le futur SUV que Renault pr�sentera le 2 f�vrier, se nommera Kadjar. Plusieurs noms avaient �t� cit�s, dont Kadjar, Djeyo ou encore Raccon du nom du concept-car des ann�es 90.
R�servez ici l'essai de votre future voiture
Ce sera donc un tout nouveau nom que Renault va introduire au sein de sa gamme, avec ce nouveau v�hicule, un crossover, qui se positionnera sur le segment C aux c�t�s du Qashqai dont il sera assez proche.
Ce nom est une fois de plus, un mix de deux noms, KAD voulant s'inspirer de "Quad", un petit v�hicule tout terrain, et JAR, qui rappelerait selon Renault les mots � agile � et � jaillir �. Soit...
L'habitacle tr�s germanis� du Kadjar
La sonorit� commen�ant par "K", comme le... Captur, mais aussi le Koleos, signe les mod�les crossovers de chez Renault.
Les 25 derni�res actualit�s li�es:
