TITRE: Champions Cup: Apr�s un d�but id�al, Toulouse a sabord� sa campagne europ�enne - Champions Cup 2014-2015 - Rugby - Rugbyrama
DATE: 26-01-2015
URL: http://www.rugbyrama.fr/rugby/european-rugby-champions-cup/2014-2015/champions-cup-apres-un-debut-ideal-toulouse-a-saborde-sa-campagne-europeenne_sto4572392/story.shtml
PRINCIPAL: 1223165
TEXT:
Derni�re mise � jour Le 25/01/2015 � 19:07 - Publi� le 25/01/2015 � 16:50
Par Rugbyrama - Le 25/01/2015 � 19:07
Apr�s avoir remport� ses quatre premiers matchs, Toulouse s'est finalement montr� incapable de franchir le cap des phases de poule en perdant contre Bath puis � Montpellier . Face au MHR, les Toulousains menaient pourtant de onze points! L'antis�che.
�
Le jeu: Quatre minutes fatales � Toulouse
Dans une rencontre assez folle, le Stade toulousain avait r�ussi � prendre la mesure du MHR et semblait filer vers la qualification pour les quarts de finale. Mais en quatre minutes, tout a bascul�. Un premier essai de Lucas Dupont apr�s une relance h�raultaise de plus de 70 m�tres conclue par une superbe acc�l�ration de l'ancien Grenoblois, venait mettre le doute dans les t�tes haut-garonnaises. Pire, Dupont inscrivait un doubl� � peine quatre minutes plus tard pour replacer Montpellier devant au score (21-20). Dommage pour les Toulousains car apr�s avoir men� de onze points, il leur suffisait d'assurer une victoire, m�me non bonifi�e car du c�t� de Bath, Glasgow a fait mieux que r�sister. Mais � l'Altrad Stadium, les joueurs de Guy Nov�s n'ont pas �t� assez performants dans les soutiens, conc�dant de nombreux contests. Surtout, ils ont men� au score plus d'une fois, mais ont �t� trop indisciplin�s � chaque fois que Montpellier revenait dans leurs 22 m�tres. Les deux p�nalit�s manqu�es par Luke McAlister en d�but de match co�tent cher au final, le Stade s'inclinant d'un petit point (27-26). Des d�tails qui symbolisent une formation en plein doute et privent les Rouge et Noir d'un quart de finale europ�en.
Les joueurs: Doubl� pour Dupont, McAlister hors-sujet
Dans ce match � rebondissements mais moyen en terme de qualit�, rares ont �t� les joueurs � se mettre en avant. Lucas Dupont a fait parler ses jambes pour un doubl� qui a mis un �norme coup derri�re la t�te des Toulousains. 107 m�tres parcourus ballon en main, deux d�fenseurs battus et donc deux essais, l�ailier h�raultais s�est av�r� l��l�ment le plus tranchant de son �quipe. Bonne activit� de Robert Ebersohn au centre du terrain avec un 14/14 au plaquage. Ben Lucas, malgr� du d�chet, a plut�t assur� dans le jeu au pied (11 points). En revanche, match totalement rat� pour l�arri�re Pierre B�rard , qui s�est fait contrer sur les deux essais toulousains. Fulgence Ouedraogo a �cop� de trois p�nalit�s.
�lu homme du match, Ga�l Fickou , absent contre Bath, a apport� du punch au sein de la ligne d�attaque toulousaine. D�cisif sur l�essai de Cl�ment Poitrenaud , le centre international a ajout� un franchissement, cinq d�fenseurs battus et un 8/8 au plaquage � son bilan. Copie correcte �galement pour Gilian Galan , auteur de plusieurs bonnes charges (60 m�tres parcourus et trois d�fenseurs battus). C�t� d�ception, Yoann Huget s�est trou� en d�fense, Vincent Clerc n�a rien apport� alors que Jean-Marc Doussain s�est encore montr� trop lent dans ses sorties de balle et a fait plusieurs mauvais choix. Sorti � la pause, Luke McAlister a �t� totalement hors-sujet.
Ga�l Fickou, homme du match avec Toulouse contre Montpellier - Icon Sport
Le tournant qui n'a pas eu lieu: la p�nalit� n'est jamais venue
Accusant un seul point de d�bours, Toulouse profitait de la possession du ballon, dans le camp h�raultais, � moins de trois minutes du coup de sifflet final. La tactique toulousaine? Encha�ner les temps de jeu pour esp�rer une p�nalit�. Sauf que les hommes de Guy Nov�s se sont pr�cipit�s. Et la qualit� des soutiens, gros point noir ce dimanche, n�a pas permis � Jano Vermaak d��tre souverain pour �jecter le ballon. Pire, un en-avant toulousain redonnait le ballon � Montpellier. Qui a su, lui, le conserver� jusqu�� obtenir une p�nalit� lib�ratrice.
Le tweet qui nous a fait sourire
M�me en son absence, Fabien Galthi� n'est pas oubli�...
�
�
La stat: 11
Rentr� � la pause en remplacement de Luke McAlister, Toby Flood n'a pas mis longtemps pour se mettre en �vidence. A la 43e minute, l'ouvreur anglais contrait en effet Pierre B�rard (d�j� contr� par Gael Fickou sur l'essai de Cl�ment Poitrenaud) et filait � l'essai. Apr�s la transformation, le Stade toulousain menait alors de onze points. Une avance qui a finalement fondue comme neige au soleil, alors qu'une victoire, m�me d'un petit point, aurait suffit � Toulouse pour voir les quarts de finale.
La d�cla: Dusautoir (capitaine de Toulouse)
" Force est de constater que, sur les deux derni�res semaines, l'on ne m�ritait pas notre place"
Ivan PETROS et Cl�ment MAZELLA
�Partager
