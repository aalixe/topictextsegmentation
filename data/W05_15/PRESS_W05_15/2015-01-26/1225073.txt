TITRE: New York en �tat d'urgence avant une temp�te aux Etats-Unis
DATE: 26-01-2015
URL: http://www.lemonde.fr/planete/article/2015/01/26/des-milliers-de-vols-annules-aux-etats-unis-avant-la-tempete_4563678_3244.html
PRINCIPAL: 0
TEXT:
New York en �tat d'urgence avant une temp�te aux Etats-Unis
Le Monde |
� Mis � jour le
26.01.2015 � 20h46
Les Etats-Unis se pr�parent � l'arriv�e d'une temp�te historique. Treize comt�s de l'Etat de New York, dont celui de la ville homonyme, ont �t� d�clar�s en �tat d'urgence par le gouverneur Andrew Cuomo, lundi 26 janvier, � quelques heures de l'arriv�e de la temp�te de neige qui doit s'abattre sur le nord-est du pays dans la nuit de lundi � mardi.
I�m declaring a state of emergency for NYS counties: Bronx, Dutchess, Kings, Nassau, New York, Orange, Putnam, Queens, Richmond... (1/2)
� Andrew Cuomo (@NYGovCuomo)
State of Emergency also declared for Rockland, Suffolk, Ulster and Westchester Counties. (2/2) #blizzardof2015
� Andrew Cuomo (@NYGovCuomo)
Plus de 2 000 vols ont par ailleurs �t� annul�s dans tout le pays, alors que le trafic a�rien �tait perturb� depuis la matin�e, accusant 271 retards selon le site sp�cialis� Flightaware.
De 5 � 10 centim�tres de neige sont attendus lundi, qui seront suivis par un autre �pisode neigeux en soir�e avec de ��lourdes�� pr�cipitations, notamment � Boston et New York, selon le Service national de m�t�orologie (NWS). Au total, 45 � 60 cm de neige pourraient s'accumuler dans les deux villes, voire pr�s de 80 cm dans l'est du Massachusetts. L'avis de blizzard s'�tend jusqu'� la fronti�re canadienne.
INQUI�TUDE � NEW YORK ET BOSTON
Le maire de New York, Bill de Blasio, a mis en garde dimanche contre l'arriv�e de cette temp�te qui s'annonce comme � l'une des plus importantes de l'histoire de cette ville �. ��Pr�parez-vous � quelque chose de pire que ce que nous avons vu jusqu'� pr�sent��, a-t-il insist�, demandant � ses administr�s de ne pas prendre la route.
Voir aussi le point sur les perturbations dans les transports publi� par le ��New York Times��
Dimanche soir, de nombreux supermarch�s new-yorkais avaient �t� pris d'assaut par des habitants inquiets. Dans le quartier de Chelsea, au centre de Manhattan, une file d'attente s'�tait form�e sur le trottoir pour pouvoir entrer dans le magasin Trader Joe's sur la 6e avenue.
Cinquante millions de personnes dans le nord-est pourraient �tre affect�es par la temp�te. A Boston, le maire Martin Walsh a appel� ses concitoyens � s'occuper de leurs voisins les plus vuln�rables.
��Des responsables de la Maison Blanche ont �t� en contact avec des responsables locaux le long de la c�te est pour s'assurer qu'ils disposaient des ressources n�cessaires pour se pr�parer et r�agir imm�diatement � la temp�te��, a d�clar� un porte-parole de l'ex�cutif am�ricain, lors d'un point presse � New Delhi, o� Barack Obama effectue une visite de trois jours.
Mega temp�te de neige pr�vue � New York ce soir.Au fait �a fait combien 20 inches?Le pr�c�dent de 1888 #Newyorkstorm http://t.co/rOCbUoErKX
� St�phane Lauer (@StephaneLauer)
