TITRE: Miss Univers 2014 : c�est Paulina Vega, Miss Colombie ! � metronews
DATE: 26-01-2015
URL: http://www.metronews.fr/people/miss-univers-2015-c-est-paulina-vega-miss-colombie/moaz!c72RDLcZ33XPQ/
PRINCIPAL: 1223594
TEXT:
Mis � jour  : 26-01-2015 16:40
- Cr�� : 26-01-2015 05:18
Miss Univers 2014 : c�est Paulina Vega, Miss Colombie !
REINE DE BEAUTE � Apr�s Miss V�n�zuela l'an dernier, c�est une autre sud-am�ricaine qui a remport� le concours Miss Univers 2014. Paulina Vega, Miss Colombie, a triomph� dans la nuit de dimanche � lundi � Miami. D�ception pour Miss France : Camille Cerf termine aux portes du top 10.
�
PUBLICIT�
La couronne reste sur le m�me continent. Comme l�an dernier, c�est une sud-am�ricaine qui a triomph�, cette nuit, lors du concours Miss Univers 2014 qui se tenait � Miami, en Floride. La Colombienne Paulina Vega a succ�d� en effet � la V�n�zu�lienne Maria Gabriela Isler. Ce mannequin de 22 ans, originaire de Barranquilla, avait �t� �lue Miss Colombie en 2013. Elle est la deuxi�me Miss Univers de son pays, apr�s�Luz Marina Zuluaga, en 1958.
Miss France dans le top 15
C�t� tricolore, c�est la d�ception. Camille Cerf , qui figurait parmi les 15 finalistes, a �t� �limin�e aux portes du top 10 qui comprenait, outre la gagnante, Miss Etats-Unis, Miss Philippines, Miss Espagne ou encore Miss Pays-Bas.
