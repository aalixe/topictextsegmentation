TITRE: Pour Macron, la France «est dos au mur»
DATE: 26-01-2015
URL: http://www.lefigaro.fr/conjoncture/2015/01/26/20002-20150126ARTFIG00427-pour-macron-la-france-est-dos-au-mur.php
PRINCIPAL: 1226113
TEXT:
Publié
le 26/01/2015 à 20:12
VIDÉOS - Les députés ont attaqué en séance l'examen du projet de loi porté par le ministre de l'Économie.
Publicité
L'emblématique loi Macron , qui a pour ambition de lever les freins à l'activité, a entamé ce lundi une nouvelle séquence de son marathon parlementaire. Après avoir passé l'épreuve des 1758 amendements lors de la commission spéciale qui s'est tenue la semaine du 12 janvier, elle est arrivée en séance à l'Assemblée. Et le nombre d'amendements est à la hauteur à la fois des ambitions que le gouvernement prête à ce texte pour «la croissance, l'activité et l'égalité des chances économiques», mais aussi de la mobilisation des parlementaires: 3034 amendements ont été déposés!
Preuve de l'enjeu, le ministre de l'Économie n'a pour une fois, en présentant sa loi, pas décollé les yeux de son discours, emprunt d'une volonté d'être «à la hauteur» des attentes des Français après les événements terroristes de ces dernières semaines.
Devant un auditoire franchement clairsemé, entouré de Christiane Taubira (Justice) et François Rebsamen (Travail), il a redit sa conviction que «notre pays est dos au mur et le statu quo n'est plus une option». Avant d'insister: «Nous avons besoin d'un nouveau souffle» qui «récompense le risque et décourage la rente». Alors «oui, cette loi traite de nombreux sujets. Oui, elle touche de nombreux domaines et une multitude de professions», a-t-il martelé, en guise de réponse à ceux qui lui ont reproché d'avoir élaboré un texte fourre-tout.
Deux semaines de débat
Mais plus que se justifier, Macron a voulu se montrer offensif: «Cette loi porte plusieurs réformes dont on a beaucoup parlé, mais qui n'ont jamais été faites», a-t-il lâché, en référence à celle des professions réglementées . Sur ce point précis, le ministre veut croire que le texte «montrera que les Français sont capables de bouger», que «nous ne sommes prisonniers d'aucun dogme, ni d'aucun intérêt en place». Mais les discussions risquent d'être vives lors des débats� Les rapporteurs du projet de loi veulent notamment tenter de faire supprimer par amendement le mécanisme de «corridor tarifaire» afin de maintenir le principe d'une tarification unique.
L'autre sujet périlleux, l'ouverture des commerces le dimanche , sera aussi très discuté. Notamment le point portant sur les compensations des salariés. Les débats doivent durer deux semaines.
Si le texte divise la majorité (les frondeurs PS y restent hostiles), comme l'opposition (des députés vont approuver certaines mesures), la position de l'UDI et des écologistes a évolué dans le bon sens ; le gouvernement devrait in fine parvenir à faire voter son texte.
