TITRE: Maroc : un Alg�rien li� � la mort d'Herv� Gourdel arr�t� - Afrik.com : l'actualit� de l'Afrique noire et du Maghreb - Le quotidien panafricain
DATE: 26-01-2015
URL: http://www.afrik.com/maroc-un-algerien-lie-a-la-mort-d-herve-gourdel-arrete
PRINCIPAL: 1225061
TEXT:
lundi 26 janvier 2015 / par Kardiatou Traor�
Un Alg�rien soup�onn� d�appartenir au groupe islamiste responsable de l�assassinat d�Herv� Gourdel a �t� interpell� au nord-est du Maroc.
La lumi�re sur les circonstances de l�assassinat d�Herv� Gourdel est en passe d��tre faite. En effet, l�un des suspects de l�ex�cution de l�ex-otage fran�ais a �t� interpell�, dimanche 25 janvier 2015, � Beni Drar, au nord-ouest du Maroc. Selon le minist�re marocain de l�Int�rieur, le suspect a �t� arr�t� ��en possession de grandes quantit�s de substances dangereuses��. Le suspect dont l�identit� est pour l�heure m�connue, d�tenait �galement des armes � feu.
L�enl�vement et l�ex�cution d�Herv� Gourdel ont �t� revendiqu�s par le groupe islamiste Jund al-Khilafa dont le chef a �t� tu� lors d�un accrochage avec des soldats alg�riens. Les investigations � l�issue de l�assassinat d�Herv� Gourdel ont conduit l�arm�e alg�rienne � lancer une vaste op�ration afin de retrouver le corps du guide fran�ais et tenter de localiser ses assassins. Durant les investigations, 15 personnes  de nationalit� alg�rienne ont �t� poursuivies. Par ailleurs, six djihadistes appartenant au groupe islamiste Jund al-Khilafa ont �t� tu�s depuis le d�but de l�enqu�te.
Herv� Gourdel, guide de hautes montagnes, �g� de 55 ans, avait �t� enlev�, le 21 septembre 2014, par le groupe arm� Jund al-Khilafa.  Ex�cut� en ��repr�sailles � l�engagement de la France�� contre l�Irak, son corps a �t� retrouv� le 15 janvier dernier enterr� sans t�te. Le corps d�Herv� Gourdel sera remis � ses proches, ce lundi.
lire aussi
