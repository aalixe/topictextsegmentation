TITRE: VIDEO. Avalanche mortelle : les victimes �taient des habitu�es de la montagne
DATE: 26-01-2015
URL: http://www.francetvinfo.fr/faits-divers/accident/video-avalanche-mortelle-les-victimes-etaient-des-habituees-de-la-montagne_806893.html
PRINCIPAL: 0
TEXT:
/ Accidents
VIDEO. Avalanche mortelle : les victimes �taient des habitu�es de la montagne
Une avalanche a tu� six personnes dans le massif du Queyras, dans les Hautes-Alpes. Plusieurs questions demeurent sur les circonstances du drame.
(FRANCE 2)
, publi� le
25/01/2015 | 22:28
Six skieurs de randonn�e qui avaient disparu dans le Queyras ont �t� retrouv�s mort s. Ces quatre hommes et deux femmes,��g�s de 58 � 73 ans, �taient partis samedi 24�janvier de Ceillac pour une randonn�e. Membres du Club alpin, ils �taient tous exp�riment�s, des habitu�s de ce massif du Queyras o� certains habitaient. "C'�taient des amoureux de la montagne", rapporte Ambroise Bouleis, journaliste de France�2 en direct du massif du Queyras.
S�rieux et �quip�s
"Parmi les victimes, il y avait une ancienne monitrice de ski. Ils avaient la r�putation d'�tre s�rieux. La veille de leur sortie, l'un d'eux �tait parti faire une reconnaissance sur une partie du circuit emprunt�. Ils �taient aussi bien �quip�s, chacun portait sur lui un d�tecteur de victimes d'avalanche, un appareil �lectronique qui a permis de les localiser, mais trop tard pour les sauver", d�plore le journaliste.
Le JT
Les autres sujets du JT
1
