TITRE: Loi Macron: manifestation � Paris contre le travail le dimanche
DATE: 26-01-2015
URL: http://www.boursorama.com/actualites/loi-macron-manifestation-a-paris-contre-le-travail-le-dimanche-9aaa86105e9cd328f03e0ee50729142a
PRINCIPAL: 1225541
TEXT:
Tweet 0
L'embl�matique projet de loi Macron, � l'ambition affich�e de lever des "blocages" de l'�conomie, �tait pr�sent� lundi dans l'h�micycle de l'Assembl�e. Une des mesures phares et contest�es du projet, la g�n�ralisation du travail le dimanche, a pouss� les principaux syndicats � manifester � Paris. Dur�e: 01:48
Copyright � 2015 AFP. Tous droits de reproduction et de repr�sentation r�serv�s.
Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par l'AFP. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, transmise, rediffus�e, traduite, vendue, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de l'AFP. l'AFP ne pourra �tre tenue pour responsable des d�lais, erreurs, omissions, qui ne peuvent �tre exclus ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
