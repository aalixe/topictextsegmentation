TITRE: VIDEO. Ouverture du proc�s de l'affaire Bettencourt
DATE: 26-01-2015
URL: http://www.francetvinfo.fr/politique/affaire/affaire-bettencourt/video-ouverture-du-proces-de-l-affaire-bettencourt_807515.html
PRINCIPAL: 1225545
TEXT:
/ Affaire Bettencourt
VIDEO. Ouverture du proc�s de l'affaire Bettencourt
Ce lundi 26 janvier s'ouvre le proc�s de l'affaire Bettencourt au tribunal correctionnel de Bordeaux. Sur le banc des pr�venus, 10 personnes soup�onn�es d'abus de faiblesse et de confiance aupr�s de la milliardaire.
(FRANCE 2 )
, publi� le
26/01/2015 | 17:06
Le proc�s de l'affaire Bettencourt s'ouvre ce lundi 26 janvier au tribunal correctionnel de Bordeaux. 10 personnes sont soup�onn�es d'abus de faiblesse et d'abus de confiance aupr�s de la milliardaire.
Parmi les personnes qui devaient �tre pr�sentes sur le banc des pr�venus, son ancien infirmier. Le procureur a annonc� ce lundi matin qu'il a tent� de se suicider � la veille de l'ouverture du proc�s.
"Fran�ois-Marie Banier a su, comme un gourou, diviser la famille"
Parmi les autres pr�venus, l'ancien ministre Eric Woerth, poursuivi pour recel d'une somme que lui aurait remise Patrice de Maistre, le gestionnaire de fortune de Liliane Bettencourt, mais aussi le photographe mondain Fran�ois-Marie Banier.
Pour la famille Bettencourt, le pr�judice s'�l�ve � des centaines de millions d'euros. Fran�ois Marie-Bannier est la principale cible "pas tellement pour l'argent mais surtout pour la fa�on dont il a su, � l'image de certains gourous, comme un pr�dateur, diviser la famille", explique ma�tre Beno�t Ducos-Ader, avocat de Liliane Bettencourt.
Liliane Bettencourt, en 2007, �tait en froid avec sa fille Fran�oise Myers, qui s'inqui�tait que sa m�re soit trop entour�e.
Le JT
Les autres sujets du JT
1
