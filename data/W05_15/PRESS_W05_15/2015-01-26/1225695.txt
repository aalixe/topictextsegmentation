TITRE: LEAD 1-Tsipras mandat� par les Grecs pour combattre l'aust�rit�- 26 janvier 2015 - Challenges.fr
DATE: 26-01-2015
URL: http://www.challenges.fr/monde/20150126.REU7585/lead-1-tsipras-mandate-par-les-grecs-pour-combattre-l-austerite.html
PRINCIPAL: 0
TEXT:
Challenges �>� Monde �>�LEAD 1-Tsipras mandat� par les Grecs pour combattre l'aust�rit�
LEAD 1-Tsipras mandat� par les Grecs pour combattre l'aust�rit�
Publi� le� 26-01-2015 � 17h12
* Syriza s'allie aux Grecs ind�pendants
* Le parti d'Alexis Tsipras a rat� la majorit� absolue de 2 si�ges
* Un nouveau gouvernement pourrait pr�ter serment mercredi
* Les discussions en vue sur la dette inqui�tent l'Europe
* Les march�s gardent leur calme   (Avec prestation de serment, r�actions, d�tails)
par Renee Maltezou et Deepa Babington
ATHENES, 26 janvier ( Reuters ) - Alexis Tsipras a pr�t� serment lundi en Gr�ce comme nouveau Premier ministre d'un gouvernement d�termin� � n�gocier pied � pied avec les cr�anciers internationaux et � tourner la page de cinq ann�es d'aust�rit�.
Quelques heures seulement apr�s sa tr�s nette victoire aux l�gislatives, le jeune chef du parti de gauche Syriza, �g� de 40 ans, a conclu dans la matin�e un accord de gouvernement avec le mouvement des Grecs ind�pendants (droite souverainiste), ce qui lui permettra d'entamer rapidement les n�gociations avec les bailleurs de fonds d'Ath�nes.
"Nous avons une route abrupte � gravir", a d�clar� Alexis Tsipras au pr�sident Karolos Papoulias juste avant de pr�ter serment, sans cravate comme � son habitude, ni la b�n�diction - orthodoxe - qui accompagne traditionnellement cette c�r�monie: il en avait fait la demande � l'archev�que Ieronymos.
Syriza a fr�l� dimanche la majorit� absolue, avec plus de 36% des voix et 149 d�put�s sur un total de 300 � la Vouli. Avec le renfort des Grecs ind�pendants, qui disposent de 13 �lus au parlement, le nouveau gouvernement disposera d'une majorit� de 162 si�ges.
L'accord conclu avec le petit parti de Panos Kammenos donne naissance � une alliance gauche-droite inhabituelle mais soud�e par l'opposition aux conditions dont d�pend l'aide financi�re de la "tro�ka" (Union europ�enne, Banque centrale europ�enne et Fonds mon�taire international).
"La Gr�ce laisse derri�re elle une aust�rit� catastrophique, elle laisse derri�re elle la peur et l'autoritarisme, elle laisse derri�re elle cinq ann�es d'humiliation et d'angoisse", a affirm� dimanche soir Alexis Tsipras, le poing lev�, devant plusieurs milliers de ses supporters r�unis � Ath�nes.
Les march�s financiers ont r�agi plut�t calmement � la victoire de Syriza: la Bourse d'Ath�nes a c�d� plus de 3%  mais l'euro, tomb� dans la nuit � 1,11 dollar, est remont� � 1,1260. Le rendement des obligations d'Etat grecques � 10 ans, lui, se tendait l�g�rement mais restait sous la barre de 9% .
UN NOUVEAU GOUVERNEMENT D�VOIL� MARDI
Le gouvernement que s'appr�te � former Alexis Tsipras sera le premier, dans la zone euro, �lu sur un programme d'opposition aux politiques de rigueur budg�taire et d'aust�rit� �conomique pr�n�es par l' UE depuis le d�but de la crise et toujours d�fendu par plusieurs pays membres, Allemagne en t�te.
La composition du cabinet devrait �tre d�voil�e mardi. Yanis Varoufakis, un �conomiste farouchement hostile aux politiques d'aust�rit�, devrait obtenir le portefeuille des Finances.
"Le peuple de Gr�ce a offert un vote d'espoir. Il s'est servi des urnes, dans une splendide c�l�bration de la d�mocratie, pour mettre un terme � une crise qui s'auto-alimente, nourrit l'indignit� et les forces les plus sombres en Europe", a-t-il �crit sur son blog.
Pour la premi�re fois depuis 40 ans et la fin de la dictature du r�gime des colonels en 1974, ni les socialistes du Pasok, ni les conservateurs aujourd'hui regroup�s dans Nouvelle D�mocratie (ND), le parti du Premier ministre sortant Antonis Samaras, ne seront au pouvoir � Ath�nes.
Nouvelle d�mocratie a pris la deuxi�me place du scrutin avec 27% des voix devant le parti d'extr�me droite Aube dor�e (6,3%) et le parti centriste To Potami (6%), le Pasok terminant � la septi�me place (4,7%).
Le mouvement des Grecs ind�pendants, n� en 2012 d'une scission de ND, diff�re de Syriza sur nombre de sujets de soci�t�, comme l' immigration ill�gale, qu'il veut r�primer, ou les liens entre l'Etat et l'Eglise orthodoxe, qu'il d�fend.
Mais les deux partis se retrouvent sur la m�me ligne dans leur rejet des contreparties li�es aux 240 milliards d'euros des plans d'aide accord�s � la Gr�ce.
Le parti de droite a notamment bataill� pendant la campagne des l�gislatives contre la lev�e d'un moratoire sur les saisies de biens immobiliers r�clam�e par les cr�anciers internationaux. "Pas de maisons aux mains de banquiers! Le chantage est termin�", a lanc� Panos Kammenos.
LA GRECE "DOIT PAYER, CE SONT LES R�GLES DU JEU"
Les ministres des Finances de la zone euro devaient d�battre de l'issue du scrutin grec lors de leur r�union mensuelle en fin de journ�e � Bruxelles .
Les Europ�ens se sont dits pr�ts � accorder plus de temps � Ath�nes pour rembourser son passif mais ne semblent gu�re dispos�s � n�gocier une r�duction de la dette.
Le ministre italien de l' Economie Pier Carlo Padoan a toutefois relev� que le message envoy� par la Gr�ce �tait le besoin de croissance et d'emploi, qu'il fallait concilier avec la rigueur budg�taire, ce que ne cesse de r�p�ter le pr�sident du Conseil Matteo Renzi .
"La France sera aux c�t�s de la Gr�ce" pour pr�parer son avenir, a de son c�t� d�clar� Fran�ois Hollande au nouveau Premier ministre grec, Alexis Tsipras, lors d'un entretien t�l�phonique.
Du c�t� de la BCE , Beno�t Coeur�, l'un des six membres du directoire de l'institution, a d'ores et d�j� exclu tout effacement, m�me partiel, de la dette grecque, sans pour autant non plus fermer la porte � un r��chelonnement.
Alexis Tsipras "doit payer, ce sont les r�gles du jeu europ�en, il n'y a pas de place pour un comportement unilat�ral en Europe , cela n'exclut pas une discussion par exemple sur le r��chelonnement de cette dette", a-t-il dit.
La Gr�ce, dont la dette, � 321 milliards d'euros, repr�sente 175% du produit int�rieur brut, est incapable de se financer seule sur les march�s alors qu'elle devra faire face � environ 10 milliards d'euros d'�ch�ances cet �t�.
Elle doit donc trouver un compromis avec la tro�ka pour d�bloquer le paiement des sept milliards d'euros d'aide en suspens, alors que le plan de financement en cours expire le 28 f�vrier. Une suspension de six mois des �ch�ances pr�vues pourrait �tre le premier sujet � l'ordre du jour des n�gociations.
VOIR AUSSI:
La victoire de Syriza, trompe-l'oeil de la gauche fran�aise
ENCADRE-Comment la Gr�ce peut esp�rer ren�gocier sa dette
ENCADRE-Le poids de la dette grecque
ENCADRE-Les Grecs ind�pendants, partenaire potentiel
(avec Angeliki Koutantou et George Georgiopoulos; Pierre S�risier, Henri-Pierre Andr�, Marc Angrand et Jean-St�phane Brosse pour le service fran�ais)
Partager
