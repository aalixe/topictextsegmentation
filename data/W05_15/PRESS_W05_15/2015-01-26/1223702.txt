TITRE: Open d'Australie : Djokovic, Wawrinka et Nishikori en quarts
DATE: 26-01-2015
URL: http://www.lemonde.fr/sport/portfolio/2015/01/26/open-d-australie-stan-wawrinka-serena-williams-et-cibulkova-valident-leur-tickets-pour-les-quarts_4563177_3242.html
PRINCIPAL: 0
TEXT:
Open d'Australie : Djokovic, Wawrinka et Nishikori en quarts
Le Monde |
� Mis � jour le
26.01.2015 � 16h05
Le num�ro 1 mondial et le tenant du titre se sont qualifi�s sans forcer pour les quarts de finale du premier Grand Chelem de l'ann�e.
1 / 5
Djokovic sans trembler - Le Serbe Novak Djokovic est le dernier qualifi� du tableau masculin pour les quarts de finale de l'Open d'Australie. Oppos� au Luxembourgeois Gilles Muller, 42e au classement ATP, le num�ro 1 mondial n'a pas trembl� et s'est impos� en trois sets (6/4, 7/5, 7/5). Cr�dits : AFP/WILLIAM WEST facebook twitter google + linkedin pinterest
Voir aussi
Foot : avalanche de buts en Europe
Rugby : six nations, six France-Galles
Foot : le carton du Bayern, la d�faite du Bar�a, et un gardien suisse dans l'histoire
Rugby : six nations, six France-Irlande
Lire le diaporamaMettre en pauseRejouer
Tennis
