TITRE: Le proc�s Bettencourt en cinq points cl�s
DATE: 26-01-2015
URL: http://www.latribune.fr/actualites/economie/france/20150126trib2bc480f93/le-proces-bettencourt-en-cinq-points-cles.html
PRINCIPAL: 1224881
TEXT:
OR 1 191,9$ -0,00                 %
Le proc�s Bettencourt en cinq points cl�s
Dix personnes sont poursuivies pour "abus de faiblesse", "abus de confiance", "blanchiment" ou "recel" au d�triment de Liliane Bettencourt, richissime h�riti�re de L'Or�al �g�e de 92 ans. (Cr�dits : reuters.com)
latribune.fr �|�
26/01/2015, 13:55
�-� 1191 �mots
Dix pr�venus, plusieurs centaines de millions d'euros qui auraient �t� extorqu�s. Remettez au clair avec "La Tribune" les enjeux du "premier" proc�s ouvert ce lundi dans le cadre de l'affaire Bettencourt.
sur le m�me sujet
Ouverte en 2007, l'affaire Bettencourt s'est transform�e�au fil des ann�es en�un imbroglio tentaculaire. La Tribune remet les points sur les i du proc�s principal qui s'ouvre le 26 janvier � Bordeaux.
Les faits reproch�s
Dix personnes sont poursuivies�pour�"abus de faiblesse", "abus de confiance", "blanchiment" ou "recel" au d�triment�de Liliane Bettencourt. �g�e de 92 ans, la richissime h�riti�re de L'Or�al, dont la fortune est estim�e �plus de 30 milliards d'euros (34,5 milliards de dollars selon le magazine am�ricain Forbes),�souffrait d'apr�s�une expertise psychiatrique de s�nilit� depuis septembre 2006.
L'�tat de cette femme, qui n'a �t�formellement plac�e sous tutelle qu'en septembre 2011,�fera sans doute l'objet d'intenses d�bats pendant le proc�s. La veuve sera repr�sent�e par un tuteur.
Les dix pr�venus
Parmi les dix personnes jug�es en correctionnelle, divers�profils sont�repr�sent�s: un artiste, un expert-comptable, un entrepreneur et�un ancien ministre, ainsi que trois�professionnels du droit, un gardien et un infirmier.
>>�DIAPORAMA�Proc�s Bettencourt : les protagonistes de l'affaire
Jug� pour "abus de faiblesse" et "blanchiment", le photographe (mais aussi peintre, dessinateur, �crivain et acteur)�Fran�ois-Marie Banier�a b�n�fici� de�plusieurs centaines de millions d'euros de largesses de Liliane Bettencourt, sous forme d'assurances-vie, donations, dons manuels et �uvres d'art. Pour chacun de ces d�lits, il risque au maximum trois ans de prison et 375.000 euros d'amende.
Martin d'Orgeval, avec qui Fran�ois-Marie Banier est pacs�, a aussi re�u 1,7 million d'euros en tableaux et 1,4 millions d'euros en photos. Il est poursuivi pour les m�mes chefs d'accusation que son compagnon.
Le gestionnaire de la fortune�de Liliane Bettencourt,�Patrice de Maistre,�ex-expert comptable, qui �tait r�mun�r� 800.000 euros par an, a en plus profit� d'une donation de�8 millions d'euros�et aurait encore re�u�4 millions en esp�ce. Il est �galement accus� d'"abus de faiblesse" et de blanchiment".
L'ancien ministre du Budget puis du Travail�Eric Woerth�est poursuivi pour "recel" d'une somme qu'il aurait re�ue�en 2007,�lorsqu'il �tait�tr�sorier de la campagne de Nicolas Sarkozy,�de�Patrice de Maistre. On lui reproche d'avoir encaiss� 50.000 euros et autres "montants ind�termin�s".
St�phane Courbit, homme d'affaires de l'audiovisuel , aurait pouss� la milliardaire � investir 143 millions d'euros dans son holding de jeux en ligne en d�cembre 2010. Il est poursuivi pour "abus de faiblesse".
Pascal Wilhem, avocat de Courbit et "mandataire de protection future" d�sign� �de la vieille dame, a facilit� cette derni�re transaction. Il est donc poursuivi pour "complicit� d'abus de faiblesse".
�galement renvoy� en correctionnel pour ce chef d'accusation, le notaire�Patrice Bonduelle aurait contribu� � la mise en place du mandat de protection future au profit de Pascal Wilhem en 2010.
L'infirmier � la retraite Alain Thurin entr� au service de la vieille dame en 2009, non seulement recevait un salaire exorbitant, mais a aussi b�n�fici� d'une donation couch�e sur testament de 10 millions d'euros. Poursuivi pour "abus de faiblesse", l'homme aurait tent� de se suicider la veille de l'ouverture du proc�s et est d�sormais "entre la vie et la mort", selon une source polici�re.
Carlos Vejarano, qui entretenait une �le des Seychelles�appartenant aux Bettencourt, aurait d�tourn� 3,4 millions d'euros.�Renvoy� pour "abus de faiblesse" et "abus de confiance", il encourt trois ans d'emprisonnement et 375.000 euros d'amende pour chacun de ces d�lits.
Jean-Michel Normand�est poursuivi pour "complicit� d'abus de faiblesse". L'ancien notaire a enregistr� plusieurs modifications du testament de Liliane Bettencourt ainsi que des dons, notamment au profit de Fran�ois-Maria Banier.
Certains de ces pr�venus ont d�j� restitu� une partie des lib�ralit�s re�ues. Fran�ois Maria-Banier a notamment renonc� � un contrat d'assurance-vie d'une valeur de 262 millions d'euros. St�phane Courbit a restitu� � la famille Bettencourt�les fonds investis dans son holding. Ces gestes n'arr�tent toutefois pas les poursuites: ils pourront seulement diminuer l'�ventuelle condamnation � payer des dommages et int�r�ts aux parties civiles, � savoir la fille et les deux petits-enfants�de Liliane Bettencourt.
Les juges
Le proc�s, dont la dur�e pr�vue est de cinq semaines, se tient devant les trois juges du Tribunal de grande instance de Bordeaux.
La juridiction bordelaise a �t� d�sign�e le 17 novembre 2010 par d�cision de la Cour de cassation dans l'int�r�t d'une bonne administration de la justice. Le transfert de l'ensemble des dossiers de l'affaire�de Nanterre, o� avait �t� d�pos�e la premi�re plainte, � Bordeaux�avait �t� demand� en raison des dissensions entre les magistrats saisis du dossier.
Les dates principales
2007: L'affaire commence,�sous la forme d'un conflit familial. Une plainte est d�pos�e � Nanterre par Fran�oise Bettencourt-Meyers, fille unique de l'h�riti�re du groupe de cosm�tiques L'Or�al, contre�le photographe Fran�ois-Marie Banier avec � l'appui des enregistrements clandestins r�alis�s au domicile de Liliane Bettencourt par le majordome, Pascal Bonnefoy.
2012: Eric Woerth est mis en examen et Patrice de Maistre en d�tention provisoire pendant trois mois.
2013: Le dossier prend un�aspect politico-financier, avec la mise en examen de Nicolas Sarkozy�pour "abus de faiblesse",�sur fond de soup�ons de financement de campagne �lectorale. L'ancien chef de l'�tat b�n�ficie la m�me ann�e�d'un non-lieu, bien que les juges bordelais pointent�son "comportement manifestement abusif". S'il aurait bien "sollicit� un soutien financier ill�gal", le parquet n'est en effet pas parvenu � d�montrer "de mises � disposition d'esp�ces".�Dix des onze autres mis en examen sont renvoy�s devant le tribunal correctionnel de Bordeaux.
Les autres proc�s
De nombreuses contre-plaintes pour d�nonciation calomnieuse ont �t� d�pos�es dans le cadre de l'affaire. Celles de Fran�ois Marie-Banier et de Patrick de Maistre�ont abouti � la�mise en examen en novembre 2014�de Claire Thibout, l'ex-comptable de Liliane Bettencourt, dans le cadre d'une proc�dure distincte�pour faux t�moignages. Cette derni�re, qui est le principal t�moin � charge par rapport aux accusations d'"abus de faiblesse", n'est donc plus � pr�sent tenue de jurer et de dire la v�rit�. Cette proc�dure risque donc d'�tre invoqu�e lors du d�but du proc�s � Bordeaux pour demander le�renvoi de ce dernier.
Un autre volet�"trafic d'influence" de l'affaire Bettencourt sera jug� � Bordeaux du 23 au 25 mars. Il y sera�discut� de la remise de la L�gion d'honneur par Eric Woerth � celui qui �tait �galement l'employeur de sa femme, Patrice de Maistre.
Un volet "violation du secret professionnel"�sera encore jug� dans la m�me ville�les 8 et 9 juin, o� compara�tra la juge d'instruction de Nanterre, Isabelle Pr�vost-Desprez.
Dans un autre volet pour "atteinte � la vie priv�e", l'ex-majordome Pascal Bonnefoy et cinq journalistes compara�tront � Bordeaux � une date encore ind�termin�e.
Les �coutes-pirates r�alis�es en 2009 et 2010 par le majordome Pascal Bonnefoy, en partie divulgu�es dans la presse, ont par ailleurs�mis�au jour des comptes en Suisse et des acquisitions de�la milliardaire�qui�lui ont valu�en 2011 un redressement fiscal de grande ampleur.
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Votre email ne sera pas affich� publiquement
Tous les champs sont obligatoires
Commentaires
aaa a �crit le 26/01/2015 � 18:59 :
C'est dingue le nombre de proche de Sarkozy qui tombent pour lui sauver les fesses et qu'ils y ait toujours autant de personne a prendre leur place...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
mqpd a �crit le 26/01/2015 � 18:03 :
vilains corbeaux m�me pas piti� d une  dames �g�e,  m�me si elle est riche ,
Signaler un contenu abusif
@ mqpd �a r�pondu le 26/01/2015                                                 � 19:00:
Malgr� ces pertes s�ches elle vis toujours aussi bien. Quand a ceux qui en ont le plus profit�, ils ne sont surement pas inqui�t�. Prot�g�s par des "fusibles"...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Le paysan a �crit le 26/01/2015 � 15:10 :
Pourquoi la justice n a pas donn�e une suite l�gale � la demande de la fille , permettant par ce fait que Mme Bettencourt soit abus�e en toute continuit� . Sa fille n �tant pas une d�linquante une suite favorable aurait due �tre signifi�e .
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
teddy19 a �crit le 26/01/2015 � 14:45 :
C'est une affaire de "principes" et de droit et surtout tr�s people.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
�
Merci pour votre commentaire. Il sera visible prochainement sous r�serve de validation.
�a le � :
