TITRE: Open d'Australie - Open d'Australie : Wawrinka tient son rang et affrontera Nishikori en quarts | francetv sport
DATE: 26-01-2015
URL: http://www.francetvsport.fr/tennis/open-d-australie/wawrinka-tient-son-rang-et-file-en-quarts-263159
PRINCIPAL: 1223997
TEXT:
Francetv sport Tennis Open d'Australie Open D'Australie : Wawrinka Tient Son Rang Et Affrontera Nishikori En Quarts
Open d'Australie
Open d'Australie : Wawrinka tient son rang et affrontera Nishikori en quarts
La joie de Stan Wawrinka (MAL FAIRCLOUGH / AFP)
Par Ga�tan Scherrer
Publi� le 26/01/2015 | 07:49, mis � jour le 26/01/2015 | 10:59
Tenant du titre, Stan Wawrinka a perdu son premier set du tournoi mais n'a pas flanch� lundi � Melbourne, venant � bout de Guillermo Garcia-Lopez en quatre manches tr�s accroch�es (7-6, 6-4, 4-6, 7-6) pour acc�der aux quarts de finale. Il y affrontera Kei Nishikori, tombeur exp�ditif de David Ferrer (6-3, 6-3, 6-3).
Wawrinka tient sa revanche. Face � celui qui l'avait surpris il y a huit mois, d�s son entr�e en lice � Roland-Garros, le Suisse a valid� son billet pour les quarts de finale � l'issue d'un combat long et difficile qui l'a oblig� � puiser dans ses ressources mentales.�
Wawrinka�sort 5 balles de set
Car�Guillermo Garcia-Lopez, 37e joueur mondial, a une nouvelle fois s�rieusement bouscul� le tenant du titre. D'entr�e, il a servi pour le gain du premier set (� 5-4) avant de le perdre au tie-break. Puis, dans la quatri�me manche, il a men� 5-0, puis 6-2 dans le tie break, et a oblig� son adversaire � sauver cinq balles de set! Heureusement pour lui, le Vaudois a su reprendre ses esprits pour s'imposer au terme d'un duel �reintant de plus de trois heures (3h02), gr�ce � quelques coups imparables sortis au meilleur des moments. Wawrinka n'a d�sormais plus perdu � l'Open d'Australie depuis le huiti�me finale m�morable de l'�dition 2013, face � Novak Djokovic (10-12 dans le 5e set).�
43 coups gagnants � 13 pour Nishikori
Pour son troisi�me quart de finale d'affil�e en Grand Chelem, Wawrinka retrouvera�Kei Nishikori, qui a �limin� avec autorit�l'Espagnol David Ferrer. En trois sets � sens unique (6-3, 6-3, 6-3), le Japonais a profit� de la fatigue de son adversaire, tombeur de Gilles Simon au tour pr�c�dent, pour le bombarder de coups gagnants (43 � 13!). "Stan the Man" sait qu'il devra se m�fier : l'�t� dernier, � l'US Open, Nishikori l'avait �limin� � ce m�me stade du tournoi au terme d'un match en cinq sets d'une rare intensit�.�
Raonic attend Djokovic
Ce lundi, Milos Raonic a lui aussi gagn� le droit de jouer les quarts de finale de l'Open d'Australie, une premi�re dans sa carri�re.�Le cogneur canadien a difficilement�domin� Feliciano Lopez, en cinq sets�(6-4, 4-6, 6-3, 6-7, 6-3) et�3h05 de jeu, et pourrait affronter au prochain tour Novak Djokovic, trois fois sacr� � Melbourne. Le Serbe devra pour cela battre le Luxembourgeois�Gilles Muller en 8e.�
