TITRE: CAN -                 CON -         Fran�ois Hollande a f�licit� Claude Le Roy
DATE: 26-01-2015
URL: http://www.lequipe.fr/Football/Actualites/Hollande-a-felicite-le-roy/531444
PRINCIPAL: 1226426
TEXT:
a+ a- imprimer RSS
Claude Le Roy a r�v�l� avoir re�u les f�licications du pr�sident de la R�publique, Fran�ois Hollande, pour la qualification du Congo en quarts de finale de la Coupe d'Afrique des nations . �Il m'a rappel� que j'�tais comme lui un ancien joueur du FC Rouen, a expliqu� le s�lectionneur fran�ais des Diables rouges, apr�s la victoire de son �quipe face au Burkina Faso (2-1), dimanche. C'�tait un exploit, une exp�rience tr�s �mouvante.� Lors de ses huit participations � la CAN � la t�te d'une s�lection africaine, Le Roy s'est qualifi� � sept reprises dans le Top 8 de la comp�tition.
