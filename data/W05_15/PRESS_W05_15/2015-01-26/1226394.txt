TITRE: Netflix est bien loin du succ�s en France...
DATE: 26-01-2015
URL: http://www.gizmodo.fr/2015/01/26/netflix-non-succes-france.html
PRINCIPAL: 1226390
TEXT:
50 123 3
Post navigation
Lorsque les premi�res rumeurs faisaient �tat d'une arriv�e dans l'hexagone du service de SVOD Netflix , tout le monde se r�jouissait. Il faut dire qu'avec un catalogue aussi fourni - aux �tats-Unis -, il y avait de quoi ! Pourtant, plusieurs mois apr�s son arriv�e en France , force est de constater que le succ�s tant attendu n'est pas l�.
Publicit�
Netflix avait mis� sur les box pour s�implanter en France� Si Bouygues Telecom, Orange puis SFR � via son d�codeur Google TV � l�ont accueilli � bras (presque) ouverts, Free et Numericable ont gard� porte close. De fait, d�apr�s l�institut Digital TV Research, Netflix en France n�aurait gagn� que 200 000 abonn�es � une goutte d�eau parmi les 54,5 millions d�adeptes au total, 17 millions tout de m�me hors des Etats-Unis -, dont la moiti� sur PC� sans passer par les FAI donc. Bien loin de l�enthousiasme extraordinaire attendu !
Le probl�me, en France, est plut�t simple � identifier : le catalogue. � cause de la chronologie des m�dias tant d�cri�e, le service ne peut proposer des films moins de 36 mois apr�s leur sortie en salle. M�me son de cloche, ou presque, concernant les s�ries TV. Alors pour �toffer le catalogue, Netflix pousse ses s�ries phare, comme Marco Polo � dont vous avez tr�s certainement vu les publicit�s � la t�l�vision � et se lance m�me dans la production de s�rie en France. ��Marseille�� sera diffus�e au cours de l�ann�e.
Cela suffira-t-il � v�ritablement lancer la machine ? Difficile � dire, le principal frein restant, encore et toujours, la chronologie des m�dias.
