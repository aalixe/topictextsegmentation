TITRE: VIDEO. Top Chef 2015: ce que r�serve la premi�re �mission - L'Express
DATE: 26-01-2015
URL: http://www.lexpress.fr/culture/tele/top-chef-2015-ce-que-reserve-la-premiere-emission_1644673.html
PRINCIPAL: 0
TEXT:
Par Ulla Majoube publi� le
26/01/2015 � 14:27
, mis � jour le 27/01/2015 � 16:26
C'est reparti pour une sixi�me saison de Top Chef! Nouveau jury, �preuves et pr�sentation retravaill�es, moins de candidats... Voici tout ce qui est pr�vu pour la premi�re, ce lundi 26 janvier.�
Zoom moins
0
� � � � �
St�phane Rotenberg et Philippe Etchebest discutent de l'�preuve des p�tes industrielles lors de la premi�re �mission de Top Chef 2015.
Pierre Olivier / M6
Top Chef reprend pour une sixi�me saison ce lundi 26 janvier. Or cette ann�e, la production et M6 changent la donne . Nouveau jury , nouvelles �preuves, �mission plus courte, plus de cuisine... Voici ce qui attend les t�l�spectateurs pour la premi�re �mission.�
Le nouveau jury et son r�le
En juillet tombait la nouvelle : exit Christian Constant , Thierry Marx , Ghislaine Arabian et Cyril Lignac . Philippe Etchebest , Michel Sarran et H�l�ne Darroze rejoignent Jean-Fran�ois Pi�ge . Et le changement ne s'arr�te pas l�. D�sormais, les chefs vont se faire entendre et voir en cuisine, avec les candidats. Les �preuves seront elles-m�mes comment�es par le chef "organisant" l'�preuve en voix off.�
�
Une soir�e en quatre temps
Preuve du changement, les �preuves sont d�sormais celles d'un chef. Ce lundi 26 janvier, les 15 candidats vont �tre divis�s en trois groupes. Honneur aux nouveaux, ce sont Philippe Etchebest, Michel Sarran et H�l�ne Darroze qui ont concoct� des �preuves pour entamer cette saison.�
Philippe Etchebest ouvre le bal: le groupe 1 doit r�aliser un plat spectaculaire avec des p�tes industrielles. Le groupe 2 se frotte � l'�preuve d'H�l�ne Darroze, � savoir une poire Belle-H�l�ne gastronomique. Enfin, Michel Sarran demande au groupe 3 de pr�parer, dans sa maison du Gers, une assiette gastronomique avec un poulet et des pommes de terre. Et oui, ces candidats vont cuisiner en plein air, dans l'herbe.�
Trois candidats seront �limin�s ce soir.�
Moins de candidats
Vous n'arriviez plus � vous en sortir l'an pass� entre les anciens et les nouveaux? Cette fois, il n'y a plus que 15 candidats, dont le jeune Xavier Koenig, qualifi� en remportant l'�mission Objectif Top Chef . On peut donc s'attendre � une �limination par soir avant d'atteindre la finale, fin avril.�
Le retour de l'�preuve "derni�re chance"
Tr�s critiqu�e l'an dernier pour son format ultra-r�duit, l'�preuve de la derni�re chance revient dans toute sa splendeur. C'est-�-dire non tronqu�e et avec la pression. �
Une �mission plus courte et plus ax�e "cuisine"
Enfin, la production a �cout� les suppliques des t�l�spectateurs. L'�mission dure maintenant 110 minutes. Donc pour un d�but � 21h, Top Chef devrait se terminer � 22h50. Mais que les couche-tard se rassurent: une �mission compl�mentaire de 40 minutes embo�te le pas avec recettes, assiettes et retour sur les anciens candidats. Autre complainte: montrer plus de cuisine. La production assure que c'est le cas dans cette saison. �
�
