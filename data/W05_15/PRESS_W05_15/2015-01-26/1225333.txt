TITRE: Une temp�te de neige paralyse New York | Brigitte DUSSEAU | �tats-Unis
DATE: 26-01-2015
URL: http://www.lapresse.ca/international/etats-unis/201501/25/01-4838295-tempete-historique-a-new-york-des-milliers-de-vols-annules.php
PRINCIPAL: 1225331
TEXT:
>�Une temp�te de neige paralyse New York�
Une temp�te de neige paralyse New York
Cette temp�te, qui touche une bande de plus de 450 km de longueur, allant de Philadelphie � l'�tat du Maine, devrait s'�loigner progressivement mercredi.
Photo: AP
Agence France-Presse
New York
Vols suspendus, spectacles annul�s, transports en commun arr�t�s au moins pour la nuit, New York tournait au ralenti lundi soir, frapp�e par une temp�te de neige accompagn�e de vents violents affectant des millions d'Am�ricains dans le nord-est des �tats-Unis.
>�Consultez le site d'A�roports de Montr�al
La neige tombait drue lundi soir � New York o� des centaines de chasse-neige �taient mobilis�s contre le blizzard. Le maire Bill de Blasio a appel� la population � sortir le moins possible, et a interdit la circulation de tous les v�hicules --sauf v�hicules d'urgence-- � partir de 23h00, une mesure rarissime.
Selon la m�t�o nationale, la neige pourrait tomber durant la nuit au rythme de 5 � 10 cm par heure, avec une accumulation attendue de 60 cm � New York, voire plus en certains endroits, et des bourrasques de vent de 72 � 88 km/heure.
�La temp�te va s'aggraver durant la nuit�, a annonc� en fin d'apr�s-midi le gouverneur de l'�tat de New York Andrew Cuomo. Il a annonc� la fermeture du m�tro, qui fonctionne � New York 24 heures sur 24, � partir de 23h00, pour une dur�e ind�termin�e.
M. Cuomo a �galement annonc� que les trains de banlieue s'arr�teraient aussi � 23h00, l� encore pour une dur�e ind�termin�e, et a �tendu � tout l'�tat de New York l'interdiction de circuler, sauf pour les v�hicules d'urgence.
Les parcs new-yorkais ont ferm� � 18h00. Les �coles resteront closes mardi, a annonc� �galement le maire. Le si�ge de l'ONU n'ouvrira pas non plus.
Les spectacles de la c�l�bre Broadway ont �t� annul�s lundi soir. Mais la Bourse sera ouverte mardi.
�Ce n'est pas une temp�te typique�, a insist� le maire, �rentrez t�t chez vous�, a-t-il ajout�, r�p�tant que le blizzard devrait �tre �l'un des plus importants de l'histoire de la ville� de 8,4 millions d'habitants.
Cette temp�te, qui touche une bande de plus de 450 km de longueur, allant de Philadelphie � l'�tat du Maine, devrait s'�loigner progressivement mercredi.
Prenant leur maire au mot, beaucoup de New-Yorkais ont quitt� t�t le travail. En milieu d'apr�s-midi, de nombreux embouteillages congestionnaient les rues de la plus grande ville am�ricaine.
�Les New-Yorkais doivent se pr�parer � de possibles pannes d'�lectricit�, dues aux vents violents qui pourraient faire tomber les lignes et les arbres�, a aussi averti le gouverneur.
Il a pr�cis� que l'�tat de New York disposait d'au moins 1806 chasse-neige et de plus de 126 000 tonnes de sel pour lutter contre le mauvais temps. Tous les bus urbains devaient selon lui �tre �quip�s de pneus neige ou de cha�nes.
�coles ferm�es mardi
Les New-Yorkais inquiets avaient d�s dimanche d�valis� les supermarch�s pour faire des provisions. Beaucoup ont fait de m�me lundi, patientant parfois dans le froid avant de pouvoir entrer, pour faire des r�serves de pain, lait, fruits et l�gumes.
Dans la r�gion de Boston, o� de 50 � 76 cm de neige sont attendus, les bourrasques de vent pourraient d�passer 100 km/heure.
Les �coles y seront aussi ferm�es mardi, selon les autorit�s et une interdiction de circuler � partir de lundi soir a �t� impos�e dans tout l'�tat du Massachusetts, comme dans l'�tat proche du Connecticut.
La s�lection du jury dans le proc�s de Djokhar Tsarnaev, accus� des attentats de Boston en 2013, a �galement �t� suspendue, le tribunal annon�ant qu'il resterait ferm� mardi.
Le manque de visibilit� associ� � la neige et au vent rend la circulation particuli�rement dangereuse, ont insist� les autorit�s dans tous les �tats touch�s par la temp�te, dont plusieurs ont d�clar� l'�tat d'urgence.
�De nombreuses routes seront impraticables�, a soulign� la m�t�o nationale. Les d�placements seront �extr�mement dangereux en raison des fortes chutes de neige et des vents violents�.
Lundi, plus de 2700 vols ont �t� annul�s de ou vers les �tats-Unis, et plus de 3700 l'�taient d�j� pour mardi, selon le site sp�cialis� Flightaware.com . Selon les autorit�s, la moiti� des vols ont notamment �t� annul�s � l'a�roport international John F. Kennedy � New York.
La compagnie United a annonc� qu'elle avait l'intention d'annuler tous ses vols mardi dans les trois a�roports new-yorkais de Newark, LaGuardia et JFK, ainsi qu'� Boston et Philadelphie.
Partager
