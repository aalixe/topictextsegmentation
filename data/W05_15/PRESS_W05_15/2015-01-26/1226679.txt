TITRE: Bourse New York: Wall Street en baisse apr�s le scrutin grec
DATE: 26-01-2015
URL: http://www.romandie.com/news/Bourse-New-York-Wall-Street-en-baisse-apres-le-scrutin-grec_RP/559054.rom
PRINCIPAL: 1226678
TEXT:
Tweet
Bourse New York: Wall Street en baisse apr�s le scrutin grec
New York (awp/afp) - Wall Street a ouvert en baisse lundi, accueillant sans inqui�tude d�mesur�e la victoire de la gauche radicale en Gr�ce, dans un calendrier pauvre en indicateurs �conomiques: le Dow Jones perdait 0,31% et le Nasdaq 0,25%.
Vers 15H05 GMT/16h05 HEC, l'indice vedette Dow Jones Industrial Average c�dait 54,81 points � 17'617,19 points, et le Nasdaq, a dominante technologique, 11,71 points � 4746,17 points.
Particuli�rement suivi par les investisseurs professionnels, l'indice �largi S&P 500 reculait de 0,28%, soit 5,65 points, � 2046,17 points.
Vendredi, la Bourse de New York avait fini en ordre dispers�, soufflant au lendemain de l'annonce de mesures de relance par la Banque centrale europ�enne (BCE): le Dow Jones avait perdu 0,79%, � 17'672,60 points, mais le Nasdaq avait pris 0,16%, 4757,88 points.
D�sormais, "les investisseurs p�sent les cons�quences de la victoire de Syriza, parti anti-aust�rit�, en Gr�ce", ont rapport� les experts de Wells Fargo.
Le chef de file de Syriza, Alexis Tsipras, a pr�t� serment comme Premier ministre apr�s la victoire de son parti, qui �tait annonc�e par les sondages mais dont l'ampleur, proche de la majorit� absolue, a d�pass� les attentes.
Plusieurs responsables europ�ens ont affich� leur volont� de ne pas passer sous les fourches caudines d'Alexis Tsipras en le pr�venant que l'Union europ�enne (UE) n'�tait pas pr�te � effacer sa dette, �galement contract�e aupr�s du Fonds mon�taire international (FMI) et de la BCE, qui �quivaut au total � 177% du produit int�rieur brut (PIB) grec.
Traditionnel lieu de refuge en cas de nervosit� des investisseurs, le march� obligataire ne semblait pas b�n�ficier de la situation. Le rendement des bons � dix ans montait � 1,805%, contre 1,790% vendredi soir, et celui des bons du Tr�sor � 30 ans � 2,378, contre 2,364% pr�c�demment.
A Wall Street, l'inqui�tude n�e du scrutin grec "est compens�e par un bon indicateur sur la confiance des entreprises allemandes", ont temp�r� les analystes de la maison de courtage Charles Schwab.
Le barom�tre Ifo du moral des entrepreneurs allemands, indicateur de confiance tr�s suivi, a atteint son plus haut niveau depuis six mois en janvier, de bon augure pour l'�conomie du pays en d�but d'ann�e.
S�RIE DE FUSIONS
Dans l'ensemble, le march� attend surtout une nouvelle s�rie de r�sultats, dont ceux de Microsoft (-0,87% � 46,77 dollars) � la cl�ture, et l'issue de la r�union de politique mon�taire de la R�serve f�d�rale (Fed), mercredi.
Par ailleurs, "plusieurs annonces de fusions et acquisitions" animent le march�, ont comment� les analystes de Charles Schwab.
Ainsi, le r�assureur PartnerRe et son concurrent AXIS Capital Holdings, bas� aux Bermudes, ont annonc� leur fusion, disant ainsi vouloir augmenter leur �chelle et leur pr�sence sur le march�. PartnerRe c�dait 1,59% � 112,33 dollars, tandis qu'AXIS Capital prenait 2,43% � 50,53 dollars, sur sa cotation sur le New York Stock Exchange (NYSE).
Le sp�cialiste de l'emballage MeadWestvaco (MWV) s'envolait de 17,18% � 52,78 dollars apr�s avoir fait part de son intention de fusionner avec son concurrent RockTenn, qui bondissait aussi, de 9,16% � 68,76 dollars.
Dans les c�r�ales, Post Holdings gagnait 7,31% � 44,47 dollars apr�s l'annonce du rachat de son compatriote MOM Brands, qui n'est pas cot� en Bourse.
Enfin, le g�ant des t�l�coms AT&T, qui a annonc� sa deuxi�me acquisition en trois mois au Mexique avec l'achat de Nextel Mexico, perdait 0,75% � 33,12 dollars.
Dans un secteur immobilier qui reste tr�s surveill�, le promoteur DR Horton prenait 5,15% � 24,29 dollars, apr�s la publication d'un b�n�fice net en hausse de 16% au titre du dernier trimestre 2014.
Le conglom�rat industriel Roper avan�ait de 4,54% � 156,50 dollars, apr�s avoir fait part d'une hausse de 12% de ses b�n�fices trimestriels, jug�e plus importante que pr�vu.
Generac, sp�cialiste des groupes �lectrog�nes de secours, prenait 5,70% � 48,61 dollars, � l'approche d'un blizzard sur le nord-est des Etats-Unis, notamment � New York.
Les compagnies a�riennes ne p�tissaient, elles, pas outre mesure de la situation m�t�orologiques, malgr� l'annulation de plus de 2.500 vols pour lundi et d�j� pr�s de 3.000 pour mardi: United Airlines gagnait 0,97% � 73,81 dollars, American Airlines et Delta Air Lines restant dans la tendance. Elles c�daient respectivement 0,31% � 55,52 dollars et 0,32% � 50,40 dollars.
afp/rp
