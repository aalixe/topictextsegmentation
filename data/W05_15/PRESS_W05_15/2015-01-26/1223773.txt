TITRE: Dix personnes jug�es pour avoir abus� de la faiblesse de Liliane Bettencourt - 20minutes.fr
DATE: 26-01-2015
URL: http://www.20minutes.fr/societe/1524111-20150126-dix-personnes-jugees-avoir-abuse-faiblesse-liliane-bettencourt
PRINCIPAL: 0
TEXT:
Soci�t�
Soci�t�
JUSTICE Dix personnes, dont l�ancien ministre UMP Eric Woerth, comparaissent devant le tribunal de grande instance de Bordeaux d�s ce lundi�
Dix personnes jug�es pour avoir abus� de la faiblesse de Liliane Bettencourt
Envoyer
La d�fense de plusieurs protagonistes de l'affaire Bettencourt conteste mardi la validation de l'enqu�te dans le volet abus de faiblesse du dossier devant la Cour de cassation, qui se penchera par ailleurs sur les agendas de Nicolas Sarkozy. - Francois Guillot AFP
Vincent Vantighem
justice
�A l��poque, c��tait le d�fil�, se souvient un proche de Liliane Bettencourt . C��tait � qui voulait son rendez-vous pour pr�senter sa demande et repartir avec son enveloppe�� Les largesses de l�h�riti�re de L�Or�al vont �tre au c�ur du proc�s qui s�ouvre, ce lundi, au tribunal de grande instance de Bordeaux (Gironde).
D�tentrice de la douzi�me fortune mondiale estim�e � pr�s de 30 milliards d�euros selon le magazine am�ricain Forbes , Liliane Bettencourt a toujours fait preuve de philanthropie. Mais les juges ont d�sormais jusqu�au 27 f�vrier pour d�terminer si certains n�ont pas profit� de l��tat de faiblesse de cette dame , une grande bourgeoise raffin�e parvenue au cr�puscule de sa vie, pour s�enrichir.
Un l�ger �tat de s�nilit� depuis 2006
Nicolas Sarkozy ayant finalement b�n�fici� d�un non-lieu dans cette affaire tentaculaire (lire l�encadr�), ils seront donc dix � prendre place, ce lundi, sur le banc des pr�venus. Le photographe Fran�ois-Marie Banier et son compagnon Martin d�Orgeval, soup�onn�s d�avoir touch� des centaines de milliers d�euros sous forme de dons et d��uvres d�art. Des notaires. L�ancien infirmier de l�h�riti�re de l�empire de cosm�tiques aussi. Et surtout l�ancien ministre UMP Eric Woerth , renvoy�, lui, pour le recel d�une somme d�argent que lui aurait remise celui qui �tait alors gestionnaire de fortune de la milliardaire.
A Lire aussi >> Portraits:�D�couvrez tous les pr�venus de l'affaire Bettencourt
�Liliane Bettencourt a toujours pr�t� beaucoup d�attention aux autres, poursuit celui qui �tait proche d�elle. Je me souviens qu�une jeune fille lui avait �crit pour lui dire qu�elle aimerait faire du tennis mais n�en avait pas les moyens. Liliane avait d�cid� de lui payer ses le�ons�� Si la g�n�rosit� n�est pas remise en cause, c�est surtout l��tat de sant� de la milliardaire qui sera au centre des d�bats .
En 2011, Liliane Bettencourt avait enfin d�cid� de se soumettre � un examen m�dical qui avait diagnostiqu� un l�ger ��tat de s�nilit� depuis 2006. Les r�sultats de cette expertise devraient �tre �prement d�battus lors de l�audience, car c�est elle qui donne corps � ce que la justice qualifie aujourd�hui �d�abus de faiblesse� .
Le t�moin cl� mis en examen pour �faux t�moignage�
Encore faut-il que le proc�s se tienne. A l�issue de six ann�es d�une proc�dure d�pays�e � Bordeaux pour la s�r�nit� de l�enqu�te, le dossier a connu un ultime rebondissement en novembre 2014. Consid�r�e comme un t�moin cl� de toute cette affaire, Claire Thibout, comptable de Liliane Bettencourt de 1995 � 2008, a �t� mise en examen pour �faux t�moignages� .
A lire aussi >> Proc�dure:�Un risque pour la tenue du proc�s Bettencourt
L�audience de ce lundi devrait donc s�ouvrir sur une v�ritable bataille juridique au cours de laquelle les avocats des diff�rents pr�venus vont tenter de remettre en cause toute la proc�dure. Si l�audience se poursuit, le proc�s est pr�vu pour durer jusqu�au 27 f�vrier.
Trois autres proc�s auront lieu dans l�affaire Bettencourt
