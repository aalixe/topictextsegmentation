TITRE: La Gr�ce et ses h�ros de la libert� - L'Express
DATE: 26-01-2015
URL: http://www.lexpress.fr/actualite/la-grece-et-ses-heros-de-la-liberte_1644755.html
PRINCIPAL: 1225506
TEXT:
La r�daction a mis en Une cet article et ses infos sont v�rifi�es.
Express Yourself
Par Daniel Salvatore Schiffer (Express Yourself) publi� le
26/01/2015 � 15:55
, mis � jour le 27/01/2015 � 09:51
Notre contributeur revient sur la victoire de Syriza, estimant qe l'Union europ�enne est devenue un "monstre technocratique et bureaucratique des temps modernes".�
Zoom moins
1
� � � � �
Le pr�sident de Syriza, Alexis Tsipras lors d'un meeting de campagne dans le centre d'Ath�nes, le 22 janvier 2015, � trois jours de l'�lection.
afp.com/Orestis Panagiotou
Ce dimanche 25 janvier 2015 restera sans nul doute, pour l'Europe comme pour la Gr�ce, une date historique: la Gr�ce, berceau culturel de la civilisation occidentale, pays qui inventa l'admirable notion de "d�mocratie" et o�, � lire la "Th�ogonie d'H�siode, naquit le mot m�me d'Europe, a d�cid�, par un vote libre et r�fl�chi, de r�sister, via Syriza, parti qui vient de remporter les �lections l�gislatives, � l'emprise de l'Union europ�enne, cette entit� qui, parce qu'elle a trahi son id�al politique au b�n�fice des lois �conomiques, est devenu, malheureusement pour les peuples qui la constituent, un sorte de monstre, technocratique et bureaucratique � la fois, des temps modernes.�
Au nom de la d�mocratie, contre le terrorisme
Entendons-nous: je suis un fervent partisan, et de la premi�re heure, de cette belle id�e qu'est la construction de l'Europe, dont je partage, bien s�r, les valeurs morales, au premier rang desquelles �mergent la tol�rance et la fraternit�, la concorde entre les peuples et le respect de la diff�rence, le pluralisme confessionnel et le pacifisme multiculturel. Bref: ce que l'on appelle, commun�ment, les droits de l'homme, sans lesquels il n'est point d'humanisme, pr�cis�ment, qui vaille!�
Mais il se fait, �galement, que cette m�me Europe, celle que tout homme comme toute femme de bonne volont� appellent l�gitimement de ses voeux, ressemble de plus en plus ces derniers temps, et surtout depuis qu'elle applique aveugl�ment une absurde politique d'aust�rit� � l'encontre des ses propres populations, � ce L�viathan que stigmatisait d�j� un philosophe tel que Thomas Hobbes: la dangereuse matrice d'un totalitarisme qui s'ignore ou, pis, qui ne dit pas, hypocritement, son nom!�
C'est contre ce type in�dit d'autoritarisme, o� le diktat financier prend trop souvent le pas sur la concertation sociale, qu'Alexis Tsipras, pr�sident de Syriza et nouveau Premier Ministre grec, s'est insurg� � juste titre, ce matin m�me, en clamant haut et fort, devant une foule en liesse, que cette historique victoire de son parti mettait fin, d�sormais, � ces trop longues et p�nibles ann�es o� son pays s'est vu contraint de subir, face � une Tro�ka qui avait l'impudence de venir faire la loi chez lui, les pires vexations et les pires sacrifices, aussi inutiles que contre-productifs de surcro�t.�
Non: on n'humilie pas un peuple impun�ment, et surtout pas les Grecs, pour qui la dignit�, ce savant dosage de fiert� individuelle et d'orgueil national, fait partie int�grante, d'antique m�moire, de son immense patrimoine culturel.�
Solidarit� avec le peuple grec
Mon admiration pour la Gr�ce, le modeste philosophe que je suis l'avait d�j� exprim�e tr�s clairement, du reste, il y a un an et demi d�j�, dans une tribune publi�e, le 15 juin 2013, sur un autre site d'information . �
Aussi, � l'heure o� je vois avec plaisir ce merveilleux flambeau de la libert�, porteur d'espoir et d'avenir tout � la fois, se rallumer � nouveau dans le ciel d'Ath�nes, est-il naturel que je r�it�re, mais plus actuels que jamais, mes mots d'alors: c'est notre solidarit� la plus totale, en plus de notre reconnaissance la plus sinc�re, que m�rite, aujourd'hui plus que jamais, le courageux peuple grec, dont les prestigieux anc�tres ont pour glorieux nom - supr�me h�ritage spirituel, mais aussi scientifique - Hom�re, H�rodote, H�siode, Pythagore, Euclide, Thal�s, Emp�docle, Parm�nide, D�mocrite, H�raclite, Thucydide, Socrate, Platon, Aristote, �picure, Hippocrate, Sophocle, Euripide, Eschyle, Aristophane, et tant d'autres.�
Car que serions-nous en effet, et que serait l'Europe m�me, sans ce miracle philosophique, unique dans les annales de l'humanit�, que fut alors, depuis le P�loponn�se jusqu'� la Cr�te, en passant bien s�r par Ath�nes, la Gr�ce, m�re nourrici�re, sur le plan des id�es, de l'invention scientifique comme de la cr�ation artistique, de l'Occident?�
A cette grande et belle Gr�ce, nous devons, sinon tout, du moins la meilleure partie de nous-m�mes: notre propre civilisation, donc! La moindre des choses, c'est que nous lui exprimions donc �galement aujourd'hui, � l'heure o� elle se l�ve contre le totalitarisme politico-�conomique, notre gratitude la plus authentique, en plus de notre ind�fectible soutien.�
Lord Byron et son amour pour le pays de la mort honorable
Je me souviens d'ailleurs, � ce propos, de ce que disait d�j� d'elle, � l'aube du XIXe si�cle, Lord Byron en personne, cet audacieux et flamboyant dandy qui s'en alla combattre et mourir, au nom de la libert� justement, sur les terres ensanglant�es de Missolonghi, alors sous l'implacable et cruel joug de l'Empire Ottoman. A ces Grecs d'aujourd'hui, je livre donc ces splendides strophes LXXIII du chant II de son imp�rissable, �pique mais po�tique Childe Harold :�
Belle Gr�ce ! Triste relique d'une grandeur disparue !�
Immortelle, bien que tu ne sois plus ; et, bien que tu sois tomb�e, grande.�
Qui va se mettre � la t�te de tes enfants dispers�s ?�
Qui te d�livrera d'un esclavage auquel tu n'es que trop habitu�e?�
Ils �taient bien diff�rents, tes fils, qui autrefois�
Guerriers sans espoir, acceptant leur destin,�
Attendaient dans le d�fil� s�pulcral des Thermopyles d�sol�es.�
Oh ! Qui retrouvera cet esprit h�ro�que?�
Qui sautera les bancs de l'Eurotas et te r�veillera dans ta tombe?�
H�ro�sme et libert�
Car, pour cet id�aliste-n� qu'�tait Byron, pour cet �tre qui ne pouvait  vivre sans une cause noble � d�fendre, la Gr�ce repr�sentait plus profond�ment aussi, depuis le si�cle d'or de P�ricl�s, et depuis donc l'av�nement de la d�mocratie elle-m�me, le berceau historique et culturel de cette civilisation � laquelle il �tait fier, nonobstant les nombreuses et cinglantes critiques qu'il n'avait cesser de lui ass�ner, d'appartenir.�
Davantage: ce lord r�va toujours, depuis sa plus tendre enfance, de faire partie int�grante de cette cat�gorie d'hommes r�put�s sup�rieurs qu'�taient, � ses yeux, les h�ros de l'antiquit� grecque! Ainsi, �crivit-il � l'un des artisans de l'insurrection hell�ne, le 7 f�vrier 1824, alors qu'il se trouvait d�j� � Missolonghi, o� il allait bient�t, un peu plus de deux mois plus tard, mourir: "La Gr�ce a toujours �t� pour moi ce qu'elle doit �tre pour tous ceux qui ont du sentiment ou de l'instruction: le terre promise de la vaillance, des arts et de la libert�. Le temps que j'ai pass� dans ma jeunesse � voyager parmi ses ruines n'a nullement refroidi l'affection que je porte � la patrie des h�ros." La Gr�ce, ou le pays, pour ce po�te romantique, r�volt� et insoumis, de la mort honorable!�
Ainsi, nul doute, effectivement, que Byron, pour qui la libert� - f�t-elle de pens�e, d'action ou de parole - repr�sentait le bien supr�me en m�me temps qu'un id�al absolu au sein de son �chelle de valeurs, aurait pu faire sienne, en cette tr�s embl�matique circonstance, cette fameuse sentence de Baudelaire d�finissant, dans LePeintre de la vie moderne, le dandysme: "Le dandysme est le dernier �clat d'h�ro�sme dans les d�cadences."�
Vive donc la Gr�ce et ses h�ros, qu'ils soient pr�sents ou pass�s, morts ou vivants, de la libert�!�
DANIEL SALVATORE SCHIFFER, philosophe, auteur de Lord Byron (Gallimard - Folio Biographies)�
�
