TITRE: Renault Kadjar: le nouveau SUV de Renault
DATE: 26-01-2015
URL: http://www.auto-actualite.eu/renault-kadjar-nouveau-suv-renault-138802015.html
PRINCIPAL: 1224589
TEXT:
29 janvier 2015 Par Charles Levault
Annonc� en grande pompe par�Carlos Ghosn pour juin 2015, le nouveau SUV crossover s�appellera�Renault Kadjar. Objectif avou� : concurrencer le Nissan�Qashqai avec un mod�le r�pondant aux crit�res esth�tiques de la Renault Captur associ�s aux volumes d�un SUV.
Face � l�impopularit� du Renault Koleos, la firme au losange se devait d�apporter une r�ponse � la hauteur du succ�s de la Renault Captur. Inspir�s par les r�actions du public vis-�-vis du�Nissan�Qashqai, Renault souhaite combiner dans ce�Renault Kadjar l�ensemble des �l�ments cl�s de la r�ussite des mod�les phares du march�.
Pourquoi�Renault Kadjar ?
��Kadjar�est un nom masculin construit autour de KAD- et �JAR. KAD- s�inspire directement de � quad �, v�hicule � quatre roues tout-chemin alors que �JAR rappelle � la fois les mots � agile � et � jaillir �. La sonorit� et l�orthographe de Kadjar�sont teint�es d�exotisme, invitant ainsi l�aventure et la d�couverte de nouveaux horizons.��
�
