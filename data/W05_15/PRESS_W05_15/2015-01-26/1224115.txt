TITRE: Bourse de Paris. En légère baisse à l'ouverture (-0,17%) - Economie - Le Télégramme, quotidien de la Bretagne
DATE: 26-01-2015
URL: http://www.letelegramme.fr/economie/bourse-de-paris-en-legere-baisse-a-l-ouverture-0-17-26-01-2015-10505468.php
PRINCIPAL: 1224110
TEXT:
Bourse de Paris. En légère baisse à l'ouverture (-0,17%)
26 janvier 2015 à 09h49
/ AFP /
La Bourse de Paris était en légère baisse ce lundi matin (- 0,17 %), après la victoire de la gauche anti-austérité en Grèce qui souhaite renégocier la dette du pays. A 9 h 20, l'indice Cac 40 perdait 7,75 points à 4.632,94 points. Le marché parisien reste sur une série de sept séances de hausse consécutives, dont une progression de 1,93 % vendredi, porté par la politique monétaire très généreuse de la Banque centrale européenne (BCE).
