TITRE: www.larep.fr - Multim�dia - Techno - Microsoft donne des d�tails sur Windows 10, propos� gratuitement pendant un an
DATE: 26-01-2015
URL: http://www.larep.fr/france-monde/actualites/societe/techno/2014/10/08/microsoft-donne-des-details-sur-windows-10-propose-gratuitement-pendant-un-an_11299754.html
PRINCIPAL: 0
TEXT:
(cliquez sur le code s'il n'est pas lisible)
Tous les articles
CyanogenMod d�fie Android et iOS
Lu 57 fois
(Relaxnews) - L'�diteur Cyanogen, � l'origine du syst�me d'exploitation pour mobile CyanogenMod enti�rement bas� sur Android, vient de lever 80 millions de dollars et ambitionne de devenir le troisi�me acteur du march� derri�re Android et iOS.Depuis 2009, la start-up am�ricaine propose...
Les membres de Facebook sont plus accros et actifs que ceux de Twitter ou Google+
Lu 49 fois
(Relaxnews) - Fin 2014, 7 internautes sur 10 avouaient �tre actifs sur les r�seaux sociaux, selon une �tude publi�e par GlobalWebIndex. C'est surtout sur Facebook qu'ils aiment revenir plusieurs fois par jour pour "aimer" une publication ou lire un article.Si 73% des personnes interrog�es...
Une deuxi�me g�n�ration de Google Glass devrait voir le jour
Lu 21 fois
(Relaxnews) - Mis en suspens en janvier 2015, le programme Google Glass n'est pas pour autant abandonn� selon Eric Schmidt, le pr�sident du conseil d'administration de Google, qui s'exprime dans le Wall Street Journal. Apr�s un premier prototype relativement d�cevant, l'objectif demeure...
Instagram lance l'application de photomontage Layout
Lu 3 fois
(Relaxnews) - Le r�seau social qui permet de partager ses photos sur smartphone propose dor�navant � ses utilisateurs de cr�er des mosa�ques de photos au sein d'une seule image.Apr�s avoir lanc� la fonctionnalit� de capture vid�o Hyperlapse en 2014, Instagram vient de d�voiler Layout,...
Des services de Microsoft vont �tre pr�-install�s sur des appareils mobiles de Samsung
Lu 69 fois
(AFP) - Le g�ant am�ricain des logiciels Microsoft a annonc� lundi que plusieurs de ses services allaient �tre pr�-install�s sur des appareils mobiles du fabricant sud-cor�en Samsung.Ce dernier a d�j� pr�vu d'int�grer plusieurs services Microsoft dans son nouveau smartphone S6, pr�sent�...
Vers la fin de l'offre gratuite illimit�e des services de streaming musicaux
Lu 249 fois
(Relaxnews) - Les principales maisons de disques se seraient mises d'accord pour davantage de restrictions concernant l'�coute musicale gratuite de leurs catalogues sur des plates-formes comme Spotify ou Deezer, selon le site du magazine Rolling Stone.Universal serait � la t�te des...
Meerkat : le nouveau ph�nom�ne du live streaming via Twitter
Lu 467 fois
(Relaxnews) - L'application Meerkat, lanc�e en f�vrier 2015 et qui permet de diffuser un flux vid�o en direct depuis un lien automatiquement envoy� sur Twitter, a fait sensation lors du dernier festival SXSW d'Austin (�tats-Unis).Disponible uniquement sous iOS pour l'instant, l'application...
100 Abribus � �cran tactile arrivent � Paris
Lu 74 fois
(Relaxnews) - La Ville de Paris et le groupe JCDecaux inaugurent lundi 23 mars 2015 leur premier Abribus "intelligent" � �cran tactile dans la quartier Bastille. Une centaine d'Abribus de ce type devrait �tre prochainement d�ploy�s sur Paris � l'occasion du renouvellement de l'ensemble...
