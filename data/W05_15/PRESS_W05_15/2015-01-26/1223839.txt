TITRE: VID�O - Ibrahimovic humilie un St�phanois en lui demandant son nom
DATE: 26-01-2015
URL: http://www.europe1.fr/sport/football/articles/video-ibrahimovic-se-moque-d-un-stepahnois-2353709
PRINCIPAL: 1223837
TEXT:
VID�O - Ibrahimovic humilie un St�phanois en lui demandant son nom
Europe 1Victor Dhollande-Monnier
Publi� � 07h36, le 26         janvier  2015
, Modifi� � 19h29, le 26         janvier  2015
Dossiers :
saint etienne
Zlatan Ibrahimovic vient juste d'essuyer ses crampons sur la jambe de Romain Hamouma. Les St�phanois lui demandent de se calmer. Ibra ne se d�monte pas et en profite pour regarder le nom du St�phanois au dos du maillot de Paul Baysse.  � Capture d'�cran Youtube
Par Victor Dhollande-Monnier
0
2
T'ES QUI TOI ? - Dimanche soir, Zlatan Ibrahimovic a agac� le St�phanois Paul Baysse juste apr�s avoir commis une grosse faute.
�a n'a pas �t� facile, ni brillant, mais en battant les Verts � Geoffroy-Guichard (1-0) , les Parisiens ont �vit� de se laisser distancer par Lyon et ont rejoint � la deuxi�me place Marseille avec 44 points. Dans un match tendu et ferm�, Zlatan Ibrahimovic a transform� un penalty � l'heure de jeu. En petite forme depuis plusieurs semaines, l'attaquant su�dois a r�alis� un meilleur match et a prouv� qu'il faudrait compter sur lui pour la deuxi�me partie de saison.
>> LIRE AUSSI - Le PSG et Zlatan sont de retour
Une grosse faute et une moquerie. Voir Zlatan chambrer un joueur adverse est un signe qui ne trompe pas. Il est bien de retour et il entend bien (r�)affirmer son statut de star de la Ligue 1. Dans les arr�ts de jeu, apr�s avoir essuy� ses crampons sur le pauvre Romain Hamouma, il s'est expliqu� avec d'autres joueurs st�phanois. Il a alors commenc� � se moquer du d�fenseur Paul Baysse avant de chercher son nom au dos de son maillot. Traduction du geste : "qui es-tu, toi, pour me parler ?" Un geste humiliant pour Paul Baysse qui s'est empress� de faire la m�me chose.
"C'est un chambreur". "Il a commenc�, c'est un chambreur et �a fait partie du jeu", a r�agi le d�fenseur st�phanois Paul Baysse avant de critiquer gentiment Ibrahimovic. "Mais c'est plus facile pour lui de chambrer quand il gagne. Il ne se serait pas amus� � faire �a en perdant 1-0. Je ne me laisse pas faire mais je ne pr�te pas attention � �a".
Un carton rouge oubli� ? Sur cette action, Ibrahimovic aurait certainement m�rit� de prendre un carton rouge. "Zlatan arrive tr�s en retard et il met clairement une semelle sur le tibia du St�phanois", analyse l'ancien arbitre fran�ais Bruno Derrien dans les colonnes du journal L'Equipe . "Il s'en tire bien, d'autant que l'action se d�roule sous les yeux de l'arbitre assistant".
Tous les articles sport
