TITRE: Lucas, d�j� � 100 - Football - Sports.fr
DATE: 26-01-2015
URL: http://www.sports.fr/football/ligue-1/articles/lucas-deja-a-100-1174562/
PRINCIPAL: 0
TEXT:
26 janvier 2015 � 17h31
Mis à jour le
26 janvier 2015 � 18h06
Seulement âgé de 22 ans et sous contrat encore pour deux ans et demi avec le PSG, le Brésilien Lucas Moura vient d'atteindre la barre des 100 apparitions toutes compétitions confondues avec le club de la capitale contre Saint-Etienne, dimanche, dans l'élite. 
Il a rejoint Ricardo, Valdo , Raï, Ceara , Nenê , Thiago Motta et Maxwell qui avaient atteint cette barre symbolique avant lui. Contre Saint-Etienne (0-1), dimanche soir, en clôture de la 22e journée de Ligue 1, Lucas Rodrigues Moura da Silva, dit Lucas , a atteint les 100 apparitions toutes compétitions confondues avec le Paris Saint-Germain.
A 22 ans, l'attaquant brésilien s'inscrit donc déjà dans l'histoire du club de la capitale et pourrait à n'en pas douter faire grimper ce total dans les prochaines années, lui qui a encore deux ans et demi de contrat à honorer avec les Rouge et Bleu. Avec 215 rencontres officielles disputées avec le PSG , Raï est très loin devant et n'a pas trop à s'en faire pour son record, tout comme Ceara, son poursuivant direct (197). En revanche, Nenê, 112 matches disputés, Maxwell (117), Valdo (153) et Ricardo (155) pourraient bientôt faire les frais de leur compatriote.
Sur un plan personnel, l'Auriverde a lui franchi un palier depuis son arrivée et son premier match joué sous le maillot parisien contre Ajaccio (0-0), le 11 janvier 2013. Arrivé au mercato d'hiver, Lucas avait d'abord participé à 15 rencontres toutes compétitions confondues pour ses premiers pas dans la capitale pour un bilan peu flatteur de 6 passes décisives. Mais depuis, sa progression est manifeste : 53 matches pour 5 buts et 15 passes décisives pour la saison 2013-2014 et 32 rencontres pour 9 buts et 3 passes décisives pour l'exercice en cours. 
Son palmarès commence lui aussi à prendre une autre envergure avec deux titres de champions de France conquis, deux trophées des champions et une Coupe de la Ligue. En attendant peut-être un jour une Coupe de France et une Ligue des champions pour compléter son tableau de chasse. Et pourquoi pas cette saison ?
