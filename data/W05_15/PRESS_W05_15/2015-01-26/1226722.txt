TITRE: Ligue 2 : Brest s'impose � Angers et se retrouve seul deuxi�me. Sport Ligue 2 - Redon.maville.com
DATE: 26-01-2015
URL: http://www.redon.maville.com/sport/detail_-ligue-2-brest-s-impose-a-angers-et-se-retrouve-seul-deuxieme_54135-2703692_actu.Htm
PRINCIPAL: 1226720
TEXT:
Twitter
Google +
Brest ont ouvert le score � la 15e minute par Belghazouani., et s'impose 2-1 � Angers pour ce match qui cl�t la 21e journ�e de Ligue 2.� Photo : Franck Dubray / Ouest-France
Brest s'est impos� (2-1) � Angers, lundi soir, pour la fin de la 21e journ�e de Ligue 2. Il se retrouve ainsi seul deuxi�me au classement, devan�ant Dijon et le GFC Ajacco.
Brest s'est impos� 2-1 � Angers, lundi soir, en cloture de la 21e journ�e de L2. Les Finist�riens se retrouvent seuls deuxi�mes au classement de Ligue 2
Malmen� en seconde p�riode, le Stade brestois a finalement fait la d�cision dans les ultimes secondes. Trois points presque inesp�r�s. M�me amput� d�une partie de son onze habituel avec les absences de Grougi, Perez et Falette notamment, Brest ne voulait pas manquer l�occasion de r�cup�rer la place de dauphin au profit de Dijon. Un nul leur suffisait. Ils ont fait mieux au bout du suspense !
ANGERS - BREST�: 1-2 (0-1).
Arbitre�: M. Hamel. 6�902 spectateurs.
BUTS. Angers�: Kodjia (58�). Brest�: Belghazouani (15�), Guillon (csc, 90�+3).
AVERTISSEMENTS. Angers�: Kodjia (68�), Ke�ta (89�). Brest�: Moimb� (48�), Th�baux (57�), Tritz (82�), Belghazouani (86�).
ANGERS�: Butelle - Angoula (Frikeche, 70�), Guillon, Thomas, Manceau - Auriac (c), Ke�ta, Diers - Ngando, Kodjia, Cl�mence (Ben Othman, 46�). Non entr�s�: Letellier (g), Ayari, Pessalli. Entra�neur�: St�phane Moulin.
BREST�: Th�baux - Tritz, Traor�, Martial, Moimb� - Belghazouani, Ramar� (c), Tour�, Laborde (Hamdi, 70�) - Sea, Alphonse. Non entr�s�: Chardonnet, Ranneaud, Doumbia, Missilou. Entra�neur�: Alex Dupont.
St�phane Moulin (entra�neur d�Angers) : � Cruel, le mot est vraiment appropri�. On a les occasions pour gagner, mais on n�a pas r�ussi � marquer un but � Brest, on en a mis un pour eux. Ce soir, on a �t� flamboyants, mais on n�a pas gagn�. Je n�ai rien � reprocher aux joueurs. Quand on perd, il y a toujours la d�ception du r�sultat, mais je crois sinc�rement qu�on ne m�ritait pas �a. On a manqu� de rythme en premi�re mi-temps, on avait du mal � se projeter, ce qu�on a �mieux fait en deuxi�me p�riode. On a mis plus d�intensit�. Je crois que les Brestois sont heureux ce soir, dans tous les sens du terme. Ce soir, �a ne rigole pas malheureusement, et la d�ception de mes joueurs est � la hauteur du match qu�ils ont fait. �Alex Dupont (entra�neur de Brest) : � Le coup �tait parfait, mais on ne l�a pas fait expr�s. On allait se contenter d�un match nul� On a fait une bonne premi�re mi-temps. On menait 1-0 � la mi-temps, c��tait le minimum. On aurait m�me d� se mettre � l�abri. En seconde p�riode, on attendait une r�action des Angevins, et on a plus subi. On aurait aussi pu �tre plus tranchants sur nos ballons de contres. Mais �a a �t� un tr�s bon match entre deux �quipes qui ont jou� pour gagner. Et �a a bascul� pour nous, tant mieux!�
Alex Dupont (entra�neur de Brest) : � Le coup �tait parfait, mais on ne l�a pas fait expr�s. On allait se contenter d�un match nul� On venait pour prendre un point, et on en prend deux de plus ! On a fait une bonne premi�re mi-temps. On menait 1-0 � la mi-temps, c��tait le minimum. On aurait m�me d� se mettre � l�abri. En seconde p�riode, on attendait une r�action des Angevins et on a plus subi. On aurait aussi pu �tre plus tranchants sur nos ballons de contres. Mais �a a �t� un tr�s bon match entre deux �quipes qui ont jou� pour gagner. Et �a a bascul� pour nous, tant mieux ! �
�Revivez Angers - Brest
