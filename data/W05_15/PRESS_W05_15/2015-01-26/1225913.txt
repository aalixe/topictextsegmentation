TITRE: New York se pr�pare � une temp�te historique | La-Croix.com - Monde
DATE: 26-01-2015
URL: http://www.la-croix.com/Actualite/Monde/New-York-se-prepare-a-une-tempete-historique-2015-01-26-1273077
PRINCIPAL: 1225910
TEXT:
petit normal grand
New York se pr�pare � une temp�te historique
New York se pr�pare depuis lundi 26�janvier au matin � un blizzard historique, accompagn� de fortes chutes de neige et de vents violents, qui affectera �galement le nord-est des �tats-Unis.
26/1/15 - 19 H 00
R�agir 0
National Weather Service
Alerte au blizzard dans le nord-est des �tats-unis. De fortes chutes de neige et des vents violents sont attendus dans la nuit de lundi 26 � mardi 27 janvier.
National Weather Service
Alerte au blizzard dans le nord-est des �tats-unis. De fortes chutes de neige et des vents violents sont attendus dans la nuit de lundi 26 � mardi 27 janvier.
Avec cet article
Les pays pauvres, les plus touch�s par le r�chauffement climatique
Depuis lundi 26�janvier, la neige tombe en abondance sur New York, laissant place au balai incessant des chasse-neige d�blayant les rues encore accessibles.
La situation climatique exceptionnelle qui frappe le nord-est du pays a d�j� conduit � l�annulation de plus de 5�000 vols, de ou vers le pays.
Les chutes de neige les plus fortes sont attendues en fin d�apr�s-midi et durant la nuit. Selon la m�t�o nationale ( National Weather Service ), la neige pourrait alors tomber au rythme de 5 � 10�cm par heure, avec une accumulation attendue de 45 � 60�cm, voire plus en certains endroits, et des bourrasques de vent pouvant atteindre de 72 � 88�km/heure.
Le maire de New York appelle les habitants � rester chez eux
Le maire de New York, Bill de Blasio, a mis en garde les habitants, leur demandant de rester � l�int�rieur s�ils le peuvent, et de ne pas circuler en voiture. Les �coles seront probablement ferm�es mardi, a-t-il ajout�.
��Pr�parez-vous � quelque chose de pire que ce que nous avons vu jusqu�� pr�sent��, a-t-il affirm� dimanche 25�janvier. ��Ne sous-estimez pas cette temp�te��, a-t-il insist�, estimant qu�elle pourrait �tre l�une des plus importantes jamais affront�es par la ville de 8,4�millions d�habitants.
Le gouverneur de l��tat, Andrew Cuomo, a �galement incit� les habitants � travailler de chez eux s�ils le pouvaient.
��Possibles pannes d��lectricit頻
��Les New-Yorkais doivent se pr�parer � de possibles pannes d��lectricit�, dues aux vents violents qui pourraient faire tomber les lignes et les arbres��, a-t-il averti.
Il a pr�cis� que l��tat disposait d�au moins 1�806 chasse-neige et de plus de 126�000 tonnes de sel � r�partir pour contrer le mauvais temps. Tous les bus urbains devaient �tre �quip�s de pneus neige ou de cha�nes d'ici lundi apr�s-midi, selon le gouverneur qui a �galement pr�cis� que des �quipes suppl�mentaires avaient �t� mobilis�es pour assurer la s�curit� des quais et des escaliers de m�tro.
Supermarch�s d�valis�s
Les New-Yorkais ont suivi � la lettre les conseils des autorit�s, et se sont pr�par�s, patientant parfois sur le trottoir pour faire des provisions dans des supermarch�s d�valis�s, qui n�avaient pour certains plus de pain ou de lait dimanche 25�janvier dans la soir�e.
Dans la r�gion de Boston, dans le Massachusetts, o� de 50 � 76�cm de neige sont attendus, les bourrasques de vent pourraient d�passer 100�km/heure.
Le manque de visibilit� associ� � la neige et au vent pourrait cr�er la pagaille dans les transports, et affecter plus de 50�millions de personnes dans le secteur touch�, allant de Philadelphie en Pennsylvanie jusqu�� l��tat du Maine.
Vols annul�s de ou vers les �tats-Unis
Dans la matin�e du lundi 26�janvier, d�j� 2�450 vols avaient �t� annul�s pour la journ�e de lundi, et 2�837 pour la journ�e de mardi 27�janvier, selon le site sp�cialis� Flightaware.com .
La compagnie United a annonc� qu�elle avait l�intention d�annuler tous ses vols mardi 27�janvier dans les trois a�roports new-yorkais de Newark, LaGuardia et JFK, ainsi qu�� Boston et Philadelphie.
Le pr�sident Barack Obama, actuellement en Inde, a �t� inform� de l�arriv�e attendue de cette temp�te, a indiqu� la Maison Blanche.
Interdiction de voyager dans certains �tats
Les �tats de Pennsylvanie et du Connecticut ont annonc� la fermeture des �coles et le gouverneur Dan Malloy a annonc� une interdiction de voyager dans tout l��tat � partir de 21h lundi 26�janvier, heure locale.
��Il est imp�ratif que vous ayez un plan en place pour rentrer chez vous en toute s�curit� ce soir, avant que ne commencent les fortes chutes de neige��, a-t-il d�clar� dans une conf�rence de presse.
Le National Weather Service �a mis en garde contre tout d�placement inutile.
��De nombreuses routes seront impraticables��, a soulign� la m�t�o nationale. Les d�placements seront ��extr�mement dangereux en raison des fortes chutes de neige et des vents violents��.
La temp�te devrait s��loigner progressivement mercredi 28�janvier.
