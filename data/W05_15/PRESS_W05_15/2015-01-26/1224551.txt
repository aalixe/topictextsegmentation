TITRE: Allemagne : indice IFO l�g�rement sup�rieur aux attentes en janvier
DATE: 26-01-2015
URL: http://www.boursorama.com/actualites/allemagne-ndice-ifo-legerement-superieur-aux-attentes-en-janvier-ba836d709186c8f353a8e73b596518c0
PRINCIPAL: 1224547
TEXT:
Allemagne : indice IFO l�g�rement sup�rieur aux attentes en janvier
AOF le
(AOF) - L'indice Ifo du climat des affaires en Allemagne est ressorti � 106,7 en janvier, contre 105,5 en d�cembre et un consensus Reuters de 106,3. Il s'agit d'un plus haut de 6 mois.
AOF - EN SAVOIR PLUS
LEXIQUE IFO (indice) : L'institut de recherche et de pr�visions �conomiques allemand IFO publie mensuellement les r�sultats d'un sondage aupr�s de plus de 7000 chefs d'entreprises et dirigeants de tous les secteurs, � l'exclusion de la finance. L'indice global est compos� d'un volet sur la perception qu'ont les sond�s du climat actuel des affaires, et d'un volet sur leurs anticipations � quelques mois. L'IFO d�termine � partir de ces r�ponses le niveau de l'indice, sachant qu'un niveau sup�rieur � 100 signale qu'une majorit� d'entreprises se montre plut�t optimiste, et un indice inf�rieur � 100 r�v�le une majorit� pessimiste.
Copyright 2015 Agence Option Finance (AOF) - Tous droits de reproduction r�serv�s par AOF.
AOF collecte ses donn�es aupr�s des sources qu'elle consid�re les plus s�res. Toutefois, le lecteur reste seul responsable de leur interpr�tation et de l'utilisation des informations mises � sa disposition. Ainsi le lecteur devra tenir AOF et ses contributeurs indemnes de toute r�clamation r�sultant de cette utilisation. Agence Option Finance (AOF) est une marque du groupe Option Finance.
