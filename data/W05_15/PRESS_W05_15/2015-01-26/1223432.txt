TITRE: Haute-Savoie | La randonneuse disparue est d�c�d�e
DATE: 26-01-2015
URL: http://www.ledauphine.com/haute-savoie/2015/01/25/la-randonneuse-disparue-est-decedee
PRINCIPAL: 1223427
TEXT:
Inscription
CH�TEL/MONTRIOND La randonneuse disparue est d�c�d�e
Mobilis�s samedi soir, les sapeurs-pompiers et les gendarmes du GMSP ainsi que les b�n�voles du secours en montagne ont retrouv� ce matin � 0 h 45 au pied de la barre rocheuse de l�Ardent le corps sans vie de la randonneuse anglaise partie randonner en raquettes. �g�e de 55 ans, elle �tait partie des pistes de Ch�tel en fin d�apr�s-midi. Elle ne devait jamais rejoindre son mari qui l�attendait pour d�jeuner aux Lindarets. Pour l�heure, les circonstances de cet accident mortel n�ont pas �t� �tablies avec pr�cision.
Publi� le 25/01/2015 � 10:21
A lire aussi
