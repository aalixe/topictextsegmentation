TITRE: Etats-Unis. Plus de 2.000 vols annulés avant une tempête de neige exceptionnelle - Monde - Le Télégramme, quotidien de la Bretagne
DATE: 26-01-2015
URL: http://www.letelegramme.fr/monde/etats-unis-plus-de-2-000-vols-annules-avant-une-tempete-de-neige-exceptionnelle-26-01-2015-10505545.php
PRINCIPAL: 1225073
TEXT:
Etats-Unis. Plus de 2.000 vols annulés avant une tempête de neige exceptionnelle
26 janvier 2015 à 14h53
/ AFP /
Quelque 2.009 vols ont été annulés ce lundi, en provenance ou à destination des Etats-Unis, où le trafic aérien était affecté par 271 retards en début de matinée. C'est ce qu'a précisé le site spécialisé Flightaware. La météo nationale a émis un avis de blizzard pour New York et Boston, jusqu'à la frontière canadienne. De 5 à 10 cm de neige sont attendus dans le Nord-Est, qui seront suivis par un autre épisode neigeux ces lundi soir et mardi, avec de "lourdes" précipitations.
