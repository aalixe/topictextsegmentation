TITRE: EN DIRECT / LIVE. Novak Djokovic - Gilles Muller - Open d�Australie messieurs - 26 janvier 2015 - Eurosport
DATE: 26-01-2015
URL: http://www.eurosport.fr/tennis/open-d-australie-messieurs/2015/novak-djokovic--gilles-muller_mtc749377/live.shtml
PRINCIPAL: 0
TEXT:
�
14:02�
Merci d'avoir suivi cette 8e journ�e de l'Open d'Australie en notre compagnie. On se retrouve d�s cette nuit pour la suite de ces aventures australes ! Bonne fin de journ�e � tous.
14:02�
En quarts de finale, Novak Djokovic aura encore rendez-vous avec un gros serveur, � savoir Milos Raonic. A match �videmment � suivre sur notre site !
��
#Djokovic wraps it up in 3 sets against #Muller at #ausopen http://t.co/HMN8DRAoCK
�
�
14:01�
Rien � redire quant � cette victoire en trois sets du n�1 mondial : Muller a fait avec les armes � sa disposition, c'est-�-dire essentiellement son service, mais sa seconde balle n'a pas tenu face � la qualit� de relance de Djokovic. Avec 51% de points gagn�s en seconde balle (et un 73% final en 1�res, quand il a longtemps �volu� � plus de 80%), Muller s'est retrouv� quasi-constamment sous pression sur ses engagements, l� o� Djokovic enquillait les jeux ais�s (81% de r�ussite en 1�re, 64% en seconde). Le ratio points gagnants - fautes directes est lui aussi sans appel : 46-10 pour "Djoko", 37-31 pour Muller. Le 42e mondial n'a pas fait un mauvais match. Mais il est � sa place.
13:58�
DJOKOVIC - MULLER : 6-4 7-5 7-5. C'est termin�. Sur un dernier jeu blanc, Novak Djokovic file en quarts de finale de l'Open d'Australie pour la 8e fois de suite. C'est aussi son 23e quart de finale majeur cons�cutif.
��
Instantan� de Djokovic tenant son break dans le troisi�me set.
13:55�
DJOKOVIC - MULLER : 6-4 7-5 6-5. Novak Djokovic tient enfin son break dans ce troisi�me set, sur un passing de revers crois� gagnant. Il serre le poing vers son box : le tiebreak devrait pouvoir �tre �vit�... sauf rel�chement coupable.
��
La vol�e de Muller n'est pas assez appuy�e : Djokovic a tout le temps d'ajuster un passing de coup droit. Troisi�me balle de break.
�
Et le Luxembourgeois sauve la deuxi�me au filet. 40A.
��
Djokovic est trop gourmand sur son attaque de revers, visant la ligne... C'est dans le couloir. 30-40.
��
Rigoureusement le m�me et, cette fois en deux temps, Novak Djokovic va chercher deux nouvelles balles de break ! 15-40.
13:50�
Retour parfait de Djokovic, dans les pieds de Muller. Et cela fait encore 0-30.
13:49�
DJOKOVIC - MULLER : 6-4 7-5 5-5. Djokovic �carte le p�ril et recolle � 5-5.
�
Double faute de Novak Djokovic : 30A. Gilles Muller � deux points du set...
13:45�
DJOKOVIC - MULLER : 6-4 7-5 4-5. Toujours l�, Gilles Muller, montrant plus de choses dans ce set que lors des deux premiers. Et sans doute va t-il prendre tous les risques au jeu suivant...
#Muller saves 1 BP v #Djokovic, & now leads 5-4 in the 3rd set. Swarming the net at every chance. #ausopen http://t.co/xAcmda6Nm9
�
�
Quel cran de Gilles Muller pour sauver cette balle de break ! Non seulement il a tenu l'�change, mais sa mont�e au filet est r�solue... et r�compens�e : la vol�e est pleine ligne. 40A.
��
Et Novak Djokovic, � nouveau saignant � l'�change, va chercher une nouvelle balle de break : 30-40.
13:40�
Vilaine vol�e de coup droit, beaucoup trop longue, de la part de Gilles Muller : 0-30, encore...
13:39�
DJOKOVIC - MULLER : 6-4 7-5 4-4. Deux aces opportuns c�t� Djokovic, les n�9 et 10, pour pr�server sa mise en jeu.
13:37�
Double faute de Novak Djokovic, la 2e du match. Celle-l� permet � Muller de mener 15-30 sur service adverse...
13:35�
DJOKOVIC - MULLER : 6-4 7-5 3-4. Bon an, mal an, Muller tient son service. C'est actuellement lui le plus tranchant des deux hommes. La preuve : il se met m�me � gagner des points � l'�change.
#Muller holds for 4-3, 3rd set v #Djokovic. Fully committed to coming forward. Only won 20/62 at the back. #ausopen http://t.co/3FbdsXRzqJ
�
13:34�
Djokovic s'est aussit�t remobilis�, apr�s ce jeu de service d�licat. Il monte � 30A sur le service de Muller.
13:32�
DJOKOVIC - MULLER : 6-4 7-5 3-3. Le point du match remport� par Novak Djokovic ! Muller s'est tromp� de c�t� sur un smash et, en outre, ne l'appuie pas assez. Djokovic para�t lui-m�me surpris de le remettre puis, derri�re, contre parfaitement la vol�e haute de revers de Muller. Ce point fou, fou, lui permet de remporter sa mise en jeu, alors qu'il a �t� pour la premi�re fois chahut� sur son engagement.
�
�
Le Luxembourgeois prend encore l'initiative, mais son attaque en slice de revers reste dans le filet. 40A.
��
Quatri�me occasion pour Muller. C'est actuellement lui qui fait le jeu.
�
Effac�e par Djokovic, sur un coup droit pleine ligne, sans aucune marge : le hawk-eye la d�signe comme effleurant la ligne. Egalit�.
��
Troisi�me chance pour Muller, opportuniste l� o� Djokovic se montre un peu passif.
�
Trop gourmand, Gilles Muller, sur son attaque de coup droit. Trop longue. 40A.
��
Djokovic sort un service gagnant sur la premi�re. 30-40.
��
Et ce sont m�me 2 balles de break pour Gilles Muller, sur une superbe attaque de coup droit long de ligne ! 15-40. Ses premi�res balles de break du match.
13:26�
Une occasion pour Muller ? Le voil� � 15-30 sur le service de Djokovic...
13:23�
DJOKOVIC - MULLER : 6-4 7-5 2-3. Muller s'accroche et empoche son engagement � 30.
13:21�
DJOKOVIC - MULLER : 6-4 7-5 2-2. Jeu blanc Djokovic.
�
-�
Pour un joueur pass� par la c�l�bre acad�mie Sanchez-Casal de Barcelone, Gilles Muller - champion du monde junior en 2001, tout de m�me - n'aura jamais su gommer ses lacunes en fond de court.
(Foto) #Muller en plena acci�n en @SanchezCasal . Qu� recuerdos... siempre mostr� un servicio potente. http://t.co/w0VC7CdAs9
�
13:18�
DJOKOVIC - MULLER : 6-4 7-5 1-2. Ouf pour Gilles Muller : son service a tenu, malgr� une balle de break � effacer.
�
Bien sauv�e au filet. Le Luxembourgeois rattrape sa bourde pr�c�dente. 40A.
��
Bon service de Muller, mont�e au filet pour poser une vol�e dans le court grand ouvert... mais c'est trop long ! 30-40, balle de break Djokovic.
13:15�
Djokovic relance de mieux en mieux le service de Muller. Celui-ci part � la faute alors qu'il dirigeait l'�change en coup droit. 15-30.
13:14�
DJOKOVIC - MULLER : 6-4 7-5 1-1. R�ponse instantan�e de Novak Djokovic : jeu blanc �galement. Le Serbe en est maintenant � 7 aces, l� o� Muller est bloqu� � 4.
13:12�
DJOKOVIC - MULLER : 6-4 7-5 0-1. Muller ne s'est pas d�mobilis� et remporte ais�ment son premier engagement du troisi�me set, blanc.
13:08�
Les stats s'affinent et refl�tent bien l'�chapp�e du Serbe, apr�s un premier set pass� � se familiariser avec les s�quences de jeu d'un adversaire qu'il n'avait jamais rencontr�. Djokovic est � 83% de r�ussite en 1�re et 73% en seconde, et n'a commis que 9 fautes directes pour 25 points gagnants. Muller, lui, peut bien afficher 80% de r�ussite en premi�re, Djokovic le met en grande difficult� sur ses secondes balles (44% de r�ussite). Enfin, Muller ne s'est toujours pas procur� la moindre balle de break, quand Djokovic est � 2/6.
13:07�
DJOKOVIC - MULLER : 6-4 7-5. Sur un 4e service gagnant en 5 points disput�s, Novak Djokovic prend le large dans cette partie. On joue depuis 1h15. Copie pour l'instant tr�s, tr�s propre du n�1 mondial, chirurgical.
13:06�
Novak Djokovic d�bute par deux services gagnants. 30-0.
�
��
La couverture de terrain toujours sensationnelle du num�ro 1 mondial, avec sa capacit� � glisser en bout de course, comme s'il �voluait sur terre battue.
13:03�
DJOKOVIC - MULLER : 6-4 6-5. Sous pression � l'�change, Muller part de nouveau � la faute sur son attaque de revers long de ligne, et offre le break � Novak Djokovic ! Le Serbe va servir pour mener deux manches � rien.
��
Troisi�me chance pour Djokovic dans ce jeu, alors que Muller exp�die son attaque de revers long de ligne dans le filet.
�
Ace n�4 de Muller. Il tombe bien, celui-l�. 40A.
��
Deuxi�me occasion pour le Serbe, appliqu� sur son passing. Avantage Djokovic.
�
Quel loup� de Djokovic au filet, alors qu'il avait fait le plus difficile pour s'ouvrir grand le court � l'�change ! 40A.
��
Et balle de break Djokovic, alors que la vol�e de revers de Muller flotte largement au-del� des limites du court : 30-40 !
�
Service gagnant sur premi�re balle, service gagnant sur... seconde, plein corps. 30A.
12:59�
Le Serbe monte � nouveau � 0-30 sur le service de Muller, plus malmen� depuis le milieu de ce set.
12:57�
DJOKOVIC - MULLER : 6-4 5-5. Contraint de servir pour rester dans le set, Djokovic r�pond par un jeu blanc.
�
12:54�
DJOKOVIC - MULLER : 6-4 4-5. Ca tient toujours pour Gilles Muller, gr�ce � un let heureux devenu amortie imparable, suivi d'un service gagnant.
#Muller gets a lucky net cord at 30-30 & holds for 5-4 lead v #Djokovic. Gilles now won 15/20 approaching. #ausopen http://t.co/Dzz53RhWkG
�
Muller se cramponne � sa premi�re et couvre bien son filet pour recoller � 30A.
12:52�
Deux magnifiques retours de Novak Djokovic, l'un tendu au T, l'autre crois� court sur un service slic� de Muller ! Le voil� � 0-30.
12:50�
DJOKOVIC - MULLER : 6-4 4-4. Jeu � 15 pour le Serbe, qui tient sa moyenne d'un point abandonn� par jeu de service depuis le d�but du match (11 en 11 jeux).
12:48�
DJOKOVIC - MULLER : 6-4 3-4. Muller sort 2 aces pour s'extirper du gu�pier. Ca tient pour le 42e joueur mondial, qui a d� �carter une balle de break.
�
Service-vol�e sur premi�re balle de la part de Muller : �a passe. 40A.
��
La seconde balle de Muller est aussit�t exploit�e par Djokovic : superbe passing crois� du Serbe en coup droit, et elle lui offre balle de break !
�
Tr�s bon retour de Djokovic sur premi�re adverse. Muller est pris. 40A.
12:43�
Au tour de Djokovic de monter � 15-30 sur le service adverse, suite � une vol�e amortie mal touch�e par Muller et revenue sous forme de passing.
12:42�
DJOKOVIC - MULLER : 6-4 3-3. Djokovic a bien �cart� le danger naissant en alignant trois points de suite. Il n'a toujours pas eu � d�fendre la moindre balle de break.
�
12:39�
Djokovic part deux fois de suite � la faute en coup droit. 15-30, une premi�re opportunit� � n�gocier � la relance pour Gilles Muller.
��
Muller est toujours port� par sa 1�re balle : 87% de r�ussite (20 points sur 23). Le pourcentage monte aussi sur la seconde : 56% (9/16).
12:37�
DJOKOVIC - MULLER : 6-4 2-3. Et jeu � 15 pour Gilles Muller. Le Luxembourgeois tient l'�preuve de celui qui est sans doute le meilleur relanceur au monde.
12:34�
DJOKOVIC - MULLER : 6-4 2-2. Jeu blanc �galement sur Novak Djokovic, sur un deuxi�me lob gagnant dans cette partie.
12:31�
DJOKOVIC - MULLER : 6-4 1-2. Jeu blanc Gilles Muller. Break ou pas break, les engagements d�filent � toute allure : 8 minutes seulement pour les 3 premiers jeux du 2e set.
#Muller leads 2-1 2nd set v #Djokovic. The Luxembourger has won 12/14 serve & volleying. Creating pressure. #ausopen http://t.co/udbVcIrQHC
�
12:28�
DJOKOVIC - MULLER : 6-4 1-1. La r�ponse de Djokovic, avec un jeu emport� � 30. En 6 jeux de services, le Serbe a abandonn� 7 points.
12:26�
DJOKOVIC - MULLER : 6-4 0-1. Muller repart du bon pied avec un premier jeu du deuxi�me set remport� � 15.
-�
Comme pr�vu, le salut de Gilles Muller passe par sa premi�re balle de service. Mais d�s qu'il a �t� l�ch� par celle-ci, la sanction est tomb�e sous forme de break. A l'oppos�, le Luxembourgeois ne parvient pas � �tre dangereux � la relance. En chiffres, cela donne : 83% de r�ussite derri�re sa premi�re et 71% derri�re sa seconde pour Djokovic, avec 3 aces et 14 points gagnants (3 fautes directes). Muller est � 81% derri�re sa premi�re mais seulement 44% en seconde. Et 2 aces, 7 points gagnants et 8 fautes directes.
12:22�
DJOKOVIC - MULLER : 6-4. Et c'est fait sur un jeu blanc pour le num�ro 1 mondial. 6-4 en 32 minutes.
12:20�
Cela d�bute bien pour Djokovic : 2 services gagnants. 30-0.
�
12:16�
DJOKOVIC - MULLER : 5-4. Jeu Muller � 15. Il oblige Djokovic � servir pour le set, alors que le Serbe s'est effleur� le genou en tombant sur un contrepied adverse.
Petite manucure pour Djokovic avant de servir pour le set #AusOpen http://t.co/svUr0m2e7m
�
DJOKOVIC - MULLER : 5-3. Break confirm� par le Serbe. Cela fait 5-3 en 24 minutes.
��
#Djokovic breaks #Muller to 15, & leads 4-3, 1st set. Outstanding defensive lob at 0-15 created the chance. #ausopen http://t.co/tpaZFeakAp
�
12:09�
DJOKOVIC - MULLER : 4-3. Le premier jeu accroch� de cette partie est aussi celui du premier break : Novak Djokovic prend les commandes gr�ce � un excellent retour qu'il fait fructifier � l'�change, son revers faisant craquer le coup droit de gaucher de Muller.
��
La premi�re est bien sauv�e � l'�change, d'une attaque de coup droit long de ligne. 15-40.
��
Et Novak Djokovic obtient les 3 premi�res balles de break du match. 0-40.
12:07�
Enorme d�fense de Novak Djokovic, dont le coup droit d�sesp�r� en bout de course se transforme en lob pleine ligne ! Muller, coll� au filet, est pris. Et �a fait 0-30, premi�re fen�tre de tir pour un relanceur...
12:06�
DJOKOVIC - MULLER : 3-3. Nouveau jeu � 15 pour le Serbe. Il est devant aux aces r�ussis : 3 contre 1.
��
#Djokovic under the pump. #Muller leads 3-2 & intentions very clear. Pressure with the serve & forward. #ausopen http://t.co/CTUnW0f2cL
�
12:03�
DJOKOVIC - MULLER : 2-3. Jeu blanc pour Gilles Muller. Son tennis d'attaque emp�che pour le moment Djokovic de se r�gler.
�
DJOKOVIC - MULLER : 2-2. Djokovic recolle, � 15.
11:56�
DJOKOVIC - MULLER : 1-2. Jeu de service bien men� par Gilles Muller, remport� � 15. Il a pass� 7 premi�res balles en 2 jeux... et a fait � chaque fois le point derri�re. Il est par contre d�j� � un dangereux 1/4 en seconde balle.
11:54�
DJOKOVIC - MULLER : 1-1. La r�ponse du num�ro 1 mondial, sur un jeu de service remport� � 15.
11:52�
DJOKOVIC - MULLER : 0-1. Gilles Muller ouvre son compteur dans ce match. Mais Novak Djokovic est d�j� mont� � 30A.
11:49�
D�but de cette rencontre entre Novak Djokovic et Gilles Muller ! Le Luxembourgeois a gagn� le toss et a choisi de servir.
-�
L'arme principale de Gilles Muller pour exister dans ce match : son service. Derri�re, il cherchera � se jeter au filet � la moindre ouverture. Si sa premi�re balle n'est pas au rendez-vous, il va au contraire souffrir en fond de court...
Gilles #Muller has hit 80 aces so far at the #ausopen. Only 12 DF's. 48% of serves have not come back in play. Now vs. #Djokovic.
�
-�
En quarts de finale de l'Open d'Australie, le vainqueur de ce match rencontrera Milos Raonic en quarts de finale. Le Canadien a sorti Feliciano Lopez en cinq manches (6-4, 4-6, 6-3, 6-7, 6-3).
-�
L'enjeu de ce match, r�sum� en une stat : Gilles Muller vise le deuxi�me quart de finale en Grand chelem de sa carri�re. Novak Djokovic vise son 23e quart d'affil�e, deuxi�me meilleure s�rie en la mati�re derri�re les 36 de Roger Federer.
-�
S'il ne part pas avec la faveur des pronostics, Muller, 31e ans et 42e joueur mondial - il �tait au-del� de la 300e place il y a un an, la faute � plusieurs blessures - a par le pass� sign� quelques victoires de prestige en Grand chelem : Rafael Nadal � Wimbledon 2005, Andy Roddick � l'US Open 2005, ou encore Nikolay Davydenko � l'US Open 2008. Lors de ce tournoi, Muller avait d'ailleurs obtenu son meilleur r�sultat dans un tournoi majeur : quart de finale - alors qu'il sortait des qualifications.
-�
Gilles Muller a quant � lui fait tomber deux t�tes de s�rie pour s'inviter en deuxi�me semaine : Roberto Bautista Agut, n�13, au 2e tour (7-6, 1-6, 7-5, 6-1), et John Isner, n�19, au 3e (7-6, 7-6, 6-4). Au premier tour, le Luxembourgeois avait �cart� Pablo Carreno Busta (6-4, 7-6, 7-6).
�
-�
D�j� quadruple vainqueur de l'Open d'Australie (2008, 2011, 2012 et 2013), Novak Djokovic n'a pas tra�n� sur la route de la deuxi�me semaine : 3 sets contre Aljaz Bedene (6-3, 6-2, 6-4), 3 sets contre Andrey Kuznetsov (6-0, 6-1, 6-4) et 3 sets contre Fernando Versdasco (7-6, 6-3, 6-4).
-
Il s'agit du premier affrontement entre les deux hommes.
-�
Bonjour et bienvenue sur notre site pour suivre, EN DIRECT, le huiti�me de finale de l'OPEN d'AUSTRALIE opposant Novak DJOKOVIC � Gilles MULLER !
