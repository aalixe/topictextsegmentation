TITRE: Carmat : jusqu'� 50 ME de ressources via un financement en fonds propres
DATE: 26-01-2015
URL: http://www.boursier.com/actions/actualites/news/carmat-jusqu-a-50-me-de-ressources-via-un-financement-en-fonds-propres-612913.html
PRINCIPAL: 1223811
TEXT:
Mon compte Je suis d�j� client
D�couvrir Je souhaite recevoir une documentation gratuite
Carmat : jusqu'� 50 ME de ressources via un financement en fonds propres
Abonnez-vous pour
moins de 1� par jour !
Le 26/01/2015 � 06h22
(Boursier.com) � Pour renforcer ses sources de financement, Carmat a sign� un accord de financement en fonds propres avec Kepler Cheuvreux. Ce dispositif, que les anglo-saxons appellent "equity line", est sign� avec un interm�diaire financier qui s'engage � acqu�rir des actions nouvelles d'une soci�t� � la demande de celle-ci, en b�n�ficiant d'une d�cote � l'acquisition. Le montant et la dur�e du programme sont plafonn�s. L'engouement des investisseurs pour les soci�t�s innovantes depuis deux ans a remis au go�t du jour ce syst�me, qui avait �t� un peu laiss� � l'abandon durant la crise financi�re.
Jusqu'� 50 ME de financement
Carmat a pour sa part sign� un accord-cadre, comprenant un maximum de trois tranches successives de douze mois chacune. Il remplace le contrat pr�c�dent qui avait d�j� �t� mis en place avec Kepler Cheuvreux en juin 2013 . La premi�re tranche porte sur 20 millions d'euros, les deux secondes, optionnelles, sur 15 ME. La soci�t� p�se actuellement 300 ME en bourse. Sur la base des cours actuels la premi�re tranche repr�senterait ainsi 6,5% du capital de Carmat, ou un peu plus de 300.000 actions, sur douze mois. Le prix de souscription des titres b�n�ficiera d'une d�cote de 6% sur les cours au moment du tirage, ce qui permettra � Kepler Cheuvreux de sa r�mun�rer.
Carmat publiera ses r�sultats 2014 le 11 f�vrier prochain.
Anthony Bondain � �2015, Boursier.com
