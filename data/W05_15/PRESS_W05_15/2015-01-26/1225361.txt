TITRE: Le Snapdragon 810 pr�t pour le Samsung Galaxy S6 ?
DATE: 26-01-2015
URL: http://www.silicon.fr/le-samsung-galaxy-s6-pourrait-finalement-accueillir-le-qualcomm-snapdragon-810-106666.html
PRINCIPAL: 1225357
TEXT:
3 15 1 0 1 commentaire
Qualcomm travaillerait � corriger le probl�me de surchauffe du Snapdragon 810 pour conserver son client Samsung.
Abandonnera, abandonnera pas?? La semaine derni�re, une information avanc�e par Bloomberg laissait entendre que Samsung aurait l�intention de d�laisser le processeur Snapdragon 810 de Qualcomm dans son prochain smartphone le Galaxy S6 . Un probl�me d��mission de chaleur du composant serait � l�origine de la d�cision de Samsung.
Mais, selon New York Times, Qualcomm travaillerait � une prochaine version de sa puce pour r�soudre ce probl�me afin de pousser le fabricant cor�en � revenir sur sa d�cision suppos�e. Le Snapdragon 810 est un SoC (system on a chip) qui int�gre � la fois l�unit� de calcul informatique et le modem radio cellulaire. Premier vendeur mondial de puces pour t�l�phones mobiles, Qualcomm s�inscrit comme le principal fournisseur de processeur de Samsung. A l�occasion du CES 2015, LG, qui utilise le Snapdragon 810 dans son G Flex 2, dit avoir r�solu le probl�me de r�chauffement en adaptant par le design le syst�me de refroidissement du smartphone.
Exynos, le rempla�ant du S810
Il restera n�anmoins � v�rifier que le Snapdragon 810 corrig� pourra �tre livr� � temps en mars prochain, date de production du Galaxy S6. Sinon, Samsung se tournera probablement vers son propre processeur, Exynos, qui �quipe d�j� les Galaxy S en parall�le des Snapdragon. Le retrait de Samsung sur les ventes du S810 pourrait impacter la valeur boursi�re de Qualcomm � hauteur de 2% � 8%, selon un analyste du cabinet Sanford C. Bernstein, cit� par le WSJ.
Lire �galement
