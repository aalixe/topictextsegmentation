TITRE: Grippe saisonni�re : seuil �pid�mique franchi et r�gles d'hygi�ne...
DATE: 26-01-2015
URL: http://www.senioractu.com/Grippe-saisonniere-seuil-epidemique-franchi-et-regles-d-hygiene_a17666.html
PRINCIPAL: 0
TEXT:
Sant�
Grippe saisonni�re : seuil �pid�mique franchi et r�gles d'hygi�ne...
Alors que la grippe saisonni�re a atteint son seul �pid�mique, rappelons ci-apr�s quelques r�gles d�hygi�ne qui permettent de limiter le d�veloppement de la maladie� Soulignons que les cas de grippe recens�s � ce jour sont principalement dus � la circulation de la souche A(H3N2), qui peut entra�ner des complications s�v�res chez les personnes fragiles et particuli�rement chez les seniors.
Ca y est. Comme chaque ann�e � la m�me �poque, la grippe est de retour� De plus, le seuil d�alerte �pid�miologique vient d��tre franchi. Plus concr�tement, cela signifie que les cas de grippe sont maintenant suffisamment nombreux pour avoir un impact r�el dans l�activit� des soignants de premi�re ligne.
�
Rappelons que la vaccination reste le moyen de pr�vention le plus efficace contre la grippe et qu'elle est particuli�rement recommand�e chez les personnes pr�sentant un risque accru de complications (qui peuvent �tre graves voire mortelles dans certains cas). La campagne de vaccination contre la grippe concerne ainsi chaque ann�e plus de dix millions de personnes qui peuvent b�n�ficier de la prise en charge � 100% du vaccin, dont : les personnes �g�es de 65 ans et plus ; les personnes atteintes de certaines maladies chroniques ; les femmes enceintes ; et enfin, les personnes ob�ses avec un IMC �gal ou sup�rieur � 40.
�
M�me si la vaccination reste la meilleure protection contre la grippe, en particulier chez les professionnels de sant� et les personnes � risque, il existe des mesures d�hygi�ne simples � suivre et qui contribuent � limiter la transmission de la maladie de personne � personne.
�
Ainsi, il est recommand� aux personnes malades, d�s le d�but des sympt�mes, de :
- limiter les contacts avec d�autres personnes et en particulier avec les personnes � risque ou fragiles et �ventuellement de porter un masque chirurgical en leur pr�sence ;
- se laver r�guli�rement les mains � l�eau et au savon, ou les d�sinfecter par friction avec une solution hydroalcoolique ;
- se couvrir la bouche et le nez � chaque fois qu�elles toussent ou �ternuent ;
- se moucher dans des mouchoirs en papier � usage unique.
�
De plus, il est recommand� aux personnes de l�entourage du malade :
- d��viter les contacts rapproch�s avec ces derni�res, si elles sont � risque ;
- de se laver r�guli�rement les mains et particuli�rement apr�s tout contact avec le malade ou le mat�riel utilis� par le malade ;
- de nettoyer les objets couramment utilis�s par celui-ci.
Lu 5763 fois
Publi� le Lundi 26 Janvier 2015
R�agissez
