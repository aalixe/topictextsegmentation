TITRE: “‘Secrets & Lies’ parle de tout ce que l’on cache derrière nos façades proprettes�? - Séries TV - Télérama.fr
DATE: 26-01-2015
URL: http://www.telerama.fr/series-tv/secrets-lies-parle-de-tout-ce-que-l-on-cache-derriere-nos-facades-proprettes,122015.php
PRINCIPAL: 0
TEXT:
�
Entretien
“‘Secrets & Lies’ parle de tout ce que l’on cache derrière nos façades proprettes�?
Publié le 26/01/2015. Mis à jour le 09/02/2015 à 12h19.
@Ten/France2
France 2 lance lundi 26 janvier la minisérie “Secrets & Lies�?, dont le héros est suspecté (à tort ?) d’avoir tué un enfant. Entretien avec le créateur de cette bonne surprise venue d’Australie.
Diffus�e en 2014 sur Network Ten en Australie, Secrets & Lies a fait l�objet d�un remake am�ricain attendu en mars sur ABC. Cette minis�rie en six �pisodes, souvent compar�e � Broadchurch pour son meurtre d�un enfant, sa description d�une petite communaut� et sa mise en sc�ne atmosph�rique, privil�gie la psychologie des personnages � l�action. Son h�ros, Ben Gundelach (Martin Henderson), est un p�re de famille sans histoires, qui dans la premi�re sc�ne rentre paniqu� de son footing matinal. Dans le bush, � quelques pas de sa tranquille rue de la banlieue de Brisbane, il vient de trouver le corps d�un enfant. Celui du fils de sa voisine� Malgr� ses protestations, Ben devient le suspect num�ro un, harcel� par la police, poursuivi par la presse. Alors que sa vie de famille part en lambeaux, il d�cide de mener sa propre enqu�te pour prouver son innocence. A moins qu�il ne soit coupable� Interview de Stephen M. Irwing, le cr�ateur de Secrets & Lies.
Votre s�rie s�appelle ��Secrets et Mensonges �. C�est assez g�n�rique, non�?
Au d�part, elle devait s�appeler The Track (��le chemin��), en r�f�rence au sentier o� Ben d�couvre le corps du jeune Tom. �a sonnait bien, mais ne disait pas grand-chose de ce qui se passe apr�s cette premi�re sc�ne, tout au long de la s�rie. Secrets & Lies parle de ce que l�on dissimule derri�re les portes ferm�es. M�me dans une banlieue proprette comme celle des Gundelach, les fa�ades cachent des histoires parfois terribles. C�est ce que Ben va d�couvrir en essayant de prouver son innocence�
Secrets & Lies a �t� compar�e � Broadchurch �
C�est toujours agr�able d��tre compar� � une �uvre de qualit�, mais Broadchurch ne m�a en aucun cas influenc�. J�avais boucl� le sc�nario de Secrets & Lies avant m�me qu�elle ne soit diffus�e en Grande-Bretagne� et je ne l�ai toujours pas vue. C�est une co�ncidence qui n�est pas surprenante. Les drames autour de la mort d�un enfant ne datent pas d�hier.
�Je me suis demand� ce qu�il m�arriverait si je trouvais un corps...�
Quel a �t� le point de d�part de votre histoire�?
Elle m�est venue comme elle commence�: lors d�un footing matinal dans la for�t qui borde ma maison. J��tais seul, il �tait t�t, et je me suis demand� ce qu�il m�arriverait si, l�, au milieu de la nature, je trouvais un corps. Si j��tais celui qui appelait la police, les gens s�imagineraient-ils que je suis coupable�? Je suis rentr� chez moi, j�ai rapidement couch� quelques lignes sur le papier. C�est une id�e tr�s simple, celle d�un type sans histoires embarqu� dans une affaire criminelle, une situation qui peut parler � tout le monde.
C�est une id�e assez cauchemardesque mais extr�mement r�pandue, ce sentiment de culpabilit�, m�me si on est innocent�
Chacun d�entre nous s�est un jour demand� avec effroi ce qu�il pourrait ressentir s�il �tait amen� � tuer quelqu�un. C�est un crime encore plus inconcevable quand il s�agit d�un enfant. D�ailleurs, au d�part, je ne pensais pas que le corps que Ben Gundelach d�couvre serait celui d�un enfant. Ce n�est que quand je me suis mis dans sa position, quand j�ai imagin� son sentiment de culpabilit� ou d�injustice, que j�ai d�cid� que Tom serait un petit gar�on. Pour pousser au maximum l�horreur de la situation.
Secrets & Lies est une s�rie r�aliste, mais il y a justement un l�ger glissement vers le cauchemar�
Ben affronte des forces qui le d�passent. Le d�tective Cornielle, en particulier, semble inflexible, un v�ritable robot qui fait respecter l�ordre et la justice sans jamais faillir. De plus, la ��r�alit頻 du r�cit est peut-�tre voil�e par le point de vue de Ben, dont on r�alise que les souvenirs ne sont pas toujours clairs. Il y a donc bien un d�calage entre ce que ressent cet homme dont la vie ordinaire va brutalement d�railler, qui va voir son monde renvers� par l�enqu�te, et la r�action de son entourage.
�Un innocent terrifi� d��tre accus� � tort� ou un coupable qui perd le contr�le�
Vous n�abandonnez jamais le point de vue de Ben Gundelach, qui se d�fend d��tre le coupable, mais qui l�est peut-�tre. Ce n�est pas simple, pour entretenir le myst�re�
En effet, il n�est pas facile de raconter une histoire dont le h�ros est aussi le principal suspect. On d�peint Ben comme un gars ordinaire, gentil, simple, qui vit une existence sans remous, mais qui de temps en temps fait aussi des choses stupides. Plus la pression monte autour de lui, plus il se comporte �trangement. Comme un innocent terrifi� d��tre accus� � tort� ou un coupable qui perd le contr�le. Son comportement doit pouvoir �tre interpr�t� dans les deux sens.
Pourquoi ne pas avoir pris plus de temps pour d�velopper les autres personnages, les voisins notamment�?
Un seul point de vue est bien plus efficace pour mettre en sc�ne l�id�e que nous cachons tous quelque chose. Comme Ben devient parano�aque, il va imaginer que ses voisins sont coupables, chercher chez eux de quoi convaincre la police qu�il est innocent, et du coup r�v�ler leurs secrets.
La version am�ricaine de Secrets & Lies, avec Ryan Phillippe et Juliette Lewis, est attendue le 1er mars sur ABC aux Etats-Unis.
Le 1er mars prochain, ABC lancera aux Etats-Unis le remake de Secrets & Lies. Vous y avez particip�?
Malheureusement non. Je suis tr�s heureux de savoir qu�une version am�ricaine est attendue, mais j�ai vendu mon id�e, et je n�ai pas eu le contr�le sur ce qu�ABC en a fait. J�ai pu jeter un �il sur le script du premier �pisode, et il est assez fid�le � ma version. Donc je suis impatient de voir o� ils vont aller � et particuli�rement s�ils vont changer la fin.
�a ne vous choque pas plus que �a que votre histoire soit entre les mains d�autres sc�naristes, sans que vous ayez votre mot � dire�?
C�est ainsi. J�ai vendu le format, je savais que si un remake �tait produit, ce serait sans moi. Je n�aurais pas �t� contre participer � cette version, mais je peux comprendre que les Am�ricains adaptent les s�ries � leur public, et qu�ils n�aient pas besoin de moi. C�est dans leurs habitudes�
�En Australie, nous sommes dans une bonne p�riode cr�ative�
Justement, une version am�ricaine de La Gifle est aussi programm�e en f�vrier. Vous avez l�impression que les s�ries australiennes sont en vogue en ce moment�?
Ce serait une bonne nouvelle. Les auteurs et les producteurs australiens pensent toujours en premier lieu au public local, mais nous gardons dans un coin de notre t�te une possible diffusion � l�international. Nous sommes aussi de plus en plus impliqu�s dans des coproductions � Top of the Lake est un excellent exemple. Je travaille moi-m�me en ce moment avec des producteurs anglais. Nous sommes dans une bonne p�riode cr�ative.
Envisagez-vous une saison 2 pour Secrets & Lies�?
Network Ten, le diffuseur, n�en veut pas. Mais je ne suis pas contre. On ne pourrait pas retrouver les m�mes personnages, sauf peut-�tre le d�tective Cornielle. Ce n�est pas au programme � l�heure actuelle, mais pourquoi pas ? On verra bien.
�
Secrets & Lies, sur France 2 le lundi � 20h50. 2 �pisodes le premier soir, 4 le suivant (ce qui est regrettable).
Disponible en DVD (Francetvdistribution) le 4 mars.
�
