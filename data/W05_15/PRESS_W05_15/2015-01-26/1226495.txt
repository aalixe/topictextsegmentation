TITRE: Espagne: Le crash d'un F-16 fait 10 morts et de nombreux bless�s -  News Monde: Faits divers - tdg.ch
DATE: 26-01-2015
URL: http://www.tdg.ch/monde/faits-divers/crash-avion-combat-10-morts-espagne/story/28186068
PRINCIPAL: 1226490
TEXT:
Description de l'erreur*
Veuillez SVP entrez une adresse e-mail valide
Le crash au d�collage d'un avion de combat grec de type F-16, a tu� lundi 26 janvier au moins huit Fran�ais et deux Grecs, d�ploy�s sur une base accueillant un centre de formation de pilotes d'�lite de l' OTAN dans le sud-est de l'Espagne.
Le chef du gouvernement espagnol Mariano Rajoy a lui-m�me d�taill� ce bilan lundi soir en direct sur une cha�ne de t�l�vision, pr�s de six heures apr�s l'accident qui s'est produit vers 15 heures au d�collage de l'avion et a conduit le F-16 � s'�craser sur d'autres a�ronefs.
�Il semble qu'il y a deux personnes qui sont d�c�d�es qui sont de nationalit� grecque et huit Fran�ais�, a d�clar� Mariano Rajoy, en d�taillant le bilan de dix morts, dont les nationalit�s �taient jusqu'alors inconnues. La pr�sidence fran�aise a confirm� vers 22h30.
Smoke from crashed #NATO #F16 in Spain visible from miles away. 10 dead, 13 injured pic.twitter.com/Ky9ZcSrVDm
� Jim Sciutto (@jimsciutto) January 26, 2015
Graves blessures par br�lures
�C'est avec une tr�s grande �motion que le pr�sident de la R�publique a appris la mort de huit aviateurs suite � un accident caus� par la chute d'un avion de combat F-16 peu apr�s le d�collage de la base d'Albacete�, a d�clar� l'Elys�e dans un communiqu�.
La pr�sidence fran�aise a pr�cis� que �cet accident a caus� par ailleurs de graves blessures par br�lures � six personnels m�caniciens, selon le bilan actuel pr�sent� par les autorit�s espagnoles�.
Selon le minist�re de la D�fense fran�ais, les corps des victimes de l'accident ont �t� transport�s directement � l'h�pital, avant m�me leur identification formelle.
Nombreux bless�s
Le chef du gouvernement espagnol, qui s'exprimait sur la cha�ne Telecinco, a ajout� qu'il y avait �pas mal de bless�s�. �On �voque le chiffre de dix Fran�ais, onze Italiens�, a-t-il pr�cis�, se montrant prudent car il �tait encore, selon lui, �tr�s t�t�.
Du c�t� du minist�re de la D�fense fran�ais, on �voque un bless� �en situation d'extr�me urgence, et deux plac�s en coma artificiel�.
Le ministre fran�ais de la D�fense, Jean-Yves Le Drian, se rendra sur la base mardi apr�s-midi. Selon l'�tat-major italien, parmi les bless�s figurent neuf Italiens, dont seulement deux sont gri�vement touch�s.
�motion sur Twitter
Le chef du gouvernement Mariano Rajoy avait auparavant exprim� sur Twitter son �motion �apr�s le tragique accident sur la base a�rienne de Los Llanos�, pr�s d'Albacete (environ 250 km au sud-est de Madrid).
Conmovido por el tr�gico accidente en la base a�rea de Los Llanos. Mi pesar por los fallecidos y mi deseo de recuperaci�n de los heridos. MR
� Mariano Rajoy Brey (@marianorajoy) January 26, 2015
Un premier bilan de dix morts et 13 bless�s a �t� revu � la hausse avec 21 bless�s �voqu�s par le chef du gouvernement et 19 par le minist�re de la D�fense espagnol.
L'avion de combat grec devait effectuer des man�uvres dans le cadre d'un entrainement organis� par l'OTAN, le Tactical leadership Programme (TLP), et s'est �cras� au d�collage.
Il s'est apparemment �cras� sur le tarmac, heurtant d'autres a�ronefs et tuant d'autres personnes qui se trouvaient sur la piste.
Incendie ma�tris�
Des images video amateur que l'AFP a pu visionner montrent un avion en feu, d'o� s'�chappent d'importantes volutes de fum�e noire puis au moins trois colonnes de fum�e.
Les �quipes de secours ont d� venir � bout de l'incendie entra�n� par le crash sur l'aire de stationnement avant de pouvoir d�terminer le nombre de victimes.
Le chef d'Etat major de l'arm�e de l'air, le g�n�ral Francisco Javier Garcia Arnaiz, �tait en route pour la base lundi soir, de m�me que le ministre de la D�fense Pedro Morenes, a pr�cis� un porte-parole � l'AFP.
Une �cole de pilotes prestigieuse
Selon le site internet de la base de Los Llanos, celle-ci accueille depuis 2010 un centre de formation de l'OTAN.
�C'est une �cole de pilotes, qui accueille dix nationalit�s�, a pr�cis� un porte-parole du minist�re. Le centre y forme des pilotes � diverses sp�cialit�s y compris la guerre �lectronique, la reconnaissance ou le combat a�rien.
Selon le site du minist�re fran�ais de la D�fense, le TLP est l'une des formations �les plus r�put�es et les plus exigeantes au monde� pour les pilotes de chasse. Les pilotes y sont d�ploy�s pour ces cours avec leurs appareils, qu'il s'agisse de Mirage ou encore de F-16.
�Je suis profond�ment attrist� par l'accident d'un avion de combat grec sur la base de Los Llanos, qui a caus� de nombreuses victimes�, a d�clar� dans un communiqu� le secr�taire g�n�ral de l'OTAN Jens Stoltenberg sans livrer davantage de d�tails.
�C'est une trag�die qui touche l'ensemble de la famille de l'OTAN�, a-t-il ajout� en adressant ses condol�ances aux familles des victimes.
#Espagne > Un F16 grec s'�crase apr�s son d�collage de la base a�rienne de Los Llanos � #Albacete (via @PopularTVCLM ) pic.twitter.com/YFbXBAmSIo
