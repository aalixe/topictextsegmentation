TITRE: La Turquie veut bloquer les pages Facebook « insultant » Mahomet, Afrique - Moyen Orient
DATE: 26-01-2015
URL: http://www.lesechos.fr/monde/afrique-moyen-orient/0204108701513-la-turquie-veut-bloquer-les-pages-facebook-insultant-mahomet-1087020.php
PRINCIPAL: 0
TEXT:
La Turquie veut bloquer les pages Facebook � insultant � Mahomet
Le 26/01 � 17:18
La Turquie veut bloquer les pages Facebook � insultant � Mahomet - CLOSON DENIS/ISOPIX/SIPA
1 / 1
La caricature du proph�te publi�e par l�hebdomadaire Charlie Hebdo dans son dernier num�ro apr�s les attentats avait d�j� �t� interdite par un tribunal turc.
La justice turque a ordonn� � Facebook d�interdire les pages dont le contenu constitue une � insulte � � l�image du proph�te Mahomet et menac� d�interdire l�acc�s au r�seau social en Turquie s�il n�obtemp�rait pas, ont rapport� lundi les m�dias turcs.
Rendue dimanche soir, l�injonction du tribunal d�Ankara a �t� communiqu�e � l�autorit� administrative en charge des t�l�communciations (TIB) et aux fournisseurs d�acc�s, a pr�cis� l�agence de presse gouvernementale Anatolie.
Il y a quinze jours, un tribunal de Diyarbakir avait d�j� interdit � la diffusion sur Internet la reproduction d�une caricature du proph�te publi�e par l�hebdomadaire satirique fran�ais Charlie Hebdo apr�s l�attaque jihadiste qui l�a vis� le 7 janvier, faisant 12 morts.
Enqu�te contre deux journalistes turcs
Une enqu�te judiciaire a par ailleurs �t� ouverte � Istanbul contre deux journalistes du quotidien d�opposition Cumhuriyet qui avaient illustr� leurs �ditoriaux par le m�me dessin de Mahomet, jug� outrageant dans le monde islamique. Sur ce dessin de Luz, un Mahomet la larme � l�oeil tient une pancarte � Je suis Charlie �, le slogan des millions des manifestants qui ont d�fil� en France et � l��tranger pour condamner les attaques jihadistes qui ont fait 17 morts en trois jours � Paris.
L�hiver dernier, le gouvernement islamo-conservateur avait d�j� bloqu� momentan�ment l�acc�s aux r�seaux sociaux YouTube ou Twitter pour y emp�cher la diffusion d�enregistrements pirates de conversations t�l�phoniques mettant en cause l�actuel chef de l�Etat Recep Tayyip Erdogan et son entourage dans un scandale de corruption. Ces d�cisions ont valu � Erdogan, � la t�te de la Turquie depuis 2003, de vives critiques dans le monde entier.
Le pouvoir turc avait dans la foul�e fait voter une loi facilitant le blocage administratif des sites internet mais ce texte avait �t� censur� par la Cour constitutionnelle.
Des d�put�s du parti au pouvoir viennent toutefois de d�poser au Parlement un amendement � un projet de loi fourre-tout qui autoriserait le Premier ministre et certains ministres � fermer un site, sans d�cision de justice, au nom de la s�curit� nationale et de la protection de la vie priv�e. Ce nouveau texte doit �tre discut� cette semaine en s�ance pl�ni�re � l�Assembl�e.
Source AFP
