TITRE: ��La BD est Charlie�� : l�album-hommage pour que vive la libert� d�expression ! | Actu-Mag.fr
DATE: 26-01-2015
URL: http://www.actu-mag.fr/2015/01/26/la-bd-est-charlie-lalbum-hommage-pour-que-vive-la-liberte-dexpression/
PRINCIPAL: 1225832
TEXT:
Partager�sur�Facebook Partager�sur�Twitter
Ce mercredi 7 janvier 2015�restera au vu de son d�nouement macabre au sein de�Charlie Hebdo�grav� dans la m�moire collective.
Nombreux sont les pairs des 4 caricaturistes tragiquement disparus (Cabu, Charb, Tignous, Wolinski)�qui ont voulu leur rendre un�hommage � leur image � savoir un recueil collectif de 183 dessins intitul� La BD est Charlie.
Les b�n�fices de cet ouvrage -o� tous sont unis pour un m�me dessein : celui de hisser haut le�pavillon un temps en berne de la libert� d�expression- seront revers�s dans leur int�gralit� aux familles ayant perdu des proches�� la fois�victimes des fr�res Kouachi et de Coulibaly.
La BD est Charlie. Les b�n�fices de cet ouvrage seront revers�s aux 17 familles des victimes. #JeSuisCharlie pic.twitter.com/TRxvpEh6sJ
� �ditions Delcourt (@DelcourtBD) 26 Janvier 2015
C�est donc un magnifique t�moignage d�altruisme et�une ultime�r�v�rence tir�e � leurs confr�res d�funts�auxquels se sont associ�s de concert�et sans escompter aucune r�tribution, 36 maisons d��ditions comme Gl�nat, Delcourt ou Dupuis, et presque�170 auteurs comme Zep, Geluck, Tine ou encore Gabs.
Le tirage est fix�� 100 000 exemplaires !
La BD est Charlie�sortira le 5 f�vrier et les plus amateurs de bulles qui auront la chance de se rendre au Festival de la bande dessin�e d�Angoul�me du 29 janvier au 1er f�vrier pourront en avoir la primeur !
Cr�dits Photo : Creative Commons
