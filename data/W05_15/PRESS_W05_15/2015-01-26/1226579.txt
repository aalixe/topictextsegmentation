TITRE: VIDEO.Mondial de handball : la France �crase l'Argentine (33-20) et file en quarts
DATE: 26-01-2015
URL: http://www.leparisien.fr/sports/autres/mondial-de-handball-la-france-ecrase-l-argentine-33-20-et-file-en-quarts-26-01-2015-4480875.php
PRINCIPAL: 1226578
TEXT:
Autres sports
VIDEO.Mondial de handball : la France �crase l'Argentine (33-20) et file en quarts
Les Bleus de Claude Onesta n'ont laiss� aucune chance � l'Argentine lundi en huiti�mes de finale du Mondial, l'emportant largement (33-20). Ils retrouveront la Slov�nie en quarts de finale.
26 Janv. 2015, 20h49 | MAJ : 27 Janv. 2015, 11h49
r�agir
4
La France, port�e par une d�fense extraordinaire devant un Thierry Omeyer de gala, s'est qualifi�e pour les quarts de finale du Mondial-2015 messieurs de handball en �c�urant l'Argentine (33-20), lundi � Doha.
AFP / Marwan Naamani
La France , port�e par une d�fense extraordinaire devant un Thierry Omeyer de gala, s'est qualifi�e pour les quarts de finale du Mondial-2015 messieurs de handball en �c�urant l'Argentine (33-20), lundi � Doha, au Qatar.
Sur le m�me sujet
Championnat du monde de hand : la France s'offre un 8e face � l'Argentine
Cette victoire est un grand pas pour les champions d'Europe sur la voie des JO-2016 � Rio. Elle leur garantit quasiment une place dans un Tournoi de qualification olympique qui devrait ensuite n'�tre qu'une formalit� pour eux.
Les Fran�ais affronteront mercredi la Slov�nie, 4e de la pr�c�dente �dition en 2013 et emmen�e par l'ailier droit de Montpellier Dragan Gajic, le meilleur buteur du Mondial avant cette journ�e (52 buts en six matchs). Les Bleus ont encore fait preuve de leur capacit� � g�rer les rendez-vous importants. Depuis 2006, ils ont remport� 20 des 24 matches � �limination directe qu'ils ont jou�s sur les trois comp�titions majeures (JO, Mondial, Euro ).
13 victoires des Bleus en 13 matchs face aux Argentins
La France, qui avait gagn� ses 12 confrontations avec l'Argentine jusque-l�, n'a gu�re souffert pour s'offrir la 13e. Les Sud-Am�ricains, qui avaient d�tonn� en sortant d'un groupe tr�s relev� en phase de poules, ont �t� pris � la gorge d'entr�e et se sont � peine d�battus. Apr�s leur entame de match rat�e contre la Su�de (25-27), les Bleus ont rectifi� le tir, avec emphase. Omeyer (9 arr�ts � la pause) les a mis en confiance en �cartant les quatre premiers tirs argentins.
Monstrueux en d�fense, les Fran�ais ont d'abord connu un peu de d�chet aux tirs, ce qui les a emp�ch�s d'imm�diatement s'envoler (5-2, 11e). Mais au fil des minutes, les Argentins sont apparus de plus en plus d�sempar�s. Le petit g�nie Diego Simonet mis sous l'�teignoir, ils n'ont jamais pu prendre � revers la d�fense fran�aise, m�me en sup�riorit� num�rique. Omeyer a continu� � lire comme dans un livre ouvert leurs tirs et l'�cart a enfl� (11-4, 24e).
Claude Onesta a acc�l�r� le rythme de ses rotations. D'une interception conclue de l'autre c�t� du terrain, Luka Karabatic a symbolis� cette premi�re p�riode, marqu�e par la totale sup�riorit� des Bleus (16-6, 30e). En marquant trois buts en quelques minutes, Valentin Porte a donn� une claire indication � l'Argentine que les Bleus n'avaient pas l'intention de desserrer l'�treinte (20-8, 37e). Mathieu Gr�bille, que son s�lectionneur, tenait absolument � relancer apr�s un d�but de Mondial compliqu�, l'a rassur� en montrant toute la palette de son talent avec deux buts rapides, l'un de l'aile, l'autre � 9 m (26-11, 42e).
Malgr� les rotations innombrables, les Fran�ais ont continu� � se montrer tr�s s�rieux jusqu'� la derni�re seconde, sans oublier de s'amuser, tous finissant par marquer.
VIDEO. Les Experts d�collent enfin
