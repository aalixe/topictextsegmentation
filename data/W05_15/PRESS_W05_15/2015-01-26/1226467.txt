TITRE: Crash d'un chasseur F-16 grec en Espagne : 10 morts, dont 8 militaires français, Aéronautique - Défense
DATE: 26-01-2015
URL: http://www.lesechos.fr/industrie-services/air-defense/0204109584049-10-morts-et-13-blesses-dans-le-crash-dun-f-16-grec-en-espagne-1087058.php
PRINCIPAL: 0
TEXT:
Crash d'un chasseur F-16 grec en Espagne�: 10 morts, dont 8 militaires fran�ais
Les Echos |
Le 26/01 � 19:17, mis � jour le 27/01 � 06:13
Le F-16 s�est �cras� peu apr�s avoir d�coll� d�un centre d�entra�nement � Albacete - Capture d��cran Vine
1 / 1
+ VIDEO - Huit aviateurs Fran�ais et deux Grecs sont morts dans l�accident � Albacete. Six autres militaires fran�ais sont gravement br�l�s.
Le chef du gouvernement espagnol Mariano Rajoy a annonc� lundi soir que huit Fran�ais et deux Grecs sont morts dans l'accident d'un avion de combat F-16 de la force a�rienne grecque sur une base situ�e non loin d'Albacete, � quelque 240 km au sud-est de Madrid. "Il semble qu'il y a deux personnes qui sont d�c�d�es qui sont de nationalit� grecque et huit Fran�ais", a d�clar� Mariano Rajoy, interrog� en direct sur la cha�ne priv�e Telecinco, en d�taillant le bilan de dix morts. Le chef du gouvernement a pr�cis� qu'il tenait ces informations du ministre espagnol de la D�fense, Pedro Morenes, ajoutant qu'il y avait "pas mal de bless�s".
"Selon les premiers �l�ments fournis par les autorit�s espagnoles, nous d�plorons la mort de huit Fran�ais", a d�clar� le minist�re fran�ais de la D�fense dans un communiqu� . "Un bless� est en situation d'extr�me urgence. Cinq autres sont en r�animation dont deux plac�s en coma artificiel", ajoute le communiqu�, pr�cisant que "trois autres bless�s l�gers sont en sortie d'h�pital".
De son c�t�, Fran�ois Hollande a adress� ses condol�ances aux familles des huit aviateurs d�c�d�s. Le pr�sident de la R�publique "a appris la mort de huit aviateurs (...)", a affirm� l'Elys�e dans un communiqu� de presse o� il est pr�cis� que "cet accident a caus� par ailleurs de graves blessures par br�lures � six personnels m�caniciens selon le bilan actuel pr�sent� par les autorit�s espagnoles".
. @fhollande adresse ses condol�ances aux familles des huit aviateurs d�c�d�s sur la base d�Albacete en Espagne pic.twitter.com/ZUhsSgkbPj
Jean-Yves Le Drian se "rendra sur les lieux du drame demain apr�s-midi", a ajout� la D�fense dans son communiqu�. Mariano Rajoy a �galement annonc� qu'une ministre italienne se rendra, mardi, sur la base espagnole o� a eu lieu l'accident.
Entra�nement organis� par l'OTAN
L'avion "s'est �cras� sur un des parkings de la base o� �taient stationn�s des avions de chasse de plusieurs nationalit�s, dont deux Alpha Jet, deux Mirage 2000D et deux rafale fran�ais", avait pr�cis�, plus t�t ce lundi soir, le minist�re fran�ais de la D�fense dans un premier communiqu� .
L'avion de combat de la force a�rienne grecque devait effectuer des manoeuvres dans le cadre d'un entra�nement organis� par l'OTAN, le Tactical leadership Programme (TLP), et s'est �cras� au d�collage, selon un communiqu� diffus� plus t�t par le minist�re de la D�fense. Il s'est apparemment �cras� sur le tarmac, heurtant d'autre a�ronefs et tuant d'autres personnes qui s'y trouvaient.
Lundi apr�s-midi, des cha�nes de t�l�vision espagnoles ont diffus� quelques secondes d'images o� on aper�oit un avion en feu, d'o� s'�chappent d'importantes volutes de fum�e noire. Les �quipes de secours ont d� venir � bout de l'incendie entra�n� par le crash sur l'aire de stationnement avant de pouvoir d�terminer le nombre de victimes.
Focus
