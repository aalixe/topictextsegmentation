TITRE: Temp�te de neige � New York : des milliers de vols annul�s
DATE: 26-01-2015
URL: http://www.europe1.fr/international/new-york-se-prepare-a-une-tempete-historique-2354273
PRINCIPAL: 1224920
TEXT:
Temp�te de neige � New York : des milliers de vols annul�s
Europe 1Barth�l�my Gaillard et C. Rainfroy avec AFP
Publi� � 13h13, le 26         janvier  2015
, Modifi� � 23h19, le 26         janvier  2015
Dossiers :
Par Barth�l�my Gaillard et C. Rainfroy avec AFP
0
0
0
M�T�O -Le maire de New York a mis en garde ses administr�s contre la temp�te de neige qui devrait bient�t recouvrir la ville de 60 centim�tres de neige.
L'info. La Grosse Pomme est une habitu�e de la neige, mais cette fois-ci, � en croire Bill De Blasio , son maire, New York devrait subir l'une des temp�tes "les plus importantes de son histoire". Le nord-est des Etats-Unis se pr�pare en effet � �tre battu par les vents et recouvert d'un �pais manteau de neige lundi et mardi.
Plus de 5.000 vols supprim�s. Cons�quence, des milliers de vols ont �t� annul�s. Lundi, plus de 2.600 vols ont �t� supprim�s depuis ou vers les Etats-Unis pour la journ�e de lundi. Et plus de 3.000 annulations sont � pr�voir pour la journ�e de mardi, selon le site sp�cialis� Flightaware . Selon les autorit�s, la moiti� des vols ont notamment �t� annul�s � l'a�roport international John F. Kennedy � New York. Au total, plus de 5.000 vols ont �t� annul�s dans le nord-est du pays, souligne la correspondante d'Europe1 aux Etats-Unis :
D�j� 5200 vols annul�s dans le NE des �tats-Unis, en pr�vision de la temp�te. #newyorkstorm
� G�raldine Woessner (@GeWoessner) 26 Janvier 2015
Jusqu'� 60 centim�tres attendus � New York. La neige tombait d�j� drue lundi en d�but d'apr�s-midi � New York, o� des centaines de chasse-neige �taient mobilis�s. Les chutes de neige les plus fortes sont attendues en fin d'apr�s-midi et durant la nuit. Selon la m�t�o nationale, la neige pourrait alors tomber au rythme de 5 � 10 cm par heure, avec une accumulation attendue de 45 � 60 cm, voire plus en certains endroits. La temp�te devrait s'�loigner progressivement mercredi.
� G�raldine Woessner (@GeWoessner) 26 Janvier 2015
Appels � la prudence. Le maire de New York, Bill de Blasio, a donc mis en garde les habitants, leur demandant de rester � l'int�rieur s'ils le peuvent. "Ne sous-estimez pas cette temp�te", a-t-il insist�, pr�cisant qu'elle pourrait �tre l'une des plus importantes jamais affront�es par la ville de 8,4 millions d'habitants. Il a pr�cis� que les parcs allaient fermer � 18h, et qu'aucune voiture, hormis les v�hicules d'urgence, ne serait autoris�e � circuler apr�s 23h. Les �coles seront ferm�es mardi, a-t-il ajout�. Le gouverneur de l'Etat de New York a de son c�t� annonc� que les trains de banlieue s'arr�teraient � 23h, et que le m�tro serait "limit�" � partir de 19h ou 20h.
Des supermarch�s pris d'assaut. Ces messages de prudence sont visiblement pass�s, puisque de nombreux supermarch�s new-yorkais ont �t� pris d'assaut par des habitants inquiets. Dans le quartier de Chelsea, au centre de Manhattan, une file d'attente s'�tait form�e sur le trottoir pour pouvoir entrer dans le magasin Trader Joe's sur la 6e avenue.
Les rayons des grandes surfaces vid�s par les New Yorkais.
From a close friend of mine in New York City as people prepare for the worst. #blizzardof2015 pic.twitter.com/lV3seFJmUO
� Rusty Dawkins (@rustywx) 26 Janvier 2015
50 millions de personnes concern�es. De Philadelphie, en Pennsylvanie), au Maine, plus de 50 millions de personnes pourraient �tre affect�es par cette temp�tede neige qui risque de paralyser les transports. L'Etat du Connecticut a ainsi annonc� une interdiction de voyager dans tout l'Etat � partir de 21h lundi soir. A Boston, o� jusqu'� 76 cm de neige sont attendus, le maire a appel� ses concitoyens � s'occuper de leurs voisins les plus vuln�rables. Les �coles devraient aussi y rester ferm�es mardi.
Plusieurs webcams, dont une plac�e dans le c�l�bre parc new-yorkais de Central Park, diffuse sur NBC les images de la neige :
La ville a d�j� subi des temp�tes de grande violence, comme sur cette photo, en 1888
It could be worse this weekend @CindiDayton " @ThislsAmazing : The Great Blizzard in New York, 1888: pic.twitter.com/7NR5ruMenE "
� USD Swim & Dive (@USDswim_dive) 24 Janvier 2015
Ou ici, en 1947.
