TITRE: David Trezeguet: la carri�re de "Trezegol" en cinq buts - L'Express
DATE: 26-01-2015
URL: http://www.lexpress.fr/actualite/sport/football/david-trezeguet-la-carriere-de-trezegol-en-cinq-buts_1643552.html
PRINCIPAL: 1223844
TEXT:
Turin, 11/03/2010. David Trezeguet, apr�s son but avec la Juventus Turin contre Fulham.
REUTERS/Alessandro Garofalo
"Trezegol" ne marquera plus de buts. A l'exception de l'�ternel pr�-retrait� Robert Pir�s (8 matches pro seulement depuis 2011), David Trezeguet, 37 ans, est le dernier champion du monde de 1998 � prendre sa retraite . Une fin de carri�re, comme joueur, qui intervient quelques semaines apr�s celle de l'autre jeunot de cette bande en or, Thierry Henry .�
18 mars 1998. Son but missile contre Manchester United
Arriv� � l'�t� 1995 � l'AS Monaco � 17 ans, David Trezeguet devient titulaire la saison de la Coupe du monde organis�e en France. Sur le terrain de Manchester United, il marque les esprits et le but qui qualifie l'ASM en demi-finales de la Ligue des Champions. La frappe est alors chronom�tr�e � 154,4 km/h, record de la comp�tition. 2e meilleur buteur de Ligue 1 derri�re St�phane Guivarc'h en Division 1, le franco-argentin est retenu par Aim� Jacquet pour disputer la Coupe du monde 1998.�
�
�
3 juillet 1998. Sang-froid aux tirs au but en quarts
Plus souvent rempla�ant que titulaire, Trezeguet r�alise le mondial qu'on attend de lui : apr�s son premier but international contre l'Arabie Saoudite, il provoque un p�nalty transform� par Youri Djorkaeff face au Danemark et r�alise une passe d�cisive pour Laurent Blanc contre le Paraguay. Lors du quart de finale contre l'Italie, il est le 3e tireur fran�ais des tirs au but. Alors que Zidane n'a pas flanch� et que Lizarazu s'est rat�, il marque avec sang-froid, avant Henry et Blanc. Le rat� de Di Biago envoie la France en demie.�
�
�
2 juillet 2000. Le but de sa vie
C'est gr�ce � un but de David Trezeguet que la France devient la premi�re nation du monde � encha�ner une victoire � l'Euro apr�s un titre Mondial. Un but inesp�r� de Sylvain Wiltord � quelques secondes de la fin du match, permet aux Bleus d'arracher les prolongations, alors sous le r�gime du but en or. C'est Trezeguet qui le marque � la 103e, d'une demi-vol�e sur un centre de Robert Pir�s.�
�
�
14 mai 2003. Un but pour une finale europ�enne
Battu 2-1 � l'aller par le Real Madrid des "Galactiques", la Juventus Turin r�alise l'un des plus grands matches de son histoire, � domicile, pour une place en finale. Dans son style typique de "renard des surfaces", Trezeguet ouvre le score de 3-1 d'une reprise, apr�s une passe de la t�te d'Alessandro Del Piero. Pour la seule finale de Ligue des Champions qu'il va disputer ensuite avec la "Juve", il rate son tir au but contre l'AC Milan. Un cauchemar qui se r�p�te trois ans plus tard en finale du Mondial 2006 avec les Bleus.�
�
�
26 octobre 2014. Automne indien avec le FC Pune
Dernier tour de terrain. En Inde, vient d'�tre lanc� le concept sportivo-marketing de l' Indian Super League . David Trezeguet s'engager avec le FC Pune et marque � deux reprises, dont la premi�re fois contre le FC Goa. Depuis son d�part de la Juventus, en 2010, apr�s une d�cennie de bons et loyaux services qui ont fait de lui le meilleur buteur �tranger de l'histoire du club, Trezeguet a beaucoup voyag� : une honorable saison chez le promu en Liga espagnole Hercules CF, sans emp�cher une rel�gation, une pige tr�s bien pay� dans le club �mirati de Baniyas, avant trois saisons � River Plate et au Newell's Old Boys en Argentine, pays de coeur o� il a jou� ses premiers matches pros (CA Platense en 1993).�
�
