TITRE: C�sar d�honneur pour l�acteur et r�alisateur am�ricain Sean Penn - sudinfo.be
DATE: 26-01-2015
URL: http://www.sudinfo.be/1198898/article/2015-01-26/cesar-d-honneur-pour-l-acteur-et-realisateur-americain-sean-penn
PRINCIPAL: 0
TEXT:
Reuters
Sean Penn va recevoir un C�sar d�honneur
�� Acteur mythique, personnalit� engag�e, r�alisateur d�exception, Sean Penn est une ic�ne � part dans le cin�ma am�ricain. Une l�gende de son vivant �� souligne l�Acad�mie dans un communiqu�.
�g� de 54 ans, Sean Penn a obtenu le prix d�interpr�tation masculine au Festival de Cannes en 1997 pour son r�le dans le film de Nick Cassavetes �� She�s So Lovely ��.
Son r�le de p�re endeuill� assoiff� de vengeance dans �� Mystic River �� de Clint Eastwood lui vaut, en 2004, son premier Oscar de meilleur acteur.
Il r�colte en 2009 une seconde statuette pour son interpr�tation de politicien militant pour la cause homosexuelle dans �� Harvey Milk �� de Gus Van Sant.
Il est pass� derri�re la cam�ra d�s 1991 avec un premier long m�trage �� The Indian Runner �� qui sera suivi de quatre autres films dont �� Into The Wild �� qui, en 2007, lui vaut une reconnaissance internationale. L�ann�e suivante, il est d�sign� pour pr�sider le Festival de Cannes.
Sean Penn va recevoir un C�sar d'honneur http://t.co/RywoDAG6h1 pic.twitter.com/X10Ps7pBBp
Faites de sudinfo.be la page d'accueil de votre navigateur. C'est facile. Cliquez-ici.
liens commerciaux
Les + lus
