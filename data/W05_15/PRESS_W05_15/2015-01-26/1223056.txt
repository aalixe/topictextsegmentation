TITRE: Gr�ce : des n�gociations difficiles en vue avec l'UE
DATE: 26-01-2015
URL: http://www.europe1.fr/international/l-ue-se-prepare-a-negocier-avec-syriza-2353561
PRINCIPAL: 0
TEXT:
Gr�ce : des n�gociations difficiles en vue avec l'UE
Europe 1No�mi Marois et Julien Ricotta avec AFP
Publi� � 23h47, le 25         janvier  2015
, Modifi� � 14h15, le 26         janvier  2015
Dossiers :
Par No�mi Marois et Julien Ricotta avec AFP
0
0
4
L�GISLATIVES GRECQUES - Le parti Syriza, vainqueur des l�gislatives dimanche, souhaite restructurer sa dette mais les n�gociations avec ses partenaires europ�ens, notamment allemands, risquent d'�tre ardues.
L'Union europ�enne, face � la large victoire de Syriza aux l�gislatives grecques , va devoir composer avec son leader Alexis Tsipras, d�sormais nouvel homme fort du pays. Alors que ce parti d'extr�me-gauche pr�ne l'anti-aust�rit�, l'UE va devoir choisir entre l'affrontement avec la Gr�ce ou bien des concessions difficiles � accepter pour certain pays, comme l'Allemagne. Le Royaume-Uni a, pour sa part, d�j� annonc� son pessimisme. Son Premier ministre, David Cameron, a estim� dimanche que la victoire de Syriza en Gr�ce, allait augmenter "l'incertitude �conomique en Europe".
>> LIRE AUSSI - Gr�ce : Syriza veut rassurer ses voisins europ�ens
Des "moments de tension" sur la dette. Si une sortie de la zone euro semble d�finitivement exclue par Syriza, Alexis Tsipras r�clame une restructuration de la dette, qui repr�sente aujourd'hui 177% du PIB grec. Mais pour obtenir satisfaction, le nouveau gouvernement grec devra d'abord convaincre ses partenaires europ�ens. Il faut donc s'attendre � des "moments de tension" entre Bruxelles et Ath�nes dans les prochaines semaines , estime un haut fonctionnaire europ�en. Car l'UE est pr�te � n�gocier, mais � une seule condition : que la Gr�ce poursuive ses r�formes. Exactement ce dont ne veut plus Alexis Tsipras...
>> LIRE AUSSI : Gr�ce, quelles sont les �ch�ances � venir ?
La France expos�e � hauteur de 40 milliards d'euros. Depuis le d�but de la crise grecque, les cr�anciers internationaux ont pr�t� pr�s de 240 milliards d'euros � la Gr�ce. Parmi les bailleurs de fond : les Etats de l'UE, dont la France. Paris serait expos� � hauteur de 40 milliards d'euros en cas de d�faut du pays du sud de l'Europe, selon Beno�t Coeur�, membre du directoire de la BCE. Une somme qui explique peut-�tre l es prudentes f�licitations adress�es par Fran�ois Hollande � Alexis Tsipras.
Une premi�re solution : accorder un nouveau d�lai. Les partenaires europ�ens de la Gr�ce sont pr�ts � donner un d�lai au nouveau gouvernement grec avant de pouvoir n�gocier sereinement sur son plan d'aide, a assur� le ministre fran�ais des Finances, Michel Sapin. Une extension de six mois du programme actuel, d�j� prolong� en d�cembre jusqu'� fin f�vrier, est envisageable comme premi�re r�ponse, a-t-on indiqu� de source europ�enne. "On ne va pas �chapper � une ren�gociation, la question est sur quoi va-t-elle porter : les �ch�ances, les montants, les deux", a confi� dimanche une source europ�enne � Bruxelles. "Pour les montants ce sera le plus difficile", a-t-elle ajout�.
>> ECOUTER AUSSI - Gr�ce : "la dette est irremboursable"
� ODD ANDERSEN / AFP
Allemagne VS Gr�ce. La position de l'Allemagne sera d�terminante. La victoire de Tsipras "mine la politique actuelle organis�e sur des principes allemands", a soulign� dimanche Julian Rappold, de l'Institut allemand de politique �trang�re. Il faut donc s'attendre � des moments tendus entre Bruxelles et Ath�nes dans les prochaines semaines, estime un haut fonctionnaire europ�en. Pour le moment, le gouvernement allemand �carte toute ren�gociation de la dette. Mais Berlin pourrait �tre contraint de n�gocier pour sortir de l'impasse.
R�union � Bruxelles lundi. L'Allemagne aura en tout cas un alli� de poids : la Banque Centrale Europ�enne. Beno�t Coeur�, membre du directoire de la BCE, a �t� clair lundi matin sur Europe 1 : "La Gr�ce doit payer, ce sont les r�gles du jeu europ�en". L'institution de Francfort n'accordera donc aucun rabais au pays, qui devra lui rembourser deux pr�ts de plus de 3 milliards d'euros cet �t� . Mais Ath�nes a la possibilit� de n�gocier un all�gement de sa dette avec ses partenaires europ�ens. Lundi, les ministres des Finances de la zone euro se r�uniront � Bruxelles, principalement au sujet de la Gr�ce. Cette session de travail donnera d�j� un premier indice : jusqu'o� l'UE est pr�te � aller pour trouver un terrain d'entente avec le nouveau pouvoir � Ath�nes.
