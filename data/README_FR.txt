=============================================================================

        Package FrNewsLink : ressources pour l'évaluation des tâches de
        segmentation et titrage thématique en français

==============================================================================
  
Ce fichier décrit de façon synthétique le package FrNewsLink comprenant un ensemble de ressources permettant d'évaluer des tâches de segmentation thématique et titrage sur un corpus varié de journaux télévisés français. En raison de droits de diffusions, ce package ne contient ni les vidéos, ni les audios des journaux télévisés.

Le corpus propose à la fois des transcriptions automatiques de journaux télévisés et des articles de presses issus du web, le tout collecté durant la même période de temps afin que journaux et articles traitent des mêmes sujets d'actualités.

Les ressources de FrNewsLink s'appuient sur 86 journaux télévisés enregistrés durant la 7e semaine de 2014 (du 10 au 16 février) et 26 journaux enregistrés les 26 et 27 janvier 2015. La première période sera référencée par W07_14 et la seconde W05_15.

Ces journaux proviennent de 8 chaines différentes, avec un total de 14 émissions différentes puisque certaines chaines proposent plusieurs journaux dans la journée. 

Ainsi on retrouvera, selon les jours, les émissions suivantes : 
    - Arte :  Le journal
    - D8 : Le JT
    - Euronews
    - France 2, les journaux de : 7h, 8h, 13h, 20h
    - France 3 : Le 12-13, Le 19-20
    - M6 : Le 12-45, Le 19-45
    - NT1 : Les infos
    - TF1, les journaux de : 13h, 20h

Durant la même période les articles parus sur la page principale de Google News ont été aspirés dans notre base de données toutes les heures. Ainsi, 28 709 entrées ont été saisies avec en moyenne 2,7k articles par jour. Plusieurs articles restent plus d'une heure sur le site et sont donc enregistrés plusieurs fois dans notre base. Lorsque l'on écarte les articles doublons, 22 141 articles sont restants. 
Google News présente ses articles sous forme de "clusters thématiques" avec un article principal mis en avant pour chacun des clusters. Seuls ces articles principaux, environ 590 par jour, ont été considérés pour notre titrage thématique.

0. Références et citation
=========================
Ce corpus a fait l'objet d'une publication à la conférence LREC 2018. Vous trouverez dans l'article de plus amples informations et un ensemble de statistiques.

Merci de citer ce papier si vous publiez sur FrNewsLink :

@inproceedings{CamelinLREC18,
  TITLE = {{FrNewsLink : a corpus linking TV Broadcast News Segments and Press Articles}},
  AUTHOR = {Camelin, Nathalie and Damnati, Géraldine and Bouchekif, Abdessalam and Landeau, Anais and Charlet, Delphine and Est{\`e}ve, Yannick},
  BOOKTITLE = {{LREC 2018}},
  ADDRESS = {Miyazaki, Japan},
  YEAR = {2018},
  MONTH = May,
  KEYWORDS = {content linking, semantic textual similarity, topic segmentation}
}

  
1. Contenu du package et version
=================================

== Contenu
Le package est structuré de la façon suivante:

<ROOT_DIR>
  |----README_EN.TXT            documentation en anglais
  |----README_FR.TXT            documentation en français
  |----RELEASE.TXT              version
  |----<W0X_XX>                 2 répertoires : W07_14 et W05_15, contenant chacun les données suivantes :
          |----<TV_W0X_XX>                     répertoire contenant les données sur les journaux
          |      |-----*.ctm                        mots issus de la transcription automatique 
          |      |-----*.stm                        transcriptions automatiques organisées selon la diarization automatique
          |----<PRESS_W0X_XX>                       répertoire contenant les articles de web journaux
          |      |-----bdd_20XX_XX_XX_articles.csv      base de données de tous les articles web 
          |      |----<20XX_XX_XX>                      un répertoire par jour
          |      |      |-----*.txt                     textes des articles web niveau 0
          |----<ANNOTATIONS>     répertoire contenant les annotations manuelles
          |      |-----*.trs        segmentation thématique manuelle             
          |      |-----*.assgn      association manuelle entre articles et segments des vidéos             

== Version
Ce package est la version 00, mise à disposition le 1/11/2017 sur le site du LIUM : https://lium.univ-lemans.fr/frnewslink/


2. Formats de fichiers
======================

 == fichiers ctm, transcriptions automatiques
Chaque ligne correspond à l'hypothèse d'un mot énoncé dans un journal. Pour chaque hypothèse de mot, on associe un ensemble de champs, chacun séparé par un espace, qui représentent les informations suivantes : 
nom du journal canal (valeur par défaut 1) deb_seg duree_mot mot mesure_confiance

 = deb_seg représente le temps de début du segment 
 
 == fichiers stm, transcriptions selon la diarization automatique
Les mots des fichiers ctm sont regroupés selon les groupes de souffles. Pour chaque groupe de souffles, on associe un ensemble de champs, chacun séparé par un espace, qui représentent les informations suivantes : 
nom_du_document 1 locuteur deb_seg fin_seg <caractériques_locuteurs> liste hypothèses de mot
 
 = locuteur est représenté par le nom du cluster du locuteur (ex : S0, S19...). Attention, le clustering de locuteurs a été fait fichier par fichier de manière indépendante. Ainsi le locuteur S0 du fichier 1 ne représente pas la même personne que le locuteur S0 du fichier 2
 = fin_seg représente le temps de fin du segment
 = <caractériques_locuteurs> contient un triplet de valeur dont la dernière représente le sexe du locuteur (trouvé automatiquement)


 == fichiers csv, base de données de tous les articles web
Pour chaque jour d'enregistrements des différents journaux, la base de données aspirant les informations de Google News toutes les heures est disponible. Google News présente ces articles par "clusters thématiques" et pour chaque cluster met en avant un article. Nous notons cet article comme principal. 
Pour chaque article, les informations suivantes sont disponibles : 
identifiant; id_categorie; id_site; id_article_principal; date_heure_aspiration; url; titre

 = identifiant : L'identifiant permet de retrouver le texte de l'article web, aspiré et traité par Boilerpipe dans le même repétertoire et sous le nom identifiant.txt.
Attention, seuls les articles principaux sont disponibles, les articles associés n'ont pas été traités.

 = id_categorie : Les catégories correspondent à celles proposés par Google News :
    1 Une
    2 International
    3 National
    4 Économie
    5 Sciences High Tech
    6 Culture
    7 Sport
    8 Santé
    9 Gros Plan

 = id_site : Il serait trop long de lister tous les sites aspirés (5 322 entrées!), notons par exemple :
    56	www.lemonde.fr	
	57	www.metrofrance.com	
    58	www.lesechos.fr	
    59	www.google.com	
    60	www.toulouse7.com	
    61	www.lejdd.fr	
    62	www.elle.fr	
    63	www.lefigaro.fr	
    64	www.lexpress.fr	
    65	www.letelegramme.com

 = id_article_principal : ce champ contient soit la valeur 0 si l'article est le principal de son cluster thématique, soit l'identifiant de l'article principal auquel il est associé.
 
 = date_heure_aspiration : ce champ contient la date JOUR/MOIS/ANNEE HEURE:MINUTE à laquel l'article a été enregistré.
 
 = url et titre : ces champs contiennent l'adresse web d'origine de l'article et le titre indiqué sur la page de Google News.


 == fichiers txt, le contenu textuel utile des articles de presses
 Dans chaque fichier identifiant.txt, on retrouve le contenu utile extrait par Boilerpipe ainsi que le rappel des informations : 
    TITRE: [...]
    DATE: [...]
    URL: [...]
    PRINCIPAL: 0
    TEXT:
    [...]


 == fichiers trs, segmentation thématique manuelle
 Ces fichiers au format xml peuvent s'ouvrir avec le logiciel transcriber. 

 = <Section> : Les bornes des segments thématiques se trouvent dans la balise <Section> grace aux attributs *startTime* et *endTime*. L'indice du thème associé est alors indiqué dans l'attribut *topic*. 

 = <Topics> : La liste des thèmes est indiqué en début de fichier dans les balises Topic. Concernant le corpus W07_14, ils sont pour la plupart simplement numéroté ainsi : "Sujet1", "Sujet2", "Sujet3"... 
 Certains sujets plus présents lors des périodes d'enregistrements ont eux été mis en avant. Par exemple pour le corpus W07_14 : "Intempérie1", "Intempérie2", "Sport", "Vacances", "Société", ... ou pour le corpus W05_15 : "Auschwitz", "Grêce1", "Grêce2"... Les numéros 1, 2, 3... ajoutés par exemples aux sujets "Intempéries", "Sport", "Grêce" ou "Société" correspondent simplement à une nouvelle occurence de ce sujet dans le même journal mais traité sous un angle différent. Il n'y a ainsi pas de lien direct entre "Sport1" d'un journal de TF1 et "Sport1" d'un journal de France 2 si ce n'est que les deux sujets traitent de sport, l'un pouvant donné les résultats de la ligue de foot et l'autre les médailles obtenues ce jour aux jeux olympiques.
 Des segments de journaux ont été exclus de l'appariemment avec les articles de presse car ils ne traitaient pas d'un thème particulier mais annonçaient les titres du journal, correspondaient à un jingle, à une pub, etc. Ils sont notés grâce au mot clé *OUT* dans le nom du thème. On trouvera ainsi les topics: "OUT:generique", "OUT:jingle", "OUT:meteo", "OUT:pub"...  

 = <Turn> : Dans tous les cas, une brève description du segment est disponible dans la balise <Turn> à l'intérieur de chaque <Section>


 == fichiers assgn, association manuelle entre articles et segments des vidéos
Les fichiers assgn (pour association google news) sont des fichiers textes qui indique l'association entre un segment d'un journal et les articles du jour (uniquement du même jour d'enregistrement du journal) qui portent sur le même thème selon notre annotatrice.
Ainsi sur chaque ligne sera indiqué les informations suivantes, séparées par des ; :
deb_seg:fin_seg;liste des identifiants des articles web associés; liste des identifiants des articles web potentiellement associés;liste des identifiants des articles évalués par notre annotatrice comme ne pouvant pas être associés au segment.

Pour avoir de plus amples renseignements sur la méthode d'association manuelle des articles et des segments, se référer à l'article [CamelinLREC18].
 
           

Contact point:
==============
Nathalie Camelin
Maitre de conférence, Le Mans Université, LIUM
https://lium.univ-lemans.fr
mail:nathalie.camelin@univ-lemans.fr
tel: +33 1 43 13 38 58
fax: +33 1 43 13 33 30