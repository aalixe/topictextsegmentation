TITRE: Cancers: faut-il se m�fier du d�pistage g�n�ralis�? - L'Express
DATE: 2014-02-13
URL: http://www.lexpress.fr/actualite/societe/sante/cancers-faut-il-se-mefier-du-depistage-generalise_1323572.html
PRINCIPAL: 0
TEXT:
Voter (1)
� � � �
Une �tude canadienne remet en cause l'utilit� des mammographies annuelles dans le cadre du d�pistage organis� du cancer du sein.
afp.com/Mychele Daniau
Le surd�pistage, mal de nos soci�t�s de la peur? Une �tude canadienne a relanc� la pol�mique mercredi. Elle montre que les femmes qui ont subi des mammographies annuelles pendant cinq ans n'ont pas moins de risque de mourir d'un cancer du sein que celles ayant seulement b�n�fici� d'une palpation. En septembre dernier, c'est le British Medical Journal qui soulevait la question du surdiagnostic du cancer de la thyro�de . M�me chose pour les cancers de la prostate. Le terme de "surdiagnostic" d�signe la d�tection de tr�s petites tumeurs qui n'auraient pas eu d'impact du vivant de la personne concern�e. Faut-il alors en conclure qu'il existe un risque de surd�pistage? �
"Plus on cherche, plus on trouve, que ce soit des images douteuses ou des r�sultats anormalement �lev�s. C'est le probl�me quand le d�pistage n'est pas cibl�. En en abusant, on peut arriver � des exc�s de m�decine qui -paradoxalement- nuisent � la sant�", estime le Dr Sauveur Boukris . Celui qui a �crit La fabrique de malades, ces maladies qu'on nous invente regrette que les normes, les constantes, que ce soit pour le cancer du sein ou de la prostate, mais aussi pour l'hypertension, le diab�te ou le cholest�rol aient �t� modifi�es. "Des objectifs difficiles � atteindre sont fix�s, ce qui contribue � cr�er du coup de nouveaux malades � qui on surprescrit et qui surconsomment des m�dicaments, affirme-t-il. La m�decine ne peut pas �tre syst�matis�e, il faut revenir � du cas par cas. Les normes ne peuvent pas �tre valables pour tout le monde."�
Ces tumeurs qui n'�voluent pas en cancers
Les m�decins prescriraient donc trop d'�chographies, de bilans de sant� ou encore de d�pistages. C'est ce qu'avait d�plor� l'Acad�mie nationale de m�decine dans un rapport publi� le 8 avril 2013. L'acad�mie regrettait en particulier que l'examen clinique minutieux du patient soit insuffisamment pris en compte. �
Faut-il pour autant remettre en question les campagnes de d�pistage organis�es par le minist�re de la Sant�? Le Dr Guy Vallancien, Professeur d'urologie � l'Universit� Paris Descartes y est oppos�, par crainte d'un retour en arri�re. �
"Ce n'est pas la question du d�pistage qui compte mais celle de savoir si la tumeur d�tect�e sera trait�e ou non. Si quelqu'un veut savoir s'il a un cancer, il en a le droit. Mais il faut ensuite que les patients comprennent qu'il est possible de surveiller, sans forc�ment traiter." C'est ce qui l'am�ne � repositionner le d�bat, en distinguant surd�pistage et surtraitement, notamment face � des connaissances m�dicales encore parcellaires pour qualifier l'agressivit� d'une tumeur, et ses risques d'�volution en cancer. �
�
