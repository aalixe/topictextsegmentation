TITRE: Somalie�: plusieurs victimes apr�s l�explosion d�une voiture pi�g�e � Mogadiscio | euronews, monde
DATE: 2014-02-13
URL: http://fr.euronews.com/2014/02/13/somalie-plusieurs-victimes-apres-l-explosion-d-une-voiture-piegee-a-mogadiscio/
PRINCIPAL: 171466
TEXT:
| Partager cet article
|
Une voiture pi�g�e a explos� ce jeudi devant l�entr�e du complexe qui abrite l�a�roport de la capitale somalienne Mogadiscio et le quartier-g�n�ral de la Force de l�Union africaine dans le pays (Amisom), faisant des victimes, a affirm� une source polici�re � l�AFP.
�Une voiture �tait gar�e pr�s de la grille de l�a�roport. Elle a explos�. Il y a des victimes�, a d�clar� � l�AFP la source polici�re, Mohammed Abdi Rahman.
Le complexe a�roportuaire de Mogadiscio, ultra-s�curis�, abrite aussi des bureaux de l�ONU et des antennes diplomatiques occidentales, notamment la mission britannique.
L�ambassadeur britannique en Somalie, Neil Wigan, a indiqu� sur Twitter avoir entendu une �forte explosion� et vu un nuage de fum�e.
I heard a few minutes ago a major explosion close to Mogadishu International Airport. Smoke still in the air.
� Neil Wigan (@FCONeilWigan) February 13, 2014
Lundi � Mogadiscio, les explosions s�par�es de deux bombes visant des responsables du gouvernement somalien avaient tu� un garde et bless� un responsable militaire.
Ces deux attentats n�avaient pas �t� revendiqu�s, mais la capitale somalienne est r�guli�rement le th��tre d�attaques attribu�es aux islamistes somaliens shebab li�s � Al-Qa�da, depuis qu�ils en ont �t� chass�s en ao�t 2011 par l�Amisom.
AFP
