TITRE: News PSG : Quand Blanc pointe les d�fauts de Verratti - T�l�foot - MYTF1
DATE: 2014-02-13
URL: http://www.tf1.fr/telefoot/news-football/psg-blanc-il-faut-effacer-certains-comportements-de-verratti-8365082.html
PRINCIPAL: 170972
TEXT:
MyTELEFOOT : Emission en replay vid�o du dimanche ...
Marco Verratti va-t-il perdre � terme sa place de titulaire au profit de Yohan Cabaye�? Certains observateurs le pensent, et les d�clarations de Laurent Blanc ce jeudi en conf�rence de presse ne risquent pas de les faire changer d'avis. Le PSG a un probl�me de riche, mais l'Italien pourrait �tre le grand perdant de la seconde partie de saison.
�
Le comportement de Verratti
Dimanche dernier � Monaco, Laurent Blanc a remplac� Marco Verratti � la 64e minute par Yohan Cabaye. Un changement qui n'a �chapp� � personne et que l'entra�neur du PSG explique ainsi�: ��Il faut effacer certains comportements de Marco Verratti. J'ai eu plusieurs discussions avec lui, mais �a prend du temps. Il progresse mais il n'a que 21 ans, m�me s'il joue comme quelqu'un qui joue depuis 15 ans.��
�
Sur le banc pour r�fl�chir
Les erreurs de jeunesse de Marco Verratti peuvent-elles lui co�ter sa place de titulaire au PSG�? Laurent Blanc n'�carte aucune possibilit�, m�me s'il compte bien utiliser le talent de l'international italien�: ��Il n'a pas plus jou� que la saison pass�e. Il apprend, il est jeune mais on n'apprend pas en restant sur le banc. Peut-�tre que je le laisserai sur le banc pour qu'il r�fl�chisse.��
�
Prendre des risques, mais pas n'importe o�
Laurent Blanc souhaite surtout que Marco Verratti simplifie son jeu et prenne des risques calcul�s�: ��Ce qui marche le mieux, c'est de discuter avec lui. Ce n'est pas un joueur m�chant, m�me s'il prend des cartons. Je lui demande de prendre des risques, mais � 30 ou 40 m�tres de la cage adverse. Je lui demande de jouer plus simple devant la d�fense.��
�
En attendant, il existe une forte probabilit� pour que Yohan Cabaye soit pr�f�r� � Marco Verratti pour le match contre Valenciennes vendredi.
