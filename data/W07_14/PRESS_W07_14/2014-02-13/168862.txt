TITRE: Venezuela: trois morts dans des manifestations contre le gouvernement - LExpress.fr
DATE: 2014-02-13
URL: http://www.lexpress.fr/actualites/1/monde/venezuela-un-militant-pro-gouvernement-tue-en-marge-d-une-manifestation_1323395.html
PRINCIPAL: 0
TEXT:
Venezuela: trois morts dans des manifestations contre le gouvernement
Par AFP, publi� le
12/02/2014 � 22:11
, mis � jour le 13/02/2014 � 14:55
Caracas - Des milliers d'�tudiants et de militants de l'opposition sont descendus mercredi dans les rues de Caracas et d'autres villes du Venezuela pour protester contre la vie ch�re et l'ins�curit�, au cours de manifestations qui ont fait au moins trois morts.
Des milliers d'�tudiants et de militants de l'opposition sont descendus mercredi dans les rues de Caracas et d'autres villes du Venezuela pour protester contre la vie ch�re et l'ins�curit�, au cours de manifestations qui ont fait au moins trois morts.
afp.com/Leo Ramirez
Un militant pro-gouvernement et un �tudiant ont �t� tu�s par balles mercredi en marge de cette manifestation � Caracas, a annonc� la responsable du minist�re public v�n�zu�lien Luisa Ortega Diaz, faisant en outre �tat de 23 bless�s dans les manifestations organis�es dans plusieurs villes du pays. Un autre militant a �t� tu�, lui aussi par balle, � Chacao, un des quartiers populaires de l'est de la capitale v�n�zu�lienne, selon le maire de cette municipalit�.�
De son c�t�, le ministre de l'Int�rieur Miguel Rodriguez a pr�cis� qu'un total de 30 personnes avaient �t� arr�t�es. "Ils avaient tous des capuches, des radios et avaient dans leurs sacs des cocktails molotov, des pierres, tous types d'objets visant � agresser les policiers", a-t-il affirm�.�
Le pr�sident v�n�zu�lien Nicolas Maduro a donn� l'ordre mercredi soir de renforcer la s�curit� dans les principales villes du pays pour �viter les "tentatives de coup d'Etat".�
"Il n'y aura pas de coup d'Etat au Venezuela, ayez-en la certitude absolue. La d�mocratie continuera, la r�volution continuera", a affirm� M. Maduro lors d'une allocution aux radios et t�l�visions du pays.�
Auparavant, le pr�sident de l'Assembl�e nationale Diosdado Cabello avait annonc� la mort d'un militant pro-gouvernement � Caracas. "Il a �t� vilement tu� par le fascisme", avait d�clar� M. Cabello, reprenant le terme habituellement utilis� par les membres du gouvernement pour qualifier l'opposition.�
Quelques minutes plus t�t, lors de la dispersion de cette marche d'opposition au gouvernement de Nicolas Maduro, qui a mobilis� des milliers de personnes dans le centre-ville de la capitale, un photographe de l'AFP a vu des hommes circulant � moto tirer sur la foule, faisant au moins deux bless�s, avant de prendre la fuite.�
- Deux cam�ras vol�es -�
Des militants pro-gouvernementaux s'en sont pris � des journalistes, dont une �quipe de l'AFP et d'une autre agence de presse, � qui ils ont d�rob� deux cam�ras. Lors de l'incident impliquant l'�quipe de l'AFP, la police �tait pr�sente sur les lieux mais n'est pas intervenue.�
Leur cam�ra contenait des images de tirs de gaz lacrymog�nes contre des �tudiants, d'arrestations d'opposants et d'agressions de manifestants par les forces de l'ordre.�
Par ailleurs, la t�l�vision colombienne NTN24, qui a largement couvert ces manifestations, a disparu de la liste des cha�nes restransmises par les deux principaux c�blo-op�rateurs du Venezuela, selon l'une de ces journalistes. Deux journalistes, dont l'identit� n'a pas �t� r�v�l�e, ont �t� arr�t�s, a �galement indiqu� le secr�taire-g�n�ral du Syndicat national des travailleurs de presse.�
A Caracas, la manifestation s'est termin�e par de br�ves �chauffour�es entre forces de l'ordre et manifestants, qui ont lanc� des pierres sur les policiers. Un journaliste de l'AFP a pu voir les policiers interpeller cinq personnes.�
En d�but de soir�e, de petits groupes continuaient de protester en divers points de la ville, dispers�s �� et l� par les brigades anti-�meutes. �
Le mouvement de protestation �tudiant, lanc� depuis une dizaine de jours en province, vise � condamner la politique �conomique du gouvernement et l'ins�curit� croissante dans le pays.�
Lors d'une pr�c�dente marche organis�e mardi dans la ville de M�rida (ouest), cinq �tudiants avaient d�j� �t� bless�s par balles par des individus circulant � moto, selon plusieurs m�dias locaux et les syndicats �tudiants.�
Mercredi, les manifestants exigeaient aussi la lib�ration imm�diate d'une dizaine d'�tudiants incarc�r�s ces derniers jours. �
"Nous marchons parce que nous voulons que nos camarades emprisonn�s soient lib�r�s, mais aussi en raison de la situation du pays, de la d�t�rioration de l'�conomie, des p�nuries qui nous rendent malades et de l'ins�curit�", a d�clar� � l'AFP Daniela Mu�oz, �tudiante en m�decine.�
- Zone de turbulences -�
Inflation, p�nuries, ins�curit�: le pays p�trolier traverse actuellement une zone de turbulences que peine � contenir le successeur de Hugo Chavez (1999-2013). �
Parall�lement � cette marche, le gouvernement avait aussi convoqu� mercredi ses partisans en divers points de la capitale pour c�l�brer le "Jour de la jeunesse".�
Ces rassemblements se sont mu�s en manifestations d'appui au gouvernement dans son combat contre une suppos�e "guerre �conomique" men�e selon M. Maduro par le secteur priv� et l'opposition soutenue par l'�tranger. �
Ces derniers sont rendus responsable par le gouvernement de la forte inflation (56,3% en 2013) et les p�nuries r�currentes frappant les produits alimentaires ou de consommation courante. �
Au point de convergence des cort�ges, et avant l'annonce des incidents, M. Maduro avait appel� ses concitoyens "� ne pas utiliser les armes" et � ne pas "chercher � agresser", lan�ant -en vain- un "appel � la conscience nationale".�
Par
