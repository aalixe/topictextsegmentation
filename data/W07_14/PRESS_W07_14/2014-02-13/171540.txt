TITRE: "Revival", le nouveau roman de Stephen King | Site mobile Le Point
DATE: 2014-02-13
URL: http://www.lepoint.fr/culture/revival-le-nouveau-roman-de-stephen-king-13-02-2014-1791465_3.php
PRINCIPAL: 171537
TEXT:
13/02/14 à 18h21
"Revival", le nouveau roman de Stephen King
L'auteur américain vient d'annoncer la parution, le 11 novembre 2014, de son prochain roman, "Revival", fresque évoquant Dieu, le diable et les démons.
Stephen King, 66 ans, auteur de plus de 50 romans, a vendu plus de 350 millions de livres dans le monde. Francois Mori / Sipa
Source AFP
Le maître du roman d'horreur, l'Américain Stephen King , a annoncé jeudi la parution, le 11 novembre prochain, d'un nouveau roman, Revival, évoquant Dieu, le diable et des démons. L'écrivain a annoncé cette nouvelle parution sur son site officiel, stephenking.com, en résumant l'intrigue : les destins croisés d'un jeune homme et de son voisin prêtre et les "démons" personnels qu'ils doivent tous deux affronter. Le roman sera publié en même temps chez Scribner aux États-Unis et Hodder et Stoughton en Grande-Bretagne .
L'auteur avait déjà auparavant annoncé pour le 3 juin un roman policier, Mr Mercedes. Stephen King, 66 ans, a écrit plus de 50 romans donnant lieu à des dizaines d'adaptations cinématographiques et a vendu plus de 350 millions de livres dans le monde.
