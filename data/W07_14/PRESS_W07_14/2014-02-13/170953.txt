TITRE: Nord: �Clients de prostitu�es, ici vous �tes film�s�, proclament des panneaux � La Madeleine - 20minutes.fr
DATE: 2014-02-13
URL: http://www.20minutes.fr/societe/1298150-20140213-nord-clients-prostituees-etes-filmes-proclament-panneaux-a-madeleine
PRINCIPAL: 170923
TEXT:
Une prostitu�e � Lille. M. LIBERT / 20 MINUTES
SOCIETE - La zone du Vieux Lille est traditionnellement arpent�e par les prostitu�es...
Le maire UMP de la commune de La Madeleine (Nord), en banlieue de Lille, a r�cemment fait installer des cam�ras de vid�osurveillance sur deux parkings, accompagn�es de panneaux proclamant: �Clients de prostitu�es, ici vous �tes film�s�.
�Il y a un probl�me: des parkings qui accueillent r�guli�rement des prostitu�es qui viennent y consommer des actes sexuels avec leurs clients�, a expliqu� jeudi � l'AFP le maire, S�bastien Lepr�tre, confirmant une information du quotidien La Voix du Nord.
Des cam�ras ont �t� install�es fin 2013 sur deux parkings et les panneaux, command�s en m�me temps, pos�s r�cemment: �Quand on installe des cam�ras de vid�osurveillance, on a un devoir d'information, donc on installe des panneaux. Tant qu'� informer, j'ai jug� utile d'�tre dans une information cibl�e�, a poursuivi l'�dile.
�Faire l�amour dans la voiture sur le domaine publique� interdit
S�bastien Lepr�tre a dit assumer le fait que ces panneaux �stigmatisent les clients des prostitu�es�, alors que la loi p�nalisant les clients n'a pas encore �t� soumise au vote du S�nat.
Le maire mise sur l'effet dissuasif des cam�ras et des panneaux: �D�placer un probl�me, c'est aussi commencer � le r�soudre�, a-t-il assur�.
Le cas �ch�ant, ajoute-t-il, les bandes vid�o peuvent aussi �tre r�quisitionn�es par la police �� la suite d'une plainte� mais aussi au regard du fait que �faire l'amour dans une voiture sur le domaine public n'est pas possible, c'est la loi�.
Ces parkings sont situ�s non loin de l'avenue du Peuple Belge, une zone du Vieux Lille traditionnellement arpent�e par les prostitu�es.�
Avec AFP
