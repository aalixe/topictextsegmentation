TITRE: News Renault Twingo 2014 : voici les toutes premi�res images officielles - Automoto - MYTF1
DATE: 2014-02-13
URL: http://www.tf1.fr/auto-moto/actualite/la-renault-twingo-iii-fuite-quelques-heures-avant-8364989.html
PRINCIPAL: 0
TEXT:
L'actu Renault Twingo
La communication prise de court
Une photo provenant du magazine allemand Autobild est parue aujourd'hui sur le site, avant d'�tre camoufl�e, mais trop tard, le mal a �t� fait.... Alors, nous pouvions apercevoir la Renault Twingo III qui sera pr�sent�e au prochain Salon de Gen�ve le 4 mars. Cette fuite a mis � mal la communication du constructeur fran�aispendant quelques heures. Mais imperturbable, Renault a lanc� une r�v�lation en direct, un "strip-tweet" via le hashtag #undressnewtwingo permettant aux � twittos � de d�voiler cette Twingo III entre 17 et 19 heures ce jeudi.
Inspiration Fiat 500 ?
Premier constat, cette nouvelle Twingo ressemble beaucoup � la Fiat 500, notamment � cause de son hayon inclin� et de sa ceinture de caisse haute. Toutefois, nous retrouvons l'avant classique du nouveau design Renault, mais nous nous attendions � cette forme gr�ce aux concept-cars d�voil�s en 2013, la sportive Twin'Run et l'�lectrique Twin'Z.
Quelles caract�ristiques ?
La nouvelle Twingo sera sp�ciale puisqu'on retrouve le moteur � l'arri�re ainsi que deux coffres. Le moteur sera donc situ� sous le coffre principal de 150 litres environ. Elle en aura un autre �galement � l'avant de moins de 100litres. Si la troisi�me g�n�ration sera moins longue que la pr�c�dente (3.630 mm), elle aura pour la premi�re fois 5 portes. Assembl�e � Novo Mesto, en Slov�nie, elle disposera de deux moteurs essence 3 cylindres. Le premier, un 0.9 TCe 90 ch et le second moins puissant, un 1.0 75 ch. Au niveau de la bo�te de vitesses, le choix se fera entre une bo�te manuelle 5 rapports et une bo�te robotis�e double embrayage EDC � 6 rapports.
La Renault Twingo III sera d�voil�e ce soir � 19h p�tantes tandis que le strip-tease de cette Twingo sera lanc� � 17h.
A venir �galement dans quelques heures, la nouvelle Peugeot 108 , qui augurera la prochaine g�n�ration de Citro�n C1 et de Toyota Aygo.
