TITRE: Sotchi 2014 : Le programme de la septi�me journ�e des JO - 20minutes.fr
DATE: 2014-02-13
URL: http://www.20minutes.fr/sport/1298766-20140213-sotchi-2014-programme-septieme-journee-jo
PRINCIPAL: 171854
TEXT:
Les anneaux olympiques de Sotchi, le 5 f�vrier 2014. MAYA VIDON-WHITE/SIPA
JEUX OLYMPIQUES � Entr�e dans les starts d�Alexis Pinturault...
�Le Fran�ais � suivre. Pour une fois, il ne s�agit pas de Martin Fourcade. Le tout r�cent double champion olympique fran�ais laisse la vedette � Alexis Pinturault vendredi. Le ski alpin, qui tire la tronche depuis le d�but de la quinzaine olympique, attendait son sauveur avec impatience. Le gar�on le plus polyvalent du ski tricolore depuis Jean-Claude Killy tente une premi�re fois sa chance en super-combin�. Son objectif�? Limiter la casse en descente pour coiffer tout le monde au poteau sur le slalom.
La question. Brian Joubert va-t-il enfin boucler des JO sans chute? Impeccable comme � ses plus belles heures lors du programme court jeudi soir, � tel point que le podium ne semble pas inaccessible � vue de nez, le Fran�ais pr�f�r� des Japonaises a �t� longuement acclam� par le public russe. D�finitivement priv� de Plushenko, l�Iceberg ne demande qu�� s�enflammer pour un programme libre de haute voltige de Brian.
L�attraction. Vous l�ignorez sans doute, mais ce vendredi la Suisse est pendue au sort de Simon Amman. Le quadruple m�daill� d�or olympique est pass� compl�tement � c�t� de son concours sur le tremplin normal. �J'ai beaucoup travaill� et r�fl�chi pour essayer de trouver une solution. Mais je n'ai pas r�ussi. Je n'arrive d�cid�ment pas � 'lire' ce tremplin� avait alors d�clar� Amman, un brin fataliste. Il lui reste le grand tremplin, dont les qualifications commencent aujourd�hui, pour se refaire.
La curiosit�. Elles sont trois. Enfin quatre, mais on a oubli� le nom de la derni�re. Anna Sidorova, Alexandra Saitova et Ekaterina Galkina sont peut-�tre les trois plus jolies filles engag�es � Sotchi. Cela tombe bien, elles d�fendent ensemble les couleurs de la Russie�en curling. Croyez-nous sur parole, elles vous scotcherons � l��cran plus longtemps qu�un titre olympique de Martin Fourcade.
J.L. et R.S.
