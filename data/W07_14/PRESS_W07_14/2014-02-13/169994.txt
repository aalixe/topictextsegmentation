TITRE: Un laboratoire am�ricain franchit une �tape vers la fusion nucl�aire - L'Express
DATE: 2014-02-13
URL: http://www.lexpress.fr/actualite/sciences/un-laboratoire-americain-franchit-une-etape-vers-la-fusion-nucleaire_1323511.html
PRINCIPAL: 169958
TEXT:
Un laboratoire am�ricain franchit une �tape vers la fusion nucl�aire
Par LEXPRESS.fr, publi� le
13/02/2014 �  11:13
Des physiciens am�ricains qui cherchent � d�clencher une fusion nucl�aire contr�l�e, alternative � la fission d'aujourd'hui, ont r�ussi � produire plus d'�nergie que leur combustible n'en a absorb�.�
Voter (2)
� � � �
La pression g�n�r�e lors de cette exp�rience est 150 milliards de fois plus forte que celle de l'atmosph�re terrestre, avec une densit� 2,5 � 3 fois sup�rieure � celle qui r�gne au coeur du Soleil.
Reuters/NASA
Une nouvelle �tape a �t� franchie dans la voie vers la fusion nucl�aire . C'est cent fois moins que ce qu'il faut pour produire de l'�nergie rentable, mais les physiciens am�ricains qui cherchent � d�clencher une fusion nucl�aire contr�l�e, alternative � la fission d'aujourd'hui, ont r�ussi � produire plus d'�nergie que leur combustible n'en a absorb�.�
Cet exploit in�gal� a �t� r�alis� � deux reprises l'automne dernier dans le laboratoire gouvernemental du National Ignition Facility (NIF), en Californie, gr�ce � la chaleur produite par 192 lasers occupant la surface d'un terrain de football.�
Certes, il a dur� moins d'un milliardi�me de seconde et n'a produit au finish qu'une �nergie correspondant � celle stock�e dans deux piles AA (17.000 joules au maximum), alors que les chercheurs ont d� injecter � l'entr�e du syst�me l'�quivalent d'une batterie de voiture.�
"Grosso modo, on doit obtenir un rendement cent fois meilleur avant d'arriver au seuil d'ignition", le point o� la r�action nucl�aire se suffit � elle-m�me. "Ca semble modeste, et �a l'est. Mais nous nous en sommes approch�s plus que quiconque par le pass�", a d�clar� � la presse Omar Hurricane, responsable des essais du NIF.�
Des obstacles majeurs restent � franchir
Malgr� ce succ�s, des obstacles majeurs restent � franchir pour esp�rer, un jour lointain, produire de l'�nergie � une �chelle industrielle gr�ce � la fusion nucl�aire contr�l�e. "On ne peut pas vous dire honn�tement quand nous atteindrons le seuil d'ignition (...) Nous pensons que si nous poursuivons nos efforts, nous avons une chance, mais on ne peut vraiment rien promettre. Nous travaillons dans un laboratoire de recherche, pas dans une centrale �lectrique", souligne le Dr Hurricane.�
La fusion nucl�aire, c'est l'autre fa�on de lib�rer l'�nergie contenue dans les atomes.�
La premi�re, d�j� utilis�e depuis des d�cennies dans les r�acteurs des centrales, est la fission, qui consiste � casser les liaisons des noyaux atomiques lourds (uranium et plutonium par exemple) pour en r�cup�rer l'�nergie sous forme de chaleur.�
La fusion est le processus inverse: on "marie" deux noyaux atomiques l�gers pour en cr�er un lourd.�
Pas de d�chets radioactifs de longue dur�e
Au coeur des �toiles, ce sont deux noyaux d'hydrog�ne qui fusionnent pour donner un noyau d'h�lium. Sur Terre, les scientifiques ont opt� pour la fusion de deux isotopes (variantes atomiques) de l'hydrog�ne, le deut�rium et le tritium, une r�action qui donne elle aussi naissance � de l'h�lium.�
Pour de nombreux experts, la fusion serait l'�nergie id�ale: son combustible est abondant � l'�tat naturel (le deut�rium peut �tre extrait de l'eau de mer), elle ne produit pas de d�chets radioactifs de longue dur�e, ne pr�sente pas de risque d'emballement nucl�aire et n'a aucune application militaire directe.�
Seul probl�me, pour y parvenir, il faut atteindre des conditions de temp�rature et de pression extr�mes, comme � l'int�rieur d'une �toile.�
Le r�acteur exp�rimental international Iter , dont la construction a d�but� en France sur le site de Cadarache, pi�gera les atomes gr�ce � des champs magn�tiques colossaux, les faisant chauffer longuement pour les porter au point de fusion � une temp�rature d'au moins 100 millions de degr�s.�
Une cible minuscule bombard�e par de puissants lasers
D'autres projets, dont le NIF am�ricain, misent quant � eux sur l'action conjugu�e de puissants faisceaux lasers, qui bombardent une cible minuscule pour cr�er des pressions intenses en un temps tr�s court.�
"Nous mettons la capsule qui contient le combustible - une couche microscopique de deut�rium et de tritium glac�s - dans une canette cylindrique d'un centim�tre de long, puis nous tirons au laser dans le trou (...) pour d�clencher la fusion", dit Debbie Callahan , physicienne au NIF.�
Comprim�e par ce soudain bombardement, la capsule devient 35 fois plus petite, "comme si on d�butait l'exp�rience avec un ballon de basket et qu'on finisse avec un petit pois", � tel point qu'elle implose et que le combustible s'effondre sur lui-m�me pour fusionner, explique la chercheuse.�
"La pression g�n�r�e sur ce tout petit point est 150 milliards de fois plus forte que celle de l'atmosph�re terrestre, avec une densit� 2,5 � 3 fois sup�rieure � celle qui r�gne au coeur du Soleil", rench�rit Omar Hurricane.�
�
