TITRE: R�sum� de match, Real Sociedad-Bar�a (1-1) - Goal.com
DATE: 2014-02-13
URL: http://www.goal.com/fr/match/129752/real-sociedad-vs-fc-barcelona/report
PRINCIPAL: 168768
TEXT:
1/2 finale retour de la Coupe du Roi :
Real Sociedad-Barcelone : 1-1
Buts : Griezmann (87e); Messi (27e)
Comme pr�vu, le Bar�a et le Real Madrid vont se retrouver le 19 avril prochain en finale de la Coupe d�Espagne et ce pour la deuxi�me fois en quatre ans. A l�instar de leur rivaux merengue, les Catalans n�ont jamais vraiment trembl� � l�occasion de leur double confrontation en demi-finale contre la Real Sociedad . Mais, les hommes de Tata Martino n�ont cependant pas pu remporter leur seconde manche. Apr�s la victoire 2-0 � Camp Nou, ils ont d� se contenter d�un 1-1 au retour � Anoeta. La faute � Antoine Griezmann . Le Fran�ais a sign� le but de l��galisation en toute fin de match.
D�j� auteur de deux buts dans cette comp�tition (et 17 au total cette saison), l�international espoir tricolore a donc permis � son �quipe basque de sortir avec les honneurs. On jouait la 87e minute de jeu lorsque d�une jolie reprise de vol�e du gauche, il a tromp� la vigilance de Sergio Pinto. Il r�pondait ainsi au but de Lionel Messi inscrit en premi�re p�riode. L�Argentin avait fait parler sa technique pour se d�faire de nombreux d�fenseurs adverses pour ensuite fixer tranquillement le portier adverse. Il confirme son regain de forme actuel. De bon augure � une semaine du rendez-vous contre Manchester City en huiti�mes de finale de la Ligue des Champions.
