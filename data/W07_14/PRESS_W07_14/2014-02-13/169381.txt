TITRE: N�mes : un homme retrouv� mort sur un chemin de garrigue - RTL.fr
DATE: 2014-02-13
URL: http://www.rtl.fr/actualites/info/article/nimes-un-homme-retrouve-mort-sur-un-chemin-de-garrigue-7769685240
PRINCIPAL: 169340
TEXT:
Le procureur de la R�publique de N�mes, le 14 d�cembre 2012.
Cr�dit : AFP / PASCAL GUYOT
Le procureur de N�mes a annonc�, ce jeudi, la d�couverte d'un cadavre au bord d'un chemin de garrigue � N�mes (Gard).
Le parquet �voque une agression "tr�s violente". �g� "entre 20 et 30 ans", il a �t� victime de "plusieurs coups", a indiqu� � l'AFP St�phane Bertrand, procureur adjoint de la R�publique � N�mes. Une autopsie doit �tre pratiqu�e en fin de matin�e pour d�terminer les circonstances exactes de la mort.
Des objets retrouv�s � proximit� du cadavre, notamment un marteau, vont �galement faire l'objet d'analyses. "La piste criminelle ne fait aucun doute, �a a �t� tr�s violent", a d�clar� le magistrat qui a confi� l'enqu�te � la police judiciaire.
La r�daction vous recommande
