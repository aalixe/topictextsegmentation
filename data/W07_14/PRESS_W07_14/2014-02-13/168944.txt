TITRE: Suspendu, Bernard Laporte devra patienter jusqu'aux demi-finales - 13/02/2014 - LaD�p�che.fr
DATE: 2014-02-13
URL: http://www.ladepeche.fr/article/2014/02/13/1816867-bernard-laporte-devra-patienter-jusqu-aux-demi-finales.html
PRINCIPAL: 0
TEXT:
Suspendu, Bernard Laporte devra patienter jusqu'aux demi-finales
Publi� le 13/02/2014 � 03:47
,
Mis � jour le 13/02/2014 � 08:10
| 2
top 14. Commission de discipline. Le manager de Toulon suspendu pendant treize semaines.
Le manager de Toulon Bernard Laporte a �t� interdit de diriger son �quipe en match pendant 13 semaines, soit jusqu�� la fin de la saison r�guli�re de Top 14.
Le club varois a imm�diatement annonc� qu�il faisait appel de cette d�cision qui interdit l�ancien entra�neur du XV de France �d�acc�der au terrain, aux vestiaires (des �quipes et des arbitres) ainsi qu�aux couloirs d�acc�s � ces zones lors des matches officiels (y compris avant et apr�s les matches) pendant 13 semaines cons�cutives, soit jusqu�au 13 mai 2014 inclus�.
Laporte n�aura donc le droit de revenir qu�en cas de qualification de son �quipe, championne d�Europe en titre et vice-championne de France, pour les demi-finales du Top 14 pr�vues les 16 et 17 mai.
Ses propos avaient fait l�effet d�une bombe dans le monde du rugby. Le 9 janvier, sur la radio RMC pour laquelle il est consultant, il �tait revenu sur la d�faite (21-22) de son �quipe � domicile le week-end pr�c�dent face � Grenoble, qualifiant l�arbitre de la rencontre, Laurent Cardona, de �nul, d��incomp�tent� et de �pipe� qui �vole � chaque fois� son �quipe.
�Bernie le r�cidiviste�
Ces paroles ont d�autant plus choqu� qu�elles ont �t� tenues � froid, cinq jours apr�s la rencontre, et non dans un de ces coups de col�re qui ont valu � Laporte le surnom de �Bernie le dingue�.
Laporte avait �galement d�plor� que l�arbitre ne lui ait pas directement r�pondu au t�l�phone mais ait pr�f�r� communiquer par message �crit. �Cardona essaie d��tre ami avec moi mais ses textos, il peut se les carrer dans le c��.
L�ancien demi de m�l�e et ex-entra�neur du XV de France est un r�cidiviste en la mati�re. Il avait d�j� �t� interdit de banc et de vestiaire 60 jours en 2012 pour des propos injurieux envers l�arbitre Romain Poite, quelques mois apr�s la suspension de son pr�sident provocateur Mourad Boudjellal pour 130 jours pour avoir affirm� avoir subi �une sodomie arbitrale� lors d�un match de Top 14.
Le club de Toulon a d�ailleurs �t� condamn� � 10 000 euros d�amende pour les propos de son manager. Ses adjoints Pierre Mignoni et Jacques Delmas seront donc ses relais durant les matches. En attendant que son �quipe se qualifie pour les demi-finales.
La D�p�che du Midi
