TITRE: La Belgique autorise l'euthanasie pour les enfants incurables - rts.ch - info - monde
DATE: 2014-02-13
URL: http://www.rts.ch/info/monde/5610604-la-belgique-autorise-l-euthanasie-pour-les-enfants-incurables.html
PRINCIPAL: 171763
TEXT:
La Belgique autorise l'euthanasie pour les enfants incurables
14.02.2014 08:41
Les enfants belges atteints de maladies incurables pourront choisir leur destin. [Jean-Philippe Ksiazek - AFP]
Les enfants que la m�decine ne peut pas gu�rir pourront d�sormais choisir l'euthanasie en Belgique. Les parlementaires ont adopt� le texte de loi jeudi au terme d'un d�bat nourri.
En Belgique, les enfants atteints d'une maladie incurable vont pouvoir choisir l'euthanasie pour abr�ger leurs souffrances. Les d�put�s ont adopt� le texte jeudi malgr� l'opposition de certains p�diatres et de l'�glise catholique.
C'est le deuxi�me pays au monde � l�gif�rer en ce sens apr�s les Pays-Bas, � la diff�rence que le N�erlandais ont fix� une limite d'�ge � 12 ans, alors que les Belges ont choisi la notion plus flexible de "capacit� de discernement".
Que quelques cas par an
Le gouvernement, o� partisans et opposants au texte se c�toient, est rest� tr�s discret, laissant une totale libert� de vote aux parlementaires. Pas moins de 34 m�decins, des sp�cialistes, des juristes et des associations de tous bords ont �t� auditionn�s par les s�nateurs.
La loi, qui ne devrait concerner que quelques mineurs par an, entrera en vigueur dans les prochaines semaines. Selon les statistiques officielles, quelque 1500 adultes choisissent l'euthanasie chaque ann�e, ce qui repr�sente environ 2% des d�c�s dans le royaume.
ats/boi
