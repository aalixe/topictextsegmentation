TITRE: FRANCE: �Mister Univers 2014� soup�onn� de dopage -  News Monde: Europe - tdg.ch
DATE: 2014-02-13
URL: http://www.tdg.ch/monde/europe/Mister-Univers-2014-soupconne-de-dopage/story/17077868
PRINCIPAL: 0
TEXT:
Votre adresse e-mail*
Votre email a �t� envoy�.
Un culturiste fran�ais r�cemment sacr� �Mister Univers 2014�, Alexandre Piel, a �t� plac� en garde � vue pour usage de produits dopants, a-t-on appris jeudi soir de source polici�re.
�Alexandre Piel a �t� entendu lundi en garde � vue dans le cadre d'une affaire en cours de produits anabolisants�, a indiqu� une source polici�re, confirmant une information d'un candidat � la direction de la mairie de la petite commune de Caudebec-l�s-Elbeuf (nord-ouest) et sur la liste duquel figurait le champion.
Champion du monde
Alexandre Piel, 39 ans, �tait colistier sur la liste du socialiste Laurent Bonnaterre, � la 17e place, en vue des �lections municipales de mars.�Je lui ai imm�diatement demand� des explications et il m'a pr�sent� � la fois ses excuses et son retrait par �crit de la liste�, a indiqu� Laurent Bonnaterre.
Alexandre Piel avait le 30 novembre dernier d�croch� le titre de Mister Univers 2014 � Hambourg (Allemagne) dans la cat�gorie �athl�tiques� (moins de 80 kg) apr�s avoir �t� champion du monde � Chypre-nord en juin, deux titres obtenus dans le cadre de la f�d�ration internationale de culturisme (NAC).
Pas de contr�les antidopage
Cette f�d�ration ne pratique pas de contr�les antidopage. Bien qu'ayant re�u un bon �cho m�diatique, les titres d'Alexandre Piel n'avaient pas �t� salu�s par la seule f�d�ration fran�aise culturiste reconnue en France et qui pratique des contr�les.
La consommation d'anabolisants pour augmenter la masse musculaire est end�mique dans les milieux culturistes. Ces produits sont interdits en France mais autoris�s dans beaucoup d'autres pays.
Interrog� sur le dopage avant son titre de Mister Univers, Alexandre Piel avait r�pondu qu'il ne prenait �rien du tout�, mettant son impressionnante musculature uniquement sur le compte de son entra�nement et de son strict r�gime prot�in� � base de poulet r�ti et de blanc d��uf. (afp/Newsnet)
Cr��: 13.02.2014, 23h05
