TITRE: Les marques misent peu sur YouTube, pourtant 2e moteur de recherche
DATE: 2014-02-13
URL: http://www.leparisien.fr/high-tech/les-marques-misent-peu-sur-youtube-pourtant-2e-moteur-de-recherche-12-02-2014-3584147.php
PRINCIPAL: 169101
TEXT:
Les marques misent peu sur YouTube, pourtant 2e moteur de recherche
Publi� le 12.02.2014, 15h21
Tweeter
YouTube reste encore peu pris en compte par les marques dans leur strat�gie marketing, alors que le site de vid�os constitue une cible de choix en tant que deuxi�me moteur de recherche derri�re Google, selon une �tude de Forrester. | Ethan Miller
R�agir
YouTube reste encore peu pris en compte par les marques dans leur strat�gie marketing, alors que le site de vid�os constitue une cible de choix en tant que deuxi�me moteur de recherche derri�re Google , selon une �tude de Forrester.
Avec plus de 3 milliards de requ�tes trait�es chaque mois sur sa plateforme, YouTube est en effet class� depuis quelques mois comme le deuxi�me moteur de recherches derri�re Google, qui n'est autre que sa maison-m�re et qui enregistre � lui seul plus de cent milliards de requ�tes par mois dans le monde.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
"Les internautes cherchent de plus en plus sur YouTube une vid�o sur tous les sujets ou les informations qui les int�ressent", r�sume mercredi le cabinet de recherche.
YouTube compte plus d'un milliard de visiteurs uniques par mois dans le monde. Plus de 6 milliards d'heures de vid�o y sont regard�es chaque mois, et plus de 100 heures de vid�o y sont mises en ligne chaque minute, selon les donn�es officielles de Google.
Actuellement, les internautes �g�s de 18 � 24 ans sont les plus gros consommateurs du site: 83% d'entre eux aux Etats-Unis et 81% en Europe s'y rendent au moins une fois par mois.
58% des internautes am�ricains dans la tranche d'�ge 40-44 ans, et 45% des 55-59 ans visitent �galement YouTube chaque mois.
"Les �quipes marketing peuvent donc atteindre tous les �ges", mais seuls 49% des 395 responsables marketing de marques interrog�s par Forrester indiquent inclure r�guli�rement YouTube dans leur strat�gie publicitaire.
"Ils devraient ouvrir les yeux sur le potentiel qu?offre l'int�gration de contenu vid�o en ligne dans leur +mix+ (l'ensemble des supports publicitaires choisis pour une campagne) marketing", r�sume James McDavid, chercheur de Forrester cit� dans le communiqu�.
Car, souligne-t-il, "pr�s de 20% des 18-24 ans aux Etats-Unis sont plus susceptibles de voir une publicit� sur YouTube qu'� la t�l�vision, et 16% des adultes am�ricains qui ont regard� des vid�os sur YouTube l'ann�e derni�re ont utilis� le service pour apprendre � se servir d'un produit en regardant des vid�os, mettant ainsi le doigt sur un usage cl� pour les marques".
