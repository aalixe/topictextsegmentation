TITRE: Renault: l'hypothèque iranienne et certaines charges pèsent sur le bénéfice | Site mobile Le Point
DATE: 2014-02-13
URL: http://www.lepoint.fr/auto-addict/actualites/renault-l-hypotheque-iranienne-et-certaines-charges-pesent-sur-le-benefice-13-02-2014-1791237_683.php
PRINCIPAL: 0
TEXT:
13/02/14 à 08h42
Renault: l'hypothèque iranienne et certaines charges pèsent sur le bénéfice
Le gel de l'activité en Iran et des charges de restructuration ont pesé en 2013 sur le bénéfice net de Renault , avec un chiffre d'affaires quasiment stable à 40,9 milliards d'euros.
Le deuxième constructeur automobile français a vu son bénéfice net pratiquement divisé par trois, à 586 millions d'euros, contre 1,75 milliard en 2012, selon un communiqué.
Il a dû passer une provision de 514 millions d'euros à cause du gel de ses activités en Iran depuis l'été dernier, consécutif aux sanctions internationales contre le programme nucléaire iranien.
Le groupe a également subi l'an dernier des dépréciations d'actifs de 488 millions "pour divers programmes véhicules" et des charges de restructuration de 423 millions "principalement liées à l'accord de compétitivité signé en France " au printemps 2013.
Il a en revanche bénéficié de la contribution de ses partenaires, essentiellement son allié japonais Nissan, à hauteur de 1,4 milliard.
Le groupe a de plus essuyé une perte d'exploitation de 34 millions d'euros, contre un bénéfice de 183 millions un an plus tôt.
Renault a cependant réussi à dégager, comme prévu, dans sa branche automobile une marge opérationnelle positive de 495 millions, et un flux de trésorerie disponible ("free cash flow") opérationnel de 827 millions.
Son chiffre d'affaires est resté quasiment stable à 40,9 milliards.
Le groupe a par ailleurs dévoilé jeudi des objectifs de ventes et de rentabilité pour 2017. Il vise ainsi à cet horizon "un chiffre d'affaires de 50 milliards d'euros" et "une marge opérationnelle supérieure à 5% du chiffre d'affaires", et un flux de trésorerie positif chaque année.
Le constructeur n'a que partiellement rempli ses objectifs stratégiques pour la période 2011 - 2013, et avait notamment renoncé rapidement à son objectif initial d'écouler 3 millions de véhicules en 2013 à cause de la crise du marché automobile européen.
laf/fpo
