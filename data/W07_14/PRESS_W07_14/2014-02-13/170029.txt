TITRE: George Clooney : "Lorsque l'on perd les �uvres d'art, on perd la m�moire et l'histoire" dans Laissez-vous tenter le 13-02-2014 sur RTL.
DATE: 2014-02-13
URL: http://www.rtl.fr/emission/laissez-vous-tenter/billet/georges-clooney-lorsque-l-on-perd-les-uvres-d-art-on-perd-la-memoire-et-l-histoire-7769687008
PRINCIPAL: 170026
TEXT:
Par St�phane Boudsocq | Publi� le 13/02/2014 � 10h49 | Laissez-vous tenter
George Clooney � la premi�re du film "Monuments men" � Londres en f�vrier 2014
Cr�dit : Andrew Cowie / AFP
RENCONTRE - Un avant-go�t de l'entretien exceptionnel que George Clooney a accord� � "RTL" pour la sortie prochaine de son film "Monuments men".
George Clooney a accord� un long entretien � RTL pour la sortie de son nouveau film Monuments men. Avec Jean Dujardin et Matt Damon, le film sortira le 12 mars prochain.
George Clooney nous parle de son prochain film, "Monuments men"
Cr�dit : St�phane Boudsocq
Un film sur la qu�te des �uvres d'art disparues pendant la guerre
Le film raconte comment des sp�cialistes de l'art se sont engag�s � la fin de la seconde guerre mondiale pour retrouver les �uvres que les nazis avaient vol�es et voulaient d�truire dans la d�route hitl�rienne.
Il rappelle que l'art est souvent une victime collat�rale des conflits : de l'Irak � l��gypte en passant par les Khmers rouges, les talibans, les djihadistes maliens qui avaient d�truit les derniers mausol�es de Tombouctou ... Bref, les exemples ne manquent pas et George Clooney est tr�s concern� par ce th�me-l�.
On aurait d� prot�ger les mus�es d'Irak
Georges Clooney
"C'est une chose terrifiante, ce que ces hommes et ces femmes ont fait, pendant la seconde guerre mondiale, c'�tait la premi�re fois qu'une telle chose existait en temps de guerre", s'indigne George Clooney � propos de la destruction des �uvres d'art. "On aurait d� prot�ger les mus�es d'Irak, poursuit-il. Il ne s'agit pas seulement de l'Irak qui a perdu une part de son histoire, c'est le monde entier. Lorsque l'on perd tout cela, on perd la m�moire et l'histoire", conclut le com�dien et r�alisateur.
George Clooney, Jean Dujardin et Matt Damon seront les invit�s exceptionnels de Laissez-vous tenter le vendredi 7 mars � 9h00.
The Monuments Men - Official Trailer (2013) [HD] George Clooney, Matt Damon
La r�daction vous recommande
