TITRE: Sochaux: H.Renard futur sélectionneur du Maroc
DATE: 2014-02-13
URL: http://www.foot-sur7.fr/130625-sochaux-hrenard-futur-slectionneur-du-maroc
PRINCIPAL: 170968
TEXT:
Sochaux: H.Renard futur s�lectionneur du Maroc
Auteur : Michael Beaupres / Publi� le : 13 f�vrier 2014
Mal en point en championnat avec une 19�me place, le FC Sochaux devrait perdre son entra�neur, Herv� Renard, � la fin de la saison. La s�lection du Maroc lui tend les bras.
Un petit tour et puis s'en va... C'est ainsi qu'on devrait renommer la relation entre Sochaux et Herv� Renard en juin prochain. A cette p�riode, il aura quitt� le Doubs pour retrouver une Afrique qu'il conna�t (NDLR, notamment en tant qu'ancien s�lectionneur de la Zambie) et plus pr�cis�ment le Maroc.
L'agent du technicien, Youssef Haijoub, a confirm� � l'AFP que "Herv� Renard est la priorit� du Maroc. Il devrait �tre choisi comme s�lectionneur d�s que le nouveau pr�sident de la F�d�ration marocaine de football (FRMF) sera en place. Le contrat sera sign� en avril".
Sa mission est vraisemblablement de mener les Lions de l'Atlas � la prochaine Coupe d'Afrique des Nations, pr�vue en 2015 et organis�e par le...Maroc.
5 0 vote(s)
