TITRE: ETHIQUE: Cerveau �augment�: faut-il craindre les �surhommes�? -  News Vivre: Soci�t� - tdg.ch
DATE: 2014-02-13
URL: http://www.tdg.ch/vivre/societe/Les-performances-cerebrales-augmentees-inquietent/story/23964600
PRINCIPAL: 0
TEXT:
Votre adresse e-mail*
Votre email a �t� envoy�.
Un regard per�ant m�me la nuit, des capacit�s de calcul immenses... l'essor des techniques d'am�lioration c�r�brale pourrait bient�t cr�er des �surhommes�, mais au risque de faire �merger une petite caste privil�gi�e, met en garde le Comit� d'Ethique fran�ais.
Les techniques d'optimisation des performances neurologiques, intellectuelles et �motionnelles comportent des enjeux de sant� publique mais �galement de choix de soci�t�, souligne le Comit� consultatif national d'Ethique fran�ais (CCNE) dans un avis prospectif sur la �neuro-am�lioration� biom�dicale des personnes en bonne sant�, rendu public mercredi.
Humains �augment�s�
Les candidats au statut d'humain �augment� recourent  � des techniques d�tourn�es de leur objectif initial, th�rapeutique ou de recherche, �dans un but suppos� d'am�lioration psycho-cognitive�: il s'agit de m�dicaments stimulants (Ritaline, Modafinil...) ou pour se sentir bien (anxiolytiques, antid�presseurs...) mais �galement d'appareils ou casques m�dicaux de stimulation c�r�brale, magn�tique ou �lectrique, �transcr�nienne� (sans n�cessit� de percer l'os) notamment, ou encore de techniques pour apprendre soi-m�me � modifier en temps r�el sa propre activit� c�r�brale (�neurofeedback�) afin de mieux se concentrer ou se relaxer...
Une am�lioration a pu �tre observ�e mais elle est �inconstante, modeste, parcellaire et ponctuelle�, note le Comit� d'Ethique. De plus, le rapport b�n�fice/risque � long terme est �totalement inconnu� et l'exemple des amph�tamines auxquelles s'apparente la Ritaline fait craindre un �risque probable d'addiction�.
Changement d'�chelle et de nature des technologies
Le Comit� d'Ethique, qui appelle � la prudence et � la rigueur scientifique, d�conseille fortement d'user de ces techniques �chez l'enfant, l'adolescent et les personnes vuln�rables�.
L'explosion des recherches sur le cerveau, l'implication consid�rable des militaires dans ces recherches, les avanc�es attendues de la convergence �NBIC� -  combinant nanotechnologies, biotechnologies, technologies de l'information et sciences cognitives - concourent au changement d'�chelle et de nature des technologies de neuro-am�lioration, note-t-il.
De la science-fiction au r��l
Avec les nouvelles modalit�s de perception (�il bionique, vision infrarouge, etc.) et de commande � distance, au moyen d'interfaces cerveau/machine, pour �crire sur un ordinateur ou d�placer un objet, l'�volution est rapide et �ne rel�ve plus de la science-fiction�.
Ce ph�nom�ne de soci�t� prend de l'ampleur, mais �il n'y a pas d'�tudes en France, comme il y en a sur le tabac, qui permettraient de mieux cerner sa fr�quence�, d�plore le Pr Marie-Germaine Bousser, co-rapporteur, avec ce travail prospectif du CNNE. En particulier, on ne sait pas combien d'enfants prennent de la Ritaline pour mieux r�ussir � l'�cole, dit-elle.
Classe sociale �am�lior�e�
Sur le plan social, le comit� pointe le risque d'�mergence d'une classe sociale �am�lior�e�, constitu�e d'une minorit� d'individus ais�s, contribuant � aggraver encore l'�cart entre riches et  pauvres.
Au point que ces derniers, les �non augment�s� courent le risque d'�tre consid�r�s comme pathologiques ou �diminu�s�. Mis � part un usage contraint chez les militaires (lutte contre le sommeil, la fatigue voire contre l'empathie des pilotes de drones envers leurs cibles), la soci�t� de comp�tition peut pousser les individus � rechercher une am�lioration de performances.
Micro�lectrodes dans le cerveau
De surcro�t, une coercition sur les personnes consid�r�es comme �antisociales� ou agressives n'est pas � �carter, d'autant que des neurochirurgiens am�ricains n'excluent m�me pas de soumettre ce genre de sujets � la stimulation c�r�brale profonde, utile pour certains Parkinson.
Or, cette technique suppose d'implanter des micro�lectrodes directement dans le cerveau (avec pourtant 2 % � 5 % de risque d'infection ou d'accident vasculaire c�r�bral).
�La scolarisation modifie aussi la structure neuronale�, rappelle le  philosophe des sciences Amin Benmakhlouf, co-rapporteur de l'avis. Il importe en effet que ces techniques ne se d�veloppent pas au d�triment de m�thodes de base susceptibles de favoriser le d�veloppement psycho-cognitif : au premier rang desquelles, la nutrition l'�ducation, l'apprentissage et l'activit� physique, �d�j� si in�galement r�parties�.
Au regard des enjeux multiples, �une veille �thique� s'impose pour le Comit�. (afp/Newsnet)
Cr��: 12.02.2014, 22h58
