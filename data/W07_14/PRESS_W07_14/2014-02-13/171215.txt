TITRE: �cole : la cl� USB renvoyait vers un site porno
DATE: 2014-02-13
URL: http://www.reponseatout.com/insolite/drole/ecole-cle-usb-renvoyait-site-porno-besancon-a1012190
PRINCIPAL: 171209
TEXT:
�cole : la cl� USB renvoyait vers un site porno
Le 13/02/2014 � 18:09:59
Vues : 453 fois JE REAGIS
La municipalit� d�nonce un � d�tournement l�gal, pervers et inadmissible � et pourrait envisager des poursuites. - cr�dit photo : DragonImages | � ThinkStock
� Besan�on (Doubs), des parents d��l�ves de CE2 ont constat� que la cl� USB remise par l��cole contenait des liens renvoyant vers un site pornographique et un jeu de guerre. La municipalit� pr�sente ses excuses aux familles.
Voil� une bourde bien f�cheuse. Dans une �cole de Besan�on (Doubs), les �l�ves de CE2 ont re�u des cl�s USB cens�es contenir des liens vers des sites �ducatifs. Ils sont en r�alit� tomb�s sur un site pornographique et un jeu de guerre assez violent. Alert�e par des parents, la municipalit� a envoy� une lettre d�excuses � 6 000 familles.
Des noms de domaines rachet�s par des propri�taires de sites pornos
La Ville, qui se dit � sinc�rement [d�sol�e] de cette situation �, demande aux parents de lui rapporter les cl�s US pour � proc�der � leur remplacement �. Une recommandation qui concerne aussi les appareils distribu�s les ann�es pr�c�dentes. En effet, les noms de domaines des sites r�pertori�s par l��tablissement ont r�cemment �t� rachet�s par des propri�taires de sites pornographiques. La technique est courante. Ils ach�tent des noms de domaine uniquement pour y publier des liens renvoyant vers leur site afin d�am�liorer leur r�f�rencement au niveau des moteurs de recherche.
La ville de Besan�on serait hors de cause
La municipalit� d�nonce un � d�tournement l�gal, pervers et inadmissible � et pourrait envisager des poursuites. Compte tenu des circonstances, la mairie et la communaut� ne devraient pas �tre tenues pour responsables. Le directeur g�n�ral des services de la ville de Besan�on, Patrick Ayache, pr�cise que les liens ont �t� v�rifi�s avant de donner les cl�s aux enfants. Par ailleurs, le nombre d��l�ves ayant pu voir des images tendancieuses serait r�duit puisque les ordinateurs fournis dans le cadre de ce projet sont �quip�s d�un contr�le parental.
