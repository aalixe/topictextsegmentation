TITRE: Ren� Teulade. Fran�ois Hollande perd "un ami"
DATE: 2014-02-13
URL: http://www.ouest-france.fr/rene-teulade-francois-hollande-perd-un-ami-1928525
PRINCIPAL: 0
TEXT:
Ren� Teulade. Fran�ois Hollande perd "un ami"
France -
Ren� Teulade et Fran�ois Hollande, en mai 1997, sur un march� de Corr�ze. �|�Photo : AFP.
Facebook
Achetez votre journal num�rique
Le chef de l'Etat a exprim� sa "tristesse" apr�s le d�c�s de l'ancien ministre socialiste Ren� Teulade, qui fut son suppl�ant � l'Assembl�e.
"Je perds un ami", �crit le pr�sident de la R�publique dans un communiqu�, disant "sa plus grande tristesse".
Serviteur de la solidarit�
"Cet homme, n� dans un milieu modeste, devait tout � l'�cole de la R�publique. Il avait donc d�cid� de consacrer sa vie � une grande et belle id�e : la solidarit�", selon Fran�ois Hollande.
"Des bancs de l'Ecole normale d'instituteurs de Tulle � la pr�sidence de la Mutualit� fran�aise, puis comme ministre des affaires sociales sous le gouvernement de Pierre B�r�govoy, il a servi cette cause avec ardeur", estime le chef de l'Etat.
"Sa m�moire est particuli�rement ch�re � la Corr�ze, qu'il a servie comme maire d'Argentat pendant 25 ans et repr�sent�e au S�nat depuis 2008", rappelle-t-il.
"Homme de conviction"
Dans un communiqu� distinct, le Premier ministre a salu� "un homme de conviction, qui a consacr� sa vie au mouvement mutualiste fran�ais" et dont l'"engagement a �t� d�cisif pour le d�veloppement des mutuelles dans notre pays et pour l'am�lioration de la protection sociale de nos concitoyens".
Tags :
