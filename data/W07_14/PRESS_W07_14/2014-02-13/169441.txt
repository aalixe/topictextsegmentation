TITRE: Mozilla : de la publicit� sur la page d'accueil de Firefox ?
DATE: 2014-02-13
URL: http://www.linformatique.org/mozilla-de-la-publicite-sur-la-page-daccueil-de-firefox/
PRINCIPAL: 169431
TEXT:
Accueil � Technologie � Mozilla : de la publicit� sur la page d�accueil de Firefox ?
Mozilla : de la publicit� sur la page d�accueil de Firefox ?
Publi� par : Emilie Dubois 13 f�vrier 2014 dans Technologie
Voter pour ce post
� l�encontre de son principe d��tre une fondation � but non lucratif, Mozilla pourrait se lancer dans la publicit�, du contenu sponsoris� pour �tre exact.
� la faveur de son engagement en tant qu�organisme � but non lucratif, la Fondation Mozilla se finance principalement par des accords avec les moteurs de recherche, majoritairement Google. Histoire de trouver d�autres financements, ou de se diversifier, la fondation vient pourtant d�annoncer des intentions de se lancer dans la publicit�, du contenu sponsoris� pour �tre exact.
Les pr�tentions de Mozilla sont pour le moment bien modeste, elles pr�voient simplement l�affichage de contenus sponsoris�s sur la page d�ouverture du navigateur Firefox. Au lieu de proposer les habituelles, neuf tuiles des sites les plus utilis�s, le projet Directory Tiles pr�voit l�affichage de liens vers les produits Mozilla d�une part et des contenus sponsoris�s � populaires de la r�gion �.
La communaut� est bien �videmment entr�e en �bullition d�s cette annonce, avec d�j� de nombreuses menaces de quitter le navire pour aller voir la concurrence.
Si la Fondation Mozilla poursuit dans cette voie, m�me modeste, une premi�re question �thique se pose au sujet des v�ritables valeurs de la fondation, mais aussi de savoir ce qu�il adviendra dans le futur, car que se passera-t-il une fois que le doigt aura �t� mis dans l�engrenage ?
Mozilla passe � la pub
Share the post "Mozilla : de la publicit� sur la page d�accueil de Firefox ?"
