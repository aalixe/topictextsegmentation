TITRE: Sotchi 2014: Ski alpin/Super-combin� messieurs: quatre en qu�te d'or
DATE: 2014-02-13
URL: http://quebec.huffingtonpost.ca/2014/02/13/sotchi-2014-ski-alpinsuper-combine-messieurs-quatre-en-quete-dor_n_4779512.html?utm_hp_ref%3Dquebec-sports
PRINCIPAL: 170421
TEXT:
Sotchi 2014: Ski alpin/Super-combin� messieurs: quatre en qu�te d'or
AFPQC �|� Par Agence France-Presse
Quatre skieurs pour l'or (AP Photo/Charles Krupa)         | ASSOCIATED PRESS
Recevoir Notre Infolettre:
Inscription
Suivre:
Sports , JO Sotchi , Jeux olympiques de Sotchi 2014 , Alexis Pinturault , Alexis Pinturault Sotchi , Descente , Jeux Olympiques Sotchi , Ski Sotchi , JEUX OLYMPIQUES DE SOTCHI , Canada Quebec News
Quatre favoris --le Fran�ais Alexis Pinturault, la paire am�ricaine Ted Ligety-Bode Miller et le Croate Ivica Kostelic-- se d�tachent pour l'or du super-combin� masculin des Jeux de Sotchi, vendredi � Rosa Khoutor.
Chacun a des arguments. Ligety est le champion du monde 2013, Miller avait gagn� le titre olympique � Vancouver, o� il avait devanc� Kostelic. Pinturault, le cadet (22 ans) du quatuor, a �t� des quatre podiums de Coupe du monde ces deux derni�res saisons.
Tr�s d��u par sa 8e place en descente, dimanche, Miller, 36 ans, craint pourtant que les temp�ratures printani�res ne favorisent les slalomeurs.
"Si la neige avait �t� glac�e, les descendeurs pouvaient creuser des �carts. Ce ne sera pas possible dans ces conditions, surtout en partant derri�re", a remarqu� le champion du New Hampshire qui est loin d'�tre fiable entre les piquets serr�s.
Pour pr�server la piste, les organisateurs ont d'ailleurs avanc� le d�part de la descente raccourcie (3,2 km), de 11h00 � 10h00 locales (de 07h00 � 06h00 GMT).
Comme � Schladming?
Le blond Ligety se veut plus positif que son compatriote. "Je me sens dans la m�me forme qu'aux Mondiaux de Schladming (en f�vrier 2013). Mais je ne sais pas si ce sera pour le m�me r�sultat", a soulign� le skieur de Park City (Utah). A Schladming, "Ted le kid" avait "d�valis�" la banque avec trois ors.
Vainqueur du 1er super-combin� de Kitzb�hel, il y a deux semaines, Pinturault n'a pas r�ussi jeudi � "corriger les deux, trois lignes" qu'il s'�taient fix�es, ni � �augmenter le rythme�, comme il souhaitait. �Il faudra rester calme et juste�, a conclu le Savoyard, meilleur slalomeur du lot.
Kostelic, 34 ans, aura l'avantage d'une manche de slalom piquet�e par son p�re Ante, qui fait dans les trac�s tortueux.
Le Norv�gien Aksel Lund Svindal, le Fran�ais Thomas Mermillod Blondin sont deux autres pr�tendants au podium. Dans la liste des possibles, on peut ajouter l'Italien Peter Fill, l'Autrichien Romed Baumann, Natko Zrncic-Dim, autre Croate, et le jeune Norv�gien Aleksander Aamodt Kilde.
Le Tch�que Ondrej Bank, ex-slalomeur de bon niveau, est venu aussi se rappeler au bon souvenir de ses adversaires en signant jeudi le meilleur chrono du dernier entra�nement de la descente.
A 33 ans, Bank n'a qu'un seul podium en Coupe du monde, mais c'est justement en super-combin�, en 2007.
Le programme:
Manche de slalom: 15h30 (11h30 GMT)
� voir aussi sur Le HuffPost:
Loading Slideshow
Jones, Humphries, Junio et Bilodeau parmi les favoris pour porter le drapeau � la c�r�monie de cl�ture.
Les Canadiens ratent le bronze de peu en patinage longue piste poursuite par �quipe.
Le choix des arbitres pour la finale de hockey soul�ve un toll� en Su�de.
Une fondeuse ukrainienne a �chou� un test anti-dopage, a annonc� le comit� olympique de son pays samedi.
Anderson a sembl� dans le coup face au Slov�ne Zan Kosir, tant en premi�re qu'en deuxi�me manche des huiti�mes de finale, apr�s que le Qu�b�cois eut affich� le 15e chrono des qualifications.
La Finlande �crase les �tats-Unis et obtient la m�daille de bronze.
Ski de fond f�minin: la Norv�ge balaie le podium au 30 km group�.
Une finale olympique de hockey toute en d�fense est � pr�voir.
L'une des trois formations canadiennes du bobsleigh masculin � quatre a bascul� sur le c�t� dans la derni�re portion du parcours, jetant un froid autour de la piste dans l'attente de voir dans quel �tat se trouvaient les athl�tes.
Le Canada conteste les r�sultats en ski cross et tente d'obtenir l'or pour Brady Leman.
Les Canadiennes ont combl� un �cart de deux buts avec seulement 3:26 � faire en troisi�me pour finalement vaincre les Am�ricaines 3-2 en prolongation sur le deuxi�me but du match de Marie-Philip Poulin et ainsi mettre la main sur <a href="http://quebec.huffingtonpost.ca/2014/02/20/sotchi-finale-hockey-feminin-etats-unis-canada_n_4825185.html">la m�daille d'or du hockey f�minin des Jeux de Sotchi</a>, leur quatri�me titre olympique cons�cutif.
Les Canadiennes ont combl� un �cart de deux buts avec seulement 3:26 � faire en troisi�me pour finalement vaincre les Am�ricaines 3-2 en prolongation sur le deuxi�me but du match de Marie-Philip Poulin et ainsi mettre la main sur <a href="http://quebec.huffingtonpost.ca/2014/02/20/sotchi-finale-hockey-feminin-etats-unis-canada_n_4825185.html">la m�daille d'or du hockey f�minin des Jeux de Sotchi</a>, leur quatri�me titre olympique cons�cutif.
L'�quipe de la Canadienne Jennifer Jones a remport� une m�daille d'or aux Jeux olympiques de Sotchi en vertu d'une victoire de 6-3 face � la Su�de<a href="http://quebec.huffingtonpost.ca/2014/02/20/sotchi-curling-feminin-or-jennifer-jones_n_4823694.html" target="_blank"> en finale du tournoi de curling f�minin</a>.
La patineuse artistique canadienne Kaetlyn Osmond a termin� au 13e rang � l'issue du programme libre, le jeudi 20 f�vrier. La Canadienne Gabrielle Daleman, quant � elle, a termin� en 17e position. C'est la Russe Adelina Sotnikova qui a remport� l'or.
La Canadienne Rosalind Groenewoud, qui a remport� une m�daille d'argent aux X-Games le mois dernier, a termin� septi�me <a href="http://quebec.huffingtonpost.ca/2014/02/20/sotchi-ski-demi-lune-bowman-or-groenewoud-7e_n_4824002.html" target="_blank">en demi-lune en ski</a>, le jeudi 20 f�vrier. Elle a fait une chute lors de la finale.
Le Canadien Brady Leman a fini quatri�me � la suite de la finale disput�e � quatre skieurs � <a href="http://quebec.huffingtonpost.ca/2014/02/20/sotchi-ski-cross-brady-leman-podium-francais_n_4821614.html" target="_blank">l'�preuve de ski cross masculin</a>, le jeudi 20 f�vrier. Ce sont trois Fran�ais qui ont termin� sur les trois marches du podium.
Les Canadiennes Kaillie Humphries et Heather Moyse ont conserv� leur titre olympique en <a href="http://quebec.huffingtonpost.ca/2014/02/19/kaillie-humphries-et-heather-moyse-_n_4816849.html" target="_blank">s'assurant la m�daille d'or</a> de l'�preuve de bobsleigh f�minin � deux aux Jeux de Sotchi, le mercredi 19 f�vrier.
Le Canada a eu chaud, tr�s chaud m�me. Mais il l'a finalement emport� 2-1 devant la Lettonie <a href="http://quebec.huffingtonpost.ca/2014/02/19/sotchi-match-canada-lettonie_n_4817448.html" target="_blank">en quarts de finale du tournoi de hockey masculin</a> des Jeux olympiques de Sotchi, le mercredi 19 f�vrier.
La Russie a subi l'�limination en quarts de finale du tournoi de hockey masculin des Jeux olympiques � la suite <a href="http://quebec.huffingtonpost.ca/2014/02/19/sotchi-finlande-elimine-la-russie-en-quarts-de-finale-du-tournoi-de-hockey_n_4815170.html" target="_blank">d'une d�faite de 3-1 aux d�pens de la Finlande</a>, le mercredi 19 f�vrier.
La Russie a subi l'�limination en quarts de finale du tournoi de hockey masculin des Jeux olympiques � la suite <a href="http://quebec.huffingtonpost.ca/2014/02/19/sotchi-finlande-elimine-la-russie-en-quarts-de-finale-du-tournoi-de-hockey_n_4815170.html" target="_blank">d'une d�faite de 3-1 aux d�pens de la Finlande</a>, le mercredi 19 f�vrier. Sur la photo, un partisan russe qui avait la mine basse.
Le Canadien Michael Lambert est tomb� lors de la ronde de qualification de l'�preuve de slalom g�ant parral�le de planche � neige aux Jeux olympiques de Sotchi, le mercredi 19 f�vrier.
Le Canadien Philip Brown tente de reprendre son �quilibre lors de la premi�re ronde de l'�preuve de slalom g�ant en ski alpin, le mercredi 19 f�vrier.
L'�qupe masculine de curling l'a emport� 10-6 contre la Chine en demi-finale, le mercredi 19 f�vrier.
Le Canada a obtenu son billet pour la grande finale, le mercredi 19 f�vrier, gr�ce � un gain de 6-4 contre la Grande-Bretagne dans l'une des deux demi-finales du tournoi f�minin de curling.
L'Italienne Gaia Vuerich et la Canadienne Daria Gaiazova sont �tendues sur la neige apr�s avoir franchi le fil d'arriv�e lors de la demi-finale de l'�preuve f�minine de sprint par �quipe en ski de fond, le mercredi 19 f�vrier. Daria Gaiazova a termin� au 5e rang.
La Canadienne Kaetlyn Osmond a termin� au 13e rang lors du programme court en patinage artistique, le mercredi 19 f�vrier.
La Canadienne Gabrielle Daleman a termin� au 19e rang lors du programme court en patinage artistique, le mercredi 19 f�vrier.
La patineuse de vitesse canadienne Ivanie Blondin a termin� au 14e rang lors de l'�preuve du 5000m, le mercredi 19 f�vrier.
Mike Riddle remporte une m�daille d'argent en demi-lune.
Ervins Mustukovs, de la Lettonie, c�l�bre son but dans un filet d�sert en fin de 3e p�riode contre la Suisse.
Marie-Eve Drolet, Jessica Hewitt, Val�rie Maltais et Marianne St-Gelais ont remport� l'argent au 3000 m courte piste.
Marianne St-Gelais chute � l'�preuve du 1000 m.
D�ception de Charles Hamelin apr�s sa chute au 500 m courte piste.
Les patineurs artistiques canadiens Tessa Virtue et Scott Moir <a href="http://quebec.huffingtonpost.ca/2014/02/17/sotchi-virtue-et-moir-remportent-la-medaille-dargent-en-danse-sur-glace_n_4803983.html" target="_blank">ont remport� l'argent</a> aux Jeux olympiques de Sotchi, le lundi 17 f�vrier.
Meghan Agosta-Marciano du Canada c�l�bre le but de sa co�quipi�re Natalie Spooner lors de la <a href="http://quebec.huffingtonpost.ca/2014/02/17/match-canada-suisse-sotchi-hokey-feminin_n_4804296.html" target="_blank">demi-finale de hockey f�minin </a>opposant le Canada � la Suisse, le lundi 17 f�vrier. Le Canada a vaincu la Suisse 3-1 et affrontera les �tats-Unis en finale.
Les Am�ricaines ont vaincu les Su�doises 6-1 lors de la demi-finale de hockey f�minin, le lundi 17 f�vrier. Les �tats-Unis affronteront le Canada en finale.
L'�quipe canadienne #3 de bobsleigh � deux a termin� au 6e rang, le lundi 17 f�vrier. C'est la Russie qui a remport� l'or.
Le brouillard �tait encore au rendez-vous, le lundi 17 f�vrier en matin�e. L'�preuve du biathlon 15 km a �t� report�e � mardi.
Le skieur chinois Jia Zongyang a fait une chute lors de l'�preuve de ski acrobatique, le lundi 17 f�vrier. Heureusement, il n'a pas �t� bless� s�rieusement, mais devait �tre un peu �tourdi apr�s avoir fait quelques culbutes.
Les athl�tes sont align�es lors de l'�preuve f�minine de biathlon 12,5 km d�part group�, le lundi 17 f�vrier. C'est la Bi�lorusse Darya Domracheva qui a remport� la m�daille d'or. La Canadienne Megan Imrie a termin� au 28e rang.
L'�preuve masculine de snowboard cross a �t� report�e en raison du brouillard, le lundi 17 f�vrier.
La planchiste Dominique Maltais c�l�bre sa m�daille d'argent en snowboard cross, le dimanche 16 f�vrier.
Le skieur Jan Hudec a remport� la m�daille de bronze lors de l'�preuve du Super-G en ski alpin, le dimanche 16 f�vrier.
Le gardien de but d'�quipe Canada, Carey Price, est �tendu dans son filet alors que le Finlandais Leo Komarov tente de marquer un but lors de la partie entre le Canada et la Finlande au hockey sur glace, le dimanche 16 f�vrier. Le Canada a vaincu la Finlande 2-1 en prolongation.
Les Canadiens Tessa Virtue et Scott Moir ont termin� en deuxi�me position apr�s le programme court de danse en patinage artistique, le dimanche 16 f�vrier.
L'�quipe canadienne masculine de bobsleigh � deux #1, lors de sa descente dans la premi�re ronde, le dimanche 16 f�vrier.
L'�quipe jama�caine de bobsleigh � deux #1 lors de la premi�re ronde de la comp�tition, le dimanche 16 f�vrier.
Le brouillard �tait au rendez-vous � l'�preuve du biathlon 15 km d�part group�, qui a �t� report�e � lundi. Des b�n�voles en ont profit� pour faire un bonhomme de neige, le dimanche 16 f�vrier.
Les partisans du Canada au curling ont de beaux chapeaux! L'�quipe canadienne masculine a remport� ses deux parties, le dimanche 16 f�vrier, en battant les �tats-Unis 8-6 et la Chine 9-8
Pas facile le ski de fond! Deux athl�tes masculins tentent de reprendre leur souffle lors de l'�preuve de ski de fond 4x10 km relais, le dimanche 16 f�vrier.
Le skieur am�ricain Bode Miller est r�confort� par sa femme apr�s sa performance au Super-G en ski alpin, le dimanche 16 f�vrier.
