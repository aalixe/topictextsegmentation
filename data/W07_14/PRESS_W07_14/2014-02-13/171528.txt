TITRE: Le nouvel Apple TV pr�sent� en avril, mais vendu � No�l | Macworld.fr
DATE: 2014-02-13
URL: http://www.macworld.fr/divers/actualites,nouvel-apple-tv-presente-en-avril-vendu-a-noel,536511,1.htm
PRINCIPAL: 171523
TEXT:
Le nouvel Apple TV pr�sent� en avril, mais vendu � No�l
Le nouvel Apple TV pourrait �tre pr�sent� rapidement, mais disponible plus tard.
Selon Bloomberg , la soci�t� Apple pr�sentera bien son nouvel Apple TV dans le courant du mois d'avril prochain. Cependant, le nouveau boitier Apple � mettre sous le t�l�viseur ne sera pas commercialis� avant la fin de l'ann�e. D'apr�s les informations obtenues par le journaliste, la soci�t� californienne n�gocierait avec les fournisseurs de contenus comme Warner Bros, CNN ou HBO afin de proposer leurs films et s�ries sur l'Apple TV.
Le principal probl�me viendrait du fait de l'identifiant unique que veut utiliser Apple pour son Apple TV. La soci�t� californienne comme � son habitude voudrait utiliser l'Apple ID pour identifier les utilisateurs alors que certains fournisseurs dont ComCast et DirecTV pr�f�reraient une inscription directe � leurs services afin d'avoir un contact avec les clients.
Bloomberg pr�cise �galement que ce nouvel Apple TV disposera d'un processeur plus puissant et d'une toute nouvelle interface. En revanche, elle n'a pas pr�cis� l'arriv�e d'un App Store pour l'Apple TV, ce qui semble pourtant in�vitable, notamment pour le jeu.
Sur le m�me th�me
