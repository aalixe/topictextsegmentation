TITRE: Deux d�cennies apr�s "Les trois fr�res", Les Inconnus racontent la suite - rts.ch - info - culture
DATE: 2014-02-13
URL: http://www.rts.ch/info/culture/5609810-deux-decennies-apres-les-trois-freres-les-inconnus-racontent-la-suite.html
PRINCIPAL: 169450
TEXT:
Deux d�cennies apr�s "Les trois fr�res", Les Inconnus racontent la suite
14.02.2014 10:47
Didier Bourdon, Pascal Legitimus et Bernard Campan s'�taient engag�s dans des carri�res solos il y a 13 ans. [AFP]
Les Inconnus renfilent les costumes des demi-fr�res Latour pour le tr�s attendu "Les trois fr�res, le retour". Le menu cin�ma est compl�t� par du fantastique avec "La Belle et la B�te" et une cin�aste escroqu�e dans "Abus de faiblesse".
Apr�s avoir entam� leurs carri�res solos il y a d�j� 13 ans, les Fran�ais Didier Bourdon, Bernard Campan et Pascal L�gitimus se sont retrouv�s pour r�aliser "Les trois fr�res, le retour".
Les Inconnus, qui signent le sc�nario et la r�alisation, renfilent ainsi les costumes des demis-fr�res Latour, h�ros de leur premier film et grand succ�s sorti en 1995 ("Les trois fr�res"). R�unis par leur m�re d�funte, ils doivent s'acquitter de ses dettes.
Bernard, Didier et Pascal sont en quasi-faillite personnelle, devenus com�dien rat�, vendeur de sextoys alors que le troisi�me joue les gigolos.
La vie chamboul�e des trois frangins
De quiproquos en situations cocasses,�les trois frangins voient leur vie � nouveau chamboul�e,�entre fille sortie de l'ombre et belle-m�re coriace.
L'id�e de donner une suite aux aventures des Trois fr�res a germ� lorsqu'ils se sont retrouv�s sur sc�ne ensemble, il y a un peu plus de 2 ans.
Jug�s "has been", Les Inconnus ont eu de la peine � motiver les producteurs. Ils ont d� se r�soudre � tourner gratuitement et ne toucheront une prime sur chaque entr�e qu'� partir d'un million et demi d'entr�es dans les salles.�Et si un nouveau succ�s propulsait � nouveau les trois complices sur sc�ne?
�
La Belle et la B�te: moderne et intacte
Apr�s une dizaine d'adaptations, la l�gende de "La Belle et la B�te" revient sur grand �cran. C'est le Fran�ais Christophe Gans qui redonne vie au conte en s'appuyant sur les effets sp�ciaux modernes et les images de synth�se.
Amateur de cin�ma fantastique, Christophe Gans ("Crying Freeman", "Le pacte des Loups", "Silent Hill") s'est inspir� de l'oeuvre de Madame de Villeneuve, auteure de la premi�re version en 1740.
Belle est incarn�e par L�a Seydoux,�en lice pour le C�sar de la meilleur actrice avec "La Vie d'Ad�le", et Vincent Cassel joue le prince arrogant�sous son impressionnant costume de "B�te".
F��rie et m�lancolie chez la B�te
L'histoire d�bute en 1810.�Apr�s le naufrage de ses navires, un marchand ruin� (Andr� Dussollier) doit s'exiler � la campagne avec ses six enfants.�Il d�couvre alors le domaine magique de la B�te. Mais celle-ci le condamne � mort pour lui avoir vol� une rose.
Belle, la plus jeune fille du marchand, se sacrifie � la place de son p�re. Elle d�couvre alors peu � peu l'histoire tragique de la B�te, dans son ch�teau, entre f��rie et m�lancolie.
Christophe Gans a choisi de respecter � la lettre le conte de f�es, cr�ant un "film de divertissement, fait pour le spectateur", a d�clar� L�a Seydoux � l'AFP.
�
"Abus de faiblesse": la cin�aste escroqu�e
Dans "Abus de faiblesse", la cin�aste Catherine Breillat raconte sa propre histoire, qu'elle avait d�j� d�voil�e dans un livre du m�me nom en 2009. C'est Isabelle Huppert qui incarne la r�alisatrice, affaiblie par un AVC et escroqu�e par son acteur principal.
Elle d�couvre ce dernier, jou� par le rappeur Kool Shen (ex-membre du groupe NTM), en regardant un talk-show t�l�vis�. La r�alisatrice veut cet�arnaqueur de c�l�brit�s pour son prochain film. Ils se rencontrent et se lient.
L'escroc emprunte alors beaucoup d'argent � la cin�aste,�tout en lui donnant une impression de chaleur familiale.
�
