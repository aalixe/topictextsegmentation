TITRE: AMD Radeon R7�265�: une Radeon HD 7850 gonfl�e
DATE: 2014-02-13
URL: http://www.lesnumeriques.com/carte-graphique/amd-radeon-r7-265-2-go-p19466/amd-radeon-r7-265-radeon-hd-7850-gonflee-n33215.html
PRINCIPAL: 170462
TEXT:
Nous soutenir, navigation sans publicit� Premium : 2�/mois + Conseil personnalis� Premium+ : 60�/an
AMD Radeon R7�265�: une Radeon HD 7850 gonfl�e
AMD continue sa saison de renommage
�
Publi� le: 13 f�vrier 2014 14:25
Par R�gis Jehl
Envoyez-moi un mail lorsque le prix descend sous :
�
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Plus de propositions Moins de propositions
Apr�s le renommage de la HD 7770 en R7�250X , voici donc la HD�7850 qui se transforme en Radeon R7�265. Le GPU de la nouvelle venue est toujours un Pitcairn compos� de 1024 unit�s de calcul, 64�unit�s d�di�es aux textures et 32�unit�s de rendu, le tout �tant accompagn� de 2�Go de m�moire graphique sur bus 256�bits.
�
Il ne s'agit toutefois pas d'une copie carbone puisque AMD en a profit� pour lifter l�g�rement les fr�quences de fonctionnement. Le GPU passe ainsi de 860�Hz � 925�MHz "au maximum". La fr�quence sera donc variable, en fonction de la charge et de la temp�rature atteinte par la puce. La m�moire graphique �volue �galement et les 1�200�MHz sont troqu�s pour des puces fonctionnant � 1�400�MHz.
�
Annonc�e � 130��, la Radeon R7�265 est donc positionn�e au m�me niveau tarifaire que la GeForce GTX�650�Ti�Boost de Nvidia et bien en dessous de sa pr�d�cesseure qui se monnaie actuellement aux alentours de 190�� (150�� en version 1�Go). C�t� performances, les m�gahertz gagn�s par rapport � la Radeon HD�7850 devraient justement permettre de d�passer le mod�le de Nvidia. Reste � voir ce qu'auront les GeForce GTX�750�Ti et GeForce GTX�750 qui arriveront dans les prochains jours.
