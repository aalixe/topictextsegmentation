TITRE: Modeste M�Bami s'engage avec un club colombien
DATE: 2014-02-13
URL: http://fr.starafrica.com/football/modeste-mbami-sengage-avec-le-club-colombien-millonarios-de-bogota.html
PRINCIPAL: 170619
TEXT:
Vous �tes ici : accueil � � la Une � Modeste M�Bami s�engage avec un club colombien
Modeste M�Bami s�engage avec un club colombien
par: Beyram Chelbi publi� le            : 13/02/2014 � 15:04 1 193 vues dans: � la Une , br�ve , Football , football europ�en , mercato
A 31 ans, l�ex-international camerounais Modeste M�Bami s�est engag� avec l��quipe colombienne des Millonarios de Bogota.
L�ancien milieu d�fensif du Paris Saint-Germain et de l�Olympique de Marseille a officialis� son contrat alors que cela fait une semaine et demi qu�il s�entra�ne avec l��quipe.
Arriv�e en provenance d�Al-Ittihad en Arabie Saoudite, le camerounais renforcera ainsi le secteur d�fensif du club Millonarios, actuel 4e du championnat colombien.
Copyright : Starafrica.com
