TITRE: iWatch:vendu avec des �crans incassables?
DATE: 2014-02-13
URL: http://www.infos-mobiles.com/apple/iwatch/iwatch-vendu-avec-des-ecrans-incassables/51139
PRINCIPAL: 169447
TEXT:
Accueil � iWatch:vendu avec des �crans incassables?
iWatch:vendu avec des �crans incassables?
Castella 12 f�vrier 2014 0
On a pu remarquer ces temps-ci qu� Apple tient � �voluer ses produits, le point le plus important �voqu� est souvent les �crans qui cassent. Mais Apple semble trouver les moyens d�am�liorer cette faille afin de toujours satisfaire ses clients.
iWatch�: l��ventuel projet d�Apple pour cette ann�e
Apple , la marque � la pomme voudrait enfin proposer des produits avec des �crans plus solides et plus. C�est dans ce sens que cette firme va avancer ce qu�affirme le groupe, d�ailleurs cette information se confirme par le fait qu�Apple a d�j� command� des grandes quantit�s du produit qui sera livr� � la construction de cette nouvelle version des iWatch.
Ce fameux produit est en fait des cristaux de saphir, l�incorporation de cette mati�re dans le composant de l��cran rendra donc l��cran plus compact mais moins susceptible aux rayures et aux cassures.
C�est un plus qui sera donc apport� aux montres connect�es d�Apple, d�sormais les iWatch seront donc �quip�es d�un �cran plus robustes ce qui tenterait bien plus les possesseurs de la marque � la pomme.
Il est important quand m�me de noter que vu la surface de l��cran d�iWatch est plus petite, dans ce sens, Apple n�aurait pas de mal � produire ce mod�le, car le co�t de production (avec du cristal de saphir pour les montres connect�es) �serait plus raisonnable que celui de l�iPhone.
La rumeur sur l�iPhone 6
R�cemment nous avons vu sur internet qu�Apple penserait � dot� l�iPhone 6 qui b�n�ficierait de cette nouvelle recherche mais plut�t les montres connect�es, les iWatch. Mais cette information est devenue un sujet de discussion pour les sp�cialistes, car ces derniers disent que si Apple pense par la suite apporter cette nouvelle strat�gie dans la fabrication de l�iPhone, le prix de ce Smartphone va certainement augmenter.
Certes, la nouvelle iPhone 6 de la marque Apple avec des �crans incassables ne sera pas encore disponible pour les prochains mois mais ce qui est sur en tout cas c�est qu�Apple penserait � avancer dans ce sens.
Cette marque pense d�sormais proposer des produits plus solides donc moins indestructible, ce qui mettra certainement fin � l�obsession de bon nombre de personne � propos des �crans cass�s ou m�me les rayures sur leur Smartphone. On peut dire alors que la marque � la pomme r�pondra bien aux attentes du march� de la nouvelle technologie, mais cette am�lioration qu�elle tente d�apporter la placerait certainement en haut de la barre vis-�-vis de ses concurrents.
