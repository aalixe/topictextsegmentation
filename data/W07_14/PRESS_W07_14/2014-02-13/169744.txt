TITRE: Huit Corvette "aval�es" par un trou g�ant aux Etats-Unis - rts.ch - info - monde
DATE: 2014-02-13
URL: http://www.rts.ch/info/monde/5610087-huit-corvette-ont-ete-avalees-par-un-trou-geant-aux-usa.html?wysistatpr%3Dads_rss_texte%26rts_source%3Drss_t
PRINCIPAL: 0
TEXT:
Huit Corvette "aval�es" par un trou g�ant aux Etats-Unis
13.02.2014 14:39
Le sol s'est effondr� mercredi dans un hall d'exposition du Mus�e national Corvette situ� dans le Kentucky, emportant avec lui huit mod�les de collection de la marque automobile.
Un trou s'est ouvert mercredi dans le sol d'un hall d'exposition du Mus�e national Corvette, dans le Kentucky (centre-est des Etats-Unis), faisant tomber huit voitures de collection.
Le crat�re, d'environ 12 m�tres de circonf�rence et de 8 � 9 m�tres de profondeur, a �t� d�couvert quand une alarme s'est d�clench�e au petit matin. Les pompiers ont rapidement s�curis� les lieux et les responsables du mus�e tentaient de rep�cher les huit Corvette.
L'accident serait de cause g�ologique. Il s'agirait de la formation d'une doline (excavation dans un sol calcaire en raison du passage de l'eau).
Des mod�les de collection
Le mus�e est situ� dans la ville de Bowling Green, o� sont construites ces voitures de l�gende de General Motors depuis plus de 60 ans.
Parmi les voitures de collection endommag�es, une Corvette de 1962, la millioni�me Corvette sortie des usines en 1992 ou une Corvette de 1993 c�l�brant le 40e anniversaire de la marque.
afp/bri
