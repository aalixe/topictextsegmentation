TITRE: Voyage aux USA : Fran�ois Hollande d�voile des mesures en faveur des start-up
DATE: 2014-02-13
URL: http://www.zdnet.fr/actualites/voyage-aux-usa-francois-hollande-annonce-des-mesures-en-faveur-des-start-up-39797782.htm
PRINCIPAL: 169888
TEXT:
RSS
Voyage aux USA : Fran�ois Hollande d�voile des mesures en faveur des start-up
Technologie : Concluant son voyage d��tat aux �tats-Unis par une visite aux entrepreneurs fran�ais de la Silicon Valley, le pr�sident de la R�publique a annonc� une s�rie d�initiatives pour soutenir les jeunes pousses de l�Hexagone.
Par L'agence EP |
Jeudi 13 F�vrier 2014
��La France doit reconna�tre le dynamisme de ses entrepreneurs��, a d�clar� Fran�ois Hollande lors de sa visite � l�incubateur d'entreprises au French Tech Hub bas� dans la Silicon Valley en Californie. Le chef de l��tat, en visite officielle aux �tats-Unis, s�est exprim� devant un parterre de patrons fran�ais auxquels il a d�voil� une s�rie de mesures pour encourager et soutenir la cr�ation de start-ups en France.
Le mois prochain, une ordonnance visera � assouplir le financement participatif ou crowdfunding. ��Un projet pourra recueillir jusqu'� un million d'euros de pr�t sur une plateforme de financement participatif��, a pr�cis� Fran�ois Hollande. Il a �galement �voqu� les ��passeports talents�� qui faciliteraient l�obtention d�un visa fran�ais pour les entrepreneurs �trangers.
L�autre piste envisag�e est� ��d'am�liorer le r�gime des attributions gratuites d'actions et de bons de souscription de parts de cr�ateurs d'entreprises��, une m�thode l� aussi appliqu�e de longue date outre-Atlantique. (Eureka Presse)
