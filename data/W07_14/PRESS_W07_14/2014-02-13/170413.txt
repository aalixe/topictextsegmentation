TITRE: Stromae grand favori des Victoires de la musique, vendredi � Paris - sudinfo.be
DATE: 2014-02-13
URL: http://www.sudinfo.be/934173/article/culture/musique/2014-02-13/stromae-grand-favori-des-victoires-de-la-musique-vendredi-a-paris
PRINCIPAL: 170411
TEXT:
Stromae grand favori des Victoires de la musique, vendredi � Paris
Afp
Avec six nominations, le chanteur belge Stromae part grand favori des 29e r�compenses des ��Victoires de la musique�� remises vendredi � Paris. Les organisateurs promettent du spectacle malgr� l�absence de Daft Punk, triomphant aux r�cents Grammy Awards, qui a refus� d��tre parmi les artistes �ligibles.
Tweet
AFP
Stromae domine d�une large t�te la liste des nomm�s, r�duite � trois artistes dans la plupart des cat�gories cette ann�e, contre quatre auparavant afin de rendre la soir�e plus dynamique.
Fait plut�t rare, le champion des ventes 2013 (1,5 million d�exemplaires de ��Racine carr�e��) est cit� � deux reprises dans deux cat�gories�: chanson originale et vid�o-clip, avec ��Papaoutai�� et ��Formidable��. Il est �galement en lice pour l�album de chansons de l�ann�e et dans la cat�gorie reine, celle de l�artiste masculin, o� il affrontera Etienne Daho et Christophe Ma�.
Vanessa Paradis, Zaz et le duo homme/femme Lily Wood and The Prick sont de leur c�t� en comp�tition dans la cat�gorie artiste f�minine.
�
�On a de la chance parce que cette ann�e le palmar�s est � la fois compos� d�artistes populaires et de d�couvertes
��, note le pr�sident des Victoires, Christophe Palatre. �
�On y retrouve les plus gros vendeurs de l�ann�e 2013 (Stromae, Ma�tre Gims, Christophe Ma�, Zaz�), des artistes dont les tourn�es ont particuli�rement bien march� (Indochine, Vanessa Paradis�) et aussi des artistes qu�on n�a pas l�habitude de voir en t�l�, mais qui sont de futurs grands talents (1995, HollySiz, La Femme, Christine and the queens, Woodkid�)��,
pr�cise-t-il � l�AFP.
