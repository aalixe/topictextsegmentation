TITRE: Les Occidentaux exigent la libération des hommes évacués de Homs | Site mobile Le Point
DATE: 2014-02-13
URL: http://www.lepoint.fr/monde/les-occidentaux-exigent-la-liberation-des-hommes-evacues-de-homs-13-02-2014-1791440_24.php
PRINCIPAL: 171779
TEXT:
13/02/14 à 17h27
Les Occidentaux exigent la libération des hommes évacués de Homs
Les Occidentaux ont exigé jeudi la libération des hommes arrêtés par les services de sécurité syriens à leur sortie du réduit rebelle de Homs tandis que l'aide humanitaire et l'évacuation des civils devrait se poursuivre vendredi.
A Genève, le médiateur de l'ONU Lakhdar Brahimi devait se réunir avec des représentants des deux parrains de la conférence de paix, la Russie et les États-Unis, pour tenter de sortir de l'impasse les négociations entre le régime et l'opposition qui doivent s'achever vendredi.
A Homs, ville du centre de la Syrie où se déroule une opération d'évacuation de civils hors de la Vieille ville assiégée par le régime depuis 18 mois, le gouverneur Talal Barazi a annoncé à l'AFP que 70 hommes âgés entre 15 et 54 ans, parmi ceux qui étaient interrogés par les services de renseignements après leur évacuation, avaient été libérés jeudi.
Selon M. Barazi, 390 hommes en âge de porter les armes avaient été arrêtés à leur sortie des quartiers assiégés, dont 181 ont été depuis libérés.
Cependant, selon l'Observatoire syrien des droits de l'homme (OSDH) et le militant Yazan, très peu d'entre eux ont été relâchés. Ceux toujours détenus le sont dans une école où se trouvent des services de sécurité et l'ONU ainsi qu'au siège des renseignements militaires.
Selon Yazan, ce retard dans les libérations vient du désir de ceux qui sont libérables de se rendre, pour leur sécurité, dans les secteurs tenus par les rebelles, comme dans le quartier de Waer (hors de la Vieille ville), ce que refusent les autorités.
Des blessés encore dans la Vieille ville refusent également d'être évacués vers des hôpitaux tenus par les autorités de peur d'être kidnappés, selon Yazan, qui est resté dans l'enclave rebelle.
Le maintien en détention des hommes évacués a outré les pays occidentaux qui réclament leur libération.
"Le régime doit savoir que le monde entier regarde avec une grande préoccupation ce qui se passe à Homs et le statut des hommes évacués. Tout tentative de les maintenir de manière arbitraire en détention ne sera pas acceptée", a affirmé Edgar Vasquez, un porte-parole du département d'Etat américain.
"Le régime a indiqué qu'il libèrera les hommes après examen, et nous espérons qu'il tiendra son engagement", a-t-il ajouté.
Londres est allé dans le même sens en réclamant leur "libération immédiate".
"Il est scandaleux que le régime syrien détienne et interroge des hommes et des garçons qui ont été évacués", a déclaré le ministre britannique des Affaires étrangères William Hague dans un communiqué.
"Nous avons besoin de réponses urgentes sur le sort qui leur est réservé", a-t-il ajouté.
- 1.417 personnes évacuées -
Au total depuis le 7 février, 1.417 personnes ont été évacuées du vieux Homs, en vertu d'un accord entre le régime syrien et les rebelles négocié par l'ONU. Il resterait 3.000 personnes dans ce réduit de 2 km2, en grande partie en ruines.
L'acheminement d'aide et les évacuations de civils de Homs reprendront vendredi après une suspension d'un jour, selon M. Barazi.
Selon lui, la trêve entrée en vigueur le 7 février pour permettre ces opérations sera renouvelée jusqu'à samedi soir.
Depuis une semaine, 6,2 tonnes de farine et 500 rations alimentaires ont été distribués dans ce secteur touché par la famine.
Dans le bras de fer entre les Occidentaux, qui appuient la rébellion, et Moscou, fidèle allié de Bachar al-Assad, la Russie a présenté un projet de résolution au Conseil de sécurité de l'ONU qui ne prévoit pas de menaces de sanctions contre le régime Assad, a indiqué le ministre russe des Affaires étrangères, Sergueï Lavrov.
Pour lui, le projet occidental et arabe, rejeté par Moscou, "est fait sous forme d'un ultimatum. Il y a des menaces de sanctions. Nous ne pouvons pas l'accepter".
Amnesty International a exhorté de son côté le Conseil de sécurité à saisir "l'opportunité de prendre en main la question des droits de l'Homme et de la catastrophe humanitaire" en Syrie.
Depuis le lancement le 22 janvier des pourparlers de paix à Genève, le nombre quotidien de morts n'a jamais été aussi élevé en trois ans de révolte, avec plus de 200 morts par jour, selon l'OSDH.
