TITRE: Le Conseil constitutionnel valide l’interdiction du cumul des mandats, Actualités
DATE: 2014-02-13
URL: http://www.lesechos.fr/economie-politique/politique/actu/0203315479770-le-conseil-constitutionnel-valide-l-interdiction-du-cumul-des-mandats-650294.php
PRINCIPAL: 171275
TEXT:
Le Conseil constitutionnel valide l�interdiction du cumul des mandats
13/02 | 18:40 | mis � jour � 18:50
Les Sages ont valid� la loi interdisant le cumul des mandats. Elle entrera en vigueur en mars 2017, et ne s�applique donc pas aux prochaines �lections municipale et europ�enne.
Actuellement, 60�% des parlementaires cumulent leur mandat avec une fonction ex�cutive locale - AFP
C�est d�sormais officiel. Le Conseil constitutionnel a annonc� jeudi avoir valid� les deux lois qui interdisent de cumuler un mandat parlementaire avec une fonction ex�cutive locale � partir de 2017. A compter du 31 mars 2017, il sera donc interdit d�avoir un mandat de d�put�, s�nateur ou d�put� europ�en et d��tre en m�me temps maire d�une ville ou pr�sident d�une intercommunalit�, d�un conseil g�n�ral ou r�gional, notamment. Dans un communiqu�, le Conseil constitutionnel � a jug� qu�il �tait loisible au l�gislateur de poser de telles incompatibilit�s �.
Actuellement, 60 % des parlementaires cumulent ce mandat avec une fonction ex�cutive locale. La loi prohibant un tel cumul avait �t� vot�e d�finitivement par l�Assembl�e nationale le 22 janvier, par 313 voix (socialistes, �cologistes et communistes), contre 225 et 14 abstentions.
132 s�nateurs UMP et UDI-UC, mais aussi de la majorit� gouvernementale, RDSE (� majorit� PRG) d�non�aient une � atteinte au bicam�risme �. Ils estimaient notamment que la loi aurait d� �tre adopt�e en termes identiques par l�Assembl�e nationale et le S�nat.
Vos derniers commentaires
kidikoa le  14/02/2014 � 11:38
