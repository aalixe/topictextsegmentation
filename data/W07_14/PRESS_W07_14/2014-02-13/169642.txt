TITRE: Les Experts�: toujours pas 6 millions de t�l�spectateurs pour la saison 13 - News TV - Toutelatele.com
DATE: 2014-02-13
URL: http://www.toutelatele.com/les-experts-toujours-pas-6-millions-de-telespectateurs-pour-la-saison-13-56821
PRINCIPAL: 169639
TEXT:
Mots-cl�s : tf1 �|� audiences tv �|� series �|� les experts �|�
En passant de deux � quatre in�dits par semaine, TF1 a consid�rablement acc�l�r� le rythme de diffusion de son stock d��pisodes des Experts. Ce mercredi 12 f�vrier, Ted Danson et Elisabeth Shue ont repris leurs investigations. Devant la t�l�vision, la s�rie am�ricaine a retrouv� la t�te des audiences, sans toutefois atteindre les 6 millions de t�l�spectateurs.
D�s 20h55, les d�tectives ont d�couvert un v�ritable carnage dans le restaurant o� ils avaient leurs habitudes depuis une dizaine d�ann�es. Au total�: huit personnes tu�s par balles et un patron enferm� dans la chambre froide. Au final, ��H�catombe en cuisine�� a convaincu 5.93 millions de t�l�spectateurs, correspondant � 22.9% de part d�audience, avec 27% aupr�s des m�nag�res.
Juste apr�s, un charnier contenant huit squelettes de femmes �taient au centre de ��L�Eclat du pass頻.  Sur l�une des victimes, Finlay reconna�t le pendentif d�une jeune femme disparue deux ans plus t�t � Seattle. Cette enqu�te lui avait co�t� son poste... En moyenne, 5.15 millions de Fran�ais sont rest�s fid�les aux Experts, pour 22.6% du public.
Puis, dans ��Fleur sauvage��, la mort d�un jeune fille en pleine rave a mis l��quipe sur le pied de guerre. L�histoire d�un trafic d��tres humains pour servir d�esclaves sexuels a int�ress� 4.03 millions fid�les. Concernant sa part de march�, Les Experts ont alors affich� 25.8% sur les 4 ans et plus.
Ce soir�e s�est conclue avec ��Nuit de folie�� et 2.72 millions d�amateurs, pour 30% de part d�audience, avec 32% sur les m�nag�res. En une soir�e, une victime et des arrestations en chaine ont sem� le trouble dans la cellule.
Les Experts continuent dans une semaine, � la m�me heure.
NEWS
