TITRE: Comcast et Time Warner Cable veulent fusionner
DATE: 2014-02-13
URL: http://fr.canoe.ca/techno/internet/archives/2014/02/20140213-104557.html
PRINCIPAL: 170900
TEXT:
Comcast et Time Warner Cable veulent fusionner
�Photo Fotolia
Sophie Estienne
13-02-2014 | 10h45
NEW YORK - Les deux premiers c�blo-op�rateurs am�ricains, Comcast et Time Warner Cable (TWC), ont annonc� jeudi leur intention de fusionner pour cr�er un nouveau g�ant du secteur, un projet dont l'envergure risque toutefois de soulever des objections des autorit�s de la concurrence.
La transaction est amicale et prendra la forme d'un rachat de TWC par Comcast pour 45,2 milliards de dollars en actions, soit 23% de son capital. En ajoutant la reprise de dette, TWC est valoris� � 66,9 milliards.
Cela devrait permettre � Comcast de souffler TWC au num�ro 4 du secteur, Charter Communications, qui avait officialis� mi-janvier une offre � environ 60 milliards de dollars, dette incluse. TWC l'avait rejet�e, estimant que le prix �tait trop bas et pr�venant � plusieurs reprises qu'il n'avait pas l'intention de �laisser Charter voler l'entreprise�.
Avantages d'�chelle
Le rapprochement de Comcast et TWC donnera naissance � un groupe de 86,8 milliards de dollars de chiffre d'affaires, aux activit�s couvrant � la fois l'internet � haut d�bit, la t�l�vision et les services de t�l�communication.
Ce sont �des activit�s o� l'�chelle compte, et les consommateurs seront gagnants�, a assur� le PDG de TWC, Robert Marcus, lors d'une t�l�conf�rence avec des analystes.
L'op�ration �va b�n�ficier � des millions de consommateurs, en acc�l�rant le d�ploiement de technologies avanc�es et le d�veloppement de nouveaux services innovants�, a aussi affirm� le PDG de Comcast, Brian Roberts.
Il a notamment fait miroiter �de la vid�o de qualit� techniquement sup�rieure et du plus haut d�bit� internet pour les clients de TWC, et soulign� que le financement en actions ne fragilisait pas les finances du nouvel ensemble et m�me �am�liorait notre capacit� � investir de mani�re importante dans notre activit�.
Comcast voit aussi des opportunit�s d'�conomies substantielles, �valu�es � environ 1,5 milliard de dollars dans les trois ans apr�s la concr�tisation de l'op�ration, dont la moiti� d�s la premi�re ann�e.
Un ca�d trop puissant?
TWC fournit des services de t�l�vision c�bl�e et d'internet � 11 millions de clients aux Etats-Unis, en particulier dans des zones urbaines riches comme New York, le sud de la Californie ou le Texas par exemple.
Comcast pointe � 22 millions d'abonn�s. Il s'est aussi diversifi� ces derni�res ann�es dans les m�dias en prenant le contr�le int�gral de NBCUniversal, ses cha�nes de t�l�vision et ses studios de production.
La perspective d'un nouveau gain d'influence de ce poids lourd fait d�j� grincer des dents: John Bergmayer, avocat de l'association de d�fense des consommateurs Public Knowledge, a appel� les r�gulateurs � interdire le mariage.
�Un Comcast �largi serait le ca�d dans la cour de r�cr�ation, capable de dicter ses conditions aux cr�ateurs de contenus, aux entreprises internet, aux autres r�seaux de communication qui doivent interagir avec lui, et aux distributeurs qui doivent acc�der � ses contenus�, a-t-il d�nonc�, estimant qu'au final cela �augmenterait les co�ts pour les consommateurs�.
Conditions � pr�voir
Bien conscients des obstacles � franchir, Comcast et TWC se donnent un long d�lai, probablement jusqu'� la fin de l'ann�e, pour consommer leur mariage.
Ils n'ont pas non plus perdu de temps pour marteler que leur rapprochement �ne r�duira la concurrence sur aucun march�, M. Roberts soulignant que sur les 50 grandes zones g�ographiques o� les deux groupes sont pr�sents, leurs zones de couverture n'ont �aucun code postal en commun�.
�Comcast n'est pas en concurrence avec TWC, cela joue en leur faveur�, estime l'analyste ind�pendant Jeff Kagan, pour qui l'op�ration devrait �tre �faisable� avec �la cession d'une partie des clients�.
Comcast s'est d'ailleurs d�j� dit pr�t � r�troc�der � un portefeuille de 3 millions d'abonn�s, qui pourraient servir de lot de consolation � Charter.
La banque RBC envisage aussi dans une note que les r�gulateurs posent des conditions � l'op�ration pour rem�dier � �des inqui�tudes potentielles� sur l'influence du nouveau groupe dans les n�gociations sur la diffusion et la retransmission des contenus vid�o.
