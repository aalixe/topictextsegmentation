TITRE: C�sar 2014 : La cons�cration pour Scarlett Johansson avec le C�sar d'honneur !
DATE: 2014-02-13
URL: http://www.staragora.com/news/cesar-2014-la-consecration-pour-scarlett-johansson-avec-le-cesar-d-honneur/479142
PRINCIPAL: 168937
TEXT:
0
Quelques jours apr�s la r�v�lation des nomm�s pour les C�sar 2014, on d�couvre le nom de l'acteur international qui sera � l'honneur, cette ann�e se sera une actrice...
La c�r�monie des C�sar 2014 approche � grands pas , plus pr�cis�ment le vendredi 28 f�vrier 2014. Cette trente-neuvi�me �dition se d�roulera une nouvelle fois au Th��tre du Ch�telet et cette ann�e, la ma�tresse de c�r�monie sera C�cile De France , le pr�sident, Fran�ois Cluzet . Nous connaissons d�j� les noms des nomm�s dans chaque cat�gorie avec notamment la nomination de Julie Gayet dans la cat�gorie Meilleure actrice dans un second r�le .
Il ne manquait plus qu�une information, le nom de celui ou celle qui va obtenir le C�sar d�honneur attribu� chaque ann�e par l�Acad�mie des Arts et techniques du cin�ma, depuis 1976. Parmi les derni�res stars qui ont re�u le C�sar d�honneur, ces derni�res ann�es, Harrison Ford , en 2010, Quentin Tarantino en 2011, Kate Winslet en 2012 et Kevin Costner en 2013. Et cette ann�e, il sera attribu� � la jolie Scarlett Johansson , qui a tout juste 30 ans a d�j� 20 ans de carri�re derri�re elle.
Le triomphe de Scarlett Johansson
Et oui, m�me si certains trouvent que cette r�compense est remise un peu trop t�t � Scarlett Johansson, rappelons qu�elle a jou� dans des films de r�alisateurs prestigieux, Woody Allen , Robert Redford et Sofia Coppola pour ne citer qu�eux.
Recevoir le C�sar d�honneur est une cons�cration pour Scarlett Johansson qui a notamment jou� dans Lost In Translation, en 2003, The Perfect Score en 2004 et Love Song, la m�me ann�e, puis dans Match Point et The Island en 2005. En 2006, l�actrice joue dans Le Dahlia Noir, avant de jouer l�ann�e suivante dans Le Journal d�une baby-sitter, dans Deux s�urs pour un roi, en 2008. Cette m�me ann�e, la belle Scarlett joue dans Vicky Cristina Barcelona et The Spirit. Apr�s quelques mois de pause, l��g�rie Dolce & Gabbana joue dans Ce que pensent les hommes. En 2010, la belle blonde triomphe dans Iron Man 2. Dans un tout autre registre, Scarlett Johansson joue dans Nouveau D�part en 2011 et Hitchcock en 2012. Mais cette ann�e-l�, son gros succ�s au box office est sans conteste, Avengers dont la sortie du deuxi�me opus est pr�vue pour 2015. Plus r�cemment, Scarlett Johansson a jou� dans Don Jon en 2013 et sera tr�s prochainement � l�affiche de Captain America�: Le soldat de l�hiver.
Les nomm�s aux C�sar 2014
Pour rappel, voici la l iste des nomm�s pour les C�sar 2014 �:
Meilleur film�:
Les gar�ons et Guillaume, � table, L�inconnu du lac, La vie d� Ad�le , Neuf mois ferme, Jimmy P., Le pass� et La v�nus � la fourrure.
Meilleure actrice�:
Catherine Deneuve (Elle s'en va),� Fanny Ardant (Les beaux jours), B�r�nice B�jo (Le pass�),� L�a Seydoux (La vie d' Ad�le ),� Sara Forestier (Suzanne),� Sandrine Kiberlain (Neuf mois ferme) et Emmanuelle Seigner (La v�nus � la fourrure)
Meilleur acteur�:
Guillaume Gallienne (Les gar�ons et Guillaume, � table) Michel Bouquet (Renoir),� Albert Dupontel (Neuf mois ferme),� Mathieu Amalric (Jimmy P.),� Fabrice Luchini (Alceste � bicyclette),� Mads Mikkelsen (Michael Kohlhaas) et Gr�gory Gadebois (Mon �me par toi gu�rie)
Meilleur r�alisateur�:
Kechiche pour La vie d'Ad�le, Asghar Farhadi pour Le Pass�, Albert Dupontel pour 9 mois ferme, Alain Guiraudie pour L'Inconnu du lac, Arnaud Desplechin pour Jimmy P. (Psychoth�rapie d'un Indien des plaines), Guillaume Gallienne pour Les gar�ons et Guillaume, � table et Roman Polanski pour La V�nus � la fourrure.
Meilleur sc�nario original
