TITRE: Au revoir la gastro, bonjour la grippe ! - Afrik.com : l'actualit� de l'Afrique noire et du Maghreb - Le quotidien panafricain
DATE: 2014-02-13
URL: http://www.afrik.com/au-revoir-la-gastro-bonjour-la-grippe
PRINCIPAL: 0
TEXT:
jeudi 13 f�vrier 2014 / par Moussa Kane
La grippe a franchi le seuil �pid�mique pour la deuxi�me semaine cons�cutive, l�information vient de l�Institut de veille sanitaire.
Ar�s l��pid�mie de gastro-ent�rite qui a fait des ravages, avec notamment des morts, la grippe a franchi le seuil �pid�mique pour la deuxi�me semaine cons�cutive, l�information vient de l�Institut de veille sanitaire.
Le r�seau Sentinelles note une baisse des syndromes grippaux les plus forts en revanche le r�seau Grog indique une situation qualifi�e d��pid�mique en Lorraine o� la situation est ainsi contrast�e.
Le r�seau Sentinelles de l�Institut national de la sant� et de la recherche m�dicale (INSERM), charg� de collecter des donn�es �pid�miologiques d�un r�seau de m�decins g�n�ralistes, observe dans son dernier bulletin diffus� mercredi une activit� qualifi�e de ��d��pid�mique�� des ��syndromes grippaux�� de?finis par une fie?vre supe?rieure a? 39�C, d�apparition brutale, accompagne?e de myalgies et de signes respiratoires.
Avec un taux d�incidence des cas de syndromes grippaux vus en consultation de m�decine g�n�rale estim� � 243 cas pour 100 000 habitants, le seuil �pid�mique de 167 cas pour 100 000 habitants a �t� d�pass�. ��Cette deuxie?me semaine conse?cutive de de?passement du seuil vient confirmer l�arrive?e de l�e?pide?mie de grippe en France me?tropolitaine��, indique l�INSERM.�En deux semaines 274 000 malades auraient ainsi consult� un m�decin.
Le r�seau GROG (Groupes R�gionaux d�Observation de la Grippe) poss�de une d�finition clinique de la grippe diff�rente du r�seau Sentinelles prenant en compte les grippes plus l�g�res. Les ratios sont r�alis�s par des pr�l�vements rhinopharyng�s envoy�s � des laboratoires pour �tre analys�s et ce sont ces pr�l�vements qui donnent une confirmation virologique de la grippe.
Selon les donn�es communiqu�es mercredi par le r�seau GROG pour la semaine 6 (du lundi 3 au dimanche 9 f�vrier 2014), la grippe est �pid�mique depuis trois semaines au niveau national, apr�s les inqui�tudes caus�es par la gastro-ent�rite qui s�vit toujours tout de m�me.
� la une
