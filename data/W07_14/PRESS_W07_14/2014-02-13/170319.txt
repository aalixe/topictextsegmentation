TITRE: L'est américain paralysé sous la neige, une quinzaine de victimes - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-13
URL: http://tempsreel.nouvelobs.com/topnews/20140213.AFP9986/etats-unis-une-nouvelle-tempete-de-neige-arrive-sur-washington.html
PRINCIPAL: 0
TEXT:
Actualité > Monde > L'est américain paralysé sous la neige, une quinzaine de victimes
L'est américain paralysé sous la neige, une quinzaine de victimes
Publié le 13-02-2014 à 08h05
Mis à jour le 14-02-2014 à 22h01
A+ A-
Une nouvelle tempête de neige, annoncée comme une des plus importantes de l'hiver, a commencé à blanchir les rues de Washington mercredi soir, après avoir traversé le sud-est du pays, privant d'électricité des centaines de milliers de foyers américains. (c) Afp
Washington (AFP) - La tempête hivernale qui a de nouveau paralysé jeudi l'est des Etats-Unis, a été fatale pour au moins une quinzaine de personnes, et a condamné des millions d'Américains à s'armer de pelles et de patience.
Les médias américains décomptaient jeudi soir entre 16 et 18 personnes mortes pour la plupart dans des accidents de la route. Une femme enceinte a été tuée par un chasse-neige et son bébé sauvé par césarienne était jeudi soir dans un état grave. Un homme, interné dans un hôpital psychiatrique à Washington a également été retrouvé mort dans la neige, selon le maire de la capitale américaine.
"De la neige abondante continuera (à tomber) dans la nuit" de jeudi à vendredi dans le nord-est du pays "mais commencera à diminuer du Sud vers le Nord au cours de la matinée de vendredi", ont indiqué les services de météorologie dans leur dernier bulletin jeudi après-midi.
Ce nouvel épisode d'un hiver particulièrement rude pour l'est américain avait dans un premier temps touché, dès mercredi, la Géorgie, la Caroline du Sud et la Caroline du Nord, avant de remonter vers le Nord.
Plus de 2.300 militaires de la Garde nationale ont été mobilisés jeudi dans sept Etats --de la Géorgie au Delaware-- pour aider les services de secours, a indiqué le Pentagone.
Le gouverneur de l'Etat du Connecticut, Dannel P. Malloy, a déclaré jeudi soir l'état d'urgence afin d'obtenir l'aide des services fédéraux car ses services de voirie manquent de sel.
Selon le département de l'Energie, près de 800.000 foyers ou entreprises étaient privés d'électricité jeudi après-midi dans 11 Etats du Sud et de l'Est, dont 340.000 en Caroline du Nord et du Sud.
6.850 vols avaient été annulés jeudi vers 21H00 (02H00 GMT) en direction ou au départ des Etats-Unis, selon le site spécialisé FlightAware.com. Au moins 3.700 l'avaient été mercredi.
Dans la capitale Washington, la neige est revenue jeudi soir, accompagnée par endroits d'orages, après avoir laissé place dans la journée à une pluie verglaçante.
La circulation était rendue extrêmement difficile par la trentaine de centimètres de neige tombée au cours de la nuit. Des habitants s'étaient armés de pelles pour déblayer leur voiture, quand d'autres avaient ressorti leurs skis sur le Mall, la grande esplanade au centre de la capitale.
Les écoles seront à nouveau fermées vendredi. Les portes des administrations sont restées closes jeudi et peu de bus ont circulé. Le vice-président Joe Biden a renoncé à un déplacement et le briefing quotidien de la Maison Blanche a été annulé.
- Neige absente seulement en Floride -
A New York, où s'achevait jeudi la Semaine de la mode, il a également neigé en abondance jeudi matin. Un avis de tempête de neige y a été décrété jusqu'à vendredi 06H00 (11H00 GMT): quelque 30 cm de neige sont attendus d'ici là.
Les chasse-neige ont tourné toute la nuit à Manhattan, mais la neige restée sur les avenues a compliqué les derniers défilés de la Semaine de la mode new-yorkaise.
"Prendre les transports en commun reste la meilleure option", a conseillé le maire de la ville Bill de Blasio lors d'un point de presse. La collecte des ordures a été suspendue "pour que les employés de l'assainissement se consacrent à la neige".
Si les écoles catholiques de New York étaient fermées, les établissements publics étaient en revanche ouverts.
La météo nationale avait mis en garde depuis plusieurs jours contre un "dôme gigantesque" de courants d'air froid venus de l'Arctique et devant s'installer sur l'est des Etats-Unis , provoquant des accumulations de glace "incroyables, si ce n'est historiques".
Le président Barack Obama avait déclaré mercredi l'état d'urgence dans 45 comtés de Géorgie et en Caroline du Sud, ce qui permet aux services fédéraux chargés des urgences d'y opérer.
En Caroline du Nord, où la météo est habituellement plus clémente, les chutes de neige ont entraîné des difficultés importantes sur les routes.
Il y a deux semaines, la Géorgie avait déjà subi une première tempête de neige, une rareté dans cet Etat du Sud, mais la gestion par les autorités de celle-ci avait été fortement critiquée.
La neige est présente dans 49 Etats américains sur 50, sauf en Floride, selon une carte des services américains de météorologie publiée jeudi.
Partager
