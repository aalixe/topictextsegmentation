TITRE: Feu vert constitutionnel au non-cumul des mandats à partir de 2017 - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-13
URL: http://tempsreel.nouvelobs.com/politique/20140213.AFP0047/le-conseil-constitutionnel-valide-la-loi-interdisant-le-cumul-des-mandats.html
PRINCIPAL: 171614
TEXT:
Actualité > Politique > Feu vert constitutionnel au non-cumul des mandats à partir de 2017
Feu vert constitutionnel au non-cumul des mandats à partir de 2017
Publié le 13-02-2014 à 18h50
Mis à jour le 14-02-2014 à 07h40
A+ A-
Le Conseil constitutionnel a annoncé jeudi avoir validé la loi interdisant de cumuler un mandat parlementaire avec une fonction exécutive locale à partir de 2017. (c) Afp
Paris (AFP) - Sujet de controverse depuis des années et symbole de modernisation de la vie politique, le non-cumul d'un mandat parlementaire avec une fonction exécutive locale entrera bel et bien en vigueur, mais seulement en 2017.
Le Conseil constitutionnel a mis fin jeudi aux derniers espoirs des partisans du cumul - surnommés péjorativement les "cumulards" - en validant la loi interdisant ce cumul, définitivement votée le 22 janvier au parlement malgré l'hostilité de la droite et d'une petite partie de la majorité de gauche.
Dans ses vœux aux assemblées le 21 janvier, le président François Hollande a prédit que cette réforme, qui aligne la France sur toutes les grandes démocraties, sera "irréversible", en dépit de déclarations de députés UMP, tel Daniel Fasquelle, promettant de "revenir dessus" en cas de retour au pouvoir.
L'interdiction s'appliquera selon la loi pour toutes les élections prévues à partir du 31 mars 2017, et de la même façon pour tous les parlementaires.
La loi s'appliquant "à compter du renouvellement de l'assemblée suivant le 31 mars 2017", la prohibition sera effective pour les députés et les sénateurs en 2017 et pour les députés européens en 2019. Au grand dam du président du groupe PS au Sénat, François Rebsamen. Si je suis réélu maire de Dijon en mars et sénateur en septembre, "jusqu'en 2020, je peux cumuler", avançait-il en janvier, au nom d'une interprétation écartée par le Conseil constitutionnel.
Conséquences concrètes de la loi, il sera interdit d'être à la fois député, ou sénateur, ou député européen et en même temps maire d'une ville ou président ou vice-président d'une intercommunalité, d'un Conseil général ou régional, ou même membre du conseil d'administration d'une société d'économie mixte.
Le Conseil constitutionnel "a jugé qu'il était loisible au législateur de poser de telles incompatibilités", selon un communiqué des neuf juges. Il a étendu l'application de la loi aux fonctions de vice-président de l'Assemblée de Corse.
Néanmoins, tout parlementaire pourra continuer à être conseiller municipal, départemental ou régional.
Actuellement, 60% des parlementaires cumulent ce mandat avec une fonction exécutive locale, et rares sont ceux qui anticipent le nouveau régime. Parmi les 577 députés, 244 au moins veulent rester ou devenir maires et se présentent ou se représentent aux élections municipales du mois de mars.
Ainsi, à Marseille, le sénateur UMP Jean-Claude Gaudin brigue un quatrième mandat de maire et son challenger socialiste Patrick Mennucci entend conserver son mandat de député s'il est élu à la mairie le mois prochain.
Le ministre de l'Intérieur Manuel Valls , qui avait défendu la réforme au parlement et "se félicite" de sa validation constitutionnelle, estime pourtant qu'elle "commence d’ores et déjà à produire ses effets" pour les municipales.
- 'Renouvellement des générations' -
La loi avait été votée définitivement par l'Assemblée nationale par 313 voix (socialistes, écologistes et communistes), contre 225 (UMP, centristes, radicaux de gauche) et 14 abstentions.
132 sénateurs UMP et UDI-UC, mais aussi de la majorité gouvernementale, RDSE (à majorité PRG) avaient saisi le Conseil constitutionnel, dénonçant une "atteinte au bicamérisme". Ils estimaient notamment que la loi aurait dû être adoptée en termes identiques par l'Assemblée nationale et le Sénat.
En dépit de pressions au sein du PS, le gouvernement avait décidé de ne faire entrer en vigueur le cumul qu'à partir de 2017 en raison, selon lui, d'un risque d'inconstitutionnalité si la loi devait s'appliquer immédiatement. Les partisans du non-cumul faisaient valoir, tels le Premier ministre Jean-Marc Ayrault, qu'il permettrait "la diversité des candidatures, un renouvellement de générations, davantage de parité".
Ses adversaires avançaient que renoncer à une mairie, par exemple, "coupera de la réalité du terrain" les élus. Le non-cumul, ont-ils aussi plaidé, "mettra les élus à la merci des appareils politiques", car ils ne pourront plus s'appuyer sur leur implantation locale pour garder une certaine indépendance à l'égard de l'exécutif.
Certains spécialistes de droit constitutionnel soutenaient aussi que, paradoxalement, cette réforme renforcerait le président de la République, dans la mesure où les députés sont élus dans la foulée de l'élection présidentielle, et presque toujours majoritairement choisis parmi ceux qui ont fait campagne pour le président élu.
Partager
