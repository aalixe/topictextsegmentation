TITRE: VIDEO. Italie: le premier ministre Enrico Letta d�missionne - L'Express
DATE: 2014-02-13
URL: http://www.lexpress.fr/actualite/monde/europe/italie-le-premier-ministre-enrico-letta-demissionne_1323853.html
PRINCIPAL: 0
TEXT:
Enrico Letta et son engagement pour l'Italie, le 12 f�vrier.
REUTERS/Remo Casilli
Apr�s l'annonce officielle de la d�mission du premier ministre italien Enrico Letta , la direction du Parti d�mocrate (PD), premi�re formation au sein de la majorit� au pouvoir, a approuv� � une large majorit� jeudi une proposition de son chef Matteo Renzi pour un "changement de gouvernement afin d'ouvrir une nouvelle phase".�
Apr�s un d�bat de plusieurs heures, les membres de la direction ont vot� � main lev�e, adoptant � une large majorit� de 136 voix pour, 16 contre, la motion du maire de Florence, pressenti pour prendre la succession de l'actuel Premier ministre Enrico Letta , ex-num�ro deux du parti.�
Matteo Renzi piaffait d'impatience
"C'est la crise de gouvernement. Renzi vers le Palais Chigi (si�ge du gouvernement) avec le feu vert du PD", titrait le journal La Stampa sur son site internet peu apr�s ces annonces.�
Depuis son arriv�e � la t�te du PD en d�cembre dernier et surtout depuis qu'il avait conclu un accord � la mi-janvier pour une nouvelle loi �lectorale avec Silvio Berlusconi , Matteo Renzi piaffait d'impatience. Il multipliait les attaques contre l'ex�cutif Letta, lui reprochant lenteur et manque de d�termination, malgr� le pacte nou� entre les deux hommes pour une poursuite de l'action du gouvernement Letta jusqu'� au moins la fin 2014.�
Selon Giovanni Orsina , professeur de Sciences politiques � l'Universit� Luiss de Rome, le dualisme Renzi-Letta �tait devenu ces derni�res semaines "un �ni�me facteur de paralysie politique".�
Le gouvernement d'Enrico Letta avait pourtant surv�cu en octobre dernier � la fronde de Silvio Berlusconi , qui demandait au Parti de la libert� de se retirer du gouvernement. La coalition avait choisi de perdurer, sacrifiant le Cavaliere � son sort judiciaire . Pour remporter la confiance, Enrico Letta avait mis en garde contre l'instabilit�, "un risque fatal" pour l'Italie.�
Avec
