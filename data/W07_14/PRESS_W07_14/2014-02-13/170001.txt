TITRE: Bient�t un mouchard dans les smartphones Samsung ?
DATE: 2014-02-13
URL: http://www.01net.com/editorial/614068/bientot-un-mouchard-dans-les-smartphones-samsung/
PRINCIPAL: 170000
TEXT:
Actualit�s �>� Techno
Bient�t un mouchard dans les smartphones Samsung ?
Le service Context serait charg� de r�colter des donn�es sur l'utilisation des smartphones. Elles seraient ensuite transmises aux d�veloppeurs pour qu'ils am�liorent leurs applis.
agrandir la photo
Conna�tre ses clients et comprendre leurs besoins est au centre du travail de toute entreprise qui veut fructifier. Selon le site The Information, Samsung travaillerait sur Context, un service destin� � r�colter des informations qui permettraient aux d�veloppeurs d�optimiser leurs applis.
Ce service, destin� � �tre int�gr� � tous les smartphones du fabricant cor�en, serait en mesure de r�cup�rer nombreuses informations : ce que tape l�utilisateur au clavier, les applications dont il se sert, les donn�es r�cup�r�es par les capteurs (GPS, acc�l�rom�tre�). The Information prend comme exemple une application vid�o qui pourrait se servir de Context pour ��pousser�� des vid�os de sport vers un individu qui effectue souvent des recherches sur une activit� phystique particuli�re.�
Cette technique permettrait donc aux d�veloppeurs d�am�liorer leurs produits... mais aussi de renforcer la publicit� cibl�e. Reste que, d�apr�s le site am�ricain, le lancement de Context aurait �t� diff�r� et pourrait m�me �tre abandonn�, Samsung n��tant pas tout � fait convaincu de l�impact positif de ce service sur ses ventes de smartphones. Il faut dire que la collecte de donn�es priv�es n�a pas bonne presse !
Source : The Information (article payant)
envoyer
