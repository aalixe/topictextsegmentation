TITRE: L'achat de Time Warner par Comcast donnera naissance � un g�ant de la t�l� c�bl�e aux Etats-Unis
DATE: 2014-02-13
URL: http://www.latribune.fr/technos-medias/20140213trib000815169/l-achat-de-time-warner-par-comcast-donnera-naissance-a-un-geant-de-la-tele-cablee-aux-etats-unis.html
PRINCIPAL: 169332
TEXT:
L'achat de Time Warner par Comcast donnera naissance � un g�ant de la t�l� c�bl�e aux Etats-Unis
A elles deux, Comcast et Time Warner comptent 33 millions d'abonn�s. (Photos Reuters)
latribune.fr �|�
Pour 45 milliards de dollars, le premier c�blo-op�rateur am�ricain s'offre son concurrent Time Carner Cable.
sur le m�me sujet
Technos & M�dias
OK
Un nouvel empire des m�dias �merge outre-Atlantique. Le groupe Comcast devrait annoncer ce jeudi la signature d'un accord en vue de l'acquisition de Time Warner Cable (TWC) pour un peu plus de 45 milliards de dollars, selon le Wall Street Journal �et le New York Times .�L'op�ration serait r�alis�e par un rachat de titre � 158.82 dollars par action, soit environ 23 dollars de plus que le cours moyen de TWC ces derniers jours.
Bataille gagn�e contre Charter
Ce compromis mettrait fin � plusieurs mois de bataille entre Comcast et Charter Communications, quatri�me sur le march�, qui convoitait �galement Time Warner. Une offre de Charter a 132,5 dollars par action a ainsi r�cemment �t� rejet�e.�
33 millions d'abonn�s � eux deux
Le vainqueur, Comcast, est notamment propri�taire du groupe de divertissement NBC Universal, achet� moins d'un an plus t�t � General Electric. Il compte �galement Universal film studio parmi ses filiales et compte 22 millions d'abonn�s selon le quotidien financier new-yorkais. 11 millions de personnes sont abonn�es � Time Warner.
La fusion entre les deux premiers c�blo-op�rateurs am�ricains donnera naissance � un g�ant. Avant qu'elle soit effective, il faudra qu'elle soit valid�e par le r�gulateur am�ricain, ce qui devrait prendre de longs mois.�
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Commentaires
CRC32 �a �crit le 15/02/2014                                     � 17:08 :
A priori cette "acquisition" est bien pire que la "fusion" de Numericable durement concurrenc� par les op�rateurs ADSL. Si le r�gulateur am�ricain accepte une telle acquisition, ce sera la mort de la concurrence des t�l�coms au pays de l'oncle Sam car il n'est pas seulement question de t�l�vision c�bl�e mais surtout d'acc�s internet.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Maxime Piolet �a �crit le 13/02/2014                                     � 8:37 :
La course au pouvoir continue, la qualit� du divertissement est hors-sujet !
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
