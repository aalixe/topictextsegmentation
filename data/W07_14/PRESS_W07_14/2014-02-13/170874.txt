TITRE: Les taxis obtiennent le gel provisoire des immatriculations de VTC - Flash actualit� - Politique - 13/02/2014 - leParisien.fr
DATE: 2014-02-13
URL: http://www.leparisien.fr/flash-actualite-politique/taxis-le-gouvernement-reporte-les-immatriculations-de-vtc-13-02-2014-3587695.php
PRINCIPAL: 170873
TEXT:
Les taxis obtiennent le gel provisoire des immatriculations de VTC
Publi� le 13.02.2014, 15h11
Le gouvernement a d�cid� de "reporter" les r�unions de la Commission d'immatriculation des v�hicules de tourisme avec chauffeur (VTC) jusqu'� la fin de la m�diation men�e avec les syndicats de taxis, a annonc� jeudi Matignon dans un communiqu�. | Jean-Philippe Ksiazek
1/3
R�agir
Les taxis ont gagn� une manche jeudi en obtenant le gel des immatriculations de voitures de tourisme avec chauffeurs, le temps de la m�diation lanc�e pour arbitrer le conflit qui les oppose aux VTC, et ils ont appel� imm�diatement � cesser leur gr�ve.
A deux jours d'une vague de d�parts en vacances, le gouvernement a d�cid� de "reporter" les r�unions de la Commission d'immatriculation des VTC jusqu'� la fin de la concertation men�e avec les syndicats de taxis, a annonc� Matignon dans un communiqu�.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Cette concertation a �t� confi�e mardi au d�put� PS Thomas Th�venoud, qui a deux mois pour proposer un syst�me de "concurrence �quilibr�e" entre taxis et VTC, "au b�n�fice des professionnels, des usagers et de l?emploi".
Dans la foul�e, l'intersyndicale des taxis (CFDT, CGT, FO, CST, FTI et SDCTP) a aussit�t "mis fin au mouvement" de gr�ve lanc� lundi, a annonc� � l'AFP l'un de ses repr�sentants.
Nordine Dahmane (FO-taxis) a salu� une "d�cision courageuse et sage" qui va permettre de "travailler dans un esprit serein" pour que taxis et VTC puissent "se compl�ter".
"Ce gouvernement a h�rit� d'une situation compliqu�e, la d�r�glementation sauvage n'est pas de son fait", sa d�cision "est une avanc�e importante pour les chauffeurs. On va pouvoir maintenant remettre � plat beaucoup de choses", a comment� Karim Asnoun (CGT).
L'intersyndicale avait �t� re�ue mercredi soir et jeudi matin par le m�diateur, qui a �galement rencontr� les repr�sentants des VTC, accus�s par les premiers de concurrence d�loyale.
Benjamin Cardoso, pr�sident de LeCab, l'une de ces soci�t�s de VTC a estim� qu'il fallait "trouver une solution � cette crise pour permettre aux taxis et aux VTC de pouvoir cohabiter pacifiquement".
"Le gouvernement a d�clar� qu'il voulait apaiser les tensions et s'est donn� deux mois pour le faire. Il faut lui donner sa chance", a-t-il d�clar� � l'AFP en saluant le "discours constructif" du d�put� m�diateur.
- "Distortion de concurrence" -
M. Th�venoud a indiqu� dans un communiqu� qu'il poursuivait "d�s cet apr�s-midi" ses rencontres pour travailler � "cr�er un nouveau syst�me avec des r�gles du jeu claires et simples".
Apr�s avoir mobilis� lundi plus d'un millier de taxis en r�gion parisienne, l'intersyndicale avait appel� mardi soir les taxis � une gr�ve illimit�e, encourageant des actions "en tous lieux et � tous moments".
La journ�e de lundi s'�tait sold�e par 64 gardes � vue et des actions sporadiques avaient perturb� la circulation mardi et mercredi. A la mi-journ�e jeudi, 300 taxis bloquaient la prise en charge des passagers � l'a�roport d'Orly.
Interrog� sur Europe 1 jeudi matin, M. Ayrault a admis "la distorsion de concurrence" entre taxis et VTC. Le co�t d'enregistrement d'une VTC est d'environ 100 euros, alors que les licences de taxis, d�livr�es gratuitement par les autorit�s mais au compte-gouttes, se n�gocient entre taxis autour de 230.000 euros � Paris, a-t-il relev�. En m�me temps, M. Ayrault a aussi plaid� pour une "am�lioration du service aux usagers".
Longtemps prot�g�s, les taxis font face � la concurrence des VTC, qui se multiplient depuis un assouplissement de la l�gislation en 2009.
Sans signal�tique lumineuse, les VTC ne peuvent travailler que sur r�servation, mais les taxis les accusent de prendre des clients � la vol�e.
Les VTC avaient eux-m�mes marqu� un premier point le 5 f�vrier en obtenant la suspension du d�cret leur imposant depuis janvier un d�lai de 15 minutes entre la r�servation et la prise en charge du client.
Les syndicats de taxis r�clament un d�lai de r�servation de 30 minutes au moins, un montant minimum de courses de 60 euros et l'interdiction des applications de r�servation de VTC sur smartphone.
Pour peser sur le gouvernement, plusieurs centaines de taxis s'�taient encore mobilis�s une partie de la nuit de mercredi � jeudi en se regroupant en deux points de Paris, pr�s du Champ-de-Mars et Porte Maillot.
A Roissy, o� M. Ayrault s'est rendu dans l'apr�s-midi dans le cadre d'un d�placement pr�vu de longue date, quelques gr�vistes �taient rassembl�s sur la base arri�re des taxis. A Orly, o� l'avion de Fran�ois Hollande devait se poser de retour des Etats-Unis, environ 300 taxis gr�vistes �taient rassembl�s en d�but d'apr�s-midi.
La capitale compte environ 20.000 taxis.
