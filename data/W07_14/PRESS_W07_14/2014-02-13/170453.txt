TITRE: Une loi sur la fin de vie devra faire consensus, estime Ayrault - Capital.fr
DATE: 2014-02-13
URL: http://www.capital.fr/a-la-une/actualites/une-loi-sur-la-fin-de-vie-devra-faire-consensus-estime-ayrault-910485
PRINCIPAL: 0
TEXT:
Une loi sur la fin de vie devra faire consensus, estime...
Une loi sur la fin de vie devra faire consensus, estime Ayrault
Source : Reuters
Tweet
Une nouvelle l�gislation sur la fin de vie en France devra faire l'objet du "consensus le plus pouss�", a estim� jeudi Jean-Marc Ayrault sur Europe 1, alors que le Conseil d'Etat doit se pencher sur le cas d'un homme en �tat de conscience minimale. /Photo d'archives/REUTERS
Une nouvelle l�gislation sur la fin de vie en France devra faire l'objet du "consensus le plus pouss�", a estim� jeudi Jean-Marc Ayrault alors que le Conseil d'Etat doit se pencher sur le cas d'un homme en �tat de conscience minimale.
Le Premier ministre a refus� de se prononcer sur le cas particulier de Vincent Lambert, patient t�trapl�gique dont le maintien en vie est au coeur d'un conflit familial.
"Je ne veux pas faire la le�on, ni dire ce qu'il faut faire. On est au coeur m�me de la question humaine", a-t-il d�clar� sur Europe 1.
"C'est pour le Conseil d'Etat une lourde responsabilit� et pour cette personne et sa famille, c'est un drame. D'autres personnes en France vivent un drame de cette nature", a-t-il ajout�.
Le Premier ministre a rappel� la volont� du pr�sident Fran�ois Hollande de faire �voluer la loi sur la fin de vie. Le Comit� consultatif national d'�thique, qui a �t� saisi, doit rendre un avis � la fin du mois.
"Si on doit l�gif�rer, il faudra le faire avec la recherche du consensus le plus pouss�", a dit Jean-Marc Ayrault � l'adresse de ceux qui pr�disent une lev�e de boucliers en France, notamment de la part de l'Eglise catholique oppos�e � l'euthanasie.
"Ce sont des questions sensibles qui touchent � l'�me humaine et � la responsabilit� la plus profonde. Sur des questions de cette nature, il faut plut�t rechercher le rassemblement que la division", a-t-il soulign�.
Sur France Inter, Marisol Touraine a estim� pour sa part qu'il ne lui appartenait pas en temps que ministre de la Sant� de porter un jugement sur la situation personnelle de Vincent Lambert mais que "la clarification que va faire le Conseil d'Etat" serait "tr�s utile" � la pr�paration d'une loi sur la fin de vie.
Selon elle, une �volution l�gislative r�pondrait � une demande de la soci�t� fran�aise.
Elizabeth Pineau et Marion Douet
� 2014 Reuters - Tous droits de reproduction r�serv�s par Reuters.
