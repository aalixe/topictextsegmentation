TITRE: Ferrari California T : avec un T comme Turbo � SPEEDFANS
DATE: 2014-02-13
URL: http://www.speedfans.fr/2014/02/12/ferrari-california-t-avec-un-t-comme-turbo/
PRINCIPAL: 168680
TEXT:
4 Commentaires
Notre p�riple � Balocco, la piste d�essais du Groupe Fiat, nous avait donc permis de voir juste ! Ferrari annonce la nouvelle California T� Avec un T comme Turbo. Changement d��re et retour dans le pass� et la suralimentation si on se rappelle feue la F40, mais aussi la 288 GTO et la 208 Turbo.
Vraie nouveaut� ? Fausse nouveaut� ? Un peu des deux. Le coup� cabriolet, jusqu�alors mod�le le plus ancien au catalogue Ferrari fait sa mue. Cette California T, au toit toujours r�tractable en 14 sec, change d�horizon sur le plan m�canique.
Elle adopte un V8 3.8 turbo de 560 ch � 7 500 tr/min et 755 Nm (!) � 4 750 tr/min pour un 0 � 100 km/h pli� en 3,6 sec et une v-max chiffr�e � 316 km/h. Les 490 ch de l�actuelle California (V8 4,3 l � injection directe) sont loin. Ah, pr�cision non n�gligeable : le poids � sec de cette California T est annonc� � 1 625 kg contre 1 660 kg � la California atmo. Ce moteur est-il in�dit ? Partiellement, car selon toute vraisemblance, c�est d�j� une version de ce bloc, made in Maranello, qui anime la Maserati Quattroporte .
En passant par la case suralimentation, moins noble peut-�tre, cette California T revendique de sacr�s gains de conso et d��missions de CO2. Et oui, d�sormais, qu�on le veuille ou non, ces sacro-saintes donn�es sont incontournables. La California T r�clame en th�orie 15 % d�essence en moins que l�actuelle � 10,5 l/100 km pour des �missions de CO2 contenues � 250 g/km (recul de 20 %).
Ferrari d�taille aussi �une courbe de couple qui augmente constamment sur toute la plage de r�gimes gr�ce � la Gestion Variable de Suralimentation (ou Variable Boost Management)�. Et si on doutait encore de la pr�sence d�un turbo, l�indicateur de pression le rappelle greff� entre les deux a�rations au centre du tableau de bord (et puis le capot doublement a�r� aussi � l�ext�rieur).
Et la sonorit� dans tout �a ? Chantera ou ne chantera pas ce V8 3,8 l turbo injection directe ? R�ponse de Ferrari qui l�che avec assurance la �bande-son la plus excitante qu�aucun turbo n�ait jamais produite.�
Tout ceci pos�, le restylage serait presque secondaire avec un look inspir� des derni�res FF et LaFerrari notamment, un habitacle bichonn�, des livr�es de carrosserie in�dites, etc. Tout ceci on en reparlera lors de la pr�sentation cette California T � Gen�ve. Plus de vingt ans apr�s la F40, retour au turbo soit clairement la grosse info pour le coup� cabriolet de Maranello.
Source et images : Ferrari.
