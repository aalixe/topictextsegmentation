TITRE: En repli de 26,4%, le b�n�fice net de BNP Paribas en 2013 d��oit
DATE: 2014-02-13
URL: http://www.latribune.fr/entreprises-finance/banques-finance/banque/20140213trib000815178/en-repli-de-264-le-benefice-net-de-bnp-paribas-en-2013-decoit.html
PRINCIPAL: 0
TEXT:
�>� entreprises & finance �>� banques / finance �>� banque
En repli de 26,4%, le b�n�fice net de BNP Paribas en 2013 d��oit
En 2013, BNP Paribas a vu les �l�ments non r�currents peser n�gativement sur son b�n�fice net � hauteur de 1,21 milliard d'euros, contre une contribution positive de 184 millions en 2012. (Reuters/Benoit Tessier)
latribune.fr �|�
13/02/2014, 8:44
�-� 646 �mots
Apr�s une ann�e 2013 marqu�e par des �l�ments exceptionnels qui ont rogn� son b�n�fice net, BNP Paribas veut mettre l'accent sur une am�lioration de sa rentabilit� pour 2016.
sur le m�me sujet
BNP Paribas Fortis acc�l�re les recrutements
Ann�e 2013 difficile pour BNP Paribas : l'an pass�, le r�sultat net de la banque fran�aise a recul� de 26,4%, � 4,83 milliards d'euros, du fait d'importants �l�ments non-r�currents, selon un communiqu�. Il est inf�rieur aux attentes des analystes, qui tablaient en moyenne sur 5,72 milliards d'euros, selon le consensus �tabli par FactSet.
La banque a d�voil� de nouveaux �l�ments de son plan strat�gique 2014/2016, notamment une rentabilit� des capitaux propres (ROE) d'au moins 10% en 2016 et une croissance annuelle d'au moins 10% du b�n�fice net par action en moyenne sur la p�riode 2013/2016, hors exceptionnels.
En 2013, la�rentabilit� des capitaux propres�de BNP Paribas s'est �tabli � 6,1% (7,7% hors exceptionnels) et le b�n�fice net par action a atteint 3,69 euros (4,67 euros hors exceptionnels).�Sa rivale Soci�t� G�n�rale a �comme BNP, fait de cette rentabilit� des capitaux propres un des �l�ments essentiels de son futur plan strat�gique, qui sera d�taill�e en mai, et vise une rentabilit� de 10% en 2015.
Bilan "tr�s solide"
BNP Paribas se targue par ailleurs d'afficher un bilan "tr�s solide", avec un ratio de fonds propres "durs" (apports des actionnaires et b�n�fices mis en r�serve rapport�s aux cr�dits consentis) de 10,3% fin 2013, nettement au-dessus du seuil de 9% exig� par les nouvelles r�gles dites de B�le III.
Ce ratio s'affiche en baisse de 50 points de base (0,50 point de pourcentage) par rapport � la fin du troisi�me trimestre, un repli d�j� annonc� par le groupe fran�ais et d� au rachat des 25% que l'Etat belge d�tenait dans sa filiale Fortis pour 3,25 milliards d'euros.
Autre �l�ment cl� des r�gles b�loises, le ratio de levier (bilan rapport� aux fonds propres) atteint 3,7% et d�passe lui aussi le minimum de 3% requis.
El�ments non r�currents
En 2013, BNP Paribas a vu les �l�ments non r�currents peser n�gativement sur son b�n�fice net � hauteur de 1,21 milliard d'euros, contre une contribution positive de 184 millions en 2012.
Parmi ceux-ci, la banque a soulign� avoir provisionn� au quatri�me trimestre 798 millions d'euros, en lien avec une enqu�te des autorit�s am�ricaines sur des paiements en dollars r�alis�s dans des pays soumis � un embargo des Etats-Unis. Dans un dossier similaire, la britannique Standard Chartered avait pay� quelque 650 millions de dollars en 2012.
L'�tablissement de la rue d'Antin a aussi vu les co�ts de transformation de son plan d'efficacit� et de simplification ("Simple & Efficient") atteindre 661 millions d'euros pour des �conomies de co�ts qui atteignent d�j� 800 millions.�Hors exceptionnels, le b�n�fice net a aussi connu une baisse en 2013, mais moindre (-5,3%).
PNB en l�ger recul
Le produit net bancaire (PNB, �quivalent du chiffre d'affaires) est pour sa part ressorti en l�ger recul de 0,6%, � 38,82 milliards d'euros, un niveau sup�rieur aux attentes (38,49 milliards).
La banque, qui proposera un dividende annuel de 1,50 euro par action soit un niveau semblable � celui vers� l'an pass�, se f�licite d'avoir contenu � 2,9% la hausse de son co�t du risque (provisions pour risque d'impay�s, ndlr), malgr� une conjoncture "peu porteuse". En d�pit de cette progression, celui-ci reste � un niveau bas compar� � celui d'autres �tablissements.
Au sein des p�les op�rationnels, "Investment Solutions" (gestion d'actifs, banque priv�e et assurance) a tir� son �pingle du jeu en �tant le seul � faire cro�tre son PNB, de 2,3%.�En ce qui concerne la banque de d�tail, la situation a �t� contrast�e selon les pays. Si la France affiche sa r�silience avec un r�sultat avant imp�t en l�g�re baisse (-2%), la Belgique r�alise une bonne performance (+3%), tandis que l'Italie souffre de la situation �conomique (-46,3%).
Sur le seul quatri�me trimestre, le b�n�fice net a �t� divis� par plus de 4 (-75,5%), � 127 millions d'euros, en raison des exceptionnels.
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Commentaires
Hugo �a �crit le 13/02/2014                                     � 10:16 :
"Autre �l�ment cl� des r�gles b�loises, le ratio de levier (bilan rapport� aux fonds propres) atteint 3,7% et d�passe lui aussi le minimum de 3% requis."
C'est l'inverse, fonds propres rapport�s au bilan.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
