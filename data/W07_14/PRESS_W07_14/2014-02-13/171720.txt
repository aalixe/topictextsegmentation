TITRE: Thiriez d�plore la lettre des pr�sidents de L1 - Goal.com
DATE: 2014-02-13
URL: http://www.goal.com/fr/news/1729/france/2014/02/13/4618045/thiriez-d%25C3%25A9plore-la-lettre-des-pr%25C3%25A9sidents-de-l1
PRINCIPAL: 171713
TEXT:
Thiriez d�plore la lettre des pr�sidents de L1
Thiriez et les pr�sidents de clubs sont en d�saccord
S�lectionn�
0
13 f�vr. 2014 21:08:00
Fr�d�ric Thiriez, le pr�sident de la LFP �tait remont� envers les clubs qui d�non�aient l'accord entre l'ASM et la Ligue.
Apr�s avoir vu les clubs de L1 et L2 se rebeller envers la Ligue de Football suite � l'accord pass� entre l'ASM et l'instance du football Fran�ais, Fr�d�ric Thiriez a d�cid� de r�pliquer � son tour. Dans une lettre que s'est procur� RMC, le patron de la LFP "d�plore le courrier que quelques clubs ont adress�". Il continue en expliquant que la mise en cause faite par les 7 pr�sidents de L1 & L2 n'�tait pas acceptable :"Que cinq clubs oppos�s � cette solution aient �t� mis en minorit� lors du vote, apr�s avoir pu largement exprimer leur point de vue, ne les autorise pas aujourd'hui � jeter le discr�dit sur la r�gularit� des proc�dures suivies. Il s'agit l� d'une mise en cause inacceptable. C'est une position mercantile et irresponsable."
Relatifs
