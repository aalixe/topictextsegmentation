TITRE: Mobilisation contre la r�forme des rythmes scolaires
DATE: 2014-02-13
URL: http://www.ouest-france.fr/mobilisation-contre-la-reforme-des-rythmes-scolaires-1927421
PRINCIPAL: 168900
TEXT:
Mobilisation contre la r�forme des rythmes scolaires
Aube -
11 F�vrier
Belle mobilisation vendredi � Aube contre la mise en place de la r�forme des rythmes scolaires.�|�
Facebook
Achetez votre journal num�rique
En d�saccord complet avec la mise en application de la r�forme des rythmes scolaires, les parents �lus au conseil d'�cole et les membres de l'APE des �coles d'Aube ont de nouveau particip� au boycott national vendredi en bloquant l'acc�s aux �coles.
� Beaucoup de parents sont contre cette r�forme qui impliquera plus de fatigue pour nos enfants mais surtout un co�t �lev� pour la Communaut� de communes qui aura la charge de mettre en place les diff�rentes animations pr�vues dans le cadre des TAP, Temps d'activit� p�riscolaire � expliquent les membres de l'APE.
De plus, � nous voulons, entre autres, exprimer nos craintes quant � l'encadrement de ces TAP avec un animateur pour 10 enfants de moins de 6 ans et un animateur pour 18 enfants de plus de 6 ans � ajoute l'APE pr�te � � taper plus haut � et rencontrer les personnes ad�quates qui sauront enfin r�pondre � leurs questions.
Lire aussi
