TITRE: Actions de groupes, lunettes en ligne... Les 5 petites révolutions de la loi consommation - 13 février 2014 - Le Nouvel Observateur
DATE: 2014-02-13
URL: http://tempsreel.nouvelobs.com/economie/20140213.OBS6267/actions-de-groupes-lunettes-en-ligne-les-5-petites-revolutions-de-la-loi-consommation.html
PRINCIPAL: 0
TEXT:
Actualité > Economie > Actions de groupes, lunettes en ligne... Les 5 petites révolutions de la loi consommation
Actions de groupes, lunettes en ligne... Les 5 petites révolutions de la loi consommation
A+ A-
Le projet de loi consommation a �t� d�finitivement adopt� par les parlementaires ce jeudi. Et les consommateurs vont s'en apercevoir tr�s vite.
Tout recours en justice devra être mené par l'une des 16 associations de défense des consommateurs agréées. (Thomas Padilla/MAXPPP)
Le Parlement, par des votes successifs du S�nat mercredi et de l' Assembl�e nationale jeudi, a adopt� d�finitivement, jeudi 13 fevrier, le projet de loi sur la consommation, ensemble de mesures tr�s diverses qui ont �volu� au fil de la navette parlementaire.
D�put�s et s�nateurs socialistes et �cologistes ont approuv� le projet de loi, tandis que ceux de l'UMP ont vot� contre et ont annonc� leur intention de saisir le Conseil constitutionnel. Les centristes ont vot� pour au S�nat et se sont abstenus � l'Assembl�e, alors que les communistes ont fait l'inverse : abstention au S�nat et vote favorable � l'Assembl�e. Les d�put�s radicaux de gauche ont vot� pour � l'Assembl�e alors que leurs coll�gues s�nateurs se sont partag�s entre approbation et abstention.�
Ce projet de loi d�fendu par le ministre d�l�gu� � la Consommation Beno�t Hamon a ainsi achev� un parcours parlementaire entam� en juin dernier et qui, apr�s deux lectures dans chaque chambre, a d�bouch� sur un accord d�put�s-s�nateurs, conclu la semaine derni�re, qui vient d'�tre approuv�. Voici les nouvelles possibilit�s qui s'ouvrent au consommateur.�
1Mener des actions de groupe
Cette action permet de regrouper, dans une seule proc�dure, les demandes de r�paration �manant d'un grand nombre de consommateurs. Pour �viter les d�rives, le projet de loi pr�voit que tout recours en justice devra �tre men� par l'une des associations agr��es de d�fense des consommateurs. Sont exclus du champ de l'action de groupe, les domaines de la sant� et de l'environnement. Il y aura une proc�dure acc�l�r�e pour les contentieux les plus simples, c'est-�-dire pour les consommateurs facilement identifiables comme des abonn�s, les consommateurs l�s�s se voyant alors indemnis�s sans avoir � accomplir la moindre d�marche.
2Acheter ses lunettes et lentilles en ligne
Plus grande ouverture de la distribution de verres et lentilles, notamment en ligne . Les prescriptions de verres correcteurs devront indiquer la valeur de l'�cart pupillaire du patient, afin de faciliter leur achat sur internet. Le prestataire en ligne devra permettre au patient d'obtenir des informations et des conseils aupr�s d'un professionnel de sant� qualifi� en optique. Ces dispositions sont vivement critiqu�es par les opticiens, mais soutenues par les associations de consommateurs afin de permettre une baisse des prix.
3Acheter son test de grossesse en grande surface
Les d�put�s ont valid� une disposition introduite pr�c�demment par un amendement socialiste au S�nat, supprimant le monopole des pharmaciens sur la distribution des tests destin�s au diagnostic de la grossesse et des tests d'ovulation.�"Il ne s'agit pas de juger la sexualit� des Fran�ais", mais de "faciliter l'acc�s aux tests", a fait valoir la ministre d�l�gu�e � la Famille Dominique Bertinotti, en soulignant que cette vente en dehors des pharmacies ne remettait pas en cause l'exigence de s�curit� du produit, et que le Planning familial �tait favorable � la mesure.�
4Conna�tre mieux l'origine de ce qu'on mange
Les restaurants ont d�sormais�obligation pour les restaurateurs et soci�t�s de vente � emporter d'indiquer qu'un plat propos� est "fait maison", c'est-�-dire �labor� sur place � partir de produits bruts.
Par ailleurs, l'indication du pays d'origine est obligatoire pour toutes les viandes, et tous les produits � base de viande ou contenant de la viande, � l'�tat brut ou transform�. Les modalit�s en sont fix�es par d�cret apr�s que la Commission europ�enne a d�clar� cette obligation compatible avec le droit de l'Union europ�enne.
Enfin, les identit�s g�ographiques prot�g�es (IGP) -�d�signant des produits dont les caract�ristiques sont �troitement li�es � une zone g�ographique de production ou d'�laboration -�qui n'existaient que pour les produits alimentaires, seront �tendues aux produits artisanaux et manufactur�s, leur assurant ainsi�une protection juridique.
5R�silier facilement les contrats d'assurance
Les consommateurs pourront r�silier un contrat d'assurance � tout moment � l'issue d'une premi�re ann�e d'engagement et non � la date anniversaire du contrat comme actuellement. Cette possibilit� sera aussi �tendue aux assurances dites affinitaires (associ�es � l'achat d'un bien ou d'un service comme la t�l�phonie mobile).
�
Les principales dispositions de la loi en infographie :�
Partager
