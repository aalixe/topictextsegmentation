TITRE: Ce que le projet de loi sur la consommation va changer dans votre quotidien
DATE: 2014-02-13
URL: http://www.francetvinfo.fr/economie/ce-que-le-projet-de-loi-sur-la-consommation-va-changer-dans-votre-quotidien_529143.html
PRINCIPAL: 0
TEXT:
Tweeter
Ce que le projet de loi sur la consommation va changer dans votre quotidien
Lunettes, contrats d'assurances, tests de grossesses... Voici les principales dispositions du texte, adopt� d�finitivement jeudi par le Parlement.
Un homme tient une carte de cr�dit et des billets de banque, le 5 f�vrier 2013 � Tours (Indre-et-Loire). (ALAIN JOCARD / AFP)
, publi� le
13/02/2014 | 15:48
Le Parlement a adopt� d�finitivement le projet de loi sur la consommation , apr�s un vote au S�nat mercredi 12 f�vrier et � l'Assembl�e, jeudi 13 f�vrier.�Il reste maintenant au texte � franchir l'�tape du Conseil constitutionnel, que l'UMP entend saisir et qui aura un mois pour se prononcer. Voici les mesures-cl�s vot�es par les parlementaires.
Des "class actions" � la fran�aise
Ce type d'action de groupe devant la justice est une premi�re en France. Elle permet de regrouper, dans une seule proc�dure, les demandes de r�paration �manant d'un grand nombre de consommateurs.
Pour �viter les d�rives, le projet de loi pr�voit que tout recours en justice devra �tre men� par l'une des associations agr��es de d�fense des consommateurs. Sont exclus du champ de l'action de groupe les domaines de la sant� et de l'environnement.
Il y aura une proc�dure acc�l�r�e pour les contentieux les plus simples, c'est-�-dire pour les consommateurs facilement identifiables, comme des abonn�s. Les consommateurs l�s�s se verront indemnis�s sans avoir � accomplir la moindre d�marche.
Les lunettes et lentilles vendues sur internet
Le texte lib�ralise la vente de lunettes et de lentilles sur internet afin de faire baisser leur prix. Les prescriptions de verres correcteurs devront indiquer la valeur de l'�cart pupillaire du patient, afin de faciliter leur achat sur internet. Le prestataire en ligne devra permettre au patient d'obtenir des informations et des conseils aupr�s d'un professionnel de sant� qualifi� en optique. Ces dispositions sont vivement critiqu�es par les opticiens, mais soutenues par les associations de consommateurs.
Des tests de grossesse en supermarch�
Le projet de loi autorise la vente des tests de grossesse hors des pharmacies, dans les grandes surfaces. Cette mesure a �t� introduite par un amendement s�natorial�en septembre.
Le ministre d�l�gu� � la Consommation�Beno�t�Hamon estime que mettre fin au monopole des pharmaciens permettrait de�"faire baisser consid�rablement les tarifs". La ministre des Droits des femmes,�Najat Vallaud-Belkacem�avance le m�me argument. �Elle souligne "qu'aujourd'hui, toutes les femmes n'ont pas acc�s � ces dispositifs, en raison de leur co�t."�
Des contrats d'assurance plus faciles � r�silier
Les consommateurs pourront r�silier un contrat d'assurance � tout moment � l'issue d'une premi�re ann�e d'engagement et non � la date anniversaire du contrat, comme actuellement.
Un an pour ren�gocier l'assurance des pr�ts immobiliers�
Les pr�ts immobiliers�s'accompagnent toujours d'un contrat assurant ce pr�t.�Les emprunteurs pourront d�sormais b�n�ficier d'un d�lai d'un an, au cours duquel ils pourront ren�gocier l'assurance de leur pr�t.
Jusqu'ici, un emprunteur b�n�ficiait d'un d�lai de dix jours apr�s la signature pour changer de compagnie d'assurances. Un d�lai jug� trop court par les associations de consommateurs. En offrant une ann�e pour trouver une assurance meilleur march�, le gouvernement veut favoriser la concurrence entre les assurances et tirer, du coup, leurs prix vers le bas.
La cr�ation d'un fichier des personnes surendett�es
Un registre national des cr�dits � la consommation souscrits par les particuliers, plac� sous la responsabilit� de la Banque de France, est �galement cr�� pour lutter contre le surendettement.
Une protection contre le�d�marchage t�l�phonique...
Une liste rouge des personnes refusant d'�tre d�march�es au t�l�phone sera constitu�e. Les entreprises concern�es devront la croiser avec leurs fichiers t�l�phoniques pour �viter de les appeler.
Une protection lors des achats en ligne
Le d�lai de r�tractation apr�s un achat en ligne passera de 7 � 14 jours.�
Le stationnement tarif� au quart d'heure
Les tarifs des parkings seront calcul�s par quart d'heure et non plus par heure. La tarification � la minute, vot�e par les s�nateurs, a finalement �t� �cart�e.
La lutte contre l'obsolescence programm�e
Les fabricants auront l'obligation d'afficher jusqu'� quelle date seront disponibles les pi�ces d�tach�es indispensables aux produits, et de les fournir.�
Des identit�s g�ographiques prot�g�es
Les IGP, indications g�ographiques prot�g�es,�n'existaient que pour les produits alimentaires. Elles seront �tendues aux produits artisanaux et manufactur�s.
L'indication du pays d'origine obligatoire pour les viandes
L'indication du pays d'origine est obligatoire pour toutes les viandes, et tous les produits � base de viande ou contenant de la viande, � l'�tat brut ou transform�. Les modalit�s seront fix�es par d�cret, apr�s que la Commission europ�enne a d�clar� cette obligation compatible avec le droit de l'Union europ�enne.
L'instauration du "fait maison" au restaurant
Les restaurateurs fran�ais devront d�sormais pr�ciser sur leurs cartes les plats "faits maison" , c'est-�-dire �labor� sur place � partir de produits bruts, pour les diff�rencier des mets industriels r�chauff�s.
La cr�ation de magasins de producteurs
Des agriculteurs pourront se regrouper afin de cr�er un magasin de producteurs. Objectif : commercialiser directement leurs produits sur le march� local.
Le march� de l'or encadr�
L'encadrement du march� des m�taux pr�cieux, en pleine croissance gr�ce � la crise �conomique, sera renforc�. Un contrat �crit sera obligatoire lors de tout achat.
Davantage de moyens pour la r�pression des fraudes
Le texte �renforce les moyens de la DGCCRF (Direction de la consommation, de la concurrence et de la r�pression des fraudes).�Les agents de la r�pression des fraudes pourront, par exemple, masquer leur qualit� lors des contr�les.
Des sanctions plus lourdes contre la fraude �conomique
Les amendes pourront atteindre jusqu'� 10% du chiffre d'affaires de l'entreprise qui aura fraud� afin de s'assurer que la sanction lui co�tera plus cher que la fraude ne lui aura rapport�. De m�me, le montant de l'amende pour les personnes physiques sera multipli� par 10, passant de 37�500 � 300�000�euros. Enfin, le juge pourra interdire au contrevenant toute activit� commerciale pendant cinq ans
