TITRE: Serge Dassault redevient un  simple  justiciable - 13/02/2014 - La Nouvelle R�publique
DATE: 2014-02-13
URL: http://www.lanouvellerepublique.fr/Toute-zone/Actualite/24-Heures/n/Contenus/Articles/2014/02/13/Serge-Dassault-redevient-un-simple-justiciable-1794219
PRINCIPAL: 169004
TEXT:
Serge Dassault redevient un " simple " justiciable
13/02/2014 05:30
Le S�nat a lev� mercredi l�immunit� parlementaire de Serge Dassault, dans le�cadre d�une enqu�te sur des achats pr�sum�s de voix � Corbeil-Essonnes.
Le bureau du S�nat a lev� mercredi sans surprise l'immunit� parlementaire de Serge Dassault, ce qui permettra � la justice de placer le s�nateur UMP et homme d'affaires en garde � vue dans le cadre d'une enqu�te sur des achats pr�sum�s de voix � Corbeil-Essonnes .
Les quatorze membres de gauche du bureau ont vot� � main lev�e pour la lev�e de l'immunit� de l'industriel, ��le reste refusant de participer au vote��, a pr�cis� le pr�sident du S�nat, Jean-Pierre Bel (PS).
Serge Dassault avait pris les devants en demandant lui-m�me lundi cette lev�e , retirant ainsi tout suspense � la r�union.
Le bureau est compos� de vingt-six membres, dont Jean-Pierre�Bel, quatorze de gauche et douze de droite. Deux membres de droite du bureau, dont l'ancien Premier ministre Jean-Pierre Raffarin, �taient absents. Jean-Pierre Bel a aussi annonc� que le bureau du S�nat voterait d�sormais � main lev�e sur les demandes de lev�e d'immunit� parlementaire, et non plus � bulletin secret. La lev�e de l'immunit� permettra aux magistrats du p�le financier de Paris, Serge Tournaire et Guillaume Da�eff, de placer le s�nateur de 88 ans en garde � vue dans le cadre d'une affaire d'achats pr�sum�s de voix � Corbeil-Essonnes (Essonne).
----------------
En savoir plus
Jean-Pierre Bel a annonc� que le bureau du S�nat voterait d�sormais � main lev�e sur les demandes de lev�e d'immunit� parlementaire, et non plus � bulletin secret. ��Nous allons mettre en place un groupe de travail pour balayer un certain nombre de sujets et sur la question de l'immunit� parlementaire��, a-t-il aussi indiqu�.
