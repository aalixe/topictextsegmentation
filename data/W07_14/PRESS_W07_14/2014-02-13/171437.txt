TITRE: Italie: le Premier ministre Enrico Letta démissionne - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-13
URL: http://tempsreel.nouvelobs.com/topnews/20140213.AFP0044/italie-le-premier-ministre-enrico-letta-annonce-sa-demission.html
PRINCIPAL: 0
TEXT:
Actualité > TopNews > Italie: le Premier ministre Enrico Letta démissionne
Italie: le Premier ministre Enrico Letta démissionne
Publié le 13-02-2014 à 18h40
Mis à jour le 14-02-2014 à 07h20
A+ A-
Le Premier ministre italien Enrico Letta a annoncé jeudi son intention de remettre sa démission, aussitôt après un vote de son parti réclamant un changement de gouvernement. (c) Afp
Rome (AFP) - Le Premier ministre italien Enrico Letta a annoncé jeudi sa démission sous la pression du chef de son parti, le bouillant Matteo Renzi, qui devrait lui succéder à la tête du gouvernement.
M. Letta a annoncé cette décision quelques minutes à peine après un vote de la direction de sa formation de centre-gauche, le Parti démocrate (PD), réclamant un changement de gouvernement. Cette motion proposée par M. Renzi demandait d'"ouvrir une phase nouvelle avec un exécutif nouveau soutenu par la majorité actuelle", avec le centre-droit. Elle a été approuvée à une très large majorité de 136 voix (sur un peu plus de 150).
"C'est la crise de gouvernement. Renzi vers le Palais Chigi (siège du gouvernement) avec le feu vert du PD", titrait le journal La Stampa sur son site internet peu après ces annonces.
En lisant le document demandant d'ouvrir "une nouvelle page", M. Renzi, maire de Florence de 39 ans, a rendu hommage à Enrico Letta "pour l'important travail accompli" et s'est défendu de vouloir "faire le procès" de son prédécesseur.
Il lui a reconnu notamment le mérite d'avoir pris la tête d'une coalition inédite gauche-droite, de "large entente" dans une situation économique, sociale et politique difficile. Mais il a jugé nécessaire et urgent de "changer d'horizon et de rythme", en exhortant les autres membres de la direction du PD à "prendre le risque du changement" et à "sortir des marécages".
Sans donner de détails sur le contenu de son futur programme, M. Renzi a évoqué un "projet de relance radicale, de changement profond".
Pour M. Renzi en revanche pas question de revenir aux urnes: des élections "ne réussiraient pas à résoudre les problèmes du pays en l'absence d'une loi électorale garantissant une majorité certaine", a-t-il dit, se fixant comme horizon la fin de la législature en 2018.
- Matteo Renzi piaffait d'impatience -
M. Letta, ex-numéro deux de ce parti, avait décidé de ne pas participer à la réunion du PD, officiellement pour permettre à ses collègues de prendre leurs décisions "avec le maximum de sérénité".
Au fil des débats au sein de la direction du PD, le sort de M. Letta est apparu scellé, la grande majorité des intervenants appelant à un "acte de clarté".
La veille encore, M. Letta avait défié son adversaire en présentant un programme pour relancer son gouvernement, appelé "Engagement Italie".
"Rompre mon action au service du pays ne fait pas partie de mon ADN", avait-il dit en se disant avant tout "homme des institutions".
Depuis son arrivée à la tête du PD en décembre dernier et surtout depuis qu'il avait conclu un accord à la mi-janvier pour une nouvelle loi électorale avec Silvio Berlusconi, Matteo Renzi piaffait d'impatience. Il multipliait les attaques contre l'exécutif Letta, lui reprochant lenteur et manque de détermination, malgré le pacte noué entre les deux hommes pour une poursuite de l'action du gouvernement Letta jusqu'à au moins la fin 2014.
Selon Giovanni Orsina, professeur de Sciences politiques à l'Université Luiss de Rome, le dualisme Renzi-Letta était devenu ces dernières semaines "un énième facteur de paralysie politique ".
M. Letta "montera" vendredi au Quirinal la colline où se trouve la présidence, après un dernier conseil des ministres au Palais Chigi. Ensuite le président Giorgio Napolitano devrait procéder à des consultations des différents partis avant de très probablement choisir M. Renzi pour former un nouveau gouvernement .
Une fois celui-ci constitué, le nouvel exécutif devra se présenter peut-être déjà mardi prochain devant le parlement pour obtenir le vote de confiance.
Partager
