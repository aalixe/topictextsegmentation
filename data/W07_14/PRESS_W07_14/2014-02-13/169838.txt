TITRE: HOW I MET YOUR DAD: Greta Gerwig sur le petit écran :: FilmDeCulte
DATE: 2014-02-13
URL: http://www.filmdeculte.com/cinema/actualite/HOW-I-MET-YOUR-DAD-Greta-Gerwig-sur-le-petit-ecran-19067.html
PRINCIPAL: 169834
TEXT:
Publi� le 13/02/2014
HOW I MET YOUR DAD: Greta Gerwig sur le petit �cran
Greta Gerwig (Frances Ha, Greenberg...) tiendra le r�le de la "m�re" dans la s�rie TV spin-off de How I Met Your Mother, qui arrive � son terme au bout de neuf saisons. Un choix surprenant pour l'actrice qui a tant enthousiasm� les foules sur le grand �cran, d'autant que How I Met Your Mother, depuis longtemps maintenant, peine � se renouveler ou m�me � tenir sur la longueur. L'espoir viendra du fait que Gerwig ne se contentera pas d'�tre devant la cam�ra, puisqu'elle sera aussi productrice et sc�nariste pour cette nouvelle s�rie.
Dans ce spin-off, Greta Gerwig interpr�tera Sally, jeune femme d�crite comme un "Peter Pan f�minin", ayant des difficult�s � grandir et se reposant sur les conseils de ses proches pour avancer dans la vie. Il n'y a pas encore de date pour la diffusion.
Et pour ne rien louper de nos news, dossiers, critiques et entretiens, rejoignez-nous sur Facebook et Twitter !
