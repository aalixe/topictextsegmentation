TITRE: Boursorama : num�ro 1 de la banque en ligne et portail leader de l'information financi�re
DATE: 2014-02-13
URL: http://www.boursorama.com/actualites/societe-generale-kerviel-en-cassation-sous-la-menace-d-une-incarceration-644c700f90cb9a4f8fc851f4f77f9cf9
PRINCIPAL: 169715
TEXT:
Boursorama Banque est la seule banque en ligne certifi�e AFNOR CERTIFICATION ENGAGEMENT DE SERVICE WEBCERT� - REF - 172-01
Site garanti VeriSign SSL pour la s�curit� et la confidentialit� des communications.
Politique d'ex�cution
Bourses de Paris, indices Euronext en temps r�el  -  Indice Francfort en diff�r� 15 minutes  -  Cours diff�r�s d'au moins 15 mn (Europe, Bruxelles, Amsterdam, Nasdaq, Francfort, Londres, Madrid,  Toronto, NYSE, AMEX)  -  20mn (Milan) ou 30mn (Z�rich, NYMEX)  -  Les indices et les cours sont la propri�t� des partenaires suivants � NIKKEI Inc, � NYSE Euronext, � TMX Group Inc.
BOURSORAMA diffuse sur son site Internet des informations dont les droits de diffusion ont �t� conc�d�s par des fournisseurs de flux, SixTelekurs et Interactive Data
Copyright � 2014 BOURSORAMA
