TITRE: Gen�ve 2014 : voici (encore) la nouvelle Twingo en avance
DATE: 2014-02-13
URL: http://www.caradisiac.com/Geneve-2014-voici-encore-Nouvelle-Twingo-en-avance-92415.htm
PRINCIPAL: 170927
TEXT:
Gen�ve 2014 : voici (encore) la nouvelle Twingo en avance
Ecrit par Patrick Garcia le 13 F�vrier 2014
Internet est une horreur pour tous les plans de communication patiemment �labor�s dans les bureaux des agences et des constructeurs pr�sentant de nouveaux mod�les. Alors que Renault a lanc� un site qui doit permettre d'effeuiller la Nouvelle Twingo en direct gr�ce � des tweets de 17h � 19h, heure � laquelle doit avoir lieu le d�voilement officiel, sachez qu'il ne vous servira � rien d'attendre car voici d�j� Nouvelle Twingo.
Il y a finalement tr�s peu de mod�les de grande diffusion qui r�sistent � la fuite sur la Toile et c'est encore plus vrai en amont du salon de Gen�ve 2014 o� quasiment toutes les premi�res annonc�es arrivent en avance. Dans le cas de la Nouvelle Twingo, apr�s la bourde de ce matin d'Autobild qui a diffus� la Une de son prochain num�ro avec en couverture la Nouvelle Twingo mais aussi la Peugeot 108 et le BMW S�rie 2 Active Tourer de s�rie, trois nouvelles images de la petite Renault d�barquent avec quelques heures d'avance.
L'auto est effectivement tr�s proche du concept Twin'run d�voil� � Monaco l'an dernier, et comme le laissent entendre ces images, le choix d'une allure sportive a �t� fait pour cette petite citadine qui ne devrait pas avoir trop de mal � faire oublier celle qu'elle remplace. On rappellera que la Nouvelle Twingo partage sa plateforme avec la future Smart 4 portes et qu'elle devient donc une propulsion � moteur arri�re. Elle devient �galement une 5 portes, la version 3 portes ayant �t� mise au placard.
Tous les d�tails arriveront demain matin apr�s la sortie du communiqu� officiel dans la nuit, en attendant, vous trouverez LA photo officielle de la Nouvelle Twingo dans le portfolio ci-dessous.
Via carscoop
