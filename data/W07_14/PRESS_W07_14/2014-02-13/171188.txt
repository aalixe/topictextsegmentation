TITRE: Cancer du sein - mammographies : encore une �tude pol�mique - Sciences - MYTF1News
DATE: 2014-02-13
URL: http://lci.tf1.fr/science/sante/le-debat-sur-le-depistage-du-cancer-du-sein-par-mammographie-relance-8365031.html
PRINCIPAL: 0
TEXT:
cancer
Sant�Une enqu�te canadienne remet en cause la pertinence des mammographies annuelles pour le d�pistage du cancer du sein. Cette pol�mique sur les mammographies n'est pas nouvelle.
Une �tude r�alis�e par la Canadian National Breast Cancer Screening Study et publi�e dans le British Medical Journal relance une nouvelle fois le d�bat sur la n�cessit� du d�pistage par mammographie. Les chercheurs ont montr� que la pratique de mammographies annuelles ne permettait pas de r�duire la mortalit� par cancer du sein.��
En France, la mammographie est le principal protocole de diagnostic du cancer du sein. Elle s'adresse � toutes les femmes �g�es de 50 � 74 ans et peut �tre r�alis�e une fois tous les deux ans, en �tant enti�rement prise en charge par l'Assurance maladie. Selon le Dr J�r�me Viguier, directeur du P�le sant� publique et soins de l'Institut national du cancer le d�pistage aurait�permis de r�duire la mortalit� par cancer du sein de 15 � 20% et d'�viter 150 � 300 d�c�s pour 100.000 femmes, qui s'�taient fait suivre r�guli�rement pendant dix ans.
Le d�pistage r�ellement n�cessaire ?
L'enqu�te canadienne�a �t� men�e aupr�s de 89.835 femmes �g�es de 40 � 59 ans, suivies pendant 25 ans. Ces derni�res ont �t� r�parties en deux groupes. D'un c�t�, 44.925 femmes ont r�alis� des mammographies chaque ann�e, d'un autre c�t�, 44.910 femmes, ont eu recours uniquement � des examens physiques.�
Au bout de cinq ans, 666 cancers du sein ont �t� d�tect�s dans� le premier groupe contre 524 dans le second groupe, soit un "exc�dent" de 142 tumeurs. Cet "exc�dent" �tait encore de 106 tumeurs au bout de 15 ans. "22% des cancers diagnostiqu�s dans le premier groupe ont �t� sur-diagnostiqu�s", analysent les chercheurs. Les tumeurs �taient de surcro�t plus petites (1,4 �cm dans le premier groupe contre 2,1 cm dans le second) au moment du diagnostic. Pr�cisons que le sur-diagnostic fait r�f�rence � la d�tection de tr�s petites tumeurs qui n'auraient pas eu d'impact du vivant de la personne concern�e.
�
Apr�s 25 ans, 500 d�c�s par cancer du sein ont �t� constat�s dans le groupe d�pist� contre 505 dans le groupe non d�pist�.�
Une pol�mique qui dure
De nombreuses enqu�tes se sont pench�es sur le sujet, que ce soit en France ou � l'�tranger. Et les r�sultats apparaissent contradictoires. Selon une �tude men�e par la Collaboration Cochrane en 2000 et r�guli�rement mise � jour, le taux de mortalit� des femmes suivies par mammographie ou de celles qui ont eu recours � des examens cliniques serait sensiblement �quivalent.
Une �tude britannique publi�e en 2012 avait, quant � elle, montr� que le d�pistage du cancer du sein sauvait des vies mais entra�nait un sur-diagnostic estim� � pr�s de 20% des cancers d�pist�s.
Commenter cet article
Ecrire votre commentaire ici ...
Vous devez �crire un avis
castor1911 : Ba je suis bien plac� pour savoir vu le milieu dans lequel je travaille , beaucoup d'examen d'imagerie sont inutiles , a part pour confirmer un diagnostic clinique .... et en faisant sa toilette , on peut s'auto-palper , cela prend 2 minutes .
Le 16/02/2014 � 20h38
isshoni : Personnellement, j'ai toujours pens� que la mammographie est tellement douloureuse que je me suis toujours demand� si ce n'�tait pas pr�cis�ment elle, qui donnait le cancer !!!!!
Le 16/02/2014 � 19h55
victorine57 : Parce que rien n'a �t� d�tect� avant ni chez le m�decin ni en palpation et auto-palpation , vous vous auto-palpez du matin au soir vous ? alors quand on sait pas ......
Le 14/02/2014 � 12h53
castor1911 : Evident pourquoi ? alors qu'il suffit de s'auto- palper .....
Le 13/02/2014 � 23h51
victorine57 : Comment peuvent-ils pol�miquer sur ce qui est �vident ?au cours d'un banal contr�le j'ai pass� une mammographie en 2000 qui � d�tect� mon cancer , c'est ce qui m'a sauv� . Que dire de plus !
Le 13/02/2014 � 19h46
