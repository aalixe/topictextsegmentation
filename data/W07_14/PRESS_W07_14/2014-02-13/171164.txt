TITRE: JO 2014 / PATINAGE ARTISTIQUE - Programme court (H) : Plushenko d�clare forfait
DATE: 2014-02-13
URL: http://www.sport365.fr/jo-2014-sotchi/programme-court-h-plushenko-declare-forfait-1103128.shtml
PRINCIPAL: 171160
TEXT:
JO 2014 / PATINAGE ARTISTIQUE - Publi� le 13/02/2014 � 17h15 -  Mis � jour le : 13/02/2014 � 18h29
0
Programme court (H) : Plushenko d�clare forfait
Incertain avant le d�but de l'�preuve, Evgeny Plushenko avait annonc� tenir sa place. Lors de l'�chauffement qui a pr�c�d� le d�but de l'�preuve jeudi, il a finalement d�clar� forfait.
Il aura essay�, mais en vain.� Un temps incertain puis disponible pour l��preuve , le tsar Evgeny Plushenko n�a pas pu faire face � ses douleurs r�currentes au dos. Lors de l��chauffement avant le programme court de jeudi, le Russe s�est essay� � quelques sauts avec des r�ceptions douteuses en se tenant le dos. Le champion olympique 2006 a �t� contraint de d�clarer forfait avant le d�but de l��preuve.
�
