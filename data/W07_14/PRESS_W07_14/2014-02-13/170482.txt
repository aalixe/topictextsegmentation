TITRE: Nicolas Demorand d�missionne de "Lib�ration"
DATE: 2014-02-13
URL: http://www.parisdepeches.fr/2-Societe/128-75_Paris/9065-Nicolas_Demorand_demissionne_%2522Liberation%2522.html
PRINCIPAL: 170480
TEXT:
Nicolas Demorand d�missionne de "Lib�ration"
Publi� le 13/02/2014�Par Roxane Bayle
Luc Legay - Flickr
Le directeur contest� du journal "Lib�ration" a annonc� sa d�mission.
Il a fini par jeter l'�ponge. Apr�s avoir subi 4 votes de d�fiance de la part de ses salari�s, et le rejet de son texte pour l'�dition du lendemain, Nicolas Demorand a fini par d�missionner.
Il se confie dans un entretien accord� au Monde : "J'ai �t� confront�, pendant mes trois ans pass�s � Lib�ration, � des crises s�v�res, mais c'est la premi�re fois qu'il m'appara�t clair que je dois bouger." dit le d�sormais ex-directeur du journal, qui traverse une crise violente. Les employ�s s'opposent au projet des actionnaires du journal de le transformer, entre autres, en "r�seau social" ou en "caf�"...
Conscient qu'il "cristallise une partie des d�bats" (4 votes de d�fiances � son encontre ces derniers mois), Nicolas Demorand pr�f�re se retirer. Il n'est plus retourn� dans les locaux du journal depuis qu'une majorit� de journalistes a "d�cid� de censurer" un de ses articles, "qui d�fendait l'imp�rieuse n�cessit� de se battre pour un Lib� fort."
Pour Demorand, il faut que l'entreprise devienne "plurim�dia" autour d'un journal "fort"
Arriv� en mars 2011 � la t�te de "Lib�ration", Nicolas Demorand a voulu faire prendre le virage num�rique au journal, mais sans rencontrer beaucoup de succ�s : "Il faut donc red�ployer une partie de l'�nergie sur tous nos supports : print, Web, �v�nements. C'est l� que les choses se bloquent, car des cultures professionnelles et des pratiques du journalisme tel qu'il doit �tre aujourd'hui entrent en conflit." Il d�nonce un "sous-investissement chronique".
Nicolas Demorand dit aujourd'hui travailler sur d'autres projets, et continue la t�l�vision. Il esp�re que son "d�part profitera � l'�quipe de Lib� et � ce journal irrempla�able".
�
Tweeter
R�agir
Si vous souhaitez voir votre commentaire appara�tre directement sur le site sans attendre la validation du mod�rateur, veuillez vous identifier ou cr�er un compte sur le site Paris D�p�ches.
