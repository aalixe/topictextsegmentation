TITRE: Natalie Portman : le tournage de son film en Isra�l critiqu� par des ultra-orthodoxes - Voici
DATE: 2014-02-13
URL: http://www.voici.fr/news-people/actu-people/natalie-portman-le-tournage-de-son-film-en-israel-critique-par-des-ultra-orthodoxes-521203
PRINCIPAL: 170759
TEXT:
Natalie Portman aurait aim� �tre moins sage
01/11/2013
Sur le tournage de son premier film en tant que r�alisatrice, Natalie Portman se frotte � la col�re des habitants ultra-orthodoxes d�un quartier de J�rusalem, qui regrettent une ��invasion �trang�re��.
Pour ses d�buts derri�re la cam�ra, Natalie Portman rencontre des difficult�s inattendues. En Isra�l pour tourner l�adaptation du roman autobiographique d�Amos Oz, A Tale of Love and Darkness, l�actrice s�est attir�e les foudres des habitants de Nahalot � J�rusalem.
Cette semaine, les leaders ultra-orthodoxes du quartier ont �crit une lettre � la municipalit� pour se plaindre de l�arriv�e d�une �quipe de tournage. Comme le rapporte le Times of Israel , des graffitis d�non�ant une ��invasion �trang�re�� ont �t� retrouv�s sur les murs.
��Le tournage doit avoir lieu dans plusieurs rues sensibles, pr�s des synagogues et des yeshivas [centres d'�tude de la Torah et du Talmud, NDLR], et les sc�nes tourn�es auraient d� �tre examin�es au pr�alable pour s�assurer qu�elles n�offenseraient aucune sensibilit頻, peut-on lire dans le courrier.
Les r�sidents regrettent �galement que les autorit�s ne les aient pas inform�s du d�roulement d�un tournage et de ne pas avoir �t� consult�s avant que Natalie Portman ne d�barque avec ses cam�ras.
Pour r�pondre aux craintes des administr�s �chaud�s, la municipalit� leur a promis que tous les acteurs impliqu�s dans le projet s�habilleraient de fa�on d�cente lors de leur pr�sence sur les lieux.
��Il y a une tension constante entre le d�sir de c�l�brer J�rusalem, une ville diverse et int�ressante, et les tentatives des groupuscules extr�mistes de l�emp�cher, explique Rachel Azaria, d�put� maire de J�rusalem. L�attractivit� de la ville, son architecture unique, ainsi que les efforts d�ploy�s par l�industrie du cin�ma et de la t�l�vision finiront par triompher.�� Et d�afficher son soutien � la r�alisatrice n�e sur le sol isra�lien, gr�ce � qui la mise en place de projets de ce type continuera de ��fleurir�� � Nahalot.
Malgr� ces al�as, le tournage a continu� selon le planning pr�vu et aucun incident n�est � d�plorer.
Comme le rappelle le Hollywood Reporter , la star de Black Swan a re�u environ 400 000 euros du Jerusalem Film Fund pour �crire, r�aliser et jouer dans le long-m�trage, qui raconte l�enfance d�un petit gar�on dans le J�rusalem de la fin des ann�es 1940, entre une m�re souffrant de troubles mentaux et les tentatives de son p�re pour l�aider � s�en sortir.
Mots-cl�s :
