TITRE: Plushenko renonce - JO 2014 - Patinage art. - Sport.fr
DATE: 2014-02-13
URL: http://www.sport.fr/patinage-artistique/jo-2014-plushenko-renonce-339879.shtm
PRINCIPAL: 171391
TEXT:
Plushenko renonce
Jeudi 13 f�vrier 2014 - 18:27
Quelques minutes  avant son programme court, le quadruple m�daill� olympique russe Evgueni Plushenko , souffrant du dos, a renonc� � participer l'�preuve de patinage artistique des jeux Olympiques, jeudi � Sotchi.
Pendant l'�chauffement, le patineur de 31 ans, sacr� dimanche avec la nouvelle �preuve par �quipes, s'est tenu le dos, op�r� il y a un an. Il est all� voir le juge central pour lui signifier qu'il renon�ait.
Le triple champion du monde s'est �chauff� avec difficult�s et apr�s s'�tre mal r�ceptionn� sur un saut, est all� voir son entra�neur de toujours en bord de piste, Alexei Mishin.
Les deux hommes ont parl� et Plushenko est revenu sur le centre de la piste � l'appel de son nom, annon�ant son entr�e en lice, sous les cris du public.
Le Russe a salu� les spectateurs, plut�t surpris, et a quitt� la glace.
Plushenko avait d�j� gagn� son pari: d�crocher � nouveau de l'or olympique, ce qu'il a fait dimanche avec le titre de la nouvelle �preuve par �quipes. Il s'est offert une 4e m�daille aux Jeux pour �galer le seul patineur � ce niveau dans l'Histoire du patinage, le Su�dois Gillis Graefstroem (de 1920 � 1932).
Le trentenaire, qui faisait � Sotchi son 2e retour sur le devant de la grande sc�ne, aurait pu viser plus loin en devenant le premier � d�tenir 5 m�dailles olympiques.
Mais le mal au dos dont il dit encore souffrir apr�s une op�ration visant � lui remplacer un disque, a calm� ses ardeurs.
Le titre individuel �tait quoi qu'il en soit inaccessible pour Plushenko tant il est surclass� par une nouvelle g�n�ration de brillants patineurs, dont les deux favoris, le triple champion du monde en titre, le Canadien Patrick Chan , et le jeune prodige japonais Yuzuru Hanyu .
