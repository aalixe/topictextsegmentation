TITRE: RPT-Duel franco-fran�ais dans la course aux �oliennes en mer - Bourse secteur International d�EasyBourse - 13 F�vrier 2014
DATE: 2014-02-13
URL: http://www.easybourse.com/bourse/international/news/1076475/rpt-duel-franco-francais-dans-la-course-aux-eoliennes-en-mer.html
PRINCIPAL: 171680
TEXT:
RPT-Duel franco-fran�ais dans la course aux �oliennes en mer
Publi� le 13 F�vrier 2014
Copyright � 2014 Reuters
RPT-Duel franco-fran�ais dans la course aux �oliennes en mer
-
R�p�tition: bien lire "la seule offre de GDF-Suez et Areva".
par Guillaume Frouin
NANTES (Reuters) - Les deux consortiums en lice pour les deux derniers champs d'�oliennes en mer fran�ais rivalisent d'arguments �conomiques et environnementaux pour emporter le soutien des acteurs locaux, deux mois avant le choix du laur�at en avril prochain.
EDF Energies Nouvelles a annonc� jeudi, en marge d'un colloque � Nantes, vouloir cr�er 1.500 emplois en France avec son fabricant d'�oliennes Alstom si leur consortium remporte les deux parcs au large des �les d'Yeu et de Noirmoutier (Vend�e) et du Tr�port (Seine-Maritime). Les appels d'offres ont �t� clos en novembre.
Ils viendraient s'ajouter aux 7.000 emplois g�n�r�s par les trois premiers champs d'�oliennes de Saint-Nazaire (Loire-Atlantique), F�camp (Seine-Maritime) et Courseulles-sur-Mer (Calvados), qui leur avaient �t� confi�s en 2012 au terme du premier appel d'offres gouvernemental.
Un quatri�me parc, au large de Saint-Brieuc (C�tes-d'Armor), avait �t� attribu� � Areva et � l'espagnol Iberdrola. Le champ d'�oliennes du Tr�port, d�j� mis en jeu � l'�poque, n'avait lui pas �t� attribu�, la seule offre de GDF-Suez et Areva n'ayant pas �t� jug�e comp�titive.
Grand perdant de ce premier appel d'offres, avec aucun parc �olien off-shore remport�, GDF-Suez est donc revenu � la charge avec Areva. Son consortium entend pour sa part cr�er "6.000 emplois dont 1.500 directs" en France le temps de la construction des deux parcs, puis � terme "500, dont 130 directs" � proximit� de chacun des deux pour leur maintenance.
Tous deux mettent �galement en avant les 400.000 heures de formation g�n�r�es par leur projet, ainsi que leurs accords d'ores et d�j� sign�s avec les �coles d'ing�nieurs et autres �tablissements universitaires des Pays de la Loire.
Leur groupement mise surtout sur sa turbine de huit m�gawatts - contre six pour celle d'Alstom -, qui permettra d'implanter selon eux 30 % d'�oliennes de moins que leur concurrent, pour une production d'�lectricit� �quivalente.
"Elles seront donc plus �loign�es les unes des autres, ce qui facilitera les possibilit�s de navigation pour les p�cheurs", dit Jean-Baptiste S�journ�, directeur d�l�gu� de GDF-Suez Energie France, ce que d�mentent ses concurrents.
UN �CHEC PROFITABLE
Le nombre r�duit d'�oliennes permettrait aussi de r�duire leur d�lai de fabrication de deux ans, selon GDF-Suez, et donc d'acc�l�rer la mise en service des parcs vend�en et normand.
L'�chec de son premier consortium en 2012 serait un atout en termes d'emploi local en Vend�e, fait m�me valoir le "leader en France de l'�olien terrestre", alors que son concurrent a remport� le champ d'�oliennes voisin de Saint-Nazaire.
"Si nous remportons le parc des �les d'Yeu et Noirmoutier, nous ne pourrons pas mutualiser nos installations avec celles de Saint-Nazaire, comme va le faire EDF-EN. Il faudra donc en cr�er de nouvelles", explique � Reuters Rapha�l Tilot, directeur du projet chez GDF-Suez.
L'attribution des deux parcs � son consortium serait �galement gage d'une "saine concurrence" chez les fabricants d'�oliennes, alors qu'Alstom dispose d�j� de trois parcs et Areva d'un seul.
"Actuellement cela fait 3-1, un 3-3 serait plus sain pour la France", estime Rapha�l Tilot. "S'il n'y avait qu'une seule offre, l'Etat et les consommateurs seraient perdants."
De son c�t�, EDF-Energies nouvelles se contenterait volontiers d'un "1-1". "Notre proposition sur les sites des �les d'Yeu et de Noirmoutier est vraiment diff�renciante, ce qui est moins le cas au Tr�port", conc�de B�atrice Buffon, directrice g�n�rale adjointe d'EDF-EN.
Edit� par Yves Clarisse
