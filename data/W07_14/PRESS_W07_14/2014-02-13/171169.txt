TITRE: Yohann Diniz : fissure du calcan�um du pied droit - Athl�tisme - Eurosport
DATE: 2014-02-13
URL: http://www.eurosport.fr/athletisme/yohann-diniz-fissure-du-calcaneum-du-pied-droit_sto4134797/story.shtml
PRINCIPAL: 0
TEXT:
Yohann Diniz : fissure du calcan�um du pied droit
le 13/02/2014 � 17:21, mis � jour le 13/02/2014 � 17:22
De notre partenaire VO�
Une mauvaise r�ception, en acc�dant aux installations du CREPS de Reims, a provoqu� une douleur du talon et une impotence fonctionnelle cons�quente, le mardi matin. Il a r�alis� un scanner dans un premier temps sur Reims qui n'a pas pu objectiver de l�sion. Mais apr�s avoir consult�, mercredi 12, le Dr Jean-Michel Serra, m�decin de l'Equipe de France, une fissure nette du calcan�um droit a �t� d�tect�e lors d�une  IRM r�alis�e � Paris avec le Dr Bella�che. Un examen de contr�le sera effectu� dans 4 semaines.
Le r�cent recordman de France de 5000 m marche en salle ne pourra pas participer aux Championnats de France en salle (Bordeaux, 22 et 23 f�vrier 2014). Il esp�re toutefois pouvoir �tre r�tabli pour la Coupe du monde de Marche (3 et 4 mai, Taicang en Chine).
"La Coupe du monde demeure un objectif, dit Yohann Diniz . Mais l�objectif num�ro un reste les Championnats d�Europe � Zurich. Je veux y conserver mon titre. Ce sont les petits al�as des sportifs de haut niveau. Mais je n�ai jamais �t� aussi fort que quand j�ai eu des p�pins ! Je dois rester vigilant, mais je suis confiant car je suis dans un excellent �tat de forme. Dans deux semaines, j�irai en Thalasso � Carnac. A l�origine, c��tait pr�vu comme une coupure pour aborder la saison estivale, mais finalement, je m�adapte et j�y ferai du travail musculaire et de la marche dans l�eau."
Retrouvez plus d�articles sur VO�
�
