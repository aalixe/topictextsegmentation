TITRE: Stephen King publiera deux romans en 2014
DATE: 2014-02-13
URL: http://www.lefigaro.fr/livres/2014/02/13/03005-20140213ARTFIG00398-stephen-king-publiera-deux-romans-en-2014.php
PRINCIPAL: 0
TEXT:
Publié
le 13/02/2014 à 19:30
L'auteur américain publiera deux livres en 2014, alors que son dernier roman, Doctor Sleep, est sorti à l'automne dernier. Crédits photo : Francois Mori/ASSOCIATED PRESS
Le maître du suspense publiera un second ouvrage à l'automne, après un premier livre attendu cet été. Il y retrouvera son genre de prédilection, l'horreur.
Publicité
Insatiable Stephen King . Son dernier livre fait frissonner les lecteurs depuis quelques mois tout juste , et voilà qu'il leur met une nouvelle fois l'eau à la bouche. Pas un, mais deux romans du maître de l'angoisse rejoindront les librairies, cette année. Fidèle à son rythme effréné, l'auteur devrait publier un livre en novembre, d'après son site officiel , alors qu'un premier ouvrage est déjà attendu.
Intitulé Revival , le roman racontera le parcours de Jamie Morton, un jeune garçon originaire d'une petite ville du nord de l'Angleterre. Tout bascule lorsque le révérend Charles Jacobs arrive dans la ville et révolutionne le quotidien. La relation entre Jamie et le révérend, devenue privilégiée en raison d'une obsession secrète de l'homme d'église, prend fin lorsque le malheur frappe la famille Morton. Devenu adulte, Jamie n'a dans sa vie que sa guitare et l'héroïne qu'il s'injecte dans le bras. Le hasard veut qu'il rencontre à nouveau Charles Jacobs. Des retrouvailles lourdes de conséquences.
Dans ce roman, l'auteur de Carrie retrouve le style qui a enchanté son public. Le livre relève de «l'essence» de l'auteur, selon son porte-parole cité par le quotidien britannique The Guardian . Angoisse, horreur et relations psychologiques complexes devraient donc être au menu, pour le plus grand plaisir des fans.
Un premier polar noir
La couverture originale du prochain roman de Stephen King. Crédits photo : Stephen King/Scribner
Avec Stephen King, tous les goûts peuvent néanmoins être satisfaits. À ceux n'ayant pas franchement d'attirance pour les ouvrages horrifiques, le premier ouvrage de l'auteur en 2014 pourrait mieux convenir. Mr. Mercedes, annoncé depuis déjà plusieurs mois, devrait être plus proche du thriller que de l'horreur et s'éloigner ainsi des sentiers dont Stephen King a l'habitude. Il est présenté par l'auteur comme «son premier livre sur un détective dur à cuire», d'après le Guardian.
Dans Mr. Mercedes , Stephen King raconte le retour à l'activité du détective Bill Hodges, retraité - et déprimé - depuis une affaire de meurtre collectif particulièrement violent non résolue. Il se remet en selle lorsqu'il reçoit une lettre de celui se présentant comme le tueur, annonçant une entreprise encore plus atroce. Une «guerre entre le bien et le mal» doit donc être menée par ce policier désabusé, le seul à pouvoir arrêter le criminel.
Le dernier roman de Stephen King, Doctor Sleep , a été publié le 30 octobre en France. Il s'agit de la suite de son célèbre The Shining , adapté au cinéma par Stanley Kubrick . La même année, il a également publié Joyland, un roman plus court. Le maître du suspense s'habitue donc aux duos annuels et ne semble pas prêt de reposer son angoissante plume.
