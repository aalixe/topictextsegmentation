TITRE: Tarzan de David Yates avec Alexander Skarsgard : une date de sortie
DATE: 2014-02-13
URL: http://www.avoir-alire.com/tarzan-de-david-yates-avec-alexander-skarsgard-une-date-de-sortie
PRINCIPAL: 170584
TEXT:
Envoyer � un ami
Trente ans apr�s Greystoke, Warner reprend le mythe de Tarzan � sa source...
Trente ans apr�s Greystoke, la l�gende de la jungle d�Hugh Hudson qui r�v�lait Christophe Lambert et Andie McDowell et 15 ans apr�s l�adaptation d�Edgar Rice Burroughs par Disney dans l�animation, un nouveau gros budget vient de se lib�rer pour Tarzan. Le film sera r�alis� par David Yates (r�alisateur des 4 derniers Harry Potter) et sera, tout comme Greystoke en son temps, distribu� par Warner Bros le 1er juillet 2016 aux USA.
La star de True Blood, Alexander Skarsgard, devrait apporter ses charmes physiques � un r�le d�shabill�, face � Margot Robbie en Jane (la magnifique �pouse de DiCaprio dans Le loup de Wall Street). Christoph Waltz et son comp�re Samuel L. Jackson seront de l�aventure.
