TITRE: PSG : Ibrahimovic "apte" pour Valenciennes - Ligue 1 - Football - Sport.fr
DATE: 2014-02-13
URL: http://www.sport.fr/football/ligue-1-psg-ibrahimovic-apte-pour-valenciennes-339845.shtm
PRINCIPAL: 170972
TEXT:
PSG : Ibrahimovic "apte" pour Valenciennes
Vendredi 14 f�vrier 2014 - 14:38
Touch� au dos apr�s la rencontre � Monaco (1-1) dimanche soir en cl�ture de la 24e journ�e de Ligue 1 , Zlatan Ibrahimovic s?est entra�n� normalement ce mercredi matin, comme l?a d�clar� Laurent Blanc en conf�rence de presse. L?entra�neur parisien a assur� que le Su�dois �tait "apte � jouer" pour la r�ception de Valenciennes, vendredi soir (20h30) en ouverture de la 25e journ�e de Ligue 1. "On va le revoir rapidement, tous les signes sont bons" a �galement signal� Blanc � propos d?Edinson Cavani.
