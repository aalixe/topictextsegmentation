TITRE: Patrick Buisson va porter plainte contre �Le Point� - Lib�ration
DATE: 2014-02-13
URL: http://www.liberation.fr/politiques/2014/02/12/patrick-buisson-va-porter-plainte-contre-le-point_979815
PRINCIPAL: 169207
TEXT:
Patrick Buisson , accus� par Le Point d�avoir enregistr� certaines de ses conversations avec l�ancien pr�sident Nicolas Sarkozy , indique mercredi dans un communiqu� qu�il va porter plainte contre l�hebdomadaire.
�J�ai demand� aujourd�hui � mon avocat, Me Gilles-William Goldnadel, de porter plainte contre Le Point � l�encontre d�un article ignominieux publi� ce jour�, �crit Patrick Buisson.�L�ancien conseiller de Nicolas Sarkozy �proteste fermement contre les basses accusations contenues dans cet article�.
�D�autre part, je suis pour le moins surpris de la co�ncidence de dates entre la parution de cet article et le fait que j�ai obtenu de la Cour d�Appel de Paris qu�elle juge recevable, contre l�avis du Parquet, la plainte que j�ai d�pos�e contre Mme Taubira, es qualit� de Ministre de la Justice, pour prise ill�gale d�int�r�ts dans l�affaire Anticor �, affirme-t-il �galement.
A lire aussi La droite � l'�cole buissonni�re
Selon lepoint.fr et Le Point � para�tre jeudi, �� plusieurs reprises durant son quinquennat, l�ancien pr�sident de la R�publique a �t� enregistr� � son insu par Patrick Buisson�.
La cour d�appel de Paris a jug� recevable la plainte pour �prise ill�gale d�int�r�t� d�pos�e le 13 novembre dernier par Patrick Buisson, en marge de l�affaire des sondages de l�Elys�e.�L�ex-conseiller de Nicolas Sarkozy reproche � la garde des Sceaux d�avoir publiquement apport� son soutien � l�action judiciaire intent�e par l�association Anticor � son encontre.
