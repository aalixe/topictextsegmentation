TITRE: � ��Les Inconnus�� r�agissent aux critiques Stars Actu
DATE: 2014-02-13
URL: http://www.stars-actu.fr/2014/02/les-inconnus-reagissent-aux-critiques/
PRINCIPAL: 0
TEXT:
Tweeter
� Les Trois Fr�res, le Retour �, le nouveau film des Inconnus, est sorti hier sur les �crans fran�ais. Un retour salu�, en tout cas pour le moment, par les spectateurs qui ont plac� le film en t�te du box-office 1er jour .
Oui mais si le public a semble t-il r�pondu pr�sent, la presse elle s�est d�cha�n�e contre le film. Et certains n�y sont vraiment pas all�s de main morte.
Quand le JDD �voque de vieux gags et une r�alisation paresseuse, Metro se demande si nos inconnus n�ont tout simplement pas d�pass� la date limite de consommation.
Pas mieux du c�t� d�Ecran Large pour qui Campan, Bourdon et L�gitimus semblent �tre devenus des caricatures d�eux-m�mes. Et pour T�l�Cin�Obs, ce film n�est que tristesse et frustration en barres.
Des critiques particuli�rement virulentes donc et qui n�ont pas laiss� insensible les 3 com�diens. Invit�s mercredi soir de l��mission ��C � vous�� sur France 5, ils ont chacun r�agi � leur mani�re.
A commencer par Pascal L�gitimus pour qui ces critiques ont peut-�tre quelque chose de bon.
��C�est bon signe  On ne peut pas plaire � tout le monde, d�une part. Et puis, ils n�ont pas le m�me regard que le peuple�� a t-il notamment d�clar�.
Un peu plus agac�, Didier Bourdon a l�ch� �  C�est toujours un peu chiant  � avant d��voquer les critiques dont avait fait l�objet le premier volet lors de sa sortie en salles. Des critiques qui, selon lui, n��taient curieusement plus les m�mes lors de sa premi�re diffusion sur Canal +.
Histoire d�en rajouter une petite couche, il a ensuite l�ch� ��Si on est toujours aussi regard� c�est qu�on ne se fout pas de la gueule du monde��. Et d�esp�rer que ces critiques n�atteignent pas certaines personnes.
Bernard Campan s�est montr� plus mesur� en d�clarant ��Ils ne se rendent pas compte. C�est une suite � un film qui a march�, donc tout est facile, y�a de l�argent, etc� mais ce n�est pas vrai, ce n�est pas la v�rit�.��
Bref, et peu importe les critiques, c�est encore une fois le public qui aura le dernier mot et qui fera ou pas de ce film l�un des champions du box-office en 2014.
Autres articles
