TITRE: VIDÉO. De quoi avez-vous rêvé cette nuit ? | Site mobile Le Point
DATE: 2014-02-13
URL: http://www.lepoint.fr/editos-du-point/anne-jeanblanc/de-quoi-avez-vous-reve-cette-nuit-13-02-2014-1791268_57.php
PRINCIPAL: 0
TEXT:
13/02/14 à 12h22
VIDÉO. De quoi avez-vous rêvé cette nuit ?
Pourquoi certaines personnes se souviennent-elles systématiquement de leurs rêves, et d'autres non ? Le mystère vient d'être levé par une équipe lyonnaise.
Le cerveau endormi n'est pas capable de mémoriser une nouvelle information. Martin Lee / Rex Featur/REX / SIPA
Par Anne Jeanblanc
Certaines personnes se souviennent de leurs rêves tous les matins et peuvent les raconter, voire les analyser ou les faire interpréter, alors que d'autres ne les mémorisent que rarement. L'équipe de Perrine Ruby*, chargée de recherche Inserm à Lyon, a étudié l'activité cérébrale des personnes en train de dormir, afin de comprendre ce qui différencie ces individus.
En janvier 2013 Perrine Ruby et ses collaborateurs avaient déjà publié dans la revue spécialisée Cérébral Cortex deux constats majeurs : les "grands rêveurs" comptabilisent deux fois plus de phases de réveil pendant le sommeil que les "petits rêveurs" et leur cerveau est plus réactif aux stimuli de l'environnement. Cette sensibilité expliquerait une augmentation des éveils au cours de la nuit et permettrait ainsi une meilleure mémorisation des rêves lors de cette brève phase d'éveil.
Dans une nouvelle étude publiée dans la revue Neuropsychopharmacology, l'équipe de scientifiques a cherché à identifier les régions du cerveau qui différencient les grands et les petits rêveurs. Pour cela, elle a mesuré l'activité cérébrale spontanée en tomographie par émission de positons (TEP) à l'éveil et pendant le sommeil chez 41 volontaires. Ces derniers ont été classés en deux groupes : 21 "grands rêveurs", qui se souvenaient de leurs songes moyenne 5,2 fois par semaine et 20 "petits rêveurs" capables de raconter seulement deux rêves par mois en moyenne.
La clef des songes
Selon les résultats obtenus avec cet examen, les grands rêveurs ont une activité cérébrale spontanée plus forte pendant leur sommeil au niveau du cortex préfrontal médian et de la jonction temporo-pariétale, une zone cérébrale impliquée dans l'orientation de l'attention vers les stimuli extérieurs. "Cela explique pourquoi les grands rêveurs réagissent plus aux stimuli de l'environnement et se réveillent plus au cours de leur sommeil que les petits rêveurs, et ainsi pourquoi ils mémorisent mieux leurs rêves", explique Perrine Ruby. "En effet le cerveau endormi n'est pas capable de mémoriser une nouvelle information, il a besoin de se réveiller pour pouvoir le faire."
Le neuropsychologue sud-africain Mark Solms avait déjà remarqué, dans de précédents travaux, que l'apparition de lésions sur ces deux zones conduisaient à une cessation des souvenirs de rêves. "Ces résultats montrent que les grands et petits rêveurs se différencient en terme de mémorisation du rêve, mais cela n'exclut pas qu'ils se différencient également en termes de production de rêve. En effet, il est possible que les grands rêveurs en fabriquent une plus grande quantité", conclut l'équipe de recherche. Bref, les songes n'ont pas encore totalement livré leur clé.
REGARDEZ ces images :
* U1028 Centre de recherche en neurosciences de Lyon (Inserm / CNRS / Université Claude Bernard Lyon 1) - équipe "dynamique cérébrale et cognition"
