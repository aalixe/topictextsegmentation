TITRE: Venezuela : deux morts lors des manifestations � Caracas - RTL.fr
DATE: 2014-02-13
URL: http://www.rtl.fr/actualites/info/international/article/venezuela-deux-morts-lors-des-manifestations-a-caracas-7769683535
PRINCIPAL: 168917
TEXT:
Un militant pro-gouvernement et un �tudiant ont �t� tu�s ce mercredi � Caracas
Cr�dit : JUAN BARRETO / AFP
Un militant pro-gouvernement et un �tudiant ont �t� tu�s par balles mercredi en marge d'une manifestation d'opposants et d'�tudiants � Caracas.
La responsable du minist�re public v�n�zu�lien Luisa  Ortega Diaz a d�plor� deux morts en marge d'une manifestation d'opposants et d'�tudiants � Caracas, le membre  d'un collectif (pro-gouvernement) Juan Montoya, tu� par balles, et  (l'�tudiant) Bassil Dacosta, �galement tu� par balles". Elle fait en outre �tat de 23 bless�s dans les  manifestations organis�es dans plusieurs villes du pays.
De son  c�t�, le ministre de l'Int�rieur Miguel Rodriguez a pr�cis� qu'un total  de 30 personnes avaient �t� arr�t�es. "Ils avaient tous des capuches,  des radios et avaient dans leurs sacs des cocktails molotov, des  pierres, tous types d'objets visant � agresser les policiers", a-t-il  affirm�.
Des �chauffour�es entre forces de l'ordre et manifestants
Auparavant, le pr�sident de l'Assembl�e nationale  avait annonc� la mort d'un militant pro-gouvernement �  Caracas. "Il a �t� vilement tu� par le fascisme", avait d�clar�  Diosdado Cabello, reprenant le terme habituellement utilis� par les membres du  gouvernement pour qualifier l'opposition.
Quelques minutes plus  t�t, lors de la dispersion de cette marche d'opposition au gouvernement  de Nicolas Maduro, qui a mobilis� des milliers de personnes dans le  centre-ville de la capitale. Un photographe a vu des hommes  circulant � moto tirer sur la foule, faisant au moins deux bless�s,  avant de prendre la fuite. Cette marche s'est �galement termin�e  par de br�ves �chauffour�es entre forces de l'ordre et manifestants, qui  ont lanc� des pierres sur les policiers.� En d�but de soir�e, de petits groupes continuaient de protester en divers points de la  ville, dispers�s �� et l� par les brigades anti-�meutes.
La politique �conomique en ligne de mire
Le  mouvement de protestation �tudiant, lanc� depuis une dizaine de jours en  province, vise � condamner la politique �conomique du pr�sident Nicolas  Maduro et l'ins�curit� croissante dans le pays. Lors d'une  pr�c�dente marche organis�e mardi dans la ville de M�rida (ouest), cinq  �tudiants avaient d�j� �t� bless�s par balles par des individus circulant � moto, selon plusieurs m�dias locaux et les syndicats  �tudiants.
Nous marchons parce que nous voulons que nos camarades emprisonn�s soient lib�r�s, mais aussi en raison de la situation du pays, de la d�t�rioration de l'�conomie.
Daniela Mu�oz, �tudiante en m�decine.
Mercredi, les manifestants exigeaient aussi la lib�ration imm�diate d'une dizaine d'�tudiants incarc�r�s ces derniers jours. "Nous  marchons parce que nous voulons que nos camarades emprisonn�s soient  lib�r�s, mais aussi en raison de la situation du pays, de la  d�t�rioration de l'�conomie, des p�nuries qui nous rendent malades et de  l'ins�curit�", a d�clar� Daniela Mu�oz, �tudiante en m�decine. Inflation,  p�nuries, ins�curit�: le pays p�trolier traverse actuellement une zone  de turbulences que peine � contenir le successeur de Hugo Chavez  (1999-2013).
La r�daction vous recommande
