TITRE: Belgique: la loi sur l'euthanasie des mineurs devrait �tre adopt�e - Monde - Actualit�s - La C�te - Journal r�gional l�manique
DATE: 2014-02-13
URL: http://www.lacote.ch/fr/monde/belgique-la-loi-sur-l-euthanasie-des-mineurs-devrait-etre-adoptee-604-1261324
PRINCIPAL: 168860
TEXT:
Belgique: la loi sur l'euthanasie des mineurs devrait �tre adopt�e
Belgique
Le Parlement belge devrait vraisemblablement adopter le texte de loi concernant l'euthanasie des mineurs d�s jeudi.
Cr�dit: KEYSTONE
Tous les commentaires (0)
Sauf �norme surprise, le texte de loi concernant l'euthanasie des mineurs devrait �tre adopt�e en Belgique jeudi. Les partisans soulignent que le projet renforce les droits des enfants et les opposants d�noncent une d�cision "pr�cipit�e". Les d�put�s belges ont d�battu une derni�re fois au parlement.
"Il n'est pas question d'imposer l'euthanasie � qui que ce soit, � aucun enfant, � aucune famille, mais de permettre le choix de l'enfant de ne pas s'�terniser dans la souffrance", a affirm� la d�put�e socialiste Karine Lalieux.
"Le droit d'aborder la vie et la mort ne peut �tre r�serv� aux adultes", a ajout� le d�put� lib�ral Daniel Bacquelaine. Depuis 2002, l'euthanasie est autoris�e en Belgique pour les adultes, "mais elle existe d�j� pour les enfants dans la pratique".
Pas d'�ge minimum
Les partisans du texte ont soulign� les "conditions strictes" pr�vues par la loi: le mineur devra se "trouver dans une situation m�dicale sans issue entra�nant le d�c�s � br�ve �ch�ance", �tre confront� � une "souffrance physique constante et insupportable qui ne peut �tre apais�e et qui r�sulte d'une affection accidentelle ou pathologique grave et incurable".
Le l�gislateur belge a choisi de ne pas imposer d'�ge minimum, contrairement aux Pays-Bas, o� il est de douze ans, pour lui pr�f�rer la notion de "capacit� de discernement". Cette capacit� de l'enfant � comprendre le "c�t� irr�versible de la mort" sera estim�e au cas par cas par l'�quipe m�dicale et par un psychiatre ou un psychologue ind�pendant. Les parents devront donner leur consentement.
"Un enfant de sept, huit ou neuf ans peut-il vraiment demander une euthanasie en toute autonomie?", a r�pondu la d�put�e chr�tienne-d�mocrate Sonja Becq, dont le groupe votera contre la proposition.
D�bat serein
Elle a �galement d�nonc� "les lacunes d'une loi" pr�par�e "dans la pr�cipitation" et qui, selon elle, ne pr�cise pas suffisamment la notion de "capacit� de discernement" ou ne pr�voit pas le cas o� les parents ne seraient pas d'accord entre eux.
Le d�bat, ouvert depuis un an, s'est d�roul� dans une grande s�r�nit� malgr� l'opposition de l'Eglise catholique et les divergences de vues dans le monde m�dical. Selon un sondage paru en octobre, 73% des Belges sont favorables � la l�galisation de l'euthanasie pour les mineurs.
Source: ATS
