TITRE: Décision le 19 mars sur la condamnation de Jérôme Kerviel | Site mobile Le Point
DATE: 2014-02-13
URL: http://www.lepoint.fr/fil-info-reuters/decision-le-19-mars-sur-la-condamnation-de-jerome-kerviel-13-02-2014-1791314_240.php
PRINCIPAL: 169888
TEXT:
13/02/14 à 11h58
Décision le 19 mars sur la condamnation de Jérôme Kerviel
PARIS ( Reuters ) - La Cour de cassation se prononcera le 19 mars sur le pourvoi de Jérôme Kerviel, condamné à trois ans de prison ferme et 4,9 milliards d'euros de dommages et intérêts pour une perte record en 2008 à la Société Générale.
L'ancien trader ne s'est pas présenté jeudi devant la chambre criminelle de la Cour, qui a examiné son recours.
L'avocat général a demandé le rejet de sa demande.
Jérôme Kerviel a entamé une véritable guérilla judiciaire contre sa condamnation en appel, en octobre 2012.
Outre son pourvoi en cassation, il a porté plainte contre la Société générale pour escroquerie au jugement et faux et usage de faux. Il conteste également devant le Conseil des prud'hommes de Paris son licenciement pour faute lourde.
Condamné pour abus de confiance, faux et usage de faux et introduction frauduleuse de données dans un système informatique, il a toujours soutenu que la Société Générale savait qu'il prenait des positions vertigineuses non couvertes.
Lors de son procès en appel, il a même accusé la banque d'avoir utilisé ces pertes pour en masquer d'autres liées aux "subprimes" - produits financiers liés aux crédits à risque américains.
Mais dans son arrêt, la Cour d'appel de Paris a balayé avec sévérité cette ligne de défense, qualifiant le trader d'"unique concepteur, initiateur et réalisateur du système de fraude ayant provoqué les dommages causés."
Jérôme Kerviel a reçu le soutien de plusieurs personnalités de gauche parmi lesquelles le co-président du Parti de gauche Jean-Luc Mélenchon.
Dernière en date, l'ex-candidate écologiste à la présidentielle et ancienne juge d'instruction Eva Joly a considéré mercredi qu'il avait été "condamné à la mort civile" alors que des questions "restent en suspens."
"Il est difficile d'imaginer que l'ancien trader junior soit le seul responsable et que la banque ignorait ce à quoi il se livrait", a-t-elle dit dans un entretien aux Inrocks. "Or le minimum pour savoir quel était le degré de connaissance de la Société Générale n'a pas été fait."
Jérôme Kerviel n'a eu de cesse depuis sa mise en cause de demander une expertise indépendante sur la perte subie par la Société générale.
Selon lui, une étude comptable et financière permettrait de démontrer que la banque a gonflé ses pertes lorsqu'elle a débouclé ses positions de 50 milliards d'euros.
Chine Labbé, édité par Yves Clarisse
