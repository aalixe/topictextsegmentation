TITRE: Les Experts sur TF1 flinguent la concurrence - telestar.fr
DATE: 2014-02-13
URL: http://www.telestar.fr/2014/02/les-experts-sur-tf1-flinguent-la-concurrence/121934
PRINCIPAL: 169639
TEXT:
Les Experts sur TF1 flinguent la concurrence
Par Guillaume BOTTON - Le 13 f�vrier 2014 �09h43
Partagez
Tweeter
Commentez
S'ils sont en perte de vitesse, Les Experts continuent de faire la course en t�te des audiences. Hier soir, le premier �pisode sur TF1 a r�uni 5 927 00 personnes (22,9% PDM) et le deuxi�me 5 150 000 (22,6% PDM).
M6 a plut�t bien march� gr�ce au prime de Sc�nes de m�nages. La s�rie a r�uni 3 924 000 personnes, soit 16,7 % de PDM.
France 2 grimpe sur la troisi�me marche du podium avec le t�l�film La d�esse aux 100 bras, avec Cristiana Reali. 2 605 000 t�l�spectateurs ont �t� s�duits par la fiction, soit 10,3 % de part de march�
Le Football sur France 3 - un huiti�me de finale de Coupe de France entre Nice et Monaco (0-1)- a f�d�r� 2 037 000 t�l�spectateurs, soit 9,3 % de part de march�
A noter le bon score, sur la TNT, du prime de Sans aucun doute (TMC) qui a r�uni 901 000 fans (4% PDM)
Partagez
