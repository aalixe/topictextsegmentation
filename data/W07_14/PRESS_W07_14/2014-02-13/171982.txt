TITRE: Kowalczyk plus forte que les Norv�giennes - JO 2014 - Ski - Sport.fr
DATE: 2014-02-13
URL: http://www.sport.fr/ski/kowalczyk-plus-forte-que-les-norvegiennes-339793.shtm
PRINCIPAL: 171981
TEXT:
Kowalczyk plus forte que les Norv�giennes
Jeudi 13 f�vrier 2014 - 17:35
La Polonaise Justyna Kowalczyk a domin� ses rivales scandinaves pour remporter le 10 km style classique des jeux Olympiques 2014 disput� sous des conditions printanni�res, jeudi.
10 km classique
1. Justyna Kowalczyk (POL) 28:17.8
2. Charlotte Kalla (SWE) � +18.4
3. Therese Johaug (NOR) +28.3
4. Aino-Kaisa Saarinen (FIN) +30.3
5. Marit Bj�rgen (NOR) +33.4
6. Stefanie Boehler (GER) +46.5
7. Natalia Zhukova (RUS) +57.7
8. Kerttu Niskanen (FIN) +58.9
9. Heidi Weng (NOR) +1:10.4
10. Krista Lahteenmaki (FIN) +1:18.2
Justyna Kowalczyk s'est impos�e avec un chrono de 28 min 17 sec 8/10e, devant la Su�doise Charlotte Kalla , 2e � 18 sec 4/10e, et la Norv�gienne Therese Johaug , 3e � 28 sec 3/10e.
La Norv�gienne Marit Bj�rgen , favorite de l'�preuve avec Kowalczyk, a termin� � la 5e place � 33 sec 4/10e et n'a jamais �t� en mesure d'inqui�ter sa grande rivale polonaise, apr�s son succ�s en skiathlon samedi.
Kowalczyk, qui souffre d'une triple micro-fracture � un pied depuis une course le mois dernier en Pologne, s'est donc bien remise de sa d�ception du skiathlon, o� elle n'avait pu prendre que la 6e place.
La Polonaise, arriv�e volontairement aux Jeux sans conna�tre la nature exacte de sa blessure afin de ne pas perdre le moral, avait finalement fait des tests apr�s la course de dimanche, puis publi� en d�but de semaine une photo de sa radio du pied sur sa page Facebook .
La Polonaise compte d�sormais � son palmar�s deux titres olympiques apr�s son sacre sur 30 km classique, son style de pr�dilection, � Vancouver en 2010.
Pour cette �preuve qui s'est d�roul�e sous une chaleur inhabituelle, plusieurs concurrentes avaient choisi de courir bras nus.
Dossiers associ�s
