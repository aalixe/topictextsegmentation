TITRE: ETATS-UNIS: Attentat de Boston: proc�s de Tsarnaev le 3 novembre -   Monde: Am�riques - lematin.ch
DATE: 2014-02-13
URL: http://www.lematin.ch/monde/ameriques/attentats-boston-proces-tsarnaev-3-novembre/story/11619316
PRINCIPAL: 169349
TEXT:
Votre adresse e-mail*
Votre email a �t� envoy�.
Un juge f�d�ral am�ricain a fix� au 3 novembre le proc�s de l'accus� de l'attentat du marathon de Boston, Djokhar Tsarnaev, passant outre � la demande de la d�fense d'avoir plus de temps.L'accus�, �g� de 20 ans, encourt la peine de mort.
L'attaque avait fait trois morts et 264 bless�s le 15 avril dernier, quand deux bombes artisanales plac�es dans des cocottes-minute avaient explos� quasi simultan�ment dans la foule, pr�s de la ligne d'arriv�e de la course tr�s populaire � laquelle participaient des milliers de coureurs.
Le juge a annonc� mercredi la date du proc�s lors d'une audience technique au tribunal de Boston, � laquelle l'accus� n'assistait pas. La d�fense souhaitait que le proc�s ne commence pas avant septembre 2015, mettant en avant l'�norme quantit� de pi�ces au dossier. Selon le juge, le proc�s devrait durer plusieurs mois.
Plaider non coupable
Djokhar Tsarnev, musulman d'origine tch�tch�ne, naturalis� am�ricain en 2012 et qui vivait depuis 10 ans � Boston, a plaid� non coupable le 10 juillet. Il est accus� d'avoir agi avec son fr�re a�n�, Tamerlan, 26 ans, le plus radical des deux, abattu apr�s une course-poursuite avec la police trois jours apr�s l'attentat.
A l'issue d'une vaste chasse � l'homme, Djokhar avait �t� arr�t� cach� dans un bateau dans un jardin en banlieue de Boston, gri�vement bless�.
Djokhar Tsarnaev est inculp� notamment d'utilisation d'arme de destruction massive ayant entra�n� la mort, d'attentat dans un espace public et d'utilisation d'une arme � feu, et risque la peine de mort pour 17 des 30 chefs d'inculpation retenus contre lui.
S'agissant d'un cas f�d�ral, le ministre am�ricain de la justice Eric Holder a en effet indiqu� le 30 janvier qu'il allait demander la peine capitale. (ats/Newsnet)
Cr��: 13.02.2014, 02h11
