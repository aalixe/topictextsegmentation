TITRE: Le vote blanc d�finitivement adopt� par le�Parlement� mais pour apr�s les municipales | Courrier des maires
DATE: 2014-02-13
URL: http://www.courrierdesmaires.fr/30966/le-vote-blanc-definitivement-adopte-par-le-parlement-mais-pour-apres-les-municipales/
PRINCIPAL: 170137
TEXT:
Le vote blanc d�finitivement adopt� par le�Parlement� mais pour apr�s les municipales
par Aur�lien H�lias
Le S�nat a adopt� en deuxi�me lecture le 12 f�vrier la proposition de loi visant � reconna�tre le vote blanc aux �lections. Mais les bulletins blancs ne seront pas compt�s parmi les suffrages exprim�s et la reconnaissance de ce vote blanc n�interviendra qu�en mai�2014, apr�s les municipales de mars.
1
� Dorange-CCommons
Apr�s l�Assembl�e le 28 novembre 2013, le S�nat a d�finitivement adopt� en deuxi�me lecture, dans le cadre d�un ordre du jour r�serv� au groupe UC-UDI, la proposition de loi visant � reconna�tre le vote blanc aux �lections sur le rapport de Fran�ois Zocchetto (UDI-UC).
Le texte, d�pos� d�s juillet 2012 par Fran�ois Sauvadet (UDI) � l�Assembl�e Nationale, pr�voit qu�� partir du 1er�avril 2014, les bulletins nuls seront bien d�compt�s, mais s�par�ment des bulletins blancs. Une limite qu�a regrett�e notamment Fran�ois Fortassin (RDSE)�: � Des membres de mon groupe regrettent qu�on n�int�gre pas le vote blanc dans les suffrages exprim�s.��
Un vote blanc possible pour les europ�ennes
Autre restriction de taille, du moins � court terme�: la proc�dure ne s�appliquera qu�� partir de mai prochain. De quoi �viter ainsi le d�compte des votes blancs aux prochaines municipales ��et de quantifier ainsi les d��us du gouvernement, et plus largement de la classe politique, le faisant savoir dans les urnes municipales�� ont analys� plusieurs responsables politiques. Une ��pr�caution�� que les parlementaires n�ont pas jug�e n�cessaire de prendre pour le scrutin suivant, celui des �lections de mai pour le Parlement europ�en. �
En outre, ce vote blanc ne sera reconnu ni aux �lections pr�sidentielles, ni aux r�f�rendums locaux, pour lesquels il aurait fallu une loi organique.
L�expression d�une ��une attente non satisfaite��
Restent que de nombreux parlementaires ont soulign� l�avanc�e que repr�sentait cette reconnaissance d�un acte civique, jusqu�alors confondu avec l�indiff�rence des �lecteurs ne se d�pla�ant pas aux urnes. Car les bulletins blancs seront, � partir de mai 2014, d�compt�s s�par�ment des bulletins nuls. Une enveloppe ne contenant aucun bulletin sera assimil�e � un bulletin blanc.
���Si l�abstention peut g�n�ralement �tre comprise comme une marque de d�sint�r�t pour la vie politique, le vote blanc doit �tre vu comme une attente non satisfaite qui peut traduire une forme d�esp�rance. C�est un choix tout aussi respectable que les autres��, a ainsi soulign� Philippe Kaltenbach (PS).
���Le vote blanc n�est pas qu�une vague fantaisie mais un thermom�tre de la d�mocratie��,� a exhort� Pierre Charon pour l�UMP. ��Mieux vaut voter blanc que bleu marine���, a pour sa part lanc� l��cologiste H�l�ne Lipietz.
Respecter les candidats
Yves D�traigne a de son c�t� soulign� l�int�r�t de cette reconnaissance� pour les candidats eux-m�mes�: ��Voter blanc, ce n�est pas se moquer des candidats � l��lection. �
Le texte �tait �galement soutenu par le gouvernement�: ��La reconnaissance du vote blanc est intimement li�e � la notion de d�mocratie repr�sentative��, a appuy� Alain Vidalies, ministre des Relations avec le Parlement. Ce dernier aurait pr�f�r� une application d�s les municipales de mars�2014, assure-t-il, mais a fait valoir ��des probl�mes pratiques, d�ordre informatique notamment��, pour justifier l�impossibilit� d�une application rapide.
R�f�rences
