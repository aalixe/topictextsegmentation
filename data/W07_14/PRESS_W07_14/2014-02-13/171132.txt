TITRE: Programmes TV - Bones : un mariage sem� d'emb�ches -                     T�l�vision - Le Figaro T�l�
DATE: 2014-02-13
URL: http://tvmag.lefigaro.fr/programme-tv/article/television/79776/bones-un-mariage-seme-d-embuches.html
PRINCIPAL: 171126
TEXT:
� voir sur le web�
Diffus� la veille de la Saint-Valentin, l'�pisode 6 de la saison 9 de Bones est � marquer d'une pierre blanche pour les fans de la s�rie. L'anthropologue judiciaire Brennan �pouse enfin l'agent du FBI Booth.
Tout commence par les r�p�titions et les ultimes pr�paratifs de la c�r�monie. Temperance Brennan, alias Bones , l'ultra-cart�sienne, a du mal � laisser l'�motion prendre le dessus et insiste pour travailler sur une enqu�te en cours.
Confuse, elle tente de se raccrocher au monde rationnel. C'est amusant de la voir progressivement �tre submerg�e par ses sentiments. Tout ne se passe pas comme pr�vu et le mariage est dans un premier temps repouss�! Les diff�rents obstacles mettent du piment dans l'intrigue. Finalement, les noces ont bel et bien lieu. Loin d'�tre mi�vre et attendu, cet �pisode de mariage est sans doute le meilleur dans le genre, pour une s�rie.
Forts de leur exp�rience de neuf ans avec leur personnage, Emily Deschanel et David Boreanaz ont tenu � participer � l'�criture de leur discours pour l'�change des voeux. R�sultat: leurs textes respectifs sont dr�les, authentiques et les multiples r�f�rences faites aux �pisodes cl�s de la s�rie (comme celui o� Bones a failli �tre enterr�e vivante) sont une belle et �mouvante r�compense pour les aficionados de la premi�re heure. D�tail qui compte, Emily Deschanel a elle-m�me choisi sa robe de mari�e parmi une s�lection de cinq, imaginant ce que son personnage aurait choisi.
Cerise sur le g�teau, Cyndi Lauper interpr�te pour les mari�s une reprise d'At Last, d'Etta James. Il s'agit de la troisi�me apparition de la chanteuse dans Bones , dans la peau de la m�dium Avalon Harmonia. D'autres personnages font aussi leur retour pour l'occasion, comme Max (Ryan O'Neal), le p�re de Temperance, et le jeune Parker, le fils de Booth, n� d'un premier mariage.
Pour faire perdurer la magie de cette romantique ambiance, le prochain �pisode in�dit sera consacr� � la lune de miel de Bones et Booth � Buenos Aires. Avec leur petite fille, Christine (jou�e par deux jeunes actrices jumelles), le couple a de beaux jours devant eux. Au moins jusqu'� la saison 10, la s�rie vient en effet d'�tre renouvel�e par la Fox pour un an de plus.
� savoir
Diffus� le 23 octobre dernier sur la cha�ne Fox aux �tats-Unis, l'�pisode du mariage a fait grimper l'audience de Bones de plus de 5 %. Plus de huit millions d'Am�ricains ont suivi leurs noces, un beau record. La saison 9 est actuellement en cours de diffusion outre-Atlantique.
VOS REACTIONS
