TITRE: Google s'explique sur l'absence d'option pour couper le son dans Chrome | PCWorld.fr
DATE: 2014-02-13
URL: http://www.pcworld.fr/logiciels/actualites,google-s-explique-sur-l-absence-d-option-pour-couper-son-dans-chrome,546487,1.htm
PRINCIPAL: 171316
TEXT:
ENCYCLO
Google s'explique sur l'absence d'option pour couper le son dans Chrome
Le saviez-vous ? Les gens ne sont jamais contents, et m�me lorsque Google leur change la vie en leur offrant une pr�cieuse fonction d'identification des sites �mettant des fonds sonores, certains n'h�sitent pas � en demander encore un peu plus aux d�veloppeurs.
Tr�s bien accueillie par tous ceux qui ont un jour cherch� � identifier un site �metteur d'une musique insupportable, et perdu quelque part dans un de leurs 60 onglets, la derni�re version de Google Chrome affiche enfin un pr�cieux indicateur permettant de trouver le coupable , en moins de temps qu'il n'en faut pour le dire. Un ajout forc�ment essentiel, mais que certains critiquent d�j�, puisqu'il n'est pas possible de couper le son nativement, Google n'ayant pas pr�vu une telle option pour son navigateur.�
Pour se d�faire d'un site parasite, il n'y a donc qu'une solution : fermer l'onglet en question. L'explication de Google � ce sujet est en fait assez simple : � la suite d'un vaste d�bat sur la question, l'entreprise a d�cid� de ne pas inclure une telle fonction, afin de ne pas transformer Chrome en une sorte de gendarme du contenu. Pour Google, dans la mesure o� de nombreux sites enregistrent les actions de leurs visiteurs, le simple fait de fermer l'onglet devrait suffire � alerter les responsables sur le probl�me, et les pousser � le r�soudre, s'ils ne veulent pas voir tout le monde prendre la poudre d'escampette.�
N�anmoins, selon le sp�cialiste de Google, Fran�ois Beaufort, Mountain View envisagerait de d�velopper une nouvelle API audio pour Chrome, permettant alors aux extensions du navigateur d'interagir avec les flux sonores. Si tel �tait le cas, des fonctions permettant de mettre en pause ou de couper d�finitivement le son pourraient alors �tre d�velopp�es assez rapidement, ce qui devrait en ravir plus d'un.
Sur le m�me th�me
