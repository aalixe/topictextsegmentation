TITRE: EDF : EDF pr�voit de g�n�rer une tr�sorerie positive en 2018, infos et conseils valeur FR0010242511, EDF - Les Echos Bourse
DATE: 2014-02-13
URL: http://bourse.lesechos.fr/infos-conseils-boursiers/infos-conseils-valeurs/infos/edf-prevoit-un-pic-d-investissements-de-e14-mds-en-2015-950857.php
PRINCIPAL: 169778
TEXT:
EDF : EDF pr�voit de g�n�rer une tr�sorerie positive en 2018
13/02/14 � 10:59
L'objectif de cash-flow exclut le projet Linky, d'un co�t �valu� � E5 mds
Pr�voit un pic d'investissements � E14 mds en 2015, c. E15 mds pr�c�demment
R�sultats 2013 en hausse, les r�ductions de co�ts ont aid�
L'Etat n'envisage pas de cession de titres EDF-Premier ministre
L'action EDF parmi les plus fortes hausses du CAC 40   (Actualis� avec conf�rence de presse, r�action en Bourse)
Le communiqu� :
EDF pr�voit un pic dinvestissements de E14 mds en 2015 | Cr�dits photo : EDF - AMIET JODY
par Benjamin Mallet
PARIS, 13 f�vrier (Reuters) - EDF a publi� jeudi des r�sultats annuels en hausse, port�s par la France et ses r�ductions de co�ts, et a fait savoir qu'il pr�voyait de g�n�rer un flux de tr�sorerie (cash-flow) positif en 2018.
Confront� � un "mur" de d�penses dans son parc nucl�aire fran�ais et dans les r�seaux, l'�lectricien public a pr�cis� que ses investissements nets devraient atteindre un pic de 14 milliards d'euros en 2015, contre 15 milliards pr�vus pr�c�demment.
Son objectif de tr�sorerie exclut toutefois le projet de compteur �lectrique "intelligent" Linky, dont l'entr�e dans tous les foyers fran�ais de 2016 � 2020 a �t� annonc�e par le gouvernement d�but juillet et qui devrait co�ter pr�s de cinq milliards d'euros.
Alors que le groupe, dont l'Etat d�tient 84% du capital, est apparu ces derniers mois comme un candidat �vident � une cession de titres de la part du gouvernement, le Premier ministre Jean-Marc Ayrault a par ailleurs d�clar� jeudi qu'un tel projet n'�tait pas envisag�.
Ces annonces font progresser l'action EDF en Bourse, qui gagne 2,3% � 26,68 euros � 10h50, parmi les plus fortes hausses de l'indice CAC 40 (-0,19%).
"Les chiffres sont globalement sup�rieurs aux attentes. Cela fait plusieurs trimestres qu' EDF arrive � faire mieux qu'attendu, ce qui lui conf�re une certaine cr�dibilit�. Le dividende est �galement l�g�rement meilleur qu'attendu (..)", a comment� un g�rant parisien.
DIVIDENDE MAINTENU � 1,25 EURO
EDF a soulign� que son cash-flow op�rationnel de 13 milliards d'euros environ (+5,4%) lui avait permis en 2013 de couvrir ses investissements nets de 12,2 milliards, mais il a enregistr� un flux de tr�sorerie total n�gatif de 366 millions apr�s dividendes.
Le groupe propose en outre de maintenir son dividende � 1,25 euro par action, ce qui repr�sente un montant proche de 2,3 milliards, et pr�voit 13 � 13,5 milliards d'investissements nets cette ann�e.
"Nos investissements seront significativement r�duits (apr�s 2015) pour atteindre en 2018 un niveau comparable � celui de 2013", a d�clar� le PDG Henri Proglio, lors d'une conf�rence avec la presse et les analystes.
L'objectif de g�n�ration de tr�sorerie pour 2018 repose aussi sur la poursuite des efforts en mati�re de co�ts, qui se sont traduits par 1,3 milliard d'euros d'�conomies en 2013 sur un programme de plus de 2,5 milliards en 2015 par rapport � 2010.
Pour 2014, EDF table sur un r�sultat brut d'exploitation (Ebitda) en croissance organique d'au moins 3% en excluant sa filiale italienne Edison.
Par ailleurs, le groupe confirme que les mises en service du terminal m�thanier de Dunkerque et du r�acteur nucl�aire de type EPR de Flamanville (Manche) sont pr�vues respectivement en 2015 et 2016. Il pr�voit toujours de prendre un d�cision finale d'investissement cette ann�e sur le projet de construction de deux EPR � Hinkley Point, en Grande-Bretagne .
CLARIFICATIONS DE L'ETAT TOUJOURS ATTENDUES
En France, EDF attend en particulier de l'Etat des clarifications sur les tarifs de distribution (Turpe 4) et sur la m�thode de calcul de l'Arenh, le prix auquel il doit revendre � ses concurrents une partie de sa production d'�lectricit� d'origine nucl�aire.
Henri Proglio a r�affirm� qu'EDF souhaitait voir l'Arenh atteindre progressivement 50 euros par m�gawatt-heure, contre 42 euros aujourd'hui.
Le niveau de l'Arenh a �t� vivement contest� depuis la mise en place du syst�me en juillet 2011, les rivaux d'EDF le jugeant trop �lev� pour leur permettre de r�ellement concurrencer l'op�rateur historique tandis que les industriels critiquent un tarif qui gr�ve leur comp�titivit�.
EDF a enregistr� en 2013 un r�sultat net part du groupe de 3,5 milliards d'euros (+7,4%), un r�sultat net courant de 4,1 milliards (-1,4%), un Ebitda de 16,8 milliards (+4,8% dont +5,5% en organique) et un chiffre d'affaires de 75,6 milliards (+4,7%).
Selon le consensus Thomson Reuters I/B/E/S, les analystes attendaient en moyenne un r�sultat net part du groupe de 3,6 milliards, un Ebitda de 16,6 milliards et un chiffre d'affaires de 74,6 milliards.
L'endettement financier net d'EDF atteint 35,5 milliards d'euros � fin 2013, en baisse de 3,7 milliards par rapport � fin 2012, ce qui repr�sente une ratio d'endettement financier net sur Ebitda de 2,1 fois.
En France, le groupe a b�n�fici� d'une hausse des tarifs de 5% en ao�t mais sa production nucl�aire a recul� de 0,3% � 403,7 t�rawatts/heure (TWh), l'objectif pour 2014 �tant fix� � un niveau compris entre 410 et 415 TWh.
http://link.reuters.com/qeb86v (Avec Geert De Clercq et Alexandre Boksenbaum-Granier, �dit� par Dominique Rodriguez)
La p�dagogie en association avec
Retrouvez la d�finition de ce(s) mot(s) cl�(s) en Bourse avec notre lexique complet :
