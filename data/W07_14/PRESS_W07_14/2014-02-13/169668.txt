TITRE: Ronaldo �teint l'Atletico - Football - Sports.fr
DATE: 2014-02-13
URL: http://www.sports.fr/football/espagne/articles/ronaldo-punit-encore-l-atletico-1008726/?Top5football
PRINCIPAL: 169666
TEXT:
11 février 2014 � 23h19
Mis à jour le
12 février 2014 � 07h48
Condamné à l'exploit contre le Real Madrid après la défaite à l'aller (3-0), l'Atletico n'a pas espéré longtemps en demi-finale retour de la coupe d'Espagne. Le club merengue s'est imposé grâce à un doublé rapide de Cristiano Ronaldo sur penalty et accède en finale (0-2).
Même pas le temps d'y croire. Large vainqueur de la demi-finale aller de coupe d'Espagne (3-0), le Real Madrid n'a pas laissé à l'Atletico le moindre espoir de relancer le suspense. Le match retour a été plié par Cristiano Ronaldo dans les 16 premières minutes au stade Vicente -Calderon. Impuissants, les Colchoneros ont même facilité la tâche de la Maison Blanche en concédant deux penaltys stupides.
Au bout de six minutes de jeu, Manquillo déséquilibre le Ballon d'Or 2013 dans la surface et condamne déjà son équipe. Le Portugais ne tremble pas et transforme le penalty (0-1, 7e). Quelques instants plus tard, Raul Garcia offre le premier et dernier frisson à son public d'un puissant tir sur le poteau (11e). Dans la foulée Insua fauche grossièrement Bale dans la zone de vérité. Ronaldo en profite encore en frappant du même côté (0-2, 16e).
Sonné, l'Atletico ne s'en remettra pas. Ronaldo, encore lui, met Aranzubia à contribution (72e). Sosa réplique en sollicitant Casillas sur une demi-volée puissante (84e). Les partenaires de Varane, titulaire, gèrent la fin de match. Incapables de déborder le Real, les Colchoneros ne parviennent pas à sauver l'honneur. Les joueurs de Diego Simeone encaissent leur troisième défaite de suite toutes compétitions confondues. Surtout, la bande à Carlo Ancelotti prend un ascendant pour la Liga où les deux équipes sont à égalité avec Barcelone .
