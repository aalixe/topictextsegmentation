TITRE: La nouvelle h�ro�ne de ��How I met your Dad�� d�voil�e | Gossy
DATE: 2014-02-13
URL: http://www.gossymag.fr/la-nouvelle-heroine-de-how-i-met-your-dad-devoilee-art55806.html
PRINCIPAL: 0
TEXT:
Apr�s l�annonce du spin-off de la s�rie �� How I Met your Mother ��, appel� simplement �� How I Met your Dad ��, l�actrice qui jouera l�alter ego de Ted Mosby vient d��tre r�v�l�e.
��How I Met your Dad�� succ�dera � ��How I Met your Mother��
CBS l�a annonc� il y a quelques mois : ��How I Met your Mother�� s�ach�ve apr�s 9 ans d�aventure. Le dernier �pisode de la sitcom sera diffus� le 31 mars prochain aux �tats-Unis. La s�rie phare laissera place � sa petite s�ur ��How I Met your Dad��, son spin-off.
Les fans restent sceptiques quant � la diffusion d�un spin-off qui garderait le m�me concept que sa s�rie m�re. Le seul grand changement sera le point de vue de la femme, au lieu de celui de l�homme. Le h�ros de la s�rie sera donc un femme appel�e ��Sally��.
Greta Gerwig dans le r�le principal
Sally sera incarn�e par l�actrice am�ricaine Greta Gerwig, �g�e de 30 ans. L�information vient de tomber, la jeune femme a d�croch� le r�le principal pour remplacer Ted Mosby. Plus que l�h�ro�ne du spin-off, Greta Gerwig participera � la production et au sc�nario de la s�rie au c�t� de Carter Bays, Craig Thomas et Emily Spivey. Cette actrice qui a fait tr�s peu d�apparition � la t�l�vision est tout de m�me apparue dans ��Sex Friends��, ��Frances Ha�� et ��To Rome With Love��.
Greta Gerwig apportera la touche f�minine de la s�rie ��How I Met your Dad��. Dans le r�le de Sally, elle envisage un divorce avec son mari actuel et s�appuie sur le soutien et les conseils de son amie Juliet, son fr�re Danny, son ami gay et avocat et Franck, l�amoureux geek. La jeune femme racontera ses multiples m�saventures avant de d�voiler qui est le p�re de ses enfants. En esp�rant que cette fois-ci, l�h�ro�ne mette moins de 9 ans avant de r�v�ler le nom.
Pour s�assurer que le spin-off rencontre le succ�s escompt�, un �pisode pilote, actuellement en pr�paration sera diffus� sur CBS avant de lancer la premi�re saison de la s�rie.
Les r�actions � chaud
