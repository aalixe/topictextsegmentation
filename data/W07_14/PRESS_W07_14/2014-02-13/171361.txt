TITRE: Cristin Milioti de How I Met Your Mother dans le pilote produit par Rashida Jones - News s�rie TV
DATE: 2014-02-13
URL: http://series-tv.premiere.fr/News-Series/Cristin-Milioti-de-How-I-Met-Your-Mother-dans-le-pilote-produit-par-Rashida-Jones-3952576
PRINCIPAL: 171358
TEXT:
Cristin Milioti de How I Met Your Mother dans le pilote produit par Rashida Jones
13/02/2014 - 12h34
0
�
TVGuide vient de r�v�ler que l'actrice Cristin Milioti, la fameuse Mother de la s�rie How I Met Your Mother, vient de rejoindre le pilote de la com�die romantique produite par Rashida Jones.
De "HIMYM" � "A to Z". TVGuide nous apprend donc que Cristin Milioti vient de rejoindre le casting du pilote de la com�die produite par l'actrice Rashida Jones . Elle interpr�tera Zelda l'un des personnages principaux selon TVGuide.
A to Z suit sa relation avec Andrew, de leur rencontre jusqu'� leur rupture. Zelda est une jeune avocate, elle est la plus r�aliste dans sa relation et aime �tre une adulte. Elle ne comprend pas comment on peut encore regarder des dessins anim�s et manger des glaces apr�s la vingtaine pass�e. Tr�s terre-�-terre elle ne croit pas au destin et pense d�tenir les cl�s du sien.
En attendant de savoir si le pilote sera retenu par NBC, nous pouvons retrouver�Cristin Milioti dans le neuvi�me et ultime saison d'How I Met Your Mother. L'actrice � par ailleurs r�cemment d�clar� qu'elle m�me ne connaissait pas le nom de la "Mother".
