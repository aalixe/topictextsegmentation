TITRE: Quand Fran�ois Hollande tend la main aux entrepreneurs...
DATE: 2014-02-13
URL: http://www.boursier.com/actualites/economie/etats-unis-francois-hollande-tend-la-main-aux-entrepreneurs-23007.html?sitemap
PRINCIPAL: 0
TEXT:
Quand Fran�ois Hollande tend la main aux entrepreneurs...
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � Comme attendu, Fran�ois Hollande a tendu la main aux entrepreneurs fran�ais en annon�ant une s�rie de mesures visant � faciliter leur d�veloppement, lors d'un d�placement en Californie, aux Etats-Unis... Symbole de cette r�conciliation, le chef de l'Etat a �treint Carlos Diaz, le leader du mouvement des Pigeons, cr�e fin 2012 pour protester contre la politique fiscale du gouvernement. Fran�ois Hollande souhaite notamment faciliter le d�veloppement du financement participatif.
Simplifier les d�marches
Le chef de l'Etat a ainsi annonc� la possibilit� pour les particuliers de pr�ter, via les plates-formes de "crowdfunding", jusqu'� un million d'euros aux PME, sans qu'elles soient soumises aux contraintes de l'Autorit� des march�s financiers. Une ordonnance allant en ce sens sera sign�e en mars prochain. "Belle avanc�e", a soulign� le Pr�sident du P�le Entrepreneuriat & Croissance des TPE PME au Medef, Thibault Lanxade sur son compte Twitter.
Projet de r�glementation
Pour m�moire, � l'issue des assises du financement participatif qui se sont tenues en septembre dernier, la ministre du num�rique Fleur Pellerin - qui tiendra d'ailleurs une conf�rence de presse sur le sujet ce vendredi - avait annonc� un projet de r�glementation pour le secteur. La premi�re mouture pr�voyait que le plafond global du pr�t octroy� se situe � 300.000 euros par projet et 250 euros par investisseur. Certains gestionnaires de plateforme, � l'image de Benoit Bazzocchi cr�ateur de Smartangels.fr l'estimaient trop bas... "Vouloir le limiter n'a pas de sens�: plus une entreprise cherche des fonds de mani�re importante, plus elle est mature, et moins c'est risqu�", avait -il expliqu� dans une interview accord�e au 'Journal du Net' .
Parmi les autres mesures annonc�es par Fran�ois Hollande figure la cr�ation d"un "passeport talents" d'une dur�e de quatre ans renouvelables qui permettra d'attirer en France entre 5.000 et 10.000 chercheurs ou travailleurs hautement qualifi�s...
Marianne Davril � �2014, Boursier.com
