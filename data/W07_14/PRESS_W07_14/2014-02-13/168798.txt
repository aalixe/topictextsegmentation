TITRE: Le Parlement belge sur le point d'adopter l'euthanasie pour mineurs
DATE: 2014-02-13
URL: http://fr.radiovaticana.va/news/2014/02/12/le_parlement_belge_sur_le_point_dadopter_leuthanasie_pour_mineurs/fr1-772690
PRINCIPAL: 168795
TEXT:
Accueil �>� Culture et Soci�t� �>�derni�re mise � jour: 2014-02-12 18:48:26
Le Parlement belge sur le point d'adopter l'euthanasie pour mineurs
(RV) Un mois apr�s son adoption au S�nat, le texte de loi autorisant l'euthanasie pour les mineurs en Belgique devrait recueillir la majorit� des voix � la Chambre des repr�sentants. Le dernier obstacle parlementaire a �t� franchi le mardi 4 f�vrier, avec l'approbation de la commission de la Justice de la Chambre. Le texte sera pr�sent� en s�ance pl�ni�re jeudi, pour un vote pr�vu � 19h. En cas de oui, la Belgique serait alors, apr�s les Pays-Bas, le deuxi�me pays � l�galiser l�euthanasie pour les mineurs.
Malgr� la vive opposition de l��glise catholique et des autres confessions religieuses, le texte �tend le cadre l�gal autorisant l�euthanasie aux mineurs atteints d�une maladie incurable. Le texte pr�voit qu'un mineur peut demander � b�n�ficier de l'euthanasie s'il fait face � des souffrances physiques insupportables et inapaisables, en phase terminale. Il doit pour cela �tre conseill� par une �quipe m�dicale et recevoir l'accord parental. Les souffrances psychiques insupportables, qui ouvrent la voie � l'euthanasie pour les adultes, ont �t� �cart�es pour les mineurs par les s�nateurs.
Partagez
