TITRE: Match Lyon - Lens en direct
DATE: 2014-02-13
URL: http://www.topmercato.com/79583,1/match-lyon-lens-en-direct.html
PRINCIPAL: 171713
TEXT:
Bayern Munich - MU min. par min. Lire
Atletico - Bar�a : le match comment� Lire
Bourges invit� du dernier carr� Lire
Actu Sport.fr
�
Match Lyon - Lens en direct
A partir de 20h45 aujourd'hui, suivez le score de Lyon - Lens sur notre section " Match en Direct " ou ici m�me en bas de page. Sur notre application iPhone et Android , suivez le match dans l'onglet "Live / R�sultats".
Qualifi� pour la finale de la Coupe de la Ligue , l'Olympique Lyonnais veut �galement poursuivre son parcours en Coupe de France . Pour rejoindre les quarts de finale, l'�quipe rhodanienne doit prendre le meilleur sur le Racing Club de Lens, troisi�me de Ligue 2 . Faciles lors de leurs deux premiers tours contre La Suze et Yzeure, les Gones ont l'avantage du terrain. Rassur�s en championnat par les trois points ramen�s de Nantes (2-1), les hommes de R�mi Garde partent favoris. M�fiance tout de m�me � la r�action des Sang et Or d'Antoine Kombouar�, ralentis dans leur �lan � Troyes (1-0) en vue de la mont�e.
1
