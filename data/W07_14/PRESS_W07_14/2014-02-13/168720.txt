TITRE: L'�pid�mie de grippe s'installe : les r�gions les plus touch�es - Sciences - MYTF1News
DATE: 2014-02-13
URL: http://lci.tf1.fr/science/sante/l-epidemie-de-gastro-s-attenue-pour-laisser-place-a-la-grippe-8364738.html
PRINCIPAL: 0
TEXT:
grippe , gastro , sant�
Sant�Si la gastroent�rite semble conna�tre une petite d�crue depuis la semaine derni�re, la grippe est au contraire d�sormais bien install�e en France. Parmi les r�gions les plus touch�es par le virus se trouvent la r�gion Provence-Alpes-C�te d'Azur, le Languedoc-Roussillon et la r�gion Rh�ne-Alpes.
L'�pid�mie de grippe est d�sormais install�e en France m�tropolitaine o� 13 patients gripp�s sont d�c�d�s depuis le 1er novembre dernier, selon l'Institut de veille sanitaire (InVS), tandis que la " gastro " tend � se calmer.
Dans son bulletin hebdomadaire, l'InVS souligne que l'�pid�mie de grippe est d�sormais "confirm�e" et qu'elle est marqu�e par une augmentation du nombre hebdomadaire de consultations pour syndromes grippaux, des hospitalisations et des admissions en r�animation pour grippe. Selon le r�seau de surveillance Sentinelles-Inserm, 274.000 personnes auraient consult� un m�decin pour des sympt�mes li�s � la grippe au cours des deux derni�res semaines.
Pour la semaine derni�re, le taux d'incidence des cas de sympt�mes grippaux a �t� estim� � 243 cas pour 100.000 habitants, "au-dessus du seuil �pid�mique" fix� � 167 cas pour 100.000 habitants, pr�cise Sentinelles qui souligne que "cette deuxi�me semaine cons�cutive de d�passement du seuil vient confirmer l'arriv�e de l'�pid�mie de grippe en France M�tropolitaine". Les taux les plus �lev�s ont �t� observ�s la semaine derni�re en Provence-Alpes-C�te d'Azur (467 cas pour 100.000 habitants), Languedoc-Roussillon (450) et Rh�ne-Alpes (369).
�
13 personnes d�c�d�es de la grippe depuis le 1er novembre
Selon les mod�les de pr�vision reposant sur des donn�es historiques et sur les ventes de m�dicaments (partenariat IMS-Health), les cas devraient continuer d'augmenter cette semaine, ajoute le r�seau Sentinelles. L'InVS pr�cise pour sa part que 46 nouveaux cas graves de grippe ont �t� admis en r�animation la semaine derni�re, portant � 165 le nombre de cas graves depuis le 1er novembre dernier.
La grande majorit� des cas concerne des adultes avec facteur de risque, infect�s tr�s majoritairement par des virus de type A.� Seuls 20 d'entre eux avaient �t� vaccin�s contre la grippe.� Quatre d�c�s ont �t� observ�s la semaine derni�re, portant � 13 le nombre total des d�c�s par grippe observ�s depuis le 1er novembre dernier, selon les chiffres fournis par l'InVs.
Petite d�crue pour la gastroent�rite
La gastroent�rite qui se d�veloppait rapidement depuis quelques semaines a en revanche entam� une petite d�crue avec 127.000 nouveaux cas observ�s la semaine derni�re contre 158.000 la semaine pr�c�dente. "La d�croissance de l'incidence des diarrh�es aigu�s semble avoir d�but� sans que le seuil �pid�mique ait �t� franchi", note pour sa part le r�seau Sentinelles.
Les r�gions les plus touch�es par la "gastro" la semaine derni�re �taient la Franche-Comt� (402 cas pour 100.000 habitants) la Basse-Normandie (394) et la r�gion Provence-Alpes-C�te d'Azur (346).
Commenter cet article
Vous devez �crire un avis
a6parterre : Pour allez sur les r�seaux sociaux vous avez du mat�riel.
Le 12/02/2014 � 22h31
