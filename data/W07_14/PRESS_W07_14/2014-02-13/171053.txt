TITRE: Royaume-Uni. S�rie de colis suspects trouv�s dans des bureaux de l'arm�e
DATE: 2014-02-13
URL: http://www.ouest-france.fr/royaume-uni-serie-de-colis-suspects-trouves-dans-des-bureaux-de-larmee-1928305
PRINCIPAL: 171014
TEXT:
Royaume-Uni. S�rie de colis suspects trouv�s dans des bureaux de l'arm�e
Royaume-Uni -
La police antiterroriste a indiqu� jeudi enqu�ter apr�s la d�couverte de colis suspects dans des bureaux de l'arm�e de six villes du Royaume-Uni.
Ces paquets repr�sentaient une menace r�elle mais "de faible niveau", ont pr�cis� des sources polici�res.�
Selon la cha�ne de t�l�vision Sky News, les paquets trouv�s � Reading (ouest de Londres) et Chatham (sud-est de Londres) �taient de petits engins explosifs, mais en �tat de fonctionner.  Les services de d�minage ont �t� appel�s, selon la proc�dure habituelle en de tels cas.
Les autres colis ont �t� trouv�s dans des bureaux de l'arm�e situ�s dans un centre commercial � Slough (ouest de Londres) et � Aldershot (sud-ouest de Londres), selon la police. Le centre commercial de Slough a �t� �vacu� et des zones de s�curit� ont �t� �tablies autour des bureaux o� les paquets ont �t� d�couverts.�
Le minist�re de la D�fense a confirm� "des incidents li�s � la s�curit� � Oxford, Slough et Brighton touchant des bureaux des forces arm�es". "Des consignes de s�curit� ont de nouveau �t� donn�es � notre personnel", a ajout� le minist�re.�
Tags :
