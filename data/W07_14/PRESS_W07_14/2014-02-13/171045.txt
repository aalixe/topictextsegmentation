TITRE: MARNE. 130 000 signatures pour la p�tition de De Courson - R�gion en direct - www.lunion.presse.fr
DATE: 2014-02-13
URL: http://www.lunion.presse.fr/region/marne-130-000-signatures-pour-la-petition-de-de-courson-ia231b0n299424
PRINCIPAL: 171043
TEXT:
MARNE. 130 000 signatures pour la p�tition de De Courson
Par la r�daction pour L'union-L'Ardennais , Publi� le 13/02/2014
Par L'union-L'Ardennais
- A +
Le d�put� UDI de la Marne, Charles de Courson a mis en ligne une p�tition pour plus de transparence dans l'utilisation de l'argent public. A ce jour, jeudi 13 f�vrier, elle a recueilli 130 000 signatures .
Le d�put� marnais�veut que ses confr�res et cons�urs d�put�s S�nateurs "justifient par des factures la bonne utilisation � des fins professionnelles de leur Indemnit� Repr�sentative des Frais de Mandat".
Charles de Courson demande aux Pr�sidents de l�Assembl�e nationale et du S�nat, par courrier accompagn� de la p�tition, de saisir les Questeurs des quatre propositions qu�il formule susceptibles d��tablir une vraie transparence quant � l�utilisation de cette indemnit�. Il insiste sur la n�cessit� d��tablir de nouvelles r�gles pour que les �lus soient davantage exemplaires.
Pour appuyer sa demande, il a envoy� un courrier aux pr�sidents de groupe.
