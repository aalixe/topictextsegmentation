TITRE: INFO ou INTOX - Apple TV 4 : pr�sent�e bient�t, commercialis�e en fin d'ann�e - GAMERGEN.COM
DATE: 2014-02-13
URL: http://www.gamergen.com/actualites/apple-tv-4-presentee-bientot-commercialisee-fin-annee-178552-1
PRINCIPAL: 171523
TEXT:
INFO ou INTOX - Apple TV 4 : pr�sent�e bient�t, commercialis�e en fin d'ann�e
Le jeudi 13 F�vrier 2014 � 18h04
1 829 lectures
La date de sortie de la prochaine Apple TV est estim�e.
Entre sa pr�sence dans le code d'iOS 7 , sa mise en avant sur l'Apple Store et les nombreuses rumeurs � son sujet, il ne fait presque plus aucun doute que la marque � la pomme va bient�t proposer une version rafra�chie de son Apple TV. Reste � savoir quand !
Alors que certains proph�tisent le mois de mars , le tr�s s�rieux Bloomberg affirme que le bo�tier TV connect� sera pr�sent� le mois d'apr�s, en avril, mais ne sera disponible que beaucoup plus tard. En effet, si Apple a pour habitude de commercialiser ses produits la semaine suivant leur officialisation, ici, il faudrait attendre jusqu'� la p�riode de No�l. La raison invoqu�e concerne les diff�rents accords avec les partenaires de la distribution (magasins sp�cialis�s ?), mais aussi avec les d�veloppeurs.
Pour rappel, cette quatri�me g�n�ration d'Apple TV devrait compl�tement changer d'approche, en proposant non seulement du multim�dia, mais aussi du jeu, avec un store d�di�. En laissant une longue p�riode de jach�re entre la pr�sentation de l'appareil et sa sortie, Apple s'assurerait ainsi un catalogue fourni d'applications et de jeux compatibles d�s son lancement.
En outre, Bloomberg indique que la firme de Cupertino serait en discussion avec Time Warner Cable (TWC) pour proposer les services de la t�l�vision par c�ble directement depuis l'Apple TV. Ce point de d�tails en revanche ne concerne bien �videmment pas la France, dont la majorit� de la distribution des cha�nes t�l�vis�es se fait par le r�seau hertzien�ou par les offres triple/quadruple-play des op�rateurs.
Patience d�sormais, il faudra attendre ce printemps pour savoir de quoi il retourne vraiment !
