TITRE: LG G Pro 2 : et une phablette de plus !
DATE: 2014-02-13
URL: http://www.presse-citron.net/lg-g-pro-2-et-une-phablette-de-plus
PRINCIPAL: 170911
TEXT:
Mobiles et PDA - 13 f�vrier 2014 :: 16:25 :: Par Fredzone
LG G Pro 2 : et une phablette de plus !
Le LG G Pro 2 a fait l�objet d�un certain nombre de rumeurs depuis le d�but de l�ann�e et beaucoup s�attendaient � une annonce durant le Mobile World Congress 2014. LG a n�anmoins d�cid� de prendre un peu d�avance et ce n�est pas forc�ment une mauvaise chose.
print
Le MWC 2014 s�annonce particuli�rement intense. Samsung, Sony et Nokia, pour ne citer que ces derniers, devraient profiter de l��v�nement pour introduire de nouveaux terminaux positionn�s sur le haut de gamme. On pensera notamment au Galaxy S5 ou encore au successeur du Xperia Z1.
Face � ces derniers, le LG G Pro 2 aurait pu finir noy� dans la masse et c�est peut-�tre ce qui a pouss� la firme � donner un coup d�acc�l�rateur.
Le LG G Pro 2 se positionne sur le segment des phablettes. Il embarque un �cran IPS de 5,9 pouces dot� d�une d�finition de type Full HD 1080p, soit du 1920�1080. L� dessus, il profite �galement d�un SoC Qualcomm Snapdragon 800 cadenc� � 2,26 GHz et �paul� � la fois par un GPU Adreno 330 et par 3 Go de m�moire vive. Il vient ainsi s�aligner sur le Galaxy Note 3.
M�me chose pour sa m�moire interne qui atteint les 32 Go et qui pourra �tre �tendue par l�entremise du port pour cartes micro SD int�gr�. Selon toute vraisemblance, il y a peu de chances que nous nous sentions � l��troit puisque le port en question pourra accepter des cartes allant jusqu�� 64 Go.
En dehors du WiFi 802.11 a/b/g/n/ac, du Bluetooth 4.0 et de la puce NFC, nous avons aussi un capteur de 13 millions de pixels qui pourra enregistrer des vid�os en 4K, ou en 1080p. Dans ce dernier cas, nous pourrons monter jusqu�� 120 images par seconde. Id�al pour les adeptes de ��slow motion��. La cam�ra frontale, elle, offre une d�finition de 2,1 millions de pixels.
Point positif, la batterie sera similaire � celle du Note 3, avec une capacit� de 3 200 mAh. Compte tenu de la fiche technique du terminal, ce dernier devrait donc profiter d�une excellente autonomie.
LG nous avait promis quelques surprises suppl�mentaires et le constructeur a tenu parole puisque le G Pro 2 profitera d�un certain nombre d�am�liorations visant � transcender les photos et les vid�os captur�es par son entremise. Comme le Magic Focus, qui nous permettra de changer la zone de mise au point de nos clich�s apr�s leur enregistrement.
Le boitier est plut�t compact (157,9 x 81,9 x 8,3 mm), pour un poids tournant autour de 170 grammes.
Le LG G Pro 2 sera d�clin� en trois couleurs : titan, blanc et rouge. Pour le moment, nous ne connaissons ni son prix, ni la date de son lancement.
Puisqu�on en parle, sachez que LG devrait annoncer tr�s prochainement un autre terminal : le LG G2 Mini.
