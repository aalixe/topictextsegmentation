TITRE: Tout Sur L'Immobilier
DATE: 2014-02-13
URL: http://www.toutsurlimmobilier.fr/des-logements-neufs-plus-petits-et-moins-chers.html
PRINCIPAL: 171298
TEXT:
Le�2014-02-13
Des logements neufs plus petits et moins chers
Les derni�res statistiques de la F�d�ration des promoteurs immobiliers (FPI) permettent de faire le point � fin 2013 sur les prix des logements neufs et leurs surfaces habitables, tous deux en l�ger repli.
Combien co�te un logement neuf ? Les derni�res donn�es diffus�es par la F�d�ration des promoteurs immobiliers (FPI) permettent d'identifier un peu plus pr�cis�ment les prix d'achat et les tailles des appartements dans le parc immobilier neuf.
Prix moyen au m�tre carr�
Sur l'ensemble de l'ann�e 2013, le prix moyen au m�tre carr� (m�) d'un logement neuf a baiss� de 1,6% en moyenne. Cette tendance est plus affirm�e en Ile-de-France (-2,6%) qu'en province (-0,6%). La FPI reconna�t, par la voix de son pr�sident Fran�ois Payelle, qu'elle ne sait pas expliquer le repli plus prononc� des prix sur le march� francilien : il est donc difficile de dire s'il s'agit d'un effet statistique, li� � un volume de ventes plus cons�quent en Grande Couronne qu'en proche banlieue parisienne, ou si la baisse des prix touche l'ensemble du march�.
Sur le quatri�me trimestre 2013, le prix moyen s'affiche � 3.606 euros/m� dans la r�gion Ile-de-France. Pour les autres r�gions de France, il se situe � 4.462 euros/m�, un prix qui cache des disparit�s importantes selon les agglom�rations (voir plus bas).
Prix de vente moyen et surface habitable
Dans l'ensemble, la taille des logements vendus par les promoteurs aux particuliers sont aussi en l�ger recul, ce qui contribue � la baisse relative des prix. Les prix s'�chelonnent en moyenne de pr�s de 123.000 euros pour un studio � environ 453.000 euros pour un appartement de cinq pi�ces en province.
Prix et surface habitable des logements neufs
en province
