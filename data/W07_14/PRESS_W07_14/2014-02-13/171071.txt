TITRE: La France pi�g�e dans la trag�die de la Centrafrique
DATE: 2014-02-13
URL: http://www.lemonde.fr/afrique/article/2014/02/12/la-france-piegee-dans-la-tragedie-de-la-centrafrique_4364851_3212.html
PRINCIPAL: 171070
TEXT:
La France pi�g�e dans la trag�die de la Centrafrique
Le Monde |
� Mis � jour le
12.02.2014 � 11h13
En Centrafrique (RCA), l' arm�e fran�aise est dans une situation de plus en plus intenable. Trop faible, beaucoup trop faible, le contingent fran�ais est aujourd'hui le t�moin impuissant de ce que l'on doit maintenant qualifier par son nom, apr�s avoir longtemps h�sit� : une �puration ethnique dont les communaut�s musulmanes sont les victimes. Accus�s de passivit�, la France et ses soldats ne le seront-ils pas un jour de complicit� ?
Lire notre reportage (�dition abonn�s) : La Centrafrique � l'heure de l'�puration ethnique
Le Monde.fr a le plaisir de vous offrir la lecture de cet article habituellement r�serv� aux abonn�s du Monde.fr.  Profitez de tous les articles r�serv�s du Monde.fr en vous abonnant � partir de 1� / mois | D�couvrez l'�dition abonn�s
En se refusant � comparer des situations qui ne s'y pr�tent pas, remontent tout de m�me � la surface des images de 1994 au Rwanda � des images de soldats fran�ais, aucunement complices, comme certains l'ont dit, mais assur�ment impuissants � enrayer les massacres.
Le g�nocide rwandais fut planifi� par le r�gime. Rien de tel � Bangui. On ne peut pas dire , non plus, que la France a entra�n� en Centrafrique de futurs g�nocidaires, comme ce fut le cas � Kigali. Il n'est pas non plus question de remettre en cause la d�cision de la France d' intervenir dans son ancienne colonie � partir du 5 d�cembre 2013. Il fallait stopper les violences des ex-rebelles de la S�l�ka, qui s'�taient empar�s du pouvoir en mars 2013 � l'issue d'une sanglante chevauch�e du nord au sud de la Centrafrique.
LA FRANCE SEULE EN PREMI�RE LIGNE
Aujourd'hui comme hier, aucun pays occidental ne voulait intervenir au centre du continent africain et emp�cher que la RCA devienne une gigantesque zone grise, plus grande que la France, aux confins d'Etats d�j� instables tels que le Tchad , le Soudan , le Soudan du Sud et la R�publique d�mocratique du Congo. La France s'est retrouv�e seule en premi�re ligne. Elle l'est toujours. Ses partenaires europ�ens d�tournent le regard plut�t que d' envoyer quelques centaines d'hommes � Bangui.
En d�ployant seulement 1 600 hommes, la France s'est-elle donn� les moyens d' atteindre ses objectifs ? Deux mois apr�s le d�but de l'intervention, la r�ponse est non.
Lirenotre reportage (�dition abonn�s) : En Centrafrique, avec les soldats de � Sangaris �
�L'INTERVENTION FRAN�AISE A D�CHA�N� DES HAINES
Rappelons-nous les � �l�ments de langage � �nonc�s il y a peu. Surfant sur le succ�s de l'intervention au Mali , il ne s'agissait, pour Paris , que de mener une � op�ration de police �, � courte �, dans un des pays africains que la France, disait-on, conna�t le mieux pour y avoir fait la pluie et le beau temps depuis l'ind�pendance de 1960. Il �tait aussi question de s' appuyer sur le contingent africain, un objectif politique louable qui manque singuli�rement de r�alisme op�rationnel.
R�sultat, loin de pacifier le pays, l'intervention fran�aise a invers� le rapport de forces et d�cha�n� des haines dont Paris ne soup�onnait pas la violence. N'y a-t-il pas l� une d�faillance dans le renseignement fourni aux autorit�s politiques ?
Lirel'entretien de Marie-Elisabeth Ingres, chef de mission de MSF : � La situation empire dans l'ensemble de la Centrafrique �
Dans le sillage des milices d'autod�fense dites anti-Balaka, essentiellement chr�tiennes, des foules haineuses se sont fix� pour objectif d' effacer toute trace de la pr�sence musulmane dans le pays. A Bangui, c'est pratiquement chose faite. Des dizaines de milliers de musulmans ont pris la route de l'exode. Combien de milliers d'hommes, de femmes et d'enfants d�coup�s � la machette ? Qu'elle le veuille ou non, la France est embarqu�e dans cette trag�die.
L�acc�s � la totalit� de l�article est prot�g� D�j� abonn� ? Identifiez-vous
La France pi�g�e dans la trag�die de la Centrafrique
Il vous reste 73% de l'article � lire
