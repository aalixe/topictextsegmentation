TITRE: Femen. Une ex-membre d�nonce "l'organisation dictatoriale" du mouvement
DATE: 2014-02-13
URL: http://www.ouest-france.fr/femen-une-ex-membre-denonce-lorganisation-dictatoriale-du-mouvement-1928224
PRINCIPAL: 0
TEXT:
Femen. Une ex-membre d�nonce "l'organisation dictatoriale" du mouvement
France -
Achetez votre journal num�rique
Une ancienne Femen a d�cid� de claquer la porte de l'organisation f�ministe pour d�noncer son "organisation dictatoriale".
Une ancienne Femen a d�cid� de claquer la porte de l'organisation f�ministe pour d�noncer son "organisation dictatoriale" et le manque de respect des personnes et de la libert� d'expression en son sein, a-t-elle dit jeudi.
"Un ph�nom�ne de meute"
"Alice", qui souhaite rester anonyme, a "milit� activement pendant un an" chez les Femen France avant d'en sortir il y a quelques mois pour raconter son exp�rience dans un livre qui doit sortir dans quelques semaines.
"C'est une organisation qui fait penser � une dictature avec des r�gles qui s'appliquent � certaines mais pas � d'autres", dit-elle am�re. Chez les Femen, "il y a un ph�nom�ne de meute. Quand on n'est pas d'accord on est mis � l'�cart", explique-t-elle.
"D��ue" par son exp�rience
Selon l'ex-militante "on n'applique pas � l'int�rieur du mouvement les revendications pour lesquelles on se bat � l'ext�rieur: le respect des personnes et de la libert� d'expression".
Elle se dit "d��ue" par son exp�rience. "Je ne suis pas un cas isol�, des filles plus fragiles ont beaucoup souffert" de l'organisation arbitraire, "sans r�gles", des Femen.
Selon elle, il reste une vingtaine d'activistes chez les Femen France et "beaucoup plus ont quitt� le mouvement".
"Femen n'est pas une bande de potes"
La chef de file du mouvement, Inna Shevchenko, a r�pondu � Alice dans une tribune publi�e jeudi matin sur le site huffingtonpost.�"Cette femme (...) raconte qu'il existe une hi�rarchie au sein du mouvement et que l'atmosph�re n'y est pas tr�s amicale", explique Inna Shevshenko. "Je ne d�mentirai pas ces informations", r�pond-elle.�
"Femen n'est pas une bande de potes, mais un groupe militant. Nous sommes unies, non pas pour sortir boire des verres, mais pour se battre. L'atmosph�re est martiale. Oui, nous avons une hi�rarchie affirm�e (...) qui nous permet de mener � bien des op�rations complexes", explique-t-elle.
"On n'est pas l� pour boire des verres mais il y a un minimum de respect � avoir", lui r�pond Alice, "des filles sont ostracis�es parce qu'elles ne r�pondent pas aux crit�res physiques ou qu'elles ne sont pas assez branch�es".
Femen, une secte?
Mme Shevchenko r�pond aussi au d�put� UMP Georges Fenech qui a estim� lundi que la Mission interminist�rielle de lutte contre les sectes (Miviludes) devait demander la dissolution du mouvement des Femen dont les actions s'apparentent, selon lui, "� des pratiques � caract�re sectaire".
"Si les f�ministes et les humanistes sont � pr�sent consid�r�-e-s comme sectaires en France, nous n'avons aucun probl�me avec �a. Et tant que cela sera affirm� par des bigots qui pr�f�rent jurer sur la Bible plut�t que sur la Constitution, nous continuerons � les combattre, avec joie", clame-t-elle
Tags :
