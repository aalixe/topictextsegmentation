TITRE: Actu sant� : OB�SIT� infantile: Mais non, mon enfant n?est pas gros!
DATE: 2014-02-13
URL: http://www.santelog.com/news/nutrition-obesite/obesite-infantile-mais-non-mon-enfant-n-est-pas-gros-_11867.htm
PRINCIPAL: 171581
TEXT:
OB�SIT� infantile: Mais non, mon enfant n?est pas gros!
Actualit� publi�e le 16-02-2014
Pediatrics
��Mais non, mon enfant n�est pas comme �a � diront la moiti� des parents d�enfants ouvertement en surpoids. Cette �tude de l�Universit� du Nebraska, un �tat o� une majorit� des enfants sont en surpoids, montre qu�on ne peut compter sur la perception des parents et donc sur leur implication dans le contr�le du poids de leurs enfants, puisque la plupart des parents ne parviennent pas � reconna�tre si leur enfant est en surpoids.
�
Alors que les taux d'ob�sit� infantile aux �tats-Unis ont tripl� au cours des 30 derni�res ann�es, que parmi les axes de lutte contre l�ob�sit�, figurent l��ducation nutritionnelle des familles ou encore les repas pris � la maison, les conclusions de cette m�ta-analyse r�v�lent que plus de 50% des parents sous-estiment le poids de leur enfant alors qu�il est en surpoids ou ob�se. Pourquoi alors ces parents seraient-ils motiv�s � encourager leur enfant � adopter un r�gime alimentaire� raisonnable et � pratiquer plus d�activit� physique�?
�
