TITRE: La Belgique autorise l'euthanasie pour les enfants atteints d'une maladie incurable
DATE: 2014-02-13
URL: http://www.francetvinfo.fr/societe/euthanasie/la-belgique-autorise-l-euthanasie-pour-les-enfants-atteints-d-une-maladie-incurable_529441.html
PRINCIPAL: 171415
TEXT:
Tweeter
La Belgique autorise l'euthanasie pour les enfants atteints d'une maladie incurable
Les d�put�s belges ont d�finitivement adopt�, jeudi, une loi �tendant le champ l�gal de l'euthanasie aux mineurs atteints d'une maladie incurable.
En 2012, 1 432 euthanasies ont �t� officiellement enregistr�es, repr�sentant 2% des d�c�s en Belgique. (VOISIN / PHANIE / AFP)
Par Francetv info avec AFP
Mis � jour le
, publi� le
13/02/2014 | 18:50
Alors que le d�bat sur l'euthanasie cr�e des crispations en France, les d�put�s belges ont d�finitivement adopt�, jeudi 13 f�vrier, une loi qui �tend le champ l�gal de l'euthanasie aux mineurs atteints d'une maladie incurable. Ils n'ont pas fix� d'�ge minimum. En Belgique, l'euthanasie pour les adultes a �t� adopt�e il y a douze ans.�Un� sondage paru en octobre �indiquait que trois quarts des Belges �taient favorables � une extension de ce texte aux mineurs.
La loi, d�j� vot�e en d�cembre par le S�nat, a �t� approuv�e par les d�put�s � une majorit� de 86 "pour", 44 "contre" et 12 abstentions. Elle entrera en vigueur dans les prochaines semaines.
Dans quels cas un mineur pourra-t-il �tre euthanasi� ?
Le texte pr�voit qu'un mineur peut demander � b�n�ficier de l'euthanasie s'il fait face � des souffrances physiques insupportables et inapaisables, en phase terminale. Il doit, pour cela, �tre conseill� par une �quipe m�dicale et recevoir l'accord parental. Les souffrances "psychiques" insupportables, qui ouvrent la voie � l'euthanasie pour les adultes, ont en revanche �t� �cart�es pour les mineurs par les s�nateurs.
Combien de personnes seraient concern�es ?
Tr�s peu. Selon l'AFP,�la loi ne devrait concerner, par an, qu'entre dix et quinze cas de mineurs atteints d'un mal incurable, en particulier d'un cancer, et dont le d�c�s est pr�vu dans un court d�lai. Cela repr�senterait une tr�s faible proportion des actes d'euthanasie.�En 2012, 1�432�euthanasies ont �t� officiellement enregistr�es, repr�sentant 2% des d�c�s en Belgique.
Si le projet de loi est adopt�, la Belgique suivra l'exemple des Pays-Bas, le pays europ�en le plus en pointe sur le sujet, o� l'euthanasie est autoris�e pour les mineurs depuis 1998.
