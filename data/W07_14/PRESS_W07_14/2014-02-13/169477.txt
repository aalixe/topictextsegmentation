TITRE: Bordeaux-Lorient et Brest-Angers reprogramm�s - Football - Sports.fr
DATE: 2014-02-13
URL: http://www.sports.fr/football/ligue-1/scans/bordeaux-lorient-et-brest-angers-reprogrammes-1009203/
PRINCIPAL: 169475
TEXT:
Hoarau a joué son premier match avec Bordeaux samedi face à Toulouse. (Reuters)
Publi� le
12 février 2014 � 19h27
Mis à jour le
12 février 2014 � 19h31
La Commission des Compétitions de la LFP a annoncé, ce mercredi, la reprogrammation des rencontres Girondins de Bordeaux -FC Lorient et Stade Brestois- Angers SCO.
La rencontre de la 24e journée de Ligue 1 opposant Aquitains à Morbihannais, reporté en raison d’un terrain rendu impraticable par les fortes pluies, aura lieu le mardi 25 février à 18h30.
Le match comptant pour la 23e journée de Ligue 2 entre Brestois et Angevins, reporté pour les mêmes raisons, a été fixé au mardi 4 mars à 19h30.
