TITRE: Comment Laporte contourne la sanction - Fil Info - Top 14 - Rugby -
DATE: 2014-02-13
URL: http://sport24.lefigaro.fr/rugby/top-14/fil-info/toulon-va-contourner-la-sanction-678770
PRINCIPAL: 170242
TEXT:
Comment Laporte contourne la sanction
12/02 17h01 - Rugby
Malgr� la sanction de 13 semaines �cop�e pour propos injurieux � l�encontre de l�arbitre Laurent Cardona, Bernard Laporte va pouvoir continuer de communiquer avec ses joueurs � la mi-temps. Dans le pass�, le RCT a d�j� contourn� l�interdiction de vestiaires en mettant en place un syst�me de visioconf�rence. Les Varois devraient encore utiliser ce syst�me dans les semaines � venir.�
