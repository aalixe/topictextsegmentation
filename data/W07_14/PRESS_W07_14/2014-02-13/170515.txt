TITRE: Explosion à Nice: il s'agirait d'un infanticide
DATE: 2014-02-13
URL: http://www.lefigaro.fr/flash-actu/2014/02/13/97001-20140213FILWWW00211-explosion-a-nice-il-s-agirait-d-un-infanticide.php
PRINCIPAL: 170513
TEXT:
le 13/02/2014 à 14:20
Publicité
La mère d'un enfant de quatre ans, retrouvé mort le 24 janvier après l'explosion de son immeuble à Nice , a été mise en examen pour meurtre et écrouée, selon une source proche de l'enquête.
L'enquête sur une explosion accidentelle au gaz s'est finalement orientée vers une piste criminelle, celle d'un infanticide. La violente explosion s'était déroulée au dernier étage d'un immeuble d'habitation de deux étages du centre-ville. La toiture avait été soufflée et le bâtiment avait perdu de larges pans de sa façade. Outre l'enfant retrouvé mort, l'explosion avait fait trois blessés. Parmi eux, la mère de l'enfant, grièvement brûlée et qui n'était pas en état d'être auditionnée ce jour-là.
Deux voisins, qui vivaient à côté de l'appartement soufflé, avaient été légèrement atteints.
