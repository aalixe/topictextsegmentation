TITRE: Foot - - Sept clubs r�futent l'accord LFP-ASM
DATE: 2014-02-13
URL: http://www.lequipe.fr/Football/Actualites/Sept-clubs-refutent-l-accord-lfp-asm/441005
PRINCIPAL: 170233
TEXT:
a+ a- imprimer RSS
Le magazine Le Point r�v�le ce jeudi que sept clubs fran�ais (PSG, OM, Lille, Bordeaux, Montpellier, Caen et Lorient) ont r�dig� une lettre aux autres formations professionnelles pour leur indiquer qu'ils allaient engager une proc�dure contre la Ligue de Football Professionnel (LFP). Ils contestent l�accord obtenu entre cette derni�re et l�AS Monaco qui exon�re le club de la Principaut� d'installer son si�ge en France contre un d�dommagement de 50 millions d�euros.�
A travers cette longue missive, les sept pr�sidents affirment ��regretter que le conseil d'administration ait �t� saisi en urgence [...] et que ces membres aient d� se prononcer [...] sans connaissance du projet de transaction et sans vision claire de cet accord [...] Dans ce contexte, les dirigeants de clubs professionnels soussign�s ont d�cid� d'engager ensemble une action contentieuse contre ce qui appara�t comme un arrangement pr�cipit�, peu transparent et insuffisants, afin de faire annuler la transaction intervenue��. Les Girondins de Bordeaux ont confirm� l�existence de cette lettre et assurent vouloir engager avec les autres clubs ��une action contentieuse afin de faire annuler la transaction intervenue et de permettre qu�une solution soucieuse des int�r�ts de l�ensemble des acteurs et de l��quit� sportive soit trouv�e��.
Le 4 f�vrier dernier, Fr�d�ric Thiriez , pr�sident de la LFP, all�guait � propos de cet accord qu� ��un arrangement, m�me s'il pouvait �tre jug� financi�rement insuffisant, �tait pr�f�rable � un mauvais proc�s��.
