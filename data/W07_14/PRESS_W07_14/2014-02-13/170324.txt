TITRE: VIDEO. Une exp�dition clandestine montre Shanghai depuis 650 m�tres de haut - L'Express
DATE: 2014-02-13
URL: http://www.lexpress.fr/actualite/monde/asie/video-une-expedition-clandestine-montre-shanghai-depuis-650-metres-de-haut_1323550.html
PRINCIPAL: 170322
TEXT:
VIDEO. Une exp�dition clandestine montre Shanghai depuis 650 m�tres de haut
Par LEXPRESS.fr, publi� le
13/02/2014 �  12:33
Deux Russes sont parvenus � se hisser clandestinement au somment de la Shanghai Tower, haute de 650 m�tres. Ils ont film� leur exploit.�
Voter (2)
� � � �
Deux Russes sont parvenus � se hisser clandestinement au somment de la Shanghai Tower, haute de 650 m�tres. Ils ont film�s leur exploit.
Capture d'�cran
En voil� qui n'ont pas le vertige. Deux jeunes Russes adeptes de sensations fortes sont parvenus � monter clandestinement au sommet de la deuxi�me plus haute tour du monde, la Shanghai Tower actuellement en construction, pour y tourner une vid�o spectaculaire.�
�
Les deux photographes "ninjas urbains", Vadim Makhorov et Vitaliy Raskalov, disent avoir profit� de l'interruption du chantier � la date du Nouvel An lunaire (31 janvier) pour lancer leur ambitieuse exp�dition nocturne. "Ce jour-l� il y avait un rel�chement de la s�curit�, les ouvriers �taient en cong� et les grues �taient � l'arr�t", a expliqu� sur son blog Vitaliy Raskalov.�
Apr�s avoir mont� en pr�s de deux heures les 120 �tages du gratte-ciel, les deux Russes sont parvenus � une plateforme au sommet de la tour, et de l� ont encore escalad� l'immense grue surplombant la structure. On les voit prendre des positions p�rilleuses au-dessus de la ligne des nuages et de la pollution qui recouvre la m�galopole chinoise.�
"Les superproductions d'Hollywood ne sont rien par rapport � cela"
La soci�t� g�rant la Shanghai Tower, cit�e dans la presse d'Etat, a promis de prendre des mesures pour emp�cher une r��dition d'un tel exploit. Les internautes chinois �taient nombreux jeudi � saluer leur performance, seulement une minorit� d'entre eux relevant qu'ils l'avaient fait en infraction de la loi. "Oh mon Dieu! C'est terrifiant!", a �crit l'un d'entre eux. "Les superproductions d'Hollywood ne sont rien par rapport � cela", a estim� un autre.�
Les deux hommes sont coutumiers des ascensions clandestines de gratte-ciel, principalement en Russie. Leur principal fait d'armes est d'avoir escalad� la pyramide de Gizeh , � la suite de quoi Vitaliy Raskalov avait pr�sent� des excuses pour cet acte formellement interdit, dans une interview � CNN.�
La Shanghai Tower a l'an dernier d�pass� en taille la tour ta�wanaise Taipei 101 et est d�sormais le plus haut gratte-ciel d'Asie et le deuxi�me au monde apr�s la Burj Khalifa � Duba�, qui atteint 830 m�tres.�
Avec
