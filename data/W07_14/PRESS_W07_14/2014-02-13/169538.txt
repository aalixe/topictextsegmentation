TITRE: Obama augmente le salaire minimum pour des centaines de milliers d'Am�ricains - L'Express
DATE: 2014-02-13
URL: http://www.lexpress.fr/actualite/monde/amerique-nord/obama-augmente-le-salaire-minimum-pour-des-centaines-de-milliers-d-americains_1323471.html
PRINCIPAL: 169537
TEXT:
Obama augmente le salaire minimum pour des centaines de milliers d'Am�ricains
Par LEXPRESS.fr, publi� le
13/02/2014 �  09:51
Le pr�sident am�ricain a augment� de 25% le salaire minimum des contractuels de l'Etat, � 10,10 dollars (7,4 euros). Lors du discours sur l'�tat de l'Union fin janvier, il avait annonc� que 2014 serait une "ann�e d'action" contre les in�galit�s.�
Voter (0)
� � � �
Lors d'une c�r�monie � la Maison Blanche, Barack Obama a sign� un d�cret portant � 10,10 dollars le salaire minimum horaire des contractuels de l'Etat. Il a exhort� le Congr�s � g�n�raliser cette mesure.
Reuters/Larry Downing
En cette ann�e �lectorale, la question des in�galit�s est le sujet qui monte aux Etats-Unis . Barack Obama en est conscient. Il a sign� mercredi un d�cret portant � 10,10 dollars (7,4 euros) le salaire minimum horaire des contractuels de l'Etat, soit 25% de hausse, et exhort� le Congr�s � g�n�raliser cette mesure. Cette augmentation b�n�ficiera aux personnes embauch�es � partir du 1er janvier 2015. Elle devrait profiter � des centaines de milliers d'Am�ricains. �
"Dans le pays le plus riche sur terre, quelqu'un qui travaille � plein temps ne devrait avoir � vivre dans la pauvret�", a plaid� le pr�sident am�ricain lors d'une c�r�monie � la Maison Blanche. minimum horaire des contractuels de l'Etat. Barack Obama a ainsi concr�tis� une annonce effectu�e lors de son discours sur l'�tat de l'Union fin janvier .�
Le salaire minimum f�d�ral est actuellement de 7,25 dollars (5,3 euros), le m�me niveau depuis 2009. Certains Etats am�ricains appliquent des taux plus �lev�s.�
Une r�gression de 20% par rapport � l'�poque o� Ronald Reagan a pris ses fonctions
Le pr�sident am�ricain a fait valoir qu'en tenant compte de l'inflation, le pouvoir d'achat des personnes au salaire minimum avait en fait "r�gress� de 20% par rapport � l'�poque o� Ronald Reagan a pris ses fonctions" de pr�sident, soit en janvier 1981.�
Porter le salaire minimum � 10,10 dollars � partir de 2015 b�n�ficiera � des "centaines de milliers de personnes", avait auparavant assur� le secr�taire au Travail, Tom Perez . Il a expliqu� lors d'un point de presse � la Maison Blanche que cette nouvelle politique pourrait s'effectuer dans le cadre des allocations budg�taires actuelles.�
En effet, seule la Chambre des repr�sentants a le pouvoir constitutionnel de voter de nouvelles d�penses, et les �lus du parti r�publicain qui y sont majoritaires ont exprim� leur hostilit� � un augmentation du salaire minimum, de nature selon eux � freiner la croissance.�
Cette hausse "ne va pas avoir d'effet n�gatif sur l'�conomie, elle va doper l'�conomie"
Mais Barack Obama s'est inscrit en faux contre cette id�e, faisant valoir qu'une telle augmentation, appel�e de ses voeux depuis un an, "aiderait des millions d'Am�ricains � sortir de la pauvret�". Une telle mesure "ne va pas avoir d'effet n�gatif sur l'�conomie, elle va doper l'�conomie", a-t-il assur� en appelant une nouvelle fois le Congr�s � "donner une augmentation aux Etats-Unis".�
Le taux de ch�mage am�ricain est retomb� � son plus bas niveau depuis plus de cinq ans (6,6% en janvier) et la croissance s'est encore acc�l�r�e (4,1% en rythme annualis� au troisi�me trimestre dernier).�
Contrairement � la situation en France, le salaire minimum n'est pas augment� m�caniquement aux Etats-Unis chaque ann�e pour tenir compte de l'inflation.�
Selon l'Organisation internationale du travail, les Etats-Unis accusent un certain retard par rapport aux autres pays industrialis�s. En 2010, le salaire minimum am�ricain repr�sentait seulement 38,8% du salaire m�dian dans le pays, contre 46,1% en Grande-Bretagne et 60,1% en France.�
�
