TITRE: Patinage artistique/JO - Plushenko renonce - Sports - La Voix du Nord
DATE: 2014-02-13
URL: http://www.lavoixdunord.fr/sports/patinage-artistiquejo-plushenko-renonce-ia0b0n1917438
PRINCIPAL: 171209
TEXT:
Le journal du jour � partir de 0,49 �
Quelques minutes  avant son programme court, le quadruple m�daill� olympique russe Evgueni Plushenko, souffrant du dos, a renonc� � participer l'�preuve de patinage artistique des jeux Olympiques. Pendant l'�chauffement, le patineur de 31 ans, sacr� dimanche avec la nouvelle �preuve par �quipes, s'est tenu le dos, op�r� il y a un an. Il est all� voir le juge central pour lui signifier qu'il renon�ait. Plushenko avait d�j� gagn� son pari: d�crocher � nouveau de l'or olympique, ce qu'il a fait dimanche avec le titre de la nouvelle �preuve par �quipes. Il s'est offert une 4e m�daille aux Jeux pour �galer le seul patineur � ce niveau dans l'Histoire du patinage, le Su�dois Gillis Graefstroem (de 1920 � 1932).
AFP
