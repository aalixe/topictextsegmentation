TITRE: Poutine souhaite du succ�s au ministre �gyptien de la D�fense � la pr�sidentielle - Derni�res infos - Politique - La Voix de la Russie
DATE: 2014-02-13
URL: http://french.ruvr.ru/news/2014_02_13/Poutine-souhaite-du-succes-au-ministre-egyptien-de-la-Defense-a-la-presidentielle-3287/
PRINCIPAL: 169916
TEXT:
Poutine souhaite du succ�s au ministre �gyptien de la D�fense � la pr�sidentielle
� Photo : RIA Novosti
Par La Voix de la Russie | Le pr�sident russe Vladimir Poutine a souhait� du succ�s au ministre �gyptien de la D�fense, le feld-g�n�ral Abdel Fattah al-Sissi qui a pr�sent� sa candidature pour l��lection pr�sidentielle dans le pays.
� Il s�agit d�une d�cision tr�s importante de se conf�rer la responsabilit� du sort du peuple �gyptien. Au nom du peuple russe, je vous souhaite beaucoup de succ�s �, a indiqu� le chef de l�Etat russe ayant souhait� �galement de r�gler les relations avec toutes les couches de la soci�t� �gyptienne car la situation au Proche-Orient d�pend de la stabilit� en Egypte.
M. Poutine a esp�r� que l��lection en Egypte contribuera � la coop�ration entre deux pays.
