TITRE: PSA Poissy : suppression annoncée d'une des deux lignes de montage | Site mobile Le Point
DATE: 2014-02-13
URL: http://www.lepoint.fr/auto-addict/strategie/psa-poissy-suppression-annoncee-d-une-des-deux-lignes-de-montage-13-02-2014-1791359_659.php
PRINCIPAL: 0
TEXT:
13/02/14 à 13h56
PSA Poissy : suppression annoncée d'une des deux lignes de montage
La direction du groupe a confirmé le "passage en monoligne" en précisant qu'il sera sans impact sur la main-d'oeuvre de production.
PSA a précisé que l'arrêt d'une des deux lignes de montage n'aurait pas d'impact sur la main-d'oeuvre. THOMAS SAMSON / AFP
AFP
La direction du groupe automobile PSA a confirmé jeudi en comité d'établissement "son intérêt pour le passage en monoligne" sur le site de Poissy (Yvelines) qui se traduira, en janvier 2015, par l'arrêt d'une des deux lignes de montage, "sans impact sur la main-d'oeuvre de production".
Selon ce projet, la production de la 208 serait transférée sur une seule ligne qui produit actuellement la C3 et la DS3 et dont la cadence augmenterait de 45 à 52 véhicules par heure pour "arriver à un taux de capacité de production de 100 %" dans l'usine. "Cette optimisation industrielle n'aura aucun impact sur la main-d'oeuvre de production", a assuré un porte-parole de la direction du site.
à lire aussi
