TITRE: Victoires de la Musique 2014 : plus que 24 heures pour �lire la chanson de l'ann�e � metronews
DATE: 2014-02-13
URL: http://www.metronews.fr/culture/victoires-de-la-musique-2014-plus-que-24-heures-pour-elire-la-chanson-de-l-annee/mnbl!Glr04vRMtnGTI/
PRINCIPAL: 169834
TEXT:
Mis � jour  : 13-02-2014 11:39
- Cr�� : 12-02-2014 14:32
Victoires de la Musique 2014 : plus que 24 heures pour �lire la chanson de l'ann�e
C�R�MONIE - La chanson originale de l'ann�e est la cat�gorie soumise au vote du public. Stromae, le grand favori est nomm� � deux reprises dans la cat�gorie "Chanson Originale de l'ann�e" aux Victoires de la Musique. Un rouleau compresseur pour les autres artistes Johnny Hallyday et Maitre Gims dont les titres sont en lice ?
Tweet
�
Le choix est difficile. Dans la cat�gorie "Chanson Originale de l'ann�e", concourent trois artistes au succ�s populaire ind�niable. Jusqu'� jeudi 17 heures sur le site de la cha�ne France 2, le public aura la mission de d�partager Johnny Hallyday, Maitre Gims et Stromae.
Ce dernier part avec un avantage : "Papaoutai" et "Formidable", qui avaient suscit� un engouement imm�diat et durable de la part des auditeurs, sont nomm�es. Maitre Gims a �t� l'une des surprises musicales de l'ann�e 2013.
Maitre Gims et Johnny Hallyday, outsiders
Au contraire de ces deux concurrents, l'interpr�te de "Bella" a � peine d�but� sa carri�re solo, si on escompte ses dix ans pass�s et quelque au sein du groupe rap Sexion d'Assaut. Il a connu un beau succ�s gr�ce au titre nomm� "J'me tire", mais pas s�r qu'il fasse le poids face aux deux titres de Stromae.
Le m�me probl�me se pose � Johnny avec son titre "20 ans", dont il a tir� un beau clip �maill� d'images d'archives. Ces deux artistes sont comme pris dans  l'�tau Stromae, malgr� la r�ception et la qualit� de leurs titres. Au public de d�partager les quatre morceaux.
"Papaoutai" totalise pr�s de 100�millions de vues sur Youtube.
Revoir le clip de "Formidable" : pris pour un canular au d�part, la vid�o a surpris et d�montr� toutes les capacit�s de Stromae pour faire le "buzz".
Revoir "20 ans" de Johnny Hallyday
"J'me tire" de Maitre Gims
Dolores Bak�la
