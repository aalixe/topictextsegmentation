TITRE: Hollande vs Obama: �battle� de blagues (vid�o) | Monde - lesoir.be
DATE: 2014-02-13
URL: http://www.lesoir.be/426584/article/actualite/monde/2014-02-12/hollande-vs-obama-battle-blagues-video
PRINCIPAL: 0
TEXT:
Hollande et Obama: le partenariat France-USA est un �mod�le�
Fran�ois Hollande termine ce mercredi sa visite d��tat de trois jours aux �tats-Unis sous l��gide de Barack Obama, la premi�re depuis 1996. Si le programme, charg�, a abord� nombre de sujets tr�s s�rieux, force est de constater que les deux dirigeants ont tent� de d�tendre l�atmosph�re en rivalisant de traits d�esprits, allant m�me jusqu�� la fameuse ��private joke��.
Leurs enfants, le temps, les journalistes ou leurs langues respectives, autant de th�mes qui les ont visiblement inspir�s.
