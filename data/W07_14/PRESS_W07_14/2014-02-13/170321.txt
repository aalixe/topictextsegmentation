TITRE: Etats-Unis: Temp�te de neige sur le sud-est - 20minutes.fr
DATE: 2014-02-13
URL: http://www.20minutes.fr/monde/diaporama-4402-etats-unis-tempete-neige-sud
PRINCIPAL: 170319
TEXT:
Toutes les images de Etats-Unis: Temp�te de neige sur le sud-est
Image 1 sur 13
D'importantes chutes de neige sont attendues dans la nuit de mercredi � jeudi dans le sud-est du pays.
Mark Wilson/Getty Images/AFP
Alors qu�une partie de la France et de la Grande-Bretagne sont sous l�eau, les Etats-Unis sont touch�s par une nouvelle temp�te de neige. Celle-ci est pass�e par les Etats de Caroline du Nord et de Virginie avant de venir blanchir les rues de Washington. Le sud-est des Etats-Unis vit au ralenti cet hiver.�
R�alisation: Charlotte Gonthier
