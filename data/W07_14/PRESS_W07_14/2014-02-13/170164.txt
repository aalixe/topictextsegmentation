TITRE: J�r�me Kerviel joue son va-tout en cassation | La-Croix.com
DATE: 2014-02-13
URL: http://www.la-croix.com/Actualite/France/Jerome-Kerviel-joue-son-va-tout-en-cassation-2014-02-13-1105944
PRINCIPAL: 170090
TEXT:
petit normal grand
J�r�me Kerviel joue son va-tout en cassation
La cour de cassation a examin� ce jeudi 13 f�vrier le pourvoi de l�ancien trader de la Soci�t� G�n�rale. Elle se prononcera le 19 mars prochain.
13/2/14 - 12 H 48
�Jug� en appel, J�r�me Kerviel ne parvient pas � convaincre
Condamn� � trois ans de prison ferme, J�r�me Kerviel esp�re faire reconna�tre le r�le jou� par la banque.
La Cour de cassation se prononcera le 19 mars sur le pourvoi de l'ancien trader de la Soci�t� G�n�rale, J�r�me Kerviel , dont la d�fense a plaid� jeudi 13 f�vrier qu'il ne pouvait �tre tenu pour seul responsable de la perte massive subie par la banque.
La responsabilit� de la banque
� Condamn� en appel � cinq ans d�emprisonnement �(dont trois ferme) et 4,91�milliards d'euros de dommages et int�r�ts, l�ancien trader reconna�t sa part de responsabilit�, mais il r�fute avoir agi en secret. ��J�ai fait ce que la banque m�a appris � faire et je n�ai vol� personne��, mart�le-t-il depuis 2008. � l�entendre, la banque savait ce qui se tramait dans sa salle de march�s et ne peut donc se pr�tendre victime. Des d�fauts de contr�le importants ont en effet �t� point�s par le r�gulateur des banques, la Commission bancaire devenue l�Autorit� de contr�le prudentiel (ACP), qui a condamn� la Soci�t� G�n�rale � 4�millions d'euros d�amende pour ces manquements.
Une banque n�gligente, mais pas consentante
L�avocat g�n�ral pr�s la Cour de cassation , Yves Le Baut, estime pour sa part qu�une ��victime n�gligente n�est pas pour autant une victime consentante��. Ainsi, selon lui, ��on ne peut tirer pour cons�quence du d�faut de vigilance�� la responsabilit� de la banque. Un point de vue contest� par la d�fense qui consid�re, elle, que le d�faut de surveillance de la Soci�t� g�n�rale devait amoindrir la faute de J�r�me Kerviel, entra�ner un partage des responsabilit�s, voire annuler les dommages et int�r�ts prononc�s. Si les juges allaient dans ce sens, il s�agirait d�un important renversement de jurisprudence. Pour l�heure en effet, la n�gligence d�une victime n�a pas de cons�quence sur l�indemnisation prononc�e.
Des dommages et int�r�ts faramineux
La d�fense de J�r�me Kerviel conteste �galement le montant des dommages et int�r�ts �attribu�s � la banque. Ce montant correspond � la perte constat�e par l��tablissement bancaire apr�s s��tre d�fait de toutes les positions accumul�es par le jeune homme. L�ancien trader rappelle qu�il n�a tir� aucun profit personnel des op�rations incrimin�es. Reste que selon l�avocat g�n�ral, le pr�judice ne peut �tre indemnis� qu�en totalit�, et non partiellement. Et ce aussi colossal soit-il.
Pas d�am�nagement de peine
En cas de rejet de son pourvoi, J�r�me Kerviel sera incarc�r�. Il ne pourra pas b�n�ficier d�un am�nagement de peine (ex : placement sous bracelet �lectronique). Ces derniers ne sont en effet propos�s qu�aux condamn�s ayant �cop� d�une peine de prison de moins de deux ans.
Marie Boeton (avec AFP) �
