TITRE: News | Les r�actions des Fran�ais, spectateurs de la finale du slopestyle
DATE: 2014-02-13
URL: http://www.ledauphine.com/skichrono/2014/02/13/les-reactions-des-francais-spectateurs-de-la-finale-du-slopestyle
PRINCIPAL: 170256
TEXT:
> News
SOTCHI 2014 / SKI SLOPESTYLE Les réactions des Français, spectateurs de la finale du slopestyle
Pas de Français en finale du ski slopestyle . Le niveau de la qualification était trop élevé.
Jules Bonnaire : « un jour sans »
3Oe des qualifications du slopestyle, Jules Bonnaire accusait un peu le coup. « Je suis super déçu de ce que je viens de faire. C�??est le jeu, ça arrive mais j�??ai rarement abordé une compétition comme ça. C�??était une pression à gérer. D�??habitude, j�??arrive bien à la gérer. J�??ai fait des erreurs. C�??est assez dur à rider avec la neige molle qui ressemble pas à des conditions de neige en février. Je ne pense pas que c�??était une histoire de mental. C�??était un jour sans où j�??avais pas le truc. »
Antoine Adelisse « déçu mais avec du plaisir »
Vingt-septième du slopestyle et éliminé pour la finale, Antoine Adelisse éprouvait des sentiments mitigés. « Je suis évidemment déçu, même si ce sont mes premiers Jeux. J�??ai 17 ans. J�??ai pris beaucoup de plaisir, c�??est que du bonus. Je suis assez content, même si j�??aurais préféré avoir un run plus propre. Mais je ne pouvais pas espérer beaucoup plus après mes entraînements. »
Jérémy Pancras : « Ca le fait pas, c�??est pas grave »
En ballotage favorable après le premier run des qualifications du slopestyle (12e), Jérémy Pancras n�??est finalement pas parvenu à se qualifier pour la finale. Seulement 19e, le Français « y a cru ». Avant d�??ajouter : « Au second run, j�??ai vu que les scores commençaient à monter, que les mecs commençaient à gérer la pression. J�??y ai donc cru mais après je savais après mon run que rien n�??était sûr. Ca le fait pas, c�??est pas grave. C�??est un bon départ pour la suite. Je suis vraiment content d�??être là et d�??avoir pu faire ce que je voulais faire. »
Par Laurent Davier, à Sotchi | Publié le 13/02/2014 à 12:21
Vos commentaires
