TITRE: Mob City annul�e par TNT apr�s sa premi�re saison
DATE: 2014-02-13
URL: http://www.cinemovies.fr/actu/mob-city-annulee-par-tnt-apres-sa-premiere-saison/26505
PRINCIPAL: 169839
TEXT:
15H35 Le 11/02/14 S�ries 0 publi� par Nicolas Germain
Mob City annul�e par TNT apr�s sa premi�re saison
Diffus�e en d�cembre 2013, Mob City n'aura finalement surv�cu que le temps d'une saison de 6 �pisodes. Apr�s des audiences d�cevantes, TNT a d�cid� de ne pas la renouveler pour une saison 2.
Mob City n'aura eu qu'une seule saison de 6 �pisodes � TNT
Tweet
Retrouvez aussi l'actu cin�ma sur notre page Facebook
Alors que Frank Darabont se lan�ait en d�cembre dernier dans des poursuites contre AMC suite � son licenciement de The Walking Dead il y a plus de deux ans, son autre s�rie, Mob City , peinait � d�coller sur TNT . Les deux premiers �pisodes n'avaient en effet r�uni que 2.3 millions de t�l�spectateurs, mais les suivants avaient fait beaucoup moins bien (1.4 millions).
Une vraie d�ception et un nouveau camouflet pour Darabont, qui a d�cid�ment du mal � s'installer durablement � la t�l�vision. Il avait pourtant esp�r� qu'� la mani�re de The Walking Dead , une premi�re saison de 6 �pisodes permettrait de montrer le potentiel de sa s�rie avant une saison 2 plus longue qui d�velopperait plus encore l'univers mis en place.
TNT vient pourtant en toute logique d'annuler Mob City, faute d'audiences suffisantes. En cause �galement, la qualit� de la s�rie qui, si elle �tait r�ussie visuellement, p�tissait d'un manque de rythme �vident.
Cela dit, TNT avait annonc� avant la diffusion de Mob City qu'il s'agissait d'une "s�rie limit�e", dont les 6 �pisodes pouvaient se suffire � eux-m�mes. Elle avait peut-�tre d�j� dans l'id�e de ne pas la renouveler.
Plus sur
