TITRE: JO/Artistique: clap de fin pour Plushenko, Hanyu a le premier r�le - Sportquick
DATE: 2014-02-13
URL: http://www.sportquick.com/actualite-afp/patinage-glace-oly-2014-mes
PRINCIPAL: 171733
TEXT:
< Retour
� AFP/Adrian Dennis
Le Russe Evgueni Plushenko salue la foule apr�s son abandon sur le programme court des jeux Olympiques de Sotchi, le 13 f�vrier 2014
La superstar russe Evgueni Plushenko a mis un terme � une carri�re exceptionnelle jeudi apr�s avoir renonc� � la toute derni�re minute � patiner le programme court des jeux Olympiques de Sotchi, o� le prodige Japonais Hanyu a �t� �poustouflant.
Pour son dernier rendez-vous avec les Jeux, sa comp�tition maudite, Brian Joubert, a assur� pour terminer 7e (85,84 pts). Florent Amodio a lui �t� bien en dessous (13e, 75,58 pts).
Joubert, champion du monde 2007, n'a d'autre objectif que de sortir enfin la t�te haute du grand rendez-vous en n'ayant pas failli et en ayant pris du plaisir. Le sextuple m�daill� mondial de 29 ans a tenu sa feuille de route en ayant pass� ses difficult�s techniques (quadruple d'entr�e, triple axel et triple lutz) devant un public qui l'adore et qui le lui a largement montr�.
Amodio a jou� petit, en transformant son quadruple en triple saut et un triple saut en double. Cela a s�v�rement affaibli sa note et le Top 10 final devient illusoire.
Plushenko, "le seul et l'unique" comme il aime � parler de lui, a quitt� la sc�ne par la petite porte apr�s avoir renonc� � patiner son programme court.
Apr�s s'�tre pr�sent� sur la glace pour les 6 minutes d'�chauffement, le double champion olympique a ressenti des douleurs au dos suite � quelques sauts. C'en �tait fini de sa journ�e, de ses Jeux, de sa carri�re.
Quatre m�dailles olympiques
� AFP/Yuri Kadobnov
Le Japonais Yuzuru Hanyu participe au programme court des �preuves de patinage artistique, le 13 f�vrier 2013 � Sotchi
Le patineur de 31 ans a n�anmoins r�ussi un exploit incroyable lors de ces Jeux en ayant accroch� une quatri�me m�daille olympique � son palmar�s alors qu'il a �t� op�r� du dos il y a plus d'un an et qu'il n'a fait que tr�s peu de comp�titions sur cette olympiade, entre blessures et break.
A Sotchi, le triple champion du monde, qui a d�but� sur des patins � l'�ge de 4 ans � Volgograd, s'est offert l'or sur la nouvelle �preuve par �quipes dimanche pour devenir le deuxi�me patineur de l'histoire � d�tenir quatre breloques aux JO.
Une magnifique conclusion pour ce champion, trop arrogant pour certains, tr�s timide pour d'autres, qui n'imaginait pas des Jeux chez lui en Russie sans lui.
L'�l�ve fid�le d'Alexei Mishin depuis 20 ans � Saint-P�tersbourg, a fait tout ce qu'il fallait pour avoir l'unique billet disponible pour Sotchi, sur fond de pol�mique.
Mais qu'importe, et qu'importe un public d��u et surpris d'un tel forfait, Plushenko a son nom tout en haut de la liste des grands patineurs de ce monde.
Yuzuru Hanyu, qui s'entra�ne depuis le printemps 2012 avec l'�minent Canadien Brian Orser, a �tabli un nouveau record de points historique (101,45), avec une avance de 4 points sur le triple champion du monde en titre, le Canadien Patrick Chan (97,52).
�
