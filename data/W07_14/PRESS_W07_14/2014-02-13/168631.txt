TITRE: En 2011, l'UMP et Jean-Fran�ois Cop� voulaient parler de "genre" d�s la maternelle � metronews
DATE: 2014-02-13
URL: http://www.metronews.fr/info/en-2011-l-ump-et-jean-francois-cope-voulaient-parler-de-genre-des-la-maternelle/mnbl!1NC3OGFokHIxM/
PRINCIPAL: 0
TEXT:
Mis � jour  : 13-02-2014 06:46
- Cr�� : 12-02-2014 21:28
Quand l'UMP et Cop� voulaient parler de "genre" d�s la maternelle...
OUPS - Depuis plusieurs semaines, l'UMP et Jean-Fran�ois Cop� tirent � boulets rouges contre les ABCD de l��galit� mis en place par le gouvernement. Pourtant, En 2011, le parti �tait pr�t � lutter contre les st�r�otypes de genre.
Tweet
�
"Pour l'UMP, il n'a jamais �t� question d'enseigner le�genre�� l'�cole mais tout au contraire de sensibiliser les enfants � l'alt�rit� sexuelle et � la richesse et � la compl�mentarit� des diff�rences entre les gar�ons et les filles", s'est d�fendu dans la soir�e Jean-Fran�ois Cop�. Sauf que...� Photo :�AFP
"Amener les enfants � se sentir autoris�s � adopter des conduites non st�r�otyp�es." Enonc�e de la sorte, la proposition ressemble mot pour mot � l'argumentaire que le gouvernement mart�le depuis plusieurs jours pour d�fendre son� "ABCD de l'�galit�" . Un outil p�dagogique exp�riment� depuis la rentr�e 2013 dans 600 classes pour lutter contre les st�r�otypes. Et qui fait grincer des dents dans les rangs de l'UMP. Or, cette mesure� contre laquelle Jean-Fran�ois Cop� temp�te ces jours-ci �mane tout simplement... de son parti.
Un utilisateur de Twitter a en effet fait cette d�couverte mercredi : en 2011, l'UMP avait organis� une convention sur l'�galit� hommes-femmes dans le cadre de ses Etats g�n�raux, �tape pr�alable � l'entr�e en campagne de Nicolas Sarkozy. "Le premier objectif de la promotion de l��galit� des sexes et du respect hommes/femmes d�s la maternelle est d�amener les enfants � se sentir autoris�s � adopter des conduites non st�r�otyp�es", explique le document . "Il faut aider les filles et les gar�ons � percevoir positivement leur�genre�et celui du sexe oppos�", est-il ajout�.
"Merci � l'UMP 2011"
"Le second objectif est d'accro�tre les capacit�s des enfants � r�soudre de fa�on non violente et coop�rative des conflits qui mettent en cause l'appartenance � l'un ou l'autre sexe ainsi que de promouvoir le respect entre les hommes et les femmes. Agir sur une population jeune reste en effet le meilleur moyen d'endiguer la naissance de comportements inacceptables chez les adolescents puis chez les adultes", poursuit la proposition�UMP. Si Jean-Fran�ois Cop� n'avait pas pilot� cette convention, le patron de l'UMP�s'�tait n�anmoins f�licit� du travail effectu�. Et avait exprim� l'envie d'"aller au fond des choses" sur ce dossier. Une aubaine pour le PS qui, trois ans plus tard, a trouv� une parade pour r�pliquer au patron de l'opposition.
"L'UMP�aurait donc �t� un parti capable de faire, sur des sujets de soci�t�, des propositions constructives dans le souci d'accompagner ces �volutions !", a ironis� la porte-parole du PS Laurence Rossignol mercredi soir. M�me rengaine du c�t� de Najat Vallaud-Belkacem sur Twitter : "Merci � l'UMP 2011 de soutenir si ardemment les ABCD de l'�galit� qu'elle conspue aujourd'hui."
"Pour l'UMP, il n'a jamais �t� question d'enseigner le�genre�� l'�cole mais tout au contraire de sensibiliser les enfants � l'alt�rit� sexuelle et � la richesse et � la compl�mentarit� des diff�rences entre les gar�ons et les filles", s'est d�fendu dans la soir�e Jean-Fran�ois Cop�. Avant de pr�ciser :  "Contrairement � ce que souhaite imposer le gouvernement, nous ne sommes pas pour l'indiff�renciation des sexes".
Quand Jean-Fran�ois Cop� introduisait la convention UMP sur l'�galit� homme-femme
