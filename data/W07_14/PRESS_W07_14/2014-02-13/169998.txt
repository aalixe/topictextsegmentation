TITRE: Flappy Bird : l'application envahit le march� noir des smartphones | Atlantico
DATE: 2014-02-13
URL: http://www.atlantico.fr/atlantico-light/flappy-bird-application-envahit-marche-noir-smartphones-980792.html
PRINCIPAL: 169995
TEXT:
ATLANTICO BUSINESS avec JM Sylvestre CULTURE Le gouvernement Valls Disparition du vol MH370 de Malaysia Airlines Fran�ois Hollande change son gouvernement �lections municipales 2014 NSA : le scandale des �coutes am�ricaines Affaire des �coutes de Nicolas Sarkozy La tribune de Nicolas Sarkozy Ukraine : Kiev s'embrase La pollution � Paris Sarkoleaks, les enregistrements pirates Election pr�sidentielle de 2017 Accident de Michael Schumacher
Flappy Bird : l'application envahit le march� noir des smartphones
Le jeu a �t� retir� des plateformes de t�l�chargement. R�sultat : des t�l�phones sur lesquels il est install� se vendent � prix d'or sur la Toile.
Le monde est fou
RSS
�
La folie Flappy Bird�s�amplifie. Quelques jours apr�s le retrait du jeu�de l'Apple Store et de Google Play, l'application a investi le march�noir. Sur la Toile, des internautes ayant d�j� install� le jeu sur leurs tablettes ou smartphones vendent ces derniers � prix d�or. Flappy Bird subit d�sormais une inflation de 150%, � plus de 1 500% par rapport � son prix d�achat neuf !
Ainsi, sur Ebay, un iPhone 4S aurait �t� vendu 3 550 euros, tandis qu�un iPad mini aurait �t� propos� pour la somme de 2 200 euros. Le Leboncoin.fr n�est pas non plus exempt de propositions farfelues. Un internaute aurait vendu son Samsung Galaxy Note 3 pour 2 500 euros, un autre son iPhone 5S pour 2 000 euros.
Si Flappy Bird n'est plus t�l�chargeable, ceux qui l'avaient d�j� install� y ont toujours acc�s.�Nguyen Ha Dong, le d�veloppeur vietnamien � l'origine du jeu, avait annonc� samedi son intention de le retirer des plateformes. "Flappy Bird est mon succ�s mais il a aussi ruin� ma vie simple. Alors je le d�teste maintenant", a-t-il �crit sur Twitter.�"Ce n'est pas du tout li� aux questions juridiques. Je ne peux simplement plus le garder", a-t-il ajout�.
