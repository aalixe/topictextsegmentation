TITRE: Lyon Lens en direct - Saison 2014, 1 8 De Finale - Coupe De France - Football
DATE: 2014-02-13
URL: http://sport24.lefigaro.fr/livescore/football/coupe-de-france/2014/1-8-de-finale/lyon-lens/live
PRINCIPAL: 171713
TEXT:
Le Lyonnais prend sa chance aux 25 mètres mais le tir est dévissé.
575216
90'+3
Et l'égalisation lensoise de VALDIVIA !!!
Le milieu de terrain du RCL prend Vercoutre à contre pied d'un plat du pied gauche.
575215
90'+2
Penalty pour Lens !!!!
Coulibaly échappe à Zeffane après un premier tacle de Koné, et le latéral droit lyonnais commet la faute !
575214
Il y aura 4 minutes de temps additionnel au minimum dans cette seconde période.
575213
Le jeu est arrêté après l'intrusion d'un supporter sur la pelouse...
575211
Dernier changement lyonnais avec l'entrée en jeu de Danic à la place de Grenier.
575209
89'
Oh le danger sur un corner lensois !
Les Nordistes tentent le tout pour le tout sur corner mais Coulibaly voit sa tête sortir hors-cadre.
575208
88'
Oh le contre lyonnais !
Briand part à la limite du hors-jeu et veut servir Lacazette, qui avait fait l'effort. Mais Yahia est bien revenu sur l'attaquant lyonnais.
575207
87'
Oh le tir de Lacazette !!
L'attaquant lyonnais enroule un ballon du droit aux 25 mètres, mais ne parvient pas à attraper le cadre !
575206
Longue séance de conservation du ballon pour l'OL !
575205
83'
Le jeu lensois manque de vitesse pour surprendre l'équipe lyonnaise, qui se dirige tranquillement vers la qualification maintenant.
575204
82'
Bourigeaud touche le ballon en dernier dans son duel avec Bedimo. Mais M. Turpin ne l'a pas vu et siffle une sortie de but en faveur d'Aréola et des Lensois.
575202
81'
Dernier changement du côté du RCL avec l'entrée en jeu de Cyprien à la place de Nomenjanahary.
575201
79'
Le cou franc de Grenier !
Le milieu de terrain adresse un bon ballon tendu dans la surface de réparation lensoise mais c'est dégagé par la défense nordiste !
575199
77'
Carton jaune pour Landre, côté lensois, alors que le défenseur nordiste a empêché Lacazette de filer au but.
575197
Les Lyonnais, quant à eux, ont tendance à baisser en intensité durant cette période.
575196
75'
Les joueurs sont entrés dans le dernier quart d'heure, une période qui réussit en général bien aux Lensois, qui ont déjà marqué 8 fois en Ligue 2 durant les 15 dernières minutes.
575195
74'
C'est au-dessus pour Grenier !!
Le coup de pied droit du milieu de terrain lyonnais passe bien au-dessus des cages d'Areola !
575193
Zeffane prend la place de Lopes avant ce coup franc, pour les Lyonnais.
575192
72'
Lacazette obtient un bon coup franc pour l'OL, plein axe et aux 30 mètres. Grenier va s'exécuter.
575191
70'
Le jeu reste assez haché dans cette seconde période. Les équipes ne se livrent pas totalement, alors que Lens doit jouer le tout pour le temps maintenant.
575189
68'
Côté lyonnais, c'est Lacazette qui prend la place de Gomis à la pointe de l'attaque lyonnaise.
575187
66'
Deux changements consécutifs pour les Lensois : Coulibaly remplace Ljuboja et N'Diaye prend la place de Chavarria.
575184
65'
Oh le cafouillage dans la défense lyonnaise !
Vercoutre manque sa sortie et permet à Landre de reprendre le ballon dans la surface de réparation ! C'est sauvé par Umtiti sur sa ligne !
575183
Nouveau corner en faveur des Nordistes !
Nomenjanahary déborde Lopes, mais le latéral lyonnais effectue un retour salvateur.
575182
62'
Et le tir de Gonalons !!
Le capitaine lyonnais prend sa chance aux 25 mètres mais son tir du droit à ras de terre ne surprend pas Aréola, qui s'est bien couché.
575181
61'
Lopes et Briand combinent sur le flanc droit mais le centre du Portugais est trop long...
575180
59'
Les Lyonnais semblent procéder en contres dans cette seconde période... mais attention aux velléités lensoises, un peu plus marquées qu'en première mi-temps.
575179
