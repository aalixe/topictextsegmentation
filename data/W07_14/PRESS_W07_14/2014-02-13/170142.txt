TITRE: Dieudonn� somm� de censurer ses vid�os
DATE: 2014-02-13
URL: http://www.01net.com/editorial/614064/dieudonne-somme-de-censurer-ses-videos/
PRINCIPAL: 170137
TEXT:
Actualit�s �>� Droit et conso
Dieudonn� somm� de censurer ses vid�os
Le tribunal de grande instance de Paris a ordonn� � Dieudonn� de retirer deux passages de la vid�o � 2014 sera l?ann�e de la quenelle �, � la suite d?une plainte d�pos�e par l?Union des Etudiants Juifs de France.
agrandir la photo
Le 31 d�cembre dernier, Dieudonn� mettait en ligne une nouvelle vid�o pol�mique sur sa cha�ne Youtube iamdieudo2 : � 2014 sera l�ann�e de la quenelle !!! �. Vue plus de 3,4 millions de fois � ce jour, elle comprend deux passages qui constituent pour le tribunal de grande instance de Pairs � une provocation � la haine raciale � et une �contestation de crimes contre l�Humanit� �. En cons�quence, l�humoriste a �t� somm� de retirer ces deux extraits. Voici la retranscription de l�un deux :
��Je n�ai pas � choisir entre les Juifs et les Nazis. Je suis neutre dans cette affaire. J��tais pas n� en 1900� machin. Je suis n� en 1966. Qu�est-ce qui s�est pass�?� Moi... Qui a provoqu� qui�? Qui a vol� qui�? Pffuuut. j�ai ma p�tit id�e, m�enfin��(rires) �
Youtube campe sur sa position
C�est l�Union des Etudiants Juifs de France qui est � l�origine de cette d�cision judiciaire. L�association avait en effet d�pos� plainte le 10 janvier dernier pour ��contestation de crime contre l�Humanit�, diffamation publique raciale, injure publique et provocation publique � la haine raciale �.
Par ailleurs, la vid�o avait �t� identifi�e par la communaut� YouTube comme potentiellement offensante ou choquante. La mention � Il vous appartient de choisir de visionner ce contenu ou pas � pr�c�dait donc sa lecture. Le Pr�sident de l�UEJF Sacha Reingewirtz avait cependant d�clar� : � Nous en appelons � la responsabilit� des acteurs du net et en particulier de Youtube pour cesser de donner une place de choix aux vid�os de Dieudonn�, � l�instar de la vid�o que nous attaquons qui avait �t� diffus�e dans la newsletter hebdomadaire du site Youtube �.
Interrog� par l'AFP, YouTube n'a pas souhait� s'exprimer, ajoutant toutefois : "Nous ne retirons aucun contenu tant qu'il n'enfreint pas les r�gles de la communaut� YouTube ou qu'il n'est pas manifestement illicite au regard du droit local, par exemple si le contenu a fait l'objet d'une d�cision de justice".
Pourtant, Youtube n'avait pas h�sit� � retirer le clip de Dieudonn� Shoa nanas au mois de janvier dernier, apr�s une condamnation de la justice en appel. Visiblement, les deux nouveaux extraits mis en cause ne constituent pas pour le site une infraction suffisante aux r�gles de la communaut� Youtube....
A lire aussi :
