TITRE: Pourquoi Docteur
DATE: 2014-02-13
URL: http://www.pourquoidocteur.fr/Protheses-auditives---mieux-rembourser-ou-baisser-les-prix--5397.html
PRINCIPAL: 169691
TEXT:
Proth�ses auditives : mieux rembourser ou baisser les prix ?
publi� le 13 F�vrier 2014 par Philippe Berrebi
D�clin cognitif pr�coce, d�pression, isolement, les personnes victimes de troubles de l�audition repr�sentent un enjeu de sant� publique, mais les pouvoirs publics et la s�curit� sociale restent sourds � leurs revendications. R�unis � Paris cette semaine, nous apprend Le Parisien, les audioproth�sistes ont r�clam� une fois encore une meilleure prise en charge par l�assurance maladie.
Pour une proth�se, qui co�te en moyenne 1 535 euros, la s�cu d�bourse 120 euros et les compl�mentaires 350 euros. Au final, compte le quotidien, le patient doit sortir de sa poche 1 000 euros. Une somme non n�gligeable qui emp�che la moiti� des 3 millions de malentendants qui ont besoin d'une proth�se de s��quiper.
Et ��lorsque vous entendez mal, confie au journaliste le Pr Bruno Frachet, chirurgien ORL � l'h�pital Rothschild de Paris, le d�clin cognitif est plus pr�coce, de l�ordre de 7 ans, ce qui acc�l�re l�entr�e dans la d�pendance��.
Alors, les audioproth�sistes ont fait leurs calculs : en augmentant la participation de la s�curit� sociale � 600, 700 euros, la d�pense suppl�mentaire pour la collectivit� serait de 250 millions d�euros.
Mais ces professionnels n��voquent pas les conclusions d�un rapport de la Cour des Comptes qui d�non�ait en septembre 2013 ��des prix opaques et une concurrence entre les principaux producteurs limit�e��.
De m�me, les professionnels de l�audition ont�ils oubli� l� enqu�te men�e l�an pass� par le Parisien avec un cabinet d�experts qui concluait que des proth�ses qui co�tent 50 euros au d�part de la Chine peuvent vendues jusqu�� 3 000 euros sur le march� fran�ais. Et cette���culbute�� incomberait en grande partie� aux audioproth�sistes�!
Sur le m�me th�me
