TITRE: Natalie Portman provoque «une invasion d'étrangers» à Jérusalem
DATE: 2014-02-13
URL: http://www.lefigaro.fr/cinema/2014/02/13/03002-20140213ARTFIG00202-natalie-portman-provoque-une-invasion-d-etrangers-a-jerusalem.php
PRINCIPAL: 0
TEXT:
le 13/02/2014 à 13:50
Natalie Portman sur le tournage de A Tale of Love and Darkness.
L'actrice a commencé le tournage de son premier long métrage en Israël, ce qui ne réjouit pas une communauté ultra-orthodoxe.
Publicité
Natalie Portman ne passe pas inaperçue dans les rues de Jérusalem. En plein tournage depuis peu de son premier film, A Tale of Love and Darkness, l'actrice s'est déjà mis à dos une communauté religieuse. Des habitants ultra-orthodoxes accusent l'actrice et réalisatrice de provoquer «une invasion d'étrangers» dans leur quartier de Nahlaot, rapporte The Guardian . Harassés, ils ont écrit une lettre à l'une des députés, Rachel Azaria, pour tenter de faire interdire les prises de vue.
«Le tournage du film a lieu dans des espaces sensibles, proches de synagogues ou de yeshivas, les scènes qui sont filmées là-bas auraient dû être préalablement analysées avant le début de la réalisation afin d'être sûr qu'elles ne heurteront pas la sensibilité de certaines personnes», indique cette lettre. Les habitants soulignent que les autorités ne les ont jamais prévenus de ce tournage. Ils l'auraient découvert il y a quelques jours seulement.
330.000 euros de subventions
Les ultra-orthodoxes auraient tenté de perturber le tournage en peignant des graffitis coléreux dans le quartier, indique The Times . «Il y a une tension constante entre le désir d'honorer la diversité, l'intérêt porté à Jérusalem et les tentatives des extrémistes d'éviter cela, a expliqué Rachel Azaria. L'attrait de la ville, son architecture unique et les efforts de l'industrie du film et de la télévision triompheront. L'essor du cinéma que nous avons pu constater à Jérusalem, ces dernières années, continuera sur sa lancée grâce à la présence de Natalie Portman à Nahlaot.» La star a reçu plus de 330.000 euros de subventions de la part de la ville pour accueillir le tournage de A Tale of Love and Darkness, précise la presse britannique.
L'actrice adapte le roman autobiographique d'Amos Oz, publié en 2002, traduit en 28 langues et vendu à plusieurs millions d'exemplaires à travers le monde. Le film retrace l'enfance de l'auteur pendant la naissance de l'État israélien. Natalie Portman habitera le temps du tournage en Israël, pays qu'elle a quitté à trois ans pour rejoindre les États-Unis. Son mari Benjamin Millepied et leur fils Aleph l'accompagnent.
