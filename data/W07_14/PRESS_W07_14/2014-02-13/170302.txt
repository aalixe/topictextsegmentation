TITRE: Afghanistan: la lib�ration de 65 talibans provoque la col�re de Washington - BFMTV.com
DATE: 2014-02-13
URL: http://www.bfmtv.com/international/lafghanistan-libere-65-talibans-colere-etats-unis-709862.html
PRINCIPAL: 170301
TEXT:
r�agir
Ils ont quitt� l'enceinte des murs de la prison de Bagram ce jeudi matin. Les autorit�s afghanes ont lib�r� 65 combattants talibans pr�sum�s. Une d�cision qui a suscit� les vives protestations des Etats-Unis qui voient en eux une menace pour la s�curit� du pays.
�� �
Kaboul avait indiqu� le 9 janvier dernier qu'un total de 72 d�tenus de la prison de Bagram, pr�s de la capitale Kaboul, seraient rel�ch�s en raison d'un manque de preuves contre eux, ce qui avait d�clench� de vives protestations de responsables am�ricains.
Des prisonniers dangereux, estiment les Etats-Unis
Selon les Etats-Unis, ces prisonniers sont des "personnes dangereuses"  directement li�es � des attaques meurtri�res contre des soldats de  l'Otan et des membres des forces nationales afghanes.
�� �
Plus t�t cette semaine, les autorit�s afghanes ont indiqu� avoir  "r��tudi�" les dossiers de ces d�tenus � la lumi�re des plaintes des  Etats-Unis.�� �
�� �
La lib�ration de ces  65 prisonniers est un "pas en arri�re pour l'�tat de droit en  Afghanistan" et "une source d'inqui�tude l�gitime" pour les forces de  s�curit� afghanes et internationales confront�es � l'insurrection  talibane, ont r�agi cette semaine les forces am�ricaines.
�� �
Le contr�le de l'essentiel de la prison de Bagram, baptis�e la  "Guantanamo d'Orient", a �t� transmis aux autorit�s afghanes par les  Etats-Unis en mars 2013. Les Etats-Unis contr�lent toutefois encore la portion de cette prison  o� sont d�tenus des combattants pr�sum�s non-afghans, notamment des  Pakistanais.
A lire aussi
