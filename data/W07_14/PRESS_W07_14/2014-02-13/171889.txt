TITRE: Heineken vs Kronenbourg, la guerre des bières - Europe1.fr - Consommation
DATE: 2014-02-13
URL: http://www.europe1.fr/Consommation/Heineken-vs-Kronenbourg-la-guerre-des-bieres-1801073/
PRINCIPAL: 171888
TEXT:
Heineken vs Kronenbourg, la guerre des bi�res
Par Fr�d�ric Frangeul avec AFP
Publi� le 13 f�vrier 2014 � 21h27 Mis � jour le 13 f�vrier 2014 � 21h32
� MaxPPP
QUI EST LE PREMIER ? Les deux marques rivalisent de chiffres pour affirmer leur leadership dans l'Hexagone.
Heineken et Kronenbourg continuent de se livrer une bataille f�roce, le premier assurant jeudi �tre devenu la bi�re la plus vendue en France, le second contestant quelques heures plus tard. La marque n�erlandaise Heineken assure d�tenir d�sormais 18,4% des parts de march� en valeur et pour la premi�re fois une majorit� des parts de march� en volume (17,3%), selon le barom�tre Iri 2013, qui compile les ventes en hyper et supermarch�s, soit 75% des ventes de bi�re en France.
Kronenbourg, marque fran�aise du brasseur danois Carlsberg, d�tient, selon son rival, 16,2% du march� hexagonal en volume et 12,5% en valeur, selon Heineken. Mais Kronenbourg assure d�tenir toujours 17,6% des parts de march� en volume. Faux, r�torque Heineken qui assure que le chiffre de 17,6% ne contient pas seulement la "Kro" classique, la blonde, mais d'autres produits de la gamme comme une Kronenbourg sans alcool ou une autre ambr�e. Iri, qui a r�alis� ce barom�tre, n'�tait pas joignable dans l'imm�diat mais Kronenbourg devra s'expliquer pr�cis�ment sur ses chiffres lors d'une pr�sentation � la presse pr�vue mercredi � l'occasion des 350 ans de la marque.
En 2012, le groupe Heineken (marques Heineken, Desperados, Pelforth, Affligem, Fischer...) estimait d�j� �tre devenu le premier brasseur en France. En 2013, les ventes de bi�re en France ont recul� de 3% en raison de l'augmentation des taxes (+160%) qui �tait intervenue en d�but d'ann�e. Le demi au comptoir a particuli�rement souffert avec un march� en repli de 7% dans les caf�s-h�tels-brasseries. Une situation qui a surtout touch� les grandes marques de bi�re, plus pr�sentes dans les caf�s que les petits brasseurs.
