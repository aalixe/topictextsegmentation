TITRE: Mort ou vif ? Confusion sur l'état du Lapin de Jade - 13 février 2014 - Sciences et Avenir
DATE: 2014-02-13
URL: http://www.sciencesetavenir.fr/espace/20140213.OBS6238/mort-ou-vif-confusion-sur-l-etat-du-lapin-de-jade.html
PRINCIPAL: 169530
TEXT:
Accueil > Espace > Mort ou vif ? Confusion sur l'état du Lapin de Jade
Mort ou vif ? Confusion sur l'état du Lapin de Jade
L'agence de presse chinoise CNS le disait d�finitivement hors service hier. Un porte-parole du programme spatial annonce aujourd'hui que "Yutu est revenu � la vie" !
Yutu, sur le stand de la foire industrielle de Shanghaï AFP
COMEBACK.�Mais comment va-t-il ? Ce jeudi 13 f�vrier, les m�dias chinois annoncent la renaissance de leur robot lunaire, le Lapin de Jade. Celui-l� m�me que l'Empire du milieu - par la voix de l'agence de presse China News Service (CNS) - pr�sentait hier mercredi comme d�finitivement hors service.
Pei Zhaoyu, porte-parole du programme spatial chinois, annonce ainsi que Yutu - l'autre nom du Lapin de Jade - est "revenu � la vie" . Concr�tement, cela signifie que le v�hicule lunaire, qui avait �t� plac� en "sommeil" depuis qu'il rencontrait des probl�mes m�caniques, survenus le�25 janvier 2013 , a �t� r�activ� ; les ing�nieurs chinois communiquent donc � nouveau avec lui depuis la Terre.
En revanche, les m�dias chinois pr�cisent que la fameuse panne m�canique n'a toujours pas �t� r�par�e. Mais, pour l'heure, Pei Zhaoyu d�livre un message r�solument optimiste : "il y a toujours une chance que le rover soit sauv�". Il faut dire que la perte de Yutu serait, au moins symboliquement, un coup dur pour le programme spatial chinois,�alors que le pays se f�licitait en d�cembre 2013 du "succ�s total" de sa mission lunaire , � l'issue de laquelle la sonde Chang'e 3 s'�tait pos�e sur notre satellite, pour y d�poser le Lapin de Jade.
Partager
