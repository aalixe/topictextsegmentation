TITRE: Mpman MPDC706 : tablette � 49,90 euros avec certification Google
DATE: 2014-02-13
URL: http://www.infos-mobiles.com/mpman-mpdc706/mpman-mpdc706-tablette-4990-euros-avec-certification-google/51251
PRINCIPAL: 170396
TEXT:
Accueil � Mpman MPDC706 : tablette � 49,90 euros avec certification Google
Mpman MPDC706 : tablette � 49,90 euros avec certification Google
Isa 13 f�vrier 2014 0
La marque d�origine asiatique� Mpman est tr�s populaire�depuis quelques ann�es�sur le segment des tablettes tactiles �d�entr�e de gamme ,le�constructeur Mpman�vient d�annoncer sa� nouvelle tablette tactile baptis�e MPDC706,comme d�habitude chez Mpman les prix sont vraiment attractifs,Mpman commercialise cette tablette � 49,90 euros.
�
Malgr� son tout petit prix ,il est � noter que cette tablette tactile MPDC706 elle est enfin certifi�e�Google,et int�gre donc les applications officielles dont le Play Store, chose rare � un tel tarif ,MPDC706 c�est la premi�re tablette avec certification,embarque�un �cran�TN�de 7 pouces�en 800 x 480 pixels,est aliment� d�un processeur double c�ur Cortex-A7 � 1,5 GHz tandis que le GPU est un MALI 400MP2,il dispose de 512 Mo de m�moire vive (RAM),et 4 Go de flash NAND l�appareil afficherait des�dimensions de 192 x 116 x 10,5 mm , il dispose d�une webcam 0,3 M�gapixel, un port Micro-USB 2.0,une prise mini-jack, une m�moire flash de 4 Go et un slot MicroSD ( jusqu�� 32 Go),le tout sous�Android 4.2�Jelly Bean,la batterie int�gr�e est �d�une capacit� de 2600 mAh avec laquelle le constructeur nous a assur� 4-5 heures en vid�o, 11 heures audio et�7 heures pour un usage mixte�( e-mail, surf, vid�o et jeu ).et pour ceux qui aimeraient se faire une id�e des performances � l�aide d�un benchmark, Mpman indique un score de�8 741 sous AnTuTu,une puce Wi-Fi 802.11n est int�gr�e alors que le port USB 2.0 est de type�Host�: on peut donc y connecter une souris, un clavier ou encore un�disque dur externe�par exemple,la connectique reste assez compl�te avec le classique,lecteur de carte�microSDHC permet d�installer jusqu�� 32 Go suppl�mentaires,un haut-parleur 1 Watt,et encore une liaison Wi-Fi ,un support des cl�s 3G+.
le�constructeur�MPman�indique aussi qu�il disposera un autre mod�le de tablette de 10? pouces d�s le mois de mai prochaine cette sera commercialis�e d�un tarif de 79,90 euros .
La tablette MPDC706�est un produit d�appel qui par son design tout en finesse�s�duit et par son prix de vente attractif 49,90 euros.
