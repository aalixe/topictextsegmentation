TITRE: Saint-Valentin version Vatican : un coussin nuptial et un discours papal pour 20 000 ��fianc�s�� | Digne de foi
DATE: 2014-02-13
URL: http://religion.blog.lemonde.fr/2014/02/13/saint-valentin-version-vatican-un-coussin-nuptial-et-un-discours-papal-pour-20-000-fiances/
PRINCIPAL: 0
TEXT:
La recette Loubavitch pour ramener les �tudiants juifs � la religion ?
19 commentaires � Saint-Valentin version Vatican : un coussin nuptial et un discours papal pour 20 000 ��fianc�s��
C�est curieux : en citant le magist�re romain, cet article laisse finalement un sentiment �trange, celui de lire une d�p�che de presse en provenance de la plan�te Mars. Parmi ces 10.000 couples combien usent d�j� d�un moyen contraceptif en d�pit de l�interdiction d�Humanae Vitae et combien divorceront parce que les vicissitudes de la vie sont les m�mes pour eux comme pour les autres, combien seront-ils au bout du bout ��� �tre en r�gle�� avec leur religion pour reprendre l�expression cl�ricale ? Cette r�ification de la foi est proprement effrayante.  Ou la R�forme va exploser comme au XVIe si�cle pour �chapper � cet �touffoir, ou alors ��le schisme silencieux�� (du jamais vu dans l�histoire) se poursuivra. Nous aussi nous br�lerons publiquement la bulle qui nous condamne !
R�dig� par :
| le 13 f�vrier 2014 � 15:32 | R�pondre Signaler un abus |
@ OM
Ou alors le mod�le individualiste, consum�riste et nihiliste va exploser. Je ne le placerai pas favori, l�Eglise a 2000 ans de sagesse derri�re elle�
R�dig� par :
| le 13 f�vrier 2014 � 16:10 | R�pondre Signaler un abus |
��Sagesse��, bla-bla, oui! 2000 ans de sagesse, vraiment? Schismes pr�coces, exclusions, meurtres entre papes et pr�tendants, reniement des pr�tentions � la pauvret�, � la simplicit�, � l�amour comme vertu premi�re (la ��caritas�� de Saul-Paul), de r�duction de la femme � n��tre que l��ternelle seconde incompl�te� et l�Inquisition, le pouvoir temporel � tout crin, les guerres, que la liste serait longue encore si elle devait �tre compl�te! De quelle sagesse parlez-vous donc, si vous ne vous bercez simplement de jolis mots? Suffirait-il d��tre vieux pour �tre sage? Les dinosaures ont cumul� 100 millions d�ann�es, avant de dispara�tre. 2000 ans? rien, devant le tribunal de la pens�e. Bien s�r que le consum�risme perdra � son tour, mais pas forc�ment avant l��glise catholique ��universelle et romaine��.
R�dig� par :
| le 13 f�vrier 2014 � 17:33 | R�pondre Signaler un abus |
��l�Eglise a 2000 ans de sagesse derri�re elle���
Pour commencer, elle a moins de mille ans, l�Eglise catholique telle qu�on la connaisse aujourd�hui� Ensuite, parler de ��sagesse�� peut porter � d�bat, mais des gens rationnels devraient vite se faire une opinion � ce propos
R�dig� par :
| le 13 f�vrier 2014 � 17:36 | R�pondre Signaler un abus |
2000 ans de quoi ? Sur ces questions en particulier, l�ignorance, la volatilite des opinions et la stupidite de l�Eglise catholique n�est plus a demontrer. Ce serait bien de ne pas l�oublier, avant de sanctifier ce type qui, certes par sa bonhommie, preche toujours la conversion des masses pour un monde plus pur, sans sexe, sans violence, mais avec beaucoup de gens qui subitement remarchent, se convertissent a la naissance et s�aiment avec moderation. Le message de cette religion brille surtout par sa connerie.
R�dig� par :
| le 13 f�vrier 2014 � 17:41 | R�pondre Signaler un abus |
L�eglise montre le chemin a suivre et le but a atteindre. En aucun cas elle est la pour vous conforter dans vos choix.
Elle ne vous force aucunement a la suivre et n�est pas un organisme de repression. Les regles sont la pour vous aider et si vous y manquez, vous pouvez et vous serez pardonne si votre repentir est sincere.
Le message de Dieu tel qu�il est vehicule par l�eglise est bien loin de ce que vous semblez croire.
R�dig� par :
