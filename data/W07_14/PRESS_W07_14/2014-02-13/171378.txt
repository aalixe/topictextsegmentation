TITRE: Sept clubs lancent la fronde - Football - Sports.fr
DATE: 2014-02-13
URL: http://www.sports.fr/football/ligue-1/articles/monaco-et-la-lfp-tiennent-tete-aux-frondeurs-1009620/
PRINCIPAL: 0
TEXT:
13 février 2014 � 12h54
Mis à jour le
13 février 2014 � 19h08
La LFP pensait sans doute avoir fait le plus dur en validant, par le biais de son Conseil d’administration, un accord forfaitaire de 50 millions d’euros avec Monaco. Une manne financière jugée très largement insuffisante par sept clubs professionnels qui ne comptent pas en rester là.
Le sourire de Frédéric Thiriez aura été bien bref. Mais au fond, le président de la LFP devait bien se douter que tous les clubs de Ligue 1 n’allaient pas avaler si facilement l'accord passé entre la Ligue et Monaco . Sur le fond, l’avantage des Monégasques est estimé, de longue date, à 50 millions d’euros, alors un accord forfaitaire de 25 millions par saison sur deux ans pour permettre au club de la Principauté de ne pas déménager en France et ainsi conserver ses avantages fiscaux, cela allait forcément faire réagir. "Il est évident qu'un versement forfaitaire [...] est très insuffisant et lèse les intérêts des clubs professionnels", dénoncent ainsi sept clubs contestataires, dans une lettre que s'est procurée Le Point.
Jean-Louis Triaud ( Bordeaux ), Jean-François Fortin ( Caen ), Michel Seydoux ( Lille ), Loïc Féry ( Lorient ), Vincent Labrune ( Marseille ), Laurent Nicollin ( Montpellier ) et Jean-Claude Blanc ( PSG ) ont ainsi lancé la fronde avec la ferme intention de dénoncer un accord qui ne leur convient pas du tout. Ces sept présidents ont donc écrit une missive à l’adresse des présidents des clubs de Ligue 1 et Ligue 2, ce qui n’a évidemment pas du tout été du goût de la LFP.
"Dans ce contexte, les dirigeants de clubs professionnels soussignés ont décidé d'engager ensemble une action contentieuse contre ce qui apparaît comme un arrangement précipité, peu transparent et insuffisant, afin de faire annuler la transaction intervenue. (...) Les clubs doivent être en mesure de concourir sans que des distorsions sociales ou fiscales ne viennent fausser le jeu de la compétition", arguent-ils.
Il s’agit évidemment d’un premier coup de semonce, les clubs en question ayant apporté des arguments qui pourraient leur permettre de rallier d’autres présidents à leur cause. Pour la LFP, le problème ne fait que commencer.
