TITRE: Assurance chômage : les intermittents dans la rue le 27 février
DATE: 2014-02-13
URL: http://www.lefigaro.fr/emploi/2014/02/13/09005-20140213ARTFIG00172-assurance-chomage-la-cgt-dechire-le-projet-patronal.php
PRINCIPAL: 0
TEXT:
Envoyer par mail
Assurance chômage : les intermittents dans la rue le 27 février
VIDÉO - Le rapport de force est engagé entre les syndicats et le patronat, qui veut mettre fin au régime spécial des intermittents. Ceux-ci ont occupé quelques heures le ministère de la Culture avant d'annoncer une journée de mobilisation le 27 février.
< Envoyer cet article par e-mail
X
Séparez les adresses e-mail de vos contacts par des virgules.
De la part de :
Assurance chômage : les intermittents dans la rue le 27 février
VIDÉO - Le rapport de force est engagé entre les syndicats et le patronat, qui veut mettre fin au régime spécial des intermittents. Ceux-ci ont occupé quelques heures le ministère de la Culture avant d'annoncer une journée de mobilisation le 27 février.
J'accepte de recevoir la newsletter quotidienne du Figaro.fr
Oui
Le compte de la r�daction du Figaro.fr. Sur Twitter : @Le_Figaro
Ses derniers articles
Réagir à cet article
Publicité
deb_intermittente
"les anges de la télé réalité" ou autre bouses. (Oui, car à mon humble avis il n'y aura plus que ça à la télé si on envoie tous les intermittents au placard :) ). Bref, jai accepté de vivre dans ces conditions pour faire ce métier. Mais si les conditions dans lesquelles j'exerce se détériorent encore, je ne pense vraiment pas que je resterais en France, et si d'autres me suivent, restera ce qui restera à voir pour vos enfants à la télé... et partout ailleurs, pour vous comme pour eux.
En espérant avoir pu vous apporter un micro éclairage sur ce statut obscur n_n
tchô! =)
deb_intermittente
"je fonde ou pas une famille, lorsque je sais que mon salaire peut chuter d'un jour à l'autre?". Voilà, c'est tout ça qui se cache derrière les projecteurs des salles de théâtre où vous vous rendez le soir, c'est ça qu'il y a derrière vos films, vos émissions et que sais-je encore.
Conclusion; de toutes les personnes que je connais, nous bossons tous comme des fous. Pour avoir un cv solide, pour avoir des expériences qui auront de la valeurs auprès des prochains employeurs, pour avoir les qualités requises à devenir indispensable, pour juste, espérer toujours pouvoir retrouver un emploi. Et ce avec mille fois moins de sécurité et d'avantages que la plupart des gens, cf la partie ou j'explique que les heures sup ne sont jamais payées etc etc etc.
Voilà, c'est pour cela que ces gens sont dans la rue :) Parce qu'ils savent que si cette lois là passe, la plupart changeront de branche s'ils le peuvent, ou partiront à l'étranger. Et parce, comme vous qui souhaitez travailler dans ce dans quoi vous êtes bon, ces gens là veulent continuer à créer tout ce qui fait que d'autres peuvent se divertir. :)
Merci pour ceux qui ont eu la foi de lire ce looooooong pavé, et encore, je n'ai pas tout raconté XD. Mais quoi qu'il en soit, j'aime beaucoup mon métier et je suis fière de savoir que des enfants s'éclatent devant les films animés sur lesquels je travaille, comme je le faisais petite devant ceux d'autres gens. Et si en plus ça peut leur éviter de regarder "les anges de la (...)
Le 27/02/2014 à 15:48
deb_intermittente
un cas isolé, vu que ça arrive par exemple à quasiment tous les graphistes travaillant dans la pub ou les effets speciaux. Pas de tickets resto même lorsque la loi y oblige, parfois même pas de remboursement partiel du transport. Pourquoi ne pas saisir la loi? Pour la stupide réponse suivante: ce monde est minuscule... Ce monde est minuscule et une réputation fait vite le tour des boites. Si vous deviez rechercher un travail toutes les 3 semaines, vous prendriez le risque de vous mettre à dos les boites qui vous embauchent?
La conclusion, c'est que nous rêverions de pouvoir obtenir des cdi, comme dans la plupart des métiers. Finis le stress de renégocier son niveau de vie, le stress d'être mis de côté de profit de petits nouveaux qui acceptent d’être payés peu pour faire des heures de fous, simplement pour se faire une place dans le milieu. Fini le stress à chaque fois que tu pars 2 semaines de vacances. A ne pas savoir si tu retrouveras vite ou non un emploi en rentrant.
Vous vous souvenez de tous ces gens qui manifestent chaque jour à la télé, parce que leur usine ferme et ils n'ont potentiellement plus d'emploi? Ces gens pour qui tout le monde a de la peine? Nous c'est ça, toutes les 2 à 3 semaines. Et personnes ne va dans la rue à chaque perte d'emploi. Fini de devoir faire des heures sup non payées dans l'espoir de faire bonne figure et d'être rappelé plus tard par la même boite. Fini la question de "je fonde ou pas une famille, lorsque je sais que mon salaire (...)
Le 27/02/2014 à 15:48
deb_intermittente
quoi faire, qui doit vous valider chaque étape de travail et qui sera seul décideur de si le produit lui convient ou pas, cela devient un métier complètement comme les autres. Comme vous ou n'importe qui. C'est à dire qu'on l'a choisi pour les affinités qu'on avait avec, et qu'une fois placé dans une chaîne ou vous n'êtes évidemment pas libre de décider de ce que vous voulez faire, vous êtes ouvrier, comme tout le monde, et c'est normal. C'est comme tout, il y a des bon et des mauvais points. Doit-on détester son job pour être considéré? Dois-je dire d'une secrétaire qu'elle ne travaille pas réellement, simplement parce qu'elle aime beaucoup organiser des événements et gérer des gros agendas?
Pour nos vacances c'est tout aussi galère. En CDI on a droit à 5 semaines de vacances par an, et vous pouvez les coordonner avec votre moitié pour partir avec elle. Nous c'est juste impossible. Car on ne peut pas partir d'une prod comme ça au milieu. Parce que refuser un travail pour partir en même temps que l'autre, c'est prendre le risque de ne pas retrouver un job en revenant. Allez profiter de vos vacances dans ces conditions :/
Aucune négociation possible de nos droits non plus, parce que, la plupart des boites dans lesquelles vous bosserez vous répondront que vous avez déjà de la chance d'avoir un boulot. Pour l'avoir vécu un sacré paquet de fois. Et je n'ai JAMAIS eu une seule heure supplémentaire de payé, quand bien même il s'agissait de nuits blanches. Et je suis tout (...)
Le 27/02/2014 à 15:47
deb_intermittente
ou 90euros/jours brut ou le chômage, bin tu dois accepter.
Car non, on ne se repose pas sur notre chômage. Et honnêtement, je ne connais personne dans mon entourage qui se dit "allez, j'arrête je m'en fiche, j'ai le chômage". Parce que ces jours là peuvent te sauver là vie le jour où c'est le creux de la vague. Ce qui arrive souvent parce que les gens ne veulent plus financer de projets "culturels" ou autre alors que "c''est la crise". Et quand je dis culturel, pas la peine d'imaginer une statue informe abstraite entre les 4 murs vide d'un musée contemporain. Je vous parle de vos émissions télés, des films que vous allez voir lorsque vous êtes fatigués après le travail de la musique que vous écoutez pour vous défouler, des spectacles que vous allez voir en famille ou avec des amis. et la liste est extrêmement longue.
Le fait est que quand on est intermittent, on est Précaire, pas privilégié. Et sans ce statut, personne ne prendra plus le risque de passer sa vie sur la corde raide, pour créer vos émissions, vos spectacles, vos pièces de théâtre, les dessins animés de vos enfants, vos films au cinéma. Plus personne n'illustrera les jeux et les livres de vos enfants. Personne ne voudra plus faire ça, aussi "sympa" que ça paraisse; si cela signifie être à la rue à chaque fois qu'on a plus besoin de vous, toutes les 2 semaines. Et je met "sympa" entre de gros guillemets, parce que lorsque vous travaillez dans le cadre d'une production, avec des clients qui vous dictent (...)
Le 27/02/2014 à 15:46
