TITRE: Sotchi 2014 : les Jeux en direct - JO 2014 - Sp. d'hiver - Sport.fr
DATE: 2014-02-13
URL: http://www.sport.fr/sports-d-hiver/jo-2014-sotchi-2014-les-jeux-en-direct-339762.shtm
PRINCIPAL: 169648
TEXT:
Suisse - Su�de 8 - 9
18h59 - La luge, sp�cialit� allemande
L'Allemagne a r�gn� sans partage sur les �preuves de luge des jeux Olympiques 2014 avec, ce jeudi, un quatri�me titre en autant de courses dans le relais qui faisait ses d�buts aux JO d'hiver.
18h27 - Plushenko renonce
Quelques minutes avant son programme court, le quadruple m�daill� olympique russe Evgueni Plushenko , souffrant du dos, a renonc� � participer l'�preuve de patinage artistique.
17h18 - Une Chinoise d�borde les Bataves
La Chinoise Zhang Hong a cr�� la surprise en remportant le 1000 m de patinage de vitesse devant les favorites n�erlandaises.
16h25 - Deuxi�me m�daille d'or pour Fourcade !
Martin Fourcade est devenu ce jeudi double champion olympique. Le Fran�ais a remport� la m�dialle d'or sur les 20 km � Sotchi.
16h02 - Stoch op�rationnel malgr� une chute
Le Polonais Kamil Stoch , sacr� champion olympique de saut � skis sur le petit tremplin aux JO-2014, ne se ressent pas d'une chute � l'entra�nement mercredi, a assur� jeudi son entra�neur.
14h30 - Short track : de l'or pour la Chine
La Chinoise Li Jianrou a remport� son premier titre international en s'imposant en finale du 500 m de short-track.
12h31 - Ski de fond : Kowalczyk en or
a Polonaise Justyna Kowalczyk a domin� ses rivales scandinaves pour remporter le 10 km style classique.
12h29 - Fauconnet �limin� en s�ries du 1000 m
Le N.1 Fran�ais Thibaut Fauconnet a �t� �limin� en s�ries du 1000 m des �preuves de short-track, trois jours avoir �t� sorti en demi-finales du 1500 m.
12h08 - Tripl� am�ricain en ski slopestyle
L'Am�ricain Joss Christensen a �t� sacr� champion olympique dans la toute nouvelle �preuve de ski slopestyle, jeudi � l'Extr�me Park de Rosa Khoutor, devant deux compatriotes.
11h40 - Skeleton (D): Yarnold en t�te � mi-parcours
La Britannique Elizabeth Yarnold est solidement install�e en t�te de l'�preuve de skeleton apr�s les deux premi�res manches disput�es jeudi. La laur�ate de la Coupe du monde devance l'Am�ricaine Noelle Pikus-Pace (+44/100e) et la Russe Elena Nikitina (+55/100e). La Britannique Shelley Rudman , championne du monde en titre, a perdu tout espoir de monter sur le podium. Elle pointe en 11e position, � 1 seconde 90/100e de sa compatriote. Le titre olympique sera attribu� vendredi apr�s les 3e et 4e manches.
11h20 - Le d�part du super-combin� avanc�
Le d�part du super-combin� messieurs, initialement pr�vu � 08h00 (heure fran�aise) vendredi, a �t� avanc� � 07h00 pour "minimiser les effets de la m�t�o sur les comp�titions". Les temp�ratures se sont s�rieusement radoucies depuis plusieurs jours, entra�nant une transformation de la neige.
La manche de slalom est maintenue � 18h30.
10h55 : "On ne fait pas le m�me sport"
"On a une �quipe de France qui a neuf mois d'existence alors que celle des Etats-Unis est en place depuis six ans, on ne fait pas le m�me sport qu'eux, raconte J�r�my Pancras, 19e en ski slopestyle. En plus, ce sport est vraiment am�ricain. Nous, pour nous entra�ner convenablement, il faut qu'on aille aux Etats-Unis."
Sur les 12 qualifi�s pour la finale, quatre sont Am�ricains.
9h55 - Slopestyle (M) : pas de Fran�ais en finale
J�r�my Pancras a r�alis� un meilleur second run (69,40 points) mais malheureusement insuffisant pour aller plus loin. La plupart des concurrents ont en effet am�lior� leur score � leur second passage. Antoine Adelisse est �galement loin du compte avec une note de 54,20 points. Jules Bonnaire termine 30e sur 32.
9h09 - Slopestyle (M) : qualifi� pour l'instant
Les qualifications de l'�preuve de slopestyle chez les messieurs ont d�marr� t�t ce matin avec trois Fran�ais engag�s : J�r�my Pancras, Antoine Adelisse et Jules Bonnaire. Pancras a obtenu 68 points pour son premier passage. Il est provisoirement qualifi�. Adelisse n'a r�colt� que 54.20 points. Quant � Bonnaire, il a chut� deux fois lors de son passage.
8h45 - L'Allemagne domine le tableau des m�dailles
L'Allemagne occupe la t�te du classement des nations aux Jeux Olympiques. Les Allemands comptent huit m�dailles mais surtout six en or. Le Canada suit avec quatre m�dailles d'or comme la Norv�ge, 3e et les Pays-Bas, 4e. La France reste bloqu�e � trois breloques, une en or et deux en bronze.
8h30 - Le programme du jour
06h00-09h00 Curling dames - 1er tour - Canada - Danemark, Chine - Grande-Bretagne, Suisse - Su�de
07h15-09h00 Ski Freestyle - Slopestyle messieurs - Qualifications
08h30-10h20 Skeleton dames - Manches 1 et 2
09h00-11h30 Hockey sur glace messieurs - Tour pr�liminaire : Finlande - Autriche
09h00-11h30 Hockey sur glace dames - Tour pr�liminaire : Japon - Allemagne
10h30-11h40 Ski Freestyle - Slopestyle messieurs - FINALE
11h00-13h30 Short-track - 500 m dames FINALE - 1000 m messieurs qualifications, Relais 5000 m qualifications
11h00-12h40 Ski de fond - 10 km classique dames - FINALE
11h00-14h00 Curling messieurs - 1er tour: Suisse - Russie, Canada - Danemark, Norv�ge - Su�de, Grande - Bretagne - Etats-Unis
13h30-16h00 Hockey sur glace messieurs - Tour pr�liminaire : Russie - Slov�nie
13h30-16h00 Hockey sur glace messieurs - Tour pr�liminaire : Slovaquie - Etats-Unis
15h00-17h20 Biathlon - 20 km individuel messieurs - FINALE
15h00-16h40 Patinage de vitesse - 1000 m dames - FINALE
16h00-19h00 Curling dames - 1er tour : Su�de - Danemark, Russie - Cor�e du Sud, Suisse - Canada, Japon - Etats-Unis
16h00-20h30 Patinage artistique - Messieurs - Programme court
17h15-18h30 Luge - Relais - FINALE
18h00-20h30 Hockey sur glace messieurs - Tour pr�liminaire : Canada - Norv�ge
18h00-20h30 Hockey sur glace dames - Tour pr�liminaire : Su�de - Russie
