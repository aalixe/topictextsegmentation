TITRE: COUCOU ! � Les clients de prostitu�s film�s � La Madeleine | Big Browser
DATE: 2014-02-13
URL: http://bigbrowser.blog.lemonde.fr/2014/02/13/coucou-les-clients-de-prostitues-filmes-a-la-madeleine/
PRINCIPAL: 0
TEXT:
Pour m�moire, la loi p�nalisant le racolage passif, issue du temps ou Sarkozy �tait ministre de l�int�rieur, a �t� abolie en m�me temps que celle p�nalisant le client a �t� vot�.
R�dig� par :
| le 14 f�vrier 2014 � 02:24 | R�pondre Signaler un abus |
Effectivement. Du coup j�ai du mal a saisir comment racoler peut etre desormais legal et en meme temps penaliser les clients. Un peu comme si acheter de l�alcool etait desormais interdit sans pour autant que la vente ne le soit?
R�dig� par :
| le 14 f�vrier 2014 � 23:36 | R�pondre Signaler un abus |
C�est pr�cis�ment l�int�r�t : on ne cherche pas � p�naliser les prostitu�es qui font leur travail par besoin, mais les clients. Sans demande, pas d�offre.
De m�me que si on voulait que personne ne boive, mais sans p�naliser des d�biteurs de boisson qui seraient socialement tr�s d�favoris� voire dans une position d�abus, on agirait sur le consommateur.
R�dig� par :
| le 18 f�vrier 2014 � 12:03 | R�pondre Signaler un abus |
��Sans demande, pas d�offre��. Il faut �tre tr�s na�f et totalement inconscient pour croire que la marche du monde ob�it � ce genre de principe simpliste et r�ducteur.
En attendant, ce sont les prostitu�es qui trinquent� Aujourd�hui, elles sont d�j� r�duites � faire des passes dans des parkings d�gueulasses, et demain, elles seront contraintes d�aller se r�fugier dans des endroits encore plus glauques, plus recul�s, loin de l�Etat de droit, � la merci de leurs clients.
Ah oui, et si vous croyez vraiment que le but du gouvernement est de prot�ger les prostitu�es, notamment ��les pauvres exploit�es sous la coupe de cruels prox�n�tes�� : http://melange-instable.blogspot.fr/2014/02/prostitution-rafles-de-putes-entre.html
R�dig� par :
| le 19 f�vrier 2014 � 14:19 |           |
C�est le principe des d�tecteurs de radars. On a le droit de les vendre l�galement, de les acheter l�galement, mais pas de les utiliser.
Rappelez-vous les cassettes vid�o ; on avait le droit de les lire, mais pas d�enregistrer la t�l� en France. Les constructeurs Sony et Philips n�ont pas suivi, donc c�est mort tout doucement. Mais c��tait carr�ment ill�gal. Ca l�est peut-�tre encore�
R�dig� par :
| le 25 f�vrier 2014 � 10:17 | R�pondre Signaler un abus |
Je suis contre la prostitution qui est l� esclavage de femmes presque toujours tr�s fragilis�es par la vie-je fus pendant un temps leur venereologue-
Mais je suis scandalis� par cette initiative de d�lation du maire de la Madeleine. Dans quel pays vit-on. Ce n�est pas le meilleur des mondes, c�est le pire des mondes.
R�dig� par :
| le 13 f�vrier 2014 � 18:14 | R�pondre Signaler un abus |
Quel petit joueur ce maire ! Moi et mes copines du GUF, on organise r�guli�rement des ratonnades de clients et on casse la gueule aux mecs qui changent pas de trottoir la nuit quand une femme seule marche dessus ou qui osent passer devant les coll�ges et les lyc�es aux heures de sortie des cours.
R�dig� par :
