TITRE: Marseille: la campagne dérape sur Internet
DATE: 2014-02-13
URL: http://www.lefigaro.fr/flash-actu/2014/02/13/97001-20140213FILWWW00057-marseille-la-campagne-derape-sur-internet.php
PRINCIPAL: 171060
TEXT:
le 13/02/2014 à 08:26
Publicité
Un membre du cabinet de Jean-Claude Gaudin (UMP), plus précisément une personne chargée de s'occuper d'Internet, a dérapé, en lançant des attaques personnelles sur Twitter, selon le journal La Provence .
Tout a commencé lundi, lorsque le compte @lepsmatue spécule dans un message sur la possible homosexualité d'un proche de Patrick Mennucci (PS). Après un démenti, le ton monte entre les deux équipes de campagne sur le réseau social, jusqu'au mardi, où le compte @lepsmatue lance une nouvelle attaque personnelle, cette fois contre la compagne de Patrick Mennucci : « Le stress, c'est pas bon pour le cancer. » Cette dernière est en effet malade.
Dans un communiqué Patrick Mennucci dénonce alors « l'indécence » et « la méchanceté gratuite » de ce message. Son auteur, Alix Normandin, finit par s'excuser : « Malheureusement aujourd'hui dans une ambiance de campagne municipale ultra tendue mon tweet qui n'engage que moi à titre personnel a été mal interprété, comme une insulte aux malades. J'en suis désolé et je reconnais que la formulation était maladroite : j'ai pour cela immédiatement rajouté deux tweets lui apportant tout mon soutien. »
