TITRE: Assurance-ch�mage : les n�gociations entre partenaires sociaux commencent mal
DATE: 2014-02-13
URL: http://www.lemonde.fr/emploi/article/2014/02/13/assurance-chomage-les-negociations-entre-partenaires-sociaux-commencent-mal_4365978_1698637.html
PRINCIPAL: 0
TEXT:
Assurance-ch�mage : les n�gociations entre partenaires sociaux commencent mal
Le Monde |
� Mis � jour le
13.02.2014 � 21h21
Les partenaires sociaux se sont quitt�s sur un d�saccord, jeudi 13 f�vrier, au premier jour du nouveau cycle de n�gociations sur la r�forme de l'assurance-ch�mage.
��Il n'y a rien qui se dessine ou alors c'est de l'art abstrait��, a d�clar� le n�gociateur de FO, St�phane Lardy. ��On n'a pas les m�mes conceptions du retour � l'�quilibre.�� Les syndicats ont rejet� toute d�gradation des droits des ch�meurs sous pr�texte de r�duire le d�ficit du gestionnaire de l'assurance-ch�mage, l'Unedic, qui �tait de 4�milliards d'euros en 2013 et pourrait atteindre 4,3 milliards en 2014.
Jeudi matin, les propositions du Mouvement des entreprises de France (Medef) avaient provoqu� la col�re des syndicalistes. Celles-ci vont de la modulation de l'indemnisation des ch�meurs en fonction de la conjoncture � la fin du r�gime sp�cial des intermittents, en passant par la cotisation des contractuels de la fonction publique.
Indign�, le d�l�gu� la Conf�d�ration g�n�rale du travail (CGT) est all� jusqu'� d�chirer le texte devant les cam�ras de t�l�vision. ��La CGT appelle les salari�s, les pr�caires, les demandeurs d' emploi , les int�rimaires, les intermittents � se pr�parer � un rapport de force��, a lanc� le n�gociateur de la centrale syndicale, Eric Aubin, avant de couper en deux le texte. En fin de matin�e, sa col�re �tait toutefois retomb�e�:
Eric Aubin (CGT) ressort apais� de la matin�e de n�go #Un�dic : "Le patronat a pr�cis� que son texte n'�tait pas un projet d'accord"
� JB Chastand (@jbchastand) 13 f�vrier 2014
La Conf�d�ration fran�aise d�mocratique du travail (CFDT) s'est elle aussi montr�e tr�s circonspecte quant au projet patronal, soulignant que son objectif ��unique�� dans cette n�gociation �tait la mise en place des droits rechargeables. Ce dispositif permettrait aux ch�meurs qui retrouvent un emploi sans avoir �puis� leurs droits de les conserver , au moins en partie, et de les cumuler avec leurs nouveaux droits s'ils redeviennent ch�meurs. La repr�sentante du syndicat, V�ronique Descacq, a notamment ferm� la porte � toute n�gociation sur le r�gime sp�cial des intermittents du spectacle, ��qui ne fait pas partie de la n�gociation��.�
��DES CALCULS D'�COLE PRIMAIRE��
��A chaque n�gociation��, le patronat ��agite le chiffon rouge : les intermittents du spectacle, les int�rimaires, la fonction publique��, a regrett� le repr�sentant de Force ouvri�re (FO), St�phane Lardy, jugeant la proposition patronale de modulation des allocations ch�mage en fonction de la conjoncture ��tout � fait absurde��. ��Ce sont des calculs d'�cole primaire��, a-t-il ironis�.
��Ne rien faire dans la p�riode actuelle, c'est prendre le risque d�s 2017 de remettre en cause l'existence m�me de l'assurance-ch�mage��, plomb�e par un lourd d�ficit et une dette proche de son niveau record, a fait valoir le n�gociateur du patronat, Jean-Fran�ois Pilliard. ��C'est un texte �quilibr�. Pour nous, c'est une question d'�quilibre des r�gimes��, a rench�ri la repr�sentante de la Conf�d�ration g�n�rale des petites et moyennes entreprises (CGPME), Genevi�ve Roy.
Patronat et syndicats (CGT, CFDT, FO, CFTC, CFE-CGC) se sont donn� jusqu'au 13 mars pour parvenir � un accord.
Des intermittents occupent le hall du minist�re de la culture
�
Une cinquantaine d'intermittents du spectacle ont occup� le hall du minist�re de la culture, rue des Bons-Enfants, � Paris, jeudi, pour d�fendre leur statut. Une d�l�gation a rencontr� dans l'apr�s-midi la ministre, Aur�lie Filippetti, qui a r�it�r� son ��attachement au r�gime d'assurance ch�mage des intermittents�� tout en rappelant que la n�gociation d�pendait des partenaires sociaux.
�
Les militants de la CGT Spectacle, du Syndeac (Syndicat national des entreprises artistiques et culturelles) et de la Coordination des intermittents et pr�caires d'Ile-de-France ont constat� dans un communiqu� ��l'incapacit� de la ministre � r�pondre � leur revendication��. Ils r�clament un communiqu� conjoint des minist�res du travail et de la culture garantissant qu'il n'y aura aucun agr�ment d'un accord sur l'assurance-ch�mage qui ne pr�voie pas le maintien des annexes 8 et 10, sp�cifiques aux salari�s du spectacle.
offres d'emploi avec
