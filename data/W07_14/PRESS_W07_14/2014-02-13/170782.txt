TITRE: PSG, Blanc :"Verratti n'est pas un joueur m�chant" - Goal.com
DATE: 2014-02-13
URL: http://www.goal.com/fr/news/29/ligue-1/2014/02/13/4617294/psg-blanc-n%25C3%25A9pargne-pas-verratti
PRINCIPAL: 170777
TEXT:
0
13 f�vr. 2014 14:43:00
Laurent Blanc, le coach du PSG, a �voqu� le cas de Marco Verratti, estimant qu'il prenait trop de risques dans la surface des parisiens.
"Il n'a pas plus jou� que la saison pass�e. Il apprend, il est jeune mais on n'apprend pas en restant sur le banc. Peut-�tre que je le laisserai sur le banc pour qu'il r�fl�chisse. Mais ce qui marche le mieux, c'est de discuter avec lui, ce n'est pas un joueur m�chant m�me s'il prend des cartons. On aime Verratti pour ses prises de risques. Personnellement contre Monaco j'ai trouv� qu'il y en avait eu un peu trop. Je veux bien qu'il prenne des risques, mais je pr�f�re qu'il les prenne dans la surface adverse plut�t que dans la notre. Je lui demande de jouer plus simple devant la d�fense", a confi� Laurent Blanc en conf�rence de presse.
Laurent Blanc fait un point sur les bless�s
Le coach du PSG a �galement fait un point sur les bless�s. Concernant Edinson Cavani, Laurent Blanc assure que ce dernier ne sera pas pr�t pour le Huiti�me de finale aller de la Ligue des champions face au Bayer. "Il suit son programme. En fin de semaine, il devrait reprendre la course. Tous les signes sont tr�s bons. Mais il n�a jamais �t� question qu�il soit de retour pour jouer face � Leverkusen. Jallet est sur le point de r�int�grer le groupe. Il commence � reprendre part au jeu, il est en avance". Concernant Zlatan Ibrahimovic ? "Il va tr�s bien, et s�entra�ne normalement."
Relatifs
