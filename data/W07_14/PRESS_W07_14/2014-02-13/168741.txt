TITRE: Chine: s�isme de magnitude 6,8 dans la r�gion du Xinjiang - France-Monde - La Voix du Nord
DATE: 2014-02-13
URL: http://www.lavoixdunord.fr/france-monde/chine-seisme-de-magnitude-6-8-dans-la-region-du-xinjiang-ia0b0n1913974?xtor%3DRSS-2
PRINCIPAL: 168731
TEXT:
Un puissant tremblement de terre de magnitude 6,8 a frapp� ce mercredi la r�gion du Xinjiang, dans le nord-ouest de la Chine, a annonc� l�institut am�ricain de g�ophysique (USGS).
Les r�gions montagneuses de l�ouest de la Chine sont r�guli�rement touch�es par des tremblements de terre.
- A +
D�abord �valu�e � 7, la magnitude du s�isme a �t� ensuite annonc�e � 7,3 par les m�dias et les autorit�s chinoises. Mais la secousse a finalement �t� �valu�e � 6,8 sur l��chelle de Richter, qui mesure l��nergie lib�r�e lors du tremblement de terre. � 6,8, la secousse est qualifi�e de � forte �.
La secousse tellurique a �branl� une zone recul�e du Xinjiang, immense r�gion en partie d�sertique situ�e aux confins occidentaux de la Chine.
L��picentre du s�isme a �t� localis� � 270 kilom�tres de la ville de Hotan, selon l�institut am�ricain de g�ophysique (USGS).
Les r�gions montagneuses de l�ouest de la Chine sont r�guli�rement touch�es par des tremblements de terre.
Le Sichuan, l�une des provinces les plus peupl�es de Chine avec 80 millions d�habitants, avait �t� endeuill� en mai 2008 par un tremblement de terre d�vastateur qui avait fait 87 000 morts et disparus.
Cet article vous a int�ress� ? Poursuivez votre lecture sur ce(s) sujet(s) :
