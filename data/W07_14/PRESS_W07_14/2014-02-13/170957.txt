TITRE: Martin Fourcade double la mise sur 20 km
DATE: 2014-02-13
URL: http://www.lemonde.fr/jeux-olympiques/article/2014/02/13/martin-fourcade-double-la-mise-sur-20-km_4366084_1616891.html
PRINCIPAL: 170923
TEXT:
Sotchi 2014 : Martin Fourcade double la mise sur 20 km
Le Monde |
� Mis � jour le
13.02.2014 � 21h24
Trois jours apr�s son titre olympique en poursuite, Martin Fourcade a doubl� la mise, jeudi 13 f�vrier, lors de l'�preuve individuelle (20 km) de biathlon , avec un temps de 49 min 31 s 7. Le Catalan de 25 ans devance l'Allemand Erik Lesser (+�12�s�2) et le Russe Evgueni Garanichev (+�34�s�5).�
C'est sur cette distance que Martin Fourcade avait d�j� �t� sacr� l'an dernier aux Mondiaux de Nove Mesto ( R�publique tch�que ). Il a donc pour l'instant remport� la moiti� des m�dailles fran�aises obtenues � Sotchi, avec celles de bronze remport�es par Jean-Guillaume B�atrix en poursuite et Coline Mattel dans le premier concours de saut � skis f�minin.
B�ATRIX TERMINE 6e
Avec cette nouvelle m�daille d'or olympique, gr�ce notamment � un 19 sur 20 au tir, Martin Fourcade devient le premier Fran�ais double champion olympique dans une m�me �dition des JO d'hiver depuis le skieur Jean-Claude Killy , qui avait brill� aux Jeux de Grenoble 1968 en finissant m�me triple m�daill� d' or .
>> Revivez l'�preuve du 20 km de biathlon
��Je suis content, il y avait un peu de suspense. Je loupe une balle sur le second tir mais je sais que sur l'individuel, j'ai un niveau � skis qui permet, avec une faute, de gagner��, d�clarait Martin Fourcade sur France T�l�visions.
Jean-Guillaume B�atrix, m�daille de bronze lors de la poursuite lundi, a termin� cet apr�s-midi 6e. Simon Fourcade, le fr�re de Martin, est arriv� � une d�cevante 13e. De bon augure pour les relais mixte et hommes, pr�vus respectivement les 19 et 22 f�vrier. Le quatri�me Fran�ais de la course, Alexis B�uf, a termin� � la 82e place.
��RAMENER UNE M�DAILLE ENSEMBLE��
��L'objectif maintenant, c'est les relais��, ajoutait Martin Fourcade. ��Il reste une mass-start, je vais donner mon meilleur, mais maintenant, c'est les relais avec les copains. �a fait quatre ans qu'on travaille ensemble, on veut ramener une m�daille ensemble.��
Le Norv�gien Ole Einar Bjoerndalen, d�j� sacr� la semaine pass�e sur l'�preuve du sprint 10 km en biathlon, o� Fourcade avait fini 6e, loupe donc l'occasion de devenir l'athl�te le plus m�daill� de l'histoire des JO d'hiver. A 40 ans, ce v�t�ran a d�j� rejoint son compatriote fondeur Bj�rn Daehlie en t�te du palmar�s, avec 12 m�dailles olympiques.
