TITRE: Vinci Park valoris� pr�s de 2 milliards d'euros
DATE: 2014-02-13
URL: http://www.boursorama.com/actualites/vinci-park-valorise-pres-de-2-milliards-d-euros-3285dc06e60dc405df33e61a81659e75
PRINCIPAL: 168974
TEXT:
Vinci Park valoris� pr�s de 2 milliards d'euros
Le Revenu le 12/02/2014 � 14:55
Imprimer l'article
DR
(lerevenu.com) - Cr�ation d'une soci�t� commune. C'est finalement Ardian (ex-Axa Private Equity) et Cr�dit Agricole Assurances qui deviendront les actionnaires majoritaires de Vinci Park. Plus pr�cis�ment, ils cr�eront avec Vinci une soci�t� commune d�tenant 100% de la filiale de parkings. Celle-ci sera contr�l�e � 37,5% par Ardian, � 37,5% par Cr�dit Agricole Assurances et � 25% par Vinci. La finalisation de l'op�ration interviendra apr�s consultation des instances repr�sentatives du personnel et approbation, si besoin, des autorit�s comp�tentes.
Un prix satisfaisant. La cession se fait sur la base d'une valeur d'entreprise totale de Vinci Park de 1,96 milliard d'euros. Une valorisation satisfaisante, dans le haut de la fourchette attendue (entre 1,6 et 2 milliards), correspondant � 9,4 fois l'exc�dent brut d'exploitation (Ebitda) 2013 de la division.
Un actif mature. Leader mondial du stationnement, Vinci Park exploite pr�s de 1,8 millions de places dans 14 pays, sous diff�rents contrats : pleine propri�t�, concessions, prestations de service. Si la soci�t� est tr�s rentable (11% de marge nette l'an dernier), elle n'offre pas un fort potentiel de croissance. Son chiffre d'affaires 2013 a recul� de 1,3% � 607 millions d'euros, contre une progression de 11,6% pour l'ensemble du p�le concessions. Les concessions de parkings immobilisent beaucoup moins de capitaux que les autoroutes, mais elles sont conclues sur des dur�es beaucoup plus
