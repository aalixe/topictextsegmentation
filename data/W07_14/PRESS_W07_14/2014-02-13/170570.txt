TITRE: MPDC706�: Mpman d�voile une tablette certifi�e Google � moins de 50��
DATE: 2014-02-13
URL: http://www.lesnumeriques.com/tablette-tactile/mpman-mpdc706-p19463/mpdc706-mpman-devoile-tablette-certifiee-google-a-moins-50-e-n33209.html
PRINCIPAL: 0
TEXT:
Nous soutenir, navigation sans publicit� Premium : 2�/mois + Conseil personnalis� Premium+ : 60�/an
MPDC706�: Mpman d�voile une tablette certifi�e Google � moins de 50��
Pas une b�te de course, mais pas si mal
�
Publi� le: 13 f�vrier 2014 15:01
Par Johann Breton
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Plus de propositions Moins de propositions
Alors que l'absence de Play Store est souvent le plus grand handicap des produits Android vendus � un tarif inexplicablement bas, Mpman d�voile sa�MPDC706, une tablette de 7 pouces qui se veut � la fois capable et tr�s accessible. Mais si le tarif de 49,90�� en fait indiscutablement un produit pour toutes les bourses, il faut, bien s�r, compter avec une fiche technique assez modeste.
Ainsi, l'�cran de 7 pouces abrite selon toute probabilit� une dalle TN, dont la d�finition annonc�e est de 800�x�480�pixels. Mpman lui pr�te une luminosit� de 200�cd/m� et un contraste de 500:1, des valeurs correctes pour de l'entr�e de gamme destin�e � servir en int�rieur, mais qui en font, a priori, une machine difficilement utilisable en plein soleil.
Du c�t� des entrailles de la b�te, c'est un processeur Allwinner A23 et ses deux c�urs (Cortex A7) cadenc�s � 1,5�GHz qui officient, suppl��s par 512�Mo de m�moire vive, tandis que la partie graphique est assur�e par un Mali-400 MP2. Loin d'en faire une machine rutilante, ces composants devraient toutefois se montrer capables de g�rer Android (version 4.2) sans trop d'encombres, � condition toutefois de ne pas faire appel � des applications trop lourdes. Le couple GPU/m�moire vive, par exemple, exclut d'embl�e les jeux aux graphismes 3D soign�s.
Concernant le stockage, ce sont 4�Go qui sont embarqu�s, mais la tablette dispose d'un port microSD pour accro�tre cet espace avec 32�Go suppl�mentaires. La connectique est par ailleurs compl�t�e par un port micro-USB et une prise Jack, tandis que la�MPDC706 est �quip�e d'une webcam dVGA (0,3�Mpx). C�t� connectivit�, point de 3G � � moins de passer par une cl� �, mais du Wi-Fi b/g/n.
Avec des dimensions de 192�x�116�x�10,5�mm et un poids de 305�g, la tablette sign�e Mpman ne donne pas dans la grande finesse ou la l�g�ret�. N�anmoins, la batterie de 2�600�mAh qu'elle h�berge devrait lui permettre, eu �gard � ses composants plut�t modestes, de fournir une autonomie honn�te. Pour indication, notre tablette Android 7 pouces de r�f�rence, la Nexus 7�2013 , se dote d'une batterie de 3�950�mAh pour alimenter les quatre c�urs de son processeur S4 Pro de Qualcomm, nettement plus performant.
Attendue en rayon au mois d'avril � un tarif de 49,90��, la Mpman�MPDC706 sera suivie en mai par une d�clinaison 10 pouces, qui devrait elle aussi disposer de la boutique d'applications de Google et afficher un prix plancher, puisque le constructeur annonce un tarif de 79,90�� � sa sortie.
