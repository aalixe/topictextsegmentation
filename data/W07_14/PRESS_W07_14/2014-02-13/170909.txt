TITRE: A. Suguenot et le p�ril internet des viticulteurs | Gazette Info, l'info en temps r�el � Dijon et en C�te d'Or - actualit�, �conomie, politique, sport, interview...
DATE: 2014-02-13
URL: http://www.gazetteinfo.fr/2014/02/13/suguenot-le-peril-internet-des/
PRINCIPAL: 170906
TEXT:
Mercredi 9 Avril 2014 | 22:53 / Bienvenue sur le site d'info en continu de
A. Suguenot et le p�ril internet des viticulteurs
Jeudi 13 f�v 2014 � 11:02 | Par redaction
�Dans un communiqu�, Alain� Suguenot exprime son inqui�tude au sujet des noms de domaine sur internet, qui pourrait ��accro�tre les possibilit�s d�usurpation et de contrefa�on des [appellations viticoles]��.
�
Communiqu�:
�
��Il est un sujet d�une urgence extr�me, que le Gouvernement a malheureusement n�glig�, et qui peut mettre en p�ril nos viticulteurs, c�est celui de la gouvernance mondiale de l�internet.
�
Actuellement, l�attribution des noms de domaine ou de sites internet, est g�r� par un organisme am�ricain, l�ICANN (Soci�t� pour l�attribution des noms de domaines et des num�ros sur internet), qui d�pend du d�partement du Commerce des �tats-Unis. Alors qu�Internet est le plus souvent consid�r� comme un r�seau mondial d�centralis� et sans gouvernance particuli�re : chacun pourrait y faire un peu ce qu�il veut, et les Etats se d�brouilleraient pour y faire r�gner l�ordre � leur mani�re, c�est donc en r�alit� un seul organisme qui d�cide des noms des sites internet (qui se montent aujourd�hui � plusieurs centaines de millions dans le monde).
L�ouverture des noms de domaine d�cid�e en 2011 par l�ICANN risque, ainsi, en mettant en danger les indications g�ographiques, d�accro�tre les possibilit�s d�usurpation et de contrefa�ons de nos appellations et, par voie de cons�quence, de nuire � la fili�re viticole qui est un des fleurons du commerce ext�rieur de la France.
�
Si certaines attributions se sont d�roul�es sans encombre, d�autres ont soulev� des probl�mes d�envergure. C�est le cas des noms de domaines .vin et .wine, qui sont la cible d�offre de trois soci�t�s sp�cialis�es dans la mise aux ench�res des noms de domaine.
Concr�tement, cela signifie qu�une entreprise �trang�re, vendant du vin de Bourgogne, mais aussi n�importe quel autre produit, pourra avoir un site internet r�f�renc� en bourgogne.vin, ce qui risque, bien �videmment, de tromper le consommateur, mais aussi de sp�culation autour des noms de domaines, enfin de p�naliser les vendeurs fran�ais qui, eux, ne vendraient que des vins de Bourgogne.
Le probl�me est que l�ICANN n�a pas de r�gles pr�cise pour limiter la sp�culation sur les noms de domaines, ni pour prot�ger l�utilisation commerciale des indications g�ographiques viticoles. Un processus d�attribution qui provoque une inqui�tude l�gitime des producteurs de vins.
�
Il est donc urgent d�op�rer une v�ritable reprise en main par la France et l�Union europ�enne des enjeux internationaux du num�rique. Seule l�UE a le poids n�cessaire pour influer dans cette nouvelle g�ographie du cyberespace, o� les �tats-Unis sont dominants. Il est dommage, � ce titre, que le Pr�sident Hollande n�en ait souffl� mot au Pr�sident Obama, lors de sa visite � Washington.
�
C�est pourquoi j�ai d�cid�, ce matin m�me, lors de la r�union du groupe ���nologie et territoires�� que je pr�side � l�Assembl�e nationale, de profiter de la visite en Europe, la semaine prochaine, du Directeur de l�ICANN pour l�interpeller sur la question.
�
Pour cela il faut que le Gouvernement soit en veille maximale et j�ai ainsi d�cid� de saisir les ministres de l�Agriculture, M. Le Foll, et de l��conomie num�rique, Mme Pellerin, pour les sensibiliser rapidement sur le sujet. Et pour le Gouvernement fran�ais se d�cide � d�fendre nos viticulteurs et exploitants.��
�
3 commentaires sur �A. Suguenot et le p�ril internet des viticulteurs�
par nantivinea
13 f�vrier 2014 � 19 h 51 min
toujours au cul des qui se d�brouillent bien sans lui- combiend�intervention en faveur des dotations informatiques pour les profs ? aucune
par Biboux
14 f�vrier 2014 � 10 h 53 min
C�est bien monsieur Suguenot de poser le probl�me mais sachez que les informaticiens s�rieux le disent depuis 20 ans et qu�ils n�ont jamais �t� �cout�s ni par le gouvernement Jupp� ni par celui de Lionel Jospin. Quant � Nicolas Sarkozy il a nomm� Eric Besson � l��conomie num�rique qui n�a rien compris et n�a rien fait, puis NKM qui a fait croire qu�elle s�y conna�t mais ne comprend pas bien le sujet et Hollande a confi� cette t�che � Fleur Pellerin qui fait son apprentissage dans ce domaine complexe. A quand un vrai grand minist�re de l�informatique dirig� par un informaticien chevronn� qui aurait autorit� sur les autres minist�res pour toutes ces choses complexes o� il faut agir vite et bien � l��chelle mondiale et europ�enne. Sans doute que le million d�emplois de monsieur Gattaz serait d�j� l� et que l�avenir serait moins sombre pour notre jeunesse et nos entreprises.  Mais ils vous a fallut caser les soutiens (encombrant comme Besson) et l�informatique a servi de bouche trou alors ne vous �tonnez pas de la situation dans laquelle nous nous trouvons aujourd�hui. Il y a un informaticien qui vient m�me de cr�er son propre parti tellement vous nous avez d��us  dans ce dom aine et qu�il y a tant � faire ( Voir Denis Payre http://www.nouscitoyens.fr/ )
par Biboux
14 f�vrier 2014 � 12 h 49 min
Un conseil monsieur Suguenot, ne r�duisez pas cela au seul minist�re de l�agriculture car c�est un probl�me g�n�ral pour toutes les entreprises Fran�aises y compris industrielles et commerciales. D�autre part si vous vous adressez seulement � l�ICANN vous n�obtiendrez rien car cela concerne aussi les moteurs de recherche comme Google, Bing ou Yahoo� Et m�me les entreprises am�ricaines souffrent de leurs m�thodes� Il faut faire front commun aussi avec d�autres pays Europ�ens pour peser dans la balance et mettre de l�ordre sur internet, alors ne perdez pas votre temps dans des petits trucs de petit d�put� qui veut faire parler de lui apr�s d�une cat�gorie d��lecteurs (les viticulteurs) aussi respectable soit-elle et agissez plut�t aupr�s de votre groupe pour l��mergence d�un v�ritable futur minist�re de l�informatique dirig� par un homme de l�art qui s�occuperait de tout cela et aussi de big-data, du cloud et m�me de la carte vitale�
