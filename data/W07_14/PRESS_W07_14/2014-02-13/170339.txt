TITRE: Bertrand Delanoë à son dernier Conseil de Paris : « Je n’ai aucune nostalgie », Municipales 2014 : la bataille se prépare déjà
DATE: 2014-02-13
URL: http://www.lesechos.fr/economie-politique/politique/dossier/0202629246171/0203311669704-bertrand-delanoe-a-son-dernier-conseil-de-paris-je-n-ai-aucune-nostalgie-649908.php
PRINCIPAL: 170336
TEXT:
Bertrand Delano� � son dernier Conseil de Paris�: � Je n�ai aucune nostalgie �
10/02 | 18:27 | mis � jour le 12/02 � 13:00
Apr�s treize ans de mandat, le maire de Paris a pr�sid� son dernier Conseil de Paris. Applaudi par les �lus de droite comme de gauche, Bertrand Delano� a affich� sa s�r�nit�.
Le maire de Paris socialiste Bertrand Delano� a pr�sid� lundi son dernier Conseil de Paris, � quelques semaines de son d�part de l�H�tel de Ville, apr�s 13 ans de mandat. Troisi�me maire de la capitale du XXe si�cle, premier de gauche, seul maire ouvertement homosexuel d�une grande ville fran�aise, Bertrand Delano�, �g� de 63 ans et chantre du non-cumul des mandats, avait promis d�s 2007 de ne pas se repr�senter en 2014.
-------
Notre dossier�:
-------
� Je n�ai aucune nostalgie, aucune tristesse, j�ai plein de joie dans le coeur. Parce que c�est Paris �, a d�clar� le maire en concluant une s�ance du Conseil tr�s all�g�e. �lus de droite comme de gauche se sont quasiment tous lev�s pour applaudir Bertrand Delano�, � l�exception du pr�sident de la f�d�ration UMP de Paris Philippe Goujon, et de la maire du VIIe Rachida Dati, qui s��tait �clips�e.
Bertrand Delano� a mis la main sur le coeur, comme lors de son �lection en 2001. Le maire, qui quitte l�H�tel de Ville au fa�te de sa popularit�, s�est montr� moins �mu que � serein �. � J�aime la vie, j�ai de la curiosit�, de l�app�tit (...) Je ne suis pas angoiss� �, a-t-il d�clar� � la presse � l�issue d�un d�jeuner auquel �taient convi�s tous les �lus.
Interrog� sur ses projets d�avenir, Bertrand Delano� a dit son souhait de se reposer en 2014 quatre mois � Bizerte, en Tunisie, o� il a grandi. Parlant pour 2015 � de voyages, d�engagement au c�t� des jeunes, d�ONG �.
Une entr�e au gouvernement�?
Nombreux sont ceux pourtant qui voient Bertrand Delano� entrer au gouvernement � la faveur d�un remaniement, pour autant qu�on lui propose un poste qui lui agr�e. Les Affaires �trang�res sont souvent cit�es par ses proches, l�Int�rieur ou m�me Matignon, plus rarement.
Conseiller de Paris en 1977
Quelles que soient ses r�alisations futures, le nom de Bertrand Delano� restera intimement li� � celui de Paris. �lu conseiller de Paris en 1977, en m�me temps que son mentor Lionel Jospin, il a aussi �t� �lu d�put� (1981-1986) et s�nateur (1995-2001) dans la capitale. Seule infid�lit� � la Ville lumi�re : une tentative avort�e de parachutage � Avignon, pour les l�gislatives de 1986, qui le poussera � se mettre pour quelques ann�es en retrait de la vie politique.
Il y reviendra en 1993, prenant la t�te du groupe socialiste au Conseil de Paris, avant de conduire en 1995 les listes � Paris s��veille �, qui mettent fin au grand chelem de la droite chiraquienne, en lui enlevant six arrondissements sur vingt. En 2001, Bertrand Delano� doit d�abord batailler ferme dans son propre camp contre Jack Lang, dont la cote de popularit� est sans commune mesure avec la sienne. Un combat incertain, jusqu�� ce que l�ancien ministre de la Culture soit appel� par le Premier ministre Lionel Jospin au gouvernement quelques jours avant la primaire qui doit les d�partager. Celui qu�on surnommait le � Petit Chose � arrache de justesse la capitale � la droite, divis�e entre le candidat investi par le RPR Philippe S�guin et le maire sortant Jean Tiberi.
R�forme de l�attribution des logements sociaux, r�duction du train de vie municipal, et des c�l�bres � frais de bouche �... Bertrand Delano� s�est attach�, comme il l�avait promis dans son livre-programme � Pour l�honneur de Paris �, � �liminer � un syst�me d�voy� �. Il a aussi profond�ment chang� le visage de la capitale, avec une politique de r�duction de la place de l�automobile offensive (et pol�mique), la remise en chantier de � 10% � du territoire parisien, le financement de 70.000 logements sociaux - au d�triment des classes moyennes, accuse la droite.
Parmi ses innovations marquantes et qui s�exportent avec succ�s � l��tranger : les locations de v�los et de voitures en libre service V�lib� et Autolib�, Paris Plages ou Nuit Blanche.
Une maison en ordre
� Bertrand Delano� est en train de r�ussir sa sortie. Il s�applique comme il l�avait promis le non-cumul (des mandats) dans le temps. Il n�y a pas tant que �a d�hommes politiques qui l�ont fait avec cette d�termination, cette classe �, juge le sociologue Jean-Louis Missika,adjoint de Bertrand Delano� en charge de l�Innovation.
Accus� d�autoritarisme par ses opposants, Bertrand Delano� laisse une maison en ordre : sa premi�re adjointe Anne Hidalgo, qu�il a tr�s t�t mise en selle, semble bien partie pour lui succ�der, dans une ville d�sormais solidement ancr�e � gauche.
POUR EN SAVOIR PLUS�:
DOSSIER Municipales 2014 : la bataille � Paris
EXCLUSIF - Les indicateurs cl�s des 36.000 communes
En quelques clics, retrouvez les principales donn�es de base les plus r�centes pour dresser le portrait socio-�conomique et politique d�une ville ou d�un village. Et pour les 15.000 communes les plus importantes, jugez de leurs performances en mati�re de comp�titivit� fiscale, de dynamisme entrepreneurial, de gestion financi�re et de politique du logement.
