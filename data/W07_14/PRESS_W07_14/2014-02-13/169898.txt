TITRE: Le cas ambigu de Vincent Lambert
DATE: 2014-02-13
URL: http://www.lemonde.fr/idees/article/2014/02/12/le-cas-ambigu-de-vincent-lambert_4365027_3232.html
PRINCIPAL: 169891
TEXT:
Le cas ambigu de Vincent Lambert
Le Monde |
12.02.2014 � 18h01
| Par V�ronique Guienne (professeur de sociologie � l'universit� de Nantes)
Comment expliquer le trouble actuel suscit� par le cas de Vincent Lambert ? La raison en est que, cinq ans apr�s son accident, son cas est ambigu, pouvant �tre interpr�t� comme relevant soit du suicide assist�, soit de l'accompagnement de fin de vie .
Concernant l'aide � mourir , il faut distinguer clairement deux situations�: celles de l'accompagnement par des s�dations, permettant � la mort d' advenir dans de bonnes conditions lorsque la fin est proche, ou serait in�vitable sans intervention m�dicale, et celles relevant des demandes d' aide au suicide. Ces deux questions souffrent d'un amalgame.
Certes, � premi�re vue, ces deux situations se ressemblent�: ce sont les m�mes produits qui sont utilis�s, elles se m�nent toutes deux au nom de l'autonomie du patient, et sont recouvertes l'une et l'autre de cette appellation confuse du ��droit � mourir��.
L'OPINION PUBLIQUE EST PARTAG�E
En fait, elles sont radicalement diff�rentes�: dans le premier cas, la d�cision est construite collectivement, est v�cue comme un accord raisonnable par les deux parties, le patient et ses proches d'un c�t�, l'�quipe soignante de l'autre. Dans la seconde option, une personne exprime le fait qu'elle ne supporte pas (ou plus) son �tat et demande qu'on l'aide � en finir .
Juridiquement, ces deux situations n'appellent pas le m�me type de choix. La premi�re rel�ve d'une l�gislation qui se doit d' �tre pr�cise, avec des protocoles l'encadrant. Ces processus de s�dation sont d�j� pratiqu�s� quotidiennement � l'h�pital, bien que dans un cadre juridique � parfaire .
La seconde rel�ve du d�bat sur la d�p�nalisation des stup�fiants (la morphine et l'hypnovel en sont), sous conditions d'acc�s (comit� de d�livrance, aide technique n�cessaire, place des associations�), et a pour vocation d' �tre exceptionnelle.
L'opinion publique est partag�e, puisque 85 % des Fran�ais sont pour une aide � mourir en cas de douleurs ou de perte de conscience en fin de vie et 85 % des Fran�ais d�sapprouvent le suicide.
UNE M�DECINE INTERVENTIONNISTE
L'ambigu�t� du cas de Vincent Lambert�tient � ce qu'il se pr�sente comme une demande d'accompagnement de fin de vie mais qu'il peut aussi �tre lu comme une demande de suicide assist�. En effet, cinq ans apr�s son accident, son �tat peut �tre assimil� � celui d'un handicap� profond, par la dur�e de son installation dans cet �tat.
Des structures enti�res s'occupent de ces personnes lourdement handicap�es et sans conscience. On ne peut d�cider que leur vie ne vaut pas la peine d' �tre v�cue.
Mais que s'est il pass� pendant cinq ans�? Il est dit que�tout a �t� tent� pour l' aider � r�agir , que des services internationalement reconnus ont �t� sollicit�s, qu'il a b�n�fici� d'innombrables s�ances d'orthophonie sans effet sur la moindre communication avec lui, et cela apr�s un long coma. Pourquoi tout cela�? Pourquoi pendant si longtemps�? Cela s'appelle de l'acharnement th�rapeutique.
La loi Leonetti aurait �t� simple � appliquer tout de suite, ou peu de temps apr�s l'accident. Mourir d'un accident, et c'est ce qui aurait d� se passer si la m�decine n'avait pas �t� aussi interventionniste, fait partie des al�as possibles de la vie. Ces situations se rencontrent tous les jours � l'h�pital. C'est l� que la plupart d'entre nous meurent, en g�n�ral suite � une d�cision m�dicale�: de ne pas r�animer , d' arr�ter des traitements lourds, de ne pas op�rer�
Cette fin programm�e cherche � accompagner dans les meilleures conditions ce qui �tait de toute fa�on un processus de fin de vie, quel que soit l'�ge du patient, parfois en utilisant des s�dations � base de morphine et d'hypnovel, pour que cela se passe bien, sans douleurs.
Ce que l'on peut reprocher � l'h�pital, c'est que cet accompagnement m�dicamenteux ne soit pas toujours mis en �uvre�; il y a encore trop de gens qui meurent sur des brancards dans la souffrance. Dans le vocabulaire du personnel hospitalier, ces derniers ne disent pas euthanasie dans ces cas de s�dations, alors que �a l'est au sens strict de la d�finition de l'�thique m�dicale.
31 JOURS EN ARR�T D'ALIMENTATION ET D'HYDRATATION
Le terme d'euthanasie, pour la plupart d'entre eux, reste associ� � l'injection de chlorure de potassium (KCL), avec un effet imm�diat, telles qu'elles ont pu �tre pratiqu�es dans les ann�es 1980.
La plupart de ces s�dations quotidiennes se passent bien�; les r�cits que l'on recueille sont m�me plut�t positifs, racontant des gens apais�s, qui remercient du regard, des familles r�unies. Les seuls r�cits de situations difficiles sont toutes les fois o�, soit la d�cision est prise trop tard, soit la s�dation est trop faiblement dos�e, produisant une attente insupportable (pour les soignants et les proches) qui peut durer des jours, voire des semaines, en maintenant le patient dans un �tat de ni mort ni vivant.
Le cas de Vincent Lambert r�pond � ces deux caract�ristiques, toujours causes de probl�mes dans les �quipes soignantes et pour les proches�: une d�cision trop tardive et une s�dation trop peu dos�e. Personne ne peut veiller un presque mort si longtemps.
Vincent Lambert est rest� 31 jours en arr�t d'alimentation et d'hydratation, avant que la justice n'ordonne sa r�alimentation�; ce d�lai est invivable pour tout le monde. Il faut d'ailleurs esp�rer que sa souffrance a �t� bien �valu�e�; ces arr�ts n�cessitent des s�dations d'accompagnement car ils sont extr�mement douloureux.
Une limite de la loi Leonetti est qu'elle impose de ne pas doser en opiac�s plus que ne le n�cessite la limitation de la souffrance. Cela permet une fin en quelques jours pour les corps vieillis et les malades tr�s fragiles, mais pas chez les personnes jeunes.
LE RISQUE : �TRE DANS L'ACHARNEMENT TH�RAPEUTIQUE
Il n'y a donc pas chez Vincent Lambert une ��farouche volont� de vivre��, comme l'a d�clar� sa m�re, mais, plus trivialement, l'illustration du fait qu'un organisme jeune peut survivre des mois � une d� nutrition et une d�shydratation si on n'acc�l�re pas le processus. La loi Leonetti est sur ce point trop restrictive.
Une fois la d�cision prise et partag�e, ces attentes sont des violences inutiles pour tous. On peut continuer � progresser dans le cadre de la loi Leonetti, en particulier en favorisant un �change sur les conditions de cette mort prochaine�: son lieu, sa vitesse, son �tat de conscience.
Dans nos d�bats actuels, il est essentiel de ne pas isoler la fin de vie de tout ce qui se passe en amont. Notre libert� est d'abord en amont, en disant en l'occurrence clairement qu'on ne veut pas d'acharnement si le risque est celui du handicap profond.
Pour cela, il faut que les m�decins acceptent d' arr�ter de nous sauver malgr� nous. Souvent, en faire moins, c'est mieux, et cela �vite ces situations inextricables. Sinon, le risque serait d' �tre dans l'acharnement th�rapeutique, d'en faire trop, au nom de ce que, apr�s, avec le m�me interventionnisme, on pourrait arr�ter .
Ces deux postures rel�vent d'un m�me fantasme de ma�trise, d'une id�ologie contemporaine de la performance. Sans �tre dans ce fantasme, il est l�gitime de pouvoir �tre entendu dans un souhait raisonnable�: ne pas �tre sauv� � n'importe quel prix et mourir dans un d�lai acceptable et sans souffrance lorsque la fin est proche.
V�ronique Guienne (professeur de sociologie � l'universit� de Nantes)
V�ronique Guienne a notamment publi� ��Sauver, laisser mourir, faire mourir�� (PUR, 2010) et� ��Nos choix de sant�, dilemmes et controverses�� (L'Atalante, 2012).
Id�es
