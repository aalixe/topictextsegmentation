TITRE: Venezuela: le gouvernement bat le rappel de ses troupes au lendemain de violences - France-Monde - La Voix du Nord
DATE: 2014-02-13
URL: http://www.lavoixdunord.fr/france-monde/venezuela-le-gouvernement-bat-le-rappel-de-ses-troupes-au-ia0b0n1917463?xtor%3DRSS-2
PRINCIPAL: 171471
TEXT:
- A +
Le gouvernement v�n�zu�lien a appel� � des manifestations jeudi "contre la violence de l'opposition", rendue responsable des incidents qui ont fait trois morts mercredi en marge d'une marche d'�tudiants et d'opposants.
La ministre de l'Information, Delcy Rodriguez, a lanc� cet appel sur Twitter sous le mot d'ordre "le Venezuela uni contre le fascisme", terme habituellement employ� par les autorit�s pour d�signer le mouvement d'opposition.
Mme Rodriguez a ainsi appel� "tous les mouvements sociaux et le peuple" � manifester "contre la violence de l'opposition".
"Le projet de coup d'Etat sera d�jou� une fois de plus", c'est exclam� de son c�t� le ministre du Tourisme, Andres Izarra, sur le site de micro-blogs, r�p�tant les accusations de tentative de renversement du gouvernement lanc�es la veille par le pr�sident Nicolas Maduro apr�s les violences.
Mercredi, des manifestations de milliers d'�tudiants et de militants de l'opposition protestant contre la vie ch�re, l'ins�curit� et les p�nuries ont �t� organis�es dans plusieurs villes du pays.
Cette marche s'inscrivait dans la continuit� d'un mouvement �tudiant lanc� en province, o� des heurts ont oppos� les �tudiants aux policiers et � des groupes pro-gouvernementaux depuis une dizaine de jours.
La manifestation de Caracas, la plus importante depuis que Nicolas Maduro a succ�d� en mars � Hugo Chavez (1999-2013), s'est achev�e par des affrontements entre jeunes oppos�s au gouvernement et policiers ou groupes "chavistes" qui ont fait au moins trois morts par balles.
Les autorit�s ont �galement fait �tat de dizaines de bless�s et d'environ 80 arrestations � travers le pays.
Nicolas Maduro a donn� l'ordre mercredi soir de renforcer la s�curit� dans les principales villes du Venezuela. "Il n'y aura pas de coup d'Etat au Venezuela, ayez-en la certitude absolue. La d�mocratie continuera, la r�volution continuera", avait affirm� le pr�sident v�n�zu�lien dans une allocution radio-t�l�vis�e.
Depuis quelques semaines, le gouvernement fait face � une grogne croissante dans un contexte de forte inflation (56,3% en 2013), de p�nuries r�currentes frappant les denr�es alimentaires ou les produits de consommation courante et d'une ins�curit� que les autorit�s ne parviennent pas � juguler.
- Les m�dias en premi�re ligne -
Dans le cadre de ce qui ressemble � une contre-offensive apr�s la mobilisation de l'opposition, les autorit�s ont annonc� jeudi l'�mission d'un mandat d'arr�t contre un des principaux opposants, Leopoldo Lopez.
Une juge v�n�zu�lienne a acc�d� � une demande du minist�re public en ordonnant son arrestation, selon le quotidien El Universal, qui a publi� une photo du mandat d'arr�t sur son portail internet.
M. Lopez, 42 ans, est accus� d'homicide et d'association de malfaiteurs, selon le journal.
Leader de Volont� populaire, une des principales composantes de la coalition de l'opposition, M. Lopez fait partie d'un petit groupe d'opposants pr�nant la contestation dans la rue, sous le mot d'ordre "La Sortie". Son parti a pr�vu de donner une conf�rence de presse � la mi-journ�e.
Jeudi matin, les centres-villes de plusieurs agglom�rations du pays demeuraient sous la haute surveillance des forces de s�curit�. Sur les r�seaux sociaux, plusieurs leaders �tudiants ont appel� leurs partisans � ne pas quitter la rue, sans toutefois lancer un appel unifi� � la manifestation.
Une porte-parole de Maria Corina Machado, qui faisait partie des personnalit�s � l'initiative de la marche de mercredi, a indiqu� � l'AFP qu'aucun appel n'avait pour l'heure �t� lanc�.
Mercredi, des jeunes ont incendi� plusieurs v�hicules de police et s'en sont pris aux forces de l'ordre, qui ont r�pliqu� � coups de gaz lacrymog�nes. Un photographe de l'AFP a pu voir des hommes arm�s circulant � moto tirer en direction des manifestants.
Du c�t� des m�dias, la cha�ne de t�l�vision colombienne d'informations NTN24, qui a assur� une large couverture des �v�nements de mercredi, avait disparu des canaux des deux op�rateurs par c�ble qui la diffusaient.
Les autorit�s n'avaient pas r�agi jeudi � ce sujet, mais le Conseil national des T�l�communications avait auparavant menac� de sanctions les m�dias qui feraient "la promotion de la violence".
Mercredi, des militants pro-gouvernementaux s'en sont pris � des journalistes, dont une �quipe de l'AFP et d'une autre agence de presse, � qui ils ont d�rob� deux cam�ras. Au cours de l'incident impliquant l'�quipe de l'AFP, la police �tait pr�sente sur les lieux mais n'est pas intervenue.
R�agir � l'article
