TITRE: Herm�s rel�ve sa pr�vision de marge apr�s un 4e trimestre solide
DATE: 2014-02-13
URL: http://www.boursorama.com/actualites/hermes-releve-sa-prevision-de-marge-apres-un-4e-trimestre-solide-9f1ab8aba66f8373c89ed0a9ec54508d
PRINCIPAL: 0
TEXT:
Herm�s rel�ve sa pr�vision de marge apr�s un 4e trimestre solide
Reuters le 13/02/2014 � 09:02
Imprimer l'article
HERM�S OPTIMISTE POUR SA MARGE OP�RATIONNELLE 2013
PARIS (Reuters) - Herm�s a vu sa croissance organique ralentir au quatri�me trimestre, tout en signant une des meilleures performances affich�es jusqu'ici par les acteurs du luxe, et a r�vis� en hausse sa pr�vision de marge op�rationnelle 2013.
Le sellier, r�put� pour sa r�silience gr�ce � un positionnement tr�s haut de gamme moins touch� par le ralentissement en Chine, a publi� un chiffre d'affaires annuel de 3,75 milliards d'euros, signant une hausse limit�e � 7,8% par l'impact d�favorable de l'appr�ciation de l'euro face au yen et au dollar, et de 13% hors variations de change.
Le consensus ThomsonReuters I/B/E/S �tait de 3,78 milliards d'euros.
Sur le seul quatri�me trimestre, sa croissance organique est ressortie � 11%, en retrait par rapport aux 12,9% du troisi�me trimestre, mais sur une base de comparaison particuli�rement �lev�e: les ventes avaient grimp� de 18,5% au dernier trimestre 2012.
Fort de ces chiffres, Herm�s dit anticiper une marge op�rationnelle courante 2013 "tr�s l�g�rement sup�rieure" au plus haut niveau historique de 32,1% atteint en 2012.
Malgr� ce tassement au 4e trimestre, le fabricant des sacs Birkin et des "carr�s" de soie fait mieux que nombre de ses grands concurrents, � commencer par LVMH, qui d�tient 23% de son capital et qui sign� une croissance de 8% en fin d'ann�e, surtout tir�e par ses circuits de distribution s�lective (Sephora, les r�seaux DFS de duty free). Le suisse Richemont, propri�taire de Cartier, a ralenti la cadence � 9%, quand l'italien Prada a lui aussi publi� une croissance organique de 11%.
Par ailleurs, compte tenu de sa tr�sorerie disponible, le groupe a d�cid� de verser � ses actionnaires un acompte sur dividende de 1,5 euro par action le 28 f�vrier.
Pascale Denis, �dit� par Jean-Michel B�lot
� 2013 Thomson Reuters. All rights reserved.
Reuters content is the intellectual property of Thomson Reuters or its third party content providers. Any copying, republication or redistribution of Reuters content, including by framing or similar means, is expressly prohibited without the prior written consent of Thomson Reuters. Thomson Reuters shall not be liable for any errors or delays in content, or for any actions taken in reliance thereon. "Reuters" and the Reuters Logo are trademarks of Thomson Reuters and its affiliated companies.
