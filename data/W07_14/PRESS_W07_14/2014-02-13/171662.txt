TITRE: Vid�o M�t�o : alertes - La Cha�ne M�t�o
DATE: 2014-02-13
URL: http://videos.lachainemeteo.com/videos-meteo/video-alertes/alerte-orange---tempete-ulla-137882.php
PRINCIPAL: 171661
TEXT:
Communiqu� sp�cial
Alerte orange : temp�te Ulla
Une nouvelle temp�te, ULLA, est attendue sur le Nord-Ouest du pays dans la nuit de vendredi � samedi. Les vents pourront atteindre 130 � 140km/h sur les caps expos�s et 120km/h dans les terres. Les sols satur�s d'eau conjugu�s � des coefficients de mar�es � la hausse font �galement craindre de nouvelles inondations en Bretagne. Le point avec R�gis Cr�pet, m�t�orologue � La Cha�ne M�t�o.
COMMENTAIRES
