TITRE: INDICES: Le CAC 40 reprend son souffle, BNP Paribas p�se - BFMTV.com
DATE: 2014-02-13
URL: http://www.bfmtv.com/economie/indices-cac-40-reprend-souffle-bnp-paribas-pese-709976.html
PRINCIPAL: 170365
TEXT:
r�agir
PARIS (Dow Jones)--Les valeurs fran�aises �voluent en baisse jeudi, reprenant leur souffle apr�s avoir align� sept s�ances cons�cutives de progression et alors que le march� doit dig�rer de nombreuses publications de r�sultats d'entreprises.
A 12h40, le CAC 40 reculait de 0,5% � 4.284,44 points. Le SBF 120 c�dait �galement 0,5%, � 3.339,54 points.
Le titre BNP Paribas p�se sur la cote parisienne, avec un repli de 4,4% � 58,16 euros. La banque a annonc� une baisse inattendue de son b�n�fice net au quatri�me trimestre, en raison d'une provision de 1,1 milliard de dollars.
A l'inverse, les publications de Renault (+6,2% � 69,95 euros), de Legrand (+4,7% � 42,59 euros), d'EDF (+3% � 26,86 euros), de Publicis (+2,2% � 67,17 euros) et de Pernod Ricard (+0,9% � 83,95 euros) sont bien re�ues par les investisseurs.
Sur le SBF 120, Solocal abandonne 5,6% � 1,34 euros, apr�s avoir chut� de plus de 12% dans la matin�e. L'�diteur d'annuaires a publi� des r�sultats annuels en baisse et a annonc� une augmentation de capital de 440 millions d'euros dans le cadre de ses efforts pour restructurer sa dette.
Rexel recule de 5,2% � 18,48 euros alors que le distributeur de mat�riel �lectrique a accus� une chute de 33,8% de son r�sultat net en 2013, en raison d'une baisse de 3,3% de son activit� et d'�l�ments exceptionnels.
Les investisseurs suivront encore dans l'apr�s-midi la publication aux Etats-Unis des inscriptions hebdomadaires au ch�mage, des ventes au d�tail pour le mois de janvier et des stocks des entreprises en d�cembre. En revanche, l'audition de la pr�sidence de la R�serve f�d�rale am�ricaine (Fed), Janet Yellen, par la commission bancaire du S�nat a �t� report�e en raison des temp�tes de neige qui frappent la c�te Est et le sud des Etats-Unis. (blandine.henault@wsj.com)
(END) Dow Jones Newswires
February 13, 2014 06:40 ET (11:40 GMT)
� 2014 Dow Jones & Company, Inc.
Bourse
