TITRE: Le choc fiscal de 33 milliards de 2013 n'a pas été efficace
DATE: 2014-02-13
URL: http://www.lefigaro.fr/impots/2014/02/12/05003-20140212ARTFIG00384-le-choc-fiscal-de-33milliards-de-2013-n-a-pas-ete-efficace.php
PRINCIPAL: 169098
TEXT:
Publié
le 12/02/2014 à 19:39
Pierre Moscovici et Barnard Cazeneuve, respectivement en charge des l'Economie et des Finances, et du Budget. Crédit: François Bouchon/Le Figaro.
Selon Coe-Rexecode, malgré la hausse spectaculaire des impôts, le déficit public n'a été réduit que de 13 milliards d'euros l'année dernière.
Publicité
Tant d'efforts demandés aux contribuables pour, au final, si peu de résultats� L'an dernier, malgré une hausse des prélèvements fiscaux considérable de 33 milliards d'euros, le déficit public n'a été réduit que de 13 milliards, constate le think-tank libéral Coe-Rexecode.
Non seulement le niveau excessif de la pression fiscale (46% du PIB) a une faible efficacité, comme le souligne également la Cour des comptes , mais il nuit à l'investissement et à la croissance, poursuit l'institut. Il préconise d'alléger «en priorité les prélèvements les plus nocifs pour la croissance», où la France est «championne». Dans ce domaine, l'écart avec l'Allemagne s'élève à 113 milliards d'euros. Sur ce total, 65 milliards de prélèvements pèsent sur la production et 35 milliards sur le rendement du capital.
Baisser les prélèvements sur la production
Coe-Rexecode recommande donc de fixer un programme d'allégements de la pression fiscale. En particulier, il faut «redonner de la compétitivité à nos exportations» en baissant les prélèvements sur la production. Le think-tank plaide également pour «une correction des excès manifestes de la fiscalité du capital», qui pèsent sur l'investissement.
Un demi bon point toutefois pour François Hollande, qui a promis de supprimer les cotisations patronales pour la famille dans le cadre du pacte de responsabilité .
«Cette démarche peut réussir en France, comme des démarches similaires ont réussi dans d'autres pays dans le passé, dès lors que des marges de man�uvre sont dégagées du côté des dépenses publiques», conclut Coe-Rexecode. L'exemple de la Suède, dans les années 1990, le prouve.
