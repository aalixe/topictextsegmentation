TITRE: Des cam�ras � La Madeleine pour les clients de prostitu�es - LExpress.fr
DATE: 2014-02-13
URL: http://www.lexpress.fr/actualites/1/styles/des-cameras-a-la-madeleine-pour-les-clients-de-prostituees_1323722.html
PRINCIPAL: 170923
TEXT:
Des cam�ras � La Madeleine pour les clients de prostitu�es
Par AFP, publi� le
13/02/2014 � 15:48
, mis � jour � 15:48
Lille - Le maire UMP de la commune de La Madeleine (Nord), en banlieue de Lille, a r�cemment fait installer des cam�ras de vid�osurveillance sur deux parkings, accompagn�es de panneaux proclamant: "Clients de prostitu�es, ici vous �tes film�s".
Le maire UMP de la commune de La Madeleine (Nord), en banlieue de Lille, a r�cemment fait installer des cam�ras de vid�osurveillance sur deux parkings, accompagn�es de panneaux proclamant: "Clients de prostitu�es, ici vous �tes film�s".
afp.com/Philippe Huguen
"Il y a un probl�me: des parkings qui accueillent r�guli�rement des prostitu�es, qui viennent y consommer des actes sexuels avec leurs clients", a expliqu� jeudi � l'AFP le maire S�bastien Lepr�tre, confirmant une information du quotidien La Voix du Nord.�
- "Information cibl�e" -�
Des cam�ras ont �t� install�es fin 2013 sur deux parkings et les panneaux, command�s en m�me temps, ont �t� pos�s r�cemment: "Quand on installe des cam�ras de vid�osurveillance, on a un devoir d'information, donc on installe des panneaux. Tant qu'� informer, j'ai jug� utile d'�tre dans une information cibl�e", a poursuivi l'�dile.�
M. Lepr�tre a dit assumer le fait que ces panneaux "stigmatisent les clients des prostitu�es", alors que la loi p�nalisant les clients n'a pas encore �t� soumise au vote du S�nat.�
Le maire mise sur l'effet dissuasif des cam�ras et des panneaux: "D�placer un probl�me, c'est aussi commencer � le r�soudre", a-t-il assur�.�
Le cas �ch�ant, ajoute-t-il, les bandes vid�o peuvent aussi �tre r�quisitionn�es par la police "� la suite d'une plainte" mais aussi au regard du fait que "faire l'amour dans une voiture sur le domaine public n'est pas possible, c'est la loi". �
Ces parkings sont situ�s non loin de l'avenue du Peuple Belge, une zone du Vieux Lille traditionnellement arpent�e par les prostitu�es.�
Le maire cite l'exemple d'une fillette, t�moin des �bats d'un couple dans une voiture sur ces parkings: "La maman me demande: +qu'est-ce que je r�ponds � ma fille, qui a six ans et qui me demande ce que font le monsieur et la dame dans la voiture'+"�
Il �voque une situation "p�nible" pour les riverains, contraints de "marcher sur des pr�servatifs, de slalomer entre des lingettes".�
S�bastien Lepr�tre avait d�j� d�fray� la chronique en 2011, en traduisant en roumain et en bulgare un arr�t anti-mendicit� et un arr�t� rappelant l'interdiction de fouiller les containers � poubelles.�
Par
