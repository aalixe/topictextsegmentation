TITRE: Du porno sur une clé USB destinée à des élèves de CE2 - Elle
DATE: 2014-02-13
URL: http://www.elle.fr/Societe/News/Du-porno-sur-une-cle-USB-destinee-a-des-eleves-de-CE2-2668326
PRINCIPAL: 0
TEXT:
Laura Boudoux @Laura_Boudoux
[ Tous ses articles ]�
Il y a eu erreur sur le contenu. Des élèves de CE2 de Besançon ont reçu des clés USB, sensées contenir des liens vers des supports pédagogiques. Mais lorsque les parents ont cliqué sur ces liens, ce sont en fait un site pornographique et une plateforme de jeu de guerre violent qui se sont ouverts, comme le rapporte ce mercredi le site de «�L’Est Républicain�» . La ville de Besançon a rapidement réagi, en envoyant un courrier aux parents d’élèves, ainsi qu’aux enseignants pour expliquer qu’il y avait eu un dysfonctionnement. Les clés USB ont dû être rendues, afin de procéder à un échange.
«�Promotion de sites pour adultes�»
Le directeur général des services de la ville de Besançon, Patrick Ayache, a tenu à faire savoir que la mairie n’avait aucune responsabilité dans cette affaire. Selon lui, les contenus ont été vérifiés une dernière fois, juste avant que les clés USB ne soient expédiées. «�Les noms de domaine ont manifestement été rachetés, de façon légale, par des sociétés peu scrupuleuses qui les utilisent pour la promotion de sites pour adultes�», a-t-il expliqué, selon le «�Parisien�».
