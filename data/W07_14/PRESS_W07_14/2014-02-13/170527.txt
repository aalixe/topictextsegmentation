TITRE: Le CAC reprend son souffle
DATE: 2014-02-13
URL: http://www.ig.com/fr/point-marche/2014/02/13/actu-marches-1302141
PRINCIPAL: 170526
TEXT:
Nicolas Paillery g+ , Paris
2014-02-13T13:26:00+0000
La Bourse de Paris baissait l�g�rement jeudi matin apr�s sept s�ances de hausse d'affil�e, les investisseurs profitant d�une s�ance en demi-teinte hier � Wall Street et du recul des places asiatiques ce matin, pour prendre quelques b�n�fices.
A mi-s�ance, le CAC reculait de 0,54 % � 4 282,38 points, le DAX de 0,21 % � 9 519,87 points et FTSE 100, 0,67 % � 6 630,16 points.
Le march� parisien a affich� une progression de 4,81% lors des sept derni�res s�ances, mais reculait l�g�rement ce matin alors que les bourses am�ricaines ne sont pas parvenues � poursuivre leur hausse hier apr�s quatre jours de progression. Le Dow Jones s�est repli� de 0,19 % � 15 963,94 points et le S&P 500 0,03 % � 1 819, 26 points alors que le Nasdaq s�adjugeait 0,24 % � 4 201,29 points. L�abaissement des pr�visions de b�n�fices annuels de Procter & Gamble et la baisse de la recommandation d'UBS sur Amazon ont, semble-t-il suscit� de l�inqui�tude chez bon nombre d�investisseurs, soulignant une conjoncture �conomique moins florissante qu�il n�y parait, alors que le � tapering � de la FED va se poursuivre et que le gouvernement f�d�ral a �t� autoris� � emprunter davantage.
La publication des r�sultats de BNP Paribas ce matin, contrairement � sa cons�ur Soci�t� G�n�rale hier, pesait sur la cote parisienne. La banque a annonc� avoir d� passer une provision de 798 millions d'euros li�e � une enqu�te des autorit�s am�ricaines sur des transactions effectu�es avec des pays soumis � un embargo des Etats-Unis. De fait, cette provision explique la chute du r�sultat net part du groupe de 75,5 % � 127 millions d'euros entre octobre et d�cembre. Pour 2013, le b�n�fice net s�est repli� de 26,4 %.
Sur un plan macro�conomique, outre-Rhin, l�inflation a baiss� de 0,6 % en janvier selon l�Office f�d�ral des statistiques confirmant ainsi sa premi�re estimation et la baisse des prix en terme annuel � 1,3 % contre 1,4 % en d�cembre. �
Outre-Atlantique, les investisseurs seront attentifs aux chiffres concernant les ventes au d�tail de janvier et les inscriptions hebdomadaires au ch�mage � 14h30 et enfin, aux stocks des entreprises de d�cembre � 16h00.
Parmi les valeurs fran�aises, BNP Paribas chutait fortement de 4,40 % � 58,17 euros entrainant � sa suite le secteur bancaire. Natixis perdait 1,63 % � 4,652 euros, Cr�dit Agricole 1,42 % � 10,405 euros et Soci�t� G�n�rale, 0,77 % � 45,995 euros.
Renault s�envolait de 6,04 % � 69,86 euros malgr� un b�n�fice net en fort recul mais une marge op�rationnelle meilleure que pr�vue.
EDF progressait de 3,26 % � 26,93 euros apr�s l�annonce de r�sultats en hausse en hausse en 2013 et la confirmation par le groupe de la poursuite de la ma�trise de ses co�ts.
Pernod Ricard s�adjugeait 1,12 % � 84,15 euros malgr� l'abaissement de ses objectifs financiers en raison de pi�tres performances en Chine.�
Publicis prenait 1,93 % % � 67,01 euros. Le groupe a annonc� une nouvelle ann�e record �pour son b�n�fice net et son chiffre d'affaires concernant l�exercice 2013.
Legrand bondissait de 4,62 % � 42,57 euros apr�s l�annonce d'un b�n�fice net record de 530 millions d'euro pour 2013.
Rexel chutait de 5,82 % � 18,79 euros apr�s la baisse de son b�n�fice net d'un tiers en 2013 et l�annonce d�un chiffre d'affaires et d�une marge op�rationnelle courante inf�rieurs au consensus.�
Kering baissait de 1,56 % � 151,70 euros p�nalis� par un abaissement de recommandation de JPMorgan Cazenove � "neutre" contre "surpond�rer".
Enfin, Haulotte Group flambait � 12,53 euros en progression de 10,69 % apr�s l'annonce d'une hausse de 3% de ses ventes en 2013.
L�Euro rebondissait � 1,3664 Dollar, l�or progressait l�g�rement � 1 292,97 dollars l�once et le Brent, �ch�ance avril 2014, se repliait � 108,18 dollars le baril.
�
�
Avertissement :�IG fournit exclusivement un service d�ex�cution d�ordre. Les informations ci-dessus ne sont fournies qu�� titre indicatif. Elles ne constituent, ni ne doivent �tre interpr�t�es comme un conseil ou une recommandation. Elles ne constituent pas non plus un historique de nos cotations ou une offre ou sollicitation � investir dans un quelconque instrument financier. IG Markets se d�gage de toute responsabilit� concernant l�utilisation qui en est faite et des cons�quences qui en r�sultent. IG ne peut garantir que l�information fournie ci-dessus soit compl�te ou exacte et se d�gage donc de toute responsabilit� quant aux risques encourus par toute personne agissant sur la seule base de ces informations. Veuillez noter que ces informations ne prennent nullement en compte la situation financi�re et les objectifs d�investissement sp�cifiques aux personnes qui les re�oivent. Enfin, ces informations n�ont pas �t� con�ues pour r�pondre aux exigences l�gales en mati�re d�ind�pendance de la recherche sur l�investissement. Elles doivent donc �tre consid�r�es comme une communication � des fins marketing. Il est strictement interdit de reproduire ou de distribuer tout ou partie de ces informations � des fins commerciales ou priv�es.
