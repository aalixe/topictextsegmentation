TITRE: Le TGV fait plonger le b�n�fice de la SNCF
DATE: 2014-02-13
URL: http://www.boursorama.com/actualites/le-tgv-fait-plonger-le-benefice-de-la-sncf-3c0c3710e5c2bfaf9923fd9ed9215637
PRINCIPAL: 170336
TEXT:
Le TGV fait plonger le b�n�fice de la SNCF
Le Figaro le 13/02/2014 � 13:00
Imprimer l'article
La SNCF a d�pr�ci� la valeur de son parc de TGV de 1,4 milliard d'euros en raison d'une forte baisse de la rentabilit� de l'activit� de ces trains en France.
Il y a deux ans, la SNCF avait �d�pr�ci� son parc de TGV de 700 millions d'euros. Cette ann�e, la d�pr�ciation des rames TGV double et atteint 1,4 milliard d'euros. Cette op�ration comptable, qui consiste � r�duire la valeur des actifs en raison d'une moins bonne rentabilit� du mod�le �conomique du TGV, a pour cons�quence de faire plonger dans le rouge le r�sultat net part du groupe pour l'ann�e 2013. La perte nette atteint 180 millions d'euros en 2013 alors qu'en 2012, le groupe avait d�gag� un b�n�fice de 376 millions d'euros. Le chiffre d'affaires du groupe en revanche est stable � 32,2 milliards d'euros.
Trois branches de la SNCF sur cinq voient leur activit� progresser: SNCF Infra (+4,2%) qui r�alise les travaux de maintenance et de renouvellement des infrastructures, SNCF Proximit�s (+1,1%) ...
