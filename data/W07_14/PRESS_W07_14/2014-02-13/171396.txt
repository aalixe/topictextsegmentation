TITRE: PSG: Lavezzi est l� - Football - Sports.fr
DATE: 2014-02-13
URL: http://www.sports.fr/football/ligue-1/scans/psg-lavezzi-est-la-1009804/
PRINCIPAL: 171391
TEXT:
13 février 2014 � 18h23
Mis à jour le
13 février 2014 � 18h30
Le Paris Saint-Germain accueille Valenciennes vendredi soir au Parc des Princes, à l'occasion de la 25e journée de Ligue 1. Pour cette rencontre, Laurent Blanc sera toujours privé d'Edinson Cavani et de Christophe Jallet . Touché au mollet, Zoumana Camara a également déclaré forfait pour ce match.
Le coach francilien pourra en revanche compter sur Zlatan Ibrahimovic , malgré ses problèmes de dos. Endeuillé cette semaine par le décès de son oncle, Ezequiel Lavezzi est bien présent dans le groupe.
Le groupe parisien : Sirigu, Douchez - Alex, Digne, Marquinhos, Maxwell , Silva, Van der Wiel - Cabaye, Verratti, Motta, Rabiot, Matuidi, Lucas , Pastore - Ménez, Ibrahimovic, Lavezzi.
 
