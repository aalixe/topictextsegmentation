TITRE: Les Trois fr�res, le retour : "ce ne sont pas les critiques qui font les entr�es" [INTERVIEW] - News films Interviews - AlloCin�
DATE: 2014-02-13
URL: http://www.allocine.fr/article/fichearticle_gen_carticle%3D18630783.html?utm_source%3Dfeedburner%26utm_medium%3Dfeed%26utm_campaign%3DFeed%253A%2Bac%252Factualites%2B(AlloCine%2B-%2BRSS%2BNews%253A%2BCinema%2B%2526%2BSeries)
PRINCIPAL: 171352
TEXT:
^^
Chris46
Pour les " trois fr�res le retour " il y a un gouffre �norme entre ce qu'� penser la presse qui trouve le film nuliissime et une grande partie des spectateurs qui on bien appr�cier le film . Je croit que c'est la premi�re fois qu'il y a un telle d�calage presse / spectateurs . N'ayant pas encore vu le film je pr�f�re me fier a l'avis des spectateurs . C'est pas la premi�re fois que la presse d�monte un bon film . Y a qu'� regarder les notes presses de forrest gumps ou retour vers le futur 2 sur allocine pour voir que c'est juste hallucinant .
Forza 69
L'exemple de la critique presse la plus en d�calage par rapport aux retours des spectateurs est pour moi celle de Fight Club � l'�poque , la presse avait d�glingu� le film alors que les spectateurs l'avaient mis sur le toit des meilleurs de l'ann�e !!
Depuis ce jours je me moque de ce que la presse pense d'un film , c'est devenu plus que secondaire � mes yeux tellement ils ne recherchent pas la m�me chose que les spectateurs lors d'un film !!
LBLebowski
Je n'avais pas sp�cialement envie de voir ce film, le 1er m'avait bien fait marrer mais ils �taient au sommet de leur forme et c'est vrai que ce retour me semblait un peu poussif.
Mais apr�s la vol�e de critiques incroyablement virulentes qu'ils se sont pris �a me fait de la peine pour eux, et j'ai presque envie d'aller voir si c'est vraiment m�rit�.
Chris46
Il y a la ligne verte aussi avec Tom Hanks ou la plupart de la presse a d�monter le film ( note presse moyenne de 2,8 /5 sur allocine ) alors que le public lui a donner une excellente note ( 4,5/5) .
Et c'est vrai que pour fight club le d�calage est hallucinant aussi avec une moyenne de 2,7/ 5 pour la presse contre 4,5/5 pour les spectateurs .
Adam Jensen
La critique presse cherche toujours la petite b�te pour d�glinguer un film. Je ne suis pas fan des films Fran�ais mais quand m�me. D�monter autant un film, faut le faire. De toute fa�on, tant que ca vous plait ou non, c'est le principal, je pense.
cinephiledu59
Oui ou encore American History X
cinephiledu59
M�me les �vad�s, qui est pour moi un des plus grands films du cin�ma, la presse : 3.2 c'est pas �norme compar� aux spectateurs.
cinephiledu59
Oui, c'est clair nous on va aller le voir pour se bidonner, c'est une com�die populaire, mais de toute fa�on la presse n'aime pas �a.
JimBo Lebowski
Je ne comprends pas vraiment pourquoi, pour ce film en particulier, le public prenne autant sa d�fense par rapport � la presse ... Ils attaquent pas Les Inconnus eux m�me mais leur cin�ma, faut reconnaitre que les loustiques trainent quelques casseroles comme "Les Rois Mages" ou "L'Extraterrestre", ils ont pas une l�gitimit� cin�matographique.
La presse, a ce que j'en ai lu sur Allocin�, n'est pas si agressive que �a, et eux ne sont pas l� pour passer un bon moment en famille ou entre amis ils sont l� pour juger un long m�trage, c'est leur boulot.
Et puis je ne pense pas que �a ai un impact sur l'�chec commercial, au contraire, chacun aura envie de se faire son avis.
Et puis de toute fa�on j'ai l'impression qu'on voue une passion a pisser contre le sens du vent, si la presse avait dit unanimement que c'�tait une r�ussite, les spectateurs auraient dit "bwa pas tant que �a faut pas exag�rer, c'est m�me une daube".
Au final �a va plus les aider qu'autre chose tout ce n�gativisme journalistique, ils finiront m�me par les remercier �a se trouve.
Chris46
Ils prennent leur d�fense car d'apr�s les spectateurs la qualit� du film ne m�rite pas des notes aussi basse de la part de la presse . Apr�s je ne sait pas ce que vaut le film je ne l'ai pas encore vu .
JimBo Lebowski
Je ne l�ai pas vu non plus mais la presse a autant d�importance que �a au final ? Un retour des Inconnus et suite d�un film culte c��tait d�j� suffisant pour rentrer dans leurs frais et faire un bon chiffre d�entr�es.
L� on tombe presque dans un d�bat social "Bobo journaliste �lite parisiano gauchiste VS Le peuple" c�est un peu ridicule.
bluelight o.
"j'ai l'impression qu'on voue une passion a pisser contre le sens du vent"
"Bobo journaliste �lite parisiano gauchiste VS Le peuple" c�est un peu ridicule"
Mais VA LE VOIR au milieu de d�battre pendant des heures.. en plus ton ressentis commence vraiment � devenir ridicule, parce que la majorit� de ceux qui prennent la d�fense du film EUX L'ONT VU.
JimBo Lebowski
Je n'ai aucun ressenti, justement je n'ai pas vu le film, je parle juste de l'importance (ou non) de la presse et de l'influence (ou non) sur le public.
Je vais le voir demain ...
bluelight o.
Tu ne peux pas dire apr�s t'avoir plusieurs fois lu que tu pars plein d'espoir, je sais de plus que tu est assez sensible � la critique comme moi voir m�me plus
JimBo Lebowski
�a c'est vrai que je pars avec un a priori, mais je ne tire pas de conclusion, j'attends de voir, j'en reparlerai demain soir.
Doctor Nico
Bien d'accord avec lui, ON S'EN FOUT DES CRITIQUES PRESSES !!! La presse adule des films o� je me suis endormi, a d�test� des films que je regarde en boucle, parfois on est m�me totalement d'accord en bien ou en mal sur certains films... Bref, je m'en contrefiche, je vais sur les sites de cin� pour les news et pour voir des avis de spectateurs, qui payent pour voir les films et pas de mecs qui sont pay�s pour les regarder. Le pire c'est qu'� la base, quand j'ai fait section audiovisuelle au bahut, je pensais devenir critique cin� ! xD Mais j'ai eu peur que �a me d�go�te du cin� quand je vois � quel point certains sont blas�s de tout... Un peu comme les critiques gastro qui chipotent sur des d�tails insignifiants.
borat8
Sauf que quand il y a autant de mauvaises critiques, d�j� c'est synonyme de mauvais signe. C'est un peu comme les derniers films de Dany Boon. �a va bien march� puis plouf �a va retomber d'un coup quand le bouche � oreille sera affreux.
ioneon33
les critiques seraient valables si nous �tions tous pareils.
donc au chiottes les critiques .... votre avis, faites le en allant voir le film.
Elisariel
Bah ! Fallait s'y attendre.
Primo c'est une com�die (est-ce qu'il y a eut des com�dies r�compens�es aux C�sars ces derni�res ann�es ?), secondo Messieurs Dubosc et Adams ne font pas partie du casting. Je sais, c'est m�chant...
J'ai lu les messages et je suis d'accord avec ceux qui pointent le gouffre abyssal qui s�pare si souvent l'opinion des spectateurs de celle des "critiques-de-cin�ma-qui-savent-tout" (pour rester polie) surtout sur des �uvres devenues cultes entretemps.
Donc, je vais voir le film et apr�s je donne ma subjective opinion. Et biiiiiiiiip aux m�dias haineux.
borat8
On peut aussi �viter de payer pour voir n'importe quoi aussi...
lexcalvin
"Ce ne sont pas les critiques qui font les entr�es"
Johnny Depp n'est pas de cet avis, puisqu'il a accus� la critique d'�tre responsable des mauvais r�sultats de Lone Ranger � l'�t� 2013 ....
http://www.lefigaro.fr/cinema/...
Mettez vous d'accord les gens.
maxime s.
les critiques sont certes virulentes mais pour la plupart justes, Pourquoi avoir fait une suite au trois fr�res ?? ce film brillant, dr�le et touchant � la fois par l'enteremise de ce jeune com�dien talentieu, se suffsait � lui m�me ( au d�part 3 loosers, orphelins, immatures qui au traver de leurs p�rigrinations et de la rencontre avec ce petit gar�on vont �voluer et devenir plus "adultes" pour finalement se trouver une famille, Dider se d�couvrira m�me une vocation de p�re). Dans ce second opus on apprend qu'ils sont finalement rest� des gros cons immatures 20 ans apr�s, �a d�truit toute la morale du 1er qui n'a servit � rien et �a n'apporte rien au second. Les inconnus auraient mieux fait pour le "retour" de nous proposer un nouveau film, dans un nouvel univers original. Cela aurait �t� plus judicieux de leur part plut�t que de simplment reprendre presque � la ligne et en beaucoup moins bien le sc�nario de leur film d'origine. Ils tendent le baton pour se faire battre.
Meldoise
Tout � fait d'accord avec Antoine du Merle ! Il faut se faire sa propre opinion et pas se baser sur les critiques presse. Je me souviens combien la presse avait descendu en fl�che le nouveau James Bond interpr�t� par Daniel Craig parce qu'il avait le malheur d'�tre blond ! Et au final le film a fait un carton au niveau des entr�es...
Robert de Rio
peut �tre qu'il manque de recul l'ami Johnny parce qu'en toute objectivit�, ce film �tait r�ellement rat� � plusieurs niveaux ! et de voir le r�sultat si d�cevant avec une telle d�bauche de budget montrait d�s les premi�res sc�nes qu'ils �taient tomb�s dans une grande simplicit�.
Alors si les critiques positives peuvent construire et augmenter le succ�s d'un film ; les n�gatives peuvent aussi le saborder dans une certaine proportion.
Mais j'ose esp�rer que le r�sultat commercial final d�pend essentiellement de la qualit� intrins�que du film ... mais je peux me tromper !
Guillaume182
Les inconnus poss�de un gros capital sympathie, mais leurs film n'est qu'un remake du premier.
Elisariel
Je viens de lire l'opinion de quelques fran�ais qui, n'�coutant pas l'opinion pointue des critiques bobos bien pensants, sont all�s voir le film, ces inconscients...
Pour l'instant, loin d'�tre traumatis�s, ils sont majoritairement tr�s contents d'avoir pass� un excellent moment au milieu d'autres inconscients regroup�s dans diverses salles de cin� et qui riaient de bon c�ur. Tout simplement.
Que penser de �a ?
Joss Beaumont
Si les plus grands comme de Fun�s et B�bel �coutaient les mauvaises critiques ils arr�taient leur carri�re depuis longtemps. Mais ils ont continu� et ils "avaient" raison car maintenant leurs films sont � tout jamais grav�s dans nos m�moires. C'est le publique qui a le dernier mot. Dans quelques ann�es ces mauvaises critiques laisseront place � l'attribution de la com�die culte avec des r�pliques inoubliables: "l'Obama de sarcelles" ou "le cousin rat� de M�lenchon". Et surtout la super chanson du g�n�rique final qu'on n'est pas pr�t d'oublier!
Joss Beaumont
Des remakes de cette qualit� j'en veux d'autres!
Joss Beaumont
La meilleur critique c'est celle de soi. La critique d'autrui pollue nos propres envies et nos propres avis. On est des cin�phile mais pas de critiquephile. Alors allons voir les films nous m�me pour avoir notre propre avis. C'est avec du recul que l'on reconna�t un film culte. Les critiques pr�matur�es sont �ph�m�res mais le nombre des ann�es justifie la vraie valeur d'un film.
Joss Beaumont
Les journalistes ne payent jamais leur place de cin�ma pour dire n'importe quoi non plus!
Mais pour nous, il y a plus valorisant que le prix d'un billet de cin�ma, c'est le prix de notre personne qui a le droit aussi de donner son propre avis.
tenia54
Quelles com�dies r�compens�es aux C�sars ?
2013 : meilleurs 2nds r�les masculins et f�minins (Le pr�nom). 13 nominations pour Camille redouble, 5 pour Le pr�nom et Populaire.
2012 : Meilleur acteur pour Omar Sy (Intouchables), meilleur 2nd r�le f�minin pour Carmen Maura (Les femmes du 6e �tage), meilleure adaptation (Carnage),. 9 nominations pour Intouchables, 6 pour La guerre est d�clar�e.
2011 : meilleure actrice (Sara Forestier - le nom des gens), meilleur 2nd r�le f�minin (Anne Alvaro - Le bruit des gla�ons), meilleur espoir f�minin (Tout ce qui brille), meilleur sc�nario original (Le nom des gens). 5 nominations pour L'arnacoeur.
2010 : meilleur 1er film (Les beaux gosses). 3 nominations pour Les beaux gosses.
Qui sont les plus souvent nomm�s et les plus grands gagnants ?
On conna�t la chanson est reparti avec 7 C�sars, Smoking / No Smoking 5.
Certains comme Jean Pierre Jeunet (pas le dernier des r�alisateurs pour bobos auteurisants... c'te clich� sophiste, mais bref) a 6 C�sars � son actif. Bacri et Jaoui (pas vraiment sp�cialis�s non plus dans le gros drame qui t�che) cumulent en duo 5 nominations pour 4 C�sars.
Les grands acteurs / actrices gagnants s'appellent Depardieu, Rochefort, Serrault et Auteuil. Ou Adjani, Baye, Deneuve et Cotillard.
Les com�dies et les t�tes connues n'ont jamais �t� autant repr�sent�es que ces 10 derni�res ann�es, o� elles gl�nent une repr�sentation tr�s importante aux C�sars.
Aussi, je tiens juste � rappeler (juste au cas o�, parce qu'on pourrait penser que c'est �vident, mais visiblement non) qu'on peut aimer Les 3 fr�res mais aussi Ingmar Bergman, tout en trouvant Les ch'tis pas dr�le du tout.
Pour rappel.
Et aussi qu'aucun critique ne cl�me avoir la v�rit� absolue. Il est pay� pour donner son avis. Il le donne. Libre aux gens de le suivre ou pas. La preuve : Les 3 fr�res font 1M d'entr�es alors qu'ils se sont fait d�monter par la presse. Je vais pas m'avancer sur le fait que "youpi, c'est le meilleur d�marrage de l'ann�e" comme le fait Antoine du Merle, parce que bon, on est qu'en f�vrier donc y a rien de glorieux, mais c'est bien la preuve que les gens n'ont pas le couteau sous la gorge.
Tout �a pour dire que les clich�s sophistes ad hominem sans aucun chiffre, ils sont rigolos, mais �a fait 15 ans qu'ils sont rabach�s. Il serait peut-�tre temps de changer de disque.
Les critiques qui aujourd'hui trouvent que Les 3 fr�res le retour n'est pas tr�s bon sont les m�mes qui ont trouv� au moins bons (si ce n'est excellents) Fantastic Mr Fox, Camille redouble, Les beaux gosses, Les lascars, La grande aventure Lego, 9 mois fermes, Paulette, Ernest et C�lestine, Looper, Les Muppets et Le pr�nom.
Maintenant, libres � vous de chaque fois remettre une couche sur le fait que tout votre argumentaire n'est bas� que sur des impressions.
Qu'il y a si souvent un "gouffre abyssal qui s�pare si souvent l'opinion des spectateurs de celle des "critiques-de-cin�ma-qui-savent-tout"" (gouffre abyssal ? Faut que je fasse l'inventaire des films o� ce n'est pas le cas ? Critiques qui savent tout ? Cf au dessus, vous avez un couteau sous la gorge ? Des gens qui font litt�ralement pression pour montrer qu'ils ont la V�rit� Absolue).
Que les spectateurs viennent en nombre (les bouses de Max P�cas faisaient 7M d'entr�es � l'�poque, faut-il le rappeler). Que les spectateurs se marrent beaucoup dans la salle (qui l'eut cru ? Moi qui pensais que le public principal payant 12� pour aller voir Les 3 fr�res 2 n'�taient que des gens qui voulaient jeter des tomates au film...).
Que les com�dies ne sont jamais r�compens�es aux C�sars (cf au dessus).
Croyez moi, ce sont les gens qui crient constamment au loup-critique qui font beaucoup de tort � des films comme celui ci. Tout ce qu'on voit, ce sont des argumentaires remplis de sophismes et autres trous de r�flexion, tous sautant de g�n�ralit�s en clich�s de pilier de comptoir.
C'est vraiment triste de toujours voir le m�me d�bat, depuis 20 ans.
Les critiques n'aiment pas Les 3 fr�res 2 ? Formidable. Ca n'emp�che visiblement personne d'aller le voir. Et si c'�tait le cas, posez-vous la question : ont-ils mis un flingue sur la tempe des gens dissuad�s ? Ou ont-ils juste des arguments convaincants ?
Bref.
On se retrouve � la prochaine com�die de qualit� discutable, je suppose.
tenia54
Sauf qu'Allocin� n'archive beaucoup de critiques presse pour un m�me film que depuis quelques ann�es. Plus on remonte dans le temps, plus il est courant de tomber sur une fiche avec tr�s peu de notes (et donc, forc�ment, une moyenne biais�e). Si vous prenez un film des ann�es 90 comme Forrest Gump, il n'y a... que 5 critiques presse pour faire la note d'Allocin�.
Combien y en a-t'il en g�n�ral ? Environ une quinzaine. C'est ce qu'on voit sur American History X (qui n'est pas terrible et m�rite bien sa note, mais c'est une autre histoire).
Apr�s, faut aussi prendre en compte les explications culturelles d'alors. Fight Club, faut rappeler que le film a �t� super mal accueilli � l'�poque (et c'est partout pareil, chez nous comme ailleurs en Europe ou aux USA).
Ce qu'il ne faut pas oublier, c'est qu'Allocin� ne prend quasi jamais en compte les critiques "nouvelles" (exemple : une critique de 2014 sur un film de 1995).
tenia54
Disons qu'il y a quelque chose qui me g�ne beaucoup dans les "argumentaires" que je lis contre les critiques.
On a le droit d'aimer le film. Je vais �liminer les �vidences maintenant. Pour autant, on a aussi le droit de ne pas l'aimer, non ?
Ensuite, le souci (donc), c'est que la plupart des "argumentaires" que je lis ici sont tr�s facilement d�montables.
Les critiques n'aiment pas Les 3 fr�res 2, c'est parce qu'ils n'aiment pas les com�dies ? Voyez les exemples ci dessus.
Les com�dies sont toujours d�laiss�es des r�compenses ? Idem. M�me si Intouchables n'est pas La soupe aux choux, je suppose qu'on est tous d'accord pour dire que ce n'est pas non plus du Haneke, et que �a a quand m�me pour but 1er de faire rire et sourire plut�t que pleurer et d�primer.
Donc voil�. Quand je lis "pronostics venimeux", �a me fait doucement sourire, parce que c'est tout sauf factuel.
Y a juste des gens, dont c'est le m�tier de voir des films, qui ont des bagages relativement remplis dans le domaine, et qui trouvent que, quand m�me, c'est pas fameux.
Des films comme �a, qu'ils ne trouvent pas fameux, y en a plein. Y a des com�dies, des drames, des films d'action, de science fiction, d'aventures. Et ils sont d'origines tr�s diff�rentes.
Mais l�, on leur pr�te toutes les intentions du monde sous pr�texte que "quand m�me, c'est le retour des Inconnus pour une suite de leur plus grand succ�s".
Moi m�me suis le 1er � trouver que c'est surtout un plan com' g�ant, o� il est tellement plus facile d'aller refaire la m�me soupe � succ�s d'il y a 12 ans plut�t que d'innover (et pourtant, j'adore Les 3 fr�res), alors que Legitimus a une excellente carri�re au th��tre (et avait une tr�s bonne s�rie polici�re sur France 2) et Campan faisait des choses tr�s bien (Se souvenir des belles choses, en particulier). Reste Bourdon qui gal�re pas mal, encha�nant navet sur navet. Si on m'apprend dans 5 ans que c'est pour lui que le film s'est fait, je ne serais pas surpris. Mais bref, je m'�gare.
Le fait est que quand les grands m�chants critiques va dans le sens du poil, tout le monde est content. Mais sinon, ah l� l�, tout de suite les grands mots.
Pas de juste milieu, pas d'argumentaire construit, rien. C'est juste des grands m�chants pas beaux. C'est tout juste si on ne leur pr�te pas un retour financier si le film fait un flop.
Top Bandes-annonces
