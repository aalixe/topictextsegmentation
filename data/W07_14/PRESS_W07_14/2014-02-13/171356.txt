TITRE: Philip Seymour Hoffman pris dans un triangle amoureux avant sa mort ?
DATE: 2014-02-13
URL: http://people.premiere.fr/News-People/Philip-Seymour-Hoffman-pris-dans-un-triangle-amoureux-avant-sa-mort-3952184
PRINCIPAL: 171352
TEXT:
Philip Seymour Hoffman pris dans un triangle amoureux avant sa mort ?
Philip Seymour Hoffman pris dans un triangle amoureux avant sa mort ?
12/02/2014 - 21h23
0
� Abaca
Depuis l�annonce de sa mort il y a dix jours, de nouveaux �l�ments de l�existence de Philip Seymour Hoffman ont filtr� dans la presse, afin de comprendre les circonstances de son d�c�s. On y apprend notamment que l�acteur faisait face � un dilemme amoureux juste avant de quitter ce monde.
Les autorit�s continuent � enqu�ter sur la tragique disparition d�un des plus grands acteurs de sa g�n�ration, retrouv� mort dans son appartement , avec une aiguille plant�e dans le bras, et plusieurs sachets d�h�ro�ne.
Selon le New York Post, le journal intime de la star est �pluch� � la lettre et on y d�couvre notamment que Philip Seymour Hoffman �tait pris dans un triangle amoureux avant son d�c�s. En plus de la m�re de ses trois enfants, Mimi O�Donnell, le h�ros de Truman Capote aurait entretenu une relation avec une autre femme. Cette autre femme aurait jou� un r�le pr�pond�rant dans le choix de Mimi O�Donnell de demander � son compagnon de quitter le domicile familial, alors qu�on pensait initialement que les addictions de la star avaient motiv� son choix.
Dans son journal, Philip Seymour Hoffman a �galement confi� sa "honte de s��tre remis � sortir et � boire", apr�s �tre rest� sobre pendant 23 ans.
Ecrit alors qu�il �tait en rehab l�ann�e derni�re, ce journal renferme quelques tentatives d�introspections, mais la grande majorit� reste des �garements d�nu�s de sens.
La disparition de Philip Seymour Hoffman a profond�ment �branl� la sph�re hollywoodienne, aux yeux de laquelle il �tait tr�s consid�r�. Depuis l'annonce de son d�c�s, les divers hommages affluent.
