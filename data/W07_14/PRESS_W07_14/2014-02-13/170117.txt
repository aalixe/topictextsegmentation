TITRE: AFGHANISTAN � Lib�ration de 65�"terroristes" | Courrier international - Mobile
DATE: 2014-02-13
URL: http://www.courrierinternational.com/breve/2014/02/13/liberation-de-65-terroristes
PRINCIPAL: 170116
TEXT:
Se connecter
AFGHANISTAN � Lib�ration de 65�"terroristes"
Malgr� les protestations de l'arm�e am�ricaine, le gouvernement afghan a lib�r� 65 d�tenus, tous accus�s par Washington d'�tre des terroristes et d'avoir tu� des soldats am�ricains, rapporte le 13 f�vrier The New York Times. Selon le quotidien, les relations entre les deux pays sont au plus mal, alors que cette ann�e sera marqu�e par une �lection pr�sidentielle et le retrait des troupes am�ricaines. On peut voir dans ce geste d'apaisement un signe lanc� par Kaboul aux talibans, de plus en plus forts politiquement en Afghanistan comme au Pakistan voisin.
<<
