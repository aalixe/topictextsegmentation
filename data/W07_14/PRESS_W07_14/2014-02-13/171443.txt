TITRE: GOUVERNEMENT: L'Italie attend son nouveau premier ministre -  News Monde: Europe - tdg.ch
DATE: 2014-02-13
URL: http://www.tdg.ch/monde/premier-ministre-enrico-letta-annonce-demission/story/29090156
PRINCIPAL: 171437
TEXT:
L'Italie attend son nouveau premier ministre
Mis � jour le 14.02.2014 2 Commentaires
Le pr�sident du Conseil italien Enrico Letta a annonc� sa d�mission sous la pression de Matteo Renzi, qui devrait lui succ�der.
A gauche, Matteo Renzi, qui lui devrait lui succ�der �  Enrico Letta (droite) la t�te du gouvernement.
Image: GABRIEL BOUYS VINCENZO PINTO / AFP/AFP
Articles en relation
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Us� par dix mois � la t�te de l'Italie, Enrico Letta a d�missionn� vendredi, d�savou� par ses propres troupes. Son rival Matteo Renzi devrait lui succ�der. A 39 ans, le maire de Florence deviendrait le plus jeune chef de gouvernement en Europe.
Enrico Letta n'aura pass� que 293 jours � la t�te d'une coalition in�dite gauche-droite mise en place apr�s les l�gislatives de f�vrier 2013. Il a officiellement quitt� ses fonctions de pr�sident du Conseil vendredi apr�s-midi.
Paradoxalement, Enrico Letta s'en va sur une bonne nouvelle pour l'�conomie transalpine. Apr�s deux ans de chute ininterrompue du PIB, la troisi�me �conomie de la zone euro semble sortir de la r�cession, avec une petite croissance de 0,1% au dernier trimestre.
Le pr�sident Giorgio Napolitano a entam� ses consultations des divers partis qu'il devrait conclure samedi. Il choisira vraisemblablement Matteo Renzi pour former un gouvernement. Une fois l'ex�cutif constitu�, le nouveau Pr�sident du Conseil devra se pr�senter devant le Parlement, sans doute mardi, pour un vote de confiance.
M�me s'il n'a pas fait acte de candidature, le fringant maire de Florence Matteo Renzi pr�sidait vendredi - pour la Saint-Valentin - une c�r�monie � Florence pour f�ter des couples ayant atteint les 50 ans de mariage. Indice sans doute de son ambition, il leur a demand� de lui souhaiter bonne chance. �On en a toujours besoin surtout en ce moment�, a-t-il dit.
Pression des milieux d'affaires
Matteo Renzi dirige le Parti d�mocrate (PD), la formation de centre gauche dont M. Letta fut le vice-secr�taire. Il a obtenu un vote �crasant jeudi soir en faveur d'une motion demandant d'�ouvrir une phase nouvelle avec un ex�cutif nouveau�. Provoquant de ce fait la d�mission de Enrico Letta.
Personne n'a expliqu� jusqu'ici pourquoi le PD a fait tomber son ex-num�ro deux (Enrico Letta) pour le remplacer par son num�ro un (Matteo Renzi), avec l'intention de conserver plus ou moins la m�me majorit� parlementaire.
La d�cision de provoquer la chute d'Enrico Letta a �t� m�rie au cours des deux semaines �coul�es, expliquent des proches. Ceux-ci ajoutent les milieux d'affaires ont fait pression, m�contents du manque de soutien affich� par le gouvernement en faveur des entreprises.
�Sortir des mar�cages�
En guise de programme, Matteo Renzi a martel� devant les dirigeants de son parti que l'Italie doit �sortir des mar�cages�. Il a annonc� des r�formes �ambitieuses�, en se fixant comme horizon la fin de la l�gislature en 2018.
Il veut en particulier r�former le code �lectoral pour en finir avec le syst�me bicam�ral o� la Chambre des d�put�s et le S�nat ont le m�me poids institutionnel. Ce qui avait notamment plong� l'Italie dans l'impasse apr�s les derni�res l�gislatives il y a un an.
Ch�mage record
Il n'a en revanche livr� aucun d�tail sur la mani�re dont il compte trouver l'argent pour relancer durablement l'�conomie italienne, abaisser la lourde fiscalit� sur les entreprises, lutter contre un ch�mage record proche des 13%, et � plus de 40% chez les jeunes.
Les Italiens, dont une partie est lasse des manoeuvres de Silvio Berlusconi pouvaient s'attendre que Matteo Renzi s'empare du pouvoir � la faveur d'une �lection. Il n'en sera semble-t-il rien. Il devrait prendre ses quartiers au palais Chigi sans passer par la case �lections. (afp/Newsnet)
Cr��: 13.02.2014, 18h21
