TITRE: Twitter se rapproche de Facebook avec une nouvelle interface
DATE: 2014-02-13
URL: http://hitek.fr/actualite/interface-twitter-ressemble-facebook_1691
PRINCIPAL: 170015
TEXT:
Twitter se rapproche de Facebook avec une nouvelle interface
De Lucile - 13 f�vrier 2014 �  9:17 dans R�seaux Sociaux
Vous aimez nos articles ? Suivez nous sur facebook
Vous aimez nos articles ? Suivez nous sur twitter
D�j� 19 r�action(s),
partagez cet article avec vos amis !
Apr�s avoir constat� un ralentissement du nombre d'inscriptions de ses utilisateurs, Twitter avait promis de nouvelles fonctionnalit�s. Selon un journaliste de Mashable qui a eu acc�s � la b�ta interface sur son profil Twitter, le r�seau social serait en train de travailler sur une refonte presque totale de son interface.
Cette nouvelle interface viserait � simplifier l'arriv�e aux nouveaux utilisateurs sur le r�seau social Twitter en rendant l'interface plus lisible, compr�hensible et visuellement plus accueillante. Une refonte qui se rapproche tr�s �trangement du design des profils Facebook, avec par exemple une grande image s'�tirant sur toute la longueur de la page, similaire � l'image de couverture de Facebook, ainsi qu'avec une biographie plac�e en haut � gauche. On s'�loignerait donc assez visiblement de l'organisation verticale pour tendre vers une appropriation totale de la page. Cette interface s'accompagne �galement de messages et de tweets plus longs ! Ce qui change tout le principe m�me de Twitter. Rien n'est encore officiel et pourtant les tweetos r�agissent d�j� en masse et le moins que l'on puisse dire, c'est qu'ils ne sont pas contents du tout.
Cette interface n'a �t� d�ploy�e que pour une toute petite partie des utilisateurs et seulement pour la version navigateur. Rien n'a encore fuit� concernant une �ventuelle refonte pour smartphones et tablettes. Pour le moment, Twitter Inc. n'a toujours fait aucun commentaire mais une chose est quasiment certaine; si une telle refonte de la plateforme de microblogging voit le jour, une grande majorit� des habitu�s du r�seau social risquent de ne pas appr�cier. Un pari dangereux pour Twitter, qui prend le risque de, peut-�tre, perdre certains de ses utilisateurs actuels. Twitter se ravisera peut-�tre, cherchant un autre moyen d'attirer de nouveaux utilisateurs tout en contentant les anciens.
