TITRE: JO/Artistique: triste fin pour Plushenko, trahi par son dos | La-Croix.com
DATE: 2014-02-13
URL: http://www.la-croix.com/Actualite/Sport/JO-Artistique-triste-fin-pour-Plushenko-trahi-par-son-dos-2014-02-13-1106140
PRINCIPAL: 171763
TEXT:
�Le sport se met � la hauteur des tout-petits
Le Russe Evgueni Plushenko, contraint de renoncer � la derni�re minute � l'�preuve individuelle de patinage artistique jeudi � Sotchi pour un probl�me de dos, a tristement conclu sa carri�re.
"Je suis sinc�rement d�sol� pour mes fans et pour tout le monde mais j'ai voulu y croire jusqu'� la fin. J'en ai presque pleur�. C'est vraiment dur, croyez-moi. Ce n'est pas de cette fa�on que je voulais terminer ma carri�re. Je suis tr�s d��u. Mais j'ai fait de mon mieux", a d�clar� Plushenko.
Pendant les six minutes d'�chauffement qui ont pr�c�d� son programme court, le patineur de 31 ans, sacr� dimanche avec la nouvelle �preuve par �quipes, s'est tenu le dos, op�r� il y a un an.
Le triple champion du monde s'est �chauff� avec difficult�s et apr�s s��tre mal r�ceptionn� sur un triple axel qu'il a tent�, il est all� voir son entra�neur de toujours en bord de piste, Alexei Mishin.
Les deux hommes ont parl� et Plushenko est revenu sur le centre de la piste � l'appel de son nom, annon�ant son entr�e en lice, sous les cris du public.
Plushenko est all� voir le juge su�dois Mona Jonsson pour lui signifier qu'il n'�tait pas en mesure de patiner.
Le Russe a ensuite salu� les spectateurs, partag�s entre surprise, incr�dulit� et d�ception, et a quitt� la glace.
'Rien fait d'antisportif'
Le champion des Jeux de 2006 avait laiss� entrevoir un possible forfait apr�s l'�preuve par �quipes qui lui a apport� sa deuxi�me m�daille d'or jeudi.
"D�s le lendemain de son programme libre par �quipes (lundi), je savais que la F�d�ration russe aurait d� proc�der � un changement m�me s'il �tait plut�t bien. Mais on n'a rien fait d'antisportif", a justifi� Mishin.
Avant l'�preuve individuelle, Plushenko avait d�j� gagn� son pari: d�crocher � nouveau l'or olympique, ce qu'il a fait dimanche avec le titre de la nouvelle �preuve par �quipes. Il s'est offert une quatri�me m�daille aux Jeux pour �galer le seul patineur � ce niveau dans l'histoire du patinage, le Su�dois Gillis Grafstr�m (de 1920 � 1932).
Le trentenaire, qui faisait � Sotchi son deuxi�me retour sur le devant de la grande sc�ne, aurait pu viser plus loin en devenant le premier � d�tenir cinq m�dailles olympiques.
Mais le mal au dos dont il dit encore souffrir apr�s une op�ration visant � lui remplacer un disque, a calm� ses ardeurs.
Le titre individuel �tait quoi qu'il en soit inaccessible pour Plushenko tant il est surclass� par une nouvelle g�n�ration de brillants patineurs, dont les deux favoris, le triple champion du monde en titre, le Canadien Patrick Chan, et le jeune prodige japonais Yuzuru Hanyu.
AFP
