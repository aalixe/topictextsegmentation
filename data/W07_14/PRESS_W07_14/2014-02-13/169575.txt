TITRE: Jérôme Kerviel sera fixé sur son sort le 19 mars
DATE: 2014-02-13
URL: http://www.lefigaro.fr/actualite-france/2014/02/13/01016-20140213ARTFIG00056-jerome-kerviel-en-cassation-veut-echapper-a-la-prison.php
PRINCIPAL: 169570
TEXT:
le 13/02/2014 à 08:24
Crédits photo : BERTRAND GUAY/AFP
VIDÉO - La Cour de cassation doit se prononcer sur le pourvoi de l'ancien trader de la Société Générale, qui continue de dénoncer le rôle joué par la banque. Le jeune homme risque la prison.
Publicité
Cela fait six ans qu'il se bat. L'ancien trader Jérôme Kerviel , dont l'histoire est emblématique des dérives de la finance, continue de clamer son innocence dans l'affaire qui l'oppose à la Société Générale . La Cour de cassation a annoncé jeudi qu'elle se prononcerait le 19 mars sur son pourvoi. Le jeune homme se trouve sous la menace d'une incarcération en cas de rejet.
Le rôle de la Société Générale
Condamné en première instance, puis en appel, à la même peine, soit cinq ans d'emprisonnement dont trois ferme et 4,91 milliards d'euros de dommages et intérêts, il reconnaît une part de responsabilité mais réfute avoir agi en secret. «J'ai fait ce que la banque m'a appris à faire et je n'ai volé personne», martèle-t-il. La banque savait ou aurait dû savoir ce qui se tramait dans cette salle de marchés d'une tour de La Défense et ne peut donc se prétendre victime, plaide Jérôme Kerviel. Des défauts de contrôle importants ont notamment été pointés par le régulateur des banques, qui a condamné la Société Générale à 4 millions d'euros d'amende pour ces manquements.
Mais dans son avis écrit, l'avocat général près la Cour de cassation, Yves Le Baut, rétorque qu'une «victime négligente n'est pas pour autant une victime consentante». Selon le magistrat, «on ne peut tirer pour conséquence du défaut de vigilance» de la banque «son adhésion à la commission des agissements qui lui ont porté préjudice».
Le montant faramineux des dommages
La défense de Jérôme Kerviel conteste également les dommages et intérêts attribués à la banque, ces 4,91 milliards d'euros dont il ne pourrait sans doute payer qu'une infime partie si la condamnation devenait définitive. Ce montant correspond à la perte constatée par l'établissement bancaire après s'être défait de toutes les positions accumulées par le jeune homme, qui avaient atteint 49 milliards d'euros environ début janvier 2008. L'ancien trader rappelle notamment qu'il n'a tiré aucun profit personnel des opérations incriminées et accuse la banque d'avoir matérialisé cette perte en soldant son exposition sans discernement.
Là encore, l'avocat général s'inscrit dans la lignée des deux premières décisions de justice, qui ont suivi la jurisprudence imposant une réparation intégrale du préjudice. Une fois le préjudice évalué, il ne peut être indemnisé qu'en totalité et non partiellement, selon lui, même si le montant est colossal.
Soutiens politiques
En cas de rejet du pourvoi par la plus haute instance judiciaire française, Jérôme Kerviel, qui a effectué 37 jours de détention provisoire en 2008, sera incarcéré, sans doute dans un délai rapproché. Il ne pourra pas bénéficier d'un aménagement de peine, qui n'est ouvert que pour les condamnations inférieures à deux ans d'emprisonnement pour les primo-délinquants.
Dans sa bataille, qu'il a cherché à porter sur le terrain politique, l'ancien trader de 37 ans s'est trouvé cette semaine une nouvelle alliée. Après le co-président du Parti de Gauche, Jean-Luc Mélenchon, Jérôme Kerviel a reçu le soutien de la députée européenne EELV Eva Joly .
