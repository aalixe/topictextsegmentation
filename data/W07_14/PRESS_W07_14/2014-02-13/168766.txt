TITRE: Laporte prend cher ! - Top 14 - Rugby - Sport.fr
DATE: 2014-02-13
URL: http://www.sport.fr/rugby/top-14-laporte-prend-cher-339657.shtm
PRINCIPAL: 168764
TEXT:
Laporte prend cher !
Mercredi 12 f�vrier 2014 - 17:14
Bernard Laporte ne pourra pas retrouver sa place sur la banc de Toulon avant les demi-finales du Top 14 ! Le manager du RCT est suspendu pour une dur�e de 13 semaines, soit jusqu'au 13 mai, pour des insultes visant un arbitre.
Si Toulon participe aux demi-finales du Top 14 , pr�vues les 16 et 17 mai, les joueur toulonnais pourront compter sur Bernard Laporte . D'ici l�, le manager du RCT se retrouve priv� de banc, ne pourra acc�der "au terrain, aux vestiaires (des �quipes et des arbitres) ainsi qu'aux couloirs d'acc�s � ces zones lors des matches officiels (y compris avant et apr�s les matches)", selon la commission de discipline de la Ligue nationale de rugby (LNR).
Des mesures s�v�res (le club est, lui, �galement condamn� � 10.000 euros d'amende) qui s'expliquent par les propos tenus par l'ancien s�lectionneur du XV de France envers l'arbitre du match Toulon-Grenoble du 4 janvier direct. Apr�s la rencontre, perdue 22-21, l'ancien secr�taire d'Etat aux Sports avait qualifi� l'arbitre de "nul", d'"incomp�tent" et de "pipe" qui "vole � chaque fois" son �quipe.
Laporte paie �galement le fait d'�tre un r�cidiviste. C'est la deuxi�me fois qu'il est sanctionn� pour des propos contre un arbitre. Il avait d�j� �t� interdit de banc et de vestiaire 60 jours en 2012 pour des propos injurieux sur un arbitre.
