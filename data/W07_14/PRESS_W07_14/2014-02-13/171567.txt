TITRE: Plushenko : �D�sol� - Fil info - Sotchi 2014 - Jeux Olympiques -
DATE: 2014-02-13
URL: http://sport24.lefigaro.fr/jeux-olympiques/sotchi-2014/fil-info/plushenko-desole-679042
PRINCIPAL: 171563
TEXT:
Plushenko : �D�sol�
13/02 18h47 - JO 2014, Patinage
Le double champion olympique russe Evgueni Plushenko, qui a d�clar� forfait sur l'�preuve individuelle, a expliqu� que mercredi il �tait �tomb� sur le quadruple � l'entra�nement et j'ai ressenti un probl�me au dos. Durant l'�chauffement (jeudi), j'ai fait un triple boucle et un triple lutz mais apr�s le premier triple axel, j'ai ressenti une terrible douleur dans la jambe et je me suis tr�s mal r�ceptionn� pour le second. Je ne pouvais plus sentir mes jambes ensuite. �a me faisait si mal que j'ai d� renoncer.� Et de confier�:��Je suis sinc�rement d�sol� pour mes fans et pour tout le monde mais j'ai voulu y croire jusqu'� la fin. J'en ai presque pleur�. C'est vraiment dur, croyez-moi. Ce n'est pas de cette fa�on que je voulais terminer ma carri�re. Je suis tr�s d��u. Mais j'ai fait de mon mieux. Je dois maintenant me reposer et me faire soigner. Ensuite je devrais avoir de la r��ducation. Bien s�r que je vais continuer � patiner mais dans les galas.�
