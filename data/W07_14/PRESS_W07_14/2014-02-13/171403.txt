TITRE: Sotchi 2014 : les temps forts de jeudi avec Martin Fourcade qui s'offre le doubl� et Gus Kenworth qui sauve des chiots � metronews
DATE: 2014-02-13
URL: http://www.metronews.fr/sport/direct-sotchi-2014-les-temps-forts-de-jeudi-avec-le-slopestyle-gus-kenworth-qui-sauve-des-chiots-et-martin-fourcade-qui-s-offre-le-double/mnbm!WNOkRUnq8LMk/
PRINCIPAL: 171402
TEXT:
Joss Christensen s'est envol� dans le ciel de Sotchi. Photo :�FRANCK FIFE / AFP
Patinage artistique : Brian Joubert 7e, Florent Amodio 14e apr�s le programme court
Apr�s le sacre de Martin Fourcade, l'autre satisfaction de la journ�e est venue du patinage artistique. Apr�s le forfait surprise du Russe Plushenko, Brian Joubert a r�alis� un programme court de toute beaut�. Le Poitevin de 29 ans, qui participe � Sotchi � ses derniers JO, a d�croch� une 7e place qui le situe � 1.14 point de la troisi�me place. Ce qui lui permet de r�ver du podium, � la condition de r�ussir un programme libre de haut vol vendredi.
Le doubl� de Martin Fourcade
Martin Fourcade est donc bien le meilleur chercheur d'or Fran�ais . Apr�s l'avoir emport� sur le 10 km mardi, le Catalans s'offre une nouvelle m�daille d'or en gagnant l'individuel sur 20 km devant l'Allemand Erik Lesser. Pas mal parti, Jean-Guillaume B�atric �choue � la sixi�me avec une seule petite faute au tir.�
S�bastien Lepape seul Fran�ais encore debout au short-Track
C'est l'h�catombe sur la piste du short-track. Les Fran�ais sont tomb�s les uns apr�s les autres, alors qu'il disputait le tour qualificatif du 1 000 m�tres. Maxime Chataignier et Thibaut Fauconnet, pourtant favori de l'�preuve, ont termin� dans les balustrades. Seul S�bastien Lepape passe l'obstacle, m�me s'il a chut� lui aussi, il a �t� rep�ch� pour avoir �t� g�n�.�
Les Bleues largu�es sur le 10 km classique
Toujours pas de m�dailles du c�t� du ski de fond puisque Celia Aymonier (26e) et Aurore Jean (28e) n'ont rien pu faire, terminant � plus de deux minutes du podium. C'est la polonaise Justin Kowalczyck qui s'est impos�e, devant la su�doise Charlotte Kalla et la sublime norv�gienne Th�r�se Johaug.�
L'ami des b�tes est un skieur
Il est am�ricain, sp�cialiste du ski freestyle et s'est pris de passion pour les chiots errants qui pullulent autour des sites olympiques. Promis � l'euthanasie, Gus Kenworthy a d�cid� de tout faire pour en sauver quatre . Il les a adopt� et fait vacciner, il va maintenant tenter de les amener dans ses bagages, direction Denver.
Le slopestyle c'est show�
Nouvelle �preuve aux JO , le slopestyle, comp�tition de ski freestyle o� les concurrents enchainent des sauts et des figures , est extr�mement spectaculaire. A ce petit jeux l�, l'Am�ricain Joss Christensen qui a fortement impressionn� et qui a remport� la m�daille d'or. Les Fran�ais, moins puisque Jeremy Pancras (19e), Antoine Adelisse (27e) et Jules Bonnaire (30e) n'ont pas pass� les qualifications. ��
�
Kowalczyk, l'or en boitant
La fin de la mal�diction. Jamais championne du monde ou olympique malgr� une domination du circuit depuis plusieurs saisons, la Polonaise Justyna Kowalczyk s'est enfin par�e d'or, jeudi en s'adjugeant le 10 km classique en ski de fond. Un sacre olympique � la saveur particuli�re quand on sait que l'athl�te de 31 ans l'a conquis alors qu'elle souffre d'une micro-fracture � un pied. "J'ai pris beaucoup de m�dicaments mais surtout, quand je cours je ne ressens pas de douleur", a indiqu� celle qui avait partag� dimanche une photo de sa radio du pied sur sa page Facebook .
�
