TITRE: Mort d'un enfant apr�s une explosion : la m�re soup�onn�e d'infanticide � metronews
DATE: 2014-02-13
URL: http://www.metronews.fr/nice-cannes/mort-d-un-enfant-apres-une-explosion-la-mere-soupconnee-d-infanticide/mnbm!ao1q6OfhQfvWQ/
PRINCIPAL: 170864
TEXT:
Cr�� : 13-02-2014 15:17
Mort d'un enfant apr�s une explosion : la m�re soup�onn�e d'infanticide
ENQU�TE - La m�re d'un enfant de quatre ans, retrouv� mort le 24 janvier apr�s l'explosion de son immeuble � Nice, a �t� mise en examen pour meurtre et �crou�e.
Tweet
�
Le souffle de l'explosion a arrach� une partie de la fa�ade de l'immeuble ainsi que sa toiture. Photo :�M.B./metronews
Le petit immeuble avait �t� litt�ralement souffl� par l'explosion, le 24 janvier, � Nice . Une d�flagration li�e au gaz dans l'appartement situ� au deuxi�me �tage. Le bilan est lourd. Un enfant �g� de 4 ans est retrouv� mort, sa m�re gravement br�l�e est h�liport�e vers l'h�pital de Toulon. Enfin deux bless�s voisins ont �t� l�g�rement bless�s.
Plac�e sous mandat de d�p�t
Ce drame ne serait pas li� aux hasards.�L'enqu�te�s'est finalement orient�e vers une piste criminelle, celle d'un infanticide.
Selon Nice-Matin qui r�v�le cette information ,�la m�re de famille a �t� mise en examen pour "meurtre sur mineur". Elle a �t� plac�e sous mandat de d�p�t provisoire.�Elle a demand� un d�lai pour pr�parer sa d�fense et sera pr�sent�e au juge de la libert� et de la d�tention lundi prochain.
Alexis Picard avec AFP
