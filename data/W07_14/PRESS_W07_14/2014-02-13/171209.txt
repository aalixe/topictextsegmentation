TITRE: La Belgique autorise l'euthanasie pour les mineurs | Site mobile Le Point
DATE: 2014-02-13
URL: http://www.lepoint.fr/monde/belgique-legalisation-de-l-euthanasie-pour-les-mineurs-13-02-2014-1791490_24.php
PRINCIPAL: 0
TEXT:
13/02/14 à 19h06
La Belgique autorise l'euthanasie pour les mineurs
À la suite du vote des députés belges, la loi rend désormais possible l'euthanasie de mineurs atteints de maladies incurables.
En Belgique, la loi adoptée par les députés ne concernerait que quelques cas par an. Martin Bernetti / AFP
Source AFP
Les députés belges ont définitivement adopté jeudi une loi étendant le champ légal de l'euthanasie aux mineurs atteints d'une maladie incurable, sans fixer d'âge minimum, 12 ans après l'avoir autorisée pour les adultes. La loi, déjà votée en décembre par le Sénat , a été approuvée par les députés à une majorité de 86 "pour", 44 "contre" et 12 abstentions. Elle entrera en vigueur dans les prochaines semaines. Alors que la population belge est aux trois quarts favorable au texte, selon un sondage, l'Église catholique belge a réitéré ces dernières semaines son opposition, disant craindre une "banalisation" de l'euthanasie et la pression qui pourrait peser sur les enfants malades.
Les partisans de la loi ont à l'inverse insisté sur les "conditions strictes" prévues par la nouvelle législation : le mineur devra "se trouver dans une situation médicale sans issue entraînant le décès à brève échéance", être confronté à une "souffrance physique constante et insupportable qui ne peut être apaisée et qui résulte d'une affection accidentelle ou pathologique grave et incurable". Contrairement aux Pays-Bas , où l'euthanasie est autorisée à partir de 12 ans, les parlementaires belges ont opté pour la notion, plus flexible, de "capacité de discernement".
Les parents devront donner leur consentement
Cette capacité de l'enfant à comprendre le "côté irréversible de la mort", selon les mots d'une députée, sera estimée au cas par cas par l'équipe médicale et par un psychiatre ou un psychologue indépendant. Et si l'initiative de demander l'euthanasie devra venir de l'enfant, les parents devront donner leur consentement. Cette loi "renforce la liberté de choix de chacun. Il n'est pas question d'imposer l'euthanasie à qui que ce soit", a martelé une nouvelle fois la députée socialiste Karine Lalieux, juste avant le vote.
"Respecter le patient mineur, c'est être à son écoute", a abondé le libéral Daniel Bacquelaine. Les députés ont pu déterminer leur vote "en âme et conscience", sans devoir respecter une discipline de parti. Mais, globalement, les socialistes, libéraux, écologistes et nationalistes flamands de la N-VA ont voté en faveur du texte, auquel se sont opposés les élus chrétiens-démocrates et les membres du parti d'extrême droite flamand Vlaams Belang. Le débat belge sur l'euthanasie des mineurs, qui a suscité l'intérêt et l'étonnement au-delà des frontières belges, est resté serein au sein du royaume.
