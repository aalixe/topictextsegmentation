TITRE: J�r�me Kerviel devant la Cour de cassation
DATE: 2014-02-13
URL: http://www.boursorama.com/actualites/jerome-kerviel-devant-la-cour-de-cassation-49fdaa2a2c3c80792bf2ecc7143669b4
PRINCIPAL: 169085
TEXT:
J�r�me Kerviel devant la Cour de cassation
Le Monde le 13/02/2014 � 06:02
Imprimer l'article
Un peu plus de six ans apr�s la r�v�lation des faits, l'ancien banquier embl�matique des d�rives de la finance continue de se battre et clame son innocence.
La Cour de cassation examine, jeudi 13 f�vrier, le pourvoi de J�r�me Kerviel, l'ancien trader de la Soci�t� g�n�rale qui se trouve d�sormais sous la menace d'une incarc�ration en cas de rejet mais continue de d�noncer le r�le jou� par la banque.
Un peu plus de six ans apr�s la r�v�lation des faits, l'ancien banquier embl�matique des d�rives de la finance continue de se battre et clame son innocence. Condamn� en premi�re instance puis en appel � la m�me peine, cinq ans d'emprisonnement dont trois ferme et 4,91 milliards d'euros de dommages et int�r�ts, il reconna�t une part de responsabilit� mais r�fute avoir agi en secret.
>> Lire aussi : Kerviel devra-t-il vraiment payer 4,9 milliards d'euros de dommages et int�r�ts ?
��J'ai fait ce que la banque m'a appris � faire et je n'ai vol� personne��, mart�le-t-il. La banque savait ou aurait d� savoir ce qui se tramait dans cette salle de march�s d'une tour de La D�fense et ne peut donc se pr�tendre victime, plaide J�r�me Kerviel.
SOUTIEN D'EVA JOLY
Des d�fauts de contr�le importants ont notamment �t� point�s par le r�gulateur des banques, la Commission bancaire devenue l'Autorit� de contr�le prudentiel (ACP), qui a condamn� la Soci�t� g�n�rale � 4 millions d'euros d'amende pour ces manquements.
Mais dans son avis �crit, l'avocat g�n�ral pr�s la Cour de cassation, Yves Le Baut, r�torque qu'une ��victime n�gligente n'est pas pour autant une victime consentante��. En cas de rejet de son pourvoi, J�r�me Kerviel, qui a effectu� 3...
