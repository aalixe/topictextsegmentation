TITRE: La mission de l'UA en Somalie condamne et qualifie de "l�che" l' attentat meurtrier � l'a�roport de Mogadiscio - china radio international
DATE: 2014-02-13
URL: http://french.cri.cn/621/2014/02/14/562s368542.htm
PRINCIPAL: 171466
TEXT:
La mission de l'UA en Somalie condamne et qualifie de "l�che" l' attentat meurtrier � l'a�roport de Mogadiscio
��2014-02-13 23:59:44��xinhua
Le chef de Mission de l'Union africaine en Somalie, Mahamat Saleh Annadif, a vivement d�plor� jeudi l'attentat meurtrier � la voiture pi�g�e perp�tr� pr�s de l'a�roport de Mogadiscio, qui a fait cinq morts.
Une voiture bourr�e d'explosifs actionn�s � distance a explos� au passage d'autres v�hicules devant la porte de l'a�roport international de Mogadiscio, tuant cinq personnes, dont un garde, et faisant sept bless�s.
"J'adresse mes sinc�res condol�ances aux amis et aux familles des victimes endeuill�s et me joins au peuple somalien pour condamner sans �quivoque cette attaque l�che qui t�moigne encore une fois des capacit�s chancelantes des Shebab", a d�clar� M. Annadif.
Le chef de l'AMISOM a appel� les Somaliens � s'unir aux forces de s�curit� pour lutter contre les activit�s des Shebab.
"Vers 12h40, un v�hicule charg� d'explosifs a explos� devant le restaurant situ� � c�t� de l'a�roport international Aden Abdulle, blessant et tuant des civils innocents", a d�clar� l'AMISOM dans un communiqu�.
Le groupe islamiste des Shebab a revendiqu� l'attentat et a d�clar� qu'ils visaient "des responsables occidentaux".
L'AMISOM a indiqu� que des secours ont �t� imm�diatement envoy�s sur place pour �vacuer les bless�s et s�curiser la zone.
La recrudescence r�cente des attentats en Somalie intervient alors que les forces gouvernementales somaliennes et les troupes de l'AMISOM se pr�parent � une offensive majeure pour chasser les rebelles des zones qu'ils contr�lent encore dans le sud et le centre de la Corne de l'Afrique.
Mot-cl� du jour
