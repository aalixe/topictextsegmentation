TITRE: Poutine appuie Sissi � la pr�sidentielle �gyptienne | L'�gypte sous tension
DATE: 2014-02-13
URL: http://www.lapresse.ca/international/dossiers/crise-dans-le-monde-arabe/legypte-sous-tension/201402/13/01-4738378-poutine-appuie-sissi-a-la-presidentielle-egyptienne.php
PRINCIPAL: 170495
TEXT:
>�Poutine appuie Sissi � la pr�sidentielle �gyptienne�
Poutine appuie Sissi � la pr�sidentielle �gyptienne
Le mar�chal Sissi (au centre), 59 ans, se pr�sentera � la pr�sidentielle.
PHOTO HASSAN AMMAR, ARCHIVERS AP
Notre dossier sur le soul�vement populaire qui secoue l'�gypte. �
� lire aussi
Agence France-Presse
Moscou, Russie
Le pr�sident russe a d�clar� soutenir la d�cision du mar�chal �gyptien Abdel Fattah al-Sissi de se pr�senter � la pr�sidentielle, lors d'une rencontre � Moscou avec le chef de l'arm�e et nouvel homme fort de l'�gypte.
�Je sais que vous avez pris la d�cision de pr�senter votre candidature � la pr�sidentielle en �gypte�, a d�clar� M. Poutine au mar�chal Sissi, selon les images de la t�l�vision russe.
�C'est une d�cision tr�s responsable, de s'investir d'une mission pour le peuple �gyptien. Je vous souhaite, en mon nom et au nom du peuple russe, de r�ussir�, a d�clar� M. Poutine au mar�chal Sissi, selon les images de la t�l�vision russe.
�La stabilit� de la situation dans tout le Moyen-Orient d�pend largement de la stabilit� en �gypte. Je suis persuad� qu'avec votre exp�rience vous allez r�ussir � mobiliser vos partisans et �tablir des relations avec toutes les parties de la soci�t� �gyptienne�, a ajout� le pr�sident russe.
Tr�s populaire en �gypte, le mar�chal Sissi, ministre de la D�fense, n'a pas encore annonc� officiellement dans son pays sa candidature mais il ne fait pas myst�re de ses intentions depuis qu'il a destitu� le 3 juillet le pr�sident Mohamed Morsi, dont le gouvernement mis en place r�prime les partisans.
Le nouvel homme fort d'�gypte est arriv� mercredi en Russie, accompagn� du ministre �gyptien des Affaires �trang�res Nabil Fahmi.
Les deux hommes ont rencontr� plus t�t dans la journ�e jeudi leurs homologues russe Sergue� Cho�gou et Sergue� Lavrov pour renforcer la coop�ration entre les deux pays, notamment militaire.
Ce voyage est la r�ponse � la visite des ministres de la D�fense et des Affaires �trang�res russes au Caire le 14 novembre, qui avaient eu lieu en plein refroidissement --caus� par la destitution de M. Morsi et la r�pression sanglante des manifestations de ses partisans-- des relations entre l'�gypte et les �tats-Unis, son principal alli� et bailleur de fonds.
�Nous avons d�cid� d'acc�l�rer la pr�paration des documents qui donneront un coup d'envoi suppl�mentaire � la coop�ration militaire et militaro-technique�, a d�clar� M. Lavrov � l'issue de la rencontre.
�Nous avons discut� de toute une liste d'actions communes qui repr�sentent un int�r�t r�ciproque, (...) dont des manoeuvres conjointes et la formation de militaires �gyptiens� en Russie, a rench�ri M. Cho�gou.
En novembre, la Russie avait annonc� qu'elle allait livrer au Caire des syst�mes de d�fense antia�rienne et discutaient de la livraison d'avions et d'h�licopt�res � l'arm�e.
Le montant des contrats atteindrait 2 milliards de dollars, selon la presse russe.
Partager
