TITRE: 20 Minutes Online - NSA: plainte collective contre Barack Obama - News
DATE: 2014-02-13
URL: http://www.20min.ch/ro/news/dossier/snowden/story/24362748
PRINCIPAL: 169365
TEXT:
Voir le diaporama en grand �
26.03 �95% des informations qu'il a prises sont de nature militaire, des informations � la fois tactiques et strat�giques et qui, selon nous, sont aux mains des Russes�, a affirm� Mike Rogers, pr�sident r�publicain de la commission du renseignement de la Chambre des repr�sentants. Image: Keystone/AP/J. Scott Applewhite
25.03 Le pr�sident am�ricain s'appr�te � proposer au Congr�s des Etats-Unis de mettre un terme � la collecte massive des �coutes t�l�phoniques par la NSA. Image: Keystone/AP/Pablo Martinez Monsivais
13 f�vrier 2014 06:17
Etats-Unis
NSA: plainte collective contre Barack Obama
Un �lu r�publicain a lanc� mercredi une action en justice contre le pr�sident am�ricain en raison du programme de surveillance t�l�phonique de la NSA.
�Il y a une vague �norme et grandissante de protestations dans le pays de la part de personnes scandalis�es que leurs relev�s soient saisis sans soup�on, sans mandat d'un juge et sans individualisation�, a d�clar� le s�nateur am�ricain Rand Paul devant un tribunal f�d�ral de Washington.
Une telle action en justice, de la part d'une personnalit� avec la notori�t� de Rand Paul,  est rare. Il n'est pas certain qu'elle soit consid�r�e valable par la justice, et encore moins qu'elle aille jusqu'� la Cour supr�me, comme l'esp�re l'�lu.
D'ob�dience libertaire, Rand Paul est l'un des parlementaires les plus critiques des divers programmes de surveillance �lectronique de l'Agence nationale de s�curit� (NSA), depuis les r�v�lations d'Edward Snowden. Le r�publicain est �galement un pr�tendant officieux � la Maison Blanche en 2016.
Violation du 4e amendement
Son action en justice, cosign�e par le militant du Tea Party Matt Kibbe de Freedomworks, se fait au nom de tous ceux qui �sont ou ont �t� clients, utilisateurs ou abonn�s d'un service t�l�phonique aux Etats-Unis depuis 2006�, selon la plainte - en th�orie, des centaines de millions de personnes.
Le programme vis� est celui de collecte des m�tadonn�es de tous les appels t�l�phoniques pass�s aux Etats-Unis: num�ro appel�, dur�e et horaire des appels, mais pas l'enregistrement des conversations. Le �Wall Street Journal� et le �Washington Post� ont d�couvert que la NSA �tait actuellement capable de stocker les donn�es de seulement 30% des appels.
Les plaignants estiment qu'il s'agit d'une violation du quatri�me amendement de la constitution, qui prot�ge les Am�ricains contre les fouilles excessives sans mandat de justice. Le programme est autoris� par une cour secr�te, qui donne un feu vert g�n�ral tous les 90 jours � la saisie des m�tadonn�es.
Outre Barack Obama, l'action vise les directeurs de la NSA, du FBI et du renseignement am�ricain. Plusieurs autres actions en justice sont en cours dans le pays, notamment � l'initiative d'associations de d�fense des libert�s.
(ats)
