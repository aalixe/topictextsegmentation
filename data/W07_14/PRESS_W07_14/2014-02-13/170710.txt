TITRE: Le gouvernement suspend les immatriculations de VTC | Site mobile Le Point
DATE: 2014-02-13
URL: http://www.lepoint.fr/societe/le-gouvernement-decide-de-reporter-les-immatriculations-de-vtc-13-02-2014-1791399_23.php
PRINCIPAL: 170706
TEXT:
13/02/14 à 15h28
Le gouvernement suspend les immatriculations de VTC
Les réunions de la commission d'immatriculation des VTC sont reportées durant toute la durée de la médiation avec les syndicats de taxis.
Photo d'illustration. Sebastien Muylaert / Maxppp
Source AFP
Le gouvernement a décidé de "reporter" les réunions de la commission d'immatriculation des véhicules de tourisme avec chauffeur (VTC) jusqu'à la fin de la médiation menée avec les syndicats de taxis, a annoncé jeudi Matignon dans un communiqué. Le médiateur, le député PS Thomas Thévenoud, a rencontré une nouvelle fois jeudi les représentants des taxis et ceux des VTC, accusés par les taxis de concurrence déloyale. "Face aux dysfonctionnements issus de la loi du 22 juillet 2009, ces consultations doivent permettre dans les deux mois de définir des règles du jeu nouvelles pour une concurrence équilibrée, au bénéfice des professionnels, des usagers et de l'emploi", souligne Matignon dans un communiqué.
"Pendant la durée de cette mission, sur la base des recommandations de Thomas Thévenoud, de Manuel Valls, ministre de l'Intérieur, et de Sylvia Pinel, ministre de l'Artisanat, un dispositif transitoire est mis en place", ajoutent les services du Premier ministre. "Les prochaines réunions de la commission d'immatriculation des exploitants de VTC sont en conséquence reportées", annoncent-ils. Selon Matignon, "les contrôles, en particulier ceux opérés par les services du ministère de l'Intérieur, sont immédiatement renforcés".
Ces contrôles "portent notamment sur l'utilisation des couloirs de bus à Paris, sur le non-stationnement des VTC aux abords des gares et des aéroports, et sur le respect du principe de la réservation préalable par les VTC", poursuit le communiqué. "Le gouvernement souhaite ainsi créer les conditions d'un dialogue réunissant tous les acteurs, afin de permettre à la mission conduite par Thomas Thévenoud de déboucher sur des propositions d'évolution durables et partagées par tous, pour un meilleur service des usagers et le développement de l'emploi", conclut Matignon. En parallèle, l'intersyndicale des taxis appelle à cesser la grève entamée depuis plusieurs jours (FO).
