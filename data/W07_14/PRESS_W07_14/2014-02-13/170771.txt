TITRE: Sotchi 2014: revivez le 2e titre olympique de Martin Fourcade - L'Express
DATE: 2014-02-13
URL: http://www.lexpress.fr/actualite/sport/en-direct-sotchi-2014-martin-fourcade-vise-le-double-en-individuel_1323647.html
PRINCIPAL: 170768
TEXT:
16h47
:
C'est termin�! Petit podium pour la photo avant la vraie remise des m�dailles demain, accompagn�e d'une nouvelle Marseillaise. Nouvelle m�daille d'or donc pour Martin Fourcade, qui est en train de marquer de son empreinte ces Jeux olympiques de Sotchi. On le retrouve dimanche pour la mass start, avant le relais mixte mercredi prochain et le relais masculin samedi 22 f�vrier. Soit trois nouvelles chances de podium!�
Merci d'avoir suivi ce live. Et prenez soin de vous.�
16h38
:
Martin Fourcade au t�l�phone. On imagine qu'il parle � sa compagne, rest�e en France. Et tout le monde est arriv�.�
Le podium final:�
1 Martin Fourcade (19/20) 49''31'7�
2 Erik Lesser (20/20) 49''43'9�
3 Evgeniy Garanichev (19/20) 50''06'02�
...�
6 Jean-Guillaume B�atrix (19/20) 50''15'5�
...�
12 Simon Fourcade (18/20) 51''29'9�
...�
82 Alexis Boeuf (16/20) 58''39'0�
16h35
:
Les derniers concurrents sont en train d'en terminer sur cet individuel. Il n'en reste plus que six, tous trop loin en temps pour inqui�ter le titre olympique de Martin Fourcade.�
16h27
:
Alexis Boeuf a fait une faute lors de chaque tir. Il va terminer en fin de classement.�
16h25
:
Je suis content. Je sais que sur l'individuel, j'ai un niveau � ski qui me permet de gagner malgr� une faute au tir. Cela m'a permis de garder confiance et de rester serein.
Martin Fourcade, au micro de France 2
16h23
:
Simon Fourcade tr�s d��u par sa prestation. S'il n'avait pas fait ses deux fautes au tir, le podium �tait possible, comme B�atrix. Sans son unique faute, il aurait termin� champion olympique, sachant qu'il est seulement 43' derri�re Martin Fourcade. Leur bonne prestation est de bon augure pour le relais par �quipe.�
16h20
:
C'est la 3e m�daille olympique de Martin Fourcade apr�s l'argent � Vancouver (mass start) et l'or sur la poursuite lundi. Jamais depuis Jean-Claude Killy en 1968 � Grenoble un athl�te fran�ais n'avait r�ussi � remporter plus d'une m�daille d'or dans les m�mes JO (3 pour Killy).�
16h13
:
Levant les bras au ciel et connaissant ses concurrents, Martin Fourcade sait qu'il est m�daill� d'or olympique.�
16h10
:
Martin Fourcade est presque champion olympique! Il ne peut plus �tre rattrap� par ses poursuivants. Lesser vient de terminer � 12 secondes. Il faudrait un miracle pour qu'un poursuivant face un temps canon et d�tr�ne le Fran�ais.�
16h07
Lesser est 6 secondes derri�re Fourcade, qui est quasiment champion olympique.�
16h07
Simon Fourcade (18/20) termine dans les 10.�
16h05
Lesser perd petit � petit son avance sur Fourcade >>> 3 secondes pour l'instant.�
16h04
:
Martin Fourcade fait une arriv�e canon pour terminer en 49''31'7. Le temps � battre probablement pour �tre champion olympique.�
16h01
:
20/20 pour l'Allemand Lesser, qui sort du 4e tir avec 9'6 d'avance sur Fourcade. Le titre olympique devrait se jouer entre les deux hommes.�
16h00
:
Jean-Guillaume B�atrix termine � la 4e place au g�n�ral et ne fera donc pas de nouveau podium. Tout s'est jou� pour lui sur un dernier tir rat�.�
15h56
:
Martin Fourcade 4/5 au tir avant le dernier tour (19/20). Il peut �tre champion olympique sauf si quelqu'un de fort sur les skis fait un sans-faute, car il vient de prendre la t�te du concours � l'issue du dernier tir!�
Sinon Svendsen en termine loin de toute possibilit� de podium.�
15h51
:
Jean-Guillaume B�atrix rate un tir! 19/20 finalement avant ce dernier tour de ski. Il va falloir cravacher au ski pour assurer. Il laisse pour l'instant la t�te du concours � l'Autrichien Simon Eder � la sortie du 4e tir.�
15h50
:
La bonne nouvelle pour Martin Fourcade, pour l'instant, c'est qu'aucun de ses pr�d�cesseurs, sauf Jean-Guillaume B�atrix, n'a r�ussi un sans-faute au tir. Et aussi l'Autrichien Landertiner, trop loin au temps cependant.�
15h45
:
Fourcade � 4/5 au 3e tir (14/15). Il peut encore r�ver d'un podium s'il cravache aux skis.�
15h42
:
Alexis Boeuf � 4/5 au tir. Il n'a d�j� plus le droit � l'erreur. Svendsen a fait finalement 19/20 et semble trop loin sur les skis pour un podium ce jeudi.�
15h41
:
5/5 (15/15) pour MAGIC B�atrix, qui prend la t�te du concours devant le Litunanien Kaukenas! Son temps apr�s le 3e tir: 29''54'7. S'il fait un sans-faute sur le dernier tir, il peux obtenir une nouvelle m�daille!�
15h40
B�atrix arrive au pas de tir pour la 3e session.�
15h39
:
Pour l'instant, les espoirs de m�daille reposent plus sur les �paules de Jean-Guillaume B�atrix (10/10) que sur celle de Martin Fourcade (9/10).�
15h37
Simon Fourcade � 3/5 au 2e tir. La podium s'�loigne pour le fr�re.�
15h35
:
Svendsen vient de rater un tir (14/15)! Fourcade vient de rater une cible lors du 2e tir (9/10).�
15h33
Dernier Fran�ais en lice, Alexis Boeuf s'�lance!�
15h31
B�atrix prend la t�te provisoire du concours (19'52''1) apr�s le 2e tir avec son 10/10!�
15h27
:
5/5 pour Simon Fourcade, qui fait un excellent d�but de course et se retrouve 5e provisoire au 1er tir.�
15h25
:
5/5 pour Martin Fourcade qui a pris son temps pour tirer. 3e provisoire en sortie de 1er tir.�
15h24
:
Svendsen � 10/10. Mais Kaukenas, plus rapide au ski, avec le m�me score, est devant apr�s le 2e tir pour l'instant.�
15h20
:
5/5 au tir pour B�atrix qui sort 3e provisoire du premier pas de tir en temps. Fak vient de faire 9/10 et prend la t�te du concours.�
15h18
:
Bjoerndalen 4/5 au tir. Vu qu'il est moins bon au ski qu'� la grande �poque, ce petit rat� hypoth�que d�j� ses chances. Simon Fourcade vient de s'�lancer sinon.�
15h15
