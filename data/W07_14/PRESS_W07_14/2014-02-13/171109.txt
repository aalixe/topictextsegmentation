TITRE: Cyber-espionnage  : Le Maroc, principale victime de �The Mask�
DATE: 2014-02-13
URL: http://www.aufaitmaroc.com/actualites/maroc/2014/2/13/le-maroc-principale-victime-de-the-mask_218104.html
PRINCIPAL: 171108
TEXT:
Cyber-espionnage :Le Maroc, principale victime de �The Mask�
Derni�re mise � jour :
13/02/2014 � 16:46
Le Maroc a subi, depuis 2007, 384 attaques informatiques provenant d'un dangereux virus, appel� �The Mask�. Kaspersky Lab a lev� le li�vre et n'exclut pas que cet espionnage de plus de 31 pays, gouvernements, grandes entreprises et activistes, soit l'�uvre d'un Etat.
Avec 384 cyber-attaques recens�es, depuis au moins 2007, par Kaspersky Lab, le Maroc reste vuln�rable en mati�re de s�curit� informatique. /DR
Une carte des principales victimes de "The Mask" ou "Careto", fournie dans le rapport de Kaspersky Lab. /DR
Les experts en s�curit� informatique de Kaspersky Lab ont fait la d�couverte d'un virus, en langue espagnole, qui servirait � des op�rations de cyber-espionnage actives depuis au moins 2007. �The Mask� ou �Careto� s'attaquerait principalement aux gouvernements, ambassades, compagnies p�troli�res, gazi�res et �nerg�tiques, mais aussi aux laboratoires de recherche, indique ces m�mes experts dans un rapport d�voil� mercredi.
Selon Kapersky Lab, 31�pays � travers le monde, allant du Moyen-Orient en passant par l�Europe, l�Afrique et le continent am�ricain ont �t� touch�s par ce virus. Selon les donn�es communiqu�es par l'entreprise am�ricaine, le Maroc arrive en t�te des victimes avec 384 attaques, loin devant le Br�sil (137 attaques), le Royaume-Uni (109 attaques) ou encore la France (33 attaques).
Les auteurs de ces attaques souhaitent, en priorit�, �collecter des donn�es sensibles aupr�s des syst�mes infect�s�. Ils allaient chercher des informations dans des�documents de travail, des cl�s de cryptage, des configurations VPN, cl�s SSH (permettant d�identifier un utilisateur sur un serveur SSH) et fichiers RDP (utilis�s par le logiciel�Remote Desktop Client�pour ouvrir automatiquement une connexion avec un ordinateur r�serv�)�, pr�cise l'entreprise de s�curit� informatique.
Un Etat derri�re ces attaques ?
Dans le rapport sur �The Mask�, les experts de Kaspersky Lab �mettent la possibilit� que�cette campagne de cyber-espionnage soit �sponsoris�e et commandit�e par certains Etats�, pas forc�ment hispaniques.
�Le tr�s haut degr� de professionnalisme dans les proc�dures op�rationnelles du groupe exclut de simples cybercriminels et fait de cette campagne l�une des menaces les plus �volu�es du�moment.�
Costin Raiu, directeur de l��quipe internationale de chercheurs et d�analystes de�Kaspersky Lab.
En effet, selon ce rapport, une infection par �The Mask��peut se r�v�ler�d�sastreuse car le virus intercepte toutes les�communications et recueille les informations les plus vitales sur les machines cibl�es. Sa d�tection reste extr�mement difficile.
Le Maroc demeure donc en premi�re ligne, surtout lorsqu'on se souvient que Microsoft annon�ait, en septembre dernier, que notre pays est 3,5 fois plus vuln�rable aux cyber-attaques que le reste du monde.
aufait
�Email
L'�quipe de aufait accueille vos commentaires avec  enthousiasme, et s'engage � respecter votre libert� d'expression. Par  contre, afin d'�viter les abus et les contenus offensants, seuls les  commentaires valid�s par notre �quipe r�dactionnelle seront publi�s.  Vous �tes pri�s de respecter la netiquette .
Vous avez droit � votre opinion, respectez donc celle des autres ! Merci.
