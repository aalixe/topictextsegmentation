TITRE: JIM.fr - Pesticides : polémiques sur les résultats de Gilles-Eric Séralini
DATE: 2014-02-13
URL: http://www.jim.fr/medecin/e-docs/pesticides_polemiques_sur_les_resultats_de_gilles_eric_seralini__143644/document_actu_pro.phtml
PRINCIPAL: 0
TEXT:
Question de ton
Le 12 février 2014
Si il n'y a aucun danger selon ces braves critiques, alors donnons du Roundup à nos enfants au petit déjeuner à la place du pamplemousse ?
Le ton de la "journaliste" est vraiment étonnant ? Ce pauvre fou de Séralini qui voit le poison partout... puisque l'on vous dit que ces produits sont sans danger, c'est clair !
R Levasseur
Un peu d'impartialité...
Le 12 février 2014
Mme Haroche, vous affichez très clairement un parti pris sur ce sujet, ce que je trouve très dérangeant pour une journaliste. Vous insinuez des doutes quant au sérieux de cette étude, je me permets d'en faire tout autant quant  à votre impartialité et votre conscience professionnelle (par curiosité, combien vous paie les lobbies agro-industriel pour cette campagne de dénigration?)
En contraste, je soulignerais juste le courage exceptionnel de M. Séralini de proposer une étude qui dit tout haut ce que tout le monde sait déjà mais dont personne ne veut entendre parler.
Bravo!
La réponse de la rédaction
Le 12 février 2014
Ne nous lançons pas ici dans le débat aussi complexe qu’éternel sur le droit (ou le devoir) du  journaliste (avec ou sans guillemet) d’être ou non partial. Mais prenons cependant quelques minutes pour souligner que cet article n’est en rien une présentation sous leur meilleur jour des pesticides (leur toxicité est rappelée à plusieurs reprises et on pourra lire sur le JIM nombre d’articles sans nuance sur ce sujet). Concernant le professeur Gilles-Eric  Séralini, on notera également que la « dénigration » (sic) est loin d’être totale (il est par exemple remarqué que son étude a le mérite de soulever la question des adjuvants).
Mais enfin, comment notre « ton » n’aurait-il pas pu être taxé de partialité si nous nous étions lancés dans l’hagiographie d’un chercheur dont la précédente étude a été l’objet de critiques émanant d’une grande partie de la communauté scientifique, qui publie aujourd’hui ses nouveaux travaux dans une revue à faible impact factor et qui de nouveau est la cible des critiques de plusieurs spécialistes (car nous ne faisons ici que relayer les commentaires de chercheurs s’étant exprimés dans la revue de l’AAAS). C’est sans doute si nous nous étions laissé aller à une telle admiration qu’il aurait alors peut-être fallu soupçonner quelques failles dans notre appréciation de la situation.
La rédaction
PS : Ne vous inquiétez pas pour les finances des « lobbies agro-industriel » ou pour le notres : nous avons été tant nourris de pesticides, que nous n’avons même pas besoin d’être payés par eux pour en vanter les mérites ! (par contre nos enfants n’aiment pas le Rondup… ni même le pamplemousse !).
A l'exemple de Louis Pasteur
Le 13 février 2014
Madame Haroche,
En lisant de temps à autres le bulletin quotidien du JIM, c'est avec plaisir que j'ai constaté que régulièrement, le JIM traite de sujets qui ne sont pas directement liés à la pratique médicale. Par exemple, il y a eu plusieurs articles sur la tragédie de la centrale nucléaire de Fukushima au Japon, des articles sur les pesticides, dont les fameux néonicotinoïdes.  J'ai remarqué que très souvent vous étiez la signataire de ces articles et je peux vous dire que j'aime beaucoup votre style direct et précis, qui ne s'enfarge pas dans les fleurs du tapis et qui ne fait pas dans l'autocensure.  Et aussi, j'ajouterais qu'au JIM, il ne semble pas y avoir de sujet tabou, par exemple sur la circoncision ou de la certification religieuse des aliments (cachère, hallal, etc.).  Pour tout cela, un gros Bravo et longue vie à vous et au JIM: vous savez vous tenir debout et résister aux nombreux lobbies et autres manipulateurs d'opinion!
Toutefois, dans cet article portant sur les "déboires" de Gilles-Éric Séralini, j'ai cru qu'il était de mon devoir de vous écrire pour vous expliquer pourquoi je pense que vous devriez écrire un nouvel article pour cette fois prendre la défense de G.E. Séralini et de son équipe.   Ils méritent notre appui parce que pour la première fois depuis l'arrivée des plantes transgéniques dans la filière agroalimentaire, une arrivée qui n'a fait l'objet d'aucun débat démocratique, d'aucun débat scientifique ouvert et d'aucune étude indépendante à long terme,  il y a des gens très décidés, animés par le leadership de l'infatigable Gilles-Éric Séralini, qui ont décidé d'utiliser la méthode scientifique, oui, celle-là même léguée par René Descartes au 17ème siècle, pour vérifier, au moyen d'étude à long terme ou chroniques, si les OGM pouvait avoir un impact sur la santé des animaux qui en sont nourris et éventuellement sur la santé des humains qui sont exposés indirectement aux OGM. Ce combat, car c'en est un, est une véritable lutte de David contre Goliath. Pourquoi ? Parce qu'il y a des intérêts financiers colossaux qui se cachent derrière ce débat. En clair, Monsieur Séralini dérange et il dérange beaucoup. Les anglo-saxons disent (et au sens figuré, ils le font) "quand on veut tuer son chien, on dit qu'il a la gale".  Monsieur Séralini et son équipe sont victimes d'une campagne de salissage qui a toutes les allures d'un mouvement de panique, d'une réaction exacerbée, chez les vendeurs d'OGM, dont Monsanto.  L'étude du CRIIGEN publiée en 2012 a eu l'effet pour eux d'un véritable pavé dans la mare.  Monsieur Séralini est depuis quelques années dans une position semblable à celle de Louis Pasteur dans le débat qui l'opposait aux supporteurs de la génération spontanée. Souvenez-vous que pour son travail, Louis Pasteur fut désigné rien de moins que "Bienfaiteur de l'Humanité". Autre époque, autres mœurs ...
Je pense que si vous y regardez de plus près, en fouillant les dossiers des OGM et des pesticides, et par exemple, en lisant ou en relisant "The Origin of Species" de Charles Darwin, vous comprendrez que les travaux du Professeur Gilles-Éric Séralini et de son équipe méritent notre respect, tous nos encouragements et toutes nos félicitations. La sélection naturelle est quelque chose qui travaille 24 heures sur 24, 365 jours sur 365 jours, décennies après décennies, siècles après siècles, sans interruption pendant des millions d'années. Voilà le principe fondamental qui a été oublié ou escamoté par tous ces laborantins bien rémunérés qui s'échinent à inventer des plantes transgéniques brevetables (merci au Sénat états-unien qui s'est laissé acheter par Monsanto ...) et commercialisables, et pourquoi au juste ?  Pour faire encore plus d'argent, en vendant les pesticides qui vont avec (le Roundup et bientôt 2,4-D et Dicamba: voir à ce sujet   http://www.panna.org/blog/usda-greenlights-new-GE-seeds ). La santé publique et la pérennité des écosystèmes, ils s'en contrefichent parce qu'à court terme ce n'est pas payant pour eux.
Quelques remarques pour finir à propos des travaux de G.E. Séralini sur les OGM.  La « communauté scientifique » n’est pas et n’a jamais été monolithique. Tant mieux. Nombreux sont ceux qui au contraire ont applaudi devant le courage de G.E.  Séralini et de son équipe et aussi devant la méthodologie exemplaire utilisée pour cette désormais célèbre expérience. Certaines firmes multinationales n’auraient jamais hésité à saboter cette recherche si le secret n’en avait pas été jalousement gardé pendant qu’elle suivait son cours (lire entre autres à ce sujet le livre de Marie-Monique Robin « Le Monde sans Monsanto »). Parler à propos de cette étude de « biais méthodologique » est faire le jeu du cartel des industries biotechnologiques et autres bricoleurs d’OGM. En outre si l’étude a été retirée, c’est parce que le Conseil d’administration de la revue Food and Chemical Toxicology est désormais noyauté par des gens qui sont en service commandé, pour le compte d’un certain lobby très facile à identifier. Voici à ce sujet tout l’argumentaire de G.E. Séralini et de son équipe sur le site internet suivant http://www.criigen.org/SiteFr//index.php).
Enfin, je pense qu’avant de relayer le discours de ceux qui s’en prennent à Séralini et à son équipe en critiquant la méthodologie qu’ils ont utilisée dans cette nouvelle étude, vous devriez par vous-même la lire et comprendre son contenu. Les pesticides méritent toujours d’être qualifiés de pesticides comme vous le faites dans votre article et non d’euphémismes comme par exemple « produits phytosanitaires ». Ce sont des biocides et des poisons et doivent donc être traités et jugés comme tels.
Quelques sites Internet qui pourraient vous fournir d'autres points de vue pertinents:
http://www.nongmoproject.org/
http://www.i-sis.org.uk/index.php
http://www.panna.org.
Autre lecture suggérée : « Notre poison quotidien » de Marie-Monique Robin, journaliste d’enquête décorée de la Légion d’honneur en 2013 et Prix norvégien Rachel-Carson en 2009.
Normand Cossette, ingénieur et agronome
Président et fondateur d'Irrigation NORCO inc., une entreprise qui a reverdi à ce jour 1500 hectares de résidus stériles miniers.
www.irrigationnorco.com
Subjectivité-objectivité le combat permanent...
Le 15 février 2014
On retrouve avec les pesticides un peu les mêmes débats qu'avec le CO2: le catastrophisme excessif des uns répondant au négationnisme irresponsable des autres. Avec les pesticides comme avec le CO2 l'honnête homme reste sur sa faim en matière d'objectivité après des débats qui semblent dirigés par des conflits d'intérêts décelables de tous les côtés...
Dans tous les cas les pro et les anti - quelque chose ont tendance a recréer une réalité qui leur convient en éludant involontairement ou sciemment les éléments gênants susceptible d'aller à l'encontre de leurs théories ou de leurs  idées fixes...On rencontre d'ailleurs la même tendance en ce qui concerne le dépistage des cancers, le danger des contraceptifs, les prétendues pandémies de grippe machin N1...etc. Il y a cependant un espoir pour tous ceux qui comme moi veulent réellement "savoir " c'est que la réalité finit toujours par être plus têtue  que ses contradicteurs, elle finit toujours  par arbitrer les conflits orientés et stériles.
Dr JF Huet
Un article reflétant l'avis de la communauté scientifique
Le 15 février 2014
Si je comprend bien les deux réactions ci-desssus, on ne peut être objectif qu'en adhérant aux thèses de monsieur Seralini. Le critiquer vous expose à des interprétations pour le moins erronées de vos propos. Je trouve au contraire l'article de madame Haroche parfaitement mesuré et reflétant l'avis de la communauté scientifique (voir notamment le retrait par la revue de l'article précédent). J'ai bien compris que la réaction était réservée aux professionnels de santé, je ne suis pas sure que les deux professionnels de santé qui s'expriment soient pour autant des scientifiques, leur réaction semble prouver le contraire.
CG
Les industriels font de même
Le 15 février 2014
Si la méthodologie de Gilles-Eric Seralini peut être discutée, alors celle des études qui ont débouché sur l'autorisation de mise sur le marché de pesticides toxiques avérés, retirés ou non, ou herbicides, doit l'être aussi. D'ailleurs, Gilles-Eric Seralini n'a t'il pas affirmé, pour son étude sur les OGM (ou plutôt plantes génétiquement modifiées) que sa méthodologie et les souris employées, étaient les mêmes que celles utilisées par Monsanto et autres?
C'est à méditer, et si G-E Seralini "froisse" la communauté scientifique, elle n'est pas suffisamment froissée par l'effet cocktail auquel les populations sont aujourd'hui exposées, et les plans anticancers oublient allègrement de prévoir une action "à la source" en tenant compte de l'expertise de l'INSERM parue en 2013 et prouvant le lien entre exposition aux pesticides et diverses maladies...L'augmentation de l'incidence des cancers, pour une population donnée identique, est certainement la face cachée de l'iceberg et la médecine environnementale, encore jeune, pourrait bien se developper. A ce titre, G-E Seralini  fait figure d'avant-gardiste, et s'il commet des erreurs, le résultat n'en débouche pas moins sur un progrès pour le bien commun et la santé publique.  Cela ne gêne donc personne que ce soient les industriels eux-mêmes qui mènent et publient les études sur leurs propres produits, débouchant sur leur commercialisation ?
Germain Poignant
Et dans la nature ?
Le 16 février 2014
Est-ce que les souris de nos campagnes, les rats des champs, bref tous les animaux sauvages qui vivent dans nos cultures et dans nos vergers (sangliers, chevreuils, perdrix, faisans, lièvres, lapins, hérissons, etc.) sont plus malades ou infertiles qu'auparavant en vivant et en consommant des plantes OGM ou des légumes arrosés de pesticides ? Voilà le vrai moyen de démontrer la nocivité des innovations de l'agroalimentaire. Attendons les avis et les plaintes des agriculteurs et des chasseurs.
Dr Jean Doremieux
Courage scientifique et biais méthodologiques
Le 17 février 2014
Le Pr. Seralini a un certain courage à mettre en oeuvre des recherches sur un temps aussi long, en ayant la certitude qu'elles ne lui rapporteront aucun retour financier...
Je n'ai pas encore lu sa publication, et vais y rêfléchir.
Mais je constate une seule chose : ses conspendieurs lui reprochent uniquement un biais méthodologique.
Très bien.
Corrigons sa méthodologie et reprenons ses études, pour vérifier la pertinence de ses affirmations!
Après nous pourrons le critiquer en toute honêteté.
Ariel Dahan
Pesticides et composés des parfums
Le 17 février 2014
Permettez moi de remettre en perspective les sujets d'actualité médicale auxquels s'attache cette lettre du JIM.
D'une part un article sur des composants qui seront interdits dans des parfums (comme Chanel numéro 5) utilisés depuis des décennies ... et qu'il semble néanmoins urgent d'interdire bien que l'impact en terme de santé publique de l'usage de ces composants n'ait pas à proprement parlé été évalué selon un approche scientifique directe et documentée.
Les composés (potentiellement) allergisants  des parfums ne seraient-ils pas "l'arbre qui cache la forêt ? Forêt sur laquelle votre article sur les pesticides attire l'attention. Vous y mentionnez essentiellement la publication du Pr Séralini portant sur ce "nouvel axe de recherche" qu'est la toxicité des pesticides. Comme souligné par  Normand Cossette, ingénieur et agronome (dans le commentaire ci-dessus) cette toxicité est avérée dans bien de cas: qu'est un produit phytosanitaire tel un herbicide ou un biocide si ce n'est un produit toxique de sélectivité plus ou moins large ? Il est remarquable de constater que pour un sujet aussi important l'engouement semble limité ... et le fameux principe de précaution oublié ! Quid des pesticides et du programme REACH ?
http://ec.europa.eu/enterprise/sectors/chemicals/reach/index_fr.htm.
Quid  des recommandation générées par l'OMS dans le cadre de l'utilisation des pesticides à des fins de lutte contre les vecteurs des maladies infectieuses ?
http://www.who.int/whopes/resources/SEA_CD_214.pdf
La démarche du Pr Séralini, peut à certain égards être imparfaite (les fabricants sauront l'analyser de manière critique), elle a néanmoins l'avantage de donner de la visibilité à ce sujet: les pesticides indispensables aux activités humaines doivent être caractérisés de manière extensive sur le plan toxicologique et pharmacologique avant et après leur mise sur le marché. Ledit marché se doit d'être fortement régulé ce qui aura comme résultante de favoriser la recherche de composants nouveaux et plus sûrs.
A quand un grand consortium de recherche public et privé pour prendre à bras le corps ce sujet négligé ? A quand un relais par le cors médical de l'information sur un sujet de santé publique d'importance majeure?
Et pour commencer, pourquoi ne pas faire un sondage JIM demandant aux professionnels de santé s'ils se sentent suffisamment informés sur le sujet de la dangerosité et de la toxicité des pesticides ?
Il y a fort à parier que beaucoup exprimerons leur souhait d'être mieux informés.
Dr. M-E Behr-Gross
Félicitations à Aurélie Haroche
Le 25 février 2014
Cet article est clair, informatif et mesuré, comme d'habitude sous son excellente plume.
PR
Réagir à cet article
Les réactions aux articles sont réservées aux professionnels de santé.
Elles ne seront publiées sur le site qu’après modération par la rédaction (avec un délai de quelques heures à 48 heures). Sauf exception, les réactions sont publiées avec la signature de leur auteur.
Sujet
Votre message
Envoyer
Lorsque cela est nécessaire et possible, les réactions doivent être référencées (notamment si les données ou les affirmations présentées ne proviennent pas de l’expérience de l’auteur).
JIM se réserve le droit de ne pas mettre en ligne une réaction, en particulier si il juge qu’elle présente un caractère injurieux, diffamatoire ou discriminatoire ou qu’elle peut porter atteinte à l’image du site.
En ce moment...
