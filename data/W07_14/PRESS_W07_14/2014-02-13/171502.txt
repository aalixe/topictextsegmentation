TITRE: Enfant mort dans une explosion � Nice: sa m�re soup�onn�e d'infanticide - 13/02/2014 - leParisien.fr
DATE: 2014-02-13
URL: http://www.leparisien.fr/nice-06000/enfant-mort-dans-une-explosion-a-nice-sa-mere-soupconnee-d-infanticide-13-02-2014-3588033.php
PRINCIPAL: 0
TEXT:
Enfant mort dans une explosion � Nice: sa m�re soup�onn�e d'infanticide
Publi� le 13.02.2014, 17h59
R�agir
Une jeune m�re a �t� mise en examen et �crou�e pour le " meurtre " de son enfant de quatre ans, retrouv� mort le 24 janvier apr�s l'explosion de son immeuble � Nice, a-t-on appris jeudi de source judiciaire.
La femme soup�onn�e d'infanticide a �t� mise en examen mercredi soir pour "meurtre sur mineur" et "destruction d'un bien par l'effet d'une substance explosive" ayant entra�n� deux bless�s l�gers, a pr�cis� le parquet de Nice.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Cette m�re c�libataire cap-verdienne de 26 ans devait compara�tre trois jours apr�s le drame devant le tribunal correctionnel de Nice pour une affaire d'escroquerie en bande organis�e concernant des faux-papiers portugais.
Elle avait d�j� effectu� quelques mois de prison dans le cadre de cette affaire et son enfant avait alors �t� plac� dans une famille.
L'explosion au gaz du 24 janvier s'�tait d�roul�e dans son appartement, situ� au dernier niveau d'un immeuble de deux �tages du centre-ville de Nice.
Certains indices avaient imm�diatement laiss� penser aux enqu�teurs que l'enfant �tait mort avant l'explosion, une th�se corrobor�e par un m�decin l�giste, a pr�cis� le commissaire Nicolas Hergot, chef de la s�ret� d�partementale des Alpes-Maritimes.
La m�re, gri�vement br�l�e dans l'explosion et transport�e dans un h�pital sp�cialis�, avait �t� entendue succinctement en tant que t�moin.
Mercredi, elle a pu �tre plac�e en garde � vue et a livr� des "aveux d�taill�s" aux policiers de la s�ret� d�partementale.
Il semblerait que la jeune femme ait voulu se donner la mort par asphyxie en remplissant sa cuisine de gaz. Les enqu�teurs doivent encore d�terminer si l'explosion �tait accidentelle.
La toiture au-dessus de l'appartement avait �t� souffl�e par l'explosion et le b�timent avait perdu de larges pans de sa fa�ade.
Deux voisins, qui vivaient � c�t� de l'appartement souffl�, avaient �t� l�g�rement atteints.
