TITRE: Le Conseil constitutionnel dit oui au non-cumul des mandats - 13 février 2014 - Le Nouvel Observateur
DATE: 2014-02-13
URL: http://tempsreel.nouvelobs.com/cumul-des-mandats/20140213.OBS6341/le-conseil-constitutionnel-dit-oui-au-non-cumul-des-mandats.html
PRINCIPAL: 0
TEXT:
D�s le�31 mars 2017, il sera interdit d'�tre�d�put�, s�nateur ou d�put� europ�en et d'�tre aussi maire d'une ville ou pr�sident d'une intercommunalit�.
Le 13 mars le texte sur le non-cumul devrait être éxaminé. (Sipa)
Le�Conseil�constitutionnel a annonc� jeudi 13 f�vrier avoir valid� la loi interdisant de cumuler un mandat parlementaire avec une fonction ex�cutive locale � partir de 2017.
A compter du 31 mars 2017, il sera alors interdit d'avoir un mandat de d�put�, s�nateur ou d�put� europ�en et d'�tre en m�me temps maire d'une ville ou pr�sident d'une intercommunalit�, d'un�conseil�g�n�ral ou r�gional, notamment.
Le�Conseil�constitutionnel "a jug� qu'il �tait loisible au l�gislateur de poser de telles incompatibilit�s", selon un communiqu� des neuf juges.
Actuellement, 60% des parlementaires cumulent ce mandat avec une fonction ex�cutive locale.
La loi prohibant un tel cumul avait �t� vot�e d�finitivement par l'Assembl�e nationale le 22 janvier, par 313 voix ( socialistes , �cologistes et communistes), contre 225 et 14 abstentions.
132 s�nateurs UMP et UDI-UC, mais aussi de la majorit� gouvernementale, RDSE (� majorit� PRG) d�non�aient une "atteinte au bicam�risme". Ils estimaient notamment que la loi aurait d� �tre adopt�e en termes identiques par l' Assembl�e nationale et le S�nat.
Partager
