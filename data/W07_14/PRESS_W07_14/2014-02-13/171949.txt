TITRE: Angry Birds Stella : tout ce qu'il faut savoir sur le jeu
DATE: 2014-02-13
URL: http://www.jeuxactu.com/angry-birds-stella-tout-ce-qu-il-faut-savoir-sur-le-jeu-91681.htm
PRINCIPAL: 171948
TEXT:
News
Angry Birds Stella annonc�
Un seul artwork. Voil� ce dont il faudra se contenter pour l'annonce d'Angry Birds Stella, un nouveau spin-off de la c�l�bre s�rie fra�chement annonc� par Rovio sur son blog . Les informations sur le jeu demeurent plut�t minces pour le moment, et on sait seulement que Stella, un oiseau de couleur rose, sera accompagn�e de tout un tas d'amis dans cette nouvelle aventure. Naturellement, des ennemis pointeront le bout de leur nez pour que ce voyage soit tout sauf paisible, et on nous promet d�j� qu'Angry Birds Stella "donnera sur une partie de l'univers de la s�rie Angry Birds encore jamais vue" ; rien que �a.
Aucun support n'a encore �t� annonc� pour le moment, mais on peut facilement supposer que les smartphones et les tablettes iOS/Android seront dans le coup. Quant � la date de sortie, elle est pr�vue pour l'automne prochain.
