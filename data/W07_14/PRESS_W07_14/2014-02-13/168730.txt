TITRE: Rythmes scolaires : Copies � revoir dans le Morbihan. Info - Lorient.maville.com
DATE: 2014-02-13
URL: http://www.lorient.maville.com/actu/actudet_-rythmes-scolaires-copies-a-revoir-dans-le-morbihan_fil-2490208_actu.Htm
PRINCIPAL: 0
TEXT:
Twitter
Google +
De gauche � droite, Philippe Jumeau, Martine Derrien, Jacques Brillet, du bureau d�partementale de la SNUipp-FSU.� Ouest-France
Le premier syndicat des enseignants du Morbihan, le SNUipp-FSU, livrera demain jeudi son rapport alternatif � la r�forme des rythmes scolaires.
Dans le Morbihan, vingt-cinq �coles publiques, r�parties dans douze communes, sont pass�es � la semaine des quatre jours et demi. Le syndicat a interrog� les enseignants dans 95 �coles, 29�% des �tablissements ont r�pondu � l�enqu�te.
Un bilan en demi-teinte
Dans l�ensemble, le bilan est n�gatif en ce qui concerne les �l�ves mais aussi les enseignants. "Les conditions d�apprentissage, le climat scolaire, l�organisation de l��cole, les conditions de travail des enseignants et les relations avec les parents se sont d�t�rior�s", indique Martine Derrien, la secr�taire d�partementale de la SNUipp-FSU.
Le bilan est�� nuancer selon que les enseignants aient �t� consult�s�sur les rythmes scolaires�et leur avis pris en compte. "Cela est marqu� � la fois pour les conditions d�apprentissage des �l�ves o� le taux de satisfaction passe � 43�% contre 20�% dans le cas contraire."
Demande de r��criture du d�cret
En l'�tat, cette r�forme n�est pas applicable avec succ�s dans toutes les �coles indique le syndicat. "� l��chelle d�partementale, nous rendons ce jeudi notre contre-rapport aupr�s de la directrice acad�mique des services de l�Education nationale."
La SNUipp-FSU demande la r��criture du d�cret. Selon le syndicat, les enseignants doivent �tre accompagn�s par les institutions en raison des lourds changements qui s�op�rent. Leur consultation doit �tre un passage oblig� et leurs avis prioritaires.
�lisa MANIAGO�� Ouest-France��
