TITRE: Gel de l'avancement des fonctionnaires : juste ou injuste, efficace ou pas ? | Atlantico
DATE: 2014-02-13
URL: http://www.atlantico.fr/decryptage/gel-salaire-fonctionnaires-juste-ou-injuste-efficace-ou-pas-jacques-bichot-980112.html
PRINCIPAL: 169499
TEXT:
- 14/02/2014 - 02:43 - Signaler un abus 72,8 milliard d'euros en 2012, 145 000 salari�s : les agences
Bonsoir,
Mais pourquoi personne ne parle des agences d'�tat ?
Ces agences ont une mission de service public, per�oivent des subventions pay�es par nos imp�ts et pr�l�vements, et leurs salari�s sont salari�s du priv�.
72,8 milliards d'euros (hors caisses de s�curit� sociale) pour 145 000 salari�s, voil� leur budget pour 2012.
Un colloque organis� par le Conseil d'�tat en octobre 2012 (http://evaluationvigie.fr/wp-content/uploads/2012/10/colloqueagences-1.pdf) a r�pertori� le budget de 103 agences.
A titre d'exemple  pour l'AFD :
Budget (millions �) : 3 191,23   Effectifs (en ETP) : 1 715
Ce qui nous fait 1 860 775 � par salari�, pas mal, non?
Par comparaison, pour P�le Emploi, le budget est de 107 154 � par salari�. C'est s�r qu'ils sont nettement moins bien pay�s !
Un rapport de l'Inspection G�n�rale des Finances (http://www.economie.gouv.fr/files/2012-rapport-igf-l-etat-et-ses-agences.pdf) datant de mars 2012 pr�sente le salaire moyen par agent, soit plus de 61 000 � par an en 2010 (page 25). Ces agents ont certainement �t� augment�s depuis, gr�ce � nous, contribuables !
Par
DEL
- 14/02/2014 - 00:44 - Signaler un abus @zelectron
Belle initiative: cela fera un ou deux millions de ch�meurs en plus, puisque les patrons ne veulent pas embaucher. Belle perspective!
Par
prochain
- 13/02/2014 - 20:43 - Signaler un abus @eole14 merci de penser aux 6 000 000 ch�meurs du priv�
Il y a une tr�s mauvaise R�partition du ch�mage ...pas vrai, le secteur priv� prend tout, pouvoir d'achat hein?
En Europe presque partout il n'y a que la fonction r�galienne qui a le privil�ge du STATUT, les r�formes structurelles sont pass�s par l�...
Un seul contrat un seul code de travail pour tous selon le principe d'�galit� lisez la Constitution ch�re eole14.
Par
vitalis
- 13/02/2014 - 20:01 - Signaler un abus Il faut commencer par les plus gros abus
Il faudrait commencer par d�graisser les super-privil�gi�s c'est � dire les edf, dockers ou cheminots qui re�oivent 4000 euros par mois de retraite � 55ans plut�t que de s'attaquer aux fonctionnaires qui travaillent s�rieusement et ont du mal � joindre les 2 bouts.
Par
eole14
- 13/02/2014 - 19:28 - Signaler un abus C EST HONTEUX
encore une mesure d'un gouvernement qui se dit "socialiste" les classes moyennes ne font que perdre du pouvoir d'achat: fiscalisation des heures suppl�mentaires, fiscalisation des participations  des employeurs aux mutuelles et maintenant gel des avancement d'�chelons des fonctionnaires
nous perdons du pouvoir d achat deja avec le gel du point d'indice, et contrairement au priv� aucune possibilit� de n�gocier notre salaire � titre personnel
pendant ce temps la on continue � verser des aides � des personnes qui profitent du syst�me, des sommes hallucinantes pour certaines familles nombreuses
si cette mesure passe je ferai gr�ve pour la premi�re fois de ma vie (j ai 42 ans)
Par
MEPHISTO
- 13/02/2014 - 18:59 - Signaler un abus La justice , c'est maintenant... rappel d'un discours
Dans le secteur public , c'est encore la base , c'est � dire les fonctionnaires des cat�gories B et C , qui apr�s la suppression des heures suppl�mentaires d�fiscalis�es,  va trinquer une fois de plus... les avantages en nature tels que les logements et voitures de fonction , les costumes sur mesure , les primes annuelles substancielles ect...ect... vont �tre pr�server pour les hauts fonctionnaires , autrement dit la cat�gorie A. le point d' indice gel� depuis des ann�es maintenant c'est au tour de l' avancement pour encore  plusieurs ann�es . cela va leur donner sans doute � r�fl�chir en comparant le fonctionnement et la gestion des administrations publiques sous l' �re SARKOZY et celle d' HOLLANDE. notamment pour celles et ceux qui ont vot� pour lui
Par
GOGGOS
- 13/02/2014 - 16:50 - Signaler un abus une sp�cificit� fran�aise ?
Est-ce une sp�cificit� fran�aise ? Il n�est nul article qui reproche les � avantages � des fonctionnaires, des � avantages � de tel ou tel employ�, SNCF, EDF, Banques�mais jamais le nombre et si on se posait la question autrement ils m�ritent ces avantages et m�me peut �tre plus, mais avec DEUX FOIS MOINS DE PERSONNEL.
Si l�on connaissait le nombre de fonctionnaires et autres qui font tourner le pays en juillet et ao�t on tirerait peut �tre des enseignements.
Une phrase souvent dite par mon grand-p�re �tait : Il vaut mieux un qui sait que cent qui cherchent.
Est ce qu�avec 200 d�put�s il y aurait moins de lois utiles qu�avec 500 ? etc etc
Par
Stratix
- 13/02/2014 - 15:26 - Signaler un abus Fonctionaires : Stop � l'amalgame!
Il faut distinguer ceux qui produisent  des richesses pour le pays ( environ 60%) et les administratifs/bureaucrates qui sont toxiques pour l'�conomie du pays ( malgr� leurs qualit�s personnelles): la France a environ deux fois trop d'administratifs ( r�sultat aussi de la complexit� destructrice) :il y a donc de grosses marges de man�uvre pour augmenter ceux qui sont utiles. En conclusion, il y a une enjeu d'efficacit�, et non de c�ut, pour ceux qui sont utiles ( passer aux 40 heures....) et un enjeu de baisser drastique des c�uts pour les administratifs.
Par
Macha92
- 13/02/2014 - 14:34 - Signaler un abus SATURNE
Effectivement je retourne dans le priv� car je peux vous assurez que le rendement est tout aussi important pour moi que dans la priv� alors la garantie de l'emploi ne me permettant pas de me loger , je c�de ma place. Dans le mairie pour laquelle je travaille je suis la 4�me cette ann�e ce qui veut bien dire que ce n'est pas si bien que �a.
Par
- 13/02/2014 - 14:13 - Signaler un abus @BGVKT, " deux philosophies totalement diff�rentes " ...?
Deux plan�tes totalement diff�rentes...pourvu que �a dure...
Par
prochain
- 13/02/2014 - 14:09 - Signaler un abus 6 millions ch�meurs 10 millions fonctionnaires retrait�s et...
assimil�s. Les UNS, revenus garantis � vie, les autres 6 millions pigeons au ch�mage. Il n'y a que les premiers qui auront droit � la retraite (calcul�e sur le dernier salaire) � taux plein tout en cotisant moins ...EGALITE en lettres dor�es sur le fronton de 36 000 mairies.
Par
saturne
- 13/02/2014 - 13:53 - Signaler un abus Macha92
En ce qui vous concerne il y a deux solutions la premi�re vous retournez dans le priv� et vous arr�tez de vous plaindre
la seconde solution vous restez � votre poste actuel car le rendement demand� c'est un tiers du public ce qui veut dire que par rapport au travail fourni vous �tes encore gagnants de 50 % avec une garantie d'emploi que vous n'aurez pas dans le priv�
Par
prochain
- 13/02/2014 - 13:48 - Signaler un abus A part La Fonction r�galienne en Europe Le Statut a disparu...
gr�ce aux r�formes structurelles. Tout le monde travaille (les profs aussi!) sous contrat de droit priv�, un seul contrat et le m�me code de travail pour tous.
Qu'en pensez-vous,  est-ce faisable en France ? Je pense que NON ... couper la branche o� ON est si bien assis...faut pas r�ver.
Par
GBCKT
- 13/02/2014 - 13:07 - Signaler un abus Priv" et public deux plilosophie totalement diff�rentes
Dans le priv� c'est le prix du march�. La convention collective et ou d'entreprise est l� pour figuration. Les salaires y sont un des seuls secrets presque gard�s tant les disparit�s sont grandes en particulier du fait de la croissance externe.
Le fonctionnaire est avant tout "fid�lis�" d'o� l'avancement � l'anciennet� sans tenir compte de la performance.  Lorsqu'il y a "h�morragie de d�parts" on ne joue pas sur l'avancement on cr�e des primes de fid�lisation, non prises en compte pour les droits � retraite. Le r�f�rent n'est actuellement pas plus que dans le priv� les dipl�mes d�tenus mais les dipl�me exig�s pour le recrutement.
Par
zelectron
- 13/02/2014 - 12:49 - Signaler un abus mesurette !
ce sont un million de fonctionnaires (ou collat�raux, collabos?) qu'il faut virer sur un programme de dix ans, tout autre option ne fait que nous enfoncer dans le trou sans possibilit�s de cr�ations de richesses ! Les sommes ainsi report�es seront pharaoniques ...
Par
- 13/02/2014 - 12:23 - Signaler un abus Renversons la table !
Il faudrait remplacer un retrait� sur deux par un fonctionnaire. :o))
Par
Macha92
- 13/02/2014 - 12:15 - Signaler un abus Fonctionnaire en col�re
Il ne faut pas confondre haut fonctionnaire dont les avantages sont inacceptables et petit fonctionnaire en bas de l'�chelle. J'ai travaill� 10 ans dans le priv�, je suis depuis 5 ans fonctionnaire  territorial c�t C �chelon 5 dans les Hauts de Seine.
15 ans d�exp�rience, bac +3, bilingue. Salaire 1650 �  net toutes primes incluses, - 30% avec mon ancien salaire. Salaire de base 1481 � brut donc retraite calcul�e sur cette base. Pas de mutuelle d'entreprise, pas de jours de cong�s sup par rapport au priv�, m�me pas de jours d�anciennet�, m�me horaires que le priv�. Parlons de l'avancement 194.47 � entre le 1er et 11�me �chelon et 18.66 ans (si,si) au MINIMUM pour passer du 1er au 11�me �chelon. Difficile de vivre dans les Hauts de Seine avec deux enfants et un tel salaire. Conclusion je cherche � repartir dans le priv�.
Par
- 13/02/2014 - 11:26 - Signaler un abus choix,reduction du millefeuille ou baisse des salaires?
Patagon.pps
Gamelledebouse
- 13/02/2014 - 11:20 - Signaler un abus Courage ?
Le casqu� et son gouvernement de clampins n'auront pas le courage de s'attaquer de front � leur coeur de cible �lectoral . Si au moins on gelait symboliquement le salaire  des hauts fonctionnaires et si on baissait de 5 ou 10% les grasses indemnit�s des parlementaires ... Ils dormiraient sans doute aussi bien sur les bancs .
La droite doit se f�liciter de n'�tre pas aux affaires . Pour une fois c'est la gauche qui doit faire le sale boulot . On n'a pas fini de rigoler .
Par
- 13/02/2014 - 11:04 - Signaler un abus De toutes fa�on c'est du "bidonnage"
Y'a du concret dans cette histoire?
Non il eu �t� supput� dans les milieux autoris�s comme disait Colluche..
Quand bien m�me cela se concr�tiserait, cela serait compens� par des primes et indemnit�s.
Le lobby du public est le plus puissant de France. Il faudra une cessation de paiement de l'�tat claire nette et sans �quivoque pour qu'un changement m�me minime existe.
Attendre comme en Argentine la faillite de l'�tat.
Par
gyvermac
- 13/02/2014 - 10:53 - Signaler un abus Quand on est en failite la
Quand on est en failite la question de ce qui est juste ou pas ne se pose pas. Quand votre compte en banque est � z�ro vous r�duisez votre train de vie point barre. J'ai vu �a � plusieurs reprises dans le priv�. Moins de ressources plus de d�merde, recentrage sur l'essentiel. Et personne n'est indispensable.
Par
tubixray
- 13/02/2014 - 10:36 - Signaler un abus au fait on attend quoi
Si le point d'indice est gel� depuis 3 ans, l'augmentation � l'anciennet� tourne toujours � plein r�gime !
Par solidarit� avec les salari�s du priv� (bac + 5 = SMIC + 300 �), les CDD renouvel�s tous les 6 mois, les temps partiels subis, les chomeurs qualifi�s cherchant vraiment un emploi la moindre des choses serait de bloquer tout avancement pour une dur�e ind�termin�e.
Jacques Bichot
Jacques Bichot est Professeur �m�rite d��conomie de l�Universit� Jean Moulin (Lyon 3), et membre honoraire du Conseil �conomique et social.
