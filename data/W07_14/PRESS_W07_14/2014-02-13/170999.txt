TITRE: Besançon: les clés USB éducatives renvoyaient vers un site porno - charentelibre.fr
DATE: 2014-02-13
URL: http://www.charentelibre.fr/2014/02/13/besancon-les-cles-usb-educatives-renvoyaient-vers-un-site-porno,1880060.php
PRINCIPAL: 0
TEXT:
Besançon: les clés USB éducatives renvoyaient vers un site porno
Le 13 février à 14h30 par charentelibre.fr (AFP)
Tweeter
La municipalité de Besançon a annoncé ce jeudi le retrait de 2.000 clés USB éducatives et lancé une vaste opération d'information dans ses écoles, après avoir découvert que ces clés renvoyaient vers des sites pornographiques et vers un jeu de guerre ultra-violent.
Dans le cadre d'un dispositif mettant à disposition des ordinateurs aux écoliers, la ville de Besançon avait transmis ces clés aux élèves de CE2.
"Chaque année depuis 2002, on donne une clé USB dans laquelle il y a une quantité impressionnante de logiciels ludo-éducatifs ainsi que quelques liens sur lesquels les élèves peuvent cliquer pour aller vers des sites de jeux", a expliqué le directeur général des services de la ville, Patrick Ayache.
Mais "cette année, les noms de domaines de deux de ces sites ont été rachetés, de façon légale, par des sociétés peu scrupuleuses qui les utilisent pour la promotion de sites pour adultes", a-t-il fait savoir.
Aucun image pornographique ou de contenus tendancieux n'est visible sur la clé USB, a également fait savoir la mairie dans un communiqué. "Il s'agit d'une forme de détournement légal, pervers et inadmissible", a également jugé la municipalité, qui se réserve le choix "d'engager d'éventuelles poursuites".
C'est un père de famille qui a fait samedi la découverte de ces deux sites douteux, avant d'en avertir la mairie. Celle-ci a ensuite décidé d'informer "au nom du principe de précaution" quelque 6.000 familles concernées. De fait, outre les 2.000 élèves de cette année, ceux des deux années précédentes, actuellement en CM1 et CM2, auraient été susceptibles d'accéder aux mêmes liens.
Selon M. Ayache, "peu d'enfants devraient avoir été exposés, si le contrôle parental est activé sur les ordinateurs".
 
