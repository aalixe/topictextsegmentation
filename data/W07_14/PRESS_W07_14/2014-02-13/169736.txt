TITRE: Afghanistan : les Etats-Unis regrettent la lib�ration de 65�prisonniers ��dangereux��
DATE: 2014-02-13
URL: http://www.lemonde.fr/asie-pacifique/article/2014/02/13/kaboul-libere-65-prisonniers-juges-dangereux-pour-washington_4365396_3216.html
PRINCIPAL: 169735
TEXT:
Afghanistan : les Etats-Unis regrettent la lib�ration de 65�prisonniers ��dangereux��
Le Monde |
� Mis � jour le
13.02.2014 � 17h23
En d�pit de vives protestations am�ricaines, les autorit�s afghanes ont approuv�, jeudi 13 f�vrier, la lib�ration de soixante-cinq combattants talibans pr�sum�s, �crou�s dans la prison de Bagram, apr�s avoir ��r��tudi頻 leurs dossiers respectifs � la lumi�re des plaintes des Etats-Unis.
��Nous les avons lib�r�s. Ils sont partis en voiture chez eux, mais nous n'avons pas organis� le transport pour eux��, a confirm�,�jeudi 13 f�vrier, le g�n�ral Ghulam Farouq, chef de la police militaire � la prison. Kaboul avait annonc� le 9 janvier que soixante-douze d�tenus au total seraient rel�ch�s en raison d'un manque de preuves contre eux.
Le gouvernement de M. Karza� a admis au d�but de f�vrier avoir engag� depuis quatre mois des n�gociations directes avec de hauts responsables talibans. Un tel processus, qui a suscit� de vives critiques am�ricaines, pourrait expliquer ces lib�rations. Il pourrait �galement avoir pes� sur le refus d'Hamid Karza� de signer avec les Etats-Unis un accord de s�curit� bilat�ral qui doit organiser la pr�sence militaire am�ricaine dans le pays apr�s le retrait de la quasi-totalit� de leurs forces cette ann�e.
Lire : Des � contacts nou�s � entre Karza� et les talibans
� UN PAS EN ARRI�RE��
Cette d�cision a �t� qualifi�e de���profond�ment regrettable�� par l'ambassade des Etats-Unis � Kaboul. Washington qualifie en effet les prisonniers lib�r�s de � personnes dangereuses �, directement li�es � des attaques meurtri�res contre des soldats de l' OTAN et des membres des forces nationales afghanes. ��Le gouvernement afghan portera la responsabilit� des cons�quences de cette d�cision��, a ajout� la mission diplomatique.
Pour le secr�taire g�n�ral de l'Organisation du trait� de l'Atlantique Nord, Anders Fogh Rasmussen, ces lib�rations constituent ��un grand pas en arri�re pour l'Etat de droit�� en Afghanistan et posent ��de s�rieux probl�mes de s�curit頻. L'OTAN a ainsi publi� un dossier qui recense les accusations pesant sur trente-sept des prisonniers lib�r�s.
L'administration de l'essentiel de la prison de Bagram a �t� transf�r�e aux autorit�s afghanes par les Etats-Unis en mars 2013, mais ceux-ci contr�lent encore la partie de l'�tablissement o� sont d�tenus des combattants pr�sum�s non afghans, notamment des Pakistanais. Hamid Karza� avait fait de cette prison un symbole des efforts de l'Afghanistan pour recouvrer sa souverainet� nationale.
Une intention sur laquelle le pr�sident afghan a � nouveau insist�, jeudi, en souhaitant que les Etats-Unis arr�tent de � harceler les autorit�s judiciaires afghanes et de contester leurs proc�dures��. � Si les autorit�s judiciaires afghanes d�cident de lib�rer des prisonniers, cela n'est pas et cela ne doit pas �tre l'affaire des Etats-Unis��, a-t-il d�clar�.
Lire aussi l'entretien d'Hamid Karza� au Monde : � Les Am�ricains agissent en puissance coloniale �
Afghanistan
