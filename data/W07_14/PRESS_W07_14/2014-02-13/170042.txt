TITRE: Greta Gerwig, star du spin-off How I Met Your Dad - Télé - FocusVif.be
DATE: 2014-02-13
URL: http://focus.levif.be/culture/tele/greta-gerwig-star-du-spin-off-how-i-met-your-dad/article-normal-12739.html
PRINCIPAL: 170041
TEXT:
à 12:14
Source: Focus Vif
L'annonce est tombée hier matin: Greta Gerwig endosse le rôle principal du spin-off de How I Met Your Mother.
Imprimer
Rapetisser
Greta Gerwig à la Berlinale 2010 pour la promotion de Greenberg. � REUTERS
Déjà neuf saisons et les rides commencaient à apparaître pour la version originale de How I Met Your Mother. Mais CBS fait renaître la bonne humeur avec l'annonce officielle de son spin-off: How I Met Your Dad. Révélée dans les films Frances Ha , To Rome With Love ou Lola Versus, la belle trentenaire Greta Gerwig ouvre le casting. Que le bal commence. Elle en sera aussi la productrice et participera à l'écriture du scénario - si le pilote suffit pour lancer la série.
L'actrice y interprétera le rôle d'une jeune femme dont le soupçon adolescent en fera craquer plus d'un. Quête de l'homme idéal. Fou rire et subtilité. Le nouveau Ted s'appelle Sally et a des airs de Peter Pan.
Si le scepticisme reste ancré chez certains fans qui ne croient pas vraiment à ce spin-off, d'autres attendent la comédie avec impatience. Have you met Sally?
News Focus Vif dans votre flux Facebook
En savoir plus sur:
