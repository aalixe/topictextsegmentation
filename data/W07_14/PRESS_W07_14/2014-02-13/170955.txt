TITRE: Fourcade, et de deux ! - JO 2014 - Sports.fr
DATE: 2014-02-13
URL: http://www.sports.fr/jo-2014/biathlon/articles/martin-fourcade-etait-trop-fort-1009708/
PRINCIPAL: 170923
TEXT:
13 février 2014 � 16h12
Mis à jour le
13 février 2014 � 16h55
Martin Fourcade a décroché jeudi sa deuxième médaille d'or olympique sur l'individuelle. Avec un temps de 49'31"7 et 1 seule faute au tir, le Français a pu faire parler son talent en ski pour ne pas trop trembler.
Histoire de ne pas gagner trop les doigts dans le nez, Martin Fourcade s’est fait peur en commettant une faute sur le deuxième tir lors de l’individuelle. Erik Lesser, lui, a sorti le 20 sur 20 devant les cibles mais ça n’a donc pas suffi. Comme une évidence, le Français entre dans l’histoire en devenant le troisième athlète tricolore à remporter deux médailles d’or sur les mêmes Jeux Olympiques d’hiver, après Jean-Claude Killy en 1968 et Henri Oreiller en 1948. "C’est joli, a-t-il lâché dans un sourire sur France 2. J’ai mis un peu de suspense en loupant cette balle, j’ai posé la seconde main par terre et c’était un peu rock and roll. Je suis content (sourire)."
Cette fois, il n’est pas accompagné d’un autre Français sur le podium. Jean-Guillaume Beatrix termine sixième et Simon Fourcade 13e. "C’est bien pour moi, en Coupe du monde je pars pour le top 10, déclare Beatrix. Par rapport à ce que je fais le reste de l’année, c’est quand même une super course." Simon était nettement moins ravi, abattu par la déception de ses deux tirs manqués alors qu’il aurait pu être dans le coup. "Il faut surtout féliciter Martin. On n’attend pas une 13e place quand on va aux Jeux, mais la médaille. Je l’avais dans les jambes."
Mais il ne suffit donc pas de l’avoir dans les jambes: il faut aussi l’avoir dans la carabine. Ce que son cadet Martin, fort de plusieurs années d’expérience au sommet de son sport, continue de faire fructifier au meilleur des moments: "Je n’étais pas du tout inquiet après ma faute. Car je sais qu’avec une erreur, sur l’individuelle, je peux gagner selon la configuration de course. Je sais qu’avec 19 sur 20, je suis devant en général. Ça m’a permis d’être serein." Heureux, bien sûr, le désormais double champion olympique n’est surtout pas surpris.
Plus d'informations à suivre...
