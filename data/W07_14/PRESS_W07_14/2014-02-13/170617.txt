TITRE: La France 18e du classement FIFA - News - Football - Sport.fr
DATE: 2014-02-13
URL: http://www.sport.fr/football/la-france-18e-du-classement-fifa-339784.shtm
PRINCIPAL: 170613
TEXT:
La France 18e du classement FIFA
Jeudi 13 f�vrier 2014 - 14:27
La France a gagn� deux places au classement FIFA et occupe d�sormais la 18e place. Le Br�sil est pour sa part 9e.
Classement FIFA au 13 f�vrier :
1. Espagne 2. Allemagne 3. Argentine 4. Portugal ( +1) 5. Colombie (-1) 6. Suisse ( +2) 7. Uruguay (-1) 8. Italie (-1) 9. Br�sil( +1) 10. Pays-Bas (-1) 11. Belgique 12. Gr�ce 13. �tats-Unis ( +1) 14. Chili ( +1) 15. Angleterre (-2) 16. Croatie 17. Bosnie-Herz�govine ( +2) 18. Ukraine 18. France ( +2) 20. Danemark ( +5)
La FIFA a d�voil� le nouveau classement des nations ce jeudi. Et l'�quipe de France � gagn� deux places et se trouve d�sormais au 18e rang. Pas de changement en t�te, avec l'Espagne � la premi�re place, suivie de l'Allemagne et de l'Argentine. Le Br�sil, organisateur de la prochaine Coupe du monde est pour sa part 9e. La Belgique occupe la 11e place.
A noter que la premi�re s�lection africaine, la C�te d'Ivoire, se trouve au 23e rang et perd six places. De son c�t�, l'Alg�rie, deuxi�me pays africain, occupe le 26e rang.
Dossiers associ�s
