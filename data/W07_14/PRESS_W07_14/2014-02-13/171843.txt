TITRE: Sotchi 2014: Brian Joubert a eu la sensation �d��tre en gala� - 20minutes.fr
DATE: 2014-02-13
URL: http://www.20minutes.fr/sport/1298742-20140213-sotchi-2014-brian-joubert-sensation-detre-gala
PRINCIPAL: 171841
TEXT:
Brian Joubert, le 13 f�vrier 2014 � Sotchi. YURI KADOBNOV / AFP
JEUX OLYMPIQUES � Le Fran�ais a boucl� un tr�s bon programme court pour sa derni�re apparition aux Jeux...
De notre envoy� sp�cial � Sotchi,
Pour le dernier programme court de sa carri�re �il arr�tera apr�s le libre vendredi- Brian Joubert a pu observer que sa cote d�amour �tait toujours aussi forte parmi les amateurs de patinage. Acclam� par le public de Sotchi du d�but � la fin, le Fran�ais a sans doute sorti les meilleures 2mn30 de sa vie olympique, pas toujours bonne fille avec lui. Cela suffit au bonheur de Joubert, qui ne veut pas croire � la m�daille malgr� une septi�me place provisoire et un podium encore accessible.�
Brian, enfin un programme court r�ussi aux Jeux. �a doit vous faire plaisir?
��a fait du bien. Je suis content, dans le sens o� ce n��tait pas �vident. Le mental a pris le dessus. Il y avait un peu de crispation sur les combinaisons mais j�ai fait ce que je savais faire. Au final, je me suis �clat� sur la piste, et pour mon dernier programme court, �a fait plaisir de terminer comme �a .
Les JO ont toujours �t� une source de stress pour vous. Avez-vous eu le sentiment de patiner sans pression cette fois?
Je sentais pas mal de stress quand je suis rentr� sur la piste apr�s Florent [Amodio]. Je n�avais aucun appui � l��chauffement, je faisais les sauts mais je ne savais pas comment je les faisais. Apr�s, le stress, il faut qu�il soit l�. �a m�aide � me concentrer. C�est � partir du moment o� je tremble et que je vois double que je ne le g�re plus. �a, c��tait Vancouver (rires) . Mais l�, il �tait hors de question que j�abandonne.
Avez-vous eu l�impression de concourir � domicile?
C��tait super. D�s l��chauffement, le public a cri� tr�s fort quand ils ont annonc� mon nom. �a donne beaucoup d��nergie. Dans ces cas-l�, on n�a pas envie de le d�cevoir. J�avais presque l�impression d��tre en gala, et quand j�ai cette sensation-l�, c�est incroyable. Les Russes ont sans doute �t� tr�s d��us par le retrait de Plushenko , et je les remercie du fond du c�ur de m�avoir soutenu � fond.
Qu�esp�rez-vous pour demain?
D�j�, je trouve que je suis sorti du programme tr�s frais physiquement. C�est rassurant pour la suite. Mentalement, je me sens toujours mieux sur le programme libre, on a moins de contraintes. Il ne faudra pas se poser de questions et faire comme aujourd�hui.
Pas d�objectif de place?
Vous voulez dire la m�daille?�Non, non, il reste des gens costauds. Il faut que je reste lucide l�-dessus. Il faudrait vraiment que plusieurs de mes adversaires fassent de grosses erreurs, pendant que moi je fais un sans faute. Souvent, quand il faut autant de conditions, �a ne marche pas (rires).
Propos recueillis par Julien Laloye
