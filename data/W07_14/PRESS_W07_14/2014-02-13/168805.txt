TITRE: France Monde | Le constructeur automobile Mia Electric placé en redressement judiciaire - Le Bien Public
DATE: 2014-02-13
URL: http://www.bienpublic.com/actualite/2014/02/12/le-constructeur-automobile-mia-electric-place-en-redressement-judiciaire
PRINCIPAL: 168795
TEXT:
Le constructeur automobile Mia Electric placé en redressement judiciaire
France - économie Le constructeur automobile Mia Electric placé en redressement judiciaire
Notez cet article :
le 12/02/2014      à 17:26
337 véhiocules Mia ont été vendus en�??2012 (Deux-Sèvres)  AFP
Le constructeur automobile Mia Electric a été placé ce mercredi en redressement judiciaire par le tribunal de commerce de Niort, alors que les difficultés s�??accumulent, en dépit d�??un marché plus porteur et de l�??implication de la région Poitou-Charentes.
Partager
Envoyer à un ami
Mercredi matin, la société de quelque 200 salariés basée à Cerizay (Deux-Sèvres), a obtenu que ce redressement soit assorti d�??une longue période d�??observation de six mois, pendant laquelle Mia sera encore pilotée par l�??actuelle présidente Michelle Boos, mais avec un administrateur judiciaire.
«La mise en redressement judiciaire est la moins mauvaise solution et va nous permettre de mettre à plat les comptes et la stratégie de l�??entreprise», a réagi Ségolène Royal, présidente de la région, actionnaire à hauteur de 12% du capital de Mia, reprise en juin 2013 par un consortium d�??investisseurs, Focus Asia.
«On ne baisse pas les bras», a insisté Mme Royal. «La bataille industrielle pour la voiture électrique 100% française continue», a-t-elle ajouté, en soulignant que cela permettrait d�??exiger de Michelle Boss, «de tenir ses engagements sur les investissements» promis.
Mia Electric, qui appartenait jusqu�??en juin aux groupes allemands ConEnergy et Kohl, avait été crée en 2010 (bien 2010) pour y concentrer l�??activité de fabrication de voitures électriques de l�??équipementier Heuliez, également soutenu par la région et dont les difficultés ont finalement conduit à sa liquidation fin octobre 2013.
La production en série des Mia avait débuté, mais les objectifs de vente n�??ont pas été atteints. En 2012, 337 voitures ont été vendues, selon la société. Le chiffre de 2013 n�??était pas disponible.
Mme Boos l�??a reprise pour un montant jamais révélé. A l�??époque Focus Asia s�??était, selon elle, engagé à injecter 36 millions d�??euros dans l�??entreprise, de quoi la faire tourner jusqu�??à ce que les ventes décollent. En juillet, Mme Boss s�??était montrée très optimiste, assurant à l�??AFP avoir «deux à cinq véhicules commandés par jour», espérant en vendre «200 par mois» dès 2014, et atteindre la rentabilité cette année-là.
Mme Boos comptait réduire les coûts de 35% en renégociant les contrats avec les fournisseurs, vendre des services d�??ingéniérie et faire venir de nouveaux investisseurs.
Chaîne de montage à l�??arrêt
Mais, dès septembre, le paiement des salaires est devenu aléatoire. Les employés ont été payés en quatre fois pour leur salaire de septembre, en cinq pour octobre, et mi-décembre pour celui de novembre. Mardi soir, encore quatre salariés sur dix attendaient leur paye de janvier, selon une source syndicale.
Pourtant, sous l�??impulsion d�??entreprises et collectivités locales, le marché français s�??est envolé en 2013: +55% avec 8.779 unités vendues selon l�??Association nationale pour le développement de la mobilité électrique.
Selon syndicats et direction, le carnet de commandes comptait mi-janvier 274 voitures commandées et facturées, mais la situation s�??est tendue car les fournisseurs refusent de livrer les pièces nécessaires à la production, faute de paiement de leurs factures.
Selon le syndicaliste CFE-CGC, Christophe Klein, depuis la mi-décembre, aucune voiture n�??est sortie de la chaîne de montage. «On ne vend plus de voitures depuis deux mois», a-t-il déploré mercredi: «Je n�??ai jamais connu ça. C�??est très dur à vivre et très étrange» a-t-il ajouté, précisant n�??avoir aucune information précise sur les fonds réellement injectés dans l�??entreprise, ni même l�??origine des sommes ayant permis le versement des salaires.
Le syndicaliste a toutefois précisé que les salariés souhaitaient le maintien de la Franco-coréenne Michelle Boos à la tête de la société, espérant qu�??elle pourrait amener, comme promis, des commandes internationales.
Mi-janvier, Mme Boos avait déclaré qu�??en 2013, le chiffre d�??affaires avait atteint «8,5 millions d�??euros» grâce à des contrats de licence de commercialisation en Corée du Sud et au Mexique. Elle n�??était pas joignable mercredi après-midi.
«Nous sommes en attente d�??une action du gouvernement qui devrait agir plus vite et plus fort pour les véhicules électriques», a pour sa part souhaité Mme Royal, en assurant que des démarches étaient aussi en cours «pour trouver de nouveaux investisseurs».
AFP
