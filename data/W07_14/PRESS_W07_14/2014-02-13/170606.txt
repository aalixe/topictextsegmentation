TITRE: JO/Ski de fond: Kowalczyk brise la mal�diction sur le 10 km - LExpress.fr
DATE: 2014-02-13
URL: http://www.lexpress.fr/actualites/1/sport/jo-ski-de-fond-10-km-classique-kowalczyk-mate-les-scandinaves_1323590.html
PRINCIPAL: 170603
TEXT:
JO/Ski de fond: Kowalczyk brise la mal�diction sur le 10 km
Par AFP, publi� le
13/02/2014 � 12:23
, mis � jour � 17:45
Rosa Khoutor (Russie) - La Polonaise Justyna Kowalczyk, malgr� une blessure � un pied, a enfin r�ussi � briser la mal�diction qui la frappait en grands championnats sur le 10 km classique en remportant le titre olympique, jeudi aux JO de Sotchi.
La Polonaise Justyna Kowalczyk a domin� ses rivales scandinaves pour remporter le 10 km style classique des jeux Olympiques 2014 disput� sous des conditions printanni�res, jeudi
afp.com/Kirill Kudryavtsev
"J'ai pris beaucoup de m�dicaments mais surtout, quand je cours je ne ressens pas de douleur": il aura donc fallu � Kowalczyk une micro-fracture pour briser la mal�diction. �
Jamais championne du monde ou olympique du 10 km classique avant ce jour, Kowalczyk en est pourtant la reine absolue depuis plusieurs saisons.�
La Polonaise �tait ainsi arriv�e en Russie avec six victoires sur ce format de course sur les sept derni�res �preuves en Coupe du monde.�
Un doute subsistait toutefois, en raison d'une blessure contract�e le mois dernier, dont la championne olympique 2010 du 30 km ne voulait pas conna�tre l'exacte nature avant les JO pour ne pas perdre le moral. �
Doute renforc� apr�s son skiathlon d�cevant de samedi, o� elle n'avait pu prendre que la 6e place, chutant au moment du changement de ski.�
La Polonaise avait finalement d�cider de se faire examiner, et la radio pratiqu�e avait r�v�l� une micro-fracture au pied.�
Kowalczyk s'en �tait amus�e, publiant sur sa page Facebook la photo de la radio.�
Sur la piste du complexe Laura jeudi, o� la temp�rature de 13�C a permis � certaines concurrentes de participer � l'�preuve avec des combinaisons sans manches, il n'y a par contre pas eu besoin d'analyse pouss�e pour constater sa sup�riorit�.�
"C'est une superbe saison pour moi. C'�tait vraiment dur aujourd'hui avec le soleil. Mais tout le monde sait que je suis tr�s forte quand les conditions sont dures alors ce temps ensoleill�, c'�tait bon pour moi!", a souri la Polonaise qui devient l'athl�te la plus m�daill�e de son pays aux JO d'hiver avec cinq m�dailles en tout.�
Derri�re, la Su�doise Charlotte Kalla a pris la 2e place � 18 secondes, et la Norv�gienne Therese Johaug la 3e place � 28 secondes.�
L'autre grande favorite de la course, la Norv�gienne Marit Bjoergen, a craqu� dans les deux derniers kilom�tres pour finalement terminer 5e.�
Les Fran�aises ont r�alis� un tir group� en terminant � la 27e place pour Celia Aymonier et la 29e place pour Aurore Jean, toutes deux � plus de deux minutes de Kowalczyk.�
Par
