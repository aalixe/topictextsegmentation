TITRE: Ouverture dominicale : le camouflet du Conseil d'�tat � Michel Sapin
DATE: 2014-02-13
URL: http://www.boursorama.com/actualites/ouverture-dominicale--le-camouflet-du-conseil-d--tat-a-michel-sapin-bb61a8c913370d2e63f21ca7b653a2cf
PRINCIPAL: 169229
TEXT:
Ouverture dominicale : le camouflet du Conseil d'�tat � Michel Sapin
Le Figaro le 12/02/2014 � 22:34
Imprimer l'article
Saisi par des organisations syndicales, le juge des r�f�r�s du Conseil d'�tat a estim� qu'il y avait un doute s�rieux sur la l�galit� d'une d�rogation temporaire � la r�gle du repos dominical. Le gouvernement a annonc� qu'il pr�parait un nouveau d�cret.
�Le souci d'apaiser la situation relative aux �tablissements de bricolage dans la r�gion �le-de-France�, le temps d'�dicter une loi, n'a pas convaincu le Conseil d'�tat. Saisi par la CGT, FO et SUD, son juge des r�f�r�s a suspendu mercredi l'ex�cution du d�cret autorisant l'ouverture des magasins de bricolage le dimanche, sign� par le ministre du Travail le 30�d�cembre.
Suivant les recommandations du rapport Bailly, remis quelques semaines plus t�t, le gouvernement a promis de remettre � plat le maquis r�glementaire sur le travail dominical. Un vaste chantier qui risque de ne pas �tre op�rationnel avant l'�t� 2015.
En attendant, l'ex�cutif a donc accord� une d�rogation temporaire au secteur du bric...
