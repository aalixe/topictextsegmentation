TITRE: Une s�rie d'alertes au colis pi�g� inqui�te l'Angleterre
DATE: 2014-02-13
URL: http://www.boursorama.com/actualites/une-serie-d-alertes-au-colis-piege-inquiete-l-angleterre-326a38f8d76c3744d85f497d11af7bad
PRINCIPAL: 171665
TEXT:
Une s�rie d'alertes au colis pi�g� inqui�te l'Angleterre
Le Figaro le 13/02/2014 � 17:29
Imprimer l'article
La police antiterroriste britannique a indiqu� jeudi enqu�ter apr�s la d�couverte de colis suspects dans plusieurs bureaux de l'arm�e de plusieurs villes du Royaume-Uni. Le premier ministre David Cameron a convoqu� une r�union d'urgence.
L'arm�e britanique serait-elle la cible d'une campagne de d�stabilisation? La police antiterroriste du Royaume-Uni a lanc� une enqu�te apr�s la d�couverte depuis trois jours de plusieurs colis suspects adress�s � des bureaux de recrutement de l'arm�e. Sept villes anglaises ont d�j� �t� vis�es, dont Brighton (sud-est), Oxford (centre), Slough (ouest de Londres) et Canterbury (sud-est), rien que sur la journ�e de jeudi. A Slough, c'est tout le centre commercial o� se trouvait le bureau de l'arm�e qui a d� �tre �vacu�. A Brighton, plusieurs rues ont �galement �t� boucl�es. Les services de d�minage, accompagn�s de robots adapt�s, ont �t� d�p�ch�s sur place.
Lire la suite de l'article sur lefigaro.fr
Copyright � 2013
