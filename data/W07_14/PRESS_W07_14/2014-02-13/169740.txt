TITRE: L'Afghanistan lib�re 65 talibans pr�sum�s, Washington proteste | Mediapart
DATE: 2014-02-13
URL: http://www.mediapart.fr/journal/international/130214/lafghanistan-libere-65-talibans-presumes-washington-proteste
PRINCIPAL: 169735
TEXT:
La lecture des articles est r�serv�e aux abonn�s.
Les autorit�s afghanes ont lib�r� ce jeudi 65 combattants talibans pr�sum�s de la prison militaire de Bagram, situ�e dans la banlieue de Kaboul, d�cision qualifi�e de ��profond�ment regrettable�� par les �tats-Unis, qui les consid�rent comme une menace pour la s�curit� du pays.
��Le gouvernement afghan devra assumer les cons�quences de cette d�cision. (�) Nous exhortons le gouvernement � prendre toutes les mesures possibles pour s'assurer que les personnes lib�r�es ne commettent pas de nouveaux actes de violence ou de terreur��, selon un communiqu� de l'ambassade am�ricaine � Kaboul.
��Les 65 prisonniers ont �t� lib�r�s et ont quitt� l'enceinte ...
