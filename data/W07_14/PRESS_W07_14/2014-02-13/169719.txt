TITRE: Christan Estrosi "favorable � un r�f�rendum sur l'immigration en France" | Derni�re minute | Corse-Matin
DATE: 2014-02-13
URL: http://www.corsematin.com/article/derniere-minute/christan-estrosi-favorable-a-un-referendum-sur-limmigration-en-france.1280314.html
PRINCIPAL: 169718
TEXT:
Tweet
Le d�put�-maire UMP de Nice a d�clar� mercredi �tre "favorable" � un r�f�rendum sur l'immigration en France, similaire au vote�qui s'est tenu en Suisse dimanche dernier.
Christian Estrosi, en campagne pour sa r��lection � la mairie de Nice, a propos� sur France 2 un r�f�rendum qui se tiendrait le 25 mai, le jour des �lections europ�ennes, puisque "probl�me est d'abord europ�en avec les r�gles de Schengen qui sont tr�s d�favorables � la France".�
Il r�agissait � la "votation" suisse de dimanche, dans laquelle les �lecteurs suisses ont approuv� � 50,3% une limitation de l'immigration.
Le m�me jour que Marine le Pen
Dans une interview aux� Echos ,�la pr�sidente du FN Marine le Pen a eu des propos similaires en y ajoutant la question de la "priorit� nationale". Elle invoque le�"droit des peuples"�� ma�triser leurs fronti�res et appelle "les Fran�ais � r�clamer un r�f�rendum sur le sujet". Une p�tition est lanc�e sur le site Internet du Front National.
La col�re du PS
Dans un communiqu�, le Parti Socialiste souligne la ressenblance entre un cadre UMP et la pr�sidente du FN . "On connaissait la tendance de l'UMP � courir derri�re le Front national. La nouveaut� c'est, qu'aujourd'hui, l'UMP pr�c�de le FN sur le terrain de l'extr�me droite"�a ainsi �crit Eduardo Rihan Cypel, d�put� PS de Seine-et-Marne.�
Tweet
Tous droits de reproduction et de repr�sentation r�serv�s. � 2012 Agence France-Presse.
Toutes les informations (texte, photo, vid�o, infographie fixe ou anim�e, contenu sonore ou multim�dia) reproduites dans cette rubrique (ou sur cette page selon le cas) sont prot�g�es par la l�gislation en vigueur sur les droits de propri�t� intellectuelle. �Par cons�quent, toute reproduction, repr�sentation, modification, traduction, exploitation commerciale ou r�utilisation de quelque mani�re que ce soit est interdite sans l�accord pr�alable �crit de l�AFP, � l�exception de l�usage non commercial personnel. �L�AFP ne pourra �tre tenue pour responsable des retards, erreurs, omissions qui ne peuvent �tre exclus dans le domaine des informations de presse, ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
AFP et son logo sont des marques d�pos�es.
J.D. avec AFP
Derni�re minute
Le d�lai de 15 jours au-del� duquel il n'est plus possible de contribuer � l'article est d�sormais atteint.
Vous pouvez poursuivre votre discussion dans les forums de discussion du site. Si aucun d�bat ne correspond � votre discussion, vous pouvez solliciter le mod�rateur pour la cr�ation d'un nouveau forum : moderateur@nicematin.com
Vos derniers commentaires
diotseu2b
13/02/2014 � 23h32
@tekichou, tu pourras tjrs virer une personne avec ou sans papiers, mais jamais tu ne le  pourras pour des familles enti�res. l�HISTOIRE l�a d�montr� au fil des si�cles, et cela s�appelle de la substitution de peuplement ! Je le vois, ici o� je suis, et nous en reparlerons lors de mon retour, avec un journaliste de CM, que tout le monde conna�t. Qu�est-ce une substitution de peuplement ? Elle commence par la perte de SA LANGUE dans l�usage courant, puis, dans ses traditions ses us et coutumes. OUI !je suis un vieux c.., mais pas encore aveugle. Toutes ces valeurs se perdent en moins de temps qu�il me le faut pour l��crire. Toutes les places d�ing�nieurs, techniciens de hautniveau, et , surtout dans le plan m�dical, sont remplac�es par des immigrants, venus de toutes les r�gions de l�Europe. Il serait bien trop long et fastidieux de d�velopper ce sujet ici ! Merci de m�avoir lu !
tekichou
13/02/2014 � 23h12
Nous, en corse,on aura jamais ce probleme de referendum,car nos elites de la C.T.C. ont vot� ..CORSE TERRE D ASILE POUR LES SANS PAPIERS.
diotseu2b
13/02/2014 � 21h15
Pour des raisons pro�, je travaille dans un pays o� l��migration est tr�s forte. Mais, cette �migration, est la force vive d�une nation. En Europe, les grandes Cies se foutent royalement de ce crit�re, pourvu qu�il rapporte! Qui dit �migration, enclanche automatiquement :immigration, de familles enti�res, parfois non assimilables. D�o� les graves probl�mes que les pays d�Europe rencontrent. Et ce n�est que le d�but ! Il ne faut surtout pas prendre la Suisse comme exemple, petit cailloux de riches banquiers, sur la surface, car Elle a aussi ses p�vres...mais Elle les c�che !L�Europe n�a pas encore bien saisi les suites de la chute du Mur de Berlin, la fin du bloc de l�Est....Bient�t, qu�on le veuille ou non, l�Ukraine entrera dans  l`Europe et, bien des cartes seront redistribu�es. Petite Corse ch�rie, tu auras un �tre tourisme !
tizzone
13/02/2014 � 16h37 | 2
j'aime bien ces hommes politique a cervelle de poisson rouge...... quand l'ump a fait un r�f�rendum, la r�ponse du peuple a �t� "non" a 55% (a comparer aux 50,4% suisse), et son parti s'est assis sur la volont� populaire...... qu'il ait donc la d�cence de fermer sa gueule......
Centurion
13/02/2014 � 16h01
On s'en fout d'o� vient l'initiative du r�f�rendum m�me si �a vient des communistes . Il faut que les politiques sachent ce que pensent r�ellement les fran�ais � ce sujet ! : allez sur ce site d�j� : http://www.france-petitions.com/
chamborant
13/02/2014 � 12h49
Qu'il se rassure,il n'est pas le seul mais c'est un fond de commerce bien utile dont les socialistes se servent ,aux d�pends des autres �lecteurs, pour imposer leur " mod�le " .
