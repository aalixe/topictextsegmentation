TITRE: Drogues: Le journal intime de Philip Seymour Hoffman -   People - lematin.ch
DATE: 2014-02-13
URL: http://www.lematin.ch/people/Le-journal-intime-de-Philip-Seymour-Hoffman/story/12312694
PRINCIPAL: 169626
TEXT:
Le journal intime de Philip Seymour Hoffman
Drogues
�
Philip Seymour Hoffman d�taille avec attention sa bataille contre l�addiction. Celle-ci est racont�e dans deux journaux intimes retrouv�s par la police.
Par LeMatin.ch / Cover Media. Mis � jour le 12.02.2014
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
E-Mail*
Veuillez SVP entrez une adresse e-mail valide
Philip Seymour Hoffman expurgeait ses d�mons dans l��criture. L�acteur oscaris� est mort il y a une dizaine de jours � New York � l��ge de 46 ans, vraisemblablement d�une overdose d�h�ro�ne.
Selon NBC, la police aurait retrouv� chez lui deux journaux intimes. Philip Seymour Hoffman y racontait notamment qu�il allait aux Narcotiques Anonymes pour essayer de rester clean. Les textes les plus r�cents datent du moment o� l�acteur est entr� en rehab en mai dernier. Selon un proche du dossier, les textes sont parfois incoh�rents.
�C�est un courant de consciences et c�est parfois difficile � suivre. Dans une ligne, il se r�f�re � �Frank qui a toujours de l�argent� et sur la m�me page il �crit � propos d�une jeune fille de 15 ans originaire du Texas�, pr�cise une source. �On dirait qu�il a fait la derni�re partie en rehab. �a contient ces �l�ments d�introspection. Mais il y a aussi pas mal de divagations qui n�ont pas de sens�, ajoute une autre source.
Les fun�railles de Philip Seymour Hoffman ont eu lieu la semaine derni�re en l��glise St Ignatius de Loyola dans l�Upper East Side de Manhattan. Pas moins de 400 personnes avaient fait le d�placement, dont Cate Blanchett, Michelle Williams, Laura Linney, Julianne Moore et Amy Adams. Mimi O�Donnel sa compagne pendant 15 ans, ainsi que leurs trois enfants Colin, 11 ans, Tallulah, 7 ans et Willa, 5 ans, �taient �galement pr�sents.
Philip Seymour Hoffman �tait rest� clean pendant 23 ans avant d�entrer en cure l�ann�e derni�re apr�s une rechute. On ne sait pas quand exactement il a recommenc� � prendre des drogues, mais un t�moin a confirm� qu�il s��tait rendu � une r�union des alcooliques anonymes le 26 janvier dernier.
Plusieurs personnes ont �t� arr�t�es en rapport avec le d�c�s de Philip Seymour Hoffman. Robert Vineberg a plaid� non coupable de possession d�h�ro�ne avec intention de vendre, tout comme Max Rosenblum et Juliana Luchkiw pour des accusations de possession de coca�ne. Ces trois personnes avaient �t� arr�t�es apr�s la perquisition de leurs domiciles au lendemain du d�c�s de l�acteur. (Le Matin)
Cr��: 12.02.2014, 10h31
