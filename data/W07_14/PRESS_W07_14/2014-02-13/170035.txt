TITRE: Mort Paul Walker : D�couvrez la bande-annonce de son dernier film Brick Mansions
DATE: 2014-02-13
URL: http://www.staragora.com/news/mort-paul-walker-decouvrez-la-bande-annonce-de-son-dernier-film-brick-mansions/479174
PRINCIPAL: 170033
TEXT:
0
D�couvrez la bande-annonce de l'un des derniers films de Paul Walker, qui sort au cin�ma le 23 avril prochain�! Le film "Brick mansions" est riche en surprises. Staragora vous raconte tout�!
Voil� qui va combler les fans de Paul Walker , mort brutalement le 30 novembre 2013�! La sortie de l'un de ses derniers films, Brick Mansions, est pr�vue pour le 23 d'avril 2014. Ce film d'action franco-canadien, produit et sc�naris� par Luc Besson est inspir� du film fran�ais Banlieue 13, r�alis� par Pierre Morel en 2004.
Un casting de qualit�!
Brick Mansions accueille d'ailleurs dans son casting de choix, le h�ros du film fran�ais David Belle, qui est aussi le cr�ateur de cet art particulier du d�placement en zone urbaine, appel� le Parkour. On retrouve aussi dans Brick Mansions le rappeur am�ricain RZA du Wu Tang Clan. L'actrice Michelle Rodriguez , d�j� partenaire de Paul Walker dans la c�l�bre saga dont il �tait le h�ros, Fast and Furious , s'est aussi jointe au casting de Brick Mansions et ce, pour notre plus grand plaisir�!
Un sc�nario palpitant�!
Encore dans la peau d'un inspecteur, Paul Walker a une fois de plus jou� ce r�le de justicier qui n'a peur de rien et qui lui allait si bien. Dans Brick Mansions, il est charg� de d�manteler un trafic d'armes de destruction massive qui s'est install� clandestinement dans un ghetto dangereux de Chicago, appel� Brick Mansions. Pour ce faire, il devra faire appel � un certain Lino, qui connait le quartier comme sa poche. Ensemble ils devront donc affronter le gang  Tremaine.
Paul Walker dans Brick Mansions
� Facebook
Paul Walker � pied
Pour ce film, Paul Walker habitu� � tourner dans de gros bolides, ne t�te que peu le volant et passe son temps � escalader, grimper et sauter, les �difices et les obstacles qui sont sur son chemin. C'est dans un univers diff�rent de celui auquel �tait accoutum� le com�dien, roi des bolides dans Fast & Furious, �volue d�sormais.
Paul Walker, le revenant�!
