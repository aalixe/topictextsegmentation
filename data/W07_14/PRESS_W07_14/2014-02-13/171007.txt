TITRE: Bercy s'oppose à la nomination d'une ex-UBS à l'AMF - 13 février 2014 - Challenges
DATE: 2014-02-13
URL: http://www.challenges.fr/economie/20140213.CHA0491/bercy-bloque-la-nomination-d-une-ex-ubs-a-l-amf.html
PRINCIPAL: 171006
TEXT:
Publié le 13-02-2014 à 18h06
Mis à jour à 19h10
Pierre�Moscovici�veut qu'une ancienne dirigeante de la banque UBS, dont la nomination � l'Autorit� des march�s financiers fait des vagues, renonce � ce poste.�
Le siège de l'AMF Sipa
Le ministre de l'Economie Pierre Moscovici veut qu'une ancienne dirigeante de la banque UBS, dont la nomination � l' Autorit� des march�s financiers (AMF) fait des vagues, renonce � ce poste, a indiqu� jeudi 13 f�vrier � l'AFP une source proche du minist�re, faisant �tat d'un s�v�re recadrage � Bercy.
Pierre�Moscovici, "tr�s m�content" de cette affaire selon cette source, "souhaite que [Fran�oise Bonfante] renonce" � sa nomination au sein de la Commission des sanctions de l'AMF, officialis�e sous son nom au Journal officiel le 20 d�cembre.
"Il ne s'agit pas de remettre en cause ses qualit�s" ou sa probit�, mais cette nomination, alors qu'UBS France est au coeur d'un vaste scandale d'�vasion fiscale pr�sum�e, "est un mauvais signal", a indiqu� la m�me source.
C'est � l'administration de Bercy qu'il revient d'examiner les parcours des candidats � la Commission des sanctions, l'organe de l'AMF charg� d'instruire les affaires de fraude dans le secteur financier et de d�cider des sanctions, avant que le choix ne soit officialis� par le ministre.
"Tout le monde est pass� au travers", au grand m�contentement de Pierre�Moscovici, selon cette source.
Fran�oise�Bonfante �tait auparavant responsable du contr�le des risques et de la conformit� � la banque UBS France.
LIRE : Une ex-UBS nomm�e � l'AMF : l'intrigant choix de Bercy
Circulez, il n'y a rien � voir...
Selon Bercy, le pr�sident de l'AMF G�rard Rameix a affirm� de son c�t� "qu'il n'y avait pas d'�l�ments de nature � remettre en cause la r�gularit� de cette nomination, ni perturber le bon fonctionnement" de la commission des sanctions.
Par ailleurs, le minist�re a fait valoir que Fran�oise�Bonfante, qui a dans le pass� travaill� � la COB, anc�tre de l'AMF, n'�tait pas � UBS France en charge des activit�s aujourd'hui dans le viseur de la justice.
Jusqu'en mai 2010, Fran�oise�Bonfante n'�tait officiellement charg�e de veiller au respect des lois et r�gles de d�ontologie qu'au sein de l'activit� de banque d'investissement d'UBS en France, et non de l'activit� de banque priv�e , celle au coeur du scandale d'�vasion fiscale. Or, c'est avant cette date que se sont d�roul�s les faits vis�s par la justice. Cependant Fran�oise Bonfante supervisait�officieusement l'ensemble du contr�le des risques d'UBS France depuis mi-2008.�
Cette nomination pose question, alors que l'Autorit� de contr�le prudentiel, autorit� de surveillance du secteur bancaire, a impos� en juin 2013 une amende record de 10 millions d'euros � UBS France pour "laxisme" dans le contr�le de pratiques commerciales pouvant relever du blanchiment de fraude fiscale .
Par ailleurs, UBS France et sa maison m�re suisse sont sous le coup d'une mise en examen pour d�marchage illicite. Elles sont soup�onn�es d'avoir incit� de riches clients fran�ais � ouvrir des comptes en Suisse.
(Avec AFP)
