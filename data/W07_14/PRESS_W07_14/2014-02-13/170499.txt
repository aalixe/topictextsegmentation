TITRE: Etats-Unis: Washington sous une temp�te de neige meurtri�re - BFMTV.com
DATE: 2014-02-13
URL: http://www.bfmtv.com/international/etats-unis-washington-une-tempete-neige-meurtriere-710054.html
PRINCIPAL: 170495
TEXT:
La temp�te a atteint Washington, apr�s avoir fait une dizaine de morts, bloqu� la circulation sur les routes verglac�es et priv� d'�lectricit� pr�s d'un demi-million de foyers et d'entreprises.
Plus de 3.700 vols pr�vus jeudi ont �t� annul�s dans de nombreux a�roports du pays.
���
Le National Weather Service (NWS) met en garde depuis plusieurs jours contre un "d�me gigantesque" de courants d'air froid venus de l'Arctique, qui devraient s'installer sur la partie est des Etats-Unis, et d�clencher une "temp�te glaciale" susceptible de "paralyser" de nombreux Etats, depuis la G�orgie et la Caroline du Sud jusqu'aux r�gions septentrionales.
Un niveau de glace "historique"
"Les accumulations de glace seront incroyables, si ce n'est  historiques", a soulign� le NWS, ajoutant que jusqu'� 30 cm de neige pourraient tomber sur les Etats de La Nouvelle-Angleterre jeudi.
�� �
Le  pr�sident Barack Obama a d�clar� mercredi l'�tat d'urgence dans 45  comt�s de G�orgie et en Caroline du Sud, deux Etats du sud-est des  Etats-Unis; sa d�cision permet aux services f�d�raux charg�s des  situations d'urgence d'y op�rer.
Selon un d�compte �tabli par CNN, la temp�te a fait au moins 10 morts, la plupart dans des accidents de la route.
DIAPORAMA
