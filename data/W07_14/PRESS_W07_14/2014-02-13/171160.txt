TITRE: Sotchi 2014: Forfait, Plushenko en avait plein le dos - 20minutes.fr
DATE: 2014-02-13
URL: http://www.20minutes.fr/sport/1298562-20140213-sotchi-2014-plushenko-plein-dos
PRINCIPAL: 0
TEXT:
Twitter
Evgeny Plushenko, le 13 f�vrier 2014 � Sotchi. JUNG YEON-JE / AFP
JEUX OLYMPIQUES � Trop diminu� par des douleurs vert�brales, la star du patinage russe termine sa carri�re sur un abandon...
De notre envoy� sp�cial �Sotchi,
Une bonne demi-heure que le public de l�Iceberg criait son nom � tue-t�te. Evgeny Plushenko, le tsar des tsars , faisait des �tirements suspects en attendant son tour. A son entr�e sur la glace, il n�essaie m�me pas. Le dos, op�r� il y a un an tout juste � un disque vert�bral douloureux- a l�ch� de nouveau. Le blondinet le plus connu de Russie annonce qu�il ne participera pas � l��preuve individuelle et s�en va dans un silence incongru.
�Ce n�est pas une trag�die. Ne retenez pas ce jour de lui. J�ai travaill� pendant 20 ans avec lui, et cela a �t� 20 ans de succ�s. Ne r�duisez pas sa carri�re � cet instant�, plaide Alexei Mishin, son coach de toujours. Champion olympique � Turin, vice-champion olympique � Salt Lake City et � Vancouver, Plushenko a voulu �tirer sa carri�re jusqu�� Sotchi . Presque aussi populaire et m�galo que Poutine � son programme s�intitulait humblement��The Best of Plushenko�-, le triple champion du monde n�aurait jamais d� participer � ces JO d�hiver.��J�avais des douleurs partout, c�est ma famille et mes proches qui m�ont convaincu de tenter ma chance�, d�clarait-il dans une interview � L�Equipe jeudi matin.
Non qualifi�, il est finalement rep�ch� par un comit� d�experts lors d�un test � huis clos� trois semaines avant les Jeux, prenant la place du jeune Kovtun. Mais Plushenko n�avait qu�un programme dans les jambes, superbement effectu� d�ailleurs, samedi dernier lors de l��preuve par �quipe.��Soyez gentils, ne le critiquez pas trop, retenez le positif d�une carri�re si riche , pas le n�gatif de ce soir�, insiste une derni�re fois Mishin, devant une nu�e de journalistes russes au bord des larmes. Jeudi�13, c�est jour de deuil national en Russie.
Julien Laloye
