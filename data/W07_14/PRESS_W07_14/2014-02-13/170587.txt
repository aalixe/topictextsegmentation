TITRE: La Warner officialise le Tarzan de David Yates - L'actu cin�ma � TOUTLECINE.COM
DATE: 2014-02-13
URL: http://www.toutlecine.com/cinema/l-actu-cinema/0002/00028404-la-warner-officialise-le-tarzan-de-david-yates.html
PRINCIPAL: 170584
TEXT:
�
La Warner officialise le Tarzan de David Yates
Warner Bros officialise le lancement d'une nouvelle adaptation des aventures de Tarzan, que dirigera David Yates . Alexander Skarsg�rd , Samuel L. Jackson, Margot Robbie, et Christoph Waltz s'y donneront la r�plique.
�
Alors que les pr�paratifs de cette adaptation du personnage mythique cr�� par Edgar Rice Burroughs viennent de commencer, les studios Warner annonce que le film sortira aux Etats-Unis le 1er juillet 2016. Il s'agira d'un film 3D en prises de vue r�elles, dans lequel Alexander Skarsg�rd campera le h�ros, qui a grandi dans la jungle depuis son plus jeune �ge, avant de revenir � Londres, une autre jungle, mais urbaine cette fois. Margot Robbie se glissera dans la peau de Jane Porter, dont s'�prend Tarzan.
�
�L'aventure de cet homme, pris en �tau entre deux mondes, captive lecteurs et spectateurs de tous �ges. Autant dire que nous sommes heureux de proposer une nouvelle adaptation du livre de Burroughs � la g�n�ration actuelle�, a d�clar� son producteur Jerry Weintraub, qui souligne que �nous allons mettre � profit le nec plus ultra des technologies actuelles pour tourner ce film d'aventures�.
Par La r�daction (12/02/2014 � 12h56)
