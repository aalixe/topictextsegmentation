TITRE: Havas : affiche une croissance organique de 1% en 2013
DATE: 2014-02-13
URL: http://www.boursier.com/actions/actualites/news/havas-croissance-organique-de-1-en-2013-566476.html
PRINCIPAL: 171675
TEXT:
Mon compte Je suis d�j� client
D�couvrir Je souhaite recevoir une documentation gratuite
Havas : affiche une croissance organique de 1% en 2013
Abonnez-vous pour
moins de 1� par jour !
Le 13/02/2014 � 19h29
(Boursier.com) � Le revenu consolid� d' Havas s'�l�ve � 1.772 ME sur l'ann�e 2013 et ressort � 514 ME pour le quatri�me trimestre. La croissance organique est de +1% sur l'ensemble de l'ann�e 2013 et de +1,6% pour le 4�me trimestre 2013. En donn�es brutes la croissance pour l'ann�e 2013 est en retrait de 1,1% d� principalement aux effets de change qui ont p�nalis� l'�volution du revenu � hauteur de 51 ME. A taux de change constant, la croissance s'�l�ve � +1,8%.
La part du revenu du groupe dans les activit�s digitales et m�dias sociaux reste importante gr�ce � la poursuite du d�ploiement de la strat�gie d'int�gration de ces m�tiers au coeur de toute activit� et de toutes les agences, partout dans le monde. Ainsi, sans acquisition significative, les activit�s num�riques et m�dias sociaux repr�sentent 26% du revenu global du Groupe en 2013...
Christophe Voisin � �2014, Boursier.com
