TITRE: Canon PowerShot G1 X Mark II, le retour du grand capteur � zoom
DATE: 2014-02-13
URL: http://www.lesnumeriques.com/appareil-photo-numerique/canon-powershot-g1-x-mark-ii-retour-grand-capteur-a-zoom-n33160.html
PRINCIPAL: 0
TEXT:
Nous soutenir, navigation sans publicit� Premium : 2�/mois + Conseil personnalis� Premium+ : 60�/an
Canon PowerShot G1 X Mark II, le retour du grand capteur � zoom
Tout nouveau, tout chang�, sauf le capteur
�
Publi� le: 12 f�vrier 2014 11:09
Par Bruno Labarbere
Tweet
Si certains se demandent qui veut la peau de Roger Rabbit, nous attendons impatiemment celui qui osera damer le pion au RX100 Mark II au sommet des compacts experts � zoom et grands capteurs. Ce ne sera clairement pas pour ce coup-ci, car le Canon G1X Mark II, s'il a subi une cure d'amaigrissement par rapport � son pr�d�cesseur, flirte plut�t avec la cat�gorie des hybrides � objectifs fixes et capteurs APS-C, avec de nouvelles armes qui ne manquent pas de charme.
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Envoyez-moi un mail lorsque le prix descend sous :
�
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Canon Ixus 240 HS
�
"Comment �a il est trop gros mon G1X �?" Pour r�pondre � la critique gauloise (mais pas que), les ing�nieurs ont proc�d� � un rabotage extr�me�: hop, disparu le viseur �triqu� et avec lui pr�s de 10�mm en hauteur. La ligne est plus �pur�e, raffin�e, les molettes moins pro�minentes, la poign�e perd sa m�choire carr�e pour mieux �pouser le contour de la main. Bref, l'ensemble est plus �l�gant, faisant de ce G1X MkII le plus rac� des experts G de Canon. Les amateurs de bo�tiers � l'allure solide appr�cieront, au d�triment d'une masse qui d�passe all�grement les 500 grammes (presque autant qu'un Leica M en laiton�!).
Nouveau bo�tier, processeur Digic 6, nouveau zoom 24-120mm, nouvelle ergonomie...
�
Notez la petite �tiquette qui indique fi�rement la pr�sence de Wi-Fi et du NFC, pour, entre autres, piloter l'appareil � distance
Dans ce G1X Mark II, tout a chang� ou presque. Seul le grand capteur BSI CMOS 14�Mpx de 18,7�x�14�mm (125�% plus grand que le 1" du RX100 Mk II mais seulement 20�% plus petit qu'un APS-C Canon) a �t� conserv�, avec une petite subtilit�. Avant, le passage d'un ratio d'image 4:3 � 3:2 faisait dispara�tre 1,5�million de pixels dans la nature. D�sormais, "seulement" 400�000�px sont perdus. Optimisation du cercle de couverture image�? Peut-�tre, mais les photos en 4:3 ne comptent d�sormais plus que 13,2 millions de pixels contre 14,2 auparavant... Qui a dit que ��ce n'est pas qu'elles se d�gonflent, c'est qu'elles font "pschitt���?
Le processeur Digic 6 entre dans la danse, accompagn� d'un syst�me AF � 31 collimateurs et d'un tout nouvel objectif qui n'a que des bons c�t�s. L'amplitude de zoom augmente (5x contre 4x), la position grand-angle est plus large (24�mm contre 28�mm), le t�l�objectif est plus long (120�mm contre 112�mm), les distances macro sont r�duites (5�cm au 20�mm et 20�cm au 120�mm) et, encore mieux, la luminosit� a �t� am�lior�e�: f/2-3,9 contre f/2,8-5,8�! Outre les profonds changements dans la formule optique, l'ergonomie de l'objectif a �galement �volu� puisqu'il comporte d�sormais deux bagues. La plus proche du bo�tier, qui est aussi la plus grosse, est crant�e, la plus proche de l'extr�mit� de l'objectif est souple. Aucune des deux ne dispose de but�e. Ces deux bagues, en conjonction avec la roue arri�re, permettent de piloter le G1X du bout des doigts et sont enti�rement programmables, avec au besoin des fonctions diff�rentes selon le mode de vue adopt�. Nous regretterons l'absence d'une ultime molette � l'avant, au niveau de l'index droit, qui aurait permis une manipulation d'une seule main plus ais�e et intuitive, mais, de toute mani�re, le poids de la b�te incite � un maintien � deux mains.
Retournons ce gros b�b� pour d�couvrir l'excellent �cran multitouch de 7,7�cm et 1�040�000�px de l' EOS 70D . Il est mont� sur un m�canisme � double charni�re permettant une inclinaison � 45� vers le bas et 180� vers le haut � id�al � la fois pour les autoportraits et pour se muscler le bras. Si l'intention est louable, le m�canisme est difficile � manier. Surtout, la charni�re qui relie le bras primaire et l'�cran nous semble extr�mement fragile et laisse un immense jour qui met � nu la nappe de connexion de l'�cran. Attention aux mauvaises manipulations�! Du coup, le mieux que nous puissions conseiller est de ne pas ouvrir au maximum l'�cran, mais c'est quand m�me un peu frustrant. Pour les amateurs de cadrages acrobatiques, Canon proposera �galement un viseur �lectronique optionnel de 2,36 millions de points qui reprend la c�l�bre dalle Epson bien connue.
Le G1X Mark II sera disponible au mois de mai, � 799�� nu, 999�� en kit avec le viseur. Ce dernier pourra �tre acquis ult�rieurement et s�par�ment, mais comptez 349��.
Nos premiers essais
Nous avons eu l'opportunit� de prendre longuement en main le G1X Mark II et, globalement, avons appr�ci� les progr�s depuis la version pr�c�dente. Il est d�sormais plus facile � manier, plus intuitif et, surtout, bien plus r�actif. Si le G1X premier du nom avait tendance � se tra�ner (le poids des lentilles, le choc de photos), le nouveau venu, alors qu'il n'est qu'� l'�tat de prototype, nous a vraiment fait plaisir. Sans divulguer de chiffres pr�cis, nous avons relev� une r�activit� globale deux fois plus rapide que celle du G1X. Il se situe plut�t bien face � la concurrence�: l'autofocus est plus vif que celui du X100s , il talonne le RX100 Mark II et distance le Leica X Vario, qui est le seul autre hybride expert � grand capteur et zoom. La rafale, elle, promise � 5 i/s en JPG, est une formalit� qui en plus est acquitt�e sur un nombre illimit� de vues. Vivement le mod�le d�finitif�!
En ce qui concerne la qualit� d'image, "notre" G1X Mark II est trop loin du produit final pour se prononcer. Toujours est-il qu'elle ne recule pas par rapport au G1X (encore heureux) mais que nous sentons tr�s rapidement que le Digic 6 et le 24-120�mm n'auraient pas rechign� � s'exprimer sur un capteur plus pixelis�... En attendant, nous nous sommes amus�s comme des petits fous avec la fonction "Creative Shot" apparue sur le PowerShot N , intelligemment remani�e pour laisser le choix entre quatre types familles de filtres (Retro, Special, Monochrome et Naturel).
Rendez-vous au d�but de l'�t� pour notre test complet...
�
