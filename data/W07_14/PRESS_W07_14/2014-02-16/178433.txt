TITRE: JO de Sotchi : le point à 13h
DATE: 2014-02-16
URL: http://www.lefigaro.fr/flash-actu/2014/02/16/97001-20140216FILWWW00067-jo-de-sotchi-le-point-a-13h.php
PRINCIPAL: 178431
TEXT:
le 16/02/2014 à 13:02
Publicité
Suivez avec Le Figaro.fr la 9e journée de compétition . La matinée a été marquée par deux nouvelles médailles dans le camp français.
Ski de fond : les Bleus en bronze
Les Français, quatrièmes à Vancouver, décrochent une superbe médaille de bronze dans le 4X10 km ! Jamais un relais français n'avait réalisé un tel exploit en ski de fond. Ils terminent à 31''9 des Suédois (or) et à 4''6 des Russes (argent). Bravo à Jean Marc Gaillard, Maurice Manificat, Robin Duvillard et Ivan Perrillat-Boiteux qui apporte une sixième breloque à la délégation bleue.
» Lire l'article
Chloé Trespeuch, un surf en bronze
La snowboardeuse de Bourg-Saint-Maurice décroche une spectaculaire médaille de bronze dans l'épreuve de snowboard cross. Une 5e médaille bleue à Sotchi qui prolonge la belle histoire française dans la discipline.
» Lire l'article
Super G : La grosse désillusion d'Adrien Théaux
Le Norvégien Kjetil Jansrud est champion olympique avec un temps de 1'18''14. Il succède à son compatriote Aksel Lund Svindal (7e). Auteur de la course de sa vie, l'Américain Andrew Weibrecht prend la médaille d'argent (+0''30). Le Canadien Jan Hudec et l'Américain Bode Miller, ex-æquo (+''53), s'emparent du bronze.
Meilleur Français de ce super-G, Adrien Théaux termine finalement 11e (+1''21) après que le Tchèque Ondrej Bank, dossard 34, a pris la 9e place. Tir groupé de Thomas Mermillod-Blondin, 15e (+1''39), David Poisson (17e, +1''60) et Johan Clarey (19e, +1''61).
