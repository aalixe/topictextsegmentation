TITRE: Montreuil : un mort et un blessé grave dans un incendie | Site mobile Le Point
DATE: 2014-02-16
URL: http://www.lepoint.fr/societe/montreuil-un-mort-et-un-blesse-grave-dans-un-incendie-16-02-2014-1792282_23.php
PRINCIPAL: 0
TEXT:
16/02/14 à 16h03
Montreuil : un mort et un blessé grave dans un incendie
Un homme de 21 ans est décédé dimanche matin et une autre personne a été grièvement blessée lors d'un incendie dans un immeuble de bureaux et d'appartements.
Une personne est morte et une autre a été grièvement blessée dans l'incendie d'un immeuble dimanche à Montreuil.
Source AFP
Un homme de 21 ans est décédé dimanche matin et une autre personne a été grièvement blessée lors d'un incendie dans un immeuble de bureaux et d'appartements à Montreuil (Seine-Saint-Denis), a-t-on appris de plusieurs sources.
Selon le commandant des pompiers Gabriel Plus, le sinistre s'est déclaré peu avant 8 h 30 au premier des quatre étages de l'immeuble. L'incendie a pu être maîtrisé rapidement, grâce à la facilité d'accès à l'immeuble, mais le feu, "très violent", avait déjà fait plusieurs victimes. "Un jeune homme a péri carbonisé au premier étage", a déclaré le porte-parole des pompiers.
Un autre homme de 22 ans, le fils du propriétaire de l'immeuble, a été grièvement blessé. "Il s'est défenestré et se trouve dans un état grave. Transporté à l'hôpital parisien du Val-de-Grâce, son pronostic vital est très engagé", a précisé le parquet de Bobigny, dont un représentant s'est rendu sur place. L'incendie a en outre fait quatre blessés légers, qui se trouvaient au quatrième étage et qui ont été intoxiqués par les fumées, selon les pompiers.
Dans cet immeuble, le propriétaire avait son domicile familial au quatrième étage. Il avait laissé une pièce à son fils au premier, a précisé le parquet. Le préfet de Seine-Saint-Denis s'est rendu sur place dans la matinée. La police judiciaire a été saisie d'une enquête pour déterminer les circonstances de l'incendie.
