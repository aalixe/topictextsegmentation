TITRE: Incendie meurtrier � Montreuil - Lib�ration
DATE: 2014-02-16
URL: http://www.liberation.fr/societe/2014/02/16/incendie-meurtrier-a-montreuil_980654
PRINCIPAL: 0
TEXT:
Lire sur le readerMode zen
Un homme de�21�ans est d�c�d� hier matin, et une autre personne a �t� gri�vement bless�e, lors d�un incendie dans un immeuble de bureaux et d�appartements � Montreuil (Seine-Saint-Denis). Le sinistre s�est d�clar� peu avant 8 h 30 au premier des quatre �tages de l�immeuble. Il a pu �tre ma�tris� rapidement, gr�ce � la facilit� d�acc�s au b�timent, mais le feu �tr�s violent� avait d�j� fait plusieurs victimes. �Un jeune homme a p�ri carbonis� au premier �tage�, a d�clar� le porte-parole des pompiers. Un�autre homme de�22�ans, le fils du propri�taire de l�immeuble, a �t� gri�vement bless�. �Il s�est d�fenestr� et�se trouve dans un �tat grave. Transport� � l�h�pital parisien du Val-de-Gr�ce, son pronostic vital est tr�s engag�, a pr�cis� le parquet de Bobigny. L�incendie a en outre fait quatre bless�s l�gers qui ont �t� intoxiqu�s par les fum�es. La police judiciaire a �t� saisie de l�enqu�te. Photo Fran�ois Guillot.AFP
les plus partag�s
