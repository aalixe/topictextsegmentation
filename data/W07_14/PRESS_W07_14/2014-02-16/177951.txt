TITRE: � Succ�s pour ��Intime conviction�� sur Arte Stars Actu
DATE: 2014-02-16
URL: http://www.stars-actu.fr/2014/02/succes-pour-intime-conviction-sur-arte/
PRINCIPAL: 177941
TEXT:
Tweeter
Vendredi soir, la cha�ne franco-allemande Arte diffusait ��Intime conviction��, un t�l�film in�dit d�une tr�s grande qualit� mettant en sc�ne Philippe Torreton.
Dans ��Intime conviction��, l�acteur incarne Paul Villers, un m�decin l�giste qui appelle la police apr�s avoir retrouv� sa femme Manon avec une balle dans la t�te. La th�se du suicide est aussit�t retenue et le corps de la d�funte incin�r�.
Mais quelques jours plus tard, les parents de la jeune femme portent plainte. L�enqu�te est confi�e au capitaine Judith Lebrun. Rapidement, celle-ci soup�onne Paul d�avoir tu� sa femme, mais les preuves sont inexistantes.
Arte
Un t�l�film salu� par la critique mais aussi par les t�l�spectateurs qui ont r�pondu nombreux � l�appel.
Ainsi, il a r�uni 1.4 million de t�l�spectateurs soit 5.6% du public pr�sent devant son poste de t�l�vision.
Un tr�s bon score pour Arte qui s�est ainsi class�e 5eme cha�ne au niveau national.
Autres articles
