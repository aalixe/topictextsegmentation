TITRE: Foot - Ligue 1 - 25e j. - Lyon peut y croire
DATE: 2014-02-16
URL: http://www.lequipe.fr/Football/Actualites/Lyon-peut-y-croire/442043
PRINCIPAL: 178783
TEXT:
a+ a- imprimer RSS
L��limination de Lyon par Lens en 8es de la Coupe de France ( 1-2, a.p. ) a donc �t� un mal pour un bien. Elle a d�gag� le calendrier de l�OL et a remis le club sur terre avant d�entamer le sprint final en L1. Ce dernier commence bien pour les hommes de R�mi Garde : ils ont logiquement battu Ajaccio ( 3-1 ) ce dimanche. Ils sont en forme et ont pris vingt points lors de leurs neuf derniers matches de Championnat !
Le joker Briand d�cisif � deux reprises
Face aux Corses, ils ont de nouveau pu compter sur un Bedimo de gala. L�ancien joueur de Montpellier a fait vivre un calvaire � Mostefa et Perozo. A la 59e, il a servi Briand sur un plateau mais l�attaquant s�est manqu� devant Ochoa. Dix minutes plus tard, la combinaison a fonctionn� et l�ex-Rennais a pu trouver le chemin des filets de la t�te (2-0, 69e). Parfait dans son r�le de joker , Briand a �galement donn� une passe d�cisive en fin de rencontre � Gomis (3-1, 90e).
Positionn�s tr�s bas, les cinq d�fenseurs de l�ACA ont longtemps perturb� Lyon avant de craquer � la 43e : absent face au RCL, Fofana a �t� bien servi par Lacazette et a ouvert le score d�une belle frappe enroul�e des 20 m�tres. Lyon, qui a fait sortir l�attaquant international fran�ais mais aussi M.Lopes sur blessure, a parfois trembl� (Lasne � la 7e, Tallo � la 49e). Il a vu Oliech r�duire la marque (2-1, 81e) sans cons�quence. Encore priv� de Gourcuff (fatigue musculaire), l�OL peut miser sur le Championnat pour l�emmener en Europe la saison prochaine.
C.O.-B.
