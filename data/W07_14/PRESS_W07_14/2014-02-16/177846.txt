TITRE: Syrie: �chec total des pourparlers � Gen�ve, Londres et Paris condamnent le r�gime
DATE: 2014-02-16
URL: http://www.huffpostmaghreb.com/2014/02/16/syrie-echec-geneve_n_4797063.html?utm_hp_ref%3Dmaghreb-international
PRINCIPAL: 177845
TEXT:
Syrie: �chec total des pourparlers � Gen�ve, Londres et Paris condamnent le r�gime
HuffPost Maghreb/AFP �|� Publication: 16/02/2014 07h54 CET
Le m�diateur de l'ONU, Lakhdar Brahimi face � la presse le 15 f�vrier 2014 � Gen�ve          | AFP
Recevoir les alertes:
S'inscrire
Suivre:
Quinze jours apr�s un premier �chec, une deuxi�me session de n�gociations � Gen�ve entre l'opposition et le gouvernement syriens n'a permis aucune avanc�e, l'avenir de ces pourparlers �tant d�sormais en question.
Impasse
Le m�diateur de l'ONU, Lakhdar Brahimi s'est dit "tout � fait d�sol�" et s'est excus� "aupr�s du peuple syrien dont les espoirs �taient si grands".
Il a mis fin aux discussions, dans l'impasse depuis trois semaines, et n'a fix� aucune date pour une reprise.
"Je pense qu'il est pr�f�rable que chaque partie rentre et r�fl�chisse � ses responsabilit�s, et (dise) si elle veut que ce processus continue ou non", a d�clar� M. Brahimi � la presse.
Il �tait pr�vu que ce deuxi�me cycle de discussions, commenc� lundi dernier, s'ach�ve samedi mais le m�diateur en accord avec les deux d�l�gations devait fixer une date pour une nouvelle r�union.
C'est un "s�rieux revers" et "la responsabilit� en incombe directement au r�gime d'Assad", a estim� le chef de la diplomatie britannique Willam Hague, soulignant "son plein soutien � Lakhdar Brahimi". M�me sentiment pour son homologue fran�ais Laurent Fabius qui "condamne l'attitude du r�gime syrien qui a bloqu� toute avanc�e".
Apr�s le refus d'appliquer l'ordre du jour par la d�l�gation du gouvernement syrien, M. Brahimi a choisi de renvoyer tout le monde sans date de retour pour donner � chacun un temps de r�flexion.
La derni�re r�union � Gen�ve entre l'opposition et le r�gime avait �galement �chou� avec toutefois une avanc�e notable puisque pour la premi�re fois les ennemis s'�taient parl�s.
Cette fois aucun progr�s n'a �t� enregistr� apr�s des discussions particuli�rement difficiles.
"Une perte de temps"
Rendant compte de l'ultime rencontre, le m�diateur a expliqu� que les deux parties avaient camp� sur leur position.
"Le gouvernement consid�re que la question la plus importante est le terrorisme, l'opposition consid�re que la question la plus importante est l'autorit� gouvernementale de transition", a-t-il dit ajoutant qu'il avait propos� d'�voquer d'abord "la violence et le terrorisme" pour passer ensuite au probl�me de "l'autorit� gouvernementale".
"Malheureusement le gouvernement a refus�, provoquant chez l'opposition le soup�on qu'ils ne veulent absolument pas parler de l'autorit� gouvernementale de transition", a ajout� le m�diateur.
"J'esp�re que les deux parties vont r�fl�chir un peu mieux et reviendront pour appliquer le communiqu� de Gen�ve", adopt� en juin 2012 par les grandes puissances comme plan de r�glement politique de ce conflit qui dure depuis pr�s de trois ans.
"J'esp�re que ce temps de r�flexion conduira en particulier le gouvernement � rassurer l'autre partie (sur le fait) que quand ils parlent d'appliquer le communiqu� de Gen�ve ils comprennent que l'autorit� gouvernementale transitoire doit exercer les pleins pouvoirs ex�cutifs. Bien s�r combattre la violence est indispensable", a ajout� M. Brahimi.
L'exercice des "pleins pouvoirs ex�cutifs" reviendrait � priver le pr�sident Bachar al Assad de ses pouvoirs, m�me si cela n'est pas �crit explicitement dans le communiqu�, d'o� le blocage de Damas sur ce point.
Un troisi�me round de discussions avec le gouvernement syrien sans parler de transition politique serait "une perte de temps", a estim� le porte-parole de la d�l�gation de l'opposition, M. Louai Safi.
"Le r�gime n'est pas s�rieux (...) nous ne sommes pas ici pour n�gocier le communiqu� de Gen�ve mais pour l'appliquer", a-t-il ajout� � propos du plan de r�glement politique en Syrie adopt� par les grandes puissances en 2012.
Les Russes doivent faire beaucoup plus
"Nous devons �tre s�r que le r�gime veut une solution politique et pas des tactiques pour gagner du temps", a encore affirm� M. Safi � propos de ces n�gociations sous m�diation de l'ONU entam�es le 22 janvier sous la pression de la communaut� internationale, en particulier les parrains russe et am�ricain de la Conf�rence.
C�t� gouvernemental, le chef des n�gociateurs, l'ambassadeur syrien aupr�s de l'ONU Bachar Al-Jafari s'en est tenu � accuser l'opposition "de ne pas respecter l'agenda", affirmant qu'il fallait d'abord conclure "par une vision commune" sur le premier point, la lutte contre la violence et le terrorisme avant de passer � un autre.
M. Brahimi a indiqu� qu'il allait rendre compte � New York au Secr�taire g�n�ral Ban Ki-moon et qu'il esp�rait que se tiendrait une r�union avec lui et les deux chefs de la diplomatie russe et am�ricaine Sergei Lavrov et John Kerry.
Un appel jeudi aux parrains russes et am�ricains de la Conf�rence n'a produit aucun effet et les Etats-Unis interpellent d�sormais publiquement la Russie pour lui demander d'en faire plus pour obtenir de la flexibilit� de la part de son alli�, le r�gime syrien.
"Nous demandons aux Russes, tr�s franchement, d'en faire beaucoup plus, parce qu'il n'y a pas beaucoup de pays qui peuvent avoir une influence sur le r�gime", a soulign� vendredi la porte-parole adjointe du D�partement d'Etat, Marie Harf.
Le pr�sident Barak Obama, qui a re�u vendredi en Californie le roi Abdallah II de Jordanie a exprim� sa frustration et annonc� sa d�cision de faire davantage pression sur le r�gime syrien.
Retrouvez les articles du HuffPost Maghreb sur notre page Facebook .
Contribuer � cet article:
