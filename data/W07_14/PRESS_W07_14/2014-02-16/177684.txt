TITRE: Liga: Villarreal-Celta bri�vement interrompu � cause d�un gaz lacrymog�ne (vid�o) | Fil info Sport Football - lesoir.be
DATE: 2014-02-16
URL: http://www.lesoir.be/469091/article/actualite/fil-info/fil-info-sport-football/2014-02-16/liga-villarreal-celta-brievement-interrompu-cause-d-un-gaz-
PRINCIPAL: 177681
TEXT:
Liga: Villarreal-Celta bri�vement interrompu � cause d�un gaz lacrymog�ne (vid�o)
Avec AFP
dimanche 16 f�vrier 2014, 01 h 33
La rencontre a �t� suspendue durant un quart d�heure.
Sur le m�me sujet
Tous les r�sultats de la Liga
Le match entre Villarreal et le Celta Vigo (0-2) a d� �tre interrompu un quart d�heure samedi au stade Madrigal lors de la 24e journ�e du Championnat d�Espagne, � cause d�un fumig�ne apparemment lacrymog�ne lanc� sur la pelouse juste apr�s le premier but des visiteurs.
La bouteille de gaz a atterri dans la surface de r�paration du Celta � trois minutes de la fin du temps r�glementaire (87), contraignant l�arbitre � suspendre le jeu alors que les Galiciens venaient de prendre l�avantage gr�ce � un but de Fabian Orellana (83).
D�apr�s les images diffus�es � la t�l�vision, on a alors vu les spectateurs quitter la tribune, visiblement incommod�s par cette fum�e.
�Au d�but, je pensais que c��tait un feu de Bengale, a expliqu� Yoel, le gardien du Celta, au micro de Gol TV. Avec ce type de gaz, sur le moment les yeux et la gorge te piquent, tu t��touffes mais apr�s �a passe, un peu d�eau sur le visage et on peut continuer le match.�
Le jeu a finalement pu reprendre quelques minutes plus tard devant des tribunes largement vid�es.
Le Celta a alors doubl� la mise sur un coup franc direct de Nolito (90) pour s�imposer 2-0.
