TITRE: France Monde | Protestations anti-gouvernementales : 23 blessés - Le Bien Public
DATE: 2014-02-16
URL: http://www.bienpublic.com/actualite/2014/02/16/protestations-anti-gouvernementales-23-blesses
PRINCIPAL: 177841
TEXT:
Protestations anti-gouvernementales : 23 blessés
VENEZUELA Protestations anti-gouvernementales : 23 blessés
Notez cet article :
le 16/02/2014      à 07:42 | AFP
AFP
Des milliers de partisans du pouvoir vénézuélien et opposants se sont à nouveau rassemblés samedi à Caracas, au cours d�??une douzième journée de protestation qui s�??est achevée par des incidents faisant 23 blessés.
Partager
Envoyer à un ami
Des milliers de partisans du pouvoir vénézuélien et opposants se sont à nouveau rassemblés samedi à Caracas, au cours d�??une douzième journée de protestation qui s�??est achevée par des incidents faisant 23 blessés.
Ces incidents ont éclaté à la tombée de la nuit dans la capitale où s�??étaient rassemblés à la mi-journée quelque 3.000 militants pro-opposition, dont une majorité d�??étudiants, vêtus de blanc pour la plupart et munis de nombreux drapeaux vénézuéliens, selon des journalistes de l�??AFP.
La police a dispersé à coups de gaz lacrymogènes et de canons à eau une centaine d�??étudiants qui défilaient sur une avenue principale dans l�??est de la capitale. 23 personnes ont été blessées, a indiqué sur twitter Ramon Muchacho, maire de Chacao, l�??un des arrondissements de Caracas.
Ces mobilisations de l�??opposition s�??inscrivent dans le cadre du mouvement de protestation anti-gouvernemental lancé il y a 12 jours en province par des étudiants qui s�??insurgent contre la vie chère, l�??insécurité et les pénuries dans ce pays pétrolier, qui dispose des plus importantes réserves de la planète.
Mercredi, la capitale avait été le théâtre de la plus importante mobilisation contre le président Nicolas Maduro depuis son élection en avril 2013. De violentes échauffourées survenues en marge de la marche ont fait trois morts, tués par balles, et plus de 60 blessés.
Le secrétaire d�??Etat américain John Kerry a condamné samedi la «violence insensée» exercée contre les manifestants opposés au gouvernement vénézuélien et demandé la remise en liberté des personnes arrêtées.
«Avant nous ne sortions pas dans la rue à cause de l�??insécurité, et maintenant que nous manifestons ils nous tuent. Nous les jeunes n�??avons plus ni foi ni espoir, nous ne pouvons pas avoir de travail et si nous en avons un, on ne nous donne pas de quoi avoir une vie décente», a déclaré samedi à l�??AFP Issac Castillo, étudiant de 27 ans à l�??Université Andres Bello.
Dans le centre, plusieurs milliers de partisans du gouvernement socialiste avaient également répondu à l�??appel lancé jeudi soir par le président pour un rassemblement «pour la paix et contre le fascisme», terme habituellement utilisé par les autorités pour désigner les opposants. Ces derniers sont accusés par les autorités de fomenter les violences pour tenter de provoquer «un coup d�??Etat».
Arborant des vêtements et drapeaux de la couleur rouge du parti au pouvoir et des pancartes à l�??effigie du libérateur Simon Bolivar et de l�??ex-président Hugo Chavez (1999-2013), les militants pro-Maduro, d�??abord rassemblés en divers points de la capitale, avaient convergé vers le centre-ville malgré une chaleur accablante.
Les deux camps ont également organisé des rassemblements dans plusieurs autres villes du pays, dont San Cristobal, Merida, El Vigia (ouest) et Valencia (nord).
- Un opposant dans la mire des autorités -
S�??adressant à ses partisans dans l�??après-midi, le président vénézuélien s�??en est pris à Leopoldo Lopez, un opposant accusé d�??homicide et visé par un mandat d�??arrêt depuis les évènements de mercredi soir.
«Les forces de sécurité de l�??Etat le cherchent. Rends-toi lâche! Le peuple veut la justice, lâche», s�??est exclamé M. Maduro sous les vivats des militants «chavistes», sans toutefois nommer l�??opposant.
Contactée par l�??AFP, une source proche de M. Lopez, qui fait partie d�??un petit groupe d�??opposants prônant la contestation dans la rue, a indiqué que l�??intéressé ne se cachait pas et qu�??il n�??avait pas quitté le pays.
La protestation lancée par les étudiants, décidés à obtenir le départ de M. Maduro, ne fait pas l�??unanimité au sein de la Table de l�??unité démocratique (MUD), la principale coalition de l�??opposition vénézuélienne. Son leader Henrique Capriles a lui-même estimé jeudi que «les conditions ne sont pas réunies pour forcer le départ du gouvernement».
La plupart des chaînes de télévision vénézuéliennes se sont abstenues ces derniers jours de diffuser les images de ces incidents, craignant visiblement les avertissements du Conseil national des Télécommunications qui a menacé de sanctions les médias qui feraient «la promotion de la violence».
Pour M. Maduro, qui a annoncé jeudi soir la suspension de la diffusion de la télévision colombienne NTN24, l�??ex-président conservateur colombien Alvaro Uribe est derrière la couverture des évènements de mercredi par cette chaîne d�??informations. En outre, M. Maduro a affirmé que l�??ex-président «finance et dirige les mouvements fascistes» accusés de semer le trouble.
Depuis quelques semaines, le gouvernement fait face à une grogne croissante d�??une partie de la population dans un contexte de forte inflation (56,3% en 2013), de pénuries récurrentes frappant les denrées alimentaires ou les produits de consommation courante et d�??une insécurité que les autorités ne parviennent pas à juguler.
Vendredi soir, le président vénézuélien a tenté de reprendre la main sur le terrain politique en annonçant un plan destiné à lutter contre la violence, qui prévoit notamment de renforcer les patrouilles de police et le désarmement de la population dans un pays où les armes abondent.
Vos commentaires
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
