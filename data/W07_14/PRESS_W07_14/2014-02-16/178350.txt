TITRE: Christiane Taubira et la justice mal vues par les Fran�ais - RTL.fr
DATE: 2014-02-16
URL: http://www.rtl.fr/actualites/info/article/christiane-taubira-et-la-justice-mal-vues-par-les-francais-7769749228
PRINCIPAL: 0
TEXT:
Accueil > Toutes les Actualit�s info > Christiane Taubira et la justice mal vues par les Fran�ais
Christiane Taubira et la justice mal vues par les Fran�ais
La ministre de la Justice, Christiane Taubira, le 2 f�vrier 2014 � Caen (Basse-Normandie).
Cr�dit : AFP / CHARLY TRIBALLEAU
Une grande majorit� de Fran�ais a une mauvaise opinion de la justice, et de la ministre Christiane Taubira, selon un sondage publi� dans "Le Parisien" dimanche 16 f�vrier.
Les trois quarts des Fran�ais (75%) estiment que la justice en  France fonctionne mal et plus de la moiti� (59%), ont une mauvaise  opinion de la ministre de la Justice Christiane Taubira , selon un sondage BVA pour Le Parisien Dimanche.
"Un niveau de d�fiance jamais atteint depuis 1962"
A la question, "Estimez-vous qu'en  France la justice fonctionne globalement...?", 75% des sond�s r�pondent  "mal", 24% r�pondent "bien". "Un niveau de d�fiance jamais atteint depuis 1962 et les premi�res enqu�tes sur ce th�me", pr�cise le journal.
Les  Fran�ais d'autre part ne croient pas � l'ind�pendance des juges  d'instruction. "Diriez-vous qu'en France les juges d'instruction sont  ind�pendants du pouvoir politique ?" 57% des sond�s r�pondent "non", 41%  "oui". Toujours dans le m�me sondage, "et pour ne rien arranger,  les Fran�ais n'aiment pas non plus leur ministre de la Justice", rel�ve Le Parisien.
Mauvaise opinion de  Christiane Taubira
A la question, "Quelle opinion avez-vous de  Christiane Taubira ?" 59% des personnes interrog�es r�pondent  "mauvaise", 39% affirment avoir une "bonne" opinion. D'autre part,  54% la jugent "moins bonne" ministre de la Justice que ses  pr�d�cesseurs lorsque Nicolas Sarkozy �tait pr�sident. 40% la trouvent  "meilleure." 6% sans opinion.
Enfin, les sond�s pr�f�rent le  ministre de l'Int�rieur Manuel Valls � la ministre de la Justice  Christiane Taubira, 65% contre 24% (11% sans opinion). Ce sondage a �t�  r�alis� les 13 et 14 f�vrier, aupr�s d'un �chantillon de 981 personnes  recrut�es par t�l�phone et interrog�es par Internet, repr�sentatif de la  population fran�aise �g�e de 18 ans et plus, selon la m�thode des  quotas.
La r�daction vous recommande
