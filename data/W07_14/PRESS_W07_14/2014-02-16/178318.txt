TITRE: JO/Snowboardcross: lourde chute de l'Am�ricaine Hernandez | La-Croix.com
DATE: 2014-02-16
URL: http://www.la-croix.com/Actualite/Sport/JO-Snowboardcross-lourde-chute-de-l-Americaine-Hernandez-2014-02-16-1107252
PRINCIPAL: 178228
TEXT:
�Un v�t�ran du Vietnam � la t�te de la d�fense am�ricaine
L'Am�ricaine Jacqueline Hernandez a �t� victime d'une lourde chute dimanche lors des qualifications de l'�preuve olympique de snowboardcross, sur le m�me parcours o� une Russe s'�tait gravement bless�e samedi en skicross.
Hernandez, 21 ans, a �t� d�s�quilibr�e sur un des nombreux sauts du parcours et s'est r�ceptionn� en travers. Sa t�te a bascul� en arri�re et a violemment heurt� la piste. Elle est rest�e inerte quelques instants avant de retrouver ses esprits et d'�tre �vacu�e sur une civi�re, consciente.
Quelques instants auparavant, la Norv�gienne Helene Olafsen avait �galement chut� sur le parcours et avait d� elle aussi �tre �vacu�e sur une civi�re.
Samedi, Maria Komissarova a �t� la premi�re athl�te des JO-2014 � se blesser tr�s gravement. Touch�e � la colonne vert�brale lors d'un entra�nement de skicross dans ce parcours du Parc Extr�me de Rosa Khoutor, la Russe avait �t� op�r�e pendant plus de six heures � l'h�pital de Krasna�a Poliana, le plus proche du site, construit sp�cialement pour les JO.
Elle �tait samedi soir dans un �tat "s�rieux et stable" mais "consciente", selon la F�d�ration russe de ski freestyle (FFR).
En skicross et en snowboardcross, les concurrents d�valent � quatre ou � six une longue piste �troite parsem�e de virages relev�s, de grands sauts et d�autres obstacles, comme des bosses, qui ressemble en fait � un parcours de motocross.
Lors des entra�nements et des qualifications, les concurrentes passent une � une, ce qui �tait le cas lorsque Komissarova, Hernandez et Olafsen ont chut�.
AFP
