TITRE: Trespeuch, le bronze � l'arrach� - JO 2014 - Sports.fr
DATE: 2014-02-16
URL: http://www.sports.fr/jo-2014/snowboard/articles/jo-2014-snowboard-trespeuch-en-plein-reve-1011052/
PRINCIPAL: 178530
TEXT:
16 février 2014 � 11h14
Mis à jour le
16 février 2014 � 13h15
La bonne surprise du jour dans le clan tricolore provient du site de snowboard cross. Chloé Trespeuch s’y est offert le bronze olympique, concluant une finale à rebondissements dans le sillage de la Canadienne Dominique Maltais et de la Tchèque Eva Samkova. Il s’agit là de la cinquième médaille tricolore dans ces Jeux.
Quatre Bleues étaient au départ de cette épreuve de snowboard cross, parmi lesquelles l’expérimentée Déborah Anthonioz, médaillée d’argent à Vancouver il y a quatre ans. Deux sont allées en demi-finales, abandonnées par la Haut-Savoyarde suscitée et une Charlotte Bankes victime d’une chute alors qu’elle menait son quart de finale et avait réalisé de belles qualifications. A son tour partie à la faute en demie alors qu’elle figurait dans le trio gagnant,  Nelly Moenne-Loccoz a elle craqué aux portes de la finale. De fait, il ne pouvait en rester qu’une: Chloé Trespeuch. Et cette dernière a fait honneur à ses consoeurs !    
A 19 ans, la vice-championne du monde junior a pris du galon ce dimanche à Sotchi, prenant une troisième place méritoire en finale derrière l’intouchable Tchèque Eva Samkova et l’épouvantail  canadien Dominique Maltais, vice-championne du monde en titre et médaillée de bronze aux JO de Turin en 2006. Une breloque de bronze arrachée dans le bas du parcours, alors qu’elle avait été freinée plus haut par la concurrence. La consécration, déjà, d’une surdouée du snow qui fait aujourd’hui la gloire de Val-Thorens, et d’une équipe de France qui cueille là sa cinquième médaille dans ces Jeux.
"Je crois que je ne réalise pas encore, c'est le plus beau moment de ma vie, c'est sûr, réagissait-elle émue après coup devant les caméras de France Télévisions. J'ai fait une qualif moyenne, après je suis rentrée dans ma course et j’ai fait tous mes runs à fond. C'est une finale où j'ai fait une grosse erreur mais je suis quand même revenue. Quand on arrive sur le podium, c'est le bonheur !" Aux Paul-Henri De Le Rue, Tony Ramoin et Pierre Vaultier de prendre le relais dès demain chez les messieurs.
La joie de Chloé Trespeuch à l'arrivée ! Bravo ! #Sochi2014 #JO2014 pic.twitter.com/L1rxnCHCYO
