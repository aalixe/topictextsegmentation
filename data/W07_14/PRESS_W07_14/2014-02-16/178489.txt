TITRE: 20 Minutes Online - Violence rare et bless�s dans les manifestations - Monde
DATE: 2014-02-16
URL: http://www.20min.ch/ro/news/monde/story/Violence-rare-et-bless-s-dans-les-manifestations-13061880
PRINCIPAL: 178486
TEXT:
17.02 Les affrontements entre la police et les opposants ont continu� � Caracas. Image: AFP/Juan Barreto
Image: Keystone/Santi Donaire
Venezuela
Violence rare et bless�s dans les manifestations
Des milliers de partisans du pouvoir v�n�zu�lien et opposants se sont � nouveau rassembl�s samedi � Caracas, au cours d'une douzi�me journ�e de protestation qui s'est achev�e par des incidents faisant 23 bless�s.
Des incidents ont �clat� � la tomb�e de la nuit dans la capitale, o� s'�taient rassembl�s � la mi-journ�e quelque 3000 militants pro-opposition. La majorit� �tait compos�e d'�tudiants, v�tus de blanc pour la plupart et munis de nombreux drapeaux v�n�zu�liens.
La police a dispers� � coups de gaz lacrymog�nes et de canons � eau une centaine d'�tudiants qui d�filaient sur une avenue principale dans l'est de la capitale. Vingt-trois personnes ont �t� bless�es, a indiqu� sur twitter Ramon Muchacho, maire de Chacao, l'un des arrondissements de Caracas.
Trois tu�s par balle
Ces mobilisations de l'opposition s'inscrivent dans le cadre du mouvement de protestation anti-gouvernemental lanc� il y a 12 jours en province par des �tudiants qui s'insurgent contre la vie ch�re, l'ins�curit� et les p�nuries dans ce pays p�trolier, qui dispose des plus importantes r�serves de la plan�te.
Mercredi, la capitale avait �t� le th��tre de la plus importante mobilisation contre le pr�sident Nicolas Maduro depuis son �lection en avril 2013. De violentes �chauffour�es survenues en marge de la marche ont fait trois morts, tu�s par balles, et plus de 60 bless�s.
Violence insens�e
Le secr�taire d'Etat am�ricain John Kerry a condamn� samedi la �violence insens�e� exerc�e contre les manifestants oppos�s au gouvernement v�n�zu�lien et demand� la remise en libert� des personnes arr�t�es.
�Avant nous ne sortions pas dans la rue � cause de l'ins�curit�, et maintenant que nous manifestons ils nous tuent. Nous les jeunes n'avons plus ni foi ni espoir, nous ne pouvons pas avoir de travail et si nous en avons un, on ne nous donne pas de quoi avoir une vie d�cente�, a d�clar� samedi � l'AFP Issac Castillo, �tudiant de 27 ans � l'Universit� Andres Bello.
Coup d'Etat
Dans le centre, plusieurs milliers de partisans du gouvernement socialiste avaient �galement r�pondu � l'appel lanc� jeudi soir par le pr�sident pour un rassemblement �pour la paix et contre le fascisme�, terme habituellement utilis� par les autorit�s pour d�signer les opposants. Ces derniers sont accus�s par les autorit�s de fomenter les violences pour tenter de provoquer �un coup d'Etat�.
Arborant des v�tements et drapeaux de la couleur rouge du parti au pouvoir et des pancartes � l'effigie du lib�rateur Simon Bolivar et de l'ex-pr�sident Hugo Chavez (1999-2013), les militants pro-Maduro, d'abord rassembl�s en divers points de la capitale, avaient converg� vers le centre-ville malgr� une chaleur accablante.
Les deux camps ont �galement organis� des rassemblements dans plusieurs autres villes du pays, dont San Cristobal, Merida, El Vigia et Valencia (nord).
Le pr�sident intervient
S'adressant � ses partisans dans l'apr�s-midi, le pr�sident v�n�zu�lien s'en est pris � Leopoldo Lopez, un opposant accus� d'homicide et vis� par un mandat d'arr�t depuis les �v�nements de mercredi soir.
�Les forces de s�curit� de l'Etat le cherchent. Rends-toi l�che! Le peuple veut la justice, l�che�, s'est exclam� M. Maduro sous les vivats des militants �chavistes�, sans toutefois nommer l'opposant.
Contact�e par l'AFP, une source proche de M. Lopez, qui fait partie d'un petit groupe d'opposants pr�nant la contestation dans la rue, a indiqu� que l'int�ress� ne se cachait pas et qu'il n'avait pas quitt� le pays.
Protestation pas unanime
La protestation lanc�e par les �tudiants, d�cid�s � obtenir le d�part de M. Maduro, ne fait pas l'unanimit� au sein de la Table de l'unit� d�mocratique (MUD), la principale coalition de l'opposition v�n�zu�lienne. Son leader Henrique Capriles a lui-m�me estim� jeudi que �les conditions ne sont pas r�unies pour forcer le d�part du gouvernement�.
La plupart des cha�nes de t�l�vision v�n�zu�liennes se sont abstenues ces derniers jours de diffuser les images de ces incidents, craignant visiblement les avertissements du Conseil national des T�l�communications qui a menac� de sanctions les m�dias qui feraient �la promotion de la violence�.
(afp)
