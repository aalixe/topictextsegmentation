TITRE: Volkswagen  - La Volkswagen Scirocco facelift prend la fuite - Autoalgerie.com - Le portail de l'information automobile en Alg�rie
DATE: 2014-02-16
URL: http://www.autoalgerie.com/la-volkswagen-scirocco-facelift,7083
PRINCIPAL: 178899
TEXT:
Paru dans : | Coup� | Fuites | Groupe Volkswagen AG | Restylage | Salon de Gen�ve | Sous-compacte | Volkswagen |
Publi� le dimanche  16 f�vrier 2014 � 09: 51 , par Khaled A.
Le Coup� Sportif Sirocco de Volkswagen n�a pas attendu le Salon de Gen�ve pour prendre la poudre d�escampette et se r�v�ler au public sous son nouveau visage restyl� en attendant une nouvelle g�n�ration d�ici 2017.
Les changements adopt�s au niveau des pare-chocs, des feux et du capot, offre �  la  Volkswagen Sirocco facelift  des formes plus en douceur sans changer l�identit� du mod�le. De nouvelles jantes et 5  nouvelles teintes (Pure White, Flash Red, Urano Grey, Ultra Violet et Pyramid Gold)  font �galement leurs apparitions.  A l�int�rieur,  la Sirocco 2014 promet une meilleure qualit� de mat�riaux tout en s�offrant un second  combin� d�instruments (temp�rature, chronom�tre, pression du turbo) ainsi qu�un nouveau volant et des syst�mes Audio et de navigation revisit�s au gout du jour.
Sous le capot, la Volkswagen Sirocco sera anim�e en essence par le 1.4 TSI 125ch et le 2.0 TSI de 180ch et 220ch,  tandis que le diesel sera uniquement disponible en 2.0 TDi mais offrant deux puissance diff�rentes, 150ch et 184ch, annonce le constructeur allemand.  Enfin, la version R de la Scirocco embarquera le 2.0 TSi de 280ch.
Noter cet article :
