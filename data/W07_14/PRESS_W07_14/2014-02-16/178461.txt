TITRE: Lavillenie, nouvelle légende de la perche, arrive en héros à Paris - 17 février 2014 - Le Nouvel Observateur
DATE: 2014-02-16
URL: http://tempsreel.nouvelobs.com/topnews/20140216.AFP0226/lavillenie-nouvelle-legende-de-la-perche-arrive-en-heros-a-paris.html
PRINCIPAL: 0
TEXT:
Actualité > Société > Lavillenie, nouvelle légende de la perche, arrive en héros à Paris
Lavillenie, nouvelle légende de la perche, arrive en héros à Paris
Publié le 16-02-2014 à 14h05
Mis à jour le 17-02-2014 à 11h25
A+ A-
Le Français Renaud Lavillenie est arrivé dimanche à la mi-journée à Paris, accueilli par une nuée de journalistes après être entré dans l'Histoire en battant le record du monde de saut à la perche de Sergueï Bubka, vieux de 21 ans. (c) Afp
Paris (AFP) - Le Français Renaud Lavillenie est arrivé dimanche à la mi-journée à Paris, accueilli par une nuée de journalistes après être entré dans l'Histoire en battant le record du monde de saut à la perche de Sergueï Bubka, vieux de 21 ans.
Alors que les Français déçoivent aux jeux Olympiques de Sotchi, en particulier en ski alpin, le perchiste est arrivé triomphant, quoique sur des béquilles après s'être blessé au pied.
"Je sais que je ne vais pas réaliser tout de suite, il va me falloir quelques jours", a-t-il expliqué aux journalistes en expliquant avoir besoin de rentrer chez lui pour mieux mesurer son exploit. "Je ne vais jamais m'arrêter, il n'y a pas de raison d'arrêter de battre" ce nouveau record, a-t-il ajouté. "Bubka n'est pas un athlète lambda. C'est assez extraordinaire".
Le perchiste le plus haut du monde s'est blessé samedi soir juste après son record, en tentant de franchir la barre hallucinante de 6,21 mètres. L'Auvergnat n'a pas réussi à sauter sur ce dernier essai, retombant sur la piste d'élan et se blessant au pied gauche à quelques semaines des Championnats du monde en salle d'athlétisme, en mars, à Sopot (Pologne)
"Il fallait que je tente quelque chose, c'est dans ma nature, c'est comme ça", a-t-il expliqué. Mais il a ajouté ne pas pouvoir annoncer s'il serait ou non forfait aux Mondiaux. Il donnera une conférence de presse lundi à 16h00 à Paris après avoir passé des examens. "Il n'y a aucune certitude" sur la question", a-t-il assuré.
Si cette blessure contrarie ses projets à court terme, elle n'effacera pas l'exploit invraisemblable que le Français a accompli à Donetsk, sur les terres du tsar ukrainien. Bubka avait sauté 6,15 m en 1993 en salle et 6,14 m en 1994 en plein air. Lavillenie est donc bel et bien devenu l'homme le plus haut du monde.
- "La dernière frontière" -
La légende de la perche mondiale, présent au meeting Pole Vault Stars en costume-cravate, a été parmi les premiers à féliciter le Français. "Une nouvelle ère dans ce sport est arrivée. Aujourd'hui, le vainqueur est un champion olympique (...). Nous nous attendions à cet événement et nous sommes ravis que cela se soit passé précisément ici, à Donetsk", a déclaré Bubka à l'AFP.
Le champion olympique avait déjà amélioré son record par deux fois en janvier, effaçant 6,04 m à Rouen ( France ) puis 6,08 à Bydgoszcz (Pologne). Sûr de ses qualités, le perchiste de poche (1,77 m/70 kg) était obsédé par la performance du monstre physique de Bubka. Elle est désormais derrière lui.
Agé de 27 ans, il est monté sur tous les podiums depuis 2009, l'année de son éveil international, et conquiert avec ce record la légitimité des géants. Il ne manque au Charentais de naissance que l'or des Mondiaux en plein air. Troisième en 2009, il dut encore se contenter de cette place en 2011, mais cette fois dans la peau du favori. Il a été une nouvelle fois deuxième en 2013 à Moscou, derrière l'Allemand Raphael Holdzeppe.
La presse française rivalisait de jeux de mots pour célébrer son exploit. "Perché !", scandait notamment l'Equipe. "Il est allé si haut qu'il nous a propulsés en apesanteur et privés d'oxygène", ajoutait le quotidien sportif, soulignant que "la marque de Bubka était la dernière frontière que l'athlétisme moderne ne parvenait pas à franchir".
Partager
