TITRE: Sotchi : le relais 4 x 10 km du ski de fond arrache une nouvelle m�daille de bronze pour la France � metronews
DATE: 2014-02-16
URL: http://www.metronews.fr/sport/sotchi-le-relais-4-x-10-km-du-ski-de-fond-arrache-une-nouvelle-medaille-de-bronze-pour-la-france/mnbp!hoMBkXmY2MF/
PRINCIPAL: 178539
TEXT:
Cr�� : 16-02-2014 12:20
Sotchi : l'exploit du relais de ski de fond fran�ais
JEUX OLYMPIQUES - L'�quipe de France olympique est en feu ce dimanche matin, le relais de ski de fond homme est all� chercher une nouvelle m�daille, encore en bronze. Robin Duvillard, Jean-Marc Gaillard, Maurice Manificat et Ivan Perrillat Boiteux ont r�alis� une course magnifique.
�
Maurice Magnificat, a r�alis� un relais d'une rare intensit�.� Photo :�ODD ANDERSEN / AFP
Quelle matin�e pour les Bleus ! Apr�s la m�daille de bronze de Chlo� Trespeuch en snowboard cross , le relais masculin du 4 x 10�km du ski de fond remporte lui aussi une m�daille de bronze.�Robin Duvillard, Jean-Marc Gaillard, Maurice Manificat et Ivan Perrillat Boiteux ont �t� avec le groupe de t�te tout au long de la course, allant chercher la m�daille aux tripes.
C'est la premi�re m�daille Fran�aise dans la sp�cialit�, la deuxi�me de la discipline, fruit d'un gros travail f�d�ral, tant au niveau des hommes que du mat�riel. Des hommes qui ont d'abord eu la bonne id�e de se porter � l'avant avec les relais de Robin Duvillard et Jean-Marc Gaillard. Maurice Manificat a ensuite pris le bon wagon lorsque la Su�de et la Russie ont acc�l�r� le rythme. Il ne restait alors, plus qu'�Ivan Perrillat Boiteux � assurer la m�daille.�
Personne n'attendait ces Bleus-l�
Il a bien failli faire bien mieux, encore en course au moment de disputer la m�daille d'argent, au sprint , � la Russie. Mais le Fran�ais s'inclinait cependant rapidement. Logique, tant les Russes sont intrins�quement plus forts que les Fran�ais. Il se consolera en apprenant qu'il a d�croch� la 100e m�daille fran�aise de l'histoire des JO.�
Personne, mis � part les membres du relais, n'avait os� r�ver de pareille performance. D'autant que les performances des fondeurs depuis le d�but de ces JO, nous avaient fait oublier qu'impossible n'est pas fran�ais. Grave erreur.
Yann Butillon
