TITRE: France : des visas en quelques jours pour les hommes d'affaires étrangers
DATE: 2014-02-16
URL: http://www.lefigaro.fr/flash-eco/2014/02/16/97002-20140216FILWWW00083-france-des-visas-en-quelques-jours-pour-les-hommes-d-affaires-etrangers.php
PRINCIPAL: 0
TEXT:
le 16/02/2014 à 14:07
Publicité
La ministre du Commerce extérieur Nicole Bricq a indiqué dimanche que la France allait réduire les délais d'obtention de visas pour les hommes d'affaires étrangers à quelques jours, contre plusieurs semaines actuellement.
"Un homme d'affaires chinois qui veut venir en France, il lui faut huit semaines pour avoir un visa. Un homme d'affaires russe qui veut venir en France, il lui faut trois semaines", a dit la ministre, interrogée lors du Grand rendez-vous Europe 1/iTélé/Le Monde.
"On pense qu'on peut le faire beaucoup plus rapidement et plus parler en semaine mais en journée", a-t-elle dit.
Nicolas Bricq a précisé que le ministère des Affaires étrangères "a déjà donné des ordres très précis pour traiter cela en heures" dans certains consulats. La France a déjà mis en place depuis le 27 janvier la délivrance de visas en 48 heures pour les touristes chinois, une promesse faire dans le cadre des célébrations des 50 ans de relations diplomatiques entre la France et la Chine populaire.
Ce type de mesures sur les visas pour les entrepreneurs étrangers entrent dans le cadre du Conseil stratégique de l'attractivité que doit présider lundi François Hollande.
Ce conseil "n'avait pas été réuni depuis quelques années", a rappelé la ministre, qui entend "donner un certain nombre de garanties aux investisseurs".
