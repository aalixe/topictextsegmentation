TITRE: Grande-Bretagne: La Tamise menace toujours les habitations -   Monde - 24heures.ch
DATE: 2014-02-16
URL: http://www.24heures.ch/monde/tamise-menace-toujours-habitations/story/24698010
PRINCIPAL: 178245
TEXT:
La Tamise menace toujours les habitations
Mis � jour le 16.02.2014
Des localit�s d�j� inond�es par la Tamise s'attendaient dimanche � une nouvelle mont�e des eaux apr�s avoir essuy� une temp�te qui a d�j� fait trois morts et priv� d'�lectricit� des milliers de foyers en Grande-Bretagne.
1/17 La Tamise pourrait atteindre localement ce week-end des niveaux record au Royaume-Uni. (16 f�vrier 2014)
� �
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre email a �t� envoy�.
La Tamise pourrait atteindre localement ce week-end des niveaux record.
Samedi la temp�te Ulla a frapp� durement: 115'000 foyers priv�s d'�lectricit� entre le Royaume-Uni et la France, fortes perturbations dans les transports et glissements de terrain.
Le Premier ministre britannique David Cameron a visit� samedi Chertsey, un village � l'ouest de Londres, o� les soldats aident les habitants � se pr�parer � la mont�e des eaux en �rigeant des barrages de sacs de sable.
�Ce que nous ferons dans les prochaines 24 heures est vital, car malheureusement le niveau du fleuve va encore monter. Chaque sac de sable, chaque maison prot�g�e, chaque barri�re anti-inondation, chaque maison prot�g�e peut compter beaucoup�, a-t-il dit.
Interrog� une nouvelle fois par la presse sur le retard pris par les autorit�s britanniques pour intervenir, David Cameron a assur� qu'il allait �tirer les le�ons� de la crise le moment venu.
Le leader de l'opposition travailliste Ed Miliband a mis en cause le changement climatique � l'origine de ces intemp�ries. Il a appel� le gouvernement � consid�rer le r�chauffement de la plan�te comme une �question de s�curit� nationale�, dans une interview publi�e dimanche par le journal The Observer.
Portugal aussi touch�
En France, environ 30'000 foyers �taient encore sans courant samedi soir en Bretagne, selon Electricit� R�seau Distribution France (ERDF), qui avait r�ussi auparavant � le r�tablir chez quelque 85'000 usagers initialement plong�s, eux aussi, dans le noir.
La ligne de chemin de fer Brest-Quimper devait en principe rester ferm�e ce week-end, des dizaines d'arbres �tant tomb�s sur la voie.
Au Portugal aussi, des vents violents et des chutes de neige ont entra�n� samedi l'annulation de plusieurs vols et la fermeture d'une dizaine de routes de montagne sur l'archipel de Mad�re, ont indiqu� les services de secours. (ats/afp/Newsnet)
Cr��: 16.02.2014, 11h21
