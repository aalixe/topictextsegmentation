TITRE: Commerce ext�rieur : Nicole Bricq promet des attributions rapides de visas aux hommes d'affaires �trangers
DATE: 2014-02-16
URL: http://www.boursier.com/actualites/economie/commerce-exterieur-nicole-bricq-promet-des-attributions-rapides-de-visas-aux-hommes-d-affaires-etrangers-23032.html
PRINCIPAL: 0
TEXT:
Commerce ext�rieur : Nicole Bricq promet des attributions rapides de visas aux hommes d'affaires �trangers
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � La Ministre du Commerce ext�rieur, Nicole Bricq, a indiqu� lors d'un entretien de presse que la France souhaiterait que le trafic de marchandises gagne en fluidit� au passage en douane. Le temps pass� par les entreprises en formalit�s douani�res et �galement les contraintes seraient att�nu�s. Par ailleurs, la Ministre du Commerce ext�rieur a pr�n� davantage d'ouverture de la France � l'�gard des hommes d'affaires �trangers. La Ministre voudrait ramener � quelques jours les d�lais d'obtention de visas les concernant.
�"Nous pouvons faire beaucoup plus rapidement"
"Un homme d'affaires chinois qui veut venir en France, il lui faut 8 semaines pour avoir un visa. Un homme d'affaires russe qui veut venir en France, il lui faut 3 semaines. Nous pensons que nous pouvons faire beaucoup plus rapidement et ne plus parler en semaines, mais en journ�es", a d�clar� Nicole Bricq lors du Grand rendez-vous Europe1/ Le Monde/ i-T�l�vision. La Ministre a d'ailleurs sp�cifi� : "Le Minist�re des Affaires �trang�res a d�j� donn� des ordres tr�s pr�cis dans certains Consulats pour traiter cela en heures".
Motus...
Ces mesures feraient partie du package propos� dans le cadre du Conseil strat�gique de l'attractivit� que pr�sidera, demain lundi, Fran�ois Hollande. Il "n'avait pas �t� r�uni depuis quelques ann�es", a rappel� Nicole Bricq, qui s'est bien gard�e lors de l'�mission de tout impair quant � la pr�s�ance pr�sidentielle. La Ministre du Commerce ext�rieur n'a pas d�taill� davantage les engagements qui pourraient �tre annonc�s par le Chef de l'Etat � cette occasion. Elle a cependant assur� que la r�flexion du Conseil veillerait � "faciliter la vie des entreprises et des entrepreneurs".
Alexandra Saintpierre � �2014, Boursier.com
