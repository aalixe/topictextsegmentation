TITRE: Bruno Trespeuch part � Sotchi soutenir sa fille Chlo�
DATE: 2014-02-16
URL: http://www.ouest-france.fr/bruno-trespeuch-part-sotchi-soutenir-sa-fille-chloe-1935608
PRINCIPAL: 178112
TEXT:
Bruno Trespeuch part � Sotchi soutenir sa fille Chlo�
Saint-Jean-de-Monts -
14 F�vrier
Bruno Trespeuch part pour Sotchi soutenir sa fille, Chlo�, qui participe aux Jeux olympiques 2014, dans l'�preuve de snowboard cross f�minin.�|�
Facebook
Achetez votre journal num�rique
Le ski � 3 ans
Dimanche marquera la cons�cration de tout le travail accompli par Chlo� Trespeuch dans sa jeune carri�re sportive. Elle participera � l'�preuve f�minine de snowboard cross aux Jeux olympiques de Sotchi (Russie).
� 19 ans, bient�t 20, cette jeune fille, Montoise par son p�re et Savoyarde par sa m�re, a d�marr� tr�s jeune sur la neige. � Comme nous naviguions entre les Alpes � la saison hivernale, et Saint-Jean-de-Monts � la saison estivale et, comme tous les enfants de la station, Chlo� a commenc� le ski alors qu'elle �tait � peine �g�e de 3 ans �, raconte Bruno Trespeuch, son p�re.
Le snowboard
� Elle a pris quelques cours de snowboard avec un moniteur qui enseignait la pratique avec un b�ton. Puis j'ai emmen� Chlo� avec moi, mais sans b�ton �, poursuit-il. En grandissant, elle a tr�s vite �t� rep�r�e par des moniteurs � car elle poss�dait des facilit�s en snowboard �. Apr�s les comp�titions d�partementales, puis r�gionales, Chlo� est pass� aux �preuves nationales et a obtenu � le titre de championne de France plusieurs fois �.
Vice-championne du monde junior
Si bien qu'en 2013 elle est vice-championne du monde junior et quatri�me mondiale chez les seniors.
En d�cembre 2013, elle se place m�me quatri�me en ouverture de la coupe du monde � Montafon (Autriche). Apr�s � un d�but de saison prometteur, elle doit gagner une place pour �tre sur le podium �, note son p�re. Chose possible car � elle a cet esprit de comp�tition familial � : son fr�re, L�o, est champion du monde universitaire 2007, en snowboard cross, et sa soeur Lilou a d�j� particip� par deux fois au championnat de France d'�quitation en CSO.
Le SMS arriv� de Sotchi
� Chlo� est arriv�e mardi � Sotchi. Dans son SMS, elle dit que tout est magnifique et qu'elle est contente d'�tre l�-bas. Elle est particuli�rement d�tendue car il n'y a pas de manche qualificative et la descente s'effectue � six, ce qui la motive. Apr�s les deux descentes chronom�tr�es qui vont d�terminer les places des 24 concurrentes sur la grille de d�part, elle doit se situer parmi les trois meilleures pour la demi-finale puis la descente finale. �
Ils seront nombreux � la soutenir depuis Val-Thorens (Savoie), mais aussi � Saint-Jean-de-Monts du c�t� du Petit Sochard o� r�side son p�re. � Je la rejoins avec L�o et ses cousins � Sotchi pour l'encourager. Lilou va assurer la permanence � Saint-Jean, tout comme les deux chevaux de Chlo�, Odyss�e et Vend�me. �
Dimanche 16 f�vrier, de 8 h � 11 h, course retransmise en direct sur �cran g�ant au Saint-Martin, chemin du Petit Sochard. Gratuit.
Lire aussi
