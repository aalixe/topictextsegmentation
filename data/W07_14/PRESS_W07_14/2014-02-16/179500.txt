TITRE: Ligue 1: Revivez Saint-Etienne-Marseille en live comme-�-la-maison (1-1, score final) - 20minutes.fr
DATE: 2014-02-16
URL: http://www.20minutes.fr/sport/football/1300410-20140216-ligue-1-suivez-saint-etienne-marseille-live-comme-a-la-maison-des-20h50
PRINCIPAL: 0
TEXT:
FOOTBALL - Fin de la 25e journ�e de Ligue 1...
SAINT-ETIENNE - MARSEILLE: 1-1
Brandao (90e) ; Nkoulou (63e)
93': C'est fini! L'OM et Saint-Etienne se quittent bons amis. C'est dommage pour l'OM, mais le nul est �quitable.
90': EGALISATION DE BRANDAOOOOOOO!!!! Il saute plus haut que Mendes pour coller une t�te parfaite
89': Diawara sauve la baraque! Quelle t�te d�fensive...
85': Brandaoooo la remise de la t�te!!!! Non, il est hors-jeu. Et de toute fa�on il y avait personne au centre.
82': Oh Gignaaaac! Il dribble toute la d�fense mais n'arrive pas � battre Ruffier....
80': Penalty sur Bayal Sall??? Non, pas siffl�. Pourtant le d�fenseur de Saint� semblait ceintur� dans la surface...
77': Saint-Etienne est incapable de faire le pressing ou de mettre de la vitesse dans le jeu pour l'instant.
74': Pour l'instant, l'OM tient son tr�s gros coup. Bon, c'est pas forc�ment m�rit�, mais si le m�rite servait � quelque chose en foot, �a se saurait.
70': Mathieu Valbuena sort. Petit match pour lui. Thauvin entre.
66': On m'annonce aussi le sosie Fredy Fautrel (l'arbitre du match) avec Jean-Marie Bigard. Je ne sais plus quoi faire.
62': BUUUUT DE L'OM !!!! Nkoulou ouvre le score d'une jolie reprise au deuxi�me poteau. Gros probl�me de marquage.
61': Oh Gignac qu'est ce que tu as tent�?????? Un ciseau � l'horizontal de l'ext�rieur du pied, tout �a pour pas mettre son pied gauche.
58': On a perdu un peu de rythme l�. C'est chiant, osons le dire.
54': Je reste dubitatif.
50': Carton pour Morel qui gratte le tibia d'Hamouma. Sinon, � ma droite, on m'indique que Lucas Mendes aurait des faux airs de Donald Sutherland dans Mash. Culture.
48': Rah Brandao que tu es rustre techniquement. Il y avait une si bonne passe � faire � Tabanou...
46': Es geht los! C'est reparti
45': C'est la mi-temps! Belle entame de Saint-Etienne, mais pas de buts
43': Roh Gignac aurait pu la pousser au fond celle-l�. Ca y est, l'OM pointe le bout de son nez.
42': @Styvee: La pelouse est en progr�s � Saint�. Enfin, elle partait de tellement loin que c'est pas non plus le P�rou...
41': @Justice 94: Moi je le trouve plut�t pas mal ce match. Y a de la vie.
39': La derni�re recrue de l'OM, le lat�ral Dja Dj�dj�, envoie un centre hyper crade au troisi�me poteau. Il cherchait le corner.
36': L'OM a laiss� passer l'orage, comme on dit pas mal en Bretagne. Ca va un peu mieux pour les Olympiens, mais on voit trop peu Valbuena/Payet/Ayew. Encore moins Thauvin, mais sur le banc, c'est plut�t normal.
33': Quelle belle ambiance au chaudron encore. La meilleure de Ligue 1 cette ann�e sans soucis. Ca vaut presque un Marcel Michelin � moiti� plein.
29': La r�ponse de Gignac! Belle reprise, mais ca s'envole.
26': Tabanou nou nou, pouss' l'
ananas et mouds l'caf�. Sa frappe du droit est trop molle, tranquille pour Mandanda.
23': Frappe trop puissante de Lemoine. Dommage, il avait gratt� un super ballon.
20': @Unpeulas:
Cher ami, nos ressources en �tre humains ne nous permettaient pas de liver en m�me temps les JO et la Ligue 1. Maintenant que le journ�e � Sotchi est termin�e, nous livons l'affiche de Ligue 1. Rien d'anti-lyonnais, nous n'avons pas liv� non plus Lille cet apr�s-midi, ou le multiplex hier soir.
18': Diawara est � deux doigts de l'expulsion. Apr�s 18 minutes de jeu, c'est dire que l'OM prend l'eau.
15': Roh ce contr�le zidane de Gignac. Le porte manteau superbe.
13': Nkoulou sauve la baraque. Avec le m�me tacle que Diawara, mais sur le ballon.
11': Oh l'assassinat de Diawara sur Cohade! Il l'a d�truit! DETRUIT ! C'est qu'un jaune, et �a ne valait qu'un jaune, tr�s bien arbitr�.
9': Bon, pour l'instant, le 3-5-2 de l'OM c'est pas franchement une r�ussite.
7': Deferlente sur le but de l'OM! Gros arr�t de Mandanda sur Brandao.
4': Grosse pression des Vets pour l'instant. Les ailiers Hamouma Tabanou sont vifs et �a pose des probl�mes au 3-5-2 marseillais.
2': On avait dit pas le physique...
URGENT : aucun chauve dans le trio arbitral. Une premi�re depuis 2011 en Ligue 1. #ASSEOM
� Maxime Dupuis (@maximedupuis) February 16, 2014
1': C'est parti ! Les Verts en vert, l'OM en bleu.
20h59: Eh oui, l'OM joue en 3-5-2 mes amis. C'est une belle tentative de Jos� Anigo, � voir ce que �a donne sur le papier. Surtout sans Thauvin.
20h58: Les compos:
Saint�: Ruffier - Perrin, Zouma, Sall, Tr�moulinas - Lemoine, Cl�ment, Cohade - Hamouma, Tabanou, Brandao.
Marseille: Mandanda  Diawara - NKoulou - Mendes  Dja Djedje - Valbuena - Ayew - Romao - Payet - Morel Gignac
20h55: Salut � tous, c'est l'heure de Saint-Etienne - Marseille. Le choc de cette 25e journ�e de Ligue 1.
Ca pourrait �tre le match charni�re de leur saison.�Entre Saint-Etienne, 41 points (4e), et Marseille (5e), 39�points, il n'y a que deux points et une ambition partag�e d'accrocher une place europ�enne. Surtout si�Lille continue de d�gringoler, la troisi�me place et la Ligue des champions sont toujours accessibles pour ces deux clubs plut�t en forme derni�rement. A condition de remporter ce match dans un chaudron bouillantissime, comme toujours. Il va y avoir du spectacle, on vous le promet.
>> On se donne rendez-vous juste avant 21 heures pour suivre ce dixi�me et dernier match de la 25e journ�e de Ligue 1...
B.V.
