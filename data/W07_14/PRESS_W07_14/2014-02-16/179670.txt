TITRE: Valbuena: ''Il faut �tre exigeant'' - Football - Sports.fr
DATE: 2014-02-16
URL: http://www.sports.fr/football/ligue-1/articles/valbuena-doute-de-l-etat-d-esprit-des-jeunes-1011351/
PRINCIPAL: 0
TEXT:
Mathieu Valbuena apprécie les méthodes de José Anigo. (Reuters)
Par Arthur Houilliez
16 février 2014 � 20h35
Mis à jour le
16 février 2014 � 22h30
Mathieu Valbuena est devenu depuis son arrivée à Marseille, en juillet 2006, un élément clé de l’effectif phocéen. Une ascension qui s’est faite lentement pour le milieu de terrain offensif, grâce au travail et à la discipline. Une discipline que l’OM avait quelque peu perdu avant le retour de José Anigo aux affaires à en croire l'international tricolore...
Le meneur de jeu marseillais est de ceux qui pensent qu'il faut être exigeant pour réussir dans le football, et pour lui il faut l'être encore plus dans un club de l’envergure de Marseille . De passage au Canal Football Club, Mathieu Valbuena s'est attardé sur le retour de José Anigo à la tête des Olympiens à la place d’Elie Baup, et sur les méthodes de travail du nouveau coach marseillais : "José a su remettre beaucoup de discipline. C’est important. Quand on est dans un club comme l’OM ou il y a une certaine exigence, il faut déjà être exigent envers soi-même", a-t-il confié.
A l’instar des critiques de Laurent Blanc et Grégory van der Wiel envers les jeunes du PSG , "Petit Vélo" n’a pas hésité à égratigner les jeunes marseillais : "Je pense que parfois les jeunes ne prennent pas trop conscience de là où l’on se trouve, il faut être souvent derrière eux pour cette notion de travail, bosser, faire beaucoup plus". Critique envers les jeunes, Valbuena ne pensent pas pour autant qu’il y ait de problèmes dans le vestiaire phocéen : "Je trouve qu’il y a une bonne ambiance. Il y a forcément des personnalités différentes, il y a eu des altercations mais je pense que ça fait partie du métier".
Par ailleurs, si l'avenir de l'international français à l'OM est pour le moment un sujet tabou, le joueur de 29 ans se montre bien plus bavard quand on lui parle de l'équipe de France, pour laquelle il reste pétri d’ambitions avant de prendre la direction du Brésil pour participer à la Coupe du monde: "Je me sens bien dans cette équipe de France, les gens ont confiance en moi. On a vraiment hâte de faire quelque chose là-bas… Pourquoi pas la gagner !".
