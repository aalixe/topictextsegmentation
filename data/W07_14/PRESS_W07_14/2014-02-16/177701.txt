TITRE: Deux ans de prison pour un d�tenteur de 3.000 images p�dopornographiques - lacapitale.be
DATE: 2014-02-16
URL: http://www.lacapitale.be/936458/article/actualite/faits-divers/2014-02-15/deux-ans-de-prison-pour-un-detenteur-de-3000-images-pedopornographiques
PRINCIPAL: 177697
TEXT:
Deux ans de prison pour un d�tenteur de 3.000 images p�dopornographiques
R�daction en ligne
Un homme a �t� interpell� jeudi pour avoir t�l�charg� pr�s de 3.000 fichiers � caract�re p�dopornographique sur son ordinateur et condamn� � deux ans d�emprisonnement, dont un avec sursis, a-t-on appris samedi aupr�s de la gendarmerie du Val-d�Oise.
Capture d��cran Google Maps
Ecouen, Val-d�Oise (France)
Le domicile de cet homme �g� d�un quarantaine d�ann�es, r�sidant � Ecouen (Val-d�Oise), a �t� perquisitionn� apr�s une enqu�te des gendarmes de la cellule �Nouvelles technologies�, sp�cialis�e dans la cybercriminalit�.
�L�analyse de son ordinateur a permis la d�couverte d�environ 3.000 fichiers � caract�re p�dopornographique, que l�homme avait t�l�charg�s via une plateforme et qu�il partageait avec d�autres utilisateurs�, a d�clar� � l�AFP une source � la gendarmerie.
Jug� vendredi en comparution imm�diate, l�homme a �cop� de deux ans d�emprisonnement, dont un avec sursis. Il a �galement �t� inscrit au fichier judiciaire national automatis� des auteurs d�infractions sexuelles (Fijais).
�Il n�a pas �t� incarc�r� et aura prochainement rendez-vous avec un juge des libert�s et de la d�tention�, a pr�cis� la source.
La cellule �Nouvelles technologies� (N�Tech) enqu�te sur des faits en rapport avec le piratage informatique et la diffusion d�images pornographiques mettant en sc�ne des mineurs.
Les + lus
