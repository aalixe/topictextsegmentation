TITRE: Sotchi 2014 : Bode Miller en larmes apr�s sa m�daille de bronze du super-G - Jeux Olympiques 2013-2014 - Ski alpin - Eurosport
DATE: 2014-02-16
URL: http://www.eurosport.fr/ski-alpin/jeux-olympiques-sotchi/2013-2014/sotchi-2014-bode-miller-en-larmes-apres-sa-medaille-de-bronze-du-super-g_sto4138495/story.shtml
PRINCIPAL: 0
TEXT:
Sotchi 2014 : Bode Miller en larmes apr�s sa m�daille de bronze du super-G
Par�Nicolas SBARRA�le 16/02/2014 � 10:36, mis � jour le 16/02/2014 � 11:37 @NSbarra
Bode Miller n'a pu retenir ses larmes � l'arriv�e du super-G, o� il a pris la m�daille de bronze. L'Am�ricain, qui a connu une ann�e 2013 noire sur le plan personnel, est devenu le plus vieux m�daill� olympique en ski alpin.
�
AFP
�
Bode Miller est un roc qui peut se fissurer, un champion qui peut �tre submerg� par l��motion. Une fois sa m�daille de bronze du super-G confirm�e, dimanche matin � Rosa Khoutor, l�Am�ricain n�a pu retenir ses larmes dans l�air d�arriv�e, tombant dans les bras de son �pouse. Si "gagner cette m�daille est magnifique", comme il l�a confi� apr�s la course, il a �t� aussi �mu parce que "l'ann�e derni�re a �t� personnellement tr�s difficile". En l�espace de quelques mois, son �pouse a perdu leur enfant en cours de grossesse, il a subi le d�c�s de son jeune fr�re Chelone, qui aurait pu �tre � Sotchi en snowboardcross, et a perdu la garde de son fils.
�
�
Cette m�daille appara�t aussi comme une lib�ration sur le plan sportif. Apr�s ses �checs en descente (8e) et en super-combin� (6e), dont il �tait un des favoris, le skieur du New Hampshire abattait probablement sa derni�re carte de ces Jeux, et de sa carri�re. Miller ne sera s�rement pas � Pyeongchang mais est d�ores et d�j� devenu le plus vieux m�daill� olympique de l�histoire en ski alpin. A 36 ans et 127 jours, il a fait mieux que la l�gende Kjetil Andr� Aamodt, �g� de 34 ans et 169 jours lors de son quatri�me titre � Turin.
�
Six m�dailles olympiques en poche
Le champion am�ricain totalise d�sormais six breloques olympiques, devenant le deuxi�me sportif plus m�daill� de son pays aux JO d�hiver, derri�re Apolo Anton Ohno (short track) et � �galit� avec Bonnie Blair (patinage de vitesse). A une seule reprise, il s�est par� d�or, avec le combin� � Vancouver. Sur la deuxi�me marche du podium, il y est mont� trois fois, sur le combin� et le g�ant en 2002 et sur le super-G en 2010. A Sotchi, il a obtenu sa deuxi�me m�daille de bronze, lui qui s��tait content� de ce m�tal lors de la descente en 2010.
S�il est superstitieux, Bode Miller pourra s�attacher au num�ro 13, son dossard du jour. C�est d�j� celui qu�il portait lors des deux entra�nements de descente qu�il avait remport�s. Malheureusement pour lui, sur les �preuves de descente et de super-combin�, il l�avait c�d� pour le 15 et le 24. Il s��tait alors plaint d�une neige trop molle pour lui correspondre, tant les temp�ratures sont douces depuis le d�but des Jeux. Mais "l�homme chewing-gum", comme il est surnomm�, a su surmonter cette nouvelle �preuve. Pour finir en beaut�?
�
A propos de l'auteur
Nicolas SBARRA�-�Eurosport
Nicolas Sbarra collabore � Eurosport.fr depuis mars 2010. Sp�cialiste de football et de biathlon, il �crit aussi sur KateiaSport.com. @NSbarra
�
