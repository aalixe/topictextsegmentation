TITRE: Centre Presse : Une chance de podium en danse sur glace pour P�chalat et Bourzat
DATE: 2014-02-16
URL: http://www.centre-presse.fr/article-289848-une-chance-de-podium-en-danse-sur-glace-pour-pechalat-et-bourzat.html
PRINCIPAL: 177968
TEXT:
16/02/2014 03:59 | Sports | | Imprimer |
Une chance de podium en danse sur glace pour P�chalat et Bourzat
Les danseurs Nathalie P�chalat et Fabian Bourzat, m�daill�s mondiaux, se lancent dans leur dernier tour de bal ce soir lors des jeux Olympiques de Sotchi. Le bronze est � la port�e de ce duo explosif.
Pour la derni�re saison de leur carri�re, d�but�e il y a treize ans, les doubles champions d'Europe et m�daill�s de bronze mondiaux 2012 sont la seule vraie chance de m�daille pour les Bleus de la glace.
Avec un programme court remani� � souhait pour coller aux attentes des juges et un libre d�j� largement appr�ci� par ces m�mes juges, le duo peut compter sur ce bronze olympique.
Sauf si les deux couples russes, Bobrova - Soloviev et Ilinykh - Katsalapov, jouent les perturbateurs dans la course aux premi�res places, d�volues aux Am�ricains Davis - White et Canadiens Virtue - Moir.
L'�quipe de France de patinage artistique n'a plus ramen� de m�dailles des JO depuis douze�ans.
R�agissez !
