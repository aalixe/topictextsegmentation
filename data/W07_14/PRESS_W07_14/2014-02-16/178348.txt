TITRE: Saut � la perche: Lavillenie, �une ic�ne comme Federer, Bolt ou encore Messi� -  News Sports: Toute l'actu sports - tdg.ch
DATE: 2014-02-16
URL: http://www.tdg.ch/sports/actu/lavillenie-icone-federer-bolt-encore-messi/story/15858603
PRINCIPAL: 178339
TEXT:
Votre adresse e-mail*
Votre email a �t� envoy�.
�Lavillenie est devenu une ic�ne contemporaine, au m�me titre que Roger Federer, Michael Schumacher, Usain Bolt, Michael Phelps ou Lionel Messi�, estime ainsi L'Equipe dans son �ditorial dominical.
Le perchiste fran�ais n'a pas am�lior� n'importe quel record, mais bien la marque du �tsar de la gaule�, Serge� Bubka, figure embl�matique au m�me titre qu'un Carl Lewis ou qu'un Sebastian Coe des ann�es 80 et 90, p�riode glorieuse de l'athl�tisme. Qui plus est dans une des disciplines les plus t�l�g�niques qui soient, et sous les yeux de Bubka lui-m�me, grand manitou du meeting de Donetsk.
�Faire mieux que Bubka, c'est comme aller plus vite qu'Usain Bolt, avoir de meilleures stats que Michael Jordan. Il d�tr�ne une l�gende�, a d�clar� Jean Galfione, champion olympique de la perche en 1996. �Je suis sid�r�.�
Au-del� du record proprement dit, ce sont la personnalit� et la trajectoire de Lavillenie qui �tonnent et s�duisent. �Ce qui frappe, c'est sa joie de sauter. Il est partout, il encha�ne les comp�titions depuis le mois de novembre, il aime �a�, a d�clar� � Sportinformation Laurent Meuwly, entra�neur au Centre national de performance de Suisse romande � Aigle.
Un fan de vitesse
La recette du succ�s du Fran�ais, c'est l'enthousiasme, la recherche du frisson et de l'adr�naline: �Je suis anim� par la perche purement par passion, pas par la destin�e d'�tre champion et de vouloir gagner�, avait d�clar� l'Auvergnat l'an dernier.
Lavillenie est un fan de vitesse, sur le sautoir - o� sa v�locit� lui permet de compenser son relativement petit gabarit - et en dehors. Il est notamment un fondu de moto et a particip� en septembre dernier aux 24 Heures du Mans.
Grand champion, le Fran�ais n'�tait pas jusqu'� pr�sent une star majeure mais cela pourrait changer, au vu des r�actions des internautes. Moins de deux heures apr�s son saut, le site de l'equipe.fr recensait d�j� plus de 1200 commentaires sous la nouvelle annon�ant le record.
�Lavillenie remet l'athl�tisme sous les feux des projecteurs, c'est une bonne chose�, s'est f�licit� Bubka, beau joueur. �Ce serait super qu'il transmette mon h�ritage. Il a un immense amour, une passion pour ce sport merveilleux.�
Le perchiste fran�ais lui-m�me planait, au propre et au figur�, apr�s son exploit: �Ce sont des sauts comme �a qu'on recherche dans une vie, je me suis r�gal�, a-t-il exult�. �Ce fut comme une surprise de me voir au-dessus de la barre, sans beaucoup, beaucoup d'efforts. Ce record est tellement mythique.� (si/Newsnet)
Cr��: 16.02.2014, 10h54
