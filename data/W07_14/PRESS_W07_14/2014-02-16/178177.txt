TITRE: Audiences TV du dimanche 16 fevrier 2014 - News TV
DATE: 2014-02-16
URL: http://tele.premiere.fr/News-Tele/Audiences-TV-Plus-de-8-millions-de-fideles-devant-The-Voice-peu-de-monde-devant-la-Tnt-3953620
PRINCIPAL: 178174
TEXT:
Audiences TV: Plus de 8 millions de fid�les devant The Voice, peu de monde devant la Tnt
Audiences TV: Plus de 8 millions de fid�les devant The Voice, peu de monde devant la Tnt
16/02/2014 - 09h41
1
Pour ses derni�res auditions � l'aveugle, The Voice est rest� au-dessus des 8 millions de fid�les sur TF1. France 2 a r�sist� un peu avec son Plus grand cabaret du monde. De son c�t�, les cha�nes de la Tnt n'ont pas r�uni grand monde.
TF1 diffusait hier soir la 6e, et derni�re, cession des auditions � l'aveugle de la saison 3 de� The Voice, la plus belle voix . On s'en doute, le show a encore �t� pl�biscit� avec�8 072 000 t�l�spectateurs, soit 35.6% de parts d'audience. La semaine prochaine, les auditions laisseront la place aux battles et l'audience devrait un peu fl�chir.
Comme toujours, France 2 a pu compter sur� Le plus grand cabaret du monde �pour se frayer un place. Le divertissement a rassembl� 3 551 000 fid�les, soit 15.3% de pda.
France 3 compl�te le podium avec sa s�rie Le sang de la vigne suivie par 3 195 000 amateurs, soit 13.2% de pda.
De son c�t�, M6 et son� Hawaii 5-0 �ont r�uni 2 705 000 assidus, soit 11% de pda.
W9 s'impose en t�te des audiences de la Tnt, gu�re suivie hier soir, gr�ce aux �Simpson �qui ont ravi 915 000 fid�les, soit 3.9% de pda.
Le documentaire d'Arte,� La statue de la Libert� , a int�ress� 880 000 curieux, soit 3.6% de pda.
Sur France 5, le magazine Echapp�es belles a s�duit 644 000 personnes, soit 2.7% de pda.
De son c�t�, la s�rie de TMC,� New York, section criminelle , a r�uni�442 000 fid�les, soit 1.8% de pda.
Sur D8, le documentaire� Au coeur de l'enqu�te �a �t� vu par 424 000 personnes soit 1.8% de pda.
Le t�l�film de France 4, Jack Wilder et la myst�rieuse cit� d'or, a r�uni 410 000 curieux, soit 1.7% de pda.
Sur Gulli, le t�l�film Barbie et le secret des sir�nes a �t� regard� par 401 000 personnes, soit 1.6% de pda.
NRJ 12 et� Le super b�tisier de l'ann�e �ont amus� 268 000 t�l�spectateurs, soit 1.1% de pda.
Le magazine de NT1,� Chroniques criminelles , a r�uni�221 000 fid�les, soit 1% de pda.
Sur HD1, Julie Lescaut a f�d�r� 181 000 nostalgiques, soit 0.7% de pda.
D17 et Le zap ont rassembl� 141 000 personnes, soit 0.6% de pda.
Sur 6ter, Commissaire Cordier a r�uni 64 000 amateurs, soit 0.3% de pda.
Le documentaire de RMC D�couverte,� Alien Theory , a r�uni�71 000 personnes, soit 0.3% de pda.
Num�ro 23 ferme la marche avec� Les Bleus, premiers pas dans la police �vus par 42 000 fans, soit 0.2% de pda.
