TITRE: Corse. Un jeune homme tu� par balles � Ajaccio
DATE: 2014-02-16
URL: http://www.ouest-france.fr/corse-un-jeune-homme-blesse-par-balles-ajaccio-1936131
PRINCIPAL: 178630
TEXT:
Corse. Un jeune homme tu� par balles � Ajaccio
Corse -
16 F�vrier
L'affaire s'est d�roul�e sur un parking en bordure de la route � l'entr�e d'Ajaccio.�|�Infographie Ouest-France.
Facebook
Achetez votre journal num�rique
Un jeune homme gri�vement bless� par balles dimanche � l'entr�e d'Ajaccio (Corse-du-Sud) est d�c�d� dans l'apr�s-midi.
Un jeune homme est d�c�d� dimanche � Ajaccio (Corse-du-Sud) apr�s avoir �t� bless� par balles plus t�t dans la journ�e, dans un affaire qui rel�ve du diff�rend d'ordre personnel, a-t-on appris de source proche de l'enqu�te.
La victime, �g�e d'une trentaine d'ann�es, morte � l'h�pital d'Ajaccio, avait re�u plusieurs balles d'une arme de poing de calibre non pr�cis�, notamment au niveau de la t�te et du thorax, alors qu'elle se trouvait sur un petit parking en bordure de la route � l'entr�e d'Ajaccio.
Le tireur, �g� d'une soixantaine d'ann�es et plac� dimanche en garde � vue, est l'ex-beau-p�re de la victime. Il avait �t� interpell� sur les lieux du crime par un motard de la gendarmerie, apparemment sans opposer de r�sistance, a-t-on pr�cis�. Plusieurs t�moins ont assist� � cette fusillade, qui s'est d�roul�e aux alentours de 11H00.
Lire aussi
