TITRE: Berlinale : l'Ours d'or du meilleur film d�cern� � �Black Coal, Thin Ice� - Cin�ma - MYTF1News
DATE: 2014-02-16
URL: http://lci.tf1.fr/cinema/news/berlinale-l-ours-d-or-du-meilleur-film-decerne-a-black-coal-thin-8365968.html
PRINCIPAL: 178905
TEXT:
berlin , ours
News Cin�-S�ries La 64e �dition du festival de cin�ma de Berlin, la Berlinale, a d�cern� samedi soir son prestigieux Ours d'or � "Bai Ri Yan Huo" (Black Coal, Thin Ice) du r�alisateur chinois Diao Yinan.
C'est le film du r�alisateur Chinois Diao Yinan, Bai Ri Yan Huo (Black Coal, Thin Ice) qui a re�u la principale r�compense du festival de cin�ma de Berlin ce samedi. Film noir qui suit le parcours d'un policier enqu�tant sur une s�rie de meurtres, le film, dont Diao Yinan a �galement �crit le sc�nario, avait �t� salu� par les critiques pendant le festival, notamment en raison de ses audaces visuelles.
�
Le r�alisateur am�ricain Richard Linklater a obtenu samedi l' Ours d'argent du meilleur r�alisateur pour "Boyhood" qui retrace avec simplicit� et naturalisme la vie d'une famille am�ricaine. Le film est une exp�rience unique dans le cin�ma de fiction, car il a �t� tourn� en 39 jours sur une p�riode de douze ans avec les m�mes acteurs. Il a s�duit autant les critiques que le public berlinois.
�
Le palmar�s de la 64e Berlinale�:
�
�Ours d'or du meilleur film : Bai Ri Yan Huo (Black Coal, Thin Ice) de Diao Yinan (Chine)
�Grand prix du jury, Ours d'argent : The Grand Budapest Hotel de Wes Anderson (Etats-Unis)
�Ours d'argent du meilleur r�alisateur : Richard Linklater pour Boyhood (Etats-Unis)
�Ours d'argent de la meilleure actrice : Haru Kuroki pour Chiisai Ouchi (The little House) de Yoji Yamada (Japon)
�Ours d'argent du meilleur acteur : Liao Fan pour Bai Ri Yan Huo (Black Coal, Thin Ice, Chine)
�Ours d'argent de la meilleure contribution artistique : Tui Na (Blind massage) de Lou Ye (Chine)
�Ours d'argent du meilleur sc�nario : Kreuzweg (Chemin de croix) d'Anna et Dietrich Br�ggemann (Allemagne)
�Prix Alfred Bauer, en m�moire du fondateur du festival pour un film qui ouvre de nouvelles perspectives : Aimer, boire et chanter d'Alain Resnais (France)
�Prix du Meilleur premier film : G�eros d'Alonso Ruizpalacios (Mexique)
�Ours d'or du meilleur court m�trage : Tant qu'il nous reste des fusils � pompe de Caroline Poggi et Jonathan Vinel (France)
�Ours d'argent du court m�trage, Prix sp�cial du jury : Laborat de Guillaume Cailleau (France)
�Ours d'or d'honneur : Ken Loach (Grande-Bretagne)�
�
