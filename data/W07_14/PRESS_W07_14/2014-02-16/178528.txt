TITRE: Ellen Page : Ricky Martin, Bryan Singer, Anna Kendrick... les stars la soutiennent sur Twitter � metronews
DATE: 2014-02-16
URL: http://www.metronews.fr/people/ellen-page-ricky-martin-bryan-singer-anna-kendrick-les-stars-la-soutiennent-sur-twitter/mnbp!8QSd1KCiyfcw/
PRINCIPAL: 178527
TEXT:
Mis � jour  : 16-02-2014 15:41
- Cr�� : 16-02-2014 12:49
Ellen Page : les stars la soutiennent avec enthousiasme sur Twitter
COMING OUT - Vendredi dernier, l'actrice canadienne Ellen Page a r�v�l� son homosexualit� lors d'une manifestation organis�e par un mouvement gay et lesbien am�ricain. Sur Twitter, les stars ont �t� tr�s solidaires de la star de "Juno".
�
Ellen Page dans "Inception". Photo :� � Warner Bros. France
Courageux. C'est probablement l'adjectif qui revient le plus souvent pour qualifier le r�cent coming out d'Ellen Page. Le soir de la Saint-Valentin , l'actrice canadienne, 26 ans, a en effet choisi de d�voiler son homosexualit� au monde entier � l'occasion de la Time to Thrive Conference, une manifestation organis�e par un c�l�bre mouvement gay et lesbien am�ricain. "Je suis �puis�e de mentir et fatigu�e de me cacher (�) J'ai souffert pendant des ann�es parce que j'avais peur de le dire, a-t-elle d�clar�, des tr�molos dans la voix. Ma sant� mentale en p�tissait, mes relations�aussi".
Aux applaudissements du public ont tr�s vite succ�d� un grand nombre de messages de soutien sur les r�seaux sociaux. Il faut dire qu'il n'est pas tr�s courant qu'une jeune actrice dont la carri�re ne cesse de monter en puissance prenne un tel risque. Dans une industrie hollywoodienne "qui �rige d'�touffantes normes (...) de beaut�, de vie, de succ�s", comme l'a clam� Page, d'autres vedettes comme Jodie Foster ou Maria Bello ont attendu tr�s longtemps avant d'assumer pleinement et au grand jour leur orientation sexuelle .
Les stars approuvent
Solidaires, plusieurs stars ont soutenu l'interpr�te de Juno et Inception sur Twitter. Le com�dien Emile Hirsch, qui avait incarn� le militant LGBT Clive Jones dans le biopic Harvey Milk, a �crit�: "Bravo @EllenPage d'aider autant de gens dans leur lutte quotidienne en montrant courageusement l'exemple". Anna Kendrick a d�clar� �tre "raide raide raide raide raide raide dingue" de l'int�ress�e tandis que le chanteur Ricky Martin a confi�: "Je suis tr�s heureux pour toi�!!! Tu es libre !".
Patrick Wilson, qui lui a donn� la r�plique dans le thriller Hard Candy, l'a f�licit�e tout en saluant son "incroyable discours". Speech qu'a �galement diffus� le r�alisateur Bryan Singer, qui a dirig� Page dans X-Men�: days of Future Past, en salles le 21 mai prochain . Dans un genre plus enjou�, Jason Biggs, l'interpr�te du cultissime Jim Levenstein dans la saga American Pie, a balanc�: "Fuck les jeux Olympiques, les enfants. Si vous cherchez des h�ros, n'allez pas chercher plus loin qu'Ellen Page".
Les r�seaux sociaux s'emballent
Outre les r�actions de Kristen Bell, Kate Mara, Sarah Paulson, Shannon Woodward ou Jesse Tyler Ferguson, le h�ros de la s�rie Modern Family , c'est la twittosph�re toute enti�re qui a trembl� pour acclamer Ellen Page. Le nom de l'actrice s'est tr�s rapidement retrouv� en trending topics sur le site de micro-blogging. "H�ro�que", "�mouvante", "bouleversante", "magnifique" : les superlatifs se sont bouscul�s au portillon au lendemain du fameux discours.
Maintenant que cette �tape est pass�e, Ellen Page peut d�sormais se concentrer � 100% sur son m�tier d'actrice. Prochain projet en ligne de mire ? Freeheld, un biopic bas� sur le documentaire homonyme et oscaris� sorti en 2007. Il retracera le parcours de Laurel, une polici�re en phase terminale d�un cancer, incarn�e par Julianna Moore, qui se bat en faveur... des droits des homosexuels. Ellen Page, plus lib�r�e que jamais, se glissera sous les traits Stacie Andree, la petite-amie de Laurel. On a h�te de d�couvrir �a.
�
