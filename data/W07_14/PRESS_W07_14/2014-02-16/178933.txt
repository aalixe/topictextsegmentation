TITRE: Sotchi : la mass start report�e, Martin Fourcade va devoir patienter � metronews
DATE: 2014-02-16
URL: http://www.metronews.fr/sport/sotchi-la-mass-start-reportee-martin-fourcade-va-devoir-patienter/mnbp!dp9P7zRs8pIBg/
PRINCIPAL: 178926
TEXT:
Mis � jour  : 16-02-2014 17:37
- Cr�� : 16-02-2014 16:35
Sotchi 2014 : Fourcade va devoir patienter
JEUX OLYMPIQUES � La mass start du biathlon a �t� report�e pour cause de brouillard. Martin Fourcade, qui esp�re remporter son troisi�me titre olympique � Sotchi, devra attendre lundi matin.
�
Tirer � la carabine avec cette brume, �a aurait �t� un peu dangereux... Photo :�ALBERTO PIZZOLI / AFP
Qui a dit qu'il faisait beau et chaud � Sotchi ? Envahi par la brume, le parc de Laura a �t� d�clar� impraticable par les organisateurs qui ont donc report� la course de biathlon du jour, la mass start.
Une course qui int�ressait les Fran�ais au plus haut point puisque Martin Fourcade en est le grand favori. En cas de victoire dans cette �preuve, le Catalan �galerait le record de Jean-Claude Killy de trois m�dailles d'or sur la m�me olympiade.
Nouveau d�part, lundi � 7 heures
Ce n'est que partie remise pour le Fran�ais qui prendra finalement le d�part lundi matin. Il faudra cependant r�gler les r�veils puisque les biathl�tes s'�lanceront � 7 heures.
Ce changement d'horaire, ne devrait pas perturber les athl�tes qui sont habitu�s � la m�t�o capricieuse tout au long de la saison de Coupe du monde.
Yann Butillon
