TITRE: Rugby. Pro D2. Le SUA en qu�te de plaisir... - 16/02/2014 - LaD�p�che.fr
DATE: 2014-02-16
URL: http://www.ladepeche.fr/article/2014/02/16/1819691-rugby-pro-d2-le-sua-en-quete-de-plaisir.html
PRINCIPAL: 178597
TEXT:
Rugby. Pro D2. Le SUA en qu�te de plaisir...
Publi� le 16/02/2014 � 09:31
20e journ�e.Narbonne-Agen
Au match aller, Narbonne avait explos� � Armandie (51-6)... Le RCNM sera forc�ment revanchard./Photo Mazet
Du plaisir� Retrouver du plaisir. Mathieu Blin, manager et R�my Vaquin pour le SUA et m�me le capitaine de Narbonne, S�bastien Petit, auront prononc� cette phrase � quelques heures du rendez-vous en terre audoise. C�est dire si les deux �quipes en pr�sence aujourd�hui � l�heure de la sieste ou du caf�, c�est selon, sur la pelouse du Parc des Sports et de l�Amiti� auront envie d��vacuer, vite, leurs contre-performances respectives de la dix-neuvi�me journ�e.
Agen aux abonn�s absents � Bourgoin, sans munition et donc sans solution ; Narbonne, pourtant bien approvisionn� en ballons, mais en manque d�inspiration face � une d�fense f�roce � Mont-de-Marsan. Malgr� les points de bonus d�fensifs ramen�s, RCNM et SUA ont envie de remettre les pendules � l�heure. Agen aura-t-il les moyens, les armes, pour mettre � mal une �quipe qui a domin� La Rochelle, Lyon ou Pau chez elle. Une �quipe qui est invaincue � domicile ?
Du c�t� des hommes forts du SUA, on pourrait avoir retenu que la m�l�e narbonnaise a souffert dans les Landes et a donc �t� p�nalis�e. Mais Agen n�offre pas beaucoup de garanties dans le secteur et puis, il manquera Ars�ne Nnomo, alors�
Champs de patates  de Rajon
Catastrophique � Bourgoin, on n�imagine mal l�alignement agenais passer encore une fois � c�t�, m�me s�il pas certain du tout que Ross Skeate bless� � une cheville � l�entra�nement en d�but de semaine soit align� (la d�cision d�finitive doit �tre prise � l��chauffement avant le match).
Priver de munitions les Narbonnais, para�t tout simplement utopique. Sur une pelouse qui devrait �tre aux antipodes du champ de patates de Rajon, � Bourgoin, les Audois, troisi�me attaque de la Pro D2 (Agen 2e) ne vont rien changer � leurs habitudes. Il y aura du mouvement. Probablement quand m�me une bonne nouvelle pour les suavistes qui ne manquent pas d�argument quand il s�agit de faire voler le cuir jusqu�aux ailes.
Le tout est de savoir si pour retrouver du plaisir, comme ils disent, Ils seront pr�sents sur les rendez-vous, les premiers dans le combat. � Bourgoin, Agen avait courb� l��chine d�j� sur le coup d�envoi.
Le SUA face � un des cadors de la Pro D2, joueur comme lui, devra absolument bonifier et surtout ne pas perdre ou rendre ses munitions.
La m�thode australienne semble porter ses fruits en septimanie. Du jeu, beaucoup de jeu. Une m�thode bien agenaise. L�adversaire est de taille, mais Agen peut et doit �tre � sa hauteur� pour retrouver du plaisir.
SUA.R�my Vaquin.3e ligne
�Ce qu'on a envie, c'est d'exister�
Le troisi�me ligne suaviste R�my Vaquin promet une r�action du SUA aujourd�hui, face � une �quipe narbonnaise qui pratique un rugby beaucoup plus complet que celui  des Berjaliens le week-end dernier.
R�my Vaquin, je pense que vous �tes conscient que le SUA est pass� � c�t� � Bourgoin. Que peut-on attendre de la rencontre � Narbonne ?
La r�ponse est facile� Ce sont des intentions dans le combat et dans le jeu. Ce ne sera pas le m�me temps, pas la m�me formation. Il va falloir que l�on se d�place beaucoup face � une tr�s belle �quipe. Le week-end dernier, nous n�avons pas �t� au niveau. On a �t� attentistes, �a n�est pas nous, �a n�est pas notre �quipe et �a ne nous r�ussit pas.
Narbonne, c�est l��quipe dont on parle en ce moment. Qu�en pensez-vous ?
Elle le m�rite . C�est une �quipe qui en voie du jeu, qui est propre dans le combat, c�est une formation assez compl�te.
�Il y a le pragmatisme australien�
La m�thode australienne aurait-elle du bon (les entra�neurs du RCNM sont Australiens) ?
On va dire qu�il y a un peu des deux. Il y a le pragmatisme australien, c�est minutieux, c�est bien fait. Et apr�s, ils ont des joueurs qui font la diff�rence. Une superbe ligne de trois-quarts, des avants tr�s mobiles, c�est plaisant.
Le fait que personne, pas plus Lyon que La Rochelle ou Pau n�y  ait gagn�, �a vous inspire quoi ?
Bien s�r qu�on a envie d�y gagner, comme tous les matchs. Mais c�est trop al�atoire. D�abord ce qu�on a envie, c�est d�exister et puis� si on existe, on verra ce qui passera. Il faut que nous retrouvions du plaisir, ce qui n�a pas �t� le cas, le week-end dernier.
Le pack de Narbonne ne fait pas partie des plus costauds du championnat. Qu�en pensez-vous ?
Je pense qu�ils en font une force.  Ils ne sont pas plus gaillards que les autres mais ils sont rapides. Ils ont un paquet d�avants qui se d�place beaucoup avec une grosse premi�re ligne. En plus il y a du banc. �a n�est pas une �quipe massive comme peuvent l��tre La Rochelle ou Lyon, mais ils envoient un autre jeu... Et ils ont l��quipe qui va avec.
L'ADVERSAIRE.S�bastien Petit.Pilier, capitaine
�Rapidit� et fluidit� dans le jeu,  l'�quipe est faite pour �a�
Ne rien regretter, voil� ce qu�esp�re le pilier S�bastien Petit, capitaine du RNCM, qui arr�tera (il a 35 ans) � la fin de la saison. Il le dit, le r�p�te Narbonne n�a pas l�ambition de monter en Top 14, juste progresser avec un effectif tr�s compl�mentaire, fait de jeunes, d�anciens sous la houlette d�un staff australien qui imprime sa m�thode� Du jeu, beaucoup de jeu, et �a paye.
S�bastien Petit, comme se pr�sente la rencontre face au SUA ?
On a beaucoup travaill�, peaufin� les derniers d�tails. On est press� d��tre � dimanche (aujourd�hui, N.D.L.R.).
Pourquoi press� ?
Press� parce qu�on rencontre une grosse �quipe et parce que le week-end dernier, nous n�avons pas �t� tr�s performants (d�faite � Mont-de-Marsan 15 � 10), donc on a toujours envie de reprendre vite et de retrouver du plaisir.
Vous avez quand m�me ramen� un point de bonus d�fensif des Landes, c�est mieux que rien n�est-ce pas ?
On aurait pu rentrer bredouille, mais nous sommes une �quipe qui ne l�che rien, c�est pour �a que nous sommes revenus avec ce point de bonus d�fensif qui est important.
Vous avez domin� le leader Lyon (17e journ�e, 22 � 6). Peut-on parler de match abouti ce jour-l� pour le RCNM ?
Oui, mais il y en a eu d�autres. Quand on rencontre des �quipes qui se rapprochent du Top 14, on a toujours envie d��lever notre niveau de jeu.
Votre �quipe avait encaiss� plus de cinquante points (51-6) � Armandie au match aller. Ce sera une source de motivation ?
C�est le seul match que j�ai manqu�, j�avais une infection, j��tais � l�h�pital. On en a pris cinquante points, c�est la seule fois. �a ne nous est jamais arriv� cette saison. Face au SUA, ce qui sera important, sera d�abord l a qualit� de notre performance.
C�est la m�thode australienne, voil� ce qu�on entend souvent quand on parle de votre �quipe. Qu�en pensez-vous ?
Ce sont des s�quences de jeu qui durent assez longtemps. Il n�y a pas de pause. Si un pilier peut faire la passe comme un neuf, il l�a fait. Rapidit� et fluidit� dans le jeu. L��quipe est faite pour �a. Des fois on est difficult� devant par rapport � la densit� physique, mais on compense par la vivacit�.
Narbonne a l�ambition de monter en Top 14 ?
Non, franchement non. Nous sommes juste partis pour faire mieux que l�an pass� (9es). L�objectif, c�est de progresser.
Textes et interviews J.-P. D.
�|�
