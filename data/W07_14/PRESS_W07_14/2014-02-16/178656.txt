TITRE: SOTCHI | Le relais fran�ais d�croche le bronze en ski de fond - France Info
DATE: 2014-02-16
URL: http://www.franceinfo.fr/sports/sotchi-1320823-2014-02-16
PRINCIPAL: 178654
TEXT:
Imprimer
Le ski de fond tricolore a enfin �t� r�compens� collectivement avec  le bronze du relais messieurs des JO de Sotchi. � Maxppp - Kay Nietfeld
L'�quipe masculine tricolore du relais 4x10 km en ski de fond a remport� la m�daille bronze ce dimanche � Sotchi. Jean-Marc Gaillard, Maurice Manificat, Robin Duvillard et Ivan Perrillat Boiteux apportent � la d�l�gation bleue sa 6e m�daille olympique depuis le d�but de ces Jeux.
Quatri�me lors des derniers JO en 2010 � Vancouver, quatri�me aussi en 2006 � Turin, l'�quipe tricolore du relais messieurs 4x10 km d�croche la m�daille de bronze � Sotchi, la deuxi�me de l'histoire du ski de fond fran�ais, apr�s la m�daille d'argent de Roddy Darragon en sprint � Turin en 2006.
Les Fran�ais ne partaient pas favoris
Jean-Marc Gaillard, Maurice Manificat, Robin Duvillard et Ivan Perrillat Boiteux, qui avaient fait ce podium olympique leur objectif majeur de la saison, ont donc relev� le d�fi.�Pourtant, la France ne faisait pas partie des favorites pour cette �preuve, dont on s'attendait qu'elle soit domin�e par trois nations : la Norv�ge, la Su�de et la Russie. Un pronostic d�jou� par la d�faillance de la Norv�ge, qui termine � la quatri�me place, � plus de 30 secondes des Fran�ais. Des tricolores qui finissent donc troisi�mes, derri�re la Russie, deuxi�me, et la Su�de, m�daille d'or.
"C'est une cons�cration pour l'�quipe" (Maurice Manificat)
Mais la contre-performance des Norv�giens n'explique pas tout. Il fallait aussi une superbe course du relais Fran�ais, �a a �t� le cas, toujours dans les avant-postes, dans les deux, trois ou quatre premi�res places tout au long des 40 kilom�tres de course. Des Fran�ais qui ont r�ussi � tenir la distance et qui s'adjugent donc la troisi�me marche du podium.
La r�action de Maurice Manificat, un des quatre relayeurs fran�ais : "C'est fabuleux, c'est une cons�cration pour l'�quipe"��
