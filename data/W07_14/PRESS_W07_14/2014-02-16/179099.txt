TITRE: Omnisport | JO Sotchi - Biathlon : La mass-start report�e � lundi
DATE: 2014-02-16
URL: http://www.le10sport.com/omnisport/jo-sotchi-biathlon-la-mass-start-reportee-a-lundi136254
PRINCIPAL: 179096
TEXT:
Envoyer � un ami
L'�preuve mass-start hommes a �t� report�e � lundi matin en raison d�un �pais brouillard qui a envahi le plateau de Laura. Le d�part sera donn� � 7h30 (heure fran�aise) sous r�serve de confirmation du programme par le CIO. Martin Fourcade devra patienter encore un peu avant de partir � la conqu�te d�un troisi�me titre olympique.
