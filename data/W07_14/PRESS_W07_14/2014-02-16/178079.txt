TITRE: Sports | Briand, le �??supersub�??
DATE: 2014-02-16
URL: http://www.ledauphine.com/sport/2014/02/16/briand-le-supersub-knzp
PRINCIPAL: 178078
TEXT:
Skichrono
ligue 1 (25e  JOURN�?E) - Lyon �?? Ajaccio, 14 heures (beIN SPORT) Briand, le �??supersub�??
Jimmy Briand, buteur face à Lens jeudi, sera à nouveau remplaçant aujourd�??hui contre Ajaccio en L1.
Super-remplaçant, voilà comment pourrait être qualifié le profil de Jimmy Briand. Cantonné sur le banc, il se montre souvent décisif quand il en sort. Cela ne sera probablement pas suffisant pour qu�??il soit encore olympien la saison prochaine. Mais cela ne le traumatise pas.
Au fait, à l�??OL il n�??y a pas que Bafé Gomis et Rémi Garde qui sont en fin de contrat et qui alimentent la chronique de leur futur. Il y a aussi Jimmy Briand arrivé entre Rhône et Saône en juin 2010. Sauf que le cas de l�??ancien Rennais ne mobilise pas les interrogations. Un anonymat qui lui sied parfaitement. « On ne parle pas trop de moi ? Tant mieux », assure l�??international aux cinq sélections.
Après ses quatre saisons lyonnaises, il y a fort à parier que l�??attaquant ne poursuivra pas son aventure à Gerland. « Aucune discussion n�??est ouverte pour une prolongation », assène-t-il. On n�??est pas loin de croire que ce manque d�??entrain de son club lui va plutôt bien.
Pas de prolongation en vue
Exemplaire dans sa conduite, Briand a été quasi forcé d�??accepter un départ à Monaco en janvier 2013, qui a finalement capoté. Avant de connaître une période de représailles en mode lofteur. Rebelote à l�??entame de cette saison avec des bruissements de départ Outre-Manche. « Cet été a été assez compliqué. Je ne veux pas me plonger dans mon avenir », complète-t-il.
C�??est davantage dans le travail qu�??il s�??est immergé. Il n�??est pas du genre à aller taper à la porte du coach pour demander des explications et a retrouvé le maillot olympien à la sixième journée contre Nantes (3-1, dont un but de �??Jimbo�??). « Mon but, c�??est évidemment de jouer un maximum. Je prendrai tout ce qu�??on me donnera », assure celui qui voit ses deux potes Gomis et Lacazette avoir davantage la confiance du coach. « Les faits sont là. Ils jouent et ils marquent. Il n�??y a pas de jalousie ; je ne suis pas envieux. J�??attends juste mon heure. »
Se complaît-il dans ce rôle de �??supersubstitute�?? (super-remplaçant) à la Ole Gunnar Solskjaer, l�??ancien joker permanent aux stats détonantes de Manchester United ? « Je suis souvent sur le banc mais je ne me considère pas comme un remplaçant », coupe-t-il.
Des stats honorables
Sur les 22 matches qu�??il a disputés, il en a débuté seulement 13. Mais affiche des statistiques tout à fait honorables : 5 buts et 4 passes décisives. Il a des ratios proches de ceux de Gomis. « En jouant moins, ce n�??est pas phénoménal mais j�??ai rentabilisé mon temps de jeu. »
Il a même fait mieux en donnant la victoire dans le derby à Saint-Etienne le 10 novembre (2-1). �?a lui vaut la mansuétude du Virage nord qui lui a même dédié un chant, certes de mauvais goût, mais un chant quand même. « Je ne m�??arrête pas à ce but, cela ferait léger dans une saison même si je sais qu�??il a fait plaisir », convainc Jimmy Briand.
Cet après-midi, il a peu de chance d�??être titulaire. Mais il s�??en accommodera comme d�??habitude. En remplaçant de luxe.
Gourcuff trop justeRémi Garde espérait récupérer plusieurs éléments forfaits contre le Lens. Ce sera le cas pour Bisevac, qui purgera ensuite une suspension de deux matches, et Fofana qui en ont fini avec leurs soucis de santé. Les deux devraient d�??ailleurs débuter. Ce ne sera pas le cas en revanche de Gourcuff, encore trop juste et en froid avec ses adducteurs. Benzia et Dabo sont toujours en phase de reprise. Le technicien a rajeuni son groupe en ne convoquant pas Danic et Malbranque et en accueillant Bahlouli et Fekir. Tolisso a été préféré à Zeffane. Le 4-4-2 en losange sera encore une fois de mise.
Par Olivier DIETLIN | Publié le 16/02/2014 à 06:00
Vos commentaires
Connectez-vous pour laisser un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
