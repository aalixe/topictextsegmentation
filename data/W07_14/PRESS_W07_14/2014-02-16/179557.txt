TITRE: Immigration: �Erasmus� et recherche gel�s, l'UE riposte � la Suisse -   Suisse - lematin.ch
DATE: 2014-02-16
URL: http://www.lematin.ch/suisse/Bruxelles-suspend-la-participation-suisse-au-programme-de-recherche/story/13242267
PRINCIPAL: 179528
TEXT:
Votre adresse e-mail*
Votre email a �t� envoy�.
La Suisse suspend l'accord pour l'extension de la libre circulation des personnes � la Croatie qu'elle estime ne pas pouvoir signer sous sa forme actuelle. Bruxelles a de son c�t� mis � ex�cution sa menace et g�le les n�gociations sur la participation suisse aux programmes �Horizon 2020� (recherche) et �Erasmus� (�tudes � l'�tranger).
Samedi, la conseill�re f�d�rale Simonetta Sommaruga a expliqu� � la ministre croate des Affaires �trang�res Vesna Pusic lors d'une conversation t�l�phonique que la Suisse suspend l'accord pour l'extension de la libre circulation des personnes � la Croatie, a dit � l'ats un porte-parole du D�partement f�d�ral de justice et police (DFJP), Philippe Schwander.
Apr�s le �oui� � l'initiative de l'UDC �Contre l'immigration de masse�, Berne ne peut pas le signer sous sa forme actuelle, a-t-elle d�clar�. La discussion portait sur les cons�quences de ce vote ainsi que sur les prochains d�veloppements.
Accord impossible dans sa forme actuelle
Simonetta Sommaruga a notamment expliqu� que la nouvelle disposition constitutionnelle est directement applicable et qu'elle ne permet pas de conclure un accord qui s'y oppose. Comme le protocole avec la Croatie pr�voit une libert� de circulation compl�te apr�s 10 ans, il ne peut �tre sign� sous cette forme, a pr�cis� Philippe Schwander.
Le Conseil f�d�ral r�fl�chit � des solutions qui ne discrimineront pas ce petit pays d'Europe de l'Est. La ministre croate a pris acte de ces informations, a ajout� le porte-parole. La conseill�re f�d�rale socialiste a convenu de rester en contact avec elle de mani�re proche et directe.
Pour l'ambassadeur de Croatie � Berne, Aleksandar Heina, il est urgent de trouver un accord. �Pour la Croatie, il est inacceptable d'�tre consid�r� durant les trois prochaines ann�es comme un Etat tiers et pas comme un Etat de l'UE�, a-t-il dit dans la NZZ am Sonntag .
Apr�s l'annonce de samedi, un porte-parole de la Commission europ�enne a dit dimanche, confirmant une information de la RTS , que Bruxelles a d�cid� de geler les n�gociations pour la participation de la Suisse aux programmes de recherche �Horizon 2020� et d'�change acad�mique �Erasmus�.
Accords importants pour la Suisse
Plusieurs responsables europ�ens, dont l'ambassadeur de l'UE en Suisse Richard Jones, avaient menac� d'une telle sanction en cas de suspension du processus de ratification de l'accord avec Zagreb. Bruxelles consid�re que �Horizon 2020� et �Erasmus� sont li�s � la libre circulation.
La prochaine rencontre entre les deux interlocuteurs aura lieu jeudi prochain. Le secr�taire d'Etat aux affaires �trang�res Yves Rossier se rendra alors � Bruxelles pour discuter avec son homologue europ�en David O'Sullivan.
Au moment de son lancement en janvier dernier, la Suisse avait dit qu'elle esp�rait cr�er 8000 emplois avec le 8e programme de recherche europ�en �Horizon 2020�. Selon un bilan du 7e programme (2007-2013), les Ecoles polytechniques f�d�rales (EPF) de Zurich et Lausanne ont �t� les principaux b�n�ficiaires des subsides europ�ens, r�coltant 40% des aides d'encouragement � la recherche.
La Suisse est un des pays qui participe le plus, obtient le plus haut taux de succ�s et le plus de fonds.
�Logique et sans surprise�
Avant l'annonce de Bruxelles, les partis avaient r�agi � la mesure prise par la Suisse contre la Croatie. �Cette d�cision est logique et sans surprise�, a r�agi Christophe Darbellay, le pr�sident du PDC, interrog� par l'ats.
�On ne peut pas signer avec la Croatie quelque chose que le peuple ne veut pas. C'est coh�rent. Il faut maintenant trouver une nouvelle solution avec l'Union europ�enne�, a poursuivi le conseiller national valaisan. Et cette solution devra ensuite �tre �tendue � la Croatie.
M�me son de cloche du c�t� de Philipp M�ller, pr�sident du PLR. Pour lui, depuis le scrutin, il est �totalement clair que l'accord n�goci� avec la Croatie ne peut pas �tre ratifi�, bien qu'une bonne solution ait �t� trouv�e avec une phase de transition de dix ans�.
Les Suisses tiennent aux bilat�rales
Malgr� l'acceptation de l'initiative, le peuple suisse tient aux accords bilat�raux avec l'UE. Selon un sondage publi� par le SonntagsBlick, 74% des personnes interrog�es se prononcent contre une d�nonciation des bilat�rales par la Suisse, alors que 19% sont pour et 7% n'ont pas d'avis.
A la question de savoir si le conseiller national Christoph Blocher (UDC/ZH) doit aller n�gocier � Bruxelles, 41% sont plut�t pour contre 54% d'avis plut�t n�gatifs. Quant � octroyer un deuxi�me si�ge du Conseil f�d�ral � l'UDC, comme l'a de nouveau exig� son pr�sident Toni Brunner, 46% approuvent alors que 44% y sont oppos�s. Pour cette question, 10% des sond�s n'ont pas �mis d'opinion.
Mise en �uvre rapide
Toni Brunner mart�le en outre que la Suisse doit mettre en �uvre l'initiative anti-immigration aussi vite que possible et introduire des contingents de travailleurs. Le St-Gallois s'inqui�te, car la libert� totale de circulation des personnes avec l'Europe de l'Est entre en vigueur en juin.
Pour Christoph Blocher, le Conseil f�d�ral semble agir de la bonne mani�re. Le vice-pr�sident de l'UDC salue la d�cision du coll�ge de vouloir �laborer une loi et d'ensuite de commencer � n�gocier avec l'UE.
Quant au pr�sident du PS Christian Levrat, il exige une nouvelle votation si la Suisse doit d�noncer les accords bilat�raux � la suite de l'acceptation de l'initiative. Les Suisses ont vot� en pensant que le texte de l'UDC �tait compatible, estime-t-il. (ats/Newsnet)
Cr��: 16.02.2014, 21h52
