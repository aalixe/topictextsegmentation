TITRE: La droite bien plac�e pour les �lections muncipales et europ�ennes | La-Croix.com
DATE: 2014-02-16
URL: http://www.la-croix.com/Actualite/Monde/La-droite-bien-placee-pour-les-elections-muncipales-et-europeennes-2014-02-15-1107036
PRINCIPAL: 177522
TEXT:
La droite bien plac�e pour les �lections muncipales et europ�ennes
Deux sondages laissent esp�rer de bons r�sultats � la droite lors des municipales comme lors des europ�ennes.�
15/2/14 - 11 H 50
- Mis � jour le 15/2/14 - 12 H 02
Mots-cl�s :
R�agir 0
Avec cet article
Dans les grandes villes, les femmes maires sont rares ���
Cependant, 26% des sond�s (+6 depuis avril 2012) sont favorables � "une disparition de l'euro et � un retour au franc"
D�apr�s un sondage BVA pour I>T�l�-CQFD-Le Parisien-Aujourd'hui en France publi� samedi 15 f�vrier, 44% des �lecteurs souhaitent la victoire d'une liste de droite ou du centre-droit dans leur commune aux municipales, 41% d'une liste de gauche et seulement 15% un succ�s du Front national.�
Dans cette �tude qui porte sur le souhait de victoire, les listes de l'UMP ou du Modem + l'UDI (29% et 15%) progressent de 2 points par rapport � janvier, 5 points par rapport � novembre. Avec 8% pour l'extr�me gauche et 33% pour le PS et ses alli�s, la gauche gagne toutefois 1 point par rapport � janvier, 2 points par rapport � novembre.�
En revanche, avec 15 %, le FN recule encore. Le parti de Marine Le Pen a perdu 3 points depuis janvier et 7 depuis novembre.�
Ces r�sultats doivent cependant �tre relativis�s. D�une part, ils portent sur les souhaits de victoire et non pas sur les intentions de vote. D�autre part, 74% des sond�s indiquent aussi qu�ils se prononceront les 23 et 30 mars sur des enjeux locaux (ils �taient 70% en janvier) plut�t que pour soutenir ou sanctionner le pr�sident de la R�publique. Ces derniers �tant cependant 22%, presque un quart, � vouloir sanctionner le pr�sident de la R�publique.�
En outre, parmi ceux qui expriment une pr�f�rence partisane, le d�sir d'alternance est plus faible que celui de reconduire le m�me bord politique. Le souhait de reconduction s'affiche � 46% dans les villes de gauche, � 50% dans celles g�r�es par la droite.
Par ailleurs, d'apr�s un sondage Le Figaro-LCI-Opinion Way publi� vendredi 14 f�vrier, l'UMP (22%) est en t�te des intentions de vote pour les �lections europ�ennes de mai, mais cette fois-ci devant le FN (20%), et loin devant le PS (16%).�
A 100 jours des �lections europ�ennes, les sond�s sont aussi 12% � vouloir accorder leur vote � une liste Modem-UDI -si les �lections avaient lieu ce dimanche-, 9 % se prononcent pour une liste Europe Ecologie-Les Verts, 9% pour une liste Front de Gauche, 4% pour une liste du NPA et 3% pour une liste de Debout la R�publique.�
A noter aussi que 26% des sond�s (+6 depuis avril 2012) sont favorables � "une disparition de l'euro et � un retour au franc", tandis que 53% sont oppos�s (-9%) et 20% "indiff�rents".
