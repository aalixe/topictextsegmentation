TITRE: "The Amazing Spider-Man"�: la nouvelle bande-annonce - Cin�Obs
DATE: 2014-02-16
URL: http://cinema.nouvelobs.com/articles/29924-bandes-annonces-the-amazing-spider-man-la-nouvelle-bande-annonce
PRINCIPAL: 178410
TEXT:
"The Amazing Spider-Man"�: la nouvelle bande-annonce
Sortie en salle mercredi 30 avril 2014
Par Cineobs 14 f�vrier 2014
��Sony Pictures Germany
Mots-cl�s�: bande annonce, the amazing spider man le destin d'un h�ros
La suite des aventures de l'Homme-Araign�e mise en sc�ne par Marc Webb d�voile sa toute nouvelle bande-annonce.
Dans la suite de " The Amazing Spider-Man ", en salles le 30 avril prochain, Peter Parker va devoir faire face �de nouvelles menaces�: Electro, le Rhino ou encore la toute puissante Oscorp qui ne semble pas �trang�re �la disparition de ses parents. Autant dire que le super-h�ros va avoir du fil �retordre, autant da sa vie de justicier que dans sa vie priv�e.
Cette nouvelle bande-annonce insiste donc sur le caract�re purement �pique de ce nouveau Spider-Man tout en donnant un aper�u des ses enjeux dramatiques.
"The Amazing Spider-Man: le Destin d'un H�ros"
?
