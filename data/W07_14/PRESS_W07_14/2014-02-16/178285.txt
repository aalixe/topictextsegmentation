TITRE: Var : pas de piste privilégiée après le meurtre d'un ado - 16 février 2014 - Le Nouvel Observateur
DATE: 2014-02-16
URL: http://tempsreel.nouvelobs.com/faits-divers/20140216.OBS6529/var-pas-de-piste-privilegiee-apres-le-meurtre-d-un-ado.html
PRINCIPAL: 0
TEXT:
Le corps d'un adolescent de 17 ans poignard� a �t� d�couvert � son domicile de Six-Fours-les-Plages.
Image d'illustration. (Sebastien JARRY/MAXPPP)
Le parquet de Toulon a indiqu� samedi 15 f�vrier que toutes les pistes restaient ouvertes apr�s la d�couverte vendredi soir � son domicile de Six-Fours-les-Plages ( Var ) du corps d'un adolescent de 17 ans poignard�.
L'adolescent vivait seul depuis l'hospitalisation de son p�re et c'est sa soeur qui venue lui rendre visite a d�couvert le corps, a indiqu� le procureur de la R�publique de Toulon, Xavier Tarabeux.
Selon les premiers �l�ments de l'enqu�te, l'adolescent a �t� tu� de plusieurs coups de couteau et une autopsie devrait �tre pratiqu�e en d�but de semaine.
L'enqu�te a �t� confi�e � l'antenne de la police judiciaire de Toulon .
Le jeune homme avait quelques ant�c�dents judiciaires et une proc�dure �tait en cours pour possession de stup�fiants, outrage et r�bellion, a pr�cis� le parquet.
Partager
