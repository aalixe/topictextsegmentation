TITRE: Gouvernement bient�t form�, Berlusconi reste dans l'opposition - Monde - Actualit�s - La C�te - Journal r�gional l�manique
DATE: 2014-02-16
URL: http://www.lacote.ch/fr/monde/gouvernement-bientot-forme-berlusconi-reste-dans-l-opposition-604-1262374
PRINCIPAL: 177603
TEXT:
Gouvernement bient�t form�, Berlusconi reste dans l'opposition
Italie
Le pr�sident italien Giorgio Napolitano doit choisir un nouveau chef de gouvernement apr�s la d�mission d'Enrico Letta.
Cr�dit: KEYSTONE
Tous les commentaires (0)
Le pr�sident italien Giorgio Napolitano a poursuivi ses consultations � Rome en vue de confier probablement � Matteo Renzi, la constitution du nouveau gouvernement. Le parti de Silvio Berlusconi va lui rester dans l'opposition.
"Nous restons dans une opposition constructive", a confirm� l'ex-chef du gouvernement, � l'issue de son entrevue avec M. Napolitano.
M. Berlusconi a aussi assur� avoir fait part au chef de l'Etat de "sa pr�occupation et stup�faction pour cette crise opaque qui s'est ouverte en dehors du Parlement et au sein d'un seul parti".
Le Cavaliere visait l'inhabituel vote de d�fiance par lequel la direction du Parti d�mocrate (PD), sous la pression de son chef Matteo Renzi, a fait chuter jeudi soir le gouvernement d'Enrico Letta, pourtant num�ro deux du parti jusqu'� il y a quelques mois.
Alfano pr�t pour une coalition
En revanche, l'ex-lieutenant de Silvio Berlusconi, Angelino Alfano, vice-Premier ministre sortant et chef du parti Nouveau centre droit (NCD), dont les 30 s�nateurs sont essentiels pour assurer la majorit� gouvernementale de coalition gauche droite, s'est d�clar� pr�t � participer � un �ventuel gouvernement Renzi, � deux conditions.
D'abord que "l'axe de l'actuelle coalition 'anormale' droite gauche ne se d�place pas vers la gauche". "Nous dirions non � un tel gouvernement", a assur� M. Alfano.
La deuxi�me condition est de "faire les choses en grand" pour sortir le pays de la crise, en ciblant "la classe moyenne". "Mais pour faire les choses en grand, il faut du temps. On ne peut pas conclure un accord (avec Matteo Renzi, NDLR) en 48 heures et une fois l'accord conclu il faudra l'inscrire noir sur blanc", a averti M. Alfano, qui se m�fie de toute �vidence du chef du PD.
Pas moins d'une quinzaine de d�l�gations �taient re�ues dans la journ�e, un v�ritable marathon pour le pr�sident Napolitano, �g� de 88 ans. Les dirigeants du PD devaient fermer le bal dans la soir�e avant que M. Napolitano n'annonce le nom du successeur d'Enrico Letta, tr�s certainement l'ambitieux maire de Florence, �g� de 39 ans.
Discussion avec Napolitano
Le Mouvement 5-Etoiles (M5S), hostile � la classe politique traditionnelle, et la Ligue du Nord, formation autonomiste, ont mis fin � leur participation aux consultations organis�es par M. Napolitano.
Le Cavaliere, toujours aux commandes de son parti Forza Italia, apr�s sa destitution du S�nat cons�cutive � sa condamnation pour fraude fiscale, s'est refait une l�gitimit� politique avec sa visite � M. Napolitano en d�but de soir�e.
Il s'est offert aussi une sorte de revanche. Il estime que le chef de l'Etat lui a forc� la main lorsqu'il l'a pouss� � la d�mission en novembre 2011 pour nommer � son poste Mario Monti.
Division
Le futur chef du gouvernement devra prendre en compte la formation actuelle du Parlement, qui n'avait pas d�gag� de majorit� claire au printemps dernier, pour constituer un gouvernement de coalition gauche droite, comme son pr�d�cesseur.
Un quotidien italien a avanc� le chiffre de 18 ministres, "dont la moiti� de femmes". L'�conomiste Lucrezia Reichlin pourrait remplacer Fabrizio Saccomani � l'Economie, un minist�re toujours scrut� de pr�s par les partenaires europ�ens de la troisi�me �conomie de la zone euro.
D'autant que, malgr� un rebond de 0,1 % du PIB au quatri�me trimestre 2013, apr�s deux ans de r�cession �conomique, l'Italie est loin d'�tre tir�e d'affaire, avec une dette publique s'�levant � 127% de son PIB et un ch�mage � 13 %.
Scrutin attendu au Parlement
Matteo Renzi n'a jusqu'� pr�sent pas pr�cis� comment il souhaitait financer les r�formes qu'il veut mettre en place.
Parmi les possibles ministres, un autre nom circule, celui de Luca Cordero di Montezemolo, pr�sident de Ferrari et chantre du "Made in Italy". Emma Bonino, radicale, ancienne commissaire europ�enne, pourrait conserver son portefeuille aux Affaires �trang�res.
Une fois son gouvernement form�, M. Renzi devra se pr�senter devant le Parlement pour un vote de confiance.
Source: ats
