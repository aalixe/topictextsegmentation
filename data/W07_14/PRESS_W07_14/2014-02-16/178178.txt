TITRE: Audiences TV : The Voice et TF1 toujours au sommet de l'audience
DATE: 2014-02-16
URL: http://fashions-addict.com/Audiences-TV-The-Voice-et-TF1-toujours-au-sommet-de-l-audience_408___14234.html
PRINCIPAL: 178174
TEXT:
|News|
16/02/2014 - Audiences TV : The Voice et TF1 toujours au sommet de l'audience
Hier soir TF1 proposait la derni�re soir�e des castings � l'aveugle de l'�mission "The Voice, la plus belle voix". L'�mission pr�sent�e par Nikos Aliagas et produite par Shine France a rassembl� 8.1 millions de t�l�spectateurs et 36% de pda, 51% de pda sur les femmes de moins de 50 ans RdA*, 57% de pda sur les 15-24 ans et 52% de pda sur les 15-34 ans. Avec ces tr�s bons scores The Voice se place tr�s largement en t�te des audiences de la soir�e. A 23h15, " The Voice, la suite " pr�sent�e par Nikos Aliagas, accompagn� de Karine Ferri a r�uni 2.9 millions de t�l�spectateurs et 25% de pda, 33% de pda sur les femmes de moins de 50 ans RdA*, 30% de pda sur les 15-24 ans et 31% de pda sur les 15-34 ans. L'�mission se place en t�te des audiences.
France 2 est deuxi�me avec le divertissement "Le plus grand cabaret du monde" pr�sent� par Patrick S�bastien qui a r�uni 3,6 millions de t�l�spectateurs en moyenne jusqu'� 23h00, soit 15,3% du public. France 3 est sur le podium avec la s�rie fran�aise "Le Sang de la vigne" avec Pierre Arditi et Claire Nebout. Cet �pisode a atir� 3,2 millions de t�l�spectateurs en moyenne jusqu'� 22h20, soit 13,2% du public de quatre ans et plus.
source M�diam�trie - M�diamat
