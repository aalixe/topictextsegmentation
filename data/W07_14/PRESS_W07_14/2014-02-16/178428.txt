TITRE: Record de Lavillenie : "Je ne suis pas d��u, je l'avais envisag�", confesse Bubka - RTL.fr
DATE: 2014-02-16
URL: http://www.rtl.fr/actualites/sport/athletisme/article/record-de-lavillenie-je-ne-suis-pas-decu-je-l-avais-envisage-confesse-bubka-7769752258
PRINCIPAL: 178423
TEXT:
Accueil > Toutes les Actualit�s sport > Record de Lavillenie : "Je ne suis pas d��u, je l'avais envisag�", confesse Bubka
Record de Lavillenie : "Je ne suis pas d��u, je l'avais envisag�", confesse Bubka
Par Ryad  Ouslimani | Publi� le 16/02/2014 � 12h28
Lavillenie bat le record du monde
Cr�dit : Alexander KHUDOTEPLY / AFP
R�ACTIONS - Au lendemain du record du monde historique de Renaud Lavillenie en saut � la perche, les r�actions et les marques d'admiration affluent de toutes parts.
Immense, fabuleux, magique, incroyable. Les superlatifs sont de sortie pour qualifier les 6m16 de Renaud Lavillenie, nouveau recordman du monde de saut � la perche. Un record �tabli sous les yeux du ma�tre, Sergue� Bubka, sur ses terres ukrainiennes de Donetsk. Une performance unanimement salu�e .
De son c�t�, le nouvel homme le plus haut de l'athl�tisme �tait encore dans les nuages apr�s son exploit de samedi. L'�motion �tait m�me "difficile � exprimer tellement c'est grandissime, c'est un record qui date de plus de 20 ans par une personne embl�matique de mon sport", a-t-il confi� sur TF1.
Se dire que j'ai de la marge, c'est extraordinaire
Renaud Lavillenie, recordman du monde de la perche
Toujours ambitieux, jamais gourmand, le Fran�ais tenta dans la foul�e de passer 6m21, sans succ�s,� se blessant au pied � la r�ception, mais ce n'est que partie remise. "Je sais que je n'ai pas touch� la barre [� 6,16m], et se dire que j'ai encore de la marge, c'est extraordinaire", s'enthousiasme le champion olympique de 2012.
Obstin� et� convaincu qu'il pouvait battre cette barre mythique, il est r�compens�, enfin, alors que depuis 21 ans, les perchistes se cassent les dents sur le roi Bubka. "On �tait tous bloqu� sur les 6m15 de Bubka, c'est un monument auquel Renaud s'est attaqu� et qu'il a r�ussi � vaincre. On pensait que ce record n'allait jamais �tre battu", raconte Antoine Blondin, consultant athl�tisme pour RTL.�
On l'appelait la machine
Georges Martin, son premier entra�neur
Ceux qui ont approch� le champion sont unanimes : il aime ce sport plus que tout. "On l'appelait la machine, il �tait capable de faire 50 sauts dans l'apr�s-midi", se rappelle Georges Martin, son premier entra�neur. Antoine Blondin raconte m�me que "c'est le seul gar�on � avoir un sautoir dans son jardin, c'est un gar�on qui adore �a". Une passion inocul�e tr�s t�t, d�s l'�ge de 4 ans, par son papa Gilles.
Son p�re justement, �tait �mu de voir son enfant entrer dans l'histoire du sport par la grande porte. "Mon fils, il a battu Bubka", s'extasie-t-il. "C'est lui que les gamins voudront un jour battre", ajoute ce p�re combl�. Mais il promet qu'"il va rester dans les nuages et continuer � monter". Monter encore plus haut et prendre ses distances vis-�-vis de Sergue� Bubka, qu'il a battu sous ses yeux.
C'est une performance fantastique
Sergue� Bubka, ex-recordman du monde
"C'est un grand jour, c'est une performance fantastique", a d�clar� Bubka, fair-play. "Je suis tr�s content de ce que Renaud a r�ussi � faire ici, dans ma ville, l� o� j'ai r�ussi � passer 6m15". � l'en croire il s'attendait � cette performance. "Je ne suis pas d��u parce que je l'avais d�j� envisag�", avoue-t-il, content que l'athl�tisme et la perche profitent de ce coup de projecteur.
Champion olympique 2012, Renaud Lavillenie est consacr�, ce qui rend l'athl�tisme fran�ais heureux pour un homme appr�ci� de tous. "C'est un homme simple et sympathique, toujours pr�t � encourager ses camarades de l'�quipe de France, c'est assez incroyable dans un sport individuel qui devient un collectif avec lui. C'est une grande fiert�", a racont� Bernard Amsalem, pr�sident de la f�d�ration fran�aise d'athl�tisme.
La r�daction vous recommande
