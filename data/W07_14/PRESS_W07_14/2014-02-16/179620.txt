TITRE: Puy-de-D�me: un enfant de trois ans meurt chez son p�re, introuvable - BFMTV.com
DATE: 2014-02-16
URL: http://www.bfmtv.com/societe/puy-de-dome-un-enfant-trois-ans-meurt-chez-pere-introuvable-712642.html
PRINCIPAL: 179611
TEXT:
Un enfant de trois ans est mort samedi en fin d'apr�s-midi � Vic-le-Comte (Puy-de-D�me) chez son p�re, qui a pris la fuite et reste introuvable, a indiqu� dimanche le parquet.
���
S�par�e de son ex-compagnon, la m�re "a d�couvert l'enfant apr�s avoir vu le p�re partir" de chez lui alors qu'elle venait le r�cup�rer. Ce dernier a �t� retrouv� "inanim�" et a �t� d�clar� d�c�d� malgr� l'intervention des secours, selon la m�me source. Les premi�res constatations ne font pas �tat de "blessures ou de violences quelconques" sur le corps de l'enfant, qui sera autopsi� lundi.
�� �
Les gendarmes sont � la recherche du p�re, qui est �g� de 40 ans et n'a "aucun ant�c�dent judiciaire connu pour l'instant", a pr�cis� le parquet. Une enqu�te a �t� ouverte pour recherche des causes de la mort.
