TITRE: Syrie: aucune pression de la part de Moscou (Damas) | International | RIA Novosti
DATE: 2014-02-16
URL: http://fr.ria.ru/world/20140214/200480572.html
PRINCIPAL: 177737
TEXT:
Syrie: Moscou appelle � ne pas d�former le communiqu� de Gen�ve
A la diff�rence d'autres pays, la Russie ne fait pas de pression sur la Syrie, Moscou et Damas �tant unanimes dans l'interpr�tation du communiqu� de Gen�ve, notamment de sa r�alisation par �tapes, a d�clar� vendredi le vice-ministre syrien des Affaires �trang�res Fay�al Moqdad.
"La Russie n'exerce pas (sur la Syrie, ndlr) une pression pareille � celle que font d'autres pays et d'autres gouvernements qui ne connaissent rien � la politique internationale et ne font qu'aggraver la tension. Nous coop�rons avec nos amis russes, et nos relations d'amiti� sont profondes", a soulign� M.Moqdad.
Et d'ajouter que Moscou et Damas estimaient n�cessaire d'examiner toutes les questions point par point, conform�ment au communiqu� de Gen�ve.
Auparavant le diplomate a regrett� que le deuxi�me round des n�gociations de Gen�ve entre les d�l�gations du gouvernement syrien et de l'opposition se soit sold� par un �chec et n'ait abouti � aucun r�sultat.
R�uni le 30 juin 2012 � Gen�ve, le Groupe d'action sur la Syrie, comprenant les ministres des Affaires �trang�res des cinq membres permanents du Conseil de s�curit� de l'ONU, de la Turquie et de pays de la Ligue arabe, a formul� les principes de r�glement de la crise syrienne. Ces principes pr�voient, entre autres, la n�cessit� de mettre un terme aux violences arm�es sous toutes leurs formes, de lib�rer les personnes arbitrairement d�tenues, de respecter le droit de manifester librement dans les conditions pr�vues par la loi et de cr�er un gouvernement de transition r�unissant toutes les parties en conflit.
Add to blog
