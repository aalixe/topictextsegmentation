TITRE: Au Japon, les fortes chutes de neige font douze morts - rts.ch - info - monde
DATE: 2014-02-16
URL: http://www.rts.ch/info/monde/5617431-au-japon-les-fortes-chutes-de-neige-font-douze-morts.html
PRINCIPAL: 0
TEXT:
Au Japon, les fortes chutes de neige font douze morts
16.02.2014 15:20
L'est du Japon fait face � d'importantes chutes de neige ce week-end. Douze personnes ont perdu la vie et des milliers de foyers sont priv�s d'�lectricit�.
De fortes chutes de neige se sont abattues au cours du week-end sur l'est du Japon. Elles ont fait douze morts et ont priv� d'�lectricit� des dizaines de milliers de foyers. Les transports terrestres et a�riens ont en outre �t� fortement perturb�s.
Comme le week-end dernier, Tokyo a re�u 27 cm de neige, ce qui n'avait pas �t� vu depuis 45 ans. La compagnie a�rienne ANA a annonc� l'annulation de 338 vols int�rieurs et douze vols internationaux samedi.
Axes routiers impraticables
A Kawasaki, � l'ouest de Tokyo, un train en a percut� un autre, car la neige a emp�ch� ses freins de fonctionner. Dix-neuf passagers ont �t� bless�s. Certains axes routiers de la capitale sont devenus impraticables.
La compagnie Tokyo Electric Power (Tepco), qui fournit l'�lectricit� � Tokyo et ses environs, a d�clar� que 246'000 foyers avaient �t� priv�s de courant samedi.
ats/rber
