TITRE: SNOWBOARDCROSS: Encore une lourde chute au Parc Extr�me -  News Sotchi 2014: L'actu des JO - lematin.ch
DATE: 2014-02-16
URL: http://www.lematin.ch/sotchi2014/lactu-des-jo/Encore-une-lourde-chute-au-Parc-Extreme/story/29026026
PRINCIPAL: 178127
TEXT:
Encore une lourde chute au Parc Extr�me
SNOWBOARDCROSS
�
Jacqueline Hernandez a �t� victime d�une lourde chute dimanche lors des qualifications de snowboardcross, sur le m�me parcours o� une Russe s��tait gravement bless�e samedi en skicross.
Mis � jour le 16.02.2014
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
E-Mail*
Veuillez SVP entrez une adresse e-mail valide
Jacqueline Hernandez, 21 ans, a �t� d�s�quilibr�e sur un des nombreux sauts du parcours et s�est r�ceptionn� en travers. Sa t�te a bascul� en arri�re et a violemment heurt� la piste. Elle est rest�e inerte quelques instants avant de retrouver ses esprits et d��tre �vacu�e sur une civi�re, consciente.
Quelques instants auparavant, la Norv�gienne Helene Olafsen avait �galement chut� sur le parcours et avait d� elle aussi �tre �vacu�e sur une civi�re.
Le pr�c�dent Komissarova
Samedi, Maria Komissarova a �t� la premi�re athl�te des JO 2014 � se blesser tr�s gravement. Touch�e � la colonne vert�brale lors d�un entra�nement de skicross dans ce parcours du Parc Extr�me de Rosa Khoutor, la Russe avait �t� op�r�e pendant plus de six heures � l�h�pital de Krasna�a Poliana, le plus proche du site, construit sp�cialement pour les JO.
Elle �tait samedi soir dans un �tat �s�rieux et stable� mais �consciente�, selon la F�d�ration russe de ski freestyle (FFR). (AFP/Le Matin)
Cr��: 16.02.2014, 08h52
Votre email a �t� envoy�.
Publier un nouveau commentaire
Nous vous invitons ici � donner votre point de vue, vos informations, vos arguments. Nous vous prions d�utiliser votre nom complet, la discussion est plus authentique ainsi. Vous pouvez vous connecter via Facebook ou cr�er un compte utilisateur, selon votre choix. Les fausses identit�s seront bannies. Nous refusons les messages haineux, diffamatoires, racistes ou x�nophobes, les menaces, incitations � la violence ou autres injures. Merci de garder un ton respectueux et de penser que de nombreuses personnes vous lisent.
La r�daction
Merci de votre participation, votre commentaire sera publi� dans les meilleurs d�lais.
Merci pour votre contribution.
J'ai lu et j'accepte la Charte des commentaires.
Caract�res restants:
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
S'il vous pla�t entrer un nom valide.
S'il vous pla�t indiquez le lieu g�ographique.
Ce num�ro de t�l�phone n'est pas valable.
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
Signup Fermer
Vous devez lire et accepter la Charte de commentaires avant de poursuivre.
Nous sommes heureux que vous voulez nous donner vos commentaires. S'il vous pla�t noter les r�gles suivantes � l'avance: La r�daction se r�serve le droit de ne pas publier des commentaires. Ceci s'applique en g�n�ral, mais surtout pour les propos diffamatoires, racistes, hors de propos, hors-sujet des commentaires, ou ceux en langues �trang�res ou dialecte. Commentaires des noms de fantaisie, ou avec des noms manifestement fausses ne sont pas publi�s non plus. Plus les d�cisions de la r�daction n'est ni responsable d�pos�e, ni en dehors de la correspondance. Renseignements t�l�phoniques ne seront pas fournis. L'�diteur se r�serve le droit �galement � r�duire les commentaires des lecteurs. S'il vous pla�t noter que votre commentaire aussi sur Google et autres moteurs de recherche peuvent �tre trouv�s et que les �diteurs ne peuvent rien et est de supprimer un commentaire une fois �mis dans l'index des moteurs de recherche.
�auf Facebook publizieren�
Veuilliez attendre s'il vous pla�t
Soumettre Commentaire
Soumettre Commentaire
No connection to facebook possible. Please try again. There was a problem while transmitting your comment. Please try again.
