TITRE: Football - MAXIFOOT : Le LOSC revient de nulle part - D�brief et NOTES des joueurs (Evian 2-2 Lille)
DATE: 2014-02-16
URL: http://www.maxifoot.fr/football/article-21018.htm
PRINCIPAL: 179347
TEXT:
Le LOSC revient de nulle part - D�brief et NOTES des joueurs (Evian 2-2 Lille)
Par Pierre-Damien Lacourte - Actu Ligue 1, Mise en ligne: le 16/02/2014 � 19h16
Taille du texte: Email Imprimer Partager:
Men� 2-1 jusqu'� quelques secondes du coup de sifflet final, le LOSC a su prendre un point inesp�r� face � Evian Thonon Gaillard (2-2). Les Dogues n'ont �vit� la d�faite que gr�ce � un but tr�s chanceux de Ryan Mendes � la derni�re minute.
Pascal Dupraz pouvait enrager apr�s le but de Ryan Mendes.
Le LOSC a confirm� ses difficult�s actuelles en arrachant un point inesp�r� sur la pelouse d'Evian Thonon Gaillard ce dimanche (2-2). Les Haut-Savoyards ne mettaient que quelques minutes � se montrer dangereux. Seul face � Enyeama, Ruben butait sur le portier lillois. B�rigaud se montrait lui plus efficace ensuite, ouvrant le score d'un joli coup de t�te (1-0, 15e). Les Dogues r�agissaient sans tarder, par Origi, adroit aux 18 m�tres (1-1, 22e).
Laquait d�cisif devant Roux
Juste avant le repos, l'ETG avait l'occasion de reprendre l'avantage mais Sougou �tait trop court sur le centre de Wass. Au retour des vestiaires, le LOSC se procurait une grosse occasion mais Laquait brillait sur la frappe crois�e de Roux. C'est finalement Evian qui allait repasser devant au tableau d'afffichage.
�Ils sont farcis les types�
Mongongu transformait parfaitement son penalty apr�s une faute de main de Souar� dans sa surface (2-1, 64e). Incapables de se r�volter en fin de match, les Nordistes allaient finalement arracher le nul gr�ce � un centre-tir de Mendes qui terminait dans la lucarne de Laquait (2-2, 93e). Cruel pour Evian. �Ils sont farcis les types�, l�chera d'ailleurs, d�pit�, Olivier Sorlin au coup de sifflet final. On peut le voir comme �a.
La note du match : 5/10
Apr�s une premi�re p�riode int�ressante, le rythme est largement retomb� apr�s le repos. Et comme les Lillois �prouvent toujours autant de difficult�s � emballer leurs rencontres...
Les buts
- Sur un bon centre de Sougou, B�rigaud saute plus haut que Kjaer et place une t�te d�crois�e qui termine dans le petit filet d'Enyeama (1-0, 15e).
- Sur le c�t� gauche, Origi fixe Wass et � l'entr�e de la surface, trompe Laquait d'une belle frappe enroul�e du droit (1-1, 22e).
- Dans la surface lilloise, Souar� tente de stopper l'offensive de Barbosa mais touche le ballon de la main. Le penalty est parfaitement transform� par Mongongu (2-1, 64e).
- Sur le c�t� droit, Mendes veut centrer mais son ballon finit miraculeusement dans la lucarne de Laquait (2-2, 93e).
Les NOTES des joueurs :
Maxifoot a d�cern� une note (sur 10) comment�e � chaque joueur.
L'homme du match : Olivier Sorlin (6/10)
Bon match du capitaine �vianais au milieu de terrain. Il n'a pas compt� ses efforts.
EVIAN TG :
Bertrand Laquait (5) : trop court sur le but d'Origi, le portier �vianais r�alise en revanche un bel arr�t sur la tentative de Roux en d�but de seconde p�riode.
Daniel Wass (5) : il a souffert face � Origi sur son c�t� droit. Trop passif notamment sur le but de l'attaquant lillois. Mieux en deuxi�me mi-temps.
Aldo Angoula (6) : important dans le domaine a�rien, il a gagn� de nombreux duels.
C�dric Mongongu (6) : un match s�rieux en charni�re centrale. Son penalty est parfaitement tir�.
Youssouf Sabaly (6) : int�ressant sur son c�t� gauche, il s'est montr� solide d�fensivement.
Djakaridja Kon� (5) : des grosses fautes commises. M. Duhamel, tr�s gentil avec lui, aurait pu l'exclure avant m�me la demi-heure de jeu. Des ballons r�cup�r�s n�anmoins. Remplac� � la 82e minute par C�dric Cambon (non not�).
Olivier Sorlin (6) : lire le commentaire ci-dessus.
Modou Sougou (5) : ses appels ont parfois d�stabilis� la d�fense nordiste, mais il manque de promptitude sur le centre de Wass en fin de premi�re p�riode.
C�dric Barbosa (6) : volontaire, il a plusieurs fois secou� la d�fense lilloise. Il est � l'origine du penalty obtenu par son �quipe.
K�vin B�rigaud (6) : il ouvre le score d'un joli coup de t�te en d�but de match. D�cisif. Remplac� � la 90e minute par Clarck Nsikulu (non not�).
Marco Ruben (5) : assez discret, il manque notamment une belle occasion en d�but de rencontre. Il se sera n�anmoins battu devant. Remplac� � la 83e minute par Facundo Bertoglio (non not�).
LILLE :
Vincent Enyeama (5) : scotch� sur sa ligne sur le but de B�rigaud, il avait auparavant sauv� les siens face � Ruben. Parti du bon c�t� mais trop court sur le penalty de Mongongu.
Adama Soumaoro (5) : il s'est content� de d�fendre sur son c�t� droit.
Simon Kjaer (4) : pas au mieux, le Danois ne s'est pas montr� tr�s s�r cet apr�s-midi. Des duels importants perdus.
Marko Basa (6) : plus costaud que Kjaer, le Mont�n�grin s'est montr� s�rieux en charni�re centrale. Bless�, il c�de sa place � la 71e minute � David Rozehnal (non not�).
Pape Souar� (5) : le d�fenseur lillois a affich� une belle combativit� sur son c�t� gauche mais il co�te le penalty � son �quipe en touchant le ballon de la main sur un tacle. Remplac� � la 79e minute par Ryan Mendes (non not�).
Florent Balmont (5) : il s'est battu au milieu mais n'a pas su apporter le surnombre en attaque.
Rio Mavuba (5) : des ballons r�cup�r�s devant sa d�fense mais pas d'emprise r�elle sur le milieu de terrain.
Idrissa Gueye (5) : comme Balmont et Mavuba, il a eu du mal � mettre le pied sur le ballon dans l'entrejeu.
Salomon Kalou (4) : peu en vue devant, l'Ivoirien n'aura pas su faire de diff�rences.
Nolan Roux (4) : une bonne frappe en d�but de seconde p�riode, bien repouss�e par Laquait, et c'est � peu pr�s tout. Remplac� � la 65e minute par Ronny Rodelin (non not�).
Divock Origi (6) : int�ressant sur son c�t� gauche, il trouve notamment le chemin des filets d'une belle frappe enroul�e. Plus discret en deuxi�me p�riode.
Et pour vous, quels ont �t� les MEILLEURS joueurs et les MOINS BONS de chaque �quipe ? R�agissez dans la rubrique "commentaires" ci-dessous !
>> Retrouvez tous les r�sultats et le classement de Ligue 1 sur Maxifoot en cliquant ici .
Ligue 1 / 25e journ�e
Evian TG 2-2 Lille (1-1)
Parc des Sports (Annecy)
Buts : B�rigaud (15e), Mongongu (64e, sp) pour Evian ; Origi (22e), Mendes (93e) pour Lille
Avertissements : Kon� (27e), Ruben (79e), Angoula (89e) pour Evian ; Souar� (19e) pour Lille
Evian TG : Laquait - Wass, Angoula, Mongongu, Sabaly - Kon� (Cambon, 82e), Sorlin (cap.) - Sougou, Barbosa, B�rigaud (Nsikulu, 90e) - Ruben (Bertoglio, 83e). Entra�neur : Pascal Dupraz.
Lille : Enyeama - Soumaoro, Kjaer, Basa (Rozehnal, 71e), Souar� (Mendes, 79e) - Balmont, Mavuba (cap.), Gueye - Kalou, Roux (Rodelin, 65e), Origi. Entra�neur : Ren� Girard.
