TITRE: Sports - Le perchiste fran�ais Renaud Lavillenie bat le record de Sergue� Bubka - France 24
DATE: 2014-02-16
URL: http://www.france24.com/fr/20140215-renaud-lavillenie-record-monde-saut-perche-serguei-bubka/
PRINCIPAL: 178112
TEXT:
Texte par FRANCE 24 Suivre france24_fr sur twitter
Derni�re modification : 16/02/2014
Renaud Lavillenie a battu, samedi, le record du monde de saut � la perche en salle en franchissant la barre des 6,16 m�tres � Donetsk, en Ukraine. Une performance qui efface la performance historique de Sergue� Bubka, il y a 21 ans.
C'est un record vieux de 21 ans que le perchiste fran�ais a pulv�ris�. Renaud Lavillenie a battu, samedi 15 f�vrier, le record du monde de saut � la perche en salle, en franchissant les 6,16 m�tres � Donetsk, en Ukraine.
L'Auvergnat, qui affirmait depuis plusieurs semaines se sentir capable de cette performance, est entr� dans l'Histoire en franchissant la barre d�s le premier essai, lors du Pole Vault Stars, une comp�tition d'athl�tisme exclusivement d�di�e � la discipline du saut � la perche..
"C'�tait totalement incroyable! Je n'ai jamais utilis� cette perche pour 6,16 m�tres. Pour ma premi�re tentative, je souhaitais r�aliser le meilleur saut possible. J'ai demand� 6,16 m�tres, parce que c'est le meilleur endroit pour battre le record de Sergue� Bubka, 21 ans apr�s son record", a d�clar� l'athl�te.
Commentaire de Renaud Lavillenie sur son compte Twitter
RECORD DU MONDE!!!!!!!! 6m16 au premier essai!!!  C'est incroyable, je suis encore dans les airs ...
� Renaud Lavillenie (@airlavillenie) February 15, 2014
Lavillenie a ensuite tent� - en vain - une barre � 6,21 m devant une salle enti�rement acquise � sa cause. Mais l'aisance avec laquelle il a franchi les 6,16 m laisse esp�rer d'autres records. �g� de 27 ans, il est d�j� mont� sur tous les podiums depuis 2009, l'ann�e de son �veil international, et conquiert avec ce record la l�gitimit� des g�ants.
Il ne manque au Charentais de naissance que l'or des Mondiaux en plein air. Troisi�me en 2009, il dut encore se contenter de cette place en 2011, mais cette fois dans la peau du favori. Il a �t� une nouvelle fois deuxi�me en 2013 � Moscou, derri�re l'Allemand Raphael Holdzeppe.
Sa brillante carri�re s'inscrit dans un riche pedigree familial : chez les Lavillenie, on est en effet perchiste de g�n�ration en g�n�ration. Le grand-p�re entra�nait d�j� le p�re.�Et Renaud conseille son fr�re cadet Valentin, un des meilleurs de la discipline dans l'Hexagone.
R�action de Serge� Bubka sur son compte Twitter
So happy for Renaud. So happy it happened in my home city. Bravo Renaud
� Sergey Bubka (@sergey_bubka) February 15, 2014
"Beaucoup de sympathie pour ce gars"
La l�gende de la perche mondiale, pr�sente au meeting Pole Vault Stars en costume-cravate, a �t� parmi les premiers � f�liciter le Fran�ais, regardant avec admiration la barre d�sormais de r�f�rence.
"Une nouvelle �re dans ce sport est arriv�e. Aujourd'hui, le vainqueur est un champion olympique, une personne qui a d�j� obtenu de nombreux succ�s. Nous nous attendions � cet �v�nement et nous sommes ravis que cela se soit pass� pr�cis�ment ici, � Donetsk", a d�clar� Bubka � l'AFP.
Mondiaux d'athl�tisme : chez les Lavillenie, la perche est une histoire de fr�res
"J'ai beaucoup de sympathie pour ce gars. Je suis s�r que ce n'est pas le dernier sommet qu'il atteint et que d'autres succ�s brillants l'attendent", a-t-il soulign�.
Samedi soir, les r�actions abondaient sur les r�seaux sociaux pour saluer cette performance. "Record du Mooonnndeeee! Chapeau l'Artiste", �crivait notamment sur Twitter l'athl�te St�phane Diagana. De son c�t�, le pr�sident Fran�ois Hollande a salu� un "exploit historique".
Avec AFP
