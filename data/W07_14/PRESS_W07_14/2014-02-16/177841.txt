TITRE: Venezuela: des milliers de partisans manifestent pour soutenir Maduro - France - RFI
DATE: 2014-02-16
URL: http://www.rfi.fr/ameriques/20140216-venezuela/
PRINCIPAL: 0
TEXT:
Modifi� le 16-02-2014 � 07:34
Venezuela: des milliers de partisans manifestent pour soutenir Maduro
par RFI
Des milliers de sympathisants du pouvoir ont manifest�, � l'appel du pr�sident Maduro, � Caracas, le 15 f�vrier 2014.
REUTERS/Carlos Garcia Rawlins
Au Venezuela, les sympathisants du pouvoir ont manifest� le 15 f�vrier � l'appel du pr�sident, Nicolas Maduro, en r�ponse � la mobilisation des �tudiants, hostiles au gouvernement, depuis plusieurs jours. Plusieurs milliers de personnes se sont regroup�s, sur l'avenue Bolivar, � Caracas, pour la paix, comme le voulait le chef de l'Etat. Mais aussi en m�moire du militant pro-gouvernement, tu� mercredi dans une autre manifestation.
�
Avec notre correspondant � Caracas, Julien Gonzalez
Op�ration de communication r�ussie. Des ateliers, peintures, des terrains de baskets improvis�s. Nicolas Maduro avait �t� clair : il faut c�l�brer la paix, comme une f�te. Tout sourire, Irislis Suarez attend le discours du Pr�sident, en dansant la rumba. De la joie, mais pas de traces de deuil :
� Nous, les r�volutionnaires, nous disons que le deuil se vit pour soi. Tous ceux qui meurent pour faire vivre le processus r�volutionnaire, pour nous, ils restent vivants, et c'est �a qui nous encourage � continuer de combattre. M�me si un de nos camarades, un de nos compatriotes, meurt dans la lutte, nous avons un devoir fondamental : continuer d'avancer �.
Pendant que sa s�ur manifeste avec l'opposition, � l'est de la capitale, Ninoska Fernandez est venue soutenir le chef de l'Etat. Elle �tudie � l'universit� centrale du Venezuela, et pour elle, c'est tout le syst�me �ducatif qui instrumentalise la contestation �tudiante :
� Ici, il y a la paix, ici, nous sommes tranquilles. Regarde, il n'y a pas de tireurs embusqu�s, pas de bagarre, pas de haine. Il y a un vrai travail id�ologique dont se chargent les professeurs et directeurs d'universit�s. Ils empoisonnent l'esprit des �tudiants. Tous les jours, ils nous bombardent d'attaques contre cette r�volution �.
Nicolas Maduro a promis � qu'il ne faiblira pas �. Et surtout, il a appel� � le peuple v�n�zu�lien � sortir dans la rue pour garantir la paix �.
Autre mouvement, celui des �tudiants qui continuent � se mobiliser. Pour le 4e jour de suite, eux, contre le pouvoir. Ils ont r�alis� une cha�ne humaine, pour rendre hommage aux trois manifestants morts mercredi.
