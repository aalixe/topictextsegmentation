TITRE: Le sculpteur de la Marianne Bardot est mort | Var | Var-Matin
DATE: 2014-02-16
URL: http://www.varmatin.com/var/le-sculpteur-de-la-marianne-bardot-est-mort.1581093.html
PRINCIPAL: 178054
TEXT:
Tweet
Le c�l�bre sculpteur Aslan, de son vrai nom Alain Gourdon, vient de mourir � l'�ge de 83 ans au Canada.
Il doit sa notori�t� � ses dessins de pin-up publi�s dans le magazine Lui et � la cr�ation, en 1968, du buste de la Marianne de la R�publique incarn�e par Brigitte Bardot.
Ce buste vendu � ce jour � plus de 20.000 exemplaires, model� sous les traits flatteurs de l'actrice, incarne l'imagerie r�publicaine. Les originaux du buste et de la gouache ont �t� acquis par Alain Trampoglieri dans la vente aux ench�res au profit de la Fondation Bardot.
L'homme des Marianne d'or conserve pr�cieusement ces pi�ces uniques dans son appartement du Latitude 43 � Saint-Tropez. Pi�ces qui feront partie de l'exposition sur la d�mocratie fran�aise qu'Alain Trampoglieri doit pr�senter en juillet prochain � Saint-Tropez.
Tweet
Tous droits de reproduction et de repr�sentation r�serv�s. � 2012 Agence France-Presse.
Toutes les informations (texte, photo, vid�o, infographie fixe ou anim�e, contenu sonore ou multim�dia) reproduites dans cette rubrique (ou sur cette page selon le cas) sont prot�g�es par la l�gislation en vigueur sur les droits de propri�t� intellectuelle. �Par cons�quent, toute reproduction, repr�sentation, modification, traduction, exploitation commerciale ou r�utilisation de quelque mani�re que ce soit est interdite sans l�accord pr�alable �crit de l�AFP, � l�exception de l�usage non commercial personnel. �L�AFP ne pourra �tre tenue pour responsable des retards, erreurs, omissions qui ne peuvent �tre exclus dans le domaine des informations de presse, ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
AFP et son logo sont des marques d�pos�es.
Th�matiques
Var , Derni�re minute , Soci�t�
Le d�lai de 15 jours au-del� duquel il n'est plus possible de contribuer � l'article est d�sormais atteint.
Vous pouvez poursuivre votre discussion dans les forums de discussion du site. Si aucun d�bat ne correspond � votre discussion, vous pouvez solliciter le mod�rateur pour la cr�ation d'un nouveau forum : moderateur@nicematin.com
Vos derniers commentaires
