TITRE: GDF Suez nie toute accusation d'évasion fiscale en Belgique - 16 février 2014 - Challenges
DATE: 2014-02-16
URL: http://www.challenges.fr/entreprise/20140216.CHA0532/gdf-suez-nie-toute-accusation-d-evasion-fiscale-en-belgique.html
PRINCIPAL: 0
TEXT:
Challenges > Entreprise > GDF Suez nie toute accusation d'évasion fiscale en Belgique
GDF Suez nie toute accusation d'évasion fiscale en Belgique
A+ A-
Le groupe fran�ais a vivement contest� dimanche des informations de presse l'accusant d'�vasion fiscale en Belgique via sa filiale Electrabel.
Selon un rapport confidentiel, les surfacturations auraient amputé le bénéfice imposable d'Electrabel de quelque 500 millions d'euros, soit une perte de près de 170 millions d'impôts pour l'Etat belge, au profit des actionnaires de GDF Suez. ANTONIOL ANTOINE/SIPA
Dans son �dition du week-end, le journal �conomique l'Echo indique que la Commission de r�gulation de l'�lectricit� et du gaz (Creg) a r�dig� un rapport confidentiel selon lequel GDF Suez aurait en 2012 factur� plusieurs centaines de millions de trop � sa filiale belge pour ses fournitures de gaz.
Apr�s transmission du rapport aux autorit�s comp�tentes, l'Inspection sp�ciale des imp�ts (ISI) a ouvert une enqu�te, assure l'Echo.
"GDF Suez s'�tonne des articles publi�s et en conteste formellement le contenu", a assur� le groupe dans un communiqu�. "GDF Suez n'a aucune connaissance d'une �ventuelle enqu�te aupr�s de l'inspection sp�ciale des imp�ts. Si une telle enqu�te devait s'ouvrir, comme � son habitude GDF Suez offrirait sa pleine et enti�re collaboration aux enqu�teurs".
Selon l'Echo, citant le rapport, les surfacturations auraient amput� le b�n�fice imposable d'Electrabel de quelque 500 millions d'euros, soit une perte de pr�s de 170 millions d'imp�ts pour l'Etat belge, au profit des actionnaires de GDF Suez.
L'opposition belge r�clame "clart� et transparence"
Elles auraient aussi priv� les communes belges associ�es � Electrabel dans ECS (Electrabel Customer Solutions), filiale sp�cialis�e dans la fourniture d'�lectricit� et de gaz, d'une participation � hauteur de 40% � ce b�n�fice, sur le dos des clients particuliers et PME.
Cependant, le quotidien pr�vient que des sp�cialistes �mettent des r�serves sur la m�thodologie employ�e par le gendarme de l'�nergie, qui n'a pas eu acc�s � tous les contrats pass�s par GDF Suez . "L'entr�e en jeu de l'ISI (...) devrait permettre d'affiner, de confirmer ou d'infirmer ces chiffres", ajoute l'Echo.
A trois mois des �lections l�gislatives en Belgique , ces informations ont suscit� des r�actions de l'opposition. "Si ces faits sont av�r�s, c'est hallucinant, nous pouvons parler de tromperie", a d�clar� le ministre flamand des Affaires int�rieures, le nationaliste (N-VA) Geert Bourgeois. Les �cologistes ont r�clam� la clart� et la "transparence".
(Avec AFP)
