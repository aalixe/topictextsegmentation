TITRE: Martin Fourcade, vers une 3e m�daille d'or ? - France 3 Languedoc-Roussillon
DATE: 2014-02-16
URL: http://languedoc-roussillon.france3.fr/2014/02/16/martin-fourcade-vers-une-3e-medaille-dor-416443.html
PRINCIPAL: 178783
TEXT:
Sotchi 2014
Martin Fourcade, vers une 3e m�daille d'or ?
Le biathl�te de Font-Romeu, dans les Pyr�n�es-Orientales, est, pour l'instant, le seul pourvoyeur de m�dailles d'or de la d�l�gation fran�aise aux jeux de Sotchi en Russie. Il concourt cependant dans une disciple dont il n'est pas le meilleur sp�cialiste. C'est sa derni�re occasion de m�daille ici.
avec AFP
Publi� le 16/02/2014 | 14:20, mis � jour le 17/02/2014 | 11:47
� ODD ANDERSEN / AFP
+  petit
Le biathl�te Martin Fourcade a l'occasion, ce samedi apr�s-midi,�de d�crocher une troisi�me m�daille d'or aux Jeux Olympiques de Sotchi, et de rejoindre Jean-Claude�Killy, dernier fran�ais auteur de pareil exploit en 1968.
Vainqueur de la poursuite puis du 20 km, Foucade, 25 ans, n'est pas � proprement�parler un grand sp�cialiste de l'�preuve du jour, la mass-start (15 km), o� tous�les athl�tes prennent le d�part (19h00 locales, 16h00 fran�aises) simultan�ment�en lignes.
Mais la forme qu'il affiche depuis le d�but des Jeux et la confiance accumul�e�peuvent lui permettre de devenir le quatri�me fran�ais � r�aliser le tripl� en�or dans les m�mes JO, apr�s Jean-Claude Killy en 1968, Paul Masson en 1896 (cyclisme)�et Roger Ducret en 1924 (escrime).
Sur la route d'un possible troisi�me succ�s cons�cutif, il retrouvera ses habituels�adversaires, et notamment le Norv�gien Ole Einar Bjoerndalen.
S'il monte sur le podium, le Norv�gien, �g� de 40 ans, deviendra l'athl�te le�plus m�daill� de l'histoire des jeux Olympiques d'hiver, alors qu'il doit pour�le moment partager ce statut avec son compatriote fondeur Bj�rn Daehlie (12 m�dailles).
Il devra cependant "r�gler la mire", apr�s avoir manqu� le bronze en poursuite�pour une seconde � cause de trois fautes au tir, avant de commettre quatre fautes�sur le 20 km.
Un autre Norv�gien Emil Hegle Svendsen, qui a rat� le d�but de ses Jeux, les Autrichiens Dominik Landertinger et Simon Eder font �galement figure de s�rieux pr�tendants�� la victoire dans cette derni�re �preuve individuelle des JO avant les relais�(messieurs et mixte).
