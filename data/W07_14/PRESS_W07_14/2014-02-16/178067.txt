TITRE: Sotchi 2014 - Les Jeux en direct - JO 2014 - Sp. d'hiver - Sport.fr
DATE: 2014-02-16
URL: http://www.sport.fr/sports-d-hiver/jo-2014-sotchi-2014-les-jeux-en-direct-340434.shtm
PRINCIPAL: 178063
TEXT:
4. Slovaquie 1
16h20 - Mass-Start : pas aujourd'hui
D'abord retard�e d'une heure, la mass-start messieurs ne s'�lancera finalement pas dimanche. L'�preuve, pour cause de brouillard, est reprogramm�e lundi matin, d�part � 7h30.
15h55 - Patinage de vitesse (D): Record olympique de ter Mors
La supr�matie n�erlandaise se poursuit sur l'anneau de vitesse des Jeux. Jorien ter Mors remporte le 1500 m�tres en 1'53"51, nouveau record olympique de la distance.
15h30 - Le d�part retard� d'une heure
Le d�part de la mass start est retard� d'une heure en raison de la mont�e du brouillard sur le site de Laura. Initialement programm� � 16h00, le nouveau d�part sera donn� � 17h00.
15h05 - Clarey s'interroge sur son avenir
Apr�s sa 19e place dans le Super-G, Johan Clarey laisse entendre qu'il pourrait mettre un terme � sa carri�re en fin de saison en raison de probl�mes physiques. "J'ai du mal � m'entra�ner physiquement maintenant. J'ai tellement mal aux genoux, le dos je ne peux plus faire grand chose non plus." Le skieur de 33 ans avait chut� en descente lors de l'�preuve d'ouverture. "Je vais essayer de finir la saison comme je peux. Bien r�fl�chir, ce printemps, � ce que je veux faire. Est-ce que �a vaut le coup de continuer? J'adore le ski mais j'ai du mal � skier sans anti-inflammatoiresl."
14h55 - Pr�sentez-vous Chlo� Trespeuch
La m�daill�e de bronze du snowboardcross se pr�sente avec toute la spontan�it� de ses 19 ans. Chlo� Trespeuch, de Val Thorens. J'ai 19 ans. J'ai commenc� le snowboard toute petite avec mon grand fr�re L�o. Je suis en �quipe de France depuis 5 ans, 3 chez les jeunes et 2 chez les adultes (sic). Je fais beaucoup de sport, de l'�quitation, du squash, du surf (d'eau) et j'ai deux chevaux, l'un que j'ai achet� moi-m�me et l'autre qui est un cadeau de mon papa. Jusqu'� mes 16 ans, j'ai v�cu six mois l'hiver � Val Thorens et six mois l'�t� en Vend�e, � Saint-Jean-de-Monts, o� mon p�re g�rait un circuit de quad.
13h25 - La Su�de �crase les �preuves de fond
La Su�de domine les �preuves de ski de fond avec neuf m�dailles, dont deux en or. La veille, son �quipe f�minine avait �galement remport� le relais. Un tel doubl� hommes/femmes n'avait plus �t� r�alis� depuis 1972.
13h15 - Le drapeau indien hiss�
Le drapeau indien a �t� hiss� dimanche dans le village olympique de Rosa Khoutor, � l'issue d'une c�r�monie en pr�sence des athl�tes indiens inscrits aux Jeux de Sotchi et du pr�sident du Comit� olympique indien Narayna Ramachandran. Le Comit� international olympique (CIO) avait r�int�gr� l'Inde mardi, avec "effet imm�diat", apr�s l'avoir suspendu en d�cembre 2012 en raison d'ing�rences gouvernementales dans les affaires sportives et de probl�mes de gouvernance sur fond de corruption.
Depuis, les trois athl�tes indiens, Nadeem Iqbal (ski de fond), Shiva Keshavan (luge) et Himanshu Thakur (ski alpin), peuvent concourir sous leurs couleurs et ils pourront d�filer derri�re le drapeau indien � la c�r�monie de cl�ture.
12h30 - Relais : m�daille de bronze pour les Fran�ais
La France d�croche la premi�re m�daille de son histoire en relais aux Jeux olympiques. Les quatre mousquetaires Jean-Marc Gaillard , Maurice Manificat, Robin Duvillard et Ivan Perrillat-Boiteux d�crochent la m�daille de bronze du relais 4x10 km. La Su�de remporte l'or devant la Russie.
12h10 - Relais : les Fran�ais en 3e position
L'�quipe de France s'accroche pour d�crocher une m�daille de bronze. Ivan Perrillat-Boiteux occupe la 3e place, � 18 secondes de la Su�de et � 14 secondes de la Russie. Pour l'instant, c'est tout bon pour les Fran�ais.
12h00 - 3e relais, les Bleus toujours 4es
Apr�s Jean-Marc Gaillard, Maurice Manificat, Robin Duvillard a pris le relais avec 27 secondes de retard sur la Finlande et la Su�de, mais � six secondes de la R�publique tch�que.
11h56 - Relais : la France en course pour le podium
Les Fran�ais restent dans la course pour le podium dans le relais 4x10 km en ski de fond. A la mi-course, Maurice Manificat en a termin� en 4e position. La Su�de m�ne devant la Finlande et la R�publique Tch�que.
11h40 - Trespeuch "ne r�alise pas encore"
"Je crois que je ne r�alise pas encore, c'est le plus beau moment de ma vie, c'est s�r. J'ai fait une qualif moyenne, apr�s je suis rentr�e dans ma course et fait tous mes runs � fond. C'est une finale o� j'ai fait une grosse erreur mais je reviens apr�s quand m�me. Quand on arrive sur le podium, c'est le bonheur", a comment� Chlo� Trespeuch au micro de France T�l�visions.
11h11 - Snowboardcross - du bronze pour Trespeuch
Chlo� Trespeuch prend la m�daille de bronze en finale du snowboardcross. La Fran�aise a profit� d'une chute en toute fin de course. La Tch�que Eva Samkova remporte l'or apr�s avoir domin� tous les runs ainsi que les qualifications. La Canadienne Dominique Maltais est vice-championne olympique.
11h00 - Komissarova toujours dans un �tat "s�rieux"
La Russe Maria Komissarova, victime d'une fracture � la colonne vert�brale samedi lors d'un entra�nement de skicross, reste dans un �tat "stable mais s�rieux" apr�s une nuit "satisfaisante", selon sa f�d�raiton.
9h50 - Snowboardcross : lourde chute
L'Am�ricaine Jacqueline Hernandez a �t� victime d'une lourde chute dimanche lors des qualifications de l'�preuve de snowboardcross, sur le m�me parcours o� la Russe Maria Komissarova s'�tait gravement bless�e samedi en skicross. Hernandez a �t� d�s�quilibr�e sur un des nombreux sauts du parcours et s'est r�ceptionn� en travers. Sa t�te a bascul� en arri�re et a violemment heurt� la piste. Elle est rest�e inerte quelques instants avant de retrouver ses esprits et d'�tre �vacu�e sur une civi�re, consciente.
Quelques instants auparavant, la Norv�gienne Helene Olafsen avait �galement chut� sur le parcours et avait d� elle aussi �tre �vacu�e sur une civi�re.
9h30 - Miller, plus vieux m�daill� olympique en ski
L'Am�ricain Bode Miller , en enlevant � 36 ans, le bronze du super-G, devient le skieur le plus �g� � �tre m�daill� olympique en ski alpin. L'Am�ricain a d�croch� son sixi�me podium olympique � 36 ans et 127 jours exactement. Il supplante ainsi dans les annales le Norv�gien Kjetil Andre Aamodt , qui s'�tait offert sa huiti�me m�daille, sa quatri�me en or, � Turin en 2006 � 34 ans et 169 jours.
8h45 - Jansrud remporte le Super-G
Ils sont quatre � monter sur le podium du Super-G des Jeux olympiques remport� par le Norv�gien Kjetil Jansrud . Aucun Fran�ais dans le Top 10. Jansrud devance l'Am�ricain Andrew et la paire compos�e du Canadien Jan Hudec et de l'Am�ricain Bode Miller.
8h15 - Le programme du jour
6h00-9h00 - Curling messieurs - 1er tour - Etats-Unis - Canada, Grande-Bretagne - Norv�ge, Su�de - Russie (Ice Cube)
7h00-09h10 - Ski alpin - Super-G messieurs - FINALE (Site alpin - Rosa Khoutor)
8h00-9h10 - Snowboard - Snowboardcross dames - Qualifications (Parc Extr�me - Rosa Khutor)
9h00-11h30 - Hockey sur glace messieurs - Tour pr�liminaire: Autriche - Norv�ge (Palais des glaces Bolcho�)
9h00-11h30 - Hockey sur glace dames - Match de classement places 5 � 8 - Finlande - Allemagne (Shaiba Arena)
10h15-11h05 - Snowboard - Snowboardcross dames - FINALE (Parc Extr�me - Rosa Khoutor)
11h00-13h30 - Ski de fond - Relais 4x10 km messieurs - (Site de Laura)
11h00-14h00 - Curling dames - 1er tour : Danemark - Cor�e du Sud, Japon - Suisse, Su�de - Russie, Etats-Unis - Canada (Ice Cube)
13h30-16h00 - Hockey sur glace messieurs - Tour pr�liminaire: Russie - Slovaquie (Palais des glaces Bolcho�)
13h30-16h00 - Hockey sur glace messieurs - Tour pr�liminaire: Slov�nie - Etats-Unis (Shaiba Arena)
15h00-16h50 - Patinage de vitesse - 1500 m dames - (Adler Arena)
16h00-17h20 - Biathlon - Mass-start 15 km messieurs - (Site de Laura)
16h00-19h45 - Patinage artistique - Danse - Programme court (Iceberg)
16h00-19h00 - Curling messieurs - 1er tour - Norv�ge - Suisse, Chine - Canada, Allemagne - Danemark, Etats-Unis - Su�de (Ice Cube)
17h15-19h55 - Bobsleigh - Bob � 2 messieurs - Manches 1 et 2 (Centre des sports de glisse de Sanki)
18h00-20h30 - Hockey sur glace messieurs - Tour pr�liminaire: Finlande - Canada (Palais des glaces Bolcho�)
18h00-20h30 - Hockey sur glace dames - Match de classement 5-8 (Shaiba Arena)
