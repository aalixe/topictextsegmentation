TITRE: Ukraine: l'opposition exige l'amnistie des manifestants
DATE: 2014-02-16
URL: http://www.boursorama.com/actualites/ukraine-l-opposition-exige-l-amnistie-des-manifestants-f07d1a18f4f18eea052f8d26b2018e52
PRINCIPAL: 179008
TEXT:
Un millier de manifestants se sont rassembl�s dimanche 16 f�vrier devant la mairie de Kiev, �vacu�e quelques heures plus t�t, pour demander l'application "imm�diate" de la loi d'amnistie envers les manifestants poursuivis. Dur�e: 00:53
Copyright � 2013 AFP. Tous droits de reproduction et de repr�sentation r�serv�s.
Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par l'AFP. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, transmise, rediffus�e, traduite, vendue, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de l'AFP. l'AFP ne pourra �tre tenue pour responsable des d�lais, erreurs, omissions, qui ne peuvent �tre exclus ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
