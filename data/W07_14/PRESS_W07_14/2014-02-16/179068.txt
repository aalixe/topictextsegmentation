TITRE: Retour vers le futur : les lacets automatiques bient�t r�els ? - Grazia
DATE: 2014-02-16
URL: http://www.grazia.fr/au-quotidien/news/retour-vers-le-futur-les-lacets-automatiques-bientot-reels-613674
PRINCIPAL: 179066
TEXT:
Je m'abonne
Partagez : �
Souvenez-vous : en 1989, Marty McFly enfilait ses incroyables Nike Air Mag � lacets automatiques. Il reste peu de temps avant que la proph�tie se r�alise : tout juste 11 mois d'ici 2015 ! Mais il semblerait que cette fois-ci, le r�ve des milliers de fans de Retour vers le futur devienne r�alit�. En effet, le site Solecollector , cit� par le Huffington Post , rapporte que Tinker Hatfield himself, le designer des Air Max, a confirm� l'info !
"Va-t-on voir des lacets automatiques en 2015? A cette question, je r�ponds oui!", aurait-il d�clar�. Reste � savoir si ces lacets automatiques seront propos�s sur un nouveau mod�le, ou carr�ment sur les Air Mag. Cela fait d�j� cinq ans que la rumeur circule. En 2010, Slate expliquait que Nike travaillait sur un brevet depuis un an avec un "syst�me de la�age automatique comportant une s�rie de lani�res qui s�ouvrent et se ferment pour basculer d�une position rel�ch�e � une position serr�e".
En 2011, Nike avait combl� ses fans en sortant les pr�cieuses Air Mag en �dition limit�e. 1500 exemplaires avaient �t� vendus aux ench�res sur Ebay, avec les m�mes LED, mais sans lacets automatiques. On les trouve aujourd'hui sur Internet pour environ 7000$. Nike avait revers� les� fonds r�colt�s � la Fondation Michael J.fox, du nom de l'acteur de Retour vers le futur , touch� par la maladie de Parkinson.
�
