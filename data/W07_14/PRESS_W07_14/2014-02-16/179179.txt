TITRE: PHOTOS - M�t�o France : la temp�te Ulla frappe durement le grand Ouest � metronews
DATE: 2014-02-16
URL: http://www.metronews.fr/nantes/photos-meteo-france-la-tempete-ulla-frappe-durement-le-grand-ouest/mnbp!QHgJIisITE3h6/
PRINCIPAL: 179115
TEXT:
Cr�� : 16-02-2014 16:53
Temp�te Ulla : le grand Ouest durement touch�
METEO � Environ 22.000 foyers �taient encore sans �lectricit� en Bretagne ce dimanche � la mi-journ�e, cons�quence de la temp�te Ulla.
Tweet
�
Les coups de vents se suivent, depuis plusieurs semaines. Dimanche, � la mi-journ�e, 22.000 foyers �taient encore sans �lectricit� en Bretagne (15 000 dans le Finist�re, 7 000 dans les C�tes-d'Armor) apr�s le passage de la temp�te Ulla . D�apr�s ERDF cependant, l�alimentation �lectrique a �t� r�tablie pour 80�% des clients touch�s : ils �taient en effet 115 000 sans courant au pic de la temp�te. En Loire-Atlantique 2 000 foyers �taient toujours sans �lectricit� samedi midi.
La temp�te Ulla, la plus forte de l'hiver selon M�t�o-France, s'est �loign�e progressivement samedi de la Bretagne et du Cotentin en direction de la mer du Nord, apr�s avoir fait un mort sur un paquebot. Un octog�naire est en effet d�c�d� vendredi � bord d'un navire au large du Finist�re � la suite d'une chute provoqu�e par les mauvaises conditions m�t�orologiques : des vents � 150�km/h ont �t� enregistr�s dans la nuit.
Perturbations dans les transports tout le week-end
C�t� transports, un millier de voyageurs en France a pass� la nuit de vendredi � samedi dans des trains ou des salles polyvalentes : la SNCF, inqui�te des chutes d'arbres sur les voies, avait d�cid� par mesure de s�curit�, d'interrompre le trafic ferroviaire entre Rennes et Brest et entre Brest et Quimper. Quelque 500 voyageurs ont d� �tre h�berg�s pour la nuit dans des rames s�curis�es en gare de Rennes, tandis que 300 autres ont dormi dans des trains et une salle des f�tes � Saint-Brieuc. La circulation a repris progressivement samedi matin : les trains circulent de nouveau entre Lorient et Quimper, ainsi qu'entre Saint-Brieuc et Brest. Mais la ligne Brest-Quimper reste ferm�e tout le week-end.
Dans le Finist�re, particuli�rement touch� par le coup de vent, les pompiers ont men� dans la nuit 563 interventions (400 dans les C�tes-d'Armor), essentiellement en raison de chutes d'arbres. La pr�fecture maritime de l'Atlantique a �galement signal� la perte de 70 conteneurs au large de la Bretagne par un navire de la compagnie danoise Maersk. Ils ne contiennent en revanche pas de mati�res dangereuses.
�
