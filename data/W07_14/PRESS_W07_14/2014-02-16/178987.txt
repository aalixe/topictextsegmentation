TITRE: Indépendance. Nouvelle mise en garde aux Ecossais - L'essentiel - Le Télégramme
DATE: 2014-02-16
URL: http://www.letelegramme.fr/actualite/fil-info/independance-nouvelle-mise-en-garde-aux-ecossais-16-02-2014-10038544.php
PRINCIPAL: 0
TEXT:
Indépendance. Nouvelle mise en garde aux Ecossais
16 février 2014 à 18h00
José Manuel Barroso
Les Ecossais seront privés d'Europe en cas d'indépendance, a averti hier le président de la Commission européenne. 
A sept mois du référendum sur l'indépendance, José Manuel Barroso a jugé "difficile voire impossible" une adhésion d'une Ecosse indépendante à l'Union européenne. "Il sera très difficile d'obtenir l'assentiment de tous les Etats membres pour intégrer un nouvel entrant issu d'un pays" déjà dans l'UE, a-t-il expliqué. Des affirmations jugées "grotesques" par la numéro 2 du gouvernement écossais Nicola Sturgeon, partisane du "oui". "Aucun Etat membre n'a dit qu'il mettrait son veto à l'adhésion de l'Ecosse", a-t-elle poursuivi. "L'adhésion d'une Ecosse indépendante à l'UE relève de la volonté démocratique du peuple écossais et de l'avis des autres Etats membres de l'Union, pas de la Commission européenne", a-t-elle insisté.
Dans un livre blanc destiné à convaincre les Ecossais des bénéfices de l'indépendance, le gouvernement écossais avait expliqué en novembre qu'il comptait entamer des négociations avec l'Union européenne dès la victoire du "oui" acquise. L'objectif est de parvenir à une "transition en douceur et dans les délais vers une appartenance à l'UE", à savoir d'ici mars 2016, date prévue par les autorités d'Edimbourg pour la proclamation de l'indépendance. 
Les déclarations de José Maria Barroso interviennent trois jours après une autre mise au point à l'adresse des Ecossais, cette fois-ci de la part du ministre britannique des Finance. George Osborne avait jugé impossible pour une Ecosse indépendante de conserver la livre sterling . 
 
