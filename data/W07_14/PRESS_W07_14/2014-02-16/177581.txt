TITRE: Ecotaxe: les "bonnets rouges" redescendent dans la rue en Bretagne - BFMTV.com
DATE: 2014-02-16
URL: http://www.bfmtv.com/societe/ecotaxe-bonnets-rouges-redescendent-rue-bretagne-712210.html
PRINCIPAL: 177570
TEXT:
r�agir
Plusieurs centaines de " bonnets rouges " - 250, selon la gendarmerie, jusqu'� un millier, selon les organisateurs - ont manifest� samedi � proximit� du portique �cotaxe de Brec'h , entre Vannes et Lorient (Morbihan). La circulation sur la Nationale 165 devait �tre r�tablie dans la nuit.
�� �
Quatre personnes, dont un membre de forces de l'ordre, ont �t� bless�es, et trois manifestants ont �t� interpell�s dont un plac� en garde � vue, selon la gendarmerie.
�� �
Au cours d'un apr�s-midi tendu, les manifestants, qui se sont dispers�s peu avant 19h, ont lanc� des fus�es de d�tresse et des oeufs sur les forces de l'ordre emp�chant l'acc�s au portique et ces derniers ont ripost� par des grenades lacrymog�nes.
"Le mouvement est loin de s'essoufler"
Les forces de l'ordre avaient boucl� le secteur, emp�chant les journalistes d'acc�der au site en cours d'apr�s-midi, ont constat� un photographe et un vid�aste de l'AFP qui se sont fait refouler.
�� �
La RN 165 avait �t� ferm�e pr�ventivement � la circulation par les forces de l'ordre et des d�viations mises en place. Selon la gendarmerie, la circulation devait �tre normalement r�tablie sur cette route "apr�s travaux, en deuxi�me partie de nuit".
"On voulait que ce soit pacifique, mais comme les forces de l'ordre emp�chaient les gens d'approcher, �a a �nerv� tout le monde", a comment� Thierry Merret, pr�sident de la FDSEA du Finist�re et l'un des principaux animateurs du collectif "Vivre, d�cider et travailler en Bretagne", � l'origine du mouvement des Bonnets rouges.
"On esp�re que le gouvernement a compris que le mouvement des Bonnets rouges est loin de s'essouffler, poursuit-il. On a encore entendu les propos de (Jean-Marc) Ayrault et de (Fr�d�ric) Cuvillier (ministre des Transports) sur l'�cotaxe, et on n'en veut pas. Maintenant, on attend le 8 mars et on esp�re que le gouvernement va adopter une autre position".
