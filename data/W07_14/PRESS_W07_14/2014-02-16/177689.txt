TITRE: Athl�tisme, perche : Lavillenie bat le mythique record du monde de Bubka ! (VID�O) - Athl�tisme - La Voix du Nord
DATE: 2014-02-16
URL: http://www.lavoixdunord.fr/sports/athletisme-saut-a-la-perche-lavillenie-bat-le-mythique-ia222b0n1922161
PRINCIPAL: 177688
TEXT:
Le journal du jour � partir de 0,49 �
Renaud Lavillenie a franchi d�s son premier essai une barre � 6,16 m ce samedi apr�s-midi, � Donetsk, en Ukraine.
Renaud Lavillenie est entr� dans l�histoire de la perche cet apr�s-midi, en battant le record du monde que Sergue� Bubka avait �tabli 21 ans plus t�t, d�j� � Donetsk. PHOTO � LA VOIX �
- A +
L'athl�te du Clermont Athl�tisme Auvergne bat ainsi le record du monde en salle du saut � la perche, �tabli � 6,15 m par Sergue� Bubka le 21 f�vrier 1993. Il a r�alis� cet exploit lors du meeting des stars, sur les terres de l�Ukrainien, sous ses yeux m�me ! Enfin d�tr�n�, le � Tsar � est alors descendu sur la piste pour f�liciter en personne son successeur.
Champion olympique en 2012, vice-champion du monde l'an dernier, et double champion d'Europe en titre, le perchiste fran�ais ajoute une nouvelle ligne � son palmar�s, avec ce prestigieux record.
L�Auvergnat, qui affirmait depuis plusieurs semaines se sentir capable de cette performance, a rel�gu� une fois pour toutes le tsar derri�re lui dans les tablettes.
L�Ukrainien avait saut� 6,15 m en 1993 en salle et 6,14 m en 1994 en plein air. Son successeur est donc bel et bien devenu l�homme le plus haut du monde, m�me si pour �tre homologu� comme tel, il devra r��diter cet exploit hors de l�atmosph�re prot�g�e d�une salle.
Se prenant la t�te � deux mains en descendant du tapis en courant, hilare, celui que ses fans surnomment � Airlavillenie � pouvait jubiler et se f�liciter d�avoir chang� de perche au milieu du concours.
� C��tait totalement incroyable ! Je n�ai jamais utilis� cette perche pour 6,16 m. Pour ma premi�re tentative, je souhaitais r�aliser le meilleur saut possible. J�ai demand� 6,16 m parce que c�est le meilleur endroit pour battre le record de Serguei Bubka, 21 ans apr�s son record �, a-t-il d�clar�.
La l�gende de la perche mondiale, pr�sent au meeting Pole Vault Stars en costume-cravate, a �t� parmi les premiers � f�liciter le Fran�ais, regardant avec admiration la barre d�sormais de r�f�rence.
� Une nouvelle �re dans ce sport est arriv�e. Aujourd�hui, le vainqueur est un champion olympique, une personne qui a d�j� obtenu de nombreux succ�s. Nous nous attendions � cet �v�nement et nous sommes ravis que cela se soit pass� pr�cis�ment ici, � Donetsk �, a d�clar� Bubka.
� J�ai beaucoup de sympathie pour ce gars. Je suis s�r que ce n�est pas le dernier sommet qu�il atteint et que d�autres succ�s brillants l�attendent �, a-t-il soulign�.
Le perchiste fran�ais a ensuite tent� une nouvelle barre � 6,21 m, mais a largement �chou�, retombant m�me sur la piste et se blessant l�g�rement � la cheville.
Sur tous les podiums
Le champion olympique avait d�j� am�lior� son record par deux fois en janvier, effa�ant 6,04 m � Rouen puis 6,08 � Bydgoszcz (Pologne). Sa progression, r�p�tait-il, devait logiquement le conduire au record du monde. S�r de ses qualit�s, le perchiste de poche (1,77 m/70 kg), �tait en effet obs�d� par la performance du monstre physique qu��tait Bubka.
D�crit comme d�autant plus fier qu�il porte les couleurs de son pays, il se pr�sente comme ayant � horreur de la d�faite �. � Je suis une pile qui ne se d�charge jamais. Dans la vie, je vais toujours avoir l�envie de gagner. Pour la moto, le tennis, le basket, la piscine, le ping-pong. D�s que je fais quelque chose je me mets � fond �, d�clarait-il en mai dernier.
Ag� de 27 ans, il est d�ores et d�j� mont� sur tous les podiums depuis 2009, l�ann�e de son �veil international, et conquiert avec ce record la l�gitimit� des g�ants.
Il ne manque au Charentais de naissance que l�or des Mondiaux en plein air. Troisi�me en 2009, il dut encore se contenter de cette place en 2011, mais cette fois dans la peau du favori. Il a �t� une nouvelle fois deuxi�me en 2013 � Moscou, derri�re l�Allemand Raphael Holdzeppe.
Sa brillante carri�re s�inscrit dans un riche pedigree familial : chez les Lavillenie, on est en effet perchiste de g�n�ration en g�n�ration. Le grand-p�re entra�nait d�j� le p�re, et Renaud conseille son fr�re cadet Valentin, d�j� un des meilleurs de la discipline dans l�hexagone.
Renaud Lavillenie a laiss� tomber, apr�s le sacre olympique, l�entra�neur qui l�avait fait �clore, Damien Inocencio, pour le plus connu Philippe d�Encausse. Un choix d�licat qui a manifestement port� ses fruits.
Samedi soir, les r�actions abondaient sur les r�seaux sociaux pour saluer cette performance majuscule. � Record du Mooonnndeeee ! Chapeau l�Artiste �, �crivait notamment sur Twitter l�athl�te St�phane Diagana.
Le pr�sident Fran�ois Hollande a salu� un � exploit historique �.
Cet article vous a int�ress� ? Poursuivez votre lecture sur ce(s) sujet(s) :
