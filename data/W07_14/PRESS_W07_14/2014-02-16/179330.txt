TITRE: Bient�t des visas express pour les hommes d'affaires �trangers  - BFMTV.com
DATE: 2014-02-16
URL: http://www.bfmtv.com/economie/bientot-visas-express-hommes-daffaires-etrangers-712608.html
PRINCIPAL: 179329
TEXT:
> Consommation, Distribution, Pharmacie
Bient�t des visas express pour les hommes d'affaires �trangers
Nicole Bricq, la ministre du Commerce ext�rieur, a indiqu� ce dimanche 16 f�vrier que la France allait r�duire les d�lais d'obtention de visas pour les hommes d'affaires �trangers. Ils pourront �tre d�livr�s en quelques jours, contre plusieurs semaines actuellement.
Y. D .avec AFP
r�agir
Le Conseil strat�gique de l'attractivit� , que pr�sidera Fran�ois Hollande lundi 17 f�vrier,� devrait accoucher de plusieurs mesures pour attirer les investisseurs �trangers dans le pays. Ce dimanche 16 f�vrier, Nicole Bricq en a d�voil� une, qui concerne les d�lais d'obtention de visas pour les hommes d'affaires �trangers.
"Un homme d'affaires chinois qui veut venir en France, il lui faut huit semaines pour avoir un visa. Un homme d'affaires russe qui veut venir en France, il lui faut trois semaines", a d�clar� la ministre du Commerce ext�rieur, interrog�e lors du Grand rendez-vous Europe 1/iT�l�/Le Monde. "On pense qu'on peut le faire beaucoup plus rapidement et plus parler en semaine mais en journ�e", a-t-elle affirm�.
"Donner des garanties" aux investisseurs
Nicole Bricq a pr�cis� que le minist�re des Affaires �trang�res "a d�j� donn� des ordres tr�s pr�cis pour traiter cela en heures" dans certains  consulats.
La France a d�j� mis en place depuis le 27 janvier la  d�livrance de visas en 48 heures pour les touristes chinois, une  promesse faite dans le cadre des c�l�brations des 50 ans de relations  diplomatiques entre la France et la Chine populaire.
�� �
Ce  conseil strat�gique de l'attractivit� "n'avait pas �t� r�uni depuis  quelques ann�es", a rappel� la ministre, qui entend "donner un certain  nombre de garanties aux investisseurs".
A lire aussi
