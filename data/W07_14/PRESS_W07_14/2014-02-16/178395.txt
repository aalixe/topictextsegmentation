TITRE: Pacte de responsabilit� : "les contreparties, c'est un faux sujet" (Nicole Bricq)
DATE: 2014-02-16
URL: http://www.atlasinfo.fr/Pacte-de-responsabilite-les-contreparties-c-est-un-faux-sujet-Nicole-Bricq_a49748.html
PRINCIPAL: 178392
TEXT:
Pacte de responsabilit� : "les contreparties, c'est un faux sujet" (Nicole Bricq)
Dimanche 16 F�vrier 2014 modifi� le Dimanche 16 F�vrier 2014 - 12:40
"J'ai une liste de 47 pays prioritaires, j'en n'ai visit� 45, on sait pourquoi les investisseurs ne viennent pas en France", a d�clar� la ministre du Commerce ext�rieur, Nicole Bricq, dimanche, lors du Grand rendez-vous Europe1-Le Monde �i>TELE. "On a, par exemple, des probl�mes de visas. Un homme d'affaires chinois, il lui faut huit semaines pour avoir un visa. Un Russe, il lui faut trois semaines. On peut le faire beaucoup plus rapidement et ne plus parler en semaine mais en journ�e", a-t-elle insist�, � la veille du Conseil strat�gique de l'attractivit�, qui doit avoir lieu lundi � l'Elys�e.
La ministre a �galement salu� l'action de son homologue du Redressement productif, vis � vis des entreprises. "Arnaud Montebourg fait le job et son job n'est pas facile. Quand il faut sauver des emplois, il est l� Arnaud Montebourg. Partout o� il peut �tre", a salu� Nicole Bricq, reconnaissant que "les temps sont durs".
"France-Etats-Unis, c'est un couple gagnant", s'est par ailleurs r�jouie la ministre, �voquant son voyage aux Etats-Unis avec Fran�ois Hollande. Et Nicole Bricq s'est laiss�e aller � une petite confidence sur le couple pr�sidentiel am�ricain. "Michelle Obama est extraordinaire, elle est superbe. Franchement, elle fait honneur � toutes les femmes de la plan�te. Avec Barack Obama, c'est un tr�s beau couple." "Les g�ants des Etats-Unis ne sont pas des ogres. Nous n'en avons pas peur. Mais il faut respecter le secret fiscal", a, par ailleurs, d�clar� la ministre au sujet des soup�ons d'�vasion fiscale qui p�sent sur des grands groupes comme Google.
"Pierre Gattaz a am�lior� aux Etats-Unis sa conception du pacte de responsabilit�", a par ailleurs ironis� Nicole Bricq, au sujet du revirement du patron du Medef sur le sujet. Pour la ministre, la question des "contreparties" aux baisses de charge accord�es aux entreprises dans le cadre de ce pacte est d'ailleurs "un faux sujet". Le sujet, "c'est de faire respirer les entreprises pour qu'elles cr�ent des emplois et investissent. Mais c'est � elles de faire les choix", pr�cise-t-elle tout de m�me, esp�rant des r�sultats "d�s cette ann�e".
Dimanche 16 F�vrier 2014 - 11:08
Avec JDD
