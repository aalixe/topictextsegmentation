TITRE: Renaud Lavillenie: "Ce n�est pas fini" - BFMTV.com
DATE: 2014-02-16
URL: http://www.bfmtv.com/sport/lavillenie-n-est-pas-fini-712502.html
PRINCIPAL: 0
TEXT:
RECORD / Lavillenie se projette sur les JO de Rio - 16/02
Renaud Lavillenie, avez-vous dig�r� votre exploit de samedi ?
�a fait � peine 24 heures que �a s�est pass�. Je n�ai pas r�ellement eu le temps de tout r�aliser. Je viens tout juste de poser les pieds en France. Apr�s mes 6,08m il y a 15 jours, il a fallu que je rentre chez moi pour me rendre compte de l�ampleur . L�, je ne suis pas encore rentr� chez moi donc �a va prendre un peu plus de temps (rires). Quand je vois le nombre de cam�ras, il n�y a qu�apr�s les Jeux (2012) que j�en ai vu autant. C�est �norme.
Avez-vous r�cup�r� physiquement ?
Non. Toujours pas de sommeil, c�est la r�gle jusqu�� lundi. J�aurai le temps pour r�cup�rer de tout �a, donc je ne suis pas press�.
Aviez-vous pens� succ�der � Sergue� Bubka ?
Dans mes r�ves, peut-�tre�! C�est assez irr�el. Bubka, ce n�est pas un athl�te lambda, donc c�est assez extraordinaire. En plus, j�ai eu des moments tr�s forts avec lui, avant la comp�tition, pendant et apr�s.
Vous rendez-vous compte que votre vie va changer ?
J�en ai bien l�impression (rires). Personnellement, je n�ai pas envie de changer et je ferai en sorte de ne pas changer. Je commen�ais d�j� � �tre difficilement tranquille � Clermont donc je pense que �a va �tre encore pire. C�est la ran�on de la gloire et �a fait aussi plaisir de voir �a . Il vaut mieux �a que de sortir de l�a�roport en recevant des tomates et des cailloux�!
Quand avez-vous pass� le cap ?
L�ann�e des Jeux a �t� constructive. Apr�s Londres, j�ai fait un choix qui a �t� critiqu� (se s�parer de son entra�neur, ndlr) mais qui, au final, cloue le bec � tout le monde parce que j�ai mis une claque � mon record et je suis bien meilleur qu�avant. �a passe par un entra�neur beaucoup plus serein et disponible. Avec les bons conseils au bon moment, on a travaill� et gagn� un peu plus de puissance.
Pouvez-vous encore sauter plus haut ?
Je ne vais pas m�arr�ter, donc il n�y a pas de raison pour ne pas essayer de battre ce record. Apr�s, il n�y a pas de date pr�vue. L�objectif, c�est de voir comment je vais pouvoir r�cup�rer au niveau de mon pied. Ce n�est pas fini, j�ai 27 ans. L�avantage, c�est que j�ai encore du temps devant moi, j�ai encore de belles saisons � venir.
Allez-vous faire comme Bubka et y aller centim�tre par centim�tre ?
Bubka a pris le record � 5,90m et l�a amen� � 6,15m. Moi, si je le prends � 6,16m, je ne vais pas en faire 35�! En avoir fait un, c�est d�j� �norme. Si je peux arriver, dans les ann�es � venir, � en faire un ou deux autres, ce sera extraordinaire. Pour l�instant, l�objectif c�est de se dire ��savoure ce que tu viens de faire��, parce que �a ne se reproduira peut-�tre jamais.
Est-ce plus fort que votre m�daille d'or aux JO ?
�a n�a rien � voir. L�, c�est extraordinaire. Le fait que �a se passe � Donetsk, devant Sergue� Bubka� Des champions olympiques � la perche, il y en a eu plusieurs. Un recordman du monde, il n�y en a qu�un et �a change compl�tement la dimension. Tout l�engouement que je vois depuis est extraordinaire. Je ne suis pas s�r qu�il y ait eu un tel engouement aux Jeux. Il n�y a pas de mots.
C'est donc encore plus d'�motion...
Oui parce que ce record n��tait pas pr�m�dit�. Il y a deux ou trois ans, je faisais bonne figure en disant que j�y pensais, mais je savais qu�il �tait quand m�me assez loin. Depuis l�an dernier, je me suis rendu compte que je pouvais vraiment m�en rapprocher. Mais c�est au-del� de ce que je pouvais imaginer . Tant mieux, parce que rien n�est �crit. Je vais faire le maximum � chaque fois, comme � chaque entrainement et chaque comp�tition.
Votre blessure au pied, sur votre essai � 6,21m, peut-elle vous priver des Mondiaux en salle en Pologne (du 7 au 9 mars) ?
Il n�y a aucune certitude l�-dessus. Je dois passer des examens ce dimanche et lundi pour voir o� �a en est et je pourrai en dire plus lundi.
En voulant allez plus haut, vous avez p�ch� par enthousiasme ?
Mon coach Philippe (d�Encausse) me connait bien et savait que je n�allais pas m�arr�ter l�. Il fallait que je tente quelque chose, c�est dans ma nature. J�ai voulu tent� les 6,21m. L�objectif n��tait pas de dire ��remets encore 5 cm � ton record�� mais plut�t ��fais un saut pour te rendre compte de ce qui t�attend��. Pas de chance, la perche se fait �jecter du butoir et elle m�a �ject�, mais pas au bon endroit. A la r�ception, le pied a pris cher. Mais si c��tait � refaire, je le referais sans h�siter, malgr� la blessure. M�arr�ter, �a n�aurait pas �t� moi.
A lire aussi :
