TITRE: Afrique du Sud�: plus de 200�mineurs pris au pi�ge dans une mine d'or ill�gale
DATE: 2014-02-16
URL: http://www.lemonde.fr/afrique/article/2014/02/16/afrique-du-sud-plus-de-200-mineurs-pris-au-piege-dans-une-mine-d-or-illegale_4367568_3212.html
PRINCIPAL: 0
TEXT:
Afrique du Sud�: plus de 200�mineurs pris au pi�ge dans une mine d'or ill�gale
Le Monde |
� Mis � jour le
17.02.2014 � 09h37
Plus de 200�mineurs seraient pris au pi�ge depuis samedi soir dans une mine d' or ill�gale � l'est de Johannesburg. ��Nous sommes entr�s en communication avec une trentaine de mineurs coinc�s. Ils nous disent qu'il y en aurait 200�autres en dessous d'eux��, a d�clar� le porte-parole de l'organisation priv�e de secours d'urgence ER24, Werner Vermaak.
Les mineurs, dont le nombre total n'a toujours pas �t� confirm�, �taient descendus dans la mine exploit�e ill�galement, creus�e derri�re un stade de cricket, dans le quartier de Benoni. Ils n'ont pas pu ressortir en raison de la chute d'un bloc de rocher qui a bloqu� l'issue du puits.
CERTAINS REFUSENT DE REMONTER � LA SURFACE
L'alerte a �t� donn�e par des policiers en patrouille, inform�s par un passant qui avait entendu des cris en provenance du sous-sol. Les accidents de mines,�exploit�es l�galement ou non, sont relativement fr�quents en Afrique du Sud , un pays riche en minerais.
Les op�rations de sauvetage ont �t� interrompues environ deux heures apr�s la sortie des onze rescap�s. Craignant d' �tre arr�t�s pour ��travail clandestin�� et ��intrusion��, un nombre inconnu d'hommes ont refus� de remonter � la surface.
Le secteur minier sud-africain est en outre agit� depuis le 23�janvier par une gr�ve qui mobilise quelque 80�000�mineurs employ�s par les trois premiers producteurs mondiaux de platine et qui r�clament une augmentation de salaire.
Selon les estimations d'une commission d' enqu�te parrain�e par le gouvernement, les accidents de mine ont fait 69�000�morts en Afrique du Sud au XXe si�cle. Le nombre d'accidents a toutefois chut� ces derni�res ann�es dans le secteur. Les chiffres donn�s par les syndicats font ainsi �tat de 112�morts en�2012.
