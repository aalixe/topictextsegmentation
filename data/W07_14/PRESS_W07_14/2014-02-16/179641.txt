TITRE: Nouvelle temp�te de neige au Japon: 15 morts, plus de 25 cm de neige � Tokyo (+vid�o)
DATE: 2014-02-16
URL: http://www.meteo-world.com/news/index-3733.php
PRINCIPAL: 179637
TEXT:
Je n'ai pas de dates de s�jour pr�cises
Rechercher
Nouvelle temp�te de neige au Japon: 15 morts, plus de 25 cm de neige � Tokyo (+vid�o)
Le 16-02-2014 � 21h15  - Envoyer une photo
Cliquez sur l'image
Neige � Tokyo
Cr�dits: Vid�o youtube
Les temp�tes de neige se succ�dent au Japon, et on d�plore la mort d?au moins 15 personnes et plus de 1650 bless�s dans 30 pr�fectures.
Un syst�me de basse pression a laiss� plus d'un m�tre de neige � Kofu et 27 cm dans le centre de Tokyo samedi.
Selon l'Agence m�t�orologique japonaise, les r�gions int�rieures de Kanto, de Koshinetsu et la partie sud de la r�gion de Tohoku ont re�u beaucoup de neige, avec plus d?un m�tre de neige � Kofu.
143 centim�tres de neige ont �t� mesur�s � Fuji-Kawaguchi, dans la pr�fecture de Yamanashi, 141 cm � Kusatsu (pr�fecture de Gunma), 98 cm � Chichibu (pr�fecture de Saitama), 99 cm � Karuizawa (pr�fecture de Nagano) et  73 cm � Maebashi.
La neige a �galement caus� d'importantes perturbations dans les transports, routiers, a�riens et ferroviaires.
Selon la soci�t� Tokyo Electric Power Co, les fortes chutes de neige et la glace sur les lignes �lectriques, ont caus� des pannes d'�lectricit� pour 200.000 foyers � Tokyo et huit autres pr�fectures.
Vid�o post�e par Dimitri Trush sur youtube
(http://www.meteo-world.com)
Reproduction interdite sauf accord de l'�diteur ou utilisation du flux d'actualit�.
Commentez cet article !
������ Article vu: 1815 fois ������ Proposer un article:
Webmasters, affichez nos actualit�s sur votre site, cliquez ici
M�t�o world.com
