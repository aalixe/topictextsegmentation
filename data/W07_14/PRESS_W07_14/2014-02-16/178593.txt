TITRE: Afrique du Sud : 200 mineurs "pi�g�s" - BBC Afrique - Afrique
DATE: 2014-02-16
URL: http://www.bbc.co.uk/afrique/region/2014/02/140216_sa-mines.shtml
PRINCIPAL: 178591
TEXT:
Imprmer la page
Un vieux centre d'extraction mini�re
En Afrique du Sud, les services de secours tentent d'acc�der � plus de 200 mineurs ill�gaux pi�g�s sous terre dans une cage d'or abandonn�e dans une banlieue � l'est deJohannesburg, selon un porte-parole des services d'urgence.
Werner Vermaak des services d'urgence ER24 a d�clar� � l�agence Reuters que les secouristes communiquaient avec un groupe d'environ 30 mineurs pi�g�s par des rochers tomb�s au-dessous de l'ancien site � Benoni.
"Ils nous ont dit qu�il y a environ 200 autres coinc�s en dessous d'eux," a indiqu� Vermaak .
"C'est un puits de mine abandonn� au milieu d�un parc public (Des champs ouverts) ... ce n'�tait pas une zone bloqu�e " , a-t-il ajout� .
Vermaak a pr�cis� qu�aucune blessure ou accident n�a �t� signal� jusqu'� pr�sent.
Des �quipements lourds ont �t� d�ploy�s pour essayer d'enlever le rocher obstruant le lieu d�impact.
L'exploitation mini�re ill�gale de sites abandonn�s est commune en Afrique du Sud, o� les mineurs creusent des minerais en toute ill�galit�, vivant souvent dans des conditions souterraines dangereuses et pr�caires.
Les accidents mortels sont fr�quents, et les batailles souterraines entre groupes rivaux ont �galement �t� souvent enregistr�es.
Les mineurs ill�gaux envahissent parfois les mines actives.
En 2009, au moins 82 hommes qui rel�veraient de l�informel sont morts apr�s un incendie souterrain dans une mine d'or en Afrique du Sud.
Nos principaux journaux
