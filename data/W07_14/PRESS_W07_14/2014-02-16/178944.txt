TITRE: Foot - Angleterre - Cup - Everton encore en quarts
DATE: 2014-02-16
URL: http://www.lequipe.fr/Football/Actualites/Everton-encore-en-quarts/442053
PRINCIPAL: 0
TEXT:
a+ a- imprimer RSS
Pour son premier match avec Everton, Lacina Traor� a marqu� sur son premier ballon touch�. (Reuters)
Pour la troisi�me fois cons�cutive, Everton va disputer les quarts de finale de la Coupe d�Angleterre. Ce dimanche, l��quipe bas�e � Liverpool a �limin� Swansea ( 3-1 ). Toujours priv�e de Lukaku, elle a vu Lacina Traor� ouvrir le score sur son premier ballon jou� depuis son pr�t par Monaco : d�une subtile talonnade, le grand attaquant ivoirien a pouss� au fond des filets un ballon de Distin (5e).
Naismith d�cisif � deux reprises
Naismith a ensuite b�n�fici� d�une passe en retrait manqu�e de Taylor pour marquer (65e). L�attaquant des Toffees a aussi obtenu un penalty transform� par Baines (73e). Les hommes de Martinez, qui ont tout de m�me encaiss� une t�te de De Guzman (16e), rejoignent donc Sunderland, Wigan et Manchester City au prochain tour.
C.O.-B.
