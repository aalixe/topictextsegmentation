TITRE: � Quand G�rard Depardieu s��clate au Carnaval de Nice� (video) Stars Actu
DATE: 2014-02-16
URL: http://www.stars-actu.fr/2014/02/gerard-depardieu-seclate-au-carnaval-de-nice-video/
PRINCIPAL: 0
TEXT:
Tweeter
Cr�dits : PR Photos
G�rard Depardieu s�est particuli�rement bien amus� vendredi soir lors de la c�r�monie d�ouverture du 130�me Carnaval de Nice. Il faut dire que cette ann�e le th�me avait de quoi le r�jouir : la gastronomie !
Et comme vous allez le voir sur ces br�ves images capt�es par le Parisien , notre G�g� national s�est �clat� comme un gosse en aspergeant ses voisins les plus proches avec des bombes � mousse.
Aux journalistes du Parisien qui avaient fait le d�placement, il a d�clar� ��C�est la premi�re fois que je participe au carnaval. Cette ann�e, c�est particulier, c�est la gastronomie qui est f�t�e, je ne pouvais pas rater �a.�
L�acteur avait aussi une autre raison de venir dans cette belle ville de Nice : cette ann�e il est en effet repr�sent� sur l�un des chars ��Le retour de Gargantua��.
Dans le dossier de presse de cette �dition 2014, ce char est pr�sent� comme �tant celui de Gargantua, un homme qui se tourne vers la Russie o� les taxes et l�art de vivre semblent plus souriants.
Pour info, le 130eme Carnaval de Nice se termine le 4 mars. Pour plus d�infos, rendez-vous sur www.nicecarnaval.com
