TITRE: Le tsar, c'est lui ! - Athl�tisme - Sports.fr
DATE: 2014-02-16
URL: http://www.sports.fr/athletisme/articles/lavillenie-le-geant-de-la-perche-1010798/?sitemap
PRINCIPAL: 178770
TEXT:
Renaud Lavillenie a battu le record du monde à son premier essai. (Reuters)
Par Damien Richy
15 février 2014 � 19h28
Mis à jour le
16 février 2014 � 15h38
Il l’a fait. Sous les yeux et sur les terres du tsar, Renaud Lavillenie a battu samedi à Donetsk le record du monde du saut à la perche vieux de 21 ans. Le champion olympique en titre s’est envolé à 6,16 mètres, un centimètre plus haut que Sergueï Bubka. 
Sur le toit du monde. En un instant, Renaud Lavillenie a dépassé le maître. Sous les yeux de Sergueï Bubka, le Français a effacé ce samedi le record du monde du saut à la perche, signé par l'Ukrainien il y a 21 ans. A son premier essai à 6,16 mètres, le champion olympique a dépassé d'un centimètre la barre historique, réussie par le tsar au même endroit, à Donetsk, le 21 février 1993.
Plus que jamais tourné vers le record depuis son saut à 6,08 mètres, Renaud Lavillenie avait coché ce 15 février 2014. Au meeting indoor de Donetsk, chez Bubka, le Tricolore a survolé la compétition, comme souvent, se retrouvant seul au-delà des six mètres. En danger après deux échecs à 6,01 mètres, le Français s'est rassuré en passant cette marque lors de son dernier essai. Suffisamment pour demander la barre suivante au-dessus du record.
Lavillenie l'avait annoncé
Encouragé par le public ukrainien, Lavillenie a signé le saut parfait. L'athlète de 27 ans a parfaitement enroulé son corps au-dessus de la barre, retombant sur le tapis les yeux écarquillés, comme ébahi d'avoir signé l'exploit à sa première tentative. Les bras grands ouverts, le Français a commencé à réaliser, filant célébrer sa performance dans les bras du héros de la discipline, descendu sur la piste.
"C'est magique", pouvait savourer son frère Valentin Lavillenie au micro de BFM TV, avant de glisser: "J'étais confiant. Il me l'a dit une demi-heure avant, « je vais battre le record du monde. » " "C’est un rêve, souriait le père du champion. C'est beau, et il a encore de la marge." "C'est une performance extraordinaire, hors-norme", surenchérissait Stéphane Diagana.
Même le détrôné Bubka, beau joueur, félicitait le nouveau géant de la perche. "C’est un grand jour ! Je suis très heureux que Renaud ait réalisé ce saut chez moi. Il met la perche et l’athlétisme en lumière. Je suis très heureux et très fier de lui, car c’est un grand athlète, un ambassadeur de la perche. Et j’ai toujours pensé, encore ce matin quand nous en avons discuté ensemble, que ce serait bien si je luis passais le témoin." Encore fallait-il effacer cette barre légendaire. Lavillenie l'a fait. 
