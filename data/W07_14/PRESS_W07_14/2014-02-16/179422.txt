TITRE: Italie: Matteo Renzi convoqu� � la pr�sidence de la R�publique - France - RFI
DATE: 2014-02-16
URL: http://www.rfi.fr/europe/20140216-italie-matteo-renzi-preparation-premier-ministre-convocation-napolitano/
PRINCIPAL: 179421
TEXT:
Modifi� le 16-02-2014 � 20:23
Italie: Matteo Renzi convoqu� � la pr�sidence de la R�publique
Matteo Renzi, le 16 f�vrier 2014 � Pontassieve, pr�s de Florence.
REUTERS/Stringer
Le leader du Parti d�mocrate devrait devenir Premier ministre, lundi 17 f�vrier, apr�s avoir �t� re�u par le chef de l'Etat italien Giorgio Napolitano. Ce dernier l'a officiellement convoqu� � Rome pour un entretien pr�vu � 9h30�TU. Matteo Renzi, 39 ans, se pr�pare donc � prendre la t�te du gouvernement. Une t�che ardue.
Avec notre correspondant � Rome, Anne Le Nir
Matteo Renzi se pr�pare, mais avec difficult�. Il y a plusieurs obstacles � franchir. Le premier, c�est qu�il doit encore trouver un accord avec son potentiel alli� principal, Angelino Alfano, leader du parti Nouveau centre droit (NCD). Celui-ci demande d�une part de garder son fauteuil de vice-pr�sident du Conseil et de ministre de l�Int�rieur. De surcro�t, il r�clame un programme de gouvernement pr�cis, et surtout pas trop orient� � gauche.
Deuxi�me obstacle�: Matteo Renzi a d�j� essuy� deux refus importants. Au minist�re de la Culture, il proposait l��crivain Alessandro Baricco, tr�s c�l�bre dans le monde entier gr�ce � son roman Soie. Mais celui-ci a refus�. Et puis, il aurait �galement voulu int�grer � l'�quipe un dirigeant tr�s important, celui du groupe Luxottica, le g�ant de l�optique. Mais Andrea Guerra a lui aussi lui a dit ��non��. Il �tait pressenti au D�veloppement �conomique.
Ensuite, Matteo Renzi n�a toujours pas trouv� la ��perle��, c'est-�-dire le ministre charg� de l�Economie et des Finances. Donc, il va sans doute se donner encore quelques jours pour poursuivre ses n�gociations. Par cons�quent, le vote de confiance auquel il devra �tre soumis au Parlement ne devrait pas se tenir avant jeudi ou vendredi.
