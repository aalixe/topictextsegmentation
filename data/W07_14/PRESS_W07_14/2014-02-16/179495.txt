TITRE: Football Ligue 1 - A l�OM, les jeunes ne bossent pas regrette Valbuena - Marseille - Foot 01
DATE: 2014-02-16
URL: http://www.foot01.com/ligue1/a-l-om-les-jeunes-ne-bossent-pas-regrette-valbuena,136491
PRINCIPAL: 179493
TEXT:
A l�OM, les jeunes ne bossent pas regrette Valbuena
Photo Icon Sport
Publi� Dimanche 16 F�vrier 2014 � 21h03 Dans�: Marseille , Ligue 1 .
Beaucoup d�explications ont �t� avanc�es pour tenter de d�crypter les raisons des pales prestations de l�OM en cette premi�re partie de saison. Courant d�cembre, outre le recrutement manqu�, le manque d�exp�rience, les mauvais choix des entraineurs ou le manque de r�alisme offensif comme d�fensif, il a �t� beaucoup question d�un �tat d�esprit d�plorable au sein du groupe olympien. Un conflit entre des jeunes qui se voyaient un peu trop beaux, et des anciens d�pit�s de ne plus �tre �cout�s aurait m�me scind� le vestiaire en deux selon l�avis de quelques observateurs. Si quelques incidents ont bien eu lieu, Mathieu Valbuena avoue qu�il ne faut pas dramatiser, mais reconna�t que les jeunes joueurs du club ne sont pas vraiment des bosseurs, et cela ne passe pas.�
� Aujourd�hui, je pense que parfois les jeunes ne prennent pas trop conscience de l� o� l�on se trouve, il faut �tre souvent derri�re eux pour cette notion de travail, bosser, faire beaucoup plus. Quand on est dans un club comme l�OM ou il y a une certaine exigence, il faut d�j� �tre exigent envers soi-m�me. Jos� a su remettre beaucoup de discipline. C�est important. Avec l�exp�rience, je n�ai plus envie de perdre de l��nergie par rapport � tout �a. Quand �a ne va pas, on essaie toujours de chercher la petite b�te, savoir si les jeunes ne s�entendent pas avec les anciens� Non, je ne pense pas. Je ne fais pas de langue de bois, je trouve qu�il y a une bonne ambiance. Il y a forc�ment des personnalit�s diff�rentes, il y a eu des altercations mais je pense que �a fait partie du m�tier �, a soulign�, sur Canal +, un Mathieu Valbuena qui reconna�t implicitement qu�Elie Baup avait perdu le contr�le sur son groupe, et que Jos� Anigo a serr� la vis � ce niveau � son arriv�e.�
On comprend mieux pourquoi ils ont tous d�marr�s sur le banc face a Saint� ....
Lundi 17 F�vrier 2014 � 12h07 R�pondre
Verratti Marco
Je pense que c'est pareil un peu partout, les joueurs fran�ais ont une sale mentalit�. Carlo avait raison
Lundi 17 F�vrier 2014 � 09h10 R�pondre
supporterdujeu
Je ne mets pas cela sur la mentalit� mais sur l'�ducation et l'envie de r�ussir.
Certains ont plus envie de r�ussir que d'autres.
En France, les gens sont bien plus laxistes, c'est une �vidence.
Lundi 17 F�vrier 2014 � 10h02 R�pondre
Verratti Marco
C'est bien pour �a qu'au niveau europ�en, nos clubs ont du mal. :)
Lundi 17 F�vrier 2014 � 10h19 R�pondre
supporterdujeu
Ou alors le PSG o� il y a trop de professionnels et peu de fran�ais...
Les fran�ais restent sur le banc :)
Lundi 17 F�vrier 2014 � 10h48 R�pondre
Verratti Marco
Quand tu vois ce que �a donne quand on aligne bcp de fran�ais, perso je pr�f�re les �trangers ^^
Lundi 17 F�vrier 2014 � 10h57 R�pondre
OL-Stars
c'est vrai qu'on voit pas beaucoup de jeune du centre de formation jouer � l'OM ! quand on vois qu'a l'OL, 6 joueurs issu du centre de formation de l'OL �tait titulaire contre ajaccio, c'est tout simplement �norme.
Dimanche 16 F�vrier 2014 � 23h40 R�pondre
supporterdujeu
Je crois que tu es hors-sujet l� :)
Lundi 17 F�vrier 2014 � 10h03 R�pondre
OL-Stars
Ahh non, il parle des jeunes � marseille au d�but puis apres il commence � parler de lui .. je faisait une simple comparaison entre ceux de marseille & l'OL :p
Lundi 17 F�vrier 2014 � 17h57 R�pondre
lyonnaise.
Mouais. Il dit beaucoup de choses et m�lange (volontairement) un peu tout. Il tacle les jeunes (certainement � juste titre) mais nous dit derri�re que ce n'est pas pour autant qu'il y a une mauvaise ambiance dans le vestiaire. Lui m�me le dit "quand �a ne va pas", les gens cherchent tout le temps la petite b�te. Mais on sait tous que quand les r�sultats ne sont pas l�, les probl�mes dans un vestiaire surgissent. Donc bon, l� c'�tait surtout un message au journaliste pour leur dire de se m�ler de leur affaires et de ne pas souligner dans la presse les probl�mes qu'un groupe peut rencontrer. "Il y a eu des altercations" donc il y eu une mauvaise ambiance � un moment donn�. Enfin bref, c'est tellement plus facile de parler maintenant que �a va mieux...
Dimanche 16 F�vrier 2014 � 21h49 R�pondre
sam
bien dit VALBUENA maintenant j attends de voir si LARQUE ou BRAVO vont le descendre comme ils l ont fait avec van derwiel sur les memes declarations... la mentalit� professionnelle en France me soulent
Dimanche 16 F�vrier 2014 � 21h30 R�pondre
ParisestMagik2
Dimanche 16 F�vrier 2014 � 21h30 R�pondre
�
