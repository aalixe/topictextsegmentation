TITRE: Le choc pour les Etats-Unis - Fil info - Sotchi 2014 - Jeux Olympiques -
DATE: 2014-02-16
URL: http://sport24.lefigaro.fr/jeux-olympiques/sotchi-2014/fil-info/le-choc-pour-les-etats-unis-679434
PRINCIPAL: 177492
TEXT:
Le choc pour les Etats-Unis
15/02 16h21 - JO 2014, Hockey
Le choc entre les Etats-Unis et la Russie pour d�signer le premier de la poule A a tourn� en faveur des Am�ricains, vainqueurs � l�issue d�une sensationnelle s�rie de tirs au but.� Le score �tait de 2-2 � la fin du temps r�glementaire. Les Etats-Unis se sont finalement impos�s 4-3 gr�ce � un dernier tir au but inscrit par Oshie apr�s huit tentatives.�
