TITRE: RCA: quelques �gros poissons� anti-balaka aux mains de la Misca - France - RFI
DATE: 2014-02-16
URL: http://www.rfi.fr/afrique/20140216-rca-quelques-gros-poissons-anti-balaka-mains-misca/
PRINCIPAL: 179296
TEXT:
Modifi� le 16-02-2014 � 21:29
RCA: quelques �gros poissons� anti-balaka aux mains de la Misca
Un combattant anti-balaka � Bangui, le 14 janvier.
REUTERS/Siegfried Modola
R�publique centrafricaine
Une op�ration de d�sarmement et de police a �t� men�e hier matin, samedi 15 f�vrier, � Bangui, dans le quartier de Boy Rab, fief des anti-balaka, ces milices accus�es depuis plusieurs semaines de s'en prendre � des civils musulmans. Plusieurs meneurs ont �t� arr�t�s. Ce dimanche matin le procureur �tait au si�ge de la Misca.
Ghislain Gresenguet, le procureur de la R�publique centrafricaine, a pr�par�, ce dimanche matin, avec les responsables de la Misca, la force africaine sur place, le transf�rement des personnes arr�t�es. C'est d�ailleurs sur la base d'un mandat qu'il avait �mis que la force africaine a pu proc�der hier aux arrestations. Les premiers interrogatoires ont eu lieu hier � Mpoko, au si�ge de la Misca, mais c'est maintenant � la justice centrafricaine de prendre le relais.
�? A (RE)LIRE : Centrafrique: vaste op�ration Sangaris-Misca � Bangui
On sait que sur la dizaine de personnes arr�t�es figurent quatre responsables des anti-balaka, et notamment le lieutenant Konat� et le lieutenant Ganagi Herv�. Ils sont consid�r�s comme des fauteurs de trouble par les autorit�s qui les accusent d'embrigader des jeunes pour semer le d�sordre. M�me au sein de leur mouvement, ils sont point�s du doigt.
Les anti-balaka divis�s
Les anti-balaka sont en fait d�sormais divis�s. Il y a une tendance qui r�clame le retour � l'ordre constitutionnel, c'est-�-dire le retour du pr�sident Boziz�. C'est celle qui est dans le collimateur de la justice. Un de leur coordonnateur, Patrice Edouard Ngaissona a �chapp� d'ailleurs hier au coup de filet de la Misca et est toujours recherch�.
L'autre tendance collabore avec les nouvelles autorit�s. Son objectif, c'�tait le d�part du pr�sident Djotodia. Maintenant que c'est fait, cette tendance a d�cid� de rentrer dans le rang. Dans ce camp-l�, on se f�licite de l'op�ration men�e hier. C'�tait le seul moyen de revenir au calme, confie l'un d'eux, le capitaine Kamizoulaye.
Joint par RFI, ce militaire explique que la rupture est consomm�e entre les deux tendances, que le dialogue est devenu presque impossible. C'est la guerre m�me au sein des anti-balaka, regrette-t-il.
Aux environs de 12 heures, on a proc�d� au transf�rement (...) Demain, ils seront entendus par les �l�ment de la PJ, la police judiciaire, pour qu'une proc�dure soit mont�e en bonne et due forme dans les 24 ou 48 heures qui suivent
Ghislain Gr�senguet, procureur de la R�publique, s'exprime sur RFI soir ce dimanche 16/02/2014                                    - par RFI
