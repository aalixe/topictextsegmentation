TITRE: Le Celta surprend Villarreal - Fil info - Espagne - Etranger - Football -
DATE: 2014-02-16
URL: http://sport24.lefigaro.fr/football/etranger/espagne/fil-info/le-celta-surprend-villarreal-679559
PRINCIPAL: 177681
TEXT:
Le Celta surprend Villarreal
16/02 00h23 - Football, Liga
Le Celta Vigo s'est impos� sur la pelouse de Villarreal samedi soir lors de la 24e journ�e de Liga (0-2). Orellana et Nolito ont inscrit les buts galiciens dans les dix derni�res minutes. Rappelons qu'entre ces deux buts, le match a �t� interrompu en raison d'un jet de bombe lacrymog�ne sur la pelouse.
R�sultats de la 24e journ�e :
Vendredi�
