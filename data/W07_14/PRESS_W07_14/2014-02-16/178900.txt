TITRE: Live Salon Gen�ve 2014 : Volkswagen Scirocco restyl� - les news du Salon de Gen�ve avec Turbo.fr
DATE: 2014-02-16
URL: http://www.turbo.fr/actualite-automobile/619238-salon-geneve-2014-volkswagen-scirocco-restyle/
PRINCIPAL: 178899
TEXT:
Live Salon Gen�ve 2014 : Volkswagen Scirocco restyl�
Publi�  le 07 mars 2014 par C�dric Pinatel
1 commentaire
DR�
Ca y est, le Volkswagen Scirocco arrive au stade du restylage de mi-carri�re. Comme pour la Polo, les modifications sont tr�s discr�tes sur le plan esth�tique.
C'est d�cid�ment la saison des restylages pour Volkswagen, qui d�voilait r�cemment sa Polo de cinqui�me g�n�ration pass�e par la traditionnelle �preuve du bistouri. Dans la gamme du constructeur allemand, le coup� Scirroco est encore plus vieux et re�oit donc lui aussi un restylage pour gagner en esp�rance de vie face � la concurrence.
CLIQUEZ ICI POUR VOIR NOTRE DIAPORAMA PHOTO LIVE GENEVE VOLKSWAGEN SCIROCCO RESTYLE
Si vous trouviez le restylage de la Polo trop discret, vous aurez sans doute � peu pr�s la m�me r�action en d�couvrant les premi�res images du nouveau Scirocco. Les boucliers �voluent, les optiques et les feux arri�res changent mais dans l'ensemble, les lignes restent extr�mement proches de la premi�re mouture du mod�le. Rappelons que ce Scirocco premi�re mouture avait en son temps inaugur� la nouvelle philosophie stylistique chez Volkswagen, avec sa calandre tr�s horizontale.
L'habitacle �volue lui aussi assez peu puisqu'on retrouve une planche de bord au style similaire, qui int�gre simplement un syst�me d'infotainment enti�rement nouveau. On note aussi un volant au design in�dit et une sellerie revue.
Comme pour la Polo, c'est sous le capot que le Scirocco restyl� �volue le plus avec une gamme de motorisations am�lior�e, plus puissante mais aussi meilleure en consommation quelque soit la version. Le 1,4 litres TSI de base passe � 125 chevaux et le 2,0 litres TSI passe � 180, 220 ou 280 chevaux sur la version R. Cot� diesel, le 2,0 litres TDI est d�sormais disponible en version 150 et 184 chevaux.
Pr�sent sur le stand de la marque au salon de Gen�ve 2014, ce Scirocco restyl� sera lanc� au mois d'ao�t sur notre march�.
SUR LE M�ME TH�ME :
