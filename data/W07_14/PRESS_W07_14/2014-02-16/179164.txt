TITRE: Ukraine: l'opposition exige l'amnistie "imm�diate" des manifestants - Flash actualit� - Monde - 16/02/2014 - leParisien.fr
DATE: 2014-02-16
URL: http://www.leparisien.fr/flash-actualite-monde/ukraine-les-manifestants-ont-evacue-la-mairie-16-02-2014-3595791.php
PRINCIPAL: 179161
TEXT:
Ukraine: l'opposition exige l'amnistie "imm�diate" des manifestants
Publi� le 16.02.2014, 08h52
L'opposition ukrainienne a somm� dimanche le pouvoir d'abandonner "imm�diatement" les poursuites contre les manifestants, quelques heures apr�s avoir �vacu� la mairie de Kiev, lieu symbolique de la contestation contre le r�gime du pr�sident Viktor Ianoukovitch.� | Martin Bureau
1/3
R�agir
L'opposition ukrainienne a somm� dimanche le pouvoir d'abandonner "imm�diatement" les poursuites contre les manifestants, quelques heures apr�s avoir �vacu� la mairie de Kiev, lieu symbolique de la contestation contre le r�gime du pr�sident Viktor Ianoukovitch.
"Nous lan�ons un ultimatum au pouvoir : s'il n'annonce pas imm�diatement une r�habilitation totale et inconditionnelle dans le cadre des 2.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
000 affaires (impliquant les manifestants), nous allons r�int�grer la mairie", a lanc� Andri� Illenko, d�put� du parti nationaliste Svoboda devant plusieurs centaines de manifestants r�unis devant la mairie.
Il a pr�cis� que cet ultimatum expirait "dans quelques heures".
La mairie prise d'assaut le 1er d�cembre et transform�e en "QG de la r�volution" a �t� �vacu�e en bon ordre dans la matin�e, les contestataires ne laissant comme traces de leur passage que les nombreuses affiches et autocollants dont ils avaient d�cor� les lieux.
"Il n'y a pas eu de d�g�ts, nous esp�rons qu'il s'agit d'un premier pas vers une normalisation de la situation", a comment� Volodymyr Make�enko, chef du conseil municipal de Kiev.
Mais en milieu d'apr�s-midi, plusieurs centaines de manifestants se sont � nouveau rassembl�s devant le b�timent.
La mairie �tait un lieu tr�s symbolique de la contestation tout comme le Ma�dan tout proche, la place centrale de Kiev occup�e depuis la volte-face pro-russe du pr�sident Viktor Ianoukovitch fin novembre, au d�triment d'un rapprochement avec l'Union europ�enne.
Le chef de la diplomatie de l'UE, Catherine Ashton, a appel� dimanche le gouvernement ukrainien � abandonner toutes les accusations en cours contre les manifestants de l'opposition, tout en saluant les derniers gestes des deux parties pour apaiser les tensions.
Sur le Ma�dan, l'un des leaders de l'opposition, Arseni Iatseniouk, a quant � lui, aussi exig� "l?abandon imm�diat des poursuites contre les manifestants", mena�ant de lancer "une offensive pacifique", si ce n'�tait pas le cas devant plusieurs dizaines de milliers de manifestants.
Les responsables de deux des principaux partis d'opposition se sont dirig�s dans la foul�e vers la r�sidence de campagne du procureur g�n�ral, pour faire davantage  pression.
L'�vacuation d'ici � lundi de la mairie �tait un pr�alable exig� par les autorit�s pour amnistier les 234 manifestants arr�t�s, qui ont �t� lib�r�s, mais qui encourent toujours de lourdes peines pouvant aller jusqu'� 15 ans de prison.
C'est l'ambassadeur de Suisse, Christian Schoenenberger, dont le pays assure la pr�sidence tournante de l'OSCE (Organisation pour la s�curit� et la coop�ration en Europe), qui est entr� le premier dans le b�timent apr�s son �vacuation.
"Nous partons d'ici le coeur gros, mais on ne discute pas les ordres, on les ex�cute. Je suis d��u, mais il faut que nos camarades soient amnisti�s", a comment� Andri�, dirigeant une unit� d?autod�fense de la mairie.
- "Mauvaise d�cision" -
Le b�timent, o� avaient �t� install�s une cantine et un h�pital de fortune, h�bergeait jusqu'� 700 manifestants qui y dormaient et s'y r�chauffaient.
Les manifestants disposent toujours de plusieurs b�timents, notamment la Maison des syndicats, dont les autorit�s n'ont pas exig� l'�vacuation.
Mais l'annonce de l'abandon de la mairie a �t� mal v�cue par les dizaines de milliers de personnes r�unies sur le Ma�dan, pour la onzi�me fois depuis le d�but il y a pr�s de trois mois du mouvement de contestation.
"Qu?a obtenu l?opposition, au cours des n�gociations avec Ianoukovitch et au Parlement? Rien! L?�vacuation de la mairie est une mauvaise d�cision", regrette Anatoli Datsenko, linguiste.
Une amertume partag�e par de nombreux manifestants dimanche, alors que la participation �tait inf�rieure aux 70.000 personnes rassembl�es le 9 f�vrier.
Les n�gociations avec le pouvoir sont au point mort, qu'il s'agisse de la r�forme constitutionnelle r�duisant les pouvoirs pr�sidentiels au profit du gouvernement et du Parlement ou de la nomination d'un nouveau Premier ministre.
- Rencontre avec Merkel -
"J?attends un plan d?actions. Je ne veux pas la r�p�tition de pneus br�l�s et de cocktails Molotov, mais en m�me temps ce sont les seuls arguments que le pouvoir comprenne", juge Oksana Antochtchenko, �conomiste, faisant allusion aux violences qui ont �clat� en janvier dans le centre de Kiev et qui ont fait quatre morts et plus de 500 bless�s.
L'un des responsables de l'opposition, Arseni Iatseniouk, a annonc� qu'il allait rencontrer lundi � Berlin la chanceli�re Angela Merkel, pour demander � l'Europe une aide financi�re pour l'Ukraine et l?instauration d'un r�gime sans visa pour les Ukrainiens en Europe.
"Nous avons besoin d'aide. Nous n'avons pas besoin de paroles, nous avons besoin d'actes", a-t-il soulign�.
Le mouvement de contestation en Ukraine s'est transform� au fil des semaines en un rejet pur et simple du r�gime du pr�sident Ianoukovitch, et ni la d�mission du gouvernement ni les n�gociations n'ont r�gl� le conflit.
