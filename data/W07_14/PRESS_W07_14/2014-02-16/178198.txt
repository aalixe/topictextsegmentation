TITRE: JO de Sotchi, snowboardcross. La Française Chloé Trespeuch en bronze - Sotchi 2014 - Le Télégramme
DATE: 2014-02-16
URL: http://www.letelegramme.fr/sports/jeux-olympiques-sotchi-2014/jo-de-sotchi-snowboardcross-la-francaise-chloe-trespeuch-en-bronze-16-02-2014-10038470.php
PRINCIPAL: 178188
TEXT:
JO de Sotchi, snowboardcross. La Française Chloé Trespeuch en bronze
16 février 2014 à 11h30
France's Chloe Trespeuch competes in the Women's Snowboard Cross seeding runs at the Rosa Khutor Extreme Park during the Sochi Winter Olympics on February 16, 2014.  AFP PHOTO / JAVIER SORIANO
La Tchèque Eva Samkova a été sacrée championne olympique de snowboardcross, dimanche à Rosa Khoutor.  Samkova a dévancé la Canadienne Dominique Maltais et Chloé Trespeuch, qui offre à la France sa cinquième médaille depuis le début des JO.
Chloé Trespeuch, 19 ans, deux fois vice-championne du monde junior, rejoint Martin Fourcade (2 médailles d'or en biathlon), Jean-Guillaume Béatrix (bronze en biathlon) et Coline Mattel (bronze en saut à skis féminin).
En finale, elle a arraché la médaille de bronze en profitant de la chute de deux concurrentes placées devant elle dans les dernières centaines de mètres.
Samkova faisait figure de favorite, car elle domine le circuit de la Coupe du monde depuis le début de la saison, avec deux succès en quatre étapes.
