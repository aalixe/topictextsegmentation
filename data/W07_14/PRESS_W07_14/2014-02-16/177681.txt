TITRE: 24e j.: du gaz lacrymog�ne interrompt bri�vement Villarreal-Celta - Football - Eurosport
DATE: 2014-02-16
URL: http://www.eurosport.fr/football/24e-j.-du-gaz-lacrymogene-interrompt-brievement-villarreal-celta_sto4138300/story.shtml
PRINCIPAL: 0
TEXT:
24e j.: du gaz lacrymog�ne interrompt bri�vement Villarreal-Celta
le 16/02/2014 � 01:06
-
Le match entre Villarreal et le Celta Vigo (0-2) a d� �tre interrompu un quart d'heure samedi au stade Madrigal lors de la 24e journ�e du Championnat d'Espagne, � cause d'un fumig�ne apparemment lacrymog�ne lanc� sur la pelouse juste apr�s le premier but des visiteurs.
La bouteille de gaz a atterri dans la surface de r�paration du Celta � trois minutes de la fin du temps r�glementaire (87), contraignant l'arbitre � suspendre le jeu alors que les Galiciens venaient de prendre l'avantage gr�ce � un but de Fabian Orellana (83).
D'apr�s les images diffus�es � la t�l�vision, on a alors vu les spectateurs quitter la tribune, visiblement incommod�s par cette fum�e.
"Au d�but, je pensais que c'�tait un feu de Bengale, a expliqu� Yoel, le gardien du Celta, au micro de Gol TV. Avec ce type de gaz, sur le moment les yeux et la gorge te piquent, tu t'�touffes mais apr�s �a passe, un peu d'eau sur le visage et on peut continuer le match."
Le jeu a finalement pu reprendre quelques minutes plus tard devant des tribunes largement vid�es.
Le Celta a alors doubl� la mise sur un coup franc direct de Nolito (90) pour s'imposer 2-0.
Villarreal a aussit�t condamn� le spectateur � l'origine de cet "incident lamentable".
"Il s'agit de quelqu'un qui a lanc� (la bouteille) depuis un vomitoire et est sorti en courant du stade", a expliqu� en conf�rence de presse Fernando Roig, pr�sident du club.
"C'est une bouteille fumig�ne semblable � celles utilis�es par la police", a-t-il ajout�, pr�cisant que personne n'avait apparemment �t� bless�.
"La police est en train de v�rifier qui est cette personne et j'esp�re qu'on arrivera � l'identifier et qu'elle sera punie", a poursuivi le dirigeant. "Villarreal condamne absolument ce type d'acte qui ne repr�sente pas notre public tranquille."
Le capitaine du "sous-marin jaune", Bruno Soriano, a lui aussi estim� que c'�tait "une chose qui n'arrive jamais (� Villarreal)".
"Je ne comprends pas ce qu'a voulu faire cette personne mais ce genre de choses ne doit pas arriver chez nous. J'esp�re qu'on va la retrouver et qu'elle sera punie", a-t-il dit sur Gol TV.
�
