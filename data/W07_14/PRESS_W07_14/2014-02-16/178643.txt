TITRE: Fiat veut transformer sa filiale Fga Capital en banque
DATE: 2014-02-16
URL: http://www.boursorama.com/actualites/fiat-veut-transformer-sa-filiale-fga-capital-en-banque-065b2378d919e1b7c80e0e7397da7a4d
PRINCIPAL: 0
TEXT:
Fiat veut transformer sa filiale Fga Capital en banque
Reuters le 16/02/2014 � 15:18
Imprimer l'article
FIAT VEUT TRANSFORMER SA FILIALE FGA CAPITAL EN BANQUE
MILAN (Reuters) - Fiat, qui vient de monter � 100% dans sa filiale am�ricaine Chrysler, a demand� � la Banque d'Italie d'accorder une licence bancaire � sa coentreprise de financement Fga Capital afin de r�duire ses co�ts, a-t-on appris dimanche d'une source au fait du dossier.
Une licence bancaire rendrait le constructeur turinois plus comp�titif face � des concurrents comme Volkswagen et Peugeot qui ont d�j� ce statut pour leurs activit�s de cr�dit, et lui donnerait acc�s aux pr�ts bon march� propos�s par la Banque centrale europ�enne (BCE) dans le cadre de son programme LTRO, a not� la source.
"Fiat n'a pu avoir acc�s aux fonds de la BCE pendant les deux ann�es d'op�rations de financement � long terme (LTRO) parce qu'il n'avait pas de banque", a dit la source � Reuters, confirmant une information du quotidien Il Sole 24 Ore.
La demande de licence bancaire a �t� pr�sent�e � la Banque d'Italie cette ann�e, a-t-on ajout� sans �tre plus pr�cis.
Fga Capital a �t� mis sur pied en 2006 en tant que partenariat � 50-50 entre Fiat et le Cr�dit agricole, qui a un important r�seau de banque de d�tail en Italie.
L'an dernier, Fiat et la banque fran�aise ont prolong� leur coentreprise jusqu'� la fin 2021.
Fiat a des co�ts de financement �lev�s du fait de ses notes de cr�dit en cat�gorie sp�culative et cherche � les r�duire pour am�liorer ses marges.
Le groupe automobile s'est refus� � tout commentaire et aucune r�action n'a pu �tre obtenue dimanche � la Banque d'Italie.
Lisa Jucca, V�ronique Tison pour le service fran�ais
� 2013 Thomson Reuters. All rights reserved.
Reuters content is the intellectual property of Thomson Reuters or its third party content providers. Any copying, republication or redistribution of Reuters content, including by framing or similar means, is expressly prohibited without the prior written consent of Thomson Reuters. Thomson Reuters shall not be liable for any errors or delays in content, or for any actions taken in reliance thereon. "Reuters" and the Reuters Logo are trademarks of Thomson Reuters and its affiliated companies.
