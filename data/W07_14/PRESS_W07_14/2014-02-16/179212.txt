TITRE: Le directeur des Victoires défend son émission - 7SUR7.be
DATE: 2014-02-16
URL: http://www.7sur7.be/7s7/fr/1540/TV/article/detail/1794684/2014/02/16/Le-directeur-des-Victoires-defend-son-emission.dhtml
PRINCIPAL: 0
TEXT:
Tweets about "Virginie Guilhaume"
Malgr� les bons r�sultats r�alis�s par les Victoires de la musique sur France 2 vendredi soir (3,1 millions de t�l�spectateurs, meilleure audience depuis 2010), la c�r�monie a �t� critiqu�e dans la presse et sur les r�seaux sociaux. Manque de rythme et pr�sentation parfois approximative, l'annuelle c�l�bration de la musique fran�aise n'a pas convaincu tout le monde.
Ce qui a un peu �nerv� le directeur g�n�ral des Victoires de la musique, Gilles Desangles. Celui-ci a r�pondu aux d�tracteurs sur sa page Facebook , une mise au point relay�e par le site Puremedias.
"Ce qu'on peut lire comme b�tises sur les r�seaux asociaux... On se dit qu'on va se rattraper avec la presse �crite, et on est effar� par les mensonges, les approximations et la mauvaise foi... Oublions les commentaires inutilement agressifs et parfois ridicules, les comparaisons stupides (qui, parmi ces experts auto proclam�s a vu des Grammy 2014 un autre moment que la formidable s�quences des Daft Punk...)", �crit-il. "R�jouissons nous d'avoir permis � plus de 3 millions de t�l�spectateurs de d�couvrir pour la premi�re fois � la t�l�vision en live la remarquable Christine & The Queens, mais aussi les non moins remarquables Cats and Trees, La Femme, 1995, Hollysiz, Albin de la Simone. R�jouissons nous aussi d'avoir donn� � chaque artiste une libert� in�dite en t�l�vision, afin qu'il s'exprime selon son d�sir et non celui pr�-format� qu'on reproche si souvent � d'autres �missions."
Il tient �galement � d�fendre Virginie Guilhaume, la plus critiqu�e suite � l'�mission. "Qui peut sans d�montrer une mauvaise foi coupable retenir de plus de 3 heures d'animation de notre tr�s convaincant tandem d'animateurs Virginie Guilhaume et Bruno Guillon un nom d'album mal prononc� ? ("Outroune" pour le "Outrun" de Kaninsky, ndlr)
Allez r�jouissons nous de cette aventure collective, �a a �t� compliqu� cette ann�e, mais �a valait notre peine !"
Lire aussi
