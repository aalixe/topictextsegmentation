TITRE: Football - MAXIFOOT - OM : Anigo a instaur� une nouvelle discipline
DATE: 2014-02-16
URL: http://news.maxifoot.fr/info-193165_140216/football.php
PRINCIPAL: 178665
TEXT:
OM : Anigo a instaur� une nouvelle discipline
�
Depuis sa prise de fonctions le 7 d�cembre dernier, Jos� Anigo a d'abord �t� confront� � un vestiaire tr�s indisciplin� mais le successeur d'Elie Baup a su ramener un peu d'ordre dans la maison.
L'entra�neur marseillais a ainsi recadr� plusieurs joueurs, comme le rapporte L'Equipe ce dimanche. Pour mettre fin aux retards r�p�t�s � l'entra�nement, Anigo a aussi mis en place des petits d�jeuners collectifs avant la s�ance. Ce qui permet � tout le monde de d�marrer le travail � l'heure pr�vue ensuite.
La nomination d'Albert Emon au poste d'adjoint a �galement fait du bien � certains joueurs qui prenaient un peu trop de libert�s.
(Par Pierre-Damien Lacourte)
� News lue par 18017 visiteurs
� �
