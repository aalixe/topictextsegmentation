TITRE: Fraude de GDF Suez: r�vision des contrats?: L'Echo
DATE: 2014-02-16
URL: http://www.lecho.be/actualite/entreprises_energie/Tromperie_hallucinante_de_GDF_Suez_la_Belgique_n_est_pas_une_colonie.9467584-3020.art?itm_campaign%3Dfloorteaser
PRINCIPAL: 178398
TEXT:
Fraude de GDF Suez: r�vision des contrats?
Article
Sauvegarder Envoyer Corriger Imprimer Lien
Les communes actionnaires ont exig� des pr�cisions ce lundi au conseil d'administration d'Electrabel Custormer Solutions sur les soup�ons de fraude. GDF Suez aurait surfactur� des contrats de livraison de gaz � sa propre filiale belge pour contourner le fisc. En attendant ces pr�cisions, l'approbation du budget pluriannuel, en ce compris les contrats d'achat de gaz, est report� au prochain conseil, le 17 mars.
