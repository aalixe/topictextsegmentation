TITRE: Officiel�: Hambourg remercie son entra�neur�!
DATE: 2014-02-16
URL: http://www.footmercato.net/breves/officiel-hambourg-remercie-son-entraineur_124452
PRINCIPAL: 177485
TEXT:
Officiel�: Hambourg remercie son entra�neur�!
Officiel�: Hambourg remercie son entra�neur�!
15/02/2014 - 22 h 18
Avant dernier de Bundesliga, Hambourg a une nouvelle fois �t� d�fait ce samedi, corrig� par l�Eintracht Brunswick (4-2). La d�faite de trop pour la direction du club allemand, qui a d�cid� de se s�parer de son entra�neur, Bert van Marwijk.
��Nous avons �t� forc�s de prendre cette d�cision, m�me si nous le regrettons. Nous remercions Bert van Marwijk pour son travail��, a  sobrement communiqu� son club pour annoncer le limogeage de l�ancien s�lectionneur des Pays-Bas.
L�o Vanpoulle
