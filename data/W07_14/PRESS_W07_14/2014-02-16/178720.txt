TITRE: Plus de 200 mineurs pris au pi�ge dans une mine d�or en Afrique du Sud | La-Croix.com
DATE: 2014-02-16
URL: http://www.la-croix.com/Actualite/Monde/Plus-de-200-mineurs-pris-au-piege-dans-une-mine-d-or-en-Afrique-du-Sud-2014-02-16-1107374
PRINCIPAL: 178699
TEXT:
petit normal grand
Plus de 200 mineurs pris au pi�ge dans une mine d�or en Afrique du Sud
Plus de 200 mineurs seraient pris au pi�ge dimanche 16 f�vrier dans une mine d�or ill�gale � l�est de Johannesburg.
16/2/14 - 16 H 06
�Mamphela Ramphele�: ��L�Afrique du Sud doit prendre un nouveau d�part��
"Nous sommes entr�s en communication avec une trentaine de mineurs coinc�s. Ils nous dit qu�il y en avait 200 autres en dessous d�eux", a d�clar� � l�AFP Werner Vermaak, porte-parole de l�organisation priv�e de secours d�urgence ER24.
Le porte-parole n�a pas �t� en mesure de confirmer par lui-m�me la pr�sence des 200 autres mineurs et des responsables municipaux n�ont de leur c�t� confirm� que le chiffre d�une trentaine de personnes prises au pi�ge.
Les mineurs �taient descendus samedi dans la mine exploit�e ill�galement, creus�e derri�re un stade de cricket, dans le quartier de Benoni. Ils n�ont pas pu ressortir en raison de la chute d�un bloc de rocher qui a bloqu� l�issue du puits.
Des exploitations ill�gales et dangereuses
"Nous sommes en train de tenter de les secourir", a d�clar� � l�AFP le porte-parole des services de secours de la municipalit� d�Ekurhuleni, Roggers Mamaila, dont fait partie Benoni.
Des �quipements lourds d�excavation ont �t� amen�s sur place mais, a dit M. Vermaak, "l�acc�s (aux mineurs) est actuellement tr�s difficile".
Les accidents de mines sont fr�quents en Afrique du Sud, qu�elles soient exploit�es l�galement ou non. De nombreux mineurs sans travail n�ont pour seule ressource que de se livrer � l�exploitation ill�gale des mines d�saffect�es, et cela dans des conditions tr�s dangereuses. Cette activit� ill�gale, r�guli�rement d�nonc�e par les organisations internationales, co�te cher en vies humaines.
La Croix avec AFP
