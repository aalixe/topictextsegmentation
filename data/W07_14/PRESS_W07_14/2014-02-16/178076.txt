TITRE: JO 2014 / SNOWBOARD - Snowboardcross (F) : Hernandez victime d'une lourde chute
DATE: 2014-02-16
URL: http://www.sport365.fr/jo-2014-sotchi/snowboardcross-f-hernandez-victime-d-une-lourde-chute-1104034.shtml
PRINCIPAL: 178075
TEXT:
JO 2014 / SNOWBOARD - Publi� le 16/02/2014 � 09h10
0
Snowboardcross (F) : Hernandez victime d'une lourde chute
L'Am�ricaine Jacqueline Hernandez a �t� victime d'une lourde chute dimanche lors des qualifications de snowboardcross.
Victime d�une mauvaise r�ception apr�s un saut, Jacqueline Hernandez a chut�. Sa t�te a bascul� en arri�re et a violemment heurt� la piste. L�Am�ricaine est rest�e quelques instants sans bouger avant de reprendre conscience et d'�tre �vacu�e sur une civi�re.
Quelques instants auparavant, c�est la Norv�gienne Helene Olafsen, victime elle aussi d�une chute, qui avait d� �tre �vacu�e.
Pour rappel, samedi, Maria Komissarova a �t� victime d�un grave accident lors d�un entra�nement de skicross. Touch�e � la colonne vert�brale sur ce parcours du Parc Extr�me de Rosa Khoutor, la Russe avait �t� transport�e d�urgence � l'h�pital de Krasna�a Poliana o� elle avait subi une op�ration de plus de 6 heures. Hier, son �tat �tait jug� � s�rieux et stable. �
