TITRE: Egypte : quatre morts dans un attentat contre un bus de touristes cor�ens
DATE: 2014-02-16
URL: http://www.latribune.fr/actualites/economie/international/20140216trib000815589/egypte-quatre-morts-dans-un-attentat-contre-un-bus-de-touristes-coreens.html
PRINCIPAL: 178822
TEXT:
Egypte : quatre morts dans un attentat contre un bus de touristes cor�ens
latribune.fr �|�
16/02/2014, 16:52
�-� 723 �mots
Le chauffeur et au moins trois touristes �trangers ont �t� tu�s dans cette attaque � la bombe contre un bus transportant une trentaine de Cor�ens. C'est la premi�re attaque contre des �trangers en Egypte depuis 2009 qui risque de mettre l'�conomie touristique � terre alors que le pays est ensanglant� par la multiplication des attentats depuis la destitution de Morsi
sur le m�me sujet
Mohamed Morsi retenu par l'arm�e
Une bombe visant un autobus de touristes sud-cor�ens a tu� au moins quatre personnes dimanche dans le Sina� �gyptien, dans le premier attentat contre des �trangers depuis que l'arm�e a destitu� le pr�sident islamiste Mohamed Morsi.
L'attaque, perp�tr�e au poste-fronti�re de Taba, une station baln�aire sur la fronti�re avec Isra�l, a �galement fait 14 bless�es et n'a pas �t� revendiqu�e dans l'imm�diat. Le chauffeur �gyptien figure parmi les morts, a annonc� le minist�re de l'Int�rieur, en ajoutant que le v�hicule transportait des "touristes cor�ens" mais sans donner de pr�cision sur la nationalit� des autres victimes.
Les attentats sont devenus fr�quents Egypte depuis depuis que l'arm�e a destitu� et arr�t� M. Morsi le 3 juillet, mais ils ne visaient jusqu'� pr�sent que les forces de l'ordre. Ils ont �t�, pour la plupart, revendiqu�s par Ansar Beit al-Maqdess, un groupe jihadiste bas� dans le Sina�, disant s'inspirer d'Al-Qa�da et assurant agir en repr�sailles � la r�pression sanglante men�e par le nouveau pouvoir dirig� de facto par l'arm�e contre les partisans de M. Morsi.
La bombe a explos� � l'avant du bus alors que le chauffeur attendait au passage frontalier de Taba, a annonc� le minist�re de l'Int�rieur dans un communiqu�, sans pr�ciser dans quel sens les touristes franchissaient la fronti�re entre Isra�l et l'Egypte. Le haut de l'autocar jaune a �t� litt�ralement d�vast� par la d�flagration et l'incendie qui a suivi. Le porte-parole du minist�re de la Sant�, Ahmed Kamel, a pr�cis� � l'AFP qu'il �tait impossible de reconna�tre les corps.
Fuite des touristes
L'attaque de dimanche risque de pousser un peu plus l'�conomie de l'Egypte vers le gouffre, le pays des pyramides et des c�l�bres spots de plong�e de la Mer Rouge �tant d�sert� par les touristes depuis la r�volte populaire de 2011 qui a chass� le pr�sident Hosni Moubarak du pouvoir.
Aucun attentat n'avait plus vis� des �trangers en Egypte depuis f�vrier 2009, quand une Fran�aise avait �t� tu�e par l'explosion d'une grenade en bordure du souk de Khan el-Khalili, au coeur du Caire historique. Il s'agissait alors de la premi�re attaque terroriste contre des Occidentaux en Egypte depuis 2006.
Entre 2004 et 2006, nombre d'Egyptiens et de touristes �trangers avaient cependant p�ri dans des attentats dans les stations baln�aires du Sina�. Et en 1997, un commando d'insurg�s islamistes avaient mitraill� des touristes sur le site des c�l�bres temples de l'Egypte antique de Louxor, dans le sud, tuant 58 �trangers, des Suisses pour la majorit�.
La s�curit� s'est consid�rablement d�grad�e en Egypte depuis que le g�n�ral Abdel Fattah al-Sissi, chef de la toute-puissante arm�e, a destitu� le premier pr�sident d�mocratiquement �lu du pays. Depuis plus de sept mois, le pouvoir mis en place et dirig� de facto par le nouvel homme fort de l'Egypte r�prime dans un bain de sang toute manifestation des partisans de M. Morsi.
Plus de 1.400 personnes, des manifestants islamistes pour la plupart, ont ainsi �t� tu�es par la police ou l'arm�e, selon Amnesty international, dont la moiti� au cours de la seule journ�e du 14 ao�t au Caire. Depuis le 3 juillet �galement, les attentats et attaques visant la police et l'arm�e se sont multipli�s dans tout le pays, mais surtout au Caire. La plupart ont �t� revendiqu�s par Ansar Be�t al-Maqdess, mais le gouvernement accuse les Fr�res musulmans, la confr�rie de M. Morsi, de les avoir commandit�s ou organis�s.
Plusieurs milliers de membres de cette organisation d�sormais d�clar�e "terroriste" ont �t� arr�t�s, dont la quasi-totalit� de leurs dirigeants. Ils encourent, � l'instar de M. Morsi en personne, la peine de mort dans divers proc�s, d�nonc�s comme "politiques" par les accus�s mais aussi par des organisations internationales de d�fense des droits de l'Homme.
M. Morsi est d'ailleurs apparu dimanche � l'ouverture du troisi�me proc�s --pour "espionnage"-- sur les quatre qui lui sont intent�s. A la barre, il a d�nonc� une "farce" de la part du r�gime issu selon lui d'un "coup d'Etat militaire".
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Commentaires
Michel �a �crit le 16/02/2014                                     � 17:32 :
Il serait temps pour l'ONU de remette un peu d'ordre en Egypte puisque le conflit semble prendre une dimension internationale. Embargo sur le transport de passagers depuis et vers l'Egypte � d�faut d'une exp�dition punitive (militaire) qui ne r�soudra rien. Qui voudrait subir le terrorisme �gyptien sur son sol ou en vacance?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
