TITRE: Stromae: quel guignol! - Gala
DATE: 2014-02-16
URL: http://www.gala.fr/l_actu/news_de_stars/stromae_quel_guignol_308730
PRINCIPAL: 0
TEXT:
Le 16/02/2014 Stromae: quel guignol!
Comme pr�vu, Stromae a d�croch� sa marionnette aux Guignols. En parall�le des Victoires de la musique dont il est sorti grand gagnant, le Belge a fait ses d�buts dans la mythique �mission de Canal + pour le plus grand plaisir des t�l�spectateurs.
Attendue depuis plusieurs semaines , la marionnette de Stromae a enfin fait ses grands d�buts aux Guignols de l�info, ce vendredi. Un r�sultat probant tant au niveau de la voix que du physique. Pour cette premi�re, la tendance �tait au second degr�. Une coutume dans l��mission satirique de Canal +. En effet, les Guignols sont revenus sur les textes sombres de l�artiste. Ainsi, le Belge s�est retrouv� dans le r�le d�animateur de� �d�tresses party�. On a donc pu voir Stromae en train de c�l�brer un licenciement avec une reprise de son titre Formidable en Fort virable. Un clin d��il amusant, qui devrait certainement faire rire l'int�ress�.
�
Stromae continue sa formidable ascension. Vendredi soir, l�interpr�te de Papaoutai a - largement - triomph� aux Victoires de la musique . Dans un Z�nith de Paris tout acquis � sa cause, le chanteur a rafl� les prix de �l�artiste masculin�, du�meilleur clip� et de �l�album de chansons de l�ann�e�. Un pl�biscite. �Je prends toutes ces victoires comme de formidables reconnaissances par les pairs, les pros, les moins pros et le public. Voir autant d�engouement, je ne peux que remercier. On ne fait pas de la musique pour le succ�s. Si le succ�s arrive, tant mieux� a reconnu le jeune homme au moment de la remise des troph�es. A 29 ans, Stromae a d�j� tout connu ou presque. Son dernier album Racine Carr�e s�est vendu � plus d�1,5 millions d�exemplaires. Ses spectacles se jouent � guichets ferm�s. Une gloire m�rit�e pour Stromae qui avouait cependant dans Les Inrocks en d�cembre �ne pas encore r�aliser vraiment ce succ�s� . Mais que l�artiste s�y fasse, son conte de f�es n�est pas pr�s de s�arr�ter.
A lire aussi :
