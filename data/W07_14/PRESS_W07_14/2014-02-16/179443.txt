TITRE: Tempête: le courant rétabli à 100% lundi
DATE: 2014-02-16
URL: http://www.lefigaro.fr/flash-actu/2014/02/16/97001-20140216FILWWW00130-tempete-le-courant-retabli-a-100-lundi.php
PRINCIPAL: 179400
TEXT:
le 16/02/2014 à 18:15
Publicité
Un total de 10.000 foyers restaient sans électricité dimanche en fin d'après-midi en Bretagne, conséquence de la tempête Ulla, a indiqué à l'AFP Electricité Réseau Distribution France (ERDF), qui prévoit un rétablissement du réseau à 100% lundi.
Un porte-parole d'ERDF a précisé que le nombre de foyers privés de courant descendrait à moins de 5000 d'ici 21h ou 22h ce dimanche, ajoutant que 95% des clients auront alors retrouvé de l'électricité.
Au total, 115.000 clients avaient été touchés au plus fort de la tempête.
ERDF explique avoir déployé des moyens exceptionnels pour rétablir au plus vite le courant. 1000 agents étaient sur le terrain et une centaine de poteaux, qui avaient été arrachés par la force des vents, ont été remplacés.
ERDF a mis en place une centaine de groupes électrogènes pour assurer un approvisionnement provisoire en électricité.
La tempête Ulla, la plus forte de l'hiver selon Météo-France, s'est éloignée progressivement samedi de la Bretagne et du Cotentin en direction de la mer du Nord, après avoir fait un mort sur un paquebot.
Selon Météo Consult , après 2 mois particulièrement mouvementés, une légère amélioration se dessine sur nos côtes en ce debut de semaine dont il faut profiter... car dès mercredi, le ballet des perturbations océaniques reprend.
En outre, trois départements de l'Ouest, le Morbihan, l'Ille-et-Vilaine et la Loire-Atlantique, étaient toujours en vigilance orange "inondations", selon le dernier communiqué de Météo-France publié dimanche, valable jusqu'à 16H00 lundi.
LIRE AUSSI:
