TITRE: La musique, atout ma�tre face au  vieillissement- RTL pour elle- RTL.be
DATE: 2014-02-16
URL: http://www.rtl.be/pourelle/article/la-musique-atout-maitre-face-au-vieillissement-274765.htm
PRINCIPAL: 177649
TEXT:
La musique, atout ma�tre face au  vieillissement
AFP , le 13 f�vrier 2014 � 09h26
On savait d�j� que la musique adoucissait les moeurs, mais voil� qu'elle adoucit aussi la vieillesse, en agissant sur la m�moire et la motricit�, selon les chercheurs invit�s � un colloque organis� par la Sacem, la soci�t� des auteurs et compositeurs.
"La musique n'active pas une zone, mais plusieurs r�gions du cerveau", expliquait mardi Herv� Platel, professeur de neuropsychologie. Tr�s longtemps, les scientifiques ont cru, sur la base d'observations empiriques, que le cerveau "droit" �tait musicien et le cerveau "gauche" celui du langage.
La neuro-imagerie c�r�brale a boulevers� la donne depuis une trentaine d'ann�es. Les travaux d'Herv� Platel, apr�s ceux du pionnier Bernard Lechevalier (unit� Inserm U1077) ont permis d'�tablir une "cartographie" c�r�brale de la m�moire musicale chez des sujets musiciens et non musiciens.
Non seulement les deux h�misph�res c�r�braux sont impliqu�s, mais on a not� chez les musiciens une hypertrophie d'une r�gion du cerveau, l'hippocampe, qui joue un r�le cl� dans la m�moire. C'est aussi une des rares zone du cerveau � produire de nouveaux neurones pendant toute la vie.
"La musique transforme le cerveau en accroissant certaines zones", rench�rit Emmanuel Bigand (Unit� mixte de recherche CNRS 5022). Depuis vingt ans, son unit� de recherche s'est sp�cialis�e sur le lien entre musique et cognition (les processus de connaissance: m�moire, raisonnement, langage...).
Ses travaux montrent que la musique active de nombreux r�seaux c�r�braux de fa�on synchronis�e. "Deux zones du cerveau d�cident de travailler ensemble", explique-t-il, "et c'est b�n�fique pour toutes les autres activit�s de l'�tre humain".
Des �tudes ont montr� que les enfants qui font de la musique voyaient leurs comp�tences scolaires s'am�liorer. Les jeunes seniors qui commencent une pratique musicale ont un d�clin cognitif r�duit.
Un atelier de chant propos� � des malades d'Alzheimer, � Bi�ville (Manche), � quelques kilom�tres de Caen, a produit des r�sultats inesp�r�s. Les r�sidents, bien qu'atteints de troubles s�v�res de la m�moire, ont �t� capables de retenir de nouvelles chansons, et de s'en souvenir, pour certains, m�me apr�s un arr�t de plus de quatre mois.
Un atelier d'apprentissage de la guitare manouche, dont les effets seront suivis par les scientifiques, va d�buter cette ann�e.
Tango et Alzheimer
A l'Abbaye de La Pr�e, dans l'Indre, des malades accueillis en s�jour th�rapeutique ont vu leur �tat s'am�liorer spectaculairement avec une pratique r�guli�re du chant et du tango, anim�e par des artistes, �galement en r�sidence.
"J'ai vu se transformer les gens qui �taient l�", a t�moign� la r�alisatrice Anne Bramard-Blagny, auteur d'un documentaire de 44 minutes, "La m�lodie d'Alzheimer".
On peut y voir un patient qui ne marchait qu'avec un d�ambulatoire se lever pour danser avec la danseuse de tango argentin Carolina Udoviko.
France Mourey, sp�cialiste des effets du vieillissement sur la motricit�, rappelle que les malades d'Alzheimer souffrent non seulement de troubles de la m�moire, mais aussi de troubles moteurs tr�s handicapants. "La musique et le mouvement aident ces patients � maintenir leur �quilibre et leur motricit�" s'�merveille-t-elle.
Les malades qui marchent en musique marchent mieux et plus vite. "Comme la musique, la motricit� passe par plusieurs zones du cerveau, cela va avec le d�sir, le plaisir, l'�motion", souligne-t-elle.
A l'Abbaye de La Pr�e, les malades se l�vent avec empressement pour aller chanter et danser. En revanche, une malade convi�e � une "�valuation" des effets de l'atelier a aussit�t perdu tout entrain et... une bonne partie de ses comp�tences.
Difficile de mesurer si l'am�lioration de l'�tat des patients est d� � l'impact de la pratique musicale ou � une ambiance g�n�rale de mieux-�tre, � l'int�r�t retrouv� pour la vie, � la convivialit� de la danse.
"Pour les victimes de l�sions c�r�brales, les malades d'Alzheimer, ceux de Parkinson, la musique est un atout", avance prudemment Emmanuel Bigand. "Avant d'utiliser la musique comme un m�dicament avec un effet dose, il y a encore beaucoup de travail", convient Herv� Platel.
Le docteur Odile Letortu, � l'origine de l'atelier de chant de Caen, met quant � elle en garde contre "la maladie de l'�valuation", soulignant qu'"on n'�value pas le bonheur".
5215
La musique, atout ma�tre face au  vieillissement
R�agissez �cet article
