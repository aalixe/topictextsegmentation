TITRE: Pas de libre circulation des Croates en Suisse - International - Actualit� - LeVif.be
DATE: 2014-02-16
URL: http://www.levif.be/info/actualite/international/pas-de-libre-circulation-des-croates-en-suisse/article-4000530224581.htm
PRINCIPAL: 178486
TEXT:
Le Vif � Actualit� � International � Pas de libre circulation des Croates en Suisse
Pas de libre circulation des Croates en Suisse
Source:�Belga
dimanche 16 f�vrier 2014 � 12h31
La Suisse suspend l'accord pour l'extension de la libre circulation des personnes � la Croatie. Elle ne peut pas le signer sous sa forme actuelle, a expliqu� la conseill�re f�d�rale Simonetta Sommaruga � la ministre croate des affaires �trang�res Vesna Pusic.
�  Reuters
Immigration : l�UE p�nalise la Suisse
La ministre de la justice a inform� samedi Mme Pusic lors d'une conversation t�l�phonique, a indiqu� un porte-parole du D�partement f�d�ral de justice et police (DFJP), Philippe Schwander. La discussion portait sur les cons�quences de l'acceptation dimanche pass� de l'initiative sur l'immigration massive ainsi que sur les prochains d�veloppements. Simonetta Sommaruga a notamment expliqu� que la nouvelle disposition constitutionnelle est directement applicable et qu'elle ne permet pas de conclure un accord qui s'y oppose. Comme le protocole avec la Croatie pr�voit une libert� de circulation compl�te apr�s 10 ans, il ne peut �tre sign� sous cette forme, a pr�cis� Philippe Schwander.
Le Conseil f�d�ral r�fl�chit � des solutions qui ne discrimineront pas ce petit pays d'Europe de l'Est. La ministre croate a pris acte de ces informations, a ajout� M. Schwander. La conseill�re f�d�rale socialiste a convenu de rester avec elle en contact de mani�re proche et directe. Pour l'ambassadeur de la Croatie � Berne, Aleksandar Heina, il est urgent de trouver un accord. "Pour la Croatie, il est inacceptable d'�tre consid�r� durant les trois prochaines ann�es comme un Etat tiers et pas comme un Etat de l'UE", a-t-il dit dans la "NZZ am Sonntag".
Malgr� l'acceptation de l'initiative, le peuple suisse tient aux accords bilat�raux avec l'UE. Selon un sondage publi� par le "SonntagsBlick", 74% des personnes interrog�es se prononcent contre une d�nonciation des bilat�rales par la Suisse, alors que 19% sont pour et 7% n'ont pas d'avis.
Les infos du Vif aussi via Facebook
�
� �
Vos commentaires sont les bienvenus ! Pour �viter les abus, nous vous demandons toutefois de poster votre message en utilisant votre v�ritable identit�. Afin de pouvoir traiter les commentaires inappropri�s dans les plus brefs d�lais, la r�daction se r�serve le droit de contr�ler les comptes de ses utilisateurs.
Cliquez ici pour vous identifier et activer la fonctionnalit� �commentaires�
R�agir
Attention: Vos commentaires sont les bienvenus! Nous vous demandons de r�agir sous votre v�ritable identit�. Vous trouverez ici les conditions � remplir pour publier des commentaires.
S'il vous pla�t entrer une  adresse  e mail valide.
roularta.view.parts.reactioninput.errorStoreMessage
