TITRE: Pacte de responsabilit� : le "Oui, mais" de Pierre Gattaz
DATE: 2014-02-16
URL: http://www.latribune.fr/actualites/economie/france/20140216trib000815595/pacte-de-responsabilite-le-oui-mais-de-pierre-gattaz.html
PRINCIPAL: 0
TEXT:
Pacte de responsabilit� : le "Oui, mais" de Pierre Gattaz
Philippe Mabille �|�
16/02/2014, 17:18
�-� 895 �mots
Sous la pression de l'Elys�e et de ses pairs, le pr�sident du Medef esquisse un virage sur l'aile � propos du pacte de responsabilit� et accepte de prendre des "engagements de moyens" mais pas de "r�sultats" pour des emplois en �change d'une baisse des charges des entreprises. Une rencontre avec les syndicats est pr�vue le 28 f�vrier.
De retour des Etats-Unis o� il a accompagn� la visite d'Etat de Fran�ois Hollande, � Washington puis � San Francisco, le pr�sident du Medef n'a pas tard� � d�couvrir les d�g�ts m�diatiques de son "couac" sur les "contreparties" exig�es par la gauche en �change des all�gements de charges patronales.
Apr�s avoir r�affirm� depuis les Etats-Unis qu'il n'y aurait pas "d'objectifs chiffr�s" sur l'emploi, Pierre Gattaz a fait mine de faire machine arri�re, suite � un s�rieux recadrage avec l'Elys�e qui lui a reproch� cette sortie m�diatique contradictoire avec l'objectif du voyage : montrer le visage d'une "�quipe de France de l'�conomie" unie et coh�rente. Le pr�sident du Medef, sous la double pression de la gauche qui a hauss� le ton contre le Medef, mais aussi celle de ses pairs patrons, notamment les responsables de l'Afep, qui poussent � un accord avec le gouvernement, a donc compl�tement chang� de discours... sur la forme. Il s'est ainsi fait applaudir � San Francisco devant la communaut� fran�aise � la demande de... Fran�ois Hollande, pour avoir indiqu� que le Medef soutenait sans ambigu�t� la d�marche du pacte de responsabilit� et sans reprendre le mot qui f�che de "contreparties", acceptait sa part en promettant des engagements "de mobilisation, d'objectifs et de moyens", pour cr�er des emplois. La nuance compte, mais le ton change.
De retour � Paris, Pierre Gattaz a enfonc� le clou en publiant sur le site du Medef, Medef TV, une vid�o empreinte de gravit� et de solennit� pour r�affirmer cet "engagement" et rappeler la position de son organisation. Fourbi de son d�sormais c�l�bre pin's "1 million d'emplois en cinq ans", le patron du Medef, dit d�couvrir "avec surprise les interpr�tations erron�es" autour de ses d�clarations et d�clare accepter de passer "des mots aux actes" avec des objectifs "ambitieux".
�
Source MEDEFtv
Le rendez-vous du 28 f�vrier
Des actes. Pierre Gattaz en propose un premier en invitant les partenaires sociaux � un premier �change le 28 f�vrier. Et un deuxi�me en promettant pour le courant mars "de documenter ces objectifs avec des chiffres � partager" qui seront autant "d'estimations" de ce qu'il est possible de faire, dans les diff�rentes branches professionnelles. Mais, attention, pr�vient le patron des patrons : "recr�er de l'emploi dans notre pays prendra des ann�es, d�pendra de la coh�rence des mesures prises et demandera de la constance dans l'effort"...
Le gouvernement est pr�venu. Les objectifs ne seront tenables que si tout est fait pour r�tablir la comp�titivit� des entreprises par une baisse des charges sociales et de la fiscalit� financ�e par une baisse de la d�pense publique. "Il faut combler l'�cart de 116 milliards d'euros qui nous s�pare" de l'Allemagne et baisser "de 60 milliards d'euros les imp�ts et les charges des entreprises d'ici 2017", r�clame le pr�sident du Medef.
Pas d'engagement juridique
Finalement, Pierre Gattaz a donc chang� de discours, mais pas le fond de sa pens�e. "Les soi-disantes contreparties" seront un "engagement de moyens" dans la "confiance", mais pas "des engagements de r�sultats dans un mode de d�fiance". Voil� Fran�ois Hollande et la majorit� pr�venus : "les entreprises ne pourront jamais prendre d'engagement juridique de r�sultats dans un environnement instable et mondialis�". Pour Pierre Gattaz, "il faut cesser la d�marche de contrainte-contr�le-sanction" et passer � celle combinant "incitation-simplification-confiance" : car "si le pacte de responsabilit� se traduit par 50 pages de plus au code du travail et 150 de plus au code des imp�ts, nous aurons manqu� totalement l'objectif et notre pays ne se rel�vera pas".
Cette intervention du pr�sident du Medef, qu'il adresse aux Fran�ais � qui il veut tenir un "discours de v�rit�", autant qu'aux pr�sidents des f�d�rations patronales et aux syndicats montre que le pacte de stabilit� progresse dans les esprits. Si elle divise la gauche et perturbe la droite, elle est d'ailleurs largement soutenue par l'opinion.
Le patronat aussi est divis� sur les attraits du pacte
Mais comme le diable, la r�ussite du pacte se cache dans les d�tails. Car pour l'heure, on ne sait rien de la forme que prendront les futurs all�gements de charges. Le Cr�dit d'imp�t comp�titivit� emploi sera-t-il maintenu voire amplifi� ? Ou remplac� par une suppression des cotisations patronales � la branche famille ? Comment se conjuguera-t-il par ailleurs avec les autres all�gements de charges Aubry-Fillon pour les salaires allant de 1 � 1,6 smic ? Tous ces points restent dans l'ombre et complique s�rieusement l'accouchement du pacte. Ces inconnues divisent aussi fortement le Medef entre les branches industrielles qui r�clament des all�gements cibl�s allant jusqu'� 3,5 smic, et celles des services, comme la distribution, qui craignent d'�tre perdantes � ce jeu de bonneteau social. C'est sans doute dans ces tensions internes au patronat que se trouve l'explication de l'intervention de Pierre Gattaz, contraint � faire la couture entre un patronat pluriel, des syndicats qui h�sitent � part la CFDT � conclure un accord, une gauche de la gauche qui conteste la logique m�me de ce qu'elle appelle "un cadeau" au patronat et un pr�sident de la R�publique qui joue le sort de son quinquennat sur ce pari audacieux.
�
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Commentaires
Cafeine �a �crit le 18/02/2014                                     � 2:11 :
Plus je le vois et lis ses saillies, plus je regrette que Laurence Parisot n'ai pu r�ussir son putsch ! M. Gattaz ne rassemble pas, il divise, sa communication est brouillonne et il est p�tri de ces ann�es de combats patronats contre syndicats d'un autre temps. Bref il est aussi pass�iste que nos trotskistes syndicalistes. Dommage car �a n'est pas avec ces vielles casseroles que l'on rendra la France moderne attractive et lib�rale.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
simplification �a �crit le 17/02/2014                                     � 18:58 :
un seul r�gime de travail, de cotisation, de retraites pour tous et de suite... la vraie justice...et un code du travail de 54 acrticles comme en suisse et non nos 11000 articles.... la honte � l'�chelle de la plan�te, l'inefficacit� totale ...l'administration et les politiques sont encore dans la Russie d'avant Gorbatchev !!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
d�lire socialo �a �crit le 17/02/2014                                     � 18:55 :
tout d'abord les socialos tuent l'emploi par le simp�ts et taxes (baisse du pouvoir d'achat et augmentation des charges, record d'europe) et attendant une baisse du ch�mage avec cette asphyxie de l'�conomie....il faudrait peut �tre diminuer les imp�ts et charges..
total d�lire de demander des r�sultats imm�diats, comme si on disait � un sportif qui a perdu parceqeu'on lui a mis un sac � dos rempli de cailloux, on t'enl�ve un kilo mais tu signes que tu gagnes !!! il faut cr�er les conditions de r�ussite et les r�sultats suivront en fonction des entreprises...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
PIC SOUS �a �crit le 17/02/2014                                     � 16:11 :
EGALITE ET FRATERNITE : m�me r�gime pour tous, l'administration fran�aise n'a eu de cesse de s'octroyer r�guli�rement des petits avantages, qui cumul�s, sont ruineux, sans parler de l'incomp�tence des �lus locaux dans les domaines de la gestion. c'est simplement honteux.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Gattaz a raison �a �crit le 17/02/2014                                     � 14:46 :
Ce n'est pas lintervensionnisme de l'�tat qui peut changer l'�conomie, mais l'�tat dans lequel les entreprises fran�aises se trouvent. Aujourd'hui elles sont 'exsangue' et les in�galit�s fran�aises font r�agir "tout le monde" sur la base des petits avantages de chacun (intermittents, syndicalistes, dockers, fonctionnaires, �ducation nationale, SNCF, EDF,...). Il est normal que chacun r�agisse et que les fran�ais comprennent que l'Etat doit avoir un cours priv� sur "Comment g�rer un budget" pour enfin pouvoir parler avec nos entreprises.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
lyon69 �a �crit le 17/02/2014                                     � 13:39 :
Le code du travail et les conventions collectives sont devenus totalement illisibles : il faut simplifier tout �a, avec l'adoption de la r�gle : "une convention collective se substitue au code du travail, charge aux n�gociateurs se conforment au code du travail lors de la r�daction des conventions". Le code du travail comme les conventions collectives ne doivent plus exc�der 100 pages chacun.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
lyon69 �a �crit le 17/02/2014                                     � 13:36 :
Comparez le nombre d'entreprises de 9 salari�s avec le nombre d'entreprises de 10 salari�s : chacun comprendra que les seuils sociaux emp�che la cr�ation d'emplois ! Relevons ces seuils de 10 � 25 ou 30, de 100 � 250 ou 300 salari�s, et vous verrez les embauches reprendre...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
lyon69 �a �crit le 17/02/2014                                     � 13:33 :
Il faut imp�rativement simplifier le recrutement de la mani�re suivante, par exemple : un nouveau contrat de travail, pour lequel l'employeur cotiserait � un fond � hauteur de 15% du salaire net. En contrepartie, l'employeur comme le salari� pourrait mettre fin au contrat sans avoir � motiver le d�part. Le salari� partira avec le pactole. C'est le syst�me Autrichien (Autriche : meilleur �l�ve en terme de chomage en europe!)
Realityshow �a r�pondu le 17/02/2014                                                 � 13:41:
L'�tat ne veut que se remplir les poches gr�ce aux cotisations sociales. Que le salari� gagne plus n'est pas sa pr�occupation, au contraire. Il serait par exemple logique de r�duire les cotisations sociales quand l'activit� �conomique marche bien, hors c'est tout le contraire qui se produit. Quand on a compris le fait que seul l'�tat doit �tre gagnant, on a tout compris sur le mod�le �conomique Fran�ais : Pile je gagne, Face tu perds :)
gc �a r�pondu le 17/02/2014                                                 � 16:52:
simplifier? mais vous allez mettre au ch�mage les comptables, conseillers fiscaux, juristes, contr�leurs des imp�ts,.....
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Popoff �a �crit le 17/02/2014                                     � 13:01 :
Alors monsieur le pr�sident n en est ou avec ce pacte de responsabilit� , c est vrai il vaut mieux attendre le choc des �lections � venir
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de renseigner votre adresse email ci-dessous :
Realiste �a �crit le 17/02/2014                                     � 11:47 :
Aucun pays n'a tout ce baratinage. on occupe la galerie.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
lili �a �crit le 17/02/2014                                     � 11:18 :
la ligne lanc�e � volo est tendue ! une prise a mordu quel est donc ce beau poisson !
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
yokikon �a �crit le 17/02/2014                                     � 10:50 :
Il faudrait tout de m�me parvenir � s'entendre sur ce qu'est une entreprise ; un regroupement de moyens avec ceux en ressources humaines - le mot personnel banni depuis longtemps- qui sont la principale variable d'ajustement des comptes d'exploitation? Et puis, s'agissant non d'engagements sur des r�sultats mais sur des moyens, que craindre, tant c'est la cas des m�tiers de services tels ceux de banque ou assurance entre autres.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Merci �a �crit le 17/02/2014                                     � 9:22 :
Avec ce qu'ils m'on pris sur les dividendes ce petit cadeau ira direct on my pocket merci Hollande
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
gc �a �crit le 17/02/2014                                     � 9:18 :
gattaz n'a rien � promettre . De toute fa�on fran�ois h c'est d�j� engag� � supprimer les cotisations familles et baisser les imp�ts sur les soci�t�s alors tout est dit.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
JB38 �a �crit le 17/02/2014                                     � 9:10 :
Du CNPF au MEDEF, on ne peut pas dire qu'� la t�te de ces organisations il y ait eu des lumi�res, et encore moins des visionnaires. Des harpagon, des grippe-sous, de petits comptables, des valets au service de leurs pairs, beaucoup plus malins, qui leur font faire le "sale boulot".Pat�thique.
cc25 �a r�pondu le 17/02/2014                                                 � 11:38:
En total accord avec vos propos... vous avez oubli� "les fils � Papa"...!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
canari �a �crit le 17/02/2014                                     � 7:18 :
le consanguin du patronnat!!
Huhu �a r�pondu le 17/02/2014                                                 � 17:40:
>canari
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Ca suffit... �a �crit le 17/02/2014                                     � 6:59 :
Mr Gattaz...!
Il faut vite d�manteler cette organisation MEDEF...
... Ce Mouvement pour l'Esclavage Des Employ�s Fran�ais...!    ;o)
Et cette semaine on va donner Peugeot pour une bouch�e de pain au chinois et l'on entend peu de protestations...!!!!!!!!!!!!!!!!!!!!!!!!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Realityshow �a �crit le 17/02/2014                                     � 0:22 :
Les patrons se plaignent de payer 30 milliards de charge en trop, mais ne disent rien sur les 65 milliards qu'ils re�oivent tous les ans... Ils d�noncent aussi un code du travail compliqu�, alors que ce sont les conventions collectives qui sont incompr�hensibles, que les entreprises d�fendent pourtant �prement. L'�tat veut encore faire un ch�que en blanc aux entreprises : nous ne seront alors plus la 5eme puissance �conomique mondiale.
Kermao1800 �a r�pondu le 17/02/2014                                                 � 17:01:
Allez un jour regarder un compte d'exploitation et amusez-vous � faire des simulations plutot que de rester dans de vagues id�es et vous comprendrez...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
labradorinfo �a �crit le 17/02/2014                                     � 0:20 :
Je pense que la plaisanterie a assez dur� au sujet du pacte de responsabilit� et des contreparties
Le MEDEF  ne veut pas de contreparties � la r�duction des cotisations familiales , Fran�ois Hollande devrait en prendre bonne note et dire pas de contreparties pas de r�duction de charges aux entreprises et utilisait les 30 milliards d'euros pour r�duire le d�ficit.
Lucide �a r�pondu le 17/02/2014                                                 � 7:57:
Les 30 milliards ne sont pas dans un coffre dans l'attente d'une affectation quelconque. Ce sont des recettes de charges que l'�tat abandonne. S'il ne le fait pas les entreprises mourront pour beaucoup ou r�duiront la voilure pour beaucoup d'autres. Certaines se d�localiseront et de toutes les mani�res les recettes diminueront dans des proportions plus importantes que les 30 milliards avec en plus un chomage qui ne fera que grimper.
Il faut maintenant choisir. Ou nous faisons confiance aux entreprises et on leur donne les moyens d'am�liorer leurs �conomies ou nous rentrons en v�ritable r�cession.
Kermao1800 �a r�pondu le 17/02/2014                                                 � 17:06:
La base serait de les affecter � qui de droit surtout (PME -20 salari�s) et de ne pas n�gocier avec le Medef qui effectivement repr�sente majoritairement des soci�t�s cot�es dont le but est le pur profit.
Quant � Labradorinfo... que dire !?... allez sur un site et simulez un compte d'exploitation d'une PME voulant par exemple vendre des boulons et embaucher 10 salari�s au SMIC... rien que �a... vous allez rire ,-)
Huhu �a r�pondu le 17/02/2014                                                 � 17:45:
>Lucide
"S'il ne le fait pas les entreprises mourront pour beaucoup ou r�duiront la voilure pour beaucoup d'autres."
Si elles meurent, c'est qu'elles ne sont pas rentables et/ou ne correspondent pas � des services utiles � la soci�t�.
Si elles en sont l�, c'est qu'elles sont dirig�s par des gens incomp�tents, incapables de comprendre les dynamiques des march�s o� elles travaillent.
Il faut arr�ter: on ne peut pas hurler contre l'incomp�tence de l'Etat et vouloir par cons�quent r�duire son champs d'action d'un c�t�, et de l'autre faire du chantage au ch�mage pour prot�ger des bras cass�s.
Lucide �a r�pondu le 17/02/2014                                                 � 20:58:
> huhu
�tes vous Robert Hue ? A vous lire on pourrai le croire !
Huhu �a r�pondu le 18/02/2014                                                 � 18:35:
>Lucide
Visiblement, le lib�ralisme et vous, �a fait deux.
D'un autre c�t�, il y a maintenant un test irr�sistible pour savoir si l'on a un entrepreneur ou un clown:
un entrepreneur serre les dents et apprends de ses �checs, un clown vient chouiner sur latribune.fr contre les fonctionnaire
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
JSCH �a �crit le 16/02/2014                                     � 23:17 :
Cette histoire de pacte n'a aucun sens. L'Etat d�pense trop : il doit r�duire ses d�penses et les charges excessives qui en d�coulent. L'Etat n'a besoin d'autorisation de personne. En r�alit�, Hollande sait qu'il �chouera et pr�pare simplement la comm. pour expliquer que c'est la "faute aux patrons".
En gros, je suis trop gros, je fais un pacte avec ma balance : elle indique un poids en baisse, et je ferais alors un r�gime. Et sinon, ce sera la faute de la balance.
aie aie �a r�pondu le 17/02/2014                                                 � 11:05:
un r�gime � la grec. les grecs ont sabr� deans les d�penses publiques et la dette a augment� ainsi que le chomage
Kermao �a r�pondu le 17/02/2014                                                 � 17:11:
@aie aie  Arr�tons avec �a, avant de se mettre au r�gime � la grecque nous avons BEAUCOUP de marges.
Un exemple ? La gestion des cong�s pay�s... quand un fonctionnaire a le droit de poser 10 jours/an de cong�s enfants malades... ce ne sont pas 10 jours que l'on prend syst�matiquement tous les ans... derri�re cela implique du travail non fait, des recrutement pour couvrir etc... un co�t.
Un autre ? Le cuimul des mandats ou alors l'accepter mais avec un plafond de r�mun�ration.
Un autre encore ? Le scandale des promotions � 7 mois de la retraite chez les fonctionnaires...
Oui � chaque fois ce n'est rien mais toutes ces petites choses nous feraient faire facilement 50 milliards d'economies sans toucher � votre bedaine �tatique...
L� on ne touche qu'aux poign�es d'amour : vous saurez vous en passer !?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
krok �a �crit le 16/02/2014                                     � 23:09 :
Les hommes/femmes politiques en France ne font quasiment que des b�tises. Tout simplement parce qu'lis r�fl�chissent pour leur int�r�t personnel et �ventuellement pour l'int�r�t de leur proches (famille, partie politique etc). En tout cas certainement pas pour l'int�r�t g�n�ral.
Eh oui, apr�s tout ils ne sont pas tap� toutes ces ann�es � avaler des couleuvres, � user leur jeans sur les bancs de l'ENA, de l'IEP et autres grands �tablissements pour servir le bien �tre des culs-terreux que nous sommes ! Ils pensent en terme d'�lection ou de r��lection.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Tibre �a �crit le 16/02/2014                                     � 23:01 :
Pour la question de l'emploi, on parle de 39h pay�es 35, c'est dans le pacte? En quoi la comp�titivit� est-elle n�cessaire?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Neuneu �a �crit le 16/02/2014 � 22:42 :
Le patronat Fran�ais est assez tordant.
Tordant car il n'a absolument aucune id�e des r�alit�s. Lu dans les commentaires des chefs d'entreprise qui souffrent, qui veulent vendre leur entreprise, qui se disent �cras�s par les salari�s, les charges, les contrats...
Je vous propose des entreprises conduites par des robots, plus d'humains, la totalit� des actifs fran�ais au ch�mage et je vous souhaite une belle vie d'entrepreneur bas�e sur la rapidit� du robot � s'auto-r�parer.
Dans la plupart des cas, les employeurs ne pensent qu'� une seule chose, engranger du cash et conserver leur jolie tr�sorerie. A ce titre Gattaz ne fait rien de mieux, il prend mais il ne donne rien. Pensez � vos salari�s, surtout les moins bien pay�s qui souffrent dans l'ombre de vos d�sirs de toujours gagner plus. Le syst�me capitaliste est une honte, c'est la destruction de l'humain, Marx l'a toujours dit et ce qu'il se passe aujourd'hui en est la preuve. Soyez responsables et agissez pour vos salari�s, ils agiront pour vos entreprises. Offrez de l'emploi et vous aurez la paix sociale, travaillez avec vos employ�s et vous serez consid�r�s comme des patrons humains. Faites tout le contraire et vous serez d�test�s...
Rv�_Blv �a r�pondu le 17/02/2014                                                 � 19:30:
Enti�rement d'accord avec vous, pour eux qui vivent dans le luxe pendant que leurs employ�s vivent dans la mis�re!!!!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Jipi61 �a �crit le 16/02/2014                                     � 22:35 :
Pour un PATRON cela n'est pas tr�s Intelligent, Faire marche arri�re toutes. Comment voulez vous que l'on soit cr�dible vis � vis des Grands Pays. Je ne sais s'il a d�j� travaill�, comme simple ouvrier ! Il � besoin de retourner � l'�cole, en commen�ant par la Maternelle.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Huhu �a �crit le 16/02/2014                                     � 22:00 :
Pierre Gattaz?! Mais qui peut prendre au s�rieux un h�ritier?
Et plus, qui peut prendre au s�rieux l'h�ritier d'Yvon Gattaz, un sacr� neuneu � la base...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Maxime Piolet �a �crit le 16/02/2014                                     � 21:26 :
La pr�somption d'innocence pr�valant encore dans notre pays, on ne dira pas que ce pacte est un march� de dupes en bande organis�e mais je n'en pense pas moins !
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Paul �a �crit le 16/02/2014                                     � 20:59 :
Le pacte anti-social et contre-performant ou bien de responsabilit�? C'est par rapport au ch�mage? Si on prend la pente des PO depuis 2010, on a une moyenne de +45 MILLIARDS par an... alors on est en bas, roue libre vers l'enfer sur une Europe terre de d�tresse! On parle de l'humanisme fran�ais, le meilleur du monde? C'est � voir, pourtant, si on fait dans les grands donneurs de le�ons colbertistes d'Etat dispendieux, on n'exporte pas. On a encore une guerre de retard, comme en 40? Nos d�bours dispendieux ne valorisent pas notre Etat tr�s mal class� en terme d'IDH. De grandes d�penses pour de petits r�sultats, nous sommes en petisme �conomique! Une petite croissance, ne cache-t-on pas un homonculus et une aiguille appel� France? Grands coups d'�p�es dans l'eau: le d�ficit relance la croissance... du ch�mage!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
pm �a �crit le 16/02/2014                                     � 20:45 :
ce  charlot ne sait +quoi raconter .l'intelligence ne transpire pas au medef ,quant � la politesse!!!! .� l'�poque du franc les patrons demandaient des d�valuations pour �tre comp�titifs ,aujourd'hui ils demandent des aides .
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Baboul55 �a �crit le 16/02/2014                                     � 20:43 :
Quand je pense que Mr Gattaz est all� faire "l'accompagnateur de luxe" de Fleur Pellerin au CES de Las Vegas, peut �tre qu'il croyait que le gouvernement voulait vraiment r�former : Quelle blague! Moi je suis petit patron depuis +de 15 ans et j'ai appris que la droite comme la gauche d�testent les entreprises (surtout les petits entrepreneurs, artisans, commer�ants..) le "dogme" est plus fort que la raison. Comment Mr Gattaz peut-il imaginer une seconde qu'un gouvernement qui d�fend le r�gime intermittents, qui annonce ne pas toucher au salaire des fonctionnaires et qui supprime la seule journ�e de carence de la fonction publique .. va faire des r�formes justes?? Il faut �tre na�f pour croire cel�. Il serait plus simple de retirer le mot "EGALITE" de notre devise de la R�publique Francaise, au moins les choses seraient claires!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Rv�_Blv �a �crit le 16/02/2014                                     � 20:13 :
Les patrons fran�ais ne savent faire qu'une chose pleurer des aides, des subventions, c'est tout ce qu'ils sont capables de r�aliser, toujours � pleurer qu'ils payent de trop de ceci, trop de cela, mais pour toutes les aides qu'ils touchent l�, on ne les entend pas. Ils demandent � leurs ouvrier(e)s, employ(e)�s, toujours plus de rendement, de production sans aucune r�compense de leurs bons et loyaux services, � sa geindre ils savent faire!!!!
lolo �a r�pondu le 16/02/2014                                                 � 20:40:
Vous confondez les grands groupes internationaux fran�ais ou �tranger pour les aides �tatiques mais cela ne concerne en rien les 90 % d'entrepreneurs en France. Les commer�ants, artisans, PME et PMI elles ne re�oivent rien du tout et vous devriez les remercier de vous offrir un emploi ou un emploi de fonctionnaire, enti�rement pay� par les contribuables.Montez votre boite et revenez nous dire apr�s si vous appr�cier l'ultra fiscalisme et les lourdeurs administratives fran�aises.
baboul55 �a r�pondu le 16/02/2014                                                 � 20:46:
lolo � raison, ce sont les grosses entreprises qui siphonent toutes les aides d'�tat et pendant ce temps-l� les petits tombent comme des mouches. 700 faillites par semaine en ce moment. La droite comme la gauche est responsable de ce massacre.
Huhu �a r�pondu le 16/02/2014                                                 � 21:50:
Il faudrait peut �tre aussi que certains petits patrons se remettent en question: quand tout le monde se met � ouvrir en m�me temps des magasins de cigarettes �lectroniques sans se poser la question de la saturation du march� par exemple, c'est qu'il y a au minimum un petit probl�me de bon sens chez certains "entrepreneurs"...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Pat34 �a �crit le 16/02/2014                                     � 19:48 :
Le jeu n'en vaut plus la chandelle. Diriger une entreprise en France consomme trop de sant�. Le gain n'est plus � la hauteur. La gestion du personnel c'est l'enfer, le march� fran�ais d�gringole depuis que Hollande est l�, et comme il faut mettre de l'argent de c�t� pour sa retraite, l'ISF prend tout le rendement, donc on y perd. Le jeu n'en vaut plus la chandelle... Pour les entrepreneurs qui le peuvent il ne reste qu'� quitter le pays
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
lecitoyen �a �crit le 16/02/2014                                     � 19:16 :
Pauvre riche !
Gorillaz �a r�pondu le 16/02/2014                                                 � 19:20:
...  Pauvre  d'esprit !!!!!!!!!!!!!!!!!!
Gorillaz �a r�pondu le 16/02/2014                                                 � 19:28:
J'avais  300 salari�s  ...  j'ai  fait  3 AVC  ...  la  moralit�  est  de  quel  cot� ???
damned �a r�pondu le 17/02/2014                                                 � 4:23:
et dans vos salaries combien d'AVC ? D'infarctus...et que vient faire la moralit� dans tout �a ?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Gorillaz �a �crit le 16/02/2014                                     � 19:09 :
Exemple  typique ma   secr�taire   attrape  un  cancer  une  maladie...  bref  compte  tenu  de  sa  sant�   je  lui  am�nage  un  poste  �  domicile  ...  liaison  par  internet ...  etc  ...  je  ne  vais  rentrer  dans  les  d�tails ....  l'inspection   du  travail   d�barque  au  bureau ...  il  me  fallait  un  contrat   sp�cifique  et  patati  et  patata .......  il  faut  arr�ter  les  conneries   deux  minutes ......  elle   est  gu�rit   maintenant  et peut  toujours  travailler  depuis  chez  elles !    (je  connais  le  souci  mon fr�re  est  mort  del�  m�me  maladie !)
Gorillaz �a r�pondu le 16/02/2014                                                 � 19:19:
...  et  je  suis  malade  aussi !
Maxime Piolet �a r�pondu le 16/02/2014                                                 � 20:36:
@Gorillaz : on ne parle jamais assez de la tension exerc�e sur les dirigeants d'entreprises ! Acceptez, s'il vous plait, toute ma sympathie et conservez vos passions !
Red! �a r�pondu le 16/02/2014                                                 � 23:42:
"Gorillaz  a r�pondu le 16/02/2014 � 19:19:
... et je suis malade aussi !"
Champagne! Bient�t un patron de moins!
Maxime Piolet �a r�pondu le 16/02/2014                                                 � 23:56:
@Red! : quelque chose ne va pas chez vous, mon ami !?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
lyon69 �a �crit le 16/02/2014                                     � 19:02 :
Rappel indispensable: en 2012-2013, les hausses de charges sur les entreprises fran�aises se sont �lev�es � 30 milliards. Pour faire passer la pilule, Hollande a promis un CICE de 20 milliards � partir de 2014-2015 ! Maintenant, il se ram�ne avec un pacte qui ne ferait que ramener le niveau des charges en 2015 au niveau de  son arriv�e au pouvoir...et il cro�t pouvoir exiger des centaines de milliers d'emplois en �change : il reve ou quoi ??!! De toutes fa�ons, les entreprises fran�aises n'en ont plus les moyens !!!
lyon69 �a r�pondu le 16/02/2014                                                 � 20:17:
Pr�cision: le "pacte" de Hollande de 2014 INCLUS le CICE. !!!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
lyon69 �a �crit le 16/02/2014                                     � 18:54 :
Commen�ons par remonter les seuils sociaux: de 10 � 25 salari�s, et de 50 � 150 salari�s ! Ca ne coute RIEN � l'�tat, et ca enl�ve un poids aux PME !!
@lyon69 �a r�pondu le 16/02/2014                                                 � 20:37:
rigolo
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Gorillaz �a �crit le 16/02/2014                                     � 18:26 :
Cot�  fonctionnaire   y' a  du  boulot  �  faire  ...  j'ai  vendu mon entreprise   en  d�cembre ...   la  demande  d�autorisations de transport    des  repreneurs  �  �t�  d�pos�e  en  novembre  2013  ....   en  principe    elles   seront  effectives   semaine  prochaine ...  soit  f�vrier 2014 !   vive  les  socialos   et  fonctionnaires !  c'est  une  honte !!!!!!!!!!!!
logique1870 �a r�pondu le 16/02/2014                                                 � 19:01:
parceque ce n �tait pas pareil du Temps de Sarko!!!,,sont marant tous ces gus qui pensent que les gens sont a leur disposition,,il y a une proc�dure et elle est pareil pour tout le monde,,tous ces gugus qui crachent a longueur de temps sur les services publics,,et qui sont les premiers a pleurer pour des aides quand �a va mal pour leur boite ,,et �a m emmerde que mes impots servent des gugus de ce genre,,
caogan �a r�pondu le 16/02/2014                                                 � 19:40:
C'est bien tout le probl�me des fonctionnaires, on les paye mais ils ne sont pas � notre disposition ... au moins dans le priv� si quelqu'un ne fait pas l'affaire, on va ailleurs ! mais la fonction publique il faut se la taper de force et � un coup d�lirant, vous comprendrez peut �tre qu'on ait parfois envie de cracher dessus.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Vive la R�publique �a �crit le 16/02/2014                                     � 18:17 :
Et la d�mocratie et la Libre Entreprise. Mais surtout pas la politique interventionniste de gouvernants ne sachant pas g�re un budget qu'ils soient de gauche ou de droite. C'est l'entreprise qui fait l'�conomie d'un pays pas les petits arrangements entre syndicats et politiciens de tout bord.
logique1870 �a r�pondu le 16/02/2014                                                 � 19:07:
on va encore filer des tunes a des parasites qui vont s empresser d augmenter leur b�n�fices,ou leur salaires,,c est bon on a asser donner,,,les tunes devraient etre distribu�es apres v�rification par un inspecteur du travaille,et fiscale,,,on a vu avec les restaurateurs,,et les entrepreneurs sans salariers,,pas d entreprises,,,ou alors chacun bosse pour son compte et assumes,,et fait sa vie,,
caogan �a r�pondu le 16/02/2014                                                 � 19:43:
Camarade on ne donne pas des tunes comme vous dites, on �vite d'en piquer pour leur laisser une chance de survivre face � la concurrence. Mais la concurrence vous ne connaissez pas d'apr�s votre poste pr�c�dent...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
