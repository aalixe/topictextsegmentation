TITRE: Afrique du Sud: plusieurs dizaines de mineurs pris au pi�ge dans une mine d'or - L'Express
DATE: 2014-02-16
URL: http://www.lexpress.fr/actualite/monde/afrique/afrique-du-sud-plusieurs-dizaines-de-mineurs-pris-au-piege-dans-une-mine-d-or_1324674.html
PRINCIPAL: 0
TEXT:
Voter (1)
� � � �
Plus de 200 mineurs seraient pris au pi�ge dimanche dans une mine d'or ill�gale � Benoni, l'est de Johannesburg.
afp.com/Stephane de Sakutin
Plus de 200 mineurs seraient pris au pi�ge ce dimanche dans une mine d'or ill�gale � l'est de Johannesburg, ont d�clar� des services de secours. Onze d'entre eux auraient �t� �vacu�s par les secours qui s'affairent sur les lieux.�
"Nous sommes entr�s en communication avec une trentaine de mineurs coinc�s. Ils nous ont dit qu'il y en avait 200 autres en dessous d'eux", a d�clar� � l'AFP Werner Vermaak , porte-parole de l'organisation priv�e de secours d'urgence ER24.�
De l'eau et de la nourriture ont pu �tre  descendues pour les mineurs � l'aide d'une corde.�
Illegal miners trapped in South Africa, 11 rescued http://t.co/n4FJKKUocE
-- Berkley Bear (@BerkleyBearNews) February 16, 2014
�
Le porte-parole n'a pas �t� en mesure de confirmer par lui-m�me la pr�sence des 200 autres mineurs et des responsables municipaux n'ont de leur c�t� confirm� que le chiffre d'une trentaine de personnes prises au pi�ge. �
La chute d'un bloc de rochers a obstru� l'issue
Les mineurs �taient descendus samedi dans la mine exploit�e ill�galement, creus�e derri�re un stade de cricket, dans le quartier de Benoni . Ils sont rest�s bloqu�s en raison de la chute d'un bloc de rocher qui a obstru� l'issue du puits.�
Les accidents de mines sont relativement fr�quents en Afrique du Sud, un pays riche en minerais. Huit mineurs au moins ont �t� retrouv�s morts il y a deux semaines apr�s un incendie provoqu� par une secousse tellurique dans une mine d'or � l'ouest de Johannesburg. En juillet 2009, neuf travailleurs avaient �t� tu�s par une chute de pierres dans une mine de platine. La m�me ann�e, 82 personnes qui cherchaient de l'or dans un puits d�saffect� avaient �t� tu�es par un incendie.�
Le secteur minier sud-africain est agit� depuis le 23 janvier par une gr�ve pour augmentation de salaire qui mobilise quelque 80.000 mineurs employ�s par les trois premiers producteurs mondiaux de platine. �
Avec
