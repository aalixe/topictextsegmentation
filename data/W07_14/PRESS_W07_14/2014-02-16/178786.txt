TITRE: OL : les + et les - de la victoire contre Ajaccio (3-1)
DATE: 2014-02-16
URL: http://www.butfootballclub.fr/1209980-ol-les-et-les-de-la-victoire-contre-ajaccio-3-1/
PRINCIPAL: 178783
TEXT:
OL : les + et les � de la victoire contre Ajaccio (3-1)
Post� le 16 f�vrier 2014
Aucun commentaire
��
Tranquillement mais en se faisant quand m�me quelques frayeurs sur la fin, l�Olympique Lyonnais a domin� l�AC Ajaccio (3-1) ce dimanche apr�s-midi � Gerland. Les buteurs rhodaniens se nomment Fofana (43e), Briand (69e) et Gomis (90e).
Les ��plus���:
-��������� Avec 15 points pris sur les six premiers matches de la phase retour, l�Olympique Lyonnais est tout simplement la meilleure �quipe de L1 depuis janvier 2014. Devant le PSG, l�AS Monaco et Montpellier.
-��������� Dans la course � la 3e place et dans l�optique de se relancer apr�s l��limination face au RC Les, les Gones se devaient de gagner � la maison face � un adversaire ��facile��. Un r�sultat qui permet aux Lyonnais de mettre la pression sur Lille (qui jouent � 17 heures) mais aussi sur Marseille et Saint-Etienne qui s�affrontent ce soir.
-��������� L�OL a des ailes en forme. A gauche, Henri Bedimo a encore �t� monstrueux d�fensivement et offensivement, d�livrant un extraordinaire caviar pour Briand et r�alisant plusieurs rushs de haute voltige. Sixi�me passe d�cisive pour lui. A droite, �Miguel Lopes a lui aussi fait le job avec s�rieux avant de sortir en grima�ant apr�s un coup sur la cuisse.
-��������� Deuxi�me but en deux matches pour Jimmy Briand. Entr� en jeu, l�ancien Rennais a n�anmoins manqu� une occasion incroyable � un m�tre du but � la 59e minute avant de scorer de la t�te (69e). A d�faut d��tre un tueur, il ne renonce jamais�
-��������� Tr�s bon dans l�entrejeu et syst�matiquement port� vers l�avant, Gueida Fofana a d�bloqu� la situation d�une frappe lob�e (43e). Son retour a fait un bien fou au milieu.
-��������� Comme face � Lens, Baf�timbi Gomis a dilapid� un nombre important de situation. Mais il n�a rien l�ch� jusqu�� marquer le but du 3-1 � la 90e minute. Son 17e cette saison toutes comp�titions confondues.
-��������� S�il n�a pas �t� d�cisif, Cl�ment Grenier nous a cr�dit� de quelques jolies friandises. En 2014, le milieu rhodanien est vraiment mieux.
-��������� 35103 spectateurs, un dimanche � 14 heures face � la lanterne rouge de Ligue 1. Encore une belle affluence pour l�OL.
Les ��moins���:
-��������� La sortie d�Alexandre Lacazette, boitant pas apr�s une faute corse � la 53e minute. Probablement une b�quille sur le genou.
-��������� Au-del� de la victoire, l�OL a quand m�me re�u beaucoup de coups et risque d�avoir quelques plaies � panser avant un long d�placement � Odessa. Emb�tant.
-��������� L�apathie de la d�fense lyonnaise, absolument pas agressive sur le but d�Oliech. L�ancien Auxerrois a largement le temps de se retourner avant de tromper Anthony Lopes.
Conclusion�: Bien s�r, tout n�a pas �t� parfait et l�OL a pris beaucoup de coups face � une rugueuse �quipe corse mais les Gones reprennent leur marche en avant apr�s la d�faite contre Lens. L�essentiel �tait de gagner pour bien aborder le d�placement � Lille dans sept jours.
A.C, � Gerland.
