TITRE: Ukraine: Didier Burkhalter salue l'�vacuation de la mairie de Kiev -  News Monde: Europe - tdg.ch
DATE: 2014-02-16
URL: http://www.tdg.ch/monde/europe/didier-burkhalter-salue-evacuation-mairie-kiev/story/31982245
PRINCIPAL: 178480
TEXT:
Didier Burkhalter salue l'�vacuation de la mairie de Kiev
Mis � jour le 16.02.2014 1 Commentaire
� Imprimer
Le pr�sident en exercice de l'Organisation pour la s�curit� et la coop�ration en Europe (OSCE), Didier Burkhalter, a salu� dimanche l'�vacuation pacifique quelques heures plus t�t de la mairie de Kiev.
1/133 L'opposante Ioulia Timochenko a d�clar� dimanche qu'elle n'�tait pas int�ress�e par le poste de Premier ministre. (23 f�vrier 2014)
Image: Keystone
� �
Aide demand�e � Merkel
Les responsables de l'opposition ukrainienne vont demander lundi � l'Europe une aide financi�re en faveur de l'Ukraine, a d�clar� dimanche l'un d'eux. Une rencontre doit avoir lieu � Berlin avec la chanceli�re allemande Angela Merkel.
�Nous avons besoin d'aide. Nous n'avons pas besoin de paroles, nous avons besoin d'actes�, a d�clar� dimanche Arseni Iatseniouk devant des dizaines de milliers de manifestants rassembl�s sur le Ma�dan, la place de l'Ind�pendance, dans le centre de Kiev. �Nous voulons que l'Europe dise clairement quel paquet de mesures �conomiques elle peut nous proposer�, a-t-il poursuivi.
Articles en relation
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Les opposants ukrainiens menacent d�j� de reprendre le b�timent d'assaut si les autorit�s violent leurs engagements.
Dans un communiqu� de l'OSCE, Didier Burkhalter �salue l'�vacuation de la mairie de Kiev comme un d�veloppement positif de la d�sescalade des tensions en Ukraine�. Le pr�sident de la Conf�d�ration met notamment en exergue �le geste des opposants ukrainiens�, en esp�rant que celui-ci participera � une �r�solution prochaine� de la crise.
La Suisse, qui assure la pr�sidence tournante de l'OSCE, salue aussi la lib�ration de plusieurs opposants par le pouvoir en place. �J'appelle toutes les parties � s'engager enti�rement dans la recherche de compromis au moyen du dialogue politique�, a conclu chef de la diplomatie suisse dans le communiqu� de l'OSCE.
Nouvel avertissement
Les opposants, eux, ne d�sarment pas. Peu apr�s l'�vacuation de la mairie, l'ancien �commandant� des lieux, Rouslan Andri�ko, du parti nationaliste Svoboda, a lanc� une mise en garde: �Si les autorit�s violent leurs engagements, nous prendrons de nouveau la mairie d'assaut, d�finitivement�, a-t-il dit.
Le b�timent a �t� �vacu� en bon ordre en d�but de matin�e, les contestataires ne laissant comme traces de leur passage que les nombreuses affiches et autocollants dont ils avaient d�cor� les lieux. �Il n'y a pas eu de d�g�ts, nous esp�rons qu'il s'agit d'un premier pas vers une normalisation de la situation�, a comment� pour sa part Volodymyr Make�enko, chef du conseil municipal de Kiev. (ats/Newsnet)
Cr��: 16.02.2014, 13h49
