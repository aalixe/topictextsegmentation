TITRE: Retour vers le futur : les baskets � la�age automatique en vente en 2015
DATE: 2014-02-16
URL: http://www.konbini.com/fr/entertainment-2/retour-vers-le-futur-baskets-lacage-automatique-2015/
PRINCIPAL: 179337
TEXT:
Retour vers le futur : les baskets � la�age automatique en vente en 2015
Shares
Le mercredi 21 octobre 2015 � 16h29, c�est la date pr�cise � laquelle Marty McFly d�barque dans le futur. Alors qu�il vient donner un coup de main � sa future prog�niture, �Marty McFly Junior, il d�couvre une flop�e d�inventions qui n�existent pas encore en 1985, son �poque. Parmi elle, des Nike Air qui poss�dent la particularit� d�avoir un la�age automatique.
D�apr�s le site Sole Collector , ces chaussures vont devenir r�alit� en 2015. En 2011 d�j�, les Nike Air Mag de Retour vers le futur 2 avaient �t� mises en vente mais il y avait deux probl�mes : d�une, pas de �Power Laces� (de la�age automatique) au programme, de deux, seulement 1500 exemplaires avaient �t� rendus disponible via des ench�res sur le site nikemag.eBay.com. L�argent avait �t� revers� � la fondation de Michael J. Fox qui lutte contre la maladie de Parkinson.
Le retour des Nike Air Mag
Pas plus tard que cette semaine, Tinker Hatfield, designer de Nike et cr�ateur pour le cin�ma de la c�l�bre chaussure, a annonc� que 2015 serait l�ann�e de sa concr�tisation. Lors de sa venue au Flight Lab Space � la Nouvelle-Orl�ans, Tinker Hatfield aurait affirm� que les Powers Laces deviendraient r�alit� en 2015 sans pr�ciser si le la�age automatique serait pr�sent. Les Nike Air Mag de 2011 pourraient donc rencontrer en 2015 un public plus large pour un prix qu�on esp�re plus abordable � 5000 euros, prix de base lors des ench�res.
Apr�s Retour vers le futur II, les Nike Air Mag prennent vie
Cette information vient confirmer les dires de Frank Marshal. Le producteur de la trilogie Retour vers le futur avait confi� en ao�t 2012 au site am�ricain Collider que Nike travaillait sur plusieurs mod�les qui seraient disponibles � la vente en 2015. Histoire d��tre en phase avec les tribulations spatio-temporelles de Marty McFly. Le futur, c�est pour bient�t.
Shares
