TITRE: La popularit� de Hollande encore en baisse en f�vrier - L'Express
DATE: 2014-02-16
URL: http://www.lexpress.fr/actualite/politique/la-popularite-de-hollande-encore-en-baisse-en-fevrier_1324612.html
PRINCIPAL: 178125
TEXT:
La popularit� de Hollande encore en baisse en f�vrier
Par LEXPRESS.fr, publi� le
16/02/2014 �  09:51
79% des Fran�ais se d�clarent m�contents du pr�sident de la R�publique en f�vrier, selon le barom�tre mensuel Ifop pour Le Journal du Dimanche. Son plus bas score de popularit� d�j� obtenu en novembre dernier.�
Voter (28)
� � � �
Fran�ois Hollande a salu� "la confiance retrouv�e par les acteurs �conomiques" en France, apr�s les chiffres de la croissance publi�s vendredi, a d�clar� la porte-parole du gouvernement Najat Vallaud-Belkacem.
afp.com/Eric Feferberg
La cote de popularit� de Fran�ois Hollande a encore perdu deux points � 20% alors que celle de Jean-Marc Ayrault reste stable � 26%, selon le barom�tre mensuel Ifop pour le Journal du Dimanche .�
20% des Fran�ais se d�clarent satisfaits du pr�sident de la R�publique en f�vrier alors que 79% (+2) sont m�contents. 1% ne se prononcent pas. �
Fran�ois Hollande retrouve ainsi son plus bas score de popularit� d�j� obtenu en novembre dernier.�
Le Premier ministre s'en sort un tout petit mieux avec 26% de satisfaits en f�vrier comme en janvier. 71% des sond�s sont m�contents comme le mois dernier. 3% ne se prononcent pas. �
Avec
Sondage r�alis� du 7 au 15 f�vrier par t�l�phone aupr�s d'un �chantillon de 1944 personnes, repr�sentatif de la population fran�aise �g�e de 18 ans et plus, selon la m�thode des quotas.�
�
Ulysses - 18/02/2014 10:29:12
@x2malta2 : Bonjour x2 , J'ai du mal avec les sondages, mais la cote de popularit� du pr�sident se situe dans les alentours de 20 % et je pense que si dette appr�ciassions se confirmait durablement, un remaniement minist�riel devrait �tre entrepris. Je pense que les ministres sont trop nombreux et les taches qui leur sont d�volus ne sont pas suffisamment pris en compte pour une gestion sans contestation de leurs mandats. Il faudrait un ministre responsable qui chapeauterait les attach�s de mission qui apporteraient les id�es avec les am�liorations et les cons�quences que cela pourrait produire. Ce ministre "chapeau" devrait faire une synth�se  pr�cise avec les autres ministres (maximum 10) et ensuite soumettre leurs conclusions au 1� ministre qui devrait en r�f�rer au pr�sident. apr�s les des assembl�es devrait examiner les projets et statuer sur la faisabilit� ou non et faire en sorte que cela soit appliqu� ou non dans les plus proches d�lais. Je fais un voeu pieux mais en sachant tr�s bien que cette forme de pens�e ne serait pas au gout des ministres mais en peut toujours r�ver. Bien � vous x2.
