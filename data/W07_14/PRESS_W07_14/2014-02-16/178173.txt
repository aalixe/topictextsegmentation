TITRE: Ellen Page : Kristen Bell, Ricky Martin... les stars r�agissent � son coming out
DATE: 2014-02-16
URL: http://www.purebreak.com/news/ellen-page-kristen-bell-ricky-martin-les-stars-reagissent-a-son-coming-out/71106
PRINCIPAL: 178170
TEXT:
Ellen Page : Kristen Bell, Ricky Martin... les stars r�agissent � son coming out
7
Ellen Page contente de se trouver�� la soir�e de lancement du jeu Beyond Two Souls le 2 octobre 2013
C'est lors de la Saint-Valentin que l'actrice Ellen Page a profit� d'une conf�rence organis�e par le mouvement gay et lesbien aux USA pour d�voiler son homosexualit�. Une d�cision forte et difficile pour la star du jeu Beyond Two Souls, qui a �mu tout le monde � commencer par Ricky Martin, Patrick Stewart ou encore Kristen Bell.
Aussi incroyable que cela puisse para�tre en 2014, l'homosexualit� a encore du mal � se faire accepter, obligeant souvent les personnes concern�es � se murer dans le silence et le secret. Et c'est notamment ce qu'a v�cu l'actrice Ellen Page durant de nombreuses ann�es, avant d'avouer �tre fatigu�e "de se cacher et mentir par omission".
Un coming-out �mouvant
Ainsi, c'est lors d'une conf�rence qu'elle a d�cid� d'imiter Jodie Foster et Wentworth Miller , d�voilant au monde entier qu'elle �tait lesbienne . Une d�cision motiv�e pour "faire changer les choses" et faire comprendre au reste du monde que les homosexuels m�ritent �galement "d'�tre aim�s int�gralement, �quitablement, sans honte et sans compromis."
Les stars r�agissent sur Twitter
Et forc�ment, un tel message ne pouvait pas passer inaper�u, celui-ci r�sonnant positivement dans le monde entier et le tout Hollywood. Que ce soit Ricky Martin , Patrick Stewart, Kristen Bell ou encore Anna Kendrick , tous en ont profit� pour faire passer son message sur le net, la f�licitant pour ce tr�s important coming-out.
Emile Hirsch : F�licitations � Ellen Page pour aider autant de gens � lutter contre leurs probl�mes quotidiens en devenant un exemple courageux et en se d�voilant le jour de la Saint-Valentin.
Jesse Tyler Furgson : J'aime j'aime j'aime Ellen Page et le message qu'elle a envoy� aujourd'hui. Quelle fa�on parfaite de terminer la Saint-Valentin.
Kate Mara : Hey Ellen Page, veux-tu �tre ma Valentine ? #respect
Ricky Martin : Absolument magnifique Ellen Page. Je suis vraiment heureux pour toi. Tu es libre.
Evan Rachel Wood : F�licitations mon amie ;)
Kristen Bell : Incroyable discours. Tellement dr�le et honn�te. Elle est rayonnante. Joyeuse Saint-Valentin pour tous.
Patrick Stewart : "Ellen Page : Merci HRC et tout le monde pour votre amour et votre soutien" Et je vais ajouter le mien.
Jason Collins : Je souhaite tout l'amour et le soutien possible � Ellen Page. Merci d'avoir partag� ton histoire avec nous.
Votre r�action
