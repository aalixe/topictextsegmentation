TITRE: "Les Vacances du Petit Nicolas" : premier teaser... tr�s estival - News films Vu sur le web - AlloCin�
DATE: 2014-02-16
URL: http://www.allocine.fr/article/fichearticle_gen_carticle%3D18630805.html
PRINCIPAL: 0
TEXT:
En savoir plus : Laurent Tirard , Les vacances du Petit Nicolas
A voir aussi
Encore plus de news Vu sur le web
The Amazing Spider-Man 2 : l'affiche qui rend hommage aux comics
mercredi 9 avril 2014 - News - Vu sur le web
Alors qu'une affiche IMAX tr�s graphique a �galement �t� r�v�l�e, "The Amazing Spider-Man 2" rend hommage � ses origines, � travers un poste...
"The Raid 2": incarnez le h�ros du film gr�ce au jeu en 8-bit
mercredi 9 avril 2014 - News - Vu sur le web
"The Raid 2" ne sort que le 23 juillet sur nos �crans. Que les fans impatients se rassurent, gr�ce au jeu en 8-bit, "The Raid 2 Arcade Editi...
Beno�t Brisefer : la bande-annonce de l'adaptation cin� !
mercredi 9 avril 2014 - News - Vu sur le web
Manuel Pradal adapte la c�l�bre bande dessin�e de Peyo, avec G�rard Jugnot et Jean Reno au casting. La sortie salles est pr�vue le 10 d�cemb...
Toutes les news Vu sur le web
Haut de page
Vous devez vous connecter pour �crire un commentaire
Willow-des-bois
Vivement!
Le 1er �tait excellent, cette suite parait � la hauteur.
Pour ceux qui pleurent que ce ne soit plus le m�me acteur ds le r�le titre, c'est pas contre lui ou par manque de respect de la coh�rence ou quoi.
Si vous r�fl�chissez, c'est parce que le Petit Nicolas a tjs autour de 9 ans ds ses aventures, hors, plusieurs ann�es sont pass�es entre les 2 films, l'autre n'aurait plus �t� cr�dible, et si on l'avait gard�, je trouve que �a aurait g�ch� un peu le truc d'avoir un Nico plus vieux que le perso d'origine.
Elisariel
Ce petit film, c'est une bouff�e de fra�cheur qui suit bien l'histoire et l'atmosph�re initiales du petit Nicolas �crite par Ren� Goscinny et superbement illustr� par Semp�.
Doctor Nico
Tout � fait d'accord avec toi. En g�n�ral je n'aime pas les changements d'acteur dans les films, mais l� y'a pas le choix, Nicolas a toujours le m�me �ge, l'acteur du 1 est trop vieux pour le r�le maintenant.
Willow-des-bois
Moi non plus je n'aime pas les changements pour un r�le sauf qd on passe d'un acteur horrible � un bon, voire m�me � un moyen.
Mais qd on passe de Brosnan � Craig pour un r�le que je n'ai pas besoin de nommer, je tire la gueule et prie que �a s'arr�te vite� mais non. D'abord on nous dit que pour 2, ensuite 3, apr�s on nous dit 3 de plus� au secours, �pargnez-nous.
Gilbert Duroux
Tous ces petits gar�ons en culottes courtes. Voil� qui va exciter Jean-Fran�ois Cop�. S'il pouvait faire un peu de promotion pour le film...
MC4815162342
Ducobu..euh....le petit nicolas j'avais bien aim� le 1 mais celui ci.... euh bah voil�....
MickDenfer
Bon ! t'auras pas rat� ta vie si tu loupes �a.
Bacta142
Justement les ann�es 60 (celles du Petit Nicolas) c'est son �poque r�v�e: ann�es De Gaulle, prosp�rit� �conomique, Fran�ais de souche.. Avant la "d�rive gauchiste" post-soixante-huitarde!..
Kao-BB
Autant le 1er �tait sympathique (et plut�t fid�le � l'esprit de l'oeuvre originale) autant cette suite s'annonce comme celle de trop...
cestmoioki
C'est moi o� on a perdu la l�g�ret� du 1er ?
Alexandre B.
Autant la sc�ne dans la cour de l'�cole semble plus ou moins r�aliste, autant la sc�ne � la plage semble plein d'anachronismes et fait penser � une sc�ne de s�rie AB Productions.
Top Bandes-annonces
