TITRE: Ligue 1:  Monaco suit le rythme de Paris - LExpress.fr
DATE: 2014-02-16
URL: http://www.lexpress.fr/actualites/1/sport/ligue-1-monaco-suit-le-rythme-de-paris_1324585.html
PRINCIPAL: 177544
TEXT:
Ligue 1:  Monaco suit le rythme de Paris
Par AFP, publi� le
15/02/2014 � 22:21
, mis � jour � 23:03
Paris - Gr�ce � un beau doubl� du Colombien James Rodriguez, Monaco est revenu � cinq points du Paris SG en s'imposant 2-0 � Bastia alors que Reims, tombeur de Bordeaux, s'est replac� � la 6e place � �galit� de points avec Marseille.
Gr�ce � beau doubl� du Colombien James Rodriguez, Monaco est revenu � cinq points du Paris SG en s'imposant 2-0 � Bastia alors que Reims, tombeur de Bordeaux, s'est replac� � la 6e place � �galit� de points avec Marseille.
afp.com
Un superbe coup franc (45e) et un contre de 60 m ponctu� d'une belle frappe (77e): Rodriguez, le co-meilleur passeur de la L1 (avec Ibrahimovic au d�but de la journ�e), a montr� qu'il savait aussi marquer, signant son premier doubl� en L1 avec Monaco.�
Monaco a domin� la rencontre de la t�te et des �paules et aurait pu ouvrir le score sur un retourn� acrobatique de Rivi�re (7e), mais c'est finalement le Colombien de 22 ans qui a fait oublier l'absence de son compatriote Radamel Falcao, bless� pour plusieurs mois, ainsi que du joker Dimitar Berbatov, touch� � un genou en Coupe de France mercredi.�
Avec ces trois points obtenus � Furiani, Monaco ne laisse pas le Paris SG, facile vainqueur de Valenciennes (3-0) vendredi, s'�chapper en t�te de la L1 et entretient un tant soit peu le suspense. �
"Cette victoire, ce soir, est la victoire de l'intelligence !" a soulign� l'entra�neur Claudio Ranieri, qui refuse toujours de dire que son club se bat pour le titre. "Vous voulez tous voir Monaco lutter pour le titre mais ce n'est pas facile. Paris sera tr�s dur � aller chercher car ils sont tr�s performants en Ligue des champions et en Championnat. Nous ferons de notre mieux".�
Dans la course pour le podium, Reims, invit� surprise � ce niveau, est revenu � la 6e place � �galit� de points avec Marseille gr�ce � une courte et pr�cieuse victoire contre Bordeaux (1-0).�
Un belle frappe de Nicolas de Pr�ville (64e) et un excellent Johny Placide, dans les buts, ont �t� le cocktail gagnant en Champagne. Autre mauvaise nouvelle pour Bordeaux, qui a perdu 4 de ses 5 derniers matches en L1, C�dric Carrasso s'est bless� sur un d�gagement.�
Ben Yedder en verve�
Le duel de pr�tendants entre Nice et Nantes a lui accouch� d'une souris et d'un match soporifique (0-0). Sans doute fatigu�s par leur prolongation malheureuse en Coupe face � Monaco mercredi, les Aiglons n'ont pas su forcer le destin face � Nantes et restent englu�s en milieu de tableau. �
Dans le bas du tableau, Rennes et Montpellier, qui r�vaient de grandeur en d�but de saison, n'ont pas pu se d�partager (2-2) avec un beau doubl� du Su�dois Ola Toivonen, d�cid�ment bonne pioche du mercato.�
Pour la lutte pour la survie, Sochaux a entretenu l'espoir du maintien en s'imposant 1 � 0 face � Guingamp, un de ses concurrents pour le maintien. Apr�s d'innombrables occasions pour Sochaux, le Zambien Stoppila Sunzu a d�livr� les siens � la 85e d'une belle t�te plongeante, possiblement en position de hors-jeu. Sochaux reste toutefois � 5 points du premier non rel�gable Evian. �
Toulouse a aussi r�alis� une belle op�ration en allant battre Lorient (3-1). Les Lorientais, qui ont men� 1 � 0, ont finalement craqu� physiquement, Alain Traor� ayant �t� expuls� d�s la 17e minute pour un tacle dangereux. A noter la grande prestation de Wissam Ben Yedder, auteur d'un but et de deux passes d�cisives d'une grande intelligence.�
Dimanche, la lutte pour le podium fera rage: Lyon accueille Ajaccio, Lille se d�place � Evian alors que Saint-Etienne et Marseille en d�coudront � Geoffroy-Guichard. �
Par
