TITRE: FA Cup: Everton file en quarts - Actu - Footballmag.fr
DATE: 2014-02-16
URL: http://www.football-mag.fr/Actu/Scans/Fa-Cup-Everton-file-en-quarts-82758/
PRINCIPAL: 178944
TEXT:
Publi� le 16 f�vrier 2014 � 16h30
FA Cup: Everton file en quarts
L'aventure continue en FA Cup pour Sylvain Distin et Everton. Dimanche apr�s-midi, les hommes de Roberto Martinez ont domin� Swansea City (3-1) en huiti�me de finale de la comp�tition, et rejoignent donc Manchester City ou Sunderland, qui ont eux valid� leur qualification pour les quarts la veille. A la mi-temps, le score �tait de parit� entre les deux formations (1-1), mais Steven Naismith et Leigton Baines ont fait la diff�rence en seconde p�riode.
