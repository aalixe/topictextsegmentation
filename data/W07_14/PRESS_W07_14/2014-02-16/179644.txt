TITRE: Afrique : Cr�ation d?un � G5 du Sahel � - Koaci Infos
DATE: 2014-02-16
URL: http://koaci.com/articles-89892
PRINCIPAL: 179643
TEXT:
Mohamed Ould Abdel Aziz SearchMohamed Ould Abdel Aziz (ph) -
Bamako le 16 f�vrier 2014 � koaci.com ? Serait ce pour tenter de lutter plus efficacement contre les groupes terroristes qui s'y installe? Le G5 du Sahel SearchG5 du Sahel a �t� cr�e ce dimanche dimanche lors d?un sommet � Nouakchott SearchNouakchott par les pr�sidents de la Mauritanie, du Mali, du Niger, du Tchad et du Burkina Faso
Cette nouvelle organisation r�gionale dont le si�ge se trouvera en Mauritanie et dont Mohamed Ould Abdel Aziz SearchMohamed Ould Abdel Aziz , en sera le premier pr�sident devrait �tre destin�e � renforcer la coop�ration sur le d�veloppement et la s�curit� de la r�gion du Sahel comme indiqu� dans le communiqu� final transmis � koaci.com.
?Nous sommes convenus d?unir nos efforts pour nous attaquer � ce double d�fi ? la s�curit� et le d�veloppement durable du Sahel? � indiqu� Mohamed Ould Abdel Aziz SearchMohamed Ould Abdel Aziz � l'issue du sommet.
Les ministres des cinq pays identifieront ensemble des projets d?investissement prioritaires et chercheront des investissements internationaux, en se concentrant sur des domaines comme les infrastructures, la s�curit� alimentaire, l?agriculture et le pastoralisme, lit-on dans le communiqu� final.
IB
