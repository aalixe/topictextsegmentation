TITRE: Foot - Angleterre - Arsenal - Giroud demande pardon
DATE: 2014-02-16
URL: http://www.lequipe.fr/Football/Actualites/Giroud-demande-pardon/442029
PRINCIPAL: 178811
TEXT:
a+ a- imprimer RSS
Depuis dimanche dernier, Olivier Giroud est pris dans la tourmente m�diatique. Le Sun avait r�v�l� que l'attaquant fran�ais d'Arsenal, qui est mari� et papa, avait �t� vu dans un h�tel londonien � 3h00 du matin avec une escort-girl � la veille d'un match face � Crystal Palace. Ce dimanche, le m�me m�dia r�v�le des photos prises par la femme en question dans la chambre...
Cela a contraint le joueur � sortir de son silence. Tr�s affect� par ces r�v�lations, il s'est exprim� sur Twitter : �Je pr�sente mes excuses � mon �pouse, ma famille, mon entra�neur, mes co�quipiers et les supporters d'Arsenal. Je dois maintenant me battre pour ma famille et pour mon club, pour obtenir leur pardon. Rien d'autre ne compte d�sormais.�
Je pr�sente mes excuses � mon �pouse, ma famille, mon entraineur, mes co�quipiers et les supporters d'Arsenal.
� Olivier Giroud (@_OlivierGiroud_) 16 F�vrier 2014
Je dois maintenant me battre pour ma famille et pour mon club, pour obtenir leur pardon. Rien d'autre ne compte d�sormais.
