TITRE: EGYPTE. Trois touristes tués dans une explosion - 16 février 2014 - Le Nouvel Observateur
DATE: 2014-02-16
URL: http://tempsreel.nouvelobs.com/monde/20140216.OBS6542/egypte-au-moins-trois-touristes-tues-dans-une-explosion.html
PRINCIPAL: 0
TEXT:
Actualité > Monde > EGYPTE. Trois touristes tués dans une explosion
EGYPTE. Trois touristes tués dans une explosion
Publié le 16-02-2014 à 14h25
Mis à jour à 16h11
A+ A-
Le bus vis� transportait une trentaine de touristes sud-cor�ens�pr�s de la station baln�aire de Taba dans le Sina� �gyptien. Le chauffeur du bus a lui aussi �t� tu�.
A Taba,dans le Sinaï égyptien à la frontière avec Israël, après une explosion qui a visé un car de touristes le 16 février 2014. (AFP / STR)
Au moins quatre personnes ont �t� tu�es dimanche 16 f�vrier dans l'explosion d'une bombe dans un autobus transportant des touristes sud-cor�ens pr�s de la station baln�aire de Taba, dans le Sina� �gyptien sur la fronti�re avec Isra�l, selon le minist�re de la Sant�. L' attentat , qui n'a pas encore �t� revendiqu�, a �galement fait 14 bless�s.
Le chauffeur �gyptien figure parmi les morts, a annonc� le minist�re de l'Int�rieur, en ajoutant que le v�hicule transportait des "touristes cor�ens" mais sans donner de pr�cision sur les autres victimes.
L'autocar transportait 31 membres d'une �glise chr�tienne de la province m�ridionale de Jincheon en Cor�e du Sud ainsi que leur guide, selon�S�oul.
Des images de la t�l�vision nationale montraient le haut de l'autocar jaune d�vast� par la d�flagration et l'incendie qui a suivi. La bombe a explos� � l'avant du v�hicule, selon le minist�re de l'Int�rieur. Le bus a �t� frapp� alors que le chauffeur attendait au passage frontalier de Taba, a ajout� le minist�re dans un communiqu�, sans pr�ciser dans quel sens les touristes franchissaient la fronti�re entre Isra�l et l'Egypte.
Le porte-parole du minist�re de la Sant�, Ahmed Kamel, a pr�cis� qu'il �tait impossible de reconna�tre les corps.
Des touristes vis�s pour la premi�re fois depuis 2009
Ce drame survient en pleine vague d'attentats qui ne visaient jusqu'alors que les forces de l'ordre en Egypte, depuis que l'arm�e a destitu� et arr�t� le pr�sident islamiste Mohamed Morsi d�but juillet.
Ces attentats ont �t�, pour la plupart, revendiqu�s par Ansar Beit al-Maqdess, un groupe jihadiste bas� dans le Sina�, disant s'inspirer d'Al-Qa�da et assurant agir en repr�sailles � la r�pression sanglante men�e par le nouveau pouvoir contre les partisans de M. Morsi.
Les attentats visant la police et l'arm�e se sont multipli�s depuis l'�viction de M. Morsi le 3 juillet, mais aucun n'avait vis� des �trangers, dans ce pays dont l'�conomie d�pend fortement du tourisme.
La derni�re attaque contre des touristes remonte � f�vrier 2009 quand une Fran�aise a �t� tu�e par l'explosion d'une grenade en bordure du souk de Khan el-Khalili, au coeur du Caire historique. Il s'agissait alors de la premi�re attaque terroriste contre des Occidentaux en Egypte depuis 2006.
Mais entre 2004 et 2006, nombre d'Egyptiens et de touristes �trangers avaient p�ri dans des attentats dans les stations baln�aires du Sina�.
Partager
