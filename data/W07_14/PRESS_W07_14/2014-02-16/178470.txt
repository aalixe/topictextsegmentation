TITRE: L'imam chiite irakien Sadr abandonne la politique
DATE: 2014-02-16
URL: http://www.zonebourse.com/actualite-bourse/Limam-chiite-irakien-Sadr-abandonne-la-politique--17956360/
PRINCIPAL: 178468
TEXT:
L'imam chiite irakien Sadr abandonne la politique
16/02/2014 | 14:05
Recommander :
0
L'imam chiite irakien Moktada al Sadr, qui mena la r�volte contre les forces d'occupation am�ricaines avant leur d�part en 2011 et joua par la suite un r�le influent au sein du gouvernement, a d�cid� de se retirer de la vie politique et de dissoudre son mouvement.
Le chef religieux annonce cette d�cision dans une lettre manuscrite post�e samedi soir sur son site internet.
Moktada al Sadr ne fournit aucune explication mais deux membres de son courant politique Ahrar, qui contr�le environ un huiti�me des si�ges au parlement de Bagdad, disent qu'il est furieux que des �lus aient enfreint ses ordres en votant en faveur d'un projet de loi controvers� garantissant aux d�put�s de fortes pensions de retraite.
"J'annonce ma non-ing�rence dans les affaires politiques en g�n�ral. Aucun bloc ne me repr�sente plus d�sormais, ni quelque position � l'int�rieur ou � l'ext�rieur du gouvernement ou au parlement", �crit l'imam chiite. "Quiconque contrevient � cela s'exposera � une responsabilit� religieuse et l�gale."
Selon le communiqu�, les organisations caritatives et les �coles g�r�es par Sadr resteront ouvertes.
Apr�s avoir �t� l'un des protagonistes du soul�vement contre les forces d'occupation am�ricaines au milieu des ann�es 2000, � la t�te de la milice de l'Arm�e du Mahdi, Moktada al Sadr a soutenu l'accession au pouvoir du Premier ministre chiite Nouri al Maliki.
Mais les deux hommes se sont brouill�s par la suite, Sadr reprochant � Maliki d'accumuler trop de pouvoirs.
Selon les analystes, le retrait de Sadr pourrait profiter au chef du gouvernement qui brigue un nouveau mandat aux �lections l�gislatives d'avril.
"La d�cision de Sadr jouera certainement en faveur du Premier ministre Maliki d'une mani�re ou d'une autre", pr�dit Ali Amir, un sp�cialiste des affaires religieuses chiites.    (Ahmed Rachid; Jean-St�phane Brosse pour le service fran�ais)
Recommander :
