TITRE: Lille-Girard: "Le mérite de s'accrocher" - Ligue 1 - Football.fr
DATE: 2014-02-16
URL: http://www.football.fr/ligue-1/scans/lille-girard-le-merite-de-s-accrocher-522844/
PRINCIPAL: 179682
TEXT:
16 février 2014 � 21h25
Mis à jour le
Réagir 5
René Girard a salué la ténacité de ses joueurs après le match nul arraché dans les arrêts de jeu par Lille sur la pelouse d'Évian Thonon Gaillard (2-2), ce dimanche lors de la 25e journée.
"Le match était dur mais on a eu le mérite d’y croire jusqu’au bout, de nous accrocher, a réagi l'entraîneur lillois sur le site de l'ETG. On a été un peu chanceux à la fin mais le penalty était un petit peu sévère aussi donc je dirai que c’est un équilibre."
"On s’est adapté à cette équipe d’Evian. Je crois que c’est bien pour nous de ramener un point ce soir car c’était mal parti et pas gagné d’avance, a poursuivi le technicien du Losc. On va continuer à s’accrocher, serrer les dents et avancer."
