TITRE: EUROPE - Les protestataires anti-gouvernement �vacuent la mairie de Kiev - France 24
DATE: 2014-02-16
URL: http://www.france24.com/fr/20140216-mairie-kiev-evacuation-manifestation-ianoukovitch-ukraine/
PRINCIPAL: 177997
TEXT:
Texte par FRANCE 24 Suivre france24_fr sur twitter
Derni�re modification : 16/02/2014
Dimanche, les manifestants ukrainiens qui protestent contre le gouvernement de Viktor Ianoukovitch ont mis fin � l�occupation de la mairie de Kiev, la capitale du pays. Une concession majeure destin�e � apaiser les tensions.
Dimanche, les opposants au pouvoir ukrainien, incarn� par le pr�sident Viktor Ianoukovitch , ont d�cid� de mettre fin � l'occupation de la mairie de Kiev.
Ils occupaient ce lieu symbolique de la capitale depuis le mois de d�cembre.
"La mairie est pratiquement �vacu�e", a d�clar� le "commandant" des lieux Rouslan Andri�ko � un journaliste de l'AFP.
L'ambassadeur de Suisse en Ukraine, dont le pays occupe la pr�sidence tournante de l'OSCE, est entr� quelques minutes apr�s l'annonce dans le b�timent �vacu�.
"La Suisse qui pr�side l'OSCE a �t� invit�e par les deux parties en conflit � participer au processus de transfert de la mairie aux autorit�s", a-t-il d�clar� � la presse.
La mairie de Kiev avait �t� investie par les protestataires le 1er d�cembre et �tait devenu le "QG de la r�volution". Depuis, le b�timent h�bergeait jusqu'� 700 manifestants qui y dormaient et s'y r�chauffaient.
Il s'agit d'un lieu hautement symbolique, tout comme le Ma�dan, place centrale de Kiev occup�e depuis la volte-face pro-russe du pouvoir fin novembre au d�triment d'un rapprochement avec l'Union europ�enne.
Son �vacuation avait �t� pr�sent� comme une condition cl� pour permettre l'application de la loi d'amnistie � l'�gard de 234 manifestants qui ont �t� lib�r�s. Ces protestataires sont toujours inculp�s de crimes passibles de peines pouvant atteindre 15 ans de prison.
Avec AFP
