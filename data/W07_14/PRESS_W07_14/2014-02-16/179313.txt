TITRE: Arnaud Montebourg, un ministre "inutile" pour Jean-Vincent Plac� - L'Express
DATE: 2014-02-16
URL: http://www.lexpress.fr/actualite/politique/arnaud-montebourg-un-ministre-inutile-pour-jean-vincent-place_1324702.html
PRINCIPAL: 0
TEXT:
Voter (211)
� � � �
Jean-Vincent Plac�, chef de file des s�nateurs �cologistes, a qualifi� ce dimanche Arnaud Montebourg de ministre "inutile".
afp.com/Alain Jocard
Arnaud Montebourg , un ministre "inutile"? C'est ce qu'a affirm� Jean-Vincent Plac� , chef de file des s�nateurs �cologistes, ce dimanche au micro du Forum Radio J . Pour lui, le gouvernement n'a pas d�velopp� une politique industrielle tourn�e vers l'innovation et l'avenir.�
"Il est impuissant, plut�t inutile en r�alit�. Pouvez-vous citer une r�ussite industrielle depuis 18 mois? Nous continuons malheureusement les m�mes politiques que sous Sarkozy", a lanc� Jean-Vincent Plac�.�
Le ministre du Redressement productif court, selon lui, apr�s "des �conomies qui sont plut�t tourn�es vers le pass�, alors qu'il y a de belles �conomies d'avenir qui pourraient �tre cr�atrices d'emplois pour la France, et cela Arnaud Montebourg le rate".�
Jean-Vincent Plac� n'en est pas � ses premi�res piques vis-�-vis du ministre. Le s�nateur de l'Essonne avait qualifi� Arnaud Montebourg de "ministre en �chec" et de "schtroumpf grognon" , en d�cembre dernier.�
Changer de Premier ministre? "Inappropri�"
Interrog� sur la situation de l'ex�cutif, il a estim� qu'il faudra prendre dans l'ann�e qui vient "des d�cisions sur les �quipes", tout en maintenant Jean-Marc Ayrault � Matignon. "Un remaniement de la m�me �quipe avec, je pense, le m�me Premier ministre", a-t-il dit.�
Jean-Marc Ayrault a lanc� sous l'autorit� du pr�sident de la R�publique "de grands chantiers cette ann�e sur la r�forme fiscale, la question du pacte de responsabilit�...", a fait valoir Jean-Vincent Plac�. "Tout cela doit se faire avant l'�t�, il me semblerait tr�s inappropri� et inefficace de changer le chef de l'�quipe".�
Le s�nateur a souhait� par ailleurs que dans le prochain gouvernement, les �cologistes soient "davantage li�s aux questions �cologiques au sens environnemental du terme", �voquant les questions de l'eau, de la pollution et des d�chets.�
Sur le m�me sujet
