TITRE: Kickstarter pirat� ! Changez vos mots de passe
DATE: 2014-02-16
URL: http://www.gameblog.fr/news/41214-kickstarter-pirate-changez-vos-mots-de-passe
PRINCIPAL: 179576
TEXT:
Jeux Vid�o Gameblog > Actualit�s > Kickstarter pirat� ! Changez vos mots de passe
Kickstarter pirat� ! Changez vos mots de passe
Par Julien Chi�ze - publi� le
16 F�vrier 2014��11h10
Les failles de s�curit� et les tentatives de vol de donn�es sont fr�quentes dans le milieu informatique. Derni�re victime en date, le g�ant du financement participatif : Kickstarter.�
Le blog de Kickstarter vient ainsi de publier une note de s�curit� �r�dig�e par Yancey Strickler, CEO de la soci�t�. Dans ce post, on apprend que dans la nuit de mercredi 12 f�vrier, les autorit�s am�ricaines ont pr�venu le site participatif que des hackers se seraient introduits et auraient eu acc�s � des informations concernant des donn�es d'utilisateurs.�
Kickstarter aurait alors imm�diatement colmat� la br�che et renforc� ses mesures de s�curit�.�
Liste des �l�ments hack�s :�
