TITRE: Marseille : un campement de Roms d�truit par un incendie, pas de victime - RTL.fr
DATE: 2014-02-16
URL: http://www.rtl.fr/actualites/info/article/marseille-un-campement-de-roms-detruit-par-un-incendie-pas-de-victime-7769753905
PRINCIPAL: 178747
TEXT:
Accueil > Toutes les Actualit�s info > Marseille : un campement de Roms d�truit par un incendie, pas de victime
Marseille : un campement de Roms d�truit par un incendie, pas de victime
faits divers
Un incendie a d�vast� un campement de Roms abritant environ 45 personnes ce dimanche matin � Marseille.
Un campement de Roms abritant environ 45 personnes a �t� d�vast� dimanche matin � Marseille par un incendie, a priori d'origine accidentelle et qui n'a fait aucune victime, a-t-on appris de sources concordantes. Les marins-pompiers de Marseille ont indiqu� avoir �t� alert�s peu apr�s 5h30 pour un incendie dans ce campement du 2e arrondissement de la ville, situ� en contrebas de l'autoroute A55, derri�re le port.
Ils ont �vacu� la "quinzaine d'habitation de fortune totalement embras�es", mobilisant 44 marins-pompiers. L'intervention s'est termin�e � 8h00, laissant un seul abri intact et il n'y a pas eu de bless�s, ont pr�cis� les pompiers dans un communiqu�. "Selon les premiers �l�ments de l'enqu�te, il s'agit a priori d'un incendie d'origine accidentelle", a d�clar� � l'AFP une source judiciaire, alors que le parquet a mandat� sur place un expert.
"La pr�f�te � l'�galit� des chances propose sur place des nuit�es d'h�tel en urgence", a �galement expliqu� � l'AFP Jean-Paul Kopp, pr�sident de l'association Rencontres tsiganes. Ce camp, de taille moyenne, ne faisait pas l'objet de mesure d'expulsion, a-t-il pr�cis�.
La r�daction vous recommande
