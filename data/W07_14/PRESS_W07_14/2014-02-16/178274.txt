TITRE: Un mort et un bless� grave dans un incendie � Montreuil - LExpress.fr
DATE: 2014-02-16
URL: http://www.lexpress.fr/actualites/1/societe/un-mort-et-un-blesse-grave-dans-un-incendie-a-montreuil_1324630.html
PRINCIPAL: 0
TEXT:
Un mort et un bless� grave dans un incendie � Montreuil
Par AFP, publi� le
16/02/2014 � 12:10
, mis � jour � 14:05
Montreuil - Un homme de 21 ans est d�c�d� dimanche matin et une autre personne a �t� gri�vement bless�e lors d'un incendie dans un immeuble de bureaux et d'appartements � Montreuil (Seine-Saint-Denis), a-t-on appris de plusieurs sources.
Un homme de 21 ans est d�c�d� dimanche matin et une autre personne a �t� gri�vement bless�e lors d'un incendie dans un immeuble de bureaux et d'appartements � Montreuil (Seine-Saint-Denis), a-t-on appris de plusieurs sources.
afp.com/Fran�ois Guillot
Selon le commandant des pompiers Gabriel Plus, le sinistre s'est d�clar� peu avant 08H30 au premier des quatre �tages de l'immeuble. L'incendie a pu �tre ma�tris� rapidement, gr�ce � la facilit� d'acc�s � l'immeuble, mais le feu "tr�s violent" avait d�j� fait plusieurs victimes.�
"Un jeune homme a p�ri carbonis� au premier �tage", a d�clar� le porte-parole des pompiers � l'AFP.�
Un autre homme de 22 ans, le fils du propri�taire de l'immeuble, a �t� gri�vement bless�. "Il s'est d�fenestr� et se trouve dans un �tat grave. Transport� � l'h�pital parisien du Val-de-Gr�ce, son pronostic vital est tr�s engag�", a pr�cis� � l'AFP le parquet de Bobigny, dont un repr�sentant s'est rendu sur place.�
L'incendie a en outre fait quatre bless�s l�gers, qui se trouvaient au 4e �tage et qui ont �t� intoxiqu�s par les fum�es, selon les pompiers.�
Dans cet immeuble, le propri�taire avait son domicile familial au 4e �tage. Il avait laiss� une pi�ce � son fils au premier, a pr�cis� le parquet.�
Le pr�fet de Seine-Saint-Denis s'est rendu sur place dans la matin�e. La police judiciaire a �t� saisie d'une enqu�te pour d�terminer les circonstances de l'incendie.�
Par
