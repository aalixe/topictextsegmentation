TITRE: La sensation Jansrud - JO 2014 - Sports.fr
DATE: 2014-02-16
URL: http://www.sports.fr/jo-2014/ski-alpin/articles/jo-2014-jansrud-fait-sensation-1010996/?sitemap
PRINCIPAL: 178320
TEXT:
16 février 2014 � 08h10
Mis à jour le
16 février 2014 � 12h09
Comme en descente, le Super-G olympique a accouché d’une surprise, ce dimanche matin à Sotchi. Alors que l’on attendait davantage son compatriote Aksel Lund Svindal, le Norvégien Kjetil Jansrud a cueilli l’or avec trois dixièmes de marge par rapport à l’Américain Andrew Weibrecht et plus d’une demi-seconde d’avance sur Bode Miller et le Canadien Jan Hudec. 
En voilà un qui aime Sotchi ! Bronzé sur la descente initiale la semaine dernière, Kjetil Jansrud a gravi deux marches ce dimanche pour cueillir l’or olympique au bas du Super-G. Une sacrée performance pour celui qui à Vancouver il y a quatre ans avait décroché l’argent du slalom géant. Sans avoir jamais décroché une médaille mondiale, lui le maudit qui l’an passé aux Championnats du monde de Schladming se rompait les ligaments croisés, poursuivi par la poisse…
Aujourd’hui les blessures et les doutes sont loin derrière le Norvégien de 28 ans, bel et bien au sommet de son art. Deux ans après sa dernière et seule victoire en Coupe du monde, sur le Super-G de Kvitfjell, Kjetil Jansrud se rappelle au bon souvenir du gratin mondial à point nommé, non sans éclipser son compatriote Aksel Lund Svindal, favori de l’épreuve ce dimanche et septième au final, à plus de six dixièmes de la gagne – neuf centièmes seulement du podium.
Weibrecht, l’autre surprise
De fait, il n’y a guère que l’Américain Andrew Weibrecht qui a su donner des sueurs froides au lauréat du jour. En avance sur le chrono de référence aux trois pointages intermédiaires, le médaillé de bronze des Jeux de Vancouver dans la spécialité s’est simplement tassé en fin de parcours pour échouer à trois dixièmes de l’or. Suffisant pour coiffer au poteau son compatriote Bode Miller et le Canadien Hudec, troisièmes ex-aequo à 53 centièmes. Un bronze qui assurément ne faisait pas la joie du premier… "Douze ans après ma première médaille, bien sûr c’est quelque chose. Je ne sais pas si ce sont mes derniers Jeux Olympiques, je vous dirais oui aujourd'hui mais on ne sait jamais dans le futur."
Pour l’équipe de France de vitesse en revanche, c’en est bien fini. Si le parcours de ce Super-G avait été tracé par un technicien français, les Bleus n’y ont guère brillé. Adrien Théaux, le premier d’entre eux, se classe 11e à 1"21. Suivent Thomas Mermillod-Bondin, 15e à 1"39, David Poisson, 17e à 1"60 et Johan Clarey, 19e à 1"61. Vivement le slalom et le géant, avec pour le coup de véritables chances de breloques tricolores chez les messieurs.
