TITRE: Victoires de la musique: plus nulle la c�r�monie | Musique | Var-Matin
DATE: 2014-02-16
URL: http://www.varmatin.com/musique/victoires-de-la-musique-plus-nulle-la-ceremonie.1582995.html
PRINCIPAL: 177941
TEXT:
Tweet
Animation nulle, r�alisation ringarde, son catastrophique, artistes peu impliqu�s, public amorphe: les Victoires de la musique 2014 ont encore prouv� l'incapacit� su service public � proposer un divertissement de qualit�.
On doit des excuses � Daft Punk. Leur refus de participer aux Victoires de la Musique, malgr� le succ�s mondial de leur dernier disque, avait pu para�tre un peu m�prisant et participer, peu ou prou, d'un sentiment � anti fran�ais �.
Il faut, au contraire, les f�liciter pour leur clairvoyance. Avec, comme ma�tres de c�r�monie, le tandem Virginie Guilhaume -Bruno Guillon, on pouvait, en effet, s'attendre � ce que la pr�sentation et les interviews ne volent pas bien haut.
Mais l�, on a touch� le fond�
�Vous faites beaucoup pour la chanson fran�aise�
Virginie Guilhaume est bien mignonne, mais personne n'a d� lui dire que pr�senter un artiste ce n'est pas que r�citer sa fiche Wikip�dia. L'animation c'est un m�tier� Et, visiblement, pas le sien. Ou alors, dans les supermarch�s et les maisons de retraite. � Bravo Salvatore Adamo ! Vous faites beaucoup pour la chanson fran�aise �, m�me Michel Drucker n'aurait pas os�. Bien que s'agissant des Victoires de la musique fran�aise, quelques notions d'anglais auraient aussi pu lui �tre utiles (� Outroune � � la place d'� Outrun �, grand moment comique).
Son discours de d�fense des droits des intermittents, �nonn� avec la conviction d'une �l�ve de sixi�me sous alphab�tis�e, restera aussi dans l'histoire des moments g�nants � la t�l�vision. Quant � Bruno Guillon, ses interventions auraient d� rester o� il semblait se trouver : au comptoir d'un bistrot.
Ajoutez � cela une mise en sc�ne et une r�alisation ringardes, une prise de son catastrophique, des r�cipiendaires incapables de dire autre chose que � merci � (personne ne leur avait visiblement demand� de pr�parer un discours de remerciement) et un public amorphe et vous obtenez l'une des c�r�monies les plus affligeantes de l'histoire des Victoires de la musique.
C'est d'autant plus rageant que le palmar�s ne souffre pas trop de critiques, avec le sacre attendu de Stromae (3 Victoires pour 4 nominations), la cons�cration de Ph�nix, les choix judicieux de Woodkid et La Femme pour les r�v�lations, ceux d'Ibrahim Maalouf, de Kavinsky et du collectif 1995 pour les musiques du monde, �lectroniques et urbaines et ceux de M et de Shaka Ponk pour le live.
Deux belles d�couvertes
On s'�tonne seulement d'une troisi�me Victoire accord�e � Vanessa Paradis, comme s'il n'y avait pas d'autre chanteuse dans ce pays (qui a dit Carla Bruni ?) et du vote du public pour la chanson de l'ann�e (20 ans de Johnny Hallyday), alors que celles de Stromae ont autrement trust� les ondes et les ventes�
Les prestations des artistes nomm�s ont cruellement souffert des probl�mes de son et de mise en sc�ne, mais ont tout de m�me permis de tenir les trois heures qu'a encore dur� la c�r�monie, avec deux belles d�couvertes pour les t�l�spectateurs : Ibrahim Maalouf et sa trompette magique et Christine and the Queens, �trange croisement de Camille, de Valerie Lemercier et de Lana Del Rey, qui n'a laiss� personne indiff�rent. On a �t� heureux �galement d'apercevoir Etienne Daho qui ne semblait pas en grande forme et ne s'est d'ailleurs pas attard�.
Ceci dit, il a bien fait, vu le niveau de la soir�e�
Tweet
Tous droits de reproduction et de repr�sentation r�serv�s. � 2012 Agence France-Presse.
Toutes les informations (texte, photo, vid�o, infographie fixe ou anim�e, contenu sonore ou multim�dia) reproduites dans cette rubrique (ou sur cette page selon le cas) sont prot�g�es par la l�gislation en vigueur sur les droits de propri�t� intellectuelle. �Par cons�quent, toute reproduction, repr�sentation, modification, traduction, exploitation commerciale ou r�utilisation de quelque mani�re que ce soit est interdite sans l�accord pr�alable �crit de l�AFP, � l�exception de l�usage non commercial personnel. �L�AFP ne pourra �tre tenue pour responsable des retards, erreurs, omissions qui ne peuvent �tre exclus dans le domaine des informations de presse, ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
AFP et son logo sont des marques d�pos�es.
Philippe Dupuy
Musique , M�dias , France
Le d�lai de 15 jours au-del� duquel il n'est plus possible de contribuer � l'article est d�sormais atteint.
Vous pouvez poursuivre votre discussion dans les forums de discussion du site. Si aucun d�bat ne correspond � votre discussion, vous pouvez solliciter le mod�rateur pour la cr�ation d'un nouveau forum : moderateur@nicematin.com
Vos derniers commentaires
wilbur
17/02/2014 � 15h21
Je suis content des autres commentaires,j'ai cru �tre sourd tellement le son �tait mauvais.Service public .....au service du public?
