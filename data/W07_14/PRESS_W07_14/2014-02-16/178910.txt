TITRE: Vie privée: Arnaud Montebourg porte plainte contre Paris Match | Site mobile Le Point
DATE: 2014-02-16
URL: http://www.lepoint.fr/ces-gens-la/vie-privee-arnaud-montebourg-porte-plainte-contre-paris-match-16-02-2014-1792285_264.php
PRINCIPAL: 178909
TEXT:
16/02/14 à 15h46
Vie privée: Arnaud Montebourg porte plainte contre Paris Match
Le ministre du Redressement productif, Arnaud Montebourg , a indiqué dimanche avoir porté plainte contre l'hebdomadaire Paris Match qui le montre en compagnie de l'actrice Elsa Zylberstein dans son édition parue jeudi.
"On a, dans la loi française, droit au respect de l'intimité de sa vie privée. Donc ce sont des photos volées, donc ce journal sera poursuivi en justice et, comme tous les autres, condamné", a dit le ministre sur France 3.
Interrogé sur le fait de savoir s'il allait porter plainte, M. Montebourg a répondu: "C'est fait."
Sollicitée dimanche par l'AFP, l'avocate de Paris Match, Me Marie-Christine Percin, a déclaré ne pas être au courant de cette plainte.
