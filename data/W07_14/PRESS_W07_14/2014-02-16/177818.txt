TITRE: Leverkusen, un club ��chelle humaine - leParisien.fr
DATE: 2014-02-16
URL: http://www.leparisien.fr/informations/leverkusen-un-club-a-echelle-humaine-16-02-2014-3595663.php
PRINCIPAL: 0
TEXT:
Leverkusen, un club ��chelle humaine
LEVERKUSEN (ALLEMAGNE)
BayArena (Leverkusen), le 2 f�vrier. Le d�fenseur central turc Toprak (� gauche) et l�attaquant allemand Kiessling sont deux pi�ces ma�tresses du dispositif du Bayer Leverkusen.
| (AP/Frederico Gambarini.)
Le Bayer Leverkusen n'est pas un club typique de Bundesliga . Cr�� en 1904 et localis� � une vingtaine de kilom�tres de Cologne, il est loin de b�n�ficier d'une popularit� comparable � celle du Bayern Munich ou du Borussia Dortmund.
L'�ternel second
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Deuxi�me en 1997, 1999, 2000, 2002 et 2011, il se caract�rise par sa discr�tion et son s�rieux. Une seule fois, il a d�fray� la chronique. C'�tait en octobre 2000. Christoph Daum, alors entra�neur du Bayer et promis au poste de s�lectionneur de l'Allemagne quelques semaines plus tard, reconna�t consommer de la coca�ne. Il est imm�diatement limog�. En 2001-2002, Leverkusen se hisse en finale de la Ligue des champions mais une reprise de vol�e, venue d'ailleurs de Zidane, ruine ses espoirs de conqu�te (2-1). Laur�at de la Coupe de l'UEFA en 1988, son dernier troph�e remonte � 1993 avec la Coupe d'Allemagne...
Des dirigeants avis�s
Leverkusen est un club davantage vendeur qu'acheteur. Comme � Porto, ses dirigeants n'ont pas leur pareil pour recruter de talentueux joueurs en devenir avant de les c�der, quelques ann�es plus tard, au double ou au triple du prix initial. Dimitar Berbatov, le n�o-Mon�gasque, a ainsi �t� nagu�re transf�r� pour 40 M� � Manchester United. Le d�fenseur br�silien Lucio est pass� au Bayern Munich pour 15 M�. Plus r�cemment, l'international allemand Andr� Sch�rrle a rejoint Chelsea pour 23 M�. Deux ans plus t�t, le Bayer l'avait arrach� � Mayence pour seulement 10 M�. � Chaque saison, notre objectif est d'�tre europ�en et, si possible, de nous qualifier pour la Ligue des champions. Ces derni�res ann�es, nous avons presque toujours atteint notre but �, se r�jouit Rudi V�ller, le directeur sportif. Cette saison, il est solidement install� � la deuxi�me place. Il est m�me le dernier club � avoir battu le Bayern (28 octobre 2012).
Des supporteurs discrets
A la BayArena, l'ambiance n'a rien de d�mentielle. Les rencontres � domicile s'y disputent rarement � guichets ferm�s, une exception outre-Rhin. Agrandie de 10 000 places voici quelques ann�es, elle peut accueillir jusqu'� 31 000 spectateurs. Il s'agit d'un joli petit bijou avec une pelouse impeccable, un �norme restaurant gastronomique, un h�tel, une agence de voyages et une boutique de supporteurs. Chaque jour ont lieu des visites guid�es. Cette saison, Leverkusen compte 18 000 abonn�s. Mercredi dernier contre Kaiserslautern (0-1) en quart de finale de la Coupe d'Allemagne, les 2 500 fans adverses ont fait plus de bruit que le kop nord du Bayer...
Bayer, un sponsor g�n�reux
L'entreprise chimique et pharmaceutique Bayer, qui a son si�ge � Leverkusen, et le club sont indissociables. Seule une station de RER les s�pare. Chaque ann�e, Bayer verse 25 M� au club. Son sponsor maillot est le groupe �lectronique LG Electronics qui offre 5 M� par an. Son budget, proche de 60 M�, le classe au 7 e rang national � l'instar de sa masse salariale estim�e � 53 M�. � Nous avons des moyens int�ressants et une bonne sant� �conomique. N�anmoins, nous sommes � des ann�es-lumi�re du Bayern et m�me de Dortmund et de Schalke 04, tient � pr�ciser V�ller. Notre joueur le mieux pay� per�oit le salaire d'un rempla�ant du Bayern. �
Le Parisien
