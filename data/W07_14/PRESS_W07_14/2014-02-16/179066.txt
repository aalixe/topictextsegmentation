TITRE: Retour vers les futur : le laçage automatique bientôt réalité ? - Europe1.fr - High-Tech
DATE: 2014-02-16
URL: http://www.europe1.fr/High-Tech/Retour-vers-les-futur-le-lacage-automatique-bientot-realite-1803097/
PRINCIPAL: 0
TEXT:
Retour vers les futur : le la�age automatique bient�t r�alit� ?
Par Marc-Antoine Bindler
Tweet
Les baskets de Retour vers le futur seront-elles bient�t r�alit� ? � Reuters
Le designer des baskets Nike de Marty Mcfly a annonc� que ce gadget l�gendaire pourrait �tre disponible d'ici 2015.
MAG. Nike Mag, c'est le nom de ces baskets � "lacage automatique" que tout individu qui a grandi entre les ann�es 80 et 90 a un jour r�v� de poss�der. Probl�me : pendant longtemps, ces chaussures de l�gende ne furent que fiction. Le mod�le a en effet �t� invent� pour les besoins du film "Retour vers le futur 2", r�alis� par le cin�aste Robert Zemekis, sorti dans les salles obscures en 1985. Apr�s une premi�re commercialisation d�cevante en 2011, un mod�le plus fid�le pourrait sortir en 2015, rel�ve le site d'actus culturelles Kombini.com .
>> Marty Mac Fly d�couvre les Nike Mag :
1.500 paires en 2011, sans le "power Laces". En 2011 d�j�, les fans du film avait cru pouvoir atteindre le Graal : 1.500 paires (seulement) de Nike Mag avaient �t� produites et commercialis�es via un syst�me d'ench�res sur le site nikemag.ebay.com. Le produit de cette vente avait alors �t� revers� � la fondation contre la maladie de Parkinson de Michael J. Fox, l'acteur qui incarne Marty Mac Fly dans la trilogie culte. Outre le nombre r�duit d'exemplaires et leur prix (5.000 euros en premi�re ench�re), les fans avaient �t� d��us par un d�tail de poids : l'absence sur ce mod�le du "Power Laces", le syst�me de la�age automatique.
