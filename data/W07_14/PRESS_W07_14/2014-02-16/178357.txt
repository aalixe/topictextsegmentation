TITRE: Ukraine: l'opposition se mobilise avant d'éventuelles concessions | Site mobile Le Point
DATE: 2014-02-16
URL: http://www.lepoint.fr/monde/ukraine-l-opposition-se-mobilise-avant-d-eventuelles-concessions-16-02-2014-1792178_24.php
PRINCIPAL: 178354
TEXT:
16/02/14 à 08h10
Ukraine: l'opposition se mobilise avant d'éventuelles concessions
L'opposition ukrainienne se mobilise à nouveau massivement dans la rue dimanche contre le président Viktor Ianoukovitch , tout en se déclarant prête à des concessions, après la libération de militants.
Pour la onzième fois depuis le début il y a près de trois mois de la contestation, née de la volte-face du pouvoir qui a renoncé à un rapprochement avec l'Union européenne pour se tourner vers la Russie , les manifestants se réunissent à midi (10H00 GMT) sur le Maïdan, la place de l'Indépendance, dans le centre de Kiev.
Le précédent rassemblement, dimanche 9 février, avait réuni près de 70.000 personnes sur le Maïdan, occupé depuis près de trois mois, couvert d'une centaine de tentes et entourée de barricades.
Le mouvement de contestation s'est transformé au fil des semaines en un rejet pur et simple du régime du président Viktor Ianoukovitch, et ni la démission du gouvernement ni les négociations engagées après les affrontements, qui ont fait quatre morts et plus de 500 blessés fin janvier, n'ont réglé le conflit.
Les opposants ont promis de préparer dimanche "une offensive pacifique" pour obtenir la satisfaction de leurs revendications.
Pour désamorcer la tension, les autorités ont annoncé vendredi avoir libéré la totalité des 234 manifestants interpellés depuis décembre. Mais les accusations pesant sur eux, qui peuvent valoir aux opposants de lourdes peines allant jusqu'à 15 ans de prison, ne seront abandonnées, en vertu d'une loi d'amnistie adoptée en janvier, que si les contestataires évacuent les bâtiments officiels qu'ils occupent, dont la mairie de Kiev.
En contrepartie, le président Ianoukovitch a appelé vendredi soir l'opposition à "faire aussi des concessions".
- Aller jusqu'au bout -
Le parti nationaliste Svoboda qui contrôle la mairie s'est déclaré samedi "prêt" à évacuer le bâtiment une fois que la décision sera rendue publique par le "conseil" du Maïdan composé de représentants des partis d'opposition et de militants civils. "Le délai expire lundi (...) Nous voulons que cette décision soit approuvée par toutes les forces d'opposition", a souligné IOuri Syrotiouk, numéro deux de la formation.
La décision pourrait être annoncée lors de la manifestation dimanche.
Située sur le boulevard central Khrechtchatik, la mairie a été prise d'assaut en marge d'une manifestation monstre, à la suite de la dispersion violente d'étudiants dans le centre de Kiev. Le bâtiment où ont été mises en place une cantine et un hôpital de fortune, héberge jusqu'à 700 manifestants qui y dorment et se réchauffent.
Mais l'éventuelle évacuation de la mairie ne marque en rien un recul, pour les manifestants. "La révolution ne fait que commencer", a déclaré à l'AFP Rouslan Andriïko, membre du parti nationaliste Svoboda et responsable des lieux.
"Maintenant qu'on est dans la rue, il faut aller jusqu'au bout. Quand (le président Viktor) Ianoukovitch donnera sa démission on va tous faire une grande fête ici", assure en souriant Marina Nekrasova, qui se promène sur le Maïdan en tenant la main de sa petite fille. "La mairie, ils l'ont prise d'assaut une fois, ils pourront très bien la reprendre".
La stratégie du pouvoir, pariant sur un essoufflement du mouvement, semble avoir échoué. "Le temps ne joue pas en faveur du président Ianoukovitch, dont le soutien décline même au sein de son parti", estime le professeur allemand Andreas Umland, qui enseigne à l'université de sciences politiques de Kiev.
"Il s'agit maintenant de mettre au point un accord de partage du pouvoir pendant une période de transition jusqu'à de nouvelles élections. La question qui reste à trancher, c'est quel pouvoir restera entre les mains du président", juge-t-il.
