TITRE: Lavillenie f�te son record... sur un pied - BFMTV.com
DATE: 2014-02-16
URL: http://www.bfmtv.com/sport/lavillenie-fete-record-un-pied-712286.html
PRINCIPAL: 177891
TEXT:
> Athl�tisme
Lavillenie f�te son record... sur un pied
Renaud Lavillenie a effac� l'un des plus vieux records de l'athl�tisme. 21 ans apr�s Serge� Bubka, le perchiste fran�ais a franchi samedi � Donetsk en Ukraine, d�s le premier essai, une barre � 6,16 m. Bless� � un pied, il est forfait pour les Championnats du monde en salle d'athl�tisme, en mars, � Sopot en Pologne.
M. R. avec Vincent Giraldo et Nicolas Jamain
Le 16/02/2014 �  8:15
Mis � jour le 16/02/2014 �  8:21
- +
r�agir
Il l'a fait. Renaud Lavillenie a donc effac� l'un des plus vieux records de l'athl�tisme. 21 ans apr�s Serge� Bubka, le perchiste fran�ais a franchi hier � Donetsk en Ukraine, d�s le premier essai, une barre � 6,16 m.
Renaud Lavillenie rejoint les sommets de l�athl�tisme. Le Fran�ais a saut� 19 cm plus haut qu�aux derniers Jeux Olympiques de Londres. Une performance exceptionnelle qu�il a peut-�tre encore du mal a r�aliser.
"Il va me falloir du temps pour redescendre sur terre. C'est incroyable, c'est un record du monde qui est tellement mythique, je vois bien qu'il s'est pass� quelque chose", confiait-il samedi au micro de BFMTV et RMC.
Forfait aux Championnats du monde en mars
C�est fatigu�, marchant avec des b�quilles mais heureux, que Renaud  Lavillenie est arriv� en fin de soir�e � son h�tel de Donestk. Il a  pass� deux heures � l�h�pital avec le m�decin de Serguei Bubka pour soigner  sa plaie � la cheville droite, apr�s une nouvelle tentative � 6 m�tres 21. Bless� � la r�ception d'un saut l'athl�te est forfait pour les Championnats du monde en salle d'athl�tisme, en mars, � Sopot en Pologne.
Les  interviews se sont enchain�es en compagnie du tsar d�chu de son record.  Dans le circuit, m�me les adversaires de Renaud Lavillenie saluent  l�exploit.
"Voir ce petit gars, saut� si haut, c�est absolument g�nial! Je pense qu�il peut �tre tr�s fier de lui et la France peut �tre fi�re de lui", s'enthousiasme Malt Mohr, vice-champion du monde en 2010.
M�me  sur un pied, Renaud Lavillenie a commenc� d�s samedi soir � f�ter son  entr�e dans l�histoire du sport. Il a y fort � parier que de nouveaux  records sont � venir.
A lire aussi
