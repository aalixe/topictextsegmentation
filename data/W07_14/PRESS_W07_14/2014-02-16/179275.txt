TITRE: Municipales : une cinquantaine de militants du PG d�missionnent en Aquitaine - Municipales 2014
DATE: 2014-02-16
URL: http://www.leparisien.fr/municipales-2014/en-regions/municipales-une-cinquantaine-de-militants-du-pg-demissionnent-en-aquitaine-14-02-2014-3591397.php
PRINCIPAL: 179271
TEXT:
R�agir
Ils d�noncent��l'absence d'�coute� et une �d�rive autoritaire� des instances r�gionales, comme nationales.�Une cinquantaine de militants aquitains du Parti de Gauche (PG) ont annonc� ce vendredi qu'ils quittaient le parti de Jean-Luc M�lenchon.
�Aujourd'hui, notre parti dit creuset�ne favorise plus l'expression collective, mais devient un appareil autoritaire qui ne nous permettra pas de construire un parti de masse�, �crivent les 56 m�contents�dans un communiqu�.
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Selon des militants, deux �v�nements sont � l'origine de cette d�mission : l'exclusion de l'un d'entre eux pressenti pour mener une liste aux municipales � P�rigueux (Dordogne), d�savou� par les instances nationales pour avoir pris l'ap�ritif au local d'un candidat de droite, en pr�sence de l'ancien ministre gaulliste Yves Gu�na et l'exclusion des membres du �comit� de Libourne (Gironde) apr�s �un d�saccord sur le choix du premier de liste PG�.
�Coalition de m�contentements�
�On se consid�re comme des lanceurs d'alerte. On d�nonce la d�rive autoritaire du parti, depuis un an. On ne le fait pas de gaiet� de coeur, on est malheureux de cette situation�, a ajout� Brigitte Duraffourg, ex-militante du  �comit� de Libourne. �Ce qui est d�solant, c'est que pendant ce temps, on ne parle pas de politique�, d�plore Fran�oise Lipchitz, d�missionnaire du comit� de P�rigueux.
Conseiller r�gional du Parti de Gauche, G�rard Boulanger a jug� cette d�cision �regrettable�. �C'est une coalition de m�contentements qui n'ont rien � voir les uns avec les autres et on ne construit rien l�-dessus�, a-t-il estim�. �Je n'ai jamais pens� que la d�mission �tait un mode d'action politique, il faut r�gler cela par la discussion en interne�, a-t-il ajout�.
En meeting le 11 f�vrier � Bergerac (Dordogne), Jean-Luc M�lenchon avait d�clar� � la presse : �Quand on est un homme public, on prend en charge une t�che publique, cela cr�� des devoirs. Donc on ne va pas se taper sur le ventre avec l'adversaire de droite, voil�, ce n'est pas permis au Parti de Gauche. Si des gens sont mal � l'aise, mais qu'ils aillent � un autre parti, je ne leur en veux pas.�
LeParisien.fr
