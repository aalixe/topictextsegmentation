TITRE: Les archives  volatilis�es  de l�Elys�e - Lib�ration
DATE: 2014-02-16
URL: http://www.liberation.fr/societe/2014/02/16/les-archives-volatilisees-de-l-elysee_980651
PRINCIPAL: 0
TEXT:
Les archives  volatilis�es  de l�Elys�e
16 f�vrier 2014 � 20:36
Lire sur le readerMode zen
HISTOIRE
Il n�y a plus trace des archives de Claude Gu�ant, bras droit de Nicolas Sarkozy durant quatre�ans, � l�Elys�e. Le�Monde a r�v�l� samedi une lettre de�Pierre-Ren� Lemas, l�actuel secr�taire g�n�ral de l�Elys�e de Fran�ois Hollande, adress�e, en mai, au juge Roger Le�Loire : �Le fonds d�archives papier de monsieur Claude Gu�ant n�a pas �t� revers� aux Archives nationales, et il n�en a pas �t� trouv� trace dans les locaux de la pr�sidence de la R�publique.� Le�magistrat du p�le financier enqu�te en effet sur le r�le jou� par Fran�ois P�rol, secr�taire g�n�ral adjoint de l�Elys�e charg� des questions �conomiques sous Sarkozy, dans la fusion des Caisses d��pargne et des Banques populaires. P�rol est suspect� d�avoir particip� aux n�gociations qui ont abouti � la cr�ation de la banque BPCE, avant d�en prendre la t�te en�2009. Ce qui constituerait une prise ill�gale d�int�r�t. �J�ai quitt� l�Elys�e � la fin f�vrier�2011, je ne me suis donc pas trouv� en situation de g�rer le suivi des archives de la pr�sidence apr�s l��lection pr�sidentielle�, a�r�torqu� Claude Gu�ant au Monde.
les plus partag�s
