TITRE: Giroud pr�sente ses excuses  - BFMTV.com
DATE: 2014-02-16
URL: http://www.bfmtv.com/sport/giroud-presente-excuses-712506.html
PRINCIPAL: 178811
TEXT:
r�agir
Le week-end dernier, Olivier Giroud avait fait la Une de l�Irish Sun pour avoir pass� la nuit pr�c�dant le match � Liverpool, qui avait tourn� � l�humiliation pour les Gunners (5-1), avec une mannequin, Celia Kay. L�attaquant fran�ais d�Arsenal s�excuse ce dimanche sur Twitter�: ��Je pr�sente mes excuses � mon �pouse, ma famille, mon entraineur, mes co�quipiers et les supporters d'Arsenal. Je dois maintenant me battre pour ma famille et pour mon club, pour obtenir leur pardon. Rien d'autre ne compte d�sormais.��
Toute l'actu Sport
