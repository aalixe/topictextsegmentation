TITRE: Faille de s�curit� pour Kickstarter, les utilisateurs invit�s � changer de mots de passe - JeuxOnLine
DATE: 2014-02-16
URL: http://www.jeuxonline.info/actualite/43380/faille-securite-kickstarter-utilisateurs-invites-changer-mots-passe
PRINCIPAL: 179576
TEXT:
Forums
Faille de s�curit� pour Kickstarter, les utilisateurs invit�s � changer de mots de passe
Suite au piratage d'une partie des donn�es personnelles des utilisateurs de sa plateforme, Kickstarter recommande � ses utilisateurs de modifier leurs mots de passe.
Par Agahnon 16/2/2014 � 14h22
9
La soci�t� Kickstarter proposant une plateforme de financement participatif via son site internet met cette fois en lumi�re un nouveau type d'activit�, celui du piratage informatique. Des pirates ont ainsi obtenu un acc�s � une partie de la base de donn�es de ses utilisateurs.
Le communiqu� de Kickstarter se veut rassurant en pr�cisant que les donn�es bancaires ne sont pas concern�es. Toutefois, il est possible que les pirates disposent de votre pseudonyme, adresses mail et postale, num�ro de t�l�phone et de votre mot de passe sous forme crypt�e. Il est donc vivement recommand� pour les utilisateurs de Kickstarter de modifier leur mot de passe actuel sur la plateforme, mais �galement sur les autres sites o� le mot de passe serait identique.
Cet �pisode illustre une nouvelle fois le probl�me des donn�es personnelles sur Internet, une marchandise de valeur pour de nombreux acteurs de la toile et m�me au-del�.
Pirat� Kickstarter, il y en a qui n'ont rien a faire le week end...
16/2/2014 � 15:21:41
Ou des contrats � honorer.
16/2/2014 � 15:27:00
Raaah !! Pirates � la noix.. !
16/2/2014 � 15:27:48
Bravo anonymoose, qui lutte encore et toujours contre le capitaliste !!!!
16/2/2014 � 15:39:08
The Londonian
Quelques commentaires auraient bien besoin d'�tre "pirat�s" l�!
Sinon, � force que mon email et num�ro de t�l�phone se retrouve dans des bases de donn�es vol�es, est-ce vraiment grave que �a arrive une fois de plus?
(enfin tant que les hash des mots de passe sont cr��s de fa�on s�curis�e)
16/2/2014 � 16:10:50 (modifi� le 16/2/2014 � 22:19:38)
Un voleur qui vol � un voleur c'est pas tellement vol� !
16/2/2014 � 18:36:15
Koren
Quelques commentaires auraient bien besoin d'�tre "pirat�s" l�!
Sinon, � force que mon email et num�ro de t�l�phone se retrouve dans des bases de donn�es vol�es, est-ce vraiment grave que �a arrive une fois de plus?  (enfin tant que les hash des mots de passe sont cr��s de fa�on s�curis�e)
Haha quand c'est hash� ouais...
16/2/2014 � 23:28:37
Et tous les piratages qu'on ne saura jamais... Difficile de battre le champion en titre : NSA.
17/2/2014 � 10:58:30
Associal
Et tous les piratages qu'on ne saura jamais... Difficile de battre le champion en titre : NSA.
Chuuut. Tu mets ta vie en danger l�... Ils te voient, tout comme David Vincent les as vu.... Si ils te lisent t'es fichu mec. Et ils te lisent, ils voient tout....
J'aime bien ce que vous prenez, ils vous en reste ou pas ?
18/2/2014 � 02:36:16
Vous devez vous identifier pour r�agir.
Nom d'utilisateur
