TITRE: Page Sotchi. Le relais dames en ski de fond, la d�ception de Marie Marchand-Arvier et les filles du snowboardcross - France 3 Alpes
DATE: 2014-02-16
URL: http://alpes.france3.fr/2014/02/15/page-sochi-le-relais-dames-en-ski-de-fond-la-deception-de-marie-marchand-arvier-et-les-filles-du-snowboardcross-416241.html
PRINCIPAL: 177982
TEXT:
+  petit
La surprise du jour aux jeux Olympiques de Sotchi a failli venir du relais dames de ski de fond qui a pris la 4e place alors que le compteur des Bleus est toujours bloqu� � quatre m�dailles.
Marie Marchand-Arvier, en Super-G, repr�sentait l'autre espoir du camp fran�ais. Mais le suspense a dur� 11 secondes, jusqu'� la sortie de piste de MMA.
Demain, c'est une �quipe f�minine de France d�termin�e qui va prendre le d�part de l'�preuve olympique de snowboardcross, o� les Bleues doivent d�fendre le bon bilan de�Vancouver, o��Deborah Anthonioz�avait termin� en argent. Mais Deborah se remet d'une saison compliqu�e (une seule course en 2012-2013) apr�s trois blessures entre mars 2012 et mars 2013. "Au final, je suis dans une situation confortable, car personne ne m'attend", estime la vice-championne olympique, �g�e de 35 ans.
Pour Nelly Moenne-Loccoz, le probl�me n'est pas physique. Celle qui est la meilleure Bleue depuis Vancouver (vice-championne du monde 2011, 2e de la Coupe du monde l'an pass�), a perdu ses rep�res techniques et ses sensations sur la neige et arrive un peu d�boussol�e. "Mais les Jeux sont une course � part", dit-elle.
Page r�alis�e par Daniel Despin, Didier Albrand�et Sophie Villatte
Page Sotchi. samedi 15 f�vrier
"En fait, les deux seules � peu pr�s valides sont Chlo� Trespeuch et Charlotte Banks", assure Nicolas Comte, l'entra�neur tricolore, en parlant des deux jeunettes du groupe, peut-�tre encore un peu "tendres" pour pr�tendre se bagarrer avec la Canadienne Dominique Maltais, qui a remport� les trois derniers globes de cristal.
