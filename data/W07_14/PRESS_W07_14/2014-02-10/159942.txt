TITRE: Super-combin� (D): Riesch de nouveau titr�e ! - JO 2014 - Sports.fr
DATE: 2014-02-10
URL: http://www.sports.fr/jo-2014/ski-alpin/scans/super-combine-d-riesch-de-nouveau-titree-!-1007922/
PRINCIPAL: 159938
TEXT:
10 février 2014 � 12h44
Mis à jour le
10 février 2014 � 12h51
Maria Hoefl-Riesch a conservé son titre de championne olympique de super-combiné ce lundi à Sotchi.
Quatre ans après son succès à Vancouver, l'Allemande a réalisé une manche de slalom superbe sur la piste de Rosa Khutor pour refaire son retard accumulé en descente (5e à 1"04) et remonter à la première place.
Elle devance au final l'Autrichienne Nicole Hosp de 40 centièmes et l'Américaine Julia Mancuso de 53 centièmes de seconde. Il s'agit de la troisième médaille d'or d'Hoefl-Riesch aux Jeux Olympiques puisqu'elle avait également remporté le slalom en 2010.
