TITRE: Des iPhone �quip�s de Flappy Bird revendus � prix d'or - 20minutes.fr
DATE: 2014-02-10
URL: http://www.20minutes.fr/high-tech/1294906-flappy-bird-des-internautes-revendent-a-prix-d-or-des-iphone-avec-le-jeu-installe
PRINCIPAL: 0
TEXT:
Captures d'�cran du jeu mobile Flappy bird, cr�� par un d�veloppeur ind�pendant. Flappy Bird
HIGH-TECH - Le cr�ateur de ce petit jeu � succ�s l'a retir� lundi des boutiques d'application...
Jusqu�o� ira la folie Flappy Bird?� Le jeu mobile star du moment que l�on adore d�tester , a disparu de l�App Store et du Google Play ce lundi au grand dam du public. Son cr�ateur, Dong Nguyen, un Vietnamien de 29 ans, a d�cid� de renvoyer le petit oiseau jaune dans son nid parce qu�il��n�en pouvait plus� . �Ceux qui n�avaient pas t�l�charg� l�application ne peuvent donc plus le faire aujourd�hui.
Une nouvelle qui a donn� des id�es � de nombreux internautes qui avaient pu installer le jeu avant qu�il ne soit retir� des boutiques d�applications:�revendre leur smartphone sur Internet en sp�cifiant��iPhone avec FLAPPY BIRD install�. Ce type d�annonce a envahi eBay . Certains proposent par exemple des Samsung Galaxy S3 et des iPhone 4 dot�s du fameux jeu � 2.000 dollars. Un internaute propose m�me un iPhone 5S avec Flappy Bird � 99.900 dollars et a b�n�fici� de 74 surench�res pour atteindre cette somme.
Le jeu mobile aux graphismes simplissimes d�cha�ne les passions depuis une semaine en raison de la difficult� de son gameplay.
La fr�n�sie autour du jeu aurait rapport� 50.000 dollars par jour � son cr�ateur, qui se dit compl�tement d�pass� par les �v�nements.
Ana�lle Grondin
