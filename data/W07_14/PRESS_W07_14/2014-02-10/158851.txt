TITRE: Le FCN avance dans le jeu mais recule au classement - 20minutes.fr
DATE: 2014-02-10
URL: http://www.20minutes.fr/nantes/1294242-fcn-avance-jeu-recule-classement
PRINCIPAL: 158843
TEXT:
Le FCN avance dans le jeu mais recule au classement Elsner / 20minutes
Football Les Canaris ont manqu� d'efficacit� (1-2) contre Lyon
Quatre points sur 21 possibles. Lors des sept derniers matchs de L1, le FC Nantes n'a quasiment rien engrang�. Si les Nantais ne s'�taient pas constitu�s un matelas confortable en 2013, il serait sans doute question aujourd'hui de crise. Cette derni�re attendra, car Nantes est bien cal� � la 9e place � douze points de Valenciennes, premier rel�gable.
Djordjevic craque,  le staff critique l'arbitrage
La crise attendra aussi, car le FC Nantes, apr�s avoir fait trembler le PSG en Coupe de la Ligue mardi, a fait vaciller (1-2) l'Olympique Lyonnais, dimanche, � la Beaujoire. �Parfois, on a gagn� des matchs, alors que ce n'�tait pas bien dans le jeu, l�, c'�tait plut�t bon.� L'analyse de Bruno Baronchelli, l'adjoint de Der Zakarian, est juste. Le FC Nantes a manqu� de r�alisme, de justesse dans les derniers gestes, mais a d�ploy� un jeu attractif, vari�. �Ce n'est pas un bon apr�s-midi, mais le contenu est bon, les joueurs ont fait ce qu'il fallait pour aller chercher la victoire�, estime Michel Der Zakarian. Men�s au score par un but de Lacazette en fin de premi�re p�riode, les Canaris n'ont pas abdiqu� et ont entretenu un projet de jeu coh�rent. A 2-0 (penalty de Gomis), m�me abn�gation, m�me coh�rence. Djordjevic a r�duit l'�cart avant de se faire expulser pour avoir fait semblant de balancer le ballon sur l'arbitre. ��Je fais un geste qu'il ne faut pas faire. C'est une faute professionnelle.� Le Serbe ne s'est pas r�fugi� derri�re les erreurs d'arbitrage. A l'inverse du staff nantais. �Je trouve qu'on est tr�s durs avec nous�, r�sume Baronchelli. Sur la d�faite de Paris, oui. Sur celle de Lyon, non. Le but de Djordjevic a �t� logiquement refus� pour une faute de Vizcarrondo au pr�alable. �Je ne veux pas passer pour une pleureuse !�, s'agace Riou. Nantes peut davantage verser une larme � cause de ses occasions manqu�es.
David Phelippeau
