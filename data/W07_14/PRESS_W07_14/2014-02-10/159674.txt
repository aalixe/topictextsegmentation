TITRE: Bose SoundLink III : nouvelle enceinte Bluetooth, 14 heures d'autonomie
DATE: 2014-02-10
URL: http://www.audiovideohd.fr/actualites/10178-Bose-SoundLink-III.html
PRINCIPAL: 159669
TEXT:
> AVHD > Actualit�s > Bose SoundLink III : nouvelle enceinte Bluetooth, 14 heures d'autonomie
Bose : nouvelle enceinte Bluetooth SoundLink III
Il y a quelques semaines, Bose lan�ait sa premi�re gamme d' enceintes multiroom Bose SoundTouch . Le fabricant poursuit sur sa lanc�e, et annonce la nouvelle enceinte sans fil SoundLink III, rempla�ante de la SoundLink II. Toujours compatible Bluetooth, elle adopte un nouveau design et une autonomie revue � la hausse.
Pour l'occasion, l'enceinte adopte des lignes plus en rondeurs et un look inspir� de la petite SoundLink Mini. Pour �viter les salissures et la poussi�re, Bose a �quip� son enceinte d'un panneau de commande en silicone et une grille m�tallique. En suppl�ment, des caches seront propos�s en option dans plusieurs coloris : gris, vert, bleu, orange et rose.
La recette reste la m�me que les pr�c�dentes SoundLink, �quip�e de quatre haut-parleurs et deux radiateurs passifs, l'enceinte permet d'�couter sa musique sans fil� stock�e sur un smartphone/tablette ou PC en Bluetooth (A2DP). Pour �viter d'avoir � effectuer l'appairage � chaque connexion, l'enceinte m�morise les six derniers appareils connect�s. Il reste possible de raccorder ses appareils � l'entr�e mini-jack 3,5 mm. Enfin, la batterie lithium-on offre une autonomie en hausse : 14 heures selon le fabricant.
L'enceinte Bose SoundLink III est disponible au prix public conseill� de 299,95 �, les caches couleurs en suppl�ment sont propos�s � 34,95 �.
