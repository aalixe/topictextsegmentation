TITRE: Mais que devient Beno�t XVI? - L'Express
DATE: 2014-02-10
URL: http://www.lexpress.fr/actualite/societe/religion/mais-que-devient-benoit-xvi_1322136.html
PRINCIPAL: 160235
TEXT:
Mais que devient Beno�t XVI?
Par LEXPRESS.fr, publi� le
10/02/2014 �  13:18
Il y a un an, Joseph Ratzinger annon�ait sa renonciation. Selon son ex-porte-parole, le pape �m�rite "ne vit pas isol� au Vatican". Il aurait m�me conserv� son sourire.�
Voter (0)
� � � �
Avec la cohabitation in�dite d'un pape en exercice (Fran�ois, � droite) et d'un pape �m�rite (Beno�t XVI, � gauche), certains avaient pari� sur de sourdes tensions.
afp.com/-
Coule-t-il une retraite dor�e? Un an apr�s l'annonce de sa renonciation , le pape �m�rite Beno�t XVI "ne vit pas isol�" au Vatican. Il donnerait m�me "une impression de grande s�r�nit� spirituelle", affirme son ancien porte-parole devenu celui de son successeur j�suite , Federico Lombardi . �
Joseph Ratzinger "vit d'une mani�re discr�te, sans dimension publique, mais cela ne veut pas dire qu'il vive isol�", assure � Radio Vatican le j�suite qui a �t� le fid�le porte-parole du pape allemand au milieu des temp�tes et pol�miques des huit ans de pontificat.�
Il r�pond � la correspondance qu'il re�oit�
Sa vie quotidienne dans l'ancien monast�re am�nag� pour lui sur la colline du Vatican est faite "de pri�re, de r�flexion, de lecture, d'�criture au sens o� il r�pond � la correspondance qu'il re�oit, de discussions, de rencontres avec des personnes qui lui sont proches, qu'il rencontre volontiers, avec qui il juge utile d'avoir un dialogue, qui lui demandent des conseils ou une proximit� spirituelle", a expliqu� le p�re Lombardi.�
"Une vie normale de rapports, dont celui avec son successeur, le pape Fran�ois , dont des moments de rencontre personnelle. (...) Et puis il y a les autres formes de contacts, le t�l�phone et les messages qui peuvent �tre envoy�s", a-t-il not�, en soulignant qu'il n'est pas dans une situation de cl�ture comme le serait un moine.�
"Il a conserv� son sourire"
Le porte-parole affirme que, pour lui-m�me "et pour l'Eglise, Beno�t XVI est le grand ancien, le sage, disons m�me saint (...) Il donne vraiment une impression de grande s�r�nit� spirituelle. Il a conserv� son sourire qui nous �tait habituel quand nous le rencontrions dans les beaux moments, et qui nous invite � aller de l'avant, avec confiance et esp�rance".�
Avec la cohabitation in�dite d'un pape en exercice et d'un pape �m�rite, certains avaient pari� sur de sourdes tensions et un poids suppl�mentaire inflig� dans l'exercice de sa charge au pape Fran�ois, mais les deux hommes semblent avoir trouv� un bon modus vivendi. Beno�t XVI avait promis qu'il ne s'immiscerait pas dans les affaires de son successeur �lu le 13 mars dernier . �
