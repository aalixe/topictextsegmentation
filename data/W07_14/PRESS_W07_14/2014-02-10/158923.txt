TITRE: Point de vue. Pendant Sotchi, soutenons l'Ukraine
DATE: 2014-02-10
URL: http://www.ouest-france.fr/point-de-vue-pendant-sotchi-soutenons-lukraine-1921435
PRINCIPAL: 158920
TEXT:
Point de vue. Pendant Sotchi, soutenons l'Ukraine
Russie -
Christian Lequesne (*)
� L'Europe consacre un Poutine triomphant � Sotchi et plus personne ne pense � nous. �
Ainsi s'expriment aujourd'hui les opposants au r�gime corrompu de Ianoukovitch en Ukraine. Leur mobilisation m�riterait pourtant toute notre attention.
Dans l'Union europ�enne, nombreux sont ceux qui affirment que l'Europe est anti-d�mocratique et qu'elle porte atteinte � nos beaux �tats. � l'inverse, � Kiev, ce sont des drapeaux europ�ens qui flottent sur les barricades de la place Ma�dan. Pour les Ukrainiens, l'Union symbolise des valeurs auxquelles ils aspirent : la d�mocratie, l'�tat de droit, le respect des lois.
Lamentation chez nous � propos de l'Europe, espoir en Ukraine.
Le soutien de l'Union aux manifestants ukrainiens devrait �tre plus profond que des d�clarations demandant au pr�sident Ianoukovitch de dialoguer avec l'opposition. On aimerait entendre les chefs d'�tat europ�ens dire � Vladimir Poutine qu'ils ne sont pas dupes de sa complicit� avec le r�gime oligarchique de Ianoukovitch et que ses remarques sur la soi-disant ing�rence des Europ�ens en Ukraine les laissent froids.
Mais dire la v�rit� � Poutine, cela co�te aux leaders des grands pays de l'Europe. Non seulement parce qu'ils veulent m�nager leurs int�r�ts �conomiques, mais aussi parce qu'ils sont nourris de cette id�e ancienne qu'il ne faut jamais � d�stabiliser � la Russie.
Il n'y a gu�re que les petits pays de l'Union europ�enne, ceux qui ont souffert de l'annexion par la Russie, comme les Lituaniens, � tenir un langage de v�rit� � l'�gard de Moscou. Mais leur voix p�se peu.
Une bataille pour l'Europe
C'est toute l'Europe qui devrait interdire � des apparatchiks du r�gime Ianoukovitch de trouver refuge au sein de l'Union ou d'y �tre accueillis officiellement. Le maire de Prague, Tomas Hudecek, a ainsi refus� avec raison de prendre part � la visite officielle de Ianoukovitch en R�publique tch�que. De m�me les Autrichiens ont eu un d�bat tr�s l�gitime sur la pr�sence � Vienne du Premier ministre ukrainien d�missionnaire, Mykola Azarov, qui a particip� � la r�pression des manifestations. Sa place est � Kiev o� il est reparti.
Il convient d'adresser un nouveau message fort � l'opposition ukrainienne sur la vocation europ�enne de ce pays, apr�s le refus par Ianoukovitch de signer l'accord d'association avec l'UE. Mais les Europ�ens h�sitent, car cela pourrait ouvrir � terme la voie � un nouvel �largissement de l'Union europ�enne dont personne ne veut.
En France, les partisans d'une petite Europe resserr�e � l'ouest ont toujours �t� nombreux. Ils sont souvent caricaturaux dans leur mani�re d'opposer l'�largissement � l'approfondissement de l'Union. Si cette derni�re stagne effectivement depuis dix ans, ce n'est pas du tout � cause de l'�largissement aux pays d'Europe centrale et orientale.
La vraie raison, c'est l'absence d'un soutien suffisant des soci�t�s, en particulier dans les vieux �tats membres de l'Union europ�enne, comme la France. Les partisans de la petite Europe n'ont finalement jamais admis que le Mur de Berlin �tait tomb�. La bataille pour l'Europe des Ukrainiens fait partie des effets - certes � retardement - de ce mouvement de la fin du XXe si�cle en faveur de la libert�. Leur combat m�rite d'�tre mieux soutenu.
�
(*) Professeur � Sciences Po, directeur de recherche au CERI (Centre d'�tudes des relations internationales).
Lire aussi
