TITRE: Tous � poil ! : qu'y a-t-il dans le m�chant livre d�nonc� par Jean-Fran�ois Cop� ? - Terrafemina
DATE: 2014-02-10
URL: http://www.terrafemina.com/societe/societe/articles/37748-tous-a-poil-quy-a-t-il-dans-le-mechant-livre-denonce-par-jean-francois-cope-.html
PRINCIPAL: 0
TEXT:
Publi� le 10 f�vrier 2014
Tous � poil ! : qu'y-a-t-il dans le m�chant livre d�nonc� par Jean-Fran�ois Cop�
��RTL
�
�
L'album jeunesse � Tous � poil ! � a h�riss� le poil de Jean-Fran�ois Cop� qui a violemment d�nonc� dimanche sur RTL le fait qu'il soit � recommand� aux enseignants �. En pleines controverses et incompr�hensions sur la th�orie du genre, que vient faire ce livre d'�ducation � la diversit� des corps dans le d�bat ?
Jean-Fran�ois Cop� s�est vivement attaqu� � un livre pour enfants lors du Grand Jury RTL dimanche 9 f�vrier. Intitul� Tous � poil !, cet album figure dans une liste d�ouvrages conseill�s aux enseignants pour lutter contre les st�r�otypes � l��cole : � Quand j�ai vu �a, mon sang n�a fait qu�un tour �, a-t-il d�clar�. � On ne sait pas s�il faut sourire, mais comme c�est nos enfants, on n�a pas envie de sourire. ��poil le b�b�, � poil les voisins� (�.) ��poil la ma�tresse, Vous voyez, c�est bien pour l�autorit� des professeurs. �
Un livre sur la tol�rance
Le livre �dit� chez Rouergue est un album r�serv� aux enfants de plus de huit ans, et utilise un ton d�cal� pour parler de la nudit�. En d�shabillant le chien, le PDG, la boulang�re ou la mamie, l�auteure, Claire Franek cherche � d�dramatiser la nudit� et � montrer la diversit� des corps : gros, petits, vieux ou jeunes. Une d�marche plut�t positive pour apprendre la tol�rance � l��cole primaire, qui n�a pas grand-chose � voir avec la � th�orie du genre � , critiqu�e derni�rement par les partisans de la Manif pour tous, except� peut-�tre que la finalit� est la m�me : des enfants qui se comprennent et s�acceptent mieux.
