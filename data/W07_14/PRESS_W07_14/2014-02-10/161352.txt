TITRE: Les plus vieilles empreintes de pas humaines trouv�es hors d'Afrique
DATE: 2014-02-10
URL: http://www.maxisciences.com/empreinte/les-plus-vieilles-empreintes-de-pas-humaines-trouvees-hors-d-039-afrique_art31961.html
PRINCIPAL: 161285
TEXT:
Publi� par �meline Ferard , le
10 f�vrier 2014
Des scientifiques ont d�couvert les plus vieilles empreintes de pas laiss�es par des humains hors d'Afrique. D�nich�es sur la c�te anglaise, elles �taient incrust�es dans des s�diments �g�s de 800.000 ans.
Il y a des milliers d'ann�es, des anc�tres de l'homme ont foul� une plage boueuse au Royaume-Uni, et des scientifiques viennent de retrouver leurs traces ! C'est � Happisburgh situ� dans le sud de l'Angleterre que l'�quipe dirig�e par la Queen Mary University of London, le British Museum et le Natural History Museum, a r�alis� cette d�couverte, totalement par hasard. "Nous les avons trouv�es par pure chance en mai l'an dernier", explique Nicholas Ashton du British Museum.
Alors que l'�quipe se trouvait sur le site, l'un des scientifiques, Martin Bates, a rep�r� des sortes de creux dans les s�diments durcis, � la base de la falaise dont la surface a �t� �rod�e au fil des ann�es. Le sp�cialiste a alors dit � ses coll�gues que ces creux ressemblaient � des empreintes de pas. "Au d�but, nous n'�tions pas s�rs de ce que nous voyions", a racont� le Dr Ashton "mais au fur et � mesure que nous avons retir� les restes de sable et �pong� l'eau, il �tait �vident que les trous ressemblaient � des empreintes".�
"Nous devions enregistrer la surface le plus vite possible", avant que la mar�e n'�rode compl�tement les creux et que tout disparaisse, a ajout� le Dr Ashton repris par Science Daily. L'�quipe a alors utilis� la photogramm�trie pour cr�er un enregistrement durable et des images 3D de ce � quoi ressemblait la sc�ne trouv�e dans les s�diments. A la fin du mois de mai, les traces avaient �t� compl�tement effac�es par la mar�e.
Vieilles de 800.000 ans
Heureusement, l'analyse des images captur�es a permis de confirmer qu'il s'agissait bel et bien d'empreintes de pas d'origine humaine. Une d�couverte importante et rare. "Nous savons que les s�diments de Happisburgh sont �g�s de 800.000 ans", a d�clar� Ashton. Aussi, les empreintes incrust�es � l'int�rieur pourraient �tre tout aussi vieilles et devenir les plus anciennes jamais retrouv�es hors d'Afrique, selon l'�tude publi�e dans la revue PLoS ONE.
Trouver des empreintes hors du continent africain est extr�mement rare, en trouver des aussi anciennes l'est encore plus. Les plus vieilles traces d�couvertes remontent � 3,5 millions d'ann�es et ont �t� trouv�es � Laetoli en Tanzanie. D'autres mises au jour � Ileret et Koobi Fora au Kenya sont �g�es d'environ 1,5 million d'ann�es. Seules ses deux ensembles d'empreintes sont connus pour �tre plus vieux que celles de Happisburgh.
Un groupe d'enfants et d'adultes
Gr�ce aux images, les chercheurs ont mesur� la largeur et la longueur de chaque empreinte et ont m�me r�ussi � estimer la taille et le poids des individus qui les ont laiss�es. D'apr�s eux, il s'agissait d'un groupe d'enfants et d'adultes comptant au moins cinq individus diff�rents dont la taille ne d�passait pas 1,70m. Si les chercheurs ignorent ce que le groupe faisait exactement, l'orientation des empreintes sugg�re qu'il voyageait vers le sud le long de l'estuaire.
A l'�poque, la vall�e �tait riche en cerfs, bisons, mammouths, hippopotames et rhinoc�ros. D'apr�s eux, les terres fournissaient donc une zone importante de ressources pour trouver des animaux, des plantes comestibles, des poissons et des crustac�s. Au vu des caract�ristiques des empreintes et de la r�gion, les scientifiques pensent qu'il pourrait s'agir d'un anc�tre connu sous le nom d'Homo antecessor.
La dispersion des anc�tres de l'homme moderne
Descendant d'Homo ergaster, cette esp�ce vivait en Europe il y a plus d'un million d'ann�es et se d�pla�ait sur ses deux jambes, a comment� Ashton ajoutant que leur cerveau �tait plus petit que le n�tre. Mais "nous en savons tr�s peu sur les personnes qui ont laiss� ces empreintes", a indiqu� le chercheur repris par le National Geographic.
La d�couverte des empreintes confirme que des hominid�s �taient bien pr�sents � cette �poque dans le Nord de l'Europe. Elle pourrait ainsi permettre de mieux comprendre comment les anc�tres de l'homme moderne ont trac� leur chemin dans l'ancien monde.�
(cr�dits photo : Simon Parfitt - Natural History Museum)
Suivez-nous sur Facebook
Vous �tes d�j� abonn� ? Ne plus afficher
Suivre
