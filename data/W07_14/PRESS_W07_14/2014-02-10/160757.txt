TITRE: Messi replace le Bar�a - Espagne - Football - Sport.fr
DATE: 2014-02-10
URL: http://www.sport.fr/football/messi-replace-le-barca-339141.shtm
PRINCIPAL: 160752
TEXT:
Messi replace le Bar�a
Lundi 10 f�vrier 2014 - 13:00
Avec sa victoire sur la pelouse du FC S�ville (1-4), lors de la 25e journ�e de Liga, le Bar�a reprend la t�te du championnat d'Espagne. Auteur d'un doubl�, Lionel Messi semble avoir retrouv� ses moyens.
Lionel Messi se sent mieux et l?a fait savoir sur la pelouse du FC S�ville , lors de la victoire facile du Bar�a (1-4). Auteur d?un doubl�, l?Argentin a permis aux Blaugrana de prendre l?avantage et de faire le break. Sur son premier but, la "Pulga" se levait le ballon, sur un service de Pedro, pour le reprendre de vol�e et tromper Beto (1-2, 44e). Sur son deuxi�me, c?est d?un frappe enroul�e aux 20 m�tres que l?Argentin signait son retour au plus haut niveau (1-3, 56e). Deux buts qui ont facilit� un match pourtant mal parti pour les Catalans.
Car ce sont bien les locaux qui ouvert la marque par Moreno. De quoi entamer le capital confiance des hommes de Gerardo Martino , qui restaient sur une d�faite face � Valence la semaine pass�e. Et si le Bar�a s?�tait impos� en milieu de semaine contre la Real Sociedad en Coupe du Roi (2-0), c?est sans briller et sous les sifflets des socios barcelonais. Mais aux final, les visiteurs ne doutaient que 20 minutes, le temps pour Alexis Sanchez d?�galiser d?une t�te retourn�e (1-1, 34e).
Les S�villans par n?�taient pas loin de r�ussir � prendre l?avantage par Vitolo (50e) et Gameiro (54e) mais Vald�s parvenaient � chaque fois � sauver son �quipe. C?�tait ensuite l?heure de Lionel Messi. En fin de rencontre, Fabregas prenait aussi part � la f�te d?un petit ballon piqu� (1-4, 88e). Ce r�sultat permet � Barcelone de reprendre son fauteuil de leader, avec le m�me nombre de points que l?Atletico et le Real, mais avec une meilleure diff�rence de buts.
Dossiers associ�s
