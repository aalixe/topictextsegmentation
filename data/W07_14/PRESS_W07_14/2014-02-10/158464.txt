TITRE: R�f�rendum suisse: l'UE s'inqui�te, l'extr�me droite se r�jouit - LExpress.fr
DATE: 2014-02-10
URL: http://www.lexpress.fr/actualites/1/monde/referendum-suisse-l-ue-s-inquiete-l-extreme-droite-se-rejouit_1321912.html
PRINCIPAL: 0
TEXT:
R�f�rendum suisse: l'UE s'inqui�te, l'extr�me droite se r�jouit
Par AFP, publi� le
09/02/2014 � 21:43
, mis � jour � 23:57
Paris - Bruxelles a annonc� un r�examen des relations avec la Suisse apr�s sa d�cision de limiter l'immigration, un choix salu� par l'extr�me droite au risque d'exacerber les tensions � quatre mois des �lections europ�ennes.
Bruxelles a annonc� un r�examen des relations avec la Suisse apr�s sa d�cision de limiter l'immigration, un choix salu� par l'extr�me droite au risque d'exacerber les tensions � quatre mois des �lections europ�ennes.
afp.com/Arnd Wiegmann
La Commission europ�enne a ainsi fait part de son intention d'examiner "les implications (du r�f�rendum) sur l'ensemble des relations entre l'UE et la Suisse" tout en regrettant la d�cision des �lecteurs suisses qui ont approuv� dimanche � une courte majorit� de 50,3% une limitation de l'immigration.�
Le vote des Suisses risque effectivement de remettre en cause de nombreux accords entre la Suisse et l'Union europ�enne. Ainsi, "l'accord sur la libre-circulation des personnes avec l'UE est remis en cause", a soulign� le pr�sident actuel de la Conf�d�ration helv�tique Didier Burkhalter qui a toutefois tenu � rappeler que son pays dispose d'un "syst�me de d�mocratie directe" et que "le peuple s'exprime".�
"C'est un mauvais r�sultat, la Suisse a besoin de bonnes relations avec l'UE", a comment� le syndicaliste et d�put� socialiste Paul Rechsteiner, dont le parti a parl� de "d�faite".�
Le ministre allemand des Finances, Wolfgang Sch�uble, le plus europ�en des ministres d'Angela Merkel, a "regrett�" le vote suisse estimant qu'il allait " cr�er de nombreuses difficult�s pour la Suisse dans beaucoup de domaines."�
"Il faut que les responsables en Suisse en prennent bien conscience", a-t-il martel�, admettant toutefois que "dans ce monde de la mondialisation, les gens ressentent un malaise grandissant vis-�-vis de la libert� compl�te d'installation".�
A l'oppos�, plusieurs partis europ�ens d'extr�me droite se sont f�licit� du choix des �lecteurs suisses.�
En France, le Front national, pr�sid� par Marine Le Pen, a salu� "la lucidit� du peuple suisse" tandis que le chef du parti britannique populiste Ukip, Nigel Farage a parl� d'une "merveilleuse nouvelle pour les amoureux de la libert� et de la souverainet� nationale en Europe".�
En Autriche, le dirigeant du parti d'extr�me droite FP� Heinz Christian Strache, a salu� un "grand succ�s", affirmant qu'"en Autriche aussi, la majorit� des personnes se prononcerait en faveur d'une limitation de l'immigration".�
En Italie, le chef de la Ligue du nord Matteo Salvini, consid�r�e comme populiste, a annonc� sur twitter l'intention de son mouvement de r�clamer un r�f�rendum pour limiter �galement l'immigration en Italie.�
"Ici chez nous, il n'y a plus de place ni de travail pour les immigr�s. Il faut donner la priorit� � nos millions de ch�meurs, de retrait�s sans droits et de jeunes", a d�clar� Massimo Bitonci, chef de file de la Ligue au S�nat.�
Mais ces d�clarations ne seront pas forc�ment suivies d'effet. La Ligue du nord ne repr�sente plus qu'environ 4% des �lecteurs dans les sondages et pour convoquer un r�f�rendum en Italie, il faut recueillir 500.000 signatures tandis que les r�f�rendums sont uniquement abrogatifs de lois existantes.�
L'ancien chef de la Ligue et actuel pr�sident de la r�gion Lombardie, Roberto Maroni, avait d'ailleurs critiqu� le r�f�rendum suisse, estimant qu'il risquait de p�naliser les nombreux travailleurs frontaliers italiens, qui tous les jours passent la fronti�re pour aller travailler dans le Tessin voisin de la Lombardie.�
"Je ne vois pas comment les Suisses feraient sans les travailleurs italiens", avait-il ajout�.�
Dans le m�me ordre d'id�e, le pr�sident du comit� de d�fense des travailleurs frontaliers (CDTF) du Haut-Rhin, d�partement fran�ais frontalier de la Suisse, a fait valoir que la Suisse pouvait difficilement se passer des travailleurs �trangers.�
De nombreux travailleurs frontaliers sont recherch�s en Suisse parce qu'ils ont des comp�tences "tr�s sp�cifiques", que ce soit dans les secteurs de la recherche, de l'artisanat, du BTP ou du tourisme, a rappel� Jean-Luc Johaneck, interrog� par l'AFP.�
Par
