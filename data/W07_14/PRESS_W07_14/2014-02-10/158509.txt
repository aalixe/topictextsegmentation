TITRE: 3 r�flexes pour �viter la gastro-ent�rite et la grippe - La Parisienne
DATE: 2014-02-10
URL: http://www.leparisien.fr/laparisienne/sante/3-reflexes-pour-eviter-la-gastro-enterite-et-la-grippe-07-02-2014-3571995.php
PRINCIPAL: 0
TEXT:
3 r�flexes pour �viter la gastro-ent�rite et la grippe
7 f�vr. 2014, 16h29
Malade? Evitez les lieux fr�quent�s pour ne pas contaminer les autres
Mon activit� Vos amis peuvent maintenant voir cette activit�
Tweeter
La gastro-ent�rite et la grippe s�vissent actuellement dans tout l'Hexagone et de nouveaux cas devraient se d�clarer prochainement. Pour �viter la contamination, l'Institut national de pr�vention et d' �ducation pour la sant� (INPES) rappelle ce vendredi 7 f�vrier les bons r�flexes pour ne pas �tre malade.�
1/ Se laver souvent les mains
Il faut se laver r�guli�rement les mains, apr�s avoir pris les transports en commun, avant de cuisinier , de passer � table, de s'occuper d'enfants. Et il est essentiel de proc�der ainsi apr�s chaque passage aux toilettes.�
2/ Eviter les contacts directs et indirects avec les malades
Pour �tre s�r de ne pas souffrir de la gastro-ent�rite ou de la grippe, il est pr�f�rable de ne pas entrer en contact avec une personne infect�e.�
3/ Les malades doivent porter un masque
Les personnes infect�es par la gastro-ent�rite ou la grippe doivent porter un masque s'ils n'ont pas d'autre choix que de sortir. Ceci dit, il est bien s�r recommand� d'�viter les lieux fr�quent�s pour ne pas contaminer les autres. Lorsque les malades �ternuent ou toussent, ces derniers doivent se couvrir la bouche avec le coude, le bras ou la manche, et non pas la main.�
