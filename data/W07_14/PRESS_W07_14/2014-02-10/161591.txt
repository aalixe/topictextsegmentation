TITRE: A la Une | Les salariés de Chapitre vont occuper leur magasin à Colmar, rue des Têtes
DATE: 2014-02-10
URL: http://www.dna.fr/actualite/2014/02/10/les-salaries-de-chapitre-vont-occuper-leur-magasin-a-colmar-rue-des-tetes
PRINCIPAL: 161589
TEXT:
Les salariés de Chapitre vont occuper leur magasin à Colmar, rue des Têtes
- Publié le 10/02/2014
Après la décision de reprise et les restructurations Les salariés de Chapitre vont occuper leur magasin à Colmar, rue des Têtes
Les salariés de la librairie Chapitre de la rue des Têtes à Colmar ont décidé ce soir d'occuper leur magasin qui avait exceptionnellement ouvert ses portes aujourd'hui.
Previous Next
Ce magasin, l'un des deux de l'enseigne Chapitre à Colmar (l'autre est situé place de la Cathédrale) est condamné dans le cadre du plan de restructuration soumis par le repreneur au tribunal de commerce de Paris.
Ce soir, les salariés ont indiqué qu'ils allaient se relayer pour occuper les lieux dans le but de faire pression dans le cadre des négociations sur les idemnités de licenciement.
C'est la CGT qui a lancé ce matin un appel aux salariés des magasins condamnés pour qu'ils occupent leurs commerces.
La CGT des Librairies Chapitre a appelé ce matin les salariés «à occuper de façon illimitée» les magasins non repris pour obtenir «des garanties d�??indemnités décentes».
Lire l'article d'Isabelle Nassoy à paraître demain dans les DNA
Articles Associ�s
