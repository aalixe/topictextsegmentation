TITRE: Pacte de responsabilit�: Montebourg r�clame 1,65 million d'emplois - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/economie/montebourg-reclame-1-65-millions-demplois-aux-entreprises-706568.html
PRINCIPAL: 0
TEXT:
r�agir
Arnaud Montebourg fait une nouvelle fois entendre sa diff�rence. Dans une interview aux Echos de ce 10 f�vrier, le ministre du Redressement productif tacle de nouveau les entreprises � propos des contreparties au pacte de responsabilit� alors que le PS se r�unit en s�minaire ce lundi pour �voquer justement ce sujet br�lant des contreparties. Et il s'en prend aussi � l'euro fort , coupable selon lui de casser la reprise en France.
La sanction ? Le regard de la nation !
"J'observe que sur ces cinq prochaines ann�es, si la conjoncture est celle que nous pr�voyons, la France cr�era environ un million d'emplois. Dans le m�me temps , nous aurons aussi 650.000 nouvaux arrivants sur le march� du travail. Du coup, le ch�mage ne baissera que d'un point", explique-t-il.
"Pour moi, l'objectif en termes de contreparties serait au minimum la cr�ation de 1,65 million d'emplois de mani�re � ce que l'effort des entreprises puisse contribuer � une baisse de 2 � 3 points du ch�mage", affirme-t-il. Voici quelques semaines, il parlait cependant de 2 millions d'emplois !
A propos d'�ventuelles sanctions en cas d'objectif non atteint, Arnaud Montebourg se veut prudent : " la sanction, ce sera le regard de la nation dans toutes ses composantes, y compris les salari�s dans l'entreprise auxquels des promesses auront �t� faites. Et c'est d�j� pas mal".
L'euro p�nalise l'industrie
Concernant l'euro fort, l'une des ses b�tes noires, le ministre du Redressement productif estime que "nous avons la zone la plus d�pressive au monde et la monnaie qui s'appr�cie le plus au monde. Cette situation est ubuesque".�
Il rappelle que, selon la direction du Tr�sor, "une d�pr�ciation (de l'euro ndlr) de 10% permettrait d'accro�tre notre taux de croissance de 1,2%. Cela cr�erait 150.000 emplois, am�liorerait la balance commerciale et r�duirait notre d�ficit public de 12 milliards". "L'euro p�nalise l'industrie au lieu de la soutenir dans la grave crise de comp�titivit� que nous traversons". Le ministre r�clame donc une reprise en main de la politique de change par les Etats europ�ens.
Querelles de famille ind�centes chez Peugeot
Arnaud Montebourg revient enfin sur le dossier PSA et la future ouverture de capital � Dongfeng et l'Etat fran�ais. " Dans cette affaire, l'Etat prend sa part de responsabilit�, qui n'est pas excessive mais qui est d�cisive".
A propos des r�ticences de certains membres de la famille Peugeot � laisser ouvrir le capital, il lance: "les querelles ind�centes au sein de la famille Peugeot ne peuvent plus avoir droit de cit� tant l'enjeu du rebond de PSA est consid�rable".
A lire aussi
