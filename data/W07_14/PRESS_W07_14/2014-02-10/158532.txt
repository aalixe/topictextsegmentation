TITRE: Bosnie: la pauvreté et le chômage plongent le pays dans la violence - 11 février 2014 - Le Nouvel Observateur
DATE: 2014-02-10
URL: http://tempsreel.nouvelobs.com/monde/20140209.AFP9653/la-pauvrete-et-le-chomage-ont-plonge-la-bosnie-dans-la-violence.html
PRINCIPAL: 0
TEXT:
Actualité > Société > Bosnie: la pauvreté et le chômage plongent le pays dans la violence
Bosnie: la pauvreté et le chômage plongent le pays dans la violence
Publié le 09-02-2014 à 16h01
Mis à jour le 11-02-2014 à 10h40
A+ A-
L'impuissance du pouvoir en Bosnie à redresser l'économie et à répondre à un appauvrissement croissant de la population, alors que près d'un habitant sur deux est au chômage, a provoqué des manifestations d'une violence inconnue dans ce pays depuis la guerre intercommunautaire des années 1990. (c) Afp
Sarajevo (AFP) - L'incapacité du pouvoir à relancer l'économie et à répondre à un appauvrissement croissant de la population en Bosnie, où près d'une personne sur deux est au chômage, a provoqué des manifestations d'une violence inconnue dans ce pays depuis la guerre intercommunautaire des années 1990.
"Ce qui s'est passé ces derniers jours est l'explosion du mécontentement et de la colère qui se sont accumulés au cours des dernières années", dit à l'AFP l'analyste Srecko Latal.
"Cette exaspération est liée à une situation politique absolument chaotique et à une situation économique et sociale qui s'est sérieusement aggravée", poursuit-il.
Des législatives en octobre
Pour calmer la rue, au cinquième jour des manifestations , une des principales formations membres de la coalition au pouvoir, le Parti social -démocrate (SDP), ainsi que le membre musulman de la présidence tripartite de la Bosnie, Bakir Izetbegovic, ont appelé à l'organisation d'élections anticipées.
Des législatives doivent normalement avoir lieu en octobre.
Composée depuis la fin du conflit (1992-95) de deux entités, l'une serbe et l'autre croato-musulmane, la Bosnie s'est vu imposer par l'accord de paix de Dayton (Etats-Unis) des institutions politiques extrêmement compliquées et au sein desquelles le pouvoir est partagé entre Serbes, Croates et Musulmans.
La Fédération croato-musulmane est à son tour formée de dix cantons, chacun avec son propre gouvernement. Cette administration est pléthorique avec quelque 180.000 salariés pour un pays de 3,8 millions d'habitants. Les administrations régionales consacrent en moyenne 65% de leur budget au versement des salaires.
La moindre décision requérant l'accord des dirigeants des trois communautés, des querelles politiciennes font que les institutions centrales du pays sont en permanence en crise et en situation de blocage.
La communauté internationale, toujours représentée en Bosnie par un Haut représentant disposant de pouvoirs discrétionnaires - actuellement le diplomate autrichien Valentin Inzko -, a néanmoins décidé depuis 2006 de progressivement mettre fin à ses interventions dans la vie politique.
Auparavant, les Hauts représentants imposaient des lois ou limogeaient des élus et des dirigeants politiques sans état d'âme pour faire fonctionner le pays.
La Bosnie s'est ainsi retrouvée à la traîne de tous les autres Etats des Balkans dans son rapprochement avec l'Union européenne.
M. Inzko a déclaré dans une interview publiée dimanche par le quotidien autrichien Kurier, que la communauté internationale devrait, en cas d'escalade de la situation, "peut-être penser à envoyer (davantage) des troupes de l'UE", contre près de 600 soldats actuellement déployés en Bosnie.
Parti de Tuzla, jadis la plus importante ville industrielle de Bosnie, où des milliers de salariés se sont retrouvés au chômage à cause des échecs en série des privatisations de leurs usines, le mouvement a gagné la capitale et plusieurs autres grandes cités.
Vendredi, des immeubles abritant des institutions régionales et municipales à Sarajevo, Tuzla (nord-est), Bihac (nord-ouest), Zenica (centre) et Mostar (sud) ont été dévastés ou incendiés par des hooligans qui ont rejoint des milliers de manifestants descendus dans la rue pour protester contre la pauvreté et réclamer la démission des autorités.
Salaire moyen de 420 euros
Les chefs des gouvernements régionaux de Tuzla, Zenica et Sarajevo ont démissionné sous la pression de la rue.
Plusieurs centaines de policiers et de manifestants ont été blessés.
Dimanche, des centaines de personnes ont manifesté sans incidents devant l'immeuble de la présidence, incendié vendredi, pour réclamer la démission des responsables au pouvoir.
Les Bosniens ont été choqués par les violences, mais certains analystes mettaient déjà en garde contre le risque d'une explosion du mécontentement populaire.
Le taux de chômage est de 44%, selon l'Agence nationale des statistiques. Néanmoins, la banque centrale évalue ce taux à 27,5% en raison du nombre très élevé des personnes employées au noir.
"Le taux élevé de chômage (...) continue de représenter une menace et doit être combattu pour assurer un avenir paisible et prospère à la Bosnie", a mis en garde en janvier la directrice de la Banque mondiale pour l'Europe du Sud-Est, Ellen Goldstein.
Un Bosnien sur cinq vit dans la pauvreté, selon les statistiques officielles. Le salaire mensuel moyen est de 420 euros. Après un recul de 0,5% en 2012, le PIB a crû d'environ 1% en 2013.
Partager
