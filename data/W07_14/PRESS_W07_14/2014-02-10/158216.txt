TITRE: "Chromebox for Meetings" : Google lance un pack de vid�oconf�rence pro pour les PME
DATE: 2014-02-10
URL: http://www.commentcamarche.net/news/5863987-chromebox-for-meetings-google-lance-un-pack-de-videoconference-pro-pour-les-pme
PRINCIPAL: 158215
TEXT:
"Chromebox for Meetings" : Google lance un pack de vid�oconf�rence pro pour les PME
ldelvalle le vendredi  7 f�vrier 2014 � 10:31:24
La firme de Mountain View commercialise aux Etats-Unis un pack de services et d'�quipement facilitant la mise en place de la visioconf�rence multi-participants en entreprise. Une solution baptis�e "Chromebox for Meetings", qui devrait �tre disponible en France dans les semaines � venir.
Ce pack qui s'adresse aux entreprises (PME) n'ayant pas encore install� de dispositif de vid�oconf�rence , pour des raisons de co�t d'infrastructure notamment, est propos�e en vente aux Etats-Unis pour 999$ pour la premi�re ann�e de souscription, puis 250 $ les ann�es suivantes.
Comprenant  une Chromebox, une webcam Logitech haute-d�finition et combin� avec la technologie de visioconf�rence "Hangouts" de Google, il entend concurrencer les offres plus haut-de-gamme propos�es par Cisco et Polycom notamment, afin de r�pondre aux besoins des entreprises confront�es aux probl�matiques de communication avec leurs employ�s nomades.
Une solution d�ploy�e en quelques minutes
Concr�tement, les entreprises ont la possibilit� de d�ployer "en quelques minutes" une Chromebox -ordinateur compact sous Chrome OS propos� par Asus - dans une salle de r�union, leur permettant de g�rer leurs r�unions virtuelles via un console cloud d�di�e. Les utilisateurs peuvent ensuite partager en WiFi l'�cran de leur terminal avec l'�cran principal.
Ce pack de services s'int�gre naturellement avec l'�cosyst�me Google Apps, pour g�rer les invitations et ajouter les participants aux r�unions depuis l'application Agenda (Google Calendar).
Il permet d'organiser des r�unions regroupant jusqu'� 15 participants, soit 5 de plus que ce permet la version Freemium de Google Hangouts.
En savoir plus
