TITRE: Hollande dans la Silicon Valley: les expatri�s attendent un choc fructueux
DATE: 2014-02-10
URL: http://www.leparisien.fr/high-tech/hollande-dans-la-silicon-valley-les-expatries-attendent-un-choc-fructueux-10-02-2014-3576887.php
PRINCIPAL: 159409
TEXT:
Hollande dans la Silicon Valley: les expatri�s attendent un choc fructueux
Publi� le 10.02.2014, 09h27
Tweeter
La visite de Fran�ois Hollande mercredi dans la Silicon Valley devrait dissiper les malentendus accumul�s entre la France et ses entrepreneurs et sera, esp�rent ces derniers, le d�clic n�cessaire pour que les responsables politiques fran�ais encouragent davantage la cr�ation d'entreprises. | Justin Sullivan
1/2
R�agir
La visite de Fran�ois Hollande mercredi dans la Silicon Valley devrait dissiper les malentendus accumul�s entre la France et ses entrepreneurs et sera, esp�rent ces derniers, le d�clic n�cessaire pour que les responsables politiques fran�ais encouragent davantage la cr�ation d'entreprises.
Que le pr�sident se d�place en Californie, "ce n'est pas forc�ment pour les entrepreneurs fran�ais que c'est bien, mais c'est plus pour la France", dit � l'AFP Pascale Vicat-Blanc, fondatrice de Lyatiss, une soci�t� sp�cialis�e dans le "cloud" (informatique d�mat�rialis�e).
Vos amis peuvent maintenant voir cette activit� Supprimer X
"Il y a une prise de conscience de quelque chose d'important, il �tait temps".
"En France --et en Europe de fa�on g�n�rale-- on a un d�ficit d'�cosyst�me organis�. Ici il y a tout: au niveau technologique, mais aussi au niveau financier, au niveau +sales+ (commercial, NDLR). On n'a pas �a en Europe", ajoute-t-elle.
Affaires consternantes
L'immersion, ne serait-ce qu'une seule journ�e, d'une bonne partie du gouvernement fran�ais dans cet "�cosyst�me" pourrait servir de cours de rattrapage express � une classe politique d�pass�e, estiment nombre d'entrepreneurs expatri�s.
Anne Bezan�on vit � San Francisco depuis 1996. Sa derni�re id�e, Placecast, a �t� lanc�e en 2005 (premier client: Microsoft), et depuis, elle a observ� comment les responsables politiques qui se rendent dans la Silicon Valley en ressortent "secou�s".
"On sent qu'il y a une esp�ce de choc une fois qu'ils ont fait deux jours de meetings avec Google, Facebook et des dizaines d'autres bo�tes", explique-t-elle � l'AFP. "D'un coup, on sent bien que c'est tellement diff�rent de la fa�on dont ils pensent le monde et le business".
"Le choc est utile car �a ouvre la porte", dit-elle, m�me si "c'est une question de g�n�ration et de culture, on ne transforme pas un pays du jour au lendemain".
Vu des Etats-Unis, les affaires Dailymotion, Netflix et Uber sont jug�es consternantes. Les freins gouvernementaux au d�veloppement de ces entreprises en France t�moignent, selon plusieurs entrepreneurs, de r�flexes protectionnistes incompatibles avec un esprit d'innovation.
Et les alli�s manquent: si la ministre fran�aise d�l�gu�e � l'Economie num�rique, Fleur Pellerin, est appr�ci�e du secteur technologique, son patron Arnaud Montebourg, ministre du Redressement productif, l'est beaucoup moins.
"Less is best"
Un petit comit� d'entrepreneurs fran�ais (dont Talend, Scality et eXo Platform, confirme le consulat � l'AFP, mais pas Criteo, contrairement � plusieurs rumeurs) doit rencontrer mercredi Fran�ois Hollande � San Francisco.
L'un d'eux, Carlos Diaz, est connu comme le loup blanc: il est � l'origine du mouvement des "pigeons", en 2012, une r�volte d'entrepreneurs via Facebook contre la hausse un temps envisag�e de la taxation des plus-values sur les cessions d'entreprises, une initiative qui aurait d�courag� les investisseurs.
"Entre l'artisan boucher et le patron du CAC 40, il y a au milieu des entrepreneurs, des jeunes issus de milieux sociaux tr�s vari�s, et qui investissent tout. Ils font un pari sur leur avenir � un moment de leur vie qui est critique", dit Carlos Diaz, fondateur de Kwarter, � l'AFP.
"La classe politique de gauche comme de droite regarde notre monde d'une fa�on pass�iste", estime-t-il. "Les gens qui arrivent ici � San Francisco ne fuient pas le fisc. Ce sont des cerveaux, ce sont les �lites de la soci�t� fran�aise qui en ont marre d'�tre stigmatis�es".
L'afflux d'ing�nieurs fran�ais et de chercheurs en biotechnologies est ind�niable, t�moigne la "dinosaure" Eliane Fiolet, fondatrice de ubergizmo.com, et qui a mis les pieds dans la Silicon Valley en 2000 pour la premi�re fois.
"La France, elle a les gens, mais elle n'en fait rien", dit-elle.
Fran�ois Hollande devrait inaugurer un "French Tech Hub", une structure publique encore mal d�finie de soutien aux jeunes pousses. L'approche est symptomatique, selon Carlos Diaz, de la myopie des dirigeants fran�ais, habitu�s � appuyer une id�e par des fonds publics.
"Les entrepreneurs ont besoin de libert�, d'espace, de reconnaissance et d'�tre encourag�s", souligne-t-il. "J'ai envie d'un Fran�ois Hollande qui me fasse r�ver, qui soit un leader, j'ai pas envie d'un Fran�ois Hollande qui vienne me dire: +j'ai de l'argent � vous donner mais il faut faire �a en �change+".
"Less is best. Moins ils en font, et mieux ce sera", conclut-il.
