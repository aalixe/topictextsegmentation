TITRE: Les lapins cr�tins feront leur apparition au cin�ma - Game-Focus
DATE: 2014-02-10
URL: http://www.game-focus.com/les-lapins-cretins-feront-leur-apparition-au-cinema-49864.php
PRINCIPAL: 161298
TEXT:
�crit par Fr�d�ric Laroche - Directeur g�n�ral
Publi� le lundi 10 f�vrier 2014 13:54
View Comments
Apr�s les jeux vid�o et la t�l�, les lapins cr�tins, qui portent assez bien leur nom, feront l�objet d�une adaptation au grand �cran par Ubisoft et Sony Pictures Entertainment. La nouvelle a �t� confirm�e aujourd�hui par Jean-Julien Baronnet, Directeur g�n�ral d'Ubisoft Motion Pictures qui avait ceci � dire :
"Sony Pictures a une formidable exp�rience pour d�velopper des blockbusters � la nature hybride entre action r�elle et images d'animation pour les publics du monde entier, ce qui en fait un partenaire de choix pour un film Lapins Cr�tins", commente Jean-Julien Baronnet. "Ce partenariat approfondit encore davantage notre relation avec Sony Pictures et met en �vidence notre approche holistique pour �tendre les marques d'Ubisoft � de nouveaux publics tout en conservant l'int�grit� cr�ative de nos marques."
Malheureusement � cette �tape les gens de Sony Pictures et d�Ubisoft n�ont pas d�voil� de d�tails concernant l'histoire ou le contexte du film. Restez branch�s, on suit le dossier pour vous.
