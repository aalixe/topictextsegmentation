TITRE: VIDEO. Le locataire laisse l'appartement inond� de canettes de bi�res
DATE: 2014-02-10
URL: http://www.leparisien.fr/insolite/video-le-locataire-laisse-l-appartement-inonde-de-canettes-de-bieres-10-02-2014-3576995.php
PRINCIPAL: 0
TEXT:
VIDEO. Le locataire laisse l'appartement inond� de canettes de bi�res
Al.H. |                     Publi� le 10.02.2014, 10h16                                             | Mise � jour :                                                      23h22
C'est un appartement inond� de milliers de canettes de bi�res qu'un couple de propri�taire de Pl�uc-sur-Li� (C�tes-d�Armor) a d�couvert alors qu'ils �taient sans nouvelle du locataire.
| Capture �cran/Ouest France
R�agir
Il y avait la c�l�bre piscine de pi�ces d'or de l'Oncle Picsou. Il y a d�sormais la piscine de canettes de bi�res de Pl�uc-sur-Li�. C'est dans cette petite ville des C�tes-d'Armor qu'un couple de propri�taires a eu la d�sagr�able surprise de retrouver son appartement envahi par des milliers de cadavres de bi�res, a rapport� lundi le quotidien Ouest France.
C'est le mari qui a fait l'invraisemblable d�couverte il y a deux semaines lorsque, sur d�cision de justice , il a obtenu un droit d'acc�s au logement .
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
�Il souhaitait,�sous contr�le d'huissiers, y prendre des mesures dans l'optique d'une vente future. Sans nouvelle du locataire depuis un an, �ils ont tent� d'ouvrir le logement mais la porte ne pouvait pas s'ouvrir de plus de 30 centim�tres, explique l'�pouse, Sophie Le Guilloux-Rousseau, � Ouest France. On ne pensait pas que c'�tait � ce point-l�. On pouvait imaginer que la porte �tait bloqu�e mais pas pour ces raisons-l�.� L'amoncellement atteint presque un m�tre de haut dans certains coins du salon ou de la chambre, selon le quotidien.
Dimanche matin, la propri�taire a crois� le locataire, qui n'habite �videmment plus les lieux. �Il reconna�t qu'il est en tort, que c'est de sa faute, mais il n'a pas d'explications, il est penaud�, explique la jeune femme, faisant preuve d'une bienveillance qui force l'admiration : �J'ai beau �tre en col�re, r�volt�e. C'est une personne qui a besoin d'aide, clairement�.
Malgr� une bonne partie des derniers loyers rest�s impay�s, selon Ouest France, le couple ne peut aujourd�hui l�expulser avant le 15 mars,�date de la fin de�la tr�ve hivernale.
VIDEO. Ils retrouvent leur appartement envahi de canettes de bi�res
LeParisien.fr
