TITRE: Burundi: les pluies font au moins 60 morts � Bujumbura | Esdras NDIKUMANA | Afrique
DATE: 2014-02-10
URL: http://www.lapresse.ca/international/afrique/201402/10/01-4737229-pluies-mortelles-au-burundi-au-moins-51-morts-a-bujumbura.php
PRINCIPAL: 160425
TEXT:
Agence France-Presse
BUJUMBURA, Burundi
Au moins 60 personnes sont mortes � la suite de pluies diluviennes dans la nuit de dimanche � lundi � Bujumbura, de m�moire le plus lourd bilan li� � des intemp�ries jamais enregistr� dans la capitale burundaise.
�Nous avons enregistr� jusqu'ici 60 personnes tu�es par ces intemp�ries,� dans la capitale et sa p�riph�rie imm�diate, a d�clar� � l'AFP le porte-parole de la Croix-Rouge burundaise, Alexis Manirakiza. �Ce sont surtout les enfants qui sont victimes�, a-t-il ajout�, sans pouvoir dire combien d'entre eux avaient p�ri.
Un pr�c�dent bilan donn� par le ministre de la S�curit� publique, le g�n�ral Gabriel Nizigama, faisait �tat de 51 personnes tu�es �par l'effondrement de leur maison ou emport�es� par des crues. �La pluie qui s'est abattue cette nuit sur la capitale et ses environs a caus� une v�ritable catastrophe naturelle�, avait ajout� le ministre.
Il s'exprimait dans un commissariat du nord de la ville, la zone la plus touch�e par les intemp�ries, o� un journaliste de l'AFP a pu voir 27 cadavres recouverts de draps blancs, dont ceux de nombreux enfants.
De m�moire, selon la police, jamais la capitale n'avait enregistr� autant de morts dans des intemp�ries. Mais les autorit�s craignent que le bilan ne s'alourdisse encore fortement, notamment car les informations sont encore parcellaires sur la situation dans d'autres provinces - le Burundi est actuellement dans sa �petite� saison des pluies.
�On a parl� de la capitale jusqu'ici, mais cette catastrophe a touch� �galement les provinces de Cibitoke, Bubanza et Bujumbura rural�, au nord et au sud de Bujumbura, a ajout� le porte-parole de la Croix-Rouge.
� Bujumbura seule, d�sormais priv�e de ses principaux connexions routi�res avec l'ext�rieur, la Croix-Rouge a aussi d�nombr� 81 bless�s et plus de 400 maisons effondr�es. Parlant de d�g�ts in�gal�s dans la capitale, le maire, Sa�di Juma, a appel� �� la solidarit� nationale et internationale�.
Les quartiers populaires de Kamenge, Kinama et Buterere sont les plus touch�s. Les maisons y sont souvent construites en fragiles briques de terre s�ch�e qui n'ont pas r�sist� aux inondations et autres coul�es d'eau et de boue venues des collines environnantes.
Axes routiers vitaux coup�s
� Kinama, un cours d'eau a notamment d�bord�. Au vu des marques sur les maisons, l'eau est ici mont�e � hauteur d'homme, jusqu'� 1m60 ou 1m70 par endroits. � la mi-journ�e, l'eau �tait largement redescendue, mais les lieux offraient un paysage de d�solation, avec de nombreuses maisons ras�es.
Zawadi, une m�re de cinq enfants, allaitait lundi son b�b� de cinq mois sur les ruines de sa maison. Autour d'elle, ne restaient que des habits �parpill�s et quelques bidons de plastique.
�Dans la nuit, j'ai entendu les enfants crier�, raconte-t-elle. Dans leur chambre, elle les a trouv�s debout sur le lit, d�j� totalement recouvert d'eau.
Comme elle, son mari et ses enfants s'en sont sortis. Mais un peu plus loin, des voisins ont eu moins de chance�: toute la famille - les parents et leurs trois enfants - est morte, dit-elle.
Le ministre de la S�curit� publique, accompagn� sur le terrain d'autres membres du gouvernement, a promis de l'aide alimentaire aux victimes et assur� que l'�tat prendrait en charge les frais d'enterrement et de relogement.
Des pluies diluviennes sont tomb�es sur Bujumbura pendant une bonne dizaine d'heures dans la nuit, provoquant �galement des coupures d'�lectricit� et d'eau dans une grande partie de la ville.
Deux axes majeurs reliant la capitale � la R�publique d�mocratique du Congo (RDC) et au Rwanda ont par ailleurs �t� coup�s. Le premier juste � la sortie ouest de la ville, � la suite de l'effondrement d'un pont, le second � une dizaine de kilom�tres au nord-ouest, par des �boulements.
Ces routes commerciales sont essentielles � l'�conomie burundaise. Tous les jours, des centaines de v�hicules transportent des marchandises via l'axe ouest en direction de la RDC. �galement quotidiennement, des dizaines de camions-remorques empruntent le second, en direction du centre du pays, puis du Rwanda, du Kenya, de l'Ouganda et de la Tanzanie.
Lundi � la mi-journ�e, la situation �tait chaotique aux alentours du pont effondr�: des pi�tons pouvaient encore traverser, mais quelque 200 v�hicules �taient bloqu�s de part et d'autre des restes de l'�difice.
Partager
