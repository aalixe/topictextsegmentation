TITRE: PMA : "Silence dans les rangs", demande Cambadélis aux socialistes
DATE: 2014-02-10
URL: http://www.midilibre.fr/2014/02/08/pma-silence-dans-les-rangs-demande-cambadelis-aux-socialistes,819565.php
PRINCIPAL: 158336
TEXT:
PMA : "Silence dans les rangs", demande Cambad�lis aux socialistes
AFP
08/02/2014, 15 h 48 | Mis � jour le 08/02/2014, 15 h 53
Le d�put� PS Jean-Christophe Cambad�lis a exhort� ses coll�gues socialistes � se taire sur la Procr�ation m�dicalement assist�e. (AFP REMY GABALDA)
"Ne pas "ajouter du trouble au trouble". C'est ce � quoi a exort� ses coll�gues socialistes, le�d�put� PS Jean-Christophe Cambad�lis��apr�s le retrait de la r�forme Bertinotti.
Le d�put� PS Jean-Christophe Cambad�lis a exhort� ses coll�gues socialistes � se taire sur la Procr�ation m�dicalement assist�e , et � ne pas "ajouter du trouble au trouble". Interrog� par Sud-Ouest paru samedi sur la col�re dans son camp apr�s le retrait de la r�forme Bertinotti et le d�sir de certains de r�introduire la PMA, l'�lu parisien r�pond : "J'appelle certains de mes camarades � la responsabilit�".
"La France traverse une �preuve majeure"
"Ils savaient que dans la loi famille, il n'y avait pas de PMA. D�s lors, ils ne peuvent estimer qu'il y a recul sur le sujet parce qu'on repousse ce texte", argumente-t-il. "La France traverse une �preuve majeure. Elle a besoin de visibilit�, d'efficacit� et donc d'unit�", insiste le secr�taire national du PS. "Alors, � partir de maintenant jusqu'en juin, c'est silence dans les rangs, on fera les comptes apr�s", tranche-t-il.
"Il ne faut pas ajouter du trouble au trouble"
"La France traverse une p�riode troubl�e et il ne faut pas ajouter du trouble au trouble", demande le d�put�. Le d�put� socialiste du Nord Bernard Roman a annonc� qu'il serait "partie prenante � toute initiative" visant � ouvrir la PMA aux couples d'homosexuelles.
naden34
le 09/02/2014, 14h41
M�me si aux municipales il y a un recul du PS, c'est le jeu ce n'est pas grave, j'esp�re que d'ici 2017 ils trouveront le moyen de faire voter la PMA parce qu'on est ridicules et tout cel� est une belle hypocrisie puisque on peut aller la faire chez nos voisins
Alertez
