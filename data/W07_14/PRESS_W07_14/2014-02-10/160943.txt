TITRE: Bayern�: Rib�ry ratera le 1/8e de finale aller de LdC�!
DATE: 2014-02-10
URL: http://www.footmercato.net/breves/bayern-ribery-ratera-le-1-8e-de-finale-aller-de-ldc_124117
PRINCIPAL: 160941
TEXT:
Bayern�: Rib�ry ratera le 1/8e de finale aller de LdC�!
Bayern�: Rib�ry ratera le 1/8e de finale aller de LdC�!
10/02/2014 - 17 h 28
Mauvaise nouvelle pour Franck Rib�ry. Le Bayern Munich vient d�annoncer qu�apr�s sa r�cente op�ration, le Fran�ais devrait observer quelques semaines de repos qui le feront rater quelques matches.
Il commencera des exercices de remise en forme en fin de semaine avant de reprendre la course la semaine prochaine. Il sera ainsi absent pour le 1/8e de finale aller de Ligue des Champions face � Arsenal.
Alexis Pereira
