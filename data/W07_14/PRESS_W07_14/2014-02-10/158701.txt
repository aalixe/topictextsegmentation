TITRE: Midi-Pyr�n�es : comment ne rien rater des exploits de nos athl�tes � Sotchi ? - France 3 Midi-Pyr�n�es
DATE: 2014-02-10
URL: http://midi-pyrenees.france3.fr/2014/02/10/midi-pyrenees-comment-ne-rien-rater-des-exploits-de-nos-athletes-sotchi-412183.html
PRINCIPAL: 158700
TEXT:
+  petit
Lundi : vous faites rel�che, vous vous concentrez sur votre boulot (ou votre famille ou ce que vous voulez). Aujourd'hui, nos sportifs se reposent et nous on en profite, pourquoi pas, pour a ller d�couvrir le curling .
Mardi : 16h, Marie-Laure�Brunet rechausse les skis et empoigne sa carabine pour le 10 km poursuite en biathlon. 56 eme sur le 7,5 km ce week-end, la Haute-Pyr�n�enne, double m�daill�e � Vancouver peut r�server des surprises. Impossible de rater �a.
Mercredi : C'est le jour des enfants. Vous faites un petit cours sur l'olympisme et les valeurs du sport � vos enfants, vos neveux et ni�ces, vos petits voisins. Au passage, le�on de g�ographie pour placer Sotchi sur la mappemonde.
Jeudi : comme c'est une petite semaine pour les sportifs de Midi-Pyr�n�es, repos encore. Vous avez le choix entre d�couverte du� patinage sur piste courte et ski acrobatique (entre autre).
Vendredi : On se l�ve t�t. A 8h, Adrien Th�aux s'aligne sur le super combin�. Le skieur de La Mongie va avoir � coeur de faire oublier la descente rat�e de dimanche. A 15 heures, Marie-Laure Brunet, encore elle, repart pour un tour avec le 15 km individuel en biathlon. Pourquoi pas un doubl� comme � Vancouver ?
Samedi : on savoure le week-end qui commence et comme il n'y a pas que les JO dans la vie, on sort prendre l'air. Pour les plus courageux, par solidarit� avec nos athl�tes, on va faire un peu de sport.
Dimanche : R�veil un peu avant 8 heures. Le temps de faire chauffer le caf�, de beurrer les tartine et zou ! C'est parti pour le super G d'Adrien�Th�aux. Attention � ne pas crier trop fort quand il va gagner une m�daille, vous allez r�veiller les voisins.
Bonne semaine olympique � tous !
Comment suivre les jeux de Sotchi ?
Les cha�nes de France 2, France 3 et France 4 diffusent lors de ces Jeux plus de 200 heures de direct exclusifs. Toute l'�quipe sportive est mobilis�e pour vous faire vivre au plus pr�s ces Jeux, que ce soit � la t�l�vision ou sur internet.
France T�l�visions ne pouvant pas diffuser toutes les �preuves en direct, le site francetvsport vous permettra de regarder sur internet les comp�titions non retransmises � la t�l�. Vous pourrez ainsi r�aliser votre propre programmation. Au total, ce dispositif vous permettra de suivre les 15 disciplines et plus de 900 heures d��preuves olympiques !
L'application francetv sport a� �t� lanc�e sp�cialement pour Sochi, afin que ses utilisateurs puissent suivre l�actualit� olympique en temps r�el.
France 3 Midi-Pyr�n�es suivra les champions r�gionaux sur son antenne et sur son site internet avec le mot cl� Sochi 2014.
Midi-Pyr�n�es
les + lus
