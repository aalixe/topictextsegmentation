TITRE: Mercedes Classe S Coup� : premi�re photo en ligne
DATE: 2014-02-10
URL: http://www.specialist-auto.fr/41662-mercedes-classe-s-coupe-premiere-photo/
PRINCIPAL: 159691
TEXT:
Accueil � Actualit�s � Mercedes Classe S Coup� : premi�re photo en ligne
Mercedes Classe S Coup� : premi�re photo en ligne
�crit le 10 f�vrier 2014 par Hugo ROBERT
La premi�re photo de la prochaine Mercedes Classe S Coup� vient d��tre mise en ligne sur la toile. Apr�s le concept d�voil� au salon de Francfort , la version de s�rie semble se rapprocher fortement de ce dernier.
La Classe S Coup�, synonyme d��l�gance
L��l�gance est le maitre mot de ce Coup� �toile. Des modifications mineures seront apport�es � l�int�rieur : l�horloge analogique sera supprim�e, le volant sera revu ainsi que le positionnement de la garniture en bois sur la planche de bord.
Sous le capot, l�ensemble des moteurs de la Classe S berline seront r�-utilis�s. On attend donc une version S63 AMG Coup� ainsi qu�une version S65 AMG .
D�voil�e � Gen�ve
Selon Autofilou, l�allemande sera d�voil�e au salon de Gen�ve (du 6 au 16 mars 2014). Mercedes pourrait d�voiler plus d�infos sur ce mod�le dans les prochains jours, restez connect� !
