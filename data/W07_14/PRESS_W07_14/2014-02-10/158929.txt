TITRE: Visite de Hollande � Obama: Les dessous du protocole - 20minutes.fr
DATE: 2014-02-10
URL: http://www.20minutes.fr/monde/1294522-20140210-visite-hollande-a-obama-dessous-protocole
PRINCIPAL: 158872
TEXT:
Barack et Michelle Obama saluent Fran�ois Hollande et Val�rie Trierweiler, � Johannesburg en Afrique du Sud, en marge de la c�r�monie d'hommage � Nelson Mandela, le 10 d�cembre 2013. E. JYANE / AFP
ETATS-UNIS - Des cartons d'invitations au plan de table...
Pour la visite d'Etat de Fran�ois Hollande aux Etats-Unis , il y a la partie officielle: honneurs militaires, tribune commune avec Obama publi�e lundi matin, vol dans Air Force One, etc. Et il y a les r�flexions en amont, sur des d�tails qui n�en sont pas quand le protocole a valeur de symbole. Ainsi, le New York Times de samedi explique�que la Maison Blanche a d� d�truire 300 cartons d�invitations pour le d�ner de gala de mardi soir, puisque sur la premi�re version figurait le nom de Val�rie Trierweiler. Il semblerait que pendant un moment, toujours d�apr�s le New York Times, les services de la Maison-Blanche se sont demand� si Fran�ois Hollande allait, ou non, venir accompagn� de sa nouvelle compagne, Julie Gayet.
Michelle Obama priv�e d'�cole
Quand il est apparu qu�au final, Fran�ois Hollande viendrait en c�libataire, d�autres r�flexions ont donn� des sueurs froides aux services du protocole d�j� stress�s par le fait qu�il s�agit du premier d�ner d�Etat en deux ans, et que l�h�te est fran�ais, et donc cens� �tre exigeant sur la cuisine. Mais il a fallu se poser d�autres questions: �Qui devrait �tre assis � c�t� du pr�sident, l� o� Mme Trierweiler aurait �t� plac�e? Est-ce que les divertissements pr�vus seraient appropri�s? Devrait-on danser, si l'invit� d'honneur au statut romantique compliqu� n'a personne avec qui danser?�, liste le New York Times.
Et pour Michelle Obama, cette visite s�annonce un peu moins charg�e que d�autres visites d�Etat. En effet, faute de premi�re dame fran�aise, le th� ou caf� entre les deux premi�res femmes n�aura pas lieu. Et Michelle Obama n�aura personne pour l�accompagner dans sa traditionnelle visite d�une �cole, organis�e avec les ��pouses de�.
M.P.
