TITRE: Jeux Olympiques Sotchi 2014: La France peut encore dire merci au biathlon - Jeux Olympiques 2013-2014 - Biathlon - Eurosport
DATE: 2014-02-10
URL: http://www.eurosport.fr/biathlon/jeux-olympiques-sotchi/2013-2014/jeux-olympiques-sotchi-2014-la-france-peut-encore-dire-merci-au-biathlon_sto4130548/story.shtml
PRINCIPAL: 161116
TEXT:
Jeux Olympiques Sotchi 2014: La France peut encore dire merci au biathlon
Par�Laurent VERGNE�le 10/02/2014 � 18:22, mis � jour le 10/02/2014 � 23:38 @LaurentVergne
La poursuite de lundi (or pour Fourcade, bronze pour B�atrix) vient confirmer � quel point le biathlon est devenu indispensable � l'�quipe de France olympique. La preuve par les chiffres.
�
Panoramic
�
Si le biathlon n'existait pas, il faudrait l'inventer. Pour la France, en tout cas, ce serait mieux. Ce sport, pourtant relativement r�cent au plus haut niveau dans notre pays, est devenu le principal pourvoyeur de m�dailles aux Jeux d'hiver. Et il s'en faut de beaucoup.
Depuis la premi�re m�daille ramen�e par le biathlon, l'or du relais dames � Albertville en 1992, 18 des 55 m�dailles tricolores sont dues au biathlon, soit un tiers environ. La proportion est la m�me en ce qui concerne les m�dailles d'or. D'Albertville � Sotchi, la France totalise 15 titres olympiques, dont 5 pour le biathlon.
Et cette tendance est m�me beaucoup plus marqu�e si l'on prend en compte la p�riode la plus r�cente, soit depuis 2006: 12 des 22 derni�res m�dailles fran�aises ont en effet �t� apport�es par les biathl�tes. Plus de la moiti�. Quant aux m�dailles d'or, quatre des six derni�res sont pour le biathlon (Baverel et Defrasne en 2006, Jay en 2010 et Fourcade ce lundi � Sotchi).
En dehors des Jeux de Nagano en 1998, o� le biathlon avait fait chou blanc, la d�l�gation fran�aise a toujours pu compter sur un apport cons�quent de cette discipline devenue incontournable. Derri�re les figures de proue (Anne Briand, Rapha�l Poir�e , Sandrine Bailly , Martin Fourcade ), le biathlon tricolore a aussi pu compter presque syst�matiquement sur de belles surprises, comme Vincent Jay , Marie Dorin ou Jean-Guillaume B�atrix. Cette force collective se traduit parfaitement dans les relais, qui ont offert 7 des 18 m�dailles.
Avec deux podiums dont un titre en trois �preuves � Sotchi, l'�quipe de France est � nouveau dans des temps de passage tr�s int�ressants. Fourcade et B�atrix ont m�me innov�: jamais deux biathl�tes fran�ais n'�taient mont�s ensemble sur un podium olympique dans une �preuve individuelle. Il ne faudra donc pas s'�tonner si, � l'heure du bilan, la moisson tricolore dans ces Jeux 2014 doit beaucoup au biathlon. C'est devenu une habitude.
�
A propos de l'auteur
Laurent VERGNE�-�Eurosport
Chef des informations � Eurosport.fr depuis 2012, entr� � Eurosport en 2003, Laurent Vergne est sp�cialiste de cyclisme, tennis, football et plus g�n�ralement de la grande histoire du sport, qu�il ne se lassera jamais de conter. Auteur principal des blogs Roue Libre et Ace. Twitter : @LaurentVergne. @LaurentVergne 104606001623733633942
�
