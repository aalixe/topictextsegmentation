TITRE: Football - MAXIFOOT - PSG : Matuidi s�me le doute sur son avenir
DATE: 2014-02-10
URL: http://news.maxifoot.fr/info-192859_140210/football.php
PRINCIPAL: 159058
TEXT:
PSG : Matuidi s�me le doute sur son avenir
�
Malgr� l'offre all�chante de Manchester City, Blaise Matuidi (26 ans, 23 matchs et 3 buts en Ligue 1 cette saison) semblait privil�gier une prolongation au Paris Saint-Germain, alors que son contrat se termine en juin prochain. Interrog� sur ce sujet sur Canal+, le milieu d�fensif parisien s'est finalement montr� tr�s �vasif.
"J'ai toujours �t� quelqu'un d'ambitieux et qui souhaite aller le plus haut possible. Je n'ai rien qui me fait peur aujourd'hui. J'ai envie de laisser une empreinte dans le monde du foot. Je suis concentr� sur l'ann�e qui va s'�couler apr�s il y a des choses dans lesquelles je n'ai pas forc�ment envie d'entrer aujourd'hui. Mon but c'est d'aller plus haut. Si Paris me le permet oui, mais je suis quelqu'un d'ambitieux", a d�clar� l'international fran�ais.
Des propos qui laissent la place au doute...
(Par Romain Rigaux)
� News lue par 24703 visiteurs
� �
