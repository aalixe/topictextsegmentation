TITRE: Firefox 28 : une b�ta plus tactile
DATE: 2014-02-10
URL: http://www.itespresso.fr/firefox-28-beta-plus-tactile-72497.html
PRINCIPAL: 158403
TEXT:
Vous �tes ici : Accueil / Solutions IT / Firefox 28 : une b�ta plus tactile
Firefox 28 : une b�ta plus tactile
Mozilla a publi� une premi�re version b�ta de son navigateur Firefox optimis� pour Windows 8.
Donnez votre avis
En conformit� avec la feuille de route �tablie en fin d�ann�e 2013, Mozilla a publi� une premi�re b�ta de son navigateur Firefox optimis� pour l�environnement tactile de Windows 8 .
Une pr�version avait �t� diffus�e fin septembre dans le canal Aurora (assimilable au stade alpha), � destination des testeurs et utilisateurs avanc�s). Instable, elle offrait un aper�u des grandes orientations prises par la fondation en attendant la sortie de la version finale, initialement pr�vue pour le mois de janvier.
Le d�veloppement de ce portage adapt� � l�interface Modern UI de Windows a d�but� voici pr�s de deux ans. A plusieurs reprises, le projet a pris du retard, le panel de testeurs n�ayant pas atteint une masse suffisamment critique, notamment de par l�adoption plut�t d�cevante du nouvel OS de Microsoft .
La situation s��claircit avec le passage en b�ta. Les sp�cificit�s de la Modern UI sont mises � profit pour r�viser le design du butineur et plus particuli�rement de sa page d�accueil. Celle-ci est divis�e en trois sections : les favoris, l�historique de navigation et les sites les plus visit�s. Fonctionnellement parlant, elle supporte des commandes tactiles multipoints comme le zoom et le glisser-d�placer. Avec un mode plein �cran et la possibilit� d�ancrer l�application sur la gauche ou la droite de l��cran, l�exp�rience utilisateur se rapproche de celle propos�e par Internet Explorer 11.
Firefox 28 ��Touch�� b�n�ficie aussi d�une int�gration avec la fonction de partage de Windows 8.x pour faciliter la diffusion de contenus Web, en premier lieu sur les r�seaux sociaux. Parmi les autres nouveaut�s, Mozilla recense l�API Gamepad, qui permet d�exploiter des contr�leurs de jeu directement dans le navigateur, de mani�re native.
Les derni�res semaines ont �t� consacr�es � l�am�lioration des performances. Pour autant, les capacit�s multim�dias du navigateur s�enrichissent : prise en charge du codec VP9 d�velopp� par Google comme alternative au H.265, contr�le du volume avec HTML5, audio Opus pour le conteneur WebM� Sur la liste compl�te des am�liorations, Mozilla recense aussi deux probl�mes : l�un concerne le rendu du texte sous Windows 7 ; l�autre, le passage en arri�re-plan lors de la lecture d�une vid�o Flash (s�ensuit un plantage).
Windows 8 : la sortie
26 octobre 2012 : sortie officielle de Windows 8 dans le monde
�� A voir aussi ��
