TITRE: Cacophonie � gauche : Cambad�lis veut le "silence dans les rangs" - SudOuest.fr
DATE: 2014-02-10
URL: http://www.sudouest.fr/2014/02/07/cacophonie-a-gauche-cambadelis-veut-le-silence-dans-les-rangs-1455254-710.php
PRINCIPAL: 158336
TEXT:
A six semaines des municipales, Jean-Christophe Cambad�lis demande aux d�put�s PS de ne pas se tromper de priorit�. L'urgence reste la croissance
Jean-Christophe Cambad�lis, d�put� PS de Paris. � Photo
AFP FRED DUFOUR
Publicit�
D
ire que Jean-Christophe Cambad�lis, le d�put� PS de Paris, n'a pas appr�ci� les d�veloppements politiques qui ont accompagn�, � gauche comme � droite, le retrait de la loi famille est un doux euph�misme. Comme il l'a expliqu� � Sudouest.fr ce vendredi, le pacte de responsabilit� aux entreprises doit rester la priorit�. Et ce d'autant plus qu'il redoute d'ores et d�j� que le patronat ne se d�fausse. Interview.
Publicit�
Sud Ouest. La volte face du gouvernement sur la loi famille vous a laiss� un go�t amer. Vous parlez m�me de � Shadok time ��
Jean-Christophe Cambad�lis. Ma col�re s'adresse � l'ensemble de la classe politique. La situation est ubuesque. On manifeste contre la PMA et la GPA alors que ces deux dispositions ne se trouvent pas dans le texte famille. Celui-ci est repouss� et la gauche hurle � la capitulation sur la PMA, tandis que l'UMP crie victoire alors qu'elle n'a pas particip� � la manifestation. Vous avouerez que c'est incompr�hensible.
�
"A partir de maintenant jusqu'en juin, c'est silence dans les rangs, on fera les comptes apr�s." �
Certains d�put�s de gauche veulent toujours d�poser des amendements sur cette loi famille. � six semaines des municipales, cela vous inqui�te-il d'entendre des socialistes �tre pr�ts � ferrailler contre le gouvernement ?
Absolument. J'appelle certains de mes camarades � la responsabilit�. Ils savaient que dans la loi famille il n'y avait pas de PMA. D�s lors, ils ne peuvent estimer qu'il y a recul sur le sujet parce qu'on repousse ce texte. Franchement ! La France traverse une �preuve majeure. Elle a besoin de visibilit�, d'efficacit� et donc d'unit�. Alors, � partir de maintenant jusqu'en juin, c'est silence dans les rangs, on fera les comptes apr�s.
La gauche doit-elle r�agir aux manifestations des 26 janvier et 1er f�vrier ? Ne serait-ce pas au PS de se mobiliser sur ce point ?
Je ne pense pas que ce soit au Parti socialiste de porter seul la mobilisation contre les ennemis de la R�publique. C'est � l'ensemble des r�publicains et des d�mocrates de proposer une manifestation � laquelle le PS pourra souscrire. Car si le PS le fait aujourd'hui, au mieux on parlera de r�cup�ration et au pire, on ne voudra pas appara�tre comme le soutenant � la veille des municipales.
�
"Messieurs du MEDEF, c'est � vous de jouer, la France vous regarde." �
Cette cacophonie sur la loi famille n'illustre-t-elle pas la difficult� du gouvernement � tenir un cap ?
Mais le fait de reporter le texte famille a pr�cis�ment �t� fait pour donner la priorit� au pacte de responsabilit�. C'�tait pour �viter que la France ne s'embrase � � tort - sur un sujet qui d�fraye la chronique depuis un an. Et surtout pour maintenir au centre du d�bat fran�ais la croissance et la lutte contre le ch�mage. La France traverse une p�riode troubl�e et il ne faut pas ajouter du trouble au trouble.
Avec ce nouveau couac, le pacte de responsabilit� a disparu des �crans radars. N'est ce pas �a le plus inqui�tant ?
On ne peut pas dire que le pacte de responsabilit� est le grand perdant, puisque les n�gociations n'ont pas encore d�but�. Mais il y a des indices inqui�tants. On voit le MEDEF abandonner progressivement ce qu'il avait dit : � savoir qu'avec des all�gements de charge, il pourrait cr�er un million d'emplois. Monsieur Gattaz avait un badge � sa boutonni�re qui rappelait cela. On a l'impression que le MEDEF veut prendre son gain avec les all�gements de charge sur les cotisations familiales, mais se refuse � indiquer quels seraient ses investissements en contrepartie. A partir de l� les syndicats ne voient pas comment ils peuvent participer � ce faux dialogue social. �a devient inqui�tant.
�
Fran�ois Hollande doit-il de nouveau faire pression sur le MEDEF ?
Jean-Marc Ayrault a �crit aux partenaires sociaux pour leur demander d'engager promptement ces n�gociations. Il faut faire pression sur les uns et les autres. Le pays ne peut attendre. Il y a urgence, non seulement pour redresser notre �conomie mais il y a urgence pour la croissance, l'emploi et la comp�titivit�. On ne peut jouer avec les difficult�s �pouvantables de la France. Le MEDEF doit s'engager � investir puisque l'�tat s'est engag� de son c�t�. Messieurs du MEDEF, c'est � vous de jouer, la France vous regarde.
�
