TITRE: Hollande aux Etats-Unis, Montebourg et l'euro fort, VTC contre taxis : la revue de presse de l'industrie
DATE: 2014-02-10
URL: http://www.usinenouvelle.com/article/hollande-aux-etats-unis-montebourg-et-l-euro-fort-vtc-contre-taxis-la-revue-de-presse-de-l-industrie.N239111
PRINCIPAL: 158971
TEXT:
Publi� le
10 f�vrier 2014, � 08h23
La presse revient ce lundi matin sur le voyage officiel de Fran�ois Hollande aux Etats-Unis qui d�bute aujourd'hui. A lire �galement : une interview d'Arnaud Montebourg aux Echos et la guerre entre les taxis et les VTC relanc�e.
Hollande aux Etats-Unis pour sceller "une alliance transform�e"
Le pr�sident de la R�publique d�marre ce lundi 10 f�vrier une visite de trois jours aux �tats-Unis. Dans une tribune commune avec Barack Obama publi�e dans Le Monde et le Washington Post , Fran�ois Hollande d�crit les contours d'"une alliance transform�e" entre la France et les Etats-Unis.
"Hollande aux Etats-Unis : voyage au pays de la r�ussite", titre Le Figaro, pour qui le chef de l'Etat devra s'inspirer du red�marrage �conomique outre-Atlantique.
�
�
Mercredi, Fran�ois Hollande fera un crochet par San Francisco pour promouvoir les start-up fran�aises r�unies au sein d�un "French Tech Hub" dans la Silicon Valley et y rencontrer les patrons des g�ants Facebook , Twitter , Mozilla ou encore Google .
Ces "moteurs � �vasion fiscale" sont � la une Lib�ration, qui revient sur ce contexte des "pratiques contestables des g�ants du net".
�
�
Montebourg repart en guerre contre l'euro fort
Le ministre du Redressement productif Arnaud Montebourg a r�affirm�, dans une interview publi�e dimanche soir sur le site internet des Echos , sa volont� de "faire baisser l'euro", estimant que son niveau actuel "annihile les efforts de comp�titivit�" lanc�s en France.
"Comme ministre de l'Industrie, je consid�re que l'euro est sorti de ses clous par une sur�valuation qui est devenue probl�matique aux yeux de tous pour nos entreprises. Entre 2012 et 2013, il s'est appr�ci� de plus de 10% face au dollar et de plus de 40% face au yen", souligne Arnaud Montebourg aux Echos. "Nous avons la zone la plus d�pressive au monde et la monnaie qui s'appr�cie le plus au monde. Cette situation est ubuesque", ajoute-t-il.
"Nous devons ouvrir une bataille politique pour faire baisser l'euro. L'euro doit �tre au service de notre �conomie et de notre industrie. Il ne s'agit pas de le d�valuer mais de le ramener � un niveau raisonnable et supportable", explique-t-il.
D'apr�s le ministre, qui cite les travaux de la direction du Tr�sor : "une d�pr�ciation de 10% permettrait d'accro�tre notre taux de croissance de 1,2%. Cela cr�erait 150 000 emplois, am�liorerait la balance commerciale et r�duirait notre d�ficit public de 12 milliards".
�
�
Nouvelle mobilisation des taxis contre les VTC
Plusieurs centaines de taxis �taient rassembl�s ce lundi matin, notamment aux a�roports de Roissy et d'Orly, � l'occasion d'une manifestation nationale contre les voitures de tourisme avec chauffeur (VTC), accus�es de concurrence d�loyale.
