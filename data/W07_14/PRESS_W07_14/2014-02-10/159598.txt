TITRE: Quinze morts et 130 bless�s dans un incendie � M�dine - china radio international
DATE: 2014-02-10
URL: http://french.cri.cn/621/2014/02/09/41s367741.htm
PRINCIPAL: 0
TEXT:
Quinze morts et 130 bless�s dans un incendie � M�dine
��2014-02-09 18:43:18��yahoo
Quinze p�lerins ont �t� tu�s et quelque 130 bless�s dans un incendie samedi dans un h�tel accueillant quelque 700 fid�les musulmans � M�dine, deuxi�me lieu saint de l'islam dans l'ouest de l'Arabie saoudite, selon un nouveau bilan de source officielle.
Les autorit�s de M�dine ont ajout� que le feu avait �t� ma�tris� en fin de journ�e et que les autres clients de l'h�tel, qui effectuent le rite de la Omra (petit p�lerinage), avaient �t� �vacu�s et install�s dans d'autres �tablissements h�teliers. Une enqu�te a �t� ouverte pour conna�tre les causes du drame.
Mot-cl� du jour
Commentaire
Annonce
Le 5 avril, c`est la F�te de Qingming en Chine. Connaissez-vous l`autre nom en fran�ais de cette f�te ?
La f�te du Printemps. On c�l�bre le Nouvel an chinois.
La f�te des Bateaux-Dragon. On organise des courses de bateaux-dragon.
La f�te des morts. On rend hommage aux d�funts.
La f�te des Lanternes. On admire les lanternes, signes de bonheur.
��
Un demi-si�cle ! Le 27 janvier 2014 marque le 50e anniversaire de l'�tablissement des relations diplomatiques entre la France et la Chine. Pour se r�m�morer tous les moments forts de ces cinq d�cennies, Radio Chine Internationale organise un grand concours :�le vote sur Internet pour les 10 �v�nements les plus marquants des 50 ans de relations diplomatiques entre les deux pays.
