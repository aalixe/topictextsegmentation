TITRE: JO/bosses: Cavet en finale, Benna �limin� et r�volt� - Sportquick
DATE: 2014-02-10
URL: http://www.sportquick.com/actualite-afp/ski-Oly-2014-freestyle-FRA
PRINCIPAL: 161137
TEXT:
< Retour
� AFP/
Le bosseur fran�ais Benjamin Cavet en qualification en ski de bosses dans le  Parc Extr�me � Rosa Khoutor, le 10 f�vrier 2014
Le jeune Fran�ais Benjamin Cavet s'est qualifi� pour la finale de l'�preuve olympique de ski de bosses, pr�vue lundi � 22h00 locales (19h00 fran�aise), contrairement � Anthony Benna, r�volt� par son �limination rapide.
Cavet, 20 ans, a termin� � la 19e place des 20 qualifi�s pour la finale, loin du Canadien Alexandre Bilodeau, champion olympique en 2010, qui a fini en t�te de la premi�re phase de qualifications devant son compatriote Mika�l Kingsbury.
Apr�s avoir compl�tement manqu� sa premi�re descente, Cavet a d� participer � la seconde phase de qualifications et est pass� en finale de justesse sur la piste du Parc Extr�me de Rosa Khoutor.
Benna, 26 ans, a �chou� lors de cette seconde partie des qualifications et a fait part de son "�norme" frustration apr�s une notre tr�s basse de 16,92 pts, synonyme de 23e place au classement final.
"J'ai pris une claque, a-t-il dit. Se prendre 16 points de la part des juges, �a fait mal. C'est comme si j'avais rat� toute ma descente alors que ce n'est pas le cas. Ces juges sont mauvais comme tout, ils ont 50 ans et ne connaissent rien au freestyle."
"Ils ne sont pas comp�tents, a jout� le bosseur de Meg�ve. Quand les tous meilleurs font des fautes, ils ne sont pas p�nalis�s mais quand c'est toi, tu prends une claque. C'est injuste. Les jeunes (de l'�quipe de France), �a les d�go�te, ils ne comprennent pas."
"Je m'�tais rat� � Vancouver (30e) et j'avais envie de tout d�chirer � Sotchi, �a fait vraiment mal au c?ur", a indiqu� le Fran�ais, qui compte trois podiums en Coupe du monde. A chaud, j'ai envie d'arr�ter (la comp�tition)."
La finale a lieu en trois phases: les douze meilleurs des vingt concurrents au d�part de la premi�re phase disputeront la deuxi�me et les six meilleurs de cette deuxi�me phase skieront dans la troisi�me, qui d�cernera les m�dailles.
L'�preuve a eu lieu sans Guilbaut Colas, victime d'une rupture du ligament crois� ant�rieur et d'une l�sion du m�nisque interne du genou gauche dimanche � l'entra�nement. Depuis son titre de champion du monde et son globe de cristal d�croch�s en 2011, le Fran�ais de 30 ans a accumul� les probl�mes physiques, notamment au dos.
�
