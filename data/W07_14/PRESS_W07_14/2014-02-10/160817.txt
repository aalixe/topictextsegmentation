TITRE: Inondations au Burundi: 60 morts - BBC Afrique - Afrique
DATE: 2014-02-10
URL: http://www.bbc.co.uk/afrique/region/2014/02/140210_burundi_floods.shtml
PRINCIPAL: 160812
TEXT:
Imprmer la page
Au moins 60 personnes sont mortes � la suite de pluies duliviennes d'une violence jamais enregistree dans l'histoire du paysdans la nuit de dimanche � lundi � Bujumbura.
"Nous avons enregistr� jusqu'ici 60 personnes tu�es par ces intemp�ries," dans la capitale et sa p�riph�rie imm�diate, a d�clar� � l'AFP le porte-parole de la Croix-rouge burundaise, Alexis Manirakiza. "Ce sont surtout les enfants qui sont victimes", a-t-il ajout�, sans pouvoir dire combien d'entre eux avaient p�ri.
Un pr�c�dent bilan donn� par le ministre de la S�curit� publique, leg�n�ral Gabriel Nizigama, faisait �tat de 51 tu�s "par l'effondrement de leur maison ou emport�es" par des crues. "La pluie qui s'est abattue cette nuit sur la capitale et ses environs a caus� une v�ritable catastrophe naturelle", avait ajout� le ministre.
La morgue de l'h�pital principal est pleine, et les patients sont trait�s � l'ext�rieur.
Des coul�es d'eau et de boue ont d�val� la colline qui surplombe la ville, d�truisant un axe important, de nombreux b�timents et des cultures.
Selon la police, jamais la capitale n'avait enregistr� autant de morts dans des intemp�ries. Mais les autorit�s craignent que le bilan ne s'alourdisse encore fortement.
Le Burundi est actuellement dans sa "petite" saison des pluies.
A Kinama, un cours d'eau a notamment d�bord�. Au vu des marques sur les maisons, l'eau est ici mont�e � hauteur d'homme, jusqu'� 1m60 ou 1m70 par endroits. A la mi-journ�e, l'eau �tait largement redescendue, mais les lieux offraient un paysage de d�solation, avec de nombreuses maisons ras�es.
Mots-cl�s
