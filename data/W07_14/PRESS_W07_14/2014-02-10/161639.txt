TITRE: Robyn, enceinte et en �tat de mort c�r�brale : le Canada boulevers� - Femmes dans la soci�t� - La Parisienne
DATE: 2014-02-10
URL: http://www.leparisien.fr/laparisienne/societe/robyn-enceinte-et-en-etat-de-mort-cerebrale-le-canada-bouleverse-04-02-2014-3558111.php
PRINCIPAL: 0
TEXT:
Robyn, enceinte et en �tat de mort c�r�brale : le Canada boulevers�
4 f�vr. 2014, 06h53              | MAJ : 07h55
r�agir
17
ILLUSTRATION. Un Canadien, dont l'�pouse en �tat de mort c�r�brale sera d�branch�e apr�s la naissance de leur enfant, suscite l'�motion au Canada en racontant sur les r�seaux sociaux et sur un blog son cheminement depuis le basculement de sa vie. Dylan Benson Twitter
Mon activit� Vos amis peuvent maintenant voir cette activit�
Tweeter
Un Canadien, dont l'�pouse en �tat de mort c�r�brale sera d�branch�e apr�s la naissance de leur enfant, suscite l'�motion au Canada en racontant sur les r�seaux sociaux et sur un blog son cheminement depuis le basculement de sa vie . Dans son premier billet intitul� �Le jour apr�s celui o� ma vie a chang�, Dylan Benson, 32 ans, explique que son �pouse du m�me �ge a �t� victime d'une h�morragie c�r�brale le 21 d�cembre alors qu'elle �tait enceinte de 22 semaines.
Sur le m�me sujet
Etats-Unis : la m�re enceinte dans le coma a �t� d�branch�e
Depuis, Robyn Benson est aux urgences de l'h�pital de Victoria, dans la province de Colombie-Britannique, sous assistance respiratoire et intub�e mais en coma v�g�tatif. Les m�decins maintiennent ses fonctions vitales pour atteindre au maximum les 34 semaines de gestation avant de proc�der � un accouchement par c�sarienne, explique son mari.
Il a pr�nomm� son fils Iver
Une porte-parole de l'h�pital g�n�ral de Victoria a confirm� que Robyn Benson est bien hospitalis�e, sans donner plus de pr�cisions. �Avoir une femme pratiquement m�re sous assistance et en mort clinique est une trag�die rare�, a-t-elle ajout�. Sur son blog, Dylan Benson �crit que le corps m�dical d�branchera les appareils d'assistance au maintien en vie de sa femme juste apr�s la naissance de son fils, qu'il a d�j� pr�nomm� Iver.
�J'ai d�cid� de prendre un cong� sans solde (...) avec tellement � faire que je ne peux tout simplement pas me concentrer sur mon travail�, �crit ce passionn� de hockey sur glace et du club des Canucks de Vancouver, principale attention de ses messages sur son compte Twitter .
@AwaitingJuno hey. Just wanted to say thanks for the post you made. Some great points for sure. Thanks for your support.
� Dylan Benson (@DylanBenson) February 3, 2014
La souscription a litt�ralement d�coll� avec les r�cits faits par les m�dias canadiens. Avec l'objectif des 32.000 dollars largement d�pass�, Dylan Benson ne dit pas dit si cette souscription va �tre suspendue. Elle est encore ouverte pour 89 jours, soit bien apr�s la naissance suppos�e du b�b�.
Lundi, la souscription en ligne lanc�e par Dylan Benson avait d�j� atteint pr�s de 50 000 dollars canadiens (35 000 euros) pour lui permettre de ne pas travailler pour les mois qui suivront la naissance de son enfant, pr�vue le mois prochain.
Le 4 f�vrier, la souscription atteignait quasiment 72�000 dollars
