TITRE: Apple : nouvelle hausse avec Carl Icahn
DATE: 2014-02-10
URL: http://www.boursier.com/actions/actualites/news/apple-nouvelle-hausse-avec-carl-icahn-565832.html
PRINCIPAL: 161706
TEXT:
Mon compte Je suis d�j� client
D�couvrir Je souhaite recevoir une documentation gratuite
Apple : nouvelle hausse avec Carl Icahn
Abonnez-vous pour
moins de 1� par jour !
Le 10/02/2014 � 18h01
(Boursier.com) � Apple gagne encore 2% � 530$ environ sur le Nasdaq ce lundi. L'investisseur activiste Carl Icahn a publi� ce jour une lettre destin�e aux actionnaires d' Apple , dans laquelle le milliardaire prend bonne note de la recommandation de l'ISS (Institutional Shareholder Services) contre sa proposition de programme massif de rachats d'actions.
Au regard des r�centes actions d'Apple pour agressivement racheter ses propres actions, Icahn pr�cise qu'il n'est pas en d�saccord global avec les affirmations et recommandations de l'ISS.
Apple , le g�ant californien de Cupertino, a acquis pour 14 milliards de dollars de ses propres titres durant les deux semaines cons�cutives � ses publications financi�res trimestrielles. Le groupe a ainsi profit� de la faiblesse des cours apr�s les publications pour mettre en place un programme acc�l�r� de rachats de 12 Mds$. 2 Mds$ de titres ont par ailleurs �t� rachet�s sur le march� !
Durant les 12 derniers mois, Apple a rachet� pour plus de 40 milliards de dollars de ses propres actions. Tim Cook a pr�cis� qu'il s'agissait l� d'un record pour une telle p�riode, pour une soci�t� cot�e.
Icahn pense qu'Apple devrait racheter pour au moins 32 Mds$ de titres durant l'exercice 2014. Compte tenu de ce niveau cons�quent de rachats, l'activiste ne voit plus de raison de persister avec sa proposition, "particuli�rement quand la compagnie est d�j� si proche de r�pondre � notre requ�te concernant l'objectif de rachats".
L'investisseur prend �galement note des plans confirm�s de Tim Cook concernant les nouveaux produits d'Apple "dans de nouvelles cat�gories cette ann�e", qui s'ajoutent aux plans de nouveaut�s sur les cat�gories existantes. Icahn se dit donc extr�mement confiant dans l'avenir d'Apple et juge toujours que le groupe est "extr�mement sous-valoris�".
Jean-No�l Legalland � �2014, Boursier.com
