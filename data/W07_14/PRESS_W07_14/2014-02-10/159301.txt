TITRE: Vote anti-immigration en Suisse : � On va revoir nos relations avec la Suisse �, dit Fabius
DATE: 2014-02-10
URL: http://www.lemonde.fr/europe/article/2014/02/10/le-vote-suisse-une-mauvaise-nouvelle-pour-l-europe_4363265_3214.html
PRINCIPAL: 0
TEXT:
Vote anti-immigration en Suisse : � On va revoir nos relations �, assure Fabius
Le Monde |
� Mis � jour le
10.02.2014 � 13h04
Au lendemain de la votation suisse en faveur d'une limitation de l'immigration , les r�actions politiques en France � pays frontalier de la Suisse o� travaillent de nombreux Fran�ais � sont divis�es : si la gauche d�plore le r�sultat du vote, la droite le comprend, quand l'extr�me droite l'encourage.
Lire notre d�cryptage Suisse : quatre questions sur la votation anti-immigr�s
Pour le chef de la diplomatie , Laurent Fabius, � c'est une mauvaise nouvelle � la fois pour l' Europe et pour les Suisses �.���C'est un vote pr�occupant parce qu'il signifie que la Suisse veut se replier sur elle-m�me et c'est paradoxal car la Suisse fait 60�% de son commerce ext�rieur avec l' Union europ�enne , a expliqu� M. Fabius. On va revoir nos relations avec la Suisse��.
Le ministre de l'int�rieur, Manuel Valls, parle d'��un signe pr�occupant d'un repli sur soi (...) et une mauvaise nouvelle pour les Suisses eux-m�mes��, en marge d'un d�placement � Florange (Moselle).
David Assouline, porte-parole du Parti socialiste , a qualifi� de ��mauvais vent venu de Suisse�� le r�sultat de la votation de dimanche.
Lire aussi Suisse : le vote limitant l'immigration inqui�te l'Europe
��PARFAITEMENT NATUREL�� POUR FILLON, � LUCIDE�� POUR LE FN
L'ancien premier ministre Fran�ois Fillon a jug�, lui, ��parfaitement naturel�� que la Suisse veuille r�duire le nombre d'�trangers sur son territoire. L'argument est � non pas la d�fense de l' emploi �, mais ��l'int�gration��, a d�velopp� M. Fillon sur BFM-TV et RMC.
� Je propose depuis des mois que la France ou l'Europe � les deux peuvent �tre possibles � adoptent le m�me syst�me [Il s'agirait de] d�cider chaque ann�e, en fonction de la capacit� d'int�gration du pays ��son �conomie, les logements disponibles, la capacit� d'accueil des services publics, les �coles,�etc. � combien de personnes ext�rieures on peut accueillir��.
Pour lui, � ce qui a fait la force de la France �, l'int�gration accompagn�e d'une ��adh�sion au projet r�publicain, s'est cass� en raison du nombre��. ��Il y a un probl�me de surcapacit�, un blocage de l'int�gration li� au nombre [d'�trangers accueillis] �, selon lui. Un d�bat et un vote au Parlement � changeraient le discours sur l'immigration��, estime l'ancien chef de gouvernement.
Le Front national a pr�f�r� saluer ��la lucidit� du peuple suisse�� apr�s la victoire du oui � la votation organis�e dans ce pays contre ��l'immigration de masse��. Lundi au micro d'Europe 1, la pr�sidente du FN, Marine Le Pen, a assur� que si les Fran�ais �taient consult�s sur la m�me question que les Suisses, � ils voteraient massivement pour��.
.@MLP_officiel, � propos de la limite de l'immigration en Suisse : "Je suis s�re que les Fran�ais voteraient massivement pour" #E1matin
� Europe 1  (@Europe1)
