TITRE: L'écran de l'iPhone 6 sera-t-il incassable ?
DATE: 2014-02-10
URL: http://www.lefigaro.fr/secteur/high-tech/2014/02/10/01007-20140210ARTFIG00271-l-ecran-de-l-iphone-6-sera-t-il-incassable.php
PRINCIPAL: 0
TEXT:
Publié
le 10/02/2014 à 18:01
L'écran de l'iPhone 6 pourrait être fabriqué à partir du saphir, réputé pour sa résistance.
Le site 9to5Mac annonce que la marque à la pomme se serait procurée du saphir en quantité suffisante pour concevoir les écrans de 150 millions iPhone.
Publicité
Apple se serait-il définitivement passé de LG et Samsung ? Le constructeur américain semble décidé à ne plus leur sous-traiter la fabrication des écrans de ses iPhone . Exit l�écran Retina, la marque à la pomme étudierait la possibilité de doter son iPhone 6 d'un écran incassable, produit dans son usine de Mesa, en Arizona. Après avoir conclu l'an dernier un partenariat avec GT Advanced, entreprise spécialisée dans les équipements électroniques utilisant des cristaux, Apple avait fait le choix de relocaliser en partie sa production aux Etats-Unis et de réduire les contrats accordés à ses sous-traitants.
Le site américain 9to5Mac annonce que GT Advanced aurait acheté suffisamment de saphir pour équiper 100 à 200 millions iPhone d'un écran incassable. Le saphir est en effet particulièrement résistant aux rayures et aux chutes, ne pouvant être abîmé que par un cristal de même qualité ou du diamant. Même si cette modification peut passer pour une petite révolution pour les détenteurs du fameux smartphone, Apple utilise déjà le saphir pour protéger le capteur photo de son dernier modèle.
L'information n'est pas confirmée par Apple, soucieux de préserver comme à chaque fois le mystère autour de son prochain produit. Samsung et LG pourraient néanmoins voir d'un mauvais oeil cette nouvelle rumeur, les deux marques ayant déjà commencé à plancher sur ce type d'innovation. LG a notamment sorti le G Flex, dont le film protecteur lui permet d'être plus résistant aux rayures que ses concurrents . Jusqu'à l'iPhone 6?
