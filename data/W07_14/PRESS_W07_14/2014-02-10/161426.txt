TITRE: Deux adultes et un enfant morts en Ari�ge: drame familial probable - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/societe/deux-adultes-un-enfant-morts-ariege-drame-familial-probable-707306.html
PRINCIPAL: 161423
TEXT:
Les corps d'un homme, d'une femme et d'un enfant d'une dizaine d'ann�es, tu�s a priori par arme � feu, ont �t� retrouv�s lundi dans une maison de Mercenac, en Ari�ge, a indiqu� une source judiciaire.
�� �
La piste du drame familial est privil�gi�e, a-t-on ajout� de m�me source.
�� �
Selon les tout premiers �l�ments de l'enqu�te, un p�re de famille aurait tu� son fils �g� d'une dizaine d'ann�es ainsi qu'une femme dans cette maison de Mercenac, localit� de 400 habitants proche de Saint-Girons. Il aurait ensuite retourn� l'arme contre lui. Les liens pr�cis unissant l'homme et la femme restent flous.
�� �
Les enqu�teurs de la gendarmerie ont �t� alert�s en d�but d'apr�s-midi et, une fois sur place, ont s�curis� la sc�ne de crime pour proc�der aux relev�s n�cessaires � l'enqu�te. Les corps des victimes devraient �tre autopsi�s mardi apr�s-midi.
