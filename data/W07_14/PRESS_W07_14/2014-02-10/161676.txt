TITRE: Burundi: Au moins 51 morts dans des intemp�ries � Bujumbura  -  News Monde: Afrique - tdg.ch
DATE: 2014-02-10
URL: http://www.tdg.ch/monde/afrique/Au-moins-51-morts-dans-des-intemperies-a-Bujumbura-/story/26682199
PRINCIPAL: 161675
TEXT:
Au moins 51 morts dans des intemp�ries � Bujumbura
Mis � jour le 10.02.2014
Des intemp�ries au Burundi ont caus� la mort de plus de 50 personnes � Bujumbura.
Signaler une erreur
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
E-Mail*
Veuillez SVP entrez une adresse e-mail valide
Au moins 51 personnes sont mortes dans la capitale burundaise Bujumbura � la suite de pluies torrentielles survenues dans la nuit de dimanche � lundi, a annonc� le ministre de la S�curit� publique.
�La pluie qui s'est abattue cette nuit sur la capitale et ses environs a caus� une v�ritable catastrophe naturelle�, a d�clar� � la presse le ministre, le g�n�ral Gabriel Nizigama. �On a d�j� retrouv� 51 cadavres de personnes tu�es par l'effondrement de leur maison ou emport�es� par des crues. (afp/Newsnet)
Cr��: 10.02.2014, 11h25
Votre email a �t� envoy�.
Publier un nouveau commentaire
Nous vous invitons ici � donner votre point de vue, vos informations, vos arguments. Nous vous prions d�utiliser votre nom complet, la discussion est plus authentique ainsi. Vous pouvez vous connecter via Facebook ou cr�er un compte utilisateur, selon votre choix. Les fausses identit�s seront bannies. Nous refusons les messages haineux, diffamatoires, racistes ou x�nophobes, les menaces, incitations � la violence ou autres injures. Merci de garder un ton respectueux et de penser que de nombreuses personnes vous lisent.
La r�daction
Merci de votre participation, votre commentaire sera publi� dans les meilleurs d�lais.
Merci pour votre contribution.
J'ai lu et j'accepte la Charte des commentaires.
Caract�res restants:
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
S'il vous pla�t entrer un nom valide.
S'il vous pla�t indiquez le lieu g�ographique.
Ce num�ro de t�l�phone n'est pas valable.
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
Signup Fermer
Vous devez lire et accepter la Charte de commentaires avant de poursuivre.
Nous sommes heureux que vous voulez nous donner vos commentaires. S'il vous pla�t noter les r�gles suivantes � l'avance: La r�daction se r�serve le droit de ne pas publier des commentaires. Ceci s'applique en g�n�ral, mais surtout pour les propos diffamatoires, racistes, hors de propos, hors-sujet des commentaires, ou ceux en langues �trang�res ou dialecte. Commentaires des noms de fantaisie, ou avec des noms manifestement fausses ne sont pas publi�s non plus. Plus les d�cisions de la r�daction n'est ni responsable d�pos�e, ni en dehors de la correspondance. Renseignements t�l�phoniques ne seront pas fournis. L'�diteur se r�serve le droit �galement � r�duire les commentaires des lecteurs. S'il vous pla�t noter que votre commentaire aussi sur Google et autres moteurs de recherche peuvent �tre trouv�s et que les �diteurs ne peuvent rien et est de supprimer un commentaire une fois �mis dans l'index des moteurs de recherche.
�auf Facebook publizieren�
Veuilliez attendre s'il vous pla�t
Soumettre Commentaire
Soumettre Commentaire
No connection to facebook possible. Please try again. There was a problem while transmitting your comment. Please try again.
Aucun commentaire pour le moment
Le compte Twitter de la rubrique Monde
