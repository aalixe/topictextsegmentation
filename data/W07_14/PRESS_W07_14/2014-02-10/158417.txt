TITRE: Barcelone reprend la tête - UEFA.com
DATE: 2014-02-10
URL: http://fr.uefa.com/memberassociations/association%3Desp/news/newsid%3D2053856.html
PRINCIPAL: 158411
TEXT:
Barcelone battu
S�ville a ouvert le score par son d�fenseur Alberto Moreno apr�s 15 minutes. Mais on ensuite retrouv� un tr�s bon Barcelone, men� par un Messi qui semble de retour � son meilleur niveau.
Le petit Argentin a donn� le ballon de l'�galisation � Alexis S�nchez � la 34e minute sur un coup franc. Il a ensuite inscrit sa premi�re r�alisation avant la pause puis il r�ussissait le break de l'ext�rieur du pied apr�s la reprise et Cesc F�bregas a envoy� Barcelone en t�te du classement.
Dimanche �galement, la Real Sociedad de F�tbol a rat� une belle chance de revenir un peu plus pr�s de la cinqui�me place avec un match nul sans but devant le Levante UD, alors que le CA Osasuna a pris la 14e place en s'imposant 2-0 � domicile contre le Getafe CF.
�UEFA.com 1998-2014. All rights reserved.
Mis � jour le: 10/02/14 10.41HEC
R�sultats
