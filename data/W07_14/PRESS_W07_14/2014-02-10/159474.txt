TITRE: Des techniciens de �P�kin Express� arr�t�s en Inde - Lib�ration
DATE: 2014-02-10
URL: http://ecrans.liberation.fr/ecrans/2014/02/10/des-techniciens-de-pekin-express-arretes-en-inde_979075
PRINCIPAL: 159472
TEXT:
P�kin Express (Photo M6)
Les trois hommes ont �t� trouv�s en possession de t�l�phones satellitaires, un syst�me tr�s encadr� en Inde.
Sur le m�me sujet
En Inde, l'�quipe de �P�kin Express� a �t� lib�r�e sous caution
Trois membres d�une �quipe de tournage de l��mission de t�l�r�alit� P�kin Express ont �t� arr�t�s samedi dans l�est de l�Inde et �taient entendus pour d�tention de t�l�phones satellites non autoris�s, a indiqu� lundi la police. Cette �quipe est en tournage en Inde pour la version fran�aise de P�kin Express, diffus�e par M6, et travaillait pour la soci�t� de production Eccho Line, selon la police.
Les trois hommes ��un N�erlandais, un Belge et un Britannique�� ont �t� pr�sent�s � un juge dimanche qui a d�cid� qu�ils pourraient �tre interrog�s par la police pendant deux jours. La d�tention de t�l�phones satellites est tr�s encadr�e en Inde essentiellement pour des raisons de s�curit�, le pays devant faire face � des rebelles mao�stes ou dans le Cachemire, susceptibles d�utiliser de tels appareils. Quarante-neuf �trangers, dont notamment 33�Fran�ais et 10�Belges avaient initialement �t� arr�t�s samedi pour d�tention de 22�t�l�phones satellites dans la ville de Chalsa dans l�Etat du Bengale Occidental, selon la police.
�Trois personnes qui supervisent l��quipe de t�l�r�alit� � Chalsa ont �t� arr�t�es et pr�sent�es � la justice dimanche�, a dit Jawed Shamim, inspecteur de police dans le nord de l�Etat du Bengale Occidental, � l�AFP. �Un magistrat du tribunal de Jalpaiguri a ordonn� le maintien de ces trois personnes en garde � vue pour deux jours dimanche�, a-t-il ajout�.
P�kin Express met en comp�tition des candidats par �quipe de deux lors d�une course sur un continent. Ils ne disposent que d�un euro par jour et par personne. Initialement diffus� en Belgique et aux Pays-Bas, le jeu a �t� adapt� ensuite dans une dizaine de pays.
