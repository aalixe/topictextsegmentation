TITRE: A la Une: une nouvelle affaire d'espionnage qui concerne les Etats-Unis - France - RFI
DATE: 2014-02-10
URL: http://www.rfi.fr/ameriques/20140207-une-une-nouvelle-affaire-espionnage-concerne-etats-unis-mais-cette-fois-sont-amer/
PRINCIPAL: 160812
TEXT:
Modifi� le 07-02-2014 � 22:46
A la Une: une nouvelle affaire d'espionnage qui concerne les Etats-Unis
Victoria Nuland entour�e des leaders de l'opposition ukrainienne, le 6 f�vrier 2014 � Kiev.
REUTERS/Andrew Kravchenko/Pool
Br�sil
Une histoire qui fait la Une de nombreux quotidiens am�ricains ce vendredi. Selon le New York Times , c'est une vid�o post�e mardi sur le site de partage YouTube qui est � l'origine de cette affaire. Intitul�e les ��Marionnettes de Ma�dan��, il s'agit en fait d'une conversation entre l'ambassadeur am�ricain en Ukraine et Victoria Nuland, la secr�taire adjointe du d�partement d'Etat en charge de l'Europe.
Durant cette conversation d�un peu plus de quatre minutes (dont le lien est disponible sur le site internet du quotidien am�ricain), les deux responsables discutent de la crise politique en Ukraine. S'ils �voquent entre autres certains leaders de l'opposition et leur avenir si d'aventure ils devaient faire partie d'un gouvernement, ils parlent �galement du r�le de l'Union europ�enne dans cette crise. Et Victoria Nuland n'h�site pas � employer des mots qui font taches avec cette petite phrase�: ��Que l'Union europ�enne aille se faire foutre !��
Victoria Nuland, qui est justement en Ukraine en ce moment, a pr�sent� ses excuses hier, selon le Washington Post .
�
La porte-parole du d�partement d'Etat, Jennifer Psaki, qui n'a pas d�menti l'existence de cette conversation, met en cause les autorit�s russes qui auraient, selon elle, popularis� cette conversation sur Twitter.
�
M�me discours du c�t� de la Maison Blanche. Le porte-parole Jay Carney, selon le quotidien de la capitale, estime que cette vid�o ��diffus�e sur Twitter par le gouvernement russe est significatif du r�le de la Russie��.
�
Du c�t� de l'Union europ�enne, victime selon les r�v�lations d'Edward Snowden d'�coutes de la part des Am�ricains, on ne pr�f�re pas encore r�agir. Mais une chose est s�re : cette affaire tombe mal alors que l'Union europ�enne n�gocie actuellement avec les autorit�s am�ricaines la mise en place d'un trait� de libre-�change.
�
Jeudi, le pr�sident ha�tien Michel Martelly �tait re�u � la Maison Blanche par Barack Obama
�
Une rencontre qui fait les gros titres en Ha�ti. ��Obama optimiste sur les �lections et le d�veloppement d'Ha�ti��, titre ainsi le Nouvelliste .
�
Le quotidien �voque les progr�s relev�s par le pr�sident am�ricain sur l'organisation des �lections, report�es depuis deux ans en Ha�ti. Des progr�s li�s au vote de la loi �lectorale qui devrait permettre cette ann�e la tenue de ce scrutin tant attendu.
�
Barack Obama s'est dit �galement satisfait des progr�s r�alis�s en mati�re de s�curit�, de d�veloppement des entreprises, il a aussi parl� des infrastructures en cours de r�fection. ��Cela a �t� un processus tr�s lent et difficile��, a tout de m�me reconnu le pr�sident am�ricain qui estime qu'il reste ��beaucoup de travail � faire��.
�
Devant les cam�ras, il a assur� au pr�sident Michel Martelly l'engagement du peuple am�ricain � se tenir aux c�t�s des Ha�tiens pendant le processus.
Michel Martelly a, quant � lui, indiqu� qu'il discuterait de s�curit� et de la lutte contre le trafic de stup�fiants avec son homologue am�ricain, tout en s'engageant � ��b�tir un Etat d�mocratique fort��.
�
Au Br�sil, une manifestation organis�e � Rio de Janeiro pour protester contre la hausse du prix de ticket de bus a d�g�n�r� en affrontements avec la police
�
C'est dans les pages du quotidien O Globo . Cette manifestation a d�g�n�r� au moment o� la police �vacuait la gare de Rio, ��Central do Brasil��, en faisant usage de gaz lacrymog�ne.
Un cam�raman a �t� bless�, touch� � la t�te par un projectile explosif. Le quotidien publie d'ailleurs en Une la photo du cam�raman au moment de l'impact. Une image impressionnante. La victime a d� subir une intervention chirurgicale et, selon le quotidien, son �tat est toujours jug� grave. Six autres personnes ont �t� bless�es, dont une femme, selon O Globo.
Les manifestants r�clamaient une baisse des tarifs de bus. ��Trois r�ais, je ne paye pas��, scandaient notamment les personnes pr�sentes. Vingt personnes ont �t� arr�t�es. Malgr� ces d�bordements, d'autres manifestations devraient �tre organis�es, selon le quotidien.
Pour rappel, c'est au Br�sil que se tiendra la Coupe du monde de football du 12 juin et au 13 juillet prochain.�
