TITRE: R�f�rendum suisse anti-immigration: le pr�sident d'un comit� de frontaliers d�dramatise - 09/02/2014 - leParisien.fr
DATE: 2014-02-10
URL: http://www.leparisien.fr/strasbourg-67000/referendum-suisse-anti-immigration-le-president-d-un-comite-de-frontaliers-dedramatise-09-02-2014-3576139.php
PRINCIPAL: 158522
TEXT:
R�f�rendum suisse anti-immigration: le pr�sident d'un comit� de frontaliers d�dramatise
Publi� le 09.02.2014, 20h33
R�agir
Le pr�sident du comit� de d�fense des travailleurs frontaliers (CDTF) du Haut-Rhin, Jean-Luc Johaneck, a d�dramatis� les r�sultats du r�f�rendum suisse de dimanche exigeant de nettes restrictions au recours aux aux travailleurs �trangers, faisant valoir que le pays pouvait difficilement se passer d'eux.
Beaucoup de travailleurs �trangers sont recherch�s en Suisse parce qu'ils ont des comp�tences "tr�s sp�cifiques", que ce soit dans les secteurs de la recherche, de l'artisanat, du BTP ou du tourisme, a rappel� M.
Vos amis peuvent maintenant voir cette activit� Supprimer X
Johaneck, interrog� par l'AFP.
Il a ainsi relev� que dans les grandes m�tropoles suisses et dans les cantons frontaliers, l'initiative du parti UDC (droite populiste) "n'a pas obtenu la majorit�", bien qu'elle l'ait emport�e � 50,3% dans l'ensemble du pays.
Le texte du r�f�rendum pr�voit le r�tablissement de quotas et de contingents annuels de travailleurs �trangers, en fonction des besoins du pays, et pr�cise que "ces plafonds doivent inclure les frontaliers".
Cependant "si on appliquait vraiment un contingentement tr�s strict, cela irait m�me � l'encontre des r�sidents suisses", car bon nombre de frontaliers disposent de contrats pr�caires et sont employ�s � moindre co�t que les Suisses, a fait valoir M. Johaneck.
Le gouvernement helv�tique s'est engag� dimanche soir � mettre en oeuvre "rapidement et de mani�re cons�quente" le texte du r�f�rendum. Cependant "entre un r�f�rendum et l'application de mesures, il y a un monde", a encore estim� M. Johaneck.
