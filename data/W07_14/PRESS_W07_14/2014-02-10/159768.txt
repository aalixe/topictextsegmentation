TITRE: Malgr� les coupes, Touraine assure qu'il n'y aura pas de �d�remboursements� - 20minutes.fr
DATE: 2014-02-10
URL: http://www.20minutes.fr/politique/1294582-20140210-malgre-coupes-touraine-assure-deremboursements
PRINCIPAL: 159767
TEXT:
Marisol Touraine le 31 janvier 2014 devant l'H�tel Matignon. REVELLI-BEAUMONT/SIPA
SANTE - La ministre n'a pas avanc� d'objectif chiffr� d'�conomies...
La ministre de  la Sant�, Marisol Touraine , a affirm� lundi que la  sant� sera concern�e  par le plan de r�duction des d�penses publiques  mais a assur� qu'il n'y  aura �pas de d�remboursements� de m�dicaments  ni de �charges nouvelles� pour les patients. �La politique en mati�re  de sant� est �galement concern�e� pour la r�alisation des �conomies, a  affirm� sur France Info la ministre  qui a particip� samedi � l'Elys�e � un deuxi�me conseil  strat�gique de  la d�pense publique autour de Fran�ois Hollande.
Pas de charges pour les patients
La ministre n'a avanc� aucun objectif chiffr� d'�conomies � r�aliser  dans le domaine de la sant�: �la guerre des chiffres n'a  absolument  aucun sens, aujourd'hui aucun chiffre n'est mis sur la table  et n'a �t� avanc� dans le cadre de ce conseil strat�gique, ni en mati�re  de sant� ni de quoi que ce soit� d'autre, a-t-elle dit. Marisol Touraine affirme  avoir fix� un �cap clair: pas de  d�remboursements, pas de franchise,  pas de charges nouvelles pour les  patients fran�ais�.
D�veloppement des g�n�riques
Selon elle, il ne s'agit pas de mener une politique de �rabots�,  mais �des politiques structurantes� de �d�veloppement de  m�dicaments  g�n�riques et une meilleure ma�trise des prescriptions�. Il s'agit aussi  de donner �plus d'�lan aux soins de  proximit�, a-t-elle dit,  notamment avec la cr�ation du statut de �praticien territorial de  m�decine g�n�rale�, des m�decins qui doivent �obligatoirement  s'installer dans des territoires ou il n'y en avait  pas�. Elle a  rappel� la cr�ation en 2014 de 200 postes suppl�mentaires  de ces  praticiens, qui s'ajoutent aux 200 cr��s en 2013.
Marisol Touraine a soulign� qu'en 2013 �3,5 milliards d'euros� d'�conomies auront �t� r�alis�es dans le domaine de la sant� par rapport � 2012 gr�ce � �une politique de ma�trise des co�ts et de   r�organisation de notre syst�me, sans pratiquer de nouveaux   d�remboursements�.
Avec AFP
