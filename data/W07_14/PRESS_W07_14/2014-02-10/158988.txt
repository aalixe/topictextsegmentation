TITRE: L'OREAL : Nestl� r�fl�chit � ses options pour c�der sa part (presse) - Capital.fr
DATE: 2014-02-10
URL: http://www.capital.fr/bourse/actualites/l-oreal-nestle-reflechit-a-ses-options-pour-ceder-sa-part-presse-909486
PRINCIPAL: 158986
TEXT:
Ajouter � la liste des valeurs Ajouter au portefeuille Cr�er une alerte L'Or�al
, a r�v�l� samedi dernier l'agence Bloomberg sur la base de sources proches du dossier. Les deux groupes en auraient discut� avec leurs banquiers, mais aucune d�cision n'aurait �t� prise sur le calendrier de ce d�sengagement. Le sort de cette participation, valoris�e � 22 milliards d'euros, suscite la sp�culation � l'approche de l'�ch�ance du 29 avril 2014.
L'an dernier, Nestl� a en effet pr�venu qu'il ne prolongerait pas le droit de pr�emption qui le liait � la famille Bettencourt, premier actionnaire de L'Or�al avec 30,5% du capital, expliquant que ce droit avait �t� synonyme de "perte de valeur dans le pass�" et qu'en tant qu'investisseur "Nestl� n'y avait aucun int�r�t".
A partir du 29 avril prochain, le g�ant suisse sera libre de disposer de sa part comme il le souhaite. Il pourrait clarifier ses intentions le 13 f�vrier, � l'occasion de la publication de ses r�sultats annuels.
AOF - EN SAVOIR PLUS
Les points forts de la valeur
- Leader mondial des cosm�tiques avec 15 % du march� global, 27 marques mondiales r�alisant un chiffre d'affaires sup�rieur � 500 millions d'euros, une pr�sence dans 130 pays ;
- R�partition �quilibr�e des divisions cosm�tiques entre les produits grand public (50,9 % des ventes), les produits de luxe (28 %), les produits professionnels (14 % des ventes) et la cosm�tique active (7 %), auxquelles s'ajoutent The Body Shop et la dermatologie ;
- Positions fortes en Europe de l'Ouest et en Am�rique du nord avec une croissance des ventes r�guli�rement sup�rieure � celle du march� ;
- Croissance rapide sur les march�s �mergents (40% des ventes), Cor�e, Ta�wan et Philippines except�es, avec pour objectif de devenir num�ro un en Chine devant Procter & Gamble et d'accro�tre la part du march� indien, gr�ce notamment � la marque Maybelline ;
- Evolution du mod�le �conomique avec de nouvelles gammes de produits � des prix attractifs et un renforcement dans les cosm�tiques pour hommes (9 % des ventes du groupe) ;
- Investissements �lev�s en R&D ;
- Marge op�rationnelle � des niveaux record ;
- Politique d'acquisitions de petite taille mais � fort potentiel (Decleor et Carita � l'automne 2013), d�velopp�es par des investissements lourds en marketing, R&D et innovation ;
- Structure financi�re exceptionnellement saine ;
- Plan de rachat d'action en 2013 et hausse de 15 % du dividende.
Les points faibles de la valeur
- Relative faiblesse de la division dermatologie et stagnation des produits professionnels ;
- Faible rendement et valorisation toujours �lev�e en Bourse ;
- Ralentissement de la croissance des ventes au 3�me trimestre 2013 ;
- Absence de guidances chiffr�es dans les objectifs annuels.
Comment suivre la valeur
- Valeur de � fonds de portefeuille � ;
- Forte sensibilit� au cours du dollar, du r�al br�silien, du peso mexicain et du yuan chinois ;
- Sensibilit� boursi�re aux annonces du japonais Shisheido et de l'am�ricain Estee Lauder ;
- Evolution de l'OPA amicale d'un montant global de 630 MEUR annonc�e en ao�t 2013 sur Magic Holding, soci�t� cot�e � Hong-Kong et r�alisant 150 MEUR de ventes en Chine ;
- R�ponse des consommateurs aux nombreux lancements orchestr�s sur la fin 2013 ;
- Reprise des rumeurs autour de la fin du pacte d'actionnaires liant jusqu'� avril 2014 la famille Bettancourt (30,5 % des titres) et Nestl� ( 29,5 %), malgr� la confirmation par la premi�re de son attachement au groupe, et le refus du second de communiquer avant l'an prochain ;
- Sp�culations sur un rachat par L'Or�al de tout ou partie de la participation de Nestl�, qui serait financ� par une vente de la position dans Sanofi.
LE SECTEUR DE LA VALEUR
Luxe et cosm�tiques
Le cuir est un enjeu strat�gique pour les fabricants suite � l'explosion de la demande mondiale pour les sacs ou les chaussures de luxe. Cette tendance cr�e des tensions sur le march�, renforc�es par le manque de mati�res qui r�pondent aux crit�res du luxe. Depuis deux ans en France un mouvement de rachat de tanneries et m�gisseries s'est engag�. De m�me que ses rivaux, notamment LVMH, Herm�s a repris en janvier dernier la tannerie d'Annonay, sp�cialis�e dans les peaux de veau, avec l'objectif de s�curiser ses approvisionnements. C'est une premi�re pour le fabricant fran�ais, qui d�tient deux tanneries en France, une en Italie et une autre en Louisiane. Elles travaillent exclusivement les peaux pr�cieuses (alligators).
DERNIERE RECOMMANDATION SUR LA VALEUR
HSBC a relev� son objectif de cours sur L'Or�al de 121 � 146 euros tout en r�it�rant sa recommandation Surpond�rer en raison du potentiel d'am�lioration de rentabilit� du groupe et de la flexibilit� de son bilan.
CONSENSUS DES PROFESSIONNELS
D'apr�s le consensus de march� calcul� � la date du 05/02/2014, les analystes conseillent de rester neutre sur le titre L'OREAL. En effet, sur un total de 10 bureaux d'�tudes ayant fourni des estimations, 5 sont � l'achat, 2 sont � la vente et 3 sont neutres. L'indice de recommandation AOF, refl�tant l'avis moyen des analystes et s'�tendant de -100% � +100%, est de 30%. Enfin, l'objectif de cours moyen est de 130,6 EUR. Le consensus pr�c�dent conseillait d'acheter la valeur .
2014 AOF - Tous droits de reproduction r�serv�s par AOF.
