TITRE: Taxis en col�re. Forte mobilisation contre les VTC
DATE: 2014-02-10
URL: http://www.ouest-france.fr/greve-plusieurs-centaines-de-taxis-roissy-et-orly-contre-les-vtc-1921439
PRINCIPAL: 160060
TEXT:
Taxis en col�re. Forte mobilisation contre les VTC
Paris -
Achetez votre journal num�rique
Des centaines de taxis manifestent une nouvelle fois, � Paris, pour d�noncer la concurrence d�loyale des VTC.
Plusieurs centaines de taxis sont rassembl�es ce lundi matin aux a�roports de Roissy et d'Orly � l'occasion d'une manifestation nationale contre les voitures de tourisme avec chauffeur (VTC), accus�s de concurrence d�loyale.
Les cort�ges, r�unis � l'appel de cinq syndicats (CFDT, CGT, FO, SDCTP et CST), doivent quitter les a�roports parisiens vers 08 h 00, pour converger au Champ-de-Mars, dans le centre de Paris, o� ils stationneront toute la journ�e.
D�lai de 15�minutes
Les VTC, d�pourvus de signal�tique lumineuse, peuvent �tre r�serv�s pour une course mais n'ont en th�orie pas le droit de prendre des passagers � la vol�e dans la rue. Selon les taxis, ils op�rent toutefois r�guli�rement sans r�servation.
Face � la grogne des taxis, le gouvernement avait publi� le 27�d�cembre 2013 un d�cret imposant un d�lai obligatoire de 15 minutes entre la r�servation et la prise en charge du client par le VTC.
Mais le Conseil d'�tat a suspendu mercredi ce d�cret, estimant qu'il portait "une atteinte grave et imm�diate aux int�r�ts �conomiques" des soci�t�s de VTC. La plus haute juridiction administrative doit d�sormais examiner le dossier sur le fond.
Pour tenter de trouver un terrain d'entente entre taxis et VTC, le gouvernement a annonc� samedi la mise en place d'une "mission de concertation", destin�e � "d�finir les conditions durables d'une concurrence �quilibr�e entre les taxis et les VTC".
Lire aussi
