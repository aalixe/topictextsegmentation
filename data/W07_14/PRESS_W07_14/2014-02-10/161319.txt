TITRE: Biathlon - JO - Martin Fourcade : �Du pur bonheur�
DATE: 2014-02-10
URL: http://www.lequipe.fr/Biathlon/Actualites/Fourcade-du-pur-bonheur/440227
PRINCIPAL: 161316
TEXT:
Martin Fourcade a pu laisser exploser sa joie. (L'Equipe)
� Martin Fourcade, comment avez-vous v�cu le dernier tir ?
Je savais pendant le dernier tir que si je faisais un sans-faute je serais champion olympique . A ce moment, c�est du pur bonheur. Les gens m�ont demand� si c��tait en r�action au public qui �tait ravi quand je ratais des cibles mais cela n�a rien � voir. Ce n'est pas de l'arrogance. C��tait seulement pour exprimer mon �motion. Je suis tout simplement heureux, j�ai partag� avec les gens qui m�ont aid�. ,Vous savez, je n�y crois toujours pas. C�est incroyable. Tout le monde s�attendait � ce que je r�agisse apr�s le sprint mais je suis tr�s, tr�s fier de l�avoir fait.
Pourquoi vous �tiez-vous si s�r de vous ?
Je suis � la bagarre avec tous ces gars tout au long de la saison, tous les week-ends. On se conna�t tr�s bien. Je savais que le dernier tir sans erreur allait me donner le titre olympique. J��tais seul sur le pas de tir. J�ai entendu que Dominik Landertinger avait manqu� une cible. Je savais que j�avais les cartes en main. Je savais que j��tais champion olympique avant le dernier tour mais il fallait aller jusqu�� la ligne d�arriv�e. Dans la derni�re boucle, je n�ai pens� qu�� ne pas perdre. Je voulais aller jusqu�� la ligne et garder des forces pour les prochaines courses.
Quels sont vos r�ves d�sormais ?
Avant m�me aujourd�hui, j�avais d�j� gagn� plus de courses que dans mes r�ves les plus fous. J�esp�re que �a va continuer comme �a. Je suis un homme heureux. J�adore le sport et je veux continuer � gagner, de garder la m�me vie.
Quand avez-vous su que Jean-Guillaume B�atrix �tait aussi � la lutte pour une m�daille ?
Je l�ai su quand j�ai fait le demi-tour pour rejoindre la ligne d�arriv�e. Je n�y avais pas pens� une seule seconde. C��tait une belle surprise. Je savais que Moravec �tait derri�re moi mais les coaches ne m�avaient pas renseign� sur Jean-Gui. Je suis super content. Sans aucune hypocrisie, c�est mon meilleur adversaire depuis que j�ai 15 ans. Il y a beaucoup d�amiti� et de rivalit� depuis cette �poque. Quand il est devenu champion du monde juniors, �a m�a fait quelque chose. Dans la construction de ma carri�re, Jean-Gui m�a beaucoup aid�. Je suis tr�s satisfait pour lui, pour le biathlon fran�ais. On ouvre magnifiquement bien le compteur.
Est-ce un soulagement aussi ?
J��tais d��u apr�s le sprint. M�me si personne ne m�avait demand�, je sentais que j'avais la responsabilit�. d�ouvrir ce compteur pour ne pas en entendre parler pendant une semaine. J�y ai pens� ce matin. Je me suis dit : ''Si tu ne le fais pas pour toi, fais le pour les autres. Sinon, tout le monde va� bien les saouler.''
�a fait deux ans que je dis que je veux �tre champion olympique. Tout le monde me dit : ''Ah, tu devrais te mettre un peu en retrait. Ce n'est pas fran�ais de dire qu'on veut gagner.'' Cela fait un moment que vous annonciez cet objectif du titre olympique. Vous le concr�tisez enfin.
Le dire, ce n�est pas si facile quand on a des ambitions. �a fait deux ans que je dis que je veux �tre champion olympique. Tout le monde me dit : ''Ah, tu devrais te mettre un peu en retrait. Ce n�est pas fran�ais de dire qu�on veut gagner.'' Il y a z�ro rancoeur. Je suis tout simplement heureux et je veux partager �a.
Et maintenant, comment voyez-vous la suite de la quinzaine ?
Pour la suite, cette m�daille va va jouer. Je suis d�tendu, je vais essayer de surfer sur la vague. Il peut se passer la m�me chose sur l�individuelle, la mass-start ou les relais. Je n�avais pas peur de ne pas �tre champion olympique. Je connais les r�gles du sport. On peut dominer son sport pendant 2 ans et rentrer bredouille d�un grand �v�nement. J�avais plus peur de rentrer sans m�daille. Dans ce cas-l�, j�aurais rat� quelque chose.�
Recueilli par Marc VENTOUILLAC, � Laura
