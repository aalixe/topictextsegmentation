TITRE: La remise de la palme d'or 2014 avanc�e d'un jour - News Cin�ma
DATE: 2014-02-10
URL: http://www.canalplus.fr/c-cinema/c-l-actu-du-cinema/cid1018092-la-remise-de-la-palme-d-or-2014-avancee-d-un-jour.html
PRINCIPAL: 160699
TEXT:
La remise de la palme d'or 2014 avanc�e d'un jour
Cannes
Commentaires
Le palmar�s du festival de Cannes 2014 sera annonc� un jour plus t�t que pr�vu en raison des �ch�ances europ�ennes du dimanche 25 mai.
La direction du festival de Cannes a d�cid� d'avancer la�c�r�monie de cl�ture au Samedi 24 mai au lieu du dimanche 25 mai � cause des �l�ctions europ�ennes. L'annonce du palmar�s se fera donc un jour plus t�t au Grand Th��tre Lumi�re de Cannes.
Vid�o : Cannes 2013 - Best of C�r�monie de cl�ture
Best of de la C�r�monie de cl�ture du 66�me Festival de Cannes 2013 qui a vu r�compenser les films fran�ais LE PASSE (avec B�r�nice B�jo) et LA VIE D'ADELE (Palme d'Or).
Cannes 2013 - Best of C�r�monie de cl�ture
