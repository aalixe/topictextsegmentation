TITRE: Une femme gri�vement bless�e et 200 personnes �vacu�es lors d'un incendie � Val-d'Is�re
DATE: 2014-02-10
URL: http://www.lemonde.fr/societe/article/2014/02/10/une-femme-grievement-blessee-et-200-personnes-evacuees-lors-d-un-incendie-a-val-d-isere_4363743_3224.html
PRINCIPAL: 160977
TEXT:
Une femme gri�vement bless�e et 200 personnes �vacu�es lors d'un incendie � Val-d'Is�re
Le Monde |
� Mis � jour le
10.02.2014 � 19h03
Une femme s'est gri�vement bless�e en sautant du 5e��tage d'un immeuble en feu dans la station de ski de Val-d'Is�re (Savoie), a-t-on appris lundi aupr�s de la pr�fecture. Son pronostic vital n'est pas engag�. Elle a �t� transport�e � l'h�pital de Bourg-Saint-Maurice. Son �ge n'a pas pu �tre pr�cis� dans l'imm�diat.
DEUX CENTS PERSONNES �VACU�ES
L'incendie, d'origine inconnue, s'est d�clar� au 5e��tage d'un immeuble communal de six niveaux, de type chalet en bois, lundi vers 13 heures. Huit autres personnes ont �t� l�g�rement bless�es par intoxication et 200�personnes ont �t� �vacu�es vers le centre des congr�s de Val-d'Is�re, dont des r�sidents d'un club de vacances mitoyen. Une �cole primaire situ�e � proximit� a aussi �t� �vacu�e par pr�caution.
Bad appartment fire in Val d'isere. pic.twitter.com/pxTE3zSsho
� Alasdair McKinnon (@Al_McKinnon) February 10, 2014
L'immeuble en feu abrite habituellement une quarantaine de saisonniers travaillant sur la station pendant l'hiver, selon le maire de Val-d'Is�re, Marc Bauer. ��Notre souci, �a va �tre de reloger les gens pour la totalit� de la saison��, a-t-il indiqu� � l'AFP. Une soixantaine de pompiers ont �t� mobilis�s pour �teindre l'incendie, qui n'�tait toujours pas ma�tris� lundi en milieu d'apr�s-midi.
Soci�t�
