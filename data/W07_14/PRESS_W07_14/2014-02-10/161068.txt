TITRE: Pr�occup�s par le blanchiment, les Russes veulent mieux contr�ler le bitcoin
DATE: 2014-02-10
URL: http://www.lemonde.fr/technologies/article/2014/02/10/preoccupes-par-le-blanchiment-les-russes-veulent-mieux-controler-le-bitcoin_4363730_651865.html
PRINCIPAL: 0
TEXT:
La Russie veut renforcer la lutte contre le bitcoin
Le Monde |
� Mis � jour le
11.02.2014 � 10h20
De plus en plus pr�occup�es par le r�le croissant des monnaies �lectroniques dans le blanchiment d'argent, les autorit�s russes ont d�cid� de renforcer la lutte contre les monnaies virtuelles, notamment le bitcoin.
Lire aussi : L'op�rateur d'une bourse d'�change de bitcoins arr�t� pour blanchiment
Dans un communiqu� publi� lundi, le parquet g�n�ral s'est dit inquiet de ��la croissance de l'int�r�t pour les monnaies virtuelles dans des buts de blanchiment de fonds obtenus par des voies criminelles��. L'institution indique avoir organis� une table ronde sur le sujet avec des repr�sentants de la banque centrale, du minist�re de l'int�rieur et des services de renseignement, au cours de laquelle il a �t� d�cid� de prendre ��des mesures communes concr�tes��, non pr�cis�es.
UNE MONNAIE QUI �CHAPPE AUX SYST�MES BANCAIRES
Le parquet g�n�ral rappelle qu'en vertu de la l�gislation en vigueur en Russie le rouble est la seule devise valable en Russie et que l' emploi de ��moyens de paiement anonymes et de monnaies virtuelles, dont la plus c�l�bre d'entre elles, le bitcoin��, est interdit. Il ajoute que le bitcoin pr�sente ��un grand risque de perte�� en raison d'une ��valeur exclusivement sp�culative��.
En janvier d�j�, la banque centrale russe avait mis en garde contre le caract�re ��sp�culatif�� du bitcoin. Invent�e en 2009 et �mise � partir de codes informatiques complexes, cette monnaie peut �tre stock�e dans des portefeuilles �lectroniques et �chang�e de gr� � gr� via des plateformes sur Internet contre des devises r�elles, sans passer par le syst�me bancaire.
OPACIT� D'UN SYST�ME MONDIALIS�
Il est parfois pr�sent� comme l'argent du crime en raison de l'opacit� suppos�e de ce syst�me. Le lobby de la finance internationale, l'IIF, a jug� que l' avenir de cette devise �tait compromis par la difficult� de la r�guler et par sa ��tr�s forte volatilit頻. Il avait perdu en d�cembre plus de la moiti� de sa valeur en trois jours apr�s avoir �t� banni des �tablissements financiers chinois.
Lire aussi : Une plateforme indienne suspend les �changes en bitcoin
Les paiements �lectroniques et les porte-monnaie virtuels constituent un march� en croissance tr�s rapide en Russie. Ils attisent la convoitise dans le secteur bancaire mais suscitent la m�fiance croissante des autorit�s, qui y voient un canal de blanchiment pris� du crime organis� et des terroristes gr�ce � l'anonymat.
Un projet de loi sur le terrorisme pr�voyant de fortes restrictions sur les transferts a �t� introduit en janvier � la Douma (chambre basse du Parlement) et doit �tre examin� dans les semaines � venir . Il a provoqu� l'inqui�tude des principaux acteurs du secteur financier.
Bitcoin et monnaies virtuelles
