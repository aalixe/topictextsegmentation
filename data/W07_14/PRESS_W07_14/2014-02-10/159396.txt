TITRE: Les taxis en gr�ve � Paris, face aux VTC | La-Croix.com
DATE: 2014-02-10
URL: http://www.la-croix.com/Actualite/Economie-Entreprises/Economie/Les-taxis-en-greve-a-Paris-face-aux-VTC-2014-02-10-1103799
PRINCIPAL: 159389
TEXT:
petit normal grand
Les taxis en gr�ve � Paris, face aux VTC
Des centaines de taxis manifestent ce lundi � Paris, accusant les voitures de tourisme avec chauffeur (VTC) de concurrence d�loyale.
9/2/14 - 17 H 04
- Mis � jour le 10/2/14 - 10 H 04
Mots-cl�s :
�Les taxis perdent une manche contre les VTC
Le gouvernement avait annonc� samedi une mission de concertation, apr�s la suspension par le Conseil d��tat de ses pr�c�dentes d�cisions concernant les VTC.
Les chauffeurs de taxi sont en gr�ve, ce lundi 10 f�vrier, � Paris. Le ministre de l�int�rieur Manuel Valls et la ministre de l�artisanat et du tourisme, Sylvia Pinel, avaient annonc� samedi 8�f�vrier la mise en place ��dans les prochains jours���d�une ��mission de concertation���dans le diff�rend opposant les taxis et les voitures de tourisme avec chauffeur (VTC). Ils pr�voient aussi des contr�les renforc�s. La mission aura pour objectif de ��d�finir les conditions durables d�une concurrence �quilibr�e entre les taxis et les VTC��. Elle devra faire des propositions sous deux mois pour ��instaurer un dispositif �quilibr� et durable, au b�n�fice de chacun (des) acteurs, en prenant en compte la satisfaction des diff�rents besoins de transports individuels��.
Une personnalit� reconnue
La mission sera pr�sid�e par ��une personnalit� reconnue���et associera ��l�ensemble des acteurs��, notamment les organisations professionnelles, les collectivit�s locales et les repr�sentants des usagers. Elle devra prendre en compte ��l�ensemble des modes de transport particuliers de personnes��, ajoute le communiqu�, qui cite les taxis, les voitures de tourisme avec chauffeur et le transport de personnes � moto. La mission devra examiner les besoins de transports de moins de 10 personnes, les contraintes techniques, r�glementaires et concurrentielles, et la situation particuli�re de l��le-de-France.
Contr�les renforc�s
Outre la cr�ation de cette mission, le gouvernement annonce ��des contr�les renforc�s���dans les prochains jours sur le respect de la r�glementation par les diff�rents acteurs. S�agissant des voitures de tourisme avec chauffeur, ��il sera notamment v�rifi� le respect de la r�servation pr�alable, de la facturation forfaitaire ou le non-stationnement aux abords des gares ou des a�roports��, selon le communiqu�.
Les minist�res rappellent que le gouvernement a adopt� une s�rie de mesures pour encadrer le transport de personnes, dont un d�lai minimum de 15 minutes pour les VTC entre la r�servation et la prise en charge du client. Cette mesure a �t� suspendue par le Conseil d��tat mercredi. Le gouvernement ��prend acte de cette d�cision de justice qui ne remet aucunement en cause sa volont� de garantir les conditions p�rennes d�une concurrence �quilibr�e���dans le transport de personnes.
Covoiturage � but lucratif
Par ailleurs, apr�s l�annonce du lancement par la soci�t� am�ricaine Uber d�un nouveau service � Paris utilisant des v�hicules de particuliers, le service de la r�pression des fraudes (DGCCRF) a annonc� ce m�me vendredi qu�il allait enqu�ter sur le covoiturage ��r�alis� dans un but lucratif���par des particuliers.
Si le covoiturage visant � partager les frais est l�gal et ���cocitoyen��, ��le transport de passagers, sous couvert de covoiturage, r�alis� dans un but lucratif est ill�gal��, car il constitue ��une activit� de transport public non autoris�e��, souligne le minist�re de l��conomie. ��Les personnes qui s�engageraient dans cette activit�, notamment via des sites de mise en relation, s�exposeraient � des sanctions p�nales��, �crit Bercy sans toutefois citer Uber ou des sites de covoiturage.
Pratique commerciale trompeuse
���L�entretien d�une confusion entre le covoiturage licite et un service de transport � but lucratif constitue une pratique commerciale trompeuse��, souligne encore le minist�re, qui rappelle que le nouveau projet de loi relatif � la consommation porte � 1,5�million d�euros l�amende encourue. Cette annonce, qui intervient en plein conflit entre les taxis et les soci�t�s de voitures de tourisme avec chauffeur (VTC) comme Uber, semble viser en particulier le nouveau service de la jeune soci�t� am�ricaine � Paris.
Ce service pr�voit que les particuliers puissent s�improviser chauffeurs � bord de leur propre v�hicule, � condition d�avoir 21�ans, le permis de conduire depuis 3�ans minimum et un casier judiciaire vierge. Le particulier-chauffeur serait r�mun�r� 4�� minimum par trajet, plus 35 centimes par minute ou 80 centimes par kilom�tre, un mode de r�mun�ration proche de celui des taxis.
La Croix (avec AFP)
