TITRE: Etats-Unis: les couples homosexuels obtiennent l��galit� de droits f�d�raux - France - RFI
DATE: 2014-02-10
URL: http://www.rfi.fr/ameriques/20140209-etats-unis-couples-homosexuels-obtiennent-egalite-droits-federaux?ns_campaign%3Dgoogle_choix_redactions%26ns_mchannel%3Deditors_picks%26ns_source%3Dgoogle_actualite%26ns_linkname%3Dameriques.20140209-etats-unis-couples-
PRINCIPAL: 160417
TEXT:
Modifi� le 09-02-2014 � 15:12
Etats-Unis: les couples homosexuels obtiennent l��galit� de droits f�d�raux
par RFI
Des partisans de l'�galit� des droits, devant le si�ge du capitole de l'Utah, � Salt Lake City.
REUTERS/Jim Urquhart
Etats-Unis
A partir de lundi 10 f�vrier, les couples homosexuels pourraient pr�tendre devant les tribunaux f�d�raux aux m�mes droits que les couples h�t�rosexuels. Le ministre am�ricain de la Justice, Eric Holder, l�a annonc� lors d�un discours, � New York, devant les membres d�une organisation de d�fense des droits des homosexuels.
Avec notre correspondant � Washington, Jean-Louis Pourtet
La nouvelle mesure concerne essentiellement le domaine juridique et va surtout profiter aux couples gays r�sidant dans les 34 Etats qui ne reconnaissent pas le mariage pour tous. Si, par exemple, un tel couple mari� l�galement au Massachusetts va s�installer en Alabama -�o� le mariage gay n�est pas reconnu�-,  et qu�� la suite de difficult�s financi�res, il veut se d�clarer en faillite, il pourra d�sormais pr�senter un dossier commun.
De m�me, si l�un des deux conjoints est emprisonn�, l�autre pourra lui rendre visite. Parmi les autres droits, les conjoints homosexuels pourront b�n�ficier de la pension alimentaire ou encore des allocations de veuvage pour les pompiers et les policiers tu�s en exercice.
Une �volution rendue possible par la Cour supr�me
Tout cela n�aurait pas �t� possible sans la d�cision de la Cour supr�me, qui a jug� que la loi qui ne reconnaissait que le mariage entre un homme et une femme �tait inconstitutionnelle. Depuis, le Tr�sor autorise les couples homosexuels � faire une d�claration d�imp�ts conjointe et le minist�re de la D�fense leur accorde les m�mes prestations sociales qu�aux h�t�rosexuels.
Le ministre de la Justice, Eric Holder, a compar� la bataille que m�ne les homosexuels � celle des noirs pour leurs droits civiques, dans les ann�es 1960, citant le r�le jou� par Robert Kennedy, alors garde des Sceaux. ��Tout fonctionnaire de la justice devra s�assurer que les couples de m�me sexe b�n�ficient des m�mes privil�ges, protections et droits que les couples h�t�rosexuels mari�s��, a-t-il d�clar� lors du gala de l�Association pour la d�fense des droits des gays. R�action de son pr�sident�:���Cette annonce historique va transformer, pour le meilleur, la vie de nombreux couples homosexuels.��
