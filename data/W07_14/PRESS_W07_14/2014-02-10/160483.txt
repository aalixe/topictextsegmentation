TITRE: Flappy Bird : le petit oiseau se cache pour mourir
DATE: 2014-02-10
URL: http://www.itespresso.fr/flappy-bird-petit-oiseau-cache-mourir-72562.html
PRINCIPAL: 160477
TEXT:
Vous �tes ici : Accueil / Solutions IT / Flappy Bird : le petit oiseau se cache pour mourir
Flappy Bird : le petit oiseau se cache pour mourir
Un d�veloppeur vietnamien stoppe l�exploitation d�un jeu sur mobile Flappy Bird qui a atteint un sommet en termes de popularit�. Il aurait craqu� devant le succ�s.
Donnez votre avis
La long�vit� des volatiles sur smartphones varie consid�rablement : ainsi, Flappy Bird sera plus �ph�m�re que les Angry Birds de Rovio. Dans la cat�gorie ��casual gaming��, le jeu mobile Flappy Bird a rencontr� un �norme succ�s depuis sa cr�ation en 2013. Le concept est banal au d�part (le joueur doit aider le petit oiseau � se faufiler entre des tuyaux verts en tapotant sur l��cran de son smartphone) mais cela se complique progressivement au point de devenir un accroc du jeu.
On consid�re que le nombre de t�l�chargement de cette application gratuite varie entre 2 et 3 millions par jour � la fois sur l�AppStore d� Apple mais aussi Google Play. Nguyen Ha Dong, du nom du d�veloppeur vietnamien � l�origine de Flappy Bird, consid�re que ce jeu g�n�re 50 000 dollars de revenus publicitaires au quotidien.
Mais il semblerait que Nguyen Ha Dong ait craqu�. Le jeu a �t� retir� des deux principales places de march� d�applications. ��Flappy Bird est mon succ�s mais il a aussi ruin� ma vie simple. Alors je le d�teste maintenant��, explique le cr�ateur via Twitter ( @dongatory ). ��Je suis d�sol�, utilisateurs de Flappy Bird, dans 22 heures, je vais retirer Flappy Bird. Je ne peux plus supporter �a.��
Petit pr�caution toujours apport�e par Twitter : ��Je ne vends pas Flappy Bird.�� �Difficile d�expliquer ce burning-out de Nguyen Ha Dong. On a �voqu� des soucis judiciaires avec Nintendo li�s � l�exploitation du jeu mobile. Mais le d�veloppeur du Vietnam a balay� cet argument. ��Ce n�est pas du tout li� aux questions juridiques. Je ne peux simplement plus le garder��.
En tout cas, ce succ�s risque de susciter quelques convoitises voire de pousser � l��mergence de clones pour reprendre le concept du jeu devenu populaire.
Flappy Bird, tu vas manquer � beaucoup de joueurs sur smartphones
R�dacteur en chef ITespresso.fr Lire mes autres articles
Autres articles sur ce sujet
