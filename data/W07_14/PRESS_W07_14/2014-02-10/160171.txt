TITRE: Omnisport | NFL : Un candidat � la draft fait son coming-out
DATE: 2014-02-10
URL: http://www.le10sport.com/omnisport/nfl-un-candidat-a-la-draft-fait-son-coming-out135508
PRINCIPAL: 160166
TEXT:
Envoyer � un ami
Annonc� dans les premiers tours de la prochaine draft NFL, le joueur de ligne d�fensive de l�universit� du Missouri, Michael Sam, a r�v�l� son homos�xualit� dans une interview accord�e � ESPN�: � Je voulais �tre certain de raconter mon histoire de la mani�re dont je le souhaitais. Je ne suis pas na�f, je suis conscient de l'importance de cette d�claration. Mais, le plus important, �a reste de m'entra�ner pour les tests en vue de la prochaine draft et de pouvoir jouer en NFL. � S�il est bien draft� en mai prochain, il deviendra le premier joueur NFL � avoir fait son coming-out.
