TITRE: Fran�ois Hollande aux �tats-Unis pour y vendre la France... Et lui-m�me - Economie Matin
DATE: 2014-02-10
URL: http://www.economiematin.fr/eco-digest/item/8508-visite-etat-unis-francois-hollande-france
PRINCIPAL: 158325
TEXT:
E-mail
Ce n'est que la sixi�me visite d'Etat que Barack Obama organise depuis le d�but de sa pr�sidence en 2008. Flickr/nabekor
On ironisait, � l'�poque, sur Sarkozy l'Am�ricain pour sa fascination pour la culture d'outre-Atlantique. Mais en r�alit�, il n'a jamais �t� recu dans le Bureau Ovale dans le cadre d une visite d'Etat, ni par George Bush ni par Barack Obama.
�
Le tapis rouge � la Maison Blanche, une premi�re pour un pr�sident fran�ais depuis 1996
�
�
Or c'est � cet honneur que Fran�ois Hollande, et � travers lui la France, a droit : le voil� qui part pour trois jours aux Etats-Unis (cela faisait dix-huit ans, depuis Jacques Chirac en 1996 pr�cis�ment, qu'aucun pr�sident fran�ais n'avait �t� invit� � la Maison Blanche en visite d'Etat, bien plus important qu'une simple visite officielle). Objectif : vendre le made in France... et son pr�sident. Bref, partir pour mieux revenir ? The Economist , magazine (britannique) devenu expert�en mati�re de french-bashing- parle d'une visite "qui donne une chance au pr�sident d'am�liorer l'image de son pays"�et la n�ccessit� pour lui "de montrer aux Am�ricains que la France ne fait pas partie du pass�''. Challenge accepted !
�
"Je soup�onne le Pr�sident Hollande de vouloir une visite avec les honneurs (''a high-profile visit'') qui le montre comme l'homme le plus puissant du monde, parce qu'il est au plus bas dans les sondages en France, dans tous les sondages, et ce depuis la cr�ation des sondages", ironise dans �USA Today un professeur de relations internationales de l'Universit� de Boston.
Sur la sc�ne internationale, "le pr�sident s'est forg� l'image d'un grand dirigeant. La d�termination avec laquelle il a g�r� les interventions au Mali et en Centrafrique, et son traitement des dossiers syrien et iranien, ont impressionn� aux Etats-Unis'' assure la correspondante des Echos a New-York .
�
�
�
Au menu : rencontre avec des patrons de grandes entreprises am"ricaines puis vol sur la c�te ouest pour de nouvelles rencontres, cette fois avec des entrepreneurs stars de la Silicon Valley comme les patrons de Google -� qui le fisc francais veut infliger un redressement fiscal d'1 milliard d'euros- Facebook, Twitter, etc.
�
Un d�placement digne du patron du Medef, ironiseront certainement certains ! Le chef de l'Etat soigne en tout cas son image de pr�sident proche des entreprises, ayant semble-t-il (enfin !) compris qu'elles sont le nerf de la guerre.
�
Le souci, c'est qu'aux Etats-Unis, son revirement soudain ne fait pas oublier qu'il est "celui qui a cr�� la taxe � 75% sur les millionnaires". Et ca, ca ne passe pas...�
�
