TITRE: Suisse: le vote limitant l'immigration inqui�te en Europe - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/international/immigration-suisse-presse-europeenne-sinterroge-consequences-vote-706574.html
PRINCIPAL: 158622
TEXT:
r�agir
En votant dimanche par r�f�rendum pour poser des freins � l'immigration dans leurs pays, les Suisses ont adress� � l'Union europ�enne un cinglant d�saveu. Mais quelles cons�quences cette d�cision pourrait avoir pour les Helv�tes dans leur relation avec leurs voisins europ�ens et avec l'Union? D'autant que la Suisse fait encore partie de l'espace Schengen et respectait, � ce titre, un accord libre circulation. Face � cette d�cision, la presse europ�enne est tr�s partag�e.
L'opinion chez le voisin fran�ais
"Les Suisses votent pour l'instauration de quotas", annonce sobrement en une Le Figaro. De son c�t�, Lib�ration juge qu'avec ce r�sultat, "les Suisses ferment les fronti�res". Le politologue Jean-Yves Camus explique dans ses colonnes que "l'�go�sme �conomique est la principale composante de ce vote". Pour Les Echos, ce vote �voque "un r�sultat aux cons�quences �conomiques difficiles � pr�voir".
Tandis que L'Humanit� d�nonce "l'extr�me droite de Blocher (qui) a remport� son pari en faisant adopter un drastique plafonnement de l'immigration".
Chez le voisin allemand
En Allemagne, le journal conservateur Die Welt estime que "la Suisse doit revoir sa proximit� avec l'UE". "Continuer comme �a n'est pas une option. L'UDC va apprendre � quel point il est difficile de s�parer les aspects positifs des contrats bilat�raux avec Bruxelles des aspects non d�sir�s", souligne le quotidien.
En revanche, le Berliner Zeitung se montre plus compr�hensif: "Ceux qui se moquent ici de la peur d'�tre envahi par les �trangers devraient au moins r�fl�chir au fait qu'en Suisse, le pourcentage des �trangers est, avec 23% de la population totale, presque trois fois plus important qu'en Allemagne."
En Belgique et en Espagne
Le quotidien belge Le Soir titre de fa�on provocatrice: "Les Suisses aux Europ�ens: dehors!" Le quotidien francophone note que "c'est tout l'�chafaudage des accords bilat�raux de la Suisse avec l'Union europ�enne qui est assur� de s'�crouler, pour 19.516 voix sur pr�s de 3 millions de votants".
En Espagne, le quotidien de centre-gauche El Pais juge sur son site que "ce r�sultat contraindra l'Union europ�enne � repenser son �troite relation avec la Suisse et met fin � libre circulation des personnes en vigueur depuis 2002". Ce r�f�rendum "ouvrira une crise politique s�rieuse entre les deux interlocuteurs".
Dans sa version papier, El Pais publie un �ditorial intitul� "Cons�quences perverses" dans lequel il affirme que "la victoire des opposants � 'l'immigration de masse' en Suisse aura des cons�quences pour tout le monde en Europe". "Non seulement cela remet en question l'accord sur la libre circulation des personnes �tabli avec l'UE, mais cela refl�te aussi l'agitation populiste et x�nophobe parcourant le Vieux Continent moins de trois mois avec les �lections europ�ennes". "Il s'agit l� du pire r�sultat possible pour la majorit� des hommes politiques et les entreprises suisses".
Pour le quotidien conservateur ABC, ce r�f�rendum "met en danger les liens (de la Suisse) avec l'Union europ�enne". "Ce r�sultat a surpris la classe politique et repr�sente un camouflet pour la politique europ�enne du Conseil f�d�ral, qui doit maintenant repenser ses relations politiques avec l'UE".
A lire aussi
