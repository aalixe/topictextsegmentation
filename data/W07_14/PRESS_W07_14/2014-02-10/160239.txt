TITRE: kipa/apic
DATE: 2014-02-10
URL: http://kipa-apic.ch/index.php?pw%3D%26na%3D0,0,0,0,f%26ki%3D251595
PRINCIPAL: 160235
TEXT:
09.04.2014
Benigno Aquino III, président des Philippines
18:03 – Philippines: Validation par la Cour suprême de la loi sur «la santé reproductive»
Dr Sam Orach, secrétaire exécutif du Bureau médical catholique de l’Ouganda (UCMB) (Image: globalhealth.duke.edu.)
17:17 – Ouganda: Les maisons de santé catholiques continueront de soigner les homosexuels
Statue du pape Jean Paul II (Photo:Parafia-gron/CC-BY-SA/Flickr)
16:56 – Pologne : Inauguration du nouveau musée Jean Paul II à Wadowice
