TITRE: Le d�brief du Grand Jury avec Jean-Fran�ois Cop� - Infos - Replay
DATE: 2014-02-10
URL: http://videos.tf1.fr/infos/2014/le-debrief-du-grand-jury-avec-jean-francois-cope-8362236.html
PRINCIPAL: 158424
TEXT:
� 22h12
59 secondes
La nomination de Jean-Christophe Cambad�lis est vivement critiqu�e par Marie-No�lle Lienemann, qui r�clame une �lection par les militants, et par le d�put� J�r�me Guedj.
� 21h50
1min 40s
Tous les jours sur LCI : le ZapNet, zapping des vid�os du web, par Thibaud V�zirian. Premi�re diffusion � 22h50, L'Oeil du Web dans LCI Soir.
� 21h45
24 secondes
La ministre de l'Ecologie et de l'Energie a appel� mercredi sur France 2 les maires � cesser l'utilisation de pesticides dans les espaces verts de leur commune, au nom de la sant� publique et de la biodiversit�.
� 20h30
1min 36s
Mardi, Manuel Valls a d�taill� l'�quivalent de 39 milliards d'�conomie sur les 50 milliards annonc�s d'ici 2017. O� trouver les 11 milliards restant?
� 20h30
2min 01s
La piscine parisienne de Molitor rena�t de ses cendres apr�s 75.000 heures de travail intensif. Les vitraux d'origine de ce joyaux de l'Art d�co ont retrouv� leur place aujourd'hui.
� 20h30
2min 06s
Les secr�taires d�Etat n'assistent pas, sauf exception, au conseil des ministres. Mais comme ces derniers, ils b�n�ficient d'une voiture avec chauffeur, d'un logement de fonction et d'une r�mun�ration de 9443 euros bruts par mois. Rencontre avec trois d�entre eux, Axelle Lemaire, Annick Girardin et Thierry Braillard.
� 20h30
2min 04s
Le remaniement engag� il y a une semaine a provoqu� un changement de locataire � Matignon et une valse des ministres. Il n'�pargne pas non plus le PS et celui qui en fut pendant 18 mois le premier secr�taire Harlem Desir. Dans les prochains jours, Jean Christophe Cambad�lis pourrait en prendre la t�te.
� 20h30
1min 37s
En Pennsylvanie, un lyc�en de 16 ans a agress� ce matin une vingtaine de personnes � l'aide d'une arme blanche dans des classes et des couloirs de son �tablissement. Sept adolescents �g�s de 15 � 17 ans et un adulte de 60 ans ont �t� transport�es � l'h�pital. Le suspect a �t� arr�t� par la police.
� 20h30
2min 01s
Les membres de la famille Agnelet r�unis pour la premi�re fois depuis des d�cennies pour une confrontation face aux jur�s ce mercredi. 37 ans apr�s les faits, Guillaume Agnelet a soutenu � la barre que son p�re est l'auteur du meurtre d'Agn�s Le Roux.
� 20h30
2min 47s
Dix jours apr�s la cinglante d�faite de la gauche aux municipales, le nouveau gouvernement de Manuel Valls est en ordre de marche. Ce mercredi, 14 secr�taires d'Etat ont �t� nomm�s.
� 20h30
2min 47s
Le retour � la semaine de 4,5 jours en primaire est une �pine dans le pied pour Manuel Valls. Le premier ministre a promis d'assouplir ce qui reste selon lui � bonne r�forme �, cinq mois avant sa g�n�ralisation.
� 20h30
2min 42s
Sur les 50 milliards du plan d'�conomie de Manuel Valls, 10 milliards concernent les collectivit�s locales. Pour cela, le Premier ministre a propos� mardi de r�duire de moiti� le nombre de r�gions en France d'ici � 2017 et la  suppression des Conseils g�n�raux en 2021.
� 20h30
6min 48s
Invit� de Gilles Bouleau, le pr�sident de l�UMP a critiqu� les annonces faites par Manuel Valls lors de son discours de politique g�n�rale. "Il n'y a pas grand chose qui changent en r�alit�", a affirm� le d�put�-maire de Meaux.
� 20h30
2min 06s
Une combinaison spatiale, une note de Buzz Aldrin ou encore le guide de survie du module lunaire d'Apollo 13... Mardi � New-York, 300 objets ont �t� vendus aux ench�res pour des centaines de milliers de dollars.
� 20h30
1min 58s
Mardi lors de son discours de politique g�n�rale devant les d�put�s, Manuel Valls a annonc� la suppression des charges patronales pour l'employeur d'un smicard". Que va concr�tement apporter cette annonce ?
� 20h12
3min 50s
Fr�d�ric Cuvillier, secr�taire d'�tat charg� des transports, de la mer et de la p�che �tait sur LCI par t�l�phone
� 19h44
1min 51s
C'est une campagne d�rangeante et �mouvante de sensibilisation de la s�curit� routi�re belge : des personnes croyant retrouver un ami ont �t� film�es en cam�ra cach�e entrant� � leurs propres fun�railles. Images.
� 19h21
2min 24s
Interrog� sur LCI mercredi soir juste apr�s sa nomination, le secr�taire d'Etat aux Transports s'est montr� prudent sur le dossier de l'�cotaxe. Il a rappel� qu'une d�l�gation de parlementaires pr�parait un rapport et a soulign� qu'il fallait attendre ses conclusions.
