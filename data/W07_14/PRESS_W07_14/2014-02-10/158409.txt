TITRE: Firefox Touch Beta pour Windows 8 est disponible en t�l�chargement - GinjFo
DATE: 2014-02-10
URL: http://www.ginjfo.com/actualites/logiciels/firefox-touch-beta-pour-windows-8-est-disponible-en-telechargement-20140208
PRINCIPAL: 158403
TEXT:
Home / Actualit�s / Logiciels / Firefox Touch Beta pour Windows 8 est disponible en t�l�chargement
Firefox Touch Beta pour Windows 8 est disponible en t�l�chargement
Publi� par : J�r�me Gianoli dans Logiciels 08/02/2014 0 884 Lectures
Firefox Touch Beta pour Windows 8 est d�sormais disponible en t�l�chargement. Cette version propose une interface sp�cifique pour l�environnement M�tro.
Propos�e en version x86 et x64, cette monture nomm�e, Firefox Touch Beta pour Windows 8 alias Firefox 28, se veut optimis�e pour l�interface tactile, de sorte que vous devriez rencontrer aucune difficult� � l�exploiter sur une tablette ex�cutant Windows 8 ou Windows 8.1. La plupart des fonctionnalit�s du navigateur classique Firefox sont disponibles comme les � principaux sites �, l�historique ou encore les signets, il manque cependant quelques outils importants, citons la navigation priv�e.
Cette fonctionnalit� devrait normalement faire son apparition dans une prochaine version. Firefox tactile pour Windows 8 est propos�e en version Beta, il y a donc des risques de bugs cependant, elle nous semble, apr�s plusieurs essais, assez stable. Le fonctionnement est correct bien que nous ayons remarqu� des ralentissements avec un nombre �lev� d�onglets ouverts.
A noter qu�il est possible de l�ex�cuter au c�t� d�une autre application Metro.
Pour le t�l�chargement, c�est ici .
-----------------------------------------------------
Vos recherches qui ont men� � cet article :
firefox touch
Firefox M�tro Windows 8 T�l�charger
telecharger navigation
