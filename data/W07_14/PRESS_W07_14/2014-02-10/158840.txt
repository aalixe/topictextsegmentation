TITRE: Un Fourcade � r�action ? - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/sport/un-fourcade-a-reaction-706566.html
PRINCIPAL: 158838
TEXT:
> JO d'hiver
Un Fourcade � r�action ?
Loin du compte et de l�or olympique samedi sur le 10 km sprint, Martin Fourcade est-il en mesure, ce lundi, de rectifier le tir lors de la poursuite ? Les derniers indices tendent � le prouver.
G.Mathieu (avec GQ) � Sotchi
Le 10/02/2014 �  5:00
r�agir
On l�avait laiss� h�b�t�, groggy. Abattu, m�me selon une source indiscr�te. A court d�arguments apr�s un sprint de 10 km qu�il avait eu le sentiment d�avoir pourtant plut�t bien g�r� (6e), Martin Fourcade s�est r�veill� dimanche matin dans un tout autre �tat d�esprit. Touch� mais pas coul�, comme tout bon champion qui se respecte. Et qui veut se faire respecter. � Il a �t� piqu� dans son estime, il a �t� bless� et je pense qu�il va avoir � c�ur de nous montrer son vrai visage �, estime Vincent Jay, champion olympique 2010 du sprint.
Pour mieux s�en convaincre, il suffisait de l�observer hier apr�s-midi,  durant l�heure et demie d�entra�nement qu�il s�est accord�e sur le site  olympique de Laura en compagnie des autres membres de l��quipe de  France. D�tendu, souriant voire hilare. La banane, m�me, plaisantant  p�le-m�le avec les journalistes pr�sents, avant de chambrer Didier  Cuche, l�ex-gloire du ski suisse, venu recueillir son suffrage en vue de  l��lection � la commission des athl�tes du CIO. Des preuves formelles  que la page semble �tre bel et bien tourn�e.
� On est assez confiant �
D�un point de vue purement sportif, l�entra�nement s�est av�r� tr�s concluant, aussi bien sur les skis qu�au tir. Des raisons de plus de le sentir d�attaque et revigor� pour la poursuite de 12,5 km. ��Il a �t� parfait, insiste Siegfried Mazet, l�entra�neur national. Apr�s, c�est une course, il se passera forc�ment quelque chose. Pr�dire ce qui va se passer, c�est un petit peu pr�tentieux mais c�est une course qu�il aime bien, qu�il affectionne particuli�rement. Il rentre plus dans du concret, dans des choses o� il a l�habitude de s�exprimer.��
Une expression que l�on imagine charg�e de r�volte et de haine  positive. ��Non, il ne sera pas revanchard, rectifie St�phane Bouthiaux,  le chef d��quipe. Il a juste envie d�aller chercher des m�dailles, �a  c�est s�r. Tous les t�moins sont au vert. Il ne part qu�� 12 secondes  (retard qu�il a accus� � l�arriv�e du sprint, ndlr). Arriv� au premier  tir, ils seront tous ensemble ou � trois quatre seconde pr�s. Le match  d�butera au premier tir couch� donc il faudra �tre super prudent  derri�re la carabine. Mais on sait qu�en principe en poursuite, il  r�pond pr�sent donc on est assez confiant.��
Bj�erndalen seul dans la l�gende ?
Quarante-huit heures apr�s �tre entr� au Panth�on olympique en �galant Bj�rn Daehlie avec 12 m�dailles glan�es aux JO d�hiver, le Norv�gien Ole Einar Bj�erndalen peut devenir � 40 ans le recordman absolu en cas de nouvelle m�daille, ce lundi en poursuivre, apr�s l�or d�croch� samedi en sprint. Pour rappel, depuis son premier titre olympique glan� � Nagano en 1998, Bj�erndalen a collectionn� 6 autres breloques en or, 4 d�argent et 1 de bronze.
Toute l'actu Sport
