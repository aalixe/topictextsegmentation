TITRE: Commerce : le match Europe - Etats-Unis s�engage
DATE: 2014-02-10
URL: http://www.lemonde.fr/economie/article/2014/02/10/commerce-le-match-europe-etats-unis-s-engage_4363369_3234.html
PRINCIPAL: 0
TEXT:
| Par Claire Gu�laud
Apr�s un d�marrage laborieux, en juillet 2013, dans un climat alourdi par l'affaire des �coutes de l'Agence nationale de la s�curit� (NSA) am�ricaine en Europe , les n�gociations entre les Etats-Unis et l' Union europ�enne (UE) sur un Partenariat transatlantique de commerce et d'investissement � ou TTIP pour � Transatlantic Trade and Investment Partnership � � vont s' engager pour de bon.
Le Monde.fr a le plaisir de vous offrir la lecture de cet article habituellement r�serv� aux abonn�s du Monde.fr.  Profitez de tous les articles r�serv�s du Monde.fr en vous abonnant � partir de 1� / mois | D�couvrez l'�dition abonn�s
Lundi 17 f�vrier, le commissaire europ�en au commerce, Karel De Gucht, rencontre le secr�taire d'Etat am�ricain au commerce, Michael Froman.
� Les choses s�rieuses vont commencer �, a indiqu�, jeudi 6 f�vrier au Monde, Nicole Bricq. La ministre fran�aise du commerce ext�rieur doit accompagner Fran�ois Hollande , qui effectue du 10 au 12 f�vrier une visite d'Etat aux Etats-Unis. Elle rencontrera M. Froman pour la quatri�me fois.
Les d�l�gations am�ricaine et europ�enne devraient rendre publiques de premi�res propositions sur les march�s publics et le d�mant�lement complet, ou la r�duction des derni�res barri�res tarifaires entre les Etats-Unis et l'UE � l'occasion d'une quatri�me session de n�gociations, en mars.
Le sommet du 26 mars � Bruxelles � entre le pr�sident am�ricain, Barack Obama , celui du Conseil europ�en, Herman Van Rompuy et celui de la Commission europ�enne, Jos� Manuel Barroso � devrait permettre de redonner une impulsion politique aux discussions entre les deux plus grandes entit�s �conomiques du monde.
D'autant que l'une comme l'autre sont soucieuses, selon le Cepii, un centre de recherche en �conomie internationale, de � conserver leur leadership sur le commerce mondial ou, � tout le moins, de limiter leur perte d'influence � face � l'Asie.
Le TTIP est plus vaste qu'un simple accord de pr�f�rence commerciale. Il porte sur la suppression ou la r�duction des derni�res barri�res tarifaires, mais aussi sur la diminution des barri�res non tarifaires � convergence des normes r�glementaires, r�gulation dans les services , acc�s aux march�s publics, investissement, indications g�ographiques prot�g�es.
INQUI�TUDES
Les discussions sur ces sujets � qualifi�s � Paris de � techniquement compliqu�s et politiquement lourds � � sont rendues difficiles aux Etats-Unis par les frictions entre la Maison Blanche et le Congr�s et par les �lections de mi-mandat pr�vues en novembre.
Elles le sont aussi de l'autre c�t� de l'Atlantique du fait du scrutin europ�en du mois de mai et de la fin du mandat de M. Barroso.
A l'exception de quelques produits sensibles sur lesquels subsistent des droits �lev�s (la viande, certains aciers, le textile-habillement, les produits laitiers�), les discussions les plus intenses devraient porter sur les questions non tarifaires.
Avec deux gros dossiers : l'acc�s aux march�s publics et, dans le volet investissement du Partenariat, l'�ventuelle mise en place d'un m�canisme de r�glement des diff�rends, une instance d'arbitrage international charg�e de trancher les litiges entre des investisseurs et un Etat.
Les autorit�s am�ricaines semblent tr�s attach�es � ce projet qui r�vulse nombre d'Europ�ens � des ONG, des juristes, des gouvernements� � Nous avons dit d�s le d�part que nous �tions contre l'inclusion d'un tel m�canisme qui permettrait � une multinationale am�ricaine de nous attaquer et, �ventuellement, de limiter notre souverainet� �, a pr�cis� Mme Bricq.
Devant ces inqui�tudes, la Commission europ�enne a annonc�, le 21 janvier, trois mois de consultation sur ce dossier et elle s'est engag�e � soumettre un �ventuel accord � la r�gle de l'unanimit�.
ACC�S AUX MARCH�S PUBLICS
En ce qui concerne les march�s publics, les entreprises europ�ennes, mais aussi certains Etats acquis de longue date au libre-�change, comme les Pays-Bas , le Danemark et la Su�de , sont d�sireux de les voir s' ouvrir aux Etats-Unis, ce qui ne va pas de soi.
Treize Etats am�ricains sur cinquante y sont aujourd'hui oppos�s. Par ailleurs, le Buy American Act (Loi � achetez am�ricain �) de 1933, qui s'impose au gouvernement f�d�ral, prot�ge les entreprises du pays.
En vertu de ce texte, toutes les marchandises destin�es � l'usage public � articles, mat�riaux ou fournitures � doivent �tre produites aux Etats-Unis, et toutes les marchandises manufactur�es doivent �tre fabriqu�es aux Etats-Unis, � partir de produits am�ricains. Plusieurs Etats et municipalit�s ont par ailleurs adopt� des dispositions similaires en mati�re d'approvisionnement.
Mme Bricq dit avoir beaucoup bataill� � la Commission et aupr�s des Etats membres de l'UE pour que celle-ci se dote d'un r�glement europ�en sur la r�ciprocit� dans l'ouverture des march�s publics.
Ce texte vient d' �tre vot� par le Parlement. Il doit encore �tre adopt� par le Conseil europ�en. Mme Bricq n'exclut pas que l' Allemagne , � l'origine oppos�e � une telle r�gle, puisse �voluer .
De l'avis du Medef � le syndicat patronal est attach� � la r�duction des obstacles r�glementaires �, le textile, la chimie, la pharmacie, les technologies de l'information, les services financiers, les dispositifs m�dicaux, les cosm�tiques sont les secteurs les plus directement concern�s par le contenu du Partenariat transatlantique.
MARGES DE MAN�UVRE R�DUITES
D'autres sujets sont consid�r�s comme tr�s sensibles. Les OGM, le b�uf aux hormones ou les poulets lav�s � l'eau de Javel sont de ceux-l�. Aussi, la volont� des Europ�ens de prot�ger et d' �tendre leurs indications g�ographiques prot�g�es � la feta, le parmesan, etc. �, consid�r�es comme de simples noms g�n�riques aux Etats-Unis.
Lire : Etats-Unis - Europe : � deux mod�les alimentaires oppos�s �
L'UE assure ne pas vouloir revoir � la baisse le niveau de protection de ses consommateurs, mais elle peut difficilement tenir un autre discours en d�but de n�gociations.
Les marges de man�uvre de l'administration am�ricaine sont r�duites. M. Obama souhaitait obtenir du Congr�s qu'il avalise ou rejette en bloc, sans en discuter le d�tail, le projet d'accord entre l'UE et les Etats-Unis en vertu de la proc�dure du � fast track � ou Trade Promotion Authority (TPA).
Il n'a pu l' obtenir . Harry Reid, le chef de file des d�mocrates au S�nat, pourtant proche de lui, s'est d�clar�, fin janvier, � contre la proc�dure acc�l�r�e �. Quelques semaines plus t�t, ce sont plus de 150 �lus d�mocrates qui avaient adopt� la m�me position. Elle n'est pas de nature � rassurer les n�gociateurs europ�ens.
L�acc�s � la totalit� de l�article est prot�g� D�j� abonn� ? Identifiez-vous
Commerce : le match Europe - Etats-Unis s�engage
Il vous reste 78% de l'article � lire
Achetez cet article 2 � Abonnez-vous � partir de 1 � D�couvrez l��dition abonn�s
Les deux parties divis�es sur les services financiers
La question de l�inclusion des services financiers dans les n�gociations est �prement d�battue entre les Am�ricains et les Europ�ens. Les premiers pr�f�reraient garder le secteur hors du champ de l�accord ; les seconds en font une question aussi strat�gique que celles sur les produits agricoles ou le textile. C�est la r�gulation et la supervision du secteur financier qui pose probl�me. Les r�glementations restent disparates de part et d�autre de l�Atlantique. Les Europ�ens aimeraient convenir d�un cadre bilat�ral de coop�ration. Il permettrait de s�assurer que les r�glementations convergent, qu�elles sont comprises et reconnues comme telles par les deux parties (avec la possibilit� d��tablir des �quivalences) et que les r�gulateurs et les superviseurs discutent entre eux afin d�en �valuer l�impact.
Zone euro et Union europ�enne
