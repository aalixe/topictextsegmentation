TITRE: Rib�ry ne verra pas Londres - Football - Sports.fr
DATE: 2014-02-10
URL: http://www.sports.fr/football/ligue-des-champions/articles/ribery-ne-verra-pas-a-londres-1008092/
PRINCIPAL: 160941
TEXT:
Franck Ribéry ne reviendra pas avant deux semaines. (Reuters)
Par François Tesson
10 février 2014 � 17h32
Mis à jour le
10 février 2014 � 17h47
Comme on pouvait le craindre, Franck Ribéry ne sera pas du déplacement du Bayern Munich à Arsenal, la semaine prochaine, en huitième de finale aller de la Ligue des Champions. L'international français, opéré d'un hématome au muscle fessier jeudi dernier, manquera deux semaines de compétition. 
Pas de miracle pour Franck Ribéry . Si son entraîneur, Pep Guardiola, espérait l'avoir dans son équipe pour le déplacement du Bayern Munich à Arsenal le 19 février prochain, l'international français ne sera pas du voyage à Londres.
Le milieu offensif du Bayern avait été opéré jeudi dernier d'un hématome au muscle fessier qui touchait un nerf. Si sa guérison est en "bonne voie", selon le médecin du club bavarois, le Dr. Hans-Wilhelm Müller-Wohlfahrt, Ribéry devra attendre deux semaines pour être de nouveau opérationnel.
Ribéry pourrait reprendre le samedi 22 sur la pelouse d' Hanovre , ou le week-end suivant face à Schalke 04 . Et il sera sans doute sur pied pour le match retour face aux Gunners, le 11 mars.
