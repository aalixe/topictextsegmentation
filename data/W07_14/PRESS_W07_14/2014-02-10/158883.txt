TITRE: Le coll�ge C�venol, trop marqu� par le meurtre d'Agn�s, va fermer - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/societe/college-cevenol-marque-meurtre-dagnes-va-fermer-706460.html
PRINCIPAL: 158880
TEXT:
Le coll�ge C�venol, trop marqu� par le meurtre d'Agn�s, va fermer
A. G. avec  AFP
r�agir
Le coll�ge C�venol du Chambon-sur-Lignon, en Haute-Loire, et qui �prouvait le plus grand mal � se relever du meurtre d'Agn�s Marin par un autre �l�ve de l'�tablissement en 2011, ne pourra pas poursuivre son activit�, a annonc� dimanche l'association qui le g�re. Le coll�ge �tait d�j� en proie � des difficult�s de tr�sorerie depuis plusieurs ann�es, et avait �t� plac� en redressement judiciaire en mai dernier.
En plus de ses difficult�s financi�res, l'�tablissement avait �t� secou� en 2011 par le meurtre d'Agn�s, une �l�ve de premi�re dont le corps calcin� avait �t� retrouv� dans la for�t proche de l'�tablissement. Le lyc�en accus� de l'avoir tu�e, Matthieu, avait �t� jug� par la cour d'assises des mineurs du Puy-en-Velay en juin dernier et condamn� � la r�clusion criminelle � perp�tuit� .
"Nous avions 87 �l�ves � la rentr�e de septembre 2013 alors qu'on disait que le minimum �tait de 90. L'administrateur judiciaire recherche toutes les possibilit�s d'une reprise, pour en faire un centre �ducatif par exemple, ou un centre �conomique", a aussi soulign� Laurent Pasteur, vice-pr�sident de l'association g�rant le C�venol. Il a pr�cis� que la scolarit� des coll�giens et lyc�ens se poursuivra bien jusqu'� juillet, les financements ayant �t� assur�s jusque l�.
�� �
Apr�s le drame de 2011, l'�tablissement a connu de nombreuses d�saffections d'�l�ves. Fond� par des protestants en 1938, l'�tablissement priv� sous contrat dont la devise est "Humanisme & Tol�rance", accueille des pensionnaires de 30 nationalit�s diff�rentes venus faire leur �tudes en France, mais aussi des enfants d'expatri�s ou encore des �l�ves en difficult� avec leur famille.
