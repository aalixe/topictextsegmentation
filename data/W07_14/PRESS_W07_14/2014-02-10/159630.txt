TITRE: Seine-Saint-Denis : un mort et sept bless�s apr�s une intoxication au monoxyde de carbone - RTL.fr
DATE: 2014-02-10
URL: http://www.rtl.fr/actualites/info/article/seine-saint-denis-un-mort-et-sept-blesses-apres-une-intoxication-au-monoxyde-de-carbone-7769611529
PRINCIPAL: 159629
TEXT:
Un pompier devant son v�hicule, le 4 novembre 2012, � Boulogne-sur-Mer (illustration).
Cr�dit : AFP
Une personne est d�c�d�e et sept autres ont �t� bless�es apr�s une intoxication au monoxyde de carbone. Une chaudi�re d�fectueuse serait � l'origine du drame.
Une intoxication au monoxyde de carbone a fait de nouvelles victimes en Seine-Saint-Denis. Une personne a �t� tu�e et sept autres ont �t� bless�es, dont deux  gravement, dimanche 9 f�vrier aux Pavillons-sous-Bois (Seine-Saint-Denis). Les victimes ont �t� intoxiqu�es "dans la  soir�e", alors qu'elles se trouvaient au sous-sol d'un petit pavillon, a  pr�cis� une source polici�re.  L'intoxication a �t� provoqu� par une chaudi�re d�fectueuse.
Un homme de nationalit� moldave est d�c�d� et sept autres  personnes ont �t� bless�es. Dans  un communiqu�, l'Agence R�gionale de Sant� (ARS) a mis en garde d�but  f�vrier contre une "forte augmentation" des intoxications au monoxyde de  carbone, "dues � l'utilisation d'appareils de chauffage de fortune de  type bras�ro ou barbecue".
"Depuis le 1er septembre 2013, la  proportion d'intoxications au monoxyde de carbone li�es � l'utilisation  de ce type d'appareils a doubl� par rapport � l'ann�e derni�re en  Ile-de-France", souligne l'ARS. Selon l'agence, 307 personnes ont  �t� expos�es au monoxyde de carbone depuis cette date en Ile-de-France,  principalement en Seine-Saint-Denis, Yvelines et Seine-et-Marne. Parmi  elles, 257 ont �t� hospitalis�es aux urgences.
La r�daction vous recommande
