TITRE: Inondations : "Tous les services de l'Etat sont mobilis�s", dit le pr�fet de Bretagne - RTL.fr
DATE: 2014-02-10
URL: http://www.rtl.fr/actualites/info/environnement/article/inondations-tous-les-service-de-l-etat-sont-mobilises-dit-le-prefet-de-bretagne-7769600094
PRINCIPAL: 158424
TEXT:
Un homme � v�lo dans la ville inond�e de Redon.
Cr�dit : FRED TANNEAU / AFP
R�ACTION - Le pr�fet de Bretagne se mobilise pour maintenir l'accessibilit� de la ville de Redon et notamment les transports scolaires.
Si, dans l'ouest, la d�crue s'est amorc�e ce dimanche 9 f�vrier, M�t�o France a maintenu trois d�partements en vigilance rouge : le Morbihan, l'Ile-et-Vilaine et la Bretagne. La r�gion de Redon, au carrefour de ces trois d�partements, est l'une des zones qui pr�occupe le plus les autorit�s. Dans ce secteur, les crues de la Vilaine et de l'Oust rendent difficile, voire impossible, l'acc�s � la ville.
"On maintient l'accessibilit� de Redon par le syst�me de transport ferroviaire. De ce c�t�-l� il n'y a pour l'instant aucun risque, les trains circulent", explique au micro de RTL le pr�fet de la r�gion, Patrick Strozda
"En revanche, toute la desserte par l'ouest est fortement perturb�e. Cela se traduit par des d�tours assez importants. �a g�ne � la fois les personnes qui travaillent � Redon et qui vivent � l'Ouest de Redon", poursuit-il.
Pas de situation critique, mais de la lassitude
Patrick Strozda
Les autorit�s se mobilisent pour maintenir les transports scolaires lundi. "Il y a beaucoup de monde qui est mobilis�, tous les services de l'Etat", pr�cise le pr�fet. "Pas de victimes � d�plorer, pas de situation critique, mais de la lassitude parce que �a dure depuis No�l", l�che-t-il.
Selon la pr�fecture d'Ille-et-Vilaine, le niveau de l'eau va continuer � monter jusqu'� lundi dans la zone de Redon, passant d'une cote de 4,31 m�tres dimanche � 4,70m attendus lundi.
Inondations : "Tous les service de l'Etat sont mobilis�s", dit le pr�fet de Bretagne
Cr�dit : Sina Mir
