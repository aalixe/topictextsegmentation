TITRE: OHR : Envoyer les troupes de l'UE en Bosnie ? | Bosnie-Herz�govine Info
DATE: 2014-02-10
URL: http://www.bhinfo.fr/ohr-envoyer-les-troupes-de-l-ue-en,3815/
PRINCIPAL: 0
TEXT:
16 f�vrier�12:15, par Ronan Chiquet (abonn�)
From Ronan.
La Yougoslavie �conomique existe toujours et ne s�est jamais arr�t�e y compris pendant les ann�es de guerre de 1990 � 2000.
La criminalit� organis�e n�a jamais cess�e et n�a pas connu les "haines inter-ethniques".
Le business du trafic d�armes sous embargo a �t� tr�s florissant.
Ce qui est bien en effet aujourd�hui c�est que ces manifestations traversent toutes les communaut�s qui ont tout int�r�t � s�unir contre leurs exploiteurs.
Si Dodik veut prendre sa retraite, personne ne le retiendra, enfin je l�esp�re�!
Quand � la r�unification de l�ex-Yougoslavie, il y a du boulot�!
Je ne pense pas que l�OTAN qui est � l�origine du d�ment�lement de la Yougoslavie comme elle est � l�origine de la guerre en Syrie, ne souhaite revoir une Yougoslavie unie. Je pense qu�Angela Merkel manquerait aussi d�humour comme ses petits copains slov�nes et croates.
R�pondre � ce message
mod�ration � priori
Ce forum est mod�r� � priori�: votre contribution n'appara�tra qu'apr�s avoir �t� valid�e par un administrateur du site.
Un message, un commentaire�?
(Pour cr�er des paragraphes, laissez simplement des lignes vides.)
Lien hypertexte (optionnel)
(Si votre message se r�f�re � un article publi� sur le Web, ou � une page fournissant plus d'informations, vous pouvez indiquer ci-apr�s le titre de la page et son adresse.)
Titre :
