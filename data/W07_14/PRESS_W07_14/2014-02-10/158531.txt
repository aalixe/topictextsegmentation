TITRE: Centrafrique: 10 tu�s � Bangui, tourn�e du ministre fran�ais de la D�fense - Flash actualit� - Monde - 09/02/2014 - leParisien.fr
DATE: 2014-02-10
URL: http://www.leparisien.fr/flash-actualite-monde/bangui-au-moins-neuf-tues-dans-des-violences-09-02-2014-3573917.php
PRINCIPAL: 0
TEXT:
Centrafrique: 10 tu�s � Bangui, tourn�e du ministre fran�ais de la D�fense
Publi� le 09.02.2014, 11h03
Tweeter
Au moins 10 personnes ont �t� tu�es lors de violences accompagn�es de pillages � grande �chelle dimanche � Bangui, au moment o� le ministre fran�ais de la D�fense a d�but� au Tchad une nouvelle tourn�e largement d�di�e � la Centrafrique. | Issouf Sanogo
1/5
R�agir
Au moins 10 personnes ont �t� tu�es lors de violences accompagn�es de pillages � grande �chelle dimanche � Bangui, au moment o� le ministre fran�ais de la D�fense a d�but� au Tchad une nouvelle tourn�e largement d�di�e � la Centrafrique.
Dans la capitale centrafricaine, une nouvelle flamb�e de violence a �clat� samedi soir aux abords de la mairie du 5e arrondissement, au centre-ville, avec cinq personnes tu�es dans des circonstances non �tablies, puis trois autres dans des affrontements interreligieux, et une neuvi�me par des soldats de la force de l'Union africaine (Misca), selon des t�moins.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Ce bilan a �t� confirm� sur place � l'AFP par Peter Bouckaert, de l'ONG Human Rights Watch, qui a �galement fait �tat du lynchage � mort d'une dixi�me personne - musulmane ou chr�tienne, selon des sources contradictoires - pr�s du march� central.
Dimanche matin, des soldats fran�ais et des gendarmes centrafricains ont pris position, au milieu de ruines de commerces encore fumantes, dans le 5e arrondissement livr� aux pilleurs et survol� par un h�licopt�re de combat fran�ais.
- "La nuit, c'�tait terrible" -
Selon des habitants, apr�s la mort de cinq personnes samedi soir, une femme chr�tienne de ce quartier mixte a �t� tu�e par un musulman. Son agresseur a �t� captur� et tu�, et son cadavre br�l� devant la mairie. Son corps calcin� gisait dimanche au milieu de la route.
Un deuxi�me civil musulman a ensuite �t� tu� et son meurtrier s'appr�tait � jeter le cadavre dans un brasier quand les soldats rwandais de la Misca ont ouvert le feu, a racont� Innocent, un habitant du quartier. "Ils l'ont tu�", a accus� Innocent, s'exprimant au milieu d'une foule surexcit�e criant "A mort les Rwandais".
"Les Rwandais sont tous des musulmans! Dehors, les Rwandais!", hurlait une femme, tandis que cr�pitaient des rafales de kalachnikov, d'origine ind�termin�e.
Dans la mairie du 5e arrondissement, une dizaine de soldats rwandais �taient retranch�s: "la nuit, c'�tait terrible", a dit l'un d'eux � l'AFP.
En fin de matin�e, malgr� les remontrances des militaires fran�ais, des bandes de jeunes pillards continuaient de venir se servir, certains avec des brouettes ou des charrettes, se glissant entre les blind�s.
- "Les Fran�ais ne vont pas tirer" -
"Les Fran�ais ne vont pas nous tirer dessus", assurait en riant l'un d'eux, un jeune coiff� d'un bonnet.
Selon une source militaire fran�aise, les pillages ont finalement �t� contenus � la mi-journ�e et des troupes de la Misca ont relev� les Fran�ais dans le secteur.
Le commandant en chef de la Misca, le g�n�ral camerounais Martin Tumenta Chomua, a menac� samedi les groupes arm�s de recourir � la force pour arr�ter assassinats, lynchages et pillages.
Dans ce climat de violences sans fin, le ministre fran�ais de la D�fense Jean-Yves Le Drian est arriv� dimanche en fin de journ�e � N'Djamena pour une nouvelle tourn�e r�gionale consacr�e en grande partie � la crise centrafricaine, a-t-on appris de source officielle.
Apr�s un entretien � huis clos avec le pr�sident tchadien Idriss D�by Itno, acteur militaire et politique majeur d'Afrique centrale, consacr� � la situation � Bangui, mais aussi � la r�organisation du dispositif fran�ais au Sahel, il devait se rendre dans la soir�e au Congo.
Le pr�sident congolais Denis Sassou Nguesso est m�diateur dans le conflit centrafricain. Mercredi il se rendra � Bangui pour une troisi�me visite depuis le d�but, le 5 d�cembre, de l'intervention fran�aise ("op�ration Sangaris").
La Centrafrique a sombr� dans le chaos depuis la prise du pouvoir en mars 2013 par Michel Djotodia, chef de la coalition rebelle S�l�ka � dominante musulmane devenu pr�sident, qui a �t� contraint � la d�mission le 10 janvier pour son incapacit� � mettre fin aux tueries interreligieuses.
II a �t� remplac� le 20 janvier par Catherine Samba Panza, qui a effectu� ce week-end son premier d�placement � l'�tranger, � Brazzaville o� elle a rencontr� M. Sassou Nguesso.
