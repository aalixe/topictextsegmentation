TITRE: Le racisme provoquerait un vieillissement pr�coce | Mediapart
DATE: 2014-02-10
URL: http://www.mediapart.fr/journal/international/100214/le-racisme-provoquerait-un-vieillissement-precoce
PRINCIPAL: 160191
TEXT:
La lecture des articles est r�serv�e aux abonn�s.
Selon une �tude r�alis�e sur des Noirs am�ricains, parue dans l'American Journal of Preventive Medicine et reprise par Le Monde, les personnes les plus en proie au racisme porteraient les marques d'un vieillissement cellulaire pr�coce.
S�ils restent prudents quant aux conclusions de leurs recherches, les auteurs de ce travail utilisent, � propos de l'effet des discriminations, l'expression de ��toxines sociales��. ��Une mani�re de dire que si le racisme est un poison au sens figur�, il pourrait aussi en �tre un au sens propre��, �crit sur son blog le journaliste scientifique Pierre Barth�l�my.
� lire sur le blog Passeur de sciences.
