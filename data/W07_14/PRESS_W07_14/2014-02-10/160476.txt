TITRE: Archives Actualit�. Archives information en ligne : photos, media - Actu Orange
DATE: 2014-02-10
URL: http://actu.orange.fr/economie/ocde-inflexion-positive-de-l-activite-en-france-et-en-zone-euro-afp_2821598.html
PRINCIPAL: 160475
TEXT:
utilisez un cr�dit pour consulter cet article pendant 7 jours.
OCDE: inflexion positive de l'activit� en France et en zone euro
L'activit� fran�aise et celle de la zone euro connaissent une inflexion positive, a estim� l'Organisation de coop�ration et de d�veloppement �conomique (OCDE) qui a publi� lundi ses indicateurs composites avanc�s.
les articles li�s
08/04/14
Le FMI craint une contagion de la crise ukrainienne : Le Fonds mon�taire international a mis en garde mardi contre les risques de contagion de la crise ukrainienne, qui affecte d�j� la Russie, � l'ensemble de l'�conomie mondiale, notamment en cas de perturbations dans les approvisionnements en hydrocarbures.
lire la suite
08/04/14
Selon le FMI, la France va rattraper le rythme de croissance allemand : Le Fonds mon�taire international (FMI) a relev� mardi sa pr�vision de croissance 2014 pour la France et estime qu'elle retrouvera l'an prochain un rythme proche de celui de l'Allemagne, et en ligne avec le reste de la zone euro.
