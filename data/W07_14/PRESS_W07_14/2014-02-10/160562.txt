TITRE: Pro A. Johann Petro signe à Limoges - Basket - Le Télégramme
DATE: 2014-02-10
URL: http://www.letelegramme.fr/basket/pro-a-johann-petro-signe-a-limoges-10-02-2014-10029544.php
PRINCIPAL: 160560
TEXT:
Pro A. Johann Petro signe à Limoges
10 février 2014 à 15h17
Le pivot Johan Petro, champion d'Europe avec la France en septembre, s'est engagé avec le club de Limoges en ProA.  
"Retour en #ProA sous les couleurs du @CSPLimoges! Un pur bonheur!", a 
tweeté le joueur de 28 ans dont les modalités du contrat reste à déterminer.
Après des débuts à Pau-Orthez en 2003, l'intérieur de 2,12 m avait passé 
neuf saisons en NBA, chez les Nuggets de Denver, les Nets du New Jersey et les 
Hawks d'Atlanta notamment.     Il avait quitté les Faucons l'été dernier pour tenter un nouveau pari en migrant pour le championnat chinois au sein des Zhejiang Guangsha Lions. Peu utilisé, avec des prestations n'ayant pas convaincu les dirigeants 
chinois, Petro avait trouvé un accord pour quitter les Lions en janvier.
Il compte 45 sélections en équipe de France où il avait connu sa première 
cape en 2006.
