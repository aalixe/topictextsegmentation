TITRE: VIDEO. En gr�ve, les taxis r�clament une plus grande r�gulation des VTC
DATE: 2014-02-10
URL: http://www.francetvinfo.fr/economie/entreprises/video-en-greve-les-taxis-reclament-plus-de-regulation-pour-les-vtc_526083.html
PRINCIPAL: 159315
TEXT:
Tweeter
VIDEO. En gr�ve, les taxis r�clament une plus grande r�gulation des VTC
Ces voitures avec chauffeur, que l'on commande le plus souvent depuis son smartphone, concurrencent de plus en plus les taxis traditionnels.
(MATHIAS SECOND - FRANCE 2)
Par Francetv info avec AFP
Mis � jour le
, publi� le
10/02/2014 | 07:46
Concerts de klaxons attendus dans les grandes villes de France, lundi 10 f�vrier. A�l'appel des syndicats CFDT, CGT, FO, SDCTP et CST, les taxis vont manifester contre�"la concurrence d�loyale"�des 10�000�voitures de tourisme avec chauffeur (VTC), autoris�es� depuis 2009 .�
A Paris, des centaines de v�hicules partiront des�a�roports de Roissy et d'Orly en direction du Trocad�ro, en plein c�ur de la capitale. Les syndicats attendent 3 000 taxis au pied de la tour Eiffel. Le trafic devrait��galement �tre perturb� � Marseille (Bouches-du-Rh�ne), Lyon (Rh�ne) et�Bordeaux (Gironde), o� d'autres manifestations sont pr�vues.
Mercredi 5 f�vrier, le Conseil�d'Etat a suspendu le "d�cret quinze minutes", qui obligeait les VTC � attendre un quart d'heure entre la r�servation d'une course et la prise en charge du client. Une d�cision v�cue comme une injustice par les taxis.�Pour tenter de sortir de l'impasse, le gouvernement a annonc� la mise en place, dans les prochains jours, d'une mission de concertation entre les VTC et les chauffeurs de taxi.
Des taxis en gr�ve ralentissent la circulation, le 13 janvier 2014 � Marseille (Bouches-du-Rh�ne). (CITIZENSIDE / FREDERIC SEGURAN / AFP)
