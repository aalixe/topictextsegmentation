TITRE: Autisme : Madame Carlotti, vous n'avez rien compris à la maladie de nos enfants - le Plus
DATE: 2014-02-10
URL: http://leplus.nouvelobs.com/contribution/1144476-autisme-madame-carlotti-vous-n-avez-rien-compris-a-la-maladie-de-nos-enfants.html
PRINCIPAL: 160962
TEXT:
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
Je publie
Erwann Meriadec a posté le 11 février 2014 à 13h37
Lisa, si vous ne supportez pas des vérités , alors en effet, mieux vaut quitter. Eh oui,des parents ont du mal à accepter que leur enfant n'est pas comme les autres.Cela prend beaucoup de temps.
Fboise, il y a une américaine, Tori Hayden, qui avait monté une classe avec uniquement des enfants très handicapés, autistes ou mentalement retardés. Elle a écrit des bouquins remarquables, elle te rejoint beaucoup dans ton expérience.
Huynh Tran, la "pochette surprise", on met des noms dedans (les potes, les renvois d'ascenseur, les "alliés" qui ont négocié des postes pour accepter de demander à leur lectorat de voter "pour"), on secoue et on tire les noms au hasard pour les postes, mais c'est aussi "am-stram-gram," au p'tit bonheur la chance.
C'est écoeurant mais c'est toujours comme cela.
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
falibade escapade a posté le 11 février 2014 à 14h40
je parlais à lisamal
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
Je publie
Leon Zappattas a posté le 11 février 2014 à 00h11
Une belle vérité. La tache est trop grande pour cette Dame Ministre. Elle n'a pas de vécu, pas les repaires, le savoir, la poigne nécessaire peut être, la volonté nécessaire pour casser le système, le réformer et le...reformer.
A t elle toutes les cartes ??
Si oui.. elle doit laisser la place à d'autres plus tailler pour ce vrai combat.
Si non, au nom de tous les enfants, elle doit le décrier!! Poser le problème, pour abattre les murs du silence !!
Faire le travail utile, nécessaire, qui doit être sa juste Mission. Stop à l'attentisme!! Nous sommes dans un scandale public!!
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
Je publie
Leon Zappattas a posté le 11 février 2014 à 00h01
Une belle vérité. La tache est trop grande pour Madame la Ministre. Elle n'a pas de vécu, pas les repaires, le savoir, la poigne nécessaire peut être, la volonté nécessaire pour casser le système, le réformer et le...reformer. A t elle toutes les cartes ?? Si oui.. elle doit laisser la place à d'autres plus tailler pour ce vrai combat. Si non, au nom de tous les enfants, elle doit le décrier!! Poser le problème, pour abattre les murs du silence !!
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
Je publie
Huynh Tran a posté le 10 février 2014 à 18h21
L'auteure a raison de dénoncer l'incurie du pouvoir politique concernant le problème des autistes mais, il me semble, a fait l'impasse sur la cause de cette incurie. Il y a dans les rouages de la santé, surtout dans l'appareil psychothépeutique une forteresse inviolée qui bloque tout progrès dans la thérapie des autistes. C'est la forteresse des psychanalystes, celle qui répètent comme des perroquets qu'ils connaissent la cause profonde de l'autisme, et qui disqualifient toutes les autres approches modernes de ce problème. Tant que cette forteresse, vide à l'intérieur, est là, il serait quasi-impossible d'apporter un vrai soutien aux parents écrasés par une tâche héroïque qui par dessus le marché ne mérite que le mépris de ces psy arrogants, bornés, rigides, dogmatiques.
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
Je publie
Isidor Cerf a posté le 10 février 2014 à 17h57
Trop facile de taper comme ça sur une responsable politique en lui mettant sur le dos ce qui résulte de plus d'un demi siècle de politique insuffisante et mal informée sur l'autisme. je suis moi même parent d'un enfant autiste et je crois que la détresse n'autorise pas tout, et surtout pas ce qui ressemble à des attaques personnelles à la limite de l'insulte.
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
lisamaria folder a posté le 10 février 2014 à 16h57
je suis d'accord avec tout mais pourquoi ne parle t on que des autistes ?
Beaucoup de personnes handicapées mentales qui grandissent deviennent adultes peuvent travailler dans un secteur adapté mais voilà bien le problème il faut aussi que la société fasse une place aux personnes handicapées mentales (et physiques)
Les moyens sont dérisoires, les éducateurs sont très mauvais pour la plupart (sauf ceux qui ont une véritable vocation), les projets sont misérables.
Un maire à qui je demandais quelles étaient les activités de la ville prévues pour les personnes handicapées mentales m'a répondu que la piscine leur était ouverte.
encore heureux ! voilà ce dont , on se satisfait et si ils ne savent pas nager ils n'auront pas d'éducateur spécialisé et si ils nagent bien, ils ne pourront pas non plus faire de l'entraînement ...
La France est très en retard mais j'aimerais que toutes les familles autour des personnes handicapées mentales se réunissent et que chaque handicap ne demande quelque chose pour lui, seulement.
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
Je publie
gerard bonnet a posté le 10 février 2014 à 16h32
autisme , justice , éducation , prison , la liste est trop longue pour tout énoncer , dans un pays ou tout a été laissé en déshérence pendant plus de 10 années comment tout faire alors que les caisses sont vides , oui où alors prendre à pierre pour donner à jacques ....
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
Je publie
Erwann Meriadec a posté le 11 février 2014 à 09h40
Dix ans, non, plus de 40 en fait, j'ai commencé mon travail social fin des années 70, l'évolution est d'une lenteur désespérante, on ne nous écoute pas, c'est la bureaucratie qui prime sur les besoins urgents. C'en est même inouï, on se demande parfois si les administrations ne laissent pas pourrir les problèmes - les gens finissent par mourir et ainsi c'est résolu !
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
francoise leroy a posté le 13 février 2014 à 00h51
+ 10
JE RÉAGIS (Max 1500 caractères)
Écrire ici...
