TITRE: Fashion week : Alexander Wang s'impose en ma�tre futuriste � Brooklyn - People - MYTF1News
DATE: 2014-02-10
URL: http://lci.tf1.fr/people/fashion-week-alexander-wang-s-impose-en-maitre-futuriste-a-brooklyn-8362386.html
PRINCIPAL: 0
TEXT:
fashion week , new york
PeopleS�r de son aura et de la fid�lit� d'un public conquis d'avance, l'avant-gardiste Alexander Wang a pr�sent� samedi soir une collection audacieuse et �tincelante.
Pourtant exil� � Brooklyn, Alexander Wang a su s'imposer avec son d�fil� plus que novateur. Sur fond d'un son tr�s �lectro, aux tonalit�s militaires, une cinquantaine de mannequins au regard durci et � la coupe d�lib�r�ment masculine ont accueilli le petit monde de la mode new-yorkais, tout juste arriv� de Manhattan.
Premier cr�ateur reconnu � oser l'aventure d'un d�fil� de mode en dehors de l'�le de Manhattan, au c�ur d'un hiver particuli�rement rigoureux, M. Wang avait choisi une gigantesque serre, la Duggal Greenhouse, pour h�berger son show. Inaugur� au printemps, Lady Gaga avait r�cemment utilis� cet espace pour lancer son dernier album.
�
Un service de voiturier Uber mis en place
Loin de d�courager les fashionistas arm�es de manteaux de fourrure et de petits escarpins Louboutin, Alexander Wang, �galement cr�ateur � la t�te de la direction artistique chez Balenciaga � Paris, a au contraire suscit� la curiosit� de monde de la mode.
Sur place, une jeune femme en talons aiguille, inqui�te � l'id�e d'arriver en retard, tente difficilement de se frayer un chemin sur la route verglac�e. "C'est assez intense comme exp�rience", avoue Maddie Raedts, apr�s 10 minutes de marche et environ une demi-heure de trajet en taxi. "Mais je suis certaine que cela en vaudra la chandelle", assure-t-elle en tendant enfin son grand carton d'invitation noir � l'entr�e du show, alors que surgit derri�re elle la ligne de gratte-ciels de
Manhattan.
Pour les autres, la maison avait organis� un service de bus et de ferrys depuis Manhattan et des rabais de 30% sur le service de voiturage Uber.Une fois dans la salle, le spectateur est d'embl�e happ� par une sc�ne futuriste et giratoire aux piliers m�talliques vers laquelle se dirigent les silhouettes androgynes de l'automne-hiver 2014 d'Alexander Wang.
�
V�tements sensibles � la chaleur
V�tue de grand cabans marine ou camel aux poches rappelant celles des vestes d'un chasseur ou d'un soldat, de sacs semblant garnis de cartouches, de pardessus aux lignes ac�r�es, loin de toute rondeur ou de facilit�, aux mati�res rugueuses, � effet bulle, la femme Wang est une femme pr�te � la "survie", selon les termes du cr�ateur, mais osant les couleurs les plus vives, le n�on.
La grande nouveaut� et star du d�fil� se trouvant dans cet ensemble de v�tements con�us dans des mati�res sensibles � la chaleur, s'illuminant et prenant tout leur �clat sous le feu des projecteurs. Seuls �l�ments de douceur, une s�rie de foulards bleu, rouge, noir au canari, habillant les cols blancs ou noirs de femmes au cou gracile mais, comme souvent cette saison, prot�g�.
"Mon id�e �tait de combiner des �l�ments vestimentaires avec les grands espaces et des conditions de temp�rature extr�me", r�v�le-t-il. "C'est un peu comme un collage entre tous ces aspects assez abstraits, que l'on a essay� de traduire en cr�ant une garde-robe".
�
Une audace r�compens�e
Le choix de Brooklyn prend alors tout son sens, selon lui, tr�s loin d'un caprice g�ographique d'une diva de la cr�ation new-yorkaise. "Pourquoi pas Brooklyn? Nous avons trouv� cette serre et elle correspondait tout � fait au concept de mati�res sensibles � la chaleur, � toute cette id�e de r�chauffement plan�taire et de (la mont�e) des temp�ratures".
Une chaleur victorieuse sur des docks enneig�s o� le public, conquis par tant d'audace, s'est empress� d'acclamer d�s les derni�res lueurs du show le jeune roi des podiums Made in NYC. "C'est l'un des d�fil�s les plus impressionnants qu'il ait jamais faits, j'ai ador�", applaudit Jawria Waupe, un jeune homme �l�gant en col roul� portant un sautoir agr�ment� d'une grosse croix. Plus que jamais, Alexander Wang s'impose comme l'un des maitres de la mode made in USA.
�
