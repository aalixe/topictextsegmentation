TITRE: Vente-privee.com publie un volume d'affaires de 1,6 milliard d'euros - JDN Web & Tech
DATE: 2014-02-10
URL: http://www.journaldunet.com/ebusiness/commerce/vente-privee-ca-2013-0214.shtml
PRINCIPAL: 160087
TEXT:
Toute l'actualit� Web & Tech
En 2013, le leader des ventes �v�nementielles a beaucoup fait progresser ses nouvelles activit�s : couponing, ticketing et voyage, qui atteint 150�millions d'euros.
Vente-privee.com a enregistr� en 2013 une progression de 23% de son volume d'affaires, qui a atteint 1,6�milliard d'euros�TTC contre 1,3 en 2012. L'e-commer�ant indique d'abord avoir organis� 65% de ventes �v�nementielles de plus que l'ann�e pr�c�dente, soit 10�100, ce qui correspond � 70�millions de produits vendus � une cadence de 50�000 � 150�000 colis exp�di�s chaque jour. Aux Etats-Unis, le site marchand annonce avoir doubl� ses revenus l'an dernier, � 50�millions de dollars pour 800 ventes, et viser l'�quilibre pour 2015.
Mais Vente-privee.com a aussi beaucoup fait progresser ses nouvelles sources de revenus et de commissions�: le couponing avec Rosedeal, la billetterie ou encore le voyage, activit� dont le volume d'affaires s'est �lev� � 150�millions d'euros en 2013. Le leader des ventes �v�nementielles revendique 20�millions de membres en Europe, o� les Fran�ais g�n�rent encore 80% de ses ventes. Il emploie 2�100 personnes, soit deux fois plus qu'il y a trois ans.
Coupon / Ventes �v�nementielles
