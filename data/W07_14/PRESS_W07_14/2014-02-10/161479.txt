TITRE: Hoefl-Riesch : �Moment �mouvant� - Fil info - Sotchi 2014 - Jeux Olympiques -
DATE: 2014-02-10
URL: http://sport24.lefigaro.fr/jeux-olympiques/sotchi-2014/fil-info/hoefl-riesch-moment-emouvant-678300
PRINCIPAL: 161468
TEXT:
Hoefl-Riesch : �Moment �mouvant�
10/02 14h53 - JO 2014
L'Allemande Maria Hoefl-Riesch est aux anges apr�s avoir conserv� son titre olympique ce lundi lors du Super-combin� olympique, � Sotchi : �Certes, l'attente autour de moi �tait grande. Je comptais parmi les favorites et devais en quelque sorte assumer ce r�le. Mais cela n'a pas �t� simple. Ce matin, j'ai connu quelques probl�mes en descente (5e). Le slalom �tait difficile, avec de la pente et la neige qui ramollissait � cause des temp�ratures. Aujourd'hui, c'est �videmment une de mes plus belles victoires, un moment �mouvant. Mais, �videmment, � Vancouver, c'�tait encore plus fort. Apr�s l'or du Super-combin�, j'avais eu celui du slalom.�
A lire aussi
