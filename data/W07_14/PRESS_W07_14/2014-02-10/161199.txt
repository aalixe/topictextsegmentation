TITRE: Une �quipe de la Croix-Rouge port�e disparue au Mali - Monde - MYTF1News
DATE: 2014-02-10
URL: http://lci.tf1.fr/monde/afrique/une-equipe-de-la-croix-rouge-portee-disparue-au-mali-8362838.html
PRINCIPAL: 0
TEXT:
cicr , mali
AfriqueLe Comit� international de la Croix-rouge a annonc� lundi qu'une de ses �quipes �tait port�e disparue dans le nord instable du Mali, sans savoir si elle avait �t� enlev�e.
"Nous pouvons confirmer que le Comit� international de la Croix-rouge a perdu le contact avec un de ses v�hicules, avec cinq personnes � son bord", a d�clar� un porte-parole du CICR , Alexis Heeb. Le v�hicule transportant quatre membres du CICR et un v�t�rinaire d'une autre organisation humanitaire, tous des Maliens, n'a plus donn� de nouvelles depuis samedi, sur la route reliant Kidal � Gao, a ajout� M. Heeb.
�
"Nous ne savons pas pourquoi le v�hicule a disparu. Cela pourrait �tre n'importe quoi", a expliqu� le porte-parole. "A ce stade, nous envisageons toutes les hypoth�ses. Nous sommes tr�s inquiets". Selon M. Heeb, le CICR est en contact r�gulier avec les autorit�s maliennes, ainsi qu'avec les divers groupes arm�s op�rant dans le nord du Mali .
�
Malgr� la pr�sence de forces fran�aises depuis d�but 2013, des rebelles islamistes arm�s continuent de mener des attaques sporadiques au Mali, surtout dans le nord du pays.
Nous confirmons : un de nos v�hicules avec 5 personnes � bord a disparu entre Gao et Kidal, au #Mali . Plus d'infos suivront.
