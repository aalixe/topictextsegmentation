TITRE: L�iPhone 6 d�t� d�un �cran incassable ? | Gossy
DATE: 2014-02-10
URL: http://www.gossymag.fr/liphone-6-dote-dun-ecran-incassable-art55725.html
PRINCIPAL: 160320
TEXT:
L�iPhone 6 d�t� d�un �cran incassable ?
Iphone 6
Dans quelques mois, Apple lancera son iPhone 6. En attendant, les rumeurs vont bon train et des vid�os ��concept�� mettent l�eau � la bouche. La derni�re concerne l��ventuel �cran ultra-r�sistant de l�iPhone 6.�
Trois mod�les pour l�iPhone 6
La derni�re vid�o concept a �t� r�alis�e par Enrico Penello et Ran Avni. Ils ont imagin� trois concepts de l�iPhone 6 d�Apple. L�iPhone 6 existerait en trois mod�les diff�rents, dont un dot� d�un �cran de 4 pouces, un qui atteindrait les 4,7 pouces et un dernier avec 5,5 pouces.
A l�origine l�iPhone 6 �tait d�clin� en deux versions , d�sormais on voit triple. Leur r�solution est exceptionnelle, allant de 326 ppp (1136�640) � 510 ppp (2272�1280). Pour un design plus �labor�, les deux concepteurs ont int�gr� des LED aux touches du contr�le du volume et � la touche ��home�� de l�iPhone. Enfin, on remarque �galement le fameux lecteur ��Touch ID��, d�j� per�u auparavant.
Toutes les nouveaut�s de l�iPhone donnent envie de se le procurer d�s sa sortie. Pourtant, les utilisateurs d�iPhone connaissent la fragilit� de l��cran de ses mobiles. Une fragilit� qui a tendance � repousser des potentiels clients qui se dirigeront alors vers la concurrence.
Adieu les coques ?
C�est pour cela que la marque � la pomme pourrait fabriquer des �crans en saphir pour les prochains iPhone. C�est du moins ce que rapporte le site 9to5Mac. Ces t�l�phones � l��cran incassable pourraient se passer d�une coque et r�sister aux rayures et aux chutes de l�iPhone. Selon les informations rapport�es par l�usine Mesa, Apple devrait sortir entre 150 et 200 millions d��crans couvert de saphir cette ann�e, d�s le mois de f�vrier.
Apple qui rencontre des difficult�s ces derniers mois pour tenir t�te � la concurrence aurait pris cette d�cision dans l�optique d�un �cran l�g�rement incurv� au niveau de sa bordure sup�rieure pour ses prochaines productions. Reste � savoir si l��cran au cristal de saphir s�duira les quelques maladroits qui ont d�j� cass� l��cran de leur iPhone. Et surtout, si cette nouvelle conception d��cran sera r�ellement incassable�
�
