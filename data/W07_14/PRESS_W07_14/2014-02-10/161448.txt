TITRE: Le Portugal retourne sur le march� de la dette - LExpress.fr
DATE: 2014-02-10
URL: http://lexpansion.lexpress.fr/economie/le-portugal-retourne-sur-le-marche-de-la-dette_428237.html
PRINCIPAL: 0
TEXT:
Le Portugal retourne sur le march� de la dette
Par AFP, publi� le
10/02/2014 � 18:33
, mis � jour � 19:44
Lisbonne - Le Portugal, sous perfusion financi�re internationale depuis 2011, a mandat� six banques pour lancer "dans un avenir proche" une �mission de dette � dix ans, sa premi�re depuis mai 2013, a annonc� lundi � l'AFP l'agence portugaise de la dette (IGCP).
Le Portugal, sous perfusion financi�re internationale depuis 2011, a mandat� six banques pour lancer "dans un avenir proche"une �mission de dette � dix ans, sa premi�re depuis mai 2013, a annonc� lundi � l'AFP l'agence portugaise de la dette (IGCP).
afp.com/Alain Jocard
L'�mission devrait normalement se d�rouler d�s mardi ou mercredi. Le Portugal souhaite ainsi b�n�ficier de la nette d�tente de ses taux d'emprunt � dix ans, qui sont pass�s en dessous de 5% alors qu'ils �taient encore � 6% en d�but de l'ann�e. �
"Il y a un groupe de six banques mandat�es pour lancer une �mission de dette dans un avenir proche. Cette �mission se fera d�s que les conditions du march� le permettront", a d�clar� un porte-parole de l'IGCP.�
Cet emprunt � long terme sera une �tape cruciale pour le Portugal qui cherche � s'affranchir de la tutelle de ses cr�anciers internationaux en mai, � la fin de son programme d'assistance de 78 milliards d'euros accord� en 2011.�
Un retour r�gulier sur les march�s est une des conditions exig�es par Bruxelles si le Portugal veut opter � l'issue de son plan de sauvetage pour une ligne de cr�dit de pr�caution � laquelle il peut avoir recours en cas de n�cessit�.�
Le Portugal avait d�j� r�alis� le 9 janvier avec succ�s un emprunt � cinq ans, qui lui a permis de lever 3,25 milliards d'euros � des taux en baisse (4,657%). L'�mission avait g�n�r� une demande de 11,2 milliards d'euros, plus de trois fois sup�rieure � l'offre.�
Gr�ce � cette �mission et un �change de dette de 6,64 milliards d'euros effectu� d�but d�cembre, le Portugal a pu r�duire ses besoins de financement pour cette ann�e � 3,85 milliards d'euros. Des produits d'�pargne plac�s par le Tr�sor aupr�s des particuliers devraient contribuer � all�ger davantage le fardeau.�
Lors du dernier emprunt � dix ans en mai 2013, le Portugal avait plac� 3 milliards d'euros au taux de 5,669%, suscitant une demande sup�rieure � 10 milliards d'euros.�
Prudent, le Portugal pr�pare � nouveau un emprunt syndiqu�, qui permet de limiter les risques en se finan�ant directement aupr�s de quelques banques s�lectionn�es � l'avance, qui peuvent ensuite conserver ou revendre les titres de dette.�
Le Portugal a ainsi mandat� les banques Barclays, Banco Espirito Santo, Citi, Cr�dit Agricole, Royal Bank of Scotland et Soci�t� g�n�rale pour conduire la r�ouverture de sa ligne obligataire assortie d'un taux de 5,65% et arrivant � �ch�ance en f�vrier 2024, a pr�cis� l'agence.�
Le niveau des taux d'emprunt de la dette du Portugal � dix ans "est compatible avec un plein retour aux march�s", avait estim� jeudi dernier Michele Napolitano, directeur de l'agence de notation Fitch pour les dettes souveraines.�
Le Portugal a cependant "int�r�t � s'assurer une ligne de pr�caution � l'issue de son actuel programme d'assistance" afin de "r�duire ses co�ts de financement" et de "renforcer la confiance des investisseurs", a-t-il ajout�.�
Par
