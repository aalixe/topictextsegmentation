TITRE: La plus vieille �toile de l'Univers observ�e par des astronomes australiens
DATE: 2014-02-10
URL: http://www.zinfos974.com/La-plus-vieille-etoile-de-l-Univers-observee-par-des-astronomes-australiens_a67994.html
PRINCIPAL: 159201
TEXT:
La plus vieille �toile de l'Univers observ�e par des astronomes australiens
Dans le m�me th�me�
Des scientifiques au coeur d'une �toile avant qu'elle ne devienne une supernova
� Space Telescope Science Institute/AAP
Des astronomes australiens ont fait une sensationnelle d�couverte. Ils ont pu r�cemment observer la plus vieille �toile connue de l'Univers, �g�e de 13,7 milliards d'ann�es et situ�e "seulement" � 6.000 ann�es-lumi�re de la Terre.
"Cela nous donne un aper�u de notre place fondamentale dans l'univers. Ce que nous observons, c'est l'origine de tous les mat�riaux dont nous avons besoin pour vivre", a d�clar� le Dr Stefan Keller, responsable des recherches de l'�cole de Recherches en Astronomie et Astrophysique de l'Australian National University.
Il aura fallu plus de 11 ann�es de travaux pour que l'�quipe �tudie enfin l'empreinte chimique de cette "premi�re �toile".
Selon Stefan Keller, cette �toile, form�e peu apr�s le Big Bang "donne un aper�u de notre place fondamentale dans l'Univers. Ce que nous observons, c'est l'origine de tous les mat�riaux dont nous avons besoin pour vivre (...) nous sommes en pr�sence de donn�es provenant de la premi�re g�n�ration d'�toiles", a-t-il soulign�.
Les recherches sur cette trouvaille sont publi�es dans la derni�re �dition de la revue scientifique Nature.
Lundi 10 F�vrier 2014 - 12:04
S.I
Notez
Lu 1068 fois, cliquez sur une des ic�nes ci-dessous pour partager cet article avec votre communaut�
Dans la m�me rubrique :
