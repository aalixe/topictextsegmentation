TITRE: La Gr�ce ne veut pas d'un troisi�me plan d'aide - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/economie/grece-assure-ne-pas-avoir-besoin-dun-troisieme-plan-daide-706458.html
PRINCIPAL: 158201
TEXT:
> Zone Euro
La Gr�ce ne veut pas d'un troisi�me plan d'aide
Le Premier ministre grec Antonis Samaras a affirm� que les objectifs fix�s seront atteint avec l'actuel plan d'aide, dans un entretien accord� au tablo�d allemand Bild � para�tre lundi 10 f�vrier.
C.C. avec AFP
Mis � jour le 09/02/2014 � 19:54
- +
r�agir
La Gr�ce n'a pas besoin d'un nouveau plan de sauvetage. Antonis Samaras, le Premier ministre grec, est ferme sur ce point:  "Nous n'avons pas besoin d'un troisi�me plan d'aide", a-t-il affirm� dans un entretien accord� au tablo�d allemand Bild � para�tre lundi 10 f�vrier.
"Nous atteignons notre objectif avec l'actuel plan d'aide, et cela fonctionne ", a-t-il ajout�.�
La Gr�ce a �t� le premier pays europ�en plac� en 2010 sous assistance financi�re de la Troika form�e par lUE-BCE-FMI qui lui a accord� deux programmes de pr�ts d'un montant total de 240 milliards d'euros pour �viter la faillite, en �change de mesures drastiques qui p�sent lourdement sur le quotidien de la population.
Ath�nes vise un exc�dent budg�taire primaire pour 2013
Face � un �lectorat majoritairement tr�s remont�, le gouvernement r�p�te ne pas avoir besoin d'une nouvelle aide mais revendique l'all�gement du poids de sa dette, en vertu d'engagements pris par ses partenaires europ�ens en 2012.
"Il y a un accord avec l'UE qui pr�voit que si nous atteignons nos objectifs parmi lesquels un exc�dent budg�taire primaire, nous pouvons compter sur un all�gement de notre dette, par exemple sous la forme de maturit�s plus longues et de taux plus bas ", a indiqu� Antonis Samaras.
Ath�nes table sur un exc�dent budg�taire primaire (hors service de la dette) pour 2013. "Il n'y a pas encore de chiffres fiables sur le d�ficit et le niveau de la dette pour 2013", a toutefois soulign� ce week-end dans la presse allemande le directeur g�n�ral d'Eurostat, Walter Radermacher.
En attendant que l'office europ�en des statistiques livre ces donn�es fin mars, "tous les autres chiffres qui sont annonc�s ne sont que pure sp�culation", a-t-il mis en garde.
A lire aussi
