TITRE: Le jury de �The Voice� n�a pas �buzz� pour elle, mais Liv s�est quand m�me offert un buzz� (VIDEO) - R�gion - La Voix du Nord
DATE: 2014-02-10
URL: http://www.lavoixdunord.fr/region/le-jury-de-the-voice-n-a-pas-buzze-pour-elle-ia0b0n1908136
PRINCIPAL: 0
TEXT:
Apr�s sa prestation remarqu�e dans The Voice, Liv raconte : �Je ne r�alise pas�
Et le Web ne s�y est pas tromp� ! Avec son interpr�tation pour le moins tr�s personnelle de Let It Be des Beatles, savant m�lange de voix gonfl�e � l�h�lium et d�une bande audio pass�e en acc�l�r�, la belle a enflamm� la Toile.
�
Liv fait le show avec sa version tr�s originale de � Let It Be � (The Beatles) sur WAT.tv
�
� peine achev�e la diffusion du passage de cette candidate originale, samedi soir, Twitter s�emballe. Les hashtags d�di�s se multiplient : #UnOvniDansTheVoice, #LIV, #StarDuLyc�eDemain� Les � twittos �, �moustill�s par la fra�cheur de ce petit bout de femme culott�e qui ne se prend pas au s�rieux, saluent sa prestation. � �pique �, � �tonnante �, � Une Martienne �, � Un Chipmunk �� Chacun y va de sa formule. De quoi faire monter la sauce du buzz� � Je n�ai pas mesur� �a, encore aujourd�hui je ne r�alise pas. C�est incroyable ! G�nial m�me ! �, confie Liv, le lendemain, alors qu�elle s�appr�te � monter � cheval, sa passion, comme si de rien n��tait�
Il faut dire qu�elle a la t�te sur les �paules, l�adolescente. Et si elle assume sans complexe sa prestation � � Je n�ai fait que rester moi-m�me, j��tais naturelle et je n�ai aucun regret � �, elle ne voudrait pas avoir donn� une image tronqu�e d�elle-m�me, que l��tiquette d�impertinente lui colle � la peau.
Pour � rigoler un peu ! �
� Je sais quand m�me prendre au s�rieux les choses qui le valent. Je ne suis pas non plus une fille l�g�re. �a n��tait pas calcul�, mais c��tait tout de m�me r�fl�chi. Et il fallait bien rigoler un peu ! �
Certes, sa chanson Let It Be revisit�e, elle l�a improvis�e un soir avec son prof de musique, le m�me depuis qu�elle a l��ge de 6 ans. � En fait, j�ai pass� un premier casting pour The Voice mais je n�ai pas �t� prise. La production m�a rappel�e � quelques jours des auditions ! � C�est le directeur artistique de l��mission lui-m�me qui aurait tenu � la voir, apr�s �tre tomb� sur des vid�os de la lyc�enne sur YouTube. Des images o� justement, la jeune fille chante avec cette voix devenue, depuis et un peu malgr� elle, sa marque de fabrique. � Je ne chante jamais comme �a. J�ai fait cette vid�o pour m�amuser. Mais la production l�a vue, elle m�a rappel�e et m�a demand� d�en faire une dans le m�me esprit, avec ces voix-l� et de venir le lendemain � Paris. C��tait la condition pour participer, mais je l�ai fait sciemment. J�aurais tr�s bien pu refuser� �
A-t-elle eu raison ? La production avait-elle flair� le ph�nom�ne ? Peu importe. Liv n�est pas d��ue. Elle dit avoir d�couvert � un autre monde �, dont elle ne soup�onnait pas l�existence. Pascal N�gre en personne, le PDG d�Universal Music France, rien que �a, a tweet� pour f�liciter l�artiste : � J�adore Liv ! Elle est d�mente� Mais gaffe il y a du talent l�-dessous� �
Hier, sur son compte Twitter, elle avait plus de 1 400 followers. Et tenait � rassurer ses nouveaux fans : � La musique ne s�arr�te pas l� ! � M�me si aujourd�hui, en bonne lyc�enne de 1re S qui se respecte, elle sera de retour sur les bancs de l��tablissement La Malassise, � Longuenesse. O� elle risque bien de continuer � faire le buzz.
�LODIE ADJOUDJ
