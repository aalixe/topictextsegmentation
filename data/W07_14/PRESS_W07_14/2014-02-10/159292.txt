TITRE: NBA - Noah et les Bulls en forme - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/sport/nba-noah-bulls-forme-706704.html
PRINCIPAL: 159291
TEXT:
r�agir
Noah et les Bulls assurent
Dans le duel le plus attendu de la soir�e, les Chicago Bulls ont pris le  meilleur sur le parquet des Lakers dimanche (92 � 86). Joakim Noah  s�est particuli�rement fait remarquer avec ses 20 points et 13 rebonds.  Les Bulls, qui prennent la 6e place de la Conf�rence Est, ont  m�me inflig� un 10-0 � leurs adversaires en d�but de match. Chris Kaman  et ses 27 points n�y ont rien fait. Les Lakers sont seulement 13e de la Conf�rence Ouest.
Les Clippers en mode record
Les Clippers n�ont pas fait dans le d�tail face � Philadelphie (123-78). 45 points d��cart�: nouveau record de la  franchise de Los Angeles. Ils ont m�me compt� jusqu'� 56 points d'�cart  dans la rencontre. Chris Paul �tait de retour mais c�est bien Blake  Griffin qui s�est une nouvelle fois distingu� en terminant meilleur  marqueur avec 26 points.
Durant au sommet de son art
A domicile, Oklahoma  s�est impos� face aux Knicks de New York (112-100). Auteur de 41 points,  Kevin Durant a survol� le match de toute sa classe, fr�lant m�me le  triple double. En face, Carmelo Anthony, pourtant deuxi�me meilleur  marqueur de NBA, a paru muet (15 points). Le Thunder est leader de la  Conf�rence Ouest avec 41 victoires pour 12 d�faites. New York pointe � la 10e place.
Orlando tombe le leader
Le match le plus serr� de la  soir�e de dimanche a tourn� � l�avantage d�Orlando, court vainqueur � domicile d�Indiana (93-92) apr�s avoir �t� men� de 17 points dans cette  rencontre. Faire tomber le leader de la Conf�rence Est, une performance  quand on sait que le Magic pointe seulement � la 13e place du  classement. Oladipo (23 points) et Vucevic (19 points) sont parvenus � contrer les assaut de Paul George, meilleur marqueur avec 27 points.
Tous les r�sultats�:
