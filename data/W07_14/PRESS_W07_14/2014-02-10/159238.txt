TITRE: Ben McKenzie jouera le jeune James Gordon dans la nouvelle s�rie Gotham - eparsa Magazine
DATE: 2014-02-10
URL: http://www.eparsa.com/index.php/culture/cinema-tv/item/2540-ben-mckenzie-jouera-le-jeune-james-gordon-dans-la-nouvelle-serie-gotham.html
PRINCIPAL: 159234
TEXT:
Ben McKenzie jouera le jeune James Gordon dans la nouvelle s�rie Gotham
Publi� dans Cin�ma & S�ries Tv dimanche, 09 f�vrier 2014 07:20
La s�rie Tv pr�quel � Batman initi�e par Fox, "Gotham", dispose officiellement de son acteur principal. Ben McKenzie, plus connu pour ses r�les dans Southland et The OC, a �t� retenu comme le jeune James Gordon - avant qu'il ne devienne le commissaire de police de Gotham City, et qu'il ne rencontre Batman.
Le mois dernier, nous avons appris que Gotham serait une s�rie d'origine charg� avec des personnages classiques de Batman. Comme le directeur de Fox, Kevin Reilly, l'avait dit � l'�poque, la s�rie suivra "l'arc de la fa�on dont [les personnages de Batman] sont devenus ce qu'ils sont maintenant.".
Bruno Heller a �crit le sc�nario du pilote et servira de producteur ex�cutif avec le r�alisateur de l'�pisode, Danny Cannon.
Un jeune Bruce Wayne est �galement pr�vu pour appara�tre dans Gotham. La s�rie se terminera s�rement au moment o� Batman enfile sa cape pour la premi�re fois. Aucun mot encore officiel sur lequel des m�chants de l'univers DC Comics fera son apparition dans la s�rie, mais nous sommes bien s�r ouverts � toutes formes de sp�culations! ;)
Lu 180 fois
