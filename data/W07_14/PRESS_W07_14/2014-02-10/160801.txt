TITRE: En Suisse, le sens d�un choix populaire | La-Croix.com
DATE: 2014-02-10
URL: http://www.la-croix.com/Actualite/Europe/En-Suisse-le-sens-d-un-choix-populaire-2014-02-10-1104285
PRINCIPAL: 160789
TEXT:
En Suisse, le sens d�un choix populaire
10/2/14 - 17 H 54
Le vote des �lecteurs helv�tiques contre l�immigration, dimanche 9 f�vrier, s�explique par la pr�sence importante des �trangers et la peur d�une perte d�identit�.Il aura des cons�quences importantes sur les relations de la Suisse avec l�Union europ�enne, les entreprises redoutant une pesanteur administrative.
Qu�est-ce qui va changer ?Le texte sur l�encadrement de l�immigration que les Suisses ont approuv� par r�f�rendum, dimanche 9 f�vrier, avec une courte majorit� (50,3�% des voix en faveur du ��oui��, soit 19�500�voix d��cart avec le ��non��) entra�ne automatiquement des ...
Pierre Cochez, Marie Dancer et Marianne Meunier
�POUR LIRE LA SUITE
Un email vous a �t� envoy� � l'adresse .
Il vous permet de choisir votre mot de passe.
�Pas encore abonn�(e) La Croix ?
ABONNEZ-VOUS
