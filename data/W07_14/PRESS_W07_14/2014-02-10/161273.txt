TITRE: Paris: gain laborieux, prudence avant Yellen et trimestriels - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/economie/paris-gain-laborieux-prudence-yellen-trimestriels-707298.html
PRINCIPAL: 161271
TEXT:
(CercleFinance.com) -  Si la bourse de Paris parvient � aligner une 3�me s�ance de hausse d'affil�e, est reste la seule place de l'Eurozone dans ce cas, avec Bruxelles.
L'euro-Stoxx50 s'enfonce dans le rouge (-0,2%) dans le sillage de Francfort (-0,13%) ou de Madrid (-0,9%).
Le CAC40 ne progresse que de +0,2% (� 4.325) et le doit int�gralement aux +4,5% de L'Or�al qui a repr�sent� 10% du volume n�goci� ce lundi.
Le N�1 mondial des cosm�tiques grimpe dans de gros volumes � quelques heures de la publication de ses r�sultats, suite aux sp�culations sur le d�sengagement de Nestl� qui permettrait � l'Or�al de racheter -et de rar�fier- ses titres en circulation).
A New York, la prudence et l'ind�cision r�gnent � la mi-s�ance, en cette veille de premi�re intervention de Janet Yellen devant le Congr�s: le Dow Jones perd 0,15% alors que le Nasdaq grappille 0,38%... et le 'S&P' affiche -0,05%.
A noter le recul plus marqu� (-0,35%) du Russel-2000 et surtout du 'Dow Transport' (-1,35%): ce sont les indices les plus m�diatiques qui se comportent le mieux.
Les op�rateurs dig�rent toujours les chiffres de l'emploi am�ricain publi�s vendredi apr�s-midi. Mitig�s et difficiles � d�crypter, ils ont fait ressortir des cr�ations d'emploi inf�rieures aux attentes mais un taux de ch�mage en repli � 6,6% : dans ce contexte paradoxal, le 1er discours de la nouvelle pr�sidente de la Fed (Janet Yellen) demain rev�tira un int�r�t tout particulier.
'C'est une semaine qui s'annonce importante pour enclencher un rebond ou alors confirmer le d�but de correction observ�e depuis le d�but de l'ann�e', r�sument les professionnels de Saxo Bank.
'Face � l'absence de visibilit� des investisseurs, aux craintes autour des �mergents et au risque de d�flation, les propos de Mme Yellen seront les market movers de la semaine', estime Aurel BGC. 'En Europe, la semaine sera marqu�e par une avalanche de publication de r�sultats d'entreprise', ajoute le bureau d'�tudes.
Il pr�cise que les grandes valeurs comme L'Or�al (aujourd'hui apr�s march�), Michelin, Metro (mardi), Heineken, Edenred, ING, Total, SG (mercredi), KBC, BNP Paribas, Pernod Ricard, Renault, Eni, EDF, Imerys, Publicis, Legrand (jeudi), ThyssenKrupp (vendredi) publieront leurs r�sultats trimestriels.
Les investisseurs prendront �galement connaissance de nombreuses statistiques �conomiques de part et d'autre de l'Atlantique. En revanche, aucune statistique am�ricaine d'importance ne sera diffus�e cet apr�s-midi.
Sur le front des valeurs, L'Or�al gagne 4,5%, Sanofi a repris +1,78%, Michelin +1,4%, Vallourec et l'Air Liquide ont fait jeu �gal avec +1,3%.
A la baisse, Orange reculait de -1,3%, Renault de -1,4% et Saint Gobain de -1,55%.
A noter la fermet� de l'Euro qui a pris +0,2% face au Dollar � 1,3650 et la poursuite de la hausse de l'once d'or � 1,276$/Oz � New York: la relation sym�trique entre la baisse du m�tal pr�cieux et la hausse des actions semble rompue depuis 1 semaine.
Copyright (c) 2014 CercleFinance.com. Tous droits r�serv�s.
Bourse
