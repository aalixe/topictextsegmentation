TITRE: Surprise au défilé de Victoria Beckham - 7SUR7.be
DATE: 2014-02-10
URL: http://www.7sur7.be/7s7/fr/1527/People/article/detail/1790396/2014/02/09/Surprise-au-defile-de-Victoria-Beckham.dhtml?show%3Dreact
PRINCIPAL: 159934
TEXT:
9/02/14 -  20h02��Source: AFP
� reuters.
Surprise dimanche � la Fashion week de New York : la famille Beckham est venue au grand complet, les quatre enfants et leur p�re, au d�fil� automne-hiver 2014 de Victoria.
� getty.
� reuters.
� afp.
Les trois gar�ons, Brooklyn, 14 ans, Romeo, 11 ans, Cruz, 8 ans, tr�s �l�gants, avaient les cheveux liss�s � l'arri�re comme leur p�re. Harper, 2 ans, toujours imp�riale face aux photographes, mini chignon ramass� sur le haut de la t�te, �tait assise sur les genoux de David Beckham, au premier rang, pr�s d'Anna Wintour, la r�dactrice en chef de Vogue USA et directrice artistique de Cond� Nast.
Le d�fil� de Victoria Beckham, 39 ans, �tait l'un des plus attendus dimanche, au quatri�me jour de la Fashion week.
Dans le d�cor blanc immacul� du "Cafe Rouge", en plein coeur de Manhattan, elle a pr�sent� une collection �l�gante et sophistiqu�e, avec beaucoup de robes tr�s longues, de nombreux pliss�s, des dos nus spectaculaires, des ampleurs soigneusement structur�es. Des cha�ne dor�es, ici et l�, soulignaient une taille, le noir et blanc �taient omnipr�sents.
"Il y a beaucoup plus de d�tails de style cette saison, et nous nous sommes aussi concentr�s sur le soir", a-t-elle expliqu� en coulisses apr�s le d�fil�.
"Il y a un �l�ment de surprise dans chaque tenue (...) Chaque saison, je pousse un peu plus loin", a-t-elle ajout�.
"J'aime les longues jupes f�minines, avec un pantalon plus masculin dessous", a-t-elle d�taill�, tr�s applaudie, et photographi�e par toute la famille � la fin de son d�fil�.
Parmi les autres d�fil�s ou pr�sentations pr�vus dimanche, figuraient notamment Delpozo, Yigal Azrou�l, Thakhoon, Derek Lam et Diane von Furstenberg, qui f�te les 40 ans de sa c�l�bre robe portefeuille.
� ap.
