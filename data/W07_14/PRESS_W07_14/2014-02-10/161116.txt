TITRE: Sotchi 2014: Fourcade et B�atrix, le duo qui d�bloque l��quipe de France - 20minutes.fr
DATE: 2014-02-10
URL: http://www.20minutes.fr/sport/1295146-sotchi-2014-fourcade-beatrix-le-duo-qui-debloque-l-equipe-de-france
PRINCIPAL: 0
TEXT:
Les deux Fran�ais m�daill�s en poursuite aux Jeux de Sotchi, Martin Fourcade (de face, or) et Jean-Guillaume B�atrix (de dos, bronze), le 10 f�vrier 2014 � Sotchi ALBERTO PIZZOLI / AFP
JEUX OLYMPIQUES - Avec leur doubl�, l��quipe de France olympique est d�sormais lanc�e�
De notre envoy� sp�cial � Sotchi (Russie),
Oubli�s les podiums effleur�s, les larmes d�apr�s course et les blessures de derni�re minute. Si la guigne a frapp� l��quipe de France olympique � Sotchi, celle-ci s�est vite envol�e, lundi, sous l�impulsion de Martin Fourcade et Jean-Guillaume B�atrix, les deux premiers m�daill�s fran�ais. Apr�s trois jours de comp�tition, le compteur bleu est enfin d�bloqu� et tous les sportifs qui entreront en lice seront d�sormais �pargn�s de la pression d�une premi�re breloque � aller chercher.
��a d�bloque tout, l�che St�phane Bouthiaux, le responsable des biathl�tes masculins. J�esp�re que �a va aider un peu tout le monde. On leur souhaite de faire comme nous. (aux skieurs alpins notamment). Qu�ils seront d�attaque pour gagner des m�dailles.��En pleurs, il reconnaissait aussi qu�une nouvelle journ�e blanche aurait �t� particuli�rement compliqu�e � g�rer.��Avec Siegfried Mazet (le coach du tir), on disait�lundi matin:��Putain si on n�a pas de m�daille, �a va commencer � grincer des dents�. Il nous la fallait. Quand tu commences � courir apr�s la m�daille, c�est tr�s compliqu�.�
Le DTN soulag�
Sentant lui aussi poindre une certaine morosit�, Martin Fourcade a reconnu qu�il avait pens� � l�ensemble du collectif bleu, dans la journ�e. ��M�me si on ne me l�a pas demand�, je devais d�bloquer le compteur de l��quipe de France. Voil�, je me suis dit, si tu ne le fais pas pour toi, au moins, fais-le pour les autres.��Grand prince, le nouveau champion olympique a aussi bien soulag� le DTN Fabien Saguez, qui se pr�parait d�j� � g�rer l�impatience du public et des m�dias fran�ais.
�Des attentes, il y en aura toujours. S�il n�y a pas de m�daille mardi, on nous dira que c��tait une journ�e sp�ciale. �a fait partie de notre quotidien. Ce n�est pas facile � vivre mais on est l� pour �a.��Pour lui, il n�est d�ailleurs pas question de revoir l�objectif de m�dailles initial. Avec quinze podiums, la France aura r�ussi ses Jeux. Mais il faut quand m�me esp�rer que les cris de joies et les embrassades du pas de tir de Laura en inspirent d�autres sur tous les sites de comp�tition.
Romain Scotto
