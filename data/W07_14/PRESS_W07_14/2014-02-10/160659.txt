TITRE: Temp�tes. Des milliers d'oiseaux morts sur le littoral atlantique
DATE: 2014-02-10
URL: http://www.ouest-france.fr/tempetes-des-milliers-doiseaux-morts-sur-le-littoral-atlantique-1921533
PRINCIPAL: 160657
TEXT:
Temp�tes. Des milliers d'oiseaux morts sur le littoral atlantique
France -
Le macareux moine fait partie de l'esp�ce la plus touch�e.�|�Cr�dit photo : Reuters
Facebook
Achetez votre journal num�rique
Les temp�tes ont provoqu� la mort de milliers d'oiseaux, la plupart succombant de fatigue.
Plusieurs milliers d'oiseaux marins ont �t� r�cup�r�s ces derniers jours, morts d'�puisement pour la plupart, sur les plages du littoral Atlantique dans le Sud-Ouest et l'Ouest. Une h�catombe � consid�rable � due � la succession r�cente de temp�tes, selon la LPO et les centres de soin.
Sur les plages des Pyr�n�es-Atlantiques � la Bretagne, un minimum de 5 000 oiseaux morts ont �t� recens�s, selon les donn�es recueillies et estimations provisoires de la Ligue de Protection des Oiseaux (LPO), a indiqu� Nicolas Gendre, ornithologue aupr�s de la LPO de Charente-Maritime.
Un millier d�oiseaux �chou�s en Pays de la Loire
� Le ph�nom�ne avait commenc� depuis une quinzaine de jours, mais a connu une mont�e en puissance ces derniers jours et ce week-end �, a soulign� Olivier le Gall, responsable de la Ligue de protection des oiseaux (LPO) d'Aquitaine.�
Selon Michael Potard, coordinateur r�gional LPO-Pays de la Loire, environ un millier d'oiseaux se sont �chou�s sur les plages de cette r�gion, d�compte provisoire ne prenant pas encore en compte le ramassage d'agents communaux.
� On est d�bord�s �
Les macareux moines constituent l'esp�ce la plus touch�e, au moins 50% des oiseaux morts recens�s selon les d�partements. Ensuite viennent les guillemots, puis en quantit� plus marginale des fous de Bassan, des petits pingouins Torda, des mouettes tridactyles.
� C'est une quantit� consid�rable, on est d�bord�s, mais par un ph�nom�ne de mortalit� presque naturelle, seulement cette fois il est visible � en raison des temp�tes amenant ces cadavres sur les plages, selon J�r�me Pensu, responsable du Centre de sauvegarde des Landes, o� environ un millier d'oiseaux morts ont �t� signal�s.
� � l'image des macareux, ce sont des oiseaux assez robustes, faits pour vivre au large, mais vu la succession des temp�tes et de la houle, ils sont plus qu'affaiblis, ne peuvent plus p�cher facilement, d�rivent, meurent �puis�s ou de faim �, a indiqu� Matthieu Sannier, de la LPO-Gironde.
Peu d�oiseaux retrouv�s vivants
Le nombre d'oiseaux pourrait augmenter avec la remont�e des donn�es �miett�es du week-end.
Dimanche, un seul b�n�vole a ainsi ramass� plus de 120 oiseaux sur trois plages des Landes sur un rayon de 10 km, Vieux-Boucau, Messanges et Moliets, 71 macareux, 54 guillemots, deux pingouins et une mouette pygm�e, selon le centre de soins Hegalaldia d'Ustaritz (Pyr�n�es-Atlantiques), qui couvre Pays basque et sud-Landes.
Le long du littoral, les oiseaux qui ont pu �tre r�cup�r�s vivants �taient trait�s dans des centres de soins, mais ils n'�taient qu'au nombre de plusieurs dizaines, a estim� la LPO.
Oiseaux mazout�s : � presque un �piph�nom�ne �
A Nantes par exemple, sur 300 oiseaux amen�s au Centre de soin v�t�rinaire Oniris, seuls cinq ont pu �tre soign�s, les autres �taient d�j� morts, a indiqu� lundi matin Michael Potard.
Partout les LPO estiment que la temp�te est la cause principale des d�c�s, m�me si quelques oiseaux sont mazout�s ou achev�s par des boulettes � presque un �piph�nom�ne. Car � chaque temp�te il y a toujours des gens qui en profitent pour d�gazer �, a estim� J�rome Pensu.
La LPO a mis sur son site web un rappel sur les premiers soins � dispenser � un oiseau r�cup�r�, notamment des conseils de prudence en raison du bec et des griffes, mais aussi un rappel � loi s'agissant du transport d'esp�ces prot�g�es.
Lire aussi
