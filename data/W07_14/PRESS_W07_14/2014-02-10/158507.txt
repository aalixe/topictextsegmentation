TITRE: Marie-Laure Brunet renonce � la poursuite - Jeux Olympiques 2013-2014 - Biathlon - Eurosport
DATE: 2014-02-10
URL: http://www.eurosport.fr/biathlon/jeux-olympiques-sotchi/2013-2014/marie-laure-brunet-renonce-a-la-poursuite_sto4129389/story.shtml
PRINCIPAL: 158506
TEXT:
18/02
Jeux Olympiques
Avec 4 m�dailles, Martin Fourcade devient le Fran�ais le plus m�daill� de l'histoire des JO d'hiver
18/02
Martin Fourcade m�daille d'argent sur la mass-start derri�re le Norv�gien Svendsen
17/02
Darya Domracheva (Bi�lorussie) remporte sa 3e m�daille d'or � Sotchi sur la mass start
17/02
Mass-Start : Marie Dorin Habert forfait
17/02
La mass-start messieurs report�e � mardi (officiel)
17/02
Le d�part de la mass-start pr�vu � 12h30
17/02
Nouveau report de la mass start en d�but d'apr�s-midi
16/02
La mass start masculine report�e � lundi
16/02
La mass-start messieurs report�e � lundi pour cause de brouillard
16/02
Le d�part de la mass-start repouss� d'une heure
14/02
Estanguet � Fourcade : "Bravo, t'es beau avec ta m�daille"
14/02
Bescond : "De loin mon meilleur r�sultat en individuel"
14/02
15km : nouveau titre pour Domracheva
13/02
S. Fourcade : " Mon fr�re �volue dans la stratosph�re depuis trois ans"
13/02
Beatrix, 6e et "pas d��u"
13/02
Jeux Olympiques
Martin Fourcade remporte un deuxi�me titre olympique � Sotchi avec sa victoire sur le 20 kilom�tres
11/02
Poursuite : Pas de m�daille pour Ana�s Bescond (12e), le titre pour Domracheva
10/02
Martin Fourcade : "J�ai eu le grain de folie pour �tre champion olympique"
10/02
Jean-Guillaume B�atrix : "Je n�ai jamais pens� � la m�daille"
10/02
Simon Fourcade : "Je suis tellement heureux pour Martin"
10/02
Fourcade - Val�rie Fourneyron : "La marque des grands champions"
10/02
Martin Fourcade champion olympique de la poursuite, Jean-Guillaume B�atrix en bronze
09/02
Brunet renonce � la poursuite
09/02
Ana�s Bescond termine � la 5e place du sprint, remport� par la Slovaque Kuzmina
09/02
L'IBU confirme les suspensions provisoires de 3 athl�tes
08/02
Ole Einar Bjoerndalen devient l'athl�te le plus m�daill� (12) avec Bj�rn Daehlie
08/02
Martin Fourcade 6e du sprint remport� par le Norv�gien Ole Einar Bjoerndalen
�
