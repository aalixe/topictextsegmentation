TITRE: Une bande de voleurs de grands crus bordelais démantelée | Site mobile Le Point
DATE: 2014-02-10
URL: http://www.lepoint.fr/societe/une-bande-de-voleurs-de-grands-crus-bordelais-demantelee-10-02-2014-1790179_23.php
PRINCIPAL: 160250
TEXT:
10/02/14 à 13h41
Une bande de voleurs de grands crus bordelais démantelée
Vingt personnes ont été placées en garde à vue lundi dans le Sud-Ouest et en Ile-de-France, dans le cadre d'une enquête sur une série de vols de grands crus dans les châteaux du Bordelais, pour un préjudice dépassant le million d'euros.
Les interpellations ont mobilisé quelque 300 gendarmes en Haute-Garonne, dans le Tarn, les Pyrénées-Atlantiques, l'Ile-de-France et la Gironde, où se trouvaient les commanditaires de ce réseau "très structuré" et "professionnel", qui sévissait depuis le mois de juin, a-t-on indiqué à la gendarmerie.
L'enquête était menée depuis plusieurs mois par le Groupement de gendarmerie de la Gironde et la section de recherches de Bordeaux pour démanteler ce réseau qui agissait notamment sur "commandes", comme en matière d'oeuvres d'art, et comportait des individus pour certains "très connus de la justice".
Sur les vingt interpellés, tous des hommes majeurs, une quinzaine de personnes l'ont été en Gironde, où elles se trouvent en garde à vue.
L'équipe, très organisée, procédait toujours selon un mode opératoire similaire, en se servant d'un véhicule volé et en dispersant de l'eau de Javel pour effacer toute trace après ses vols. Parmi les personnes interpellées, cinq sont soupçonnées de former le noyau dur du réseau, c'est-à-dire les "casseurs", et les autres organisaient le recel, avec deux équipes distinctes de receleurs.
Les gendarmes, qui agissaient dans le cadre d'une commission rogatoire d'une juge d'instruction du TGI de Bordeaux, ont retrouvé lors des interpellations des armes de poing et d'épaule, des espèces dont le montant n'a pas été révélé et une quantité importante de bouteilles de vin, a précisé la gendarmerie.
Souci d'image oblige, le milieu bordelais du vin était resté très discret sur ces vols, une quinzaine au total depuis juin, qui se produisaient environ "tous les quinze jours", a-t-on précisé de même source. Treize châteaux au total et deux dépôts, où étaient stockées des bouteilles provenant de plusieurs châteaux, ont été visés, a indiqué une source proche de l'enquête.
Les gardes à vue devraient être maintenues jusqu'à jeudi, les gendarmes ayant la possibilité d'interroger les suspects pendant 96 heures dans cette affaire concernant une association de malfaiteurs, mise en cause pour des vols "en bande organisée".
"C'est rassurant", a déclaré à l' AFP Bernard Farges, président du Conseil interprofessionnel du vin de Bordeaux (CIVB) en espérant que ce démantèlement dissuadera d'autres malfaiteurs. Selon lui, voler des grands crus peut être tentant, s'agissant de "produits de valeur", ayant une "notoriété", qui peuvent s'écouler facilement en se vendant à l'unité. Des vols similaires ont été répertoriés, mais jamais dans ces proportions, a-t-il estimé.
En juin, la gendarmerie avait déjà identifié une autre bande, sans lien avec les personnes interpellées lundi, accusée d'avoir volé plusieurs centaines de bouteilles et des bijoux, pour un butin estimé à plusieurs milliers d'euros.
Cette bande était soupçonnée d'avoir dérobé, dans un lieu de stockage à la périphérie de Bordeaux, environ 500 bouteilles de vins, dont la moitié de grands crus. Les bouteilles retrouvées par les enquêteurs, d'une valeur estimée à environ 300.000 euros, avaient été restituées à leur propriétaire.
