TITRE: Tempête de neige au Japon: 7 morts et plus de 1.000 blessés - 9 février 2014 - Le Nouvel Observateur
DATE: 2014-02-10
URL: http://tempsreel.nouvelobs.com/topnews/20140209.AFP9621/tempete-de-neige-au-japon-7-morts-et-plus-de-1-000-blesses.html
PRINCIPAL: 158190
TEXT:
Actualité > TopNews > Tempête de neige au Japon: 7 morts et plus de 1.000 blessés
Tempête de neige au Japon: 7 morts et plus de 1.000 blessés
Publié le 09-02-2014 à 08h05
Mis à jour à 19h21
A+ A-
Une exceptionnelle tempête de neige, inédite en près d'un demi-siècle sur la capitale, a tué 7 personnes et fait plus de 1.000 blessés samedi et dimanche à travers le Japon, selon un bilan provisoire de la chaîne de télévision NHK. (c) Afp
Tokyo (AFP) - Une exceptionnelle tempête de neige, inédite en près d'un demi-siècle sur la capitale, a tué 7 personnes et fait plus de 1.000 blessés samedi et dimanche à travers le Japon , selon un bilan provisoire de la chaîne de télévision NHK.
Un manteau blanc de quelque 27 centimètres a recouvert Tokyo samedi, ce qui ne s'était pas produit depuis 45 ans, selon l'agence nationale de météorologie.
Dimanche à la mi-journée, les grands axes intra-muros avaient été déblayés de même que certains trottoirs, par les services municipaux, ainsi que par des habitants ou commerçants qui s'affairaient dès les premières heures.
Les abords des bureaux de vote étaient nettoyés pour faciliter l'accès à plus de dix millions d'électeurs appelés à élire le gouverneur de la capitale.
La dépression, qui avait parcouru la veille le sud-ouest de l'île principale de Honshu avant de gagner Tokyo, à l'est, s'est ensuite déplacée vers le nord-est, déposant 35 centimètres de neige sur la ville côtière de Sendai qui n'avait pas connu un tel phénomène depuis 78 ans.
Temps relativement doux à Tokyo
Selon les différentes antennes locales de la chaîne publique NHK, 7 personnes au total sont mortes dans des accidents dus à la neige et au moins 1.051 individus à travers le pays ont été blessés, lors de chutes ou de collisions. Pas moins de 5.300 accidents divers auraient été enregistrés dans 35 préfectures affectées par les chutes de neige, selon la même source.
Plus de 20.000 foyers étaient de plus privés d'électricité dimanche matin et 300 vols ont été annulés en plus des 740 supprimés la veille.
Quelque 5.000 voyageurs sont restées bloqués en début de journée à l'aéroport international de Tokyo-Narita, à cause du mauvais état des routes le reliant à la capitale, distante d'une soixantaine de kilomètres.
Le temps était relativement doux à Tokyo dimanche le matin et à la mi-journée, mais d'autres chutes de neige étaient redoutées dans le nord du pays.
Partager
