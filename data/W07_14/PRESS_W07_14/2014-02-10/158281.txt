TITRE: HubiC : 10 To de stockage en ligne pour 10 � par mois, ou 100 Go pour 1 � - Next INpact
DATE: 2014-02-10
URL: http://www.pcinpact.com/news/85792-hubic-10-to-stockage-en-ligne-pour-10-par-mois-ou-100-go-pour-1.htm
PRINCIPAL: 0
TEXT:
HubiC : 10 To de stockage en ligne pour 10 � par mois, ou 100 Go pour 1 �
Et toujours 25 Go gratuits
OVH vient de mettre � jour les tarifs de HubiC , son service de stockage en ligne. Le premier palier de 25 Go est toujours gratuit, tandis qu'il est d�sormais question de 1 euro par mois pour 100 Go et de 10 euros pour 10 To, � chaque fois sans engagement.
Il y a un peu plus de deux ans , OVH lan�ait son service de stockage en ligne : HubiC. L'offre avait de quoi marquer les esprits avec 25 Go gratuits et de l'illimit� pour 83,71 euros par an. Un an plus tard, l'illimit� �tait arr�t� et l'h�bergeur revoyait ses offres, mais en conservant sa politique tarifaire agressive (0,1 euro�par Go par an).
�
Depuis, de nombreux changements ont eu lieu, notamment avec le passage vers OpenStack , la fermeture de l'acc�s WebDAV ,�la mise en place d'une API �attendue par de nombreux utilisateurs�et d'un client de synchronisation .�Mais la soci�t� roubaisienne ne compte pas en rester l� et Octave Klaba, son DG,� annonce �que les offres 2014 sont d�sormais en ligne :
�
�
Cette fois-ci, le prix varie entre 0,012 et 0,12 euro par Go et par an, contre 0,12 � 0,15 euro auparavant, ce qui est donc jusqu'� dix fois moins cher qu' Amazon Glacier .�Compar� � Dropbox, Google Drive ou OneDrive, l'�cart est encore bien plus important, mais ces derniers ont l'avantage de proposer plus de fonctionnalit�s.�Si HubiC frappe fort avec 10 To de stockage pour 10 euros par mois, des soci�t�s comme Nero , Bitcasa ou encore Carbonite pour ne citer que ces trois-l�,�proposent de l'illimit� pour respectivement 49,99 euros, 999 euros et 59,99 dollars par an.
�
Les conditions g�n�rales d'utilisation sont les m�mes que celles du 3 octobre 2013 et il est donc pr�cis� que�� la bande passante est limit�e � 10 Mbit/s en montant et descendant. Le d�bit de connexion d�pend �galement de la qualit�de la connexion Internet du Client �. Un point qui devrait principalement concerner ceux qui disposent de la fibre et quelques chanceux qui sont en VDSL2 (voir notre dossier ).
�
Notez enfin que la roadmap annonce l'arriv�e d'une nouvelle fonctionnalit� pour les applications Android et iOS : la possibilit� de sauvegarder les donn�es de son smartphone, un point int�ressant. L� encore, certains concurrents comme Nero proposent d�j� ce genre de service.
�
Voici enfin une vid�o de pr�sentation de HubiC :
�
