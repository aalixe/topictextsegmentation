TITRE: D�mant�lement d'un r�seau de voleurs de grands crus bordelais | France | Nice-Matin
DATE: 2014-02-10
URL: http://www.nicematin.com/france/une-bande-de-voleurs-de-grands-crus-bordelais-demantelee.1617908.html
PRINCIPAL: 160455
TEXT:
Tweet
Bordeaux (AFP)
Vingt personnes ont �t� plac�es en garde � vue lundi dans le Sud-Ouest et en Ile-de-France, dans le cadre d'une enqu�te sur une s�rie de vols de grands crus dans les ch�teaux du Bordelais, pour un pr�judice d�passant le million d'euros.
Les interpellations ont mobilis� quelque 300 gendarmes en Haute-Garonne, dans le Tarn-et-Garonne (bien Tarn-et-Garonne), les Pyr�n�es-Atlantiques, l'Ile-de-France et la Gironde, d�partement o� se trouvaient les commanditaires de ce r�seau "tr�s structur�" et "professionnel", qui s�vissait depuis le mois de juin, a-t-on indiqu� � la gendarmerie.
L'enqu�te �tait men�e depuis six mois par le Groupement de gendarmerie de la Gironde et la section de recherches de Bordeaux pour d�manteler ce r�seau qui agissait notamment sur "commandes", comme en mati�re d'oeuvres d'art, et comportait des individus pour certains "tr�s connus de la justice", notamment pour des faits de vols � main arm�e.
Les braqueurs, "tr�s bien renseign�s", proc�daient toujours selon un mode op�ratoire similaire, en se servant de v�hicules vol�s et en dispersant des d�tergents ou de l'eau de Javel pour effacer toute trace apr�s les effractions. Les v�hicules, d�rob�s lors de "home-jacking", �taient g�n�ralement retrouv�s br�l�s quelques heures apr�s les faits.
Cinq hommes, �g�s de 18 � 55 ans, sont soup�onn�s de former le noyau dur du r�seau, c'est-�-dire les "casseurs". Les autres organisaient le recel, avec deux �quipes distinctes de receleurs. Sur les vingt interpell�s, tous des hommes majeurs, une quinzaine l'ont �t� en Gironde, o� ils se trouvaient lundi en garde � vue.
Les gendarmes, qui agissaient dans le cadre d'une commission rogatoire d'une juge d'instruction du TGI de Bordeaux, ont retrouv� lors des interpellations "plusieurs centaines de bouteilles de vin", des "dizaines de milliers d'euros" en liquide, des v�hicules vol�s, des armes de poing et d'�paule, a pr�cis� lors d'un point presse le colonel Ghislain R�ty, commandant du Groupement de gendarmerie.
Jamais de cette ampleur
Souci d'image oblige, le milieu bordelais du vin �tait rest� tr�s discret sur ces vols, une quinzaine entre juin et d�but f�vrier, qui se produisaient environ "tous les quinze jours". Les derniers faits remontaient au 5 f�vrier, selon le colonel R�ty.
Treize ch�teaux au total et deux d�p�ts, o� �taient stock�es des bouteilles provenant de plusieurs ch�teaux, ont �t� vis�s, a indiqu� une source proche de l'enqu�te. Aucune complicit� avec des employ�s des ch�teaux vis�s n'a �t� �tablie "pour l'instant", a pr�cis� le commandant du groupement.
Les gardes � vue devraient �tre maintenues jusqu'� jeudi, les gendarmes ayant la possibilit� d'interroger les suspects pendant 96 heures dans cette affaire concernant une association de malfaiteurs, mise en cause pour des vols "en bande organis�e".
"C'est rassurant", a d�clar� � l'AFP Bernard Farges, pr�sident du Conseil interprofessionnel du vin de Bordeaux (CIVB) en esp�rant que ce d�mant�lement dissuadera d'autres malfaiteurs.
Selon lui, voler des grands crus peut �tre tentant, s'agissant de "produits de valeur", ayant une "notori�t�", qui peuvent s'�couler facilement en se vendant � l'unit�. Des vols similaires ont �t� r�pertori�s, mais jamais dans ces proportions, a-t-il estim�.
En juin, la gendarmerie avait d�j� identifi� une autre bande, sans lien avec les personnes interpell�es lundi, accus�e d'avoir vol� plusieurs centaines de bouteilles et des bijoux, pour un butin estim� � plusieurs milliers d'euros.
Cette bande �tait soup�onn�e d'avoir d�rob�, dans un lieu de stockage � la p�riph�rie de Bordeaux, environ 500 bouteilles de vins, dont la moiti� de grands crus. Les bouteilles retrouv�es par les enqu�teurs, d'une valeur estim�e � environ 300.000 euros, avaient �t� restitu�es � leur propri�taire.
Tweet
Tous droits de reproduction et de repr�sentation r�serv�s. � 2012 Agence France-Presse.
Toutes les informations (texte, photo, vid�o, infographie fixe ou anim�e, contenu sonore ou multim�dia) reproduites dans cette rubrique (ou sur cette page selon le cas) sont prot�g�es par la l�gislation en vigueur sur les droits de propri�t� intellectuelle. �Par cons�quent, toute reproduction, repr�sentation, modification, traduction, exploitation commerciale ou r�utilisation de quelque mani�re que ce soit est interdite sans l�accord pr�alable �crit de l�AFP, � l�exception de l�usage non commercial personnel. �L�AFP ne pourra �tre tenue pour responsable des retards, erreurs, omissions qui ne peuvent �tre exclus dans le domaine des informations de presse, ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
AFP et son logo sont des marques d�pos�es.
"Source AFP" � 2014 AFP
France
Le d�lai de 15 jours au-del� duquel il n'est plus possible de contribuer � l'article est d�sormais atteint.
Vous pouvez poursuivre votre discussion dans les forums de discussion du site. Si aucun d�bat ne correspond � votre discussion, vous pouvez solliciter le mod�rateur pour la cr�ation d'un nouveau forum : moderateur@nicematin.com
