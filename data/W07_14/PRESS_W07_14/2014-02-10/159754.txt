TITRE: Un espoir de la NFL d�voile son homosexualit� | Ralph D. Russo | Football
DATE: 2014-02-10
URL: http://www.lapresse.ca/sports/football/201402/09/01-4737149-un-espoir-de-la-nfl-devoile-son-homosexualite.php
PRINCIPAL: 159753
TEXT:
>�Un espoir de la NFL d�voile son homosexualit�
Un espoir de la NFL d�voile son homosexualit�
�Je suis ouvertement gai et j'en suis fier�, a affirm� Michael Sam.
Photo: AP
Ralph D. Russo
Associated Press
Le joueur de football de l'Universit� du Missouri, Michael Sam, a r�v�l� son homosexualit� et l'ailier d�fensif pourrait devenir le premier joueur ouvertement homosexuel � jouer dans la NFL.
Dans une entrevue avec ESPN , le New York Times et Outsports , Sam a affirm� qu'il avait fait part de sa situation � ses co�quipiers et � ses entra�neurs au mois d'ao�t.
�Je suis ouvertement gai et j'en suis fier�, a-t-il dit.
Le joueur de 255 livres a particip� au Senior Bowl le mois dernier apr�s avoir men� l'Association de football du Sud-Ouest au chapitre des sacs (11,5) et des plaqu�s pour des pertes (19). On s'attend � ce qu'il soit s�lectionn� lors du rep�chage de la NFL.
�C'est important. Personne n'a fait �a auparavant. Ce n'est pas un processus facile, mais je sais ce que je veux �tre... Je veux jouer dans la NFL�, a-t-il d�clar� en entrevue.
Quelques joueurs de la NFL ont affirm� leur homosexualit� apr�s avoir accroch� leurs crampons, comme Kwame Harris et Dave Kopay.
L'an dernier, le joueur de la NBA Jason Collins a r�v�l� son homosexualit� � la fin de la saison. Collins, un centre de 35 ans, est toujours joueur autonome. La vedette de la MLS et de l'�quipe nationale am�ricaine, Robbie Rogers, est aussi sorti du placard il y a un an.
Le botteur de Willamette, en troisi�me division, Conner Mertens, a affirm� �tre bisexuel le mois dernier.
�Nous admirons l'honn�tet� et le courage de Michael Sam, a indiqu� la NFL dans un communiqu�. Michael est un joueur de football. N'importe quel joueur qui a les habilet�s et la d�termination peut conna�tre du succ�s dans la NFL. Nous avons h�te d'accueillir Michael Sam et de le soutenir en 2014.�
Sam croit que plusieurs personnes semblaient �tre au courant de son homosexualit� lors du Senior Bowl.
�Je ne savais pas combien de gens le savaient, et je craignais que quelqu'un le dise, a-t-il d�clar� � ESPN. Personne ne devait r�v�ler �a � part moi.�
Avant d'annoncer la nouvelle � ses co�quipiers et � ses entra�neurs, Sam en avait d�j� parl� � des amis proches et il avait fr�quent� un autre athl�te de l'Universit� du Missouri, qui n'�tait pas un joueur de football.
�Les entra�neurs voulaient en savoir un peu plus sur nous, nos �tudes, d'o� nous venions et quelque chose que personne ne savait � notre propos, a racont� Sam. J'ai utilis� cette occasion pour leur dire que j'�tais gai. Ils ont sembl� dire �Michael Sam nous l'a enfin dit�.�
L'entra�neur de l'�quipe Gary Pinkel a publi� un communiqu� dimanche, affirmant qu'il �tait fier de Sam et de la mani�re dont il repr�sentait le programme.
�Michael est un bel exemple de l'importance d'�tre respectueux envers les autres. Il a appris � plusieurs personnes que peu importe notre pass� ou notre orientation, nous sommes tous dans la m�me �quipe et nous nous soutenons tous, a �crit Pinkel. S'il n'avait pas obtenu le soutien de ses co�quipiers, je ne crois pas qu'il aurait connu une saison aussi exceptionnelle.�
Partager
