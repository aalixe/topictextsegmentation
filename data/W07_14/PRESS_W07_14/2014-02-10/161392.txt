TITRE: Tensions: Les Bosniens continuent � descendre dans la rue -   Monde - lematin.ch
DATE: 2014-02-10
URL: http://www.lematin.ch/monde/Les-Bosniens-continuent-a-descendre-dans-la-rue/story/14353988
PRINCIPAL: 161379
TEXT:
Les Bosniens continuent � descendre dans la rue
Tensions
�
Ils r�clament des �lections anticip�es, promises par le chef du gouvernement selon une membre d'une d�l�gation d'opposants.
Mis � jour le 10.02.2014 9 Commentaires
1/8 Plusieurs centaines de policiers et de manifestants ont �t� bless�s � Sarajevo lors de heurts dimanche 9 f�vrier . (11 f�vrier 2014)
Image: AFP
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Des milliers de Bosniens sont � nouveau descendus dans la rue de plusieurs villes lundi pour manifester.
Des manifestations ont rassembl� des centaines de protestataires � Brcko (nord), Tuzla et Bihac (nord-ouest), Bugojno (centre) ainsi qu'� Vitez et Mostar (sud). A Sarajevo, plusieurs centaines de personnes se sont rassembl�es devant le si�ge de la pr�sidence. Ils r�clament la d�mission du gouvernement de l'entit� croato-musulmane qui avec une entit� serbe, forme la Bosnie.
Les manifestants se sont ensuite rendus devant le si�ge du gouvernement o� une d�l�gation improvis�e a �t� re�ue par le Premier ministre, Nevnin Niksic.
�Virer ces criminels�
Selon Amra Dulic, une membre de cette d�l�gation, le chef du gouvernement a promis d'oeuvrer pour la tenue d'�lections anticip�es tout en refusant de pr�senter sa d�mission. Il se serait engag� � demander � la justice d'ouvrir des enqu�tes dans des privatisations soup�onn�es d'avoir �t� r�alis�es d'une mani�re ill�gale.
�Il faut virer ces criminels. Nous n'avons rien � manger�, s'est exclam� un manifestant, Asim Kadric. �Ma retraite est de 340 marks (170 euros) et en hiver je donne la moiti� pour le chauffage. Ces bandits doivent partir tout de suite�, a lanc� cet ancien chauffeur de bus.
Archives de 1939-45 br�l�es
Vendredi, jour des rassemblements les plus violents, �un d�p�t des archives, celui qui abrite les plus anciens documents couvrant la p�riode entre l'Empire ottoman et la fin de la Seconde Guerre mondiale a �t� touch� par les flammes�, a-t-on indiqu� lundi de source officielle.
Une commission est en train d'�valuer l'�tendue des d�g�ts.
Villes musulmanes touch�es
Parti de Tuzla (nord-est), jadis la plus importante ville industrielle de Bosnie, o� des milliers de salari�s se sont retrouv�s au ch�mage � cause des �checs en s�rie des privatisations de leurs usines, le mouvement a gagn� la capitale et plusieurs autres grandes villes, notamment Mostar (sud) et Zenica (centre).
Il s'agit toutefois des villes o� les Musulmans sont majoritaires. Le mouvement n'a pas embras� les r�gions peupl�es par des Serbes et des Croates. Sauf � Mostar, une ville divis�e entre Croates et Musulmans, o� des manifestants des deux communaut�s ont saccag� vendredi ensemble l'immeuble abritant les autorit�s r�gionales.
Sous la pression de la rue, depuis le d�but mercredi de ces manifestations, les chefs de quatre administrations r�gionales de la F�d�ration croato-musulmane ont pr�sent� leurs d�missions.
Et dimanche, plusieurs dirigeants et partis politiques ont appel� � la tenue d'�lections l�gislatives anticip�es pour calmer le m�contentement populaire. Normalement, ces �lections doivent avoir lieu en octobre.
Institutions extr�mement compliqu�es
L'accord de paix de Dayton (Etats-Unis), a impos� � la Bosnie des institutions politiques extr�mement compliqu�es, au sein desquelles le pouvoir est partag� entre Musulmans, Serbes et Croates et les d�cisions doivent �tre adopt�es avec l'accord des trois communaut�s.
L'entit� croato-musulmane est form�e de dix cantons, chacun avec son propre gouvernement. Cette administration est pl�thorique avec quelque 180'000 salari�s pour un pays de 3,8 millions d'habitants. (ats/Newsnet)
Cr��: 10.02.2014, 17h38
