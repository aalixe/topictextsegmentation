TITRE: Ubisoft et Sony Pictures adaptent Les Lapins Cr�tins au cin�
DATE: 2014-02-10
URL: http://www.afjv.com/news/3402_ubisoft-et-sony-pictures-adaptent-les-lapins-cretins-au-cine.htm
PRINCIPAL: 161298
TEXT:
A propos
Ubisoft et Sony Pictures adaptent Les Lapins Cr�tins sur grand �cran
Ubisoft et Sony Pictures Entertainment annoncent leur collaboration pour r�aliser et adapter un film tir� des Lapins Cr�tins, les personnages compl�tement d�lur�s et irr�v�rencieux d'Ubisoft.
Jean-Julien Baronnet, Directeur g�n�ral d'Ubisoft Motion Pictures, et Hannah Minghella, Pr�sidente pour la Production chez Columbia Pictures, ont comment� cette nouvelle collaboration:
"Sony Pictures a une formidable exp�rience pour d�velopper des blockbusters � la nature hybride entre action r�elle et images d'animation pour les publics du monde entier, ce qui en fait un partenaire de choix pour un film Lapins Cr�tins", commente Jean-Julien Baronnet. "Ce partenariat approfondit encore davantage notre relation avec Sony Pictures et met en �vidence notre approche holistique pour �tendre les marques d'Ubisoft � de nouveaux publics tout en conservant l'int�grit� cr�ative de nos marques."
Hannah Minghella ajoute, "Il y a un caract�re tr�s communicatif chez les Lapins Cr�tins. Leur joie simple et leur comportement absurde et anarchique nous poussent � consid�rer le monde avec un regard nouveau. Leur invasion dans nos cin�mas entra�nera rires et chaos pour tous."
Les Lapins Cr�tins ont d�j� envahi le petit �cran cette ann�e gr�ce aux partenariats entre Ubisoft Motion Pictures, Nickelodeon et France T�l�visions. La s�rie t�l� � succ�s, intitul�e " Lapins Cr�tins : Invasion " a �t� vue plus de 165 millions de fois en cinq mois avec notamment plus de 2 millions de t�l�spectateurs en moyenne sur chaque �pisode aux �tats-Unis.
Les Lapins Cr�tins ont d�but� comme les ennemis surprenants mais craquants dans un �pisode du jeu vid�o Rayman et n'ont eu de cesse ensuite d'envahir le monde des humains: de la bande dessin�e, aux figurines, en passant par les jeux mobiles et la t�l�vision (et bien plus encore). L'exemple le plus r�cent �tant l'attraction The Lapins Cr�tins, La Machine � voyager dans le temps, construite dans le second plus grand parc � th�me de France, Le Futuroscope. L'attraction Lapins Cr�tins a attir� de nombreux visiteurs ravis lors de son week-end d'ouverture, d�montrant ainsi la capacit� d'Ubisoft Motion Pictures � d�velopper avec succ�s les marques d'Ubisoft sur de nouveaux m�diums.
Le projet sera supervis� chez Ubisoft par Ubisoft Motion Pictures, et le groupe participera activement � la production du film. Hannah Minghella et Jonathan Kadin superviseront le projet pour Sony Pictures.
Plus d'informations sur Ubisoft
Publi� le 10 f�vrier 2014 par Emmanuel Forsans
L'actualit� de Ubisoft
Derni�res publications
