TITRE: Les valeurs suivies � la cl�ture de la Bourse de Paris | �conomie | Reuters
DATE: 2014-02-10
URL: http://fr.reuters.com/article/businessNews/idFRPAEA1905E20140210
PRINCIPAL: 161271
TEXT:
Les valeurs suivies � la cl�ture de la Bourse de Paris
lundi 10 f�vrier 2014 18h37
�
[ - ] Texte [ + ]
PARIS (Reuters) - Liste des valeurs suivies lundi � la Bourse de Paris, o� l'indice CAC 40 a enregistr� un gain de 0,21% � 4.237,13 points apr�s un rebond de 2,7% au cours des deux s�ances pr�c�dentes, l'app�tit pour le risque marquant le pas � la veille de la premi�re audition de Janet Yellen devant le Congr�s depuis sa prise de fonction � la t�te de la R�serve f�d�rale am�ricaine.
* L'OR�AL a gagn� 4,45% � 129,0 euros, plus gros volume de la cote et plus forte hausse des indices CAC 40 et SBF 120, la sp�culation autour du titre ayant refait surface apr�s une information de presse selon laquelle Nestl� (-0,07%) r�fl�chirait aux moyens de r�duire sa participation dans le num�ro un mondial des cosm�tiques.
SANOFI a progress� de 1,78% � 71,99 euros, des investisseurs estimant que L'Or�al devrait vendre sa participation d'environ 9% dans le groupe pharmaceutique, valoris�e autour de 8,5 milliards d'euros, pour racheter les parts de Nestl�.
* Les PARAP�TROLI�RES ont gagn� du terrain alors que des analystes se disent plus optimistes pour le secteur cette ann�e apr�s un exercice 2013 p�nalis� par une absence de visibilit� sur la demande mondiale de p�trole et par des d�ceptions sur la croissance.
BOURBON a grimp� de 2,55% � 20,715 euros, CGG de 2,33% � 12,07 euros et VALLOUREC de 1,32% � 38,135 euros.
* ARCELORMITTAL (-3,2% � 12,095 euros) a accus� la plus forte baisse du CAC 40 et du SBF 120. Bank of America Merrill Lynch a abaiss� son conseil sur la valeur � "neutre" : le broker souligne la faiblesse de la reprise en Europe et craint que le groupe soit confront� � une baisse des prix de l'acier ou � des difficult�s de tr�sorerie.
* La CONSTRUCTION (-0,64%), les T�L�COMS (-0,4%) et les BANQUES (-0,3%) ont accus� les plus fortes baisses sectorielles en Europe.
SAINT-GOBAIN a l�ch� 1,54% � 39,74 euros et EIFFAGE 1,31% � 44,46 euros. ORANGE a c�d� 1,29% � 9,037 euros. BNP PARIBAS a perdu 0,84% � 59,01 euros, CR�DIT AGRICOLE 0,53% � 10,28 euros tandis que SOCI�T� G�N�RALE a r�sist� (+0,07% � 43,925 euros).
* RENAULT a perdu 1,37% � 63,18 euros. Son partenaire Nissan Motor a maintenu ses objectifs pour l'exercice qui sera cl�tur� fin mars, apr�s un b�n�fice trimestriel meilleur que pr�vu, ce qui a d��u les analystes qui s'attendaient � une r�vision en hausse. � Suite...
