TITRE: Short-track - 500 m dames: Pierron �limin�e d�s les s�ries - Jeux Olympiques 2013-2014 - Short track - Eurosport
DATE: 2014-02-10
URL: http://www.eurosport.fr/short-track/jeux-olympiques-sotchi/2013-2014/short-track-500-m-dames-pierron-eliminee-des-les-series_sto4129944/story.shtml
PRINCIPAL: 159962
TEXT:
Short-track - 500 m dames: Pierron �limin�e d�s les s�ries
le 10/02/2014 � 11:51, mis � jour le 10/02/2014 � 12:11
La Fran�aise V�ronique Pierron a �t� �limin�e d�s les s�ries de l'�preuve du 500 m de short-track des jeux Olympiques apr�s une chute sans gravit�.
Pierron, alors en 4e position, a chut� toute seule, chute sans gravit� mais qui est synonyme d'�limination. La seule Fran�ais engag�e dans les �preuves de short-track doit encore participer aux 1000 et 1500 m.
�
