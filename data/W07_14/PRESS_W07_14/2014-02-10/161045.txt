TITRE: Municipales 2014 - Paris : Sarkozy au premier meeting de NKM ? | Site mobile Le Point
DATE: 2014-02-10
URL: http://www.lepoint.fr/municipales-2014/municipales-2014-paris-sarkozy-au-premier-meeting-de-nkm-10-02-2014-1790219_1966.php
PRINCIPAL: 160977
TEXT:
10/02/14 à 16h44
Municipales 2014 - Paris : Sarkozy au premier meeting de NKM ?
L'ancien chef de l'État serait l'invité du premier meeting de la candidate UMP à la mairie de Paris, qui a lieu lundi soir dans le 11e arrondissement.
Nathalie Kosciusko-Morizet et Nicolas Sarkozy à un match du PSG, au Parc des princes, ici en mai 2013. KENZO TRIBOUILLARD / AFP
Le Point.fr (avec AFP)
La rumeur court depuis la semaine dernière dernière : Nicolas Sarkozy pourrait assister au premier meeting, lundi soir, de la candidate UMP à la mairie de Paris Nathalie Kosciusko-Morizet. Pour le moment, l'ancienne ministre n'a ni confirmé ni démenti la présence de l'ancien chef de l'État. La réunion doit se dérouler au gymnase Japy, dans le 11e arrondissement, un lieu qui peut contenir jusqu'à 2 500 personnes debout.
Hormis la possible présence de Nicolas Sarkozy, Nathalie Kosciusko-Morizet, qui se présente dans le 14e arrondissement, sera entourée de ses alliés Christian Saint-Étienne (UDI, candidat dans le 11e), et Marielle de Sarnez (MoDem), et des 18 autres têtes de liste. Selon Le Monde, les têtes de liste ont reçu un SMS lundi matin pour avancer l'heure à laquelle elles devaient se présenter ce lundi soir, le meeting impliquant des candidats du MoDem, qui ont soutenu François Hollande à la présidentielle de 2012.
Mécontentements et dissidences
Les porte-parole de la candidate, Pierre-Yves Bournazel et Valérie Montandon, animeront la soirée, rythmée par les interventions du président de la fédération UMP de Paris Philippe Goujon, de Christian Saint-Étienne et de la députée de l'Essonne. Le meeting devrait être l'occasion pour la droite et le centre de célébrer une nouvelle fois leur union, scellée le 5 décembre, même si la constitution de listes communes ne va pas sans mal : les négociations tendent à s'éterniser et s'accompagnent, une fois conclues, de multiples mécontentements et menaces de dissidences.
Malmenée dans les sondages à six semaines du premier tour des élections, la candidate de l'UMP devrait reprendre dans son discours les thèmes développés dans une "lettre aux Parisiens" diffusée massivement depuis jeudi : "révolution des horaires", stabilité des impôts, doublement de la vidéosurveillance...
Ni l'ancien Premier ministre François Fillon ni le président de l'UMP Jean-François Copé ne devraient être présents. Le premier a une nouvelle fois apporté son soutien à la candidate jeudi, à l'occasion d'un déplacement dans le 8e. Le second doit effectuer mercredi une tournée électorale dans les 5e, 9e et 17e arrondissements. Selon un sondage Ifop-Fiducial pour le JDD et Sud Radio publié dimanche, la liste soutenue par NKM dans le 12e serait largement battue si l'élection avait lieu aujourd'hui. Le 12e et le 14e sont les deux arrondissements que la droite doit reconquérir pour espérer reprendre la capitale à la gauche.
