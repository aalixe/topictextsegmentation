TITRE: JO: Martin Fourcade, premier médaillé d'or français | Site mobile Le Point
DATE: 2014-02-10
URL: http://www.lepoint.fr/fil-info-reuters/jo-martin-fourcade-premier-medaille-d-or-francais-10-02-2014-1790235_240.php
PRINCIPAL: 0
TEXT:
10/02/14 à 16h48
JO: Martin Fourcade, premier médaillé d'or français
SOTCHI, Russe ( Reuters ) - Martin Fourcade a offert à la France sa première médaille d'or des Jeux olympiques de Sotchi en remportant lundi l'épreuve de poursuite sur 12,5 km de biathlon dont son compatriote Jean-Guillaume Beatrix a pris la médaille de bronze.
Le Tchèque Ondrej Moravec s'est intercalé entre les deux Français. Le Norvégien Ole Einar Bjoerndalen, vainqueur samedi de l'épreuve de sprint, est resté au pied du podium.
Jean-Paul Couret pour le service français
