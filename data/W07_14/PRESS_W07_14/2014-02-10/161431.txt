TITRE: Espace: Ils d�couvrent la plus vieille des �toiles -  News Savoirs: Sciences - 24heures.ch
DATE: 2014-02-10
URL: http://www.24heures.ch/savoirs/sciences/decouvrent-vieille-etoile-monde/story/17213179
PRINCIPAL: 161423
TEXT:
Ils d�couvrent la plus vieille des �toiles
Mis � jour le 10.02.2014
Des scientifiques australiens ont identifi� l'�toile la plus vieille jamais d�couverte, relate la revue scientifique Nature dans sa derni�re �dition.
La fl�che pointe sur la plus vieille �toile connue actuellement.
Image: ESO
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
E-Mail*
Veuillez SVP entrez une adresse e-mail valide
D'autres scientifiques en avaient s�rement r�v� avant eux. Ils l'ont fait: ils ont d�couvert la plus vieille des �toiles
L'astre a 13,6 milliards d'ann�es, soit la p�riode de l'origine de l'univers lui-m�me.
Selon les calculs des scientifiques, l'�toile ne s'est form�e qu'environ 200 millions d'ann�es apr�s le Big Bang. Jusque-l�, les astres les plus vieux que l'on connaisse �taient deux corps c�lestes �g�s d'environ 13,2 milliards d'ann�es.
L'�toile dont il est question est relativement proche de la Terre, a estim� Stefan Keller de l'Observatoire du mont Stromlo de Canberra, capitale de l'Australie. Elle se trouve dans notre galaxie, la Voie lact�e, et � environ 6000 ann�es-lumi�re de notre plan�te.
Cet astre nouvellement d�couvert porte le num�ro J031300.36-670839.3. (ats/Newsnet)
Cr��: 10.02.2014, 18h05
Votre email a �t� envoy�.
Publier un nouveau commentaire
Nous vous invitons ici � donner votre point de vue, vos informations, vos arguments. Nous vous prions d�utiliser votre nom complet, la discussion est plus authentique ainsi. Vous pouvez vous connecter via Facebook ou cr�er un compte utilisateur, selon votre choix. Les fausses identit�s seront bannies. Nous refusons les messages haineux, diffamatoires, racistes ou x�nophobes, les menaces, incitations � la violence ou autres injures. Merci de garder un ton respectueux et de penser que de nombreuses personnes vous lisent.
La r�daction
Merci de votre participation, votre commentaire sera publi� dans les meilleurs d�lais.
Merci pour votre contribution.
J'ai lu et j'accepte la Charte des commentaires.
Caract�res restants:
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
S'il vous pla�t entrer un nom valide.
S'il vous pla�t indiquez le lieu g�ographique.
Ce num�ro de t�l�phone n'est pas valable.
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
Signup Fermer
Vous devez lire et accepter la Charte de commentaires avant de poursuivre.
Nous sommes heureux que vous voulez nous donner vos commentaires. S'il vous pla�t noter les r�gles suivantes � l'avance: La r�daction se r�serve le droit de ne pas publier des commentaires. Ceci s'applique en g�n�ral, mais surtout pour les propos diffamatoires, racistes, hors de propos, hors-sujet des commentaires, ou ceux en langues �trang�res ou dialecte. Commentaires des noms de fantaisie, ou avec des noms manifestement fausses ne sont pas publi�s non plus. Plus les d�cisions de la r�daction n'est ni responsable d�pos�e, ni en dehors de la correspondance. Renseignements t�l�phoniques ne seront pas fournis. L'�diteur se r�serve le droit �galement � r�duire les commentaires des lecteurs. S'il vous pla�t noter que votre commentaire aussi sur Google et autres moteurs de recherche peuvent �tre trouv�s et que les �diteurs ne peuvent rien et est de supprimer un commentaire une fois �mis dans l'index des moteurs de recherche.
�auf Facebook publizieren�
Veuilliez attendre s'il vous pla�t
Soumettre Commentaire
Soumettre Commentaire
No connection to facebook possible. Please try again. There was a problem while transmitting your comment. Please try again.
