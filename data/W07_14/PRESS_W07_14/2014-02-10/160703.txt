TITRE: Festival de Cannes: le palmar�s d�voil� un jour plus t�t - L'Express
DATE: 2014-02-10
URL: http://www.lexpress.fr/culture/cinema/festival-de-cannes-le-palmares-devoile-un-jour-plus-tot_1322224.html
PRINCIPAL: 160699
TEXT:
Voter (0)
� � � �
La r�alisatrice n�o-z�landaise Jane Campion pr�sidera le jury du 67e Festival de Cannes qui se d�roulera du 14 au 25 mai.
afp.com/Anne-Christine Poujoulat
Les �lections europ�ennes bouleversent le calendrier du Festival de Cannes. Le palmar�s du 67e Festival de Cannes sera d�voil� un jour plus t�t, le samedi 24 mai, soit la veille de la cl�ture du festival le dimanche. De cette fa�on, l'annonce du palmar�s ne co�ncidera pas avec les �lections europ�ennes, a annonc� ce lundi la direction du festival.�
La direction du Festival de Cannes a annonc� que "le dernier film de la comp�tition sera projet� le vendredi 23 mai et le jury d�lib�rera le lendemain pour d�signer les laur�ats". Le dimanche 25 sera consacr� � la projection de la Palme d'or . Pendant le week-end des 24 et 25 mai, tous les films de la S�lection officielle feront l'objet d'une reprise dans les salles du Palais des festivals.�
Le 67e Festival de Cannes, pr�sid� par la r�alisatrice n�o-z�landaise Jane Campion , d�butera le mercredi 14 mai. Les prix "Un Certain Regard" et Cin�fondation seront annonc�s le vendredi 23 mai. �
�
Avec
Sur le m�me sujet
