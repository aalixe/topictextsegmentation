TITRE: Bertrand Delano� n'est �pas triste� de quitter la mairie de Paris
DATE: 2014-02-10
URL: http://www.boursorama.com/actualites/bertrand-delanoe-n-est-pas-triste-de-quitter-la-mairie-de-paris-68e16b2e207a62357397ca966d4c1984
PRINCIPAL: 161527
TEXT:
Imprimer l'article
Bertrand Delano� n'est �pas triste� de quitter la mairie de Paris
Treize ans apr�s avoir arrach� de haute lutte la capitale � la droite,�le maire socialiste Bertrand Delano� pr�sidait ce lundi son dernier Conseil de Paris.��g� de 63 ans et adepte du non-cumul des mandats, il avait promis d�s 2007 de ne pas briguer un troisi�me mandat en 2014.
�Je n'ai aucune nostalgie, aucune tristesse, j'ai plein de joie dans le coeur. Parce que c'est Paris. Parce que cette ville m�rite tous les enthousiasmes, m�rite tous les �lans d'amour, m�rite de travailler, m�rite qu'on donne tout�, a-t-il d�clar� en�concluant une s�ance�tr�s all�g�e, au cours de laquelle se sont succ�d� les hommages de la majorit� et de l'opposition.
Standing ovation
�voquant tr�s bri�vement son bilan, Bertrand Delano� a une nouvelle fois soulign� qu'il y avait �125.000 Parisiens de plus� qu'en 2001, 16.000 familles, 31.000 jeunes.�
�Les Parisiens m'ont fait l'honneur de me faire confiance en 2001 et en 2008. Je crois que je n'ai jamais rien re�u de plus grand, de plus noble, de plus exigeant�, a-t-il poursuivi.  Le maire PS de Paris a salu� les anciens maires RPR Jacques Chirac et Jean Tiberi, ainsi que ses adversaires malheureux de 2001 et 2008 Philippe S�guin et Fran�oise de Panafieu.�Il a aussi rendu hommage � ceux qui comme lui vivaient �leurs derniers instants... de conseiller de Paris�.�
Elus de droite comme de gauche se sont lev�s � la fin du discours du maire pour l'applaudir, � l'exception du pr�sident de la f�d�ration UMP Philippe Goujon. Une r�action que n'a pas manqu� de noter�sur Twitter Bruno Julliard, adjoint � la culture.�Pr�sente au cours des d�bats, la maire UMP du VIIe arrondissement, Rachida Dati, s'�tait quant � elle absent�e.
Philippe Goujon, sectaire et goujat, est le seul elu du Conseil de Paris a ne pas rendre hommage a Bertrand Delanoe pour sa derni�re s�ance.? Bruno Julliard (@BrunoJulliard) 10 F�vrier 2014
Sur Twitter, de nombreux conseillers de ...
