TITRE: Les loyers ont moins augment� que l'inflation - Prix immobilier | LaVieImmo.com
DATE: 2014-02-10
URL: http://www.lavieimmo.com/prix-immobilier/les-loyers-ont-moins-augmente-que-l-inflation-24794.html
PRINCIPAL: 159413
TEXT:
Prix des villes au m2 :
Rechercher sur le site :
Prix immobilier lundi 10 f�vrier 2014 � 10h08
Les loyers ont moins augment� que l'inflation
Tweet
Les loyers se tassent � Paris (�dr)
Les loyers ont progress� de 0,6 % sur douze mois tous types de biens confondus d�apr�s Century 21, soit 0,1 point de moins que les prix � la consommation. Pour le r�seau, le march� locatif � s�autor�gule �.
(LaVieImmo.com) - Le march� locatif s�est ajust� cette ann�e, comme le montre la derni�re note du r�seau Century 21. Les loyers ont recul� de 0,1% en euros constants entre novembre 2012 et novembre 2013, affichant une hausse de 0,6 % contre une inflation � 0,7 %. Une d�c�l�ration des prix plus flagrante dans les zones tendues : le loyer moyen est en retrait de 0,7 % � Paris, � portant ainsi � -1,4% la baisse nette relative � l�inflation (de 26,8 �/m� � 26,6 �) � selon le groupe. Tout comme en Ile-de-France  (-1,2 % ; de 16,21 �/m� � 16,12 �), mais aussi Lyon ou Marseille.
Les jeunes moins pr�sents sur le march�
Mais les loyers sont aussi en tassement par rapport � l�indice de r�f�rence des loyers (IRL), qui a augment� de 0,9 % sur un an. � Le march� s�autor�gule : les locations nouvelles pr�sentent des loyers plus mod�r�s tandis que sur les baux en cours, l�application de l�indice IRL n�a pas �t� effectu�e �, analyse Century 21.
A lire aussi
