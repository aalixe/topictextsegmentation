TITRE: Google est oblig� d'afficher sa condamnation sur sa page d'accueil
DATE: 2014-02-10
URL: http://www.boursorama.com/actualites/google-est-oblige-d-afficher-sa-condamnation-sur-sa-page-d-accueil-6d2e53eb67c2cfa7147079e322a8ed8e
PRINCIPAL: 0
TEXT:
Ajouter des widgets dans vos pages Actualit�s
Google est oblig� d'afficher sa condamnation sur sa page d'accueil
Zone Num�rique le 08/02/2014 � 12:40
Imprimer l'article
Suite � la condamnation de Google �� une amende exemplaire de 150 000 euros pour non-respect de la loi "Informatique et Libert�s" le g�ant du Web a d�pos� un recours aupr�s du Conseil d'�tat pour s'opposer � cette amende maximale. C'est la Cnil qui avait demand� la peine maximale encourue suite � la mise en place de la politique de confidentialit� de Google et sa non-modification comme demand�e pr�c�demment.�Cette modification avait pour objectif de respecter la protection des donn�es personnelles des internautes. Google avait huit jours pour publier sur son site Internet (Google.fr), un communiqu� suite � cette d�cision, et devait le laisser durant deux jours suite � sa notification.  [caption id="attachment_72611" align="aligncenter" width="500"]
Message affich� sur la page d'accueil de Google durant ce weekend[/caption]  Le Conseil d'�tat a refus� de suspendre la d�cision de la Cnil et Google a d� afficher sa condamnation sur la page d'accueil de son moteur de recherche. Ce communiqu� sera visible pendant 48 heures comme le demandait la Cnil. En plus de cet affichage, le g�ant du Web devra verser une amende de 150 000 euros, certes une goutte d'eau pour la firme de Mountain View mais c'est la peine maximale qui pouvait �tre prononc�e par les autorit�s Fran�aises. Un lien est ajout� � la fin du message afin d'expliquer cette condamnation aux internautes
