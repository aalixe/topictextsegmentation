TITRE: Les Inrocks - John McTiernan r�alisera un nouveau film � sa sortie de prison
DATE: 2014-02-10
URL: http://www.lesinrocks.com/2014/02/08/cinema/john-mctiernan-realisera-un-nouveau-film-des-sa-sortie-de-prison-11470712/
PRINCIPAL: 159458
TEXT:
John McTiernan r�alisera un nouveau film � sa sortie de prison
08/02/2014 | 18h47
John Travolta, Connie Nielsen et Samuel L. Jackson dans "Basic" de John McTiernan
Arrivant au terme de sa peine controvers�e d�un an de prison, John McTiernan passe aux manettes du projet �Red Squad�. Un retour aux affaires inesp�r�.
Le cin�aste n�a pas sign� un film depuis Basic en 2003, emp�tr� dans l��affaire Pellicano� qui l�oppose � son ancien producteur Charles Roven et qui l�a finalement, malgr� un remarquable mouvement de soutien, men� en prison. Il y purge une peine d�un an qui s�ach�vera en avril prochain.
Le r�alisateur de Die Hard et d�� la poursuite d�Octobre rouge avait �t� inculp� apr�s s��tre offert les services d�un d�tective priv�, Anthony Pellicano, pour espionner son producteur pendant le tournage cauchemardesque de Rollerball en 2002�: il le soup�onnait alors de manigancer un sabordage de son propre film. M�me si la peine n��tait que d�un an, l�avenir de John McTiernan en tant que cin�aste restait extr�mement incertain�: outre ces poursuites judiciaires, le ma�tre du cin�ma d�action peinait surtout � faire assurer ses r�alisations depuis plus de dix ans. Le calvaire judiciaire finalement achev� par une incarc�ration qu�il avait certes tout fait pour �viter, le chemin des tournages �tait encore loin de lui �tre trac�.
C�est donc une excellente surprise que Hannibal Films fait aux fans, en mettant fin � dix ans d�absence�: Red Squad racontera la traque d�un baron des cartels par un ancien agent des stups accompagn� de son �quipe de mercenaires. Un synopsis qui, m�me si McTiernan vient tout juste d��tre attach� au projet sans avoir pr�alablement pris part � l��criture, peut rappeler son dernier film, Basic, qui suivait �galement l�enqu�te officieuse d�un stup � la retraite (interpr�t� par John Travolta).
Le tournage d�marrera d�s sa sortie de la prison f�d�rale du Dakota du Nord. Aucune autre information n�a pour l�instant �t� donn�e par Hannibal Films.
