TITRE: Le Wifi gratuit contre de la pub dans les gares du Mans, d'Angers et de Nantes avant la fin 2014 - France 3 Pays de la Loire
DATE: 2014-02-10
URL: http://pays-de-la-loire.france3.fr/2014/02/10/le-wifi-gratuit-contre-de-la-pub-dans-les-gares-du-mans-dangers-et-de-nantes-avant-la-fin-2014-412767.html
PRINCIPAL: 160865
TEXT:
SNCF
Le Wifi gratuit contre de la pub dans les gares du Mans, d'Angers et de Nantes avant la fin 2014
Les trois gares lig�riennes�profiteront d'un wifi�illimit� et gratuit en �change de publicit� au moment de la connexion.
Par   Xavier Collombier
Publi� le 10/02/2014 | 17:53, mis � jour le 10/02/2014 | 18:02
� Josselin CLAIR / MAXPPP En gare d'Angers Saint-Laud bient�t nous n'aurons plus le temps d'�couter le piano, riv�s sur notre smartphone.
+  petit
Wifi gratuit dans 128 gares fran�aises
L'op�rateur Nomosph�re a remport� l'appel d'offre du wifi gratuit de la SNCF en association avec SFR et wifi M�tropolis. Lille et Avignon seront les premi�res gares �quip�es d�s la fin du mois de mars. D'ici la fin de l'ann�e, 128 gares fran�aises auront acc�s au service, dont Angers Saint-Laud, Nantes et le Mans.
Comment �a marche ?
Le mode?le e?conomique repose sur la publicite? : l'acce?s� au�WiFi�est gratuit et illimite? apre?s ... visionnage d�un spot. Le portail de connexion est identique dans toutes les gares et il suffit au client de cre?er un compte utilisateur pour acce?der au service. Ensuite, l�acce?s se fait de fac?on transparente et automatique dans tous les espaces publics des gares.
Qui fait quoi ?
"NOMOSPHE?RE s�occupera du de?ploiement technique de l�infrastructure et de sa gestion quotidienne en s�appuyant sur le re?seau national de collecte de SFR. Quant a? WiFi Me?tropolis, il sera en charge de la gestion du portail et des services de publicite?, garantissant la gratuite? du WiFi pour l�utilisateur. " explique la SNCF�dans son communiqu�.
