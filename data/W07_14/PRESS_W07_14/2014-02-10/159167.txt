TITRE: La gr�ve des taxis s�me la pagaille � Paris
DATE: 2014-02-10
URL: http://www.boursier.com/actualites/economie/la-greve-des-taxis-perturbe-la-circulation-ce-lundi-22958.html
PRINCIPAL: 159093
TEXT:
La gr�ve des taxis s�me la pagaille � Paris
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � La circulation a �t� fortement perturb�e lundi matin en r�gion parisienne�en raison d'une gr�ve des chauffeurs de taxis qui protestent contre la concurrence qu'ils jugent "d�loyale" des VTC. L'intersyndicale FO, CFDT, CGT, SDCTP, et la CST avaient appel� � un rassemblement d�s sept heures du matin aux a�roports de Roissy et d'Orly.
A 9h30, Bison Fut� relevait jusqu'� 250 kilom�tres de retenues cumul�es pour l'ensemble des axes franciliens. "La tendance est en hausse", pr�cise le site d'informations routi�res, appelant les automobilistes � �viter de circuler sur le boulevard p�riph�rique. La circulation �tait �galement dense aux abords des grandes agglom�rations, comme � Marseille o� l'A7 et l'A50��taient tr�s encombr�es, en raison de ce mouvement de protestation...
Victoire des VTC devant le Conseil d'Etat
La grogne monte du c�t� de la profession, alors que le Conseil d'Etat, plus haute autorit� administrative en France a donn� raison aux VTC en suspendant la semaine derni�re en r�f�r� le d�cret qui les contraignait depuis le 1er janvier � attendre quinze minutes entre la r�servation d'une course et la mont�e du client. Le Conseil d'Etat a estim� que l'urgence invoqu�e par les soci�t�s de VTC �tait fond�e compte tenu du pr�judice commercial subi. Or, pour les taxis, ce d�lai de 15 minutes �tait d�j� largement insuffisant. Ils r�clament l'instauration d'une course minimum � 60 euros et un d�lai de r�servation de 30 minutes sur Paris et d'une heure pour les a�roports...
Marianne Davril � �2014, Boursier.com
