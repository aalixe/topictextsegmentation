TITRE: Une voiture plonge dans le lac du Bourget et trois personnes meurent - France 3 Alpes
DATE: 2014-02-10
URL: http://alpes.france3.fr/2014/02/10/une-voiture-au-fond-du-lac-du-bourget-avec-trois-personnes-mortes-l-interieur-savoie-412257.html
PRINCIPAL: 160458
TEXT:
faits divers
Une voiture plonge dans le lac du Bourget et trois personnes meurent
Lundi 10 f�vrier, vers 6 heures, les secours sont intervenus pour un grave accident survenu sur la commune de Saint-Germain-La-Chambotte. Une voiture a plong� dans le lac du Bourget. Les trois personnes qui �taient � l'int�rieur sont d�c�d�es.�
France 3 Alpes
Publi� le 10/02/2014 | 09:36, mis � jour le 11/02/2014 | 00:21
� Photo Nathalie Rapuc-Mulac / France 3 Alpes
+  petit
L'alerte a �t� donn�e par des agents de la DDE qui passaient par l�. On ignore depuis quand la voiture se trouvait dans le lac. L'accident s'est produit � hauteur de la casquette de protection sur la RD991, juste apr�s Brison-Saint-Innocent. Le v�hicule a d�fonc� la barri�re de s�curit� qui longe la D�partementale, avant de plonger dans l'eau.
Trois personnes sont d�c�d�es. Les victimes seraient trois hommes �g�s de 25 � 30 ans.
D'importants�moyens ont �t� d�ploy�s. Les plongeurs des sapeurs-pompiers, la brigade nautique d'Aix-les-Bains, les gendarmes�d'Aix et de�Chamb�ry sont rest�s sur place pendant plusieurs heures.�La voiture a �t� sortie de l'eau vers 11 heures. L'enqu�te a alors d�but� avec l'intervention des techniciens en identification criminelle.�
Reportage de Nathalie Rapuc-Mulac et Vincent Habran
Accident mortel dans lac du Bourget
