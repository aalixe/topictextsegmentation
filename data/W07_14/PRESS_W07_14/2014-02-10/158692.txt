TITRE: Panasonic GH4 : de la 4K compacte et abordable
DATE: 2014-02-10
URL: http://www.journaldugeek.com/2014/02/07/panasonic-gh4-de-la-4k-compacte-et-abordable/
PRINCIPAL: 158687
TEXT:
Par Jeremy , 07 f�vrier 2014 � 17:58 Photo 5 avis
Panasonic lance la quatri�me version de sa s�rie GH : le GH4. Le chiffre 4 porte pourtant malheur au Japon et le constructeur a souvent �vit� de l�int�grer aux r�f�rences de ses appareils. Mais si le constructeur en a fait abstraction, c�est pour une bonne raison, puisque le GH4 permet de r�ellement filmer en 4K.
�
Panasonic GH : itin�raire d�une gamme surdou�e
Cet appareil a droit � un petit historique rapide, pour que vous compreniez comment Panasonic en est arriv� l�. Initialement, le GH1 �tait une version du G1 embarquant la vid�o. Panasonic avait m�me travaill� sur l�autofocus et le suivi AF. C��tait histoire de proposer un 2 en 1 : appareil photo ET cam�scope afin de rapporter un maximum de souvenirs de vacances, ces histoires sans lendemain� Pardon.
Mais il s�av�re que le GH1 filmait en 1080p et 24 fps. Un format pratique pour le montage. De plus, on pouvait y monter, via diff�rentes bagues d�adaptation, toutes sortes d�objectifs manuels pas chers mais performants (des vieux 85mm f/2.0, 55mm F/2.0 ou encore 24mm f/2.8), qui, par leurs bagues de mise au point pr�cises et leurs grandes ouvertures, permettaient d�obtenir des effets cin�matographiques tr�s int�ressants (changement de PDC sur un m�me plan par exemple).
Panasonic, voyant le succ�s du GH1 aupr�s du public, d�cida de faire un GH2 orient� vid�o. Une sorte de cam�scope grand public de qualit�, � objectifs interchangeables. Mais le GH2 commen�a � int�resser les professionnels qui y ont vu un appareil d�enregistrement de qualit�, pratique et abordable, passant du statut de cam�scope � celui de cam�ra.
Avec le GH3, la marque a propos� une cam�ra orient�e professionnels. Modes d�enregistrement nombreux, dynamique remarquable (un soulagement pour le chef op�rateur), taille r�duite et une monture permettant de nombreux bidouillage. Le GH3 fut d�ailleurs une cam�ra pris�e pour les sc�nes d�action o� les risques de casse sont �lev�s.
Revers de la m�daille, avec une orientation professionnelle, la marque a augment� sa notori�t� aupr�s de l�industrie vid�o pro, mais s�est ferm�e au march� du grand public. Un choix assum�.
Panasonic GH4 : la 4K au creux de la main
Avec le GH4, Panasonic souhaite se faire une v�ritable place sur le march� professionnelle et enlever de sa s�rie GH l��tiquette d�outsider. L�id�e n��tant pas d�affronter les Red et autres Panavisions, mais de proposer un outil 4K parfaitement adapt� au broadcast vid�o.
Et apr�s en avoir vu une d�monstration sur une TV 4K et discut� avec quelques professionnels, le GH4 a tout pour y parvenir.
Allez on passe aux choses s�rieuses.
Le nombre de fonctions param�trables est cons�quent. L�ergonomie ne change pas du GH3 et pour cause, le boitier est identique, tout de magnesium fait, et prot�g� contre les intemp�ries. � l�arri�re, l��cran orientable 3 pouces �OLED de 1,040 millions de points�est second� par un viseur OLED �galement de 2,360 millions de points�couvrant 100% du champs pour un grossissement x 0.67 en �quivalent 35 mm.
Les formats d�enregistrement vid�o sont tout simplement d�ments, avec entre autres :
De la 4K en 24p ou en 30p avec un flux � 100 Mbps
Du 1080p en .mp4 � 200 Mbps (ALL-INTRA)
Du AVCHD Progressif en 1920�1080 � 60Hz
Pour faire court, vous pouvez enregistrer dans quasiment tous les formats, en progressif comme en entrelac�, avec un d�bit de 100 Mbps en 4K, qui peut atteindre 200 Mbps en Full HD. Il ne manque en fait que le RAW !
Vous allez �galement pouvoir utiliser un mode VFR (Variable Frame Rate) qui permet de filmer au ralenti ou en acc�l�r�.
Un nouveau processeur�
S�il reste � 16 megapixels, le capteur est d�un genre nouveau. La dynamique a �t� am�lior�e, et il lit les canaux deux fois plus vite, ce qui permet de diminuer l�effet rolling shutter.
Exemple d�effet rolling shutter
Et niveau photo �a donne quoi ?
L�appareil est �quip� du processeur 4 c�urs Venus Engine 9, peut grimper � 25600 ISO, poss�de une rafale de 12 images par seconde avec un buffer atteignant 40 images en RAW et 100 en JPEG. L�obturateur est garanti pour 200 000 d�clenchements, la vitesse monte � 1/8000 s et la sychro flash � 1/250 s.
L�autofocus est bas� sur la d�tection de contraste, mais l�appareil se sert �galement de la focale pour d�terminer la distance du sujet par rapport � l�appareil afin de faire le point. Une technologie d�velopp�e par la marque et nomm�e DFD (Depth From Defocus). Le focus peaking est �galement pr�sent.
Enfin, le Wifi et le NFC sont de la partie.
Le boitier mesure 133 x 93 x 84 mm pour un poids de 560g.
Un grip de dingue
On termine avec l�accessoire le plus WTF jamais con�u pour un hybride. Un �norme socle qui ravira les pros par les connectiques et possibilit�s qu�il offre.
Avec notamment, la sortie HD-SDI, d�sormais indispensable pour la retransmission vid�o, ou encore, une sortie XLR, massivement utilis�e pour les raccords de sources audio, l� encore, dans le milieu pro).
Bref. Ce GH4 envoie du lourd, sur le papier du moins. C�t� tarif, rien n�a encore �tait fix� par Panasonic, mais le prix du GH4 ��devrait �tre tr�s proche de celui du GH3 � sa sortie��. Soit environ 1300� pour le boitier et un peu plus de 2000� pour le kit avec le ��grip��. Quant � la disponibilit�, on peut tabler sur la fin du premier trimestre 2014, mais ces informations sont � prendre avec des pincettes.
�
