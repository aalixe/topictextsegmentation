TITRE: Poursuite (F) : Marie-Laure Brunet renonce - France Info
DATE: 2014-02-10
URL: http://www.franceinfo.fr/autres-sports/poursuite-f-marie-laure-brunet-renonce-1312509-2014-02-09
PRINCIPAL: 158506
TEXT:
Renan Luce : "il faut attendre d'avoir des choses � dire!"
Son premier album, Repenti, avait fait sensation en 2007, lui valant deux Victoires de la musique, et un vrai succ�s populaire, prolong� en 2009 par Le Clan des miros. Cinq ans plus tard, Renan Luce revient, r�g�n�r�, avec D'une tonne � un tout petit poids, album pop-folk pleine de petites histoires et d'imagination.
le�09 Avril 2014�dans� Rencontre avec... �
"J'ai rendez-vous avec toi. Mon p�re de l'int�rieur", de Lorraine Fouchet
Christian Fouchet a �t� un gaulliste et un r�sistant de la premi�re heure. Dans son nouveau livre, sa fille, la romanci�re Lorraine Fouchet, renoue avec son p�re qu'elle a perdu quand elle avait dix-sept ans. Un voyage �mouvant et plein d'humour aux c�t�s de Saint-Exup�ry, Malraux et de Gaulle.
Erreurs judiciaires : combien y a-t-il d'innocents en prison en France ?
Affaire Dreyfus, ratage judiciaire d'Outreau, affaire Patrick Dils : quand la justice se trompe, lui faire admettre qu'elle a envoy� un innocent en prison rel�ve du parcours du combattant. Une loi a �t� vot�e fin f�vrier pour faciliter les proc�dures de r�vision de condamnations p�nales.
Un oeil bionique pour retrouver la vue
Un �il bionique capable de redonner une vision partielle � des personnes aveugles a convaincu la Haute Autorit� de Sant� et le Minist�re de la Sant�. Il y a une dizaine de jour, le Minist�re a annonc� que, d�sormais, la S�curit� Sociale allait prendre en charge ces implants bioniques.
