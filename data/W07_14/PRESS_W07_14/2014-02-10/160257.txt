TITRE: Pacte de responsabilité : la gauche de la gauche met la pression sur Hollande | Site mobile Le Point
DATE: 2014-02-10
URL: http://www.lepoint.fr/politique/pacte-de-responsabilite-la-gauche-de-la-gauche-met-la-pression-sur-hollande-10-02-2014-1790191_20.php
PRINCIPAL: 160256
TEXT:
10/02/14 à 14h25
Pacte de responsabilité : la gauche de la gauche met la pression sur Hollande
L'aile gauche du PS met en garde contre la baisse des charges pour les entreprises et contre les "risques majeurs" d'une réduction des dépenses publiques.
Henri Emmanuelli Fred Dufour / AFP
Source AFP
Des représentants de l'aile gauche du PS s'alarment des propositions "déséquilibrées" avancées pour le Pacte de responsabilité, mettant en garde contre la baisse des charges pour les entreprises et les "risques majeurs" d'une réduction des dépenses publiques. Dans un texte adressé au séminaire du PS consacré lundi après-midi au pacte de responsabilité aux entreprises, les signataires du texte considèrent en particulier que "la focalisation exclusive sur la baisse du coût du travail ne constitue pas une réponse adaptée" aux problèmes actuels de l'économie.
"Nous ne nous reconnaissons pas dans le discours qui tend à faire de la baisse des charges et du coût du travail la condition d'un retour de la croissance. Il n'y a pas de charges mais des cotisations sociales qui sont en réalité du salaire différé", écrivent les signataires, parmi lesquels la sénatrice Marie-Noëlle Lienemann et le député et ancien ministre Henri Emmanuelli .
"Compétitivité hors-coût"
"Et nous sommes inquiets quand nous découvrons que la baisse des cotisations promise aux entreprises s'accompagne d'une réduction de 50 milliards d'euros des dépenses publiques en trois ans, sans même savoir quels sont ceux qui en supporteront les conséquences", poursuivent-ils. Pour eux, "le renouveau industriel nécessite un renforcement de notre compétitivité hors-coût qui ne sera rendu possible que par des aides ciblées et d'une réorientation des bénéfices de la rente vers l'investissement productif (...) La priorité doit donc être la suivante : favoriser l'emploi et l'investissement productif aux dépens de la rente".
"L'objectif de baisse accélérée des dépenses publiques comporte des risques majeurs", soulignent aussi les auteurs du texte intitulé "Il n'y a pas qu'une seule politique possible". Selon eux, la baisse des dépenses publiques d'ici 2017 "nous fait craindre une réduction du périmètre d'intervention de l'État, nuisible aux politiques sociales existantes et au fonctionnement des services publics".
Contreparties ?
Et d'insister : "À trop se focaliser sur l'offre et la baisse des charges, le pacte de responsabilité risque de comprimer l'activité économique". Enfin, poursuit le document, le PS doit soutenir les syndicats de salariés "pour arracher un compromis au patronat". Les contreparties réclamées au patronat en échange de la baisse des charges pour les entreprises "restent floues".
"Il n'y aura pas de compromis social favorable aux salariés sans mobilisation du parti, des parlementaires, du mouvement social. Salaires, embauches, réduction et partage du temps de travail, droits des salariés, contrôle des licenciements abusifs, (...) : dans tous ces domaines nous devons porter des exigences fortes", souligne le texte. Les signataires du document appartiennent pour plusieurs à "Un monde d'avance", réunissant des proches de Benoît Hamon, le ministre délégué à la Consommation, ou à "Maintenant la gauche", deux sensibilités de l'aile gauche du PS.
