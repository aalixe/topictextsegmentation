TITRE: Foot am�ricain : une jeune star fait son coming out et brise un tabou
DATE: 2014-02-10
URL: http://www.leparisien.fr/sports/foot-americain-une-jeune-star-fait-son-coming-out-et-brise-un-tabou-10-02-2014-3577487.php
PRINCIPAL: 160744
TEXT:
Foot am�ricain : une jeune star fait son coming out et brise un tabou
�
R�agir
Michael Sam a bris� un tabou. Ce jeune joueur de football am�ricain de 24 ans,�meilleur linebacker (joueur d�fensif) de son �quipe universitaire du Missouri, a fait son coming out ce dimanche,�ce qui pourrait faire de lui le premier joueur officiellement gay � jouer en NFL, bastion du machisme s'il en est.�
Dimanche, dans les colonnes du New York Times et � l'antenne de la cha�ne sportive ESPN, cette future star du football am�ricain a r�v�l� au grand public ce qui n'�tait qu'un secret de polichinelle au sein de son �quipe de l'universit� du Missouri: son homosexualit�.
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
NBA : Jason Collins, premier joueur � avoir fait son coming-out, a jou�
�Je suis homosexuel, et j'en suis fier�, a l�ch� Michael Sam, r�cemment �lu d�fenseur de l'ann�e dans la conf�rence universitaire Sud-Est et bien parti pour �tre recrut� par les clubs professionnels de la NFL en mai. Une �draft� (s�lection)�qui pourrait devenir un �v�nement : le joueur serait alors le premier joueur officiellement homosexuel de l'histoire de la NFL.
VIDEO. Le t�moignage de Michael Sam au New York Times
I want to thank everybody for their support and encouragement,especially @espn , @nytimes and @nfl . I am proud to tell my story to the world!
� Michael Sam (@MikeSamFootball) 10 F�vrier 2014
�Je comprends � quel point c'est important� a-t-il d�clar� � ESPN, �c'est �norme. Personne ne l'avait fait avant. Et je suis bien s�r un peu nerveux, mais je sais ce que je veux devenir... Je veux devenir pro au sein de la NFL�
Sam a aussit�t re�u le soutien de Gary Pinkel, son entra�neur � l'universit�, avec qui il a d�croch� le Cotton Bowl (une des finales du championnat universitaire) la saison derni�re, avec 12 victoires et seulement 2 d�faites: �Nous sommes vraiment heureux pour Michael, qu'il ait pris cette d�cision, et nous sommes fiers de lui et de la fa�on dont il repr�sente le Missouri�, a d�clar� le coach dans un communiqu�. Ces co�quipiers l'ont ouvertement soutenu sur les r�seaux sociaux�
We knew of his status for 5 years and not one team member, coach, or staff member said anything says a lot about our family atmosphere
� Donovan Bonner (@ElTorroOcho) 10 F�vrier 2014
Un coming out qui suscite des r�actions contrast�es�
Applaudissement �galement du c�t� de la NFL, qui a soulign� dans un communiqu� �le courage et l'honn�tet黠du jeune homme, se d�clarant �press�e d'accueillir et d'encourager Michael Sam en 2014�. �Michael est un joueur de football am�ricain. Et tout joueur qui en a les capacit�s et la d�termination peut r�ussir en NFL�, �crit l'institution.
Derri�re cette fa�ade politiquement correcte, les discours sont moins optimistes: �Je ne pense pas que le foot US soit encore pr�t�, a d�clar� l'agent d'un joueur NFL au magazine Sports Illustrated, sous le sceau de l'anonymat. �Dans 10 ou 20 ans peut-�tre, mais pour l'heure c'est toujours un sport de mecs. Traiter quelqu'un de "p�d�" est toujours tellement courant. Ca provoquerait un vrai changement de chromosome dans le vestiaire�.
Selon un recruteur de la NFL, �galement cit� par le magazine, cette d�claration de Michael Sam va �in�luctablement� le faire chuter dans la liste pour la �draft�. Ce n'est pas �une d�cision tr�s intelligente�, a d�clar� de son c�t� un entra�neur adjoint au sein de la NFL.
Cette d�cision a toutefois �t� encourag� par des joueurs NFL ou m�me une star r�cemment retrait� Chad �Ochocinco� Johnson.�
That takes courage, congrats to him, not sure how accepting NFL teammates will be. RT @jDERO26 : Missouri all-American DE announced he's gay
� Chad Johnson (@ochocinco) 10 F�vrier 2014
Le quarterback rempla�ant des Kansas City Chiefs, Chase Daniel et ancien co�quipier de Michael Sam, a lui aussi soutenu le joueur�
Had multiple convos with @MikeSamFootball this year, amazed at his honesty & courage! Once a tiger, ALWAYS a Tiger!
� Chase Daniel (@ChaseDaniel) 10 F�vrier 2014
Michelle Obama a elle aussi soutenu le jeune homme. � vous �tes un exemple pour nous tous� a t-elle partag� sur son compte Twitter.
You're an inspiration to all of us, @MikeSamFootball . We couldn't be prouder of your courage both on and off the field. -mo
� FLOTUS (@FLOTUS) 10 F�vrier 2014
Jason Collins, pr�curseur en NBA
En mars dernier, un reportage de CBS Sports avait fait des remous en avan�ant qu'un joueur actuel de NFL �envisageait s�rieusement� de faire son coming-out. Mais cette suppos�e annonce n'a toujours pas �t� faite au grand jour.
Face � l'hostilit� latente autour de cette question de l'homosexualit� dans le sport, l'Am�ricain Robbie Rogers avait d�clar� craindre en 2013 que son coming out en tant que gay n'entra�ne la fin de sa carri�re en MLS, le championnat nord-am�ricain de football. Il avait pourtant pu signer pour le Los Angeles Galaxy, l'ancien club de David Beckham, devenant le premier joueur officiellement gay de l'histoire de la MLS.
En basket-ball, Jason Collins, ex-pivot des Wizards de Washington et professionnel pendant 12 ans en NBA, avait lui r�v�l� son homosexualit� en avril 2013 . Hors contrat au moment de cette annonce, le joueur de 34 ans n'a plus re�u ensuite de proposition.
L'annonce de Collins, premier sportif d'un sport majeur aux Etats-Unis � r�v�ler son homosexualit�, avait eu un �norme retentissement, faisant m�me na�tre des comparaisons avec la situation en 1947, quand Jackie Robinson �tait devenu le premier joueur noir � �voluer dans la Ligue majeure de baseball (MLB).
LeParisien.fr
