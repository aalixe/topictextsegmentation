TITRE: Fourcade, l�or comme dans un r�ve - Sotchi 2014 - Jeux Olympiques -
DATE: 2014-02-10
URL: http://sport24.lefigaro.fr/jeux-olympiques/sotchi-2014/actualites/fourcade-l-or-comme-dans-un-reve-678326
PRINCIPAL: 161468
TEXT:
Fourcade, l�or comme dans un r�ve
Par Emmanuel Quintin, 10-02-2014
Tweeter
Fourcade, un favori au rendez-vous -  Panoramic
Le Pyr�n�en a, ce lundi, d�croch� l�or en poursuite. Avec l�assurance d�un grand champion sur le complexe nordique de Laura. Un plaisir compl�t� par la m�daille de bronze de Jean-Guillaume B�atrix.
Sa derni�re balle vient de faire mouche et Martin Fourcade l�ve d�j� les bras vers les tribunes. Lib�r�. D�livr� de la pression qui pesait sur ses �paules de leader de la d�l�gation fran�aise. Celle qu�il s��tait lui-m�me impos�e aussi en clamant haut et fort depuis des mois qu�il venait � Sotchi pour d�crocher (au moins) un titre olympique, seule ligne manquant � son immense palmar�s. Il reste pourtant une boucle lors de cette �preuve de la poursuite, mais le Fran�ais, s�r de son ski, �conna�t le poids de cette derni�re balle�. �Si je la mettais, j��tais champion olympique�, confia-t-il apr�s la course.
La cible touch�e, l�or olympique auquel il a �pens� matin et soir depuis quatre ans�,depuis sa m�daille d�argent de la mass-start � Vancouver, et qu�il avait rat� samedi ne pouvait plus lui �chapper. Sa sixi�me place sur le sprint l�avait frustr� . Les critiques, touch�. �Il lit ce qui s��crit sur lui, mais c�est aussi l� qu�il puise sa force�, fait remarquer Siegfried Mazet, entra�neur de tir des Fran�ais. Le Catalan, v�ritable machine � gagner, avait donc soif de revanche. Par rapport � ses adversaires, pour leur rappeler qu�il �tait le patron, et par rapport � lui-m�me surtout. �Les gens ont dit que sa sixi�me place �tait un �chec, poursuit Mazet, entre deux accolades avec Fabien Saguez, le DTN, et St�phane Bouthiaux, l�entra�neur des Bleus.�Mais je lui ai dit que c��tait tout sauf un �chec, de faire 6e � 12 secondes du vainqueur aux JO apr�s avoir annonc� vouloir �tre champion olympique. Tous les voyants �taient au vert, il fallait juste qu�il retrouve le feu qu�il a toute l�ann�e en Coupe du monde.�
Martin Fourcade l�a retrouv� sur la piste de Laura au terme d�une course parfaitement ma�tris�e. Apr�s 1 kilom�tre, il �tait d�j� revenu aux avant-postes et s�est alors appliqu� � assurer sur le pas de tir. Quitte � prendre plus de temps que ses adversaires. Une tactique payante puisque, apr�s un sans-faute au tir couch� (10/10), il abordait la deuxi�me moiti� de course en t�te. �Les consignes �taient les m�mes que d�habitude, d�crypte Mazet. Il fallait passer sans encombre les deux tirs couch�s, �tre dans le match et faire la diff�rence apr�s.� Martin Fourcade, 25 ans, a appliqu� le plan � la lettre, d�gainant le premier lors du premier passage au tir debout. Une vitesse d�ex�cution qui lui a peut-�tre co�t� une balle (la deuxi�me), mais qui a mis sous pression ses rivaux, qui manquaient eux aussi la cible (1 pour Landertinger et 2 pour Soukup).
�
� JO 2014 Sotchi (@_JeuxOlympiques) 10 F�vrier 2014
Malgr� un tour de p�nalit�, le leader de la Coupe du monde prenait les commandes avec une avance confortable qu�il veillait � ne pas dilapider b�tement avec un sans-faute au deuxi�me tir debout (19/20 au total). La victoire assur�e, il pouvait savourer dans la derni�re ligne droite, bras en l�air, sourire en bandouli�re, puis dans les bras de Jean-Guillaume Beatrix, son��pote d�enfance�,�rival chez les jeunes, m�daille de bronze surprise. �C�est fantastique. Tout le monde a dit que j�allais r�agir apr�s un sprint en dedans. Et l�orgueil a parl�,�savourait le h�ros du jour.
Quintuple champion du monde et vainqueur � deux reprises du classement g�n�ral de la Coupe du monde, le Catalan rejoint Vincent Defrasne (2006) et Vincent Jay (2010) dans le cercle restreint des champions olympiques individuels fran�ais de biathlon. Et, apr�s deux jours de d�ceptions tricolores, permet � l��quipe de France de retrouver le sourire (avec la 28e�m�daille d�or de son histoire). Il fallait voir l��motion sur le visage de Fabien Saguez pour se rendre compte de la port�e de cette victoire.��Quand on d�bute comme on l�a fait ces deux derniers jours, on commence forc�ment � se poser des questions�,�soufflait le DTN, soulag�.��Si on n�avait pas eu de m�daille ce soir(lundi, NDLR),��a allait commencer � grincer des dents, explique St�phane Bouthiaux.�C�est bon, on l�a. On va souffler, maintenant.��Et se tourner d�sormais vers la suite de la semaine, qui, le sport �tant ainsi fait, se pr�sente maintenant sous les meilleurs auspices.��Maintenant que Martin a gagn�, il sera plus fort�,�annonce d�j� Jean-Guillaume Beatrix. �a promet.��Je n�ai pas envie de m�arr�ter l�,�confirme le champion olympique.
�
Le player � utiliser par d�faut dans les articles Sport24.
Classement de la poursuite:
1. Martin Fourcade(Fra) 33�48��6 (1 p�nalit�)
2. Ondrej Moravec (Rtc) � 14��1 (0)
3. Jean-Guillaume Beatrix (Fra) � 24��2 (1)
4. Ole Einar Bjoerndalen (Nor) � 25��9 (3)
5. Evgeny Ustyugov (Rus) � 36��7 (1)
6. Simon Schempp (All) � 39��1 (1)
7. Emil Hegle Svendsen (Nor) � 40��2 (1)
8. Simon Eder (Aut) � 40��3 (2)
9. Andrejs Rastorgujevs (Let) � 48��3 (1)
10. Dominik Landertinger (Aut) � 48��9 (3)
...
18. Simon Fourcade(Fra) � 1�26��4 (1)
21. Simon Desthieux(Fra) � 1�47�� (1)�
La 3e journ�e en images :
A lire aussi
