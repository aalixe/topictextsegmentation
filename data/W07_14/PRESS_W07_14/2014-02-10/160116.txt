TITRE: Guerre des brevets : HTC et Nokia trouvent un accord
DATE: 2014-02-10
URL: http://www.zdnet.fr/actualites/brevets-htc-et-nokia-trouvent-un-accord-39797707.htm
PRINCIPAL: 160115
TEXT:
RSS
Guerre des brevets : HTC et Nokia trouvent un accord
Juridique : Engag� dans une bataille de propri�t� intellectuelle, Nokia et HTC ont finalement scell� un accord dont les termes pr�voient que la firme ta�wanaise versera des royalties ; les deux entreprises �changeront �galement sur les brevets et certaines technologies.
Par L'agence EP |
Lundi 10 F�vrier 2014
Nokia et HTC ont mis fin � leur guerre des brevets en signant un� accord �de collaboration. Il pr�voit qu�HTC devra verser des royalties � la firme finlandaise dont le montant n�est pas pr�cis�. Nokia a remport� plusieurs victoires juridiques au� Royaume-Uni �et en� Allemagne �contre HTC. L�accord pr�voit un �change portant � la fois sur les technologies et les brevets respectifs des deux entreprises.
Nokia mentionne en particulier les brevets HTC sur la 4G/LTE. ��Les deux compagnies exploreront �galement des opportunit�s futures de collaboration technologique �, indique le communiqu�. Cette sortie de crise intervient alors qu�HTC s�efforce de redresser la barre.
Ses derniers r�sultats trimestriels ont �t� positifs , bien qu�en de�� des attentes des analystes. HTC compte �galement se relancer en proposant un successeur � son smartphone best-seller, le HTC One, ainsi qu�un� objet connect� �qui pourrait �tre une montre ou un bracelet. (Eureka Presse)
