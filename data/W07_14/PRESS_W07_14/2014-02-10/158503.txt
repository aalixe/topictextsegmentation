TITRE: De nouveaux projets pour Victoria Beckham | C�l�brit�s | Spectacles | Le Journal de Qu�bec
DATE: 2014-02-10
URL: http://www.journaldequebec.com/2014/02/09/de-nouveaux-projets-pour-victoria-beckham
PRINCIPAL: 158499
TEXT:
De nouveaux projets pour Victoria Beckham
Brigitte Dusseau / AFP
dimanche 09 f�vrier 2014, 18H05
| Mise � jour:
dimanche 09 f�vrier 2014, 18H32
Photo Don Emmert / AFP
Modifier la taille du texte
NEW YORK - Ouverture d'un magasin � Londres, de bureaux � New York: Victoria Beckham, qui a pr�sent� dimanche sa nouvelle collection � la Fashion week de New York, a de grands projets pour 2014.
Toute la famille, David et leurs quatre enfants, �taient venus applaudir la styliste pour son d�fil�, tous assis au premier rang pr�s d'Anna Wintour, la r�dactrice en chef de Vogue USA, pour la plus grande joie des photographes.
Harper, 2 ans, petit chignon sur le haut de la t�te, �tait comme � son habitude imp�riale sur les genoux de son p�re. Les trois gar�ons, Brooklyn, 14 ans, Romeo, 11 ans, Cruz, 8 ans, tr�s �l�gants, ont fait montre d'une parfaite �ducation.
�Je voulais juste qu'ils voient ce que je fais, ils n'�taient jamais venus � un d�fil�, a expliqu� Victoria Beckham, 39 ans. �Voir que Maman travaille et que Papa travaille est pour eux un message tr�s positif�.
Son d�fil� �tait l'un des plus attendus dimanche, au quatri�me jour de la Fashion week.
Robes tr�s longues, pliss�s audacieux, ampleurs nouvelles et dos nus vertigineux, �il y a beaucoup plus de d�tails de style cette saison�, a-t-elle ensuite expliqu� en coulisses. �Il y a un �l�ment de surprise dans chaque tenue (...) Chaque saison, je pousse un peu plus loin�, a-t-elle ajout�.
Six ans apr�s ses d�buts dans la mode � New York, celle qui en ao�t dernier disait vouloir �construire un empire�, designer d�sormais reconnue et femme d'affaires assum�e, a donc d�cid� de passer � l'�tape sup�rieure.
L'ancienne Spice Girl vend d�j� ses collections dans des dizaines de pays, mais n'avait pas encore de magasin � son nom. Ce sera chose faite � l'automne, avec l'ouverture de sa premi�re boutique, 650m2 sur Dover Street � Londres. �Nous ferons une f�te�, plaisante-t-elle.
Mais elle veut surtout, dit-elle � l'AFP, �se concentrer sur les �tats-Unis�. �Ma collection marche tr�s bien, mais il y a beaucoup de potentiel� ici, explique-t-elle. �Et pour passer � l'�tape sup�rieure, j'ai besoin de passer plus de temps ici, j'ai besoin d'avoir des gens pr�sents ici pour travailler avec mes partenaires revendeurs�, ajoute-t-elle.
Pas question de s'installer � Miami
Elle dit avoir �beaucoup d'id�es, beaucoup de choses qu'(elle) veut encore faire�.
Mais moins de deux ans apr�s avoir quitt� la Californie pour Londres, pas question pour autant de r�installer la famille aux �tats-Unis, m�me si David Beckham a annonc� il y a quelques jours sa d�cision de cr�er une nouvelle �quipe de football � Miami.
�Le fait que David ait un club � Miami ne veut pas dire que nous allons aller vivre � Miami. Nous sommes install�s � Londres, c'est chez nous, c'est l� o� les enfants vont � l'�cole. C'est s�r, nous n'irons pas � Miami�, dit-elle.
Dimanche, imm�diatement apr�s son d�fil�, elle a annonc� que sa collection pr�t-� porter �tait pour la premi�re fois disponible sur son site internet.
�Je ne l'avais pas fait jusqu'� pr�sent, car je voulais vraiment prendre le temps de comprendre mes clientes et ce qu'elles attendaient de moi�, a-t-elle expliqu�.
La Fashion week a �galement �t� l'occasion pour elle de lancer un documentaire multim�dia interactif, dans le cadre d'une collaboration avec Skype, le Skype collaboration project, retra�ant sa carri�re de styliste depuis ses d�buts, avec force interviews, photos et films.
M�re de quatre enfants, mari�e � un footballeur c�l�bre, et d�sormais designer r�put�e, comment r�ussit-elle tout?
Elle pr�f�re esquiver la question. �Vous faites de moi une bien meilleure personne que celle que je suis en r�alit�.�
Vos commentaires
En commentant sur ce site, vous acceptez nos conditions d'utilisation et notre netiquette .
Pour signaler un probl�me avec Disqus ou avec la mod�ration en g�n�ral, �crivez � moderation@quebecormedia.com .
Les commentaires sont mod�r�s. Vous pouvez �galement signaler aux mod�rateurs des commentaires que vous jugez inappropri�s en utilisant l'ic�ne.
