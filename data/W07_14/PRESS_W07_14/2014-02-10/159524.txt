TITRE: � Rapport 2013 sur les cancers en France MyPharma Editions | L'Info Industrie & Politique de Sant�
DATE: 2014-02-10
URL: http://www.mypharma-editions.com/rapport-2013-sur-les-cancers-en-france
PRINCIPAL: 159522
TEXT:
Rapport 2013 sur les cancers en France
Publi� le Lundi 10 f�vrier 2014
L�INCa vient de publier en ligne son rapport annuel sur les cancers en France. Cette sixi�me �dition propose une mise � jour des connaissances pour l�ensemble des th�matiques du cancer et de la canc�rologie. L�ensemble des donn�es ont servi de r�f�rences pour l��laboration du Plan cancer 2014-2019, lanc� le 4 f�vrier dernier, par Fran�ois Hollande.
Ce rapport propose les donn�es les plus r�centes concernant l��pid�miologie, la pr�vention, le d�pistage, les soins, la vie avec le cancer, les in�galit�s de sant� et la recherche. Ce rapport est le fruit d�un travail collaboratif entre l�Institut national du cancer, ses partenaires et l�ensemble des acteurs de la lutte contre le cancer.
Le rapport s�articule autour de sept chapitres qui regroupent les principales th�matiques, dont les in�galit�s de sant�, plac�es plus que jamais au c�ur des pr�occupations du nouveau Plan cancer. Afin d�en faciliter la lecture et d�apporter un meilleur �clairage, chaque chapitre est introduit par un r�capitulatif des principaux faits marquants survenus depuis les pr�c�dentes versions du rapport et pr�sente un r�sum� des donn�es essentielles pour permettre un aper�u rapide du contenu d�taill�.
