TITRE: PSA - Dongfeng : toujours pas d'accord | Site mobile Le Point
DATE: 2014-02-10
URL: http://www.lepoint.fr/auto-addict/actualites/psa-dongfeng-toujours-pas-d-accord-10-02-2014-1790152_683.php
PRINCIPAL: 0
TEXT:
10/02/14 à 12h31
PSA - Dongfeng : toujours pas d'accord
Dongfeng confirme les discussions avec PSA Peugeot Citroën en vue d'un partenariat commercial et capitalistique, mais aucun accord n'a été trouvé à ce jour.
Les négociations paraissent au point mort entre PSA et Dongfeng alors que l'État français pousse à un accord d'ici le prochain conseil de surveillance, le 18 février prochain. DR
Auto-Addict (avec AFP)
À ce jour, Dongfeng "n'a pas signé d'accord en lien" avec un partenariat éventuel, écrit le groupe industriel chinois dans un communiqué publié à la Bourse de Hongkong ce lundi 10 février, où la cotation du groupe avait été suspendue dans l'attente de ce texte. Donfgeng confirme que "des négociations ont eu lieu" avec PSA "en lien avec un investissement potentiel dans son capital et un partenariat industriel et commercial élargi". Interrogé par l'AFP, le constructeur français s'est refusé à tout commentaire.
Validation par le conseil de surveillance le 18 février
Après plusieurs mois d'atermoiements, PSA a validé en janvier le projet d'arrivée de Dongfeng à son capital ( lire notre article ), qui s'accompagnerait d'une entrée à hauteur équivalente de l'État français. En difficulté chronique, le numéro un automobile hexagonal espère boucler les négociations en vue d'un accord au niveau du conseil de surveillance, le 18 février, qui pourrait alors être annoncé officiellement le lendemain à l'occasion de la publication de ses résultats annuels ( lire notre article ).
