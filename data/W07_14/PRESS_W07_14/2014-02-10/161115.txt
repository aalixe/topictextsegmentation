TITRE: 20 Minutes Online - Il casse un fauteuil de Napol�on en s asseyant - Insolite
DATE: 2014-02-10
URL: http://www.20min.ch/ro/news/insolite/story/Il-casse-un-fauteuil-de-Napol-on-en-s-asseyant-23608228
PRINCIPAL: 161114
TEXT:
France
Il casse un fauteuil de Napol�on en s'asseyant
Le vent du scandale a parcouru Ajaccio (sud) lundi quand s'est propag�e la nouvelle qu'un employ� avait endommag� un fauteuil pliant de Napol�on (1769-1821) en s'y asseyant avant une exposition consacr�e aux bivouacs de l'empereur.
Gard� secret, l'incident r�v�l� lundi par la presse locale, s'est produit jeudi au Palais Fesch-Mus�e des Beaux-Arts, au coeur historique de la Cit� imp�riale d'Ajaccio. Se prenant peut-�tre pour le plus illustre des fils d'Ajaccio, l'employ� du Palais Fesch a d�pos� son post�rieur sur ce fauteuil pliant en bois et cuir de 1808, identique aux si�ges des metteurs en sc�ne de cin�ma.
Mais, crime de l�se-majest�, le cuir rouge de l'assise du fauteuil appartenant au mobilier national s'est d�chir� sous le poids de l'employ� fac�tieux et les montants de bois ont �t� d�t�rior�s. Une restauratrice sp�cialis�e a �t� d�p�ch�e de Grenoble pour venir rendre son lustre au fauteuil imp�rial pr�sent� avec plus de 70 autres objets � l'exposition �Napol�on au bivouac�.
�Cet incident a le m�rite d'avoir tr�s bien fait restaurer ce fauteuil, qui l'avait d�j� �t�, mais moins bien�, a d�clar� le conservateur du Palais Fesch-Mus�e des Beaux-Arts d'Ajaccio, Philippe Costamagna. Il a aussi indiqu� que l'employ� ind�licat compara�trait prochainement devant une commission de discipline pour son geste d�plac�.
Premi�re reconstitution d'un bivouac imp�rial, cette exposition se d�roule du 13 f�vrier au 12 mai autour de l'unique tente de campagne conserv�e de Napol�on. La plupart des meubles, g�n�ralement pliants (lit, tables, si�ges, commodes) proviennent du Mobilier national, partenaire de l'exposition.
D'autres meubles, des accessoires et des �l�ments de d�coration (tableaux, accessoires, n�cessaires de toilette, vaisselle) ont �t� pr�t�s par le Mus�e de l'Arm�e, les ch�teaux de Fontainebleau et Versailles et les Archives nationales.
(afp)
