TITRE: News FC Barcelone : Messi et le Bar�a en patron � S�ville - T�l�foot - MYTF1
DATE: 2014-02-10
URL: http://www.tf1.fr/telefoot/news-football/fc-barcelone-messi-prend-ses-responsabilites-a-seville-8362651.html
PRINCIPAL: 160752
TEXT:
MyTELEFOOT : Emission en replay vid�o du dimanche ...
A l'instar du Real Madrid, le FC Barcelone a profit� de la d�faite de l'Atletico lors de la 23e journ�e de Liga pour passer devant les Colchoneros au classement. Les trois �quipes comptabilisent d�sormais 57 points chacune, mais le Bar�a occupe la place de leader � la diff�rence de buts. Lionel Messi a inscrit un doubl�, soit la moiti� des buts catalans � S�ville (1-4).
�
La Bar�a ne panique pas
Tout avait pourtant mal commenc� pour le FC Barcelone, cueilli au quart d'heure de jeu par un but d'Alberto Moreno. Cinq minutes plus tard, le FC S�ville �tait � deux doigts de doubler la mise gr�ce � Bacca. Le meilleur buteur s�villan pla�ait une t�te qui trompait Victor Valdes, mais le ballon heurtait finalement le poteau. Sans paniquer, le Bar�a est pourtant tranquillement revenu dans le match, d'abord gr�ce � l'�galisation d'Alexis Sanchez (34e).
�
Lionel Messi voit double
Une fois l'�galisation acquise, Lionel Messi a mis la machine en route. Un but en fin de premi�re p�riode (44e), puis un autre au d�but de la seconde (56e) ont permis au FC Barcelone de passer devant et prendre le large, inscrivant ses 10e et 11e but en championnat cette saison. Sachant que l'Argentin est �galement l'auteur de la passe d�cisive sur le but d'Alexis Sanchez. La d�monstration catalane a pris fin avec le but de Cesc Fabregas (88e).
�
Un championnat ultra serr�
Cette victoire (1-4) � S�ville permet au FC Barcelone de reprendre la t�te du championnat gr�ce � une meilleure diff�rence de buts. Une Liga particuli�rement ind�cise cette saison puisque les trois clubs de t�te comptent le m�me nombre de points (57). Personne ne peut dire actuellement qui du Bar�a, du Real ou de l'Atletico remportera le titre.
�
Prochain match du FC Barcelone, mercredi 12 f�vrier � la Real Sociedad pour la demi-finale retour de la Coupe du Roi.
