TITRE: Tennis - ATP - Tsonga rejoint Mathieu � Rotterdam  | francetv sport
DATE: 2014-02-10
URL: http://www.francetvsport.fr/tsonga-rejoint-mathieu-a-rotterdam-205631
PRINCIPAL: 161658
TEXT:
Tennis - ATP - Tsonga rejoint Mathieu � Rotterdam
Publi� le 10/02/2014 | 21:22, mis � jour le 10/02/2014 | 22:51
Jo-Wilfried Tsonga (PAUL CROCK / AFP)
Jo-Wilfried Tsonga, t�te de s�rie N.5, s'est qualifi� lundi pour le 2e tour du tournoi de Rotterdam en battant l'Allemand Florian Mayer en trois sets 4-6, 6-3, 6-1.  Il rejoint Paul-Henri Mathieu qui a pass� le premier tour aux d�pens du Croate Ivan Dodig 4-6, 7-6 (7/2), 6-4.
Tsonga eu besoin de 1h27 de jeu pour dominer Mayer�qui avait pris un meilleur d�part. Le N.10 mondial, finaliste du tournoi en 2011, a eu besoin d'un jeu pour� r�gler la mire, notamment sur son service (80% de r�ussite en premi�re balle).�Au tour suivant, le Manceau affrontera le Tch�que Lukas Rosol ou le Croate� Marin Cilic.
Paul-Henri Mathieu, 135e joueur mondial, s'est bagarr� pendant 2 h 15� minutes pour se d�faire du Croate Dodig (33e).�Le Fran�ais s'est montr� performant sur les balles importantes, en� transformant notamment 5 des 6 balles de break qu'il s'�tait offertes, tandis que la machine � aces (21) de Dodig s'est enray�e dans la derni�re manche (4).�"PMH" aura la lourde t�che d'affronter le vainqueur du match opposant Ga�l� Monfils (23e), gagnant dimanche du 5e titre de sa carri�re � Montpellier, � l'Argentin Juan Martin Del Potro, tenant du titre et N.4 mondial.
R�sultats du 1er�tour
Paul-Henri Mathieu (FRA) b.Ivan Dodig (CRO) 4-6, 7-6 (7/2), 6-4
Tomas Berdych (CZE/N.3) bat Andreas Seppi (ITA) 6-3, 6-3
Michael Berrer (GER) bat Jesse Huta Galung (NED) 6-4, 2-6, 6-3
Jo-Wilfried Tsonga (FRA) bat Florian Mayer (GER) 4-6, 6-3, 6-1
Philipp Kohlschreiber (GER) bat Sergiy Stakhovsky (UKR) 6-2, 7-5
