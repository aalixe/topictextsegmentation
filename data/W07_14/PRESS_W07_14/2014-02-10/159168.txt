TITRE: Gr�ve des taxis : 13 repr�sentants des taxis en col�re seraient re�us � Matignon � metronews
DATE: 2014-02-10
URL: http://www.metronews.fr/paris/greve-des-taxis-le-trafic-en-direct-120-kilometres-de-bouchons/mnbj!GyA507XSnKn6/
PRINCIPAL: 159093
TEXT:
Mis � jour  : 13-02-2014 12:06
- Cr�� : 10-02-2014 07:05
Gr�ve des taxis : 13 repr�sentants des taxis en col�re seraient re�us � Matignon
TAXIS CONTRE VTC - Apr�s la d�cision mercredi dernier du Conseil d�Etat de suspendre le d�cret imposant des contraintes � la r�servation aux Voitures de tourisme avec chauffeur (VTC), les taxis ont appel� � une manifestation ce lundi. Les cort�ges sont parti des a�roports d'Orly et de Roissy � l'aube et ont rejoint le 7e arrondissement.
Tweet
�
16h :�13 repr�sentants des taxis en col�re auraient quitt� la   place de l'Ecole militaire pour �tre re�us par le Premier ministre �   Matignon, selon une journaliste de France Info.
13 repr�sentants de #taxi quittent la place de l'�cole militaire pour �tre re�us � #Matignon .
� Lemaire Mathilde  (@MathildeL75) 10 F�vrier 2014
15h : Selon des sources  a�roportuaires, pr�s de 800 taxis ont  pris part � l'op�ration escargot  men�e depuis Roissy et 300 depuis  Orly. Selon les syndicats, 5.000 � 6.000 taxis, dont une partie a  rejoint  directement le Champ-de-Mars, se sont mobilis�s lundi. Ce  rassemblement a mobilis� deux fois plus  de taxis que le 13 janvier, au  vu des estimations des organisateurs. Cette fois, aucun incident n'a �t�  signal� dans les a�roports.
13h : Un nouveau cort�ge a quitt� l'a�roport d'Orly et se  dirige vers Paris par l'A106 alors que trois barrages filtrants sont en  cours sur le boulevard p�riph�rique. Le plus important se situe en  chauss�e ext�rieure � la porte Maillot et l'autre en chauss�e   int�rieure � la porte de la Chapelle. Le troisi�me est en chauss�e  ext�rieure, � hauteur de la porte Dor�e. La circulation s'effectue  actuellement sur une voie.
11h30 : Un de nos lecteurs nous signale que des  chauffeurs de taxi ont sorti un ballon de foot � la porte  d'Aubervilliers (voir photo ci-dessus).
11h : Les bouchons diminuent � environ 69 kilom�tres. Le  p�riph�rique reste cependant tr�s ralenti surtout entre la porte Maillot  et la porte de la Chapelle, dans les deux sens. Comptez environ deux  heures.
9h : Les blocages sont moins nombreux, mais la situation est  toujours totalement bloqu�e porte Maillot. Les tensions sont vives et  les forces de l'ordre sont sur place.�
8h45 : L'A106 est coup�e � Orly, tandis qu'un barrage filtrant est toujours en place sur la bretelle de l�a�roport de Roissy.
8h30 : Tr�s gros ralentissements sur le p�riph�rique. Il faut  compter deux heures pour aller de la porte de la Chapelle � la porte  Maillot en chauss�e ext�rieure et 50 minutes de la porte de Bercy � la  porte de la Chapelle.�
8h : Les v�hicules commencent � rallier la capitale. Il y a 240 kilom�tres de bouchons sur les routes autour de Paris.�Le p�riph�rique est satur�.
7h45 : La soci�t� de VTC "Chauffeurs priv�s" a d�cid� de ne  pas desservir les a�roports parisiens. "Nous ne pouvons assurer votre  s�curit�, et nos chauffeurs pour la plupart ne souhaitent �galement pas  prendre de risque", explique l'entreprise. Il y a deux semaines, de  nombreux chauffeurs de VTC avaient �t� pris � parti par les gr�vistes.
7h30 : Plusieurs dizaines de v�hicules se sont r�unis porte  Maillot. La gendarmerie mobile s'est rendue � Porte Maillot pour tenter  de d�gager l'acc�s. Apr�s de vives discussions, la voie est d�bloqu�e,  mais le trafic reste fortement impact�.
7h15 : Gros ralentissements sur l'A86, dans les Yvelines (sens A12 vers N118) � la hauteur de V�lizy, selon Le Parisien .
7h : Les v�hicules commencent � converger vers les a�roports.  D'autres rassemblements ont lieu, notamment � porte Maillot o� le trafic  est fortement perturb�.
6h : Les taxis convergent vers les deux a�roports parisiens. Deux rassemblements sont pr�vus autour d'Orly et de Roissy, avant de converger vers la capitale � partir de 8�heures .  Le rendez-vous final est fix� aux abords de la place Joffre (7e).�Les  perturbations devraient durer toute la journ�e, puisque la dispersion  n'est pr�vue sur place qu'� partir de 18�heures.
Des rassemblements suppl�mentaires
Ce matin, les op�rations escargots vont donc commencer � partir de  8�heures. Les deux principales autoroutes, l�A1 au nord et l�A6 au sud,  menant � la capitale sont absolument � �viter. D'autres rassemblements  de chauffeur ont �t� annonc�s sur les r�seaux sociaux, en marge de la  manifestation : Certains parlent de "1 500 taxis porte Maillot �  6�heures du matin" ainsi qu'un "autre groupement (qui) va faire  converger 500 taxis depuis la porte de Saint-Cloud". Et de pr�ciser :  "les coll�gues qui nous rejoindront en retard resteront sur le p�riph,  sur l'avenue Charles-de-Gaulle � Neuilly, sur l'avenue de la  Grande-Arm�e".
Les taxis protestent contre la d�cision du Conseil d�Etat de  suspendre mercredi le d�cret imposant aux Voitures de tourisme avec  chauffeur (VTC) un d�lai de 15 minutes entre la r�servation et la prise  en charge du client.�Cette manifestation fait suite au mouvement  organis� le 13�janvier dernier qui avait �t� �maill�e d�incidents. A  Paris, plusieurs centaines de chauffeurs (800 selon les organisateurs)  avaient suivi le m�me parcours. Cons�quence, au plus fort du mouvement,  jusqu�� 250�km de bouchons recens�s en Ile-de-France.
Taxis et Gendarmes Mobiles � pied Porte Maillot prenez le Metro ce matin mes amis pic.twitter.com/S04CyIrQp1
