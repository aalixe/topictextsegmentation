TITRE: VID�O. Obama/Beyonc�: La rumeur dont on ne parle qu�en France  - 20minutes.fr
DATE: 2014-02-10
URL: http://www.20minutes.fr/medias/1294962-20140210-obamabeyonce-rumeur-dont-parle-quen-france
PRINCIPAL: 160511
TEXT:
Twitter
Barack Obama fait la bise � Beyonc� lors de son inauguration, le 21 janvier 2013. A.WONG/AFP/GETTY IMAGE
MEDIAS � N�e de quasi-rien, d�une simple annonce d�un paparazzi fran�ais faite ce lundi matin � la radio, la rumeur se propage en France tandis qu�outre-Atlantique la presse am�ricaine, y compris people, n�en dit pas un mot...
�Beyonc� serait-elle crazy in love de Barack Obama?� , � Obama/Beyonc�: une liaison d�voil�e demain dans le Washington Post �. La presse � avant tout people � a d�marr� au quart de tour. Jean-Marc Morandini recevait ce lundi matin sur Europe 1 le paparazzi Pascal Rostain pour son livre, son troisi�me, Voyeur, M�moires Indiscrets du roi des paparazzi � para�tre chez Grasset.
Au fil de l�interview et des diff�rents sujets abord�s, Pascal Rostain lance, l�air de rien, l�air en tout cas de le dire �au passage� comme le sugg�re son �d�ailleurs�: �Vous savez, en ce moment, aux �tats-Unis, il y a quelque chose d��norme qui est en train de se passer. D�ailleurs, �a va sortir demain dans une �dition du Washington Post. On ne peut pas dire que �a soit vraiment de la presse de caniveau, sur une liaison suppos�e entre le pr�sident Barack Obama avec Beyonc�. Je peux vous assurer que la presse du monde entier va �videmment en parler�.
C�est tout. Pas une pr�cision de plus. Et c�est apparemment suffisant pour que la presse fran�aise reprenne la rumeur, au potentiel ��norme�, certes, d�autant plus par la r�sonnance qu�elle aurait avec les �affaires� de Fran�ois Hollande. Du coup, sur Twitter, c�est l� affolement , au premier et au second degr�, certains y croyant, d�autres pas du tout. Brain�Magazine ironise avec �11 preuves irr�futables qu'Obama et Beyonc� sont ensemble�.�Un utilisateur am�ricain constate avec surprise l'emballement:
Wow. French press is going crazy about a potential Obama-Beyonc� affair (no joke). Washington Post to publish an article today?
