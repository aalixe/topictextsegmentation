TITRE: Programme de d�tonateurs  : T�h�ran va s'expliquer  - LE MATIN.ma
DATE: 2014-02-10
URL: http://www.lematin.ma/express/2014/programme-de-detonateurs-_teheran-va-s-expliquer/196479.html
PRINCIPAL: 158918
TEXT:
T�h�ran va s'expliquer
Publi� le : 10 f�vrier 2014 - AFP
L'Iran a accept� de s'expliquer sur la mise au point de d�tonateurs utilisables pour une bombe afin de r�pondre aux inqui�tudes de l'AIEA sur une possible dimension militaire du programme nucl�aire de T�h�ran.
La question des �d�tonateurs � fil � exploser� (FE) est l'une des �sept mesures pratiques� que l'Iran s'est engag� dimanche � appliquer d'ici au 15 mai pour tenter d'am�liorer la transparence de son programme nucl�aire, � l'issue de deux jours de discussions avec l'AIEA � T�h�ran.
L'Iran a, toujours, affirm� que son programme nucl�aire �tait exclusivement pacifique.�
Cette avanc�e dans les discussions avec l'AIEA pourrait influencer celles tenues en parall�le entre l'Iran et les grandes puissances concernant un accord global sur le nucl�aire iranien, qui doivent commencer le 18 f�vrier � Vienne.
Les n�gociations avec le groupe 5+1 (�tats-Unis, Chine, Russie, France, Royaume-Uni et Allemagne) ont abouti fin novembre � Gen�ve � un accord int�rimaire. T�h�ran a suspendu pour six mois l'enrichissement d'uranium � 20% et a gel� � son niveau actuel ses autres activit�s nucl�aires, en �change d'une lev�e partielle des sanctions occidentales.
La coop�ration de l'Iran avec l'Agence joue un r�le essentiel dans ces derni�res n�gociations, l'AIEA �tant charg�e de surveiller l'application de l'accord de Gen�ve.
L'AIEA a confirm� dimanche dans un communiqu� ��tre parvenu (avec l'Iran) � un accord sur sept mesures pratiques qui devront �tre appliqu�es d'ici le 15 mai�, pr�cisant que T�h�ran �a appliqu� les mesures pratiques initiales� stipul�es dans une feuille de route �tablie le 11 novembre.
Le directeur g�n�ral de l'AIEA, Yukiya Amano a affirm� � l'AFP en janvier qu'il �tait d�sormais temps d'aborder la question hautement sensible de d�terminer si T�h�ran a ou non cherch� � se doter de la bombe atomique avant 2003, voire ensuite. �Nous souhaitons certainement inclure les questions (relatives) � la possible dimension militaire dans les prochaines �tapes�, avait-il dit.
Mais une visite � la base militaire de Parchin, pr�s de T�h�ran, soup�onn�e d'avoir abrit� des tests d'explosions conventionnelles applicables au nucl�aire, ne fait pas partie du nouvel accord. Cette visite est r�clam�e depuis 2012 par l'AIEA.
�Motif de pr�occupation�
Dans un rapport de novembre 2011, l'agence affirmait que la mise au point des d�tonateurs FE �tait �un motif de pr�occupation� en raison de �son application possible dans un dispositif nucl�aire explosif�.
Un membre de l'�quipe nucl�aire iranienne cit� par l'agence Isna a confirm� dimanche que l'Iran �va donner plus d'informations que celles d�j� fournies � l'agence sur les besoins et les applications des d�tonateurs FE�.
�Il y a eu des progr�s dans l'enqu�te sur une possible dimension militaire jusqu'en 2008 quand l'Iran a stopp� sa coop�ration et a refus� jusqu'en novembre 2013 de fournir les informations demand�s par l'AIEA�, a expliqu� � l'AFP Mark Hibbs, du centre de r�flexion Carnegie pour la paix internationale. L'accord de novembre ��tait une tentative de repartir de z�ro�, a-t-il ajout�.
L'Iran doit �galement communiquer des informations �actualis�es� - les derni�res remontent � mai 2006 - sur la construction du r�acteur � eau lourde d'Arak, qui utilise la fili�re du plutonium.
Les experts de l'AIEA ont visit� le 8 d�cembre l'usine de production d'eau lourde d'Arak, situ�e � c�t� du r�acteur qui est l'un des points d'achoppement des n�gociations entre l'Iran et les grandes puissances.
Le site pourrait en effet fournir en th�orie � l'Iran du plutonium susceptible d'offrir une alternative � l'enrichissement d'uranium pour la fabrication d'une bombe atomique.
Le chef de l'OIEA, Ali Akbar Salehi, a affirm� r�cemment que l'Iran �tait pr�t � �faire quelques modifications (...) pour produire moins de plutonium� et lever les inqui�tudes sur l'utilisation de ce r�acteur de 40 m�gawatts. L'Iran a toujours affirm� qu'il n'avait qu'un but de recherche.
L'accord comprend aussi des visites de la mine d'uranium de Saghand et de l'usine de production concentr� d'uranium (�yellow cake�) d'Ardakan (centre).�
Les activit�s nucl�aires iraniennes ont �t� au coeur des inqui�tudes internationales ces dix derni�res ann�es, des pays occidentaux et Isra�l craignant qu'elles ne cachent un volet militaire, en d�pit des innombrables d�mentis de T�h�ran.
L'AIEA reproche depuis de nombreuses ann�es � l'Iran un manque de coop�ration qui entretient, selon elle, le doute sur les vis�es de son programme nucl�aire. Elle d�plore ainsi r�guli�rement l'impossibilit� pour ses inspecteurs de se rendre sur la base de Parchin. �
