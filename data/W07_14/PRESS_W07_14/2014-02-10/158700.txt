TITRE: Marie-Laure Brunet renonce � la poursuite - 10/02/2014 - LaD�p�che.fr
DATE: 2014-02-10
URL: http://www.ladepeche.fr/article/2014/02/10/1814542-marie-laure-brunet-renonce-a-la-poursuite.html
PRINCIPAL: 0
TEXT:
Marie-Laure Brunet renonce � la poursuite
Publi� le 10/02/2014 � 03:49
Biathlon. Poursuite dames.
La Fran�aise Marie-Laure Brunet, d�cevante 56e de l��preuve de sprint du biathlon des jeux Olympiques de Sotchi dimanche, renonce � participer � la poursuite, aujourd�hui, a indiqu� l�entra�neur de l��quipe dames Thierry Dusserre.
Brunet, auteure d�un sans-faute en tir, a affich� un niveau de ski tr�s en de�a de ses standards, pour terminer tr�s loin de la Slovaque Anastasiya Kuzmina (2 min 20 sec 6/10e), sacr�e pour la 2e fois de sa carri�re en sprint.
Les 60 premi�res du sprint �tant automatiquement qualifi�es pour la poursuite, Brunet aurait pris le d�part de cette �preuve particuli�re -o� les biathl�tes s��lancent avec le handicap du retard accumul� en sprint- sans aucune chance de bien y figurer.
Brunet, double m�daill�e olympique aux JO de Vancouver il y a quatre (bronze en poursuite, argent par �quipes), renonce donc � l��preuve afin de se r�g�n�rer physiquement et mentalement en vue des prochaines �ch�ances.
Elle doit �tre au d�part de l�individuel vendredi et participer aux relais (mixte et par �quipes) de la semaine prochaine, sauf �volution de la situation.
La Nouvelle R�publique des Pyr�n�es
�|�
