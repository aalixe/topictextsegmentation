TITRE: Fillon assure n'avoir "aucun conflit" avec Sarkozy - � la une - Actualit�s sur orange.fr
DATE: 2014-02-10
URL: http://actu.orange.fr/une/fillon-assure-n-avoir-aucun-conflit-avec-sarkozy-afp-s_2821602.html
PRINCIPAL: 159806
TEXT:
10/02/2014 � 12:30
Fillon assure n'avoir "aucun conflit" avec Sarkozy
"Je n'ai aucun conflit avec Nicolas Sarkozy, simplement, je suis libre, je suis ma route", a affirm� l'ex-Premier ministre Fran�ois Fillon.
r�agir 14 �
photo : MIGUEL MEDINA, AFP
"Je n'ai aucune animosit�, il n'y a aucune comp�tition personnelle, c'est un probl�me politique, c'est un probl�me d'id�es, c'est un probl�me de vision du redressement national", a insist� Fran�ois Fillon (UMP) ce lundi 10 f�vrier, alors qu'il �tait interrog� par BFMTV et RMC sur sa relation avec Nicolas Sarkozy. L'ancien Premier ministre assure n'avoir "aucun conflit" avec l'ancien Pr�sident. "Simplement, je suis libre, je suis ma route", tranche-t-il.
"J'ai mis en oeuvre la politique qu'ensemble on avait d�finie, mais enfin qu'il conduisait", a ajout� l'ancien Premier ministre. "J'ai tir� les enseignements de cette p�riode - et des p�riodes pr�c�dentes d'ailleurs - et je pense aujourd'hui que la France a besoin d'un vrai projet de rupture".  Nicolas Sarkozy avait �t� �lu en 2007 sur ce th�me de la rupture. Doit-il rencontrer l'ancien chef de l'Etat ? "Ce sont des questions qui sont entre lui et moi", a-t-il tranch�.
voir (0)��
