TITRE: Woody Allen accable Mia Farrow - Voici
DATE: 2014-02-10
URL: http://www.voici.fr/news-people/actu-people/woody-allen-accable-mia-farrow-520647
PRINCIPAL: 159025
TEXT:
Publi� le dimanche 09 f�vrier 2014 � 09:34                              par F.P
Woody Allen accable Mia Farrow
Sa lettre ouverte au New York Times
�
Coup dur pour Woody Allen : son fils a�n� ne serait pas de lui
02/10/2013
La guerre est de nouveau d�clar�e entre Mia Farrow et Woody Allen. Chacun tente, comme il peut, de dire sa v�rit�. Le r�alisateur voudrait faire admettre que son ex-femme n�a dit que des mensonges�
Cette semaine, Woody Allen avait d�j� r�fut� les accusations �d� agressions sexuelles de sa fille adoptive. Pour tenter de se d�fendre au mieux, il a demand� au New York Times s�il pouvait publier une lettre ouverte. Cette derni�re a �t� d�voil�e dans les pages du journal vendredi. Il s�agit d�une longue missive dans laquelle le r�alisateur revient sur toute cette affaire d�abus sexuels mais dans laquelle, surtout, il trace un portrait peu am�ne de Mia Farrow . Selon lui, cette derni�re n�est pas une personne honn�te ni saine d�esprit.
��Mia insistait sur le fait que j�avais abus� de Dylan et l�a imm�diatement conduite chez un m�decin pour la faire examiner. Dylan a dit au m�decin qu�elle n�avait pas �t� abus�e. Mia a alors emmen� Dylan manger une glace et quand elle est revenue avec elle, Dylan avait chang� sa version de l�histoire. La police a alors ouvert une enqu�te. (�) Je suis volontiers pass� au d�tecteur de mensonges et j�ai bien �videmment r�ussi le test car je n�avais rien � cacher. J�ai demand� � Mia d�y passer aussi et elle n�a pas voulu �, a �crit le r�alisateur avant de raconter autre une anecdote.
��La semaine derni�re, une femme avec laquelle je suis sorti il y a des ann�es et qui s�appelle Stacey Nelkin a dit � la presse que lorsque Mia et moi �tions en train de divorcer il y a 21 ans, Mia lui a demand� de dire qu�elle �tait mineure lorsqu�elle sortait avec moi alors que c��tait faux. Stacey a refus�. Je raconte cette histoire afin que nous comprenions tous bien � qui nous avons affaire avec Mia. On peut facilement imaginer pourquoi elle n�a pas voulu passer au d�tecteur de mensonges �, a ajout� le r�alisateur.
Mia Farrow serait-elle manipulatrice�? Pour tenter de convaincre l�opinion, Woody Allen revient �galement sur cette histoire de paternit�. Selon Mia Farrow, Frank Sinatra serait le p�re de leur fils Ronan . Encore une preuve de sa capacit� � mentir.
��Je fais une parenth�se pour �crire quelques mots au sujet de Ronan. Est-il mon fils ou, comme Mia le sugg�re, celui de Frank Sinatra�? Je vous l�accorde, il ressemble beaucoup � Frank avec ses yeux bleus et au niveau des traits du visage. Et alors�? Si c�est le fils de Frank, qu�est-ce que cela signifie�? Que pendant toute notre proc�dure de divorce, on a entendu Mia mentir sous serment et pr�senter Ronan comme notre fils�? M�me s�il n�est pas le fils de Frank, le fait qu�elle ait pu le croire prouve qu�elle a �t� avec lui durant notre mariage. Ai-je besoin que parler de tout l�argent que j�ai d�pens� pour l��ducation de notre enfant�? Etais-je en train d��lever le fils de Frank�? Encore une fois, je voudrais attirer l�attention sur l�int�grit� et l�honn�tet� d�une personne (Mia Farrow, ndlr) qui m�ne sa vie comme �a �, a �crit Woody Allen.
Voil�. Le ver est dans la pomme. Le doute s�installe. Mia Farrow est-elle cr�dible�? C�est la question que Woody Allen voudrait que tout le monde se pose d�sormais.
Mots-cl�s :
