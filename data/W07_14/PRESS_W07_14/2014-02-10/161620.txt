TITRE: Monaco a pu "poser des probl�mes au PSG", explique Val�re Germain - RTL.fr
DATE: 2014-02-10
URL: http://www.rtl.fr/actualites/sport/football/article/monaco-a-pu-poser-des-problemes-au-psg-explique-valere-germain-7769628369
PRINCIPAL: 161618
TEXT:
Les Dossiers de RTL.fr - Ligue 1 : saison 2013-2014
Monaco a pu "poser des probl�mes au PSG", explique Val�re Germain
Val�re Germain esp�re rester dans son club formateur
Cr�dit : FRED TANNEAU / AFP
INVIT� RTL - L'attaquant de l'AS Monaco Val�re Germain est revenu dans Le Club Liza sur la rencontre de dimanche (1-1) entre le PSG et les Mon�gasques.
Au lendemain du choc de Ligue 1 entre Monaco et le PSG , Val�re Germain, l'attaquant mon�gasque, �tait l'invit� du Club Liza sur RTL. Le joueur form� au club est revenu sur la performance de ses co�quipiers.
"On a fait un bon match, on a montr� qu'on pouvait leur poser des probl�mes", a-t-il confi�.
De temps en temps ils en rajoutent
Val�re Germain, attaquant de l'AS Monaco
Conscient que l'�quipe de Claudio Ranieri a bouscul� le leader incontest�, il d�taille la cl� d'un match r�ussi face au champion de France. "Quand on joue contre Paris, il faut essayer d'aller les chercher, de mettre la pression. Quand on leur laisse le ballon on est en danger", explique-t-il. Ainsi, le fils de l'ancien marseillais Bruno Germain a constat� qu'au PSG , "de temps en temps ils en rajoutent et essaient de faire des passes devant leur but, �a peut leur poser des probl�mes comme hier soir".
Celui qui remplace Falcao aux c�t�s d'Emmanuel Rivi�re a aussi �voqu� son avenir au sein d'un effectif qui risque de s'�toffer encore � l'intersaison, et qu'il a failli quitter lors du mois de janvier. "�a m'aurait fait dr�le de partir sans avoir la chance de m'imposer en Ligue 1", avoue-t-il. "Quand on est un attaquant ici, on sait qu'on va avoir des ballons, j'ai eu la chance de toujours m'accrocher et �a a pay�", r�v�le-t-il plein d'espoir. N�anmoins, il n�exclut pas un d�part l'�t� prochain. "Il faut que j'essaye d'�tre performant, de marquer des points et des buts".
Val�re Germain indique aussi qu'"avec la Ligue des champions, il y aura plus de matchs, il y aura de grands joueurs avec qui progresser".� Par contre, "S'il arrive 3 attaquants de classe internationale, il faudra que j'aille voir ailleurs, on verra. Mon but est de progresser", conclut-il.
La r�daction vous recommande
