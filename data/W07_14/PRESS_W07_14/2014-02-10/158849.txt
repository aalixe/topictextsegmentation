TITRE: JO/Patinage de vitesse: Fauconnet, un franc-parler et une ambition | Site mobile Le Point
DATE: 2014-02-10
URL: http://www.lepoint.fr/sport/jo-patinage-de-vitesse-fauconnet-un-franc-parler-et-une-ambition-10-02-2014-1789896_26.php
PRINCIPAL: 158843
TEXT:
10/02/14 à 00h06
JO/Patinage de vitesse: Fauconnet, un franc-parler et une ambition
Thibaut Fauconnet a un certain franc-parler et un petit grain de folie: il se définit lui-même comme "un super héros" capable d'offrir au short-track français une première médaille historique à Sotchi à partir de lundi.
"On mérite d'être connus, on n'est pas juste des mecs qui tournent en rond dans une patinoire", explique Fauconnet qui s'alignera lundi sur 1.500 m, la première des trois distances individuelles (avec le 500 m et le 1000 m).
S'il réussit à monter sur un podium à Sotchi, il deviendra alors le premier Français de l'histoire à être médaillé en short-track, cette variante courte du patinage de vitesse dominée par les Sud-Coréens, Chinois et Canadiens.
Le short-track reste très confidentiel et cela n'est guère pour plaire à l'ambassadeur français à l'aise au milieu de tous ces patineurs qui savent "déconner".
"On ne se prend pas au sérieux quand il faut, notre vie est super drôle", lance le Dijonnais avec son débit très rapide.
Fauconnet, un grand barbu aux cheveux ébouriffés de 28 ans, compose une fine équipe avec ses partenaires tricolores.
"On partage beaucoup et c'est pas triste ! Faut arriver à rendre tout ce qu'on fait plus humain et moins important parce que ce sont des enjeux super importants. L'autodérision ça aide beaucoup", souligne le pensionnaire de Font-Romeu depuis 4 ans.
Le parc à grenouilles
Et de citer en exemple leur dernière trouvaille: le parc à grenouilles.
"On a ramassé des têtards, on les a fait grandir et on leur a fait un parc. Là ils sont morts, ils ont pris un coup de chaud cet été ! On n'a plus les grenouilles mais on a toujours le parc!"
Dès lundi, Fauconnet sera sérieux sur la glace avec le 1.500 m. L'octuple champion d'Europe dont un inédit grand chelem lors de l'Euro-2011, est monté à deux reprises sur un podium de Coupe du monde sur 1.500 m et 1.000 m.
Le 1.000 m aura lieu le 15 février et le 21 février, ce sera le 500 m, la distance qu'il préfère, même si cette saison il n'a pas été très convaincant.
Peut-être pourra-t-il compter sur le facteur chance du short-track à qui on prête à un caractère aléatoire.
"Non ! Et celui qui me redit ça, je lui donne mes patins, il va sur la glace à ma place et alors il verra si c'est aléatoire !"
"Je déteste ce mot aléatoire parce que ça voudrait dire qu'on s'entraîne pour rien finalement. Moi je gagne parce que je suis un super héros ! J'ai la combinaison mais j'ai pas la cape."
