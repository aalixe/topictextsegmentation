TITRE: Ski de fond - JO (H) - R�clamation de la Russie
DATE: 2014-02-10
URL: http://www.lequipe.fr/Ski-de-fond/Actualites/Reclamation-de-la-russie/440008
PRINCIPAL: 159511
TEXT:
a+ a- imprimer RSS
La Russie a fait appel du classement final du skiathlon de dimanche, remport� par Dario Cologna , annonce la F�d�ration internationale de ski (FIS) lundi. Le pays h�te conteste la troisi�me place du Norv�gien Martin Johnsrud Sundby, qui a pr�c�d� d'un souffle le Russe Maxim Vylegzhanin . Il est reproch� � Sundby d'avoir chang� de trajectoire pour se mettre dans celle de Vylegzhanin dans les derniers m�tres. Les Russes avaient imm�diatement protest�, obligeant le jury de la F�d�ration internationale de ski (FIS) � se r�unir. Celui-ci avait reconnu la faute du Norv�gien mais, apr�s avoir entendu sa version des faits, lui avait inflig� un avertissement �crit pour �infraction aux r�gles sur les lignes�, sans modifier le r�sultat final de la course.
�Le combat �tait serr�, les dix derniers m�tres j'ai chang� de ligne mais �a n'�tait pas du tout intentionnel. J'�tais tellement fatigu� que je ne tenais plus debout sur les skis�, avait d�clar� Sundby apr�s la course. La FIS a maintenant 72 heures pour �tudier l'appel de la Russie.
Avec AFP
