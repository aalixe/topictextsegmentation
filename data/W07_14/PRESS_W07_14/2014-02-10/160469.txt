TITRE: Wall Street ouvre en légère baisse
DATE: 2014-02-10
URL: http://www.lefigaro.fr/flash-eco/2014/02/10/97002-20140210FILWWW00195-wall-street-ouvre-en-legere-baisse.php
PRINCIPAL: 160389
TEXT:
le 10/02/2014 à 15:42
Publicité
Les marchés d'actions américains ont ouvert sur une note stable aujourd'hui, les investisseurs jouant la carte de la prudence après les gains appréciables enregistrés en fin de semaine dernière.
Les opérateurs attendent en outre l'audition, mardi, de Janet Yellen devant le Congrès, sa première en tant que présidente de la Réserve fédérale américaine.
L'indice Dow Jones cède 15,02 points, soit 0,1%, à 15.779,06. Le Standard & Poor's 500, plus large, recule de 0,09% à 1795,47 et le Nasdaq Composite prend 0,1% à 4129,94.
Aux valeurs, on retient l'envolée de 6,25% signée par Yelp dans la foulée de la publication d'un article du Wall Street Journal qui rapporte que le groupe est en passe de nouer un partenariat avec Yahoo
