TITRE: L'Europe lance des négociations en vue d'un accord politique avec Cuba - 7SUR7.be
DATE: 2014-02-10
URL: http://www.7sur7.be/7s7/fr/1505/Monde/article/detail/1790975/2014/02/10/L-Europe-lance-des-negociations-en-vue-d-un-accord-politique-avec-Cuba.dhtml
PRINCIPAL: 0
TEXT:
10/02/14 -  16h37��Source: Belga
� reuters.
Les ministres des Affaires �trang�res de l'Union europ�enne ont adopt� lundi des directives de n�gociations en vue d'un accord bilat�ral de dialogue politique et de coop�ration avec Cuba. La d�cision marque un tournant, dix ans apr�s la suspension des relations � la suite d'une vague de r�pression.
"Il ne s'agit pas d'une rupture par rapport � la politique men�e par le pass�. Nous tenons � soutenir les r�formes et le processus de modernisation � Cuba, tout en continuant d'exprimer nos pr�occupations concernant les droits de l'homme comme nous l'avons toujours fait", a expliqu� la Haute repr�sentante pour la politique �trang�re de l'Union, Catherine Ashton.
Depuis plusieurs ann�es, avec la lib�ration d'opposants politiques par les dirigeants communistes cubain, les relations entre l'Europe et Cuba �taient en voie d'apaisement. Le rythme des n�gociations d�pendra de nouveaux efforts en mati�re de droits humains, a pr�cis� Mme Ashton.
