TITRE: Philippe Torreton : l'acteur sugg�re qu'il est "blacklist�" sur TF1 - Actualit� T�l�vision - Reviewer.fr
DATE: 2014-02-10
URL: http://reviewer.lavoixdunord.fr/fr/tv/actualites/85991/philippe-torreton-l-acteur-suggere-qu-il-est-blackliste-sur-tf1/
PRINCIPAL: 159247
TEXT:
Philippe Torreton : l'acteur sugg�re qu'il est "blacklist�" sur TF1
lundi 10 f�vrier 2014 � 08:57 Charles Martin
Philippe Torreton
Venu pr�sent� sa tr�s attendue fiction�"Intime conviction", qui sera diffus�e sur Arte et sur le web cette semaine,�l'acteur Philippe Torreton�a clairement sugg�r�, lors d'une interview � La Nouvelle Edition de Canal +, qu'il �tait persona non grata sur TF1. Tr�s engag� politiquement � gauche, Philippe Torreton�laisse entendre que son absence des t�l�films et autres fictions de la cha�ne priv�e a peut-�tre une raison politique...
"C'est peut �tre TF1 qui a des pr�jug�s aussi, je ne sais pas", r�pond l'acteur, quand on lui demande s'il pourrait jouer un jour sur TF1. "Je regrette mais si je suis blacklist� sur TF1, �a les regarde."�Philippe Torreton sugg�re ainsi que son nom appara�t peut-�tre sur une sorte de�liste noire officieuse, dans les bureaux de la cha�ne et cela expliquerait qu'il n'ait jamais boss� pour elle. "Je sais pas, il faut croire. � eux de le prouver que non !", a-t-il conclu, lan�ant un appel du pied � TF1. Alors verra-t-on bient�t Philippe Torreton�dans un t�l�film sur la Une ?
Commenter cet article
� voir aussi sur "Philippe Torreton"...
