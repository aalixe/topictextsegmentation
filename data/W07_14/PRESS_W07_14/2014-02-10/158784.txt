TITRE: France-Monde | Le WiFi gratuit déployé dans une centaine de gares SNCF à partir de juin
DATE: 2014-02-10
URL: http://www.lalsace.fr/actualite/2014/02/10/le-wifi-gratuit-deploye-dans-une-centaine-de-gares-sncf-a-partir-de-juin
PRINCIPAL: 158779
TEXT:
Le WiFi gratuit déployé dans une centaine de gares SNCF à partir de juin
événement
- Publié le 10/02/2014
NUM�?RIQUE Le WiFi gratuit déployé dans une centaine de gares SNCF à partir de juin
Le WiFi gratuit sera déployé dans 128 gares SNCF à partir du mois de juin, et sera développé et géré par les entreprises Nomosphère et WiFi Métropolis, avec l�??opérateur SFR.
«L�??opérateur de solutions WiFi Nomosphère, en étroite collaboration avec WiFi Métropolis, a remporté l�??appel d�??offres du WiFi gratuit dans les gares, lancé par Gares & Connexions (la branche de la SNCF chargée des gares, ndlr) en septembre 2013», détaille la SNCF dans un communiqué de presse.
Le déploiement débutera au mois de mars, dans deux gares pilotes, Lille-Flandres et Avignon TGV. Une quarantaine de gares parisiennes et régionales seront ensuite équipées d�??ici fin juin 2014.
Au total, les 128 plus grandes gares françaises, sur un total de 3.000, seront connectées à fin février 2015.
«Nous regardons pour (ensuite) l�??étendre à une trentaine de gares d�??Ile-de-France», a précisé à l�??AFP Rachel Picard, directrice générale de Gares & Connexions.
L�??utilisateur devra regarder un spot de pub avant de pouvoir accéder gratuitement au WiFi. «L�??objectif pour nous était que ça soit gratuit pour le voyageur. Il ne fallait pas faire porter de coût supplémentaire à la gare, mais que le système se finance lui-même», a précisé Rachel Picard.
L�??entreprise Nomosphère sera chargée du «déploiement technique de l�??infrastructure et de sa gestion quotidienne en s�??appuyant sur le réseau national de collecte de SFR. Quant à WiFi Métropolis, il sera en charge de la gestion du portail et des services de publicité, garantissant la gratuité du WiFi pour l�??utilisateur», souligne la SNCF dans son communiqué.
Jusqu�??à présent, un accès gratuit au WiFi était proposé dans certaines gares, pour les clients SFR uniquement.
Pour Rachel Picard, le déploiement du WiFi dans les gares, «une première en Europe», est «un vrai progrès dans la digitalisation de la gare», et doit également servir au travail des agents SNCF.
Le président de la SNCF Guillaume Pepy avait fait état, en octobre dernier, de la «bascule numérique de l�??entreprise».
«On pense qu�??on doit être le plus digital des transporteurs», avait-il indiqué.
Ainsi, les cheminots vont être équipés de tablettes tactiles, une myriade d�??applications smartphones sont développées, et des bornes interactives vont être installées dans les gares notamment pour se repérer ou signaler une anomalie.
AFP
Poster un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
