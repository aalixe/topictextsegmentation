TITRE: Votations du 9 f�vrier: La presse romande pointe le clivage entre deux Suisse -   Suisse - lematin.ch
DATE: 2014-02-10
URL: http://www.lematin.ch/suisse/presse-romande-pointe-clivage-deux-suisses/story/21652120
PRINCIPAL: 158609
TEXT:
La presse romande pointe le clivage entre deux Suisse
Votations du 9 f�vrier
�
Apr�s le oui � l'initiative UDC �Contre l'immigration de masse� dimanche, la presse romande souligne le clivage entre deux Suisse et s'interroge sur les d�fis � venir. C�t� al�manique, les r�actions sont plus contrast�es.
Mis � jour le 10.02.2014 5 Commentaires
1/14 Apr�s le �oui� � l'initiative de l'UDC �Contre l'immigration de masse� dimanche, la presse romande souligne le clivage entre deux Suisse et s'interroge sur les d�fis � venir.
� �
Votre email a �t� envoy�.
Articles en relation
Les Suisses acceptent de justesse l'initiative UDC �Contre l'immigration de masse�, disent oui aussi au fonds ferroviaire, mais balaient l'initiative contre le remboursement de l'avortement. Images.
Immigration
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
E-Mail*
Veuillez SVP entrez une adresse e-mail valide
Dans son �ditorial, Le Temps , qui montre en Une un dessin de Chappatte montrant un douanier suisse baissant les fronti�res sous  le titre �Bruxelles on a un probl�me...�, tente de d�cortiquer le scrutin de dimanche, affirmant qu'�une partie de ce pays a peur de l'�volution en cours�, alors que le Tessin, tr�s expos� aux frontaliers, a exprim� son �exasp�ration�. Selon le quotidien, la Suisse des campagnes est �d�s�curis�e� et �semble ne percevoir que les aspects n�gatifs qui naissent en p�riode de forte croissance�. Le Temps rel�ve aussi qu'outre-Sarine, l'UE est consid�r�e comme une ennemie, �y compris dans les milieux �conomiques connus pour �tre europhobes par principe�.
De son c�t�, Le Matin ne m�che pas non plus ses mots: la Suisse al�manique �s'est recroquevill�e sur ses petites certitudes, jalouse de ses privil�ges et nostalgique d'un temps glorieux qui n'existe que dans les tableaux d'Anker�, �crit le quotidien orange qui titre en une: �Et maintenant on fait quoi?�
�Fracture b�ante�
Rejoignant ses confr�res, Le Quotidien jurassien parle de �fracture b�ante entre Suisse romande et Suisse al�manique�. Il met le r�sultat du scrutin sur le dos des autorit�s, �pas assez strictes dans le contr�le des mesures d'accompagnement�.
Le scrutin constitue aussi un signal d'une �Suisse qui souffre, peine � finir le mois et craint pour son avenir�, rapporte Le Courrier .
24 heures , qui montre sur sa une un dessin de B�rki montrant Christoph Blocher sur le podium, m�daille d'or au cou, avec des skis en forme de barri�re de fronti�re, pointe pour sa part la �d�connexion des institutions avec une partie de la population�, qui �coute �les sir�nes populistes de l'UDC� et �saute dans l'inconnu en croyant choisir la s�curit�. Il �voque une autre Suisse plus confiante en l'avenir et rationnelle (dont fait partie le canton de Vaud), qui a d�sormais �du souci � se faire�, le pays s'�tant �invent� une crise majeure�.
�Gifle � l'Europe�
La Libert� souligne la t�che ardue du Conseil f�d�ral. Le gouvernement devra convaincre les 28 pays de l'UE de boire un bouillon o� ont mijot� �les vieux poireaux des initiatives contre la surpopulation �trang�re, les carottes recuites du combat contre l'Espace �conomique europ�en (EEE) et des gousses d'ail rassis de l'initiative contre les minarets�. Le quotidien fribourgeois pr�voit �une �re de glaciation� entre Berne et Bruxelles.
Pour la Tribune de Gen�ve , qui publie en une un dessin de Herrmann montrant une table de n�gociation cass�e en deux sous le titre �La rupture�, le retour aux contingents �claque comme une magistrale gifle � l'Europe�. Le journal genevois se demande comment sortir de ce �mauvais pas�, entreprise qui exigera un �suppl�ment de g�nie et de pragmatisme�. Les besoins des cantons et des soci�t�s ne doivent pas �tre limit�s par les contingents, Gen�ve n�cessitant pour sa part 27'000 autorisations � l'embauche d'�trangers par ann�e, ajoute la Tribune de Gen�ve.
L'Express et L'Impartial veulent eux garder espoir: �Reste que le vote d'hier aura peut-�tre un aspect positif: le oui � l'initiative pourrait amener certains milieux patronaux � ne plus simplement consid�rer la main-d��uvre �trang�re comme une simple mani�re d'exercer une pression sur les salaires. Une sous-ench�re qui a pes� lourd dans le choix de nombreux citoyens helv�tiques au moment de remplir leur bulletin de vote�, notent les quotidiens neuch�telois dans un �dito commun.
D'apr�s L'Agefi cependant, il n'est pas n�cessaire de revenir au vieux r�gime des contingents. Une alternative lib�rale serait de vendre le droit d'immigrer, un montant �raisonnable� pour la Suisse pouvant se situer autour de 50'000 francs, selon un expert interrog� par le quotidien �conomique.
�Victoire� pour tous
Les journaux al�maniques s'attardent notamment sur la signification historique du vote. Comme plusieurs quotidiens romands, ils rel�vent le paradoxe entre le scrutin de dimanche, qui menace les accords bilat�raux, et le rejet de l'EEE par le peuple en 1992. Les bilat�rales avaient alors �t� pr�n�es par l'UDC comme alternative au non-rapprochement avec l�UE.
�Il est certain que le 'oui' au texte de l'UDC n'est pas favorable � l'�conomie suisse et, partant, au bien-�tre de la population�, note la Neue Z�rcher Zeitung .
Un avis pas partag� par la Basler Zeitung , pour laquelle �toute la Suisse� a remport� une �victoire�: le journal b�lois juge pourtant ce scrutin comme �tant peut-�tre �la plus grande d�faite jamais subie pour l'�conomie et les syndicats�.
A l'�tranger, le vote suisse fait parler de lui...
1/15 �Lib�ration� consacre sa Une ce mardi 11 f�vrier 2014 � ce que le quotidien a appel� �le virus suisse�. A l'int�rieur, cinq pages pleines sont consacr�es � la votation sur l'immigration de masse.
Lib�ration
