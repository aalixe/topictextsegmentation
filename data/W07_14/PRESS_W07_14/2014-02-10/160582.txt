TITRE: Les entreprises suisses victimes de la limitation de l'immigration?  - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/economie/entreprises-suisses-victimes-limitation-limmigration-707038.html
PRINCIPAL: 0
TEXT:
> Zone Euro
Les entreprises suisses victimes de la limitation de l'immigration?
La Suisse a vot�, dimanche 9 f�vrier, en faveur de la limitation de l'immigration. Mais la libre circulation des personnes est l'une des conditions pour que l'Union europ�enne maintienne des relations avec le pays.
Mis � jour le 10/02/2014 � 16:40
- +
r�agir
Le vote suisse est en train de cr�er un raz de mar�e diplomatique. Les Suisses ont dit "oui" dimanche 9 f�vrier, � une faible majorit� de 50,3%, � une limitation de l'immigration dans leur pays. Le gouvernement suisse a trois ans pour mettre en place les mesures d'application.
Cette d�cision helv�te a provoqu�, ce lundi, une vague de r�actions. Si les eurosceptiques se r�jouissent, ce n'est pas le cas de tout le monde. A Londres, David Cameron estime que "ce vote montre qu'il y a une inqui�tude grandissante quant � l'impact que peut avoir la libert� de circulation au sein de l'UE".
En Allemagne, Steffen Seibert, porte-parole d'Angela Merkel, a soulign� que "la libert� de circulation est pour nous un bien de haute valeur". Laurent Fabius a, lui, d�clar� que "c'est une mauvaise nouvelle pour l'Europe".
"Cons�quences graves pour les relations UE-Suisse"
Mais quelle cons�quence pratique? Pia Ahrenkilde Hansen, porte-parole de la Commission europ�enne , explique � BFM Business que " le r�sultat du vote a des cons�quences potentiellement graves pour les relations UE-Suisse". Si elle pr�cise que le vote en lui-m�me ne modifie pas le statut quo de nos accords existants, elle souligne �galement que pour l'Union europ�enne "les restrictions � la libre circulation sont hors de question".
En effet, la Suisse entretient des relations �troites avec l'Union europ�enne. Ces relations sont r�gl�es par un ensemble de 7 accords bilat�raux conclus au fil des ans entre la Suisse et l'UE. Ils sont li�s les uns aux autres. Ainsi, peut-on lire sur le site de la Conf�d�ration suisse, "les accords sont juridiquement li�s par une clause guillotine, qui pr�voit qu'ils ne peuvent entrer en vigueur qu'ensemble. Si l'un des accords n'�tait pas prolong� ou �tait d�nonc�, les parties ont la possibilit� de d�clarer caducs les autres".
La Suisse pourrait donc perdre ses relations �conomiques avec le march� europ�en. Et d'apr�s la d�l�gation de l'UE en Suisse, au plan �conomique, elle est de loin le principal partenaire commercial de la Suisse. Pr�s de 80% des importations suisses proviennent de l�UE, et environ 60% des exportations helv�tiques lui sont destin�es.
Les entreprises suisses menac�es
Autre cons�quence importante: l'impact sur les entreprises suisses. La mise en place de quotas au sein des entreprises posera des probl�mes bureaucratiques �normes. Les entreprises suisses risquent non seulement de perdre des comp�tences et d'�tre moins comp�titives mais �galement de conna�tre une forme d'isolement.
Une �tude de Credit Suisse publi�e ce lundi montre que les soci�t�s suisses et �trang�res qui envisagent des investissements (suppl�mentaires) en Suisse vont avoir tendance � retarder les d�cisions, � la fois sur les investissements et les embauches suppl�mentaires.�
"Cette d�c�l�ration de l'investissement pourrait r�duire la production �conomique de 1,2 milliard, soit 0,3% du produit int�rieur brut (PIB), durant les trois ann�es pr�c�dant l'instauration de quotas".
Les analystes de la banque table sur 80.000 emplois de moins cr��s sur cette p�riode.
Les instances europ�ennes �taient d�j� en train de se r�unir ce lundi, nous dit-on � la Commission. Mais "les choses ne devraient pas �tre plus claires avant la fin de la semaine".
A lire aussi
