TITRE: Le WiFi bient�t gratuit dans les gares | Enviro2B
DATE: 2014-02-10
URL: http://www.enviro2b.com/2014/02/10/le-wifi-bientot-gratuit-dans-les-gares/
PRINCIPAL: 159641
TEXT:
Accueil / Fil Info / Le WiFi bient�t gratuit dans les gares
Le WiFi bient�t gratuit dans les gares
Post� par: R�daction dans Fil Info 10/02/2014 0 363 vu(s)
D�s le mois de mars prochain les gares SNCF proposeront progressivement le WiFi gratuit. Si Lille et Avignon donneront le coup d�envoi de cette op�ration, d�ici f�vrier 2015, 126 autres gares les auront suivies. A quand le WiFi dans les trains ?
Il s�agit d�une premi�re en Europe, � partir du mois de mars, les gares de Lille et d�Avignon proposeront le WiFi gratuit � leur usagers. Cette gratuit� sera ensuite d�ploy�e dans 126 autres gares du r�seau. �L�op�rateur de solutions WiFi Nomosph�re, en �troite collaboration avec WiFi M�tropolis, a remport� l�appel d�offres du WiFi gratuit dans les gares, lanc� par Gares & Connexions en septembre 2013�, explique la SNCF dans son communiqu�.
Les gares pilotes de Lille-Flandres et Avignon TGV donneront donc le coup d�envoi au mois de mars, une quarantaine de gares parisiennes et r�gionales suivront d�ici juin 2014. Et, d�ici f�vrier 2015, ce sont les 128 plus grandes gares fran�aises qui seront �quip�es du WiFi gratuit. Les usagers int�ress�s n�auront qu�� regarder un spot de publicit� avant de pouvoir acc�der au r�seau WiFi de la gare. Gr�ce � ce syst�me, le dispositif se finance de lui-m�me explique Rachel Picard, directrice g�n�rale de Gares & Connexions.
