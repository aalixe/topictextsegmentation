TITRE: SFR se met au roaming avec sa formule Carr� Europe  - DegroupNews.com
DATE: 2014-02-10
URL: http://www.degroupnews.com/actualite/n9257-sfr-roaming-offre-4g-mobilite.html
PRINCIPAL: 160688
TEXT:
Stop Pub DegroupNews 1�/mois
Si Free a tir� le premier sur le roaming obligeant Bouygues Telecom et Orange � r�agir, SFR a pr�f�r� prendre son temps. Mais il semble que cela valait le coup car l�op�rateur au carr� rouge r�pond avec vigueur � ses concurrents. SFR propose l�offre Carr� Europe valable dans toute l�Europe, toute l�ann�e !
La bataille de la t�l�phonie se joue non seulement sur le r�seau 4G mais d�sormais sur le roaming ou en fran�ais, l�itin�rance. C�est-�-dire la capacit� pour un abonn� mobile de t�l�phoner et d��tre appel� via le r�seau d�un op�rateur autre que le sien notamment lors de ses d�placements � l��tranger.
A ce jeu-l�, Free a d�gain� le premier en supprimant les frais de roaming depuis le Portugal, puis l�Italie et de l�Allemagne . L�op�rateur semble avoir choisi la strat�gie du compte-gouttes pour son offre en itin�rance. Bouygues Telecom a aussit�t r�agi avec une offre �largie aux pays de l�Union Europ�enne mais soumise � une restriction de 35 jours d�utilisation. Quant � Orange , il aussi r�pliqu� mais avec une offre souffrant de bien plus de restrictions .
SFR abolit les fronti�res 365 jours par an
Finalement, SFR a pr�f�r� attendre pour abattre ses cartes. SFR vient de d�voiler la s�rie limit�e Carr� Europe compos�e d�un forfait comprenant les appels, SMS/MMS illimit�s et 3 Go d�Internet /mois utilisables en France, dans l�Union Europ�enne et dans les DOM, sans distinction du pays et ce, 365 jours par an. � Qu�ils soient en France, en Europe ou dans les DOM, les clients pourront, en toute libert�, t�l�phoner et utiliser leurs 3Go de data, sans distinction g�ographique ni restriction de jours. � pr�cise le communiqu� de SFR . Les destinations concern�es sont :
Union Europ�enne : Acores, Allemagne, Angleterre, Autriche, Bal�ares, Belgique, Bulgarie, Canaries, Chypre, Corfou, Cr�te, Croatie, Cyclades, Danemark, Ecosse, Espagne, Estonie, Finlande, Gr�ce, Gibraltar, Guernesey, Hongrie, Irlande, Irlande du Nord, Italie, �le Jersey, Lettonie, Lituanie, Luxembourg, Mad�re, Monaco, �le Man, Malte, Pays de Galles, Pays-Bas, Pologne, Portugal, �le de Rhodes, Roumanie, Saint-Marin, Sardaigne, Sicile, Slovaquie, Slov�nie, Su�de, R�publique Tch�que, Vatican.
DOM : Guadeloupe, Guyane Fran�aise, Martinique, Mayotte, R�union, Saint Barth�l�my, Saint-Martin, Saint-Pierre-et-Miquelon.
Offre Carr� Europe SFR
D�p�chez-vous, il n�y en aura par pour tout le monde !
Pionnier des offres illimit�s il y a 15 ans avec le forfait millenium, SFR utilise les m�mes ficelles pour promouvoir son offre. La formule Carr� Europe est vendue 59,99 � /mois et elle est limit�e au 10 000 premiers clients et sera disponible � compter du 11 f�vrier.
De la 4G pour tous
Par ailleurs, l�op�rateur au carr� rouge d�mocratise la 4G � tous ses abonn�s puisque le premier forfait 4G commence � 9,99 �/mois avec 50 Mo de data et un engagement de 12 mois. Enfin, SFR redouble de g�n�rosit� avec ses abonn�s puisque les forfaits 4G 3Go, 5Go, 7Go et 9Go passent respectivement � 5Go, 8Go, 12Go et 16Go.
En supprimant les restrictions pour le roaming et en doublant quasiment les volumes de trafic pour l�Internet mobile, SFR place la barre tr�s haute. Il nous tarde de voir la r�action de la concurrence.
Br�ve r�dig�e le 10/02/2014 � 16h30 par Arik Benayoun
