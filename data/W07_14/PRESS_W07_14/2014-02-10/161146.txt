TITRE: Sotchi 2014: Elimin� sur les bosses, le Fran�ais Anthony Benna d�nonce des �juges mauvais comme tout� - 20minutes.fr
DATE: 2014-02-10
URL: http://www.20minutes.fr/sport/1295074-sotchi-2014-elimine-sur-les-bosses-le-francais-anthony-benna-denonce-des-juges-mauvais-comme-tout
PRINCIPAL: 161137
TEXT:
Le skieur fran�ais Anthony Benna, le 10 f�vrier 2014, sur la piste des bosses de Sotchi, lors des JO. JAVIER SORIANO / AFP
JEUX OLYMPIQUES - S�v�rement not�, le skieur tricolore allume les juges du ski de bosses...
Il l�a mauvaise. Sorti en qualifications du ski de bosses , le Fran�ais Anthony Benna s�en est violemment pris aux juges de l��preuve.��J�ai pris une claque, a-t-il dit. Se prendre 16 points de la part des juges, �a fait mal. C�est comme si j�avais rat� toute ma descente alors que ce n�est pas le cas. Ces juges sont mauvais comme tout, ils ont 50 ans et ne connaissent rien au freestyle.�
�Ils ne sont pas comp�tents�
Alors que Benjamin Clavet s�est lui qualifi�, Benna n�a pas h�sit� � allumer les officiels de Sotchi .��Ils ne sont pas comp�tents, a jout� le bosseur de Meg�ve. Quand les meilleurs font des fautes, ils ne sont pas p�nalis�s mais quand c�est toi, tu prends une claque. C�est injuste. Les jeunes (de l��quipe de France), �a les d�go�te, ils ne comprennent pas.�
�A chaud, j�ai envie d�arr�ter�
D�moralis�, il envisage d�sormais d�arr�ter purement et simplement sa carri�re.��Je m��tais rat� � Vancouver (30e) et j�avais envie de tout d�chirer � Sotchi, �a fait vraiment mal au c�ur�, a indiqu� le Fran�ais, qui compte trois podiums en Coupe du monde. A chaud, j�ai envie d�arr�ter (la comp�tition).�
Antoine Maes
