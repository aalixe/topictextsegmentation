TITRE: Les zombies de �The Walking Dead� l�ch�s dans Manhattan -  En bref - S�ries TV - T�l�rama.fr
DATE: 2014-02-10
URL: http://www.telerama.fr/series-tv/les-zombies-de-the-walking-dead-laches-dans-manhattan,108463.php
PRINCIPAL: 0
TEXT:
Accueil > S�ries TV >Les zombies de �The Walking Dead� l�ch�s dans Manhattan
Les zombies de �The Walking Dead� l�ch�s dans Manhattan
En bref
-
Mis � jour le 10/02/2014 � 11h49
Pour lancer la seconde moiti� de sa saison 4 de The Walking Dead , qui a d�but� sur AMC outre-Atlantique ce dimanche (et lundi sur OCS), la s�rie de zombies a organis� une petite plaisanterie dans les rues de New York. Cach�s sous une grille d�a�ration, une horde de zombies levait ses bras � travers le sol au passage de malheureux new-yorkais. But de la man�uvre, une vid�o ��virale�� mise en ligne, o� l�on voit surtout des gens r�agir dans la bonne humeur (et pas mal de passant(e)s jeunes et joli(e)s, comme par hasard). On imagine que la blague n�a pas fait rire tout le monde�
.
