TITRE: Le meurtrier schizophr�ne, sa psychiatre et la volte-face du parquet - 20minutes.fr
DATE: 2014-02-10
URL: http://www.20minutes.fr/ledirect/1294938/20140210-debut-proces-appel-psychiatre-apres-meurtre-commis-patient
PRINCIPAL: 0
TEXT:
Soci�t�
Le meurtrier schizophr�ne, sa psychiatre et la volte-face du parquet
Dans une volte-face inattendue, le minist�re public a requis lundi la relaxe, devant la cour d'appel d'Aix-en-Provence, de la psychiatre Dani�le Canarelli, condamn�e fin 2012 � un an de prison avec sursis pour homicide involontaire � la suite du meurtre commis par un patient. Anne-Christine Poujoulat AFP
Mis � jour le 10.02.14 � 22h30
Dans une volte-face inattendue, le minist�re public a requis lundi la relaxe, devant la cour d'appel d'Aix-en-Provence, de la psychiatre Dani�le Canarelli, condamn�e fin 2012 � un an de prison avec sursis pour homicide involontaire � la suite du meurtre commis par un patient.
Se livrant � une magistrale le�on de droit, l'avocate g�n�rale Isabelle Pouey a �cart� toute �faute caract�ris�e� de la praticienne de 59 ans et tout lien de causalit� avec le crime, demandant par ailleurs aux juges de �constater la prescription de l'action publique� qui expirait en mars 2007, trois ans apr�s les faits.
Une analyse � rebours du tribunal correctionnel de Marseille qui avait estim� que �les d�faillances relev�es� dans le suivi de Jo�l Gaillard, de 2000 � 2004 � l'h�pital Edouard-Toulouse, �taient �� l'origine de l'errance du patient, de la fuite de l'�tablissement puis du passage � l'acte�.
Vingt jours apr�s sa fugue, le malade tuait � coups de hachette, le 9 mars 2004 � Gap (Hautes-Alpes), le compagnon octog�naire de sa grand-m�re, Germain Trabuc. Des faits pour lesquels il a �t� d�clar� irresponsable p�nalement en raison de ses troubles psychiatriques.
A l'issue de l'audience, Mme Canarelli a fait part de son soulagement, entour�e de coll�gues radieux, quand le fils de la victime, Michel Trabuc, � l'origine de la plainte, se disait �abasourdi�.
Un patient �sortant de l'ordinaire�
Au cours des d�bats, la psychiatre a �voqu� un malade �sortant de l'ordinaire, un peu exceptionnel�, dont �les sympt�mes disparaissaient rapidement � l'int�rieur du service�.
Pourtant, l'interpelle le pr�sident, �il y a une r�alit� objective: ce sont ses violences r�p�t�es (plusieurs agressions, dont une tentative d'assassinat avant le crime, ndlr)�, �et de nombreuses expertises vont dans le m�me sens�.
�La dangerosit� est �valu�e au moment o� je l'examine. Or � ces moments-l�, le patient ne s'est jamais montr� agressif envers l'�quipe soignante�, a racont� � la barre le Dr Canarelli, d�crivant un patient �m�fiant au d�part� qui �s'apaisait petit � petit�.
�N'y avait-il pas de sa part un travail de manipulation?�, insiste le magistrat. �Je ne crois pas qu'on puisse jouer la com�die sur la dur�e (...). Je ne me suis pas laiss� abuser par ce patient, j'ai pris des d�cisions en conscience�, a r�pondu le Dr Canarelli, sans nier les �difficult�s� que Jo�l Gaillard lui avait pos�es.
Quand on lui demande si �cette fugue ne l'a pas inqui�t�e�, le Dr Canarelli s'agace: �je pensais que l'avis de recherche suivait son cours, mes pr�rogatives ne vont pas jusqu'� aller chercher le patient dans sa famille dans les Alpes!�.
'Chronique d'une mort annonc�e'?
Parmi les t�moins cit�s par la d�fense, le psychiatre Daniel Zagury a apport� un soutien sans faille � sa confr�re, appelant � �ne pas m�langer psychiatrie et police�.
�A sa place, j'aurais fait exactement la m�me chose. Il est ais� de reconstruire les �tapes du dossier quand on conna�t le pr�sent, c'est facile de dire aujourd'hui +chronique d'une mort annonc�e+�, a-t-il jug�. Et d'ajouter: �Il y a 650.000 schizophr�nes en France, 99,7% ne commettront jamais d'homicide�.
Dans le m�me sens, le pr�sident du Syndicat des psychiatres des h�pitaux (SPH), Jean-Claude Penochet, a fait part de son ��tonnement�. �Il y a un malentendu: le diagnostic en psychiatrie n'a pas la m�me clart�, la m�me puret� qu'un diagnostic somatique o� on sait imm�diatement s'il s'agit de varicelle ou de rub�ole�, a-t-il t�moign� � la barre, notant �la complexit� de cette discipline.
Le premier proc�s avait fortement �branl� la profession, dont de nombreux repr�sentants avaient fait le d�placement, s'inqui�tant des cons�quences d'une condamnation sur l'exercice de leur m�tier, et d'une possible �d�rive s�curitaire�.
�Le premier proc�s, c'�tait l'�motion. Le second, c'est la raison�, a r�agi Alain Abrieu, un confr�re de la pr�venue, � l'issue du r�quisitoire. �L'avocate g�n�rale a fait le droit, un travail de recherche impressionnant, chaque mot �tait pes�, a-t-il soulign�.
D�cision rendue le 31 mars.
� 2014 AFP
