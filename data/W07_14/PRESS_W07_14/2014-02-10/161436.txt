TITRE: Chapitre: 34 librairies sauv�es et 23 ferm�es d�finitivement  - Flash actualit� - Economie - 10/02/2014 - leParisien.fr
DATE: 2014-02-10
URL: http://www.leparisien.fr/flash-actualite-economie/chapitre-34-librairies-sauvees-et-23-ferment-definitivement-10-02-2014-3577939.php
PRINCIPAL: 161434
TEXT:
Chapitre: 34 librairies sauv�es et 23 ferm�es d�finitivement
Publi� le 10.02.2014, 19h43
Tweeter
Le tribunal de commerce de Paris a valid� lundi de nouvelles offres de reprise des librairies Chapitre (groupe Actissia), portant le nombre total d'�tablissements repris � 34, mais 23 vont fermer leurs portes d�s lundi soir, a-t-on appris aupr�s de la direction du groupe. | Francois Guillot
R�agir
Le tribunal de commerce de Paris a valid� lundi de nouvelles offres de reprise des librairies Chapitre (groupe Actissia), portant le nombre total d'�tablissements repris � 34, mais 23 vont fermer leurs portes d�s lundi soir, a-t-on appris aupr�s de la direction du groupe.
"C'est au total 34 librairies et quelque 750 emplois qui ont ainsi �t� sauv�s d?une fermeture d�finitive", et, "� d�faut de dossier de reprise recevable, 23 librairies fermeront leurs portes ce soir" lundi, �crit dans un communiqu� la direction, pr�cisant qu'"il y a 434 pertes d'emplois".
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Le r�seau des librairies CHapitre, qui comptait 57 �tablissements r�partis dans tout l'Hexagone et quelque 1.200 salari�s, a �t� plac� en liquidation judiciaire le 2 d�cembre, mais avait �t� autoris� � poursuivre l'activit� jusqu'� ce lundi.
Avant la proc�dure judiciaire, cinq librairies avaient pu �tre c�d�es. Puis deux vagues de cession avaient �t� organis�es en d�cembre et en janvier, permettant la cession de 26 librairies au total.
L'�diteur Albin Michel et le groupe Madrigall (Gallimard) avaient repris plusieurs boutiques. De nombreux magasins ont aussi �t� sauv�s par leurs actuels directeurs ou salari�s.
Lundi, huit librairies suppl�mentaires ont fait l?objet d?un "transfert de propri�t�" lors de la derni�re vague de d�cisions du tribunal. "Sont donc sauv�es de la fermeture, les librairies de Colmar, Saint-Louis, Bergerac, Grenoble, Brive-la-Gaillarde, Ch�lons-en-Champagne, Sarreguemines et Laval", a indiqu� la direction.
