TITRE: LinkedIn ferme l'application "Intro" et le service "Slidecast"
DATE: 2014-02-10
URL: http://www.commentcamarche.net/news/5864000-linkedin-ferme-l-application-intro-et-le-service-slidecast
PRINCIPAL: 160877
TEXT:
LinkedIn ferme l'application "Intro" et le service "Slidecast"
ldelvalle le lundi 10 f�vrier 2014 � 10:15:16
Lanc�e en octobre dernier, l'application "Intro", qui permet d'int�grer LinkedIn au coeur du client mail d'iOS, ne sera plus disponible � partir du 7 mars prochain. "Intro" �tait dans le collimateur des experts de s�curit� informatique compte tenu de sa vuln�rabilit� aux risques d'interception de donn�es. LinkedIn a par ailleurs annonc� la fermeture de SlideCast, un service permettant de diffuser sur LinkedIn des pr�sentations audio h�berg�es sur SlideShare .
LinkedIn n'attend pas le printemps pour faire le m�nage parmi ses outils qu'il juge aujourd'hui superflus. Affirmant vouloir "faire moins, mais faire mieux", LinkedIn a annonc� la fermeture de "Intro", une application email pour l' iPhone lanc�e en octobre dernier , qui permet aux utilisateurs de voir s'afficher les informations LinkedIn de leurs contacts membres du r�seau social professionnel.
Vuln�rabilit� au phishing
Pour fonctionner, l'application requ�rait le transfert des donn�es e-mail vers les serveurs LinkedIn, exposant ces donn�es � un risque d'attaque de type "man-in-the-middle" (interception de donn�es), et donc de phishing � grande �chelle. Linkedin a annonc� que les utilisateurs pouvaient d'ores et d�j� d�sinstaller l'application, qui sera mise d�finitivement hors service le 7 mars prochain.
Le r�seau social a par ailleurs annonc� mettre fin � Slidecast, un service permettant aux utilisateurs de la plateforme SlideShare d'int�grer des pr�sentations audio sur leur profil LinkedIn. Celui-ci ne sera plus disponible � partir du 30 avril prochain.
En savoir plus
