TITRE: Elections | Bertrand Delanoë, l'atypique
DATE: 2014-02-10
URL: http://www.dna.fr/politique/2014/02/09/bertrand-delanoe-politique-atypique
PRINCIPAL: 158766
TEXT:
- Publié le 10/02/2014
politique  Dernier conseil municipal de Paris aujourd�??hui Bertrand Delanoë, l'atypique
Il va quitter l�??hôtel de ville de Paris en plein succès et après deux mandats. Un choix qui rend Bertrand Delanoë singulier dans la vie politique française, ce qui ne lui déplaît pas.
Bertrand Delanoë, ici à Toulouse où il a fait ses études,  est populaire dans toute la France.  Photo AFP
Une page va se tourner pour les Parisiens : Bertrand Delanoë, maire depuis 2001, présidera ce matin son dernier conseil de Paris. Il sera sans doute très ému, comme à chaque fois qu�??il fait les choses pour la dernière fois en tant que maire.
On l�??a ainsi vu très touché lors du Congrès des maires en novembre dernier à Paris. Et il était visiblement bouleversé lors de ses derniers v�?ux, en janvier.
Bertrand Delanoë, c�??est ça : une personnalité très forte, souvent perçue (à juste raison) comme cassante et autoritaire, mais en même temps un être fidèle, sincère, qui n�??hésite pas à montrer ses émotions en authentique pied noir de Tunisie. Il était ému aux larmes quand il a reçu à l�??Hôtel de ville de Paris François Hollande, le jour de son intronisation. Il a avoué que pour lui, c�??était un très grand moment dans sa vie de maire.
Une vie de maire qui aura donc duré deux mandats, pas un de plus. Pourtant, Bertrand Delanoë, très populaire, aurait décroché haut la main un troisième bail.
Et maintenant ?
Mais Bertrand Delanoë est un homme de principe : opposé au cumul des mandats, il a annoncé dès 2007 qu�??il ne briguerait plus les suffrages des Parisiens. Mieux, il a organisé sa succession en adoubant sa première adjointe Anne Hidalgo, qui mène actuellement campagne.
Cet éléphant du PS fait donc le choix à 63 ans, de quitter un poste auquel tant se seraient attachés. Cela renforce son image atypique qu�??il aime afficher et polir. Ainsi, il oublie rarement de dire qu�??il a passé une partie de sa vie dans le privé. Il a aussi assumé son positionnement « libéral et socialiste », ce qui lui a coûté le poste de Premier secrétaire du PS au congrès de Reims.
Très discret sur sa vie privée, Bertrand Delanoë est aussi le premier �?? et le seul �?? homme politique d�??envergure nationale à avoir dévoilé son homosexualité. Il quittera ses fonctions le 30 mars. Il sera ensuite libre d�??entrer au gouvernement au prochain remaniement. Il en rêve. Sur ce point en revanche, il n�??a rien d�??atypique.
Nathalie MAURET
Poster un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
