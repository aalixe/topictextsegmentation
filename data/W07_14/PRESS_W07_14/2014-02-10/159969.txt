TITRE: JO: la Russie fait appel du r�sultat du skiathlon
DATE: 2014-02-10
URL: http://www.boursorama.com/actualites/jo-la-russie-fait-appel-du-resultat-du-skiathlon-cbb30ec257e98f42e14374385ad21bbb
PRINCIPAL: 159967
TEXT:
Imprimer l'article
LA RUSSIE FAIT APPEL DU R�SULTAT DU SKIATHLON DE SOTCHI
ROSA KHOUTOR, Russie (Reuters) - La Russie a officiellement fait appel du r�sultat du skiathlon masculin disput� dimanche aux J.O de Sotchi, apr�s le rejet de sa premi�re contestation de l'ordre d'arriv�e.
L'�quipe russe avait protest� juste apr�s l'�preuve, dimanche, en estimant que l'un de ses membres avait �t� priv� de m�daille de bronze parce qu'il avait �t� g�n� par un concurrent dans les derniers instants.
Le Russe Maxim Vylegzhanin a manqu� le bronze d'un dixi�me de seconde, terminant quatri�me derri�re le Norv�gien Martin Johnsrud Sundby. Celui-ci semble avoir travers� la piste de Vylegzhanin, contrairement � la r�glementation. Il a re�u une r�primande par �crit mais a �t� autoris� � conserver sa m�daille, le jury ayant estim� que son comportement n'avait pas eu d'influence sur le r�sultat.
La F�d�ration internationale de ski a 72 heures pour se prononcer sur l'appel de la Russie.
La m�daille d'or de l'�preuve est revenue au Suisse Dario Cologna. Le Su�dois Marcus Hellner a pris la m�daille d'argent.
Julien Pr�tot; Eric Faye pour le service fran�ais
� 2013 Thomson Reuters. All rights reserved.
Reuters content is the intellectual property of Thomson Reuters or its third party content providers. Any copying, republication or redistribution of Reuters content, including by framing or similar means, is expressly prohibited without the prior written consent of Thomson Reuters. Thomson Reuters shall not be liable for any errors or delays in content, or for any actions taken in reliance thereon. "Reuters" and the Reuters Logo are trademarks of Thomson Reuters and its affiliated companies.
