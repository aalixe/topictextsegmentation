TITRE: AVC : les femmes plus touch�es que les hommes � Top Actus Sant�
DATE: 2014-02-10
URL: http://topactu.fr/epidemies-pandemies/avc/avc-les-femmes-plus-touchees-que-les-hommes/24342/2014/02/08.html
PRINCIPAL: 158303
TEXT:
Article �crit par La R�daction dans la cat�gorie AVC
�
Les femmes auraient un plus grand risque de faire un accident vasculaire c�r�bral que les hommes, et ceci quelque soit leur �ge, selon une nouvelle �tude de l�association am�ricaine du coeur. Afin de pr�venir les AVC, cette derni�re recommande de surveiller r�guli�rement la tension art�rielle.
L�accident vasculaire c�r�bral est la cons�quence de l�obstruction par un caillot ou de l��clatement d�un vaisseau sanguin alimentant le cerveau. L�attaque c�r�brale entra�ne la destruction des tissus c�r�braux. Il se caract�rise par la survenue brutale d�un engourdissement ou une faiblesse du bras, des difficult�s � parler ou � comprendre ce que dit quelqu�un. Chez les femmes, certains de ces signes pourraient selon certains experts �tre plus subtiles. Les femmes conna�traient ainsi plus souvent des difficult�s pour s�exprimer ou encore � �tre conscientes de leur environnement.
Alors que les accidents vasculaires c�r�braux sont la troisi�me cause de mortalit� chez les femmes, apr�s les maladies cardiaques et le cancer, ils ne repr�sentent que la cinqui�me cause chez les hommes. Pour expliquer cette diff�rence entre les sexes, les experts de l�association am�ricaine du coeur �voquent notamment le fait que les femmes sont plus sujettes � certaines pathologies ou troubles comme les migraines, la d�pression, le diab�te ou encore l�arythmie cardiaque.
Elles ont �galement plus de risques sp�cifiques li�s non seulement � la grossesse, mais �galement � l�utilisation de la pilule contraceptive, affirme le professeur adjoint de neurologie au Centre m�dical Wake Forest de Caroline du Nord Cheryl Bushnell.
Ce m�decin, a pr�sid� un groupe d�experts qui a �labor� des recommandations concernant la pr�vention de l�AVC. Parmi les recommandations figurent le contr�le r�gulier de la tension art�rielle, notamment chez les jeunes femmes, et principalement cette surveillance avant de prendre des contraceptifs et d�entamer une grossesse.
