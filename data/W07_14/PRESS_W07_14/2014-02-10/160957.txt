TITRE: Des pesticides mille fois plus toxiques qu'annonc� par les fabricants? | YOUPHIL
DATE: 2014-02-10
URL: http://www.youphil.com/fr/article/07218-pesticides-toxiques-seralini-roundup-monsanto?ypcli%3Dano
PRINCIPAL: 160956
TEXT:
Imprimer
Apr�s son �tude controvers�e sur les OGM, le professeur S�ralini publie une nouvelle �tude sur la toxicit� des pesticides.
Le professeur Gilles-�ric S�ralini n'en a pas termin� avec son combat contre les pesticides . Dans une �tude publi�e en janvier 2014, le scientifique montre que certains produits phytosanitaires que l�on trouve dans le commerce seraient entre "deux et mille fois plus toxiques" que ce qui est annonc� par les fabricants.
Pour ses recherches, le professeur S�ralini et son �quipe de chercheurs de l'universit� de Caen se sont int�ress�s aux pesticides apr�s l'ajout d'adjuvants, tels qu'ils sont vendus dans le commerce. Ces�substances sont g�n�ralement destin�es � renforcer l�efficacit� d�un produit. En agriculture, ils sont utilis�s dans les pesticides pour faciliter le principe actif � p�n�trer dans les plantes. Mais la combinaison du principe actif et de l'adjuvant peut �galement avoir des incidences sur les organismes animaux ou humains, lorsqu�ils entrent en contact.
Les r�sultats, s�ils se confirment, sont stup�fiants. Les chercheurs estiment que huit des neuf principaux pesticides utilis�s dans le monde, sont "en moyenne des centaines de fois plus toxiques que leur principe actif". En clair, la toxicit� du pesticide serait sous-estim�e.�Comme le souligne le site d'information Rue89.com,�l' Institut national de la sant� et de la recherche m�dicale (Inserm) avait d�j� alert� sur la possible toxicit� des adjuvants, en juin 2013.
Les pr�c�dentes recherches du professeur S�ralini avaient suscit� de nombreuses critiques. En novembre 2013, ses travaux montrant les effets n�fastes, � long terme, d�un ma�s transg�nique et de son herbicide associ� (le Roundup) sur des souris, ont �t� d�savou�s par la revue Food and Chemical Toxicology . Celle-ci�avait accept� l��tude, mais l�avait ensuite d�publi�e. On reprochait alors au scientifique un nombre trop faible de souris �tudi�es (200).
