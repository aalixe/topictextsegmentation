TITRE: VIDEO. Edouard Martin refoul� lors d'une visite de Manuel Valls � Florange - L'Express
DATE: 2014-02-10
URL: http://www.lexpress.fr/actualite/politique/video-edouard-martin-refoule-lors-d-une-visite-de-manuel-valls-a-florange_1322299.html
PRINCIPAL: 161026
TEXT:
VIDEO. Edouard Martin refoul� lors d'une visite de Manuel Valls � Florange
Par LEXPRESS.fr, publi� le
10/02/2014 �  18:50
Accueil rat� pour la t�te de liste socialiste aux Europ�ennes. Venu accueillir Manuel Valls � Florange, l'ex-syndicaliste du site ArcelorMittal Edouard Martin n'a pas �t� autoris� � s'approcher du ministre, retenu par les responsables de la s�curit�.�
Voter (1)
� � � �
Venu accueillir Manuel Valls � Florange, Edouard Martin n'a pas pu passer la barri�re humaine qui l'a emp�ch� de s'approcher du ministre.
DR
Apr�s plusieurs accueils chaleureux , Edouard Martin , l'ex-syndicaliste du site ArcelorMittal de Florange devenu t�te de liste PS aux europ�ennes a �t� refoul� lors de la visite de Manuel Valls . �
Ce lundi, le ministre de l'Int�rieur �tait en visite � Florange. "Les �lus lorrains" avaient alors demand� � Edouard Martin "de venir accueillir le ministre", explique-t-il aux cam�ras de BFM . Quelle ne fut pas la surprise de la nouvelle recrue socialiste quand le responsable de la s�curit� lui a tout simplement emp�ch� d'approcher le ministre.�
�
�
Face � la barri�re humaine, Edouard Martin a fait mine de prendre avec recul cet accueil rat�: "Ce n'est pas grave, je ne vais pas lui courir derri�re apr�s tout". Et de taquiner des syndicalistes venus avec lui: "Je pense que ce sont les chasubles oranges qui ont d� faire peur".�
�
bleudesvosges - 16/02/2014 13:27:31
Evidemment, Martin ne peut pas r�agir, s'il ne veut pas perdre le soutien du Parti qui l'a adoub�. Mais la m�thode Valls est, dans ce cas, assez contestable car ind�licate.
cartesien - 10/02/2014 23:56:57
Comme quoi........ Bov� no2..... Ce n'est pas la comp�tence qui compte. Tu fais du bruit, tu casses et on te r�cup�re pour te museler. Coluche le disait : quand on a pas de talent on agite les bras.
Greeny2 - 10/02/2014 23:04:38
@canigou66 : ...Mais il le sera forc�ment puisque ces �lections europ�ennes sont calcul�es � la proportionnelle, c'est � dire que les candidats sont �lus en fonction de leur ordre sur la liste....et rien d'autre....( c'est d'ailleurs la raison pour laquelle de nombreux "t�nors" politiques se sont pr�cipit�s en t�te de listes dans diverses r�gions...). Mr Martin sera donc bien obligatoirement �lu avant Mme Trautmann....ind�pandemment de leurs qualit�s respectives... C'est l� toute la particularit� de la proportionnelle.....Tiens, Juste pour plaisanter, comme �a, en passant...moi-m�me, personne parfaitement inconnue, je le serai donc aussi si j'�tais n�1 d'une des listes des trois ou quatre partis majeurs...Dingue, �a, non?....Mais dommage, pour �a, il faut des contacts...  Sachant donc, que pour calculer le r�sultat, on se base bien �videmment sur les suffrages exprim�s et non pas les inscrits....� la louche, on dira qu'un score de 8 � 10% des SE assure forc�ment l'�lection � un candidat...Apr�s il faut prendre la calculette..... Si on se r�f�re aux r�sultats de 2009...Pour le Grand Est il y a 9 si�ges � pouvoir... la majorit� pr�sidentielle ( de l'�poque donc droite ) avec 29.20% avait obtenu 4 si�ges, le PS avec 17.24%, 2 si�ges , Europe �cologie, 14,28%, 1 si�ge, le mouvement d�mocrate ( JF Kahn ) 9.44%, 1 si�ge et le FN avec 7.57%, 1 si�ge.... Ce qui change tout cette ann�e, c'est que les premiers sondages donne un FN autour de 20-25%.....??? cela ajout� � la morosit� ambiante actuelle, �a change toute la donne... Par contre pour chaque t�te des listes majeures, tout baigne, c'est succ�s assur�..... D�sol� d'avoir �t� un peu long...mais si cela peut servir � quelqu'un.....
