TITRE: Procès Schembri : le récit douloureux de 30 ans de sévices - Elle
DATE: 2014-02-10
URL: http://www.elle.fr/Societe/News/Proces-Rene-Schembri-bourreau-de-son-epouse-durant-30-ans-2665188
PRINCIPAL: 158905
TEXT:
Laura Boudoux @Laura_Boudoux
[ Tous ses articles ]�
Colette raconte avoir subi pendant 30 ans, la torture physique et mentale de son époux, René Schembri. Le procès de cet homme de 71 ans accusé de «�tortures et actes de barbarie�» s’ouvre aujourd’hui, devant la cour d’assises des Bouches-du-Rhône. Colette a rencontré cet ancien enseignant en 1969, date à laquelle les coups ont commencé, la violence� étant «�allée crescendo�», selon l’avocat de la plaignante, Me Laurent Epailly, qui s’est exprimé auprès de l’AFP. « Perte de dents, ablation des �muscles du bras, cécité de l’œil gauche, mutilation du sexe, atrophie d'une �lèvre, déformation nasale�»�: l’énoncé des violences à l’encontre de Colette montre le sadisme dont aurait fait preuve René Schembri.�Sous l’emprise de son mari, Colette n’a osé porter plainte qu’en 2009, après s’être séparée de son époux en 2002 et avoir obtenu le divorce en 2005.
27 années prescrites
Seuls les actes� qui auraient été commis par l’accusé entre 1999 et 2002 pourront être jugés par les jurés, puisqu’une prescription de 10 ans intervient dans cette affaire. L’avocat de la victime a fait savoir à l’AFP qu’il contestera la prescription et tentera de faire requalifier les «�actes sexuels non consentis�» en «�viols�». Viols que la fille du couple aurait elle aussi subis. En effet, la fille de Colette est témoin au procès et pourrait également être reconnue comme victime. Elle affirme non seulement avoir assisté à des scènes de violences infligées par son père, mais également avoir été abusée sexuellement par l’homme. Malgré ce témoignage, René Schembri continue de nier les faits, imputant les blessures de son ex-épouse à des «�accidents domestiques�».
