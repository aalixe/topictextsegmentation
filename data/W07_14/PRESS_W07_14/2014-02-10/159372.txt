TITRE: Redressement fiscal de Google : "� nous de faire notre m�nage avec l'Europe", dit Fabius - RTL.fr
DATE: 2014-02-10
URL: http://www.rtl.fr/actualites/info/international/article/redressement-fiscal-de-google-nous-de-faire-notre-menage-avec-l-europe-dit-fabius-7769607914
PRINCIPAL: 159369
TEXT:
Laurent Fabius, invit� de RTL, lundi 10 f�vrier 2014
Cr�dit : Damien Rigondeaud / RTL.fr
INVIT� RTL - Pour le ministre des Affaires �trang�res, le cas de Google est l'illustration d'un probl�me d'�quit� entre la fiscalit� des entreprises des pays membres de l'Union europ�enne.
Laurent Fabius plaide pour la recherche sur le gaz de schiste
Cr�dit : RTL
Pour Laurent Fabius, l'�vasion fiscale des g�ants du web est un probl�me europ�en. Invit� � commenter les informations du Point, selon lesquelles Bercy r�clamerait 1 milliard d'euros � Google pour �vasion fiscale, le ministre des Affaires �trang�res a admis un probl�me d'�quit� entre la fiscalit� des entreprises de certains pays membres de l'Union europ�enne.
"Les dispositions fiscales font que non seulement Google, mais aussi beaucoup d'autres soci�t�s, ne sont pas domicili�es fiscalement en France. La plupart le sont en Irlande. C�est quelque chose qui est � voir avec la r�glementation europ�enne. Il faut certainement reconsid�rer tout �a. Mais c'est � nous de faire notre m�nage avec l'Europe", a assur� Laurent Fabius.
Le ministre des Affaires �trang�res vise tout particuli�rement l'Irlande, o� Google poss�de une filiale par l'interm�diaire de laquelle il d�clare les achats d'espaces publicitaires de ses clients fran�ais pour ne d�clarer qu'une simple assistance marketing et technique dans l'Hexagone . "Ce n'est �pas normal que d'un c�t�, ils b�n�ficient de subvention, et que de l�autre, ils puissent attirer les entreprises par un dumping fiscal", a d�plor� Laurent Fabius.
Depuis plusieurs semaines, outre Google, McDonald's , Amazon et d'autres grands groupes am�ricains sont dans le collimateur de Bercy pour des pratiques d'�vasion fiscale.
La r�daction vous recommande
