TITRE: L'Iran et l'AIEA s'entendent sur 7 nouveaux points de coop�ration - France - RFI
DATE: 2014-02-10
URL: http://www.rfi.fr/europe/20140209-iran-aiea-nucleaire-teheran-yellow-cake-parchin-ardekan/
PRINCIPAL: 0
TEXT:
Modifi� le 10-02-2014 � 00:02
L'Iran et l'AIEA s'entendent sur 7 nouveaux points de coop�ration
Le yellow cake, une �tape interm�diaire de l'uranium utilis�e pour obtenir le combustible nucl�aire.
( Photo : United States Department of Energy)
L'Iran et l'AIEA ont annonc� ce dimanche 9 f�vrier, � l'issue de deux jours de �discussions techniques constructives� � T�h�ran, �tre parvenus � un accord sur sept �mesures pratiques� de coop�ration nucl�aire qui devront �tre appliqu�es d'ici le 15 mai.
Avec notre correspondant � T�h�ran, Siavosh Ghazi
L�accord en sept points pr�voit une visite de l�AIEA � la mine d�uranium de Sagand et � l�usine de yellow cake d�Ardekan dans le sud du pays.
En revanche, il ne pr�voit pas une visite au site militaire de Parchin dans les environs de T�h�ran, r�clam�e par l�AIEA depuis 2012. L�Iran est soup�onn� d�avoir proc�d� � une explosion pouvant �tre utilis�e dans le domaine de l�arme atomique. De m�me, l�accord ne pr�voit pas une rencontre de l�AIEA avec les experts et scientifiques nucl�aires iraniens.
Ce nouvel accord montre que l�Iran joue le jeu et poursuit sa coop�ration avec l�AIEA pour faire la lumi�re sur son programme nucl�aire. Il intervient alors que parall�lement, l�Iran et les grandes puissances doivent reprendre les discussions le 18 f�vrier prochain � Vienne pour obtenir un accord final pour r�gler la question du nucl�aire iranien et permettre la lev�e des sanctions �conomiques contre l�Iran.
Mais plusieurs questions sensibles doivent �tre r�gl�es. Il y a, tout d�abord, la dimension du programme d�enrichissement d�uranium, notamment le nombre et le type de machine que l�Iran pourra poss�der mais aussi le niveau d�enrichissement.
Il y a ensuite le r�acteur � eau lourde d�Arak, actuellement en construction, qui pourra produire du plutonium utilisable th�oriquement pour la fabrication de l�arme atomique. Sur les deux sujets, les positions restent encore �loign�es mais T�h�ran se dit d�termin� � obtenir un accord.
