TITRE: L�application Flappy Bird retir�e de l�App Store et Google Play | Vie du net - lesoir.be
DATE: 2014-02-10
URL: http://www.lesoir.be/422890/article/economie/vie-du-net/2014-02-09/l-application-flappy-bird-retiree-l-app-store-et-google-play
PRINCIPAL: 158619
TEXT:
L�application Flappy Bird retir�e de l�App Store et Google Play
R�daction en ligne
dimanche 9 f�vrier 2014, 22 h 18
Le d�veloppeur avait annonc� samedi qu�il retirerait son jeu � succ�s des boutiques en ligne, ce dimanche soir.
Photo DR.
Flappy Bird retir� de l�App Store et de Google Play d�s ce dimanche
Mauvaise nouvelle pour les utilisateurs de smartphones qui n�ont pas encore pu t�l�charger ��Flappy Bird��, celui-ci n�est plus disponible sur l�App Store et le Google Play depuis ce dimanche soir. Le d�veloppeur vietnamien � l�origine du jeu avait annonc� sur Twitter ce samedi qu�il avait pris la d�cision de retirer Flappy Bird. Il a donc tenu parole.
��Je suis d�sol� chers utilisateurs de Flappy Bird. D�ici 22 heures (ce dimanche, NDLR), je retirerai mon application (des boutiques en ligne, NDLR). Je n�en peux plus��, avait-t-il indiqu�.
Pour ceux qui ont eu le temps de la t�l�charger, l�application continuera � fonctionner malgr� son retrait des boutiques en ligne. Du moins, pour autant qu�il reste compatible avec les futures versions d�iOS et d�Android.
