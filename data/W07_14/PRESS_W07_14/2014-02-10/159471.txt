TITRE: VID�O. Shia LaBeouf imite �ric Cantona � Berlin
DATE: 2014-02-10
URL: http://www.huffingtonpost.fr/2014/02/10/shia-labeouf-berlin-cantona_n_4758165.html
PRINCIPAL: 159467
TEXT:
VID�O. Shia LaBeouf imite �ric Cantona � Berlin
Le HuffPost �|� Par Alexis Ferenczi
Publication: 10/02/2014 10h30 CET��|��Mis � jour: 10/02/2014 23h45 CET
Recevoir les alertes:
S'inscrire
Suivre:
Culture , Eric Cantona , Nymphomaniac 2 , Shia LaBeouf , Berlinale , cin�ma , cin�ma , Conf�rence De Presse , C�l�brit�s , Nymphomaniac , People , Shia Labeouf Berlin , Shia Labeouf Nymphomaniac , Tapis Rouge , Actualit�s
CIN�MA - Lors du festival de Berlin, dimanche 9 f�vrier, Shia LaBeouf a encore copi� . L'acteur am�ricain, venu pr�senter le premier volet de Nymphomaniac, sulfureux diptyque de Lars Von Trier, a assur� le show en conf�rence de presse et sur le tapis rouge.
Entre deux schnitzels, l'ancienne star de la franchise Transformers est apparue en pleine forme malgr� les accusations r�p�t�es de plagiat. LaBeouf a d'abord interrompu la conf�rence de presse face aux journalistes en citant �ric Cantona dans le texte (visible dans la vid�o ci-dessus).
"Quand les mouettes suivent un chalutier, c'est parce qu'elles pensent que des sardines seront jet�es � la mer."
L'original:
Deuxi�me �tape de son sketch: arpenter le tapis rouge en costume avec un sac en papier sur la t�te o� l'on peut lire "I'm not famous anymore" - "Je ne suis plus c�l�bre". Un mantra qu'il r�p�te depuis plusieurs semaines sur Twitter .
I AM NOT FAMOUS ANYMORE
� Shia LaBeouf (@thecampaignbook) February 9, 2014
Pendant que Shia faisait des siennes, Lars Von Trier n'�tait pas en reste. Le r�alisateur danois arborait lui un t-shirt avec le logo du festival de Cannes et la mention "Persona non grata, s�lection officielle". Un joli pied de nez.
Retrouvez les articles du HuffPost sur notre page Facebook .
Pour suivre les derni�res actualit�s en direct, cliquez ici .
Lire aussi:
