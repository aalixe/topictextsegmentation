TITRE: Quand la s�rie Dr. House permet de sauver une vie
DATE: 2014-02-10
URL: http://www.gizmodo.fr/2014/02/10/serie-dr-house-sauver-vie.html
PRINCIPAL: 0
TEXT:
24 0 6
Post navigation
Si les s�ries TV sont la plupart du temps bien loin de toute r�alit�, certaines assument pleinement leur r�alisme. Et parfois, cela peut s'av�rer v�ritablement salvateur. Gr�ce � un �pisode Dr House , un homme a pu sauver la vie de son patient. R�cit d'un diagnostic pas comme les autres.
Publicit�
Le Docteur Juergen Schaefer est un praticien respect� au Centre des Maladies Non Diagnostiqu�es de Francfort. Lorsqu�on lui pr�sente un patient souffrant de nombreux sympt�mes s�v�res � insuffisance cardiaque, pertes de vision et d�audition, reflux acides, etc. -, le diagnostic s�annon�ait plut�t d�licat.
Heureusement, notre docteur est grand fan de la s�rie Dr House. Dans l�un des �pisodes, le patient souffre d�un empoisonnement au cobalt � cause d�une proth�se de hanche. Il n�en fallait pas davantage : ��En moins de cinq minutes, je savais ce qui n�allait pas.�� Son dossier m�dical confirmera la chose. En 2010, un remplacement de proth�se fut effectu�, du plastique frottait sur l�ancienne proth�se, lib�rant dans son sang du cobalt.
Aujourd�hui, le patient ne souffre plus du c�ur ni de reflux acide, malheureusement, sa vue et son audition n�ont que peu progress�. Le Docteur Juergen Schaefer est certes un fan de House, il ne lui en accorde pas non plus tout le m�rite, ��quelques minutes sur Google�� et il en serait arriv� � la m�me conclusion.
