TITRE: La production industrielle est repartie en baisse en France
DATE: 2014-02-10
URL: http://www.boursier.com/actualites/economie/la-production-industrielle-est-repartie-en-baisse-en-france-22960.html
PRINCIPAL: 161262
TEXT:
La production industrielle est repartie en baisse en France
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � Coup de froid sur la production industrielle fin 2013... Au mois de d�cembre elle a recul� de 0,3% apr�s une progression de +1,2% en novembre, d'apr�s les donn�es publi�es ce lundi par l'institut national de la statistique. En revanche, au quatri�me trimestre, elle affiche une hausse de 0,7% par rapport � la m�me p�riode 2012.
Baisse de la production dans le raffinage
Sur le seul mois de d�cembre, le secteur de la cok�faction et raffinage est celui qui enregistre la plus forte baisse (-9,5% apr�s +19,3% en novembre), "en raison de mouvements sociaux dans plusieurs unit�s", explique l'INSEE. Les cinq des huit raffineries exploit�es par le groupe Total ont en effet �t� paralys�es pendant deux semaines. Les salari�s r�clamaient des revalorisations salariales.
La baisse est �galement marqu�e dans la "fabrication de mat�riels de transport" (- 4,6%). Elle augmente pourtant de +3,6% dans l'automobile. Notons par ailleurs que la douceur relative de la m�t�o a entra�n� une baisse de 2,1% de la production d'�lectricit�. A l'inverse, elle progresse de 4,5% pour la construction et de 1,4% dans la "fabrication d'�quipements �lectriques, �lectroniques et informatiques".
Marianne Davril � �2014, Boursier.com
