TITRE: Van der Wiel y va franco - Football - Sports.fr
DATE: 2014-02-10
URL: http://www.sports.fr/football/ligue-1/articles/van-der-wiel-pas-tendre-avec-les-francais-1007900/
PRINCIPAL: 0
TEXT:
Gregory van der Wiel s'est payé Christophe Jallet et tous les Français.
Par Thomas Siniecki
10 février 2014 � 12h04
Mis à jour le
10 février 2014 � 12h08
Gregory van der Wiel a sorti ses quatre vérités. Et quand le défenseur néerlandais parle sans détour, ce n'est parfois pas piqué des hannetons, notamment lorsqu'il s'en prend aux jeunes joueurs du PSG.
Gregory van der Wiel est un des joueurs et des hommes les plus discrets de l’effectif du PSG . Il n’en demeure pas moins que ses avis sont intéressants et surtout tranchés, à l’image de son interview vérité délivrée au Journal du Dimanche. Devenu l’incontestable titulaire du poste d’arrière droit avant même la blessure longue durée de Christophe Jallet , il analyse cette concurrence sans fausse modestie: "Je pense faire plus de courses. Et plus de choses techniquement. Avec Carlo Ancelotti, je ne comprenais pas toujours pourquoi je ne jouais pas. Je n'ai jamais eu la chance d'aligner trois matches de suite. Du coup, je cogitais. Impossible de donner le meilleur de soi dans ces conditions."
� Gregory van der Wiel (@Gvanderwiel) 9 Février 2014
Au-delà du terrain, Van der Wiel la joue comme Scarlett Johansson. A savoir qu’il n’aime pas beaucoup les Français… "Ce n’est pas facile de se faire des amis. Les Français ont l'air toujours en colère. Aux Pays-Bas, nous sommes plus ouverts, plus solidaires, plus positifs. Sur la route, les gens ne te laissent jamais passer. Et pas un sourire." Au moins, c’est direct. En revanche, il relativise plus quand il se remémore la finale du Mondial 2010: "Nigel de Jong a mis un coup de pied à un type, Mark Van Bommel a donné un peu aussi, c'est tout…"
Un tacle mythique
Pas question non plus de froisser Zlatan Ibrahimovic , encensé par le Néerlandais pour son mental de compétiteur: "Quand on ne gagne pas, ou même si on gagne mais qu'on ne joue pas bien, il nous secoue: ‘Eh, les mecs, faut se réveiller!’ Ça, on l'entend souvent…" Et lui, comment se définit-il ? Comme un chanceux. "Gamin, je n’avais rien. Nous étions une famille vraiment pauvre. Souvent, nous n'avions pas de quoi payer le loyer et on nous foutait dehors. Alors, désormais que j'ai tout, je peux vraiment en profiter. Ceux qui ont toujours été riches ne connaissent pas leur bonheur."
Je comprends pourquoi les jeunes sont bien meilleurs aux Pays-Bas.
Décidément prêt à se payer les Français, Van der Wiel livre aussi un tacle mythique aux jeunes footballeurs du PSG: "Les jeunes aux Pays-Bas sont beaucoup plus consciencieux qu'en France. Ils veulent vraiment arriver au top. Donc ils vivent sainement, mangent et dorment bien. La mentalité est très stricte. A l'entraînement, tu dois être concentré du début à la fin. Une minute de retard et tu as une amende. A Paris, une fois, je suis arrivé deux minutes en retard. Je stressais. Et 10 minutes après, je vois débarquer des mecs tranquilles, en train de rigoler. Je n'arrivais pas à y croire !"
Laurent Blanc trouve ainsi un écho, lui qui a récemment fustigé l’attitude des joueurs français comme son prédécesseur, Carlo Ancelotti, avant lui. Van der Wiel s’axe décidément plus sur les jeunes, mais c’est dans l’esprit: "Les jeunes sont toujours en train de se marrer, de s'interpeller à voix haute. C'est sympa, mais je comprends pourquoi les jeunes sont bien meilleurs aux Pays-Bas. Là, c’est comme s'ils n'en avaient pas grand-chose à faire du football. On ne les voit pas souvent en salle de gym, alors qu'un gars comme Thiago Silva y est tout le temps. Je ne vois pas quoi faire pour changer leur état d'esprit." En tout cas, le message est passé.
