TITRE: Omnisport | JO Sotchi - Short-track : Aucun Fran�ais en finale du 1500m
DATE: 2014-02-10
URL: http://www.le10sport.com/omnisport/jo-sotchi-short-track-aucun-francais-en-finale-du-1500m135519
PRINCIPAL: 159938
TEXT:
Envoyer � un ami
Pr�sents en demi-finales, les Fran�ais Thibaut Fauconnet et S�bastien Lepape n�ont pas r�ussi � obtenir leur ticket pour la finale du 1500m. Distanc� en fin de course lors de la premi�re s�rie, Thibaut Fauconnet a m�me h�rit� d�une p�nalit�. Quant � S�bastien Lepape, il n�a pas profit� d�une chute lors de la deuxi�me s�rie et a termin� troisi�me.
