TITRE: Puni par la CNIL, Google c�de et publie son communiqu�
DATE: 2014-02-10
URL: http://www.gizmodo.fr/2014/02/10/google-cnil-communique.html
PRINCIPAL: 0
TEXT:
16 0 2
Post navigation
Le droit am�ricain ne pr�vaut pas en France, la CNIL a donc �pingl� Google qui�refuse encore et toujours de se conformer au droit fran�ais concernant sa politique de confidentialit� des donn�es sur internet. Outre une amende , la firme devait placarder un encart sur sa page google.fr afin de notifier les usagers.
Publicit�
Plus symbolique que punitif, Google devait publier pendant 48h, un communiqu� sur la page d�accueil de google.fr, sous huit jours � compter de la notification. Disons qu�ils ont franchement tra�n� la patte en invoquant une proc�dure d�appel, qui suspendait l�obligation.
Finalement ce weekend, Google a pli� et a bien publi� la notification juridique, visiblement, sur sa page. Certes, la justice et la CNIL ont, dans un sens, obtenu ce qu�elles d�siraient, toutefois, le but reste d�apporter�une v�ritable transparence afin de savoir o� finiront les donn�es personnelles des internautes et combien de temps Google les garde sous le coude.
Quoi qu�il en soit, samedi et dimanche, la CNIL a mis Google au coin et a r�ussi � lui faire copier ses lignes. Bizarrement, l�amende de 150 000 euros doit sans doute avoir un peu plus d�effet�
