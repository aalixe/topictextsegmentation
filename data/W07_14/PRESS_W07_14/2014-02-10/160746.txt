TITRE: Une future star du football am�ricain fait son coming out
DATE: 2014-02-10
URL: http://www.francetvinfo.fr/sports/une-future-star-du-football-americain-fait-son-coming-out_526563.html
PRINCIPAL: 160744
TEXT:
Tweeter
Une future star du football am�ricain fait son coming out
En mai prochain, Michael Sam, pressenti pour devenir une star de la NFL, pourrait devenir le premier joueur ouvertement homosexuel dans un championnat r�put� tr�s machiste.
Michael Sam, joueur de football am�ricain de l'�quipe universitaire du Missouri, le 9 novembre 2013 � Lexington (Kentucky, Etats-Unis). (JOE ROBBINS / GETTY IMAGES NORTH AMERICA)
Par Francetv info avec AFP
Mis � jour le
, publi� le
10/02/2014 | 16:59
Un bastion du machisme version yankee va peut-�tre tomber : la NFL, le championnat de football am�ricain d'outre-Atlantique, va peut-�tre compter pour la premi�re fois dans ses rangs un joueur qui s'est ouvertement d�clar� homosexuel.�Michael Sam, future grande star des terrains, a fait son coming out dimanche 9 f�vrier, alors qu'il est pressenti pour int�grer une des franchises professionnelles en mai prochain.
Dans les colonnes du New York Times �(en anglais) et � l'antenne de la cha�ne sportive ESPN , le jeune d�fenseur de 24�ans a r�v�l� au grand public ce qui n'�tait qu'un secret de polichinelle au sein de son �quipe de l'universit� du Missouri�: "Je suis homosexuel, et j'en suis fier" a d�clar� celui qui sera une des principales attractions la prochaine "draft", sorte de march� aux joueurs o� les �quipes professionnelles se r�partissent les meilleures jeunes recrues. "Je comprends � quel point c'est important,�c'est �norme. Personne ne l'avait fait avant. Et je suis, bien s�r, un peu nerveux, mais je sais ce que je veux devenir... Je veux devenir pro au sein de la NFL".
I want to thank everybody for their support and encouragement,especially @espn , @nytimes and @nfl . I am proud to tell my story to the world!
� Michael Sam (@MikeSamFootball) 10 F�vrier 2014
Sam a aussit�t re�u le soutien de Gary Pinkel, son entra�neur � l'universit� :�"Nous sommes vraiment heureux pour Michael, qu'il ait pris cette d�cision, et nous sommes fiers de lui et de la fa�on dont il repr�sente le Missouri". La NFL a �galement soutenu l'annonce du joueur, soulignant dans un communiqu� "le courage et l'honn�tet�" du jeune homme, se d�clarant "press�e d'accueillir et d'encourager Michael Sam en 2014". De nombreuses personnalit�s, comme la Premi�re dame Michelle Obama qui l'a tweet� sur le compte de son cabinet, ont �galement soutenu l'initiative du jeune d�fenseur.
You're an inspiration to all of us, @MikeSamFootball . We couldn't be prouder of your courage both on and off the field. -mo
� FLOTUS (@FLOTUS) 10 F�vrier 2014
"Un vrai changement de chromosome dans le vestiaire"
Mais derri�re ces encouragements, se cachent quelques grincements de dents.�"Je ne pense pas que le foot am�ricain soit encore pr�t", a d�clar� l'agent d'un joueur NFL au magazine Sports Illustrated, sous le sceau de l'anonymat. "Dans dix ou vingt ans peut-�tre, mais pour l'heure, c'est toujours un sport de mecs. Traiter quelqu'un de 'p�d�'�est toujours tellement courant. �a provoquerait un vrai changement de chromosome dans le vestiaire."
Selon un recruteur de la NFL, �galement cit� par le magazine, cette d�claration de Michael Sam va "in�luctablement" le faire chuter dans la liste pour la 'draft'. Ce n'est pas "une d�cision tr�s intelligente", estime de son c�t� un entra�neur adjoint au sein de la NFL. Face aux nombreuses r�actions dubitatives apr�s la r�v�lation de l'homosexualit� de Michael Sam, plusieurs personnalit�s ont vivement r�agi, notamment Deion Sanders, ancienne star des terrains, qui a affirm� que Sam ne serait pas le premier joueur de football am�ricain gay, mais seulement le premier � l'assumer.
Michael Sam isn't the 1st gay player in the NFL although he is the 1st 2 come out. #realtalk Let's show him love like a family member. Truth
� DeionSanders (@DeionSanders) 10 F�vrier 2014
En 2013, l'Am�ricain�Robbie Rogers avait d�clar� craindre que son coming out n'entra�ne la fin de sa carri�re en MLS, le championnat nord-am�ricain de football. Il avait pourtant pu signer pour le Los Angeles Galaxy, l'ancien club de David Beckham, devenant le premier joueur officiellement gay de l'histoire de la MLS. En basket, Jason Collins, ex-pivot des Wizards de Washington et professionnel pendant douze ans en NBA, avait lui r�v�l� son homosexualit� en avril 2013. Hors contrat au moment de cette annonce, le joueur de 34�ans n'a plus re�u ensuite de proposition.
