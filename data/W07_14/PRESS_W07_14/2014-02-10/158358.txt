TITRE: Judo. Tournoi de Paris : Sur le fil, Maret �vite le z�ro point�
DATE: 2014-02-10
URL: http://www.ouest-france.fr/judo-tournoi-de-paris-sur-le-fil-maret-evite-le-zero-pointe-1919032
PRINCIPAL: 158355
TEXT:
Judo. Tournoi de Paris : Sur le fil, Maret �vite le z�ro point�
France -
Cyrille Maret se d�fait de Lukas Krpalek.�|�Photo : AFP
Facebook
Achetez votre journal num�rique
Cyrille Maret a remport� la plus belle victoire de sa carri�re en dominant � la derni�re seconde d'un combat mal engag� le Tch�que Lukas Krpalek.
Champion d'Europe et m�daill� mondial en 2013, Krpalek menait devant le Fran�ais quand, au moment du gong, Maret marquait un yuko pour ravir la victoire, sous les yeux d'un Teddy Riner laiss� au repos, mais insatiable � propos de son co�quipier: "Je savais qu'il fallait qu'il franchisse un cap. C'est fait. C'est un tr�s grand".�
Cinqui�me en 2008, 2009 et 2010, troisi�me en 2013, Maret a gagn� � Bercy des ambitions �normes.�
" Je n'arr�tais pas de faire des 3e places. Maintenant, je sais que je serai un vrai client pour les Euro (du 24 au 27 avril � Montpellier). Et sur le long terme, �a sert pour les JO ", estimait Maret, 26 ans, au sortir du tatami.�
�ternel espoir, le Levalloisien a pass� un dimanche de r�ve, effa�ant notamment le triple finaliste mondial et double m�daill� olympique Henk Grol.�
La victoire de Maret a apport� un vrai soulagement dans un camp fran�ais plut�t en d�veine dimanche. Avec cinq repr�sentants en finale, les Bleus pouvaient en effet attendre mieux qu'une seule m�daille d'or.�
La premi�re � s'incliner a �t� la mi-lourde Fanny Estelle Posvite, exp�di�e sur ippon par la N�erlandaise Linda Bolder. Apr�s le r�gne de Lucie Decosse, la pr�sence de la jeune fille en finale des -70kg �tait d�j� un exploit en soi.�
Idem pour Madeleine Malonga, inattendue en finale des -78kg et battue par la Slov�ne Anamari Velensek, comme pour Emilie Andeol, battue sur ippon par la Japonaise Kanae Yamabe.�
La seule vraie d�ception est venue du champion du monde des -81kg, Lo�c Pi�tri, battu en finale par le G�orgien Avtandili Tchrikishvili. " Je ne regrette rien ", d�clarait le Ni�ois au judo d'attaque, pris sur un contre alors qu'il menait au score. " C'est mon judo, c'est comme �a. "�
Tr�s attendue � l'image de Pi�tri, Audrey Tcheum�o s'est loup�e en quarts, trop passive devant la N�erlandaise Guusje Steenhuis, avant de se racheter en arrachant le bronze � la Chinoise Xin Li, victime d'un ippon hyper rapide.�
Lire aussi
