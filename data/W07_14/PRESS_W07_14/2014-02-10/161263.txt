TITRE: Les taxis en colère réclament le gel des immatriculations de VTC
DATE: 2014-02-10
URL: http://www.lefigaro.fr/societes/2014/02/10/20005-20140210ARTFIG00338-les-taxis-en-colere-reclament-le-gel-des-immatriculations-de-vtc.php
PRINCIPAL: 161262
TEXT:
Publié
le 10/02/2014 à 19:43
Quelque 800 taxis ont pris part à l'opération escargot menée depuis Roissy et 300 depuis Orly. Crédits photo : GONZALO FUENTES/REUTERS
Des milliers de chauffeurs de taxi ont mené lundi des opérations escargot pour être reçus à Matignon.
Publicité
Les taxis en colère ont bloqué le périphérique parisien, paralysant hier certains axes de la capitale pendant une bonne partie de la journée, coagulant leurs véhicules dans les principales artères et même le c�ur de Paris, jusque sous la tour Eiffel.
Quelque 800 taxis ont pris part à l'opération escargot menée depuis Roissy et 300 depuis Orly . Les syndicats, quant à eux, revendiquaient la participation de 5000 à 6000 voitures en région parisienne alors qu'à Marseille on annonçait la présence de 600  taxis. Au-delà de l'habituelle querelle de chiffres, les chauffeurs de taxi avaient décidé de gâcher la journée des Parisiens pour dire «non à la mort programmé de (leur) profession», en raison de la concurrence qu'ils jugent déloyale des voitures de tourisme avec chauffeur ( VTC ).
Nouvelles habitudes
Avec ce mouvement, les taxis espèrent obtenir un rendez-vous à Matignon. «Nous souhaitons être reçus au moins par un chef de cabinet. Et, en plus, nous voulons qu'il ait quelque chose à négocier. Ils connaissent bien nos revendications. Qu'ils agissent sinon nous continuerons!» expliquait hier, en fin d'après-midi, Nordine Dahmane ( FO ), l'un des porte-parole de l'intersyndicale des taxis.
Ils militent pour la mise en place d'une demi-heure de carence avant de prendre le client et les courses à un tarif minimum de 60 euros. Une demande ambitieuse alors que le Conseil d'État a rejeté (sans se prononcer sur le fond) le décret gouvernemental instaurant un délai de 15 minutes entre la réservation et la prise en charge des clients par les VTC. Les responsables des principales compagnies de VTC avaient d'ailleurs tenté de minimiser la portée de l'arrêt du Conseil d'État pour atténuer le courroux des taxis. Sans résultat. La décision sur le fond ne sera pas rendue avant la fin de l'année. D'ici là, les taxis redoutent que leurs clients prennent l'habitude de faire appel à leurs concurrents.
«Nous voulons le gel des autorisations de VTC pendant toute la durée des négociations, s'insurgeait hier Abdel Ghalfi ( CFDT ). Nous voulons que taxis et VTC aient les mêmes contraintes.» Si seulement 12.400 VTC sont recensées contre 55.000 taxis, la disparité des forces en présence ne calme pas les taxis.
«La concurrence est trop déloyale, poursuit Abdel Ghalfi, tant en matière d'investissements que de contraintes réglementaires. Nous n'imaginons pas l'avenir, nous vivons au présent. Si les pouvoirs publics veulent régler le problème, ils le peuvent!» Et assure ne pas être hostile au rachat des licences. «S'ils veulent le faire, qu'ils le fassent!» assure-t-il. Les Parisiens prennent leur mal en patience... redoutant déjà un prochain grand embouteillage.
