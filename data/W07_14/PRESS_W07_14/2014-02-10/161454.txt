TITRE: Des astronomes d�couvrent la plus vieille �toile connue de l'Univers - Derni�res infos - Sci-Tech - La Voix de la Russie
DATE: 2014-02-10
URL: http://french.ruvr.ru/news/2014_02_10/Des-astronomes-decouvrent-la-plus-vieille-etoile-connue-de-lUnivers-6640/
PRINCIPAL: 161452
TEXT:
Des astronomes d�couvrent la plus vieille �toile connue de l'Univers
� Photo : SXC.hu
Par La Voix de la Russie | Des astronomes de l'Australian National University ont d�couvert la plus vieille �toile connue de l'Univers. Nomm�e SMSS J031300.36-670839.3, elle se serait form�e il y a 13,7 milliards d'ann�es, annonce le site Maxisciences.
Une nouvelle �toile pas comme les autres, c'est ce qu'ont rep�r� des astronomes australiens � 6.000 ann�es-lumi�re de la Terre. Nomm� SMSS J031300.36-670839.3, l'astre fait partie des 60 millions d'�toiles observ�es par le t�lescope SkyMapper durant sa premi�re ann�e de fonctionnement. Mais celui-ci est tr�s diff�rent des autres et pour cause, il s'agit de la plus vieille �toile jamais rep�r�e dans l'Univers.
Install� au Siding Spring Observatory, le t�lescope SkyMapper est actuellement utilis� pour un projet de cinq ans visant � rechercher de vieilles �toiles afin de cr�er la premi�re carte digitale du ciel du sud. N�anmoins, les chercheurs ne s'attendaient pas � faire une telle d�couverte. On � avait une chance sur 60 millions �, a expliqu� le Dr Stefan Keller de l'Australian National University. � N
