TITRE: Ligue 1: le PSG en panne au plus mauvais moment - RTL Sport
DATE: 2014-02-10
URL: http://www.rtl.be/sport/football/footballetranger/266773/ligue-1-le-psg-en-panne-au-plus-mauvais-moment
PRINCIPAL: 160161
TEXT:
R�agir (0)
Des difficult�s contre les gros bras, un d�but d'ann�e poussif: le Paris SG n'est pas dans une forme optimale et ses petits rat�s font d�sordre � une semaine de son 8e de finale aller de Ligue des champions � Leverkusen, le 18 f�vrier.
Le nul conc�d� � Monaco (1-1) dimanche n'a en soi rien de dramatique pour le PSG, toujours en t�te de la Ligue 1 avec 5 points d'avance. Mais il ne fait que rajouter un �chec de plus � la liste des petites d�convenues essuy�es cette saison face � ses principaux poursuivants.
Les Parisiens ne peuvent nier l'�vidence. Malgr� une sup�riorit� technique �crasante et des stars � la pelle, ils n'ont toujours pas r�ussi � s'imposer contre l'un de leurs trois rivaux au classement: Monaco, Lille et Saint-Etienne.
Les vedettes de la capitale continuent de dominer leur sujet en termes de possession de balle mais celle-ci s'av�re de plus en plus st�rile et donne plut�t la sensation d'une �quipe qui ronronne, seuls les coups de g�nie de Zlatan Ibrahimovic parvenant � masquer les carences d'une formation beaucoup moins souveraine.
L'indisponibilit� pour 3 semaines d'Edinson Cavani, bless� � la cuisse, prive certes Paris d'une arme offensive majeure mais cela faisait d�j� quelques semaines que le "Matador" uruguayen, visiblement g�n� par des probl�mes personnels, avait perdu son sens du but, laissant le pauvre Ibra se d�battre avec la maladresse chronique de Lucas et d'Ezequiel Lavezzi.
Depuis la d�monstration r�alis�e en championnat contre Nantes (5-0, le 19 janvier au Parc des Princes), Paris avance au ralenti avec 2 victoires, 2 nuls et une �limination � domicile en 16e de finale de la Coupe de France contre un mal-class� Montpellier (2-1). Le bilan statistique de 2014 est � cet �gard encore plus �clairant avec 9 buts conc�d�s en 9 rencontres, toutes comp�titions confondues.
Envie de revanche
Des chiffres qui font mal et font m�me peur avant les retrouvailles avec la C1, l'objectif affich� du Qatar.
Avec son effectif 5 �toiles, le PSG ne devrait pas avoir trop de soucis pour repartir de plus belle en L1 et contenir un �ventuel retour de Monaco. D�s vendredi au Parc des Princes, Valenciennes, 18e, pourrait ainsi faire les frais de l'envie de revanche des superstars parisiennes.
Mais les principales r�ponses aux interrogations l�gitimes de 2014 sont surtout attendues le 18 f�vrier � Leverkusen o� le PSG devra hisser son niveau de jeu. En est-il capable?
Pour le moment, Laurent Blanc s'abrite derri�re l'argument physique.
"Les joueurs sont assez fatigu�s, a-t-il d�clar� dimanche. On esp�re avoir fait une bonne pr�paration � Doha en janvier pour �tre en forme en f�vrier. On sait qu'on a des �ch�ances importantes auxquelles il faudra r�pondre. Actuellement, on est un peu moins bien physiquement. Ce n'est pas illogique apr�s le nombre de matches faits depuis le d�but de saison. A un moment, les matches p�sent sur les organismes et au niveau psychologique."
Trouver le rem�de
L'explication est valable mais Blanc, qui esp�re obtenir une prolongation de son contrat courant jusqu'en 2015, a int�r�t � trouver tr�s vite le rem�de, histoire de ne pas laisser le doute s'installer � son sujet chez les d�cideurs qatariens, impatients de dominer l'Europe.
Le pr�sident du PSG Nasser Al Khelaifi ne s'en est pas cach�. L'objectif est de faire au moins aussi bien que Carlo Ancelotti lors de l'exercice pr�c�dent (championnat, quart de finale de C1) et de ramener une Coupe nationale.
Paris, qualifi� pour la finale de la Coupe de la Ligue contre Lyon, est dans les temps et son entra�neur garde intact son cr�dit aupr�s de ses dirigeants. Mais ceux-ci ne feront pas de sentiments en cas d'�chec.
Il lui reste une semaine pour �liminer ces petits grains de sable qui enrayent pour le moment la belle m�canique parisienne.
