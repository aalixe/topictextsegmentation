TITRE: Obama et Beyonc� ? �Insens�, selon les m�dias am�ricains | Monde - lesoir.be
DATE: 2014-02-10
URL: http://www.lesoir.be/424206/article/actualite/monde/2014-02-10/obama-et-beyonce-outrageux-selon-medias-americains
PRINCIPAL: 161594
TEXT:
Obama infid�le? Une rumeur n�e d�une provocation
D�contenanc�s, les m�dias en ligne am�ricains n�ont relev� que du bout des l�vres l�emballement du monde francophone sur une liaison suppos�e entre le pr�sident Barack Obama et la chanteuse pop Beyonc� Knowles , se demandant quelle mouche avait bien pu piquer journalistes, paparazzis et tweetos de l�autre c�t� de l�Atlantique.
��Outrageux��, confesse le site d�actualit� people Gawker, qui juge l�affaire ���videmment compl�tement insens�e��, puisque ��la seule chose qu�Obama et Beyonc� ont en commun, c�est d��tre noirs, Am�ricains et d�aimer Beyonc�, mais c�est � peu pr�s tout��.
Outrag�s
Outrag�s, eux aussi, les internautes am�ricains commencent � r�agir dans les r�seaux sociaux, d�couvrant la rumeur reprise par le site tabloid MediaTakeOut.com.
��La Reine Bey et le Pr�sident�? S�rement pas�!��, s�insurge @Fitzyliz.
��Je lis dans la presse fran�aise que Beyonc� et Obama auraient une liaison. Premi�re r�action�: on n�est pas le 1er avril�!��, affirme @ianhardingworld.
��Lol�: il semblerait qu�Obama ait une relation secr�te avec Beyonce. C�est le truc le plus tordu que j�aie jamais entendu��, s�amuse @Armitagina.
��Je ne m�le pas � ceux qui croient vraiment � cette rumeur sur Beyonc� et Obama, s�insurge @WittyBeyotch. Allez, s�rieusement, trouvez-vous autre chose � faire dans la vie��.
D�menti
Le Washington Post, pour sa part, a d�menti toute r�v�lation imminente . Les autres m�dias quant � eux expriment leur consternation et cherchent � comprendre les origines de cet emballement. ��Une nouvelle rumeur douteuse�� pour Obama d�j� contest� dans les cercles les plus conservateurs de la soci�t� am�ricaine pour son lieu de naissance et sa religion, note le quotidien Huffington Post . ��La rumeur de la liaison entre Obama et Beyonc� est la chose la plus absurde que nous avons entendue de toute la journ�e��, ajoute le journal.
Le site f�ministe Jezebel, relevant que le tr�s s�rieux quotidien Figaro a relay� la rumeur se demande si apr�s l�affaire Hollande-Gayet, ��peut-�tre que les m�dias fran�ais essaient de d�tourner l�attention��.
��Rendez-vous mardi dans le Washington Post pour savoir��, conclue Gawker, un brin sceptique comme tous ses confr�res, tout en pr�cisant �tre ouvert � toute information concernant Barack Obama et / ou Beyonc�. Visiblement d�pass� par la bombe qu�il vient de lancer, le photographe et paparazzi Pascal Rostain a, lui, cherch� � revenir sur ses propos, interrog� par le site Public�: ��Je n�ai pas du tout dit �a. J�ai dit que des journalistes am�ricains travaillaient sur le dossier et qu�ils pouvaient d�gainer leurs infos d�un jour � l�autre. Mais en aucun cas je n�ai dit que �a sortirait demain mardi��.
