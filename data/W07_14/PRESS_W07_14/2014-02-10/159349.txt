TITRE: FAIT-RELIGIEUX
DATE: 2014-02-10
URL: http://www.fait-religieux.com/en-bref-1/2014/02/10/arrestation-de-l-assassin-du-depute-tunisien-mohamed-brahmi
PRINCIPAL: 159345
TEXT:
Viadeo linkedin google + twitter facebook
Dans la nuit de samedi 8 � dimanche 9 f�vrier 2014, le minist�re de l'int�rieur tunisien a annonc� avoir arr�t� l'un des suspects dans l'assassinat de l'opposant� Mohamed Brahmi en juillet dernier. Le suspect en question �tait un voisin de Mohamed Brahmi. Depuis l'assassinat, il s'�tait retranch� dans une maison de la banlieue de Tunis. Le minist�re de l'int�rieur a pr�cis� : � Apr�s un �change de tirs  nourris, quatre �l�ments, dont l'un est dans un �tat tr�s critique, ont �t� arr�t�s. Parmi eux figure Hmed el-Melki, alias ?Somali? (le Somalien, ndlr),  l'un des �l�ments impliqu�s dans l'assassinat du martyr Mohamed Brahmi �.
Mardi 4 f�vrier, les forces de l'ordre tunisiennes avaient annonc� la mort de Kamel  Gadhgadhi , l'assassin pr�sum� de l'avocat et militant de gauche Chokri  Bela�d, au cours d'une op�ration anti-terroriste.
Les assassinats de Chokri Belaid en f�vrier 2013, puis de Mohamed Brahmi quelques mois plus tard - tous deux des opposants aux islamistes d'Ennahda, majoritaires � l'Assembl�e Constituante - avait plong� le pays dans une grave crise politique.
Source : AFP.
