TITRE: Cop� d�nonce la "recommandation" du livre pour enfants "Tous � poil" aux enseignants - L'Express
DATE: 2014-02-10
URL: http://www.lexpress.fr/actualite/politique/cope-denonce-la-recommandation-du-livre-pour-enfants-tous-a-poil-aux-enseignants_1321959.html
PRINCIPAL: 159315
TEXT:
Cop� d�nonce la "recommandation" du livre pour enfants "Tous � poil" aux enseignants
Par LEXPRESS.fr, publi� le
10/02/2014 �  10:16
Le pr�sident de l'UMP Jean-Fran�ois Cop� s'est emport� dimanche sur RTL contre le livre pour enfants Tous � poil, qui selon lui est "recommand� aux enseignants".�
Voter (6)
� � � �
Jean-Fran�ois Cop� a brandi, scandalis�, le livre pour enfants "Tous � poil" sur le plateau de "Grand jury" (RTL-LCI-Le Figaro).
Capture d'�cran
Le pr�sident de l'UMP Jean-Fran�ois Cop� a brandi dimanche soir un livre pour enfants intitul� Tous � poil sur le plateau du "Grand Jury" pour RTL - LCI -Le Figaro. Il l'a pr�sent� comme faisant "partie de la liste des livres recommand�s aux enseignants pour faire la classe aux enfants de primaire".�
Le d�put�-maire de Meaux a feuillet� le livre sur le plateau, en s'interrogeant: "On ne sait pas s'il faut sourire, mais comme c'est nos enfants, on n'a pas envie de sourire". "A poil le b�b�, � poil la baby-sitter, � poil les voisins, � poil la mamie, � poil le chien..." a-t-il continu�. Le pr�sident de l'UMP a pr�cis� que "�a vient du Centre de documentation p�dagogique " et a accus� le gouvernement d'�tre "p�tri d'id�ologie".�
La r�action de C�cile Duflot
La ministre du logement C�cile Duflot a imm�diatement r�agi sur Twitter pour d�noncer l'approximation des propos de Jean-Fran�ois Cop�. L'ouvrage en effet, ne ferait pas partie des recommandations faites aux enseignants. Ce livre fait en fait partie d'une s�lection d'ouvrages "Pour l'�galit� entre filles et gar�ons, 100 albums jeunesse", diffus�e par une association ard�choise, l'Atelier des Merveilles .
tiens @leLab_E1 voil� le document qui r�pertorie les albums et la d�marche http://t.co/ApErlW87bi trop intelligent pour M. Cop�...
-- C�cile Duflot (@CecileDuflot) 9 F�vrier 2014
�
La seule r�f�rence au livre sur le site du centre national de documentation p�dagogique se trouve en r�alit� dans une sous-rubrique consacr�e � l'association Atelier des Merveilles. C�cile Duflot n'a ensuite pas manqu� de railler Jean-Fran�ois Cop� sur son approche "bourrine" de la litt�rature jeunesse.
@leLab_E1 je croyais peu possible d'avoir une approche aussi bourrine de la litt�rature jeunesse ! Les enfants ont tellement plus d'humour !
-- C�cile Duflot (@CecileDuflot) 9 F�vrier 2014
�
Et JF Cop� a un avis sur "De la petite taupe qui voulait savoir qui lui avait fait sur la t�te" ? #comprendrelalitteraturejeunesse #oupas
-- C�cile Duflot (@CecileDuflot) 9 F�vrier 2014
�
"De l'humour sans tabou et sans honte"
L'auteure de l'ouvrage Claire Franek s'�tait d�j� exprim�e sur le blog tousapoilalbum.canalblog.com en 2012. Derri�re ce livre, laur�at du prix "Libbylit" (d�cern� par la branche belge de l'International board on books for young people) pour son "humour sans tabou et sans honte", se cachent des questions que voulaient soulever l'auteure: "Qu'est-ce que se d�shabiller signifie? Est-ce forc�ment pour s�duire? Ou est-ce un geste tr�s naturel? Pourquoi ne sommes-nous pas tous beaux?" Claire Franek a d�cid� d'�crire ce livre pour enfants car elle se demandait "s'il n'y a pas une marche arri�re dans la soci�t� par rapport � la nudit�". �
Un livre � prendre au second degr� qui ne met pas les enfants "mal � l'aise" puisque "l'image de fin montre qu'en r�alit� tout le monde se d�shabille pour aller nager dans la mer".�
