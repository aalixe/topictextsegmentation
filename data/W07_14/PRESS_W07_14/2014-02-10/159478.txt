TITRE: S�rie Gotham : Voici le nouveau visage du commissaire Gordon
DATE: 2014-02-10
URL: http://www.journaldugeek.com/2014/02/10/serie-gotham-voici-le-nouveau-visage-du-commissaire-gordon/
PRINCIPAL: 159472
TEXT:
Par Pierre , 10 f�vrier 2014 � 10:31 Cinema 8 avis
La Fox va tr�s prochainement lancer une s�rie bas�e sur la jeunesse du commissaire Gordon, ami et alli� de Batman. M�lant origin-story sur Bruce Wayne et enqu�tes polici�res faites par le jeune Jim Gordon, la s�rie devrait arriver � la rentr�e.
Et aujourd�hui, on conna�t le visage du nouveau Gordon. Jimbo sera donc incarn� par Ben McKenzie, jeune acteur d�j� � l�affiche de la s�rie Southland et de Newport Beach. Fun Fact, l�acteur a pr�t� sa voix � Bruce Wayne dans le dessin anim� Year One.
Si le visage du h�ros de la s�rie est maintenant connu, il reste maintenant � savoir qui incarnera les m�chants arpentant Gotham. On sait en effet que le Pingouin, le Joker et le Sphinx seront �galement pr�sents dans la s�rie.
Notons que nous ne savons pas si la s�rie Gotham se d�roulera dans le m�me univers que les films de Zack Snyder.
