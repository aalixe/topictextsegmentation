TITRE: R�publique d�mocratique du Congo: Un ancien chef de guerre jug� pour crimes ethniques -   Monde - lematin.ch
DATE: 2014-02-10
URL: http://www.lematin.ch/monde/Un-ancien-chef-de-guerre-juge-pour-crimes-de-guerre/story/29454036
PRINCIPAL: 161204
TEXT:
Un ancien chef de guerre jug� pour crimes ethniques
R�publique d�mocratique du Congo
�
Le procureur de la Cour p�nale internationale a accus� lundi l'ancien chef de guerre Bosco Ntaganda, surnomm� �Terminator�, de crimes �ethniques� dans l'est de la R�publique d�mocratique du Congo.
Mis � jour le 10.02.2014 1 Commentaire
Bosco Ntaganda devant la Cour p�nale internationale.
Image: AFP
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Surnomm� �Terminator� car r�put� sans piti�, le chef rebelle doit r�pondre de crimes contre l'humanit� et crimes de guerre commis en 2002 et 2003 en Ituri, en R�publique d�mocratique du Congo.
Le procureur de la Cour p�nale internationale tente de convaincre les juges que son dossier est assez solide pour un proc�s.
�Bosco Ntaganda et l'UPC/FPLC (sa milice, ndlr) ont pers�cut� des civils sur des bases ethniques�, a assur� la Gambienne Fatou Bensouda lors d'une audience de confirmation des charges devant la CPI, en pr�sence du suspect, �crou� depuis 2013.
�Les crimes n'�taient pas commis au hasard et n'�taient pas spontan�s�, a ajout� Mme Bensouda : �Ils visaient d�lib�r�ment la population non-Hema� de l'Ituri (est de la RDC), r�gion riche en ressources naturelles, notamment de l'or, dont la milice de M. Ntaganda voulait le contr�le.
Surnomm� �Terminator� car r�put� sans piti�, le chef rebelle, cr�ne ras� et fine moustache, a �cout� le procureur, bras crois�s et le regard souvent vide, assis au fond de la salle d'audience.
Crimes contre l'humanit�
L'audience de confirmation des charges doit avoir lieu de lundi � vendredi. Le procureur doit convaincre les juges que les �l�ments de preuve qu'il a r�colt�s sont assez solides pour justifier un proc�s. Les juges rendront leur d�cision dans les 60 jours.
Bosco Ntaganda doit r�pondre de crimes contre l'humanit� et crimes de guerre commis en 2002 et 2003 en Ituri par les Forces patriotiques pour la lib�ration du Congo (FPLC), dont il �tait le chef militaire.
Les poursuites contre Bosco Ntaganda sont les premi�res de la CPI qui incluent des charges de viols contre des enfants soldats. L'un des chefs de guerre les plus recherch�s de la r�gion des Grands Lacs, Bosco Ntaganda avait �t� en mars 2013 le premier � se livrer � la CPI.
Ambassade des Etats-Unis
Contre toute attente, il s'�tait r�fugi� � l'ambassade des Etats-Unis au Rwanda, apr�s l'�clatement du M23, sa milice congolaise, et demand� son transfert � la CPI, qui avait �mis deux mandats d'arr�t � son encontre.
A ce jour, la CPI, qui a �t� officiellement cr��e en juillet 2002, n'a prononc� qu'une seule condamnation, visant un autre chef de guerre de RDC (R�publique d�mocratique du Congo), Thomas Lubanga, lequel a �cop� en 2012 d'une peine de 14 ans de r�clusion pour avoir enr�l� des enfants dans les rangs de ses combattants.
Accus�s k�nians
La CPI doit �galement se prononcer dans les semaines qui viennent sur la poursuite des proc�dures entam�es � l'encontre du pr�sident k�nyan - son dossier le plus chaud - apr�s le d�sistement de plusieurs t�moins-cl�s.
Uhuru Kenyatta et son vice-pr�sident, William Ruto, sont poursuivis pour cinq chefs de crimes contre l'humanit� (meurtre, expulsion ou d�placement forc� de population, viol, pers�cution, et autres actes inhumains) pour les violences post-�lectorales de 2007-2008 qui ont fait 1200 morts au Kenya.
Fils Kadhafi
Deux autres personnalit�s inculp�es par la CPI - le pr�sident soudanais Omar Hassan al Bachir et Sa�f al Islam Kadhafi, fils du d�funt num�ro un libyen Mouammar Kadhafi - ne peuvent �tre jug�s � La Haye car leurs pays refusent de les livrer � La Haye. (ats/afp/Newsnet)
Cr��: 10.02.2014, 14h48
