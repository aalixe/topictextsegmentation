TITRE: JDLI.com : Apple joue les princes
DATE: 2014-02-10
URL: http://www.jdli.com/apple-joue-les-princes-art-638-1.html
PRINCIPAL: 160505
TEXT:
14 milliards de dollars d'action !
Par Thomas Debelle,  posté le 10/02/2014 à 13h56
Apple traverse une passe difficile, qui semble s'installer dans la durée. Pour éviter que le navire ne prenne trop l'eau, la société a accepté de mettre la main à la poche.
Avec une chute de 8% du titre Apple, enregistrée suite à l'annonce des derniers résultats trimestriels, et malgré les très belles ventes de l'iPhone qui représentent la moitié des revenus de l'entreprise, il fallait un signe fort de la direction en faveur du marché et des actionnaires. C'est ainsi qu'Apple a opté, dans le cadre de son programme de rachat d'actions, pour une dépense de 14 milliards de dollars sur deux semaines visant à racheter ses propres actions ! Tim Cook expliquait récemment cette stratégie en la présentant comme « opportuniste et agressive ». Reste sur les 12 derniers mois, c'est donc 40 milliards de dollars d'actions qu'Apple a déboursés. « C'est un signe fort de la confiance que nous avons en l'avenir d'Apple. Ce n'est pas une incantation, nous en faisons la démonstration dans nos actes », affirme Tim Cook au Wall Street Journal. Si certains investisseurs souhaitent voir Apple aller encore plus loin, pour l'heure aucune réponse officielle n'est parvenue, si ce n'est qu'une « mise à jour » sera dévoilée au printemps prochain autour du programme de rachat d'actions.
�
