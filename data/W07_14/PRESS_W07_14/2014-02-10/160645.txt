TITRE: Valls � Florange : le candidat PS Edouard Martin refoul�
DATE: 2014-02-10
URL: http://www.lemonde.fr/economie/video/2014/02/10/valls-a-florange-le-candidat-ps-edouard-martin-refoule_4363700_3234.html
PRINCIPAL: 0
TEXT:
Valls � Florange : le candidat PS Edouard Martin refoul�
Le Monde |
Drahi investira � 3 milliards d'euros � en France, sans r�sider fiscalement dans le pays
Vingt-quatre heures de trafic a�rien au-dessus de l'Europe en deux minutes
Comment voir si votre ville a des emprunts toxiques
Consultez la cha�ne Vid�os
L'ex-syndicaliste CFDT Edouard Martin a �t� refoul� par le service d'ordre de Manuel Valls alors que ce dernier venait faire une visite � Florange. Celui qui a incarn� le combat des employ�s d'ArcelorMittal � Florange, avant de devenir t�te de liste PS aux �lections europ�ennes dans la r�gion Grand Est, devait pourtant accueillir le ministre de l'int�rieur.
� Vous communiquez pas entre vous, parce qu'on m'a demand� de venir��, lance-t-il au policier qui lui bloque le passage sur ces images tourn�es par BFMTV.�� C'est pas grave, je vais pas lui courir apr�s��, se r�signe-t-il finalement avant d'�mettre l'hypoth�se que ce sont les chasubles orange des syndicalistes CFDT qui ont effray� les forces de l'ordre.
Le Monde.fr
