TITRE: Vers une utilisation d'un traitement diur�tique pour combattre l'autisme - LaSant�Publique.fr
DATE: 2014-02-10
URL: http://www.lasantepublique.fr/recherche-medicale/08022014,vers-une-utilisation-d-un-traitement-diuretique-pour-combattre-l-autisme,912.html
PRINCIPAL: 161507
TEXT:
Vers une utilisation d'un traitement diur�tique pour combattre l'autisme
Vers une utilisation d'un traitement diur�tique pour combattre l'autisme
R�dig� par Cyrille Humbert | Le 08 f�vrier 2014 � 14:46
Le niveau de chlore dans l'eau pourrait d�terminer l�apparition de l'autisme. En effet un niveau �lev� dans les neurones du f�tus pendant l'accouchement peut provoquer une apparition de l'autisme. Une administration pr�coce d'un m�dicament diur�tique pourrait r�duire cette apparition.
Yehezkel Ben-Ari directeur honoraire de l�Institut de neurobiologie de la M�diterran�e � Marseille et sont �quipe ont publi� leurs travaux dans la revue � Science � et gr�ce � eux de nouveaux espoirs sont � pr�sent possibles.
Les chercheurs de l'Inserm sont parvenus sur des souris dont la descendance �tait autistes en administrant un traitement diur�tique sur la m�re pendant 24h � r�tablir un comportement et une activit� c�r�brale normale.
Selon Yehezkel Ben-Ari, � L�accouchement permet de graver dans le marbre ou pas des probl�mes survenus in utero pendant le d�veloppement du cerveau. � ce qui expliquerait pourquoi un traitement pr�coce chez la m�re est n�cessaire.
L'�tude a permis de valider des essais cliniques utilisant ce traitement diur�tique chez des enfants de 11 � 13 ans, les r�sultats ont �t� publi�s en fin 2012. Le traitement a permis pour les trois quarts des enfants de diminuer la s�v�rit� de leurs troubles en am�liorant leur capacit� d '�changes avec leur entourage. Par contre cette am�lioration chute des que le traitement s�arr�te. Un essai compl�mentaire est en cours.
Ben Ari n'a pas trouv� de financement en France et s'est donc tourn� vers un fond am�ricain qui a apport� 3 millions d'euros pour une poursuite de recherche. Par contre il n'est pas envisag� de traiter la femme avant la naissance car on ne peut pas d�pister l'autisme chez le f�tus humain. Ben Ari assure tout de m�me qu'un diagnostic pr�coce associ� a un m�dicament sera s�rement un traitement pour l'avenir.
