TITRE: Toyota Motor Corporation : Apr�s Ford et GM, Toyota quitte l'Australie, infos et conseils valeur US8923313071 - Les Echos Bourse
DATE: 2014-02-10
URL: http://bourse.lesechos.fr/infos-conseils-boursiers/infos-conseils-valeurs/infos/apres-ford-et-gm-toyota-quitte-l-australie-949693.php
PRINCIPAL: 159656
TEXT:
Toyota Motor Corporation : Apr�s Ford et GM, Toyota quitte l'Australie
10/02/14 � 10:01
Apr�s Ford et GM, Toyota quitte lAustralie | Cr�dits photo : � Ford Motor Company
(Actualis� avec pr�cisions et citations)
MELBOURNE/TOKYO, 10 f�vrier (Reuters) - Toyota a annonc� lundi qu'il cesserait de produire des voitures et des moteurs en Australie � compter de 2017, embo�tant ainsi le pas � General Motors et Ford qui avaient annonc� l'an dernier leur retrait du pays.
L'Australie, autrefois une base de production dynamique pour l'industrie automobile, paie ainsi le niveau �lev� de ses co�ts et l'appr�ciation de sa devise, qui rend les voitures import�es meilleur march�.
"Nous avons tout fait pour nous adapter, mais la r�alit� est qu'il y a trop de facteurs au-del� de notre contr�le qui font qu'il n'est pas viable de produire des voitures en Australie", a d�clar� dans un communiqu� Max Yasuda, pr�sident de Toyota Australia.
Quelque 2.500 emplois seront concern�s par la fermeture de l'usine australienne en 2017, a ajout� le groupe.
Le d�part de Toyota apr�s plus d'un demi-si�cle de pr�sence en Australie porte un nouveau coup au gouvernement conservateur du Premier ministre Tony Abbott, qui cherche � g�rer au mieux le ralentissement de l'�conomie nationale d� la fin du boom des investissements dans l'industrie mini�re.
"C'est une nouvelle terrible pour toutes les personnes concern�es et c'est terrible pour le gouvernement et moi-m�me", a d�clar� le Premier ministre � Canberra.
Pour David Oliver, secr�taire g�n�ral de la Conf�d�ration australienne des syndicats (ACTU), la disparition de la production automobile aura "des cons�quences lourdes pour toute l'�conomie".
"Ils (le gouvernement) n'ont absolument rien fait pour garder Toyota", a-t-il dit en estimant la perte � 21 milliards de dollars australiens (13,7 milliards d'euros) pour l'�conomie.
La production de v�hicules en Australie a �t� r�duite de moiti� en quelques ann�es, passant de plus de 400.000 en 2004 � 200.000 en 2012. Le secteur automobile emploie directement et indirectement quelque 45.000 personnes, selon des chiffres officiels.
Il s'est vendu l'an dernier 1,14 million de v�hicules en Australie, un record, mais la part de v�hicules produits localement a �t� � peine de 10%, l� aussi du jamais vu. Toyota est arriv� en t�te des ventes avec pr�s d'un cinqui�me du march�.
Par contraste, les grands constructeurs mondiaux ont construit de nouvelles usines dans des pays comme l'Indon�sie, devenue une base de production plus attrayante gr�ce � ses co�ts plus avantageux et � l'�mergence d'une classe moyenne. (Sonali Paul � Melbourne, avec James Regan � Sydney et Chang-Ran Kim � Tokyo, V�ronique Tison pour le service fran�ais)
Laisser un commentaire
