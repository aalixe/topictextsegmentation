TITRE: "Fuck The EU" : une diplomate am�ricaine d�rape - Arr�t sur images
DATE: 2014-02-10
URL: http://www.arretsurimages.net/breves/2014-02-10/Fuck-The-EU-une-diplomate-americaine-derape-id16889
PRINCIPAL: 160812
TEXT:
Vous avez besoin d'aide ?
Vous avez oubli� votre mot de passe ?
Saisissez ci-dessous le courriel avec lequel vous vous �tes abonn� au site. Un message vous sera envoy� pour obtenir un nouveau mot de passe�:
Vous avez besoin d'aide ?
Vite Dit Les "vite dit" sont tous les contenus des m�dias fran�ais et �trangers qui nous semblent dignes d��tre signal�s. Si n�cessaire, ils feront l'objet d'un traitement approfondi dans nos articles payants. La page des "vite dit" a pour fonction de donner aux non-abonn�s une impression des centres d�int�r�t du site. La page des "vite dit" est tr�s fr�quemment actualis�e au cours de la journ�e.
16h31 tweet
Par Gilles Klein le 10/02/2014
"Que l'Union Europ�enne aille se faire foutre" a dit une diplomate am�ricaine � l'ambassadeur des USA en Ukraine. La conversation t�l�phonique a �t� enregistr�e, et diffus�e sur Internet. Le New York Times souligne que la conversation a sans doute �t� mise en ligne par les services russes. Sur Twitter, l'ambassadeur a r�pondu avec humour � l'officiel russe qui l'avait twitt�e. Le Monde revient sur cette �pisode et en tire des le�ons.
"Fuck EU" "(Que l'Union Europ�enne aille se faire foutre) a dit Victoria Nuland, vice-secr�taire d'Etat charg�e de l'Europe au minist�re am�ricain des Affaires �trang�res, � Geoffrey Pyatt, ambassadeur � Kiev, au cours d'une conversation t�l�phonique. Alors qu'elle discute avec lui de la strat�gie US visant � faire venir un nouveau repr�sentant de l'ONU en Ukraine, elle lance "And you know, fuck the EU".
L'enregistrement est apparu sur Internet mercredi soir , sous titr� en russe via Twitter et YouTube. sous le titre "Les marionnettes de Ma�dan" sous-entendant que les manifestants du square Ma�dan, � Kiev sont manipul�s par les USA, explique le New York Times .
Le journal ajoute que, jointe par t�l�phone, Nuland semblait plus "amus�e que contrari�e", disant "cela fait partie des risques du m�tier".
Victoria Nuland
�
"Jugement pol�mique de la vice-s�cr�taire d'Etat Victoria Nuland � propos de l'Union Europ�enne" annonce sur Twitter, Dmitry Loskutov @DLoskutov , l'assistant du Premier ministre russe, qui attire l'attention sur l'enregistrement en mettant un lien vers YouTube, mercredi soir.
�
"On s'amuse du Tweet ici � Kiev" r�pond Geoffrey Pyatt, l'ambassadeur am�ricain en Ukraine en mettant en copie� @DLoskutov , l'assistant du Premier ministre qui a twitt� le fameux lien vers YouTube.
L'ambassadeur trouve cela dr�le� si l'on en croit la photo qui accompagne son tweet . On le voit ordinateur � la main en train de lire le tweet en compagnie de plusieurs personnes dont Nuland.
�
� �
Pour Le Monde, ce n'est pas l'interjection de la diplomate qui compte, mais plut�t les le�ons que l'on peut en tirer . Tout d'abord "Moscou n'h�site pas � recourir aux vieilles ficelles du KGB". En effet, selon l'agence Reuters citant une source diplomatique, l'enregistrement a �t� diffus� sur YouTube jeudi par Dmitri Loskoutov, un collaborateur du vice-premier ministre russe, Dmitri Rogozine, avec transcription de l'�change en russe, alors que Mme Nuland arrivait � Kiev.Mais la chanceli�re allemande, Angela Merkel, et le pr�sident du Conseil europ�en, Herman Van Rompuy, n'ont pas ri. Ils ont tous les deux employ� le m�me terme face � ces propos "inacceptables". Nuland a pr�sent� ses excuses, tandis que la Maison Blanche par la voix de Jay Carney, son porte-parole a soulign� le fait que la conversation ait �t� "diffus�e sur Twitter par le gouvernement russe est significatif du r�le de la Russie".
Deuxi�me point aux yeux du Monde, "il n'y a plus de diplomatie secr�te", en rappelant que grace � Snowden, on sait que "La NSA �coute le monde entier, mais deux hauts responsables am�ricains s'entretiennent d'une des crises mondiales les plus sensibles du moment sur leur t�l�phone portable sans se soucier d'�tre �cout�s ? C'est l'arroseur arros�."
En outre, l'Europe et les USA ne sont pas d'accord sur la crise ukrainienne. Selon Le Monde, pour les Am�ricains c'est une crise dans le style de la guerre froide, tandis que pour l'l'UE, elle concerne l'Europe. Le Monde estime ensuite que les USA sont "arrogants" face � la crise ukrainienne. "La familiarit� avec laquelle la vice-secr�taire d'Etat �voque les dirigeants de l'opposition ukrainienne (� Yats � pour Arseni Iatseniouk, � Klitsch � pour Vitali Klitschko) et les postes qu'elle leur attribue dans un �ventuel gouvernement traduit une �tonnante maladresse , voire arrogance". Et enfin, toujours selon Le Monde : "L'affaire enfonce un coin suppl�mentaire dans les relations entre l'Allemagne et les Etats-Unis, de plus en plus froides". En effet, Merkel a �t� la premi�re � r�agir, en parlant de propos inacceptables.
