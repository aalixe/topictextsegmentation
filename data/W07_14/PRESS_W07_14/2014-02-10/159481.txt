TITRE: � ��Ast�rix et les jeux olympiques�� en t�te des audiences ! Stars Actu
DATE: 2014-02-10
URL: http://www.stars-actu.fr/2014/02/asterix-et-les-jeux-olympiques-en-tete-des-audiences/
PRINCIPAL: 159480
TEXT:
Tweeter
Pas de grande surprise aujourd�hui encore � la lecture des chiffres du sacro-saint audimat. Comme il fallait s�y attendre c�est la com�die propos�e par TF1 ��Ast�rix et les jeux olympiques�� qui s�est impos�e en t�te des audiences de premi�re partie de soir�e de ce dimanche 9 f�vrier 2014.
Sign�e Fr�d�ric Forestier et Thomas Langmann, ce long-m�trage port� par Clovis Cornillac et G�rard Depardieu a convaincu 6.1 millions de t�l�spectateurs soit 24% de l�ensemble du public et 31% des m�nag�res de moins de 50 ans.
Dans un communiqu� envoy� aux r�dactions, TF1 se f�licite du score r�alis� aupr�s des 11-14 ans. Sur cette cible, la part d�audience atteint les 50%.
capture bande-annonce TF1 (DR)
C�est le magazine de M6 ��Capital�� qui s�adjuge la seconde place. Hier soir le th�me ��D�penses publiques : l��tat jette-il notre argent par la fen�tre�� a suscit� la curiosit� bien ��naturelle�� de 3.6 millions de personnes soit 14% des t�l�spectateurs.
Et c�est France 2 qui compl�te le podium avec la rediffusion du film de Pascal Thomas ��L�Heure z�ro��. Il a �t� suivi par 3.4 millions de t�l�spectateurs (pda 12.5%).
France 3 prend la quatri�me place avec sa s�rie ��Les enqu�tes de Morse��. L��pisode du jour ��Le fant�me de l�op�ra�� a convaincu 2.8 millions de t�l�spectateurs (pda 10%).
Autres articles
