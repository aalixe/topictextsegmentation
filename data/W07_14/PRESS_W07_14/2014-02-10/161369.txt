TITRE: La tourn�e am�ricaine de Fran�ois Hollande a commenc�
DATE: 2014-02-10
URL: http://www.leparisien.fr/international/francois-hollande-est-arrive-aux-etats-unis-10-02-2014-3577997.php
PRINCIPAL: 0
TEXT:
La tourn�e am�ricaine de Fran�ois Hollande a commenc�
�
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Washington : Hollande accueilli avec faste � la Maison-Blanche
Ce dernier entame une visite officielle de trois jours dans la capitale f�d�rale, Washington, puis � San Francisco.�Fran�ois Hollande est sorti seul de son Airbus pr�sidentiel aux alentours de 14h30, heure locale (20h30, heure de Paris). Sur un tapis rouge, entour� de Marines, Fran�ois Hollande a �cout� la marseillaise, puis l'hymne am�ricain, aux c�t�s Natalie Jones, chef du protocole des Etats-Unis.�
Il a par la suite �t� accueilli par son homologue am�ricain Barack Obama. Ensemble, les Chefs d'Etat ont emprunt� le Boeing pr�sidentiel de Barack Obama direction la Virginie. Les deux chefs d'Etat sont arriv�s � Monticello, aux alentours de 22h, heure fran�aise, � 200 km au sud-ouest de Washington. Ils doivent y visiter une r�sidence luxueuse appartenant � l'ancien pr�sident, tr�s francophile, Thomas Jefferson. Une visite symbolique qui sera l'occasion d'�voquer les relations (franco-am�ricaines) dans un environnement �voquant �leur histoire, et des possibilit�s qu'elles rec�lent�, pr�cise un haut responsable de l'administration Obama.
Sujet internationaux et �conomiques au menu
Ce mardi, les pr�sidents s'envoleront t�t le matin pour Washington, o� ils s'entretiendront dans le bureau ovale de la Maison Blanche puis d�jeuneront avec le vice-pr�sident, Joe Biden. Un d�ner d'Etat se tiendra ce mardi soir � la Maison Blanche, avec une centaine d'invit�s de marque, tri�s sur le volet (voir encadr�).
Plusieurs sujets internationaux sont au coeur de cette visite officielle, un privil�ge car seuls quatre Chefs d'Etat y ont eu droit. Barack Obama et Fran�ois Hollande devraient aborder la lutte contre le terrorisme au Sahel, en Syrie, au Liban... Les deux hommes devraient �galement aborder les questions �conomiques. Ombre au tableau n�anmoins, le scandale des �coutes de la NSA, visant notamment les alli�s europ�ens et d�voil�s par Edward Snowden. Ces r�v�lations avaient ouvert �une p�riode difficile, non seulement entre la France et les Etats-Unis, mais aussi entre l'Europe et les Etats-Unis�, a d�clar� Fran�ois Hollande au magazine Time.��
VIDEO. Fran�ois Hollande est arriv� aux Etats-Unis
L�gumes de la Maison Blanche et barbe � papa au menu pour Fran�ois Hollande
La centaine d'invit�s sera bichon�e mardi soir au d�ner d'Etat de la Maison Blanche. Un parfum marqu� d'Am�rique, avec des l�gumes du jardin de Michelle Obama, des vins de Virginie et m�me de la barbe � papa, flottera sur le repas.
Sous une tente sp�cialement dress�e sur la pelouse sud, les convives se r�galeront de �caviar osci�tre am�ricain� de l'Illinois, d'un velout� de pommes de terre avec oeufs de caille avant une �petite salade d'hiver� aux l�gumes du jardin que Michelle Obama a plant� � la Maison Blanche.�Ils se d�lecteront ensuite d'un faux-filet de boeuf du Colorado avec �chalotes et blettes brais�es avant une glace � la vanille avec mandarines de Floride et ganache au chocolat de Hawa�, l'Etat o� est n� le pr�sident am�ricain.
Le tout sera arros� de vins de Californie, de l'Etat de Washington et d'un blanc de chardonnay de Virginie, dans un d�cor m�lant pi�ces de vaisselles et de mobilier ancien et moderne, au d�cor floral blanc ou mauve.
La chanteuse am�ricaine de soul Mary J. Blige se produira � la fin du d�ner alors que seront servies des assiettes de friandises aux couleurs tricolores, avec sirop d'�rable du Vermont, biscuits � la lavande et barbe � papa.
La Maison Blanche n'a pas indiqu� qui prendrait place � la droite du pr�sident am�ricain, o� s'assied traditionnellement la Premi�re Dame du pays invit�.
Eric Hacquemand, envoy� sp�cial du Parisien nous d�voile le menu du d�ner d'Etat
#Hollande Demandez le menu du diner d'Etat avec le couple #Obama � la #Maison Blanche pic.twitter.com/tyjYms60UO
� eric hacquemand (@erichacquemand) February 10, 2014
LeParisien.fr
