TITRE: Inondations : la d�crue est amorc�e - France 3 Bretagne
DATE: 2014-02-10
URL: http://bretagne.france3.fr/2014/02/10/inondations-la-decrue-est-amorcee-412237.html
PRINCIPAL: 159379
TEXT:
+  petit
La plupart�des cours d'eau de Bretagne connaissent une lente d�crue, amorc�e depuis hier dimanche. Seule la Vilaine aval continue de monter et cr�e une situation critique sur le secteur de Redon, encercl� par les eaux. �Dans le Morbihan et l'Ille-et-Vilaine�o� la vigilance pour les crues vient de passer � l'orange, de tr�s nombreuses routes sont coup�es car inond�es . La RN�24 (Rennes-Lorient) est r�ouverte�� la circulation depuis ce matin.
Dans le Finist�re, la situation s'est am�lior�e d�s samedi. � Quimperl�, la La�ta�a amorc� sa d�crue. Elle est pass�e en vigilance jaune.
Dans les C�tes-d'Armor, la situation reste difficile dans le secteur de Plancoet . Avec 1,30 m d'eau par endroit sur les quais ce matin, la crue est historique, comparable � celle de 1974. La d�crue s'amorce tout doucement mais l'Arguenon ne devrait pas retrouver son lit avant demain.
Morbihan
� Malestroit, la ville restait coup�e en deux par l'Oust ce matin, le pont entre les deux parties �tant interdit � la circulation depuis samedi.�Les �tablissements scolaires de la commune ont �t� ferm�s aujourd'hui et le transport scolaire �tait annul�. 600 porcs ont �t� �vacu�s dans une ferme encercl�e par les eaux depuis vendredi.
Josselin�et�Pontivy�ont toujours des secteurs avec les pieds dans l'eau. De nombreuses routes sont coup�es � la circulation. Des transports scolaires ont �t� perturb�s. L'Oust et le Blavet restent en vigilance orange ce soir.
Pr�visions du service Vigicrues�(19h30 ce lundi) pour les principaux cours d'eau :
Le Blavet :
A Pontivy, le niveau restera �lev� au cours de la nuit prochaine (autour de 0,80m +/-20cm)
A Languidic et � Inzinzac Lochrist, la lente d�crue va se poursuivre jusqu'� demain mardi 11 f�vrier.
L'Oust, la Vilaine m�diane et la Seiche:
Les pluies tomb�es au cours de la nuit derni�re ne provoqueront pas de nouvelles r�actions significatives. La tendance actuelle est � une lente d�crue sur l'ensemble du bassin.
Le Meu :
Quelques r�actions peu significatives sur les amonts se sont produites suite aux pluies de la nuit derni�re. Ces r�actions ne provoqueront pas de nouvelles remont�es des niveaux � Montfort sur Meu et et Br�al-Mordelles. Sur ces secteurs, la d�crue se poursuivra jusqu'� demain.
La Vilaine amont :
La lente d�crue se poursuivra au cours de la nuit prochaine.
La Vilaine aval :
La crue de l'Oust a atteint son maximum sur le tron�on aval. Une lente d�crue s'est amorc�e.�La crue de la Vilaine a �galement atteint son maximum. Les niveaux resteront stables pendant plusieurs heures avant le d�but d'une lente d�crue.
La La�ta :
Les pluies de la nuit derni�re ont provoqu� une nouvelle r�action sur les amonts. Les c�tes pr�vues pour les prochaines pleines mers, � la station Charles de Gaulle de Quimperl�, sont de�3,35m +/15cm vers 4h demain. Entre la pleine mer de mardi matin et celle de mardi apr�s-midi, le niveau restera �lev� (3,00m +/- 15cm).�
Axes de circulation coup�es (informations CRIR lundi 10 f�vrier 17h30)
D�partement de l�Ille-et-Vilaine
