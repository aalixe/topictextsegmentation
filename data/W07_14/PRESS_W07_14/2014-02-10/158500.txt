TITRE: VID�O. Nicolas Bedos revient sur sa critique de Dieudonn�, puis s'adresse � Hollande et Gayet dans "On n'est pas couch�"
DATE: 2014-02-10
URL: http://www.huffingtonpost.fr/2014/02/09/nicolas-bedos-dieudonne-francois-hollande-julie-gayet-video-pas-couche_n_4755765.html
PRINCIPAL: 158499
TEXT:
VID�O. Nicolas Bedos revient sur sa critique de Dieudonn�, puis s'adresse � Hollande et Gayet dans "On n'est pas couch�"
Le HuffPost �|� Publication: 09/02/2014 17h06 CET��|��Mis � jour: 09/02/2014 17h11 CET
S'inscrire
Suivre:
France 2 , Nicolas Bedos , On n'Est Pas Couch� , Bedos , Bedos Dieudonn� , Bedos Gayet , Bedos Hollande , Chronique Bedos , Chronique Nicolas Bedos , Dieudonn� , T�l�vision , Laurent Ruquier , Actualit�s
T�L�VISION - Il est de retour. Nicolas Bedos, apr�s une chronique sur l'affaire Dieudonn� il y a un mois , s'est une nouvelle fois illustr� sur le plateau d'"On n'est pas couch�", l'�mission de Laurent Ruquier sur France 2.
Apr�s son dernier passage t�l�vis� le 12 janvier pendant lequel il avait vigoureusement attaqu� Dieudonn� , Nicolas Bedos, menac�, avait �t� contraint de d�m�nager chez un ami. Samedi soir, l'humoriste est revenu sur le sujet, remettant une couche sur le pol�miste .
"Il se trouve mon cher Laurent que j'ai �t� contraint de m'absenter quelque temps suite � une petite chronique taquine sur Monsieur M'Bala, M'Bala, M'Bala, M'Bala...", lance Nicolas Bedos devant un public conquis. Il continue: "Je demande du fond du c�ur � tous les juifs de France de me l�cher la grappe. Ce n'est pas parce que je me suis oppos� � M. Dieudonn� que vous avez le droit de me faire des bisous dans la rue".
L'humoriste jongle avec l'actualit�, quand il passe ensuite au "cas" Hollande-Gayet . Et l� encore, il y va fort, lui qui affirme avoir pass� une nuit (platonique) avec l'actrice en 2009 dans une lettre ouverte .
"Cher Fran�ois, que ce soit bien clair, l'�lys�e c'est � toi. (...) Les gonzesses sont nos attributions. En plus c'est nouveau, on ne t'a jamais vu aux r�unions des infid�les c�l�bres. Tu portais un casque l� aussi?" Avant d'encha�ner : "Fran�ois Hollande, tu arrives bidoche au vent et tu sors avec Gayet. Franchement, avec la tronche que tu as, si ce n'est pas un abus de pouvoir!"
� Voir sa chronique dans la vid�o en haut de l'article
Retrouvez les articles du HuffPost sur notre page Facebook .
Pour suivre les derni�res actualit�s en direct, cliquez ici .
Contribuer � cet article:
