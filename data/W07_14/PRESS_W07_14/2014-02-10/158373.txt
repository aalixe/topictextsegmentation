TITRE: Etats-Unis : un b�b� survit sans son sang � metronews
DATE: 2014-02-10
URL: http://www.metronews.fr/info/etats-unis-un-bebe-survit-sans-son-sang/mnbi!0Jcx0Vjy3LX/
PRINCIPAL: 158367
TEXT:
Mis � jour  : 10-02-2014 07:35
- Cr�� : 09-02-2014 22:16
Etats-Unis : un nouveau-n� survit sans son sang
MIRACLE � La petite Hope a �t� surnomm�e le "b�b� fant�me" par les journalistes. Il y a quelques semaines, elle est n�e d�nu�e de sang mais a tout de m�me surv�cu. Elle a aujourd'hui six semaines et se porte bien.
Tweet
�
La petite Hope a �t� surnomm�e le "b�b� fant�me" par la presse am�ricain (photo d'illustration). Photo :�Wickley Bob/SUPERSTOCK/SIPA
Le "b�b� fant�me", ce miracle am�ricain. Hope Suarez n'aurait jamais d� survivre � sa mise au monde. Et pour cause : elle est n�e avec un taux d'h�moglobine extr�mement faible cens� la condamner. En temps normal, les b�b�s perdent environ 2�% de sang � la naissance. Elle en a perdu plus de 80�%. "Elle �tait vraiment p�le", a confi� le Dr. Marielle Nguyen, du service de n�onatologie, aux journalistes d�ABC . "Je savais que quelque chose n'allait pas lorsqu'ils ont piqu� son doigt de pied et qu'aucune goutte de sang n'est sortie", a expliqu� son p�re aux journalistes.
De l'espoir, les m�decins et ses parents n'en avaient quasiment pas. Et pourtant. Six semaines apr�s sa naissance, sans intervention chirurgicale, la petite Hope, qui porte bien son nom (��espoir�� en anglais) va tr�s bien et a le teint tout rose. Les m�decins ne s'expliquent pas ce qui a r�g�n�r� son sang, ni ce qui a caus� cette h�morragie : "Dans de nombreux cas, cela arrive subitement, sans cause. Parfois, cela survient apr�s un accident de la route ou une rupture du placenta", ce qui n'est pas le cas ici, a d�clar� le docteur Nguyen, m�decin au service de n�onatalogie de l'h�pital d'Irvine.
Pas un cas unique
La petite doit tr�s certainement la vie � sa maman. En effet, trois semaines avant la date pr�vue de l'accouchement, cette derni�re ne sentait plus sa fille bouger dans son ventre et a d�cid� de se rendre d'urgence � l'h�pital. Grand bien lui en a pris : les m�decins ont affirm� qu'� une ou deux heures pr�s, Hope n'aurait tr�s certainement pas surv�cu.
Un cas exceptionnel, tr�s rare, mais pas unique. En 2012 d�j�, le quotidien britannique The Telegraph rapportait une histoire similaire, celle de la petite Olivia �galement n�e avec tr�s peu de sang mais qui avait surv�cu. Une nouvelle preuve, donc, que les b�b�s, aussi fragiles soient-ils peuvent d�montrer d'une incroyable r�sistance dans les cas les plus extr�mes.
Thomas Roure
