TITRE: Un retraité jugé pour tortures sur sa femme nie les faits - 11 février 2014 - Le Nouvel Observateur
DATE: 2014-02-10
URL: http://tempsreel.nouvelobs.com/societe/20140210.AFP9695/un-retraite-comparait-en-assises-pour-tortures-sur-sa-femme.html
PRINCIPAL: 159838
TEXT:
Actualité > Société > Un retraité jugé pour tortures sur sa femme nie les faits
Un retraité jugé pour tortures sur sa femme nie les faits
Publié le 10-02-2014 à 11h15
Mis à jour le 11-02-2014 à 11h35
A+ A-
Aix-en-Provence (AFP) - René Schembri, un enseignant à la retraite, poursuivi pour tortures et actes de barbarie sur son ex-femme Colette, a nié les faits lundi, au premier jour de son procès devant la cour d'assises des Bouches-du-Rhône.
L'homme, âgé de 72 ans, s'est présenté libre à l'audience prévue pour durer trois jours.
"Je confirme ce que j'ai toujours dit : n'avoir été en rien" dans les blessures dont souffre son ex-épouse, Colette Renault, qui a porté plainte contre lui en 2009 après 32 ans de vie commune marquée, selon elle, par des actes de tortures psychologiques et physiques.
"Je compatis", a ajouté l'ex-mari, un homme de petite taille, moustache et cheveux poivre et sel, s'exprimant d'une voix claire.
Comme lors de l'enquête, il a de nouveau accusé Mme Renault, qui a porté plainte contre lui quatre ans après leur divorce, d'être motivée par une volonté de vengeance et dans un but financier après avoir été déboutée dans la procédure de liquidation de la communauté de biens du couple.
En appel, Mme Renault avait finalement obtenu 140.000 euros. Entre temps, deux mois après avoir été déboutée en première instance, elle avait porté plainte contre son ex-mari, pour actes de torture.
"Ce n'est pas du tout financier, pas une vengeance, pas de la haine", a rétorqué Colette Renault, petite femme rousse de 70 ans, à l'allure fragile et à la voix fluette.
"Je veux la reconnaissance de la justice, il a une influence psychologique sur moi, je n'arrive pas à me défaire de cet homme, là, au-dessus de moi", a-t-elle ajouté, évitant de regarder son ex-mari.
Mais "il y a eu un élément déclencheur" au dépôt de plainte, a-t-elle déclaré. "J'ai été déboutée, il m'a encore enfoncé la tête en disant que je n'avais pas droit à l'aide juridictionnelle."
Une partie des faits prescrits
"Si la séparation s'était faite de manière satisfaisante pour vous, vous n'auriez pas fait revivre ces situations de violence" ?, demande le président, Jacques Calmettes, notant que "cela n'enlevait rien à la réalité des faits".
"Oui, je n'aurais pas pensé à porter plainte. On avait été séparés, il ne faisait plus pression sur moi", répond Mme Renault.
Prenant à nouveau la parole, celle qui dit n'avoir n'avait "jamais osé parlé" y compris "durant la procédure de divorce" a longuement exposé sa vie de souffrance et de soumission.
"Je suis victime, il est le bourreau", dit-elle, regardant cette fois droit dans les yeux, la voix brisée par les sanglots, celui qu'elle n'appelle plus que "M. Schembri".
"Je ne savais pas qu'il pouvait y avoir viol entre époux, j'ai appris après", ajoute-t-elle, précisant ne "jamais l'avoir trompé", alors qu'il avait des maîtresses "parmi ses élèves".
Par deux fois, Colette a tenté fuir, mais a été rattrapée par son mari, la première fois à peine quatre mois après leur mariage, en 1971. Mais "j'ai pardonné, je suis repartie avec lui".
Peu après les coups reprennent, "quand je pleurais, je prenais double", raconte-t-elle. Après les blessures aux bras, elle perd une partie de ses dents, et puis un oeil, à la suite d'une violente gifle.
René Schembri continue de nier, "tombe des nues", devant les "accusations de violences sexuelles" ne se sentant "absolument pas responsable" des faits énumérés. "Tout ça est de la broderie, un mille-feuille de mensonges", lance-t-il, ajoutant comme preuve qu'ils avaient, à chaque fois, fait la paix".
"C'est une constante des femmes qui subissent des violences, de rester", fait remarquer le président Calmettes.
René Schembri ne répondra pas de la totalité des sévices dont l'accuse son ex-femme : une partie d'entre eux sont prescrits. Seuls une partie de ceux, commis entre fin 1999 et 2002, sont pris en compte.
Les huit expertises médicales pratiquées sur Mme Renault sont pourtant sans appel : toutes les plaies sont d'origine traumatique et susceptibles de correspondre à des coups de poing, des coups de nerf de bœuf ou de tout autre objet contondant.
Présent à la barre lundi après-midi, un médecin a détaillé "les lésions chroniques" dès 1973 et les "mutilations sexuelles qui ressemblent à des techniques d'excision".
René Schembri risque 15 à 20 ans de réclusion criminelle.
Partager
