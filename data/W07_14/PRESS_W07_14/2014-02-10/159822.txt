TITRE: En Egypte : les Fr�res musulmans accus�s d'avoir form� une branche arm�e  - France - RFI
DATE: 2014-02-10
URL: http://www.rfi.fr/moyen-orient/20140209-egypte-police-accuse-freres-musulmans-branche-militaire-tuer-forces-ordre/
PRINCIPAL: 159820
TEXT:
Modifi� le 10-02-2014 � 16:33
En Egypte : les Fr�res musulmans accus�s d'avoir form� une branche arm�e
par RFI
Partisan des Fr�res musulmans d�log� par la police �gyptienne lors d'un rassemblement pro Mohamed Morsi, le 8 janvier 2014 au Caire
REUTERS/Amr Abdallah Dalsh
Egypte
Le minist�re �gyptien de l�Int�rieur a annonc� dimanche le d�mant�lement d�une cellule terroriste appartenant � la branche arm�e de la Confr�rie des Fr�res musulmans. Cinq personnes ont �t� arr�t�es et neuf autres sont recherch�es. Ils sont accus�s de l�attentat contre un barrage policier qui a fait cinq morts en d�but de semaine.
Avec notre correspondant au Caire, Alexandre Buccianti
C�est la premi�re fois que la police accuse officiellement les Fr�res musulmans d�avoir form� une branche arm�e dont la mission est de tuer les membres des forces de l�ordre.
Une police qui s�est aussi attaqu�e aux possesseurs de pages Facebook qui avaient publi� des photos, noms et adresses de policiers consid�r�s comme des � ennemis de l�islam �.
Des forces de l�ordre d�autant plus nerveuses que pr�s de 200 policiers ont �t� tu�s depuis la destitution du pr�sident Fr�re musulman Mohamed Morsi en juillet.Des attentats qui ont provoqu� un mouvement de contestation dans les rangs m�me des forces de l�ordre.
Plusieurs centaines de policiers de base ont entam� depuis deux jours des gr�ves de protestation sporadiques et des manifestations devant plusieurs commissariats d�Egypte.
Ils r�clament une acc�l�ration des enqu�tes concernant les responsables des attentats dont les policiers de base sont les principales victimes. Ils veulent aussi disposer de gilets pare-balles et de pistolets en permanence comme cela est le cas pour les officiers.�
