TITRE: Val-d'Is�re : un bless� grave dans l'incendie d'une r�sidence
DATE: 2014-02-10
URL: http://www.boursorama.com/actualites/val-d-isere--un-blesse-grave-dans-l-incendie-d-une-residence-40822de1cdc41f8748e74f3088c413b1
PRINCIPAL: 160977
TEXT:
Val-d'Is�re : un bless� grave dans l'incendie d'une r�sidence
Le Point le 10/02/2014 � 18:43
Imprimer l'article
Un incendie ravage une r�sidence, le 10 f�vrier 2014, � Val d'Is�re.
Une femme de 63 ans a �t� gravement bless�e en sautant d'un immeuble en feu dans la station de ski de Val-d'Is�re (Savoie), a-t-on appris lundi aupr�s de la pr�fecture. Son pronostic vital n'est pas engag�. Elle s'est d�fenestr�e du 8e ou 9e �tage, faisant une chute d'une dizaine de m�tres. Une autre personne a �t� gravement intoxiqu�e et quatre autres plus l�g�rement. Tous les bless�s ont �t� transport�s � l'h�pital de Bourg-Saint-Maurice. Lundi en fin d'apr�s-midi, les pompiers s'assuraient qu'aucune victime suppl�mentaire n'�tait � d�plorer sur les lieux de l'incendie. "On est sans nouvelles de trois personnes, dont on ne sait pas si elles �taient au 9e �tage ou �vacu�es", a indiqu� �lisabeth Castellotti, sous-pr�f�te d'Albertville. Une cinquantaine de pompiers�lisabeth Castellotti a cependant pr�cis� disposer d'"�l�ments favorables" laissant penser que ces personnes ont �t� �vacu�es. L'incendie, d'origine inconnue, s'est d�clar� au 5e �tage d'un immeuble communal de neuf niveaux, de type chalet en bois, lundi vers 13 heures. Quelque 200 personnes ont �t� �vacu�es vers le centre des congr�s de Val-d'Is�re, dont des r�sidents d'un club de vacances mitoyen. Une �cole primaire situ�e � proximit� a �galement �t� �vacu�e par pr�caution. Une cinquantaine de pompiers ont �t� mobilis�s pour �teindre l'incendie, �quip�s de sept lances � incendie. L'immeuble incendi� abrite habituellement une...
