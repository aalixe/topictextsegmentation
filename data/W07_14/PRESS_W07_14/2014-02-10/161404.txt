TITRE: Nucl�aire: l'Iran fixe ses "lignes rouges" avant Vienne - LExpress.fr
DATE: 2014-02-10
URL: http://www.lexpress.fr/actualites/1/monde/nucleaire-l-iran-fixe-ses-lignes-rouges-avant-vienne_1322162.html
PRINCIPAL: 161401
TEXT:
Zoom moins
Zoom plus
L'Iran "est pr�t" � poursuivre les n�gociations avec les grandes puissances, la semaine prochaine � Vienne, pour conclure un accord global sur son programme nucl�aire controvers�, a affirm� lundi le pr�sident Hassan Rohani.
afp.com
Ces discussions, pr�vues les 18 et 19 f�vrier, promettent d'�tre difficiles pour parvenir � un accord global garantissant la nature exclusivement pacifique du programme nucl�aire iranien. Les Occidentaux et Isra�l soup�onnent, depuis plus d'une d�cennie, ce programme de cacher un volet militaire, malgr� les d�n�gations de T�h�ran.�
Le pr�sident Hassan Rohani a toutefois affirm� que l'Iran �tait "pr�t" � ces n�gociations et "s�rieux" dans sa volont� de "parvenir � un accord global et final".�
Les deux parties ont d�j� conclu en novembre � Gen�ve un accord historique. T�h�ran a stopp� pour six mois certaines activit�s nucl�aires sensibles contre une lev�e partielle des sanctions. L'Iran a notamment cess� d'enrichir l'uranium � 20%, �tape importante vers un niveau militaire (90%).�
En parall�le, l'Iran a accept� dimanche d'aborder la possible dimension militaire de son programme avec l'Agence internationale de l'�nergie atomique (AIEA). T�h�ran va fournir, pour la premi�re fois depuis plusieurs ann�es, des informations sur le d�veloppement de d�tonateurs susceptible d'�tre utilis�s dans la fabrication d'une bombe nucl�aire.�
"Comme lors des pr�c�dentes n�gociations (...) nous ne permettrons pas qu'on aborde les questions de d�fense (qui) constituent notre ligne rouge", a affirm� Abbas Araghchi, vice-ministre des Affaires �trang�res et chef des n�gociateurs nucl�aires iraniens.�
La sous-secr�taire d'Etat Wendy Sherman a r�cemment affirm� que la question du programme balistique de l'Iran devait �tre abord�e par le groupe 5+1 (Etats-Unis, France, Royaume-Uni, Russie, Chine et Allemagne) lors des prochaines discussions. Elle a aussi estim� que l'Iran n'avait pas besoin du r�acteur � eau lourde d'Arak, actuellement en construction, ou du site souterrain d'enrichissement d'uranium de Fordo.�
Le programme balistique iranien inqui�te les pays occidentaux, notamment les missiles d'une port�e de 2.000 km capables d'atteindre Isra�l, et a �t� condamn� par plusieurs r�solutions de l'ONU, assorties de sanctions internationales.�
Pour sa part, Majid Takhte Ravanchi, un autre n�gociateur nucl�aire iranien, a r�p�t� lundi que l'Iran n'acceptera la fermeture d'"aucun de ses sites nucl�aires".�
Mais, pour tenter de "lever les inqui�tudes" occidentales, T�h�ran est pr�t � modifier les plans du r�acteur d'Arak pour y limiter la production du plutonium et � ne pas construire d'usine de retraitement, obligatoire pour purifier le plutonium � un niveau militaire.�
"Manque de confiance"�
Ali Akbar Salehi, le chef de l'Organisation iranienne de l'�nergie atomique (OIEA) a �galement �cart� tout abandon de l'enrichissement d'uranium � 20%. Le guide supr�me, l'ayatollah Ali Khamenei, qui a la haute main sur les n�gociations nucl�aires, "a dit qu'il ne fallait pas abandonner le droit de l'enrichissement � 20% car c'est le droit du pays", a-t-il ajout�. M. Salehi a par ailleurs annonc� la mise au point d'un nouveau type de centrifugeuse "15 fois plus puissante" que celles de premi�re g�n�ration, actuellement en activit�. �
Les prochaines n�gociations seront "difficiles", ont pr�venu les responsables iraniens. Le chef de la diplomatie, Mohammad Javad Zarif, a dit esp�rer que les discussions de Vienne fixent seulement le cadre des futures n�gociations.�
"La plus grande difficult� vient de l'absence de confiance" envers les Etats-Unis, a-t-il expliqu�. Washington a r�cemment �toff� sa liste noire de personnes ou entit�s soup�onn�es de contourner les sanctions contre T�h�ran. �
Pourtant, selon l'analyste bas� � T�h�ran Mohammad Ali Shabani, les avanc�es avec l'AIEA peuvent servir les int�r�ts du 5+1. �
"C'est un bon indicateur du s�rieux de l'Iran dans la recherche d'une solution politique et qu'il est pr�t � r�pondre � toutes les questions en suspens" de l'agence onusienne, a-t-il dit � l'AFP. L'AIEA veut d�terminer si T�h�ran a ou non cherch� � se doter de la bombe atomique avant 2003, voire apr�s.�
Soulignant que le dialogue avec l'AIEA "est d�pendant de celui avec le 5+1, pas l'inverse", M. Shabani a mis en garde contre une "exag�ration de l'importance de l'accord", m�me si celui-ci "permettra au 5+1 de vendre plus facilement les n�gociations � un auditoire sceptique".�
La coop�ration avec l'Agence joue un r�le essentiel dans ces derni�res n�gociations, l'AIEA �tant charg�e de surveiller les mesures pr�vues par l'accord de Gen�ve.�
Par
