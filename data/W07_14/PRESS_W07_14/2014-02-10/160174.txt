TITRE: Petro rebondit � Limoges - Basket - Sports.fr
DATE: 2014-02-10
URL: http://www.sports.fr/basket/pro-a/articles/petro-rebondit-a-limoges-1007964/?sitemap
PRINCIPAL: 0
TEXT:
10 février 2014 � 13h55
Mis à jour le
10 février 2014 � 14h00
Evoqué il y a quelques semaines à Strasbourg pour remplacer Alexis Ajinça, Johan Petro va finalement rejoindre Limoges. Le pivot français, champion d’Europe avec les Bleus en septembre dernier, a confirmé l’information sur son compte Twitter.
Un champion d’Europe s’en va, un autre revient. Quelques semaines après le départ d’Alexis Ajinça aux New Orleans Pelicans, son remplaçant en sélection, Johan Petro, fait son retour en Pro A. Le pivot français (28 ans, 2,13 m), médaillé d’or avec les Bleus en Slovénie au mois de septembre dernier, va s’engager avec Limoges jusqu’à la fin de la saison. Une excellente nouvelle pour le championnat mais aussi et surtout pour le CSP, qui pourra compter sur lui lors de la Leaders Cup, du 14 au 16 février prochains à Disneyland Paris, s’il est qualifié à temps par la LNB.
C’est le joueur, lui-même, qui a annoncé son arrivée à Beaublanc sur son compte Twitter, lundi matin: "Retour en Pro A sous les couleurs du CSP Limoges ! Un pur bonheur !" Son nom avait d’abord circulé début janvier du côté de Strasbourg , où Vincent Collet avait fait de lui une recrue possible pour suppléer Alexis Ajinça. Mais des problèmes administratifs avec son précédent club, en Chine, avaient retardé les négociations et finalement fait capoter l’affaire, la SIG se repliant ensuite sur l’expérimenté intérieur australien David Andersen.
Retour en #ProA sous les couleurs du @CSPLimoges ! Un pur bonheur !
� Johan Petro (@Johan_Petro) 10 Février 2014
A Limoges, Johan Petro aura sûrement à cœur de se montrer aux yeux du sélectionneur de l’équipe de France à quelques mois de la Coupe du monde 2014, cet été en Espagne, après une courte expérience de quelques matches aux Zhejiang Guangsha Lions, où il tournait à 13,5 points et 9,8 rebonds de moyenne avant d’être coupé. Neuf ans après avoir quitté Pau-Orthez et décide de tenter l’aventure aux Etats-Unis, où il s’est un peu perdu (Seattle, Oklahoma City, Denver, New Jersey et Atlanta), l’ex-NBAer a là une belle occasion de rebondir.
