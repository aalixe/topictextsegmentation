TITRE: www.lamontagne.fr - Editorial - ORLEANS (45000) - Plan cancer� de gauche
DATE: 2014-02-10
URL: http://www.lamontagne.fr/france-monde/actualites/a-la-une/editorial/2014/01/31/plan-cancer-de-gauche_1860360.html
PRINCIPAL: 161358
TEXT:
(cliquez sur le code s'il n'est pas lisible)
Tous les �ditos
Une part de v�rit�
Lu 516 fois 2
l'�ditorial  jacques camus  La complexit� de la t�che qui attendait Manuel Valls hier avait �t� suffisamment soulign�e pour qu'on le cr�dite d'une performance tr�s honorable, ponctu�e par une vibrante tirade sur le m�rite r�publicain. Imaginez qu'il lui fallait r�aliser une synth�se...
Contrat de confiance
Lu 881 fois 1
l'�ditorial  jacques camus  Est-ce de cela dont le pays a vraiment besoin pour sortir de la crise et amorcer son redressement ? On veut parler des palabres de ces derniers jours entre Manuel Valls et les repr�sentants des partis de la gauche pour � m�riter � leur...
Semaine de� v�rit�s
Lu 432 fois
l'�ditorial  jacques camus  C'est une semaine de? v�rit�s qui s'ouvre dans le paysage politique fran�ais. Une semaine o� va se jouer la deuxi�me partie du quinquennat de Fran�ois Hollande et son propre destin. Rien que �a ! Apr�s le d�sastre...
La danse de Saint-Guy d�EELV
Lu 699 fois 1
Apr�s avoir men� la vie dure � l'ancien Premier ministre Jean-Marc Ayrault, les Verts du pr�c�dent gouvernement, C�cile Duflot et Pascal Canfin, �taient devenus d'ardents partisans de son maintien � Matignon.   Oubli�es l'infamie du trait� budg�taire...
Boomerangs
Lu 1395 fois 1 1 1 1
Le d�sir d'avenir est plus fort que tout. Apr�s Falorni et la d�faite de La Rochelle, les larmes, l'humiliation du � embrasse-moi sur la bouche � dans la nuit tulliste, S�gol�ne Royal revient, en position de force, dans le nouveau gouvernement pour apporter son soutien � Fran�ois...
Dette� de confiance
Lu 422 fois 1
l'�ditorial  jacques camus  Il se dit que, d�s l'annonce de sa nomination au poste �largi de ministre de l'�conomie, du Redressement productif et du Num�rique, Arnaud Montebourg avait voulu investir le 6 e �tage de Bercy, r�put� �tre la passerelle de commandement. Futile, sans doute,...
Remue-m�nage(s)
Lu 1357 fois
l'�ditorial  jacques camus  Manuel Valls a �trenn� m�diatiquement, hier soir, ses habits de Premier ministre sur le plateau du JT de TF1. Il s'y est montr� d�cid� et �nergique, mais sans la moindre asp�rit�, tr�s respectueux du verbe pr�sidentiel dont,...
Passation sans effusion
Lu 514 fois 2
l'�ditorial  jacques camus  Entre Jean-Marc Ayrault et Manuel Valls, ce fut une passation sans effusion. Pas de chaleureuse �treinte entre les deux hommes, mais juste ce qu'il fallait de courtoisie r�publicaine. C'�tait beaucoup mieux comme cela, car personne n'aurait cru � ces...
