TITRE: USA-Les valeurs � suivre � Wall Street (actualis� - LExpress.fr
DATE: 2014-02-10
URL: http://lexpansion.lexpress.fr/high-tech/usa-les-valeurs-a-suivre-a-wall-street_428138.html
PRINCIPAL: 159901
TEXT:
USA-Les valeurs � suivre � Wall Street (actualis�
Par Reuters, publi� le
, mis � jour � 14:14
(Actualis� avec McDonald's, cours de Yelp et Hasbro)
* Pour l'agenda des r�sultats, double-cliquer sur�
* Pour les changements de recommandations, cliquer sur�
NEW YORK, 10 f�vrier (Reuters) - Principales valeurs � suivre lundi � Wall Street :�
* YAHOO a conclu un partenariat avec Yelp, site de conseils aux consommateurs, pour enrichir les r�sultats de son moteur de recherche, a rapport� samedi le Wall Street Journal. Yelp prend 8,5% en avant Bourse. (voir: )�
* HASBRO. Le fabricant de jouets a publi� des b�n�fices en baisse et inf�rieurs aux attentes pour le trimestre de No�l, avec un b�n�fice net de 129,8 millions de dollars et un chiffre d'affaires de 1,28 milliard. Hors exceptionnels, le b�n�fice par action ressort � 1,12 dollar, dix cents de moins que le consensus �tabli par Thomson Reuters I/B/E/S. Le titre est en baisse de 2,2% en avant Bourse�
* BOEING. L'avionneur a maintenu lundi, � l'occasion du Salon a�ronautique de Singapour, sa pr�vision d'une demande mondiale d'avions totalisant 4.800 milliards de dollars sur les 20 prochaines ann�es, dont la moiti� concernera la r�gion Asie-Pacifique. La flotte cumul�e de la r�gion devrait passer de 5.090 appareils en 2012 � 14.750 en 2032.�
* AOL - Le directeur g�n�ral du groupe est revenu sur sa d�cision de baisser le niveau des retraites de ses employ�s et s'est excus� pour les remarques qu'il a faites associant le cas de deux femmes salari�es de la soci�t�, ayant des "b�b�s souffrants", � l'augmentation des d�penses de sant� du groupe, qui ont d�clench� une avalanche de critiques.�
* BEBE STORES. Le sp�cialiste du pr�t-�-porter f�minin a mandat� Guggenheim Securities pour explorer l'option d'une vente pure et simple de l'entreprise, de pr�f�rence � une soci�t� de capital-investissement, ont dit des sources proches du dossier � Reuters. Le titre avait bondi de 14% en Bourse vendredi apr�s l'annonce par le groupe d'une baisse moins forte que pr�vu de ses ventes � magasins comparables lors du trimestre �coul�.�
* MCDONALD'S. Les ventes de la cha�ne de restauration rapide ont augment� de 1,2% en janvier � p�rim�tre comparable, la solidit� de la r�gion Europe ayant compens� un d�clin aux Etats-Unis. McDonald's a par ailleurs annonc� lundi l'ouverture de son premier restaurant au Vi�tnam, � H�-Chi-Minh-Ville.�
* AMERICAN EXPRESS. Morgan Stanley a relev� sa recommandation sur le groupe financier, passant de "pond�ration en ligne" � "surpond�rer", selon le site theflyonthewall.com. (Service fran�ais)�
Par
