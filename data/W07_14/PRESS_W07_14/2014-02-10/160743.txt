TITRE: 20 Minutes Online - Casey Stoney r�v�le son homosexualit� - Football
DATE: 2014-02-10
URL: http://www.20min.ch/ro/sports/football/story/Casey-Stoney-r-v-le-son-homosexualit--25678361
PRINCIPAL: 160739
TEXT:
Casey Stoney (avec le brassard de capitaine), lors du Mondial 2011 en Allemagne. (photo: Keystone)
10 f�vrier 2014 15:02
Football
Casey Stoney r�v�le son homosexualit�
La capitaine de l'�quipe d'Angleterre de football f�minin a r�v�l� son homosexualit�, lundi sur la BBC.
�Je vivais dans le mensonge, a comment� la d�fenseur de 31 ans d'Arsenal, interrog�e par la radio anglaise. Je ne l'ai jamais cach� dans mon cercle de football car c'est accept� mais � l'ext�rieur je n'ai jamais parl� de ma sexualit�. C'est tr�s important pour moi de parler maintenant en tant que joueuse homosexuelle car il y a tant de gens qui le sont qui doivent se battre. Vous entendez parlez de gens qui se suicident � cause de �a, cela ne devrait jamais arriver�.
Stoney, capitaine de l'Angleterre lors des JO 2012, compte 116 s�lections. Elle a expliqu� qu'elle avait �t� incit�e apr�s le coming out du plongeur anglais Tom Daley � l'issue de ces Jeux olympiques � Londres.
Elle a �galement jug� �invraisemblable� d'avoir attribu� des Coupes du monde � la Russie et au Qatar en raison de l'attitude de ces pays envers les homosexuels.
�Je n'irais pas l�-bas car je sais que je ne serais pas accept�e. Il y a des athl�tes gays qui concourent actuellement en Russie pour les JO. Je n'imagine pas � quel point ils doivent �tre effray�s�, a-t-elle poursuivi au sujet des JO de Sotchi.
(afp)
