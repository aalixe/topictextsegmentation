TITRE: Municipales: Un nombre important de villes pourrait �basculer� � droite �par rejet� du gouvernement  - 20minutes.fr
DATE: 2014-02-10
URL: http://www.20minutes.fr/societe/1293962-20140209-municipales-nombre-important-villes-pourrait-basculer-a-droite-par-rejet-gouvernement
PRINCIPAL: 158201
TEXT:
Patrick Devedjian, lors de la s�ance des questions au gouvernement, le 22 octobre 2013 LCHAM/SIPA
POLITIQUE - D'apr�s l'UMP Patrick Devedjian...
L'UMP Patrick  Devedjian, d�put� et pr�sident du  conseil g�n�ral des Hauts-de-Seine, a  estim� dimanche qu'un nombre �important� de villes pourrait basculer � droite aux municipales �non  pas � cause des qualit�s fabuleuses de l'UMP  mais par rejet de la  politique gouvernementale�.
�L'�lectorat de droite est tr�s insatisfait de ses partis  politiques  mais il est encore plus insatisfait du gouvernement, beaucoup  plus  remont� contre�, a d�clar� M. Devedjian sur Radio J, consid�rant �toutes les conditions r�unies pour qu'il y ait une abstention   diff�renci�e lors de ces �lections, que l'�lectorat de gauche se   mobilise moins que l'�lectorat de droite tr�s m�content de la politique   gouvernementale�.
A l'appui de son analyse, l'�lu des Hauts-de-Seine a donn� trois  arguments: �Aux derni�res �lections municipales, la gauche a fait  un  vrai succ�s, elle a fait le plein (...) donc il y a un reflux  naturel�, �les �lections interm�diaires locales sont les �lections  habituelles  de r�action contre la politique gouvernementale et le  pouvoir central�, �la politique socialiste ne convainc pas�.
Interrog� sur les difficult�s de la candidate de l'UMP � Paris,  Nathalie Kociuscko-Morizet, il a r�pondu qu'une victoire de la  droite  dans la capitale constituait �un d�fi difficile� car �il faut  qu'elle  fasse 53%� mais aussi car �la structure en arrondissements  d�veloppe un  syst�me un peu clanique (...) en m�me temps les �lecteurs  en ont assez  des dissidences�.
Et Patrick Devedjian d'affirmer encore que �la mayonnaise n'est  pas  encore constitu�e, le corps �lectoral dans son ensemble n'a pas  encore  bien pris conscience de l'�lection et de ses enjeux�.
A Marseille, �la droite va gagner aussi parce que la gauche  est  d�mobilis�e et parce qu'il y a une sanction nationale dans les  grandes  m�tropoles�, selon lui.
Quant � Levallois-Perret, le pr�sident du conseil g�n�ral des   Hauts-de-Seine s'est born� � souhaiter �que la ville reste � droite�,   se refusant � dire s'il souhaitait la victoire du dissident face au   sortant UMP Patrick Balkany et assurant ne tirer �jamais contre son   camp�.
Le parquet de Nanterre a ouvert mi-janvier une information   judiciaire contre le maire, soup�onn� d'avoir utilis� un chauffeur de la   mairie � des fins personnelles. Le couple Balkany est aussi vis� par   une enqu�te pr�liminaire � Paris pour �blanchiment de fraude fiscale�.
Avec AFP
