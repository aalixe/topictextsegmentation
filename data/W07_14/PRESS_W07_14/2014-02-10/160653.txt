TITRE: Mantes-la-Jolie. Des policiers caillass�s pendant le tournage d'un clip
DATE: 2014-02-10
URL: http://www.ouest-france.fr/mantes-la-jolie-des-policiers-caillasses-pendant-le-tournage-dun-clip-1921618
PRINCIPAL: 0
TEXT:
Mantes-la-Jolie. Des policiers caillass�s pendant le tournage d'un clip
Yvelines -
Une quinzaine de policiers ont �t� pris � partie dimanche apr�s-midi par une cinquantaine de jeunes qui tournaient un clip de rap.
Les faits se sont produits dans le quartier du Val-Fourr�, � Mantes-la-Jolie (Yvelines).
Les policiers, intervenus pour un diff�rend entre deux automobilistes qui se disputaient une place de stationnement, ont �t� pris � partie "par une cinquantaine de jeunes hostiles", qui tournaient un clip de rap non loin de l�, selon une source polici�re.
Trois arrestations
Caillaiss�s, les policiers ont r�pliqu� par des tirs de flash-ball et de gaz lacrymog�ne.
Aucun policier n'a �t� bless� et trois mineurs, �g�s de 14 � 17 ans, ont �t� interpell�s et plac�s en garde � vue au commissariat de Mantes-la-Jolie.
Tags :
