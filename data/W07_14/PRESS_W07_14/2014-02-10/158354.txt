TITRE: JO/Patinage de vitesse: Fauconnet, un franc-parler et une ambition - Flash actualit� - Sports - 09/02/2014 - leParisien.fr
DATE: 2014-02-10
URL: http://www.leparisien.fr/flash-actualite-sports/jo-patinage-de-vitesse-fauconnet-un-franc-parler-et-une-ambition-09-02-2014-3574797.php
PRINCIPAL: 158352
TEXT:
R�agir
Thibaut Fauconnet a un certain franc-parler et un petit grain de folie: il se d�finit lui-m�me comme "un super h�ros" capable d'offrir au short-track fran�ais une premi�re m�daille historique � Sotchi � partir de lundi.
"On m�rite d'�tre connus, on n'est pas juste des mecs qui tournent en rond dans une patinoire", explique Fauconnet qui s'alignera lundi sur 1.500 m, la premi�re des trois distances individuelles (avec le 500 m et le 1000 m).
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
S'il r�ussit � monter sur un podium � Sotchi, il deviendra alors le premier Fran�ais de l'histoire � �tre m�daill� en short-track, cette variante courte du patinage de vitesse domin�e par les Sud-Cor�ens, Chinois et Canadiens.
Le short-track reste tr�s confidentiel et cela n'est gu�re pour plaire � l'ambassadeur fran�ais � l'aise au milieu de tous ces patineurs qui savent "d�conner".
"On ne se prend pas au s�rieux quand il faut, notre vie est super dr�le", lance le Dijonnais avec son d�bit tr�s rapide.
Fauconnet, un grand barbu aux cheveux �bouriff�s de 28 ans, compose une fine �quipe avec ses partenaires tricolores.
"On partage beaucoup et c'est pas triste ! Faut arriver � rendre tout ce qu'on fait plus humain et moins important parce que ce sont des enjeux super importants. L'autod�rision �a aide beaucoup", souligne le pensionnaire de Font-Romeu depuis 4 ans.
Le parc � grenouilles
Et de citer en exemple leur derni�re trouvaille: le parc � grenouilles.
"On a ramass� des t�tards, on les a fait grandir et on leur a fait un parc. L� ils sont morts, ils ont pris un coup de chaud cet �t� ! On n'a plus les grenouilles mais on a toujours le parc!"
D�s lundi, Fauconnet sera s�rieux sur la glace avec le 1.500 m. L'octuple champion d'Europe dont un in�dit grand chelem lors de l' Euro -2011, est mont� � deux reprises sur un podium de Coupe du monde sur 1.500 m et 1.000 m.
Le 1.000 m aura lieu le 15 f�vrier et le 21 f�vrier, ce sera le 500 m, la distance qu'il pr�f�re, m�me si cette saison il n'a pas �t� tr�s convaincant.
Peut-�tre pourra-t-il compter sur le facteur chance du short-track � qui on pr�te � un caract�re al�atoire.
"Non ! Et celui qui me redit �a, je lui donne mes patins, il va sur la glace � ma place et alors il verra si c'est al�atoire !"
"Je d�teste ce mot al�atoire parce que �a voudrait dire qu'on s'entra�ne pour rien finalement. Moi je gagne parce que je suis un super h�ros ! J'ai la combinaison mais j'ai pas la cape."
