TITRE: Chelsea | Chelsea : Quand Mourinho ironise sur le dipl�me d'ing�nieur de Pellegrini...
DATE: 2014-02-10
URL: http://www.le10sport.com/football/angleterre/chelsea/chelsea-quand-mourinho-ironise-sur-le-diplome-d-entraineur-de-pellegrini135571
PRINCIPAL: 161611
TEXT:
�
��JE NE PENSE PAS QU�UN ING�NIEUR AIT BESOIN D�UNE CALCULETTE��
��Je ne pense pas qu�un ing�nieur ait besoin d�une calculette pour savoir que Mata vendu 44,5 millions d�euros et de Bruyne 21,5 millions d�euros, cela fait 66 millions de recettes. Et Matic achet� 25 millions d�euros et Salah 13 millions d�euros, cela fait 38 millions de d�penses. 66-38=28, donc Chelsea a g�n�r� 28 millions d�euros cet hiver. Nous on construit une �quipe pour la d�cennie et eux ils ont une �quipe pour gagner maintenant ou dans les 3-4 ans � venir��, a indiqu� Jos� Mourinho, le coach de Chelsea, dans des propos rapport�s par The Guardian .
�
��UN TR�S RICHE PETIT CHEVAL��
��Peut-�tre qu�il pense que son �quipe est un petit cheval. C�est peut-�tre un petit cheval, mais alors c�est un tr�s riche petit cheval. Chelsea est l��quipe qui a d�pens� le plus d�argent ces 10 derni�res ann�es. C�est l��quipe qui a d�pens� le plus d�argent cette ann�e, et l��quipe qui a d�pens� le plus lors du mercato d�hiver. Donc petit peut-�tre, mais riche. Je pense que Mourinho est favori. Il pense que s'il gagne le m�rite sera pour lui. Mais s'il perd, il ne sera pas responsable. C'est sa fa�on de faire. Chacun est libre de faire comme il l'entend pour le bien de son �quipe �, avait d�clar� Manuel Pellegrini, l�entra�neur de Manchester City, en conf�rence de presse.
