TITRE: Baptis�e b�b� fant�me, Hope survit sans sang - Belgique Magazine
DATE: 2014-02-10
URL: http://www.belgiquemag.com/baptisee-bebe-fantome-hope-survit-sans-sang-265-2014/
PRINCIPAL: 160966
TEXT:
Accueil � actualites � Baptis�e b�b� fant�me, Hope survit sans sang
Baptis�e b�b� fant�me, Hope survit sans sang
Baptis�e b�b� fant�me, Hope survit sans sang
10 f�vrier 2014 dans actualites , Faits Divers Laisser un commentaire
Hope Suarez, est une petite fille ��miracle��. Elle est n�e sans sang et a pu survivre � une an�mie f�tale chronique. Selon les informations des m�decins, Hope souffrait d�une maladie tr�s rare. A sa naissance, elle a perdu plus de 80 % de son sang et malgr� cela, elle a pu survivre.
N�e dans un h�pital � Irvine, en Californie, les m�decins et les parents de la petite ont �t� surpris de voir le teint si p�le du nouveau n�. Apr�s avoir proc�d� � un pr�l�vement sanguin, les m�decins ont �t� choqu�s de voir que le b�b� n�avait que quelques gouttes de sang. Selon les d�clarations du p�re de Hope � la chaine ABC7�: ��Je savais que quelque chose n�allait pas surtout lorsqu�ils ont essay� de piquer ses pieds pour faire des tests. Aucune goutte de sang ne sortait��. Aujourd�hui, �g�e de 6 semaines, Hope r�cup�re au grand bonheur de ses parents apr�s avoir souffert d�une h�morragie foeto-maternelle.
Selon les d�clarations de la maman de Hope, ne sentant plus son b�b� bouger dans son ventre, elle a d�cid� d�aller voir son m�decin. Elle a eu raison de le faire car elle a �t� imm�diatement dirig�e vers le bloc op�ratoire pour subir une c�sarienne d�urgence, selon la m�me source. Hope n�est pas le seul b�b� n� avec cette anomalie. Une autre petite fille, pr�sentant les m�mes sympt�mes, avait �t� signal�e en 2012 au Royaume-Uni.
2014-02-10
