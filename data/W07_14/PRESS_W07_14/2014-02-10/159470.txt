TITRE: Shia LaBeouf : la t�te dans le sac - Closermag.fr
DATE: 2014-02-10
URL: http://www.closermag.fr/people/people-anglo-saxons/nymphomaniac-shia-labeouf-pris-la-tete-dans-le-sac-!-photos-272151
PRINCIPAL: 159467
TEXT:
Suivant
Taille du texte : � � �
Shia LaBeouf a jou� la carte de la provocation hier soir au festival de Berlin. Le com�dien portait un sac en papier sur la t�te sur le tapis rouge !
Si les stars rivalisent d'originalit� sur le red carpet, Shia LaBeouf a mis tout le monde d'accord hier soir. A l'occasion de la projection de son dernier film Nymphomaniac au festival du film de Berlin, l'acteur est en effet apparu... avec un sac en papier sur la t�te ! Shia LaBeouf portait un smoking noir mais avait mis un sac en papier craft sur sa t�te sur lequel �tait �crit au marqueur noir "I am not famous anymore", traduisez : "Je ne suis plus c�l�bre". �Le com�dien est tout de m�me parvenu � se diriger gr�ce aux deux trous qu�il avait perc�s au niveau des yeux.
Selon le Hollywood Reporter, Shia LaBeouf �tait le premier com�dien du film � arriver sur le tapis rouge et il �tait d�j� dans la salle quand ses coll�gues sont apparus. Quelques heures plus t�t, il avait fui la conf�rence de presse du film apr�s une �ni�me question sur les sc�nes de sexe pr�sentes dans le film de Lars Von Trier, dans lequel joue �galement Charlotte Gainsbourg . En partant, il s�est permis de citer Eric Cantona .�"Quand les mouettes suivent un chalutier, c�est parce qu�elles pensent que des sardines vont en tomber. Merci beaucoup", a-t-il �nigmatiquement d�clar�. C�est en 1995 que le footballeur fran�ais avait lanc� cette phrase lors d�une conf�rence de presse.
Entre autres controverses r�centes,�Shia LaBeouf a r�cemment �t� accus� de plagiat. On lui a r�cemment reproch� d��tranges similitudes entre la bande dessin�e Justin M. Damiano de Daniel Clowes et son court m�trage HowardCantour.com.
