TITRE: SFR Carr� Europe : le forfait avec roaming le plus rentable, mais en s�rie limit�e
DATE: 2014-02-10
URL: http://www.clubic.com/telephone-portable/operateur-telephonie-mobile/sfr/actualite-617958-sfr-carre-europe-roaming-le-plus-rentable.html
PRINCIPAL: 160109
TEXT:
SFR Carr� Europe : le forfait avec roaming le plus rentable, mais en s�rie limit�e
Partager cette actu
Publi�e                 par Romain Heuillard le Lundi 10 Fevrier 2014
C'est finalement au tour de SFR de faire une annonce en mati�re de roaming. L'op�rateur lance un forfait Carr� Europe, le plus int�ressant du march�, mais propos� sous forme de s�rie limit�e.
Mise � jour du 25/03/2014 : L'�dition sp�ciale Carr� Europe a disparu au profit de l'int�gration du roaming � certains forfaits de la gamme
Trois semaines apr�s que Free a d�clench� la bataille du roaming, SFR r�agit. Contrairement � ses concurrents op�rateurs de r�seaux (par opposition aux op�rateurs virtuels), SFR n'a pas choisi d'int�grer plus ou moins de roaming � des forfaits existants. Il a plut�t lanc� un forfait d�di�.
Comme ceux d' Orange , de Bouygues Telecom et dans une moindre mesure de Free , le forfait Carr� Europe peut �tre utilis� indiff�remment depuis la France et depuis l'Europe et les DOM : les prestations incluses sont utilisables sans surco�t. Mais l'offre de SFR se distingue par l'absence de restriction de dur�e, le forfait peut-�tre utilis� depuis l'�tranger 365 jours/an, et pas seulement 7 � 60 jours, elle est donc adapt�e aux frontaliers et aux grands voyageurs.
Le forfait inclut les appels, SMS et MMS en illimit� vers et depuis la France, l'Europe et les DOM, 3 Go d'Internet en 4G rechargeable, un extra au choix1, une seconde carte SIM (MultiSurf) et 100 Go d'espace de stockage en ligne.
Limit� � 10�000 souscriptions, la s�rie limit�e Carr�e Europe de SFR sera disponible � partir du 11 f�vrier 2014 et sera factur�e 60 euros/mois sans mobile et avec un engagement de 12 mois. Elle offre donc une alternative int�ressante aux forfaits EuroPass de Transatel , les plus comparables.
Aller plus loin
