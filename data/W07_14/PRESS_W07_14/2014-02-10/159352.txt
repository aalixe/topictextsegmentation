TITRE: Emeutes en Bosnie: le d�but d'un "printemps des Balkans"? - BFMTV.com
DATE: 2014-02-10
URL: http://www.bfmtv.com/international/emeutes-bosnie-un-printemps-balkans-706750.html
PRINCIPAL: 0
TEXT:
r�agir
L'�conomie ne repart pas, et la population s'enfonce dans la pauvret�. Deux partis politiques membres de la coalition au pouvoir en Bosnie ont appel� dimanche � la tenue d'�lections l�gislatives anticip�es pour calmer la gronde populaire qui s'�tait traduite par des manifestations contre le pouvoir et la pauvret� d'une violence sans pr�c�dent depuis la guerre des ann�es 1990.
�� �
"Il est imp�ratif d'arr�ter les violences, de r�tablir la s�curit� pour les citoyens et d'organiser rapidement des �lections anticip�es", lit-on dans un communiqu� du Parti social-d�mocrate (SDP).
�� �
Des �lections l�gislatives sont normalement pr�vues en octobre.
Manifestations pour un changement du pouvoir
Le membre musulman de la pr�sidence tripartite de Bosnie et leader du  principal parti politique musulman (SDA), Bakir Izetbegovic, a �galement  appel� � la tenue d'�lections anticip�es. "Le peuple veut un changement du pouvoir", a affirm� Bakir Izetbegovic.
�� �
"Je pense qu'il faudrait avoir les �lections dans trois mois pour  offrir aux gens la possibilit� d'�lire ceux auxquels ils font  confiance", a ajout� Bakir Izetbegovic.
�� �
Dans la journ�e � Sarajevo, plusieurs centaines de personnes ont manifest� sans incident  devant l'immeuble, incendi� vendredi, de la pr�sidence du pays, pour  r�clamer la d�mission des autorit�s. "Nous r�clamons le  changement du pouvoir � tous les niveaux. Nous avons faim, �a suffit! Le  peuple n'a rien � manger. C'est une honte. Nous sommes une bombe qui va  exploser!", s'est exclam�e un femme d'une cinquantaine d'ann�e, Belkisa  Kovacevic, au ch�mage.
A lire aussi
