TITRE: TOUT SEUL � Fran�ois Hollande chamboule le plan de table de la Maison Blanche | Big Browser
DATE: 2014-02-10
URL: http://bigbrowser.blog.lemonde.fr/2014/02/10/tout-seul-francois-hollande-chamboule-le-plan-de-table-de-la-maison-blanche/
PRINCIPAL: 0
TEXT:
10 f�vrier 2014
TOUT SEUL � Fran�ois Hollande chamboule le plan de table de la Maison Blanche
Les 300 invitations marqu�es du sceau pr�sidentiel en or �taient pr�tes � �tre post�es. Mais, pour qu'il ne soit pas fait mention de�Val�rie Trierweiler,�les organisateurs du "state dinner" qui se tiendra mardi 11 f�vrier au soir � la Maison Blanche ont d� les r�imprimer.
Le New York Times� raconte �que les organisateurs de ce�tr�s rare et protocolaire�gala qui cl�t les visites d'Etat n�ont pas l�habitude de recevoir des chefs d�Etat c�libataires. Et surtout pas de l�apprendre si peu de temps avant.
Sera t-il accompagn� de "l'autre femme" ? Qui va-t-on placer � c�t� de lui ? En clarifiant sa situation sentimentale quelques jours seulement avant sa visite aux Etats-Unis, Fran�ois Hollande a oblig� les organisateurs de ce d�ner o� tout est pr�vu au millim�tre � improviser.
De quoi achever de les stresser alors qu�ils sont d�j� sur les dents � l�id�e de cuisiner pour le pr�sident du pays r�put� pour ses standards gastronomiques.
Une presque tradition fran�aise
Fran�ois Hollande n'est pas le premier pr�sident fran�ais � venir seul aux Etats-Unis. En 2007, Nicolas Sarkozy, avait surpris le couple Bush en annon�ant son divorce, � peine trois semaines avant son arriv�e et le d�ner organis� � la Maison Blanche. Mais ce voyage n��tait pas class� comme une visite d'Etat.
Tout comme le d�ner cens� d�voiler le prestige des Etats-Unis, la visite d�Etat est en effet un honneur rare r�serv� � quelques chefs d�Etat. Depuis 2008, Barack Obama en avait organis� seulement six. Fran�ois Hollande est le premier pr�sident fran�ais � �tre accueilli de cette fa�on aux Etats-Unis depuis Jacques Chirac, en 1996.
D�s le d�but de cette rencontre tr�s protocolaire, le pr�sident fran�ais aura droit au tapis rouge et � la haie d�honneur. Apr�s un passage en Virginie dans l�ancienne maison de Thomas Jefferson � le r�volutionnaire am�ricain le plus francophile �, Fran�ois Hollande sera accueilli par 21 coups de canon � la Maison Blanche mardi. Le fameux d�ner cl�turera cette journ�e d�entretiens entre les deux pr�sidents.
Pas de th� pour Michelle
Les �pouses des pr�sidents ne participent pas aux r�unions mais ont pour habitude de visiter une �cole de Washington, avant de boire un caf� ou un th�. Ces passages oblig�s ont d�j� �t� annul�s.
Les organisateurs du d�ner, qui se tiendra non pas dans la salle de d�ner officielle de la Maison Blanche mais dans une tente de "style pavillon" permettant de recevoir plus de convives, ont �t� confront�s � d�autres graves questions. Faut-il maintenir les divertissements pr�vus ? Dans ce cas, avec qui Fran�ois Hollande va-t-il danser ? Walter Scheib, l�ancien chef de la Maison Blanche imagine d�j� les commentaires : "Si Fran�ois Hollande demande � Michelle Obama de danser avec lui, �a va faire la 'une' de tous les tablo�ds � 'Un Fran�ais renverse la premi�re dame !'"
La pr�sence en solo de Fran�ois Hollande fait quand m�me beaucoup moins de vagues que celles qu'avait suscit�es la derni�re visite d'Etat. En septembre,�apr�s avoir appris qu'elle �tait espionn�e par la NSA, la pr�sidente du Br�sil, �Dilma Roussef, avait d�clin� l'invitation de Barack Obama.
