TITRE: Sam, premier coming-out en NFL - Sports US - Sports.fr
DATE: 2014-02-10
URL: http://www.sports.fr/sports-us/nfl/articles/michael-sam-fait-son-coming-out-avant-d-arriver-en-nfl-1007827/?sitemap
PRINCIPAL: 0
TEXT:
Michael Sam avait révélé son homosexualité à ses partenaires en pré-saison, l'an passé. (Reuters)
Par François Kulawik
10 février 2014 � 10h02
Mis à jour le
10 février 2014 � 16h42
Moins d’un an après Jason Collins en NBA, Michael Sam est devenu le premier joueur NFL à faire son coming-out aux Etats-Unis.
Enfin presque. Car si le jeune défenseur a révélé son homosexualité dans des interviews accordées au New York Times et à Espn, Michael Sam n’est pas encore un joueur de NFL. Ce qui ne devrait pas tarder puisque celui qui a fait ses classes à l’Université du Missouri devrait être sélectionné lors de la prochaine draft début mai.
"Je ne savais pas combien de personnes étaient au courant de mon homosexualité et je voulais être certain de raconter mon histoire de la manière dont je le souhaitais, a expliqué Sam, je ne suis pas naïf. Je sais l'importance de cette déclaration et personne ne devait raconter mon histoire à part moi. Mais, le plus important, ça reste de m'entraîner pour les tests en vue de la prochaine draft et de jouer en NFL."
Jusque-là attendu dans les premiers tours de la draft 2014, Michael Sam a été élu first-team all-American et défenseur de l’année pour la Southeastern Conference, en plus d’être désigné MVP de son équipe. Mais son coming-out pourrait jouer en sa défaveur. "Je sais juste que cette histoire va le pénaliser, a en effet assuré un scout NFL à SI.com, je n’ai pas de doutes là-dessus. C’est la nature humaine, car les équipes ne vont pas vouloir être celle qui casse cette barrière."
Son initiative a pourtant été saluée par le monde de la NFL, la Ligue américaine publiant d’ailleurs un communiqué pour lui rendre hommage. "Nous admirons l’honnêteté et le courage de Michael Sam", a-t-elle ainsi fait savoir.
