TITRE: Un an apr�s, des "r�sultats concrets" contre les d�serts m�dicaux, selon Marisol Touraine. Info - Valenciennes.maville.com
DATE: 2014-02-10
URL: http://www.valenciennes.maville.com/actu/actudet_-un-an-apres-des-resultats-concrets-contre-les-deserts-medicaux-selon-marisol-touraine_fil-2488732_actu.Htm
PRINCIPAL: 161149
TEXT:
Twitter
Google +
La ministre de la Sant� Marisol Touraine a salu� lundi les "r�sultats concrets", � "poursuivre et amplifier", obtenus, selon elle, dans la lutte contre les d�serts m�dicaux, en pr�sentant le bilan de son action en ce domaine, lanc�e voici un an, lors d'un d�placement � Chalon-sur-Sa�ne (Sa�ne-et-loire).
"La transformation profonde de notre syst�me (de sant�) est engag�e (...) Les moyens qui ont �t� mis en place permettent d'aboutir � des r�sultats concrets : les m�decins s'installent plus nombreux dans des territoires fragiles", a d�clar� la ministre � Chalon-sur-Sa�ne, o� elle a inaugur� un h�licopt�re de secours au centre hospitalier.
"Tout cela, ce sont des changement de vie concrets qu'il s'agit de poursuivre et d'amplifier", a-t-elle ajout�.
Mme Touraine a notamment indiqu� que le gouvernement allait diminuer de moiti� fin 2014 le nombre de Fran�ais �loign�s de plus de 30 minutes d'un acc�s � des soins urgents, de 2 millions de personnes en 2012 � "moins d'un million � la fin de cette ann�e".
Ainsi 650 m�decins correspondants du SAMU, form�s et �quip�s pour intervenir "en avant-poste" des services de secours devraient �tre install�s en 2014, contre 150 en 2012.
La possibilit� d'accueillir en urgence les patients en moins de 30 minutes �tait une promesse de campagne du candidat Fran�ois Hollande, qui a r�affirm� cet engagement � plusieurs reprises apr�s son �lection � la pr�sidence de la R�publique, au nom du "droit fondamental" que constitue l'acc�s � la sant�.
La ministre a aussi rappel� que 200 postes de praticiens territoriaux de m�decine g�n�rale (PTMG) avaient �t� cr��s en 2013: 180 ont �t� sign�s et "les 20 derniers contrats au titre de 2013 sont en voie de finalisation", a-t-elle d�clar�. 200 suppl�mentaires seront mis en place en 2014.
La mise en place de ces PTMG vise notamment � garantir aux jeunes m�decins qui acceptent de s'installer dans les territoires manquant de praticiens un revenu net mensuel de 3.640 euros, gr�ce � des compl�ments de r�mun�ration.
"Sans doute y a-t-il eu des praticiens qui allaient s'installer et ont b�n�fici�" du dispositif, a reconnu Marisol Touraine. "Mais c'est une fa�on de les ancrer dans le paysage", a-t-elle aussi soulign�.
"Les trois r�gions o� il y a le plus de PTMG sont Rh�ne-Alpes (33), Centre (13) et Bourgogne (12)", a-t-elle pr�cis� � la presse, admettant qu'"il y a des r�gions qui ont plus de mal que d'autres".
"Il y a des r�gions qu'il faudra booster un peu. La r�gion Nord-pas-de-Calais n'y est pas" et "n'a pas suffisamment embray�", puisqu'aucun contrat n'y a �t� sign�, a-t-elle indiqu�.
Elle a �voqu� "des raisons techniques", �voquant le "d�marchage" r�alis� par la r�gion Rh�ne-Alpes, mais aussi des raisons "plus sociologiques", citant le manque de stages pour les �tudiants en m�decine dans certaines facult�s.
AFP�� AFP��
