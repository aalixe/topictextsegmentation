TITRE: Intemp�ries. D�crue amorc�e mais les inondations ont laiss� des traces
DATE: 2014-02-10
URL: http://www.ouest-france.fr/intemperies-direct-le-temps-du-nettoyage-1921450
PRINCIPAL: 159604
TEXT:
Intemp�ries. D�crue amorc�e mais les inondations ont laiss� des traces
France -
Retour sur les �v�nements de la journ�e de lundi en Bretagne, sur le front des inondations.
20 h 15. La vid�o des inondations � Planco�t
Notre photographe David Ad�mas a pass� une partie du week-end � saisir les inondations de Planco�t. Il en a aussi ramen� une vid�o.
19 h 25. A Auray, un mur menace de s'effondrer aux Rampes du Loch �
La municipalit� a choisi de faire fermer le chemin qui surplombe les rampes du Loch. Un mur menace de s�effondrer.
Le mur, � droite sur la photo, menace de s'effondrer.�|�Ouest-France
18 h 45.�Le long travail de nettoyage des pompiers
Ce lundi, deux �quipes d�une vingtaine de sapeurs-pompiers ont sillonn� le centre-ville de Quimperl�. Au programme : nettoyage et pompages.
17 h 30. Op�ration nettoyage � Olonne-sur-Mer
A Olonne-sur-Mer, quarante agents municipaux ont �t� mobilis�s ce matin pour nettoyer les 8 km de c�te sauvage.
17 h 20. Le ministre de l'Ecologie attendu en Bretagne
Philippe Martin, ministre de l'Ecologie, va visiter jusqu'� mercredi les r�gions touch�es par les intemp�ries et les inondations.
Ce lundi soir, il sera en Bretagne : d'abord dans le Morbihan (� Gudiel et Languidic) puis � Redon (Ille-et-Vilaine).�
17 h 10. Nouvelle vid�o des inondations � Redon
16 h 55. La falaise s'�croule dans le centre nautique � Douarnenez
Un �boulement a mis � mal le hangar technique du centre nautique municipal de Douarnenez, dont un mur a �t� perfor� par les gravats. Plus de d�tails.
10 m3 de roche granitique friable se sont �croul�s dans le hangar du centre nautique municipal. La chaufferie a �t� �pargn�e.�|�Ouest-France
16 h 35.�Gros d�g�ts annonc�s apr�s 3 jours d�inondations � Planco�t
La ville est rest�e sous plus d�1 m d�eau pendant tout le week-end. L'eau devrait se retirer des quais demain.�
Les habitants �vacu�s d�une centaine de logements ne pourront pas regagner leur habitation avant demain pour �valuer les d�g�ts.� Plus de d�tails. ��
Ici dans certain logements et commerces du centre ville l�eau est mont�e jusqu�� un m�tre.�|�DAVID ADEMAS
16 h 15. Rieux les pieds dans l'eau
Le niveau a mont� depuis dimanche � Rieux, mais il a globalement baiss� depuis samedi. A Rieux la zone d'Aucfer reste dans l'eau. Certaines entreprises ne sont pas accessibles en voiture, ni sans bottes.
15 h. L'Oust et le Blavet en d�crue
La d�crue s'amorce sur l'Oust et le Blavet . La Vilaine aval se stabilise. mais une nouvelle perturbation s'annonce entre lundi et mardi.
13 h 40. Malestroit : le niveau baisse, le pont rouvert aux pi�tons
Bonne nouvelle pour les habitants de Malestroit. Le niveau de l�eau a bien diminu� ce lundi matin , permettant la r�ouverture du Pont Neuf, mais uniquement aux pi�tons.
Ce lundi matin, les services techniques de la ville ont d�cid� de faire installer un pont flottant place des f�tes pour permettre aux habitants des seize logements de se d�placer.
13 h 20. La maire de Morlaix pousse un coup de gueule contre l��tat
Agn�s Le Brun, la maire UMP de Morlaix, pousse un coup de gueule contre les services de l'�tat de pr�vision et de pr�vention. � les �lus locaux devraient avoir acc�s aux donn�es directement sans avoir � passer par les services de l'�tat �, estime-t-elle.
LIRE ICI.
13 h 10. Le rempart du ch�teau inqui�te
Les deux fissures qui s��taient produites dans le mur�du ch�teau des Rohan, � Pontivy, se sont encore agrandies .
Le rempart �croul� du ch�teau des Rohan, � Pontivy (Morbihan), continue l�g�rement de s�affaisser, ce lundi.�|�Photo : Yann-Armel Huet / Ouest-France.
13 h 05. La d�crue se confirme � Pontivy
Les Pontivyens n�en finissent pas de nettoyer, apr�s les inondations du week-end. Mais la d�crue semble se maintenir .
Grand nettoyage ce lundi au restaurant du Signan, o� l'eau est mont� jusqu'� 60 cm de hauteur.�|�Photo : Yann-Armel Huet / Ouest-France.
13 h. La d�crue se fait peu � peu � Planco�t
Apr�s avoir atteint 1,20 m, l'Arguenon amorce sa d�crue depuis hier soir. L'eau se situait � 40 cm ce matin.
Les quais de Planco�t ont �t� recouverts par l'Arguenon. La d�crue est annonc�e.�|�Photo : Emilie Chassevant / Ouest-France
12 h 25. Dans le Morbihan, 140 sapeurs-pompiers en renfort
En 24 heures, 347 sapeurs-pompiers ont �t� mobilis�s sur 76 interventions inh�rentes aux inondations.
12 h. La Vilaine continue de monter � Redon
Les renforts sont arriv�s � Redon pour tenter de contenir les eaux de l'Oust et de la Vilaine, prot�ger les zones sensibles et les riverains.
� On n�attend pas de vraie d�crue avant mercredi �, pr�cise le maire. La circulation va rester coup�e entre Redon et le Morbihan, et probl�matique avec la Loire-Atlantique jusqu�� mercredi au moins.
11 h 30. Flers : la route qui relie le bourg de Cerisy� au Vieux-Caligny, inond�e, est actuellement coup�e � la circulation.
10 h 55. La liaison a repris vers l'�le de Sein�
Samedi et dimanche, l'�le de Sein a �t� coup�e du monde une nouvelle fois, la faute aux condtions m�t�o. D�s ce lundi 10 f�vrier, 10 h, la liaison de l'�le avec le continent a repris. L'Enez Sun, le navire de la compagnie Penn ar bed, a quitt� le quai du Rosmeur, � Douarnenez, avec, � son bord, 27 passagers.
10 h 50. Fin de l'alerte rouge
L'Ille-et-Vilaine, le Morbihan et la Loire-Atlantique sont ramen�s en vigilance orange, a indiqu� M�t�o-France dans son bulletin de 10 h 00. La Vilaine aval, � l'origine du passage en alerte rouge, repasse ainsi � l'orange sur le site de Vigiecrues.
L'Ille-et-Vilaine, le Morbihan et la Loire-Atlantique sont ramen�s en vigilance orange�|�M�t�o France
10 h 30. Malestroit : d�crue et nouvelle op�ration de sauvetage de porcs
Une trentaine de militaires de la S�curit� civile sont mobilis�s pour �vacuer un �levage de porcs � Malestroit o� la d�crue s�annonce lente.
L�op�ration de sauvetage mobilise une cinquantaine de pompiers du Morbihan et militaires de la S�curit� civile�|�Photo : Ouest-France
10 h. Dans le Morbihan, des routes rouvertes et renforts de trains�
Depuis 7h ce matin, plusieurs routes sont rouvertes � la circulation. Pour acc�der � Redon, la SNCF renforce son service de TER.
9 h 45. Des routes toujours barr�es en C�tes-d'Armor
Une quinzaine de routes du d�partement sont toujours coup�es ce matin en raison des inondations. Les automobilistes sont appel�s � la prudence.
9 h. A Quimperl�, les rivi�res sont contenues
Les quais et places lib�r�es par les eaux sont en cours de nettoyage par les �quipes techniques de la Ville.La circulation est � nouveau possible quai Brizeux.
6 h. Trois d�partements de l'Ouest toujours en alerte rouge
Le Morbihan, l'Ille-et-Vilaine et la Loire-Atlantique rouge sont toujours en vigilance rouge pour crues dangereuses par M�t�o France.
Le Morbihan, l'Ille et Vilaine et la Loire Atlantique, toujours en alerte rouge.�|�M�t�o France
La carte des routes coup�es dans le Morbihan
La carte ici
La carte des routes coup�es dans la r�gion de Redon
Dans la r�gion de Redon, de nombreuses routes restaient coup�es dimanche soir . Voici le meilleur moyen d'acc�der � la commune :�
La carte des routes coup�es dans la r�gion de Redon, dimanche soir.�|�Infographie Ouest-France
Lire aussi
