TITRE: Le Bayern privé de Ribéry - UEFA Champions League - News - UEFA.com
DATE: 2014-02-10
URL: http://fr.uefa.com/uefachampionsleague/news/newsid%3D2054038.html
PRINCIPAL: 161487
TEXT:
Lewandowski vers le Bayern
Rib�ry a �t� op�r� la semaine derni�re pour un vaisseau sanguin �clat�. Le Pr Hans-Wilhelm M�ller-Wohlfahrt, a a conduit la chirurgie, a affirm� qu'il ne pourrait faire son retour pour le match � Londres.
L'international fran�ais de 30 ans, troisi�me du classement du Ballon d'Or FIFA et meilleur joueur d'Europe UEFA en 2012/13, esp�re prendre part au match retour le 11 mars. Il est �galement incertain pour la rencontre amicale de l'�quipe de France le 5 mars contre les Pays-Bas.
�UEFA.com 1998-2014. All rights reserved.
Mis � jour le: 11/02/14 3.42HEC
