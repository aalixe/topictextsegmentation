TITRE: The Voice 3 : Voici les audiences du cinqui�me prime
DATE: 2014-02-10
URL: http://www.stephanelarue.com/The-Voice-3-Voici-les-audiences-du-cinquieme-prime_a8182.html
PRINCIPAL: 158343
TEXT:
The Voice 3 : Voici les audiences du cinqui�me prime
Dimanche 9 F�vrier 2014 - 15:35
Hier, " THE VOICE,�LA PLUS BELLE VOIX�", pr�sent� par Nikos Aliagas et produit par Shine France, a rassembl�8,1 millions de t�l�spectateurs, 36% de PdA, 49% de PdA sur les femmes de moins de 50 ans RdA*, 54% de PdA sur les individus de 15 � 24 ans et 50% de PdA sur les individus de 15 � 34 ans.
Avec ces scores exceptionnels, TF1 s'est plac� tr�s largement en t�te des audiences de la soir�e.
A 23h15, "�THE VOICE,�LA SUITE�" pr�sent�e par Nikos Aliagas accompagn� de Karine Ferri, �a r�uni 2.8 millions de t�l�spectateurs, 23% de PdA, 26% de PdA sur les femmes de moins de 50 ans RdA*, 39% de PdA sur les individus 15 � 24 ans et 28% de PdA sur les individus 15 � 34 ans.
L'�mission s'est plac�e en t�te des audiences.
�
DEJA 1.8 MILLION DE COMMENTAIRES ECHANGES AUTOUR DE THE VOICE
�
Hier, pr�s de 200 000 �tweets ont �t� publi�s par les t�l�spectateurs, faisant du show le programme le plus comment� de la semaine. Depuis�son lancement, "The Voice, la plus belle voix"�a g�n�r� 1.8 millions de commentaires.�
Les prestations les plus comment�es, hier soir, furent celles de #Liv , #No�mie , et #M�lissa .
Un pic de tweets�a �t� observ� �22h50 avec pr�s de�3700 tweets/minute. Pendant le show, les t�l�spectateurs ont pu se glisser dans la peau d'un coach, gr�ce au jeu "�5e�coach�" sur MYTF1 et�voir les r�sultats s'afficher � l'antenne.
