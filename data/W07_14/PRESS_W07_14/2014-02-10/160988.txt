TITRE: Flamanville : l'un des réacteurs en panne redémarre | Site mobile Le Point
DATE: 2014-02-10
URL: http://www.lepoint.fr/societe/flamanville-l-un-des-reacteurs-en-panne-redemarre-10-02-2014-1790261_23.php
PRINCIPAL: 0
TEXT:
10/02/14 à 17h35
Flamanville : l'un des réacteurs en panne redémarre
L'un des deux réacteurs de la centrale nucléaire, arrêtés à la suite d'un problème de "moins de deux secondes", a pu redémarrer. L'autre reste à l'arrêt.
La centrale nucléaire de Flamanville, dans la Manche. AFP
Source AFP
L'un des deux réacteurs de la centrale nucléaire de Flamanville (Manche), arrêtés à la suite d'un problème de "moins de deux secondes", selon RTE, sur le réseau samedi, a pu redémarrer dimanche soir, mais l'autre demeurait lundi après-midi à l'arrêt, selon EDF . Le réacteur 2 fonctionne à pleine puissance après avoir redémarré dimanche soir, a indiqué lundi le service communication de la centrale. EDF n'était pas lundi en milieu d'après-midi en mesure de dire quand le réacteur 1 pourrait être relancé.
Samedi "à 22 heures, à la suite d'un défaut sur le réseau de transport d'électricité, l'unité de production n° 2 de la centrale de Flamanville s'est isolée du réseau électrique et a cessé de produire. L'ensemble des systèmes de protection a parfaitement fonctionné", avait expliqué dimanche EDF dans un communiqué. "Simultanément, un court-circuit s'est produit sur un des pôles du transformateur principal de l'unité n° 1, entraînant un arrêt automatique du réacteur", avait poursuivi l'opérateur.
"Un réacteur a redémarré, l'autre pas"
RTE de son côté a constaté samedi soir "un défaut qui a duré moins de deux secondes" sur les deux lignes qui relient chacune un réacteur au réseau, a indiqué lundi à l' AFP le service communication de la société qui gère les lignes électriques françaises. "Un réacteur a redémarré, l'autre pas. Je ne sais pas pourquoi", a-t-il ajouté, renvoyant à EDF. "Les équipes locales et les experts nationaux d'EDF poursuivent leur travail d'investigation et d'analyse" sur le réacteur 1, a indiqué EDF dans un communiqué diffusé lundi.
RTE travaille de son côté sur le lien éventuel entre ce "défaut fugitif" et "l'orage violent qui provoquait des impacts de foudre dans la zone" samedi soir. Selon EDF, "les deux unités étaient à pleine puissance au moment de l'incident". "Aucun blessé n'est à déplorer. L'incident n'a pas eu de conséquence sur l'environnement", selon l'électricien.
