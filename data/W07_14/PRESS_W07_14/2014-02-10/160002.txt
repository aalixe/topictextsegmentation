TITRE: Suisse: apr�s le vote anti-immigration, Marine Le Pen parle de � bon sens � (VIDEO) - France-Monde - La Voix du Nord
DATE: 2014-02-10
URL: http://www.lavoixdunord.fr/france-monde/suisse-apres-le-vote-anti-immigration-le-pen-parle-de-ia0b0n1908667
PRINCIPAL: 0
TEXT:
Florian Philippot  marqu� "par le mod�le suisse" par rfi
Le vote suisse en faveur d�une limitation de l�immigration est une mauvaise nouvelle pour l�Europe, a regrett� lundi le chef de la diplomatie fran�aise, Laurent Fabius, qui a soulign� que l�Union europ�enne allait devoir r�viser ses relations avec la Suisse.
� C�est un vote pr�occupant parce qu�il signifie que la Suisse veut se replier sur elle-m�me (�) et c�est paradoxal car la Suisse fait 60 % de son commerce ext�rieur avec l�Union europ�enne �, a soulign� le ministre des Affaires �trang�res � la radio RTL.
� C�est une mauvaise nouvelle � mon avis � la fois pour l�Europe et pour les Suisses, parce que la Suisse referm�e sur elle-m�me, �a va les p�naliser �, a poursuivi le ministre, en estimant que � la Suisse toute seule ne repr�sente pas une puissance �conomique consid�rable �.
Le r�f�rendum, intitul� � Contre l�immigration de masse �, a �t� organis� � l�initiative du parti UDC (droite populiste),exc�d� par la forte hausse du nombre des immigr�s ces derni�res ann�es depuis l�adh�sion de la Suisse � la libre circulation dans l�Europe, appliqu�e depuis 2002.
� On va revoir nos relations avec la Suisse �, apr�s cette d�cision des Suisses d�introduire des quotas d�immigration, a ajout� Laurent Fabius.
� Il y a depuis 1999 des accords avec la Suisse qui portent notamment sur la libre circulation des travailleurs et sur beaucoup d�autres �l�ments et il y a une clause dite de guillotine qui fait que si l�un des �l�ments est mis en cause, tout tombe, donc �a veut dire qu�il va falloir ren�gocier �, a-t-il d�taill�.
La Commission europ�enne a aussit�t � regrett� � la d�cision des Suisses d�introduire des quotas d�immigration et a pr�venu qu�elle � examinera les implications de cette initiative sur l�ensemble des relations entre l�UE et la Suisse. �
Marine Le Pen salue la victoire du � oui �
En France, pays frontalier avec la Suisse o� travaillent de nombreux Fran�ais, le parti d�extr�me droite Front national, pr�sid� par Marine Le Pen, a pour sa part salu� � la lucidit� du peuple suisse. �
Marine Le Pen a appel� lundi les Fran�ais � suivre l�exemple suisse apr�s la victoire du � oui � � la votation contre � l�immigration de masse �, estimant que les Fran�ais voteraient � tr�s largement � dans le m�me sens en cas de r�f�rendum.
� Les Suisses font preuve de beaucoup de bon-sens �, a applaudi la pr�sidente du Front national sur Europe 1. � J�aimerais bien qu�on les suive d�ailleurs et je pense que s�il y avait un r�f�rendum en France sur ce m�me sujet, les Fran�ais voteraient tr�s largement pour l�arr�t de l�immigration de masse. �
� C�est �videmment vers ce chemin que j�appelle les Fran�ais, ce chemin de la libert�, de la souverainet�, de la d�fense de notre �conomie, de notre syst�me de protection sociale et de notre identit� �, a-t-elle affirm�.
� Il ne s�agit pas de mettre un mur, il s�agit d�avoir une porte. On l�ouvre ou on la ferme selon son int�r�t, selon l�int�r�t du peuple �, a-t-elle pr�conis�, � �a s�appelle la souverainet�. �
� Par exemple sur le terminal m�thanier de Dunkerque, il y a 40 % de travailleurs qui sont des �trangers, et par cons�quent, il y a 40 % de salari�s qui pourraient �tre Fran�ais sur ce terminal et qui ne le sont pas. Je consid�re que �a doit �tre combattu �, a-t-elle pr�n�, ajoutant: � Quand on est en crise �conomique, il faut donner la priorit� d�acc�s � l�emploi � nos compatriotes �.
Marine Le Pen a �galement attaqu� l�Union europ�enne, qui a pr�venu dimanche qu�elle examinerait � les implications � de la d�cision suisse � sur l�ensemble des relations entre l�UE et la Suisse �.
� L�Union europ�enne menace, elle va envoyer les chars, peut-�tre, pour laisser ouvertes les fronti�res de la Suisse... �, a ironis� la pr�sidente du FN. � C�est assez r�v�lateur du fonctionnement de l�Union europ�enne qui multiplie les menaces, qui multiplie les chantages. On peut parfaitement commercer avec l�ensemble de ses voisins sans �tre soumis aux r�gles qu�impose l�Union europ�enne. �
Cet article vous a int�ress� ? Poursuivez votre lecture sur ce(s) sujet(s) :
