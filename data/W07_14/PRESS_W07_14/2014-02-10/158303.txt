TITRE: Chikungunya : l'�pid�mie prend de l'ampleur en Martinique - Afrik.com : l'actualit� de l'Afrique noire et du Maghreb - Le quotidien panafricain
DATE: 2014-02-10
URL: http://www.afrik.com/chikungunya-l-epidemie-prend-de-l-ampleur-en-martinique
PRINCIPAL: 0
TEXT:
dimanche 9 f�vrier 2014 / par Moussa Kane
L��pid�mie de chikungunya s�intensifie en Martinique et se poursuit dans le reste des Antilles. Selon le dernier bulletin �pid�miologique officiel, en une semaine, le nombre de cas de Chikungunya a explos� de 34% en Martinique.
Selon le dernier bulletin �pid�miologique officiel, en une semaine, le nombre de cas de Chikungunya a explos� de 34% en Martinique, poussant le Directeur g�n�ral de la sant� � se rendre sur place, ce lundi.
L��pid�mie de chikungunya s�intensifie en Martinique et se poursuit dans le reste des Antilles. C�est ce qui ressort du point �pid�miologique de la Cellule inter-r�gionale d��pid�miologie (CIRE) Antilles-Guyane, diffus� ce vendredi. D�apr�s ce bulletin officiel, en une semaine, le nombre de cas cliniquement �vocateurs a bondi de 34%, d�passant d�sormais les 500 hebdomadaires. Il s�agit uniquement des cas qui ont fait l�objet d�une consultation en m�decine de ville. Depuis la mise en place du dispositif de surveillance, en d�cembre dernier, 1 480 personnes auraient consult� un m�decin g�n�raliste. La cinqui�me semaine de janvier, 52 adultes sont pass�s aux urgences et 7 enfants. Si le nombre hebdomadaire de passages aux urgences se stabilise, les admissions d�adultes ont progress� de 17% ces deux derni�res semaines.
La CIRE rappelle qu�il est indispensable que les prescripteurs r�servent les analyses de confirmation biologique aux patients qui pr�sentent des facteurs de risque ainsi qu�� ceux atteints de formes inhabituelles de la maladie.
� la une
