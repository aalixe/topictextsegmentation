TITRE: Centrafrique: 10 tu�s � Bangui, tourn�e du ministre fran�ais de la D�fense - France-Monde - La Voix du Nord
DATE: 2014-02-10
URL: http://www.lavoixdunord.fr/france-monde/centrafrique-10-tues-a-bangui-tournee-du-ministre-ia0b0n1906873
PRINCIPAL: 0
TEXT:
Au moins 10 personnes ont p�ri en moins de 24H � Bangui dans des violences interreligieuses accompagn�es de pillages, au moment o� le ministre fran�ais de la D�fense d�bute dimanche une nouvelle tourn�e r�gionale largement consacr�e � la Centrafrique.
AFP PHOTO
- A +
Dans la capitale, une nouvelle flamb�e de violence a �clat� samedi soir aux abords de la mairie du 5e arrondissement, au centre-ville, avec cinq personnes tu�es dans des circonstances non �tablies, puis trois autres dans des affrontements intercommunautaires, et une neuvi�me par des soldats de la force de l�Union africaine (Misca), selon des t�moins.
Ce bilan a �t� confirm� sur place � l�AFP par Peter Bouckaert, de l�ONG Human Rights Watch, qui a �galement fait �tat du lynchage � mort d�un musulman dimanche dans la matin�e, pr�s du march� central de Bangui. Un deuxi�me musulman a eu la vie sauve gr�ce � l�intervention de soldats de la Misca, a-t-il pr�cis�.
Dimanche, des soldats fran�ais et des gendarmes centrafricains ont pris position dans le 5e arrondissement livr� aux pilleurs, au milieu de ruines de commerces encore fumantes. Le quartier �tait survol� par un h�licopt�re de combat fran�ais, faisant baisser la tension sans arr�ter les pillages.
�La nuit, c��tait terrible�
Selon des habitants, apr�s la mort de cinq personnes samedi soir, une femme chr�tienne de ce quartier mixte a �t� tu�e par un musulman. Son agresseur a �t� captur� et tu�, et son cadavre br�l� devant la mairie, o� son corps calcin� gisait au milieu de la route dimanche matin.
Un deuxi�me civil musulman a ensuite �t� tu� et son meurtrier s�appr�tait � jeter le cadavre dans un brasier quand les soldats rwandais de la Misca ont ouvert le feu, a racont� Innocent, un habitant du quartier. �Ils l�ont tu�, a accus� Innocent, s�exprimant au milieu d�une foule surexcit�e criant �A mort les Rwandais�.
�Les Rwandais sont tous des musulmans! Dehors, les Rwandais!�, hurlait une femme, tandis que cr�pitaient des rafales de kalachnikov � vraisemblablement des tirs de sommation de la Misca.
Dans la mairie du 5e arrondissement, une petite dizaine de soldats rwandais �tait retranch�e: �la nuit, c��tait terrible�, a dit l�un d�eux � l�AFP.
En fin de matin�e, malgr� les remontrances des militaires fran�ais, des bandes de jeunes pillards continuaient de venir se servir, certains �quip�s de brouettes ou de charrettes se glissant entre les blind�s.
�Les Fran�ais ne vont pas tirer�
Au fil des heures, le nombre de pillards n�a cess� d�augmenter: �Les Fran�ais ne vont pas nous tirer dessus�, assurait en riant un jeune coiff� d�un bonnet.
Le commandant en chef de la Misca, le g�n�ral camerounais Martin Tumenta Chomua, a menac� samedi les groupes arm�s de recourir � la force pour arr�ter assassinats, lynchages et pillages qui se poursuivent � Bangui et en province en toute impunit�.
Dans ce climat de violences sans fin, le ministre fran�ais de la D�fense Jean-Yves Le Drian est attendu dimanche apr�s-midi au Tchad pour une nouvelle tourn�e consacr�e en large partie � la crise centrafricaine.
Il doit s�entretenir � N�Djamena avec le pr�sident Idriss D�by Itno, acteur militaire et politique majeur d�Afrique centrale, de la situation � Bangui, mais aussi de la r�organisation du dispositif fran�ais au Sahel, o� l�arm�e tchadienne a combattu en premi�re ligne aux c�t�s des Fran�ais lors de l�intervention au Mali d�but 2013.
M. Le Drian se rendra ensuite au Congo, dont le pr�sident Denis Sassou Nguesso est m�diateur dans le conflit centrafricain, puis mercredi � Bangui pour sa troisi�me visite depuis le d�but, le 5 d�cembre, de l�intervention fran�aise (�op�ration Sangaris�).
La Centrafrique a sombr� dans le chaos depuis la prise du pouvoir en mars 2013 par Michel Djotodia, chef de la coalition rebelle S�l�ka � dominante musulmane devenu pr�sident, qui a �t� contraint � la d�mission le 10 janvier pour son incapacit� � mettre fin aux tueries interreligieuses.
II a �t� remplac� le 20 janvier par Catherine Samba Panza, qui a effectu� samedi et dimanche son premier d�placement � l��tranger, � Brazzaville o� elle a rencontr� M. Sassou Nguesso.
Cet article vous a int�ress� ? Poursuivez votre lecture sur ce(s) sujet(s) :
