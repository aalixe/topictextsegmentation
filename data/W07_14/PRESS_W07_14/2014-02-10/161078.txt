TITRE: L'iPhone 6 pourrait doter d'un �cran incassable gr�ce au cristal de saphir
DATE: 2014-02-10
URL: http://www.infos-mobiles.com/iphone-6/liphone-6-pourrait-doter-dun-ecran-incassable-grace-au-cristal-de-saphir/50308
PRINCIPAL: 161077
TEXT:
Accueil � L�iPhone 6 pourrait doter d�un �cran incassable gr�ce au cristal de saphir
L�iPhone 6 pourrait doter d�un �cran incassable gr�ce au cristal de saphir
Fati 10 f�vrier 2014 0
Bient�t l� iPhone 6 sera dot� d�un ecran incassable. La firme Apple aurait investi dans la production d��crans en cristal de saphir, c�est ce que �nonce le site sp�cialis�9to5mac: ��Apple s�est procur� suffisamment de cristal de saphir pour fabriquer 100 � 200 millions d��crans d�iPhone��, indique la r�daction du site.
Apple aurait adopt� le cristal de saphir pour le prochain iPhone 6 afin de rendre les �crans de t�l�phone presque fort aux chutes, cette mati�re est beaucoup plus solide que le verre utilis� pour faire les �crans d�iPhone. Seul un minerai semblable ou du diamant pourrait mettre � mal un tel �cran. En effet, le cristal de saphir prot�ge d�j� le bouton principal de l�iPhone 5s int�grant le lecteur d�empreinte digitale Touch ID et le capteur photo des derniers iPhone.
Au niveau caract�ristique, l�iPhone 6�Air�devrait �tre int�gr� d�un processeur quad-core A7 cadenc� � 2,8 Ghz Plus fin, il devrait doter d�un �cran plus grand de 5 pouces, avec un capteur photo de 20,2 m�gapixels � l�arri�re et 8 m�gapixels en fa�ade, ce qui est assez impressionnant. Egalement il devrait poss�der une�connectivit� Wi-Fi am�lior�e avec la prise en charge de la norme 802.11ac, d�j� ajout�e aux Mac de derni�re g�n�ration, avec sa coque en aluminium et sa r�sistance � l�eau.
D�apr�s les derni�res informations, le Smartphone pourrait lancer le 20 juin prochain, mais pour l�instant,�Apple�n�officialise aucune information.
