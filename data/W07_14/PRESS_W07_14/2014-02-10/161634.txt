TITRE: D�veloppement rural : des ��r�sultats concrets�� contre les d�serts m�dicaux (Marisol Touraine) - Actualit�s - La France Agricole
DATE: 2014-02-10
URL: http://www.lafranceagricole.fr/actualite-agricole/developpement-rural-des-resultats-concrets-contre-les-deserts-medicaux-marisol-touraine-84127.html
PRINCIPAL: 161628
TEXT:
Des ��r�sultats concrets�� contre les d�serts m�dicaux (Marisol Touraine)
Publi� le lundi 10 f�vrier 2014 - 18h27
La ministre de la Sant�, Marisol Touraine, a salu� le 10�f�vrier 2014 les ��r�sultats concrets�� obtenus depuis un an dans la lutte contre les d�serts m�dicaux.
�
��La transformation profonde de notre syst�me [de sant�] est engag�e. [...] Les moyens qui ont �t� mis en place permettent d'aboutir � des r�sultats concrets�: les m�decins s'installent plus nombreux dans des territoires fragiles��, a d�clar� la ministre � Chalon-sur-Sa�ne (Sa�ne-et-Loire) o� elle a inaugur� un h�licopt�re de secours au centre hospitalier. ��Tout cela, ce sont des changements de vie concrets qu'il s'agit de poursuivre et d'amplifier��, a-t-elle ajout�.
�
La ministre a notamment indiqu� que le gouvernement allait diminuer de moiti� � la fin de 2014 le nombre de Fran�ais �loign�s de plus de 30�minutes d'un acc�s � des soins urgents, de 2�millions de personnes en 2012 � ��moins d'un�million � la fin de cette ann�e��. Ainsi, 650�m�decins correspondants du Samu, form�s et �quip�s pour intervenir ��en avant-poste�� des services de secours, devraient �tre install�s en 2014, contre 150 en 2012.
�
La possibilit� d'accueillir en urgence les patients en moins de 30�minutes �tait une promesse de campagne du candidat Fran�ois Hollande, qui a r�affirm� cet engagement � plusieurs reprises apr�s son �lection � la pr�sidence de la R�publique, au nom du ��droit fondamental�� que constitue l'acc�s � la sant�.
�
Marisol Touraine a aussi rappel� que 200 postes de praticien territorial de m�decine g�n�rale (PTMG) avaient �t� cr��s en 2013�: 180 ont �t� sign�s et ��les vingt derniers contrats au titre de 2013 sont en voie de finalisation��, a-t-elle d�clar�. 200 suppl�mentaires seront mis en place en 2014. La mise en place de ces PTMG vise notamment � garantir aux jeunes m�decins qui acceptent de s'installer dans les territoires manquant de praticiens un revenu net mensuel de 3.640 euros, gr�ce � des compl�ments de r�mun�ration.
�
��Sans doute y a-t-il eu des praticiens qui allaient s'installer et ont b�n�fici頻 du dispositif, a reconnu Marisol Touraine. ��Mais c'est une fa�on de les ancrer dans le paysage��, a-t-elle aussi soulign�. ��Les trois R�gions o� il y a le plus de PTMG sont le Rh�ne-Alpes (33), le Centre (13) et la�Bourgogne (12)��, a-t-elle pr�cis�, admettant qu'��il y a des R�gions qui ont plus de mal que d'autres��. ��Il y a des R�gions qu'il faudra booster un peu. La R�gion Nord-Pas-de-Calais n'y est pas�� et ��n'a pas suffisamment embray頻 puisqu'aucun contrat n'y a �t� sign�, a-t-elle indiqu�. La ministre a �voqu� ��des raisons techniques��, �voquant le ��d�marchage�� r�alis� par la R�gion Rh�ne-Alpes, mais aussi des raisons ��plus sociologiques��, citant le manque de stages pour les �tudiants en m�decine dans certaines facult�s.
�
Mots-cl�s : d�serts m�dicaux , d�veloppement rural , Marisol Touraine , social
Ces articles peuvent �galement vous int�resser :
