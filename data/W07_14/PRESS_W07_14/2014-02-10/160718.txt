TITRE: Ed Skrein de "Game of Thrones" remplace Jason Statham dans "Le Transporteur" - Terrafemina
DATE: 2014-02-10
URL: http://www.terrafemina.com/societe/buzz/articles/37786-ed-skrein-de-game-of-thrones-remplace-jason-statham-dans-le-transporteur.html
PRINCIPAL: 160715
TEXT:
�
�
L'acteur Ed Skrein, qui jouait le mercenaire Daario Naharis dans la s�rie � Game of Thrones �, va succ�der � Jason Statham dans le reboot du � Transporteur �, dont la sortie est annonc�e pour 2015.
Vir� Jason Statham ! L'acteur britannique Ed Skrein vient d'�tre choisi pour reprendre le r�le de Frank Martin alias le Transporteur, dans la franchise � succ�s produite par Luc Besson .
Dans une interview accord�e � Variety, Christophe Lambert, patron d'Europacorp, explique le choix d'Ed Skrein pour incarner Frank Martin. � Nous avons cherch� partout un nouveau visage qui aurait le potentiel de devenir une star de films d'action et nous avons trouv� la bonne �quation avec Ed Skrein, qui n'est pas seulement un grand acteur, mais qui a �galement le charisme et l'�nergie physique pour jouer Frank Martin avec brio �.
Ed Skrein, aka Daario Naharis
Contrairement aux trois premiers volets, �crits par Luc Besson et Robert Mark Kamen, le script de ce reboot a �t� confi� � Bill Collage et Adam Cooper, auteurs notamment du sc�nario du Casse de Central Park, avec Ben Stiller. Ce nouvel �pisode se d�roulera sur la C�te d'Azur, comme le premier film de la s�rie.
Le visage d�Ed Skrein est familier pour les fans de s�ries t�l� puisque l�acteur a incarn� Daario Naharis, le mercenaire �pris de Daenerys Targaryen dans la saison 3 de Game of Thrones. Il y a peu, on apprenait d�ailleurs que l�acteur ne serait pas au g�n�rique de la saison 4 , le r�le ayant �t� repris par le n�erlandais Michiel Huisman.
�
