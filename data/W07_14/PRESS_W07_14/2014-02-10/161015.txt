TITRE: Le nouveau m�dia du cr�ateur d'eBay d�marre avec une r�v�lation sur la NSA - Lib�ration
DATE: 2014-02-10
URL: http://ecrans.liberation.fr/ecrans/2014/02/10/le-nouveau-media-du-createur-d-ebay-demarre-avec-une-revelation-sur-la-nsa_979160
PRINCIPAL: 0
TEXT:
Premier regard sur �First Look�
Par Rapha�l Garrigos et Isabelle Roberts
Un nouveau site internet d�informations financ� par le fondateur d�eBay, Pierre Omidyar, a fait ses d�buts lundi avec un article du journaliste Glenn Greenwald, porte-voix des r�v�lations d�Edward Snowden, sur le r�le de la NSA dans les frappes de drones.
Baptis� The Intercept , le site est le produit de l�association du milliardaire irano-am�ricain et du journaliste d�investigation d�missionnaire du Guardian, qui a contribu� aux r�v�lations sur le vaste syst�me d�espionnage am�ricain de la NSA, l�agence charg�e des interceptions de communications. Dans ce premier article, Greenwald affirme que la NSA utilise la g�olocalisation des t�l�phones portables gr�ce � leur carte SIM pour d�terminer la position de personnes qui sont ensuite vis�es par une frappe de drones.
Cette m�thode a �t� utilis�e aussi bien au Pakistan, qu�en Afghanistan ou au Y�men, selon Greenwald, qui dit tenir ces informations de documents de la NSA fournis par l�ancien consultant Edward Snowden et d�un ancien op�rateur de drones.
Des frappes peu pr�cises
Cette unit� de la NSA, appel�e GeoCell, serait � l�origine de frappes sans que la cible soit formellement identifi�e mais simplement parce qu�elle utilise le t�l�phone dont la carte SIM a �t� localis�e. �Il peut s�agir de terroristes ou il peut s�agir de membres de leur famille qui n�ont rien � voir avec les activit�s de la cible�, affirme dans l�article l�ancien op�rateur de drones.
Les talibans en Afghanistan seraient de plus en plus m�fiants vis-�-vis de leurs t�l�phones, n�h�sitant pas � changer fr�quemment de cartes SIM pour �viter d��tre rep�r�s, affirme encore The Intercept. Un responsable de l�administration am�ricaine s�exprimant sous le couvert de l�anonymat a d�nonc� un article qui pr�sente une version d�form�e et �s�lective� de la fa�on dont fonctionnent les agences de renseignements.
�L�id�e que la communaut� du renseignement n�aurait recours qu�� une seule source d�information ou m�me deux sources avant de prendre une telle d�cision [�], �a ne se passe tout simplement pas comme �a�, a d�clar� � l�AFP ce responsable. Avant toute frappe de drone, toute une s�rie de renseignements sont pass�s en revue et �pes�s avec pr�caution� avant que le feu vert ne soit donn�, selon lui.
Pierre Omidyar a indiqu� qu�il investirait 250�millions de dollars (183�millions d�euros), dont 50 pour lancer les op�rations, dans ce projet qui doit permettre aux journalistes de �poursuivre la qu�te de la v�rit�.�Selon le site internet, la �mission � court terme� est de fournir une plateforme pour d�voiler les informations contenues dans les documents fournis par Edward Snowden sur la NSA.
