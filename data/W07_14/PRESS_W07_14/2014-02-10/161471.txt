TITRE: Programme TV : On vous recommande... Collaborations (France 3) - news t�l�
DATE: 2014-02-10
URL: http://www.programme-tv.net/news/tv/48375-programme-tv-recommande-collaborations-france-3/
PRINCIPAL: 161468
TEXT:
Programme TV : On vous recommande... Collaborations (France 3)
Programme TV : On vous recommande... Collaborations (France 3)
� AP / British Movietone
Ce lundi, ne manquez pas sur France 3 Collaborations, un document remarquable et sans concession qui �claire une partie sombre de notre histoire et incite � la vigilance.
Ce lundi, Collaborations �(France 3, 20h45) d�crypte ce jeu de dupes tragique au cours duquel entre 1940 et 1944, l'Etat fran�ais a collabor� avec l'Allemagne nazie. Une collaboration qui n'a fonctionn� que dans un sens : la France donne sans rien obtenir en retour.
Le 22 juin 1940, la France est vaincue et scind�e en deux. Au Nord, elle est occup�e par les Allemands, au sud, c'est la zone libre, o� le mar�chal P�tain a install� son gouvernement � Vichy. En 1941, Adolf Hitler nomme son homme de main Otto Abetz, ambassadeur du IIIe reich � Paris. Pour tirer les ficelles de la politique fran�aise, cet homme manipulateur choisit comme interlocuteur Pierre Laval qui a �t� rejet� par P�tain. L'objectif allemand est de vassaliser le pays et le vider de ses richesses. En 1942, cette collaboration prend une direction cruelle et dramatique. Le mar�chal P�tain installe Ren� Bousquet � la t�te de la police. Pr�nant une politique x�nophobe et antis�mite, le r�gime arr�te des juifs de France et les livre aux Allemands :� 78 000 p�riront en d�portation.
Cette s�rie en deux volets est sans concession et retrace le parcours des collaborationnistes fran�ais et surtout elle apporte un �clairage nouveau � travers le point de vue des Allemands et notamment celui de Otto Abetz. Une le�on d'histoire pour ne jamais oublier�
Sylvie Breton - lundi 10 F�vrier 2014 � 09:45
A lire aussi
