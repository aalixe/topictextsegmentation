TITRE: Beyonc� et Barack Obama en couple : la rumeur enfle - Actu - People - Plurielles.fr
DATE: 2014-02-10
URL: http://people.plurielles.fr/news-people/beyonce-et-barack-obama-en-couple-la-rumeur-enfle-8362515-402.html
PRINCIPAL: 160511
TEXT:
Article  par Ambre DEHARO , le
10/02/2014 � 15h38  , modifi� le
ZOOM
Hollande/Gayet c'est du pass�. La derni�re rumeur en date d�voile une possible histoire d'amour entre Beyonc� et Barack Obama...
Apr�s le Gayetgate, est-on sur le point de vivre un Beyoncegate ? Car oui, en cette journ�e du 10 f�vrier 2014, la rumeur enfle. Barack Obama et Beyonc� auraient eu une liaison. Pour l'instant, les d�tails de l'histoire sont inconnus, si histoire il y a bien. La rumeur est n�e ce matin au micro de la radio Europe 1. Le paparazzi Pascal Rostain est l'invit� de la station et il affirme que le pr�sident des Etats-Unis entretiendrait une liaison amoureuse avec la star du r'n'b mondiale. D'apr�s lui, le quotidien am�ricain � The Washington Post � pr�voit m�me d'en faire sa une du mardi 11 f�vrier.
� Vous savez, en ce moment, aux Etats-Unis, il y a quelque chose d'�norme qui est en train de se passer. D'ailleurs �a va sortir demain dans le Washington Post, on ne peut pas dire que ce soit de la presse de caniveau, sur une liaison suppos�e entre le pr�sident Barack Obama et Beyonc�. � Alors que la presse fran�aise et mondiale se remet tout juste de l'affaire Gayet-Hollande, qui a tout de m�me fait couler beaucoup d'encre outre-Atlantique, voil� qu'un autre scandale m�lant politique et show-business menace d'�clater. Et l�, on parle quand m�me de deux des personnalit�s les plus influentes du monde...
Barack Obama et Queen Bee ont toujours �t� tr�s proches. En janvier 2013, elle et son mari Jay-Z �taient invit�s � l'inauguration du second mandat du pr�sident. Beyonc� a m�me chant� � cette occasion. Le mois dernier encore, la chanteuse �tait convi�e aux 50 ans de Michelle Obama. Par ailleurs, le pr�sident am�ricain ne tarit pas d'�loges au sujet du couple Beyonc�/ Jay-Z . En 2012 il avait d�clar� � la radio Colby Colb que le couple �tait ce qu'il appelait � de bonnes personnes �. Ajoutant, � Beyonc� ne pourrait pas �tre plus gentille avec Michelle et les filles. Ce sont de bons amis �. D�but f�vrier, le tablo�d am�ricain � National Enquirer � laissait d�j� entendre que le couple pr�sidentiel am�ricain �tait sur le point de divorcer...
Pour l'instant, ces affirmations ne sont qu'� l'�tat de rumeur, aucune source pr�cise n'ayant sorti l'information.
�
Mise � jour du 10/02/2014 17:55 : Contact�e par Vanity Fair France, la directrice de la communication du Washington Post, Kristine Coratti, a d�menti tout projet d'article sur une liaison Obama-Beyonc� : "Je peux vous dire que c'est faux", a-t-elle affirm� au magazine.
