TITRE: Les pesticides seraient jusqu�� 1000 fois plus toxiques que ce qu'on dit - France - RFI
DATE: 2014-02-10
URL: http://www.rfi.fr/france/20140201-france-sciences-pesticides-1-000-fois-toxiques-seralini-inserm-roundup-ogm/
PRINCIPAL: 160956
TEXT:
Modifi� le 06-02-2014 � 11:12
Les pesticides seraient jusqu�� 1000 fois plus toxiques que ce qu'on dit
Un agriculteur traite son champ avec des pesticides.
Acap
Le professeur Gilles-Eric S�ralini revient � la charge. Apr�s avoir �tudi� les effets d�l�t�res d�un OGM  et du Roundup, un pesticide largement utilis�, le chercheur s�attaque cette fois plus largement � neuf pesticides les plus vendus dans le monde qui seraient selon lui de 2 � 1 000 fois plus toxiques que ce qui est g�n�ralement annonc�.
L'�tude controvers�e du professeur Gilles-Eric S�ralini sur les effets d�un OGM et du pesticide Roundup sur des rats avait frapp� les esprits en 2012. Appuy� par des photos des rongeurs d�form�s par des tumeurs impressionnantes, le travail du chercheur de l�universit� de Caen n�avait cependant pas convaincu l�Agence europ�enne de s�curit� des aliments (Efsa) pas plus que l�Agence nationale de s�curit� sanitaire (Anses).�
La revue Food and chemical toxicology , qui avait � l��poque publi� l�article du professeur S�ralini, l�a�retir� en novembre 2013. Le chercheur lie cette d�cision peu courante � l�arriv�e dans le comit� �ditorial de Richard Goodman , � un biologiste qui a travaill� plusieurs ann�es chez Monsanto �, le fabricant du Roundup .
�
Le professeur Gilles-Eric S�ralini  � Bruxelles le 20 septembre 2012.
REUTERS
Cela n�a cependant pas entam� la d�termination du professeur S�ralini qui vient de publier le prolongement de cette premi�re �tude dans la revue Biomed Research International . Co-sign� avec le Comit� de recherche et d�information ind�pendantes sur le g�nie g�n�tique (Criigen). � Nous avons �tendu les travaux que nous avions faits avec le Roundup et montr� que les produits tels qu�ils �taient vendus aux jardiniers, aux agriculteurs, �taient de 2 � 1�000 fois plus toxiques que les principes actifs qui sont les seuls � �tre test�s in vivo � moyen et long terme � pr�cise le professeur.
�
Effectivement, avant leur mise sur le march�, seuls les effets de la substance active sont �valu�s et non le produit tel qu�il est commercialis� au final avec tous les composants que les industriels, la plupart du temps, refusent de d�voiler. � Il y a m�prise sur la r�elle toxicit� des pesticides � poursuit M. S�ralini qui explique qu�il y a toxicit� � quand les cellules commencent � se suicider � au contact du produit et � qu�elles meurent en quantit�s beaucoup plus significatives que les cellules contr�les �.
�
L��tude qui vient d��tre publi�e a port� sur les 9 pesticides les plus vendus dans le monde�: trois herbicides (Roundup, Matin E1, Starane 200), trois insecticides (Pirimor G, Confidor, Polysect Ultra) et trois fongicides (Maronee, Opus, Eyetak). Chaque produit a �t� test� in vitro sur des cellules humaines.
�
Pr�somption forte
�
Au final, les chercheurs concluent que sur les 9 pesticides, � 8 formulations sont clairement en moyenne des centaines de fois plus toxiques que leur principe actif �. Ils insistent sur le r�le des additifs qui � sont souvent gard�s confidentiels et sont d�clar�s comme �tant inertes par les fabricants �. Ils soulignent �galement � propos du Roundup, pourvu disent-ils, d�une r�putation de relative b�nignit�, ��que ce produit s�est r�v�l�, et de loin, le plus toxique parmi tous les herbicides et insecticides test�s��.��
�
Ce constat, s�il est v�rifi�, devrait interpeller les pouvoirs publics et les organismes qui d�terminent la dose journali�re admissible pour les pesticides parce que celle-ci est calcul�e � partir de la toxicit� des seuls principes actifs des produits en question. Ce que met en lumi�re le travail de l��quipe du professeur S�ralini pourrait inciter � aller voir un peu loin en int�grant tous les composants dans les tests limit�s jusque-l� aux seules substances actives. C�est en tout cas ce que demande l�ONG G�n�rations futures qui d�plorait lors d�un r�cent colloque � l�Assembl�e nationale sur les pesticides � une sous-estimation des risques � due notamment � ces tests insuffisants.
�
Alors que les impacts sanitaires des pesticides sont de plus mis en avant par les scientifiques, 1�200 m�decins viennent de rendre public un appel mettant en garde sur les risques pos�s par beaucoup de ces produits. Une vaste expertise collective de l�Institut national de la sant� et de la recherche m�dicale ( Inserm ) publi�e au printemps 2013 est venue renforcer les craintes qui se multiplient envers les pesticides. L�Inserm concluait alors avoir � une pr�somption forte � entre l�usage des pesticides par des professionnels (agriculteurs, personnels des fabricants ou charg�s des espaces verts) et plusieurs pathologies. �
