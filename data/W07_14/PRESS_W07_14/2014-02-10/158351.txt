TITRE: Rugby - Tournoi - Saint-Andr� : �On n'a pas paniqu�
DATE: 2014-02-10
URL: http://www.lequipe.fr/Rugby/Actualites/Saint-andre-on-n-a-pas-panique/439930
PRINCIPAL: 158309
TEXT:
a+ a- imprimer RSS
�Philippe Saint-Andr�, que retenir de cette victoire mi-figue, mi-raisin ?
Je retiens d�j� que l'on a une deuxi�me victoire d'affil�e, �a ne nous �tait pas arriv� depuis 2012. C'est important. En premi�re mi-temps, on a beaucoup cr��, on a us� les Italiens. Mais on a manqu� un peu d'alternance, on a jou� parfois trop sur la lat�ralit�. Ce qui est bien c'est que, m�me si le score �tait accroch�, on n'a pas paniqu�, les joueurs ont gard� confiance en eux. On s'�tait dit durant toute la semaine et � la mi-temps qu'on allait bien attaquer le d�but de seconde mi-temps, car c'est ce qui nous avait fait d�faut contre l'Angleterre. Et l�, en deuxi�me mi-temps, on fait vraiment dix minutes de tr�s grande qualit�. On est beaucoup plus direct, Mathieu Bastareaud prend le centre du terrain, on arrive � faire des lib�rations rapides...et on marque trois essais.
�Deux victoires d'affil�e, �a ne nous �tait pas arriv� depuis 2012� En revanche, la suite fut plus terne...
Comme tout le monde, durant les vingt derni�res minutes, les joueurs se sont ennuy�s sur le terrain, nous on s'est ennuy�s dans les tribunes. On prend un carton jaune, j'ai d'ailleurs dit � S�bastien Vahaamahina que son geste n'�tait pas acceptable. Puis un carton rouge (� Rabah Slimani, ndlr) o� le premier geste (un coup de t�te, ndlr) est italien et il y a un mauvais r�flexe. Apr�s, on joue � 14, � 13, c'�tait une bouillie de rugby.
Vous attendiez-vous � �tre g�n�s autant par les Italiens ?
Il faut respecter les Italiens. C'est une �quipe de haut niveau qui pose des probl�mes � tout le monde. Ils ont �t� dans la partie face au pays de Galles pendant 80 minutes (d�faite 23-15, ndlr). Il ne faut pas croire qu'on peut leur mettre des points aussi facilement, ils ont de bons joueurs, ils conservent bien le ballon. Il faut les f�liciter. Sur les 20 derni�res minutes, on n'a pas touch� le ballon et dans ces conditions c'�tait tr�s compliqu� pour nous de jouer.�
AFP
