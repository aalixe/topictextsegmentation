TITRE: Audiences TV : �Ast�rix aux Jeux Olympiques� remporte la m�daille d'or - leParisien.fr
DATE: 2014-02-10
URL: http://www.leparisien.fr/tv/audiences-tv-asterix-aux-jeux-olympiques-remporte-la-medaille-d-or-10-02-2014-3577225.php
PRINCIPAL: 0
TEXT:
Vos amis peuvent maintenant voir cette activit� Supprimer X
�Les parts de march� atteignent 24,3% sur les individus de quatre ans et plus.
M6 suit avec son magazine �Capital�, consacr� aux d�penses publiques, qui a attir� 3,6 millions de curieux (14 % de PDA). Sur France 2, la rediffusion de �L'heure z�ro� a su s�duire pr�s de 3,4 millions de t�l�spectateurs, soit 12,5% du public.
France 3 se trouve au pied du podium avec la suite de la s�rie polici�re britannique �Les enqu�tes de Morse�. L'�pisode port� par Shaun Evans a convaincu 2,8 millions de fans soit 10,1% du public. Mais dimanche �tait aussi soir�e foot, avec le match choc de cette saison de Ligue 1 : Monaco / PSG sur Canal+ . Le match, qui s'est sold� par � 1 � 1, a r�uni plus de 2 millions de fans, pour une part d'audience de 34% sur les abonn�s de la cha�ne crypt�e.
Du c�t� des autres cha�nes, France 5 et France 4 sont au coude � coude. Le documentaire �Plus haut, plus fort : jusqu'o� ira Chamonix ?� sur la premi�re a rassembl� pr�s d'un million de  t�l�spectateurs (soit 3,5% du public), tout comme France 4 avec le film �Danger imm�diat� avec Harrison Ford (3,8% du public).
LeParisien.fr
