TITRE: La d�prime, c'est que du bonheur ! - France Info
DATE: 2014-02-10
URL: http://www.franceinfo.fr/arts-spectacles/a-l-affiche/la-deprime-c-est-que-du-bonheur-1312965-2014-02-10
PRINCIPAL: 0
TEXT:
La d�prime, c'est que du bonheur !
le Lundi 10 F�vrier 2014 � 06:55
Votre �valuation : Aucun Moyenne : 5 (1 vote)
Lien RSS
copier
Pour la deuxi�me ann�e, Rapha�l Mezrahi s'engage contre le bonheur obligatoire et organise une nuit de la d�prime.Chansons et textes tristes, une soir�e pour rire o� Nomwenn Leroy c�toie Yann Moix, oui c'est original et le public r�pond pr�sent. La deuxi�me nuit de la d�prime, c'est ce soir � l'Olympia.�
la nuit de la d�prime � Radio France
Cr�er une alerte avec ces th�mes
Par
Barre lat�rale de remont�es de contenus
- publicit� -
les sons du jour
dans    Faits divers
"�a s'appelle le r�ve d'enfant bris�, c'est tout"
Chaque jour, France Info vous propose un retour sur l'actualit� du jour � travers les reportages et les interviews les plus marquantes diffus�s sur notre antenne.�Au sommaire de ce mercredi, l'annonce de secr�taires d'Etat pour le gouvernement Valls, le projet sanglant de deux adolescentes de 13 ans, l'inqui�tude de la m�re d'une jeune fille partie faire le djihad en Syrie ou encore la situation toujours explosive en Ukraine.
Chine : une op�ration de chirurgie esth�tique pour �tre recrut�
La fin de l'ann�e universitaire approche en Chine, et les cliniques de chirurgie esth�tique font le plein. De plus en plus de jeunes dipl�m�s pensent que des paupi�res d�brid�es ou un nouveau nez vont augmenter leurs chances sur le march� du travail. En Chine, la comp�tition pour trouver un premier emploi est f�roce, et � comp�tence �gale, les employeurs optent souvent, et sans tabou, pour le candidat le plus beau.
Faire ex�cuter un jugement
Marion a obtenu un jugement favorable � la suite d'une proc�dure judiciaire. Son adversaire a �t� condamn� � lui verser une certaine somme d'argent. Mais malgr� plusieurs relances, il refuse de lui verser cette indemnit�.Interview avec ma�tre Matthieu Gallet, avocat au barreau de Paris.�
Renan Luce : "il faut attendre d'avoir des choses � dire!"
Son premier album, Repenti, avait fait sensation en 2007, lui valant deux Victoires de la musique, et un vrai succ�s populaire, prolong� en 2009 par Le Clan des miros. Cinq ans plus tard, Renan Luce revient, r�g�n�r�, avec D'une tonne � un tout petit poids, album pop-folk pleine de petites histoires et d'imagination.
le�09 Avril 2014�dans� Rencontre avec... �
"J'ai rendez-vous avec toi. Mon p�re de l'int�rieur", de Lorraine Fouchet
Christian Fouchet a �t� un gaulliste et un r�sistant de la premi�re heure. Dans son nouveau livre, sa fille, la romanci�re Lorraine Fouchet, renoue avec son p�re qu'elle a perdu quand elle avait dix-sept ans. Un voyage �mouvant et plein d'humour aux c�t�s de Saint-Exup�ry, Malraux et de Gaulle.
Erreurs judiciaires : combien y a-t-il d'innocents en prison en France ?
Affaire Dreyfus, ratage judiciaire d'Outreau, affaire Patrick Dils : quand la justice se trompe, lui faire admettre qu'elle a envoy� un innocent en prison rel�ve du parcours du combattant. Une loi a �t� vot�e fin f�vrier pour faciliter les proc�dures de r�vision de condamnations p�nales.
Un oeil bionique pour retrouver la vue
Un �il bionique capable de redonner une vision partielle � des personnes aveugles a convaincu la Haute Autorit� de Sant� et le Minist�re de la Sant�. Il y a une dizaine de jour, le Minist�re a annonc� que, d�sormais, la S�curit� Sociale allait prendre en charge ces implants bioniques.
