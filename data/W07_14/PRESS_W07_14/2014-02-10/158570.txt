TITRE: Centre Presse : ��� De belles images mais moins de magie
DATE: 2014-02-10
URL: http://www.centre-presse.fr/article-288193-bull-bull-bull-de-belles-images-mais-moins-de-magie.html
PRINCIPAL: 158569
TEXT:
��� De belles images mais moins de magie
La b�te � la poursuite de la belle.
(Photo, Eskwad/Path� production/TF1 films production/
Achte/Neunte/Zw�lfte/Achtzehnte Babelsberg Film Gmbh./120 Films)
Un marchand prosp�re (Andr� Dussolier), ruin� par un naufrage, est contraint de s'exiler � la campagne avec ses trois fils et trois filles. Un soir, il se perd dans la for�t et trouve refuge dans un magnifique ch�teau. En repartant, il cueille une rose pour sa fille, Belle. Le ma�tre des lieux, une terrible b�te, le condamne � mort. Belle d�cide de retourner au ch�teau � l'insu de son p�re.
En s'attaquant � La Belle et la B�te, conte qu'on associe tellement au film de Jean Cocteau, Christophe Gans (Le Pacte des loups) a cherch� � creuser l'intrigue et les personnages: pourquoi la B�te est-elle frapp�e de mal�diction? Qui sont les autres enfants du marchand? Il a aussi trait� le c�t� transgressif de la relation entre Belle et le monstre.
En visionnant ce qui est, reconnaissons-le, un tr�s grand spectacle, on ne peut que constater que les com�diens sont tr�s talentueux. Ils ont en effet tourn� dans un monde � imaginer, sur fond vert: pas d'imposante salle � manger ni de longs couloirs. Et si le masque de la b�te existe r�ellement (une oeuvre d'art avec des poils ins�r�s un par un), il n'a �t� plac� sur Vincent Cassel que num�riquement. L'acteur portait une sorte de casque de hockey pourvu de croix.
Les images sont tr�s belles et ceux qui d�couvrent La Belle et la B�te seront sans doute enthousiasm�s par ces d�cors f�eriques de for�ts, chiens et temp�tes num�riques. Les spectateurs nourris des images de Cocteau et d'Henri Alekan trouveront que le monde de Christophe Gans est finalement moins myst�rieux, moins effrayant et finalement moins magique que l'autre.
Sortie nationale
