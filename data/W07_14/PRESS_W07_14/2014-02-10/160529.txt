TITRE: Miley Cyrus : «Ma tournée est éducative pour les enfants»
DATE: 2014-02-10
URL: http://www.lefigaro.fr/musique/2014/02/10/03006-20140210ARTFIG00177-miley-cyrus-ma-tournee-est-educative-pour-les-enfants.php
PRINCIPAL: 0
TEXT:
Publié
le 10/02/2014 à 15:01
Le twerk aurait-il une visée pédagogique? Miley Cyrus a tort de croire au père Noël. Crédits photo : Evan Agostini/Evan Agostini/Invision/AP
Le jeune public qui va assister à son spectacle sera «exposé à un art que la plupart des gens ne connaissent pas», s'est vantée la chanteuse lors d'une interview accordée à sa s�ur Brandi.
Publicité
La vertu de son spectacle nous avait jusqu'alors échappé... Miley Cyrus s'apprête à donner le coup d'envoi de son Bangerz Tour aux États-Unis et elle le défend bec et ongles, au point de lui trouver des qualités insoupçonnées. Interviewée par sa s�ur Brandi, correspondante pour le site Fuse , la chanteuse a prétendu que sa tournée avait une visée pédagogique.
«Même si les parents ne penseront probablement pas ça, mon spectacle est éducatif pour les enfants. Ils vont être exposés à un art que la plupart des gens ne connaissent pas», se défend-elle devant sa s�ur incapable de contenir sa stupeur. Ses danses trash, ses tenues dénudées, son twerk légendaire et ses actes de provocation impliquant des nains et des personnes noires sont donc les composantes d'un «art» utile à l'éducation des enfants... «Les gens voient les choses de manière si manichéennes, surtout dans les petites villes. Je suis très excitée de montrer ce spectacle où cet art n'est pas accepté, où les enfants n'apprennent pas ce différent genre d'art», a-t-elle ajouté très sérieusement.
Un show à la Madonna mais en plus jeune
La jeune star confie avoir été inspirée par les tournées de Madonna, avec qui elle a récemment enregistré une session acoustique sur la chaîne américaine MTV. «J'aimerais faire quelque chose d'ambitieux, quelque chose que l'on aurait pu voir sur les tournées de Madonna mais qui auraient l'air plus jeune».
La mère de Miley Cyrus, Tish, intervient dans cette même vidéo pour défendre la conduite de sa fille. Si les médias critiquent aujourd'hui le twerk de Miley Cyrus, dans les années 1980 les groupes de rock comme Mötley Crue ou le chanteur Bon Jovi avaient eux-aussi des clips avec des «filles à moitié nues, des danses osées et des soirées arrosées. Ce n'était que sexe, drogue et rock'n'roll!». Eux au moins ne se vantaient pas d'être pédagogues.
