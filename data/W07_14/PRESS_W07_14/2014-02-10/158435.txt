TITRE: Incendie d'une usine en Inde: propri�taires incarc�r�s
DATE: 2014-02-10
URL: http://fr.canoe.ca/infos/international/archives/2014/02/20140209-060105.html
PRINCIPAL: 0
TEXT:
M�t�o
Incendie d'une usine en Inde: propri�taires incarc�r�s
Le Rana Plaza s'est effondr� en avril 2013 , un accident qui a fait au moins 1135 morts. �Phot Archives / AFP
Derni�res nouvelles
Tweeter
09-02-2014 | 06h01
DACCA - Les propri�taires d'une usine textile dont l'incendie en 2012 avait caus� la mort de 111 personnes, mettant en lumi�re les conditions de travail d�plorables dans les ateliers du pays, ont �t� �crou�s dimanche.
Delwar Hossain et sa femme Mahmuda Akter se sont constitu�s prisonniers quelque six semaines apr�s qu'un tribunal de Dacca eut d�livr� un mandat d'arr�t � leur encontre.
Ils ont �t� inculp�s d'homicide involontaire par n�gligence. Au total, 13 personnes sont poursuivies dans cette affaire. Ils encourent la prison � vie.
�Le tribunal a rejet� dimanche leur demande de lib�ration sous caution et les a envoy�s en prison�, a d�clar� � l'AFP le procureur Anwarul Kabir.
L'incendie �tait survenu en novembre 2012 dans l'usine Tazreen, dans la banlieue de Dacca, o� les ouvriers fabriquaient des v�tements pour l'am�ricain Walmart, les magasins n�erlandais C&A ou encore ENYCE, une marque d�tenue par le rappeur Sean Diddy Combs.
Incendie meurtrier
Les victimes, en majorit� des femmes gagnant quelques dizaines de dollars par mois, ont �t� pi�g�es par les fum�es toxiques ou se sont d�fenestr�es pour �chapper aux flammes.
Selon le t�moignage de survivants, les contrema�tres et les gardiens ont forc� les ouvriers � retourner � leur poste malgr� l'�paisse fum�e qui se d�gageait du rez-de-chauss�e d'o� l'incendie s'est d�clar�.
Cet incendie fut le plus meurtrier jamais survenu dans une usine textile au Bangladesh. Mais la catastrophe la plus terrible s'est produite quelques mois plus tard, lorsque s'est effondr� en avril 2013 le Rana Plaza, un immeuble de neuf �tages d'ateliers de confection pr�s de Dacca, un accident qui a fait au moins 1135 morts.
Le Bangladesh est le deuxi�me exportateur de v�tements au monde, fournissant notamment des grands noms tels que l'am�ricain Walmart, le fran�ais Carrefour ou encore le su�dois H&M.
Pilier de l'�conomie, le secteur repr�sente 80% des exportations annuelles s'�levant � 27 milliards de dollars, et emploie quatre millions de personnes, en majorit� des femmes.
Suite cette s�rie de trag�dies, une centaine de grands noms du textile occidentaux et le Bangladesh se sont mis d'accord fin novembre pour instaurer des normes de s�curit� renforc�es dans pr�s de 3500 usines, ouvrant la voie � une intensification des inspections.
Selon la police, l'inculpation des propri�taires et cadres de Tazreen est vraisemblablement une premi�re pour le pays qui compte 4500 usines textiles.
