TITRE: Le boom des drones inqui�te les autorit�s - Le Figaro �tudiant
DATE: 2014-02-14
URL: http://etudiant.lefigaro.fr/les-news/actu/detail/article/le-boom-des-drones-inquiete-les-autorites-4350/
PRINCIPAL: 0
TEXT:
Vous �tes ici� Les news �>� Actu �>�Le boom des drones inqui�te les autorit�s
Le boom des drones inqui�te les autorit�s
Par  Ang�lique N�groni
La place Stanislas de Nancy vue du drone de Nans Thomas.
Recevez nos newsletters :
7
Un Nanc�ien de 18 ans a �t� convoqu� au tribunal apr�s avoir fait des prises de vue a�riennes de sa ville.
Son clip a fait le tour de la Toile .Muni d�un drone �quip� d�une cam�ra, Nans Thomas, un jeune de 18 ans, a film� en janvier dernier Nancy ,ses toits, ses ruelles, ses chemin�es� Un petit voyage qui a permis, gr�ce � cet engin, de rapporter des prises de vue a�riennes visionn�es plus de 400.000 fois sur le Net. Mais le p�riple a�rien s�est achev� par un atterrissage muscl� En d�but de cette semaine, le jeune gar�on a �t� convoqu� par la gendarmerie et il en est ressorti avec une convocation en correctionnelle o� il devra r�pondre de �mise en danger de la vie d�autrui� le 20�mai prochain.
Pour la direction g�n�rale de l�aviation civile (DGAC) ,cette affaire illustre parfaitement la mani�re dont est utilis� aujourd�hui ce type d�appareil. En toute m�connaissance des r�gles en vigueur. Cet engin de plusieurs tonnes, au d�but cr�� par les militaires, est aujourd�hui devenu un mod�le r�duit, utilis� comme un loisir ou � des fins professionnelles pour filmer et inspecter, par exemple, des ouvrages d�art, des chantiers, mais aussi surveiller les vignes�
En plein essor depuis un an, ce march� attire ainsi tous les amateurs de nouvelle technologie qui ach�tent des drones et les font voler n�importe o�, sans conna�tre la toute r�cente r�glementation datant d�avril 2012. �La France est le premier pays au monde � s��tre dot� d�un cadre r�glementaire dans ce domaine�, dit-on � la DGAC.
� Nous redoutons un drame avec l�usage non ma�tris� de ces appareils. Cela porterait atteinte � un march� en pleine expansion �
Emmanuel de Maistre, pr�sident de la F�d�ration professionnelle du drone civil
Or, selon les textes et sauf autorisation pr�alable aupr�s de la pr�fecture, il est interdit pour un particulier d�utiliser cet a�ronef en zone peupl�e pour des raisons �videntes de s�curit�. �Nous nous effor�ons de faire conna�tre ces r�gles et nous sommes extr�mement vigilants face � ce ph�nom�ne en vogue car le danger est r�el�, indique le chef d�escadron Christophe Masset, de la gendarmerie des transports a�riens. Et depuis septembre, apr�s une phase d�assimilation de la r�glementation, les enqu�tes d�marrent enfin.
Selon nos informations, quatre appareils ont �t� depuis confisqu�s et 21 proc�dures sont aujourd�hui en cours. L�une d�elles concerne un particulier qui � Paris, en fin d�ann�e derni�re, a fait voler son drone � � proximit� de l�h�liport d�Issy-les-Moulineaux. L�accident redout� avec un h�licopt�re n�a pas eu lieu mais tout a bien failli mal finir. Apr�s un incident technique, le drone est tomb� d�un coup sur la route� et a �t� �cras� par une voiture.
�Nous redoutons un drame avec l�usage non ma�tris� de ces appareils. Cela porterait atteinte � un march� en pleine expansion�, s�alarme Emmanuel de Maistre, pr�sident de la F�d�ration professionnelle du drone civil qui, comme son nom l�indique, rassemble les acteurs professionnels de cette branche particuli�rement dynamique. Aujourd�hui, la France compte en effet 25 fabricants et 345 soci�t�s qui travaillent � partir de ces engins.
Quant � la Cnil, la Commission nationale de l�informatique et des libert�s, elle surveille aussi l��volution de ce ph�nom�ne. Car cet a�ronef qui fait fi des murs et des barri�res peut ais�ment porter atteinte � la vie priv�e� Filmer son voisin dans son jardin devient en effet un jeu d�enfant! Depuis janvier, la Cnil travaille d�ailleurs avec la DGAC pour �dicter prochainement des r�gles de bonne conduite dans ce domaine.
La r�daction vous conseille
