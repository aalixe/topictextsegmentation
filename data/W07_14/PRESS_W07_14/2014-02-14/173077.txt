TITRE: Indon�sie : spectaculaire �ruption du volcan Kelud | Atlantico
DATE: 2014-02-14
URL: http://www.atlantico.fr/pepitesvideo/indonesie-spectaculaire-eruption-volcan-kelud-982005.html
PRINCIPAL: 173070
TEXT:
Indon�sie : spectaculaire �ruption du volcan Kelud
Impressionnant
Publi� le 14 f�vrier 2014
Les cons�quences sont assez importantes : on d�nombre deux morts et l'�vacuation de 200�000 personnes.
RSS
�
L�Indon�sie compte environ 130 volcans actifs : le pays est r�guli�rement frapp� d'�ruptions assez consid�rables. Preuve en est avec le mont Kelud culminant � 1 731 m�tres : ce volcan a fait pr�s de 15 000 morts depuis le 16e si�cle. Ce volcan est donc surveill� comme le lait sur le feu par les autorit�s indon�siennes, d'autant plus que celui-ci est de nouveau entr� en �ruption jeudi soir, projetant des gerbes de cendres et de roches ardentes sur les villages environnants � des kilom�tres � la ronde.
Le bilan est d�j� lourd. En effet, un homme et une femme �g�s d�une soixantaine d�ann�es ont p�ri sous les d�combres de leurs maisons respectives. Autre cons�quence de cette spectaculaire �ruption : 200 000 personnes ont �t� �vacu�es et le trafic a�rien est tr�s fortement perturb� ont confirm� les autorit�s. A noter que d�but f�vrier, un autre volcan indon�sien, le Sinabung, est entr� en �ruption, faisant au moins 16 morts.
�
