TITRE: Une quinzaine de nouveaux partenaires pour Tizen dont ZTE
DATE: 2014-02-14
URL: http://www.lesmobiles.com/actualite/13231-une-quinzaine-de-nouveaux-partenaires-pour-tizen-dont-zte.html
PRINCIPAL: 0
TEXT:
Accueil > Actualit� > Une quinzaine de nouveaux partenaires pour Tizen dont ZTE
Une quinzaine de nouveaux partenaires pour Tizen dont ZTE
Par Samir Azzemou, publi� le
14 f�vrier 2014 � 19h14
PARTAGER
Le premier mobile anim� par le syst�me d�exploitation Tizen se fait de plus en plus d�sirer. Cependant, sa communaut� ne cesse de s�enrichir de membres �clectiques. 15 nouveaux partenaires rejoignent ses rangs, dont un �quipementier, deux op�rateurs, des d�veloppeurs de jeux et un moteur de recherche.
Que se passe-t-il chez Tizen ? D�une part, l�association semble tr�s active. Mais d�autre part, aucun smartphone ne parvient � �tre commercialis�. Repouss� plusieurs fois l�ann�e derni�re, le lancement de l�OS, soutenu principalement par Samsung et Intel, est devenu une Arl�sienne dans la mobilit�. Heureusement, l�association peut compter sur l�afflux r�gulier de partenaires et pas des moindres. Elle annonce d�ailleurs 15 nouveaux noms � son r�pertoire.
ZTE : sp�cialiste des OS alternatifs ?
Parmi ses noms, des entreprises bien connues. Notamment ZTE, l�un des cinq premiers �quipementiers chinois et l�un des dix premiers dans le monde. Opportuniste, ZTE a toujours fait preuve d�une grande ouverture d�esprit. L�ann�e derni�re d�j�, il s�affichait aux c�t�s de Mozilla pour lancer Firefox OS. Est n� de ce partenariat le ZTE Open, petit smartphone entr�e de gamme destin� aux march�s �mergents. Sa pr�sence au sein de l�association n�est qu�une confirmation. La bonne surprise serait de retrouver cette ann�e un mobile Tizen sur le stand de ZTE...
L'Asie : principale partenaire de Tizen ?
Parmi les autres noms de cette liste, citons le moteur de recherche chinois Baidu, le d�veloppeur cor�en de jeu vid�o mobile Gamevil, l�op�rateur japonais Softbank et sa filiale am�ricaine Sprint, ou encore l��diteur de service m�t�o AccuWeather. Remarquez que nombreux sont les entreprises asiatiques qui soutiennent ce projet.
15 qui viennent s'ajouter aux�36�de novembre
Si Samsung �prouve des difficult�s � trouver un partenaire commercial pour Tizen, NTT Docomo ayant repouss� son propre projet avec cet OS , il parvient cependant � trouver des alliances technologiques int�ressantes. Cette nouvelle liste s�ajoute � celle diffus�e en novembre , o� se trouvaient alors Sharp, Panasonic, The Weather Channel, eBay, Here Maps, Trend Micro, McAfee ou encore Konami. Esp�rons que ces alliances finiront par servir � quelque chose...
Tags :
