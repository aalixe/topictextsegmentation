TITRE: Facebook : transsexuel, bisexuel, androgyne... �toffez votre identit� sexuelle
DATE: 2014-02-14
URL: http://www.purebreak.com/news/facebook-transsexuel-bisexuel-androgyne-etoffez-votre-identite-sexuelle/71058
PRINCIPAL: 174135
TEXT:
Facebook : transsexuel, bisexuel, androgyne... �toffez votre identit� sexuelle
8
Facebook : les genres d�barquent sur le r�seau social
Facebook favorise d�sormais la diversit� des genres. Comment ? En proposant une nouvelle option qui permet aux utilisateurs du r�seau social d'�toffer leur identit� sexuelle (transsexuel, bisexuel, androgyne...) autrement qu'avec les traditionnels choix "Homme" ou "Femme".
Si l'on met de c�t� sa politique publicitaire douteuse (comme l'arriv�e des spots vid�os en autoplay ), Facebook a toujours fait en sorte de proposer des innovations qui puissent am�liorer la navigation de ses adeptes. Le r�seau social - de plus en plus boud� par les jeunes - cherche par ailleurs � fid�liser ses utilisateurs, voire d'en attirer de nouveaux, gr�ce � elles, � l'instar d'une fonctionnalit� in�dite que propose depuis peu la plateforme.
Facebook milite pour la diversit� des genres
Les membres du site de Mark Zuckerberg ont la possibilit� d�sormais d'�toffer leur identit� et leur genre. Outre les traditionnelles "Homme" et "Femme", de nouvelles options viennent ainsi de faire leur apparition telles que "transsexuel", "intersexuel", "androgyne" et bien d'autres. Les adeptes de Facebook peuvent �galement choisir comment ils souhaitent �tre d�sign�s. Aux c�t�s de "Il" et "elle" appara�t � pr�sent l'option neutre "on".
Des options limit�es aux anglophones
Pour �tablir cette liste exhaustive, Facebook s'est associ� � diff�rentes associations de d�fense des droits des lesbiennes, gays, bisexuels et transgenre. "Il y a beaucoup de personnes pour qui �a ne va rien changer, mais pour les quelques autres, cela signifie �norm�ment", a expliqu� Brielle Harrisson, ing�nieure chez Facebook. Seule ombre au tableau : ces options ne sont accessibles qu'aux utilisateurs anglophones. Un responsable de Facebook France a n�anmoins confi� qu'un d�ploiement � l'international est pr�vu.
