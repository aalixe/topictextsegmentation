TITRE: Enrico Letta a officiellement démissionné - 7SUR7.be
DATE: 2014-02-14
URL: http://www.7sur7.be/7s7/fr/1505/Monde/article/detail/1793736/2014/02/14/Enrico-Letta-sur-le-point-de-demissionner-officiellement.dhtml
PRINCIPAL: 173700
TEXT:
14/02/14 -  13h51��Source: Belga
� epa.
MISE � JOUR Apr�s seulement dix mois � la t�te de l'Italie, Enrico Letta a officiellement d�missionn� vendredi pour c�der tr�s probablement sa place � Matteo Renzi, dans un de ces coups de th��tre dont la politique italienne a le secret.
Matteo Renzi � photo news.
"Le pr�sident de la R�publique ne peut que prendre acte de la position exprim�e par le pr�sident du Conseil", qui a remis "la d�mission irr�vocable du gouvernement qu'il dirige", a indiqu� un communiqu� de la pr�sidence � l'issue de la rencontre avec Enrico Letta. "Merci � tous ceux qui m'ont aid�, chaque jour comme si c'�tait le dernier", avait auparavant tweet� le Premier ministre.
Ce chr�tien d�mocrate de gauche n'aura pass� que 293 jours � la t�te d'une coalition in�dite gauche-droite mise en place apr�s les l�gislatives, faute d'une majorit� parlementaire claire. Paradoxalement, M. Letta s'en va sur une bonne nouvelle pour l'�conomie: la tant attendue sortie de la r�cession apr�s deux ans de chute ininterrompue du PIB, avec une petite croissance de 0,1% au dernier trimestre.
M�me s'il n'a pas fait formellement acte de candidature, son successeur probable est le fringant maire de Florence Matteo Renzi, le chef de sa formation de centre gauche, le Parti d�mocrate, qui a obtenu un vote �crasant jeudi soir sur une motion demandant d'"ouvrir une phase nouvelle avec un ex�cutif nouveau". Mais personne n'a pu expliquer pourquoi le PD avait fait tomber un gouvernement form� par son ex-num�ro deux pour le remplacer par son num�ro un, avec l'intention de conserver plus ou moins la m�me majorit� parlementaire.
Apr�s la d�mission de M. Letta, le pr�sident Giorgio Napolitano doit proc�der aux consultations des diff�rents partis avant de vraisemblablement choisir M. Renzi pour former un nouveau gouvernement. Il d�butera ses consultations dans l'apr�s-midi pour les conclure samedi, a-t-il indiqu� dans un communiqu�.
Une fois l'ex�cutif constitu�, M. Renzi devra se pr�senter, peut-�tre d�s mardi devant le Parlement, pour un vote de confiance.
Lire aussi
