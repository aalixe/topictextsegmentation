TITRE: Pas de reprise des recrutements de cadres en 2014
DATE: 2014-02-14
URL: http://www.latribune.fr/actualites/economie/france/20140213trib000815222/pas-de-reprise-des-recrutements-de-cadres-en-2014.html
PRINCIPAL: 0
TEXT:
Pas de reprise des recrutements de cadres en 2014
Les perspectives de recrutements des jeunes cadres ne sont pas tr�s bonnes pour 2014
Jean-Christophe Chanut �|�
14/02/2014, 9:29
�-� 607 �mots
Entre 163.500 et 171.200 recrutements de cadres seraient r�alis�s cette ann�e, selon l'Association pour l'emploi des cadres (Apec). Soit une variation comprise entre 0% et +5% par rapport � 2013, une ann�e m�diocre. Les jeunes cadres d�butants n'auront pas le vent en poupe.
sur le m�me sujet
Formation professionnelle: 68% des cadres en profitent... contre 37% des ouvriers
Sans r�elle surprise, il n'y a aucun miracle � attendre cette ann�e sur le march� de l'emploi des cadres... qui va rester atone. Selon l'enqu�te annuelle r�alis�e aupr�s de 11.000 entreprises par l'Association pour l'emploi des cadres (Apec), entre 163.500 et 171.200 embauches de cadres sont attendues cette ann�e.... contre 163.400 en 2013, soit une baisse de 10% par rapport � 2012 . La variation entre 2013 et 2014 serait donc comprise comprise entre... 0% et 5% d'une ann�e � l'autre.
C'est faible. Mais comme l'explique Jean-Marie Marx, directeur g�n�ral de l'Apec:
" Les entreprises continuent d'�tre dans l'attente. Si la reprise se confirme, le volume des recrutements augmentera. Il faut aussi noter que cette enqu�te a �t� r�alis�e en octobre-novembre, soit avant l'annonce du pacte de responsabilit� qui aura peu-�tre des r�percussions positives sur l'embauche de cadres".
�
Peu de recrutements de cadres d�butants
En attendant l'impact du futur "pacte de responsabilit�", on reste loin des volumes de recrutements d'avant la crise financi�re de 2008, quand les entreprises recrutaient plus de 200.000 cadres par an!! Et cela perdurera tant que la reprise ne sera pas affirm�e et que les entreprises manqueront de visibilit�. Selon l'Apec, ce sont notamment les cadres d�butants qui vont p�tir de cette conjoncture morose. Ils seraient entre 33.600 et 38.100 � �tre recrut�s cette ann�e, soit une �volution sur un an comprise entre -9% et + 3%. Les embauches de jeunes repr�senteront ainsi "� peine plus de deux recrutements sur dix.
Les cadres avec une exp�rience de quelques ann�es davantage recherch�s
En revanche, les cadres ayant acquis une exp�rience comprise entre un et cinq ans seront les "plus courtis�s" (53.100embauches possibles) tout comme ceux de 6 � 10 ans d'exp�rience (42.100 recrutements pr�vus). Ces deux cat�gories repr�senteront plus d'une embauche de cadres sur deux cette ann�e. En revanche, les cadres cumulant plus de 15 ans d'anciennet�, par d�finition les plus �g�s, vont �tre eux aussi victime de la morosit� ambiante avec � peine, et au mieux, 18.500 recrutements envisag�s, soit encore moins qu'en 2013.
Concernant l'�volution des effectifs cadres (environ 20% des effectifs salari�s globaux en France), 8% des entreprises pr�voient de les augmenter , un pourcentage �gal � celui de 2013, alors que 6% songent � les diminuer. Par ailleurs, et pour la deuxi�me ann�e cons�cutive, les promotions au statut cadre devraient fl�chir, dans une fourchette comprise entre - 10% et -2%.
Les Services concentreront 7 embauches de cadres sur 10
Si l'on se focalise sur les secteurs, l'Apec souligne que, une nouvelle fois, celui des Services sera le plus porteur, concentrant � lui seul 7 embauches sur 10.. Au total, les entreprises de ce secteur pourraient embaucher jusqu'� 117.500 cadres. Ce sont essentiellement les activit�s "Informatiques et t�l�communications", avec plus de 35.000 recrutements pr�vus, "l'Ing�nierie-recherche et d�veloppement" et, dans une moindre mesure, les "activit�s juridiques, comptables-conseil et gestion des entreprises" qui embaucheront. D'ailleurs dans la fonction informatique le volume d'embauches pourrait d�passer les 36.800 apr�s une ann�e de baisse importante en 2013.La fonction "Commercial" ne se porte pas trop mal non plus avec 34.000 recrutements �voqu�s.
Dans l'industrie, les embauches se stabiliseraient, alors que dans la construction, elles accuseraient une r�gression par rapport � 2013 oscillant entre - 12% et -9%.
L'Ile-de-France moteur
Enfin, sur le plan g�ographique, les r�gions les plus optimistes sont l'Ile-de-France (qui concentre la moiti� des intentions de recrutements), la Champagne-Ardennes et la Bourgogne. Et les moins bien orient�es sont la Haute-Normandie, l'Auvergne et la Bretagne.
Globalement, donc, l'ann�e 2014 devrait constituer un cr� moyen pour l'emploi des cadres. L'Apec s'attend � du mieux en 2015 et surtout en 2016. A v�rifier.
�
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Commentaires
je suis jeune et je me casse �a �crit le 15/02/2014                                     � 11:47 :
J'ai bac +5 depuis 2012, j'ai 1 an d'exp, je cherche � ne pas �tre d�class�...
Ainsi je postule a des emplois jug�s de mon niveau: on me r�pond
1) faire un doctorat
2) trouver du travail moins qualifi�
3) d�sol� il y a trop de candidature , on ne vous retiens pas malgr� votre profil plus qu�int�ressant
Sur ce je fais mon sac et bye bye la France. Je parts sans regret, ce pays se paup�rise personne ne veut regarder cela en face, j'�tais pr�t � payer des imp�ts sans broncher, tant pis pour elle, elle ne veut pas de moi, elle ne m'aura pas.
Votre bulletin de vote �a r�pondu le 15/02/2014                                                 � 12:07:
Gardez votre inscription pour voter en France et utilisez votre bulletin de vote pour �liminer les politicards qui s�ment la mis�re en France depuis 25 ans ... �liminez tout ce qui vient du RPR UMP UDF PS Nouveau Centre UDI ... Ils ont cohabit� et sont tous d'accord entre eux pour servir les requins sur le dos des fran�ais d'en bas.
je suis jeunes et je me casse �a r�pondu le 16/02/2014                                                 � 11:16:
Sachez que je vote (contrairement � ceux de mon �ge) et je continuerai.
N'oubliez pas le FN dans votre liste.
La France � besoin d'un L�on Blum et d'un front populaire or elle tant � vouloir un P�tain qui ne s'est pas priv� d'abolir les institutions et des libert�s fondamentales.
le PS d'aujourd'hui n'a rien de socialiste, il se contre fou des jeunes comme moi et mise tout sur son �lectorat qui dans une soci�t� g�rontocratique sont les vieux et leur pouvoir d'achat...
Je ne la trouve pas tr�s belle cette France o� tout n'est qu'opportunisme et carri�risme; qu'elle continue � se paup�riser intellectuellement tout va tr�s bien.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Destruction d'emplois �a �crit le 14/02/2014                                     � 22:33 :
En 2013, plus de 63 000 entreprises ont fait l'objet de proc�dures collectives, et plus de 50% d'entre elles ont �t� liquid�es ... tirez-en les conclusions qui conviennent.
Et s'agissant des cr�ations nettes d'emplois, voici le r�cit trouv� sur le net, sur Viadeo :
[copi� in extenso] :
Un exemple des dysfonctionnements d�plorables qui peu ou prou tuent les cr�ations d'emplois et entravent le r��quilibrage de la balance commerciale fran�aise qui accumule dramatiquement des d�ficits :
Un porteur de projet a pr�sent� un projet industriel cr�ateur de pr�s d'une centaine d'emplois dont 50% de cadres, ing�nieurs et maitrise, avec un potentiel � l'exportation sup�rieur � 75% ...
- Un cadre de l'ex Oseo ex anvar lui a conseill� de vendre son projet � l'�tranger, ce cadre d'une structure parapublique est toujours en fonction, � la m�me place chez BPIFrance innovation
- le m�me projet est pr�sent� dans une autre structure parapublique pour l'innovation, cette structure convient de l'envoi du projet au format acrobat.pdf pour son �tude,
* ce qui fut fait, exp�di� aux 5 interlocuteurs requis, avec un fichier prot�g� par un mot de passe � l'ouverture, l'exp�diteur demandant � chacun de pr�ciser en retour s'ils souhaitaient recevoir le mot de passe oralement par t�l�phone ou par SMS... (pour des questions de s�curit� sur la propri�t� industrielle, ce mot de passe ne pouvait pas �tre remis avec le fichier dans un m�me envoi)
* aucun destinataire n'ayant donn� suite, ils n'ont donc pas ouvert le dossier pour le consulter.
Ne nous posons plus de question sur l��tat de d�labrement de l�industrie en France.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Nerik92 �a �crit le 14/02/2014                                     � 16:40 :
Heureusement qu'il y a Londres pour nos jeunes qualifies!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
charles �a �crit le 14/02/2014                                     � 11:36 :
Les cadres doivent etres plus reactifs beaucoup d'offres non pourvues en Bulgarie et Albanie
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
sidoni �a �crit le 14/02/2014                                     � 11:33 :
Les cadres doivent maintenant  accepter n'importe quel emploi  et s'ils refusent il faut desormais les radier un cadre doit pouvoir fair un tres bon garcon de cafe
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
PIERRICK �a �crit le 14/02/2014                                     � 9:59 :
Bonjour
votre article ne pr�cise pas s'il s'agit d'embauches nettes. Le remplacement des d�parts � la retraite sont-ils int�gr�s ?
L'auteur �a r�pondu le 14/02/2014                                                 � 10:59:
Non, il ne s'agit pas d'embauches nettes, les remplacements de d�parts � la retraite sont en effet int�gr�s. Ceci dit, en 2013, le march� de l'emploi cadres a �t� cr�ateur net de pr�s de 15.000 postes. On verra en 2014.
Respectueusement et merci de lire La Tribune
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
