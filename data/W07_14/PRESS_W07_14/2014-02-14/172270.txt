TITRE: R�gion | D�explosive, la bouteille devient stup�fiante !
DATE: 2014-02-14
URL: http://www.republicain-lorrain.fr/actualite/2014/02/14/d-explosive-la-bouteille-devient-stupefiante
PRINCIPAL: 0
TEXT:
dans le train paris-venise D�explosive, la bouteille devient stup�fiante !
le 14/02/2014 � 05:00 par Willy GRAFF.
Un individu a �t� plac� en garde � vue pour d�tention et transport de substance explosive, hier au poste de douanes de Vallorbe, � l�issue d�une journ�e d�intenses investigations. Contr�l� dans un train, cet Egyptien �g� de 33 ans transportait une bouteille tr�s suspecte. Une bouteille suspecte, saisie sur un Egyptien dans le Paris-Venise a fait craindre un contenu explosif. Il s�agissait en fait de drogue.
Abonnez-vous au R�publicain Lorrain
