TITRE: Ski alpin - Fabien Saguez : "On doit s�accrocher" - leJDD.fr
DATE: 2014-02-14
URL: http://www.lejdd.fr/Sport/Sports-d-hiver/Ski-alpin-Fabien-Saguez-On-doit-s-accrocher-653013
PRINCIPAL: 174196
TEXT:
Tweet
Saguez�: "On doit s�accrocher"
Apr�s une nouvelle d�ception en super-combin�, le DTN du ski fran�ais veut croire � une r�action de ses prot�g�s
Fabien Saguez, le directeur technique national du ski remobilise ses troupes. (Maxppp)
Les d�mons de Vancouver, o� le z�ro point� su ski alpin avait fait beaucoup parler, commencent � r�der autour des sommets de Rosa Khutor. Quatre courses alpines, aucune m�daille c�t� fran�ais, pas m�me une place dans le Top 15. Le super-combin� de ce vendredi devait pourtant �tre un temps fort avec l�entr�e en lice d�Alexis Pinturault , plac� dans le lot des favoris, et de Thomas Mermillod-Blondin, pas loin d��tre un outsider. Las, aucun n��tait � l�arriv�e.
Du coup, Fabien Saguez, le Directeur technique national du ski, avait la mine des mauvais jours. "C�est la course sur laquelle on avait deux gar�ons qui pouvaient monter sur le podium et c�est une course rat�e. D�s ses deux premiers appuis dans la manche du slalom, on a vu que ce n�est pas du Alexis dans le texte. On ne l�a pas senti d�li�. Le mot d�ordre c�est de repartir � l�entra�nement et de se remobiliser. On va d�abord se concentrer sur le Super-G avec des gar�ons qui ont de r�elles chances. La psychose? Non, il faut rester calme. Mais sur les quatre courses, on n�est pas l� o� on voulait �tre, clairement.�On doit s�accrocher, croire en ce qu�on a fait tout l�hiver. Aux Mondiaux de Garmisch (2011), on avait aussi �t� en difficult� en vitesse et dans le super-combi, mais derri�re on avait bien r�agi. On sera forc�ment dans cet �tat d�esprit l�.
Damien Burnier, � Rosa Khutor (Russie) - Le Journal du Dimanche
vendredi 14 f�vrier 2014
