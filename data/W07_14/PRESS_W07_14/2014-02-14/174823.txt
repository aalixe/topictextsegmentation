TITRE: Fuite d'informations : un ministre allemand d�missionne | Mediapart
DATE: 2014-02-14
URL: http://www.mediapart.fr/journal/international/140214/fuite-dinformations-un-ministre-allemand-demissionne
PRINCIPAL: 174820
TEXT:
14 f�vrier 2014 |� Par La r�daction de Mediapart
Le ministre allemand de l'agriculture est soup�onn� d'avoir fait fuiter en octobre des informations judiciaires sur un ministre potentiel, inqui�t� par la justice, pour �viter sa pr�sence dans le nouveau gouvernement d'Angela Merkel.
