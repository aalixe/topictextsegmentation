TITRE: Windows 8 passe la barre des 200 millions de licences vendues | geeko
DATE: 2014-02-14
URL: http://geeko.lesoir.be/2014/02/14/windows-8-passe-la-barre-des-200-millions-de-licences-vendues/
PRINCIPAL: 173163
TEXT:
Windows 8 passe la barre des 200 millions de licences vendues
Par Etienne Froment post� le 14 f�vrier 2014
Un peu plus d�un an apr�s sa commercialisation, Windows 8 passe la barre des 200 millions de licences vendues � travers le monde. Un succ�s mitig� pour un syst�me d�exploitation qui �tait cens� redonner du souffle � tout un secteur�
� AFP
Attendu comme le messie il y a encore deux ans, Windows 8 n�est pas parvenu � r�tablir l��quilibre sur le march� informatique. En un peu plus d�un an, Microsoft ne serait parvenu � vendre �que� 200 millions de licences. Un succ�s �mitig� qui reste � relativiser.
Par comparaison, Windows 7 avait d�pass� la barre des 240 millions de licences un an apr�s sa commercialisation. En prenant en compte la croissance fulgurante du march� des tablettes et l�essoufflement du march� informatique, les performances de Windows 8 ne sont donc pas aussi mauvaises que certains m�dias le pr�sageaient.
Microsoft, qui se f�licite de ces r�sultats, manifeste cependant un certain m�pris vis-�-vis des critiques, �vitant de noircir le tableau et assurant contre vents et marr�es que Windows se porte bien.
Si les performances de Windows 8 ne sont pas aussi mauvaises que pr�vues, le secteur affiche cependant un net recul qui t�moigne d�un d�sint�r�t croissant du consommateur pour le march� informatique. La popularit� des tablettes tactiles et de syst�mes concurrents comme iOS ou Android sur mobiles ont petit � petit affaibli Microsoft sur le march� grand public, mais l�entreprise am�ricaine reste incontournable aupr�s des entreprises.
Pour limiter la casse, Microsoft mise aujourd�hui sur la prochaine mise � jour de Windows 8, qui devrait permettre aux utilisateurs de lancer leurs applis Windows depuis le bureau classique, et rendre l�interface moderne optionnelle sur les terminaux non-tactiles. Un retournement de veste qui donnera un peu d�air � Microsoft en attendant la conf�rence annuelle du groupe, en avril 2014.
