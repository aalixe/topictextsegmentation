TITRE: Oscar Pistorius sort du silence un an après avoir tué sa petite amie - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-14
URL: http://tempsreel.nouvelobs.com/monde/20140214.OBS6364/oscar-pistorius-sort-du-silence-un-an-apres-avoir-tue-sa-petite-amie.html
PRINCIPAL: 0
TEXT:
Actualité > Monde > Oscar Pistorius sort du silence un an après avoir tué sa petite amie
Oscar Pistorius sort du silence un an après avoir tué sa petite amie
Publié le 14-02-2014 à 07h53
Mis à jour à 08h04
A+ A-
"Je vais porter pour le reste de ma vie la perte de Reeva et le traumatisme total de ce jour-l�", a �crit le champion paralympique sud-africain sur son site internet.�
Le 19 août 2013, Oscar Pistorius a été inculpé d'assassinat et de possession illégale de munitions après la mort par balle de sa petite amie à Pretoria (Themba Hadebe/AP/SIPA).
Le champion paralympique sud-africain Oscar�Pistorius, qui sera jug� en mars pour le meurtre de sa petite amie Reeva Steenkamp , a bris� le silence vendredi 14 f�vrier, exactement un an apr�s l'avoir tu�e, �voquant sa "douleur".
"Aucun mot ne peut bien traduire mes sentiments au sujet de l'accident d�vastateur qui a caus� tant de chagrin pour tout le monde qui aime vraiment, et continue � aimer, Reeva", a �crit Oscar�Pistorius, 27 ans, dans un court message post� sur son site internet .
�
Capture d'�cran du site internet d'Oscar Pistorius .
�
"La douleur et la tristesse, surtout pour les parents, la famille et les amis de Reeva, me consume avec douleur. Je vais porter pour le reste de ma vie la perte de Reeva et le traumatisme total de ce jour-l�", a-t-il ajout�, signant simplement "Oscar".
Le champion double amput�, surnomm� "Blade Runner" � cause de ses proth�ses de carbone, a �galement envoy� vendredi matin son premier tweet depuis l'avant-veille du meurtre : "Quelques mots du fond du coeur", avec un lien renvoyant vers son message.�
A few words from my heart on http://t.co/WNF7RXMNLP
� Oscar Pistorius (@OscarPistorius) February 13, 2014
�
Oscar�Pistorius� a tu� sa petite amie Reeva Steenkamp , un mannequin de 29 ans, aux premi�res heures de la Saint Valentin 2013 dans sa propri�t� de la banlieue de Pretoria.
Le champion affirme l'avoir abattue par accident, la prenant pour un cambrioleur cach� dans les toilettes, tandis que l'accusation croit � un meurtre, affirmant que le couple s'�tait violemment disput� avant le drame.
Le proc�s doit se tenir du 3 au 20 mars � Pretoria , et�Pistorius�s'est entour� d'avocats et d'experts de premier plan pour sa d�fense.
Partager
