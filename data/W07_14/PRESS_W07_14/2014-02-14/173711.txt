TITRE: Arche de Zo�: Breteau et Lelouch condamn�s � deux ans avec sursis en appel - 20minutes.fr
DATE: 2014-02-14
URL: http://www.20minutes.fr/societe/1299358-20140214-arche-zoe-breteau-lelouch-condamnes-a-deux-ans-sursis-appel
PRINCIPAL: 173706
TEXT:
Ils ne retourneront donc pas derri�re les barreaux.
Le logistiecien relax�
La cour d'appel les a  condamn�s pour escroquerie au pr�judice de familles qui comptaient  accueillir les enfants en France. Elle les a aussi jug�s coupables  d'exercice illicite de l'activit� d'interm�diaire � l'adoption, mais  pas, contrairement � la condamnation de premi�re instance, de tentative  d'aide � l'entr�e ou au s�jour de mineurs en situation irr�guli�re.
La cour d'appel de Paris a par ailleurs relax� le logisticien de l'association, Alain P�ligat. L'avocat g�n�ral avait requis une peine de deux � trois ans  de prison sans retour en d�tention contre le couple et demand� une  dispense de peine pour Alain P�ligat. En premi�re instance, ils avaient �t� condamn�s � trois ans de prison, dont deux ferme.
Stopp�e net
L'�vacuation des enfants, affubl�s de faux pansements, avait �t� stopp�e net le 25 octobre 2007 lorsque les b�n�voles de  l'association avaient �t� arr�t�s en route vers l'a�roport d'Ab�ch�,  dans l'est du Tchad, o� ils devaient les faire embarquer � bord d'un  avion. Destination la France o� les attendaient des familles d'accueil.  Au Tchad, L'Arche de Zo� avait cach� aux autorit�s, pour des raisons de  s�curit� selon Breteau, le but ultime de l'op�ration. L'association voulait qu'une fois sur le sol fran�ais, les enfants se voient reconna�tre le statut de r�fugi�.
A l'�poque, plusieurs institutions internationales et  organisations non gouvernementales avaient affirm� que la plupart des  enfants venaient de villages de la r�gion frontali�re entre le Tchad et  le Soudan, autour des localit�s tchadiennes d'Adr� et de Tin�. Leur  enqu�te avait aussi �tabli que la quasi-totalit� des enfants avaient au  moins un parent ou un adulte qu'ils consid�raient comme tel.
Tout comme trois autres b�n�voles, Eric Breteau, Emilie  Lelouch et Alain P�ligat avaient �t� condamn�s au Tchad � huit ans de  travaux forc�s pour tentative d'enl�vement d'enfants. La peine avait �t� commu�e en ann�es de prison en France, avant que le pr�sident tchadien  Idriss Deby ne prononce une gr�ce en leur faveur.
Avec AFP
