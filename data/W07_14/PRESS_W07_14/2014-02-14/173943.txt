TITRE: Android : les nombreuses exigeances de Google aupr�s des fabricants
DATE: 2014-02-14
URL: http://www.zdnet.fr/actualites/android-ce-que-google-exige-des-fabricants-39797832.htm
PRINCIPAL: 173942
TEXT:
RSS
Android : les nombreuses exigeances de Google aupr�s des fabricants
Business : Des documents portant sur l�accord de distribution des applications mobiles de Google r�v�lent les exigences que le g�ant am�ricain impose aux OEM.
Par L'agence EP |
Vendredi 14 F�vrier 2014
Les constructeurs qui veulent int�grer les services et applications de Google sur Android doivent respecter un certain nombre de r�gles tr�s strictes.
C�est ce que r�v�le Ben Edelman, un professeur de la Harvard Business School qui s�est procur� le contrat d�accord de distribution des applications mobiles de Google (Mobile Application Distribution Agreement ou MADA) qui figure dans les pi�ces du proc�s opposant la firme de Mountain View � Oracle.
Pr��minence
Si un fabricant de terminaux Android veut par exemple int�grer Maps, il doit obligatoirement pr�installer toutes les applications Google. Le contrat stipule �galement que les ic�nes des fonctions t�l�phone, recherche et de l�Android Market doivent figurer au moins dans le panneau de lancement adjacent � l��cran d�accueil principal.
On apprend aussi que la fonction t�l�phone et de recherche Google doivent �tre utilis�s par d�faut. Cette pr��minence offre un avantage majeur � Google face � d�autres services.
Selon le professeur Edelman, cela emp�che les fabricants de vendre � d�autres �diteurs des emplacements de premier plan pour leurs applications, ce qui pourrait leur permettre de r�duire leurs co�ts. (Eureka Presse)
Voir aussi notre page
Connectez vous ou Enregistrez-vous pour rejoindre la discussion
?
Rien de choquant l� dessus.
Google fourni d�ja un OS gratuit et open source, donc si un constructeur souhaite int�grer des services Android, il n'y a pas de raison qu'il n'y ai pas de conditions. Les constructeurs sont libres de faire comme Amazon et ses Kindle Fire.
R�pondre
?
@ mrpatator : ce qui est choquant, c'est plut�t qu'on ose encore parler de libert� quand l'utilisation du produit ou du service est conditionn� au respect d'obligations non n�cessaires au fonctionnement.
R�pondre
?
@p�p�75. Google a bien raison de le faire. Il d�pense des million en d�veloppement il faut bien qu'il rentre dans ces co�t. si quelqu'un n'est pas d'accord il n'a qu'a faire comme Amazon.et oublier les mise a jour
R�pondre
?
@ kimbus : c'est sans rapport avec ce que j'ai �crit, qui ne concernait que la question de la libert� des "partenaires" de Google.
Sinon, concernant la justification de ces exigences par le co�t important des d�veloppements, je pense qu'elle est peu pertinente quand on songe que les b�n�fices de l'entreprise ont atteint pr�s d'un milliard de dollars par mois.
Je vois plut�t dans ces exigences un moyen pour Google de s'assurer une situation dominante en s'imposant comme le fournisseur d'un ensemble de fonctions strat�giques, a priori non d�sir�es ou pour lesquelles on ne l'aurait pas choisi.
Pour les OEM, le besoin de pouvoir proposer quelques �l�ments se traduit par une obligation de placer leurs produits sous le contr�le de l'�co-syst�me envahissant et indiscret de la firme, au m�pris de leur libert� de choix, de celles de leurs clients, et des r�gles de la concurrence.
En cela, Google semble suivre les traces de Microsoft...
?
@p�p�75: ce ne sont pas les OS mobiles qui manquent.
Au pire, les constructeurs peuvent encore choisir de prendre une version AOSP, sans services Google. Mais ils choisissent un �cosyst�me Google Android car les services sont populaires et que Google assure des mises � jour r�guli�res de ces services. Il y a un monopole apparent pour cette raison, mais rien n'emp�che les constructeurs de changer d'OS, contrairement � ce que fait Microsoft et ses versions EOM Windows (payante en plus!).
Un partenariat est synonyme de r�gles � suivre de part et d'autre: mise en avant des services Google contre licence � moindre co�t de ces services + OS gratuit.
Quant � la R&D peu couteuse par rapport au CA: le CA 2013 de Google est de 60 milliards, le b�n�fice net de 13 milliards, donc on ne peut pas dire non-plus que les d�pensee de Google sont faibles et la R&D en fait partie.
c'est bien parce qu'Android est li� aux services Google
R�pondre
?
@p�p�75 : Google prend en charge le d�veloppement du produit et la gestion de son marketplace, c'est bien normal qu'il exige des contreparties.
Le Business model de Google, c'est d'offrir gratuitement ses produits pour que chaque utilisation de ses produits lui rapporte de l'argent via la publicit�. Ils ne s'en sont jamais cach� !
Si tout le monde d�sactivait les services de Google, je ne vois pas l'int�r�t de Google � les proposer gratuitement !
Chaque OEM fait comme bon lui semble.
Il y a au moins 5 ou 6 OS mobiles, c'est � eux de voir ...
Je ne vois pas ce qui est choquant dans la demande de Google.
Pourtant, on ne peut pas me reprocher d'�tre pro-Google :-)
R�pondre
?
@ LeClown : Google vit d�j� tr�s bien de la publicit� ind�pendamment de son marketplace. Et elle en vivrait encore tr�s convenablement si tout le monde d�sactivait ce service, comme au temps encore r�cent o� il n'existait pas.
Non, les OEM ne font pas comme bon leur semblent, pas plus que leurs clients.
Tout d'abord, ce sont principalement les besoins en terme de produits tiers et le manque d'interop�rabilit� de ces derniers qui imposent le choix de l'OS. Les utilisateurs, d�veloppeurs et fabricants se voient donc imposer l'OS, non pas du fait de ses caract�ristiques propres, mais � cause des choix ou des contraintes subies (comme ici) par les fournisseurs et utilisateurs d'autres produits.
Avoir le choix entre tout devoir accepter en bloc et aller voir ailleurs (et donc ne pas trouver), cela part du m�me principe que la VENTE LI�E... � ceci pr�s qu'ici on ne paye pas le produit directement ni de la m�me mani�re, mais au final on le paye quand m�me ! Tenez-le vous pour dit, Google ne propose rien vraiment gratuitement.
Ce qui est choquant, c'est qu'aujourd'hui, il n'est pratiquement plus possible d'acheter le smartphone ou la tablette qu'on souhaite sans avoir Android et les services Googles pr�-install� dessus, avec tout ce que cela implique derri�re (il n'y a que quelques appareils chinois bas de gamme qui ne proposent que l'OS et un nombre limit� de services).
La cons�quence, c'est qu'� moins d'�tre un expert pour pouvoir �liminer proprement les logiciels ind�sirables (en admettant que cela soit possible), ces appareils finissent CONTRE NOTRE GR� par �changer des informations (les n�tres) avec la firme am�ricaine.
Les manigances de Google constituent une extorsion, une violation des r�gles de la concurrence, et une atteinte � nos droits fondamentaux.
R�pondre
?
@p�p�75 : "Google vit d�j� tr�s bien de la publicit� ind�pendamment de son marketplace. Et elle en vivrait encore tr�s convenablement si tout le monde d�sactivait ce service, comme au temps encore r�cent o� il n'existait pas."
Et alors ? une entreprise a pour vocation de se faire de b�n�f, encore et toujours plus de b�n�f.
On ne demande pas � Coca-Cola, ou Mac Donald d'�tre moins agressifs commercialement, sous pr�texte qu'ils se font d�j� assez d'argent.
@p�p�75 : "Ce qui est choquant, c'est qu'aujourd'hui, il n'est pratiquement plus possible d'acheter le Smartphone ou la tablette qu'on souhaite sans avoir Android"
Comme pour Microsoft sur PC, un ordinateur sans OS, ce n'est pas un ordinateur. On doit donc vendre un Smartphone avec un OS install�.
Google propose le sien, avec les conditions qui lui conviennent : ce sont aux OEM de voir si la proposition leur conviennent. Si cela ne leur conviennent pas, ils sont libres d'install�s d'autres OS.
D'ailleurs de nombreux OEM, proposent des Smartphones avec des OS diff�rents. Donc, il est faux de dire que l'utilisateur n'a pas le choix.
Et puis les autres OS mobiles mettent aussi leurs services en avant, par d�faut. Ce n'est pas une sp�cificit� de Google.
Ce sont aux autres fournisseurs d'OS de donner envie aux OEM d'installer leur OS plut�t que celui de Google ....
@p�p�75 : "Les manigances de Google constituent une extorsion, une violation des r�gles de la concurrence, et une atteinte � nos droits fondamentaux."
La d�marche de Google a toujours �t� claire. Je ne vois pas trace de manigances.
Sans ses services, je ne vois aucun int�r�t pour Google de supporter Android et son Google Play. Autrement, ils changeraient de Business model, et vendraient leur OS.
On ne peut pas avoir le beurre et l'argent du beurre.
Amazon leur a d�j� fait le coup en d�sactivant leurs services, et il semblerait que Microsoft veuille r��diter l'exploit (pour remplacer l'OS de leur gamme Asha). Du coup, cela peut se comprendre que Google veuille serrer la vis ....
