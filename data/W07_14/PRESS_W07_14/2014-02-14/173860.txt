TITRE: La France renoue timidement avec les cr�ations d'emplois - Lib�ration
DATE: 2014-02-14
URL: http://www.liberation.fr/economie/2014/02/14/la-france-renoue-timidement-avec-les-creations-d-emplois_980237
PRINCIPAL: 173859
TEXT:
Lire sur le readerMode zen
Dans une boulangerie parisienne, le 26 avril 2013.  (Photo Pierre Verdy. AFP)
Au 4e trismestre 2013, dans le secteur marchand, l'�conomie a vu une petite progression des embauches, essentiellement li�e au regain de l�emploi dans l�int�rim.
Pour la premi�re fois depuis d�but 2012, l��conomie fran�aise a renou� avec les cr�ations d�emplois dans le secteur marchand au 4e trimestre 2013, une progression essentiellement li�e � une hausse dans l�int�rim, selon l�Insee.
Sur l�ensemble de l�ann�e, la tendance est rest�e n�anmoins n�gative avec 65�500�emplois d�truits (-0,4�%), selon une estimation provisoire fournie vendredi par l�Insee. Ce chiffre marque toutefois une nette d�c�l�ration par rapport � 2012 o� quelque 112�000 emplois avaient �t� ray�s de la carte.
Au 4e trimestre, 14�700 cr�ations d�emplois nettes ont �t� enregistr�es dans le secteur marchand non agricole. Cette l�g�re progression (+0,1�% par rapport au trimestre pr�c�dent) �est plut�t une bonne nouvelle� car �c�est la premi�re fois depuis le d�but de l�ann�e�2012 que l�on assiste � des cr�ations d�emplois dans ce secteur�, rel�ve Fr�d�ric Tallet, chef de la cellule conjoncture de l�emploi � l�Insee. Elle fait suite � une d�c�l�ration des destructions d�emplois depuis deux trimestres: pr�s de 38�000�postes ray�s de la carte au 2e trimestre, environ 16�000 au 3e.
Regain dans l�int�rim
Cette am�lioration au 4e trimestre est due essentiellement au regain de l�emploi dans l�int�rim, qui a enregistr� une forte progression (+23�900�postes) sur les trois derniers mois de 2013.
L�int�rim, qui est g�n�ralement consid�r� comme un pr�curseur des �volutions � venir du march� du travail, a gagn� au total 35�400�postes en�2013, revenant � son niveau de la mi-2012. �Le r�sultat du c�t� de l�int�rim est plut�t une bonne surprise�, rel�ve Fr�d�ric Tallet. Dans ce secteur, �les cr�ations d�emplois entam�es au d�but 2013 se sont accentu�es au 4e�trimestre�.
Le secteur tertiaire dans son ensemble enregistre une hausse de 0,3�% par rapport au 3e�trimestre.
En revanche, les destructions d�emplois se sont poursuivies au 4e�trimestre dans la construction (-0,4�%) et dans l�industrie (-0,5�%) o� elles ont continu� au m�me rythme que pendant toute l�ann�e 2013, soit environ 15�000 postes par trimestre. �La tendance est � la baisse dans l�emploi industriel depuis le d�but�2000�, rappelle l�Insee.
Au final, hors int�rim, l�emploi dans le secteur marchand a continu� � diminuer au 4e�trimestre (-0,1�%, soit -9�200�postes).
0,3% de croissance, mieux qu�attendu
Cette �claircie au 4e trimestre sur le front de l�emploi co�ncide avec la public ation vendredi par l�Insee de chiffres un peu meilleurs que pr�vus sur la croissance en France. Celle-ci a atteint 0,3�% en 2013, au lieu de 0,2�% attendu par l�Institut de la statistique.
L�investissement, moteur attendu de la reprise, est notamment reparti � la hausse au quatri�me trimestre (+0,6�%) apr�s sept trimestres de recul. �Il y a visiblement un changement de perspective de la part des chefs d�entreprise�, note Philippe Waechter, directeur de la recherche �conomique chez Natixis. �Ils per�oivent la situation �conomique comme un peu meilleure, mais pas encore suffisamment, ce qui est corrobor� par le fait que les cr�ations d�emplois se concentrent essentiellement dans l�int�rim�.
Le gouvernement, qui n�a pas r�ussi � inverser la courbe du ch�mage comme il le voulait � la fin de l�ann�e 2013, est rest� prudent vendredi, soulignant qu�il �fallait aller plus loin� en mati�re de croissance �pour faire reculer le ch�mage�.
Selon les �conomistes, une croissance annuelle d�environ 1,5�% est n�cessaire pour inverser la tendance. En d�cembre, 10�200 nouveaux demandeurs d�emploi sans activit� ont �t� recens�s en m�tropole, portant leur nombre au niveau record de 3,3 millions.
