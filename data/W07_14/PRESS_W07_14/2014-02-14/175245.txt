TITRE: Que faire à Paris ce week-end?
DATE: 2014-02-14
URL: http://www.lefigaro.fr/sortir-paris/2014/02/14/30004-20140214ARTFIG00304-que-faire-a-paris-ce-week-end.php
PRINCIPAL: 175242
TEXT:
Publié
le 14/02/2014 à 18:24
Yoda exposé à la Cité du Cinéma de Saint-Denis (93) dans le cadre de Star Wars Identities . Crédits photo : Frederic Berthet
Partir en quête de son père du côté de Saint-Denis, passer la nuit avec une star hollywoodienne, goûter aux spécialités finlandaises ou danser comme Maya l'abeille.
Publicité
� Sombrer du côté obscur de la force. La date du 15 février est probablement marquée d'une étoile dans tous les agendas des fans de Star Wars . Dès ce samedi, et jusqu'à la fin du mois de juin, curieux et aficionados de la saga crée par George Lucas sont attendus à la Cité du Cinéma de Luc Besson à Saint-Denis pour Star Wars Identities . Sur deux plateaux de tournage de 2000 m², plus de deux cents pièces originales issues des archives de Lucasfilm sont exposées: maquettes, accessoires, costumes, croquis... Novices et connaisseurs s'évaderont devant le vaisseau de course d'Anakin Skywalker, les robes de la Princesse Amidala, ou le cercueil de carbonite de Han Solo. Que la force soit avec vous!
Notre reportage vidéo à Star Wars Identities
Star Wars Identities à la Cité du Cinéma. 20, rue Ampère, Saint-Denis (93). Plateaux 9 et 8. Du 15 février au 30 juin 2014. Tlj de 10h30 à 20h (nocturne ven. jsq à 21h30). Tarifs: 14,90� à 19,90�. Coupe-files: 17� à 22�.
� Passer la nuit avec Johnny Castle. Avec ou sans Valentin(e), venez passer la nuit de samedi à dimanche avec l'idole des jeunes femmes dans les années 90, Patrick Swayze . Le Max Linder diffuse sur son écran géant, dès 23h30, trois films avec l'acteur hollywoodien si charmant: Dirty Dancing (1987), Ghost (1990) et Point Break (1991). Les deux premiers sont des chefs-d'�uvre de romantisme qui feront fondre les plus durs des c�urs de pierre. Quant au dernier, il propose de reluquer Swayze et Keanu Reeves en surfeurs ... What else ?
Nuit Patrick Swayze au Max Linder. Samedi 15 février à 23h30. Tarif: 15� (+1� résa). Surprise + goodies, café à volonté et petit-déjeuner inclus. Réservez vos places avec Fnac.com
� Devenir chef d'un jour. Venu d'Helsinki, le «Restaurant Day» commence, pour sa troisième édition, à se faire une petite place en France. Le principe? Dans le monde entier, le public gourmand est invité à ouvrir un restaurant éphémère de rue le temps d'une journée et à s'inscrire en ligne pour qu'on le localise. Pour soutenir cette manifestation participative, l'Institut finlandais a fait appel aux bonnes volonté pour vous faire découvrir la cuisine nordique. Rendez-vous donc dans ses locaux pour goûter Lihapyörykät (boulettes de viande), Laskiaispulla (brioches de Mardi gras) et autres Glögi (vin chaud). De quoi réchauffer les c�urs en cette période hivernale!
Restaurant Day à l'Institut finlandais. 60, rue des Écoles, Ve. Dimanche 16 février, de 11h à 17h.
� Buzzer comme une abeille. Les rois de la nuit se retrouveront samedi soir au Centquatre, pour un Carnaval électro qui s'annonce déjanté. Pour fêter les dix ans du N.A.M.E festival , dress-code jeune et noir conseillé, et DJ sets pointus jusqu'à l'aube - avec l'ambassadrice de l'électro berlinoise Ellen Allien, le quatuor lillois Rocky, le performeur londonien Jonny Woo, les DJ's d'Art Point M, Péo Watson et Toilet Disco. Pour faire une pause entre deux danses, jeux gonflables, cabines de relooking, attraction visuelle et autres espaces lounge seront installés dans la halle Curial. Avis aux fêtards.
Carnaval électro au 104. 5, rue Curial, XIXe. Samedi 15 février de 21h à 5h. Tarifs: 15-20�.
