TITRE: Sotchi 2014. Joubert termine sa carri�re sans m�daille aux JO
DATE: 2014-02-14
URL: http://www.leparisien.fr/JO-hiver/sotchi-2014-en-direct/en-direct-sotchi-2014-pinturault-un-espoir-de-medaille-au-super-combine-14-02-2014-3588403.php
PRINCIPAL: 172405
TEXT:
Sotchi 2014. Joubert termine sa carri�re sans m�daille aux JO
�
Sotchi (Russie), vendredi. Brian Joubert a pr�sent� le dernier programme libre de sa carri�re lors de ces JO. Il ne sera pas sur le podium. | AFP/Yuri Kadobnov
1/22
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
EN DIRECT. Sotchi 2014. Super-G : la deuxi�me chance de Marie Marchand-Arvier
Septi�me apr�s le court jeudi, le Poitevin de 29 ans n'a pas r�alis� l'exploit qui lui aurait permis de monter sur le podium.
Plus t�t dans la journ�e,�Alexis Pinturault, 22 ans, a manqu� ses grands d�buts sur la sc�ne olympique. Le Fran�ais, 23e du super-combin� apr�s la descente, est parti � la faute dans le slalom, qui est pourtant sa sp�cialit�. Le meilleur Tricolore, Adrien Th�aux, termine 17e.�
Au biathlon, Ana�s Bescond a pris la 5e place du�15 km individuel dames. Elle peut nourrir quelques regrets car elle n'�tait pas loin de monter sur le podium. L' �quipe de France reste ce vendredi soir bloqu�e � quatre m�dailles (2 en or et 2 en bronze).
Le tableau des m�dailles � l'issue de la journ�e de vendredi
>>�Revivez la journ�e minute par minute :
20h40. C'est fini pour aujourd'hui. Merci de nous avoir suivi ! A demain !
20h25. Saut � ski.�Ronan Lamy Chappuis qualifi� pour la finale. Le cousin du porte-drapeau fran�ais Jason Lamy-Chapuis, a termin� vingt-huiti�me des qualifications ce vendredi soir, gr�ce � un saut � 121,5m. Il participera donc samedi � la finale du concours sur le grand tremplin (HS 140). La qualification a �t� remport�e par l'Autrichien Michael Hayboeck (131m).
20h06. Patinage artistique (programme libre) : Yuzuru Hanyu m�daille d'or, Patrick Chan en argent et Denis Ten en bronze. Jason Brown n'est que 9e.
20h04. Patinage artistique (programme libre) :��Je suis extr�mement fier�, explique Patrick Chan au micro de France T�l�visions.��J'�tais vraiment nerveux�, avoue celui qui �tait arriv� � Sotchi dans la peau du grand favori. Il devra certainement se contenter de l'argent.
19h57. Patinage artistique (programme libre) : Peter Liebers 8e avec un total de 239.87. Il ne reste plus qu'un seul concurrent : l'Am�ricain Jason Brown, 6e du programme court. Hanyu va vraisemblablement devenir champion olympique. Joubert est 12e, Amodio 17e.
19h49. Patinage artistique (programme libre) : Patrick Chan 2e.��I'm sorry guys�, souffle le Canadien � la cam�ra dans la zone du��kiss and cry�. Il obtient 178.10 au libre. Son total de 275.62 lui octroie pour l'instant l'argent.
19h44. Patinage artistique (programme libre) : Patrick Chan en difficult�. Le Canadien, triple champion du monde, met les mains au sol � deux reprises.
19h42. Saut acrobatique f�minin : la B�larusse Alla Tsuper, 34 ans, championne olympique.�La Chinoise Xu Mengtao, championne du monde en titre et vice-championne du monde 2009 et 2011, a pris la m�daille d'argent alors que celle de bronze est revenue � l'Australienne Lydia Lassila, m�daill�e d'or il y a quatre ans � Vancouver (Canada).
19h40. Patinage artistique (programme libre) : Yuzuru Hanyu n�anmoins en t�te ! Avec 178.64 au libre, il obtient un total de 280.09.
Yuzuru Hanyu, 19 ans, lors de son programme libre. (AFP/Yuri Kadobnov.)
19h39. Patinage artistique (programme libre) : Yuzuru Hanyu � terre ! Incroyable ! Le Japonais, 1er du programme court avec un record du monde (101.45), chute deux fois au d�but de son libre. Il se reprend pour finir, mais il pourrait bien �tre d�pass� par le Canadien Patrick Chan, qui va patiner dans quelques minutes.
19h32. Patinage artistique (programme libre) :�Daisuke Takahashi ne sera pas sur le podium. Le Japonais, 4e � l'issue du programme, est provisoirement 4e.
19h21. Patinage artistique (programme libre) : l'Espagnol Javier Fernandez 2e. Le champion d'Europe ne parvient pas � d�loger le Kazah Denis Ten. Il reste cinq concurrents en lice.�
19h04. Saut � ski (grand tremplin) : Ronan Lamy-Chappuis atterrit � 121,5 m. Il est provisoirement 5e des qualifications. Il faut �tre dans les 30 meilleurs pour participer � la finale qui a lieu samedi.
19h01. Patinage artistique (programme libre) : Brian Joubert, c'est 16 m�dailles internationales au compteur. Il a �t� champion du monde en 2007 et champion d'Europe en 2004, 2007 et 2009. Il manquera � ce gigantesque palmar�s une m�daille olympique.
18h55. Skeleton dames : le titre pour la Britannique�Elizabeth Yarnold. Elle devance l'Am�ricaine Noeelle Pikus-Pace et la Russe Elena Nikitina.
18h50. Patinage artistique (programme libre) :��C'est dur tr�s dur, je ne sais pas ce qui s'est pass�, explique Amodio � notre reporter. Le Fran�ais est pass� compl�tement � c�t� de ses JO.
18h43. Patinage artistique (programme libre) : l'�motion de Joubert. Le tout jeune retrait� se dit��assez satisfait� de ses JO au micro de France T�l�visions. �Le programme court �tait tr�s bien, s'est f�licit� le Poitevin. Le d�but du libre �tait bien. Les deux quadruples (ndlr : sauts), �a m'a flingu�, je n'avais plus rien.�J'�tais assez �mu car le public russe a toujours �t� exceptionnel. Il�a toujours �t� cool avec moi. Je dis merci � ma famille (ses yeux rougissent). Ils m'ont toujours soutenu.� Interrog�e sur la suite et notamment sur sa d�cision de patiner en couple ou non, il r�pond :��J'ai besoin de temps, j'ai besoin de me reposer et j'ai un caract�re pas facile. Je ne sais pas si je trouverai une partenaire.�
18h32. Patinage artistique (programme libre) : Brian Joubert seulement 4e ! Le Poitevin n'obtient que 145.93. Avec un total de 231.77, il n'est m�me pas dans le trio de t�te alors que les meilleurs ne sont pas encore pass�s. Dommage !
18h30. Patinage artistique (programme libre) : Brian Joubert en termine. Le Fran�ais a l'air �puis�. Apr�s un d�but de programme r�ussi o� il a notamment encha�n� deux quadruple-sauts, il a petit � petit baiss� pied.
18h28.�Patinage artistique (programme libre) : Brian Joubert pioche un peu.
18h26. Patinage artistique (programme libre) : Brian Joubert d�marre le dernier programme libre de sa carri�re ! S�quence �motion garantie !
18h25. Patinage artistique (programme libre) : Denis Ten prend la premi�re place. Avec 171.04 au programme libre, il totalise 255.10. Place � Brian Joubert !
18h19. Patinage artistique (programme libre) : les douze derniers concurrents vont bient�t patiner. Le premier d'entre eux sera le Kazakh Denis Ten, 9e du programme court. Il sera suivi par Brian Joubert.
17h52. Patinage artistique (programme libre) :��J'ai tout essay�, sanglote Florent Amodio au micro de France T�l�visions. �a ne passe pas aujourd'hui, �a ne passe pas hier. Je suis triste, je pense � mes parents. C'est dur, j'ai beaucoup de mal, je m'en remettrai.�
17h45. Patinage artistique (programme libre) : Tomas Verner provisoirement leader. Le Tch�que �tablit le nouveau total de r�f�rence avec 232.99.
17h42. Biathlon : Martin Fourcade re�oit sa deuxi�me m�daille d'or, obtenue sur les 20�km en individuel,�des mains de Tony Estanguet. Ce dernier, triple champion olympique de cano�-kayak, est d�sormais membre du CIO.
Martin Fourcade re�oit sa m�daille d'or des mains de Tony Estanguet. (France 2)
17h38. Patinage artistique (programme libre) : les larmes d'Amodio. Le Fran�ais �clate en sanglots. Il obtient 123.06 pour ce programme libre. Avec 198.64, il est pour l'instant 4e d'un concours men� par l'Am�ricain Jeremy Abbott.
Florent Amodio en larmes apr�s le programme libre. (France 2)
17h35.�Patinage artistique (programme libre) : Florent Amodio en termine, t�te baiss�e. Le Tricolore n'a jamais r�ussi � se l�cher et a sembl� vivre un calvaire sur la glace.
17h30. Patinage artistique (programme libre) : Florent Amodio sur la glace. 14e � l'issue du programme court, le Fran�ais va essayer d'aller chercher une place d'honneur.
17h15. Patinage artistique (programme libre) : Misha Ge en t�te. Apr�s le passage de sept concurrents, l'Ouzbek est provisoirement 1er avec un total de 203.26.
16h42. Skeleton hommes : le Russe Tretiakov toujours en t�te � l'issue de la 2e manche. Il devance le Letton Dukurs et l'Am�ricain Daly. Les 3e et 4e manche auront lieu samedi.
16h40. Biathlon (15 km individuel) : Ana�s Bescond admet �deux fautes de trop� au micro de France T�l�visions. �Si j'�tais en coupe du monde, je sauterais de joie, ajoute la meilleure Fran�aise, 5e � l'arriv�e. Mais l� j'ai un petit peu de regrets pour le podium. 18/20 au tir, �a ne suffit pas.�
16h35. Biathlon (15 km individuel dames) :�le classement des Fran�aises est connu. Ana�s Bescond est 5e � 2'14''4, Marie-Laure Brunet 17e � 3'54, Marine Bolliet 26e � 4'52''5 et Marie Dorin Habert 39e � 5'46''9.
16h23. Biathlon (15 km individuel dames) : comme Martin Fourcade, Darya Domracheva est d�sormais double championne olympique. D�j� vainqueur mardi de la poursuite, la B�larusse a devanc� la Suissesse Selina Gasparin, 2e � 1'15''7, et une autre B�larusse, Nadezhda Skardino, 3e � 1'38''2. La meilleure Fran�aise, Ana�s Bescond, obtient une belle 5e place. Elle pourra nourrir quelques regrets au tir, o� elle a commis deux fautes. Avec une erreur en moins, elle serait certainement mont�e sur le podium...
Darya Domracheva a survol� le 15 km individuel dames. (AFP/Alberto Pizzoli.)
16h18. Patinage artistique : pendant que les biathl�tes en terminent, les hommes ont commenc� leur programme libre. Florent Amodio, 14e � l'issue du court, et Brian Joubert, 7e, sont les deux Fran�ais en lice.
16h16. Biathlon (15 km individuel dames) : Marie-Laure Brunet 16e avec un 20/20 au tir. La Fran�aise n'a pas �t� assez rapide sur les skis pour se m�ler � la lutte pour les premi�res places. Elle pointe � 3'54'' de la Bi�lorusse Domracheva.
16h12. Biathlon (15 km indivuel dames) : �J'ai rat� ma course�, soupire Marie Dorin Habert au micro de France T�l�visions. La Fran�aise a fini � 5'46''9 de Domracheva. Elle est pour l'instant 28e.
16h06. Biathlon (15 km individuel dames) : Ana�s Bescond provisoirement 5e � 2'14'' de l'intouchable Domracheva ! La Fran�aise s'effondre apr�s la ligne d'arriv�e.
16 heures.�Biathlon (15 km individuel dames) : Ana�s Bescond 6e apr�s le dernier passage devant les cibles. La Fran�aise signe un 18/20 au tir. Elle pointe � 2'02'' de la t�te de course.
15h57.�Biathlon (15 km individuel dames) : Marie-Laure Brunet � 15/15 au tir ! Elle est 14e � 1'18'' de la t�te.
15h54. Biathlon (15 km individuel dames) : Marie-Laure Brunet 16e �1'08''7 apr�s 7,8 km. La Fran�aise r�alise un beau 10/10 au tir.
15h50.�Biathlon (15 km individuel dames) : Marie Dorin Habert 18e apr�s le quatri�me passage devant les cibles. La Fran�aise a commis quatre fautes � la carabine.
15h49.�Biathlon (15 km individuel dames) : Domracheva finit fort ! La Bi�lorusse �tablit le temps de r�f�rence en 43'19''6 gr�ce notamment � un 19/20 au tir.
15h45. Hockey sur glace masculin : Su�de-Suisse 1-0. Dans le groupe C, les Su�dois remportent leur deuxi�me match en autant de rencontres.
15h39. Biathlon (15 km individuel dames) : deux fautes pour Ana�s Bescond ! Deux minutes de p�nalit� pour la Fran�aise... Elle est 20e � 1'36'' apr�s deux passages devant les cibles.
15h35. Saut acrobatique f�minin : les six qualifi�es directement pour la finale connues. Il s'agit de l'Am�ricaine�Ashley Caldwell, de la Chinoise Nina Li, de l'Australienne Danielle Scott, de la Chinoise Shuang Cheng, de l'Am�ricaine Emily Cook et de la Russe�Assoli Slivets (Russie). Les autres participantes effectuent un second run. Il n'y a pas de Fran�aise en lice.
15h34. Biathlon (15 km individuel dames). Kuzmina rate un tir. Elle �cope d'une minute de p�nalit�. La championne olympique de poursuite �tait pourtant bien partie pour virer en t�te lors de ce deuxi�me passage devant les cibles.
15h31.�Biathlon (15 km individuel dames) : Ana�s Bescond � 5/5 au premier tir. Elle est 5e � 7''3�de Kuzmina.
15h29. Biathlon (15 km individuel dames) : Ana�s Bescond 10e � 13'' apr�s 1,8 km.
15h22. Biathlon (15 km individuel dames) : Ana�s Bescond prend le d�part. Cinqui�me du sprint puis douzi�me de la poursuite, elle est la Fran�aise la plus en vue pour l'instant � Sotchi.
15h14. Ski alpin : Felix Neureuther partira samedi pour Sotchi. L'Allemand, l'un des favoris du slalom, a �t� victime d'un accident de voiture ce matin alors qu'il se rendait � l'a�roport de Munich. ��Je vais bien dans les circonstances actuelles. Je m'envole pour Sotchi demain�, a-t-il d�clar� apr�s des examens m�dicaux ayant r�v�l� qu'il souffrait des cons�quences d'un coup du lapin et de probl�mes ligamentaires, selon un communiqu� de la f�d�ration allemande de ski.�Le champion bavarois a pass� dans l'apr�s-midi une IRM au cabinet du docteur Hans-Wilhelm M�ller-Wohlfahrt � Munich o� il s'�tait rendu imm�diatement apr�s son accident matinal.
15h10.�Biathlon (15 km individuel dames) : Marie Dorin Habert s'est �lanc�e.
15 heures. Biathlon (15 km individuel dames) : c'est parti ! Quatre Fran�aises sont en lice : Ana�s Bescond, Maire Dorin Habert, Marie-Laurent Brunet et Marine Bolliet. La premi�re concurrente � s'�lancer est l'Ukrainienne Semerenko.
14h50. Super-combin� : s�ance de rattrapage si vous avez rat� le slalom d'Alexis Pinturault. Le Fran�ais, qui esp�rait d�crocher une m�daille, est parti � la faute et a enfourch�.
VIDEO. Alexis Pinturault enfourche lors du slalom
AUDIO. Pinturault :��Ne pas s'affoler�
14h40. Ski de fond (15 km classique) : le courage du P�ruvien Carcelen. L'athl�te�s'�tait fractur� deux c�tes avant les JO. Il�a donc transform� sa course en tour d'honneur, recevant l'ovation du public et du vainqueur Dario Cologna � l'issue de son effort ponctu� de la derni�re place.�Il a longtemps ram�, tr�buch�, gliss�. Dans les mont�es tr�s exigeantes du parcours, il a m�me tout simplement march� plut�t que ski�.�Mais Roberto Carcelen a franchi la ligne d'arriv�e drapeau p�ruvien en main la ligne d'arriv�e. En 1 h 06 min 28 sec 9/10e, le P�ruvien a termin� � la 87e place.
Roberto Carcelen a franchi la ligne d'arriv�e du 15 km classique drapeau p�ruvien en main. (AFP/Kirill Kudryavtsev.)
14h33. Super-combin� :�Alexis Pinturault n'�tait �pas particuli�rement stress�. �Je ne pars pas tr�s bien, raconte le Fran�ais, �limin� au slalom. Le d�part �tait tr�s compliqu�, avec une premi�re porte tr�s pr�s. Derri�re, du coup, je ne suis pas tr�s bien dans le rythme. Mais, finalement, je me remets plut�t bien. C'est surtout apr�s le premier (ndlr : chrono) interm�diaire que je me remets � skier mieux. Malheureusement, je fais ma faute et c'est la sortie de piste. Je tape la porte dans ma chaussure parce que je suis tr�s pr�cis � cet endroit-l�, et �a me colle les deux pieds. Du coup, dans la double, je n'ai pas le temps de me remettre et j'enfourche. �a fait partie du slalom. Je n'�tais pas particuli�rement stress�. Maintenant, on va se concentrer sur le g�ant et le slalom. Il faut rebondir. La faute de la descente ? Il n'y a pas de question � se poser. Je ne suis pas en bas du slalom et je fais une descente correcte, mais pas exceptionnelle. Du coup, �a peut �tre les deux. Un podium surprenant ? Mais �a ne m'�tonne pas du tout. Les jeux Olympiques, c'est souvent comme �a. Il y en a beaucoup qui arrivent � briller quand on ne les attend pas. Et briller, quand on est attendu �norm�ment, c'est toujours plus compliqu�. C'est pourquoi il faut que vraiment je skie pour moi. Je suis un peu frustr�. Le trac� ? Difficile, oui et non. Il avait peu de rythme. C'est un trac� � la Kostelic, c'est-�-dire que c'est d'un coup tournant, apr�s �a l'est moins, apr�s serr�. �a change sans cesse, sans rythme impos�.�
Pinturault va maintenant se concentrer sur le g�ant et le slalom. (AFP/Alexander Klein.)
14h30. Hockey-sur-glace. Match disput� entre la Suisse et la Su�de. Pas encore de but entre la Suisse et la Su�de, toutes deux en t�te de leur groupe. Il reste la moiti� du match (un tiers de temps de 20 minutes et 10 minutes d'un autre) � disputer.
(AFP/Jonathan Nackstrand)
14h20. A 131,1 km/h sur le skeleton. Le Russe Tretiakov, actuellement en t�te de la 1ere manche de skeleton, a gliss� � une vitesse maximale de 131,1 km/h.
(AFP/Leon Neal)
14 heures. Curling f�minin. Le Danemark s'offre les Etats-Unis. Les Danoises ont battu les Am�ricaines 9-2. Dans les autres matchs, le Royaume-Uni a battu le Japon 12-3, la Chine a battu la Cor�e du Sud 11-3 et la Russie a domin� la Suisse 6-3.
13h50. Le podium du super-combin�. Le champion olympique suisse, Sandro Viletta (au centre), avec le Croate Ivica Kostelic (m�daille d'argent, � gauche) et le m�daill� de bronze, l'Italien Christof Innerhofer.
(AFP/Alexander Klein)
13h23. Super-combin� : Adrien Th�aux se classe 17e. Il est le seul Fran�ais class� dans cette �preuve, puisqu'Alexis Pinturault et Thomas Mermillod-Blondin n'ont pas termin� le slalom.
13h21. Super-combin� : Sandro Viletta champion olympique ! Il devance le Croate Ivica Kostelic et l'Italien Christof Innerhofer. Jansrud, 1er apr�s la descente, termine 4e.
13h19. Super-combin� : Bank 6e. Il ne reste qu'un seul concurrent, le Norv�gien Jansrud.
13h15. Super-combin� : le Norv�gien Kilde chute ! Pour l'instant, le trio de t�te est le suivant : 1. Viletta (Sui) en 2'45''20 ; 2. Kostelic (Cro) � 0''34 ; 3. Innerhofer (Ita) � 0''47.
Le Suisse Sandro Viletta, nouveau champion olympique du super-combin�. (AFP/Fabrice Coffrini.)
13h13. Super-combin� : Svindal tr�s loin. Le Norv�gien est provisoirement 6e.
13h10. Super-combin� : Kostelic 2e � 0''34. Le Croate sourit dans l'aire d'arriv�e. Mais il est clairement d��u.
13h09. Super-combin� : Innerhofer 2e ! Le vice-champion olympique de descente r�alise une jolie manche et finit � 0''47 de Viletta.
13h08. Super-combin� : Alexis Pinturault �forc�ment d��u�. Au d�but, �j'ai repris un peu d'avance, explique le Fran�ais au micro de France T�l�visions. Je passe un peu trop pr�s d'un piquet, (...) je n'arrive pas � m'en d�patouiller, �a me fait enfourcher par la suite. (...) Il ne faut pas s'affoler. Cela reste du sport. �a peut sourire le jour d'apr�s. Quand les choses vont bien, il faut rester calme. Maintenant le g�ant et le slalom. On va essayer de se reposer, de bien s'entra�ner et et d'arriver serein sur les courses.�
13h05. Super-combin� : Carlo Janka 4e. Le Suisse finit � 1''68. Il reste huit candidats.
13h02. Super-combin� : �Je savais qu'il fallait envoyer un gros slalom, je fais une erreur sur le haut, raconte�Mermillod-Blondin au micro de France T�l�visions. (...) Un coup de malchance, pas de vigilance et au tapis direct. Je me suis engag�, je me suis donn�. �a n'a pas voulu rigoler aujourd'hui. Des journ�es comme �a, ce n'est pas simple, surtout aux jeux olympiques.�
13 heures. Super-combin� : Bode Miller 3e � 1''40. L'Am�ricain, tenant du titre, va avoir du mal � garder sa place sur le podium.
12h57. Super-combin� : Sandro Viletta en t�te ! Le Suisse en finit avec 1''14 d'avance sur Zampa.
12h55. Super-combin� : �C'est dommage, on peut nourrir des regrets, avoue Fabien Saguez, l'entra�neur des Fran�ais au micro de France T�l�visions. On avait deux gar�ons dans le coup, c'est une course rat�e.�
12h50. Super-combin� : Adrien Th�aux 6e � 2''32. Le Fran�ais, sp�cialiste de la descente, a logiquement perdu beaucoup de temps dans ce slalom. Il sera n�anmoins le seul Tricolore class�.
12h48. Super-combin� : Ted Ligety largement battu ! L'Am�ricain, l'un des favoris, finit � 1''05 de Zampa. Il est 3e pour l'instant. Il ne montera vraisemblablement pas sur le podium.
12h41. Super-combin� : Pinturault � la faute ! Le Fran�ais, en t�te apr�s les deux premiers temps interm�diaires, a enfourch� une porte. C'est termin� pour lui.�Quelle d�ception pour le ski alpin tricolore.
12h40. Super-combin� : Pinturault au d�part !
12h39. Super-combin� : Koroshilov �choue. Le Russe, sp�cialiste du slalom, rate une porte.
12h38. Super-combin� : le Canadien Pridy � 3''69 du Slovaque Zampa.
12h35. Super-combin� : Thomas Mermillod-Blondin chute ! Le Fran�ais �tait en t�te au deux tiers du slalom. ll est parti � la faute car il a voulu prendre tous les risques. Il reste donc deux Fran�ais : Alexis Pinturault (dossard 8) et Adrien Th�aux (dossard 14).
La chute de Thomas Mermillod-Blondin lors du slalom du super-combin�. (AFP/Dimitar Dilkoff.)
12h34. Super-combin� : le Slovaque Zampa prend la t�te en 2'46''34 (temps cumul�).
12h31. Super-combin� : il fait 11� C dans l'aire d'arriv�e. La temp�rature est moins �lev�e que pr�vu.
12h30. Super-combin� : Kosi premier � s'�lancer au slalom.
12h23. Super-combin� : d�but du slalom dans quelques minutes. Les concurrents vont s'�lancer � partir de 12h30.��Je reste dans la course au moins pour un podium�, a confi� Alexis Pinturault, 23e apr�s la descente. Le Fran�ais partira avec le dossard 8. Cela pourrait �tre un avantage car la piste sera moins creus�e. Ted�Ligety s'est dit, lui, �content de n'�tre pas si loin�.��J'aurais aim� �tre plus proche d'Ivica (ndlr : Kostelic) et grappill� encore un peu plus � Alexis mais je crois que je suis toujours en bonne position�, a expliqu� l'Am�ricain, candidat au titre.
12h20. Ski de fond (15 km classique). Cologna champion olympique. Le Suisse Dario Cologna remporte le titre, devant le Su�dois Johan Olsson et un autre Su�dois, Daniel Richardsson.
12h15. Ski de fond (15 km classique). Le �manque de lucidit� de Gaillard. Interrog� sur France 2, Jean-Marc Gaillard revient sur son erreur de parcours � l'approche de la ligne d'arriv�e. �C'est un manque de lucidit�. Mais sur la course en elle-m�me, ce n'est pas une catastrophe, car je ne jouais pas le podium aujourd'hui. Je pr�f�re �tre au contact avec les autres ; l� (le 15 km classique) est vraiment un effort individuel�, explique-t-il.
12h01. Ski de fond (15 km classique). Gaillard termine. Jean-Marc Gaillard termine � 1'53"1 du Suisse Cologna.
12h01. Ski de fond (15 km classique). Gaillard se trompe. Jean-Marc Gaillard se trompe de chemin � quelques m�tres de l'arriv�e et perd quelques secondes � cause de cette erreur.
11h55. Ski de fond (15 km classique). Cologna prend la t�te. Le Suisse Cologna prend la t�te avec 28"5 d'avance sur le Su�dois Olsson et 39" d'avance sur le Finlandais Niskanen.
11h52. Ski de fond (15 km classique). Backsheider et Miranda d�j� loin. Les Fran�ais Adrien Backscheider et Cyril Miranda ont fini respectivement � plus de trois et quatre minutes du leader provisoire.
11h50. Ski de fond (15 km classique). Court en haut et en bas. Le Norv�gien Jespersen skie en manches courtes, mais aussi en short.
(AFP/Odd Andersen)
11h46. Ski de fond (15 km classique). Niskanen en t�te. Le Finlandais Niskanen a termin� en 39'08"7, un tr�s beau temps de r�f�rence.
11h40. Ski de fond (15 km classique). Le point apr�s 8 km. Apr�s 8 km de course, Gaillard a 59"9 de retard, Backscheider 1'23"5, Miranda 2'16"6.
11h22. Ski de fond (15 km classique). Au tour de Gaillard. Jean-Marc Gaillard prend le d�part.
11h15. Hockey-sur-glace. R�publique tch�que - Lettonie 4-2.�La R�publique tch�que domine la Lettonie. Les Tch�ques ont 3 points, � �galit� avec la Su�de et la Suisse, qui s'affrontent � 13h30. La Lettonie a 0 point.
(AFP/Alexander Nemenov)
11h08. Ski de fond (15 km classique). D�part de Backscheider. Le Fran�ais Backscheider s'�lance.
11h02. Ski de fond (15 km classique). D�part de Miranda. Cyril Miranda est le premier des trois Fran�ais � prendre le d�part.
11h01. Ski de fond (15 km classique).�En manches courtes. L'Autrichien Hauke part en manches courtes en raison des temp�ratures �lev�es.
11 heures. Ski de fond. D�part du 15 km classique. L'Arm�nien Mykayelyan est le premier � s'�lancer.
10h55. Ski de fond. Les Fran�ais du 15 km style classique. Trois Fran�ais prennent le d�part, � 11 heures, du 15 km style classique (sans utiliser le pas de patineur) de ski de fond : Cyril Miranda (dossard 5), Adrien Backscheider (16), Jean-Marc Gaillard (44). 92 fondeurs participent � cette �preuve ; les d�parts sont espac�s de 30 secondes jusqu'� 11h46.
10h35. Bain de soleil. Il fait tellement chaud pour un mois de f�vrier dans la r�gion de Sotchi que spectateurs et membres de l'organisation se d�v�tissent et profitent du soleil.
(AFP/Kirill Kudryavtsev)
(AFP/Kirill Kudryavtsev)
10h15. Hockey-sur-glace. La R�publique tch�que devant la Lettonie. Apr�s la moiti� du match entre la R�publque tch�que et la Lettonie, qui ont toutes deux perdu leur premi�re rencontre, ce sont les Tch�ques qui m�nent 3-2. Il reste un tiers temps de 20 minutes et 10 minutes de jeu.
(AFP/Alexander Nemenov)
10 heures. Slalom. Neureuther accident�. L'Allemand Felix Neureuther, adversaire d'Alexis Pinturault, a eu un accident de voiture sur la route de l'a�roport pour prendre l'avion vers Sotchi, selon l'agence allemande SID. Il aurait d�rap� en raison du verglas. Il passe une visite m�dicale de contr�le. Son arriv�e en Russie est retard�e.
9h50. Bobsleigh. La Jama�que � l'entra�nement. L'�quipe jama�caine messieurs de bob � deux est � l'entra�nement. La mobilisation des internautes a permis de financer le voyage de cette �quipe.
(AFP/Leon Neal)
9h30. Super-combin�. L'atout de Pinturault. Alexis Pinturault attaquera le slalom avec un retard cons�quent de 2"44, mais rattrapable. L'atout qu'il a, en revanche, sera de partir parmi les premiers, avec le dossard 8. La piste de slalom sera moins creus�e par les passages des diff�rents skieurs qu'au moment o� s'�lanceront d'autres favoris, qui partiront apr�s Pinturault.
9h25. Super-combin�. Les tribunes peu garnies. La descente du super-combin� n'a pas attir� les foules.
A la descente du super combin� avec peu de monde dans les tribunes #sotchi2014 pic.twitter.com/YtqB9FlTUR
� Le Parisien Sports (@LeParisienSport) 14 F�vrier 2014
9h20. Super-combin�. Les positions pour le slalom. Thomas Mermillod-Blondin partira avec le dossard 4, Alexis Pinturault avec le dossard 8, Adrien Th�aux avec le dossard 14. Kostelic aura le 24, Bank le 29
9h10. Curling hommes. La Su�de finit par s'imposer. La Su�de l'a finalement emport� 6-5 contre la Chine.
(AFP/Damien Meyer)
Dans les autres rencontres, les Etats-Unis ont battu l'Allemagne 8-5 et le Canada a battu la Norv�ge 10-4.
8h55. Ante Kostelic au trac�. C'est le p�re d'Ivica Kostelic, Ante Kostelic, qui assure la trac� du slalom du super-combin�.
8h40. Slalom du super-combin� : mode d'emploi. Les 30 premiers de la descente partiront par ordre d�croissant, le 30e descendeur commen�ant le slalom, puis le 29e, ainsi de suite jusqu'au premier. Les descendeurs ayant termin� au-del� de la 30e place partiront ensuite.
8h30. Th�aux �hyperd��u�. Adrien Th�aux explique sur France 3 qu'il a r�alis� une descente �pas du tout encourageante. Je reprends du temps en bas, mais �a n'a pas suffi. Je suis hyperd��u, car je sais ce qu'il faut faire, mais j'ai du mal � le mettre en place�. Revenant sur la suite du super-combin� et le slalom qui se d�roulera � partir de 12h30, Th�aux estime que �Kostelic skie un peu moins bien que les autres ann�es en slalom. Mais il va y avoir une belle bagarre�.
8h20. Jusqu� 16 �C. Il fait d�j� 12 �C � Sochi. Lors du slalom, il pourrait faire jusqu'� 16 �C . Ces temp�ratures ont incit� certaines skieuses de fond � concourir en t-shirt jeudi .
8h10. La �grosse erreur� de Mermillod. �Je suis pass� � c�t�. Je fais une grosse erreur. Il va falloir se ressaisir pour le slalom et envoyer un gros gros slalom�, explique Thomas Mermillod-Blondin sur France 3.
7h59. Th�aux � 1"76. Adrien Th�aux termine � 1"76 et prend la 16e place provisoire.
7h57. D�part de Th�aux. Adrien Th�aux est au d�part.
7h55. Pinturault y croit. �J'ai d�j� remont� beaucoup plus en super-combin�. Le terrain va beaucoup jouer�, explique Alexis Pinturault sur France 3.
7h49. Le danger Kostelic. Alexis Pinturault est rel�gu� � 1"51 de Kostelic, m�daill� d'argent aux combin�s olympiques de Turin 2006 et de Vancouver 2012, et sp�cialiste du slalom.
7h47. La performance de Kostelic. Le Croate Ivica Kostelic termine � seulement 93 centi�mes de Jansrud.
(AFP/Olivier Morin)
7h45. La piste se d�grade. Les zones ensoleill�es de la piste commencent � se d�grader.
7h41. Mermillod-Blondin 16e provisoire. Thomas Mermillod-Blondin termine � 2"99 de Jansrud et prend la 16e place provisoire.
7h40. L'�cart de Mermillod-Blondin. Thomas Mermillod-Blondin fait un �cart en milieu de descente et prend beaucoup de retard.
7h39. Mermillod-Blondin en piste.�Thomas Mermillod-Blondin prend le d�part.
7h36. Pinturault � plus de deux secondes. Alexis Pinturault termine � 2"44 de Jansrud et prend la 13e place provisoire. C'est un retard un peu plus important qu'esp�r�, mais qui peut le laisser esp�rer un bon r�sultat apr�s le slalom.
(France T�l�visions)
Pinturault avait d�clar� ne pas vouloir perdre plus de 2 secondes apr�s la descente. Il reste le slalom #Sotchi2014 #supercombin�
� Le Parisien Sports (@LeParisienSport) 14 F�vrier 2014
7h34. Au tour de Pinturault. Alexis Pinturault s'�lance.
(France T�l�visions)
7h24. Svindal 5e. Le Norv�gien Svindal, leader de la Coupe du monde de descente, n'est que 5e provisoire de la descente du super-combin�.
7h18. Jansrud passe devant. Le Norv�gien Jansrud prend la t�te avec 14 centi�mes d'avance sur Bank.
7h15. La Chine menace la Su�de. En curling, en phase pr�liminaire, la Chine m�ne actuellement devant la Su�de, championne du monde en titre chez les hommes.
(France T�l�visions)
7h12. Les dossards des Fran�ais. Pinturault partira avec le dossard 17, Thomas Mermillod-Blondin avec le dossard 19. Adrien Th�aux partira en 26e position.
7h08. Le Tch�que Bank en t�te. Le Tch�que Ondrej Bank r�alise un bon temps en 1'53"38.
7 heures. C'est parti. Le Norv�gien Aleksander Aamodt Kilde est le premier � s'�lancer sur les pentes de la descente.
6h50. Un troisi�me Fran�ais. Adrien Th�aux est le troisi�me Fran�ais engag� dans ce super-combin�. Mais il est sp�cialiste de la descente. Sa meilleure performance en Coupe du monde de super-combin� cette saison est une 13e place.
6h30.�Un autre fran�ais, Thomas Mermillod-Blondin, peut nourrir des espoirs de m�daille. L'opposition s'annonce cependant relev�e avec les Am�ricains Bode Miller et Ted Ligety, mais aussi le Suisse Carlo Janka, l'�tonnant Tch�que Ondrej Bank, le vieux renard croate Ivica Kostelic, le Norv�gien Aksel Lund Svindal ou encore l'Italien Christof Innerhofer, m�daill� d'argent de la descente.
6h20. Le super-combin� est l'�preuve qui r�ussit le mieux � Pinturault depuis le d�but de la saison. Le skieur de Courchevel a remport� celui de Kitzb�hel en Autriche le 26 janvier. Quinze jours plus t�t, il avait sign� une belle deuxi�me place � Wengen en Suisse, seulement battu par l'Am�ricain Ted Ligety.
6h15. Pinturault avoue son impatience. Sur Twitter, le skieur fran�ais a expliqu� jeudi avoir h�te d'entrer en piste � Sotchi.
La flamme olympique devant les sommets de Rosa Kuthor ! :) 1er course demain ! :) Impatient de debuter ces Jeux... http://t.co/PsFMqsN4Lj
� Alexis Pinturault (@AlexPinturault) 13 F�vrier 2014
Les Fran�ais en lice vendredi
Ski alpin, combin� (descente+slalom) messieurs : Alexis Pinturault, Thomas Mermillod-Blondin, Adrien Th�aux.
Biathlon,�15 km individuel dames : Marie Dorin Habert, Ana�s Bescond, Marie-Laure Brunet, Marine Bolliet.
Ski de fond,�15 km classique messieurs : Cyril Miranda, Adrien Backscheider, Jean-Marc Gaillard.
Patinage artistique, programme libre hommes : Florent Amodio, Brian Joubert.
Le calendrier des �preuves
