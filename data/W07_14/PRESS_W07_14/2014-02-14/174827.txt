TITRE: Breivik menace de faire une grève de la faim - 7SUR7.be
DATE: 2014-02-14
URL: http://www.7sur7.be/7s7/fr/13756/Proces-Breivik/article/detail/1793688/2014/02/14/Breivik-menace-de-faire-une-greve-de-la-faim.dhtml
PRINCIPAL: 174826
TEXT:
14/02/14 -  13h25��Source: Belga
� afp.
Le tueur norv�gien d'extr�me droite Anders Behring Breivik menace d'entamer une gr�ve de la faim pour obtenir une am�lioration de ses conditions de d�tention qu'il assimile � de la "torture", faute notamment d'obtenir la console de jeux voulue, ressort-il d'un courrier re�u vendredi par l'AFP.
En plus d'une lettre dactylographi�e dat�e du 29 janvier et exp�di�e semble-t-il � plusieurs r�dactions, le courrier contient quatre pages recto verso envoy�es aux autorit�s carc�rales en novembre dans lesquelles l'extr�miste qui avait tu� 77 personnes le 22 juillet 2011 pose 13 exigences.
Susceptibles selon lui de rendre son s�jour en prison conforme avec la r�glementation europ�enne, ces demandes portent sur des droits fondamentaux, comme la possibilit� de promenade ou de communiquer, et d'autres besoins plus anecdotiques. Il r�clame notamment que la Playstation 2 mise � sa disposition soit remplac�e par la version 3, plus moderne, "avec acc�s � des jeux pour adulte que je peux moi-m�me choisir".
Maintenu � l'isolement depuis 2011 pour des questions de s�curit�, Breivik estime avoir droit � une "offre d'activit�s" am�lior�e par rapport aux autres d�tenus pour compenser son tr�s strict r�gime carc�ral. Il demande aussi le doublement des indemnit�s hebdomadaires de 300 couronnes (36 euros) qu'il re�oit comme tout autre d�tenu, notamment pour payer les frais de port de ses correspondances.
Dans le pli dat� de janvier, il affirme que, faute d'am�lioration v�ritable de ses conditions de d�tention, une gr�ve de la faim semble "une des seules et rares alternatives".
Le 22 juillet 2011, Breivik avait d'abord tu� huit personnes en faisant exploser une bombe pr�s du si�ge du gouvernement � Oslo puis 69 autres, des adolescents pour la plupart, en ouvrant le feu sur un rassemblement de la Jeunesse travailliste sur l'�le d'Utoeya.
