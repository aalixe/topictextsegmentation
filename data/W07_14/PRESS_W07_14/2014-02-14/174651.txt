TITRE: Breivik menace de faire gr�ve de la faim pour une nouvelle console | Pierre-Henry DESHAYES | Tuerie en Norv�ge
DATE: 2014-02-14
URL: http://www.lapresse.ca/international/dossiers/tuerie-en-norvege/201402/14/01-4738944-breivik-menace-de-faire-greve-de-la-faim-pour-une-nouvelle-console.php
PRINCIPAL: 174645
TEXT:
>�Breivik menace de faire gr�ve de la faim pour une nouvelle console�
Breivik menace de faire gr�ve de la faim pour une nouvelle console
Anders Behring Breivik en ao�t 2012.
PHOTO ARCHIVES AFP
Tuerie en Norv�ge
La Norv�ge, l'un des pays les plus s�rs au monde, a �t� frapp�e le 22 juillet par ses plus lourdes attaques depuis la Seconde Guerre mondiale. Une bombe a d'abord explos� au centre-ville d'Oslo, puis une fusillade a tourn� au carnage, sur l'�le d'Utoya, pr�s d'Oslo. �
� lire aussi
Agence France-Presse
Oslo
Le tueur norv�gien d'extr�me droite Anders Behring Breivik menace d'entamer une gr�ve de la faim pour obtenir une am�lioration de ses conditions de d�tention qu'il assimile � de la �torture� faute notamment d'obtenir la console de jeux voulue, dans un courrier re�u vendredi par l'AFP.
En plus d'une lettre dactylographi�e dat�e du 29 janvier et exp�di�e semble-t-il � plusieurs r�dactions, le courrier contient quatre pages recto verso envoy�es aux autorit�s carc�rales en novembre dans lesquelles celui qui avait tu� 77 personnes le 22 juillet 2011 pose 12 exigences.
Susceptibles selon lui de rendre son s�jour en prison conforme avec la r�glementation europ�enne, ces demandes portent sur des droits fondamentaux, comme la possibilit� de promenade ou de communiquer, et des d�tails anecdotiques.
Il r�clame notamment que la PlayStation�2 mise � sa disposition soit remplac�e par la version�3, plus moderne, �avec acc�s � des jeux pour adulte que je peux moi-m�me choisir�.
�Les autres d�tenus ont acc�s � des jeux pour adultes alors que je n'ai le droit de jouer qu'� des jeux pour enfants d'un moindre int�r�t. Un exemple est Rayman Revolution, un jeu (d'aventure dont la suite s'appelle "La grande �vasion", NDLR) con�u pour des enfants de trois ans�, �crit le tueur �g� de 35 ans.
Maintenu � l'isolement depuis 2011 pour des questions de s�curit�, Breivik estime s'�tre comport� �de fa�on exemplaire� et avoir droit � une �offre d'activit�s� am�lior�e par rapport aux autres d�tenus pour compenser son tr�s strict r�gime carc�ral.
� ce titre, il demande le doublement du p�cule hebdomadaire de 300 couronnes (54�$) qu'il re�oit comme tout autre d�tenu, notamment pour payer les frais de port de ses correspondances.
Tous ses courriers sont m�ticuleusement scrut�s et filtr�s par les autorit�s carc�rales, ce qui, d�plore-t-il, ralentit consid�rablement les �changes.
Autres exigences: la fin des fouilles corporelles quasi quotidiennes dans l'une des deux prisons o� il purge sa peine de 21 ans (avec possibilit� de prolongation), l'acc�s � un PC plut�t qu'� une �machine � �crire sans valeur d'une technologie remontant � 1873�, de meilleures possibilit�s de promenades et davantage de contacts avec le monde ext�rieur.
�Vous m'avez soumis � un enfer (...) et je ne parviendrai pas � survivre bien longtemps. Vous �tes en train de me tuer�, �crit-il aux autorit�s carc�rales, agitant d�j� la menace d'une gr�ve de la faim dans cette lettre.
�Si je meurs, tous les extr�mistes et radicaux de droite du monde europ�en sauront pr�cis�ment quels individus m'ont tortur� � mort (...) Cela pourrait avoir des cons�quences pour certains individus � court terme, mais aussi quand la Norv�ge se sera de nouveau dot�e d'un r�gime fasciste d'ici 13 � 40 ans�, met-il en garde, se pr�sentant comme un �prisonnier politique�.
Le 22 juillet 2011, Breivik avait d'abord tu� huit personnes en faisant exploser une bombe pr�s du si�ge du gouvernement � Oslo puis 69 autres, des adolescents pour la plupart, en ouvrant le feu sur un rassemblement de Jeunes travaillistes sur l'�le d'Utoya.
Militant des droits de l'homme
Dans le pli de janvier, le tueur affirme que, faute d'am�lioration v�ritable de ses conditions de d�tention, une gr�ve de la faim semble �une des seules et rares alternatives�.
�La gr�ve de la faim ne s'ach�vera pas avant que le ministre de la Justice (Anders) Anundsen et la directrice du KDI (Direction norv�gienne des affaires p�nitentiaires, NDLR) Marianne Vollan cessent de me traiter plus mal qu'un animal�, ajoute-t-il, pr�cisant qu'il informerait �bient�t� de la date du d�but de son action.
La direction de la prison de haute s�curit� de Skien (sud-est de la Norv�ge), o� il se trouve actuellement, n'a pu �tre jointe pour un commentaire.
Dans sa derni�re lettre, l'extr�miste s'en prend vivement aux m�dias scandinaves accus�s de soutenir sciemment les �supplices� dont il serait victime en ne relayant pas ses protestations, et laisse au passage entendre qu'il est un �militant des droits de l'homme�.
�Vous semblez penser que nous, les militants des droits de l'homme qui nous battons pour ce droit de l'homme fondamental (un suppos� "droit d'autod�termination culturelle", NDLR), (...) sommes des "monstres nazis" qui doivent �tre pouss�s au suicide�, �crit-il.
En janvier 2013, les avocats de Breivik avaient annonc� que celui-ci avait port� plainte pour �torture aggrav�e�. Interrog�e par l'AFP, la police norv�gienne a indiqu� qu'elle devrait se prononcer sur les suites � donner � cette plainte la semaine prochaine.
Partager
