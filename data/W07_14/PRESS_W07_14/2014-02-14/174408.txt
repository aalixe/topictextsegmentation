TITRE: Domracheva copie Fourcade - Sotchi 2014 - Jeux Olympiques -
DATE: 2014-02-14
URL: http://sport24.lefigaro.fr/jeux-olympiques/sotchi-2014/actualites/domracheva-copie-fourcade-679210
PRINCIPAL: 0
TEXT:
Tweeter
Domracheva, biens seule -  Panoramic
La Bi�lorusse s�est par�e d�or dans l��preuve individuelle (15 km), s�offrant un deuxi�me sacre apr�s la poursuite. Comme Martin Fourcade. Ana�s Bescond se classe 5e.
Un doubl� en or. Imp�riale dimanche lors de l��preuve de poursuite (10 km) avec une seule faute au tir, Darya Domracheva� (27 ans, double championne du monde, laur�ate de 15 manches de Coupe du monde) s�est offert une nouvelle m�daille d�or ce vendredi en dominant le 15 km individuel, au terme d�une course de m�tronome.
�
� JO 2014 Sotchi (@_JeuxOlympiques) 14 F�vrier 2014
Solide au tir (1 faute), intouchable au ski de fond. 4e � la sortie du 1ertir � 3���, 7e � la sortie du deuxi�me � 11��, elle a ensuite pris solidement les r�nes de la course, repoussant loin ses dauphines, la Suissesse Selina Gasparin � 1�15�� et sa compatriote Nadezhda Skardino � 1�38��. Toutes deux pourtant cr�dit�es de sans faute au tir�
"Une course rat�e"
� Marie Dorin-Habert
C�t� fran�ais, Ana�s Bescond, d�j� 5e du 7,5 km (sprint) se classe une nouvelle fois 5e (2 fautes au tir). A 2�14��. Si pr�s, si loin. Marie-Laure Brunet, apr�s un nouveau sans faute au tir (aucune cible rat�e depuis le d�but des JO) termine 17e � 3�54��. Marie Dorin-Habert�(39e � 5�46��) a, frustr�e, r�sum� au micro de France Television : �C�est une course compl�tement rat�e. Je suis tr�s d��ue. Je ne m�attendais pas � cela. C�est facile de rater une course, c�est mon cas aujourd�hui. Je pense que je vais rater la mass start (lundi)�� Marine Bolliet se classe 26e � 4�52��.
Classement du 15 km individuel femmes�:
1. Darya Domracheva (Blr) 43�19�� (1 faute au tir)
2. Aita Gasparin (Sui) + 1�15�� (0)
3. Nadezhda Skardino (Blr) + 1�38�� (0)
4. Gabriela Soukalova (Rtc) +1�57�� (2)
5. Ana�s Bescond (Fra) +2�14�� (2)
6. Veronika Vitkova (Rtc) +2�26�� (1)
7. Juliya Dzyhyma (Ukr) +2�30�� (1)
8. Olena Pidhrushna (Ukr) +2�39�� (1)
9. Kaisa Makarainen (Fin) +2�42�� (3)
10. Krystyna Palka (Pol) +3�07�� (0)� 78 class�es.
COMMENTAIRES DES INTERNAUTES ( 0 )
