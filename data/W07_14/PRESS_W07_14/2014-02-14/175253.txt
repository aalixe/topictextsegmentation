TITRE: GRAND FORMAT | Alexis Pinturault rate son premier rendez-vous - France Info
DATE: 2014-02-14
URL: http://www.franceinfo.fr/autres-sports/grand-format-alexis-pinturault-rate-son-premier-rendez-vous-1319155-2014-02-14
PRINCIPAL: 175250
TEXT:
Imprimer
Les �preuves de ski acrobatique f�minin se sont d�roul�es aujourd'hui � AP/SIPA
Alexis Pinturault, pr�sent� comme le favori num�ro un du super-combin�, n'a pas �t� au rendez-vous en ce jour de Saint-Valentin. Apr�s une descente moyenne, il a pris tous les risques dans le slalom. Il a finalement enfourch� une porte. D�ception aussi pour Brian Joubert qui termine hors du podium pour ce qui devrait �tre la derni�re comp�tition de sa longue carri�re. Une journ�e � revivre en image et avec les commentaires de nos reporters sur place.�
Une nouvelle journ�e blanche pour les Bleus. Les Fran�ais n'ont obtenu aucune m�daille ce vendredi. Le grand favori du super-combin�, Alexis Pinturault, n'a pas �t� au rendez-vous pour son entr�e dans les jeux. Rel�gu� � plus de deux secondes � l'issue de la descente, il a manqu� son slalom, sa sp�cialit�.
Alexis Pinturault a en effet enfourch� une porte le plongeant dans un ab�me de d�solation. Il aura d'autres occasions de se rattraper lors de ces jeux. D�solation aussi pour Brian Joubert, qui termine loin du podium. Il �a annonc� qu'il arr�tait sa carri�re sportive.
D�ception, enfin, pour les Fran�aises au biathlon qui terminent aux places d'honneur sur le podium. Une journ�e � revivre dans "Un jour � Sotchi" en cliquant ??? ici o� sur la photo ci-dessous.�
Le patinage artistique n'a pas souri aux Fran�ais �  AP/SIPA
�
