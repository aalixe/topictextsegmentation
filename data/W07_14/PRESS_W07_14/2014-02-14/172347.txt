TITRE: PSA-Renault : trajectoires invers�es pour les deux constructeurs - 14/02/2014 - La Nouvelle R�publique France-Monde
DATE: 2014-02-14
URL: http://www.lanouvellerepublique.fr/France-Monde/Actualite/Economie-social/n/Contenus/Articles/2014/02/14/Trajectoires-inversees-pour-les-deux-constructeurs-1795179
PRINCIPAL: 0
TEXT:
PSA-Renault : trajectoires invers�es pour les deux constructeurs
14/02/2014 05:35
Carlos Ghosn, patron de Renault-Nissan. - (AFP)
Alors que PSA Peugeot Citro�n fait planer de nouvelles incertitudes sur ses emplois en France, Renault rapatrie de l�activit� dans ses usines.
L'usine Renault de Flins va passer de 82.000�� 132.000�v�hicules produits par an. La mont�e en puissance de la future Micra (du partenaire Nissan) confirm�e par Carlos Ghosn confirme les engagements du groupe d'augmenter la production en France suite aux contreparties demand�es aux salari�s. ��Malgr� tous les gains de productivit� que l'on peut faire, �a va �tre difficile de produire 710.000 voitures en France contre 500 00 aujourd'hui sans embauches notamment au niveau des ouvriers dans les usines��, a expliqu� le grand patron.
50 milliards d'euros de CA
Malgr� le gel des activit�s en Iran et de lourdes charges de restructuration, Renault a r�alis� un chiffre d'affaires de 40,9�milliards d'euros en 2013�(en hausse de 0,5 %) pour un r�sultat net de 695�millions d'euros. En dessous de ses objectifs, mais la marque au losange vise tout de m�me un chiffre d'affaires de 50�milliards en 2017.
Renault qui surfe sur les succ�s de la Clio (num�ro�1�en France et num�ro�3�en Europe) ou plus r�cemment du Captur va renouveler en 2014�sa Twingo, son Trafic, son Espace et pr�senter les successeurs de la M�gane et du Sc�nic. Surtout, Renault esp�re beaucoup de sa croissance � l'international et notamment en Chine avec l'implantation d'une nouvelle usine � Wuhan , d'une capacit� de 150.000�v�hicules par an.
>> A LIRE : Le march� de la voiture �lectrique attend l'�tincelle
Du c�t� de l'autre constructeur fran�ais, en revanche, les signaux envoy�s sont moins positifs. M�me si PSA Peugeot Citro�n dans le cadre de son ��nouveau contrat social���a r�affirm� son souhait d'�tre pr�sent en France, sa direction a pr�vu de supprimer une des deux lignes de montage � Poissy. Une annonce qui rappelle le sc�nario d'Aulnay-sous-Bois, site vou� aujourd'hui � la fermeture. Le constructeur s'en d�fend expliquant que le passage en ��monoligne�� se ferait ��sans impact sur la main-d'�uvre de production��.
Selon ce projet toujours � l'�tude, la production de la 208�serait transf�r�e sur la ligne qui produit actuellement la C3 et la DS3 dont la cadence augmenterait pour optimiser la capacit� de production du site. La CGT y voit, elle, ��la suppression de centaines d'emplois suppl�mentaires sur le site de Poissy��.
