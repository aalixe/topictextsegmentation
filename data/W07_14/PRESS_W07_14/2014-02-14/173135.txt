TITRE: Airbus rach�te une banque allemande pour cr�er sa propre banque - Capital.fr
DATE: 2014-02-14
URL: http://www.capital.fr/bourse/actualites/airbus-rachete-une-banque-allemande-pour-creer-sa-propre-banque-910858
PRINCIPAL: 173133
TEXT:
Airbus rach�te une banque allemande pour cr�er sa propre...
Airbus rach�te une banque allemande pour cr�er sa propre banque
Source : Reuters
Valeurs cit�es
NL0000235190 PAR http://bourse.capital.fr/stocks/fiche_valeur.html?ID_NOTATION=
Airbus Group a conclu un accord en vue d'acqu�rir la banque allemande Salzburg M�nchen Bank AG en vue de cr�er son propre �tablissement bancaire. /Photo prise le 3 janvier 2014/REUTERS/Beno�t Tessier
Airbus Group a annonc� vendredi la conclusion d'un accord en vue d'acqu�rir la banque allemande Salzburg M�nchen Bank AG en vue de cr�er son propre �tablissement bancaire.
Une fois l'op�ration boucl�e, la banque, qui est bas�e � Munich, sera renomm�e Airbus Group Bank et aura pour fonction de fournir des solutions de financement � toutes les activit�s du groupe europ�en d'a�rospatiale et de d�fense.
"Airbus Group entend finaliser l'accord dans les meilleurs d�lais en 2014", pr�cise la soci�t� dans un communiqu�, en pr�cisant que la transaction doit encore recevoir l'accord des autorit�s comp�tentes.
Gw�na�lle Barzic, �dit� par Jean-Michel B�lot
� 2014 Reuters - Tous droits de reproduction r�serv�s par Reuters.
