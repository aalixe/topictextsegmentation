TITRE: VOS PAPIERS ! � Deux �tudiants am�ricains ont d�velopp� un vrai Robocop | Big Browser
DATE: 2014-02-14
URL: http://bigbrowser.blog.lemonde.fr/2014/02/14/vos-papiers-deux-etudiants-americains-ont-developpe-un-vrai-robocop/
PRINCIPAL: 174713
TEXT:
VOS PAPIERS ! � Deux �tudiants am�ricains ont d�velopp� un vrai Robocop
Telebot en d�monstration le 12 f�vrier � Miami. (AP Photo/WILFREDO LEE)
Alors que le remake du film Robocop vient de sortir sur les �crans, deux �tudiants du Discovery Lab de l�universit� internationale de Floride , fans de science-fiction, ont mis au point un robot de t�l�pr�sence, destin� � appuyer les forces de l�ordre.
Son petit nom ? Telebot. La b�te mesure 1,82 m�tre pour 34 kilos. Comme l'explique le site Mashable , il se pilote avec un casque Oculus Rift et la personne qui le commande � distance doit �galement rev�tir un gilet, des brassards et des gants.
Ce prototype a pu �tre d�velopp� gr�ce � un don de 20�000 dollars de Jeremy Robins, un officier du corps des marines, afin d'aider ses camarades de combats bless�s en op�ration.
Le projet n�en est toutefois qu�� ses d�buts, comme le souligne Nagarajan Prabakar, professeur au d�partement des sciences informatiques de l'universit� de Floride. La prochaine �tape sera "de cr�er la coque externe du robot et de mettre au point le logiciel de commande et, enfin, de l�essayer sur le terrain".
Ce n'est donc pas demain que vous verrez Telebot arpenter les rues. Mais la robotique est devenue un enjeu majeur dans l'industrie des nouvelles technologies. Google a ainsi rachet� le 13 d�cembre dernier la soci�t� de robotique Boston Dynamics. Cette entreprise am�ricaine, cr��e en 1992, a notamment d�velopp� des prototypes de robots humano�des pour le compte de l'Agence de recherche et d�veloppement de l'arm�e am�ricaine (Darpa)
AMOUR CORSE � Le maire de Propriano, in�ligible, vole au secours de son �pouse, candidate de fa�ade ?
18 commentaires � VOS PAPIERS ! � Deux �tudiants am�ricains ont d�velopp� un vrai Robocop
Au lieu de mettre 20 000 euros dans cette �tude inutile, qui ne menera a rien selon mon humble avis, il aurait fallut investir cet argent ailleurs !
On aurait pu mettre cet argent pour financer la recherche et l�innovation !! De cette fa�on on aurait pu cr�eer des emplois, r�duire les d�ficits et augmenter le smic !!
R�dig� par :
| le 14 f�vrier 2014 � 16:58 | R�pondre Signaler un abus |
Excusez-moi, mais si �a c�est pas de la recherche et de l�innovation, je sais pas ce que c�est. Et de toute fa�on, pourquoi vous inqui�ter vous de l�emploi, du d�ficit et du smic fran�ais alors qu�ils sont am�ricain bossant aux Etats-Unis?
R�dig� par :
Ni�me troll dans la veine du beaugossedu28, pepeto et cie� Lassant !
R�dig� par :
| le 14 f�vrier 2014 � 17:43 | R�pondre Signaler un abus |
Cela ne changera rien �tant donn� que nous avons d�j� � faire � des robots. Les policiers eux-m�mes sont obtus et binaires. De nombreux fran�ais en font l�exp�rience tous les jours entre les amendes inflig�es � tort et � travers, les contr�les et les gardes � vue cibl�s. Ce n�est pas pour rien que ce sont des fonctionnaires. On retrouve la m�me logique chez les enseignants qui fonctionnent en mode arr�t/marche. Il faudrait d�abord penser � humaniser nos fonctionnaires avant de se pr�cipiter � construire des clones informatiques et m�caniques. Mais peut-�tre qu�ils r�flechissent au bilan co�t/avantages.
R�dig� par :
| le 14 f�vrier 2014 � 17:49 | R�pondre Signaler un abus |
Les forces de l�ordre sont comme �a, car c�est la loi qui les y obligent� Et je ne vois le rapport avec les enseignants ???!!! Au derni�res informations, ce n�est pas � eux de faire respecter la loi�
Et vous serez le premier � les appeler si quelqu�un ou quelque chose vous arrive (cambriolage etc�)
R�dig� par :
J�ignore qui se cache derri�re le pseudo ��ang�lique��. Mais il faut reconna�tre que ses commentaires rivalisent de stupidit�.
R�dig� par :
C�est un troll, c�est volontaire. Pour Charles Henri, peut-�tre aussi, mais je n�en suis pas s�r.
R�dig� par :
| le 14 f�vrier 2014 � 21:10 | R�pondre Signaler un abus |
pas tr�s bien inform� le ��journaliste�� qui a �crit ce papier potache.
Le vrai robocop s�appelle Petman et b�n�ficie de 26 millions de dollars de cr�dit de l�arm�e US� Il est vrai qu�il est plus proche de terminator que de robocop qu�il rendrait presque sympathique au demeurant
| le 14 f�vrier 2014 � 18:10 | R�pondre Signaler un abus |
�nerie pour faire r�ver les foules. Il n�est pas arriv� le jour o� l�humain pourra construire un robot qui pense. D�accord, qui pense comme un flic, ce sera le d�but, mais on en est encore loin, tr�s loin.
R�dig� par :
| le 15 f�vrier 2014 � 12:39 | R�pondre Signaler un abus |
Jean pour copier certains flics c�est pas trop compliqu�..les chercheurs ont d�j� incorpor�s dans des ordinateurs des cellules de limaces pour les faire fonctionner�..tu penses qu�ils n�arriveraient pas a construire un robot avec un QI de moule ?���.
R�dig� par :
| le 15 f�vrier 2014 � 13:52 | R�pondre Signaler un abus |
L�a pas l�air tr�s effrayant, leur robocop. Un bon coup de bate de base-ball sur la nuque sans aucun remord et sa petite t�te va embrasser les nuages. Cela dit, j�aime bien son autocollant ��Police�� sur le torse, �a impose le respect. Et puis, il a les m�mes yeux que Carla Bruni.
R�dig� par :
| le 15 f�vrier 2014 � 14:18 | R�pondre Signaler un abus |
en regardant bien la vid�o � partir de 1.38M le robot devance les mouvements du pilote � l�arri�re plan ! regardez bien ! bizard non?  le robot tend son bras m�canique sur son cot� gauche et le pilote a toujours son bras lev� face � lui ! et m�me � partir de 1.31M si vous regardez bien aussi vous remarquerez que les mouvements que fait le pilote n�est pas du tout coh�rent avec celui du robot ! �� je dit sa je dit rien ! mais bon� bizard ! �� apr�s �� personnellement �� je suis contre se genre de chose ! dieux cr�e les dinosaures , dieux d�truit les dinosaures , dieux cr�e l�homme , l�homme d�truit dieux , l�homme cr�e le robot ,�
R�dig� par :
