TITRE: Air France autorise l'usage des terminaux mobiles en mode avion - Le Monde Informatique
DATE: 2014-02-14
URL: http://www.lemondeinformatique.fr/actualites/lire-air-france-autorise-l-usage-des-terminaux-mobiles-en-mode-avion-56583.html
PRINCIPAL: 174677
TEXT:
Air France autorise l'usage des terminaux mobiles en mode avion
Air France n'interdit plus l'usage des smartphones en mode. Cr�dit D.R.
Apr�s la Lufthansa, Air France annonce � son tour qu'il est d�sormais possible d'utiliser les smartphones et les tablettes en mode ��avion�� sur tous ses vols durant les phases de roulage, de d�collage et d'atterrissage. Une d�cision attendue par beaucoup de passagers.
Apr�s les principales compagnies a�riennes am�ricaines, Air France se d�cide � son tour � autoriser l'usage des smartphones et tablettes en mode ��avion�� dans ses a�ronefs durant toutes les phases du vol . C'est � dire qu'il ne sera plus n�cessaire d'�teindre son terminal mobile durant les phases de roulage, de d�collage et d'atterrissage. Si la tablette du si�ge doit toujours �tre rabattue pour des questions de s�curit�, il est d�sormais possible d'�couter de la musique, de prendre des photos ou de consulter un document � condition d'�viter l'utilisation d'accessoires Bluetooth ou WiFi. Casque et clavier filaires uniquement donc.
Jeudi dernier, la compagnie allemande Lufthansa a �galement autoris� l'usage des terminaux mobiles en mode ��avion�� pendant le roulage, le d�collage et l'atterrissage. Ces compagnies a�riennes appliquent en fait, avec un peu de retard, une directive publi�e le 9 d�cembre dernier par l'Agence Europ�enne de S�curit� A�rienne (AESA).
