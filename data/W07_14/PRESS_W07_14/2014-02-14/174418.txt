TITRE: Arr�ter de fumer rend plus heureux
DATE: 2014-02-14
URL: http://fr.canoe.ca/artdevivre/bienetre/article1/2014/02/14/21470656-afp.html
PRINCIPAL: 0
TEXT:
Arr�ter de fumer rend plus heureux
Photo Fotolia�
14-02-2014 | 09h32
Derni�re mise � jour: 14-02-2014 | 09h33
L'arr�t du tabac dope le bien-�tre mental, au moins autant qu'une cure d'antid�presseurs, selon une �tude publi�e vendredi dans la revue m�dicale British Medical Journal (BMJ).
Selon des chercheurs britanniques qui ont pass� en revue 26 �tudes sur le sujet, l'effet d'un arr�t du tabac pourrait �tre ��quivalent ou sup�rieur � celui d'antid�presseurs utilis�s dans le traitement de l'anxi�t� ou des troubles de l'humeur�.
Les fumeurs inclus dans les travaux �taient �moyennement d�pendants�. Ils avaient en moyenne 44 ans et fumaient entre 10 et 40 cigarettes par jour. 48 % �taient des hommes.
Ils avaient �t� interrog�s avant leur tentative d'arr�t du tabac puis � nouveau apr�s, dans un d�lai allant de six semaines � six mois.
Ceux qui avait r�ussi � cesser de fumer �taient moins d�prim�s, moins anxieux, moins stress�s et avaient une vision plus positive de la vie que ceux qui n'avaient pas r�ussi � le faire.
L'am�lioration �tait perceptible m�me chez les personnes atteintes de troubles mentaux d�s lors qu'elles arr�taient de fumer.
En revanche, aucune �valuation de l'�tat mental n'a �t� effectu�e de nouveau par la suite, notamment sur des ex-fumeurs ayant rechut�.
Interrog�e par l'AFP, la coordinatrice de l'�tude, Genma Taylor, de l'Universit� de Birmingham, dit esp�rer que les r�sultats permettront de dissiper quelques id�es fausses, comme celle attribuant au tabac des vertus anti-stressantes ou relaxantes.
�En comparant des non-fumeurs et des fumeurs, on trouve une association avec une moins bonne sant� mentale chez les fumeurs�, a-t-elle ajout�.
Le tabac est d�j� impliqu� dans de nombreux troubles et maladies, comme le cancer, les probl�mes cardiaques ou l'impuissance.
Selon des chiffres fournis en juillet dernier par l'Organisation mondiale de la sant� (OMS), le tabac tuerait pr�s de 6 millions de personnes par an, un chiffre qui devrait atteindre 8 millions de morts en 2030.
