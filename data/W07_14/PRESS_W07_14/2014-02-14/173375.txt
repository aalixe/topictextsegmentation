TITRE: Windows 8 se vend moins bien que Windows 7 en son temps - JDN Web & Tech
DATE: 2014-02-14
URL: http://www.journaldunet.com/solutions/dsi/ventes-windows-8-0214.shtml
PRINCIPAL: 173374
TEXT:
Windows 8 se vend moins bien que Windows 7 en son temps
Derni�re minute
Toute l'actualit� Web & Tech
Microsoft a �coul� 200�millions de licences Windows�8 en 15�mois. Un nombre important mais � relativiser compar� aux 300�millions de licences �coul�es par Windows�7 � p�riode comparable.
Microsoft a r�v�l� le nombre de licences de Windows�8 vendues depuis son lancement. Quinze mois apr�s sa sortie, ce sont donc 200�millions de licences qui ont �t� �coul�es - contre 100�millions en mai 2013 selon TNW . Si ce nombre peut para�tre satisfaisant pour Microsoft, dans la mesure o� le rythme de croissance des ventes sur un an a progress�, il est en revanche quelque peu d�cevant par rapport � ce qu'avait r�alis� en son temps Windows�7 .
A p�riode comparable, c'est-�-dire sur les 15�premiers mois de commercialisation, Windows�7 s'�tait en effet �coul� � 300�millions de copies. Soit 50% de ventes en plus par rapport aux 200�millions de licences Windows�8 �coul�es dans le m�me laps de temps. Le lancement de la prochaine mise � jour de Windows�8.1 , � savoir Windows�8.1 Update�1 , permettra-t-elle � Microsoft de redresser la barre�? A suivre.
Microsoft / Syst�mes d'exploitation
