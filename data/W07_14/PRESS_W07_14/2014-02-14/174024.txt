TITRE: Une autre girafe menacée d'euthanasie au Danemark - Europe1.fr - International
DATE: 2014-02-14
URL: http://www.europe1.fr/International/Une-autre-girafe-menacee-d-euthanasie-au-Danemark-1801083/
PRINCIPAL: 0
TEXT:
Une autre girafe menac�e d'euthanasie au Danemark
Par Anne-Julie Contenay avec AFP
Publi� le 13 f�vrier 2014 � 21h37 Mis � jour le 14 f�vrier 2014 � 15h05
Tweet
Au Danemark, une girafe risque d'�tre euthanasi�e, quelques jours apr�s la mort d'un girafon, qui a suscit� l'indignation. � REUTERS
ANIMAUX - Une autre girafe est menac�e d'euthanasie au Danemark, quelques jours apr�s le scandale suscit� par la mort d'un girafon.
L'INFO. Nouvelle pol�mique en vue au Danemark ? Un zoo danois a indiqu� jeudi qu'il allait peut-�tre devoir euthanasier une girafe en bonne sant� appel�e Marius, quelques jours seulement apr�s l'indignation soulev�e par la mort d'un autre Marius dans un autre zoo danois.
Un patrimoine g�n�tique peu int�ressant. Le zoo Jyllands Park � Videbaek (ouest) envisage cette possibilit� pour cette girafe m�le de sept ans, pour les m�mes raisons: il n'a pas un patrimoine g�n�tique int�ressant. "On ne va pas pouvoir le garder si on obtient une femelle, parce qu'on aurait deux m�les qui se battraient", a expliqu� une gardienne du zoo, Janni L�jtved Poulsen. "On a re�u une girafe m�le qui est haut plac�e d'un point de vue g�n�tique, et tout d�pend du coordinateur de la reproduction quand ils auront une autre girafe pur-sang", a-t-elle ajout�.
Un m�le "pur-sang". Le zoo a adh�r� il y a un peu plus d'un an au programme pour les esp�ces prot�g�es de l'Association europ�enne des zoos et aquariums (EAZA), et re�u dans ce cadre un m�le "pur-sang" en avril 2013. Marius, dont les g�nes ne sont pas suffisamment originaux, doit maintenant trouver un autre zoo ou risque de mourir.
La mort de Marius a fait le tour du monde. Dimanche, la nouvelle de la mort de l'autre Marius a fait le tour du monde, � la plus grande surprise du zoo de Copenhague qui l'a autopsi� devant des visiteurs, dont des enfants. Le zoo avait longuement expliqu� ne pas avoir d'autre choix que de ne pas laisser le girafon devenir adulte, � cause du risque de consanguinit�.
I will never forgive @CopenhagenZoo for exploiting #Marius BETRAYING Marius & lastly murdering Marius for NO reason. pic.twitter.com/4q1j48TgNT
� Amy L. Waz (@amylwaz) February 13, 2014
Des salari�s avaient m�me re�u des menaces de mort.
Le soutien de la SPA danoise. Mais le zoo a re�u mardi le soutien de la plus grande association de d�fense des animaux danoise, Dyrens Beskyttelse, l'�quivalent de la SPA fran�aise, qui a estim� normale la s�lection pour pr�server l'esp�ce.
