TITRE: cyclisme: Tour du Qatar - La derni�re �tape � D�mare, la course � Terpstra -   Sports: D�p�ches - lematin.ch
DATE: 2014-02-14
URL: http://www.lematin.ch/sports/depeches/tour-qatar--derniere-etape-demare-course-terpstra/story/20884035
PRINCIPAL: 174011
TEXT:
Images
Tour du Qatar - La derni�re �tape � D�mare, la course � Terpstra
DOHA, 14 f�v 2014 (AFP) - Le N�erlandais Niki Terpstra (Omega Pharma) a remport� le Tour cycliste du Qatar dont la derni�re �tape est revenue au sprint au Fran�ais Arnaud D�mare (FDJ.fr), vendredi � Doha.
Terpstra �tait en t�te du classement g�n�ral depuis la premi�re des six �tapes de cette �preuve, qu'il avait remport�e au sprint devant ses quatre compagnons d'�chapp�e.
bnl/ol/gv (AFP/Le Matin)
