TITRE: Pinturault a encore une chance avant le slalom du super combin�
DATE: 2014-02-14
URL: http://www.lemonde.fr/jeux-olympiques/article/2014/02/14/pinturault-22e-avant-le-slalom-du-super-combine_4366337_1616891.html
PRINCIPAL: 172815
TEXT:
Sotchi 2014 : Pinturault a encore une chance avant le slalom du super combin�
Le Monde |
� Mis � jour le
14.02.2014 � 11h31
Apr�s la descente de ce matin, c'est le Norv�gien Kjetil Jansrud qui a pris la meilleure option sur la victoire, vendredi 14 f�vrier, lors de l'�preuve du super-combin� (1 min 53 s 24), qui se d�roule dans des conditions plus que printani�res (15�C).
Le v�t�ran Bode Miller (36 ans), champion olympique en titre , est 24e(+ 1 s 43). Son compatriote Ted Ligety, champion olympique quatre ans plus t�t, fait un peu mieux avec une 18e place provisoire (+ 1 s 93).
>> Lire aussi : Alexis Pinturault, aspirant champion
D�j� m�daill� de bronze lors de l'�preuve de descente, dimanche dernier, Kjetil Jansrud devance pour l'instant au classement provisoire le Tch�que Ondrej Bank (+ 0 s 14) et l'Autrichien Matthias Mayer (+ 0 s 37), qui avait, quant � lui, remport� la descente du week-end pass�.
� AU MOINS POUR UN PODIUM��
Les trois Fran�ais engag�s, eux, se classent un peu plus bas : Adrien Th�aux est pour l'instant 17e (+ 1 s 76),� Alexis Pinturault, 23e (+ 2 s 44) et Thomas Mermillod-Blondin, 27e (+ 2 s 99).
Au lendemain de la deuxi�me m�daille d' or de Martin Fourcade en biathlon , Alexis Pinturault faisait partie des grands favoris pour ce super-combin�. ��Je reste dans la course au moins pour un podium��, a d�clar� le skieur de 22 ans, r�solument optimiste.
Revivez la descente du super-combin�
M�me si l'�cart qui le s�pare du trio de t�te est important, le Fran�ais b�n�ficiera d'un l�ger avantage car il s'�lancera parmi les premiers entre les piquets : autrement dit, il profitera d'une neige de qualit�.
Pr�sent pour la premi�re fois aux JO, ce sp�cialiste de super-combin� (il a remport� l'une des deux courses sur le circuit de la Coupe du monde ) repr�sente le principal espoir de m�daille pour le ski alpin fran�ais, rentr� bredouille de Vancouver en 2010.
ACCIDENT DE CIRCULATION POUR NEUREUTHER
Dans le m�me temps, l'agence allemande SID a annonc� que l'un des pr�tendants au titre de slalom (22 f�vrier), Felix Neureuther, avait d� retarder son d�part pour Sotchi en raison d'un accident de la circulation sur le chemin de l'a�roport de Munich.
Sa voiture aurait d�rap� sur du verglas. La date de son d�part pour Sotchi, initialement programm�e � 7 h du matin, d�pendra de la visite m�dicale de contr�le qu'il s'appr�te � passer aupr�s du docteur bavarois Hans-Wilhelm M�ller-Wohlfahrt, tr�s pris� des sportifs.
