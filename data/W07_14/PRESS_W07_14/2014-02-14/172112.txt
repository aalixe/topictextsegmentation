TITRE: La Bourse de New York cl�ture en hausse - Le Figaro Bourse
DATE: 2014-02-14
URL: http://bourse.lefigaro.fr/indices-actions/actu-conseils/la-bourse-de-new-york-cloture-en-hausse-982214
PRINCIPAL: 172110
TEXT:
Graphique
La Bourse de New York cl�ture en hausse
Par Le figaro.fr , AFP agence | Publi� le 13/02/2014 � 12:23 | Mise � jour le 13/02/2014 � 22:45 | R�actions (0)
La Chambre des repr�sentants am�ricaine a approuv� hier au soir le rel�vement du plafond de la dette des �tats-Unis. Cr�dit Photo : � Brendan McDermid / Reuters/REUTERS
Wall Street a choisi d�ignorer de mauvais chiffres �conomiques aux Etats-Unis jeudi: le Dow Jones a progress� de 0,40% et le Nasdaq de 0,94%.
Selon les r�sultats d�finitifs � la cl�ture, le Dow Jones Industrial Average a avanc� de 63,65 points � 16.027,59 points et le Nasdaq , � dominante technologique, a engrang� 39,38 points � 4.240,67 points. L�indice �largi S&P 500 a gagn� 0,58% (+10,57 points) � 1.829,83 points.
Mis sous pression en d�but de s�ance par une salve d�indicateurs am�ricains peu brillants, les indices boursiers new-yorkais ont rapidement repris du poil de la b�te, dans un march� � la recherche d�opportunit�s d�achats � bon compte. �Lorsque que l�on voit � quel point le temps a �t� terrible au cours du dernier mois, les chiffres ne sont certes pas extraordinaires mais ils ne sont pas si catastrophiques�, a comment� Michael James, de Wedbush Securities.
Apr�s deux rapports mensuels cons�cutifs faisant �tat d�un rythme d�embauches d�cevant aux Etats-Unis, les inscriptions hebdomadaires au ch�mage ont affich� une progression sup�rieure aux attentes au cours de la premi�re semaine de f�vrier, s��tablissant � 339.000 contre 335.000 attendues.
D�autre part, les ventes au d�tail ont recul� contre toute attente en janvier, de 0,4%, alors que les analystes misaient sur des ventes stables, et les stocks des entreprises ont gonfl� l�g�rement plus que pr�vu en d�cembre. Cela �indique que les d�penses de consommation �taient plus faibles � tous les niveaux�, ont relev� les analystes du site d�information financi�re Briefing.com. Mais cela ne signifie pas que �l�activit� dispara�t, c�est seulement qu�elle est report�e � demain�, a expliqu� Gregori Volokhine, g�rant du fonds Meeschaert USA. �Les ventes en magasins, la fr�quentation des restaurants, les cr�ations d�emplois sont toutes influenc�es par (...) un hiver exceptionnellement rude�, a-t-il pr�cis�.
En outre, les �r�sultats d�entreprises ont �t� g�n�ralement bons cette semaine, mis � part (l��quipementier en t�l�coms) Cisco� que personne ne regarde plus vraiment comme un barom�tre du secteur technologique, a poursuivi M. James.
Dans ces conditions, apr�s avoir en outre re�u l�assurance cette semaine du maintien du cap ultra-accommodant de la politique mon�taire am�ricaine par la nouvelle pr�sidente de la banque centrale am�ricaine, Janet Yellen , �les investisseurs sont partis � la recherche de bonnes opportunit�s d�achats�, a-t-il ajout�.
Le march� obligataire a progress�. Le rendement des bons du Tr�sor � 10 ans a recul� � 2,736% contre 2,763% mercredi soir et celui � 30 ans � 3,686% contre 3,723% � la pr�c�dente cl�ture.
� lire aussi
