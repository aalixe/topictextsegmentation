TITRE: Venezuela : le gouvernement mobilise peu, petits d�fil�s d'�tudiants opposants | Courrier international
DATE: 2014-02-14
URL: http://www.courrierinternational.com/depeche/newsmlmmd.urn.newsml.afp.com.20140213.6ded8bff.e33a.42f8.98f2.8ecc8741fa89.xml
PRINCIPAL: 172010
TEXT:
Le pr�sident v�n�zu�lien Nicolas Maduro � La Victoria, le 12 f�vrier 2014 AFP
�
�
� Caracas (AFP)
Un appel � la mobilisation lanc� par les autorit�s au Venezuela, en r�ponse aux manifestations d'�tudiants et d'opposants la veille ayant fait trois morts, s'est traduit jeudi par une faible participation.
Des unit�s de la police anti�meute surveillaient les points strat�giques de Caracas tandis que de petits groupes d'�tudiants oppos�s au gouvernement du pr�sident Nicolas Maduro organisaient de petites marches, au lendemain d'une mobilisation de l'opposition qui s'est achev�e dans le sang.
A la mi-journ�e, dans le centre de la capitale, une poign�e de manifestants ayant r�pondu � l'appel des autorit�s � participer � "une marche anti-fasciste" pour d�noncer "la violence de l'opposition" s'�parpillaient dans le calme.
� AFP
Un manifestant au sol alors que la garde nationale se pr�pare � tirer lors d'une manifestation d'opposition � Caracas le 12 f�vrier 2014
A la m�me heure, des journalistes de l'AFP ont observ� un groupe d'environ 200 �tudiants converger vers la place Altamira, dans les quartiers Est, lieu traditionnel des rassemblements anti-chavistes.
Mercredi, des manifestations de milliers d'�tudiants et de militants de l'opposition protestant contre la vie ch�re, l'ins�curit� et les p�nuries avaient �t� organis�es dans plusieurs villes du pays.
Cette marche s'inscrivait dans la continuit� d'un mouvement �tudiant lanc� en province, o� des heurts ont oppos� les �tudiants aux policiers et � des groupes pro-gouvernementaux depuis une dizaine de jours.
La manifestation de Caracas, la plus importante depuis que Nicolas Maduro a succ�d� en mars � Hugo Chavez (1999-2013), s'est achev�e par des affrontements entre jeunes oppos�s au gouvernement et policiers ou groupes "chavistes" qui ont fait au moins trois morts par balles.
- 'Canaliser le m�contentement' -
Les autorit�s ont �galement fait �tat de dizaines de bless�s et d'environ 80 arrestations � travers le pays.
"Nous allons canaliser le m�contentement, mais je ne vais pas vous mentir, les conditions ne sont pas r�unies pour obtenir le d�part du gouvernement", a d�clar� jeudi la principale figure de l'opposition, le gouverneur Henrique Capriles, au cours d'une conf�rence de presse.
"Le gouvernement doit assumer sa responsabilit� de ce qui est s'est pass� hier (mercredi)", a exig� le gouverneur, condamnant les affrontements entre manifestants et partisans du pouvoir.
Nicolas Maduro a donn� l'ordre mercredi soir de renforcer la s�curit� dans les principales villes du Venezuela.
"Il n'y aura pas de coup d'Etat au Venezuela, ayez-en la certitude absolue. La d�mocratie continuera, la r�volution continuera", avait affirm� le pr�sident v�n�zu�lien dans une allocution radio-t�l�vis�e.
� AFP
L'�tudiant Bassil DaCosta, mort par balles lors d'une manifestation d'opposition � Caracas le 12 f�vrier 2014
"S'exprimer n'est pas un coup d'Etat (...) Un civil ne commet pas un coup d'Etat", a r�pliqu� Henrique Capriles jeudi.
Depuis quelques semaines, le gouvernement fait face � une grogne croissante dans un contexte de forte inflation (56,3% en 2013), de p�nuries r�currentes frappant les denr�es alimentaires ou les produits de consommation courante et d'une ins�curit� que les autorit�s ne parviennent pas � juguler.
- Mandat d'arr�t contre un opposant-
Dans le cadre de ce qui ressemble � une contre-offensive apr�s la mobilisation de l'opposition, les autorit�s ont �galement annonc� jeudi l'�mission d'un mandat d'arr�t contre l'un des principaux opposants, Leopoldo Lopez.
M. Lopez, 42 ans, est accus� d'homicide et d'association de malfaiteurs, selon le journal El Universal.
Leader de Volont� populaire, une des principales composantes de la coalition de l'opposition, M. Lopez fait partie d'un petit groupe d'opposants pr�nant la contestation dans la rue, sous le mot d'ordre "La Sortie".
M. Lopez se trouve "chez lui, avec ses avocats. Il reste au Venezuela, et va faire face parce qu'il n'a rien � craindre, parce qu'il va continuer de sortir manifester dans la rue", a affirm� dans la journ�e Carlos Vecchio, un des responsables de son parti.
Mercredi, des jeunes ont incendi� plusieurs v�hicules de police et s'en sont pris aux forces de l'ordre, qui ont r�pliqu� � coups de gaz lacrymog�nes. Un photographe de l'AFP a pu voir des hommes arm�s circulant � moto tirer en direction des manifestants.
Du c�t� des m�dias, la cha�ne de t�l�vision colombienne d'informations NTN24, qui a assur� une large couverture des �v�nements de mercredi, avait disparu des canaux des deux op�rateurs par c�ble qui la diffusaient.
Les autorit�s n'avaient pas r�agi jeudi � ce sujet, mais le Conseil national des T�l�communications avait auparavant menac� de sanctions les m�dias qui feraient "la promotion de la violence".
�
