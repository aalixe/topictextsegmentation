TITRE: EXCLUSIF. Un putsch en préparation chez Veolia | Site mobile Le Point
DATE: 2014-02-14
URL: http://www.lepoint.fr/economie/exclusif-un-putsch-en-preparation-chez-veolia-13-02-2014-1791524_28.php
PRINCIPAL: 173127
TEXT:
13/02/14 à 20h24
EXCLUSIF. Un putsch en préparation chez Veolia
Le clan Dassault intrigue pour installer un haut fonctionnaire de gauche dans le fauteuil d'Antoine Frérot, actuel P-DG. Avec l'espoir de s'attirer les bonnes grâces de l'État ?
Matignon aurait été agacé par l'intransigeance d'Antoine Frérot dans le dossier SNCM. Éric Piermont / AFP
Par Mélanie Delattre et Christophe Labbé
Les agapes auraient dû rester secrètes. En début de semaine, plusieurs membres du conseil d'administration de Veolia se sont réunis en toute discrétion dans un restaurant étoilé parisien. Au menu : la destitution d' Antoine Frérot , PDG du géant de l'eau et des déchets, et son remplacement par un haut fonctionnaire de Bercy.
Officiellement, David Azema, actuel patron de l'Agence des participations d'État, en clair, l'homme qui veille sur l'argent public investi dans les fleurons nationaux n'est candidat à rien. Pourtant, ce discret énarque est depuis peu "sponsorisé" par le clan Dassault pour prendre la place d'Antoine Frérot aux commandes de Veolia. L'avionneur, qui possède moins de 6 % du capital, serait parvenu à convaincre certains administrateurs. Au sein de la multinationale de l'eau, qui emploie 220 000 collaborateurs dans 48 pays, les opérations de déstabilisation se succèdent depuis le départ pour EDF de l'ancien patron Henri Proglio, qui, durant une décennie, a régné sur l'entreprise d'une main de fer.
Coriace
Comme Le Point l'avait raconté, en février 2012 déjà, Antoine Frérot avait fait l'objet d'une tentative avortée de putsch. À l'époque, même s'il l'a toujours nié, Jean-Louis Borloo, l'ancien ministre de l'Écologie, avait tenté de s'installer dans le fauteuil du P-DG, soutenu par le clan Dassault et plusieurs administrateurs historiques. Depuis, d'autres noms de prétendants ont circulé. Parmi eux, Jean-Pierre Denis, un ancien de Veolia, actuellement président du Crédit mutuel Bretagne, Jacques Veyrat, ex-patron du groupe Louis Dreyfus, ou encore Pierre Blayau, finalement nommé au conseil de surveillance d'Areva.
Jusqu'à présent, toutes les tentatives pour se débarrasser du coriace Frérot, qui a su diviser la dette de l'entreprise par deux, ont échoué. Mais cette fois, Matignon, agacé par l'intransigeance du P-DG dans l'épineux dossier du sauvetage de la compagnie de ferries SNCM, pourrait laisser faire. D'autant que "le postulant", David Azema, étiqueté à gauche, est à la fois un proche de Pierre Moscovici et d'Arnaud Montebourg. En offrant à Azema un poste convoité, et au gouvernement une belle prise au sein du CAC 40, Serge Dassault espère peut-être s'attirer une certaine mansuétude de l'État, alors que la semaine judiciaire s'annonce chargée pour lui...
