TITRE: Sursis en appel pour le couple de l'Arche de Zoé | Site mobile Le Point
DATE: 2014-02-14
URL: http://www.lepoint.fr/fil-info-reuters/sursis-en-appel-pour-le-couple-de-l-arche-de-zoe-14-02-2014-1791763_240.php
PRINCIPAL: 173886
TEXT:
14/02/14 à 14h31
Sursis en appel pour le couple de l'Arche de Zoé
PARIS ( Reuters ) - Le président de l'Arche de Zoé et sa compagne ont été condamnés vendredi en appel à deux ans de prison avec sursis pour une tentative de transfert en France d'une centaine d'enfants faussement présentés comme des orphelins du Darfour, en octobre 2007.
Eric Breteau et Emilie Lelouch avaient été condamnés à deux ans de prison ferme en première instance.
Cette condamnation en appel est conforme aux réquisitions de l'avocat général, qui avait demandé une peine aménageable comprise entre deux et trois ans d'incarcération, sans retour en prison.
Le logisticien Alain Péligat, qui avait été condamné à six mois de prison avec sursis en première instance, a été relaxé.
Le ministère public avait requis une dispense de peine à son endroit.
Eric Breteau et Emilie Lelouch, sous contrôle judiciaire depuis avril 2013 après un peu plus de deux mois d'incarcération, s'étaient présentés lors des débats devant la cour d'appel, décidés à s'expliquer après avoir boycotté leur procès en première instance.
Ils ont été reconnus coupables d'exercice illégal de l'activité d'intermédiaire pour l'adoption mais ont été relaxés du chef de tentative d'aide à l'entrée ou au séjour irrégulier de mineurs étrangers.
Les deux animateurs de l'organisation caritative avaient été arrêtés en octobre 2007 au Tchad alors qu'ils s'apprêtaient à emmener en France 103 enfants présentés comme des orphelins du Darfour, une région du Soudan, mais dont la plupart étaient de nationalité tchadienne et n'étaient pas orphelins, selon des associations.
Ils avaient été condamnés au Tchad à huit ans de travaux forcés pour tentative d'enlèvement d'enfants. La peine avait été commuée en années de prison en France, avant que le président tchadien ne prononce une grâce en leur faveur.
Chine Labbé, édité par Sophie Louet
