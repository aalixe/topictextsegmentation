TITRE: "Neknomination" : le nouveau jeu d'alcool qui se propage sur internet - SudOuest.fr
DATE: 2014-02-14
URL: http://www.sudouest.fr/2014/02/10/neknomination-le-nouveau-jeu-d-alcool-qui-se-propage-sur-internet-1457630-5166.php
PRINCIPAL: 0
TEXT:
Un nouveau jeu d'alcool, qui se r�pand via Facebook et YouTube, consiste � se filmer en train de boire un verre d'alcool cul sec. Deux jeunes Irlandais en seraient morts.
Certains boivent une bi�re... d'autre une bouteille enti�re de vodka � Photo
illustration AFP
Publicit�
C
es derniers jours, vous avez peut-�tre vu passer sur votre page d'accueil Facebook des vid�os montrant vos "amis" boire un verre d'alcool cul sec. Ce n'est �videmment pas une co�ncidence : il s'agit d'un jeu, qui se r�pand sur la toile dans plusieurs pays d'Europe. Son nom ? "Neknomination".
Publicit�
Derri�re ce nom un peu barbare se cache l'expression "neck your drink", "bois ton verre cul sec" en argot anglais. Car le ph�nom�ne est bas� sur un concept assez simple : une personne se filme en train de descendre son ou ses verre(s) d'une traite, incite face cam�ra trois de ses "amis" � en faire de m�me dans les 24h qui suivent, poste la vid�o sur Facebook (ou sur une plateforme vid�o type YouTube), puis les amis "nomin�s" perp�tuent le sch�ma.
"Je ne vois que �a quand je me connecte � Facebook", confie Marine, jeune Landaise de 20 ans qui a post� sa propre vid�o, longue d'une trentaine de secondes, samedi matin, au cours de laquelle on la voit se servir et avaler un rhum-coca.
"J'ai �t� d�sign�e par une copine, j'ai voulu le faire parce que j'ai trouv� �a marrant. J'ai laiss� la vid�o pendant tout le week-end pour montrer que je l'avais fait, mais je l'ai retir�e lundi matin parce que j'ai des coll�gues de travail en amis Facebook et je n'ai pas trop envie qu'ils puissent la voir...", explique-t-elle. Elle a �t� "nomin�e" deux autre fois dans la foul�e.
Tout serait parti d'Australie, si l'on en croit le fondateur de la page Facebook "The best Neknominate videos" (supprim�e depuis). Interrog� par le site Vice , Jay Anthony affirme que c'est au cours d'une soir�e � l'universit� de Scotch - �a ne s'invente pas - qu'un �tudiant "a bu sa bi�re cul sec et a dit � son copain�: ��C'est ton tour maintenant.�� C'est devenu une mode, et ensuite quand j'ai cr�� ma page, �a a pris plus d'ampleur", raconte-t-il.
Le ph�nom�ne s'est propag� jusqu'au Royaume-Uni, o� il a pris une tournure dramatique. En Irlande, un jeune de 19 ans a �t� retrouv� noy� dans une rivi�re et un autre de 22 ans est mort dans son appartement. Selon The Irish Independant , ces deux d�c�s seraient directement li�s au jeu.
Dans les pays anglo-saxons, la pratique est plus extr�me. Les quantit�s d'alcool (voire de drogue) ing�r�es sont plus importantes, et les participants vont plus loin qu'une simple vid�o film�e dans une pi�ce. Beaucoup se mettent en sc�ne, parfois � dos de cheval rapportent les Inrocks , ou encore dans la mer, comme le prouve les nombreuses compilations disponibles sur YouTube.�
"Ce jeu est peut-�tre d'abord apparu comme quelque chose de dr�le, mais en r�alit� il s'agit surtout de consommer de l'alcool en trop grande quantit�" a estim� la ministre irlandaise de la protection de l'enfance, Frances Fitzgerald .
Le gouvernement d'Irlande a demand� � Facebook de proscrire toutes les pages li�es au jeu, ce que le r�seau social a refus�, arguant que ses membres �taient libres de partager les contenus de leur choix tant qu'ils ne violaient pas ses r�gles d'utilisation.
