TITRE: ENI : L'Eni promet de r�compenser ses actionnaires, infos et conseils valeur IT0003132476 - Les Echos Bourse
DATE: 2014-02-14
URL: http://bourse.lesechos.fr/infos-conseils-boursiers/infos-conseils-valeurs/infos/l-eni-promet-de-recompenser-ses-actionnaires-951134.php
PRINCIPAL: 0
TEXT:
ENI : L'Eni promet de r�compenser ses actionnaires
13/02/14 � 21:32
- Reuters 0 Commentaire(s)
LEni promet de r�compenser ses actionnaires | Cr�dits photo : Eni
LONDRES, 13 f�vrier (Reuters) - L'Eni s'est engag�e jeudi � davantage r�compenser ses actionnaires et � r�duire ses d�penses au cours des quatre prochaines ann�es, embo�tant ainsi le pas aux autres grandes compagnies p�troli�res europ�ennes en d�pit d'une r�duction de ses objectifs de production.
Pour augmenter le dividende, la compagnie italienne dit viser des cessions d'actifs de neuf milliards d'euros sur quatre ans, et elle n'exclut pas de vendre d'autres participations dans les champs gaziers qu'elle a d�couverts au large du Mozambique.
L'Eni, qui pr�sentait � Londres sa strat�gie pour la p�riode 2014-2017, cible un dividende de 1,12 euro au titre des r�sultats de cette ann�e, apr�s l'avoir port� de 1,08 � 1,10 euro en 2013.
Le groupe, sixi�me "major" mondiale et principale capitalisation boursi�re en Italie , entend r�duire ses d�penses d'investissement de 5% par rapport � son plan pr�c�dent, tout en augmentant la g�n�ration de cashflow.
A Wall Street, l'action cot�e � New York s'adjugeait 1,2% � 46,34 dollars alors qu'� Milan le titre avait cl�tur� en baisse de 0,18% � 16,98 euros.
La pr�vision de hausse de la production a �t� ramen�e � 3% pour la p�riode 2014-2017, � comparer � un objectif de plus de 4% en 2013-2016, pour refl�ter les perturbations au Nigeria et en Libye ainsi que la baisse de la demande de gaz.
Royal Dutch Shell et Total , les principaux concurrents du groupe en Europe, ont eux aussi promis des dividendes en hausse et des d�penses d'investissement en baisse lors d'une saison des r�sultats marqu�e par la stagnation ou la baisse des profits des compagnies p�troli�res des deux c�t�s de l'Atlantique.
L'Eni a r�alis� au quatri�me trimestre un b�n�fice d'exploitation ajust� de 3,52 milliards d'euros, en baisse de 29%, sous le coup des perturbations de production en Afrique et du renforcement de l'euro. (Sarah Young et Giancarlo Navach, V�ronique Tison pour le service fran�ais)
La p�dagogie en association avec
Retrouvez la d�finition de ce(s) mot(s) cl�(s) en Bourse avec notre lexique complet :
