TITRE: VIDEO. Convoqu� devant la justice pour avoir film� Nancy avec un drone
DATE: 2014-02-14
URL: http://www.francetvinfo.fr/faits-divers/video-convoque-devant-la-justice-pour-avoir-filme-nancy-avec-un-drone_529687.html
PRINCIPAL: 0
TEXT:
Tweeter
VIDEO. Convoqu� devant la justice pour avoir film� Nancy avec un drone
Le procureur justifie les poursuites par les risques en cas d'accident, ainsi que par la violation de la vie priv�e que peut entra�ner ce type d'engins.
(MATHIAS SECOND - FRANCE 2)
Par Francetv info avec AFP
Mis � jour le
, publi� le
14/02/2014 | 07:53
Il n'imaginait sans doute pas avoir fait quoi que ce soit de r�pr�hensible. Un jeune homme de 18�ans, qui avait utilis� un drone �quip� d'une cam�ra pour survoler Nancy (Meurthe-et-Moselle) afin de r�aliser un clip post� sur les r�seaux sociaux, a fait l'objet de poursuites pour "mise en danger de la vie d'autrui". Il s'agit d'une premi�re en France, a indiqu� jeudi 13�f�vrier le procureur de la ville. Celui-ci a justifi� les poursuites�par les risques d'accident, ainsi que par la violation de la vie priv�e que pouvait entra�ner ce type d'engins.
"Manifestement, il ne s'est pas bien rendu compte de ce qu'il faisait, mais l'usage de drones est tr�s r�glement�, de m�me que tout a�ronef qui circule dans l'espace a�rien", a expliqu� le magistrat du parquet. Le jeune homme avait tourn� des prises de vues de Nancy fin janvier, notamment de la place Stanislas ainsi que des monuments les plus c�l�bres de la ville lorraine.
Formation obligatoire pour les vid�astes du ciel
Son clip , post� sur diff�rentes plateformes de partage de vid�os, avait �t� vu plusieurs dizaines de milliers de fois par les internautes. Mais son succ�s a alert� les autorit�s, notamment la Direction de l'aviation civile, qui a rappel� � l'auteur de la vid�o les r�gles relatives aux drones civils.
Les utilisateurs doivent notamment suivre une formation similaire � celle des pilotes d'avions et obtenir une autorisation particuli�re lorsqu'ils souhaitent faire survoler un drone au-dessus d'un espace urbain. Apr�s une audition cette semaine par les gendarmes, le jeune homme compara�tra devant le tribunal correctionnel dans les prochains mois pour "mise en danger de la vie d'autrui".
Le clip, post� sur diff�rentes plateformes de partage de vid�os, avait �t� vu plusieurs dizaines de milliers de fois par les internautes. (GOLOX / YOUTUBE)
