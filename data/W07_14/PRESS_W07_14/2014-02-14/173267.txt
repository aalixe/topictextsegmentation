TITRE: Tempête Ulla : un homme meurt au large de la Bretagne - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-14
URL: http://tempsreel.nouvelobs.com/societe/20140214.OBS6362/vents-et-vagues-5-departements-du-nord-ouest-en-viligance-orange.html
PRINCIPAL: 0
TEXT:
Publié le 14-02-2014 à 07h30
Mis à jour à 23h06
Six�d�partements du Nord-Ouest sont en vigilance�orange�vent, vagues et inondation. Des raffales � plus de 120km/h ont d�j� �t� enregistr�es.
Le phare de Le Guilvinec. Photo d'illustration. (JEAN-SEBASTIEN EVRARD / AFP PHOTO)
Un octog�naire est d�c�d� vendredi 14 f�vrier�� bord d'un paquebot navigant au large de la Bretagne � la suite d'une chute, alors que les conditions m�t�orologiques sont particuli�rement difficiles avec des vents violents et une forte houle, a annonc� la pr�fecture maritime de l'Atlantique.
Une femme, � bord du m�me paquebot, a �t� �vacu�e par h�licopt�re � la suite �galement d'une chute, a indiqu� la pr�fecture maritime.�"Les deux personnes ont chut� sans aucun doute en raison des conditions climatiques particuli�rement difficiles", a assur� le capitaine de corvette Karine Foll, de la pr�fecture maritime, indiquant que le vent soufflait sur zone en rafales � plus de 70 noeuds (120 km/h).
Pr�s de 100.000 foyers �taient par ailleurs priv�s d'�lectricit� � 19h30 dans les quatre d�partements bretons en raison de la temp�te Ulla, selon le point effectu� par ERDF.�Selon le d�compte d'Electricit� R�seau distribution France, 50.000 foyers �taient concern�s dans le Finist�re, 40.000 dans les C�tes d'Armor, 4.000 dans le Morbihan et 1.000 en Ille-et-Vilaine.
Six d�partements du nord-ouest -�Finist�re,�C�tes-d'Armor,�Morbihan, �Ille-et-Vilaine,�Loire-Atlantique et Manche - sont plac�s en vigilance orange "vent, vagues-submersion et inondation" par M�t�o France.
La vigilance orange "vent violent" concerne les d�partements du Finist�re, des C�tes d'Armor, du Morbihan et de la Manche.�Le vent de sud puis sud-ouest se renforce cet apr�s-midi, les rafales d�passant par endroits les 100km/h � 110km/h. Mais c'est ce soir que l'on observera les plus fortes rafales : les rafales atteindront g�n�ralement 110 � 120 km/h dans l'int�rieur des terres sur le Finist�re d�partement le plus touch�, 100 � 110km/h sur les C�tes d'Armor, l'ouest du Morbihan ainsi que le d�partement de la Manche. Le long des c�tes de la Cornouaille au Tr�gor, on s'attend � des pointes � 130km/h, jusque 140 km/h � 150 km/h sur les caps les plus expos�s.
Selon�Ouest France , plusieurs chutes d'arbres ont �t� signal�es et�des lignes EDF ont �t� endommag�es�dans les C�tes-d'Armor. A Concarneau, dans le Finist�re, la mer se fait d�j� mena�ante, comme on peut le voir sur cette vid�o post�e sur le site du quotidien r�gional.
Crues et fortes vagues
La vigilance vagues-submersion concerne le�Finist�re, le Morbihan, la Manche et la Loire-Atlantique. Elle est due � une d�pression pr�vue sur�l'Irlande dans la nuit de vendredi � samedi, qui est accompagn�e de vents temp�tueux g�n�rant de tr�s fortes vagues sur la fa�ade atlantique et en entr�e de Manche � partir de vendredi soir.�Cette perturbation sera accompagn�e de forts coefficients de mar�e, pr�cise M�t�o France.�
La conjugaison de ces niveaux marins �lev�s et de ces fortes vagues risque d'engendrer, surtout au moment de la pleine mer, des submersions sur les parties expos�es ou vuln�rables du littoral des d�partements concern�s ajoute M�t�o France, pr�cisant que la p�riode � risque se situe entre vendredi 15 heures et samedi 10 heures.
Les crues restent importantes sur plusieurs cours d'eau dans le Finist�re, le Morbihan, la Loire-Atlantique et l'Ille et Vilaine (vigilance orange crue sur la La�ta, l'Oust, la Vilaine aval et m�diane), mais seuls ces deux derniers d�partements sont plac�s en vigilance orange "inondation".
La temp�te Ulla, d�j� bien visible au milieu de l'Atlantique jeudi, alors qu'elle se dirigeait vers les c�tes occidentales de l'Europe. (Capture d'�cran M�t�o France ).
Partager
