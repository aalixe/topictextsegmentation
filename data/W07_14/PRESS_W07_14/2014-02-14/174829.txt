TITRE: Les autorit�s ukrainiennes lib�rent les 234 manifestants arr�t�s | Thibauld MALTERRE et Olga NEDBAEVA | Europe
DATE: 2014-02-14
URL: http://www.lapresse.ca/international/europe/201402/14/01-4738948-les-autorites-ukrainiennes-liberent-les-234-manifestants-arretes.php
PRINCIPAL: 174826
TEXT:
>�Les autorit�s ukrainiennes lib�rent les 234 manifestants arr�t�s�
Les autorit�s ukrainiennes lib�rent les 234 manifestants arr�t�s
Des opposants manifestent � Kiev, le 14 f�vrier.
PHOTO KONSTANTIN CHERNICHKIN, REUTERS
Ukraine
N�e en novembre de la volte-face du pouvoir, qui a renonc� � un rapprochement avec l'UE pour signer un accord avec Moscou, la contestation ukrainienne s'est depuis mu�e en r�volte contre le pr�sident Ianoukovitch. Une crise qui plonge l'Ukraine au bord de la guerre civile, alors que les affrontements entre opposants et forces de l'ordre ont fait des dizaines de morts et des centaines de bless�s. �
Thibauld MALTERRE, Olga NEDBAEVA
Agence France-Presse
Kiev
Les autorit�s ukrainiennes ont annonc� vendredi avoir lib�r� tous les manifestants interpell�s en deux mois, le pr�sident Viktor Ianoukovitch appelant en retour l'opposition � �faire aussi des concessions�, tandis qu'une nouvelle manifestation est pr�vue pour dimanche.
Les 234 manifestants lib�r�s sont assign�s � r�sidence et les accusations pesant sur eux ne sont pas abandonn�es, a indiqu� dans un communiqu� le procureur g�n�ral ukrainien, Viktor Pchonka.
�234 personnes ont �t� arr�t�es entre le 26 d�cembre et le 2 f�vrier. Aujourd'hui, plus aucune d'entre elles n'est en d�tention�, a d�clar� le procureur, ajoutant que les poursuites, qui pourraient valoir aux opposants de lourdes peines de prison, seraient abandonn�es dans le mois � venir si les conditions fix�es par la loi d'amnistie �taient remplies.
�Nous avons les moyens de remettre n'importe qui � sa place, mais nous ne voulons pas que des innocents souffrent. (...). Je ne veux pas faire la guerre. Je veux sauvegarder l'�tat et reprendre un d�veloppement stable�, a d�clar� vendredi soir le pr�sident Viktor Ianoukovitch, dans un entretien t�l�vis�.
�Nous nous adressons � l'opposition pour qu'elle accepte aussi de faire des concessions (...). Les appels � une lutte sans merci, � prendre les armes, c'est dangereux�, a-t-il ajout�, faisant allusion � des d�clarations de groupes radicaux au sein de l'opposition.
Dans le centre de Kiev, sur la place de l'Ind�pendance - le Ma�dan -, occup�e depuis novembre et entour�e de barricades, le �conseil� improvis� du mouvement de contestation a soulign� que, pour l'heure, les manifestants lib�r�s restaient menac�s de prison.
Une loi d'amnistie avait �t� vot�e en janvier, avec pour condition l'�vacuation des lieux publics et b�timents officiels occup�s par les contestataires, dont la mairie de la capitale. Ce d�lai expire lundi.
L'opposition exigeait de son c�t� la lib�ration sans conditions de toutes les personnes incarc�r�es, et la lev�e des poursuites.
L'une des responsables de l'opposition, l'ancienne Premi�re ministre emprisonn�e Ioulia Timochenko, a r�affirm� dans un entretien � para�tre samedi dans l'hebdomadaire Dzerkalo Tyjmia que �le seul sujet de n�gociation avec Ianoukovitch, c'est les conditions de son d�part et des garanties pour sa famille�.
Manifestation dimanche
En signe de bonne volont� apr�s l'annonce du procureur, les opposants ont toutefois promis de d�bloquer �en partie� la rue Grouchevski o� se trouvent le gouvernement et le parlement, th��tre de heurts violents fin janvier, pour y permettre la circulation automobile.
Pour la onzi�me fois depuis le d�but fin novembre de la contestation, n�e de la volte-face du pouvoir qui a renonc� � un rapprochement avec l'Union europ�enne pour se tourner vers la Russie, les manifestants se r�uniront dimanche � midi (10H00 GMT) sur le Ma�dan.
Le pr�c�dent rassemblement, dimanche 9 f�vrier, avait r�uni pr�s de 70 000 personnes.
Le mouvement de contestation s'est transform� au fil des semaines en un rejet pur et simple du r�gime du pr�sident Viktor Ianoukovitch, et ni la d�mission du gouvernement ni les n�gociations engag�es apr�s les affrontements qui ont fait quatre morts et plus de 500 bless�s fin janvier, n'ont r�gl� le conflit.
Apr�s la d�mission fin janvier du Premier ministre Mykola Azarov, son successeur n'a toujours pas �t� d�sign�, les alli�s du pr�sident, majoritaires au Parlement, faisant savoir qu'ils ne soutiendraient pas un candidat d'opposition.
Les d�bats sur une �ventuelle r�forme constitutionnelle r�clam�e par l'opposition semblent aussi au point mort.
D�termination intacte
Sur la place, les manifestants ne d�sarment pas.
�Tout est calme pour le moment, mais si la police vient, nous nous d�fendrons et la repousserons !�, dit Anna Lazarenko, 20 ans, membre d'une unit� d'autod�fense sur le Ma�dan.
La jeune fille est casqu�e, encagoul�e et �quip�e d'un gilet de protection.
Dans la mairie de Kiev occup�e, m�me d�termination chez Rouslan Andre�ko, 27 ans, le �commandant� sur place.
�La seule condition pour que nous quittions les lieux, c'est la d�mission du pr�sident (Viktor) Ianoukovitch, suivie d'une �lection pr�sidentielle anticip�e�, dit-il.
La crise �tait vendredi au centre de discussions � Moscou entre les ministres russe et allemand des Affaires �trang�res.
Le Russe, Sergue� Lavrov, a accus� l'Union europ�enne de chercher � �tendre sa zone d'�influence� � l'Ukraine en soutenant l'opposition.
Il avait jug� la veille que les relations entre la Russie et l'UE, qui se son d�t�rior�es ces derni�res ann�es, �taient arriv�es � un �moment de v�rit� avec le diff�rend sur l'Ukraine.
Le ministre allemand, Frank-Walter Steinmeier, a r�torqu� que la crise en Ukraine n'�tait �pas un jeu d'�checs g�opolitique� et a soulign� que �personne n'avait int�r�t � un envenimement de la situation� en Ukraine.
Partager
