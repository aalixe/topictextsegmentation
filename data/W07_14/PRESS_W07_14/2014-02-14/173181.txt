TITRE: Inondations � Morlaix. Des d�bordement sont pr�vus ce soir
DATE: 2014-02-14
URL: http://www.ouest-france.fr/inondations-morlaix-des-debordement-sont-prevus-ce-soir-1930890
PRINCIPAL: 173177
TEXT:
Inondations � Morlaix. Des d�bordement sont pr�vus ce soir
Morlaix -
Des d�bordements pourraient avoir lieu ce soir � la pleine mer, 18 h 13.�|�Ouest-France
Facebook
Achetez votre journal num�rique
Avec l'arriv�e de la temp�te Ulla, la pr�fecture pr�voit de l'eau dans le centre-ville, vers 18 h.
C'est reparti ! Avec l'arriv�e de la nouvelle temp�te baptis�e Ulla (ventes � 140�km/h et fortes pr�cipitations), la pr�fecture du Finist�re pr�voit des risques r�els d'inondation, ce vendredi, dans le centre-ville de Morlaix avec 10 � 15�cm, place des Otages. Ces d�bordements devraient avoir lieu au moment de la pleine mer (18 h 13, coefficient 81).
Lire aussi
