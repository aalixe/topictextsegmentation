TITRE: "Neknomination" ou le nouveau d�fi d�alcool qui fait fureur sur Facebook - RTL info
DATE: 2014-02-14
URL: http://www.rtl.be/info/belgique/societe/1069356/-neknomination-ou-le-nouveau-defi-d-alcool-qui-fait-fureur-sur-facebook
PRINCIPAL: 173598
TEXT:
?
"Neknomination" ou le nouveau d�fi d�alcool qui fait fureur sur Facebook
En quelques jours, la � Neknomination � est devenue la toute derni�re tendance sur les r�seaux sociaux. Le principe de ce nouveau jeu Facebook est simple. Il consiste � relever un d�fi d'alcool en d�signant ses amis pour qu'ils le rel�vent � leur tour. Une pratique dangereuse.
14 F�vrier 2014 11h42
R�agir (9)
Voil� une pratique quelque peu stupide, mais qui rencontre pourtant un franc succ�s. Tir� de l�expression anglaise "to neck a drink", qui signifie "boire un verre cul-sec", le but du jeu est tr�s simple. Sur les r�seaux sociaux, un de vos amis vous choisit � travers une vid�o dans laquelle celui-ci a du boire cul-sec des verres d'alcool. Vin, bi�re, vodka ou encore rhum, vous avez ensuite 24 heures pour r�aliser le d�fi. C�est ensuite au tour du candidat d�sign� d'y r�pondre. Il filme alors son challenge en se pr�sentant et finit par choisir � son tour trois copains. Une v�ritable chaine qui ne finit plus puisque vous pouvez �tre nomin� � chaque fois par un de vos amis.
�
Une pratique qui effraie
Apr�s la chaine par e-mail des trois v�ux � r�aliser, aujourd�hui c�est l�alcool, une pratique dangereuse qui inqui�te de plus en plus. A travers le monde, la "neknomination" aurait d�j� fait quatre morts. Le site anglais "Guardian" rapporte les faits : "Jonny Byrne et Ross Cummings sont morts apr�s avoir apparemment essay� de r�ussir leurs d�fis. Byrne, 19 ans, s'est noy� apr�s avoir bu une pinte et avoir saut� dans une rivi�re".
�
Que se passe-t-il si le d�fi n�est pas relev� ?
Lorsque une personne lance un d�fi � l�un de ses proches, il le provoque en duel mais l'expose aussi aux regards des autres et fait jouer la pression de ses autres contacts. Il est possible pour la personne qui a �t� choisie par l�un de ses amis de ne pas relever le d�fi. Mais en faisant cela, elle s�exclut elle-m�me du groupe car elle brise la cha�ne et s�expose ainsi � des moqueries de la part de ses proches.
Articles en relation
