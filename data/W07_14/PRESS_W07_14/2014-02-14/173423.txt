TITRE: Saint-Valentin en Chine: quand la f�te occidentale se m�le aux traditions  - RFI
DATE: 2014-02-14
URL: http://www.rfi.fr/emission/20140214-saint-valentin-chine-quand-fete-occidentale-mele-traditions/
PRINCIPAL: 173419
TEXT:
print
�
Un bouquet de fleurs de 298 roses pr�par� par un fleuriste de P�kin pour la Saint-Valentin, le 13 f�vrier 2014.
Et nous sommes le 14 f�vrier 2014, le jour J pour d�clarer sa flamme. Particuli�rement en Chine o�, chose rare, la Saint Valentin tombe cette ann�e � la m�me date que Yuan Xiao, la f�te des lanternes et des amours. La tradition remonte aux empereurs Song, elle est d�sormais remise aux go�ts du jour. Karaok�, cin�ma, op�ra et m�me usine d�saffect�e, servant de d�cors � nos jeunes Rom�o et Juliette chinois d�aujourd�hui.
�
