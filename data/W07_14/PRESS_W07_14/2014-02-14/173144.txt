TITRE: Windows 8, bien loin du best-seller Windows 7, Actualités
DATE: 2014-02-14
URL: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0203317551030-windows-8-bien-loin-du-best-seller-windows-7-650617.php?xtor%3DAL-4003-%255BChoix_de_la_redaction%255D-%255Bwindows_8_bien_loin_du_best_seller_windows_7%255D
PRINCIPAL: 0
TEXT:
Par Les Echos | 14/02 | 10:24
Microsoft a �coul� en 15 mois plus de 200 millions de licences de la derni�re version de son syst�me d�exploitation.
Windows 8 a �t� con�u pour fonctionner aussi bien sur PC que sur tablette. - REUTERS
Ressusciter le bouton � d�marrer � n�a pas suffi. Microsoft, � l�aube d�une nouvelle �re qui s�ouvre avec la nomination au poste de PDG de Satya Nadella , en succession de Steve Ballmer, a annonc� jeudi soir avoir vendu plus de 200 millions de licences de son syst�me d�exploitation Windows 8, lanc� il y a 15 mois... Une performance bien inf�rieure � celle de Windows 7 qui s��tait �coul�e � 240 millions d�exemplaires au cours de sa premi�re ann�e sur le march�. Ces chiffres, fournis par la directrice financi�re et marketing de Microsoft, Tami Reller, au cours d�une conf�rence organis�e par Goldman Sachs, sont les premiers diffus�s par le g�ant am�ricain depuis six mois.
Les ventes d�cevantes de Windows 8 et sa derni�re version, Windows 8.1, qui r�tablit notamment le fameux bouton � d�marrer � , r�pondent au fl�chissement des ventes d�ordinateurs personnels constat� depuis deux ans au b�n�fice des smartphones et des tablettes. Les ventes mondiales de tablettes devraient d�passer cette ann�e celles des PC.
11�% des PC dans le monde sous Windows 8
Pour surfer sur ce segment dynamique, Windows 8 avait pourtant �t� con�u comme un syst�me d�exploitation capable de fonctionner aussi bien sur PC que sur tablette. Mais il n�a pas convaincu les utilisateurs qui continuent � pr�f�rer les iPad d�Apple aux tablettes Surface de Microsoft.
Plus inqui�tant encore pour la soci�t� bas�e � Redmond, pr�s de Seattle, peu d�ordinateurs fonctionnent sous Windows 8 et de nombreuses entreprises qui l�ont achet� ne l�ont m�me pas install�. Selon les statistiques de NetMarketShare, seuls 11% des utilisateurs de PC dans le monde ont adopt� Windows 8 ou 8.1, alors qu�ils sont encore 48% � utiliser Windows 7 et 29% � �tre rest�s fid�les au � vieux � Windows XP, qui date de plus de dix ans.
Windows 7, qui avait remplac� le tr�s impopulaire Windows Vista, reste � ce jour le syst�me d�exploitation de Microsoft qui s�est le mieux vendu, avec plus de 450 millions de licences �coul�es.
