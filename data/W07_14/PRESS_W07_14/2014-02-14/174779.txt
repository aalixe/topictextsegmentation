TITRE: L'Europe s'attaque aux allergies provoqu�es par le parfum - Consommation - Le Particulier
DATE: 2014-02-14
URL: http://www.leparticulier.fr/jcms/p1_1554364/l-europe-s-attaque-aux-allergies-provoquees-par-le-parfum
PRINCIPAL: 174778
TEXT:
14/02/14 � 18:34
La Commission europ�enne veut interdire aux parfumeurs l'utilisation de trois substances tr�s allergisantes.
Pr�s�3 % des europ�ens souffrent d'allergies cutan�es caus�s par du�parfum. Face � ce fl�au, la Commission europ�enne a d�cid�d'interdire trois allerg�nes : l'atranol, le chloroatranol ainsi que l'extrait synth�tique de muguet (HICC).
"Si ces substances allergisantes se trouvent dans un parfum, la formulation de celui-ci devrait �tre revue et l'allerg�ne interdit remplac� par une autre substance", indique la Commission.
Les�grands noms�de la cosm�tique, comme Guerlain, Dior, ou Chanel, vont �devoir trouver des recettes magiques pour remplacer ces�substances�all�g�nes sans d�naturer�leurs fragrances emblatiques.�
Pour�limiter que les�risques, la Commission recommande �galement qu'un �tiquetage des allerg�nes soient obligatoirement port� sur les emballages de tous les produits cosm�tiques comme les d�odorants, cr�mes�et eaux de toilette.
Ces mesures vont faire l'objet d'une consultation pendant trois mois, avant d'�tre soumises � l'approbation des �tats et du Parlement europ�en, pour une entr�e en vigueur pr�vue�d�but 2015.
St�phanie Alexandre
