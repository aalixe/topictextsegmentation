TITRE: Air France autorise les smartphones et tablettes au d�collage
DATE: 2014-02-14
URL: http://www.reponseatout.com/reponse-conso/voyage-vacances/compagnies-aeriennes/air-france-autorise-smartphones-tablettes-decollage-a1012202
PRINCIPAL: 174323
TEXT:
Air France autorise les smartphones et tablettes au d�collage
Le 14/02/2014 � 16:57:13
Vues : 526 fois JE REAGIS
Les smartphones et tablettes devront �tre en mode avion pendant la dur�e du voyage | � Sipa
Plus besoin d'�teindre son t�l�phone au d�collage et � l'atterrissage des appareils. Il suffira d'activer le mode avion.
Le temps o� les appareils �lectroniques devaient �tre coup�s durant le d�collage et l�atterrissage d�un avion est r�volu. La compagnie Air France vient d�annoncer qu�elle autorisait d�sormais ses passagers � utiliser leurs smartphones et leurs tablettes durant la totalit� du voyage.
Cependant, les h�tesses et stewards demanderont aux voyageurs d�activer le mode � avion � de leurs appareils, afin de couper toutes les connexions cellulaires, Wi-Fi et Bluetooth pouvant provoquer des interf�rences avec l��lectronique de l�avion. Air France se f�licite de pouvoir laisser les passagers � continuer � travailler ou � se divertir d�s qu'ils montent dans l'avion et jusqu'� l'arriv�e � destination, en totale libert� �.
> Lire aussi : Est-il vraiment dangereux de t�l�phoner en avion ?
Air France rappelle cependant que � la premi�re priorit� en terme de s�curit� des vols est que les passagers soient attentifs aux annonces et aux consignes des �quipages �. Elle se r�serve donc le droit de demander de tout �teindre � si les conditions op�rationnelles l'exigent �.
En r�alit�, la compagnie a�rienne se plie simplement aux demandes formul�es par l�Union europ�enne en d�cembre dernier. La Commission y pr�conisait l�assouplissement des r�gles d�utilisation d�appareils �lectroniques � bord des avions.
