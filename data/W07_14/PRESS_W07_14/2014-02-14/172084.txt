TITRE: Miranda Kerr n�a pas vu sa famille depuis plus d�un an ! - Voici
DATE: 2014-02-14
URL: http://www.voici.fr/news-people/actu-people/miranda-kerr-n-a-pas-vu-sa-famille-depuis-plus-d-un-an-520623
PRINCIPAL: 0
TEXT:
Publi� le samedi 08 f�vrier 2014 � 18:56                              par F.P
Miranda Kerr n�a pas vu sa famille depuis plus d�un an !
Ses parents sont d�sesp�r�s
PHOTOS Orlando Bloom et Miranda Kerr : s�par�s, mais proches
29/10/2013
Les parents de Miranda Kerr ne savent plus quoi faire pour attirer l'attention de leur fille qui les a manifestement mis de c�t�. Voil� plus d'un an qu'elle ne leur a pas rendu visite ni m�me parl� au t�l�phone. Quelle tristesse !
S�par�e d�Orlando Bloom depuis octobre dernier , on aurait pu penser que Miranda Kerr se serait tourn�e vers sa famille pour trouver un peu de stabilit� et de r�confort. Il n�en est rien. On apprend par le Daily Mirror que le mannequin n�a pas vu sa famille depuis plus d�un an�! Une triste nouvelle surtout lorsqu�on imagine le petit Flynn, �g� de trois ans, priv� de ses grands-parents maternels.�Que s�est-il pass�?
Mardi prochain sera diffus� sur une cha�ne australienne un documentaire sur la jeune femme intitul� Family Confidential. Dans ce film, ses parents se confient sur elle et bien entendu, sur son silence. La maman de Miranda Kerr, Therese, semble mettre ce dernier sur le compte de la c�l�brit�. ��La vie de Miranda est tellement diff�rente maintenant. Elle est constamment entour�e de gens qui lui passent tout �, d�clare-t-elle.
Le papa de Miranda Kerr, John, va m�me plus loin en expliquant qu�il regrette que sa fille soit devenue c�l�bre. ��Je voulais juste qu�elle soit une fille normale �, confie-t-il. Le p�re du mannequin explique �galement que c�est Orlando Bloom qui les a avertis de sa rupture avec leur fille. Le comble�!�
Dans le documentaire, Therese lit une lettre qu�elle a �crite et dans laquelle elle explique � sa fille combien elle l�aime. Emouvant. Esp�rons que Miranda Kerr prendra au moins le temps de regarder ce film vu qu�elle n�a m�me pas daign� y participer. A l��poque du tournage, elle a expliqu� qu�elle ne pouvait se rendre en Australie pour y participer � cause de son emploi du temps surcharg�. Elle avait tout de m�me fait savoir, via son agent, qu�elle �tait pleine de reconnaissance. Qu�elle remerciait sa famille pour son soutien. Ce qui serait chouette c�est qu�elle leur dise de vive voix maintenant� non�?
Mots-cl�s :
