TITRE: Windows 8 atteint � enfin � les 200 millions
DATE: 2014-02-14
URL: http://www.linformaticien.com/actualites/id/32084/windows-8-atteint-enfin-les-200-millions.aspx
PRINCIPAL: 173580
TEXT:
Windows 8 atteint � enfin � les 200 millions
par St�phane Larcher, le 14 f�vrier 2014 12:30
15 mois apr�s son lancement, la derni�re version du syst�me d�exploitation Microsoft vient de franchir la barre des 200 millions de licences vendues. C�est bien moins que Windows 7 qui avait r�alis� 240 millions en un an.
L�annonce a �t� faite hier par Tami Reller, patronne du marketing de l�entreprise, � l�occasion d�un forum sur la technologie organis�e par Goldman Sachs. Il s�agit du premier chiffre rendu public par Microsoft depuis plus de 6 mois. Si les ventes sont effectivement d�cevantes en comparaison de ce qui s��tait pass� pour Windows 7, il faut toutefois tenir compte d�un d�clin g�n�ral des ventes de PC au profit des tablettes et smartphones, un contexte confirm� en France o� les ventes de tablettes ont d�pass� celles des PCs en 2013.
Mais le plus ennuyeux pour Microsoft est le faible taux d�adoption et d�usage du syst�me. En effet, si Windows 8 est d�sormais livr� en standard avec les PCs, beaucoup d�utilisateurs laissent la licence sur l��tag�re et continuent � travailler avec Windows 7 voire des versions inf�rieures.�
Les ventes de Windows 8 diminuent avec le march� PC
Selon des chiffres fournis par NetMarketShare, la part des utilisateurs de Windows 8 ou 8.1 serait de 11% contre 48% pour Windows 7 et 29% pour Windows XP qui accuse plus de 10 ans d��ge. Au total Windows 7 a �t� vendu � 450 millions d�exemplaires. Par ailleurs, il semblerait que le rythme de ventes continue � d�cliner � l�instar du march� des PCs et la tablette Surface 2 demeure trop marginale face aux iPad et tablettes sous Android pour inverser la tendance, du moins pour le moment.
Une nouvelle mise � jour baptis�e Windows 8.1 update 1 devrait sortir dans le courant du mois d�avril. Les fuites concernant les am�liorations se sont multipli�es sur la toile ces derni�res semaines. Parmi les am�liorations figureraient une r�duction de l�usage du disque c�est-�-dire une optimisation du fonctionnement et une interface utilisateur plus intuitive pour tous ceux � et ils demeurent nombreux � qui utilisent clavier et souris et non pas l�interface tactile.
Noter cet article
