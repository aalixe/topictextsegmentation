TITRE: Temp�te Ulla: un mort et 100.000 foyers sans �lectricit� en Bretagne - BFMTV.com
DATE: 2014-02-14
URL: http://www.bfmtv.com/planete/intemperies-six-departements-nord-ouest-alerte-orange-711416.html
PRINCIPAL: 174834
TEXT:
r�agir
Un octog�naire est mort vendredi soir, apr�s avoir fait une chute � bord d'un paquebot au large de la Bretagne � cause de la temp�te Ulla qui s'abat actuellement sur le Nord-Ouest de la France. Une femme, � bord du m�me paquebot, a �t� �vacu�e par h�licopt�re � la suite �galement d'une chute, a indiqu� la pr�fecture maritime. La nationalit� des deux victimes n'�tait pas connue dans l'imm�diat.
Par ailleurs, au moins 100.000 foyers sont priv�s d'�lectricit� en Bretagne a annonc� ERDF. Selon le d�compte, 50.000 foyers �taient concern�s dans le Finist�re, 40.000 dans les C�tes d'Armor, 4.000 dans le Morbihan et 1.000 en Ille-et-Vilaine.
Sur BFMTV, le pr�fet du Finist�re a indiqu� avoir mis en place une cellule de crise et a appel� les habitants � "limiter au maximum les d�placements".
La SNCF a annonc� vendredi soir avoir d�cid� d'interrompre le trafic ferroviaire entre Rennes et Brest par mesure de s�curit�. C�t� trafic a�rien, les vols sont annul�s � l'a�roport de Rennes.
La Bretagne encore touch�e
Apr�s Erich, Dirk et Christian en d�cembre 2013, Petra, Quamaira, Ruth et St�phanie en janvier, la r�gion voit s'approcher une nouvelle temp�te, pr�nomm�e cette fois Ulla.
Le Finist�re, les C�tes-d'Armor, le Morbihan et la Manche devraient �tre touch�s par des vents violents, tandis que l'Ille-et-Vilaine et la Loire-Atlantique ont �t� plac�es en vigilance orange crues.
Les plus fortes rafales attendues vendredi soir
C'est vendredi soir "qu'on observera les plus fortes rafales, elles atteindront g�n�ralement 100 � 120 km/h dans l'int�rieur des terres sur le Finist�re, 100 � 110kmh sur les C�tes-d'Armor, l'ouest du Morbihan ainsi que le d�partement de la Manche", indique M�t�o-France.
Des pointes � 130km/h et jusque 140 km/h sont attendues sur les caps les plus expos�s. Ces rafales pourraient provoquer des ph�nom�nes de "vagues-submersion", ce qui a oblig� M�t�o France a plac� six d�partements du Nord-Ouest en vigilance orange.
Ces vents, "g�n�rant de tr�s fortes vagues sur la fa�ade atlantique et en entr�e de Manche � partir de vendredi soir", provoqueront une sur�l�vation du niveau de la mer avec des coefficients de mar�e de 81 pour la pleine mer de vendredi soir, et 84 pour la pleine mer de samedi matin.
