TITRE: Kool Shen: "Devant Catherine Breillat, j'�tais comme un �l�ve face � un prof" - Studio Cin� Live
DATE: 2014-02-14
URL: http://www.lexpress.fr/culture/cinema/kool-shen-devant-catherine-breillat-j-etais-comme-un-eleve-face-a-un-prof_1319642.html
PRINCIPAL: 172133
TEXT:
Kool Shen dans Abus de faiblesse.
Dr
Que repr�sentait le cin�ma pour vous enfant?
Kool Shen: Comme tout le monde, j'allais au cin�ma de temps � autre. Mais je vais �tre franc avec vous, ce n'�tait pas une grande passion. Je ne suis pas du tout cin�phile par exemple. Et m�me pire, ces 15 derni�res ann�es, je ne suis quasiment plus all� en salles si ce n'est pour accompagner mon fils d�couvrir des dessins anim�s. Donc le cin�ma ne constitue pas pour moi une passion d�vorante de base. �
Vous n'aviez donc aucun r�ve d'en faire?
Absolument pas, . Et aujourd'hui, je me sens d'ailleurs un peu g�n� aux entournures de tourner alors que je n'ai aucune formation quand je pense � ceux qui en r�vent depuis l'enfance...�
Comment vous �tes- vous retrouv� alors � �tre dirig� par Catherine Breillat ?
Un jour on m'a appel� pour me dire qu'elle souhaitait me rencontrer pour jouer dans un film qui serait l'adaptation de son histoire avec Christophe Rocancourt . Mais je n'y suis pas tout de suite all� en courant, croyez- moi! Jusque l�, je n'�tais apparu que dans deux ou trois trucs de pote pour me marrer: Old school, La beuze... Donc je ne comprenais pas pourquoi elle voulait faire appel � moi...�
�
Mais vous allez quand m�me au rendez- vous. Pourquoi?
Parce que je me suis inform� aupr�s de gens plus cin�philes que moi sur son cin�ma. Parce que son sc�nario m'int�ressait m�me si je ne savais pas si je serais capable de le faire. Et pour la personnalit� de Catherine. Je me souviens l'avoir vue sur France 2 d�fendre corps et �me son livre Abus de Faiblesse dans l'�mission de Ruquier face � Eric Zemmour et Eric Naulleau . Elle m'avait profond�ment touch�. Donc � force d'entendre que je n'avais pas le droit de ne pas aller passer des essais pour elle, tout cela a fait �cho en moi...�
En quoi consistaient ces essais?
Deux sc�nes du film � jouer. J'y suis d'ailleurs all� � deux reprises. Car la premi�re, je ne connaissais pas les textes au rasoir et ce n'�tait donc pas brillant. Mais Catherine m'a juste dit: "ce n'est pas grave. Je sais que si tu connaissais le texte, tu jouerais mieux..." J'ai compris le message et je suis revenu quelques jours plus tard avec le texte su au rasoir. Et, manifestement, �a lui a plu...�
Vous vous �tes senti � l'aise?
Pas la premi�re fois �videmment mais la deuxi�me un peu plus. En fait, Catherine a su me mettre en confiance tr�s vite. D�s notre deuxi�me rencontre, j'avais l'impression de la conna�tre et d'�tre face � un pote!�
A partir du moment o� elle vous a choisi, comment avez-vous travaill�?
J'�tais comme un �l�ve face � son prof. Je lui demandais ce qu'il fallait faire. Et l�, elle m'a tout de suite dit qu'il fallait apprendre par coeur le sc�nario. Or, moi, je ne me voyais pas conna�tre ce pav� au rasoir. Cela n'a rien � voir avec mes textes sur sc�ne qui sont les miens et que je connais donc par coeur. Mais je m'y suis mis et il m'a fallu un bon mois et demi pour y parvenir. Je suis assez perfectionniste car, quand je fais les choses, je n'ai pas envie de passer pour un baltringue. C'est la raison pour laquelle j'ai aussi pris un coach qui, lui, a su me mettre � l'aise dans les sc�nes o� je pouvais avoir quelques difficult�s. Il m'a en tout cas permis d'avoir le sentiment que je pouvais faire le job et tenir ce r�le.�
Et vous avez beaucoup vu Catherine Breillat en amont du tournage?
Quasiment pas et surtout, alors que je n'arr�tais pas de le lui demander, elle m'a interdit de voir Isabelle Huppert pour qu'on se d�couvre sur le plateau�
Comment s'est alors pass�e la rencontre avec cette derni�re?
Ce premier jour fut vraiment d�stabilisant. J'arrive sur le plateau, on me donne un caf� et l� j'entends au loin des cris. Catherine et Isabelle commencent � s'engueuler sur une sc�ne. L'ambiance est vraiment tr�s tendue et je me demande o� j'ai mis les pieds (rires) et surtout comment je vais pouvoir jouer au milieu de cette tension. Et puis on se met en place pour la sc�ne o� je lui remets ses chaussures � l'h�pital. Et l�, comme par miracle, tout s'apaise et se passe bien. Et �videmment cela participer � me mettre en confiance, tout particuli�rement l'attitude d'Isabelle avec moi qui a su m'accompagner de bout en bout�
Qu'est ce qui vous a s�duit dans le personnage que vous incarnez?
Sa complexit�. J'avais de lui l'image qu'il r�ussit parfaitement � se donner: celui d'un Robin des Bois , dont la force est d'arriver � retourner le cerveau des gens.�
Kool Shen et Isabelle Huppert dans Abus de faiblesse de Catherine Breillat.
DR
D'ailleurs, dans votre interpr�tation, vous choisissez l'ambigu�t�. On ne sait jamais si on doit le d�tester ou le prendre dans ses bras...
Je l'ai juste jou� de mani�re sinc�re, au premier degr�, sans sous-entendu ou arri�res pens�es.�
Est-ce que vous avez cherch� � "sauver" le personnage?
Je n'en ai pas l'impression m�me si Catherine m'a dit que je l'avais rendu au final presque sympathique, alors que ce n'�tait pas son intention de d�part. Mais, � mes yeux, si Abus de faiblesse parle de manipulation, elle est r�ciproque. �
Quels sont les sc�nes qui vous ont pos� le plus de probl�mes?
Celles o� je devais exprimer la col�re alors que je pensais que �a ne me poserait aucun souci vu que je peux �tre assez col�rique dans la vie. Mais le cin�ma n'est pas la vie. Et les choses les plus naturelles se r�v�lent les plus complexes � jouer. �
Avez-vous eu des tics � gommer?
Oui, j'ai eu des choses � effacer. Catherine ne voulait absolument pas dans ce r�le d'un banlieusard qui s'�nerve. Il m'a donc fallu enlever tout un lexique qui est mien depuis toujours, en fait tout ce qui aurait pu me ressembler ! Mais, � partir du moment o� l'�motion qu'elle recherchait �tait l� et qu'elle n'avait pas devant elle le "Kool Shen du 93", elle me laissait m�me parfois une libert� dans le choix des mots.�
Et avez-vous pris du plaisir � jouer?
Enorm�ment ! Une fois pass� le trac du d�but, plus le film avan�ait, plus je me sentais bien. Et � la fin, j'avais vraiment envie de tout recommencer pour �tre meilleur dans les premi�res sc�nes que j'ai pu interpr�ter !�
Abus de faiblesse de Catherine Breillat.
DR
Avez vous envie de vous retrouver devant une cam�ra?
Oui et d'ailleurs j'ai d�j� tourn� dans deux s�ries, Paris de Gilles Banier pour Arte et Le soldat d' Erick Zonca pour Canal +. Et c�t� cin�ma, je serai � l'affiche de Vacances de r�ves de Sophie Laloy (Je te mangerais) puis dans La saison des �mes de Toni Baillargeat aux c�t�s de Simon Abkarian, Slimane Dezi et Sa�d Taghmaoui .�
Vous avez totalement laiss� tomber la musique?
Oui et depuis pas mal de temps au profit... du poker. J'ai vendu mon label il y a 6 ans, au tout d�but de la crise qui a frapp� dans l'industrie de la musique. �
Et la sensation sur un plateau de cin�ma est tr�s diff�rente de la sc�ne?
Quand je monte sur sc�ne pour faire du rap, m�me s'il y a 80 000 personnes devant moi, je suis d�tendu du slip. Parce qu'on a beaucoup travaill� en amont pour arriver � ma�triser ce qu'on veut faire et parce qu'une fois sur sc�ne, on ext�riorise imm�diatement tout, y compris la petite boule au ventre. Ce n'est pas encore le cas au cin�ma mais j'esp�re un jour parvenir � jouer lib�r�. J'ai encore trop en moi la peur de ne pas savoir mon texte et ce souci de bien faire. Je ne suis pas encore assez naturel et trop scolaire. �
Suivez L'Express
