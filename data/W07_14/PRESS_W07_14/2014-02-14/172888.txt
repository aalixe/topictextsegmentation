TITRE: Italie: le Premier ministre Letta d�missionne, son rival Renzi devrait lui succ�der - RTL info
DATE: 2014-02-14
URL: http://www.rtl.be/info/monde/europe/1069298/italie-le-premier-ministre-letta-demissionne-son-rival-renzi-devrait-lui-succeder
PRINCIPAL: 172880
TEXT:
R�agir (1)
Le chef du gouvernement italien, Enrico Letta, remet officiellement sa d�mission vendredi, pouss� vers la sortie par son rival au sein du Parti D�mocrate (PD), le jeune maire de Florence Matteo Renzi, qui devrait lui succ�der � la t�te de la m�me coalition in�dite gauche-droite, form�e il y a moins d'un an.
Apr�s un dernier conseil des ministres dans la matin�e, M. Letta "montera" au Quirinal, la colline o� se trouve la pr�sidence pour remettre sa d�mission au pr�sident Giorgio Napolitano.
Il a annonc� sa d�cision d�s jeudi soir juste apr�s un vote �crasant de sa formation de centre gauche, le Parti d�mocrate (PD) r�clamant un changement de gouvernement. La motion propos�e par M. Renzi demandait d'"ouvrir une phase nouvelle avec un ex�cutif nouveau soutenu par la majorit� actuelle".
"Nouveau gouvernement: le PD choisit Renzi", titre sur son site internet le Corriere della Sera.
Sans demander une investiture officielle et tout en rendant hommage "� l'important travail accompli" par M. Letta, le maire de Florence de 39 ans, a propos� d'"ouvrir une nouvelle page" avec "un projet de relance radicale".
L'Italie doit "changer d'horizon et de rythme", "sortir des mar�cages", a estim� M. Renzi, en se fixant comme horizon la fin de la l�gislature en 2018 et en annon�ant des r�formes "ambitieuses".
L'Italie est enfonc�e depuis deux ans dans la plus grave crise �conomique de l'apr�s-guerre, avec un PIB en baisse de pr�s de 4% et un taux de ch�mage sup�rieur � 12%. Les �conomistes s'attendent vendredi � l'annonce d'une sortie de la r�cession au quatri�me trimestre de 2013.
- Matteo Renzi piaffait d'impatience -
M. Letta, ex-num�ro deux de ce parti, avait d�cid� de ne pas participer � la r�union du PD, pour permettre � ses coll�gues de prendre leurs d�cisions "avec le maximum de s�r�nit�".
Au fil des d�bats, le sort de M. Letta est apparu scell�, la grande majorit� des intervenants appelant � un "acte de clart�" et � une "acc�l�ration".
Le maire de Rome, membre du PD, a assur� le nouveau probable Premier ministre Matteo Renzi de son soutien: "je pense que Matteo est quelqu'un qui est � la hauteur", a-t-il d�clar� � l'AFP-TV.
La veille encore, M. Letta avait d�fi� son adversaire en pr�sentant un programme pour relancer son gouvernement, appel� "Engagement Italie".
M. Renzi a rel�gu� ce document au rang de simple "contribution" � son action future. Mais il n'a pas donn� de d�tails sur comment il compte trouver l'argent pour relancer l'�conomie italienne, r�duire la bureaucratie, abaisser la lourde fiscalit� sur les entreprises, relancer le march� du travail.
Depuis son arriv�e � la t�te du PD en d�cembre dernier et surtout depuis qu'il avait conclu un accord � la mi-janvier pour une nouvelle loi �lectorale avec Silvio Berlusconi, Matteo Renzi piaffait d'impatience.
Il a multipli� les attaques contre l'ex�cutif Letta, lui reprochant lenteur et manque de d�termination, malgr� un pacte nou� entre les deux hommes pour une poursuite de l'action du gouvernement Letta jusqu'� au moins la fin 2014.
Selon Giovanni Orsina, professeur de Sciences politiques � l'Universit� Luiss de Rome, le dualisme Renzi-Letta �tait devenu "un �ni�me facteur de paralysie politique".
Apr�s la d�mission de M. Letta, le pr�sident Giorgio Napolitano devrait proc�der � des consultations des diff�rents partis avant de vraisemblablement choisir M. Renzi pour former un nouveau gouvernement.
Les centristes de Choix civique ont d�j� dit vouloir �tre du voyage. Tout comme les anciens proches de Silvio Berlusconi rest�s au gouvernement derri�re la banni�re du Nouveau centre droit d'Angelino Alfano. Mais "uniquement si c'est un gouvernement de service (au pays), pas si c'�tait un gouvernement politique avec des connotations de centre gauche", a expliqu� M. Alfano.
Une fois l'ex�cutif constitu�, M. Renzi devra se pr�senter, peut-�tre d�s mardi prochain devant le parlement, pour un vote de confiance.
Galerie - Italie: le Premier ministre Letta d�missionne, son rival Renzi devrait lui succ�der
