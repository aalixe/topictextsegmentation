TITRE: Rakuten s'offre Viber pour 900 millions de dollars
DATE: 2014-02-14
URL: http://www.numerama.com/magazine/28427-rakuten-s-offre-viber-pour-900-millions-de-dollars.html
PRINCIPAL: 173388
TEXT:
Publi� par Julien L., le Vendredi 14 F�vrier 2014
Rakuten s'offre Viber pour 900 millions de dollars
C'est un rachat spectaculaire. Le g�ant nippon du e-commerce Rakuten, d�j� propri�taire de nombreux produits et services, a mis la main sur la soci�t� Viber, qui est sp�cialis�e dans la VoIP. Montant ? 900 millions de dollars.
4
900 millions de dollars. C'est donc le montant faramineux que Rakuten, la principale plateforme de commerce en ligne du Japon, a d�bours� pour acqu�rir Viber, un logiciel sp�cialis� dans les appels t�l�phoniques en voix sur IP (VoIP). Par la m�me occasion, Rakuten r�cup�re les utilisateurs de Viber, estim�s � 280 millions dans le monde (dont plus de 100 millions sont actifs tous les mois).
Le rachat de Viber permet � Rakuten d'ajouter une corde � son arc, dans la mesure o� le groupe est d�j� propri�taire du site de e-commerce fran�ais PriceMinister, de l'Am�ricain Buy, du Britannique Play et du fabricant canadien sp�cialis� dans les liseuses num�riques, Kobo (c'est d'ailleurs les produits de Kobo que met en vente la FNAC). Mais Rakuten poss�de de nombreuses autres filiales .
Startup isra�lienne, Viber est le dernier exemple du succ�s que conna�t le pays dans le domaine des nouvelles technologies. Avant le service de VoIP, d'autres jeunes entreprises ont �t� achet�es par des g�ants de la Silicon Valley, � l'image de Face , r�cup�r�e par Facebook pour un montant situ� entre 40 et 50 millions d'euros, ou de Waze , qui a �t� croqu�e par Google l'ann�e derni�re pour 966 millions de dollars.
Logiciel propri�taire, Viber permet, outre des appels gratuits, d'envoyer des messages et des photos via WiFi ou par le biais du r�seau d'un op�rateur. Compatible avec un PC de bureau sous Windows, Viber l'est aussi avec un environnement Mac ou les distributions Linux . Le programme est aussi d�clin� en application mobile pour iOS, Android, Windows Phone, BlackBerry, Bada (Samsung) et Symbian (Nokia).
