TITRE: Pourquoi les gouvernements italiens tombent-ils comme des mouches ? - RTL info
DATE: 2014-02-14
URL: http://www.rtl.be/info/monde/europe/1069388/pourquoi-les-gouvernements-italiens-tombent-ils-comme-des-mouches-
PRINCIPAL: 173502
TEXT:
R�agir (1)
Le futur gouvernement, probablement dirig� par Matteo Renzi, l'ambitieux chef du Parti d�mocrate (PD) qui a �ject� jeudi Enrico Letta de son si�ge, ce jeudi, sera le quatri�me ex�cutif du pays en moins de deux ans, depuis la d�mission en novembre 2011 de Silvio Berlusconi. "La v�rit� est qu'il est difficile (...) d'expliquer au monde ce qui se passe dans le labyrinthe de notre syst�me politique. Nous sommes revenus � l'�ternelle anomalie italienne", �crit le quotidien La Stampa. "L'instabilit� est fruit du syst�me politique qui pr�voit deux chambres (S�nat et Chambre des d�put�s, ndlr) ayant le m�me poids alors que les majorit�s y sont diff�rentes", rappelle � l'AFP Ernesto Galli della Loggia, historien et �ditorialiste du principal tirage italien, le Corriere della Sera.
�
Majorit� absolue � la Chambre des d�put�s mais pas au S�nat
Le PD a remport� en f�vrier 2013 la majorit� absolue � la Chambre des d�put�s, mais aucune majorit� claire ne s'�tait d�gag�e au S�nat o� le centre gauche, le centre droit et le Mouvement Cinq Etoiles (M5S) de l'ex-humoriste Beppe Grillo ont un poids �quivalent.
�
Un syst�me politique qui ne permet pas � un seul parti de gagner les �lections
Les diff�rentes lois �lectorales italiennes de ces derni�res d�cennies n'ont pas permis la naissance d'un syst�me permettant � un parti seul de remporter les �lections. Des coalitions plus ou moins h�t�rog�nes se cr�ent, octroyant aux plus petits partis capacit� de chantage et de nuisance. "En plus, la figure du chef du gouvernement est d'un point de vue institutionnelle extr�mement faible. Il n'a m�me pas le pouvoir de nommer ou r�voquer ses ministres sans l'accord du pr�sident de la R�publique", poursuit M. Galli della Loggia.
�
"Le chef du gouvernement ne peut m�me pas dicter une politique � un ministre qui �coute davantage les ordres provenant de son parti d'origine que ceux du pr�sident du Conseil", ajoute l'expert. "Le pr�sident du Conseil est un primus inter pares (le premier des �gaux) et cela repr�sente une source ind�niable de faiblesse", a-t-il pr�cis�.
�
Matteo Renzi, un "carri�riste sans scrupules"
La personnalit� et le parcours de Matteo Renzi, excellent tribun, expliquent aussi en partie la chute d'Enrico Letta, homme des institutions terne devant les cam�ras. "Carri�riste sans scrupules", selon Beppe Grillo, Matteo Renzi est un homme press� et "il �tait illusoire de penser que quelqu'un comme lui se serait r�sign� � vivoter en attendant son tour", �crit La Stampa.
�
Le jeune maire de Florence (39 ans), qui a fait l'essentiel de sa carri�re politique au niveau r�gional, "a d�but� sa partie il y a deux ans et il a fini par devenir chef du PD contre la volont� de son parti, ce qui l'oblige � relancer sans arr�t et il ne peut pas s'arr�ter", estime M. Galli della Loggia.
�
Matteo Renzi est devenu chef du PD � l'issue des primaires fin 2013 qui ont vu la participation d'environ 2,5 millions de sympathisants de la gauche. Il a acc�d� � ce poste contre la volont� des caciques du parti qui ne l'ont jamais consid�r� comme l'un des leurs � cause de ses origines politiques d�mocrate-chr�tienne.
�
"Cet �ni�me assassinat politique sera difficile � oublier"
La destitution d'Enrico Letta par la direction du PD jeudi a �t� particuli�rement s�che et rapide, sans �gard envers celui qui �tait pourtant le num�ro deux du parti avant d'�tre nomm� chef du gouvernement.
�
Si Matteo Renzi "poursuit avec cette violence, ce mode d�cisionnaire, on pourra alors suspecter que c'est une m�thode de travail", a estim� Pipo Civati, membre de l'aile gauche du parti, trouvant que "Renzi est en train de devenir un personnage de roman noir".
�
"Cet �ni�me assassinat politique sera difficile � oublier", �crit le quotidien La Repubblica, tandis qu'Il Fatto quotidiano s'interroge: "Mais Renzi, combien de temps il va durer?".
Articles en relation
