TITRE: La presse grecque parle de censure apr�s l'interdiction d'un documentaire sur les migrants
DATE: 2014-02-14
URL: http://www.lemonde.fr/europe/article/2014/02/14/la-presse-grecque-parle-de-censure-apres-l-interdiction-d-un-documentaire-sur-les-migrants_4367069_3214.html
PRINCIPAL: 0
TEXT:
Pol�mique en Gr�ce autour de l'interdiction d'un documentaire sur le naufrage de migrants
Le Monde |
� Mis � jour le
14.02.2014 � 19h33
Censure ou protection de l'instruction�? La pol�mique enfle en Gr�ce , o� la justice a interdit la diffusion d'un reportage sur un naufrage en mer Eg�e qui avait caus� en janvier la mort de 12�migrants. Le motif communiqu� vendredi 14 f�vrier par des sources judicaires est que ��l'instruction �tait en cours�� au moment o� le reportage devait �tre diffus�, mardi�11�f�vrier. Une explication qui ne convainc pas les m�dias .
Lire ausi�: En Espagne, le bilan des migrants noy�s s'alourdit et la pol�mique gagne en intensit�
Le quotidien Ta Nea, plus gros tirage de Gr�ce, a qualifi� l'interdiction d'atteinte � la libert� de la presse et ��de censure pr�ventive��. Le journaliste d'investigation charg� de l'�mission devant diffuser le reportage, Stavros Th�odorakis, a exprim� son incompr�hension en d�clarant que ��si les journalistes devaient demander l'autorisation de la justice pour faire une enqu�te ��, aucune de ses pr�c�dentes �missions sur des affaires retentissantes n'aurait �t� r�alis�e ces derni�res ann�es.
Dans une ordonnance judiciaire adress�e � la cha�ne de t�l�vision priv�e Mega, la juge d'instruction charg�e de l'enqu�te sur ce naufrage avait interdit l'�mission. Ce reportage revenait sur les circonstances du naufrage � la fin de janvier de l'embarcation qui transportait 28�migrants, principalement des Afghans.
LES GARDE-C�TES ONT-ILS PROVOQU� LE NAUFRAGE�?
Douze personnes, principalement des femmes et des enfants, sont mortes dans le naufrage du bateau, qui avait �t� remorqu� par les garde-c�tes grecs apr�s avoir �t� arr�t� au large de l'�lot de Farmakonisi (sud-est) . Une enqu�te judiciaire a �t� ouverte, car plusieurs rescap�s avaient accus� les garde-c�tes de les avoir fait chavirer en les remorquant � trop vive allure en direction des c�tes turques.
Lire aussi�: La Gr�ce va supprimer le droit de vote des �trangers aux �lections municipales
Les autorit�s grecques ont cependant d�menti tout mauvais traitement envers les migrants et contest� que l'embarcation ait �t� dirig�e vers les c�tes turques, en violation des conventions internationales. Dimanche dernier, l'�pave du bateau chavir� a �t� retrouv�e, et les corps de quatre des douze disparus avaient pu �tre extraits. Deux cadavress avaient d�j� �t� retrouv�s apr�s le naufrage par les garde-c�tes turcs, tandis que d'autres corps se trouvent encore dans l'�pave.
De nombreux migrants se noient chaque ann�e en mer Eg�e, principale porte d'entr�e dans l' Union europ�enne pour les personnes fuyant leur pays, pauvre ou en guerre, en provenance d'Asie ou d' Afrique .
