TITRE: PSG-Valenciennes : attention au pi�ge avant la Ligue des champions - RTL.fr
DATE: 2014-02-14
URL: http://www.rtl.fr/actualites/sport/football/article/psg-valenciennes-attention-au-match-piege-avant-la-ligue-des-champions-7769715740
PRINCIPAL: 174395
TEXT:
Les Dossiers de RTL.fr - Ligue 1 : saison 2013-2014
PSG-Valenciennes : attention au pi�ge avant la Ligue des champions
Par Julien  Absalon | Publi� le 14/02/2014 � 16h36
Rudy Mater et Ezequiel Lavezzi lors du match de la 7e journ�e de Ligue 1 entre Valenciennes et le PSG
Cr�dit : AFP/D.Charlet
PR�SENTATION - Le Paris Saint-Germain accueille le Valenciennes FC vendredi soir (20h30) en ouverture de la 25e journ�e de Ligue 1, quatre jours avant son d�placement � Leverkusen.
C'est un match a priori facile pour le PSG. � l'occasion de la 25e journ�e, vendredi 14 f�vrier (20h30), le leader de la Ligue 1 re�oit au Parc des Princes le Valenciennes FC, qui n'est que dix-huiti�me au classement. Autant dire que sur le papier, la rencontre est sacr�ment d�s�quilibr�e.
Paris toujours priv� de Cavani
Pourtant, les derni�res prestations de l'�quipe de Laurent Blanc sont loin d'�tre resplendissantes . Si la possession de balle d�passe souvent les 60%, il y a un cruel manque d'inspiration et de mouvements lors des phases offensives. Et lorsque des occasions se pr�sentent, Lucas et Ezequiel Lavezzi se montrent bien trop brouillons dans les derniers gestes. "Quand on peut tuer le match, il faut tuer le match", affirmait jeudi l'entra�neur parisien Laurent Blanc.
Pour ne rien arranger, le club de la capitale, toujours priv� des bless�s Cavani, Jallet et Camara, n'est pas totalement efficace en d�fense ces derniers temps. Depuis le match de d�but d�cembre 2013 contre Sochaux (5-0), Salvatore Sirigu n'a gard� inviol�e sa cage que lors de deux rencontres, face � Nantes (5-0) et Bordeaux (2-0).
On n'ira pas en victime consentante, ce n'est pas le propre du sport
Ari�l Jacobs
Le PSG devra surtout se mettre en confiance apr�s son match nul de dimanche dernier contre Monaco (1-1) et avant son huiti�me de finale aller de Ligue des champions contre le Bayer Leverkusen , mardi 18 f�vrier. Il faudra donc � la fois ne pas laisser trop de forces contre les Nordistes tout en allant chercher les trois points sans les cadres Maxwell, Matuidi et Verratti, qui seront vraisemblablement laiss�s au repos au profit de Digne, Cabaye et Pastore.
Valenciennes croit � l'exploit
Autant dire qu'avec tous ces param�tres, les Valenciennois ont des raisons de croire � un r�sultat surprise. Pour leur coach Ari�l Jacobs, les Parisiens "auront plus que la t�te � la Ligue des champions, c'est une r�alit�. C'est un match un peu pi�ge pour eux. Et m�me si le PSG a beaucoup de possibilit�s pour faire tourner, c'est normal qu'un joueur ait un tout petit peu la t�te tourn� � ce match, et son corps aussi, pour �viter les contacts".
Le technicien belge, dont la formation vient de revenir � deux points du premier non rel�gable gr�ce � sa victoire contre Nice (3-1) , estime que ses joueurs devront �tre "� 250%", esp�re que ses adversaires ne seront "qu'� 70%" et pr�vient : "On n'ira pas en victime consentante, ce n'est pas le propre du sport. Ce respect doit dispara�tre � 20h30".
La r�daction vous recommande
