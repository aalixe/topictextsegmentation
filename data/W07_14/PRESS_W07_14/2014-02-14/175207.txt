TITRE: SNCF. Le trafic rétabli entre Rennes et Brest ! - Actualité - Le Télégramme
DATE: 2014-02-14
URL: http://www.letelegramme.fr/bretagne/tempete-ulla-2-000-voyageurs-bloques-dans-les-gares-14-02-2014-10036433.php
PRINCIPAL: 175206
TEXT:
SNCF. Le trafic rétabli entre Rennes et Brest !
15 février 2014 à 13h00
La SNCF estimait tard hier soir à 2.000 le nombre de personnes bloquées dans les gares de Rennes, Guingamp et Saint-Brieuc. Le trafic a repris ce matin progressivement.
Le trafic a repris ce matin dès 6h entre Rennes et Saint-Brieuc, puis à 13h entre Saint-Brieuc et Brest. On roule désormais sur toute la ligne Rennes - Brest...  Il faudra être plus patient entre Brest et Quimper où des cars sont prévus pour assurer les liaisons.
Les équipes techniques ont travaillé toute la nuit et toute la matinée sur les voies. Et poursuivront jusqu'au retour complet à la normale.
Retour sur une journée particulière
Dès 19h, la décision est prise de stopper tous les trains entre Rennes et Brest, dans les deux sens, "pour la sécurité des voyageurs", insiste la SNCF. Depuis 14h ce vendredi en effet, à cause de la tempête Ulla, des arbres et des branches tombent sur les voies. Les agents de la SNCF s'activent.
A 22h la SNCF estime pouvoir être en mesure de rétablir le trafic à 12 h ce samedi, quand le vent aura faibli et que les voies auront été déblayées.
En soirée, les "naufragés" sont pris en charge. La SNCF a rappelé ses agents qui étaient en repos pour venir aider les voyageurs. "Ils sont au chaud dans les gares et ont à manger", précise la SNCF. La protection civile et les services des préfectures sont aussi sur le pont. 
Les villes de Saint-Brieuc, Guingamp, Rennes ouvrent  des salles. Des voyageurs s'apprêtent à dormir dans des rames de train.
En complément
