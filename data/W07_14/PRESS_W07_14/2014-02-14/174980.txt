TITRE: Paris : la g�rante d'un bar assassin�e - Soci�t� - MYTF1News
DATE: 2014-02-14
URL: http://lci.tf1.fr/france/faits-divers/paris-la-gerante-d-un-bar-assassinee-8365780.html
PRINCIPAL: 174945
TEXT:
fait divers , meurtre , paris
Faits diversLa g�rante d'un bar, situ� avenue de la Grande Arm�e � Paris, a �t� tu�e dans son �tablissement vendredi en d�but de soir�e. L'auteur pr�sum� des coups de feu a �t� arr�t� quelques minutes apr�s les faits.
Un homme arm� a �t� interpell� ce vendredi en d�but de soir�e � la sortie d'un bar � Paris apr�s y �tre entr� et avoir ouvert le feu � plusieurs reprises, tuant la g�rante de l'�tablissement. Vers 18 heures, cet homme est entr� dans le bar L'Aiglon, avenue de la Grande-Arm�e, dans le 16e arrondissement, "il a sorti une arme et a tir� � plusieurs reprises", a expliqu� une source proche de l'enqu�te.
�
La g�rante de l'�tablissement a �t� touch�e par trois projectiles "au niveau du thorax" et est d�c�d�e quelques instants plus tard, selon cette source. Peu apr�s, des policiers ont aper�u un homme sortant du bar "avec une arme � la main", et sont parvenus � l'interpeller.
Commenter cet article
Vous devez �crire un avis
Nous suivre :
