TITRE: Airbus Group veut cr�er sa banque et pr�voit de racheter la Salzburg M�nchen Bank | Actualit�s entreprise Toulouse et Midi Pyr�n�es : Objectif News. Informations �conomie, business, politique et innovation
DATE: 2014-02-14
URL: http://www.objectifnews.com/Economie/airbus-group-banque-salzburg-munchen-bank-ag-toulouse-14022014
PRINCIPAL: 174512
TEXT:
Airbus Group veut cr�er sa banque et pr�voit de racheter la Salzburg M�nchen Bank
le 17/02/2014 � 17h16�� ObjectifNews.com
Economie Midi-Pyr�n�es
Le groupe Airbus veut racheter la Salzburg M�nchen Bank pour en faire son propre �tablissement bancaire.
Airbus Group a annonc�, ce vendredi 14 f�vrier, la conclusion d'un accord en vue d'acqu�rir la banque allemande Salzburg M�nchen Bank AG. Objectif : cr�er son propre �tablissement bancaire.
L'avionneur europ�en Airbus Group veut sa banque de groupe. Dans un communiqu�, l'entreprise annonce son intention de racheter la banque allemande Salzburg M�nchen Bank, pour "�largir les capacit�s de financement du groupe".
Airbus Group Bank
"La Salzburg M�nchen Bank, d�tenue � 100 % par le Raiffeisenverband Salzburg, est une banque bas�e � Munich, disposant d'une licence bancaire � part enti�re et servant une client�le de PME ainsi qu'une client�le priv�e", explique le groupe Airbus dans un communiqu�.
Une fois l'op�ration boucl�e, la banque sera renomm�e Airbus Group Bank et aura pour fonction de fournir des solutions de financement � toutes les activit�s du groupe europ�en d'a�rospatiale et de d�fense.
Meilleurs d�lais en 2014
"Airbus Group entend finaliser l'accord dans les meilleurs d�lais en 2014", indique la soci�t� dans le communiqu�, en pr�cisant que la transaction doit encore recevoir l'accord des autorit�s comp�tentes.
"L'acquisition de la Salzburg M�nchen Bank nous offre une base de lancement solide pour notre projet de banque de groupe", a d�clar� Harald Wilhelm, directeur financier d'Airbus Group, "� l'avenir, l'ensemble du groupe b�n�ficiera ainsi d'une plus grande flexibilit� financi�re".
LaTribune.fr
