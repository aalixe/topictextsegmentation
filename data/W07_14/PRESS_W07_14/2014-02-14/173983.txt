TITRE: "Star Wars Identities" : une exposition interactive avec tous les personnages de la saga - RTL.fr
DATE: 2014-02-14
URL: http://www.rtl.fr/actualites/culture-loisirs/cinema/article/star-wars-identities-une-exposition-interactive-avec-tous-les-personnages-de-la-saga-7769714066
PRINCIPAL: 173981
TEXT:
Les Dossiers de RTL.fr - "Star Wars Identities : L'exposition"
"Star Wars Identities" : une exposition interactive avec tous les personnages de la saga
Par St�phane Boudsocq | Publi� le 14/02/2014 � 14h07
L'exposition Star Wars Identit�s
Cr�dit : Lucasfilm LTD
VID�O - Une des expositions �v�nement de ce d�but d'ann�e : "Star Wars Identities", ouvre ses portes samedi 15 f�vrier � la Cit� du Cin�ma de Saint-Denis.
C'est l'exposition �v�nement de ce d�but d'ann�e : Star Wars Identit�s ouvre ses portes samedi 15 f�vrier � la Cit� du Cin�ma � Saint-Denis jusqu'au 30 juin, avec RTL.
Un parcours interactif
L'exposition est un d�dale de pi�ces qui revisitent l'univers des 6 films de la saga de George Lucas. Mais tout l'int�r�t de Star Wars Identities c'est de faire de vous un des personnages du monde de Lucas.
Tout au long du parcours, on cr�e notre personnage, humain ou non en passant devant 10 bornes interactives o� l'on nous pose des questions sur nos origines, nos qualit�s physiques et morales. Star Wars c'est du spectacle mais �a parle aussi de courage, de loyaut�, de famille, d'engagement � mi-chemin entre la mythologie et Shakespeare.
A la fin de l'exposition, sur un �cran, on d�couvre notre personnage,  cr��, dessin�, selon nos choix durant le parcours, puis il sera envoy�  par mail.
Tous les h�ros de la saga sont r�unis
Luke Skywalker, Han Solo, princesse Leia, Yoda : ils sont tous l�. Au total, pr�s de 200 objets originaux sont expos�s pour la premi�re fois en Europe : costumes, croquis,  maquettes, �l�ments de d�cors parfois impressionnants comme le vaisseau  dans lequel le jeune Anakin, (papa de Luke et futur Dark Vador), fait la  course dans La menace fant�me.
Parmi les tr�s grands moments de ce parcours on retrouve l'original de Han Solo fig� dans une plaque de carbone, (L'empire contre attaque), les maquettes de tous les vaisseaux de la flotte imp�riale r�unies dans une immense vitrine et puis toutes  les �tudes qui ont servi � imaginer le personnage de Yoda, un des  ma�tres jedi.
L'entr�e co�te 19,90 euros pour les adultes, 14,90 euros pour les enfants, il y des tarifs sp�ciaux et aussi une boutique qui vend des objets de la Guerre des �toiles.
"Star Wars Identities : L'exposition" du 15 f�vrier au 30 juin 2014 � la Cit� du cin�ma
Cr�dit : X3 Productions
