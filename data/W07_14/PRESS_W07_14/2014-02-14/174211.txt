TITRE: Mourinho allume Wenger: "Un spécialiste de l'échec" - 7SUR7.be
DATE: 2014-02-14
URL: http://www.7sur7.be/7s7/fr/1510/Football-Etranger/article/detail/1793841/2014/02/14/Mourinho-allume-Wenger-Le-specialiste-de-l-echec.dhtml
PRINCIPAL: 174206
TEXT:
Ce n'est pas le grand amour entre Jos� Mourinho et Ars�ne Wenger.
� getty.
� getty.
Ars�ne Wenger s'est attir� les foudres de son homologue portugais de Chelsea en d�clarant que ce dernier n'assumait pas �tre candidat au titre. "Si vous d�tes que vous n'�tes pas candidat, vous ne pouvez pas perdre", avait lanc� l'entra�neur d'Arsenal.
Quand un journaliste britannique lui a soumis les propos de l'entra�neur fran�ais ("Wenger a dit que vous avez la peur d'�chouer"), "The Special One" est sorti de ses gonds.
"Ars�ne Wenger est un sp�cialiste de l'�chec, pas moi", a attaqu� Mourinho en conf�rence de presse. "S'il a raison et que j'ai peur d'�chouer, c'est parce que �a m'est rarement arriv�. Peut-�tre qu'il a raison, mais peut-�tre que je n'ai pas l'habitude d'�chouer. La v�rit�, c'est que lui est un sp�cialiste. Huit ans sans le moindre troph�e, c'est un �chec. Si �a m'arrivait � Chelsea, je quitterais Londres et je ne reviendrais pas", a-t-il comment� de mani�re cinglante".
Lire aussi
