TITRE: SPORT - � Sotchi, �a drague sur Tinder dans le village olympique - France 24
DATE: 2014-02-14
URL: http://www.france24.com/fr/20140213-sotchi-athletes-utilisent-tinder-faire-rencontres-jeux-olympiques-sexe/
PRINCIPAL: 174018
TEXT:
Texte par St�phanie TROUILLARD Suivre stbslam sur twitter
Derni�re modification : 13/02/2014
Aux JO d'hiver, les sportifs ne sont pas seulement concentr�s sur leurs r�sultats. Certains d'entre eux profitent de la comp�tition pour essayer de trouver l'�me s�ur. Ils utilisent notamment l'application de rencontres en ligne Tinder.
Dans le village olympique de Sotchi, 2 800 athl�tes vivent en vase clos pendant quinze jours. Cette promiscuit� entre des jeunes gens venus du monde entier favorise comme � chaque olympiade les rencontres amoureuses. � l�heure du num�rique, les sportifs utilisent les r�seaux sociaux pour draguer les autres participants aux JO.
Comme l�explique la snowboardeuse am�ricaine Jamie Anderson dans le magazine " US Weekly ", l�application la plus populaire s�appelle Tinder : "Tinder au village olympique, c�est une autre dimension. Dans le village de montagne, il n�y a que des athl�tes, c�est marrant ! Il y a tellement de beaux mecs !".
� Jamaica Bobsled Team (@Jambobsled) 12 Février 2014
Cette application, cr��e en 2012, permet de mettre en relation des utilisateurs. L�abonn� doit indiquer s�il appr�cie ou non un autre profil. Si l�attraction est r�ciproque, les deux personnes peuvent alors �changer des messages.
Une autre snowboardeuse, la N�o-Z�landaise Rebecca Torr a indiqu� sur son compte Twitter qu�elle allait utiliser ce syst�me � Sotchi pour "trouver l�amour". Avec humour, la sportive a indiqu� qu�elle �tait d��ue de voir que l��quipe de bobsleigh de la Jama�que n��tait pas pr�sente sur Tinder. Un message qui lui a finalement permis de rencontrer quelques jours plus tard les "Rasta Rockets".
"Trop distrayant"
L�application n�offre toutefois pas que des avantages. Selon Jamie Anderson, elle peut m�me devenir dangereuse : "C�est beaucoup trop distrayant. J�ai effac� mon compte pour me concentrer sur les Jeux olympiques". Une sage d�cision qui lui a finalement permis de d�crocher l�or dans l��preuve du slopestyle.
Ce n�est pas la premi�re fois que le sexe est au c�ur des pr�occupations des athl�tes. En 2012 � Londres, la gardienne de but am�ricaine Hope Solo avait bris� un tabou en expliquant dans le magazine " ESPN " que l�olympiade donnait lieu � des orgies : "J'ai vu des gens coucher � l'air libre, sur les pelouses, entre les b�timents, les gens se l�chent et deviennent "dirty"". Pour prot�ger les athl�tes, le Comit� international olympique a annonc� qu�ils allaient distribuer 100 000 pr�servatifs durant les JO.
Les rencontres homosexuelles sont en revanche beaucoup plus compliqu�es � Sotchi. D�apr�s Ibtimes , Hunters, une version russe de Tinder pour la communaut� gay, a �t� pirat�e et mise hors service. Les utilisateurs ont re�u un message les informant qu�ils risquaient d��tre arr�t�s pour propagande homosexuelle. "Le Kremlin pourrait �tre derri�re cette attaque afin de d�courager les homosexuels de venir � Sotchi", note ce site internet anglais. En juin dernier, une loi punissant la "propagande" homosexuelle devant mineurs de peines d'amende et de prison a en effet �t� promulgu�e par le pr�sident russe Vladimir Poutine . Ce texte a provoqu� une vive pol�mique, notamment dans les pays occidentaux et entra�n� des menaces de boycott des Jeux olympiques.
Premi�re publication : 13/02/2014
