TITRE: L'emploi salari� est reparti en hausse au 4e trimestre 2013...
DATE: 2014-02-14
URL: http://www.boursier.com/actualites/economie/l-emploi-salarie-est-reparti-en-hausse-au-4eme-trimestre-2013-23017.html
PRINCIPAL: 173546
TEXT:
L'emploi salari� est reparti en hausse au 4e trimestre 2013...
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � Tr�s l�g�re progression pour l'emploi salari� dans les secteurs marchands non agricoles : il a augment� de 0,1% au quatri�me trimestre 2013, ce qui mat�rialise 14.700 cr�ations de postes, selon l'estimation "flash" publi�e vendredi par l'Insee . C'est le premier trimestre de cr�ation nette d'emplois depuis le premier trimestre 2012 (+3.500). Au troisi�me trimestre, 15.600 postes avaient �t� d�truits dans ces secteurs, pr�cise l'Insee, 17.000 destructions ayant �t� annonc�es dans un premier temps. Sur l'ensemble de 2013, 65.500 emplois ont �t� d�truits dans le priv� en France...
Au quatri�me trimestre, la hausse a �t� dop�e par le secteur tertiaire (+0,3%), port� par l'int�rim, l'emploi dans l'industrie et la construction continuant de reculer (respectivement de -0,5% et de -0,4%). Hors int�rim, l'emploi marchand a diminu� de 9.200 postes,soit -0,1%, apr�s 20.600 postes au trimestre pr�c�dent.
L�g�re hausse du salaire mensuel
"L'emploi int�rimaire a fortement progress� au quatri�me trimestre (+23.900 postes), revenant ainsi � son niveau de mi-2012. En 2013, l'int�rim a gagn� 35.400 postes, soit 6,9�% de ses effectifs, les deux tiers de cette hausse s'observant au 4�me�trimestre", d�taille l'Insee.
La Dares (direction des �tudes et des statistiques du minist�re du Travail), annonce que le salaire mensuel de base dans les entreprises de plus de 10 salari�s a progress� de 0,2% au quatri�me trimestre et de 1,5% sur l'ensemble de 2013. Par comparaison, le taux d'inflation annuelle se situait � 0,7% � fin d�cembre. L'indice du salaire horaire de base des ouvriers et des employ�s a augment� pareillement de 0,2%, sa progression sur 2013 �tant de 1,5%.
Claire Lemaitre � �2014, Boursier.com
