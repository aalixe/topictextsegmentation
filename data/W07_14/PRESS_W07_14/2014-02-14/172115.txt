TITRE: L�OCDE pr�sente une norme d��changes d�informations - L'Orient-Le Jour
DATE: 2014-02-14
URL: http://www.lorientlejour.com/article/854494/locde-presente-une-norme-dechanges-dinformations.html
PRINCIPAL: 172114
TEXT:
L�OCDE pr�sente une norme d��changes d�informations
Fraude fiscale
Abonnez-vous
L'Organisation pour la coop�ration et le d�veloppement �conomiques (OCDE) a pr�sent� hier une norme mondiale ��unique�� d'�change de renseignements entre les administrations fiscales des pays volontaires pour lutter contre l'�vasion et la fraude fiscale.
� l'issue de leur sommet de Saint-P�tersbourg en septembre, les dirigeants du G20 avaient demand� � l'OCDE de leur pr�senter les instruments techniques permettant le passage � un �change automatique des donn�es � des fins fiscales avant la mi-2014.
Dans un rapport d'une quarantaine de pages, l'OCDE d�taille cette nouvelle norme et d�finit les renseignements relatifs aux comptes financiers � �changer, les institutions financi�res soumises � d�claration, les diff�rents types de comptes et les contribuables concern�s.
Cette nouvelle norme, que plus de 40 pays se sont engag�s � mettre en �uvre rapidement, sera officiellement pr�sent�e pour adoption par les ministres des Finances du G20 qui se r�unissent les 22 et 23 f�vrier � Sydney.
��Cette initiative va r�ellement changer les r�gles du jeu��, a estim� le secr�taire g�n�ral de l'OCDE dans un communiqu�. ��Avec la mondialisation du syst�me financier mondial, il est de plus en plus facile pour les contribuables d'effectuer, de conserver et de g�rer des placements en dehors de leur pays de r�sidence��, rappelle Angel Gurria. ��Cette nouvelle norme sur l'�change automatique de renseignements va intensifier la coop�ration fiscale internationale.��
La nouvelle norme est largement bas�e sur les travaux ant�rieurs de l'OCDE consacr�s � l'�change automatique de renseignements. Elle int�gre les avanc�es r�alis�es dans ce domaine au sein de l'Union europ�enne et les efforts en cours en vue de renforcer les normes mondiales de lutte contre le blanchiment de capitaux, pr�cise l'OCDE.
Elle reconna�t �galement le r�le de catalyseur jou� par l'application de la loi am�ricaine relative au respect des obligations fiscales concernant les comptes �trangers (dite loi Fatca) dans l'action men�e par le G20 pour parvenir � un �change automatique de renseignements dans un contexte multilat�ral.
(Source�: Reuters)
