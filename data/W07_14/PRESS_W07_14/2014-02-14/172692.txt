TITRE: Syrie: 5.000 morts depuis le d�but de Gen�ve-2 (diplomate US) | International | RIA Novosti
DATE: 2014-02-14
URL: http://fr.ria.ru/world/20140214/200473591.html
PRINCIPAL: 172690
TEXT:
Syrie: un village lib�r� des djihadistes dans la province de Lattaqui�
Pr�s de 5.000 personnes ont trouv� la mort en Syrie depuis le 22 janvier, date de lancement du dialogue inter-syrien, dit Gen�ve-2 , a annonc� l'ambassadrice permanente des Etats-Unis aupr�s de l'Onu, Samantha Power.�
"Selon certaines informations, pr�s de 5.000 personnes ont �t� tu�es depuis le d�but des n�gociations de Gen�ve. C'est la p�riode de mortalit� la plus intense depuis le d�but du conflit et il ne s'agit que des trois derni�res semaines", a d�clar� la diplomate am�ricaine � l'issue de consultations du Conseil de s�curit� de l'Onu � huis clos consacr�es � l'assistance humanitaire au peuple syrien.
Selon Mme Power, il faut admettre que la situation en Syrie se d�grade et que des mesures suppl�mentaires s'imposent.�
Les consultations en question se sont d�roul�es en pr�sence de Valerie Amos, secr�taire g�n�rale adjointe de l'Onu aux Affaires humanitaires et coordinatrice des secours d'urgence. Les diplomates de 15 pays examinent au Conseil de s�curit� deux textes de projet de r�solution visant � assurer le libre-acc�s de l'aide humanitaire au peuple de Syrie , pays secou� depuis bient�t trois ans par un conflit opposant les troupes gouvernementales � l'opposition arm�e.�
Le premier texte a �t� pr�sent� par l'Autriche, le Luxembourg et la Jordanie, le second par la Russie. Dans le m�me temps, la d�l�gation russe insiste sur l'adoption d'une d�claration du pr�sident du Conseil de s�curit� sur la lutte contre le terrorisme en Syrie. "Si la r�solution permet de fournir une aide sur le terrain, elle sera opportune", a dit Valerie Amos.�
Samantha Power a soutenu le premier projet de r�solution qui pr�voit une menace de sanction si un cessez-le-feu n'est pas instaur� sous quinze jours. "Nous consid�rons que c'est un document juste et comptons sur le soutien de cette r�solution par nos coll�gues au sein du Conseil de s�curit� des Nations unies", a-t-elle indiqu�.�
"Nous n'avons pas besoin d'une r�solution pour la forme. Il vaut mieux ne rien adopter qu'adopter une r�solution inutile", a poursuivi la diplomate am�ricaine.�
De son c�t�, l'ambassadeur permanent de Russie aupr�s des Nations unies Vitali Tchourkine� a esp�r� que les parties arriveraient � un consensus sur le texte de la r�solution. "Le fait que la situation humanitaire soit critique et que des mesures suppl�mentaires s'imposent pour l'am�liorer nous rapproche", a expliqu� le diplomate russe.�
La Russie avait plus t�t rejet� le projet de r�solution pr�sent� par ses partenaires du Conseil de l'Onu, craignant que ce document n'aggrave davantage la situation en Syrie et ne torpille les efforts de r�glement politique traduits par la tenue de la conf�rence de Gen�ve.�
Add to blog
