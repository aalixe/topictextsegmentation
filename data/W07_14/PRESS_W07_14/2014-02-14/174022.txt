TITRE: Insolite: A Sotchi, les athl�tes se chauffent sur l'appli �Tinder� -  News Sotchi 2014: Les coulisses - tdg.ch
DATE: 2014-02-14
URL: http://www.tdg.ch/sotchi2014/coulisses/sotchi-athletes-chauffent-tinder/story/11384572
PRINCIPAL: 174018
TEXT:
A Sotchi, les athl�tes se chauffent sur l'appli �Tinder�
Par J�r�my Santallo . Mis � jour le 13.02.2014
Championne olympique en slopestyle, la snowboardeuse am�ricaine Jamie Anderson a confi� que les athl�tes se passionnaient pour une application de rencontres.
La snowboardeuse Rebecca Torr a rencontr� les bobeurs jama�cains.
Image: Twitter
Tinder, c'est quoi?
Facebook - L'inscription � Tinder se fait via son compte Facebook. L'application r�cup�re alors les donn�es de l'utilisateur, comme le pr�nom, ses photos de profil, son �ge, sa liste d'amis et des informations sur les pages qu'il aime.
G�olocalisation - Tinder se sert ensuite de la g�olocalisation pour trouver les autres utilisateurs les plus proches et fait d�filer les photos de ces profils. Deux personnes ne peuvent �tre mises en relation que si elles ont mutuellement �lik� leur page. Elles peuvent alors communiquer par messagerie instantan�e.
Mots-cl�s
JO Sotchi 2014 �
Cinq jours apr�s le d�but des Jeux olympiques d'hiver, le marronnier du sexe est revenu au go�t du jour � Sotchi. C'est la snowboardeuse Jamie Anderson -qui fait partie de notre s�lection des plus belles sportives pr�sentes en Russie- qui aurait lanc� les hostilit�s en r�v�lant au magazine � US Weekly � que plusieurs athl�tes, dont elle et ses compatriotes, occupaient leur temps libre avec �Tinder�, rapporte Slate .
Adul�e par les jeunes, cette application mobile permet de faire des rencontres de proximit� sur la base de photos (voir encadr� ci-contre). �Tinder au village olympique, c�est une autre dimension. Dans le village de montagne, il n�y a que des athl�tes. C�est hilarant. Il y a tellement de beaux gosses!�
Olympic Snowboarder @Jme_Anderson : "Tinder in the Olympic Village Is Next Level"  via @usweekly http://t.co/9v9GEbo2C7 #Sochi2014 �
� Tinder (@Tinder) 12 F�vrier 2014
L�application n�offre toutefois pas que des avantages. Selon Jamie Anderson, elle peut m�me devenir trop divertissante: �A un moment, j'ai d� me dire "OK", c�est beaucoup trop distrayant. J�ai effac� mon compte pour me concentrer sur les Jeux olympiques�. Une d�cision qu'elle ne doit pas regretter puisque cela lui a permis de d�crocher l�or olympique en slopestyle.
Une autre snowboardeuse, Rebecca Torr, a fait parler d'elle au d�but de ces Jeux olympiques. Elle a indiqu� sur son compte Twitter qu�elle souhaitait utiliser �Tinder� pour �trouver l�amour� � Sotchi. La N�o-Z�landaise, qui pr�texte une blague, s'est ensuite moqu�e des m�dias de son pays qui relayaient l'information.
Toutefois, elle semblait particuli�rement contente d'avoir enfin rencontr� l'�quipe jama�caine de Bobsleigh.
I found them!! @Jambobsled such an honor to meet you guys! #inspirations Goodluck guys I'm cheering for you! pic.twitter.com/d5Me0AHR5y
� Rebecca Possum Torr (@PossumTorr) 12 F�vrier 2014
Les organisateurs des Jeux olympiques d'hiver ont distribu� pr�s de 100'000 pr�servatifs aux 2800 athl�tes. �Chaque athl�te re�oit deux pr�servatifs par jour�, a comment� mercredi la station radio russe Echo de Moscou. Le Comit� International Olympique (CIO) pr�voit ainsi �une vie sexuelle anim�e dans le village olympique, comme lors des pr�c�dents jeux de Vancouver.� (Newsnet)
Cr��: 13.02.2014, 15h18
