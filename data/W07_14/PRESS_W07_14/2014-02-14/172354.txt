TITRE: Une image de l'iPhone 6 fait surface sur le Net
DATE: 2014-02-14
URL: http://www.francemobiles.com/actualites/id/201402141392323890/une_image_de_l_iphone_6_fait_surface_sur_le_net.html
PRINCIPAL: 0
TEXT:
�
Une image de l'iPhone 6 fait surface sur le Net
Alors que l'iPhone 6 ne cesse de faire du bruit sur le Net, il n'existait jusqu'ici aucune image illustrant ce smartphone. Ceci n'est plus le cas, car une image de ce qui devrait �tre le ch�ssis de l'iPhone 6. Cependant, la photo en question soul�ve pas mal de questions, surtout du c�t� de certains experts, qui affirment qu'elle n'est qu'un montage.
L'image en question provient de Sonny Dickson, un adolescent australien vivant � Melbourne et qui encha�ne les r�v�lations sur son site Internet. R�put� comme �?tr�s suivi?� par les �quipes de Cupertino, ce jeune adolescent a post� quelques clich�s de ce qui devrait �tre l'iPhone 6 en expliquant que ses sources travaillent pour Apple en Chine.
Il faut n�anmoins souligner que les photos publi�es par Sonny Dickson n'ont pas fait l'unanimit� au sein de la communaut�. En effet, la police de caract�re utilis�e n'est pas la m�me qu'utilise normalement Apple (Myriad Pro Light). Certains soulignent aussi que la texture utilis�e est identique sur les deux faces du pr�sum� iPhone, ce qui n'est pas le cas normalement sur les smartphones de la marque � la pomme.
�
Plus d'actualit�s sur le m�me th�me
Le 3310 de Nokia refait surface
Consid�r� comme un mod�le de t�l�phone mobile culte, le 3310 du constructeur Nokia refait surface en 2014, soit apr�s avoir �t� abandonn� il y a d�j� plusieurs ann�es de cela ...� [Lire]
La toute premi�re image de BBM pour Android OS fait son apparition
Les choses commencent � se confirmer encore plus autour de la version Android OS du syst�me de messagerie instantan�e BBM. En effet, la toute premi�re image d�montrant la fonctionnalit� du service BBM sur la plateforme mobile Android OS a ...� [Lire]
Apocope invente la reconnaissance d?image sur mobiles
Apocope, agence marketing mobile,  vient de lancer un tout nouveau service pour mobiles relativement digne d?int�r�t : il s?agit de la reconnaissance d?image depuis un simple t�l�phone portable ...� [Lire]
Netsize et Cognacq-Jay Image veulent proposer un acc�s simplifi� � la vid�o sur mobile
Netsize et Cognacq-Jay Image ont d�cid� de r�unir � nouveau leurs comp�tences afin de mettre en place une offre compl�te de vid�o sur mobile ...� [Lire]
ModeLabs con�oit pour Virgin Mobile un mobile et une gamme d'accessoires � son image
Gr�ce � son offre "Mobile On Demand", le groupe ModeLabs va accompagner Virgin Mobile dans sa strat�gie de diff�renciation afin de permettre � l'op�rateur de t�l�phonie mobile virtuel de commercialiser un t�l�phone et une gamme d'accessoires ...� [Lire]
�
