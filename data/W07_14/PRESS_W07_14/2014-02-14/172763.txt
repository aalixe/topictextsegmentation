TITRE: H�t�ro, bisexuel, intersexuel... : les cinquante combinaisons du genre selon Facebook - High-Tech - MYTF1News
DATE: 2014-02-14
URL: http://lci.tf1.fr/high-tech/facebook-s-ouvre-aux-genres-8365268.html
PRINCIPAL: 172762
TEXT:
homosexualit� , sexualit� , facebook
High-TechDepuis jeudi, les abonn�s am�ricains de Facebook peuvent s'affirmer librement avec des options de genre (transsexuel, bisexuel, s'interroge...) disponibles sur chaque fiche de profil. Une fa�on de faire de Facebook un lieu o� les gens peuvent �tre eux-m�mes et exprimer leur v�ritable identit�, se f�licite le site.
Les utilisateurs am�ricains de Facebook vont pouvoir faire leur total coming-out num�rique. Depuis jeudi, ils peuvent ajouter leur genre sur leur profil. Vous ne serez plus juste "un homme" ou "une femme", mais vous pourrez mettre "transsexuel" ou m�me "intersexuel". Et comme le r�seau social a d�cid� d'�tre tr�s � l'�coute de ses abonn�s, ces derniers ne seront plus juste "elle" (she/her) ou "lui/il" (he/him/his), mais pourrait demander � ce que le "on" soit utilis�.
Facebook a annonc� cet ajout sur sa page Diversit� . "Si pour beaucoup, ce changement ne signifiera pas grand-chose. Pour ceux qui ont �t� touch�s par le pass�, c'est un grand pas", a �crit un porte-parole du g�ant am�ricain. "Nous consid�rons cela comme une fa�on de faire de Facebook un lieu o� les gens peuvent �tre eux-m�mes et exprimer librement leur identit�".
Ce r�sultat est le fruit de discussions avec des associations LGBT de d�fense des droits des lesbiennes, gays, bisexuels et transgenre. Cela permis d'ajouter des options � la cat�gorie "sexe". Il y aura d�sormais une case "personnalis�" avec possibilit� de choisir parmi une cinquantaine de propositions dont transsexuel, bisexuel, intersexuel... et m�me "s'interroge sur son genre".
Les associations n'ont pas tard� � r�agir positivement � l'annonce. "Beaucoup de transgenres vont �tre plus que ravis d'avoir enfin les bons mots pour parler d'eux sur le r�seau", a indiqu� le Transgender Law Center de San Francisco sur son site, avec un gros "like" (le pouce en l'air, symbole d'acquiescement popularis� par Facebook) en illustration. "Cela sera sans doute difficile pour certains de comprendre l'importance de pouvoir choisir parmi diff�rents genres. Ce qui ne se reconnaissait dans les st�r�otypes homme/femme pouvait avoir une identit� complexe � exprimer. Nous applaudissons Facebook de pouvoir enfin permettre � ces personnes d'�tre r�ellement elles."
Cette modification de profil n'est pour le moment accessible que sur le seul r�seau des Etats-Unis. Mais Facebook envisage de l'�tendre progressivement aux autres r�gions du monde.
Commenter cet article
Ecrire votre commentaire ici ...
Vous devez �crire un avis
nico_37 : 50 possibilit�s ???? C'est possible d'avoir la liste parce que au del� de 5 ou 6 je vois pas trop.....???
Le 14/02/2014 � 13h02
georgesroma : Un autre moyen astucieux de mieux cibler les utilisateurs et de revendre leurs listes aux annonceurs.
Le 14/02/2014 � 11h09
