TITRE: "Star Wars, c'est l'Iliade de notre �poque" - BFMTV.com
DATE: 2014-02-14
URL: http://www.bfmtv.com/divertissement/star-wars-cest-liliade-epoque-711228.html
PRINCIPAL: 174734
TEXT:
> Cin�ma
"Star Wars, c'est l'Iliade de notre �poque"
L'exposition Star Wars Identities d�marre samedi 15 f�vrier � la Cit� du cin�ma � Saint-Denis, au nord de Paris. A cette occasion, BFMTV.com a rencontr� Patrice Girod, grand sp�cialiste de la saga en France et consultant de l'exposition.
r�agir
Star Wars, Patrice Girod est tomb� dedans quand il �tait petit. En 1977, il d�couvre le premier film de la saga. Il a 10 ans et c'est pour lui un v�ritable "choc culturel". Patrice Girod a cr�� en France LucasFilm Magazine, aujourd'hui disparu, a rencontr� George Lucas, a jou� les figurants sur le tournage de La Menace Fant�me, a doubl� quelques voix. Il a �galement �t� commissaire de l'expo Star Wars � la Cit� des sciences, en 2005. Aujourd'hui, il dirige les expositions de sciencefictionarchives.com , � qui l'on doit notamment l'expo aux mus�e des Arts d�coratifs .
A l'occasion de l'ouverture de l'expo Star Wars Identities, samedi 15 f�vrier � la Cit� du cin�ma, nous lui avons demand� pourquoi, depuis plus de trente ans, la saga Star Wars remporte toujours un tel succ�s.
> Pourquoi Star Wars pla�t-il toujours autant?
C'est un conte mythologique moderne, c'est l'Iliade de notre �poque. Ca joue toujours sur les m�mes ressorts. Ca a beau �tre de la science fiction, un monde onirique, ce qui nous pla�t dans Star Wars, c'est l'humanit� de Luke Skywalker et de son p�re Anakin. C'est tr�s humain, ce ne sont pas que des machines et des effets sp�ciaux. Le squelette du film c'est le voyage du h�ros, comment un ado devient un h�ros, pour la premi�re trilogie. Et comment un ado devient un m�chant, en faisant de mauvais choix, dans la seconde. Le th�me, c'est la qu�te de l'identit�.
Encore une fois ce sont les th�mes de la mythologie, comment le personnage quitte sa famille, affronte son p�re, doit le tuer. Lucas a m�lang� plusieurs mythes, emprunt� par exemple � la l�gende du roi Arthur. Sa grande force c'est de tout m�langer et d'obtenir quelque chose de coh�rent.
Quand on a dix ans, on ne pense pas � tous les codes de la mythologie, mais cela a une r�sonance. On l'absorbe sans le savoir mais on l'absorbe. C'est ce qui fait le succ�s de Star Wars. Tous les autres films de science fiction, ce sont des films pop corn.
> Que dire du "clivage" entre les fans de la premi�re et de la deuxi�me trilogie?
Dire "je pr�f�re tel ou tel film", c'est normal, on a tous des go�ts diff�rents. Le tout premier de 1977 m'a marqu� � jamais. Mais les enfants qui sont n�s avec les nouveaux films, ceux qui ont vu Jar Jar Binks � 8 ans, continuent d'adorer Jar Jar Binks aujourd'hui, alors que les gens de ma g�n�ration ne l'aiment pas.
Et puis on oublie que les six films s'�talent sur 35 ans. George, quand il a fait les nouveaux films, avait �volu�. Star Wars, ce sont des films pour les enfants, pas pour des trentenaires qui d�cortiquent tout. Il faut essayer de voir Star Wars comme une oeuvre globale qui s'�tale sur 35 ans et toujours garder � l'esprit son �me d'enfant.
> Qu'attendez-vous des prochains �pisodes?
Je suis tr�s content qu'il y en ait de nouveaux. George Lucas avait dit qu'il n'y en aurait plus. On aura tous des d�ceptions, et on aura s�rement des joies. Rien ne dit que demain, ils ne vont pas faire le meilleur Star Wars. Pourquoi commencer � critiquer? On jugera sur pi�ce!
Star Wars, c'est la jeunesse. J. J. Abrams, qui va r�aliser le prochain, est jeune, dans l'air du temps, il va je l'esp�re insuffler cette jeunesse. Lawrence Kasdan, sc�nariste de la premi�re trilogie, est de retour, et Phil Tippett, l'inventeur du go motion [l'animation image par image, NDLR] et le roi de l'animation est aussi pressenti. C'est un message.
A lire aussi
