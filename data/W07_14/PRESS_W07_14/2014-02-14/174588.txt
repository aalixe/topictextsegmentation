TITRE: OM : Payet et la possibilit� de retrouver Galtier
DATE: 2014-02-14
URL: http://www.topmercato.com/79633,1/om-payet-et-la-possibilite-de-retrouver-galtier.html
PRINCIPAL: 174583
TEXT:
Man U peut-il le faire ? Lire
Cette fois, il ne pourra en rester qu'un Lire
D�faite interdite pour Guardiola Lire
Actu Sport.fr
�
OM : Payet et la possibilit� de retrouver Galtier
Les propos de Jos� Anigo � l'encontre de Christophe Galtier font r�agir. En conf�rence de presse, le coach olympien a laiss� entendre que le technicien st�phanois avait le profil pour entra�ner l'OM.
Apr�s Anigo, Dimitri Payet s'est pr�sent� devant les journalistes et ces derniers n'ont pas manqu� l'occasion d'interroger le milieu de terrain tricolore sur la venue �ventuelle du Marseillais, les deux hommes s'�tant c�toy�s dans le Forez. S'il se refuse d'entrer dans ce jeu, le R�unionnais ne tarit pas d'�loges � son �gard. "Ce n'est pas mon r�le de choisir l'entra�neur. Apr�s, c'est un entra�neur que je connais, se souvient-il. Il a apport� sa pierre � ce que je suis aujourd'hui. On a une relation tr�s particuli�re donc c'est vrai que s'il vient � l'OM, je serais content de travailler encore avec lui."
