TITRE: JO de Sotchi. Suivez en direct les épreuves - Sotchi 2014 - Le Télégramme
DATE: 2014-02-14
URL: http://www.letelegramme.fr/sports/jeux-olympiques-sotchi-2014/jo-de-sotchi-le-programme-du-jour-14-02-2014-10036102.php
PRINCIPAL: 173807
TEXT:
JO de Sotchi. Suivez en direct les épreuves
14 février 2014 à 17h45
Ski alpin, ski de fond, skeleton, freestyle, biathlon, patinage artistique... Ce vendredi à Sotchi, les épreuves seront nombreuses. Suivez en direct les JO 2014 sur letelegramme.fr !
17 H 45. Martin Fourcade reçoit sa deuxième médaille d'or
Le champion de biathlon déjà médaillé reçoit son deuxième titre olympique des mains de Tony Estanguet. La Marseillaise résonne.
Capture d'écran
17 H 39. Patinage artistique : déception pour Florent Amodio
Le patineur qui participe à ses premiers Jeux Olympiques n'a pas réalisé de véritable performance dans ce programme. Il a posé la main plusieurs fois sur la glace après un saut et n'a pas effectué de quadruple. En larmes à l'annonce de son résultat, il se classe 4ème au classement provisoire.
17 H 30. Patinage artistique : au tour de Florent Amodio
17 H 25. Patinage artistique : Brian Joubert très attendu
24 patineurs sont en lice dans ce programme libre. Brian Joubert, dernier espoir de médaille du jour, et Florent Amodio sont les deux Français de la compétition. Jeremy Abbott, l'Américain prend la tête du classement, avec une belle prestation sur une musique du groupe Muse.
16 H 30. Biathlon : pas de médaille pour la France
La victoire de Martin Fourcade jeudi, n'aura pas porté chance aux dames. Les quatre françaises qui se sont lancées dans le 15 km individuel dames n'ont pas réussi à remporter de médailles. Mais Anaïs Bescond est tout de même 5ème au classement provisoire : "deux fautes de trop au tir", commente l'athlète, visiblement déçue mais pas découragée. La Biélorusse Domracheva prend la première place du classement.
 
— Morgan (@_Morgan23) 14 Février 2014
15 H 45. Hockey sur glace : victoire de la Suède 
La Suède s'impose face à la Suisse sur le score 1-0. Dans le groupe C, ils remportent ainsi leur deuxième match. 
15 H 30. Biathlon : 5/5 au premier tir pour Anaïs Bescond
La Française à effectué 5/5 au premier tir. Elle est 5ème à 7''3 de Anastasia Kuzmina. 
15 H 00. Biathlon : le 15 km individuel dames
C'est l'heure de la course ! La première concurrente est l'Ukrainienne Semerenko. Avec Anaïs Bescond, Maire Dorin Habert, Marie-Laurent Brunet et Marine Bolliet, la France peut espérer décrocher une médaille. 
 
