TITRE: Affaire Merah: deux garde � vues lev�es - BFMTV.com
DATE: 2014-02-14
URL: http://www.bfmtv.com/societe/affaire-merah-deux-garde-a-vues-levees-710954.html
PRINCIPAL: 173096
TEXT:
r�agir
Deux hommes, plac�s en garde � vue cette semaine dans l'enqu�te sur les possibles complicit�s de Mohamed Merah , ont �t� rel�ch�s jeudi soir � Toulouse. Dans ce dossier, les enqu�teurs cherchent � savoir si le tueur au scooter de Toulouse et de Montauban a b�n�fici� d'aide pour commettre ses crimes, en mars 2012.
Interpell�s lundi soir et mardi matin, les deux hommes de 38 et 44 ans ont �t� interrog�s sur des armes qu'ils auraient d�tenues, et qui provenaient d'un casse perp�tr� en 2011 dans la banlieue toulousaine. L'une des armes de Mohamed Merah provenait en effet du m�me stock.
Apr�s sa garde � vue, l'un des deux hommes est retourn� en prison, d'o� les policiers l'avaient extrait pour l'interroger. L'autre est reparti libre.
