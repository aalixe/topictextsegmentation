TITRE: Evgueni Plushenko : Ce n'est pas de cette fa�on que je voulais terminer ma carri�re - Jeux Olympiques 2013-2014 - Patinage artistique - Eurosport
DATE: 2014-02-14
URL: http://www.eurosport.fr/patinage-artistique/jeux-olympiques-sotchi/2013-2014/evgueni-plushenko-ce-n-est-pas-de-cette-facon-que-je-voulais-terminer-ma-carriere_sto4134937/story.shtml
PRINCIPAL: 0
TEXT:
Evgueni Plushenko : "Ce n'est pas de cette fa�on que je voulais terminer ma carri�re"
Par�Alexandre COIQUIL�le 13/02/2014 � 18:42, mis � jour le 13/02/2014 � 23:53 @Coik
Bless� au dos et forfait pour l'�preuve individuelle, Evgueni Plushenko est revenu sur les raisons de son renoncement face � la presse. C'est une chute survenue lors de son entra�nement de mercredi qui a tout d�clench�.
Evgueni Plushenko (RUS/forfait de l'�preuve individuelle): "Hier (mercredi), je suis tomb� sur le quadruple � l'entra�nement et j'ai ressenti un probl�me au dos. Aujourd'hui (jeudi), je suis all� � l'entra�nement pour voir ce que je pouvais faire mais je n'ai pas r�ussi � sauter. Je n'ai pas pu patiner plus de 7 minutes. J'ai essay�, essay� et encore essay�. Durant l'�chauffement (juste avant le programme court), j'ai fait un triple boucle et un triple lutz mais apr�s le premier triple axel, j'ai ressenti une terrible douleur dans la jambe et je me suis tr�s mal r�ceptionn� pour le second. Je ne pouvais plus sentir mes jambes ensuite. Ca me faisait si mal que j'ai d� renoncer. Je suis sinc�rement d�sol� pour mes fans et pour tout le monde mais j'ai voulu y croire jusqu'� la fin. J'en ai presque pleur�. C'est vraiment dur, croyez-moi. Ce n'est pas de cette fa�on que je voulais terminer ma carri�re. Je suis tr�s d��u. Mais j'ai fait de mon mieux. Je dois maintenant me reposer et me faire soigner. Ensuite je devrais avoir de la r��ducation. Bien s�r que je vais continuer � patiner mais dans les galas."
�
