TITRE: Ski: Sandro Viletta, seigneur des anneaux du super-combin� - rts.ch - sport - tous les dossiers - 2014 - jo sotchi
DATE: 2014-02-14
URL: http://www.rts.ch/sport/dossiers/2014/jo-sotchi/5613676-ski-sandro-viletta-seigneur-des-anneaux-du-super-combine.html
PRINCIPAL: 173673
TEXT:
Ski: Sandro Viletta, seigneur des anneaux du super-combin�
14.02.2014�13:21 mise � jour le 15.02.2014 � 20:31
Le Grison a d�jou� tous les pronostics. [Christophe Ena - Keystone]
La Suisse tient sa 6e m�daille aux Jeux olympiques de Sotchi! Un peu plus d'une heure apr�s le sacre du fondeur Dario Cologna, Sandro Viletta a r�colt� l'or lors du super-combin�. Le Grison de 28 ans n'occupait que la... 14e place apr�s la descente, � 1"64 du Norv�gien Jansrud. Kostelic en argent, Innerhofer 3e.
Cette m�daille est des plus inattendues. Sandro Viletta (28 ans) n'est mont� qu'une seule fois sur un podium en Coupe du monde, remportant le super-G de Beaver Creek en d�cembre 2011. Sa meilleure performance jusqu'ici en super-combin� avait �t� enregistr�e en janvier � Wengen, o� il avait termin� 4e!
Sandro Viletta a sign� le 2e meilleur temps en slalom. [Fabrice Coffini - AFP] Sandro Viletta, qui avait r�ussi le 14e temps de la descente, a sorti la manche de sa vie en slalom. Le skieur de La Punt signait le 2e meilleur chrono derri�re le Slovaque Adam Zampa, surprenant 5e du classement final, offrant ainsi � la Suisse son premier titre olympique dans un super-combin� (ou combin�) masculin en ski alpin.
Jamais pr�sent dans les quinze premiers d'un slalom en Coupe du monde, Sandro Viletta a pu commencer � croire en ses chances de podium apr�s le passage d'Ivica Kostelic. Surprenant 7e de la descente, le favori de cette �preuve terminait � 0''34 du futur champion olympique. Le Croate de 34 ans conservait n�anmoins jusqu'au bout sa 2e place, avec 0''13 d'avance sur le m�daill� de bronze Christof Innerhofer. Vainqueur de la descente le matin, Kjetil Jansrud �chouait au 4e rang, � plus d'une demi-seconde de l'Italien.
Les autres Suisses en lice dans cette �preuve n'ont pas sign� d'exploit. Plus � l'aise en descente mais d�cevants le matin, Carlo Janka et Beat Feuz prenaient respectivement les 8e et 15e rangs. Quant � Mauro Caviezel, il prenait tous les risques en slalom mais partait � la faute
Apr�s Lara Gut (3e) et Dominique Gisin (1�re) mercredi, la Suisse r�colte ainsi une 3e m�daille olympique en ski alpin � Sotchi.
