TITRE: Victoires de la Musique : pourquoi Daft Punk boude la c�r�monie - RTL.fr
DATE: 2014-02-14
URL: http://www.rtl.fr/actualites/culture-loisirs/musique/article/victoires-de-la-musique-pourquoi-daft-punk-boude-la-ceremonie-7769713811
PRINCIPAL: 0
TEXT:
Les Dossiers de RTL.fr - 29�mes Victoires de la Musique
Victoires de la Musique : pourquoi Daft Punk boude la c�r�monie
Par Marie-Pierre  Haddad | Publi� le 14/02/2014 � 13h45
Daft Punk, lors des 56�me Grammy Awards, le 26 janvier 2014.
Cr�dit : AFP
D�CRYPTAGE - Les Fran�ais de Daft Punk ne participeront pas � la 29e c�r�monie des Victoires de la Musique vendredi 14 f�vrier. Une d�cision qui s'explique notamment par leur succ�s mondial.
Les Daft Punk ont survol� l'ann�e 2013. Apr�s le succ�s ph�nom�nal de leur dernier album Random Access Memories, ils ont r�cemment domin� la 56�me c�r�monie des Grammy Awards . Nomm�s dans cinq cat�gories, ils sont repartis avec autant de statuettes .
�
Ces nominations outre-Atlantique contrastent avec leur absence aux prochaines Victoires de la Musique , le 14 f�vrier. Le groupe �lectro a refus� de figurer dans la liste des artistes �ligibles. Christophe Palatre, directeur des Victoires d�plorait au micro de RTL la d�cision des deux Fran�ais : "C'est dommage qu'ils ne soient pas l� cette ann�e".
La r�daction vous recommande
VOTRE AVIS : Sacr�s aux �tats-Unis, �tes-vous choqu�s que les Daft Punk boycottent les Victoires de la musique ?
Une maison de disque am�ricaine
Daft Punk boudera donc la c�r�monie fran�aise. "Ils ont peut-�tre d'autres priorit�s. C'est un groupe international, ils l'ont �t� d�s le d�part donc c'est normal que les d�cisions soient moins port�es sur le pays dont on est originaire", explique Christophe Palatre. Pourtant apr�s ce premier refus, ce dernier leur a propos� d'�tre "invit�s d'honneur" � la 29e c�r�monie des Victoires. Sans succ�s.
�
Avec leur dernier album Random Access Memories, les Daft Punk coupent les ponts avec la France. Les deux "robots DJ" ont quitt� leur label historique Virgin � Emi chez qui ils �taient depuis 1996 pour signer chez les Am�ricains Columbia (Sony).�
Tensions avec la Sacem
Autre point de d�saccord avec la France, la guerre entre le groupe �lectro et la Sacem concernant les droits d'auteur per�us par le groupe.
Daft Punk s'est affranchi de l'instance fran�aise sur tout ce qui concerne Internet et l'utilisation de leur musique dans les publicit�s ou � la t�l�vision. �
L'attrait d'une prestation avec Stevie Wonder
Daft Punk, c'est avant tout un concept. Toujours sous leurs casques, les deux Fran�ais ont r�ussi � cr�er un �v�nement � chaque passage � la t�l�vision.
La sc�ne du Zenith de Paris o� se d�rouleront les Victoires de la Musique semble �tre moins attractive pour le groupe �lectro qui a pr�f�r� se rendre sur celle des Grammy Awards aux c�t�s de Steve Wonder, Pharell Williams et Nile Rodgers .
Daft Punk aux Grammy Awards
La r�daction vous recommande
