TITRE: Ski de fond: Dario Cologna: �J'ai disput� la course parfaite� -  News Sotchi 2014: L'actu des JO - 24heures.ch
DATE: 2014-02-14
URL: http://www.24heures.ch/sotchi2014/lactu-des-jo/dario-cologna-j-dispute-course-parfaite/story/11070051
PRINCIPAL: 174388
TEXT:
Votre adresse e-mail*
Votre email a �t� envoy�.
�C'est assur�ment l'une des plus belles courses que j'ai vue de sa part, peut-�tre m�me la course de sa vie. Sur le plan technique, il a �t� incroyable de ma�trise, d'autant plus dans ces conditions rendues difficiles avec la chaleur (r�d: 11 degr�s)�, a estim� sa coach norv�gienne Guri Hetland.
�C'est vrai que c'�tait difficile�, a reconnu Cologna. �Je ne me rappelle plus avoir eu si chaud lors d'une course. Mais quand on est en forme et que, mentalement, on est pr�t � faire face, les conditions de course ne sont plus un probl�me�, a-t-il relev�.
Fort mentalement
Question forme physique et une force mentale, le Grison n'est effectivement pas � plaindre. �Il se d�gage de lui une puissance, mais aussi un calme impressionnants. Il en impose dans l'aire de d�part, et on remarque tout de suite qu'il est l'homme � battre�, a soulign� Hippolyt Kempf, chef du ski de fond chez Swiss-Ski .
�Je pense aussi qu'il n'a jamais �t� aussi solide mentalement�, a rench�ri Guri Hetland. � Sa blessure � la cheville (r�d: d�chirure ligamentaire � la mi-novembre) l'a rendu encore plus fort . Malgr� ce coup du sort, il n'a pas arr�t� de se battre et, jour apr�s jour, il me r�p�tait qu'il serait pr�t � temps pour les Jeux et qu'il allait remporter des m�dailles�, a-t-elle racont�.
La Norv�gienne a �galement relev� que son prot�g� n'�tait pas du genre � se contenter du minimum. �C'est un grand champion et il sait se remobiliser en toute occasion. Apr�s son titre dimanche sur skiathlon, il a pris le temps de savourer. Mais il a ensuite vite tourn� la page pour se concentrer sur la suite�, a-t-elle dit.
Dario et Gianluca Cologna ici au Tessin en 2012
Courir avec son fr�re Gianluca
Et il faut compter sur Cologna pour en faire de m�me lors de la deuxi�me semaine des Jeux. �J'aurai un bon coup � jouer lors du 50 km de cl�ture (dimanche prochain)�, a-t-il soulign�. �Je souhaite aussi disputer le sprint par �quipes (jeudi). C'est une occasion unique de courir aux Jeux avec mon fr�re Gianluca�, a-t-il expliqu�, ajoutant qu'il n'avait en revanche pas encore d�cid� s'il allait participer ce dimanche au relais 4 x 10 km.
Peut-�tre jamais aussi fort, Cologna peut aussi s'appuyer sur une structure efficace. Alors qu'il a souvent �t� trahi par le pass� par son mat�riel lors des grands rendez-vous, il b�n�fice � Sotchi de ce qui se fait de mieux.
�C'est notre r�le chez Swiss-Ski de nous mettre � la hauteur d'un tel champion�, a expliqu� Hippolyt Kempf. �Du coup, on a �norm�ment oeuvr� dans le secteur du mat�riel en achetant un camion sp�cial et des machines, mais aussi en engageant une personne d�vou�e � cette question. Depuis les pr�c�dents Jeux de Vancouver en 2010, nous avons investi 600'000 francs. Ces efforts ont pay�, a-t-il conclu. (si/Newsnet)
Cr��: 14.02.2014, 16h15
