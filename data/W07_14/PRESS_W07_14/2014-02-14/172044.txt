TITRE: Heineken d�tr�ne Kronenbourg en France - Economie - La Voix du Nord
DATE: 2014-02-14
URL: http://www.lavoixdunord.fr/economie/heineken-detrone-kronenbourg-en-france-ia0b0n1916955
PRINCIPAL: 172043
TEXT:
- A +
Heineken a d�tr�n� l'embl�matique Kronenbourg sur le march� fran�ais de la bi�re en 2013, avec des parts de march� sup�rieures tant en valeur qu'en volumes, a annonc� jeudi le brasseur n�erlandais.
La marque n�erlandaise d�tient d�sormais 18,4% des parts de march� en valeur et pour la premi�re fois une majorit� des parts de march� en volume (17,3%), selon le barom�tre Iri 2013, qui compile les ventes en hyper et supermarch�s, soit 75% des ventes de bi�re en France.
Kronenbourg, marque fran�aise du brasseur danois Carlsberg, d�tient 16,2% du march� hexagonal en volume et 12,5% en valeur.
En 2012, le groupe Heineken (marques Heineken, Desperados, Pelforth, Affligem, Fischer...) estimait d�j� �tre devenu le premier brasseur en France.
En 2013, les ventes de bi�re en France ont recul� de 3% en raison de l'augmentation des taxes (+160%) qui �tait intervenue en d�but d'ann�e. Le demi au comptoir a particuli�rement souffert avec un march� en repli de 7% dans les caf�s-h�tels-brasseries. Une situation qui a surtout touch� les grandes marques de bi�re, plus pr�sentes dans les caf�s que les petits brasseurs.
Le chiffre d'affaires d'Heineken en France a ainsi recul� de 10% l'an dernier, � 1,610 milliard d'euros mais 2014 devrait �tre une meilleure ann�e, selon le pr�sident du groupe dans l'Hexagone Pascal Sabri�.
Heineken emploie plus de 4.000 personnes en France.
Kronenbourg d�voilera de son c�t� ses chiffres en France mercredi prochain. En 2014, le brasseur alsacien f�te ses 350 ans.
R�agir � l'article
