TITRE: Un enfant tu� dans l'explosion d'un appartement, la m�re soup�onn�e d'infanticide
DATE: 2014-02-14
URL: http://www.24matins.fr/enfant-tue-dans-lexplosion-dun-appartement-la-mere-soupconnee-dinfanticide-80242
PRINCIPAL: 173091
TEXT:
>        Un enfant tu� dans l�explosion d�un appartement, la m�re soup�onn�e d�infanticide
Faits Divers
Photo d'illustration
Fin janvier, un gar�on de 4 ans a �t� tu� dans l'explosion d'un appartement. Sa m�re est aujourd'hui soup�onn�e de meurtre sur mineur.
Le 24 janvier dernier, un petit gar�on de 4 ans a perdu la vie dans l� explosion d�un appartement � Nice. Le dernier �tage de l�immeuble avait �t� souffl� par la d�flagration. La m�re du gar�on avait �t� gri�vement bless�e dans l�accident et elle avait �t� hospitalis�e. Les policiers l�avaient interrog� bri�vement.
D�apr�s les 1ers �l�ments de l�enqu�te, l�explosion serait due � une fuite de gaz.
Mais certains�indices�ont rapidement r�v�l� que le d�c�s de l�enfant remonte � avant l�explosion. Une th�se confirm�e par le m�decin l�giste. La m�re du petit gar�on est donc soup�onn�e d� infanticide et a �t� mise en examen pour ��meurtre sur mineur�� et ��destruction d�un bien par l�effet d�une substance explosive��.
Explosion � Nice : la m�re a avou�
Au moment de sa garde-�-vue, la m�re de l�enfant a donn� des ��aveux d�taill�s�� � la police. Elle aurait tent� de se donner la mort par asphyxie. La jeune femme de 26 ans, d�origine cap-verdienne, devait compara�tre devant le tribunal correctionnel de Nice�3 jours apr�s l�explosion, pour une affaire d� escroquerie en bande organis�e�concernant des faux-papiers portugais. La m�re du gar�on avait d�j� �t� incarc�r�e pour cette affaire. Son fils avait �t� plac� en famille d�accueil.
Cr�dits photos : Misha Stepanov
