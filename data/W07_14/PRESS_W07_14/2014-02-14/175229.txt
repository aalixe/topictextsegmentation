TITRE: Malgr� des b�n�fices en baisse, NVIDIA a bien r�sist� � la crise des PC - Next INpact
DATE: 2014-02-14
URL: http://www.pcinpact.com/news/85915-malgre-benefices-en-baisse-nvidia-a-bien-resiste-a-crise-pc.htm
PRINCIPAL: 175225
TEXT:
Malgr� des b�n�fices en baisse, NVIDIA a bien r�sist� � la crise des PC
Les Tegra ne sont par contre pas � la f�te
Le cr�ateur des cartes graphiques GeForce a d�voil� hier soir le bilan financier de son dernier trimestre fiscal, clos le 26 janvier 2014. Et le cam�l�on peut avoir le sourire. Si son b�n�fice net a certes recul� d'une ann�e sur l'autre, il reste malgr� tout important et son chiffre d'affaires est en progression.
Donn�es pour le dernier trimestre fiscal de NVIDIA
Des ventes mais aussi des d�penses en hausse
NVIDIA a de quoi �tre heureux. Avec la crise sans pr�c�dent qui a touch� le march� des PC et la forte concurrence dans le secteur mobile (en particulier de Qualcomm), l'entreprise am�ricaine pouvait en effet craindre le pire. Elle a pourtant r�ussi � limiter les d�g�ts tout le long de l'ann�e, y compris au dernier trimestre. Ce dernier a ainsi cumul� un chiffre d'affaires de 1,144 milliard de dollars, en hausse de 8,6 % en trois mois et de 3,3 % en un an. Sa marge brute a de plus augment� de 1,2 point d'une ann�e sur l'autre, pour atteindre 54,1 %. N�anmoins, son b�n�fice a chut� de 15,5 %, atteignant tout de m�me 147 millions de dollars.
�
Ce b�n�fice inf�rieur malgr� une marge brute sup�rieure s'explique logiquement par des d�penses plus importantes. NVIDIA a notamment augment� ses investissements en Recherche & D�veloppement de 13 % (+38,6 millions de dollars), tandis que ses d�penses administratives et marketing ont cr� de plus de 11 % (+11,6 millions de dollars).
�
Sur toute son ann�e fiscale, le bilan est par contre l�g�rement diff�rent. Du fait des premiers mois assez difficiles, son chiffre d'affaires total a r�gress� de 3,5 % d'une ann�e sur l'autre, avec tout de m�me 4,13 milliards de dollars � la cl�. Du fait de co�ts de production plus faibles (-10 %), sa marge brute est en tr�s l�g�re hausse (+1,8 %), atteignant tout de m�me 54,9 %.�� titre de comparaison, la marge brute d'AMD n'a �t� que de 37 % en 2013, contre 23 % en 2012. NVIDIA �volue donc dans une autre sph�re sur ce point. La soci�t� californienne se rapproche d'ailleurs plut�t d' Intel , qui a r�alis� une marge brute de 59,8 % l'an pass�.
�
Mais l� encore, � l'instar du dernier trimestre, malgr� une marge en hausse, ses b�n�fices pour son ann�e fiscale ont r�gress� de pr�s de 22 %, soit 440 millions de dollars contre 562,5 millions l'ann�e pr�c�dente. La faute � une explosion de ses frais en Recherche & D�veloppement (+188,6 millions de dollars sur 1 an),�
Les GPU tirent NVIDIA vers le haut
Pour�Jen-Hsun Huang, le co-fondateur de NVIDIA et son actuel PDG, ce dernier trimestre a �t� bien sup�rieur � ses attentes, en grande partie d� au succ�s des cartes d�di�es aux joueurs sur PC. Ses produits haut de gamme Tesla et Quadro ont pour leur part atteint des plus hauts niveaux historiques. Sur toute l'ann�e, ses GPU ont tout de m�me g�n�r� 3,468 milliards de dollars (+7 %), soit 84 % de son chiffre d'affaires total.
�
Donn�es pour la derni�re ann�e fiscale de NVIDIA
�
Les Tegra, du fait d'une concurrence tr�s forte, s'�croulent (-48 %), avec seulement 398 millions de dollars cumul�s sur douze mois, n�anmoins, la baisse lors du dernier trimestre a �t� moins importante (-37 %). Enfin, le reste de ses activit�s, c'est-�-dire la gestion de ses brevets principalement, est stable avec un chiffre d'affaires de 66 millions au dernier trimestre et de 264 millions sur toute l'ann�e.
�
Rappelons que la soci�t� am�ricaine, contrairement � AMD, n'est pas pr�sente dans les consoles de nouvelle g�n�ration que sont la Wii U, la Xbox One et la Playstation 4. Le lancement de ces consoles a ainsi eu des cons�quences tr�s positives sur les derniers bilans financiers du cr�ateur de l'Athlon et du Fusion, en particulier lors du second semestre 2013. Les r�sultats de NVIDIA, qui ne profitent pas de cette manne non n�gligeable, sont donc d'autant plus remarquables. On notera toutefois que l'entreprise reste assez discr�te sur les r�sultats de la Shield .
�
D'apr�s ses pr�visions, NVIDIA s'attend � un chiffre d'affaires d'environ 1,05 milliard de dollars pour son premier trimestre 2014 (entre f�vrier et avril), ceci pour une marge brute aux alentours de 54,2 %. Notez enfin qu'en bourse, l'action de la soci�t� est actuellement en forte hausse, la valorisant entre 9,8 et 10 milliards de dollars, en progression de 50 % depuis le mois de novembre 2012. Le concurrent d'AMD est toutefois encore bien loin de son plus haut historique d'octobre 2007, �poque o� il valait plus de 21 milliards de dollars en bourse.
