TITRE: Sur Facebook, on peut se définir comme «transsexuel»
DATE: 2014-02-14
URL: http://www.lefigaro.fr/secteur/high-tech/2014/02/14/01007-20140214ARTFIG00048-sur-facebook-on-peut-se-definir-comme-transsexuel.php
PRINCIPAL: 0
TEXT:
Publié
le 14/02/2014 à 08:34
Le réseau social ne se limite plus aux genres «homme» et «femme». Crédits photo : Noah Berger/ASSOCIATED PRESS
Dans sa version anglophone, le réseau social a décidé d'enrichir les deux choix initiaux - homme ou femme - pour définir son genre.
Publicité
Nouveauté sur la version anglophone du site Facebook: les internautes ont désormais un choix de genres qui se diversifie. Depuis ce jeudi 13 février, ils peuvent désormais se définir comme «transsexuel» ou «intersexuel», en plus des deux genres «homme» et «femme». Dans un post publié sur sa page «Facebook diversity» , le réseau social explique cette nouvelle fonctionnalité dans un texte agrémenté d'une photo du drapeau arc-en-ciel, symbole de la communauté homosexuelle et transgenre. «Aujourd'hui, nous sommes heureux d'offrir cette nouvelle option, qui vous aidera à afficher plus facilement votre propre identité sur Facebook» peut-on lire dans ce post.
Outre le genre, les abonnés ont aussi la possibilité de choisir le pronom personnel par lequel ils voudraient être interpellés dans les posts. A côté des classiques «lui/il» et «elle», il y aura désormais «on», jugé plus neutre. «Si pour beaucoup ces changements n'ont pas beaucoup d'importance, pour ceux qui en ont souffert c'est quelque chose d'important», écrit Facebook dans un post sur sa page Diversité, qui affiche une photo du drapeau arc-en ciel, étendard de la communauté homosexuelle et transgenre, flottant sur le campus du groupe dans la Silicon Valley. «Nous voyons ces évolutions comme un moyen supplémentaire pour faire de Facebook un lieu où les gens peuvent exprimer librement leur identité», ajoute le réseau social.
Le réseau social indique avoir collaboré avec des associations de défense des droits des lesbiennes, gays, bisexuels et transgenre pour créer ces nouvelles options qui peuvent être trouvées dans la catégorie «autres».
