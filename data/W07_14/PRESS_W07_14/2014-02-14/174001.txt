TITRE: Pinturault : �Un peu frustr酻 - Sotchi 2014 - Jeux Olympiques -
DATE: 2014-02-14
URL: http://sport24.lefigaro.fr/jeux-olympiques/sotchi-2014/actualites/pinturault-un-peu-frustre-679185
PRINCIPAL: 173996
TEXT:
Tweeter
-  AFP
Sorti dans le slalom du super-combin� ce vendredi, Alexis Pinturault est d��u d�avoir manqu� son premier rendez-vous avec les Jeux olympiques. Mais se projette d�j� vers la suite�
Comment avez-vous v�cu ce slalom�?
Alexis Pinturault�: Plut�t bien dans l�ensemble. Je ne pars pas tr�s bien. Le d�part �tait assez compliqu� avec une premi�re porte tr�s proche du d�part. Je n��tais pas vraiment dans le rythme mais je me remets plut�t bien. Apr�s le premier interm�diaire, j�ai r�ussi � skier un peu mieux. Malheureusement, je fais ma faute apr�s�
Comment expliquez-vous cette faute�?
Elle vient uniquement de moi. Je tape la porte avec ma chaussure et �a me colle les deux pieds et dans la double, je n�ai pas le temps de me d�patouiller et j�enfourche. �a fait partie du slalom.
Ressentiez-vous une pression particuli�re avant le d�part�de ce slalom ?
Pas sp�cialement m�me si bien entendu, il y a un peu plus de pression �tant donn� que ce sont les Jeux olympiques. On ne peut pas dire qu�on est sans pression au d�part quand on aux JO. Mais je pense que je l�avais tr�s bien g�r�e. Maintenant, on va se concentrer sur le g�ant et le slalom.
Il faut vraiment que je skie pour moi
�
Quel sentiment pr�domine�? La col�re�? La frustration�?
Il y avait de la place mais c��tait quand m�me serr�. Je suis un peu frustr� car j�aurais aim� faire quelque chose aujourd�hui. Mais on n�a rien sans rien. J�aurais peut-�tre d� �tre meilleur en descente. Et en slalom aussi. Je n�ai pas assez jou� et il faudra jouer sur la suite de la comp�tition. Un grand champion, �a sait rebondir et il va falloir le montrer.
Sentiez-vous une responsabilit� particuli�re apr�s le d�but de Jeux rat� du ski alpin fran�ais�?
Je ne sais pas trop. J�ai senti de la pression mais est-ce qu�elle �tait li�e � �a�? Je ne sais pas. Ce qui est s�r, c�est qu�il faut que je me concentre sur moi-m�me et rien d�autre. Il faut �tre un peu �go�ste. Si on est l�, c�est pour nous m�me et pour rien d�autre. Et gr�ce � nous-m�me surtout.
Le podium vous surprend-t-il�?
Oui, c�est un podium surprenant mais �a ne m��tonne pas du tout. Les Jeux olympiques, c�est souvent comme �a. Il y en a qui arrivent � briller quand on les attend le moins et briller quand on vous attend, c�est toujours plus compliqu�. Maintenant, je le sais et il faut vraiment que je skie pour moi.
Propos recueillis par Emmanuel Quintin, envoy� sp�cial � Sotchi
