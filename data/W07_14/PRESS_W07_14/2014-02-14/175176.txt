TITRE: Ligue 1 : Paris gagne sans forcer contre Valenciennes � metronews
DATE: 2014-02-14
URL: http://www.metronews.fr/sport/ligue-1-paris-gagne-sans-forcer-contre-valenciennes/mnbn!IASo6WagvQX8E/
PRINCIPAL: 175173
TEXT:
Cr�� : 14-02-2014 22:39
Ligue 1 : Paris gagne sans forcer
FOOTBALL � En g�rant ses efforts en vue de la Ligue des Champions, Paris s�est facilement d�barrass� de Valenciennes (3-0) vendredi au Parc des Princes. Lavezzi, touch� par la mort tragique de son oncle quelques jours auparavant, a marqu� un but en son hommage.
�
Ezequiel Lavezzi a inscrit le premier des trois buts parisiens contre Valenciennes. Photo :�AFP
C�est une habitude : les visiteurs du Parc des Princes viennent pour se d�fendre. Limiter les d�g�ts. Ils n�esp�rent gu�re plus. En formation tortue, triangle ou ligne triple, c�est, � chaque fois, une �quipe aux allures d�infanterie romaine qui vient courir apr�s le ballon. Les fantassins valenciennois ont ce soir du souffle et les reins solides. Mais, � force de subir, l�issue du combat est sans surprise : les Parisiens sont irr�sistibles � domicile.
Plut�t qu�un combat, c�est un entra�nement que semblent suivre les joueurs du PSG. Ne manque qu�aux Valenciennois des chasubles, des plots et des sifflets pour parfaire les conditions de ce match de pr�paration "pr�-Ligue des Champions". Car les Parisiens semblent avoir la t�te ailleurs � � Leverkusen, sans doute � tant ils loupent le plus facile dans une premi�re p�riode pourtant si ma�tris�e. M�me Ibra, gauche et nonchalant, manque la cible (4e, puis 8e).
Cabaye et Digne font le boulot
Les Parisiens n�ont pas le temps de s��nerver. Lavezzi, transcend� par le jeu apr�s une semaine marqu�e par le d�c�s tragique de son oncle, ouvre le score d�une reprise s�che, qui trompe Penneteau sans fioriture (1-0, 19e). Les Nordistes, bien en jambes mais inoffensifs, peuvent appr�cier la pr�cision des passes de Cabaye, align� d�entr�e pour la premi�re fois sous la tunique parisienne. Ils auront aussi constat� l�application de Lucas Digne, omnipr�sent c�t� droit, et remarqu� que J�r�my Menez, d�cidemment, n�est plus dans son assiette.
Berc�e par une ambiance toujours mollassonne, la s�ance de pr�paration se poursuit au retour des vestiaires. Et quand le rythme chancelle, deux buts coup sur coup, aussi faciles qu�impitoyables (demi-vol�e de Zlatan puis Kagelmacher csc) concr�tisent la domination des locaux (49e et 51e). La fin de match file en en pente douce. Les visiteurs peuvent retourner � leurs pr�occupations habituelles : le maintien. Les Parisiens, eux, reprennent leur train-train de VIP : g�rer les d�sormais huit points d�avance sur Monaco, et poursuivre, au Camp des Loges cette fois, la pr�paration du match de mardi, sur le terrain du Bayer Leverkusen.
Thomas Saintourens
