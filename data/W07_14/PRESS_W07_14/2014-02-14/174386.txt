TITRE: Enqu�te pour mise en danger de la vie d'autrui apr�s une plainte de Julie Gayet - 20minutes.fr
DATE: 2014-02-14
URL: http://www.20minutes.fr/societe/1299426-20140214-enquete-mise-danger-vie-autrui-apres-plainte-julie-gayet
PRINCIPAL: 174384
TEXT:
Julie Gayet pose pendant le photocall du jury au 15eme Festival de la Fiction TV � la Rochelle, le 11 octobre 2013 PDN/SIPA
JUSTICE - L'actrice d�nonce un harc�lement de paparazzi...
Une enqu�te a �t� ouverte apr�s une plainte pour �mise en danger de la vie d'autrui� de l'actrice Julie Gayet qui d�nonce un harc�lement de paparazzi, a  indiqu� vendredi � l'AFP une source judiciaire.
Ouverte par le parquet de Paris, cette enqu�te a �t� confi�e � la brigade de r�pression de la d�linquance aux personnes (BRDP), selon  cette source qui confirme une information du Monde.
Ces enqu�tes sont distinctes du proc�s au civil pour atteinte � la vie priv�e qui opposera la com�dienne de 41 ans � Closer le 6 mars � Nanterre.
Photographies
Les r�v�lations de Closer, le 10 janvier, s'appuyaient sur  des photos vol�es de Mme Gayet et M. Hollande, photographi�s � chaque  fois s�par�ment sur la voie publique, devant un immeuble parisien o� le  couple se retrouvait r�guli�rement.
Une semaine plus tard, le magazine publiait une nouvelle �dition en affirmant que la liaison entre le pr�sident et l'actrice  durait depuis deux ans.
Lors de sa conf�rence de presse du 14 janvier, M. Hollande  avait fait part de son �indignation totale� apr�s la publication du  dossier de Closer, mais avait annonc� qu'il n'attaquerait finalement pas  personnellement en justice l'hebdomadaire people.
Avec AFP
