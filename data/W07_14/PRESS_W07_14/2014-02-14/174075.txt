TITRE: CHRONOLOGIE de la crise politique en Italie | Reuters
DATE: 2014-02-14
URL: http://fr.reuters.com/article/frEuroRpt/idFRL6N0HS32E20140214
PRINCIPAL: 174066
TEXT:
CHRONOLOGIE de la crise politique en Italie
vendredi 14 f�vrier 2014 15h25
�
[ - ] Texte [ + ]
ROME, 14 f�vrier (Reuters) - La d�mission d'Enrico Letta, accept�e vendredi par le pr�sident Giorgio Napolitano, est un nouvel �pisode de la crise politique italienne n�e des �lections d'il y a un an qui n'ont pas permis la constitution d'une majorit� solide.
Retour sur les d�veloppements politiques en Italie depuis les �lections de f�vrier dernier.
2013
24/25 f�vrier - Les �lections l�gislatives ne produisent pas de majorit� claire. La coalition de centre gauche dirig�e par Pier Luigi Bersani, du Parti d�mocrate, est majoritaire � la Chambre des d�put�s mais l'�mergence des contestataires du Mouvement Cinq Etoiles de Beppe Grillo brouille les cartes et aucun parti n'est majoritaire au S�nat.
Or le syst�me bicam�ral italien octroie les m�mes pr�rogatives aux deux chambres du Parlement: l'Italie est dans une impasse.
20 mars - Le pr�sident de la R�publique, Giorgio Napolitano, dont le mandat expire le 15 mai, entame ses consultations avec les repr�sentants des partis.
Troisi�me �conomie de la zone euro, l'Italie peut difficilement se permettre de replonger dans une crise politique prolong�e apr�s le bouleversement provoqu� en 2011 par la chute du gouvernement de Silvio Berlusconi au sein de la zone euro. � Suite...
