TITRE: La croissance fran�aise limit�e � 0,3% en 2013 - Capital.fr
DATE: 2014-02-14
URL: http://www.capital.fr/bourse/actualites/la-croissance-francaise-limitee-a-0-3-en-2013-910822
PRINCIPAL: 172651
TEXT:
La croissance fran�aise limit�e � 0,3% en 2013
La croissance fran�aise limit�e � 0,3% en 2013
Source : Reuters
Tweet
Apr�s avoir stagn� au troisi�me trimestre, l'�conomie fran�aise a enregistr� une croissance de 0,3% au quatri�me trimestre, dop�e par la consommation des m�nages et un rebond de l'investissement, selon l'Insee. Sur l'ensemble de 2013, l'�conomie fran�aise affiche une croissance moyenne de 0,3%, soit plus que celle de 0,1% pr�vue par le gouvernement. /Photo prise le 29 ao�t 2013/REUTERS/Charles Platiau
L'�conomie fran�aise a enregistr� une croissance de 0,3% au quatri�me trimestre 2013, soit l�g�rement plus qu'attendu, pour retrouver enfin son niveau d'avant-crise il y a six ans � la faveur d'un rebond de l'investissement et d'une consommation des m�nages solide en fin d'ann�e.
Selon les premiers r�sultats des comptes nationaux publi�s vendredi par l'Insee, la croissance de la France en 2013 a atteint �galement 0,3% apr�s avoir �t� nulle en 2012, alors que le gouvernement tablait sur 0,1%.
"Ceci signifie que l'�conomie fran�aise est non seulement sortie de la r�cession mais aujourd'hui a repris un rythme de croissance de 1% par an si on extrapole sur l'ann�e 2014", a d�clar� sur France 2 le ministre de l'Economie Pierre Moscovici.
L'Allemagne, premi�re �conomie de la zone euro, a annonc� pour sa part une croissance de 0,4% sur le dernier trimestre et de 0,4% sur l'ensemble de 2013.
L'Insee a fait �tat dans le m�me temps d'un solde de cr�ations d'emplois positif (+14.700) dans les secteurs marchands de l'�conomie fran�aise au 4e trimestre et ce pour la premi�re fois depuis d�but 2012, gr�ce � l'int�rim.
PROFIL HEURT�
Malgr� ces chiffres, Pierre Moscovici a reconnu qu'une croissance sup�rieure � 1% �tait n�cessaire pour enrayer la hausse du ch�mage, le pari perdu du gouvernement fin 2013, et s'est dit persuad� qu'il �tait possible d'y parvenir en 2014 si les entreprises retrouvent confiance avec le "pacte de responsabilit�" que leur propose le gouvernement.
Les �conomistes attendaient en moyenne une hausse de 0,2% du PIB fran�ais au 4e trimestre, alors que l'Insee tablait encore fin d�cembre sur une hausse de 0,4%.
L'institut a d'autre part revu en hausse vendredi de 0,1 point les chiffres du PIB des premier et troisi�me trimestres 2013, ce qui fait qu'il n'a �t� n�gatif sur aucune p�riode. Mais le profil de la croissance sur l'ann�e � �t� heurt�: 0,0%, puis +0,6%, 0,0% et enfin +0,3%.
Cette performance permet n�anmoins � l'�conomie fran�aise d'aborder 2014 avec un acquis de croissance de 0,3%, qui cr�dibilise la pr�vision de hausse de 0,9% du PIB faite par le gouvernement dans son budget.
Pour Fabrice Montagne, �conomiste de la banque Barclays, ces chiffres, bien que faiblement positifs, dessinent un panorama relativement bon de l'�conomie fran�aise.
"En particulier, la r�vision � la hausse des trimestres pr�c�dents contredit nettement l'id�e d'un d�couplage entre la France et l'Allemagne", �crit-il dans une note, m�me si la croissance est encore trop faible pour inverser les tendances sur le march� du travail ou d'autres composants cycliques.
Le sc�nario pour 2014 s'annonce diff�rent: une reprise poussive est escompt�e en France, l'Insee tablant sur des hausses limit�es � 0,2% du PIB au premier comme au deuxi�me trimestres, alors que l'Allemagne envisage de son c�t� une croissance de 1,8% sur l'ann�e.
INVESTISSEMENT DES ENTREPRISES EN HAUSSE
En attendant, la France a retrouv� pour la premi�re fois au 31 d�cembre son niveau d'activit� l�g�rement sup�rieur (+0,1%) � celui du premier trimestre 2008, le dernier avant la crise.
Elle le doit � une consommation solide (+0,5%), les m�nages ayant sans doute puis� dans leur �pargne pour acheter, et surtout � un investissement en territoire positif (+0,6%) pour la premi�re fois depuis fin 2011.
Celui des entreprises a notamment progress� de 0,9% mais il reste n�anmoins inf�rieur de plus de 10% � son niveau de d�but 2008.
En cons�quence, la contribution de la demande int�rieure finale (hors stocks) � la croissance du quatri�me trimestre a atteint 0,5 point, un plus haut depuis d�but 2011.
Celle de la variation des stocks a �t� n�gative de 0,3 point en contrecoup de son niveau �lev� du 3e trimestre (+0,6 point) et celle du commerce ext�rieur positive de 0,2 point.
Les �conomistes attendent un d�but 2014 moins solide c�t� consommation, la fin 2013 ayant profit� de facteurs exceptionnels comme les achats de voitures avant le durcissement du malus �cologique au 1er janvier, le d�blocage de l'�pargne salariale par le gouvernement ou encore l'anticipation de la hausse de la TVA pour l'entretien des logements.
Edit� par Yves Clarisse
