TITRE: La croissance en France meilleure que prévu en 2013 - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-14
URL: http://tempsreel.nouvelobs.com/economie/20140214.OBS6365/la-croissance-en-france-meilleure-que-prevu-en-2013.html
PRINCIPAL: 0
TEXT:
Actualité > Economie > La croissance en France meilleure que prévu en 2013
La croissance en France meilleure que prévu en 2013
Publié le 14-02-2014 à 08h15
Mis à jour à 09h56
VIDEO. Elle a �t� de 0,3% sur l'ensemble de l'ann�e derni�re, selon l'Insee. Un chiffre sup�rieur de deux points aux pr�visions du�gouvernement, qui se r�jouit avec prudence de cette r�vision.
Pierre Moscovici. (ROBERT ALAIN/APERCU/SIPA)
La croissance �conomique en France a �t� de 0,3% en 2013, un taux un peu meilleur que pr�vu, a annonc� vendredi 14 f�vrier l'Institut national de la statistique et des �tudes �conomiques (Insee).
L'Insee pr�voyait en d�cembre une croissance moyenne du produit int�rieur brut pour 2013 de 0,2%, mais elle a revu en hausse l'�volution du PIB aux premier et troisi�me trimestres, d'une contraction de 0,1% � une stagnation. Le gouvernement pr�voyait quant � lui une croissance d'ensemble de 0,1%.
Le gouvernement prudent�
Une bonne nouvelle qu'a accueilli modestement Pierre Moscovici. Le ministre de l' Economie �a ainsi estim� sur France 2 que cette croissance n�cessitait d'"aller plus loin" pour "faire reculer le ch�mage ".
Je ne me satisfais pas, je dis : 'faisons plus', a-t-il ajout�.
Alain Vidalies, ministre des Relations avec le Parlement, s'est r�joui, de son c�t�, vendredi de cette r�vision l�g�rement � la hausse.
"C'est plut�t mieux que ce que l'on esp�rait, plut�t mieux que ce qui �tait pr�vu au d�but de l'ann�e, y compris au mois de juin. En m�me temps, il n'y a pas de quoi crier victoire", a d�clar� Alain Vidalies sur RTL.�"C'est un bon acquis. Ca justifie totalement ce qu'on est en train de faire", a insist� le ministre des Relations avec le Parlement.
Par ailleurs, la�croissance�a �t� de 0,3% au quatri�me trimestre 2013, un chiffre plus faible qu'attendu.
Dans sa derni�re note de conjoncture en d�cembre, l' Insee pr�voyait ainsi un taux de� croissance �du produit int�rieur brut (PIB) l�g�rement sup�rieur, de 0,4% par rapport au trimestre pr�c�dent.
Partager
