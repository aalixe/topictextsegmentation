TITRE: Le fonds GXP Capital s'associe � un �lu local pour reprendre Nice-Matin - 14/02/2014 - leParisien.fr
DATE: 2014-02-14
URL: http://www.leparisien.fr/nice-06000/le-fonds-gxp-capital-s-associe-a-un-elu-local-pour-reprendre-nice-matin-14-02-2014-3591547.php
PRINCIPAL: 0
TEXT:
Le fonds GXP Capital s'associe � un �lu local pour reprendre Nice-Matin
Publi� le 14.02.2014, 17h22
R�agir
Le fonds d' investissement "GXP Capital" s'associe � l'�lu local Jean Icart pour reprendre le groupe Nice-Matin, a annonc� vendredi dans un communiqu� Groupe Hersant M�dia (GHM), actuel propri�taire.
Le nom du fonds d'investissement a �t� d�voil� vendredi aux membres du comit� d'entreprise du groupe Nice-Matin. GXP Capital, dirig� par Gilles P�rin, est pr�sent dans l'h�tellerie haut de gamme et dans la construction navale.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
L'�lu DVD ni�ois Jean Icart avait annonc� le 6 f�vrier dernier son arriv�e au capital, associ� � un "fonds d'investissement priv� europ�en" dont il n'avait pas r�v�l� le nom.
Gilles P�rin a indiqu� vendredi avoir l'intention d'apporter avec Jean Icart un soutien financier de 20 millions d'euros via Nice Morning, nouvel actionnaire majoritaire du Groupe Nice-Matin, pr�cise le communiqu�. L'accord devrait �tre finalis� � la fin du mois.
Gilles P�rin et Jean Icart ont pr�cis� leur projet pour la relance de Nice-Matin qui passe par "une acc�l�ration des investissements sur le num�rique, un enrichissement des contenus �ditoriaux et la cr�ation d'un media center", pr�cise encore le communiqu� de GHM.
"L'arriv�e de ce nouvel investisseur va permettre de financer et mener � bien la relance des titres de Nice-Matin et d'assurer ainsi leur p�rennit�. Elle marque �galement l'ach�vement de la restructuration de GHM", a comment� Dominique Bernard, pr�sident de Nice-Matin.
Jean Icart, ex-UMP devenu opposant au d�put�-maire Christian Estrosi (UMP) au sein du conseil municipal et qui devait se pr�senter aux prochaines municipales, avait cr�� surprise et soulagement aupr�s des organisations syndicales. Il a expliqu� qu'il ne pouvait se r�soudre � "la perte d'un fleuron du patrimoine ni�ois" et pr�cis� qu'il abandonnait ses mandats politiques.
En grande difficult� financi�re, le groupe Nice-Matin, propri�t� de GHM, emploie 1.250 salari�s. Il a termin� l'ann�e 2013 avec une perte d'exploitation de 6 millions d'euros.
Le groupe publie les quotidiens Nice-Matin (qui tire � 90.000 exemplaires), Var-Matin (65.000 exemplaires) et Monaco-Matin. Le groupe d�tient aussi 50% du capital de la soci�t� Corse Presse, qui �dite Corse-Matin.
