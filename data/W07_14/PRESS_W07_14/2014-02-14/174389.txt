TITRE: Soupe � la grimace pour Pinturault - Jeux paralympiques Sotchi 2014
DATE: 2014-02-14
URL: http://www.francetvsport.fr/les-jeux-olympiques/soupe-a-la-grimace-pour-pinturault-206655
PRINCIPAL: 174388
TEXT:
Les jeux olympiques - Soupe � la grimace pour Pinturault et Mermillod-Blondin
Publi� le 14/02/2014 | 16:53, mis � jour le 14/02/2014 | 21:54
Alexis Pinturault (ALEXANDER KLEIN)
Le slalom du super combin� n�a pas souri aux Fran�ais. Adrien Th�aux a termin� 17e tandis que Thomas Mermillod-Blondin et Alexis Pinturault son tomb�s sur une neige qui ressemblait � de la soupe ou � la glace du poissonnier � la fin du march�. Retour sur la d�ception inattendue du camp tricolore.
Une m�t�o trop cl�mente pour �tre vraie un 14 f�vrier en Russie, et une strat�gie trop offensive, expliqueraient la d�faillance des sp�cialistes du combin�, les plus polyvalents skieurs fran�ais. Devant des tribunes pas compl�tement remplies, le favori Alexis Pinturault et son comp�re outsider Thomas Mermillod-Blondin, contraints de prendre des risques apr�s leur prestation du matin en descente, ont rat� le coche, laissant au m�connu suisse Sandro Viletta, auteur d�un superbe slalom, le soin de conclure victorieusement devant le Croate Ivica Kostelic et l�Italien Christof Innerhofer.
Bozzetto pressent le pi�ge
D�s l�entame du slalom, l�ancien leader du snowboard Mathieu Bozzetto, consultant pour France T�l�visions, pr�vient : � C�est de la neige humide qui accroche. Quand tu n�es pas �quilibr�, tu sors. Il faut absolument descendre son centre de gravit� afin d��tre monoblock. Cet �quilibre avant-arri�re donne une stabilit� indispensable car elle permet de doser, de mettre le frein � main quand il faut pour ne pas tomber �, explique-t-il alors sans savoir qu�il aura malheureusement raison �pour les Bleus- quelques minutes plus tard.
�
�
Le Slovaque Adam Zampa pointe en t�te quand Alexis Pinturault s��lance, en 8e position. Le Savoyard d�bute prudemment puis commence � trouver son rythme quand la faute survient. La strat�gie offensive n�a pas pay� : � C��tait difficile de faire de gros �carts �, confie-t-il dans l�aire d�arriv�e, press� de questions. � Je n��tais plut�t pas trop mal parti. J�avais repris un peu de temps. Est-ce que �a aurait suffi ? Je ne sais pas. Ma chute ? Ca fait partie des trucs qui peuvent arriver. Je tape le piquet avec la chaussure parce que je suis pr�cis donc tr�s pr�s du piquet. Sauf que, dans une double porte, �a nous colle les deux pieds et g�n�ralement on n�arrive pas � s�en d�patouiller. Je touche � la sortie de la double porte. C�est frustrant mais je suis conscient que �a reste du sport et qu�il faut mettre des choses en place pour que la m�daille arrive �.
Pinturault : "Pas de quoi s�affoler"
Calme et serein malgr� la d�ception, Pinturault r�pond sans se d�biner aux journalistes pr�sents en zone mixte : � Je ne suis pas en bas mais il n�y a pas de quoi s�affoler. Le ski �tait l� mais c��tait dur d��tre fluide sur un parcours trac� par le Kostelic. Rythme changeants, tr�s peu de vitesse, il y a plus ou moins d��cart entre les portes, c�est tournant : c�est tr�s particulier �, poursuit la meilleure chance de m�daille du ski alpin fran�ais. Il n�est pas le seul � le penser. � Ante Kostelic, le papa d�Ivica (2e ce vendredi), a trac� un slalom biscornu sp�cialement adapt� aux qualit�s de son fils, et les cadors ont eu du mal � r�ussir � ce trac� �, l�che ainsi S�bastien Amiez, vice-champion olympique de slalom en 2002 � Salt Lake City, qui en conna�t un rayon sur le sujet.
�
�
� Quand on voit le podium, on se dit que ce n�est jamais facile de briller m�me si on fait partie des meilleurs �, ajoute Alexis Pinturault. � Il va falloir que je skie plus lib�r�, engag�, et pour moi-m�me. C��tait ma premi�re course olympique et je me suis quand m�me fait plaisir sur la descente. Je sais faire avec la pression m�me s�il y en a davantage sur des JO �.
Mermillod lucide
Thomas Mermillod-Blondin (DR)
�
Son comp�re Thomas Mermillod-Blondin reconna�t davantage l��chec. � Une chute aux Jeux, c�est terrible �, avoue-t-il d�embl�e. � J�attendais ce 14 f�vrier depuis longtemps. Au d�part du slalom, je suis � 3 secondes du 1er donc il ne faut plus trop calculer mais tout envoyer. Je skie bien surtout apr�s le deuxi�me interm�diaire mais je ne traverse pas bien un mouvement de terrain. Je ramasse un peu de neige dans ma protection juste avant et tout explose contre les carreaux (sic). Il y a un dixi�me de seconde o� je ne vois plus clair et je me retrouve directement au tapis sachant qu�il n�y avait plus que deux portes � tailler �, regrette le Haut-Savoyard, lucide. � La neige �tait difficile mais c��tait la m�me pour tout le monde. Je suis pass� � c�t� mais j�ai encore une course dimanche (le Super-G) �.
Th�aux : "Encore quelques chances de m�dailles"
�
L'aire d'arriv�e du super combin� (DR)
�
Adrien Th�aux, troisi�me larron de la bande et le seul � l�arriv�e, compatissait sur le malheur de ses potes : � J��tais vert pour eux parce que je sais qu�ils fondaient beaucoup d�espoirs sur ce combin�. Il reste quelques chances de m�dailles, un Super-G dimanche pour moi notamment, un g�ant et un slalom. On va essayer de rebondir �. En quittant Rosa Khutor, nous avons crois� Jean-Luc Cr�tier. Le champion olympique de descente 1998 � Nagano r�sumait bien le sentiment g�n�ral. � La course a souri � des gens qui sont d�habitude beaucoup plus loin au classement. C�est rageant parce que c�est une discipline importante pour les Fran�ais �.
