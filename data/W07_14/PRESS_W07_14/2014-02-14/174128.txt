TITRE: Les d�parts en retraite anticip�e ont fortement augment� en 2013, Actualit�/Actu Quotidien
DATE: 2014-02-14
URL: http://argent.boursier.com/quotidien/actualites/forte-hausse-des-departs-en-retraite-anticipee-en-2013-1276.html
PRINCIPAL: 174125
TEXT:
Seniors Actu 0
Pr�s de 150.000 Fran�ais ont b�n�fici� d�une retraite anticip�e l�an pass�, contre 87.500 en 2012.Ce qui a fait reculer l��ge de d�part moyen � 62,1 ans
Reuters
Les Fran�ais partent d�j� plus tard � la retraite
Les chiffres sont spectaculaires. En 2013, 847.484 Fran�ais sont partis � la retraite, soit 13% de plus qu�en 2012. Mais ce qui marque surtout dans les derniers chiffres de la Cnav, c�est le recul de l��ge de d�part. Alors qu�il n�a cess� d�augmenter depuis la r�forme de 2010 jusqu�� atteindre 62,2 ans en 2012, il est pass� � 62,1 an en 2013.
Cela peut s�expliquer par la forte progression des d�parts anticip�s. La Cnav en a recens� 149.594 en 2013 alors qu�ils n��taient que 87.531 un an plus t�t. Ainsi en un an la proportion de d�parts anticip�s est pass�e de 14,4% � 17,6%.
C�est principalement du c�t� des d�parts anticip�s longue carri�re que se trouve la raison de cette hausse. Le gouvernement actuel a en effet mis en place un syst�me permettant aux personnes ayant commenc� � travailler jeunes de faire valoir leurs droits d�s 60 ans et non � l��ge l�gal de 62 ans. Ainsi, les d�part pour longue carri�re ont augment� de pr�s de 70% avec 147.208 personnes concern�es.
Notons enfin que les nouveaux retrait�s de 2013 ont b�n�fici� d�une pension moyenne de 643 euros au titre du r�gime de base (727 euros pour les hommes et 565 euros pour les femmes). Un chiffre qui concerne toutes les carri�res.
Pour les seules carri�res compl�tes, la pension moyenne s��l�ve � 1.072 euros. A titre de comparaison, les retrait�s partis en 2012 avait alors obtenu en moyenne 1.066 euros pour une carri�re pleine.
Commentaires (1)
jermauppost� le 22.02.2014 � 21:17
Compr�hensible, en France le travail est une source de torture, plus qu'ailleurs.
