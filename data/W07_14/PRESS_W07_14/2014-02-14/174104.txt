TITRE: Tempête Ulla : "Le sol n'a pas le temps d'absorber l'eau" - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-14
URL: http://tempsreel.nouvelobs.com/societe/20140214.OBS6410/tempete-ulla-le-sol-n-a-pas-le-temps-d-absorber-l-eau.html
PRINCIPAL: 0
TEXT:
"Les pr�cipitations les plus fortes sont attendues entre 13 heures et 17 heures cet apr�s-midi en Bretagne", explique Katia Conte, m�t�orologue.�
Un homme sur une route inondée, le 13 février dans le sud-ouest de Londres. (AFP)
Sept�d�partements - Les C�tes-d'Armor, le Finist�re, la Loire-Atlantique, la Manche le Morbihan, l'Ille-et-Vilaine et la Vend�e -�ont �t� plac�s en vigilance orange �vent et vagues-submersion par M�t�o France ce vendredi 14 f�vrier dans la matin�e.
Apr�s la Bretagne , c'est la Vend�e qui se retrouve noy�e par la temp�te Ulla , dont les vents devraient atteindre les 140km/h dans la journ�e. Contact�e par "le Nouvel Observateur", la pr�visionniste de M�t�o France , Katia Conte, a r�pondu � nos questions.�
Vous annonciez ce matin l'arriv�e de la temp�te Ulla sur les c�tes bretonnes, avec des vents pouvant atteindre les 140km/h dans l'apr�s-midi, d'o� viennent ces pr�cipitations temp�tueuses ?
- Une pression tr�s tr�s basse, de 958 hectopascal, s'est form� au dessus de l' Irlande .�Ulla est ensuite descendue sur les c�tes britanniques pour finalement arriver en France dans la matin�e, avec des vents de 90 km/h. Le coeur de cette d�pression a amen� de fortes pr�cipitations,�avec des rafales de vent. C'est une�temp�te est classique, due � l'�volution de la circulation atmosph�rique.
Pourquoi cette temp�te, que vous qualifiez de "classique", est-elle particuli�rement ravageuse ?�
- Parce que les pr�cipitations qu'elle engendre s�vissent par vague. Les pluies sont r�p�t�es et incessantes. Le sol n'a pas le temps d'absorber l'eau. On comprend bien les inqui�tudes des habitants de la r�gion, qui voient des trombes d'eau tomber et le sol s'enfoncer.�
Quel sera la suite du parcours de la temp�te ?�
- Les pr�cipitations les plus fortes sont attendues entre 13 heures et 17 heures cet apr�s-midi en Bretagne. Doucement, vers 1 heure du matin, les vents vont s'amoindrir pour atteindre les 90 km/h et la temp�te �voluera vers le nord, pour finalement dispara�tre. Le week-end devrait donc �tre plut�t agr�able et les Fran�ais retrouveront les temp�ratures douces qu'ils observent depuis le d�but de l'hiver. Une nouvelle perturbation temp�tueuse est pr�vue pour lundi prochain, le 17 f�vrier. Le soleil reviendra sur la Bretagne le 18.
Barbara Krief - Le Nouvel Observateur
Partager
