TITRE: Avec BBM 2.0, Blackberry crée son réseau social
DATE: 2014-02-14
URL: http://www.numerama.com/magazine/28426-avec-bbm-20-blackberry-cree-son-reseau-social.html
PRINCIPAL: 173591
TEXT:
Publi� par Guillaume Champeau, le Vendredi 14 F�vrier 2014
Avec BBM 2.0, Blackberry cr�e son r�seau social
Blackberry a lanc� la nouvelle version de sa messagerie BBM, qui devient un v�ritable r�seau social dont les fonctionnalit�s d�j� nombreuses ne demandent qu'� s'�tendre.
1
Misant d�sormais davantage sur le logiciel que le mat�riel pour regagner des parts de march�, BlackBerry veut relancer le succ�s de sa messagerie BBM, avec le lancement d'une version 2.0 qui int�gre d�sormais les appels vocaux avec BBM Voice, mais aussi toute une s�rie de fonctionnalit�s qui rapprochent le service d'un v�ritable r�seau social � la Facebook.
Disponible sur les t�l�phones Blackberry , le nouveau BBM est aussi propos� sur Android et sur iOS , avec une s�rie de fonctionnalit�s qui pourraient convaincre ceux qui cherchent une alternative int�ressante � Skype, mais �galement les jeunes qui utilisent davantage WhatsApp, Facebook ou Google Hangouts.
BBM 2.0 permet notamment de :
cr�er un profil et mettre � jour son statut ;
"aimer" les messages publi�s par ses contacts ;
savoir quand un message a �t� lu par le destinataire (une fonctionnalit� qui dispara�t progressivement des SMS) ;
partager des photos, vid�os, documents, agendas, ou autres messages vocaux avec ses interlocuteurs ;
autoriser des contacts � voir sa localisation g�ographique pr�cise sur une carte ;
rejoindre un groupe de discussion en cours de session ;
envoyer le m�me message � plusieurs contacts en m�me temps.
cr�er des "canaux th�matiques" (BBM Channels) pour discuter d'un sujet particulier avec d'autres utilisateurs, dans un mode plus ais� qu'avec les hashtags de Twitter.
Fid�le � son esprit originel, BBM 2.0 n'impose pas d'identifiant nominatif. Les utilisateurs sont identifi�s par un num�ro unique (PIN), � l'instar d'un num�ro de t�l�phone. Seuls les contacts ajout�s par les deux interlocuteurs peuvent s'appeler ou s'envoyer des messages.
