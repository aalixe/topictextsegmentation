TITRE: Quarante ans dans l�objectif de Cartier-Bresson
DATE: 2014-02-14
URL: http://evene.lefigaro.fr/arts/actualite/quarante-ans-dans-l-objectif-de-cartier-bresson-2577807.php
PRINCIPAL: 173195
TEXT:
Quarante ans dans l�objectif de Cartier-Bresson
Par Pauline Le Gall - Le 12/02/2014
Vous aussi �crivez votre commentaire ou votre critique
R�agissez
0 avis
Membres (0) �
Le centre Pompidou propose une exposition chronologique qui pr�sente plus de 400 �uvres du photographe. � d�couvrir jusqu�au 9 juin.
�
C�est la premi�re fois en France qu�un mus�e propose une exposition chronologique et bas�e sur des tirages originaux de l��uvre d�Henri Cartier-Bresson. L�id�e, pour le commissaire de l�exposition Cl�ment Ch�roux, est de montrer le travail du photographe dans toute sa diversit� et sa complexit�. �Le meilleur service que l�on pouvait rendre � la richesse de son �uvre, explique-t-il, c�est de retranscrire la chronologie de son travail et d�oublier l�approche th�matique habituellement privil�gi�e.� Ce n�est pas par pays, par genre et par style de photographies que l�on d�couvre Cartier-Bresson, mais par l�image. Logique, puisque c�est ce qui comptait pour l�artiste, cet instant d�cisif, ce mouvement, ce moment captur� pour toujours sur la pellicule. �
Henri Cartier-Bresson, Martine Franck, Paris, France, 1967, � Henri Cartier-Bresson / Magnum Photos, courtesy Fondation Henri Cartier-Bresson.
L�image surr�aliste
Pour Cl�ment Ch�roux, les deux mentors de Cartier-Bresson portent le m�me pr�nom: Andr�. Lhote et Breton vont �tre essentiels dans le d�veloppement du jeune photographe. Chez Lhote, dans son atelier, il apprend la peinture, ses formes et ses sujets. Dans les �crits de Breton , il pioche des concepts. Influenc� par le surr�alisme, il multiplie les jeux visuels au d�but de sa carri�re. Il fige des objets inconnus, drap�s sous des b�ches et qui se d�robent pour toujours au regard du spectateur. Il capture des associations de sens (un homme dont la t�te est cach�e par un �norme n�ud), et exploite le th�me des r�ves avec des visages photographi�s les yeux clos� Avec son Leica, il capture des ombres, dans des cadres toujours extr�mement travaill�s, o� chaque ligne de fuite est �tudi�e.
Les voyages
D�s sa majorit�, Cartier-Bresson abandonne la vie qui lui �tait trac�e, celle de successeur � la t�te de l�industrie familiale, pour partir en Afrique. C�est � cette �poque que les premiers visages remplacent les ombres sur ses photographies. En 1931, Cartier-Bresson entame le d�but d�une vie d�vou�e aux voyages. Ce qui l�int�resse quand il visite un pays, ce n�est pas le folklore ou les coutumes, mais les corps et les visages de ceux qui y vivent et travaillent.
De l�Afrique, on d�couvre les marins qui l�vent des filets et habitent les rues. Quand, en 1937, il est envoy� par le quotidien du Parti communiste Ce soir photographier le couronnement en Angleterre de George VI, c�est la foule qu�il capture. Les visages de ceux qui l�vent les yeux vers le haut, vers le pouvoir. Sa sublime s�rie de 1936 retrace, quant � elle, les premi�res vacances des fran�ais. Les images, infus�e de tendresse, montrent des badauds au bord du lac, en contre-plong�e. �
Henri Cartier-Bresson, Martine Franck, Paris, France, 1967, � Henri Cartier-Bresson / Magnum Photos, courtesy Fondation Henri Cartier-Bresson.
L�engagement politique
L�exposition, dans son aspect chronologique, permet de voir na�tre chez Cartier-Bresson le regard profond�ment politique. Il se passionne, au long de sa carri�re pour les combats r�volutionnaires et pour le communisme. Il r�alise un premier film sur la guerre d�Espagne, tentant de percer avec un cin�ma engag�. En 1947, il fonde avec Robert Capa, George Rodger, William Vandivert et David Seymour l�agence Magnum. Ils se partagent le monde et Cartier-Bresson arpente l�Asie, ramenant des clich�s des fun�railles de Gandhi, qu�il a photographi� juste avant son assassinat.
Il multiplie les reportages, ne sacrifiant jamais l�exigence et le sens artistique qui l�habitent. En parall�le, il r�alise des portraits d�artistes, jouant toujours avec la composition et l�arri�re-plan. Truman Capote en Louisiane, Sartre sur le Pont des Arts, Matisse et ses oiseaux� Ces quelques parenth�ses entourent des images politiques qui questionnent en permanence l�American Way of Life, le rapport entre l�homme et la machine et la soci�t� de consommation. Il photographie une vieille femme avec un drapeau am�ricain en �charpe, des enfants fascin�s par des petites voitures dans la vitrine des Galeries Lafayette, un couple convoitant une nouvelle voiture�
La retraite
Tous ces clich�s semblent mener, inexorablement, � la fin de sa carri�re dans les ann�es 70. M�content du devenir de Magnum, il quitte l�agence et arr�te presque enti�rement son activit� de photographe. Dans les ann�es 70, il retourne � ses premi�res amours et se consacre � ses dessins. Passionn� par le bouddhisme, il r�alise aussi quelques photographies de paysages, o� les �tres humains sont d�sormais absents. Il a, en quarante ans de curiosit�, et sans jamais rien figer, fait le tour complet de l�humanit�.
��Quarante ans dans l�objectif de Cartier-Bresson��
1 personne a d�j� ajout� cet article � ses favoris
