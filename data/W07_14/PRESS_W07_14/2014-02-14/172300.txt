TITRE: SOTCHI | Alexis Pinturault attendu au tournant - France Info
DATE: 2014-02-14
URL: http://www.franceinfo.fr/autres-sports/sotchi-alexis-pinturault-attendu-au-tournant-1317109-2014-02-13
PRINCIPAL: 172272
TEXT:
Imprimer
Alexis Pinturault est l'un des favoris de l'�preuve de super-combin� � Reuters - Mike Segar
Alexis Pinturault d�bute ses JO ce vendredi. Le Fran�ais participe au super-combin� (descente et slalom). Ils sont quatre en tout � viser une m�daille dans cette �preuve. Mais les cartes pourraient �tre rebattues en raison de la m�t�o. La descente a d'ailleurs �t� avanc�e d'une heure � cause de la douceur.
Ils sont quatre pour trois marches sur le podium. Quatre, surtout, pour une m�daille d'or qu'ils visent tous. Alexis Pinturault, Ted Ligety, Bode Miller et Ivica Kostelic se d�tachent nettement pour le titre olympique dans cette �preuve du super-combin�.
Et chacun a des arguments � faire valoir. Alexis Pinturault a �t� des quatre podiums de Coupe du monde ces deux derni�res saisons et il arrive � Sotchi aur�ol� d'une victoire � Kitzb�hel. Vex� par sa huiti�me place en descente, Bode Miller a soif de revanche. Ted Ligety lui se sent dans une forme exceptionnelle "comme aux Mondiaux de Schladming" il y a un an dit-il o� il avait tout rafl�. Enfin, excellent slalomeur, le Croate Ivica Kostelic compte...sur son p�re. C'est en effet Ante Kostelic qui a dessin� le parcours du slalom.
Un parcours qui pourrait aussi profiter au Fran�ais, meilleur slalomeur du lot. Alexis Pinturault n'a en revanche r�ussi a trouver ses marques lors des entra�nements sur la piste.
La m�t�o trop cl�mente
Mais tout pourrait �tre rebattu par la m�t�o (trop) cl�mente. Pour pr�server la piste, les organisateurs ont d'ailleurs d�cid� d'avancer le d�part de la descente d'une heure. Les skieurs s'�lanceront � 7h au lieu de 8h (heure fran�aise). Pas de quoi calmer les inqui�tudes de Bode Miller : "Si la neige avait �t� glac�e, les descendeurs pouvaient creuser des �carts. Ce ne sera pas possible dans ces conditions, surtout en partant derri�re", explique l'Am�ricain, excellent descendeur mais beaucoup moins bon en slalom.
D�s lors, d'autres champions pourraient tenter de tirer leur �pingle du jeu. Le Norv�gien Aksel Lund Svindal, le Fran�ais Thomas Mermillod Blondin sont deux autres pr�tendants au podium. Dans la liste des possibles, on peut ajouter l'Italien Peter Fill, l'Autrichien Romed Baumann, Natko Zrncic-Dim, autre Croate, et le jeune Norv�gien Aleksander Aamodt Kilde.
� suivre �galement
Il faudra avoir un �il sur la patinoire olympique ou les deux Fran�ais, Brian Joubert et Florent Amodio vont d�voiler leur programme libre. Il y a aura aussi du biathlon f�minin en 15km individuel avec trois Fran�aises. Enfin, Ronan Lamy-Chappuis s'�lancera sur le grand tremplin pour les qualifications.�
