TITRE: Edward Snowden, comme un roman | Le Cercle Les Echos
DATE: 2014-02-14
URL: http://lecercle.lesechos.fr/cercle/livres/critiques/221191219/edward-snowden-comme-roman
PRINCIPAL: 0
TEXT:
14/02/2014 | Les Echos | Critiques | Lu 1573 fois | aucun commentaire
Edward Snowden, comme un roman
LE CERCLE. Chronique du livre d'Antoine Lef�bure � L�Affaire Snowden. Comment les Etats-Unis espionnent le monde � (La D�couverte, 276 pages, 19 euros).
�crit par
Tous ses articles
Depuis le 5 juin 2013, jour o� le quotidien britannique � The Guardian � a publi� le premier article sur les �coutes de la NSA, le nom d�Edward Snowden a fait le tour du monde. Les r�v�lations du jeune informaticien ont montr� l��tendue de la surveillance men�e en secret par les Etats-Unis.
Le livre d�Antoine Lef�bure relate, avec beaucoup de p�dagogie, les dessous et les enjeux de ce scandale plan�taire. La premi�re partie, qui raconte le parcours de Snowden et la fa�on dont il a contact� les jour�nalistes, est digne d�un roman d�espionnage. Mais l�ouvrage a le m�rite d�aller beaucoup plus loin, en retra�ant l�histoire de la NSA et du programme PRISM, qui trouve son origine dans le syst�me d�espionnage Echelon des ann�es 1990 et dans le traumatisme post-11 Septembre.
S�il d�taille avec beaucoup de p�dagogie les techniques de surveillance am�ricaines, l�auteur n�oublie pas d��voquer le r�le ambigu des services secrets britanniques et fran�ais.
� lire �galement
