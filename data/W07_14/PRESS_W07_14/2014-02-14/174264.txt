TITRE: Un clich� de migrants africains �lu photo de presse de l'ann�e - rts.ch - info - monde
DATE: 2014-02-14
URL: http://www.rts.ch/info/monde/5614417-un-cliche-de-migrants-africains-elu-photo-de-presse-de-l-annee.html
PRINCIPAL: 174262
TEXT:
Un clich� de migrants africains �lu photo de presse de l'ann�e
14.02.2014 18:11
Un clich� du photographe am�ricain John Stanmeyer, repr�sentant des migrants africains cherchant du r�seau � Djibouti, a �t� sacr� vendredi photo de presse de l'ann�e au World Press Photo 2014.
Un clich� du photographe am�ricain de l'agence VII Photo John Stanmeyer a re�u le titre de photo de l'ann�e lors de la proclamation des r�sultats du World Press Photo 2014 , vendredi � Amsterdam.
L'image, prise pour le National Geographic et illumin�e seulement au clair de lune, montre des migrants africains sur les c�tes de Djibouti City, levant leur t�l�phone portable dans l'espoir de capter le signal mobile de la Somalie voisine. Djibouti est un point d'arr�t fr�quent pour les migrants en transit depuis la Somalie ou l'Erythr�e vers les pays d'Europe ou du Moyen-Orient.
Exposition itin�rante � travers le monde
Le World Press Photo r�compense depuis 55 ans les meilleures productions de journalisme visuel. Choisies par un jury de professionnels renouvel� chaque ann�e, les meilleures images sont ensuite pr�sent�es lors d'une exposition itin�rante d'une ann�e, qui traverse plus de 100 villes et 45 pays. Elle d�butera � Amsterdam le 18 avril.
jvia
Swiss Press Photo en mars
Les r�sultats du concours suisse de la photo de presse seront rendus publics le 2 mars.
En 2013, il avait prim� le travail de Laurent Gilli�ron, photographe de l'agence Keystone, pour ses clich�s des suites de l'accident de car qui avait co�t� la vie � 28 personnes dont 22 enfants � Sierre (VS) en mars 2012.
archives
