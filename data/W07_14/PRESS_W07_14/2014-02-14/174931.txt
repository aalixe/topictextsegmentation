TITRE: Pourquoi seulement certaines personnes se rappellent de leurs rêves ?, Médecine & Santé
DATE: 2014-02-14
URL: http://www.lesechos.fr/entreprises-secteurs/innovation-competences/medecine-sante/0203318596089-pourquoi-seulement-certaines-personnes-se-rappellent-de-leurs-reves-650719.php
PRINCIPAL: 0
TEXT:
Par Yann Verdo | 14/02 | 18:59
Les � grands r�veurs � sont davantage sujets aux micro-r�veils, seuls moments o� le cerveau est capable de m�moriser un r�ve.
Les travaux de neuroscientifiques de l�Inserm publi�s dans la revue � Neuropharmacology � �clairent une �nigme. - AFP
C�est un fait de nature : les hommes ne naissent pas tous �gaux� jusque dans leur capacit� � se souvenir � ou pas � de leurs r�ves. Pourquoi certains d�entre nous se les rappellent-ils tous les matins ou presque, alors que d�autres n�ont cette chance (ou malchance) qu�une ou deux fois par mois ?
Les travaux de neuroscientifiques de l�Inserm publi�s dans la revue � Neuropharmacology � �clairent cette �nigme. Perrine Ruby et son �quipe ont mesur� avec un scanner TEP l�activit� c�r�brale d�une quarantaine de � grands � et � petits � r�veurs. Ces analyses ont montr� que les grands r�veurs pr�sentent une activit� c�r�brale spontan�e plus forte pendant leur sommeil au niveau du�cortex pr�frontal m�dian mais aussi de la jonction temporo-pari�tale, zone du cerveau impliqu�e dans l�orientation de l�attention vers les stimuli ext�rieurs.
Traduction : les grands r�veurs r�agissent plus aux stimuli de l�environnement et ont un sommeil davantage entrecoup� de micro-r�veils que les petits. Ce�qui explique pourquoi ils m�morisent mieux leurs r�ves. � En effet, explique Perrine Ruby, le cerveau endormi n�est pas capable de m�moriser une nouvelle information, il a besoin de se r�veiller pour le faire. �
