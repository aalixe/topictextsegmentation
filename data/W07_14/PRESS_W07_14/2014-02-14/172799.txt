TITRE: www.lamontagne.fr - A la Une - CLERMONT-FERRAND (63000) - F�te de la Saint-Valentin : un cauchemar pour les c�libataires ?
DATE: 2014-02-14
URL: http://www.lamontagne.fr/auvergne/actualite/2014/02/14/fete-de-la-saint-valentin-un-cauchemar-pour-les-celibataires_1873324.html
PRINCIPAL: 172797
TEXT:
(cliquez sur le code s'il n'est pas lisible)
Tous les articles
Red�coupage des r�gions au 1er janvier 2017: Faites votre choix !
Lu 312 fois
Lors de son discours de politique g�n�rale � l'Assembl�e nationale, mardi 8 avril 2014, le premier ministre Manuel Valls a annonc� vouloir r�duire de moiti� le nombre de r�gions fran�aises d'ici au 1er janvier 2017. Comment regrouperiez-vous les d�partements du centre de la France...
Ces Auvergnats chanceux qui ont gagn� de gros lots � la Fran�aise des jeux
Lu 1020 fois
Outre le million d'euros remport� au Loto samedi dernier, 5 avril, par un Puyd�mois, le dernier montant cons�quent gagn� en Auvergne, a �t� pay� le 25�f�vrier dernier, suite au tirage du nouveau jeu de la Fran�aise des Jeux, l�Euro Millions-My Million, du 11�f�vrier. C�est...
TER et Intercit�s : Un plan de d�veloppement pour 2020
Lu 2159 fois 2
Nouveau d�l�gu� g�n�ral SNCF aux r�gions et aux trains Intercit�s, Alain Le Vern a d�voil� un plan 2020 destin� � r�duire les d�s�quilibres existants.
N� � Issoire le 6 mars 1911, Jean Roussel vit dans la r�sidence clermontoise Les Opalines
Lu 120 fois
Jean Roussel a f�t� ses 103 ans, le 6 mars dernier, dans la r�sidence Les Opalines de Clermont-Ferrand. L�occasion d��voquer avec lui ses souvenirs mais aussi son quotidien de centenaire.
Apr�s touslesprix.com, la soci�t� cournonnaise de Jean-Christophe Janicot s�internationalise
Lu 175 fois
Treize ans apr�s avoir cr�� touslesprix.com, l�un des premiers comparateurs de prix en ligne, Jean-Christophe Janicot et sa soci�t� cournonnaise lancent eanfind.fr. Pour continuer � innover et cibler une nouvelle client�le, europ�enne.
Concours de po�sie aux Opalines
Lu 1 fois �
Q8-2V avec photo 2 col : Concours de po�sie aux Opalines.  Centre-ville. S'inscrivant dans la dynamique du Printemps des Po�tes, la r�sidence Les Opalines a organis� un concours de po�sie inter�tablissements. Les repr�sentants de sept EHPAD (*) se sont ainsi...
Le MARQ en a offert pour tous les go�ts et tous les �ges
Lu 5 fois �
Un dimanche vraiment exceptionnel  Ce dimanche il y avait foule au MARQ. Chacun pouvait trouver une animation � sa convenance, qu'il soit jeune ou vieux, amateur de musique ou de danse, de peinture ou de meuble ancien, de vid�os, de photos.  Et les enfants...
Des tests physiques et un d�pistage du diab�te ont eu lieu
Lu 13 fois �
Les associations L�Ouverture et Diabet 63 ont organis� des tests physiques et un d�pistage du diab�te, derni�rement, au centre d�animation de La Gauthi�re.
Deux mois avec sursis et 10.000 � � rembourser pour un quinquag�naire
Lu 123 fois �
Un Puyd�mois a �t� condamn� lundi dernier pour abus de confiance. Pendant pr�s de trois ans, il a vol� des milliers d�euros dans les distributeurs de boissons de son entreprise � Ambert. Fort de caf�
D�couverte du Photogramme � l�H�tel Fontfreyde
�
Q8-2V avec photo 2 col : D�couverte du Photogramme � l'H�tel Fontfreyde.  Rue des gras. Une dizaine de personnes a particip� � l'atelier � D�couverte du photogramme �, propos� par l'H�tel Fontfreyde dans le cadre de la manifestation Culture dans...
En pr�lude au 6e festival Terre de Bulles � Langeac
Lu 1 fois �
La bande dessin�e entre en gare Trois dessinateurs invit�s du prochain festival de Langeac se sont pr�t�s � une s�ance de d�dicace en gare de Clermont-Ferrand. Une mani�re originale d'�changer avec le public et de faire conna�tre cette manifestation qui revendique sa petite...
Est-ce que votre d�put�(e) a vot�
Lu 46 fois �
Les d�put�(e)s �taient amen�(e)s, hier, � voter, ou non, la confiance au nouveau Premier ministre Manuel Valls, � l'issue de sa d�claration de politique g�n�rale ( lire en pages France).  Sur les cinq d�put�(e)s du Puy-de-D�me, les socialistes...
