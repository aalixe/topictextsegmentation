TITRE: ��Intime conviction�� sur Arte : un proc�s d�assises vu de l�int�rieur | La-Croix.com
DATE: 2014-02-14
URL: http://www.la-croix.com/Culture/Actualite/Intime-conviction-sur-Arte-un-proces-d-assises-vu-de-l-interieur-2014-02-14-1106501
PRINCIPAL: 173975
TEXT:
Les greffiers r�clament une am�lioration de leur statut
Le 9�mars 2012, � Bayonne, Paul Villers, m�decin l�giste appelle la police : il vient de retrouver son �pouse sans vie, une balle dans la t�te. La police conclut d�abord � un suicide, avant qu�une enqu�trice, convaincue de la culpabilit� du mari, reprenne le dossier. Au terme d�une instruction � charge, sans la moindre preuve, le procureur envoie le docteur Villers en cour d�assises.
R�sum�e ainsi, l�intrigue d�Intime conviction�semble rab�ch�e. Rien de tr�s original dans sa trame. Sauf que tout est dans ��le dispositif�� invent� par Arte et le producteur Denis Poncet. Apr�s la diffusion du t�l�film (assez caricatural et sans grand relief, malgr� l�interpr�tation tendue de Philippe Torreton et de Camille Japy), la cha�ne entra�ne les spectateurs sur le Web �pour assister � une premi�re : le d�roulement du proc�s, vu de l�int�rieur, dans toutes ses phases.
Sur le Net, le proc�s au jour le jour
Les internautes pourront consulter le dossier d�instruction, les rapports d�expertise et d�analyse balistique, suivre, au jour le jour, les d�positions, la d�fense, le ballet des t�moins, les escarmouches contradictoires� Ils pourront aussi acc�der au profil de chaque personnage, revoir le t�l�film qui sert de base � l�accusation, en totalit� ou par bouts.�
Sur le Net, les audiences s��taleront sur trois semaines, comme un feuilleton, avec diffusion progressive des 35 modules, � raison d�un � trois par jour.
Chaque internaute pourra faire conna�tre son ��intime conviction��
Jusqu�au 2�mars, le suspense sera aussi intense que pour les acteurs soumis aux questions de vrais magistrats (pr�sident de cour d�assises, avocat g�n�ral, avocats des parties civiles et de la d�fense, plus les neuf jur�s tir�s au sort). L�issue demeurera incertaine jusqu�au bout, jusqu�� la diffusion du long d�lib�r� qui d�cidera de la culpabilit� ou non de Paul Villers. De vrais journalistes commenteront le proc�s et chaque internaute pourra faire conna�tre son ��intime conviction��.
Ambitieux projet bim�dia (l�enqu�te sur Arte, le proc�s sur Internet), Intime conviction�vise � faire mieux conna�tre le fonctionnement de la justice. Film� sans interruption, sous l�objectif de six cam�ras (que les internautes pourront isoler pour se fixer sur tel ou tel protagoniste), Philippe Torreton dit que ce tournage fut une �preuve, et lui qui d�teste l�improvisation a d� jouer ��un personnage de fiction plong� dans un bain de r�alit頻.�
���Dans ce d�corum impressionnant, m�me si on n�a rien fait, on se sent coupable���
Camille Japy, dans le r�le de l�inspectrice de police, acharn�e � faire condamner le suspect, avoue que, lorsqu�on entre, pour la premi�re fois, ��dans ce d�corum impressionnant, m�me si on n�a rien fait, on se sent coupable��.�
De l�avis de tous ceux qui ont particip� � ce vrai-faux proc�s, ��ce fut un combat dont personne ne savait comment il se terminerait��. ��C��tait stressant, �puisant. Je devenais parano�aque,�explique Philippe Torreton. La veille du verdict, j�errais dans la ville avec la trouille de passer le reste de mes jours en prison����
Jean-Claude Raspiengeas
Sur Arte, vendredi 14 f�vrier � 20�h�50. Et du 10�f�vrier au 2�mars, le proc�s sur arte.tv �
