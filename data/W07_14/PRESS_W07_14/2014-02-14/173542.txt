TITRE: Croissance de 0,3%, un peu meilleure qu'attendu, sur l'ensemble de 2013 - 20minutes.fr
DATE: 2014-02-14
URL: http://www.20minutes.fr/economie/1299022-20140214-croissance-03-4e-trimestre-2013-inferieure-a-prevision-04
PRINCIPAL: 173539
TEXT:
Un poids lourd quitte une usine fran�aise FRANK PERRY / AFP
ECONOMIE - Selon les estimations de l�Insee...
La croissance �conomique en France a �t� de 0,3% en 2013, un taux un peu meilleur que pr�vu, a annonc� ce vendredi l'Institut national de la statistique et des �tudes �conomiques ( Insee ).
L'Insee pr�voyait en d�cembre une croissance moyenne du produit int�rieur brut pour 2013 de 0,2%, mais elle a depuis revu en hausse l'�volution du PIB aux premier et troisi�me trimestres, d'une contraction de 0,1% � une stagnation.
Le gouvernement pr�voyait quant � lui une croissance d'ensemble de 0,1%.�R�agissant sur France 2, le ministre de l'Economie, Pierre Moscovici, a estim� que le chiffre finalement arr�t� par l'Insee, m�me meilleur que pr�vu, n�cessitait d'�aller plus loin� pour �faire reculer le ch�mage�.
Au quatri�me trimestre, la croissance a �t� de 0,3%, selon la premi�re estimation de l'Insee qui, dans sa derni�re note de conjoncture en d�cembre, pr�voyait un taux de progression du PIB l�g�rement sup�rieur, de 0,4% par rapport au trimestre pr�c�dent.
�Il faut aller plus loin�
La consommation des m�nages a rebondi en 2013, progressant de 0,4% apr�s une baisse du m�me taux l'ann�e pr�c�dente, a annonc� l'Insee. Quant � l'investissement, moteur attendu de la reprise, il est reparti � la hausse au quatri�me trimestre (+0,6%) apr�s sept trimestres de recul.
La correction des premier et troisi�me trimestres �est marginale, cela joue sur l'arrondi�, explique-t-on � l'Insee. �C'est en quelque sorte une r�vision des normales saisonni�res� statistiques.
�Il y a vraiment une conjugaison de trois facteurs: l'investissement � la hausse apr�s deux ans de recul, la consommation robuste en fin d'ann�e et les exportations qui ont rebondi au quatri�me trimestre�, a indiqu� l'Institut � l'AFP.
�
�Je ne me satisfais pas, je dis: "faisons plus"�, a d�clar� de son c�t� Pierre Moscovici. �Il faut aller plus loin pour avoir des cr�ations d'emploi, pour faire reculer le ch�mage�, a-t-il insist� sur France 2.
Avec AFP
