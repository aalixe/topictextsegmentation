TITRE: VIDÉO : Pluie de cendres en Indonésie
DATE: 2014-02-14
URL: http://www.lepoint.fr/video/video-pluie-de-cendres-en-indonesie-14-02-2014-1791703_738.php
PRINCIPAL: 173310
TEXT:
14/02/14 à 12h02
VIDÉO : Pluie de cendres en Indonésie
Le mont Kelud s'est réveillé : l'un des 130 volcans actifs d'Indonésie a expulsé vendredi des millions de mètres carrés de cendres dans l'atmosphère.
