TITRE: Live Salon Gen�ve 2014 : Peugeot 108 - les news du Salon de Gen�ve avec Turbo.fr
DATE: 2014-02-14
URL: http://www.turbo.fr/actualite-automobile/618960-salon-geneve-2014-peugeot-108/
PRINCIPAL: 0
TEXT:
Live Salon Gen�ve 2014 : Peugeot 108
Publi�  le 05 mars 2014 par Richard Burgan
1 commentaire
� R�daction Turbo.fr�
Rempla�ante de la 107, la Peugeot 108 insiste sur les possibilit�s de personnalisation et sera propos�e � travers une version d�couvrable baptis�e 108 Top. La mini-citadine au Lion doit �tre expos�e lors du Salon de Gen�ve en mars.
Le d�but d'ann�e 2014 annonce un affrontement entre mini-citadines fran�aises. Parmi elles, les nouvelles Peugeot 108 et Renault Twingo 3.
La prot�g�e au Lion a d'ailleurs �t� aper�ue hier aux cot�s de sa future rivale au Losange via la couverture vol�e du journal allemand AutoBild. Peugeot la d�voile � pr�sent sous toutes ses coutures.
Suivant la tendance initi�e par l'Opel Adam, la 108 soigne son look en mettant l'accent sur les possibilit�s de personnalisation. Cela comprend notamment le nombre de portes, trois ou cinq, la teinte parmi huit choix dont deux in�dits, le cuivre dor� A�kinite et le violet in�dit Red Purple, et l'ambiance int�rieure.
CLIQUEZ ICI POUR VOIR NOTRE DIAPORAMA LIVE GENEVE PEUGEOT 108
Citro�n C1, Peugeot 108 et Toyota Aygo en vid�o live au Salon de Gen�ve 2014
A l'instar des r�centes 208 et 308, la mini-citadine au Lion affiche une allure dynamique � partir de ses feux ac�r�s au sein d'une face avant robuste. Le volume de coffre de 196 litres peut �tre port� � 750 litres en rabattant les dossiers 50/50 de la banquette arri�re.
Cette rempla�ante de la 107 sera �galement disponible � travers une version d�couvrable baptis�e 108 Top. La commande �lectrique du toit toile permet de d�couvrir l'espace � volont� sur 80 cm de large et 76 cm de long en ajutant l'ouverture en n'importe quelle position.
La gamme est constitu�e de quatre groupes motopropulseurs trois-cylindres : les 1.0 e-VTi 68 BVM5 (88g/km de CO2), 1.0 VTi  68 BVM5 (95g/km), 1.0 VTi 68 ETG5 (97g/km) et le nouveau bloc PureTech 1.2 VTi 82 BVM5 (99g/km). Parmi les �quipements propos�s, on d�nombre notamment la cam�ra de recul, l'Acc�s D�marrage Mains Libres et le Mirror Link pour la connectivit� du smartphone au v�hicule via un �cran de 7 pouces.
Apr�s les nouvelles 208 et 308, Peugeot aura renouvel� les �l�ments forts de sa gamme en l'espace de deux ans. La 108 sera expos�e sur le stand de la firme au Lion � l'occasion du prochain Salon de Gen�ve en mars.
SUR LE M�ME TH�ME :
