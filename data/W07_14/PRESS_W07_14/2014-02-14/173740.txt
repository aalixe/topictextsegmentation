TITRE: Zone euro: du mieux au sud au quatri�me trimestre !
DATE: 2014-02-14
URL: http://www.boursier.com/actualites/economie/croissance-zone-euro-du-mieux-dans-la-region-sud-au-quatrieme-trimestre-23022.html?sitemap
PRINCIPAL: 173737
TEXT:
Cr�dit photo ��Reuters
(Boursier.com) � La croissance a acc�l�r� en zone euro � la fin de l'ann�e 2013. Au quatri�me trimestre, le PIB a cr� de +0,3% apr�s +0,1% au cours des trois mois pr�c�dents, d'apr�s les donn�es publi�es vendredi par Eurostat. Cette performance est toutefois inf�rieure � celle enregistr�e au quatri�me trimestre 2012 (+0,5%), et sur l'ensemble de l'ann�e, le PIB du Vieux continent recule de -0,4%. Pour 2014, le Fonds mon�taire qui a mis � jour ses pr�visions en janvier dernier, vise une croissance de +1%.
Dans le d�tail, la France et l'Allemagne ont enregistr� des performances sup�rieures aux attentes au quatri�me trimestre, avec un PIB en hausse de respectivement +0,3% et +0,4%. Sur l'ensemble de l'ann�e, ils enregistrent une expansion de +0,3% et +0,4% �galement.
Du mieux en Europe du Sud !
Du c�t� de l'Europe du Sud, l'Italie a enfin renou� avec la croissance (+0,1%) au quatri�me trimestre, apr�s �tre rest� stable aux cours des trois mois pr�c�dents. L'Espagne sort �galement doucement de la crise. Pour la premi�re fois depuis 2011, le pays a encha�n� deux trimestres cons�cutifs de croissance � la fin de l'ann�e derni�re (+0,3% apr�s +0,1%), m�me si sur un an le PIB se contracte de 1,1%. Au Portugal, la croissance s'est aussi nettement acc�l�r�e: +0,5% apr�s +0,3% au troisi�me trimestre.
En revanche, les donn�es relatives � la Gr�ce ne sont pas disponibles. Enfin, les difficult�s demeurent pour Chypre, dont le PIB a recul� de 1% sur les trois derniers mois de l'ann�e.
Marianne Davril � �2014, Boursier.com
