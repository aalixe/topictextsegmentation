TITRE: Jean Dujardin, George Clooney, des b�b�s, Jeremy Abbott... Retrouvez votre zapping vid�o � metronews
DATE: 2014-02-14
URL: http://www.metronews.fr/video/jean-dujardin-george-clooney-des-bebes-jeremy-abbott-retrouvez-votre-zapping-video/mnbn!lVhg9N0G2Tvg/
PRINCIPAL: 174561
TEXT:
Cr�� : 14-02-2014 16:58
MetroZap : le plus long c�lin de tous les temps
ZAPPING - Au menu du jour, retrouvez aussi des b�b�s qui distinguent le bien du mal, la jalousie de George Clooney pour Jean Dujardin, la journaliste de France 3 interrompu, le courage d'un patineur am�ricain et un but malchanceux.
�
Au programme du MetroZap aujourd'hui :
- Aujourd'hui c'est la Saint Valentin et � cette occasion un centre-commercial de Pattaya (Tha�lande) mise tout sur le romantisme : il a lanc� hier le record du monde du plus long c�lin de tous les temps pour lequel des dizaines de concurrents s'affrontent en s'enla�ant. Ils sont encore au travail en ce moment m�me puisque le record � battre avoisine les 26 heures.
- Reportage dans un centre am�ricain qui tente de prouver que les b�b�s distinguent le bien du mal : ces chercheurs montrent une sc�ne aux enfants avec deux peluches, l'une qui repr�sente le bien et l'autre le mal. Au terme de cette mise en sc�ne, 80% des b�b�s arriveraient � reconnaitre la peluche gentille.
- Georges Clooney jaloux de Jean Dujardin : l'acteur n'a visiblement pas dig�r� que son ami fran�ais ait remport� l'Oscar du meilleur acteur l'an dernier avec The Artist !
- Quand la pr�sentatrice de la m�t�o de France 3 se fait interrompre brutalement : la sc�ne s'est pass�e dans le 12/13, un journal qui demande une ponctualit� parfaite pour les d�crochages r�gionaux.
- Le courage d'un patineur am�ricain : Jeremy Abbott a poursuivi sa prestation apr�s une lourde chute sous les ovations du public.
- On se quitte avec la vid�o virale du jour : il s'agit d'un p�nalty incroyable en division amateur, d'abord arr�t� par le gardien. Le destin (ou plut�t le vent) avait visiblement d�cid� que ce ballon entrerait dans les filets.
