TITRE: Indon�sie : L'�ruption du Kelud sur l'�le de Java provoque l'�vacuation de plus de 200.000 personnes
DATE: 2014-02-14
URL: http://www.zinfos974.com/Indonesie-L-eruption-du-Kelud-sur-l-ile-de-Java-provoque-l-evacuation-de-plus-de-200-000-personnes_a68186.html
PRINCIPAL: 172424
TEXT:
Indon�sie : L'�ruption du Kelud sur l'�le de Java provoque l'�vacuation de plus de 200.000 personnes
�  @PoetryReading
Plus de 200.000 personnes ont �t� �vacu�es dans nuit suite � l'�ruption du Kelud, situ� � l'Est de l'�le de Java. Situ� dans une r�gion dens�ment peupl�e, ce volcan est particuli�rement dangereux. Les villes alentours sont d�j� recouvertes de cendres tandis que la lave coule dans certains villages o� l'air est satur� de soufre.
Les services de secours ont envoy� des SMS aux habitants pour les exhorter � ne pas rentrer chez eux, expliquant que la lave continuait de couler dans certains villages et que l'air �tait localement satur� de soufre.
"Des pluies de gravier et de sable ont atteint jusqu'� 15 km de distance du crat�re, touchant et blessant ceux en train d'�tre �vacu�s. Cela signifie que les d�plac�s doivent se rendre � plus de 15 km de distance du volcan", a fait savoir le volcanologue Umar Rosadi.
Selon des t�moins, les cendres ont m�me atteint Surabaya, la deuxi�me plus grande ville du pays situ�e � 130 kilom�tres du volcan.
Vendredi 14 F�vrier 2014 - 09:38
S.I
Notez
Lu 629 fois, cliquez sur une des ic�nes ci-dessous pour partager cet article avec votre communaut�
Dans la m�me rubrique :
