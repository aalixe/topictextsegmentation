TITRE: House of Cards : Obama contre les spoilers - News s�rie TV
DATE: 2014-02-14
URL: http://series-tv.premiere.fr/News-Series/House-of-Cards-Obama-contre-les-spoilers-3953115
PRINCIPAL: 173981
TEXT:
House of Cards : Obama contre les spoilers
14/02/2014 - 12h59
0
�
Barack Obama est tr�s actif sur Twitter, il a profit� r�cemment du r�seau social pour faire une demande assez insolite. Le pr�sident des �tats-Unis ne veut pas �tre spoil� sur la saison 2 d'House of Cards !
Les S�ries TV et Barack Obama , une grande histoire d'amour. Il y a 4 ans, presque jour pour jour, le pr�sident des �tats-Unis avait d�cid� de d�caler un discours qui tombait au m�me moment que la diffusion du premier �pisode de la saison 6 de... Lost . Aujourd'hui, c'est House of Cards qui a l'honneur d'�tre mise en avant par Obama.
Afin de ne rien conna�tre des intrigues de la saison 2 d' House of Cards , qui d�barque aujourd'hui sur Netflix, le pr�sident a publi� sur son compte Twitter une demande assez insolite :
�
� Barack Obama (@BarackObama) February 13, 2014
"Demain House of Cards. Pas de spoilers s'il vous pla�t"
Si aux �tats-Unis les 13 �pisodes de la saison 2 sont d�j� disponibles, en France ils arriveront d�s le 13 mars sur Canal +.
