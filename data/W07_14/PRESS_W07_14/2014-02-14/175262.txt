TITRE: Sotchi 2014: les m�daill�s d�or de la 7�me journ�e | JO - lesoir.be
DATE: 2014-02-14
URL: http://www.lesoir.be/468366/article/sports/jo/2014-02-14/sotchi-2014-medailles-7eme-journee
PRINCIPAL: 0
TEXT:
Sotchi 2014: les m�daill�s d�or de la 7�me journ�e
B.Dn (Avec AFP)
vendredi 14 f�vrier 2014, 23 h 32
La moisson du jour a �t� bonne pour le Belarus et la Suisse qui ont remport� deux m�dailles d�or. Retour sur la journ�e de vendredi.
La Japonais Yuzuru Hanyu a domin� le concours de patinage artistique. Photo AFP.
Sur le m�me sujet
Au cours de la journ�e, le Belge Jorik Hendrickx s�est empar� de la 16e place du concours de patinage artistique � l�issue du programme libre.
Patinage artistique
Les chutes ont rythm� le �libre� de patinage artistique messieurs, jusqu�� ternir l�or historique conquis par le Japonais Yuzuru Hanyu.
Premier Japonais sacr� chez les messieurs, Hanyu, 19 ans, dirig� par l��minent entra�neur canadien Brian Oser � Toronto, est tomb� � deux reprises pendant son programme, ex�cut� sur l�air de Rom�o et Juliette.
Visiblement d�pit� par sa performance dans le �kiss and cry�, cette chambre de torture en mondovision o� les patineurs attendent leurs notes, Hanyu a pourtant �t� class� en t�te du programme libre, devant le Canadien Patrick Chan qui, lui, posa les deux mains sur la glace, et le Kazakh Denis Chen.
Qu�importe... Ce titre consacre un talent pr�coce dont l�unique m�daille, en bronze, avait �t� obtenue aux Mondiaux 2012 � Nice. Elle r�compense aussi un bel acharnement, puisque sa maison et la patinoire dans laquelle il s�entra�nait avaient �t� d�truites par le tremblement de terre qui frappa le Japon en mars 2011.
Ski alpin
Le ballet des chutes qui a accompagn� le titre de Hanyu a �galement frapp� le slalom du super-combin� de ski alpin, sur une piste rapidement parcourue de pi�ges, en raison d�une neige molle et sablonneuse.
Quatorzi�me temps de la descente dans la matin�e, Sandro Viletta a profit� d�une piste pas encore d�grad�e par le r�chauffement ambiant pour r�aliser une manche de slalom quasi parfaite. Ses adversaires sont all�s � la faute. Le Fran�ais Pinturault est sorti de la piste, son compatriote Mermillod Blondin a d�val� le mur final sur le dos.
Puis le Croate Ivica Kostelic, septi�me temps de la descente et grand sp�cialiste du super-combin� a �t� victime de la piste tr�s d�grad�e. Il a �chou� � la deuxi�me place, devant l�Italien Christof Innerhofer, en bronze apr�s avoir termin� deuxi�me de la descente.
Ski de fond
Apr�s Sandro Viletta, vainqueur surprise du super-combin�, un autre Suisse a d�croch� de l�or. Dario Cologna , d�j� sacr� en skiathlon, est devenu champion olympique du 15 km classique.
Cette deuxi�me m�daille d�or individuelle lui permet de rejoindre le Fran�ais Martin Fourcade et la B�larusse Daria Domracheva (biathlon), eux aussi candidats au titre d�homme (ou de femme) des JO.
Pour les Suisses, la performance est �galement collective. Une semaine apr�s la c�r�monie d�ouverture, ils ont accumul� 7 m�dailles dont 5 en or, et occupent la deuxi�me place au classement des m�dailles derri�re l�Allemagne.
Biathlon
Le Belarus a �galement ajout� deux m�dailles d�or � son palmar�s. D�abord, Daria Domracheva s�est impos�e dans le 15 km en biathlon.
Ski acrobatique
Un peu plus tard, Alla Tsuper a remport� le concours de saut acrobatique (ski freestyle) devant la Chinoise Xu Mengtao et l�Australienne Lydia Lassila.
Skeleton
Enfin, apr�s la luge, domaine r�serv� des Allemands, et le patinage de vitesse, chasse gard�e des N�erlandais, les Britanniques ont fait main basse sur le skeleton. Elizabeth Yarnold a �t� sacr�e championne olympique quatre ans apr�s sa compatriote et logeuse Amy Williams.
Yarnold est un pur produit du syst�me britannique, qui a r�ussi � placer un athl�te sur chaque podium depuis l�apparition du skeleton f�minin au programme olympique en 2002.
