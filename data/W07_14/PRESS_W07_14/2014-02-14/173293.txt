TITRE: Qui sera le grand gagnant des Victoires de la musique 2014 ? - Actualit� musique - MusicActu
DATE: 2014-02-14
URL: http://www.musicactu.com/actualite-musique/148926/qui-sera-le-grand-gagnant-des-victoires-de-la-musique-2014/
PRINCIPAL: 173291
TEXT:
Accueil > Flash > Sc�ne fran�aise > Victoires de la musique
Qui sera le grand gagnant des Victoires de la musique 2014 ?
Stromae, Etienne Daho, Vanessa Paradis, Zaz... Ils sont nomm�s aux Victoires de la musique, qui se d�rouleront ce vendredi 14 f�vrier.
Envoyez cet article � un ami
Ne pas remplir :
r�daction le 14/02/2014 pour MusicActu
Partager
La 29e �dition des Victoires de la musique aura lieu ce vendredi 14 f�vrier. La c�r�monie se d�roulera au Z�nith de Paris et sera retransmise en direct sur France 2, France Inter et France Bleu. Si le duo fran�ais Daft Punk ne sera pas de la partie, plusieurs artistes ont r�pondu positivement � l'invitation, comme Stromae, qui interpr�tera, durant la soir�e, plusieurs de ses titres issus de son album "Racine carr�e". Le chanteur belge, grand favori pour cette ann�e 2014, est nomm� six fois.
Lors de la c�r�monie des Victoires, Vanessa Paradis tentera sa chance pour le prix de l'"Artiste interpr�te f�minine", Julien Dor� d�fendra son album "L�ve" dans la cat�gorie "Album de chansons", et Phoenix, groupe de pop rock fran�ais � la renomm�e internationale, est en lice pour l'"Album rock".
Commenter
