TITRE: Somalie: attentat meurtrier contre un convoi de l�ONU - France - RFI
DATE: 2014-02-14
URL: http://www.rfi.fr/afrique/20140213-somalie-attentat-voiture-piegee-contre-convoi-onu/
PRINCIPAL: 172239
TEXT:
Modifi� le 13-02-2014 � 18:32
Somalie: attentat meurtrier contre un convoi de l�ONU
Devant les locaux de l'ONU � Mogadiscio, apr�s l'attentat des shebabs, le 19 juin 2013.
Reuters / Taxta
Somalie
Une attaque � la voiture pi�g�e, revendiqu�e par les shebabs, a fait au moins six morts � Mogadiscio ce jeudi 13 f�vrier. Chass�s de la capitale en 2011, ces islamistes y commettent r�guli�rement des attentats. Cette fois, l'op�ration a eu lieu en bordure du complexe ultra-s�curis� o� se trouve l'a�roport. Les Nations unies �taient clairement vis�es.
Vers 12h30, une voiture a barr� la route � deux v�hicules blind�s de l'ONU, avant d'exploser. Les shebabs d�clarent avoir vis� ceux qu'ils d�signent comme des ��envahisseurs�� gr�ce au sacrifice d'un fr�re, sugg�rant donc une attaque suicide.
Aucun employ� de la mission des Nations unies en Somalie (UNSOM) n'a, en tout cas, �t� touch�, selon la mission. Quatre Somaliens escortant le convoi ont �t� bless�s et les victimes sont surtout civiles.
L'attaque a eu lieu juste � l'entr�e du complexe a�roportuaire, un secteur tr�s s�curis�, avec blocs de b�ton, murs anti-d�flagrations, et barri�re de contr�le. Or, c'est souvent l� que des gens attendent les passagers venant d'atterrir � Mogadiscio. On trouve d'ailleurs des boutiques et des restaurants � cet endroit.
Pour les shebabs, c'est une op�ration tr�s symbolique puisque, outre l'a�roport, le complexe abrite des bureaux de l'ONU, plusieurs ambassades occidentales et le quartier g�n�ral de l'Amisom. La mission africaine en Somalie parle d'un acte d�sesp�r� des islamistes, et d�un signe de leur faiblesse.
