TITRE: Facebook permet � ses utilisateurs de d�finir le genre qu'ils souhaitent
DATE: 2014-02-14
URL: http://www.huffingtonpost.fr/2014/02/14/facebook-genre-lgbt-trans-bi-intersexuel_n_4786958.html
PRINCIPAL: 173388
TEXT:
Facebook permet � ses utilisateurs de d�finir le genre qu'ils souhaitent
Le HuffPost avec AFP �|� Par Lauren Provost
Publication: 14/02/2014 11h36 CET��|��Mis � jour: 14/02/2014 11h36 CET
Facebook donne de nouvelles options de genre � ses utilisateurs         | Facebook
Recevoir les alertes:
Facebook , LGBT , Androgyne , Diversit� , Facebook Genre , Facebook Lgbt , Genre , r�seaux sociaux , Techno , Transexuel , Transgenre , Actualit�s
LGBT - Les utilisateurs de Facebook ne seront plus seulement limit�s � "homme" ou "femme" pour renseigner leur genre, a annonc� jeudi le r�seau social. Facebook a ajout� de nouvelles options comme "transsexuel" ou encore "intersexuel".
En se rendant dans les param�tres de leur compte, les membres de Facebook peuvent choisir "custom" et renseigner le genre de son choix.
Outre le genre, les abonn�s ont aussi la possibilit� de choisir le pronom personnel par lequel ils voudraient �tre interpell�s dans les posts. A c�t� des classiques "lui/il" et "elle", il y aura d�sormais "on", jug� plus neutre.
"Si pour beaucoup ces changements n'ont pas beaucoup d'importance, pour ceux qui en ont souffert c'est quelque chose d'important", �crit Facebook dans un post sur sa page Diversit� , qui affiche une photo du drapeau arc-en ciel, �tendard de la communaut� homosexuelle et transgenre, flottant sur le campus du groupe dans la Silicon Valley.
Post by Facebook Diversity .
"Nous voyons ces �volutions comme un moyen suppl�mentaire pour faire de Facebook un lieu o� les gens peuvent exprimer librement leur identit�", ajoute le r�seau social.
Facebook indique avoir collabor� avec des associations de d�fense des droits des lesbiennes, gays, bisexuels et transgenre pour cr�er ces nouvelles options qui peuvent �tre trouv�es dans la cat�gorie "autres".
Pour l'instant, les options ne sont disponibles que pour les internautes utilisant le r�seau en anglais (am�ricain), mais Facebook pr�voit de les �tendre � d'autres r�gions dans l'avenir.
Loading Slideshow
Ici, des choix comme "sans genre", "androgyne", "trans" ou "<a href="http://en.wikipedia.org/wiki/Pangender" target="_blank">pangender</a>" le terme anglosaxon pour d�signer les personnes qui ne souhaitent pas �tre �tiquet� homme ou femme.
Ici "bigenre" (les personnes qui appartiennent � la fois aux genres f�minin et masculin) ou "<a href="http://gender.wikia.com/wiki/Non-binary" target="_blank">non-binaire</a>" (un autre terme pour les personnes qui ne correspondent ni au genre masculin ni au genre f�minin).
Ici, diff�rentes propositions de transidentit�s.
LIRE AUSSI
Retrouvez les articles du HuffPost sur notre page Facebook .
Pour suivre les derni�res actualit�s en direct, cliquez ici .
Egalement sur Le HuffPost:
Loading Slideshow
Octobre 2003 : Facemash
Avant Facebook, il y a eu Facemash.  Alors en seconde ann�e � Harvard, Mark Zuckerberg et trois camarades de classe lancent facemash.com. Un jeu de "hot or not" qui met c�te � c�te deux photos d'�tudiants du campus et propose aux utilisateurs de choisir qui est le "plus hot".  Pour obtenir ces photos, Zuckerberg a hack� le trombinoscope d'Harvard. L'universit� fera d'ailleurs fermer son site pour des questions l�gales. "Est-ce nous sommes arriv�s l� pour notre apparence physique? Non. Est-ce que nous serons jug� dessus? Oui", pouvait-on lire sur le site. "Qui est le plus hot? Cliquez pour choisir".
4 f�vrier 2004 : "Le" Facebook
Malgr� les risques qu'il a pris avec Facemash, Mark Zuckerberg met en ligne "The Facebook", un nouveau d�riv� du trombinoscope de l'universit� (d'ailleurs "the facebook" veut dire "le trombinoscope").  En 24 heures, le site compte d�j� 1200 abonn�s. Un mois plus tard, la moiti� des �tudiants de l'universit� poss�dent un profil sur le r�seau.
A ces d�buts, The Facebook est un simple annuaire virtuel. Chaque utilisateur peut y afficher son nom, ses coordonn�es, ses cours, une photo, ce qu'il recherche (des amis ou une relation?), s'il est c�libataire ou pas, ses centres d'int�r�ts...   D�s le mois de mars, le site s'�tend aux campus de Stanford, Columbia, Yale... La conqu�te des campus se poursuit � une vitesse folle et le r�seau social s'installe � Palo Alto d�s le mois de juin. En 2005, le "the" dispara�t et l'�quipe <a href="http://www.theregister.co.uk/2007/10/01/facebook_domain_dispute/" target="_blank">ach�te un nouveau nom de domaine pour 200.000 dollars</a> : facebook.com.
A l'�poque, pas de pages, les groupes sont rois.
2005 : Facebook tout court
En ao�t 2005, Facebook laisse tomber son "the" et revendique  d�j� 5,5 millions d�utilisateurs en fin d�ann�e.   Progressivement, le profil des utilisateurs s'enrichit. Les photos prennent plus d'ampleur et chaque compte peut en afficher d'avantage. En septembre, le "mur" (le "wall") appara�t sur les profils des utilisateurs.
2005 : le poke
2005, c'est aussi l'apparition du "poke", une option bien myst�rieuse.  Facebook lance une petite icone, peu esth�tique, montrant un doigt tendu ou plut�t deux doigts en train de pincer. Pour quoi faire? Bonne question. Le poke ne veut rien dire et n'a jamais voulu rien dire.  En anglais, "poke" peut vouloir dire "coucou" comme "coucher". Et Facebook laisse les utilisateurs s'amuser de cette ambigu�t� et faire ce qu'ils veulent du poke. "On pensait que �a serait marrant de cr�er une appli qui n'aurait pas de vraie finalit� et de voir ce qui se passerait. Alors d�brouillez-vous, vous n'aurez pas d'explication de notre part", a d'ailleurs esquiv� Zuckerberg. Mais on ne va pas se mentir, le poke devient tr�s vite une option pour flirter, un pr�liminaire virtuel.
Avril 2006 : Facebook fait ses premiers pas sur mobile
Septembre 2006: le lancement du fil d'actualit�
A la rentr�e 2006, Facebook cr�e le "news feed". Au-del� des profils des uns et des autres, le r�seau social se dote d'un flux qui met en avant ce qu'il se passe dans notre sph�re sociale, tout au long de la journ�e.   C'est aussi en septembre 2006 que Facebook s'ouvre � tous. Plus besoin d'�tre �tudiant.
Septembre 2006 : les utilisateurs s'insurgent
Alors ce nouveau fil d'actualit�? Figurez-vous que les utilisateurs ont d�test�! Ces derniers ont imm�diatement d�nonc� le manque de confidentialit� du news feed. <a href="http://mashable.com/2006/09/08/facebook-gets-egg-on-its-face-changes-news-feed-feature/"> Mark Zuckerberg d�clare m�me que Facebook</a> "a foir� sur ce coup-l�" car le nouveau flux manque cruellement d'outils pour choisir ce qui y appara�t et ce qui n'y appara�t pas.   Facebook est contrait de le revoir quelques jours plus tard.
Novembre 2007 : les pages arrivent
... et la publicit�.
Novembre 2007: Facebook demande votre avis
Fin 2007, <a href="http://www.insidefacebook.com/2008/07/31/facebook-news-feed-preferences-return/">Facebook donne la parole � ses utilisateurs</a> avec un outil leur permettant de dire s'ils aiment voir un contenu sur leur fil d'actualit� ou pas. Le fil d'actualit� s'orne d'un "pouce en l'air" et d'une croix "x" � c�t� des publications.   Facebook explique que cela va permettre � ses �quipes de faire �voluer le news feed.
F�vrier 2008 : Facebook s'ouvre au monde
"Hola", "guten tag", "bonjour". <a href="http://www.lefigaro.fr/medias/2008/03/08/04002-20080308ARTFIG00659-le-site-facebook-en-francais-est-lance.php" target="_blank">Facebook lance ses premi�res versions en langue �trang�re avec un site en espagnol, en allemand et en fran�ais</a>. La conqu�te du monde d�bute vraiment et Facebook s'�vertue � trouver des �quivalents � ses fonctions dans toutes les langues.   Comment dit-on "poke" alors? "Dar un toque" en espagnol, "anstupsen" en allemand et "envoyer un poke" en fran�ais.
Avril 2008 : la premi�re messagerie instantann�e
Le 7 avril 2008, appara�t Facebook Chat. Adieu MSN, bonjour le flirt instantan�?
Juillet 2008: les pr�f�rences du fil d'actualit�
Facebook permet � ses membres de dire <a href="http://www.insidefacebook.com/2008/07/31/facebook-news-feed-preferences-return/">s'ils veulent voir plus ou moins un type de contenu</a> sur leur fil d'actualit�.   Ils peuvent choisir leurs pr�f�rences pour chaque ami.
Juillet 2008 : La premi�re appli sur iPhone
Un an apr�s le site mobile, Facebook lance sa premi�re application IOS: Facebook for iPhone.
F�vrier 2009 :  le "like" d�barque
Vous avez l'impression que le pouce en l'air de Facebook a toujours �t� l�? Eh bien non, le "like" n'est arriv� qu'en f�vrier 2009 en bas des statuts, commentaires, photos, liens... Une fa�on pour les utilisateurs d'approuver sans avoir besoin de commenter!  Que se passe-t-il � l'�poque quand on clique sur le pouce? Le contenu appara�t syst�matiquement dans le fil d'actualit� des amis.   Pour les pages Facebook, on vous propose encore de "devenir fan" durant quelques mois. Le "like" pour s'abonner n'arrive qu'en 2010. Il va permettre aux utilisateurs de voir combien de personnes suivent leur page. C'est � ce moment l� que Facebook propose aux sites d'int�grer son "like" chez eux pour que les internautes partagent leurs liens.
Mars 2009: un flux en temps r�el
Alors que Twitter prend de l'ampleur,<a href="https://blog.facebook.com/blog.php?post=59195087130"> Facebook se pr�occupe du temps r�el</a>.   Le fil d'actualit� se met d�sormais � jour chronologiquement et la page d'accueil est redessin�e.
Octobre 2009: le  flux s'adapte � vos go�ts
Facebook d�cide de montrer... ce que vous aimez! Gr�ce � un algorithme, le fil d'actualit� propose les contenus les plus populaires et "engageants" depuis la derni�re connexion de l'utilisateur. <a href="http://gigaom.com/2009/10/23/facebook-news-feed-no-longer-just-live/">La chronologie pure et simple dispara�t</a>.
Ao�t 2010 : La g�olocalisation
En ao�t 2010 Facebook lance son "Foursquare killer", une fonction sur mobile qui permet aux utilisateurs de dire o� ils se trouvent. Son nom: Facebook Places.  Mais en 2011, <a href="http://www.lemondeinformatique.fr/actualites/lire-facebook-abandonne-places-mais-pas-la-geolocalisation-34476.html" target="_blank">Facebook laisse tomber Places</a> et g�n�ralise la g�olocalisation. D�sormais, les utilisateurs peuvent associer un statut, une photo, un message...
D�cembre 2010 - les premiers filtres par type de contenus
Avec l'onglet "plus r�centes", <a href="http://mashable.com/2010/12/21/facebook-news-feed-filtering/">l'utilisateur peut filtrer son fil d'actualit�</a> en diff�renciant photos, liens, pages et jeux.
F�vrier 2011 - Tous les amis ou les amis proches ?
Facebook lance une option permettant de voir les contenus de tous vos amis et des pages auxquelles vous �tes abonn�s... ou seulement celles de vos amis proches et des pages avec lesquelles vous interagissez le plus.  Source:<a href="http://www.insidefacebook.com/2011/02/11/edit-news-feed-settings/"> Inside Facebook</a>.
Septembre 2011 : Le "follow"
Facebook donne la possibilit� de suivre un autre utilisateur sans devenir son "ami". Un bouton "follow", ou "s'abonner" en fran�ais appara�t en haut des profils.   D'ailleurs, saviez-vous que Mark Zuckerberg est la seule personne que vous ne pouvez pas demander en ami sur Facebook?
Septembre 2011 : la timeline
Le "mur" Facebook devient un "journal" ou comme on dit � Palo Alto : une "timeline". Le profil s'organise par date, avec une frise chronologique pour remonter le temps et renseigner de nouvelles infos sur votre vie... avant Facebook.  Zuckerberg vous propose de devenir votre propre biographe. Il devient possible de compiler toute sa vie sur une seule page, de sa naissance � son premier baiser, de son premier stage � son premier enfant... Ce nouveau profil devient obligatoire pour tous fin mars 2012.
... et une photo de couverture
Avec la timeline arrive la photo de couverture, la grande banni�re qu'il faut maintenant personnaliser en plus d'avoir une photo de profil au poil. <- <a href="http://realitypod.com/2013/03/top-10-great-facebook-timeline-cover-photo-ideas/" target="_blank">Certains ont de tr�s bonnes id�es</a> pour utiliser ce nouvel espace
Septembre 2011: l'apparition des "top stories"
Peu de temps apr�s l'apparition de l'option "s'abonner", Facebook permet � ses utilisateurs de visualiser directement les informations les plus importantes pour eux.   D�sormais, le fil d�actualit� s'adapte pour ressembler davantage au profil de l'utilisateur. <blockquote>"Vous n�aurez pas � vous inqui�ter de passer � c�t� d�informations importantes, explique alors Facebook. Toutes les informations s�afficheront dans un seul et m�me fil avec les informations les plus importantes plac�es tout en haut. Si vous ne vous �tes pas rendu sur Facebook pendant un certain temps, les premi�res choses que vous verrez appara�tre dans votre fil d�actualit� seront les meilleurs photos et statuts ayant �t� publi�s sur Facebook en votre absence. Vous pourrez les identifier facilement gr�ce � un petit onglet bleu."</blockquote>
Septembre 2011 - le telex
Avec tous ces changements, le fil d'actualit� manque maintenant de temps r�el. Pour cette raison,<a href="https://blog.facebook.com/blog.php?post=10150286921207131"> Facebook invente le "telex"</a> un flux - plus petit - qui affiche les conversations en temps r�el en affichant instantan�ment les mises � jour.  D�sormais, lorsqu�un ami commente, pose une question ou partage quelque chose comme par exemple une visite, l'utilisateur le voit dans son telex et peut s'inviter dans la conversation.
Janvier 2012 : l'apparition de la publicit� dans le flux
<a href="http://www.zdnet.com/blog/facebook/facebook-starts-displaying-ads-in-the-news-feed/7143">Les publicit�s d�barquent dans le fil d'actualit�</a>. Elles sont signal�es  par les mentions "sponsoris�" et "actualit� sugg�r�e" ("featured" et "sponsored" en anglais). Critiqu�, ce type de publicit� <a href="http://www.zdnet.fr/actualites/actualites-sponsorisees-facebook-abandonne-ce-format-publicitaire-decrie-39796956.htm" target="_blank">devrait dispara�tre</a> dans les mois � venir.
Mars 2012 : les listes d'int�r�ts
Les utilisateurs sont invit�s � s'abonner � des "listes d'int�r�ts". Une nouvelle fa�on de suivre ses sujets favoris et de cr�er du contenu.
Juin 2012 : l'App Center
<a href="http://www.huffingtonpost.fr/2012/06/08/facebook-app-center-le-re_n_1579973.html" target="_blank">Le r�seau social lance son "espace applications"</a>, une plateforme web et surtout mobile qui permet aux utilisateurs de d�couvrir des applications recommand�es par Facebook et leurs amis. Facebook (alors � la tra�ne sur mobile) se positionne enfin sur le march� des applications en recommandant ou pas celles des autres...
Ao�t 2012 : Instagram arrive sur Facebook
Facebook a rachet� Instagram en avril 2012 (pour un milliard de dollars) et int�gre progressivement sa nouvelle application photos � son univers.
Janvier 2013 : un moteur de recherche omnipotent (ou presque)
Pour commencer 2013, Facebook lance "Graph Search", "recherche dans le graphe" dit-on en fran�ais. Il s'agit d'un moteur de recherche avanc� qui recoupe toutes les informations et les "actions" (comme le fait d'aimer ou de commenter une publication disponibles) sur Facebook. Tr�s d�cri�, il est pour l'instant r�serv� aux utilisateurs en langue anglaise. <a href="http://www.lemonde.fr/technologies/article/2013/07/17/facebook-comment-le-nouvel-outil-de-recherche-va-affecter-votre-profil_3448705_651865.html" target="_blank">En lire plus ici. </a>
Mars 2013 : un nouveau design et un fil � la carte
Le 7 mars 2013, Mark Zuckerberg  r�unit la presse pour pr�senter un tout nouveau design du fil d'actualit�. Le nouveau news feed fait la part belle � l'image et offre la possibilit� de faire le tri dans ce qu'on souhaite consulter avec une s�rie de filtres. <a href="http://www.huffingtonpost.fr/2013/03/07/facebook-fil-actualite-newsfeed-nouveau-mode-emploi_n_2829598.html" target="_blank">En lire plus ici</a>.
Ce nouveau design touche aussi la timeline. Facebook propose aux utilisateurs d'organiser diff�remment leurs centres d�int�r�ts avec des sections d�di�es au cin�ma, � la musique, � la litt�rature ou aux s�ries t�l�vis�es... Histoire d'en dire un peu plus encore sur soi.
4 avril 2013 : le presque-t�l�phone Facebook
�a n'est pas encore un t�l�phone Facebook, mais la firme de Palo Alto lance "Home", un ensemble d'applis qui permet de mettre Facebook au c�ur de l'utilisation de son mobile (sous Andro�d).
29 mai 2013 : des profils v�rifi�s
Facebook lance ses "Verified Pages and Profiles" pour les entreprises et les c�l�brit�s (comme sur Twitter).
12 Juin 2013 : les hashtags arrivent
Dans la s�rie "comme sur Twitter", Facebook se met aux hashtags.   Des # peuvent �tre utilis�s dans les statuts des utilisateurs et les posts des pages pour participer � des discussions publiques et d�couvrir ce qui se dit sur un sujet sp�cifique.
D�cembre 2013 - Plus d'informations dans le fil d'actualit�
Le 2 d�cembre 2013, Facebook annonce vouloir mettre l'accent sur l'information.   Les utilisateurs voient progressivement appara�tre davantage d'actualit�s issues des m�dias et moins de statuts de la part de leurs amis par exemple.  Le r�seau social introduit �galement des propositions d'articles connexes en dessous des actualit�s sur lesquelles clique l'utilisateur.
Ao�t 2013 : des sentiments et des ic�nes
Alors que les statuts persos disparaissent progressivement et que de nouvelles applications de messagerie instantan�e cartonnent avec leurs "stickers" (de gros smileys), Facebook propose � ses utilisateurs de mettre � jour leurs statuts et de faire part de leurs "sentiments", � l�aide d�ic�nes.
D�cembre 2013 : les permi�res pubs vid�os
Apr�s les posts sponsoris�s, Facebook annonce l'introduction<a href="http://www.huffingtonpost.fr/2013/12/17/facebook-pub-video-profil_n_4457974.html" target="_blank"> des publicit�s vid�o</a> dans le fil d'actualit�.
17 janvier 2014 : des tendances � la Une
Encore une fois, comme Twitter, Facebook lance des "trending topics" soit les sujets � la Une. Pour les utilisateurs aux Etats-Unis, au Royaume-Uni, au Canada, en Inde et en Australie (pour commencer) un module appara�t en haut � droit du fil d'actu et affiche les sujets les plus discut�s � un moment donn�.
F�vrier 2014 : de nouvelles options de genre
Les utilisateurs de Facebook ne sont plus seulement limit�s � "homme" ou "femme" pour renseigner leur genre. Facebook ajoute de nouvelles options comme "transsexuel" ou encore "intersexuel".
Contribuer � cet article:
