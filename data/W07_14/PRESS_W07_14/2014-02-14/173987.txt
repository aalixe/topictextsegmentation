TITRE: Star Wars Identities : visite guid�e de l'exposition - L'actu cin�ma � TOUTLECINE.COM
DATE: 2014-02-14
URL: http://www.toutlecine.com/cinema/l-actu-cinema/0002/00028432-star-wars-identities-visite-guidee-de-l-exposition.html
PRINCIPAL: 173981
TEXT:
�
Star Wars Identities : visite guid�e de l'exposition
Il y a longtemps, dans une galaxie lointaine, tr�s lointaine... Pas besoin d'aller aussi loin pour retrouver l'univers de la saga culte puisque du 15 f�vrier au 30 juin � La Cit� du Cin�ma se tient l'exposition Star Wars Identities. Visite guid�e d'une fan conquise.
�
Entre l'annonce d'une nouvelle trilogie et les rumeurs sur le casting de Star Wars VII, la plan�te geek est en �moi. D�sol�e pour les allergiques de l'hyperespace mais ce n'est qu'un d�but�! L'Exposition Star Wars Identites entame sa tourn�e mondiale et la manquer, tu ne dois pas. Voir la marionnette de Maitre Yoda en vitrine, celle anim�e par Franck Oz, c'est peu comme voir une rock star. Et cela continue avec les costumes de Dark Vador, Chewbacca, la maquette du Faucon Millenium, le projojet grandeur nature d'Anakin et bien entendu le bikini de Le�a. Des tr�sors d'archives sont � d�couvrir dans cette exposition interactive.
�
Car oui, Star Wars Identities n'est pas seulement une succession de story-boards et de maquettes, c'est un vrai dialogue qui s�installe avec le public, m�me n�ophyte. Au-del� des anecdotes de tournage, c'est l'essence m�me, la morale de la saga qui nous est racont�e. Le parall�le entre Luke et Anakin notamment, p�re et fils pour ceux qui ont �t� cryog�nis�s ces 35 derni�res ann�es, illustre tr�s bien les diff�rentes trajectoires des personnages dans les deux trilogies. Ceux qui se sont laiss�s s�duire par le c�t� obscur de la Force et ceux qui lui ont r�sist�. La sc�nographie permet de profiter pleinement du spectacle, entre les vitrines, les �crans et les tablettes interactives. C�est une v�ritable plong�e dans l'imaginaire de George Lucas. Comme des gamins, on joue � �tre des Jedis.
�
L'autre grand atout c'est que tout au long du parcours, chaque visiteur, muni d'une merveille technologique sous la forme d'un bracelet, va enregistrer diff�rentes donn�es pour au final, cr�er son propre personnage de Star Wars. Au cours des diff�rentes pi�ces de l'exposition et en rapport avec le th�me abord�, vous choisirez sa couleur de peau, sa plan�te d'origine, son histoire familiale, son m�tier pour arriver � l'ultime question, tomberez-vous dans le c�t� obscur � votre tour�? Plut�t natif de Naboo ou de Tatooine, plut�t s�nateur ou chasseur de prime, peau verte ou violette, taux de midichloriens �lev�e ou non (traduction est ce que la Force est tr�s pr�sente dans votre famille ou non) ce sera � vous de choisir et de d�couvrir votre personnage dans la derni�re salle. Pas besoin de conna�tre la saga par c�ur pour en profiter, laissez vous juste porter par l'ambiance magique de cette exposition tr�s r�ussie.
�
Que la Force soit avec vous�!
Par Laura Terrazas (14/02/2014 � 12h37)
�
