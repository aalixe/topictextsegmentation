TITRE: Julie Gayet : une enquête ouverte pour mise en danger de la vie d'autrui | Site mobile Le Point
DATE: 2014-02-14
URL: http://www.lepoint.fr/medias/julie-gayet-une-enquete-ouverte-pour-mise-en-danger-de-la-vie-d-autrui-14-02-2014-1791802_260.php
PRINCIPAL: 0
TEXT:
14/02/14 à 15h52
Julie Gayet : une enquête ouverte pour mise en danger de la vie d'autrui
L'actrice dénonce un harcèlement de paparazzi après la révélation par "Closer" de sa liaison avec François Hollande.
Le parquet de Nanterre (Hauts-de-Seine) a récemment lancé une enquête pour "atteinte à l'intimité de la vie privée" après la publication dans le magazine "Closer" d'une photo de Julie Gayet le 17 janvier. AFP
Source AFP
Une enquête a été ouverte après une plainte pour "mise en danger de la vie d'autrui" de l'actrice Julie Gayet qui dénonce un harcèlement de paparazzi, a indiqué vendredi une source judiciaire. Ouverte par le parquet de Paris, cette enquête a été confiée à la brigade de répression de la délinquance aux personnes (BRDP), selon cette source qui confirme une information du Monde. Le parquet de Nanterre (Hauts-de-Seine) a récemment lancé une enquête pour "atteinte à l'intimité de la vie privée" après la publication dans le magazine Closer d'une photo de Julie Gayet le 17 janvier, une semaine après les révélations par le même journal sur sa liaison avec François Hollande.
Ces enquêtes sont distinctes du procès au civil pour atteinte à la vie privée qui opposera la comédienne de 41 ans à Closer le 6 mars à Nanterre.
Les révélations de Closer, le 10 janvier, s'appuyaient sur des photos volées de Julie Gayet et François Hollande, photographiés à chaque fois séparément sur la voie publique, devant un immeuble parisien où le couple se retrouvait régulièrement. Une semaine plus tard, le magazine publiait une nouvelle édition en affirmant que la liaison entre le président et l'actrice durait depuis deux ans. Lors de sa conférence de presse du 14 janvier , François Hollande avait fait part de son "indignation totale" après la publication du dossier de Closer, mais avait annoncé qu'il n'attaquerait finalement pas personnellement en justice l'hebdomadaire people.
