TITRE: Eruption du volcan Kelud en Indon�sie: deux morts et 200.000 personnes �vacu�es - BFMTV.com
DATE: 2014-02-14
URL: http://www.bfmtv.com/international/indonesie-leruption-volcan-kelud-fait-deux-morts-710824.html
PRINCIPAL: 172865
TEXT:
L'�ruption spectaculaire du mont Kelud sur l'�le de Java en Indon�sie a fait deux morts, entra�n� l'�vacuation de 200.000 personnes et paralys� plusieurs a�roports, ont annonc� les autorit�s vendredi.
�� �
Le volcan Kelud, particuli�rement dangereux car situ� dans une r�gion dens�ment peupl�e, est entr� en �ruption jeudi soir, projetant cendre et roche ardentes sur les villages environnants � des kilom�tres � la ronde.
�� �
Un homme et une femme �g�s d'une soixantaine d'ann�es ont p�ri sous les d�combres de leurs maisons respectives dont les toits se sont effondr�s sous le poids des d�bris du volcan, a annonc� le porte-parole de l'agence de pr�vention des situations d'urgence, Sutopo Purwo Nugroho. "Ces maisons �taient mal b�ties et semblent avoir c�d� facilement", a-t-il dit.
Les a�roports ferm�s
Le porte-parole de l'agence de gestion des catastrophes a indiqu� que   200.000 personnes habitant 36 villages dans un rayon de 10 kilom�tres   autour du Kelud avaient re�u l'ordre d'�vacuer.
Les services de  secours ont envoy� des SMS aux habitants pour les  exhorter � ne pas  rentrer chez eux, expliquant que la lave continuait de  couler dans  certains villages et que l'air �tait localement satur� de  soufre.
Les  a�roports de Surabaya, Yogyakarta et Solo ont �t� ferm�s, a annonc� le  responsable de l'aviation au minist�re des Transports, Herry Bakti, sur  la cha�ne Metro TV qui a film� des avions couverts de cendre sur le  tarmac.
DIAPORAMA
