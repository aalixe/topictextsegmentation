TITRE: Se laver les mains att�nue le sentiment de culpabilit�
DATE: 2014-02-14
URL: http://www.huffingtonpost.fr/2014/02/14/laver-mains-culpabilite_n_4787007.html
PRINCIPAL: 173829
TEXT:
Se laver les mains att�nue le sentiment de culpabilit�
Le HuffPost/AFP �|� Par Le HuffPost
Publication: 14/02/2014 12h23 CET��|��Mis � jour: 14/02/2014 12h25 CET
Se laver les mains att�nue le sentiment de culpabilit� chez une personne (�tude)         | Paul Bradbury via Getty Images
Recevoir les alertes:
Culpabilite , Etude , Experience , Hygiene , Laver Mains Culpabilit� , psychologie , Se Laver Les Mains , Actualit�s
PSYCHOLOGIE - Vous �tes le genre de personne � culpabiliser pour un rien? Des chercheurs grenoblois ont trouv� la solution pour att�nuer le sentiment de culpabilit� que vous pouvez d�velopper. Il suffirait de se laver les mains un peu plus souvent.
Cette �tude r�alis�e par des chercheurs grenoblois, en parall�le de celle men�e par l' Universit� de Louvain (Belgique) et l' Universit� d'Etat de l'Ohio (USA), a �t� publi�e dans la revue internationale Frontiers in Human Neuroscience , et d�voile que se laver les mains, ou regarder quelqu'un le faire, att�nue le sentiment de culpabilit� chez une personne.
Lire aussi
� Plus de microbes dans les s�che-mains que sur les serviettes
Soixante-cinq usagers d'une biblioth�que municipale grenobloise ont �t� invit�s � se rem�morer et � mettre par �crit une mauvaise action commise � l'�gard d'un ami proche ou d'un membre de leur famille. Ils ont ensuite �t� r�partis en trois groupes: le premier a d� se laver les mains avec une lingette, le deuxi�me a regard� une vid�o montrant quelqu'un qui se lavait les mains et le troisi�me a visionn� une vid�o montrant des mains tapotant sur un clavier d'ordinateur. Les participants pensaient que leur sentiment de culpabilit� �tait mesur� lors de cet exercice.
La petite "farce" des chercheurs
A la fin, les chercheurs ont propos� aux participants d'aider les doctorants dans leurs recherches et pour cela, ils pouvaient remplir des questionnaires et les renvoyer par courrier trois semaines plus tard. L'exp�rience a montr� que les participants s'�tant lav� les mains avaient un sentiment de culpabilit� beaucoup plus faible et sont ceux qui ont renvoy� le moins de questionnaires.
"La simple �vocation du fait de se laver les mains, en voyant quelqu'un le faire sur un �cran, suffit � produire cet effet", quoique de mani�re moins prononc�e que si l'individu se lave lui-m�me les mains, souligne Laurent B�gue, professeur de psychologie sociale � l' Universit� Pierre-Mend�s-France de Grenoble (France).
A l'inverse, les participants ne s'�tant pas lav� les mains et n'ayant regard� personne le faire ont �prouv� le plus fort sentiment de culpabilit� et ont eu le comportement le plus altruiste. "Quand les gens se sentent coupables de quelque chose qu'ils ont fait, ils r�alisent souvent des actions "prosociales" pour se d�barrasser de leur culpabilit�", expliquent les auteurs de l'�tude.
En somme, "se laver les mains peut effacer la culpabilit�. Malheureusement, cela r�duit aussi les comportements altruistes", soulignent les chercheurs, pour qui "se laver les mains est bon pour l'hygi�ne mais mauvais pour les relations sociales". Pour Laurent B�gue, ces r�sultats s'expliquent par "l'universalit� des rituels de purification par l'eau qui lient la propret� physique � la propret� morale".
D�couvrez d'autres articles sant�, alimentation, tendances et sexualit� dans notre rubrique C'est la vie
Retrouvez les articles du HuffPost C'est la vie sur notre page Facebook .
Pour suivre les derni�res actualit�s en direct, cliquez ici .
Contribuer � cet article:
