TITRE: L'�lu Jean Icart reprend Nice Matin avec le fonds GXP Capital
DATE: 2014-02-14
URL: http://www.lemonde.fr/actualite-medias/article/2014/02/14/l-elu-jean-icart-reprend-nice-matin-avec-le-fonds-gxp-capital_4366815_3236.html
PRINCIPAL: 0
TEXT:
L'�lu Jean Icart reprend � Nice Matin � avec le fonds GXP Capital
Le Monde |
14.02.2014 � 15h47
| Par Paul Barelli
C'est le fonds d'investissement GXP Capital qui va s' associer � l'�lu Jean Icart pour devenir actionnaires majoritaires de Nice Matin, a annonc� la direction du groupe vendredi 14 f�vrier, en r�union du comit� d'entreprise.
GXP Capital est repr�sent� par Gilles P�rin, actif notamment dans l'h�tellerie haut de gamme et dans la construction navale.
Jean Icart, ancien conseiller municipal (div. droite) de Nice et conseiller g�n�ral des Alpes-Maritimes, a confirm� au Monde qu'il serait PDG en principe le 1er mars. Chef d'entreprise de 67 ans, il �tait opposant au premier magistrat actuel de Nice, Christian Estrosi ( UMP ) et proche de l'ancien maire, Jacques Peyrat, avant de finalement rompre avec ce dernier en d�cembre .
� Je vais apporter avec ce fonds GXP, indique Jean Icart , un soutien financier de 20 millions d'euros via Nice Morning, nouvel actionnaire majoritaire du Groupe Nice-Matin. � La mise en place de ce soutien est en cours de finalisation, avec une signature des accords pr�vue pour la fin du mois.
� Nous avons tr�s longuement travaill�, anim�s par le souci de pr�server ce fleuron de la PQR [presse quotidienne r�gionale] auquel le personnel est tr�s attach�, explique M. Icart. ''Nice Matin'' ne sera pas limit� � l'�dition papier que nous maintenons bien s�r. Le journal sera au centre d'une vaste structure de traitement de l'information du papier au num�rique avec la cr�ation d'un ''media center'', reflet de toutes les technologies les plus avanc�es. � Jean Icart n'a toutefois pas donn� plus de pr�cisions sur ce ��media center�� .
Ancien homme politique de droite, M. Icart assure que ��le journal ne doit en aucune fa�on �tre l'organe de propagande de qui que ce soit. Comprenne qui pourra� Notre m�tier c'est l'information. Afin d' �viter toute critique je mets fin � tous mes mandats. �
La premi�re mission de Jean Icart� sera de mettre en �uvre le futur plan de r�organisation et tout d'abord le plan social n�goci� avec les syndicats depuis plusieurs mois. Selon Groupe Hersant Media (GHM), ce dernier pr�voit la � suppression de 128 postes d'ici � la fin 2015 �, sur un total de 1 250 salari�s. Les syndicats, eux, parlent de � 146 d�parts volontaires �. Ce plan pourrait co�ter jusqu'� 15 millions d'euros. Il doit permettre un retour � l'�quilibre� des comptes d'une soci�t� qui a fini l'ann�e 2013 avec une perte d'exploitation de 6 millions d'euros.
Paul Barelli
