TITRE: Facebook : transsexuel, bi, androgyne� Pr�s de 50 nouveaux genres disponibles | Atlantico
DATE: 2014-02-14
URL: http://www.atlantico.fr/pepites/facebook-transsexuel-bi-androgyne%25E2%2580%25A6-pres-50-nouveaux-genres-disponibles-982131.html
PRINCIPAL: 173566
TEXT:
Facebook : transsexuel, bi, androgyne� Pr�s de 50 nouveaux genres disponibles
Depuis jeudi, les abonn�s anglophones du r�seau social ne sont plus uniquement des "hommes" ou des "femmes".
Nouveaut�s
RSS
�
En France, le d�bat sur la "th�orie du genre" fait rage... Mais cette question n'est pas aussi pol�mique � l'�tranger. Preuve en est avec la derni�re mise � jour de Facebook : en effet, depuis jeudi, les renseignements sur le r�seau concernant le sexe de l'utilisateur ne se limitent plus � "homme" ou "femme". Entre "transsexuel", "intersexuel", "bisexuel" ou encore "androgyne", ce sont pr�s de cinquante nouveaux genres qui sont d�sormais propos�s. Toutefois, ces nouvelles options ne sont pour le moment disponibles que pour les abonn�s anglophones.
Mais ce n'est pas le seul changement op�r� par la firme de Mark Zuckerberg : en plus du genre, les abonn�s ont maintenant la possibilit� de choisir le pronom personnel par lequel ils voudraient �tre interpell�s dans les posts. A c�t� des classiques "lui/il" et "elle", il y aura d�sormais "on" jug� plus neutre. "Si pour beaucoup ces changements n'ont pas beaucoup d'importance, pour ceux qui en ont souffert, c'est quelque chose d'important", �crit Facebook dans un post sur sa page Diversit�, qui affiche une photo du drapeau arc-en-ciel, �tendard de la communaut� homosexuelle et transgenre.
"Nous voyons ces �volutions comme un moyen suppl�mentaire pour faire de Facebook un lieu o� les gens peuvent exprimer librement leur identit�" est-il aussi �crit.�Facebook indique avoir collabor� avec des associations de d�fense des droits des lesbiennes, gays, bisexuels et transgenre pour cr�er ces nouvelles options qui peuvent �tre trouv�es dans la cat�gorie "autres".
�
Post by Facebook Diversity .
�
A noter que Facebook n'est pas le premier r�seau social � proposer des options alternatives au simple terme de "homme" et "femme". En effet, Google+ propose d�j� une troisi�me alternative "autre", un statut adopt� par 1 % des membres.
