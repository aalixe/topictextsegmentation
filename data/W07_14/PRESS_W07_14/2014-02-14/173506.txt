TITRE: Affaire NSA : Snowden aurait simplement vol� un mot de passe
DATE: 2014-02-14
URL: http://www.phonandroid.com/affaire-nsa-snowden-se-serait-empare-mot-de-passe-collegue.html
PRINCIPAL: 0
TEXT:
Affaire NSA : Snowden se serait juste empar� du mot de passe d�un coll�gue
Affaire NSA : Snowden se serait juste empar� du mot de passe d�un coll�gue
Par Kevin Jeffries le
13 f�vrier 2014
La protection de notre vie priv�e est l�un des th�mes qui pr�occupe le plus les Fran�ais. Malheureusement pour eux, les r�centes affaires n�inspirent pas confiance. Orange a �t� pirat�, la messagerie Yahoo a �t� victime d�une attaque ou encore les applications Instagram et Snapchat comportaient des failles de s�curit�. L�affaire ayant eu l��cho le plus important reste celle de la surveillance de masse r�alis�e par la NSA .
Le vol d�un mot de passe par Snowden � l�origine de l�affaire NSA ?
Vous connaissez sans doute Edward Snowden, l�homme � l�origine du scandale et ex employ� de la NSA. Alors que l�on imaginait des pratiques ultra-complexes afin d�obtenir des informations confidentielles, on apprend que Snowden aurait tout simplement obtenu le mot de passe de coll�gues. Il a travaill� � Hawa� peu avant la divulgation des documents et aurait convaincu entre 20 et 25 coll�gues de lui fournir son mot de passe.
Si Snowden a publiquement r�fut� cette information, expliquant ne s��tre jamais empar� de mots de passe de coll�gues, NBC News nous apprend aujourd�hui qu�il aurait tout simplement subtilis� le mot de passe d�un autre coll�gue de la NSA, cette fois � son insu. Ce mot de passe lui aurait offert un acc�s plus important aux informations secr�tes et classifi�es de l�agence. La NSA a par la suite limit� les acc�s de l�employ� tout en lui sugg�rant de quitter son poste. Le 10 janvier dernier, l�employ� a d�missionn�.
Quelques mots de passe auraient donc suffi � Edward Snowden pour faire �clater aux yeux du monde ce scandale de surveillance de masse. Scandale qui aura eu des victimes collat�rales dont cet employ� dup� mais qui aura donn� des id�es � de nombreux citoyens, organisant m�me une journ�e mondiale contre la surveillance de masse. On se joint � Orange qui souhaite mieux prot�ger vos donn�es pour vous conseiller de changer r�guli�rement vos mots de passe et, a fortiori, ne jamais le donner. Est-il possible de mettre fin � cette surveillance de masse selon vous�?
