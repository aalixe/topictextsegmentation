TITRE: Face � iOS et Android, Windows Phone monte lentement mais surement
DATE: 2014-02-14
URL: http://www.01net.com/editorial/614122/windows-phone-monte-lentement-mais-surement/
PRINCIPAL: 173764
TEXT:
agrandir la photo
Les chiffres d� IDC sur les parts de march� des OS mobiles au 4e trimestre 2013 devraient donner le sourire � Microsoft et Nokia. Windows Phone s'adjuge la troisi�me place avec une part de march� de 3%. Une goutte d�eau face aux 78,1% d�Android (Google) ou aux 17,6% d�iOS (Apple). Pourtant, pour la soci�t� de Redmond, c�est une v�ritable performance. Quant aux concurrents, ils prennent conscience qu'il y a un d�but de r�action.
Entre les quatri�mes trimestres 2012 et 2013, l�OS de Microsoft a progress� de 46,7% quand les deux leaders affichent des �volutions de 40,3% pour Google et 6,7% pour Apple. Sur l�ann�e 2013, la progression de Microsoft est de plus de 90%. Windows Phone r�ussit une perc�e sur un terrain ultra concurrentiel domin� par les deux entreprises les plus puissantes du monde.
Android n�est pas en reste, �videmment. Sa progression au 4e trimestre 2013 compar�e � la m�me p�riode de l�ann�e pr�c�dente est de 40,3% mais sa part de march� (PDM) le place tr�s largement en t�te avec 78,1%. IDC se permet une pointe d�humour�: ��Android termine l�ann�e comme il l�a commenc�: en leader�!��
Apple est loin de progresser autant : la firme de Cupertino affiche la croissance la plus faible avec 6,7% pour le 4e trimestre et 12,9% pour l�ann�e 2013. IDC est toutefois confiant dans les chances d�Apple de retrouver du dynamisme. Le cabinet estime qu�iOS va profiter du lancement probable d�un iPhone � �cran plus grand en 2014.
Quant au quatri�me du classement, Blackberry, il n�avance pas, bien au contraire. Il s'effondre m�me de jour en jour. Ses parts de march� se sont �croul�es de 77%. M�me avec son OS BB10 lanc� en janvier 2013 pour s�duire le grand public, Blackberry repr�sente moins de 1% de part de march�. Lors du 4e trimestre 2012, la firme canadienne flirtait avec les 3%.
Lire aussi :
