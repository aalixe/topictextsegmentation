TITRE: JO de Sotchi : Joubert annonce la fin de sa carrière - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-14
URL: http://tempsreel.nouvelobs.com/jo-de-sotchi/20140213.OBS6347/en-direct-jo-de-sotchi-pinturault-entre-en-piste.html
PRINCIPAL: 173003
TEXT:
� EspritGlisse (@EspritGlisse) 14 F�vrier 2014
15h35 - Sans faute pour Ana�s Bescond
La Fran�aise fait un sans faute au premier tir. Et sors 5e apr�s ce premier tir. Tout est encore possible !
Ana�s Bescond, rapide sur ses skis (5e temps provisoire au premier pas de tir) r�alise un sans faute au 1er tir. #espritbleu
� France Olympique (@FranceOlympique) 14 F�vrier 2014
Anais Bescond sort 5e � 7sec apr�s le 1er tir ! Tout est encore jouable mais il faudra bien tirer ! #live #Sochi2014
15h 30 -�Marie Dorin rate deux tirs...
Marie Dorin, premi�re des quatre Fran�aises � s'�lancer rate�deux balles, compromettant fortement ses chances de m�dailles.
15h15 - Six Fran�aises � suivre sur le 15 km de biathlon
Les concurrentes du 15 km Individuel de biathlon se sont �lanc�es.�Les Bleues seront-elles inspir�es par le sacre de Martin Fourcade jeudi ? Les Fran�aises en lice sont Ana�s Bescond, Marine Bolliet, Marie-Laure Brunet, Ana�s Chevalier, Marie Dorin-Habert et Sophie Boilley.
Ana�s Bescond, qui r�vait d'accrocher une premi�re m�daille, a �chou� mardi dans l'�preuve de la poursuite.
13h30 - �a rigole pour la Suisse
Apr�s le 2e sacre olympique de Dario Cologna un peu plus t�t, Sandro Viletta offre une nouvelle m�daille d'or aux Helv�tes. Qui vient s'ajouter � celles d�j� obtenues par Dominique Gisin lors de la descente dames et celle de Iouri Podlatchikov en half-pipe snowboard.
Pourtant, en ski alpin, les Suisses n'en menaient pas large avant le d�but des Jeux apr�s la retraite du champion Didier Cuche, l'�ge avanc� de Didier Defago, la petite forme de Beat Feuz et le retour discret de Carlo Janka, qui a r�alis� quelques bons r�sultats cet hiver, mais qui reste bien loin du niveau qui lui avait permis de remporter le classement g�n�ral Coupe du monde en 2010. Les Suisses pensaient donc tout miser sur Lara Gut, mais c'�tait sans compter sur Sandro Viletta et Dominique Gisin.
13h20 - Viletta champion olympique du super-combin�
Comme pr�vu, aucun des 6 meilleurs de la descente n'a pu se glisser sur le podium de ce super-combin� apr�s la manche de slalom. Jansrud, dernier � s'�lance apr�s son meilleur temps ce matin, termine 4e, au pied du podium.
Sandro Viletta devient donc champion olympique de super-combin�, devant Kostelic et Innerhofer. Une sacr�e surprise m�me si le Suisse a d�j� obtenu quelques bons top 10 en super-combin� en Coupe du monde (il compte �galement une victoire en Super-G). Ligety ne termine que 12e. Pinturault pourra lui nourrir de gros regrets au regard du r�sultat final.
Viletta ne sort pas de nulle part non plus: 5e du combin� des Mondiaux 2013, 4e � Wengen et 7e � Kitzbuehl cet hiver.
� Laurent Vergne (@LaurentVergne) 14 F�vrier 2014
13h15 - C'est presque fait pour Viletta
Il ne reste plus que des sp�cialistes de vitesse au d�part et compte de la d�gradation de la piste, Sandro Viletta n'est plus tr�s loin du titre olympique. On ne voit pas qui de Franz, Mayer, Kilde, Bank ou Jansrud peut venir le d�loger de la premi�re place.
13h10 - Kostelic derri�re Viletta !
Comme on vous l'annon�ait un peu plus t�t, le Croate n'est plus le monstre qu'il fut en slalom. Parti avec une tr�s grosse avance (71 centi�mes), il �choue � 34 centi�mes de Viletta � l'arriv�e. Il est 2e. Ce qui pourrait suffire pour accrocher une m�daille olympique, quand m�me.
Entre-temps, Aksel Lund Svindal et Carlo Janka, deux grands polyvalents et outsiders de la course, n'ont pas brill�. Ils sont 6e ex aequo.
13h10 - Innerhofer, la belle surprise
L'Italien, qui est plut�t un sp�cialiste des disciplines de vitesse, montre une nouvelle fois de belles aptitudes en slalom et s'empare de la 2e place de ce super-combin� derri�re Viletta.
13h05 - Bode Miller distanc� � son tour
L'Am�ricain, champion olympique du super-combin� � Vancouver, a pratiqu� un ski heurt� sur une piste qui se d�grade � vue d'oeil (la neige semble particuli�rement molle). Il n'est que 3e � l'arriv�e et ses chances de rester sur le podium sont minces.
13h - Viletta passe devant Zampa
Sandro Viletta, skieur assez polyvalent, vient de d�loger Adam Zamp de la premi�re place. Et assez largement. Le Suisse compte 1''14 d'avance sur le Slovaque qui effectue tout de m�me une superbe remont�e.
12h55 - Pour se consoler...
Rien de tel qu'une bonne vid�o LOLcat + curling pour se remettre de la grosse d�ception Pinturault.
12h50 - Ligety hors du coup
Il y a de quoi se mordre les doigts pour Pinturault qui �tait bien en avance sur les temps de Zampa, toujours en t�te apr�s 13 passages. Ligety, l'autre favori, a termin� � plus d'une seconde du Slovaque, plus de deux secondes sur la seule manche de slalom. L'Am�ricain, champion olympique 2006 de la discipline, n'est que 3e. Il y avait de la place pour "Pintu", beaucoup de place...
12h40 - Pinturault enfourche � son tour !
Grosse d�sillusion pour Alexis Pinturault qui a enfourch� sur une porte double au milieu du parcours alors qu'il �tait largement en t�te... Ca fait mal, tr�s mal.
Alexis Pinturault qui enfourche dans le Slalom du Super-Combin�. Mal�diction FR ? #live #Sochi2014 pic.twitter.com/MaHrPsuIKK
Sans parler de mal�diction, on peut dire que Pinturault a peut-�tre manqu� un peu de lucidit� sur une piste visiblement pi�geuse (d�j� de nombreux sorties de piste).
12h35 - Mermillod-Blondin, la chute
En avance aux deux tiers du parcours, le Fran�ais, bon sp�cialiste de slalom, tombe apr�s avoir enfourch� une porte. C'est fini pour lui. Le Slovaque Zampa est pour l'instant en t�te (50''11 sur la manche de slalom, 2'46'' sur l'ensemble des deux manches). Pinturault en piste dans 3 dossards.
12h30 - L'heure de Pinturault
On y est. Le slalom du super-combin� vient de d�marrer. Alexis Pinturault, 23e de la descente ce matin, s'�lancera en 8e position. Les 30 skieurs de la descente s'�lancent dans le sens inverse de leur position, du 30e au meilleur, donc.
12h05 - Cologna remet �a
Comme Martin Fourcade jeudi, le Suisse d�croche un 2e titre olympique � Sotchi sur le 15km classique. Il a devanc� deux Su�dois, Johan Olsson et Daniel Richardsson. Martin Johnsrud Sundby, leader de la Coupe du monde, ne termine que 13e. Les Fran�ais sont au-del� de la 20e place.
[Ski de fond 15km] Dario Cologna (SUI) champion olympique devant Olsson (SUE) et Richardsson (SUE) et #Sochi2014 pic.twitter.com/8MRFzRkSbZ
� JO 2014 Sotchi (@_JeuxOlympiques) 14 F�vrier 2014
11h30 - Des m�dailles de l'espace
Les m�dailles d'or gagn�es samedi seront incrust�es de morceaux de la m�t�orite Tcheliabinsk #anniversaire #Sochi2014 pic.twitter.com/Yw6ah4qe7Q
� JO 2014 Sotchi (@_JeuxOlympiques) 14 F�vrier 2014
11h15 - Panne de r�veil pour Petter Northug ?
Le double champion olympique et nonuple champion du monde (oui, quand m�me), en m�forme depuis plusieurs mois, ne s'est pas align� au d�part du 15km classique. La l�gende norv�gienne n'est pas le seul absent de marque au d�part puisqu'on note aussi le forfait du Su�dois Marcus Hellner ou du Fran�ais Maurice Manificat, entre autres.
11h - Cologna veut remettre �a
D�part en ce moment du 15km classique en ski de fond avec un grand favori, la star suisse du fond, Dario Cologna. Face � lui, l'armada norv�gienne emmen�e par les deux leaders de la Coupe du monde, Martin Johnsrud Sundby et Chris Andre Jespersen.
Jean-Marc Gaillard, 6e du skiathlon, sera la meilleure chance fran�aise. Il sera accompagn� de Cyril Miranda et d'Adrien Backsheider.
10h50 - Pinturault vous donne rendez-vous
23rd after the downhill...go for the slalom at 15h30 for sochi and 12h30 for french fans!!!
� Alexis Pinturault (@AlexPinturault) 14 F�vrier 2014
10h30 - La Su�de balaie tout sur son passage
Cinqui�me victoire en autant de rencontres pour les Su�dois dans la phase de poules du tournoi de curling masculin. Les Scandinaves ont battu la Chine 6-5, apr�s prolongations.
D'ailleurs, si vous pensez que faire glisser une pierre sur la glace, c'est simple, c'est parce que vous ne savez pas que la "p�tanque sur glace" exige strat�gie et concentration. Pr�sentation en images par ici .
10h15 - VladiBird, un jeu d�j� cul(te)
Apr�s le succ�s, certes �ph�m�re, de FlappyBird, les jeux qui s'en inspirent pullulent sur le web. Et forc�ment, avec les JO, Vladimir Poutine ne pouvait pas �chapper � sa version du jeu. Une version tr�s explicite dans ses d�cors. On vous laisse donc appr�cier VladiBird, par ici
10h10 - Et bonne Saint-Valentin bien s�r
� JO 2014 Sotchi (@_JeuxOlympiques) 14 F�vrier 2014
10h - Pour Ligety, "les favoris restent les m�mes"
L'Am�ricain, qui a r�alis� une meilleure descente que Pinturault et qui compte 51 centi�mes d'avance sur le Fran�ais avant le slalom, ne pense pas que la performance de Kostelic en fasse le grand favori pour la victoire finale.
"Deux secondes derri�re les purs descendeurs, c�est correct. Une seconde derri�re Ivica Kostelic et un poil d�avance sur Alexis, c�est jouable. Surtout sur cette piste o� il y aura beaucoup de changements de position. Je pense que les favoris restent les m�mes".
On a envie de croire Ligety car Kostelic (34 ans), qui conna�t des probl�mes de dos r�currents, a �t� en retrait dans les disciplines techniques, et notamment en slalom, cet hiver. Seul point potentiellement avantageux pour le Croate :� c'est son p�re Ante Kostelic, coach de l'�quipe croate, qui a trac� la manche de slalom, annonc�e particuli�rement tortueuse.
La start-list est en tout cas connue et voici les dossards des principaux concurrents : Mermillod-Blondin partira avec le 4, Pinturault 8, Ligety 13, Th�aux 14, Miller 19, Janka 22, Kostelic 24, Bank 29 et Jansrud (meilleur temps de la descente ce matin) 30.
On rappelle enfin un d�tail qui aura son importance : la d�gradation de la piste. Les premiers dossards sont toujours avantag�s en slalom, o� des trous se creusent au niveau des portes au fur et � mesure des passages des skieurs. Sans compter qu'avec le temps printanier � Sotchi, la neige, d'ordinaire verglac�e pour ce type de course, risque d'�tre assez molle. Un avantage peut-�tre d�cisif pour Pinturault, qui partira id�alement plac�, en huiti�me position.
9h15 - Deneriaz, la preuve par les chiffres
L'ancien champion olympique de descente � Turin, en 2006, rappelle que Pinturault, qui compte 1 seconde et 51 centi�mes de retard sur Kostelic avant le slalom du super-combin� olympique (12h30), a d�j� remont� beaucoup plus de temps sur le Croate, et sur les autres, lors du slalom du super-combin� de Kitzb�hel, qu'il avait remport� fin janvier.
� Antoine D�n�riaz (@deneriazantoine) 14 F�vrier 2014
8h45 -�Pinturault y croit encore
"Je n'ai pas fait une mauvaise course en partant de derri�re, et puis je devrais profiter d'une piste propre pour le slalom", a d�clar� le skieur de Courchevel.
Le Savoyard, 23e de la descente du super-combin�, va devoir cravacher dur pour arracher une place sur le podium.�Adrien Th�aux est 17e et�Mermillod-Blondin 27e.
8h10 -�Th�aux encourageant,�Mermillod-Blondin en difficult�>
Nos deux autres fran�ais se sont � leur tour �lanc�s sur la piste de�Rosa Khutor : Adrien Th�aux pointe � 1"76 du leader norv�gien Jansrud,�Mermillod-Blondin �2"99.
7h35 - Pinturaut d��oit mais pr�serve ses chances
Apr�s sa descente, le skieur fran�ais affiche un retard de 2"44 sur le leader. Il devra r�aliser une grosse performance sur l'�preuve du slalom pour concr�tiser ses r�ves de podium. Rendez-vous est pris � 12h30.
7h10 - Une descente ensoleill�e, en or pour Pinturault ?
Le Fran�ais devrait s'�lancer vers 7h30.�
Une belle lumi�re inonde le centre de ski alpin de Rosa Khutor #Sochi2014 pic.twitter.com/bsTFvbPkd8
� Yann Bertrand (@YannBertrand) 14 F�vrier 2014
7h - D'autres tricolores dans l'ombre de Pinturault...
Il n'y a pas que�le jeune prodige Savoyard sur la piste ce matin, deux autres Fran�ais sont bien d�cid�s � jouer les troubles-f�tes.�Adrien Th�aux, d�cevant lors de la descente, et Thomas Mermillod-Blondin.
6h45 -�Lizeroux : "Pinturault, c'est une b�te"
L'ancien vice-champion du monde du slalom et du super-combin�, Julien Lizeroux,�porte un regard particulier sur le ph�nom�ne du ski fran�ais, avec lequel il est tr�s proche en �quipe de France. Interview � lire ici .
6h20 -�Alexis Pinturault : "Me faire plaisir tout en essayant de skier vite"
Bien�que novice aux JO, le Fran�ais porte les principales chances de m�dailles tricolores en ski alpin. Son marathon olympique d�bute ce matin avec le super-combin�. Nous l'avons rencontr� il y a une semaine. Notre interview � lire ici .
6h10 - Alexis Pinturault "impatient"
Le�jeune skieur tricolore �quelques heures de son entr�e en lice :
La flamme olympique devant les sommets de Rosa Kuthor ! :) 1er course demain ! :) Impatient de debuter ces Jeux... http://t.co/PsFMqsN4Lj
� Alexis Pinturault (@AlexPinturault) 13 F�vrier 2014
6h - Pour ne rien rater de la journ�e, demandez le programme !
Le programme de vendredi avec l'entr�e en lice tr�s attendue d'Alexis Pinturault #Sochi2014 #JO2014 pic.twitter.com/yEvr9hc1Cl
