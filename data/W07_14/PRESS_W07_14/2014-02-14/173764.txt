TITRE: Nokia Normandy, un aper�u de son prix au Vietnam | Ubergizmo FR
DATE: 2014-02-14
URL: http://fr.ubergizmo.com/2014/02/nokia-normandy-apercu-prix-au-vietnam/
PRINCIPAL: 0
TEXT:
Par Liliane Nguyen on 13/02/2014
Selon les rumeurs, Nokia pourrait annoncer le Nokia Normandy/Nokia X � MWC 2014, ce qui est �tonnant, vu que Nokia est pr�s d��tre rachet� par Microsoft , et Microsoft �tant l�un des concurrents d�Android avec son propre syst�me d�exploitation Windows Phone. Ceci dit, on dirait qu�un d�taillant en ligne vietnamien a affich� le t�l�phone sur leur site web, ainsi que son prix, qui serait d�environ 110$ apr�s conversion.
Cependant, on dirait que le revendeur a simplement sp�cul�, car ils ont mis dans son statut que le t�l�phone n�a pas encore �t� annonc� officiellement, alors toutes les sp�cifications ci-dessus et son prix seraient simplement le r�sultat de devinette et pourrait �galement �tre bas� sur les rumeurs qu�on a entendu jusqu�� maintenant. Le t�l�phone a eu une grande attention, et pour ceux qui ne le savent pas, le Nokia Normandy serait une exp�rience de Nokia pour voir ce qu�ils pouvaient faire avec la plate-forme Android, et � l�origine, ce n��tait qu�un prototype et rien de plus. Mais quand le temps est venu, les rumeurs ont commenc� � dire que l�appareil pourrait devenir une r�alit�, et selon la derni�re rumeur, Nokia ne compte pas s�arr�ter l� .
Cet article a �t� categoris� dans Accueil > Rumeurs > T�l�phones Portables et a �t� "tagg�" avec MWC , mwc 2014 et nokia . La news a aper�ue sur wmpoweruser
Billet Suivant:
