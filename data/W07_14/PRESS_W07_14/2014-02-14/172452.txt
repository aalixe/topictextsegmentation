TITRE: L'euro monte face � un dollar min� par des chiffres d�cevants aux USA - DH.be
DATE: 2014-02-14
URL: http://www.dhnet.be/dernieres-depeches/afp/l-euro-monte-face-a-un-dollar-mine-par-des-chiffres-decevants-aux-usa-52fd41963570c16bb1cc58e3
PRINCIPAL: 0
TEXT:
Vous �tes ici: Accueil > Derni�res d�p�ches
L'euro monte face � un dollar min� par des chiffres d�cevants aux USA
Publi� le
13 f�vrier 2014 � 23h02
NEW YORK, 13 f�v 2014 (AFP)
L'euro rebondissait jeudi face � un dollar p�nalis� par des indicateurs am�ricains d�cevants sur le front de l'emploi et de la consommation.
Vers 22H00 GMT (23H00 � Paris), l'euro valait 1,3678 dollar, contre 1,3593 dollar mercredi � la m�me heure.
La monnaie unique europ�enne montait un peu face � la devise nippone, � 139,74 yens contre 139,33 yens mercredi.
Le dollar perdait du terrain face � la monnaie japonaise, � 102,15 yens contre 102,50 yens la veille.
"Le march� semble incertain et h�site entre le retour vers des valeurs s�res, comme l'illustre la bonne tenue du yen et du franc suisse, et l'app�tit pour le risque, � l'instar du march� des actions am�ricains qui a d�but� dans le rouge avant de se reprendre", relevait David Gilmore de Foreign Exchange Analytics.
Dans ce contexte, le billet vert p�tissait d'indicateurs d�cevants sur l'�conomie am�ricaine: les inscriptions aux allocations ch�mage aux �tats-Unis ont augment� plus qu'attendu la semaine derni�re, et surtout les ventes au d�tail ont recul� en janvier dans le pays.
Le recul de la monnaie am�ricaine est toutefois rest� limit� apr�s la publication de ces chiffres dans la mesure o� "une grande part de cet acc�s de faiblesse peut �tre attribu�e au mauvais temps" qui affecte le centre et le nord-est des Etats-Unis depuis le d�but de l'ann�e, commentait David Gilmore.
L'euro de son c�t� avait souffert mercredi apr�s la publication des mauvais chiffres sur la production industrielle et surtout apr�s l'allusion par Beno�t Coeur�, membre du directoire de la Banque centrale europ�enne (BCE), aux taux d'int�r�t n�gatifs comme outil de politique mon�taire valide.
"Les acteurs du march� s'interrogent sur ce que la Banque centrale europ�enne va d�cider lors de sa prochaine r�union en mars mais pour l'instant, les propos de M. Coeur� semblent avoir fait long feu", estimait David Gilmore.
Les investisseurs faisaient par ailleurs peu de cas jeudi de la confirmation du ralentissement de l'inflation en Allemagne en janvier, la hausse des prix s'affichant � 1,3% apr�s avoir atteint 1,4% en d�cembre.
Les chiffres de l'inflation en zone euro sont scrut�s par les cambistes car la Banque centrale europ�enne avait baiss� en novembre son taux directeur � 0,25% dans un contexte de net ralentissement de la hausse des prix � l'automne 2013.
"L'inflation faible est un probl�me av�r� en zone euro, un probl�me que la BCE semble toujours tr�s r�ticente � aborder", notait Craig Erlam, analyste chez Alpari UK.
Les cambistes attendent d�sormais la publication vendredi des chiffres sur la croissance au quatri�me trimestre de la zone, ainsi que de l'Allemagne, la premi�re puissance �conomique de la r�gion.
Vers 22H00 GMT, la livre britannique baissait face � l'euro, � 82,13 pence pour un euro - apr�s avoir atteint 81,76 pence, son cours le plus fort en trois semaines. La livre montait en revanche face au dollar, � 1,6653 dollar pour une livre, grimpant m�me vers 13H35 GMT � 1,6673 dollar, son plus haut niveau depuis d�but mai 2011.
La devise helv�tique montait face � l'euro, � 1,2221 franc suisse pour un euro, comme face au dollar, � 0,8934 franc suisse pour un dollar.
La devise chinoise a fini � 6,0639 yuans pour un dollar, contre 6,0627 yuans la veille.
L'once d'or a termin� � 1.296 dollars au fixing du soir - avant de monter vers 16H45 GMT � 1,300,40 dollars, un nouveau plus haut en trois mois - contre 1.289,50 dollars mercredi soir.
Cours de jeudi   Cours de mercredi
----------------------------------
22H00 GMT        22H00 GMT
EUR/USD        1,3678           1,3593    EUR/JPY        139,74           139,33    EUR/CHF        1,2221           1,2240    EUR/GBP        0,8213           0,8191    USD/JPY        102,15           102,50    USD/CHF        0,8934           0,9004    GBP/USD        1,6653           1,6593
� 2014 AFP. Tous droits de reproduction et de repr�sentation r�serv�s. Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par l'AFP. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, rediffus�e, traduite, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de l'AFP.
Derni�res d�p�ches
