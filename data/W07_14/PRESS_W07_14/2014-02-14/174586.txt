TITRE: MARSEILLE / JOSE ANIGO - Jos� Anigo pr�pare Saint-Etienne sans mettre de pression sur ses joueurs
DATE: 2014-02-14
URL: http://www.football365.fr/france/infos-clubs/marseille/jose-anigo-prepare-saint-etienne-sans-mettre-de-pression-sur-ses-joueurs-1103446.shtml
PRINCIPAL: 174583
TEXT:
MARSEILLE / JOSE ANIGO - Publi� le 14/02/2014 � 16h35 -  Mis � jour le : 14/02/2014 � 18h41
0
Jos� Anigo pr�pare Saint-Etienne sans mettre de pression sur ses joueurs
En conf�rence de presse, alors qu'un d�but de pol�mique a �clat� avec les journalistes sur place concernant leur acc�s aux entra�nements, Jos� Anigo se r�jouit du retour d'Andr� Ayew et de la forme actuelle de son secteur offensif. Le technicien phoc�en peut pr�parer sereinement le d�placement de dimanche � Saint-Etienne.
Jos� Anigo, comment vont vos joueurs ?
Ce matin (ndlr : vendredi), ils se sont entra�n�s et tout allait bien !
Quid d� Andr�-Pierre Gignac (ischios) ?
On le m�nage, �a devrait aller, �a devrait rouler.
On n�a pas vu Steve Mandanda ce matin � l�entra�nement ?
Ce matin, Steve avait de petits soucis physiques qu�on a trait�s. Il n�y a rien de grave. Cela arrive parfois, on m�nage les joueurs. Steve a �t� m�nag� ce matin.
Est-ce qu� Andr� Ayew est pr�t pour reprendre la comp�tition ?
Il commence � l��tre. Cette semaine, maintenant qu�on a des semaines compl�tes pour travailler, on peut vraiment travailler comme on le souhaite et � D�d� � se rapproche de son niveau qui nous int�resse.
� Je n�ai absolument pas matraqu� la communication �
Pol�mique avec les journalistes pr�sents sur la fermeture des entra�nements � la presse
Pourquoi avoir d�cid� de vous entra�ner � huis clos ce matin ?
Je n�ai pas d�cid� de huis clos ce (vendredi) matin�
Hier, quinze minutes pour l��chauffement�
On s�est entra�n� hier (jeudi) de la m�me mani�re, c��tait ouvert. Mais si vous voulez venir une deuxi�me fois dans la semaine, je n�ai aucun probl�me avec �a. Il n�y a aucun souci mais il y a des s�ances, notamment quand on se rapproche du match, o� on pr�pare des aspects tactiques. C�est pour cela qu�on n�ouvre que vingt minutes.
Habituellement, l�entra�nement tactique, c�est plut�t la veille du match. Mais depuis quelques temps, c�est l�avant-veille. On ne voit plus de football�
C�est pour cela que je vous dis que si vous voulez venir un deuxi�me jour dans la semaine, il n�y a aucun probl�me pour moi.
Ce sera le cas d�s la semaine prochaine ?
Vous verrez avec l�encadrement du club mais, moi, �a ne me g�ne pas !
L�avant-veille du match, vous proc�derez toujours de la m�me mani�re d�sormais ?
La veille du match, on pr�f�re all�ger, faire pratiquement rien pour avoir un maximum d��nergie le jour du match. On travaille plus l�avant-veille sur les aspects tactiques. Il n�y a rien d�extraordinaire dans ce qu�on fait. Mais, simplement, on essaye de garder pour nous ce qu�on met en place. C�est compl�tement idiot de le rendre lisible pour l�adversaire. Mais �a n�a rien � voir avec vous.
Dans une interview, vous expliquiez que vous aviez un probl�me d�image car vous n��tiez pas assez ouvert. Est-ce que cela ne participe pas � une �volution de l�OM o� vous vous fermez plus ?
Il y a deux sujets. Le premier, on peut penser que La Commanderie est un camp retranch� o� on ferme. Ce n�est pas le cas. Si vous voulez venir un jour de plus dans la semaine, �a ne me d�range pas que vous puissiez voir une s�ance, �a vous permettra de voir comment on bosse. Le deuxi�me, c�est par rapport � la communication. Tout le monde a pens� que j�avais matraqu� la communication. Je vais �tre tr�s clair : je n�ai absolument pas matraqu� la communication.
� Pas de pression �
Dans quel �tat d�esprit abordez-vous ce match face � Saint-Etienne ?
On ne s�est pas mis de pression, on sort d�un match face � Bastia (3-0, 24eme journ�e de Ligue 1) qu�on a plut�t bien ma�tris�. On est tout de suite pass� � autre chose. On est dans le match de Saint-Etienne depuis le d�but de la semaine, mais on n�en a pas fait une montagne. On prend �a comme un match o� il y a trois points comme enjeu, ni plus ni moins. On sait que Saint-Etienne est une �quipe de qualit�, on sait que c�est une �quipe qui est � la place qu�elle m�rite. C�est un match int�ressant pour tout le monde. Pour vous les m�dias mais aussi pour nous. On va jouer dans un beau stade avec un enjeu. Il y aura trois points pour eux ou pour nous, mais �a n�hypoth�que rien pour personne.
Que peut-on attendre de plus de cette association entre Payet et Valbuena ?
Il y a aussi Andr� Ayew qui arrive. On peut donc commencer � r�fl�chir � une articulation o� ces associations-l� peuvent �tre int�ressantes pour l�OM. Les animations offensives peuvent �tre tr�s int�ressantes. Si Mathieu Valbuena, Dimitri Payet et les deux autres, Gignac et Thauvin, arrivent � se trouver presque les yeux ferm�s, ce qu�on essaye de bosser tous les jours, je pense que �a sera b�n�fique pour nous.
R�dig� par R�daction Football365 Suivre @toto
Plus d'Actualit�s
