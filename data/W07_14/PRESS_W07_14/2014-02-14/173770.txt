TITRE: L'association Tizen rejointe par 15 nouveaux partenaires
DATE: 2014-02-14
URL: http://www.begeek.fr/lassociation-tizen-rejointe-par-15-nouveaux-partenaires-119439
PRINCIPAL: 173764
TEXT:
>
L�association Tizen rejointe par 15 nouveaux partenaires
L'association Tizen, dont le but est de favoriser le d�veloppement et la commercialisation de l'OS mobile, compte d�sormais 51 partenaires.
D�velopp� par Samsung et Intel et bas� sur MeeGo, le syst�me d�exploitation mobile open source Tizen vient de prouver une nouvelle fois l�int�r�t qui lui est port� par le monde high-tech. Lanc�e en novembre 2013, l�association Tizen (un programme pour acc�l�rer le d�veloppement et la commercialisation du syst�me) vient d��tre rejointe par 15 nouveaux acteurs.
Parmi eux, on retrouve notamment le constructeur ZTE, les op�rateurs SoftBank Mobile et Sprint ou encore le chinois Baidu. La liste compl�te est disponible ici .
D�sormais au nombre de 51, ces acteurs devront accompagner l��volution de l�OS dont l�avenir est toujours relativement flou. Dans les 36 premiers on retrouve des soci�t�s aux activit�s vari�es, comme eBay, McAfee, Panasonic ou encore Konami.
Le Mobile World Congress (MWC) qui d�butera le 24 f�vrier prochain devrait �tre l�occasion pour l�association et ses partenaires de r�v�ler des informations sur l�OS et sur ses applications potentielles (smartphones, tablettes, voitures, t�l�visions�etc.).
Plus d'actu sur Tizen
