TITRE: Sport national | Martin Fourcade : a tsar is born - Le Bien Public
DATE: 2014-02-14
URL: http://www.bienpublic.com/sport-national/2014/02/14/a-tsar-is-born
PRINCIPAL: 172272
TEXT:
Martin Fourcade : a tsar is born
Omnisports - JO 2014 Martin Fourcade : a tsar is born
Notez cet article :
le 14/02/2014      à 05:01 | de Sotchi, Bertrand JOLIOT
Martin Fourcade a su accélérer dans le dernier tour pour aller chercher un deuxième titre olympique en quatre jours.  Photo AFP
Trois jours après la poursuite, Martin Fourcade a complété sa collection dorée avec l�??individuel. Le Pyrénéen semble désormais dans une autre dimension. Seul, sinon inaccessible.
Partager
Envoyer à un ami
Dans la nuit de Sotchi, une nouvelle étoile brille de mille feux. Tellement fort, tellement plus fort que les autres qui tentent d�??illuminer le ciel russe pour cette grande quinzaine olympique semblent ne plus exister. �?clipsées. Envolées. Disparues. Cette étoile que Stéphane Bouthiaux avait remarquée bien avant les autres est aujourd�??hui à son firmament et semble partie pour y rester quelque temps. Un météore venu des Pyrénées, parti se polir dans l�??ombre et l�??anonymat du laboratoire jurassien du pôle France de Prémanon, pour resplendir comme jamais depuis trois ans maintenant. Pour faire pâlir ses adversaires, aussi�?�
Hier, cet astre a pris une autre dimension pour se positionner comme le potentiel homme de ces Jeux. Et pas que dans le clan français Un homme qui peut se permettre de lâcher une balle sur une épreuve où la moindre perte peut coûter cher, très cher. Un athlète qui flirte avec la chute mais qui, à l�??image de son enchaînement sprint-poursuite, trouve toujours les ressources pour se rétablir. Un homme qui aime autant le show des épreuves courtes et rapides que le froid de cette longue partie d�??échecs qu�??est l�??individuel. « Une course où il faut être patient, un dénouement sur le dernier tir où il faut arriver là sans se pénaliser, c�??est très exigeant au niveau physique et mental », souligne Siegfried Mazet, l�??entraîneur du tir. Un homme qui comme tout le monde a dû attendre l�??arrivée du dernier concurrent pour être sûr de son fait.
Hier, en gagnant une épreuve que tout bon biathlète complet se doit de remporter, Martin Fourcade est entré dans une autre dimension. Celle d�??un champion hors-normes, qui peut regarder les yeux dans les yeux Jean-Claude Killy et Henri Oreiller, eux aussi doubles champions olympiques, respectivement à Grenoble (1968) et Saint-Moritz (1948). Et qui peut les laisser, comme il en a l�??habitude avec ses adversaires, sur place.
Dans la cour des très grands
Car Martin le fait comprendre : sa soif d�??or n�??est pas étanchée. « Il reste trois courses, trois belles opportunités de ramener des médailles ». Ce qu�??il peut réaliser est d�??autant plus grand qu�??entre la mass-start, ce « tsar wars » à la russe où ses adversaires norvégiens voudront se réveiller, et les deux relais, où il emmènera les plus fous espoirs de victoires par équipe, Martin Fourcade a l�??occasion de montrer qu�??il est unique. Et altruiste. Martin Fourcade, tout simplement�?�
Sur le même sujet
