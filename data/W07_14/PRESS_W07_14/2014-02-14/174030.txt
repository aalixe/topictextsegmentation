TITRE: St-Valentin : faire l�amour 2 fois par semaine est bon pour le c�ur
DATE: 2014-02-14
URL: http://www.reponseatout.com/pratique/sante-bien-etre/st-valentin-faire-amour-2-fois-semaine-bon-pour-cur-a1012199
PRINCIPAL: 0
TEXT:
St-Valentin : faire l�amour 2 fois par semaine est bon pour le c�ur
Le 14/02/2014 � 14:37:54
Vues : 8774 fois 1 REACTION
Faire l'amour deux fois par semaine r�duirait les risques de maladie cardiovasculaire | � ThinkStock
En ce jour de St-Valentin, la F�d�ration fran�aise de Cardiologie rappelle les m�faits de la solitude sur le c�ur.
La solitude est mauvaise pour le c�ur. A l�occasion de la Saint-Valentin, la F�d�ration fran�aise de Cardiologie (FFC) rappelle que plusieurs �tudes r�centes ont prouv� que l�isolement et la privation de relations sociales avaient un impact n�gatif sur la sant�, en particulier cardiovasculaire.
D�apr�s les sp�cialistes, le risque de mortalit� d� � la solitude serait comparable � celui des fumeurs. Le manque de relations sociales r�duirait �galement les d�fenses immunitaires et augmenterait les inflammations.
Faire l�amour muscle le c�ur
En ce 14 f�vrier, la FFC indique que l�activit� sexuelle est, du point de vue cardiovasculaire ( et pas pour maigrir ), une activit� physique comme une autre. Un rapport d'intensit� mod�r�e � sollicite le muscle cardiaque, permet d'activer la circulation art�rielle et veineuse, de muscler le c�ur et de lib�rer des hormones de bien-�tre relaxantes. � D�apr�s les cardiologues, il suffirait de deux rapports par semaine pour r�duire de 45 % le risque de d�velopper une maladie cardiovasculaire .
Enfin, sans parler de couple et d�intimit�, la FFC explique qu�il est bon, en r�gle g�n�rale, de se tourner vers les autres. Claire Mounier Vehier, premi�re vice-pr�sidente de la FFC explique que � le partage, la volont� d�am�liorer le bien-�tre d�autrui comme le b�n�volat, l�empathie� am�liorent nettement la sant� cardiaque. Ce sont des facteurs d��panouissement, il est bon de se sentir utile. �
