TITRE: SKI DE FOND: Justyna Kowalczyk atteint son objectif -  News Sports: Sports d'hiver - lematin.ch
DATE: 2014-02-14
URL: http://www.lematin.ch/sports/sports-d-hiver/justyna-kowalczyk-atteint-objectif/story/24611396
PRINCIPAL: 172213
TEXT:
Justyna Kowalczyk atteint son objectif
SKI DE FOND
�
La Polonaise avait fait du 10 km classique son principal objectif dans les JO de Sotchi. Elle n'a pas flanch�, s'adjugeant le deuxi�me titre olympique de sa carri�re dans des conditions printani�res.
Mis � jour le 13.02.2014
Image: Keystone
R�sultats
Laura. Dames. 10 km classique: 1. Kowalczyk (Pol) 28'17''8. 2. Kalla (Su) � 18''4. 3. Johaug (No) � 28''3. 4. Saarinen (Fin) � 30''3. 5. Bj�rgen (NO � 33''4. 6. B�hler (All) � 46''5. 7. Zhukova (Rus) � 57''7. 8. Niskanen (Fin) � 58''9. 9. Weng (No) � 1'10''4. 10. Lahteenm�ki (Fin) � 1'18''2.
Mots-cl�s
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Justyna Kowalczyk, championne olympique 2010 du 30 km classique a m�me survol� les d�bats jeudi dans sa discipline de pr�dilection, devan�ant sa dauphine su�doise Charlotte Kalla de 18''4 !
Elle n'avait pourtant pas abord� ces Jeux dans les meilleures dispositions, souffrant d'une triple micro-fracture au pied gauche, mais s'�tait pleinement rassur�e en terminant 6e du skiathlon samedi dernier.
Kustyna Kowalczyk (30 ans) a figur� en t�te � tous les pointages interm�diaires, comptant 23''8 d'avance sur Charlotte Kalla apr�s 9,4 km avant de rel�cher son effort. Elle a m�me fait craquer la reine Marit Bj�rgen: encore 2e apr�s 8 km - mais � 19''9 de sa rivale polonaise -, la Norv�gienne aux quatre m�dailles d'or olympiques "explosait" dans les derniers hectom�tres pour �chouer au 5e rang.
Marit Bj�rgen terminait � 5''1 de la m�daill�e de bronze, sa compatriote Therese Johaug. Cette derni�re a su s'arracher dans la derni�re ligne droite pour souffler la 3e place � la Finlandaise Aino Kaisa Saarinen pour 2''0, et s'offrir ainsi une premi�re m�daille olympique en individuel. (SI/Le Matin)
Cr��: 13.02.2014, 12h50
Votre email a �t� envoy�.
Publier un nouveau commentaire
Nous vous invitons ici � donner votre point de vue, vos informations, vos arguments. Nous vous prions d�utiliser votre nom complet, la discussion est plus authentique ainsi. Vous pouvez vous connecter via Facebook ou cr�er un compte utilisateur, selon votre choix. Les fausses identit�s seront bannies. Nous refusons les messages haineux, diffamatoires, racistes ou x�nophobes, les menaces, incitations � la violence ou autres injures. Merci de garder un ton respectueux et de penser que de nombreuses personnes vous lisent.
La r�daction
Merci de votre participation, votre commentaire sera publi� dans les meilleurs d�lais.
Merci pour votre contribution.
J'ai lu et j'accepte la Charte des commentaires.
Caract�res restants:
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
S'il vous pla�t entrer un nom valide.
S'il vous pla�t indiquez le lieu g�ographique.
Ce num�ro de t�l�phone n'est pas valable.
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
Signup Fermer
Vous devez lire et accepter la Charte de commentaires avant de poursuivre.
Nous sommes heureux que vous voulez nous donner vos commentaires. S'il vous pla�t noter les r�gles suivantes � l'avance: La r�daction se r�serve le droit de ne pas publier des commentaires. Ceci s'applique en g�n�ral, mais surtout pour les propos diffamatoires, racistes, hors de propos, hors-sujet des commentaires, ou ceux en langues �trang�res ou dialecte. Commentaires des noms de fantaisie, ou avec des noms manifestement fausses ne sont pas publi�s non plus. Plus les d�cisions de la r�daction n'est ni responsable d�pos�e, ni en dehors de la correspondance. Renseignements t�l�phoniques ne seront pas fournis. L'�diteur se r�serve le droit �galement � r�duire les commentaires des lecteurs. S'il vous pla�t noter que votre commentaire aussi sur Google et autres moteurs de recherche peuvent �tre trouv�s et que les �diteurs ne peuvent rien et est de supprimer un commentaire une fois �mis dans l'index des moteurs de recherche.
�auf Facebook publizieren�
Veuilliez attendre s'il vous pla�t
Soumettre Commentaire
Soumettre Commentaire
No connection to facebook possible. Please try again. There was a problem while transmitting your comment. Please try again.
