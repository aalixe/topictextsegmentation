TITRE: Football - Angleterre - Mourinho s'en prend � Wenger | francetv sport
DATE: 2014-02-14
URL: http://www.francetvsport.fr/wenger-specialiste-de-l-echec-selon-mo-206641
PRINCIPAL: 174401
TEXT:
Football - Angleterre - "Wenger, sp�cialiste de l'�chec" selon Mourinho
Publi� le 14/02/2014 | 16:18, mis � jour le 14/02/2014 | 16:36
L'entra�neur de Chelsea, Jos� Mourinho (AFP - SEBASTIEN BOZON)
Jos� Mourinho a tent� aujourd'hui en conf�rence de presse de destabiliser son concurrent londonien, Arsenal, et plus particuli�rement son manager Ars�ne Wenger. Une r�ponse sous forme de pique apr�s la premi�re saillie de l'Alsacien.
"C'est un sp�cialiste de l'�chec, pas moi, a l�ch� Mourinho en conf�rence de presse. S'il a raison et que j'ai peur d'�chouer, c'est parce que �a m'est rarement arriv�. Peut-�tre qu'il a raison ? La r�alit� est �c'est que lui est un sp�cialiste car huit ans sans le moindre troph�e, �a c'est un �chec. Si cela m'arrivait � Chelsea, je quitterais Londres et je ne reviendrais pas."�Quand on demande ensuite au manager portugais�de r�agir au pronostic de Wenger, qui voit Chelsea favori pour le titre, il nie cat�goriquement : "Manchester United est champion en titre. Ils le sont math�matiquement jusqu'� ce qu'ils le perdent. Et s'ils ne conservent pas leur couronne - ce qui sera vraisemblablement le cas - c'est United qui perd le titre, personne d'autre."
En conf�rence de presse, Ars�ne Wenger avait mani� l'ironie en visant Jos�Mourinho. "Si l'on n'est pas dans la course au titre, on ne peut pas la perdre, c'est aussi simple que �a", a d�clar� l'entra�neur fran�ais des Gunners.
