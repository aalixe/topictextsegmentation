TITRE: Syrie: Valerie Amos d�nonce l'inaction - L'Express
DATE: 2014-02-14
URL: http://www.lexpress.fr/actualite/monde/proche-moyen-orient/syrie-valerie-amos-denonce-l-inaction_1323954.html
PRINCIPAL: 0
TEXT:
Voter (0)
� � � �
1400 civils assi�g�s depuis juin 2012 patr les forces gouvernement dans la vieille ville de Homs ont �t� �vacu�s ces derniers jours.
Reuters/Yazan Homsy
La patronne des op�rations humanitaires de l'ONU Valerie Amos a estim� jeudi que l' �vacuation de Homs en Syrie n'�tait pas un progr�s suffisant et a demand� au Conseil de s�curit� de donner aux humanitaires "les moyens de faire leur travail".�
L'�vacuation de pr�s de 1400 civils de Homs est certes "un succ�s �tant donn� les circonstances extr�mement difficiles" mais il reste encore 250.000 personnes bloqu�es par les combats en Syrie sans aucun acc�s aux secours, a-t-elle soulign�.�
Elle a �galement indiqu� � la presse que l'ONU avait des "assurances verbales" des bellig�rants mais toujours pas de confirmation �crite que la tr�ve � Homs serait prolong�e. "Sans assurances �crites nous ne pouvons pas continuer", a-t-elle ajout�.�
Pessimisme
Se d�clarant "non seulement pessimiste mais tr�s frustr�e", elle a dit avoir averti le Conseil que "les progr�s sont extr�mement limit�s et douloureusement lents" pour porter secours aux civils syriens.�
Elle a dit avoir demand� aux 15 membres du Conseil "d'user de leur influence sur les parties (au conflit) pour qu'ils respectent des pauses (humanitaires), facilitent la fourniture d'aide (..) et �vitent � nos �quipes d'�tre prises pour cibles quand elles livrent cette aide". "La guerre elle-m�me a des r�gles", a-t-elle rappel�. "Il faut que nous ayons les moyens de faire notre travail sur le front humanitaire", a martel� Mme Amos, jugeant "inacceptable" la d�t�rioration de la situation sur le terrain.�
En attendant une r�solution humanitaire
Interrog� sur le projet de r�solution humanitaire n�goci� au Conseil, elle a jug� "important qu'il y ait des leviers pour s'assurer de (son) application".�
Un projet occidental de r�solution sur la table du Conseil pr�voit la possibilit� de sanctions ult�rieures si les bellig�rants bloquent l'aide humanitaire. Mais la Russie est farouchement oppos�e � cette mention et a d�pos� un contre-projet de texte qui met l'accent sur la mont�e du terrorisme en Syrie, un des leitmotiv du r�gime de Bachar el-Assad.�
430 hommes arr�t�s � leur sortie de Homs, 181 lib�r�s
L'ONU a indiqu� jeudi que 430 hommes �g�s de 15 � 55 ans avaient �t� arr�t�s pour �tre interrog�s � leur sortie de la vieille ville de Homs, et 181 ont ensuite �t� lib�r�s.�
Parmi ces derniers, 111 ont �t� rel�ch�s mercredi et 70 jeudi. "Nous pensons qu'il reste environ 2.000 personnes dans la vieille ville de Homs", a pr�cis� le porte-parole des Nations unies Martin Nesirky.�
Dans la r�gion de damas, nourriture et m�dicaments ont par ailleurs �t� livr�s � 4000 personnes � Bloudan, � 45 km de la capitale, a ajout� Martin Nesirky. Bloudan est difficile d'acc�s en raison des combats et il a fallu quatre heures, et 20 barrages, au convoi de l'ONU pour franchir les 15 derniers km, a-t-il soulign�.�
En revanche, dans le camp palestinien de Yarmouk , pr�s de Damas, une distribution d'aide pr�vue par l'UNRWA (agence de l'ONU charg�e des r�fugi�s palestiniens) n'a pas pu avoir lieu. �
�
