TITRE: VIDEO. Il filme Nancy avec un drone, le tribunal le convoque - L'Express
DATE: 2014-02-14
URL: http://www.lexpress.fr/actualite/societe/fait-divers/il-filme-nancy-avec-un-drone-le-tribunal-le-convoque_1323986.html
PRINCIPAL: 172988
TEXT:
VIDEO. Il filme Nancy avec un drone, le tribunal le convoque
Par LEXPRESS.fr, publi� le
14/02/2014 �  09:52
Un lyc�en de 18 ans est inqui�t� pour "mise en danger de la vie" d'autrui apr�s avoir utilis� un drone pour filmer des lieux publics sans autorisation.�
Voter (1)
� � � �
Ne peut pas filmer un lieu public avec un drone qui le veut. L'usage des appareils est r�glement� en France.
AFP
Nans Thomas , tout juste 18 ans, devra se pr�senter au tribunal correctionnel pour un motif rarissime en France. Ce lyc�en est en effet inqui�t� pour "mise en danger de la vie d'autrui" parce qu'il a utilis� un drone pour filmer la ville de Nancy, rapporte L'Est R�publicain . �
L'usage des appareils est en effet r�glement�. La Direction r�gionale de l'aviation civile l'a d'abord rappel� dans un courrier au lyc�en, qui a mont� une entreprise de location de cam�ras et de drones. Pour avoir le droit de filmer Nancy avec son drone, il aurait d� poss�der une habilitation de l'aviation civile et une "d�rogation de survol" de la pr�fecture.�
�
Mais lundi 10 f�vrier, Nans Thomas est convoqu� par la gendarmerie et en ressort avec une convocation devant le tribunal correctionnel. "Je ne savais pas qu'il fallait des autorisations", raconte-t-il � l'Est R�publicain. On ne m'a d'ailleurs donn� aucune indication quand j'ai achet� le drone sur Internet."�
"Si l'appareil se crashe dans une zone dens�ment peupl�e, les cons�quences peuvent �tre dramatiques", explique le parquet de Nancy pour justifier le cadre impos� aux drones. Autre question pos�e par les appareils: le respect de la vie priv�e.�
Sur le m�me sujet
