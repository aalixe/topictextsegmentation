TITRE: Dwayne Johnson : The Rock h�ros d'une com�die sur le football pour HBO
DATE: 2014-02-14
URL: http://www.purebreak.com/news/dwayne-johnson-the-rock-heros-d-une-comedie-sur-le-football-pour-hbo/71046
PRINCIPAL: 173789
TEXT:
Dwayne Johnson : The Rock h�ros d'une com�die sur le football pour HBO
6
Dwayne Johnson : The Rock dans la peau d'un footballeur retrait� pour HBO
Dwayne Johnson, aka The Rock, devient star de s�rie. Le site Deadline rapporte en effet qu'HBO a command� la com�die Ballers, pour laquelle l'ancien catcheur sera acteur principal mais aussi producteur. Ballers vient s'ajouter � la longue liste de projets de la cha�ne c�bl�e am�ricaine.
Le foot US vu par HBO et Dwayne Johnson
HBO a �t� convaincu par le pilote de Ballers, r�alis� par Peter Berg (Hancock, Battleship ...) et produit par les co-stars de No Pain, No Gain , Mark Wahlberg et Dwayne Johnson. La cha�ne c�bl�e a donc d�cid� de commander une saison compl�te de cette s�rie consacr�e � des stars du foot US d'hier et d'aujourd'hui, dans laquelle Dwayne Johnson, aka The Rock , interpr�te l'un des sportifs retrait�s. Egalement au casting : Omar Benson Miller vu dans Les Experts � Miami, John David Washington (fils de Denzel Washington ) mais aussi Rob Corddry.
Les autres projets d'HBO pour 2014
Ballers n'est pas la seule s�rie sur laquelle HBO mise pour 2014. La cha�ne, qui diffusera d�s le 6 avril prochain la tr�s attendue saison 4 de Game of Thrones , a �galement command� The Brink, une com�die sombre avec Jack Black et Tim Robbins sur trois hommes qui tentent de sauver le monde d'une Troisi�me Guerre Mondiale. Mais aussi la s�rie The Leftovers cr��e par Damon Lindelof (Lost), avec Justin Theroux au casting. Egalement en d�veloppement : Open de Ryan Murphy ou encore le remake am�ricain de la s�rie britannique Utopia, produit par David Fincher .
Votre r�action
