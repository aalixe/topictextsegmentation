TITRE: D�ficitaire, la SNCF veut �conomiser 350 millions d'euros en 2014 - Capital.fr
DATE: 2014-02-14
URL: http://www.capital.fr/bourse/actualites/deficitaire-la-sncf-veut-economiser-350-millions-d-euros-en-2014-910681
PRINCIPAL: 172345
TEXT:
D�ficitaire, la SNCF veut �conomiser 350 millions...
D�ficitaire, la SNCF veut �conomiser 350 millions d'euros en 2014
Source : Reuters
Tweet
La SNCF, d�ficitaire en 2013, vise 350 millions d'�conomies cette ann�e afin d'am�liorer sa marge op�rationnelle, tout en am�liorant de plus de 2% son chiffre d'affaires. /Photo d'archives/REUTERS/Stphane Mah�
La SNCF, d�ficitaire en 2013, vise 350 millions d'�conomies cette ann�e afin d'am�liorer sa marge op�rationnelle, tout en am�liorant de plus de 2% son chiffre d'affaires.
La compagnie publique ferroviaire fran�aise a fait �tat jeudi d'une perte nette part du groupe de 180 millions d'euros l'ann�e derni�re contre un b�n�fice de 376 millions un an plus t�t, sous l'effet de la d�pr�ciation comptable des rames TGV pour 1,4 milliard d'euros.
L'op�rateur ferroviaire a am�lior� son chiffre d'affaires annuel de 0,5% � p�rim�tre et change constants, � 32,232 milliards d'euros, malgr� une baisse de 1,4% pour SNCF Voyages (activit� TGV).
La SNCF a l�g�rement r�duit sa dette financi�re, de 131 millions, pour la ramener � 7,391 milliards d'euros, et a d�gag� un cash-flow libre record de 464 millions.
En 2014, la SNCF table sur une reprise �conomique mod�r�e, mais avec des "contraintes fortes" comme l'augmentation du taux de TVA- de 7% � 10%- et des p�ages ferroviaires. Les volumes de transport de marchandises devraient stagner, tandis que le trafic de passagers est attendu en recul de 0,3% pour SNCF Voyages.
Pour autant, la SNCF vise une croissance de son chiffre d'affaires sup�rieure � 2%, un cash-flow libre positif pour la quatri�me ann�e d'affil�e et un niveau de dette contenu.
Dans ces conditions, l'op�rateur pr�cise dans un communiqu� qu'il entend poursuivre "sans rel�che" ses plans de performance, avec 350 millions d'euros d'�conomies, dont pr�s des deux tiers r�alis�s sur les frais de structure.
Par ailleurs, le ministre des Transports Fr�d�ric Cuvillier a pr�sent� jeudi une premi�re s�rie de mesures destin�es � relancer le fret ferroviaire, comme l'exp�rimentation de "clusters" (groupement d'entreprises) pour s'adapter aux besoins de l'�conomie locale et l'int�gration d'objectifs de d�veloppement du fret ferroviaire dans les projets strat�giques des grands ports maritimes qui seront red�finis cette ann�e.
Cyril Altmeyer, �dit� par Dominique Rodriguez
� 2014 Reuters - Tous droits de reproduction r�serv�s par Reuters.
