TITRE: ASSE | ASSE : Galtier met la pression sur l'OM !
DATE: 2014-02-14
URL: http://www.le10sport.com/football/ligue1/asse/asse-galtier-met-la-pression-sur-l-om-136035
PRINCIPAL: 174895
TEXT:
�
��C�EST MARSEILLE QUI A INVESTI BEAUCOUP D�ARGENT��
��ASSE - OM est toujours une affiche. Ce n�est jamais un match banal. C�est un match important pour les deux �quipes, compte tenu du classement actuel. Sur le papier, l�OM est favori. C�est Marseille qui a investi beaucoup d�argent sur le march� des transferts.�Battre l�OM serait une grande performance. Nous devons �tre au maximum de notre forme et de notre d�termination.�Nous avons montr� que nous savions r�pondre pr�sents lors de ces rendez-vous. Nous ne devrons jouer sans aucun complexe. Concernant cette fin de saison, il y a une opportunit� qui se pr�sente. Il faut y aller � fond.��
�
��UN MATCH IMPORTANT MAIS PAS CAPITAL��
M�me son de cloche du c�t� du milieu st�phanois Fabien Lemoine. ��Nous jouons contre un concurrent direct. C'est un match important mais pas capital. Le calendrier qui nous attend est difficile. D'ici quelques matches, on en saura plus sur nos ambitions. On d�fend collectivement, tout le monde fait les efforts. Cela nous permet de mieux maitriser nos matches.�L�OM est une grosse �curie. Ils ont un potentiel offensif impressionnant. Cette saison, le danger offensif vient de partout. On est plusieurs � pouvoir marquer. L�ASSE et l�OM sont deux clubs historiques en France. Ce sera un gros match. �
