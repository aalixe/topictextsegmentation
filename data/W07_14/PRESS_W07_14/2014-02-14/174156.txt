TITRE: Victoires de la musique : Stromae favori - Musique - 14/02/2014 - leParisien.fr
DATE: 2014-02-14
URL: http://www.leparisien.fr/musique/victoires-de-la-musique-stromae-favori-14-02-2014-3591127.php
PRINCIPAL: 174104
TEXT:
EN IMAGES. Les 29es Victoires de la musique
�Derri�re, Etienne Daho Christophe Ma� , Ma�tre Gims, le groupe 1995, et Woodkid cumulent deux nominations dans la liste que nous publions ci-dessous.
Seul manquera � l'appel Daft Punk . Le duo casqu�, qui a triomph� en janvier aux Grammy Awards, a refus� d'�tre nomm�. L'association des Victoires a propos� aux deux hommes d'�tre les invit�s d'honneur de la soir�e, mais leur venue est plus qu'improbable. Avec ou sans robots, les Victoires promettent du spectacle.
A suivre en direct � partir de 20h45 sur le Parisien.fr : les moments forts et les coulisses de la soir�e
LA LISTE DES NOMIN�S
Artiste interpr�te masculin : Etienne Daho, Christophe Ma�, Stromae
VIDEO. Les confidences d'Etienne Daho
VIDEO. En studio avec Christophe Ma�
Artiste interpr�te f�minine : Lilly Wood and The Prick, Vanessa Paradis, Zaz.
VIDEO. Lilly Wood and the Prick en live au Parisien : �Let's not pretend�
VIDEO. Vanessa Paradis : le clip de �Love Song�
VIDEO. Zaz en live au Parisien
Album r�v�lation : Cats on Trees � Cats on Trees �, Hollysiz � My Name is �, La Femme � Psycho tropical Berlin �.
Groupe ou artiste r�v�lation sc�ne : 1995, Christine and The Queens, Albin de la Simone, Woodkid.
Album de chansons : Etienne Daho � les Chansons de l�innocence retrouv�e �, Julien Dor� � L�ve �, Stromae � Racine carr�e �.
Album rock : Phoenix � Bankrupt! �, Indochine � Black City Parade �, D�troit (Bertrand Cantat et Pascal Humbert) � Horizons �.
Album de musiques urbaines : Grand Corps Malade � Funambule �, 1995 � Paris Sud Minute �, Ma�tre Gims � Subliminal �.
Album de musiques du monde : Winston McAnuff & Fixi � A New Day �, Ibrahim Maalouf � Illusions �, Mayra Andrade � Lovely Difficult �.
Album de musiques �lectroniques : Gesaffelstein � Aleph �, Kavinsky � Outrun �, Elephanz � Time for a Change �.
Chanson originale : Johnny Hallyday � 20 ans �, Stromae � Formidable �, Ma�tre Gims � J�me tire �, Stromae � Papaoutai �.
Spectacle, tourn�e, concert : C2C, M, Christophe Ma�.
Clip : Stromae � Formidable �, Woodkid � I Love You �, Stromae � Papaoutai �.
VIDEO. Stromae : les coulisses de son clip "Formidable"
DVD musical : Shaka Ponk � Geeks on Stage �, Laurent Voulzy � Lys and Love Tour �, Eddy Mitchell � Ma derni�re s�ance �.
LeParisien.fr
