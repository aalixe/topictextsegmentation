TITRE: Municipales : un candidat FN �vinc� � cause d'un tatouage nazi
DATE: 2014-02-14
URL: http://www.francetvinfo.fr/elections/municipales/municipales-un-candidat-fn-evince-a-cause-d-un-tatouage-nazi_530383.html
PRINCIPAL: 174981
TEXT:
Tweeter
Municipales : un candidat FN �vinc� � cause d'un tatouage nazi
Matthieu Colombier, t�te de liste � Ch�teauroux, a fini par �carter son colistier, vendredi, alors qu'il l'avait soutenu la veille.
Par Francetv info avec AFP
Mis � jour le
, publi� le
14/02/2014 | 19:25
Bastien Durocher, candidat du Front national aux �lections municipales � Ch�teauroux (Indre), a �t� exclu de la liste, vendredi 14 f�vrier. Il a �t� �cart� apr�s la diffusion de photos le montrant avec un tatouage nazi : un blason de la division�Charlemagne�de la�Waffen-SS. Cette division �tait compos�e de volontaires fran�ais partis combattre les soldats sovi�tiques en 1944 et 1945.
"Suite � la l�gitime �motion suscit�e en interne par le tatouage d'un sympathisant du Front national de Ch�teauroux, il a �t� d�cid� que Bastien Durocher ne figurerait pas sur la liste du Rassemblement bleu Marine pour Ch�teauroux", a indiqu� Matthieu�Colombier, la t�te de liste locale, dans un communiqu�. Pourtant, il l'avait soutenu la veille :�"Je�ne pas lui retirer ma confiance (...)�Je ne l'exclus pas de la liste", avait-il d�clar�.
Son adh�sion au FN n'est pas �tablie
Le secr�taire g�n�ral adjoint du FN, Nicolas Bay, a d�clar� � ne pas savoir si Bastien�Durocher �tait adh�rent au Front national,�mais qu'il n'�tait de toute mani�re "pas en position �ligible" sur la liste � Ch�teauroux. "Au d�part, M. Colombier avait voulu le d�fendre, puis il a d�cid� de l'�carter, je lui ai confirm� que c'�tait une bonne id�e", a ajout� Nicolas Bay,�responsable du FN pour les �lections municipales dans les villes de plus de 1�000�habitants.
"Bastien incarne cette jeunesse fran�aise qui, plut�t que de sombrer dans l'extr�misme et la radicalit� facile, a su prendre ses responsabilit�s en s'engageant concr�tement et dans le respect de la d�mocratie et des valeurs r�publicaines en faveur de ses compatriotes", a n�anmoins soulign� vendredi�Matthieu�Colombier.
Bastien Durocher s'est d�fendu dans le quotidien r�gional La Nouvelle R�publique , paru vendredi. Il plaide une "erreur de jeunesse". Et pr�cise : "J'ai d'ailleurs un rendez-vous le 4 mars pour faire enlever ce tatouage. J'avais 19 ans [lorsque je me suis fait tatouer]."
