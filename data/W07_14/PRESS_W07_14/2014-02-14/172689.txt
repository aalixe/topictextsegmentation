TITRE: La Russie reprend pied dans le monde arabe / France Inter
DATE: 2014-02-14
URL: http://www.franceinter.fr/emission-geopolitique-la-russie-reprend-pied-dans-le-monde-arabe
PRINCIPAL: 0
TEXT:
Tweet
Vladimir Poutine et Abdel Fattah al-Sisi  � Reuters - 2014
Un dictateur en recevait un autre et lui apportait son soutien. Le nouvel homme fort de l��gypte, le mar�chal Abdel Fatah al-Sissi, �tait re�u hier au Kremlin o� Vladimir Poutine lui a d�clar� : � Je sais que vous avez pris la d�cision de pr�senter votre candidature � la pr�sidentielle. C�est une d�cision tr�s responsable que de s�investir d�une mission pour le peuple �gyptien. Je vous souhaite de r�ussir (car) la stabilit� de tout le Proche-orient d�pend largement de la stabilit� en �gypte �.
C��tait clair. C��tait net. Le mar�chal vient d��tre investi par le pr�sident russe avant m�me d�avoir officiellement d�clar� sa candidature devant les citoyens �gyptiens mais pourquoi Vladimir Poutine a-t-il aussi spectaculairement contrevenu aux usages diplomatiques qui interdisent � un pays de s�immiscer dans les �lections d�un autre ?
La premi�re raison en est toute personnelle. Form� � l��cole du KGB, le pr�sident russe a une vision polici�re de l�histoire et a toujours consid�r� que les r�volutions arabes n��taient que le fruit d�une manipulation de la CIA, d�une volont� des Etats-Unis de remplacer des dictateurs en bout de course par des hommes neufs qui ne devraient leur pouvoir qu�� Washington.
Non seulement il n�a jamais cru � l�authenticit� de ces r�volutions mais il a toujours �t� persuad� qu�il �tait le prochain sur la liste de la CIA, qu�apr�s les dictateurs arabes viendrait son tour et que la preuve en �tait les manifestations de masse auxquelles sa propre candidature s��tait heurt�e en 2012. A ses yeux, tout cela n��tait que complot am�ricain et c�est ainsi l� qu�est la raison profonde de son ind�fectible soutien au chef du r�gime syrien, Bachar al-Assad, qu�il ne veut pas voir � son tour renverser par son peuple de peur que cela ne donne des id�es aux Russes.
D�o� cette totale empathie de Vladimir Poutine � l��gard de ce militaire �gyptien qui a d�pos� le 3 juillet dernier le premier civil �lu � la pr�sidence �gyptienne � l�issue d�un scrutin libre, l�islamiste Mohamed Morsi aujourd�hui derri�re les barreaux. Le mar�chal al-Sissi est un homme comme Vladimir Poutine les aime, un homme � poigne qui n�h�site pas � faire arr�ter les Fr�res musulmans mais aussi les jeunes d�mocrates qui avaient renvers� Hosni Moubarak il y a trois ans.
C�est une sainte alliance entre r�gimes r�pressifs qui a �t� scell�e hier � Moscou mais, au-del� de cette solidarit� de dictateurs, ces embrassades relevaient aussi de la Raison d��tat car le mar�chal al-Sissi n�est en odeur de saintet� d�aucun c�t� de l�Atlantique. Am�ricains et Europ�ens lui reprochent d�avoir d�pos� un pr�disent �lu, de fouler aux pieds toutes les libert�s et d�embastiller tous ses opposants, islamistes ou pas.
�
Washington a �t� jusqu�� geler une partie de son aide militaire � �gypte et la Russie a donc une place � reprendre au Caire en offrant � Abdel Fatah al-Sissi son soutien politique et toutes les armes, surtout, qu�il souhaite lui acheter pour signifier aux �tats-Unis qu�il peut se passer d�eux.
�
A Damas comme au Caire, la Russie reprend pied dans le monde arabe et le fait en aidant � y �touffer ses r�volutions.
1
commentaire � propos de l'�mission
toutes les r�actions
