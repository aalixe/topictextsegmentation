TITRE: Allergies : Chanel n�5 changera-t-il de composition ?
DATE: 2014-02-14
URL: http://www.reponseatout.com/pratique/sante-bien-etre/allergies-chanel-n5-changera-composition-a1012197
PRINCIPAL: 0
TEXT:
Allergies : Chanel n�5 changera-t-il de composition ?
Le 14/02/2014 � 12:12:07
Vues : 1675 fois JE REAGIS
Le synth�tique HICC et deux essences naturelles extraites de mousses, utilis�es pour la note bois�e des parfums pourraient �tre interdits par la Commission europ�enne | � ThinkStock
Trois ingr�dients allerg�nes contenus dans les parfums pourraient �tre interdits par la Commission europ�enne. Certains classiques, comme Chanel n�5, devraient changer de composition.
Les plus grandes marques de parfums vont devoir remettre le nez dans leurs fragrances. La Commission europ�enne envisage d'interdire trois ingr�dients jug�s hautement allerg�nes : le synth�tique HICC et deux essences naturelles extraites de mousses, utilis�es pour la note bois�e des parfums. Ces substances sont accus�es de provoquer des r�actions allergiques chez 15 millions d�Europ�ens, qui d�veloppent notamment de l�ecz�ma. Petit souci : ces composants sont utilis�s dans la plupart des grands classiques de la parfumerie, comme le fameux N�5 de Chanel.
� Nous ne visons aucun parfum en particulier, il n�est pas question d�interdire Chanel n�5 �, s'est d�fendu le commissaire � la Consommation, Neven Mimica. Cependant, si la proposition est valid�e par l�Union europ�enne, la composition de certains parfums devra �tre � reformul�e � � partir de d�but 2015.
Le secteur du bio sera le plus touch�
La concentration de 12 autres ingr�dients, notamment aux parfums de rose ou citron, devra, elle, �tre limit�e, tandis que la pr�sence de 106 composants, contre seulement 26 actuellement, susceptibles de d�clencher des r�actions allergiques devra figurer sur l��tiquetage.
Et contrairement � ce que l�on pourrait penser, la cosm�tique bio sera la plus touch�e par cette r�forme. � Selon les estimations du secteur, plus de 90 % des produits devront �tre modifi�s �, a indiqu� une source europ�enne.
D�apr�s la Commission, certains industriels ont anticip� cette mutation en reformulant dans le plus grand secret certaines de leurs fragrances. Si la proposition est accept�e, l�industrie du cosm�tique et du luxe disposera de deux � cinq ans pour s�adapter.
Par Mathilde Bourge
