TITRE: Brian Joubert sort la t�te haute - 14/02/2014 - La Nouvelle R�publique
DATE: 2014-02-14
URL: http://www.lanouvellerepublique.fr/Toute-zone/Sport/Sports-de-glisse/Patinage/n/Contenus/Articles/2014/02/14/Brian-Joubert-sort-la-tete-haute-1796315
PRINCIPAL: 174950
TEXT:
Vienne -                     Poitiers -                                     Jeux Olympiques - Patinage artistique
Brian Joubert sort la t�te haute
14/02/2014 20:16
Pour la derni�re comp�tition de sa carri�re, Brian Joubert termine � la 13e place des Jeux Olympiques.  - (Photo d'archives Patrick Lavaud)
Malgr� une d�cevante 13e place finale, le patineur poitevin n'a pas manqu� de panache pour la derni�re sortie de sa carri�re.
Septi�me hier � l'issue d'un programme court parfaitement ma�tris�, Brian Joubert avait surtout conserv� toutes ses chances de m�daille olympique.
Le Poitevin pointait en effet � 1,14 point de l'Espagnol Javier Fernandez, qui occupait la troisi�me place avant le programme libre de ce soir derri�re le Canadien Patrick Chan et l'impressionnant Japonais Yuzuru Hanyu.
Pour la derni�re �preuve de sa carri�re, Brian Joubert (29 ans) patinait en deuxi�me position de l'avant-dernier groupe, avant ses principaux concurrents.
" Je suis content de ces Jeux "
Sur la musique du concerto d'Aranjuez, le champion du monde 2007 r�alisait une superbe entr�e en mati�re. Il r�ussissait d'entr�e une tr�s belle combinaison quadruple boucle piqu�e - double boucle piqu�e avant d'encha�ner avec un nouveau quad.
Malheureusement, par la suite, il posait la main � la r�ception d'un triple axel avant qu'un triple lutz se transforme en double. Visiblement marqu� physiquement, il perdait l�g�rement l'�quilibre sur la pirouette finale.
S'il ne montait pas sur le podium,�Brian Joubert se montrait tout de m�me satisfait � l'issue de la derni�re sortie de sa carri�re malgr� la 13e place finale (231,77 points). " Je suis content de ces Jeux. J'ai r�alis� un programme court fabuleux. J'ai tr�s bien d�but� le libre. Le deuxi�me quadruple saut est fantastique mais je l'ai ensuite pay� physiquement. Je n'avais plus de jus. J'avais les jambes qui tremblaient ", avouait-il au micro de France T�l�vision.
" Je pense � ma famille, � tous ceux qui m'ont soutenu "
L'�motion le gagnait ensuite. Brian Joubert ne pouvait retenir ses larmes au moment d'�voquer le soutien de ses supporters et de sa famille. " Le public russe a �t� exceptionnel. Je pense � ma famille, � tous ceux qui m'ont soutenu et je veux leur dire merci ", concluait-il.
A terme d'une formidable carri�re, ponctu�e de seize m�dailles internationales, d'un titre de champion du monde, de trois championnats d'Europe et de huit couronnes nationales, Brian Joubert peut faire son adieu aux lames avec la t�te haute.
> Classement final : 1. Yuzuru Hanyu (Japon) 280,09 points, 2. Patrick Chan (Canada) 275,62 points, 3. Denis Ten (kazakhstan) 255,10 points... 13. Brian Joubert (France) 231,77 points...
Pierre Samit
