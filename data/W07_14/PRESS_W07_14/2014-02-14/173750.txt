TITRE: Cin�ma : la fr�quentation baisse pour la 4�me ann�e cons�cutive en Europe - Capital.fr
DATE: 2014-02-14
URL: http://www.capital.fr/a-la-une/actualites/cinema-la-frequentation-baisse-pour-la-4eme-annee-consecutive-en-europe-910910
PRINCIPAL: 0
TEXT:
Tweet
La fr�quentation des salles de cin�ma dans l'Union europ�enne a baiss� de 4,1% en 2013 pour atteindre 908 millions de billets vendus, quatri�me baisse annuelle cons�cutive du march� et deuxi�me plus bas niveau des dix derni�res ann�es, selon l'Observatoire europ�en de l'audiovisuel. /Photo d'archives/REUTERS/Danish Siddiqui
La fr�quentation des salles de cin�ma dans l'Union europ�enne a baiss� de 4,1% en 2013 pour atteindre 908 millions de billets vendus, le deuxi�me plus bas niveau des dix derni�res ann�es, annonce vendredi l'Observatoire europ�en de l'audiovisuel.
Il s'agit de la quatri�me baisse cons�cutive du march� depuis quatre ans, selon des chiffres encore provisoires, et d'un quasi doublement de la tendance observ�e l'an dernier, quand le nombre d'entr�es avait baiss� de 2,2%.
Les principaux march�s sont touch�s, � commencer par le premier, la France, en baisse de 5,3%, l'Espagne (- 16%), le Royaume-Uni et l'Allemagne (- 4%). Le march� italien remonte en revanche de 6,6% apr�s une forte chute en 2012.
"Comme souvent ces derni�res ann�es, seuls les pays hors UE ont enregistr� une croissance significative", souligne l'Observatoire, un organisme d�pendant du Conseil de l'Europe.
La F�d�ration de Russie devient ainsi le deuxi�me plus grand march� europ�en avec un total de 173,5 millions d'entr�es, derri�re la France (192,8 millions) mais devant le Royaume-Uni (165,5 millions).
Gilbert Reilhac, �dit� par Yves Clarisse
� 2014 Reuters - Tous droits de reproduction r�serv�s par Reuters.
