TITRE: Paris : trois bless�s graves dans une explosion sur un chantier - RTL.fr
DATE: 2014-02-14
URL: http://www.rtl.fr/actualites/info/article/paris-trois-blesses-graves-dans-une-explosion-sur-un-chantier-7769720553
PRINCIPAL: 174238
TEXT:
Accueil > Toutes les Actualit�s info > Paris : trois bless�s graves dans une explosion sur un chantier
Paris : trois bless�s graves dans une explosion sur un chantier
faits divers
Une explosion est survenue dans une chocolaterie en travaux, � Paris, blessant gri�vement trois personnes.
Trois personnes ont �t� gravement  bless�es ce vendredi 14 f�vrier  � Paris, du fait d'une explosion sur un chantier.
L'explosion a eu lieu vers 16 heures "pour une raison encore inexpliqu�e" dans la chocolaterie "� l'�toile d'or", situ�e dans le IXe arrondissement et qui se trouvait en travaux, a indiqu� une source polici�re. Les pompiers sont intervenus pour "trois urgences absolues".
"Cela pourrait �tre une explosion due au gaz mais rien n'est s�r, pour l'instant on ne sait pas vraiment", a pr�cis� une source proche de l'enqu�te. Les bless�s ont �t� pris en charge pour �tre soign�s non loin des lieux de l'explosion.
La r�daction vous recommande
