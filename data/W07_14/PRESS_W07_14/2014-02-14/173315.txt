TITRE: Affaire Snowden: trois employ�s de la NSA sanctionn�s - swissinfo.ch
DATE: 2014-02-14
URL: http://www.swissinfo.ch/fre/nouvelles_agence/international/Affaire_Snowden:_trois_employes_de_la_NSA_sanctionnes.html?cid%3D37956470
PRINCIPAL: 173314
TEXT:
Toute l'actu en bref
13 f�vrier 2014 - 17:14
Trois personnes travaillant pour la NSA ont �t� sanctionn�es pour leur implication pr�sum�e dans les actes d'Edward Snowden, a r�v�l� la cha�ne NBC. Celle-ci se fonde sur un m�morandum de la NSA adress� lundi aux commissions parlementaires du renseignement.
L'habilitation d'acc�s aux documents secrets a �t� r�voqu�e pour un employ� civil de la NSA, qui a ensuite d�missionn�, selon la cha�ne.
Le 18 juin 2013, cet employ� avait reconnu devant les enqu�teurs du FBI avoir accept� qu'Edward Snowden utilise son propre certificat num�rique PKI d'acc�s au r�seau classifi� de la NSA alors qu'il "savait que cet acc�s n'�tait pas autoris� pour M. Snowden", relate la NSA dans ce m�morandum.
L'ancien consultant, aujourd'hui r�fugi� en Russie, s'�tait ensuite d�brouill� pour "capturer le mot de passe" de l'employ� civil "� son insu". Ce qui lui permettait d'acc�der aux documents, dont une partie a �t� r�v�l�e depuis l'�t� 2013 dans la presse mondiale, d�clenchant une pol�mique plan�taire sur les agissements de la NSA.
"Le civil n'�tait pas au courant que M. Snowden comptait divulguer ill�galement des informations classifi�es. Mais en partageant son certificat PKI, il n'a pas respect� les proc�dures de s�curit�", justifie la NSA.
Acc�s interdit
Deux autres personnes travaillant pour la NSA, un militaire et un sous-traitant, ont pour leur part �t� interdits d'acc�s aux locaux de la NSA en ao�t 2013. Le degr� de leur implication dans les actions de Snowden n'est pas pr�cis� dans le m�morandum.
Lors d'une s�ance de questions-r�ponses �crites en direct sur Internet le 23 janvier, Edward Snowden avait d�menti avoir vol� les mots de passe de ses anciens coll�gues de la NSA. "Je n'ai jamais vol� aucun mot de passe, ni dup� une arm�e de coll�gues", avait-il affirm�.
sda-ats
