TITRE: Rattrapé par une affaire de trafic d'anabolisants, Mister Univers renonce aux municipales à - paris-normandie.com
DATE: 2014-02-14
URL: http://www.paris-normandie.fr/article/caudebec-les-elbeuf/rattrape-par-une-affaire-de-trafic-danabolisants-mister-univers-renonce-
PRINCIPAL: 172566
TEXT:
Rattrap� par une affaire de trafic d'anabolisants, Mister Univers renonce aux municipales � Caudebec-l�s-Elbeuf
Publi� le                                                                                                                                      14/02/2014 � 06H43
Partager
R�agir
Alexandre Piel s'impose devant les 27 meilleurs culturistes mondiaux
CAUDEBEC-LES-ELBEUF (Seine-Maritime). Selon Radio France, Alexandre Piel, �lu "Mister Univers" fin novembre, a �t� arr�t� en d�but de semaine � Rouen, et plac� en garde � vue par le Service R�gional de Police Judiciaire (SRPJ) de Rouen . Il est mis en cause pour des produits anabolisants interdits � la consommation, ainsi que des produits v�t�rinaires. C'est l'interception d'un colis adress� au nom d'Alexandre Piel en provenance de Tha�lande...
