TITRE: Quatre ados convoqu�s devant le juge pour avoir tortur� un canard - 20minutes.fr
DATE: 2014-02-14
URL: http://www.20minutes.fr/societe/1299178-20140214-quatre-ados-convoques-devant-juge-avoir-torture-canard
PRINCIPAL: 173327
TEXT:
Illustration d'une famille de canards traversant une route Tim Scrivener / Rex Fea/REX/SIPA
FAITS DIVERS - Les adolescents ont �galement agress� d�autres gar�ons qui les avaient vu faire...
Quatre adolescents de 14 � 15 ans ont �t� bri�vement plac�s en garde � vue et seront convoqu�s devant un juge des enfants pour avoir tortur� et tu� un canard pr�s de Toulouse, puis frapp� deux t�moins, a-t-on appris vendredi de source polici�re.
Les adolescents s'en sont pris au palmip�de mercredi apr�s-midi au plan d'eau de Colomiers (Haute-Garonne), ville de la banlieue de Toulouse. Ils ont jet� l'oiseau au sol et l'ont frapp� � coups de pied jusqu'� ce que mort s'en suive. Ils ont ensuite port� des coups � deux adolescents qui les avaient vus faire, a-t-on appris aupr�s du commissariat de Colomiers.
Animal tu�
Alert�s par des t�moins, les policiers ont interpell� cinq adolescents, dont un a �t� mis hors de cause. Quatre seront convoqu�s devant un juge pour enfants en avril pour r�pondre d'actes de cruaut� envers un animal et violences volontaires en r�union.
Les blessures des deux t�moins frapp�s sont sans gravit�.
Le 3 f�vrier, un jeune homme qui avait martyris� un chaton de cinq mois et diffus� les vid�os de ses actes sur internet a �t� condamn� � un an de prison ferme � Marseille. L'affaire avait �t� tr�s m�diatis�e.
Le 5 f�vrier, un restaurateur a �t� condamn� � Angers � quatre mois de prison avec sursis pour avoir lanc� un produit � base de soude sur un chat en cage qui l'exasp�rait.�
Avec AFP
