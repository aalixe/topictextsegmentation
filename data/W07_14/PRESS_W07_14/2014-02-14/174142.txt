TITRE: OS mobile : 15 nouveaux partenaires pour Tizen
DATE: 2014-02-14
URL: http://www.zdnet.fr/actualites/os-mobile-tizen-s-offre-quinze-nouveaux-partenaires-39797831.htm
PRINCIPAL: 0
TEXT:
RSS
OS mobile : 15 nouveaux partenaires pour Tizen
Business : Dans la liste des nouveaux arrivants ont trouve notamment Baidu, Sprint et ZTE. Reste que l'OS mobile alternatif est encore dans le brouillard.
Par Olivier Chicheportiche |
Vendredi 14 F�vrier 2014
Du sang frais pour Tizen , l'OS mobile open source d�velopp� par Samsung et issu de MeeGo. Le programme d�di� aux partenaires pour "�largir le soutien � la plateforme  Tizen chez plus de fabricants d'appareils connect�s, d'op�rateurs, de  d�veloppeurs d'applications et d'�diteurs de logiciels" s'�toffe en effet de 15 nouveaux membres.
Ils viennent grossir les rangs d'une arm�e � l'origine compos�e de 36 acteurs. "Le projet Tizen est de plus en plus soutenu par les acteurs de l��cosyst�me des appareils connect�s qui prennent conscience de la formidable opportunit� que repr�sente l�explosion de l�offre et de la demande de smartphones et d�appareils connect�s au niveau mondial, de l�entr�e de gamme aux mod�les les plus perfectionn�s. Ces int�r�ts communs feront de Tizen un moteur en termes d�innovation, et ce au-del� du champ des seuls smartphones � chaque �diteur et chaque op�rateur pourra proposer � sa client�le une immense vari�t� de services personnalisables", commente Ryoichi Sugimura, membre du Conseil d�Administration de Tizen.
�
Parmi les 15 nouveaux membres figurent le g�ant chinois Baidu, Gamevil, les op�rateurs SoftBank Mobile et Sprint, ainsi que le fabricant de smartphones ZTE.
Pour autant, l'avenir de Tizen reste assez opaque. Rappelons que Samsung voit son principal projet de d�ploiement avec le japonais NTT DoCoMo battre de l'aile. Ce  dernier a annonc� que le march� du smartphone ne connaissait pas une  croissance suffisante au Japon pour qu'il pousse l'�mergence d'un  troisi�me syst�me d'exploitation mobile derri�re Android et iOS.
Si Samsung  n'est que l'un des soutiens de Tizen, c'est une claque douloureuse pour  lui et ses partenaires, dont Intel, Orange et Vodafone. Aucun de ces  op�rateurs n'a vraiment annonc� de proposition pour l'instant mais des annonces pourraient �tre faites lors du prochain Mobile World Congress qui ouvre ses portes le 24 f�vrier prochain.
Voir aussi notre page
