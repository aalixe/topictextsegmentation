TITRE: La Ferrari California met le turbo - Autoblog FR
DATE: 2014-02-14
URL: http://fr.autoblog.com/2014/02/13/la-ferrari-california-met-le-turbo/
PRINCIPAL: 172052
TEXT:
La Ferrari California met le turbo
Post� le 13 - 02 - 2014 par Guillaume Navarro
Cat�gories: Ferrari , Marketing/pub , Nouveaut�s , Salon de Gen�ve , Voitures de sport
L'arriv�e d'une nouvelle Ferrari est toujours un �v�nement � part dans le monde des voitures de sport. Si cette California T est, sur le plan esth�tique, globalement semblable � son a�n�e lanc�e en 2008, c'est sous son capot que se trouve la plus grande nouveaut� avec un in�dit bloc V8 turbocompress�, une premi�re dans l'Histoire moderne de la marque au Cheval Cabr�.
Il faut en effet remonter � la F40 pour trouver trace d'un moteur suraliment� sous le capot d'une production de Maranello. Attendue au Salon de Gen�ve, la California T, pour turbo, est donc �quip�e d'un V8 turbocompress� cubant 3 855 centim�tres cube capable de d�velopper 560 chevaux � 7 500 tours par minute et d'offrir un couple maximal de 755 Nm. C'est tout de m�me 70 pur-sang et 49 % de couple en plus que la California de 2008 !
Les performances s'en ressentent puisque le 0 � 100 km/h est d�sormais abattu en seulement 3,6 secondes, soit deux dixi�mes de mieux qu'auparavant. La consommation et les �missions de gaz � effet de serre profitent �galement de l'arriv�e du turbo puisque la California T consomme 15 % de moins que sa devanci�re et n'�met plus que 250 grammes de CO2 par kilom�tre, contre 299 pour la California atmosph�rique. La suralimentation fait donc progresser le mod�le d'entr�e de gamme de Ferrari dans tous les domaines. Reste maintenant � savoir si le bruit ne p�tira pas de cette nouveaut�...
