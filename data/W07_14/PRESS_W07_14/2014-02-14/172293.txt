TITRE: Microsoft reconna�t que Windows 8 se vend moins bien que le 7 | Reuters
DATE: 2014-02-14
URL: http://fr.reuters.com/article/frEuroRpt/idFRL5N0LI50220140213
PRINCIPAL: 0
TEXT:
Microsoft reconna�t que Windows 8 se vend moins bien que le 7
vendredi 14 f�vrier 2014 00h50
�
[ - ] Texte [ + ]
SEATTLE, Etat de Washington, 13 f�vrier (Reuters) - M icrosoft a annonc� jeudi avoir vendu plus de 200 millions de licences de son syst�me d'exploitation Windows 8, lanc� il y a 15 mois, un r�sultat inf�rieur � celui de Windows 7 qui s'�tait vendu � 240 millions d'exemplaires au cours de sa premi�re ann�e sur le march�.
Ces chiffres, fournis par la directrice financi�re et marketing de Microsoft, Tami Reller, au cours d'une conf�rence organis�e par Goldman Sachs, sont les premiers diffus�s par le g�ant am�ricain depuis six mois.
Les ventes d�cevantes de Windows 8 et sa derni�re version, Windows 8.1, r�pondent au fl�chissement des ventes d'ordinateurs personnels constat� depuis deux ans au b�n�fice des smartphones et des tablettes. Les ventes mondiales de tablettes devraient d�passer cette ann�e celles des PC.
Windows 8 a �t� con�u comme un syst�me d'exploitation capable de fonctionner aussi bien sur PC que sur tablette, mais il n'a pas convaincu les utilisateurs qui continuent � pr�f�rer les iPad d'Apple aux tablettes Surface de Microsoft.
Plus inqui�tant encore pour la soci�t� bas�e � Redmond, pr�s de Seattle, peu d'ordinateurs fonctionnent sous Windows 8 et de nombreuses entreprises qui l'ont achet� ne l'ont m�me pas install�.
Selon les statistiques de NetMarketShare, seuls 11% des utilisateurs de PC dans le monde ont adopt� Windows 8 ou 8.1, alors qu'ils sont encore 48% � utiliser Windows 7 et 29% � �tre rest�s fid�les au "vieux" Windows XP, qui date de plus de dix ans.
Windows 7, qui avait remplac� le tr�s impopulaire Windows Vista, reste � ce jour le syst�me d'exploitation de Microsoft qui s'est le mieux vendu, avec plus de 450 millions de licences �coul�es.
(Bill Rigby; Tangi Sala�n pour le service fran�ais)  )
� Thomson Reuters 2014 Tous droits r�serv�s.
�
