TITRE: MWC 2014 - Le syst�me Tizen re�oit le soutien d'une quinzaine de partenaires
DATE: 2014-02-14
URL: http://www.clubic.com/os-mobile/actualite-618958-systeme-tizen-recoit-soutien-quinzaine-partenaires.html
PRINCIPAL: 0
TEXT:
Vid�os
MWC 2014 - Le syst�me Tizen re�oit le soutien d'une quinzaine de partenaires
1 commentaire
D�velopp� conjointement par Intel et Samsung, le syst�me mobile Tizen annonce avoir recu le soutien d'une quinzaine de soci�t�s, parmi lesquelles nous retrouvons des constructeurs, des �diteurs et des op�rateurs mobiles.
Quelques jours avant le coup d'envoi officiel du Mobile World Congress, l'association Tizen annonce avoir re�u le soutien de 15 nouvelles soci�t�s, lesquelles se sont ainsi engag�es � favoriser l'adoption du syst�me d'exploitation mobile. Se joignent ainsi � cette initiative les op�rateurs Sprint et Softbank Mobile, le constructeur ZTE mais aussi Baidu et AccuWeather. Retrouvez la liste compl�te ici .
Plusieurs grands groupes avaient pr�c�demment rejoint cette initiative et notamment Orange, Fujitsu, Vodafone, SK Telecom, Huawei, LG ou encore Docomo, eBay, Panasonic ou McAfee. En th�orie, ces partenaires aideront donc � financer le d�veloppement du syst�me, choisiront de l'embarquer au sein de leurs prochains smartphones ou distribueront ces derniers au travers de leur catalogue. C�t� �diteur, l'association entend proposer d'embl�e des applications phares.
Samsung et Intel ont articul� les bases de Tizen sur les pr�c�dents travaux de MeeGo. L'objectif est de d�velopper un syst�me multiplatforme. La fondation Linux l'a d'ailleurs choisi comme OS embarqu� de r�f�rence pour les voitures connect�es. Notons en outre que certains d�veloppeurs planchent d�j� sur la prise en charge des applications Android .
Davantage d'informations sur les nouveaut�s de Tizen seront communiqu�es � l'occasion du Mobile World Congress. Cette �dition devrait d'ailleurs mettre en lumi�re les efforts port�s sur d'autres syst�mes mobiles qu'il s'agisse de Firefox OS, Ubuntu Touch ou encore Sailfish OS.
�
