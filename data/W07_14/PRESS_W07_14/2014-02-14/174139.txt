TITRE: Rakuten s'offre Viber
DATE: 2014-02-14
URL: http://www.lesnumeriques.com/rakuten-offre-viber-n33226.html
PRINCIPAL: 174135
TEXT:
Nous soutenir, navigation sans publicit� Premium : 2�/mois + Conseil personnalis� Premium+ : 60�/an
Rakuten s'offre Viber
Un concurrent majeur pour Skype et consorts
�
Publi� le: 14 f�vrier 2014 15:34
Par Johann Breton
Tweet
Sortant peu � peu de la sph�re e-commerce, le Japonais Rakuten vient d'annoncer son entr�e sur le march� de la communication instantan�e avec le rachat de Viber.
Le g�ant nippon du e-commerce, Rakuten, poursuit sa tentaculaire expansion hors des fronti�res nationales. Apr�s �tre rest�e en terrain connu en faisant main basse sur PriceMinister, Buy.com et Play.com, puis s'�tre un brin diversifi�e avec le rachat de Kobo, la firme s'int�resse maintenant aux applications grand public, avec le rachat de Viber. Une fois n'est pas coutume, le montant de la transaction est connu�: ce sont 900 millions de dollars qui ont �t� d�bours�s.
Concurrent notamment de Skype, Viber est un service de messagerie et de t�l�phonie sur IP qui compte plus de 300 millions d'utilisateurs. Pr�sent sur de nombreuses plateformes (Windows, iOS, Android, Bada, BlackBerry, Windows Phone..), il semble avoir �t� rachet� dans l'optique d'une �ventuelle convergence entre les diff�rentes boutiques en ligne de Rakuten et les technologies de communication ma�tris�es par le service de messagerie. Le Japonais n'a toutefois pas d�voil� de plans pr�cis.
