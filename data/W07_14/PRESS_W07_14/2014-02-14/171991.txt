TITRE: Bouteille suspecte. De la drogue, pas d'explosif
DATE: 2014-02-14
URL: http://www.ouest-france.fr/bouteille-suspecte-de-la-drogue-pas-dexplosif-1928539
PRINCIPAL: 0
TEXT:
Bouteille suspecte. De la drogue, pas d'explosif
France -
Achetez votre journal num�rique
La bouteille retrouv� jeudi dans le train Paris-Venise ne contenait pas d'explosif mais un produit stup�fiant.
"Le produit a r�agi positivement aux tests du GHB", a d�clar� le procureur de Besan�on, Alain Saffar. Le GHB est une drogue de synth�se surnomm�e la "drogue du violeur".
Les premi�res analyses avaient laiss� supposer que la bouteille, sur laquelle il �tait �crit "'Nitro", puisse contenir des explosifs.
Tags :
