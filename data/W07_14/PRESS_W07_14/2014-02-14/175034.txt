TITRE: PSG-VA, Lavezzi buteur �mu - Goal.com
DATE: 2014-02-14
URL: http://www.goal.com/fr/news/29/ligue-1/2014/02/14/4620340/psg-va-lavezzi-buteur-%25C3%25A9mu
PRINCIPAL: 175033
TEXT:
0
14 f�vr. 2014 21:03:00
Titulaire au Parc des Princes face � Valenciennes, Ezequiel Lavezzi a ouvert le score apr�s sa semaine �prouvante d'un point de vue �motionnel.
Titulaire sur le c�t� gauche apr�s l'annonce de la mort de son oncle, assassin� par balle en Argentine , Ezequiel Lavezzi a pu rendre hommage au membre de sa famille. Affect� par son d�c�s, l'international argentin a ouvert le score face � valenciennes. Les doigts lev�s au ciel, c'est avec �motion que "Pocho" s'est dirig� vers le poteau de corner pour c�l�brer son but. F�licit� par ses partenaires, le num�ro 22 parisien trouvera peut-�tre un peu de r�confort avec le quatri�me but de sa saison en Ligue 1.
Relatifs
