TITRE: BlackBerry : BBM 2.0 pour Android et iOS int�gre de nouveaux outils de communication
DATE: 2014-02-14
URL: http://www.commentcamarche.net/news/5864027-blackberry-bbm-2-0-pour-android-et-ios-integre-de-nouveaux-outils-de-communication
PRINCIPAL: 173173
TEXT:
BlackBerry : BBM 2.0 pour Android et iOS int�gre de nouveaux outils de communication
ldelvalle le vendredi 14 f�vrier 2014 � 10:56:26
Le constructeur canadien annonce la disponibilit� de la version 2.0 de son application de messagerie instantan�e BlackBerry Messenger (BBM) pour Android et iOS. Plusieurs nouveaut�s au menu, dont l'arriv�e d'un outil de gestion des appels vocaux et la possibilit� de partager des documents avec ses contacts, y compris via Dropbox.
Cette nouvelle mouture de BlackBerry Messenger apporte plusieurs am�liorations sensibles aux utilisateurs de l'application sur iPhone et Android.
Le premier ajout important est celui de BBM Voice : cet outil permet de r�aliser des appels vocaux vers ses contacts BBM via une connection Wi-Fi ou 3g/4g.
Int�gration avec Dropbox
La deuxi�me nouveaut� est la possibilit� pour les utilisateurs de cr�er  des groupes de conversations th�matiques via les "BBM Channels". Cet espace est �galement ouvert aux marques.
Enfin, l'application am�liore ses fonctionnalit�s de partage en permettant aux utilisateurs d'envoyer rapidement photos, m�mos vocaux ou documents joints � leurs contacts. Une innovation qui s'accompagne de l'int�gration de l'application de partage et de synchronisation de fichiers dans le cloud Dropbox. Elle donne la possibilit� aux utilisateurs de partager rapidement avec leurs contacts BBM leurs documents stock�s sur Dropbox.
