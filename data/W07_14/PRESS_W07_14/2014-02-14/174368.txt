TITRE: Victoires de la Musique : "Avant tout un show", dit le pr�sident de la c�r�monie - RTL.fr
DATE: 2014-02-14
URL: http://www.rtl.fr/actualites/culture-loisirs/musique/article/victoires-de-la-musique-avant-tout-un-show-dit-le-president-de-la-ceremonie-7769713609
PRINCIPAL: 174357
TEXT:
Christophe Palatre, invit� de RTL, vendredi 14 f�vrier 2014
Cr�dit : Fanny Bonjean / RTL.fr
INVIT� RTL - Les Victoires de la Musique all�gent leur programme pour cette 29�me �dition. Christophe Palatre, pr�sident de la c�r�monie mise sur une soir�e centr�e "sur les artistes, le show et le spectacle".
Christophe Palatre : "Les Victoires de la musique, avant tout un show"
Cr�dit : RTL
Christophe Palatre : "La c�r�monie des Victoires de la musique sera plus courte cette ann�e"
Cr�dit : Laurent Bazin
La musique est � l'honneur ce vendredi 14 f�vrier avec les Victoires de la Musique. Pour cette 29�me �dition, Christophe Palatre le nouveau pr�sident de la c�r�monie promet "avant tout un show".
�
Grand favori de ces Victoires de la Musique avec six nominations, Stromae est le ph�nom�ne musical de cette ann�e avec plus d'un million  d'exemplaires de Racine Carr�e �coul�s. "C'�tait donc logique qu'il d�marre la soir�e et qu'on le retrouve aussi un peu plus tard. Il va chanter trois titres ", explique-t-il.
Une c�r�monie �court�e
"Cette ann�e, la c�r�monie va �tre plus courte. On devrait terminer vers 23h30. On a voulu faire un show plus fluide, plus centr� sur le spectacle, les prestations des artistes",� d�taille le pr�sident des Victoires de la Musique.
Pour y parvenir, le programme de l'�mission a �t� all�g�. "au lieu d'avoir 4 nomm�s par cat�gorie, il n'y en a plus que 3".
Moins de t�l�-r�alit�
La "chanson originale de l'ann�e" met en comp�tition Papaoutai et Formidable de Stroame , J'me tire de Ma�tre Gims et 20 ans de Johnny Hallyday . L'aspect t�l�-r�alit� avec le vote pour la chanson de l'ann�e pendant l'�mission, qui n�cessitait des magn�tos pour rappeler aux gens tous les quarts d'heure: tapez 1, tapez 2, tapez 3, va se faire en amont de l'�mission avant 20 heures ".
�
Un seul mot d'ordre pour ces Victoires de la Musique : "on va vraiment �tre concentr� sur les artistes, le show et le spectacle", conclut Christophe Palatre.
La r�daction vous recommande
