TITRE: Windows 8 : nouveau bide pour Microsoft - ServiceEntreprise.com
DATE: 2014-02-14
URL: http://www.serviceentreprise.com/nouveau-bide-microsoft-windows-8-n111902.html
PRINCIPAL: 173374
TEXT:
Tweet
Le nouveau syst�me d�exploitation de Microsoft qui devait rectifier le tir manqu� de Windows 8, ne d�colle toujours pas. la firme de Redmond en a �coul� 200 millions en 15 mois.
Apr�s l��chec de Windows 8, dont l�offre avait rebut� les puristes, Microsoft esp�rait relancer la machine avec Windows 8.1 , la nouvelle version de son syst�me d�exploitation vedette qui r�tablit notamment le fameux bouton � d�marrer �, effac� par la pr�c�dente formule.
Lanc� il y a quinze mois, ce nouvel outil peine � faire son trou : Microsoft annonce la vente de 200 millions de licences � ce jour, loin des objectifs initiaux. Pour info, Windows 7, le dernier gros succ�s de Microsoft, s��tait vendu � 450 millions d�exemplaires, dont la moiti� d�s la premi�re ann�e. Edit� en 2009, ce syst�me avait, en moins de cinq mois, atteint la barre des 10% de parts de march�s.
Selon NetMarketShare, 48% des utilisateurs de PC se servent encore de Windows 7, et 29% travaillent encore avec Windows XP, une solution en date de�2001, �poque rose pour la march� du Personal Computer (PC).
Echec de la tablette de Microsoft
Aujourd�hui, la double peine est s�v�re pour l�ancien groupe de Bill Gates et Steve Ballmer, r�cemment remplac� par Satya Nadella : l��chec de Windows 8 traduit en fait la d�gringolade du march� du PC depuis deux ans, incapable de r�sister au boom des smartphones et autres tablettes num�riques.
Le constat d��chec est d�autant plus retentissant pour Microsoft que Windows 8 et sa petite s�ur 8,1 devait justement permettre � la marque de se faufiler, via sa tablette Surface, sur le march� domin� par l�iPad d�Apple. Mais la � greffe � n�a pas pris.
Cette ann�e encore, les ventes mondiales de tablettes devraient d�passer celles des PC.
Au chapitre des innovations, la version 8.1 de Windows propose un r�glage personnalis�e des vignettes pr�sentes sur l�interface des applications. Il est �galement possible de consulter plusieurs applications � la fois.
Publi� le 14 f�vrier 2014 par J�r�me Albert
Vous avez appr�ci� cet article ?
