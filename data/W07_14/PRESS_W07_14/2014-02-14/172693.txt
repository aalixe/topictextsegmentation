TITRE: Syrie : la responsable humanitaire demande "les moyens de faire son travail" - RTL.fr
DATE: 2014-02-14
URL: http://www.rtl.fr/actualites/info/international/article/syrie-la-responsable-humanitaire-demande-les-moyens-de-faire-son-travail-7769704032
PRINCIPAL: 172690
TEXT:
Valerie Amos se d�clare pessimiste et frustr�e sur la situation humanitaire en Syrie.
Cr�dit : CARL DE SOUZA / AFP
Val�rie Amos a estim� que l'�vacuation de Homs (centre de la Syrie) n'�tait pas un progr�s suffisant.
Val�rie Amos, responsable des op�rations humanitaires de l'ONU a demand� au Conseil de s�curit� de donner aux  humanitaires "les moyens de faire leur travail ce jeudi 13 f�vrier". Elle a �galement  indiqu� que l'ONU avait des "assurances verbales" des  bellig�rants mais toujours pas de confirmation �crite que la tr�ve �  Homs serait prolong�e. "Sans assurances �crites nous ne pouvons pas  continuer", a-t-elle ajout�.
Se d�clarant "non seulement  pessimiste mais tr�s frustr�e", elle a dit avoir averti le Conseil que  "les progr�s sont extr�mement limit�s et douloureusement lents" pour  porter secours aux civils syriens.
250.000 personnes bloqu�es dans les combats
L'�vacuation de pr�s de 1.400  civils de Homs est certes "un succ�s �tant donn� les circonstances  extr�mement difficiles" mais il reste encore 250.000 personnes bloqu�es  par les combats en Syrie sans aucun acc�s aux secours, a-t-elle  soulign�. Elle a dit avoir demand� aux 15 membres du Conseil  "d'user de leur influence sur les parties (au conflit) pour qu'ils  respectent des pauses (humanitaires), facilitent la fourniture d'aide  (..) et �vitent � nos �quipes d'�tre prises pour cibles quand elles  livrent cette aide". "La guerre elle-m�me a des r�gles", a-t-elle  rappel�.
"Il faut que nous ayons les moyens de faire notre travail  sur le front humanitaire", a martel� Mme Amos, jugeant "inacceptable"  la d�t�rioration de la situation sur le terrain.� Interrog� sur le  projet de r�solution humanitaire n�goci� au Conseil, elle a jug�  "important qu'il y ait des leviers pour s'assurer de (son) application".
Des sanctions en cas de blocages l'aide humanitaire
Un  projet occidental de r�solution sur la table du Conseil pr�voit la  possibilit� de sanctions ult�rieures si les bellig�rants bloquent l'aide  humanitaire. Mais la Russie est farouchement oppos�e � cette mention et  a d�pos� un contre-projet de texte qui met l'accent sur la mont�e du  terrorisme en Syrie, un des leitmotiv du r�gime de Bachar al-Assad. L'ambassadeur  russe Vitali Tchourkine a indiqu� � la presse qu'Occidentaux et Russes  allaient essayer de fusionner les deux textes.
"Nous sommes en  discussion, il y a de bonnes chances que nos coll�gues acceptent notre  langage ferme sur le terrorisme", a-t-il estim�. "Je ne dirais pas que  nos positions sont tr�s �loign�es". L'ambassadrice am�ricaine  Samantha Power de son c�t� d�fendu le projet occidental et souhait� "un  texte qui ait un vrai impact sur le terrain". "Pour nous, a-t-elle  pr�venu, vu la gravit� de la situation, il vaut mieux pas de r�solution  du tout qu'une mauvaise r�solution".
La r�daction vous recommande
