TITRE: Tha�lande: la police d�gage des sites occup�s par les manifestants - LExpress.fr
DATE: 2014-02-14
URL: http://www.lexpress.fr/actualites/1/monde/thailande-la-police-degage-des-sites-occupes-par-les-manifestants_1323957.html
PRINCIPAL: 172902
TEXT:
Tha�lande: la police d�gage des sites occup�s par les manifestants
Par AFP, publi� le
14/02/2014 � 08:08
, mis � jour � 12:19
Bangkok - Des milliers de policiers ont �t� d�ploy�s vendredi dans la capitale tha�landaise pour d�gager plusieurs sites occup�s depuis des semaines par les manifestants qui r�clament le d�part de la Premi�re ministre Yingluck Shinawatra.
Des milliers de policiers ont �t� d�ploy�s vendredi dans la capitale tha�landaise pour d�gager plusieurs sites occup�s depuis des semaines par les manifestants qui r�clament le d�part de la Premi�re ministre Yingluck Shinawatra.
afp.com/KC Ortiz
Cette intervention marque un changement inattendu de la strat�gie du gouvernement qui a privil�gi� l'�vitement entre police et manifestants pour limiter les violences lors de cette crise qui a fait au moins dix morts.�
Yingluck fait face depuis plus de trois mois � un mouvement de rue r�clamant, outre sa t�te, la fin de l'influence de son fr�re Thaksin Shinawatra, ancien Premier ministre en exil, renvers� par un coup d'Etat en 2006.�
Les manifestants, alliance h�t�roclite r�unis par la haine de Thaksin, ont occup� ou assi�g� de nombreux minist�res et administrations, avant de lancer mi janvier une op�ration de "paralysie" de Bangkok.�
Ils continuent d'occuper des carrefours strat�giques du centre de la capitale, mais leur nombre s'est largement r�duit, ce qui pourrait avoir encourag� le changement de tactique d'un gouvernement �galement confort� par le rejet cette semaine par la justice d'une requ�te de l'opposition pour invalider les l�gislatives du 2 f�vrier.�
Les policiers anti�meutes ont repris sans r�elle r�sistance vendredi les zones autour du si�ge du gouvernement, largement d�sert�es par les manifestants. Ils ont enlev� les tentes et les diverses barricades de pneus et de barbel�s.�
Mais le r�sultat de l'op�ration n'�tait pas clair, des manifestants �tant retourn�s sur place pour reconstruire leurs d�fenses apr�s le retrait de la police.�
"L'op�ration de la police ce matin est un �chec total", a ainsi raill� Akanat Promphan, porte-parole des manifestants qui ont appel� � une nouvelle grande manifestation � partir de ce vendredi.�
Mais le ministre du Travail Chalerm Yubamrung, qui supervisait l'intervention, a lui assur� que les fonctionnaires reprendraient d�s lundi le travail � Government House, que le gouvernement n'a pu utiliser depuis pr�s de deux mois.�
"Manifestants, vous devriez rentrer chez vous", a-t-il d�clar� � la t�l�vision.�
"Si vous �tes toujours obstin�s, nous appliquerons la loi avec douceur", a-t-il ajout�, soulignant que Yingluck avait demand� � la police de ne pas utiliser la force.�
- Le spectre de 2010 -�
Le gouvernement semble � tout prix vouloir �viter une r�p�tition de la crise de 2010 qui avait fait plus de 90 morts, lorsque son pr�d�cesseur avait envoy� l'arm�e d�loger les "chemises rouges" pro-Thaksin.�
L'intervention de vendredi s'est d�roul�e sans affrontements. Deux personnes ont toutefois �t� l�g�rement bless�es par une explosion ind�termin�e lors d'une br�ve confrontation entre police et militants.�
Trois autres sites, dont le minist�re de l'Int�rieur, sont cibl�s par les autorit�s qui veulent �galement arr�ter les leaders des manifestants, a indiqu� le chef du Conseil de s�curit� nationale, Paradorn Pattanatabut. �
Il a qualifi� l'op�ration de vendredi de "succ�s", mettant en avant la saisie d'armes. "Bien que les manifestants soient revenus, ils ne sont pas entr�s dans Government House", a-t-il ajout�.�
La Tha�lande est englu�e depuis le putsch de 2006 dans des crises politiques � r�p�tition faisant descendre dans la rue tour � tour les ennemis et les partisans de Thaksin, qui reste le facteur de division de la soci�t�.�
Et les l�gislatives anticip�es du 2 f�vrier, boycott�e par l'opposition, n'ont pas permis de sortir du dernier �pisode de ce cycle de violences. �
Les manifestants, qui veulent remplacer le gouvernement par un "conseil du peuple" non �lu, ont en effet fortement perturb� le vote par anticipation du 26 janvier et emp�ch� le d�roulement du scrutin dans 10% des bureaux de vote le 2 f�vrier.�
Dans ces conditions, aucun r�sultat n'a �t� publi�, en attendant au moins deux nouvelles journ�es de scrutin les 20 et 27 avril.�
Aucune solution n'a en revanche �t� annonc�e pour les 28 circonscriptions o� le vote n'a pas eu lieu, les manifestants ayant emp�ch� l'enregistrement des candidatures.�
Faute d'un quorum de 95% des 500 si�ges pour se r�unir, le parlement ne pourra se r�unir, prolongeant le mandat d'un gouvernement condamn� � exp�dier les affaires courantes et plus vuln�rable, selon les analystes, � un nouveau "coup d'Etat judiciaire", dans un pays o� la justice a d�j� chass� deux gouvernements pro-Thaksin en 2008.�
Par
