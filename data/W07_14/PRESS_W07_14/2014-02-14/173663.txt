TITRE: Bienvenue dans l'�re du super-num�rique
DATE: 2014-02-14
URL: http://www.latribune.fr/opinions/tribunes/20140214trib000815451/bienvenue-dans-l-ere-du-super-numerique.html
PRINCIPAL: 0
TEXT:
Bienvenue dans l'�re du super-num�rique
Didier Schmitt, Commission europ�enne �|�
14/02/2014, 11:48
�-� 1058 �mots
L'Europe doit agir vite, pour ne pas accumuler du retard dans le domaine des super calculateurs, au c�ur des produits et services du futur. Un enjeu qui concerne toutes les entreprises, y compris les PME. Par Didier Schmitt, conseiller scientifique, commission europ�enne*
L'�re du 'super-num�rique' est bien l�; sans superordinateur, plus de pr�visions m�t�orologiques, et pas de mod�les de changement climatique. L'innovation sera digitale ou ne sera pas.
Nous sommes d�j� d�pendants
Sans que nous en soyons conscients, des pans entiers de notre �conomie sont d�j� d�pendants du calcul intensif. Il est par exemple impensable de se passer de cet outil pour concevoir un avion. Ainsi, la mod�lisation dynamique permet de se passer de la r�alisation de maquettes et de tests en soufflerie, d'�valuer la r�sistance de nouvelles structures, ou m�me de simuler les chaines d'assemblage. Il en est de m�me pour la conception de voitures�: du crash-test virtuel � l'optimisation de la combustion dans les moteurs.
Nous le serons encore plus
Mais le digital sera encore plus au c�ur des produits et des services du futur. Vers 2030, l'optimisation des flux de v�hicules sans conducteurs sera aux mains d'un calculateur (qui aujourd'hui est un supercalculateur�!). De m�me,�la sant� personnalis�e d' ici une g�n�ration ne peut se concevoir qu'en agr�geant des 'Big Data' (donn�es massives), de la g�nomique - les donn�es doublent tous les 18 mois - aux senseurs environnementaux qui seront pr�sent partout, y compris dans nos v�tements. La mod�lisation du fonctionnement du cerveau,�elle aussi, requi�re d�j� des supercalculateurs;�et ceci sans compter que les technologies de 'spintronique' vont am�liorer, dans les dix ans � venir, la r�solution des scanners c�r�braux d'un facteur dix mille, tout en �tant de la taille d'un casque de v�lo�!
C'est la course
Ces exemples montrent � l'�vidence que la comp�titivit� sera digitale et qu'elle reposera sur la fusion de donn�es multisectorielles. Le calcul intensif est donc bien un domaine hautement strat�gique pour n'importe quelle puissance �conomique et politique. Ceci explique la maxime d'Obama dans ce domaine qui est "outcompute to outcompete". Une posture qui fait r�f�rence � une notion de domination typiquement am�ricaine - en l'occurrence par le calcul intensif. Ce n'est donc pas par hasard que la Chine � son tour soit entr�e dans la course - et de suite par la grande porte - en fabriquant le supercalculateur le plus puissant au monde.�Celui-ci peut r�aliser 30 millions de milliards d'op�rations par seconde en consommant une puissance �lectrique �quivalant � six TGV � pleine vitesse. L'Inde, la Russie et d'autres ne sont d'ailleurs pas en reste.
Sommes-nous en reste?
Les seuls pays encore en comp�tition en Europe ne se comptent plus que sur les doigts d'une main. Pire, leurs vues divergent quant aux strat�gies � avoir, qui sont fonction des int�r�ts nationaux ou des comp�tences existantes, voire de plus en plus absentes. Et pourtant plus aucun pays Europ�en n'a les moyens de rivaliser avec les grands. Soyons clair, nous commen�ons s�rieusement � �tre en retard; l'Europe n'a plus que 5% des parts de march� dans ce domaine.
La Commission Europ�enne, quant � elle, soutient des programmes de coordination, soit pour la mise en commun de moyens de calcul intensifs (France-Allemagne-Italie-Espagne) pour des projets de recherche, soit par des partenariats public-priv�s pour des d�veloppements technologiques, des applications et des infrastructures. Mais cela ne suffit pas. Il y a urgence � ce que les �tats membres se mettent d'accord, au plus haut niveau politique, sur un objectif commun de comp�titivit� afin d'aligner les efforts nationaux et communautaires.
Des enjeux de comp�titivit�
Acheter un supercalculateur sur �tag�re n'est pas une option car pour beaucoup d'utilisations les logiciels doivent �tre co-d�velopp�s. Ne pas avoir cette comp�tence int�gr�e en Europe nous fragiliserait de fa�on intol�rable. Il y a �galement un enjeu de comp�titivit� imm�diat; il est essentiel que les applications du calcul intensif se d�mocratisent�et ne restent pas l'apanage de la recherche ou de certains secteurs industriels de pointe. Trop peu de PME ont acc�s � ces outils indispensables � la comp�titivit�de demain; d'ailleurs pas besoin d'avoir un super-ordinateur sous la main, l'av�nement du 'Cloud' permet de d�porter de son ordinateur les logicielles et la puissance de calcul; ce qui permet aussi la collaboration de masse. Pour cela, il faudrait d�s � pr�sent cr�er des centres d'excellence, former des experts, et cr�er des start-ups qui puissent faire le lien entre le monde du calcul intensif et les applications potentielles.
Et apr�s-demain?
Nous savons d�j� que la physique quantique va permettre de surpasser largement les limites de rapidit� et de consommation en �nergie des supercalculateurs; nous verrons na�tre une nouvelle g�n�ration d''hyper-calculateurs' d'ici 10 � 15 ans. Mais il ne suffit pas d'avoir le prix Nobel de physique dans ce domaine (Fran�ais en l'occurrence), encore faut-il aller jusqu'aux r�alisations concr�tes; nous en avons encore les moyens. Il ne faudrait surtout pas, l�-aussi, inventer en Europe et devenir d�pendant en devant acheter hors d'Europe.
Un sursaut salutaire
Dans ce domaine, comme dans d'autres, le syndrome europ�en de fragmentation existe, et nous sommes arriv�s � un moment charni�re o� les capacit�s d'un seul pays ne suffisent plus pour '�tre � la hauteur'. Nous avons su dans le pass� d�passer les frictions, en cr�ant Airbus, pour fabriquer nos propres avions ou l'agence spatiale europ�enne pour mettre en orbite nos propres satellites avec les lanceurs Ariane. Savons-nous encore avoir ce sursaut de lucidit�et apprendre de nos errements pass�s (pas de train � grande vitesse ou d'avion de combat commun)? Le calcul intensif est un de ces domaines o� nous avons la comp�tence scientifique de (re)devenir leader et de concevoir les superordinateurs du future. Le risque d'�tre vassalis� � jamais en nous laissant couper un tendon d'Achille�est r�el. Sommes-nous assez conscients que l'avenir est digital�?
Didier Schmitt est conseiller scientifique et coordinateur de la prospective aupr�s de la conseill�re scientifique principale et dans le bureau des conseillers de politique europ�enne aupr�s du Pr�sident de la Commission europ�enne. Les opinions exprim�es dans le pr�sent article sont celles de l'auteur et ne refl�tent pas n�cessairement la position de la Commission europ�enne.
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Commentaires
PCT �a �crit le 16/02/2014                                     � 11:13 :
la puissance d'un supercalculateur est li� � 3 param�tres cl�s : la puissance de calcul ( en nombre flottant ) par watt, le d�bit de communication de la puissance de calcul par watt, et la capacit� du logiciel a utiliser cette puissance de calcul ( la parallelisation du logiciel ).
Concernant les 2 premiers parametres, les processeurs actuels ( xeon/ xeon phi ) sont � bout de course, on ne pourra pas monter � l'exaflop avec ca. Les architectures actuelles sont bien trop gourmandes en energie, il faudra donc probablement passer par des architecture moins universelles et plus li�s � l'architecture m�moire, car la limite actuelle des super-calculateurs n'est pas la puissance de calcul, mais l'acces m�moire et le partage des donn�es entre les processeurs, et l� l'europe a toutes ses chances, car l'articulation logiciel/mat�riel est un facteur clef de la puissance de calcul -intel l'a bien compris, qui finance a tour de bras tout un tas de laboratoire de par le monde ( en particulier l'INRIA )-.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
ProCo �a �crit le 14/02/2014                                     � 13:52 :
Pour faire un super calculateur, il faut, du silicium, un OS, et des applis parall�lisables..  On n'a d�j� plus un seul fondeur de silicium, ou du moins ce qu'il en reste est absolument incapable de concevoir des processeurs dignes de ce nom... l'OS des supercalculateurs est souvent un d�riv� d'une souche X quelconque, malheureusement, Linus et ses comp�res ne sont plus en Europe depuis un bail, quant aux applis, no comment... Il va y avoir du boulot si on veut se remettre dans la bonne direction
Maxime Piolet �a r�pondu le 14/02/2014                                                 � 15:12:
Sans compter que - jusqu'� preuve du contraire - les backbones sont toujours aux USA...
@ tous �a r�pondu le 14/02/2014                                                 � 20:04:
Y a pas de probl�me pour les ressource de la France. �a fait belle lurette que notre pays ne produit plus de processeurs ou de circuits int�gr�. on les achet� en Chine. Nous ne somme pas d�pendant que des l�informatique mais surtout des autres pays...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
rolli �a �crit le 14/02/2014                                     � 13:43 :
c'est trop tard ! nous ne disposons plus des technologies n�cessaires et suffisantes ! Il ne nous reste qu'� d�tecter les cerveaux capables d'aider � l'�criture des "syst�mes" et langages, ainsi qu'� l'analyse des applications et la cr�ation des applications correspondantes ! Par ailleurs, la globalisation des donn�es implique la normalisation plan�taire, donc l'unicit� de la technologie centrale ! Qui est dans les bo�tes de d�part : 1 USA, 2 CHINE, 3 INDE...Europe ? = outsider, si la CE parvient, dans les 18 mois, � cr�er un conglom�rat style Airbus et surtout en int�grant la RUSSIE !
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Maxime Piolet �a �crit le 14/02/2014                                     � 13:21 :
Sacr� d�terminisme ! A part �a, en mati�re de s�curit� des infos, des biens et des personnes on en est o� ?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
tous aux abris �a �crit le 14/02/2014                                     � 12:42 :
Dans le domaine du calcul scientifique, il y a effectivement seulement quelques grands pays qui ont gard� les moyens de concevoir et d'�tre autonomes.
Et cela est le fruit des politiques industrielles honnies des eurocrates bruxellois, ainsi que des fili�res d'excellence traditionnels de ces pays (que les cabris fran�ais aimeraient bien d�truire, pour "faire comme les autres").
Ce que l'Europe peut surtout faire dans ce domaine, c'est donc sans doute �viter d'y mettre le d�sordre libre et non fauss�. En sera-t-elle capable ?
info �a r�pondu le 15/02/2014                                                 � 10:27:
st micro�lectronique et bull, sont en phase avec cette recherche/production...
Nous avons des atouts, les jeux ne sont pas encore fait : mettre un ENORME cierge pour que nos politique rame dans le bon sens (ok =, la on est mal...)
YopLaBoum �a r�pondu le 16/02/2014                                                 � 10:43:
@Info : Ben oui, vous voulez le mettre o� votre ENORME cierge ?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
