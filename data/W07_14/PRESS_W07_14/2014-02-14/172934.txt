TITRE: Airbus se paie une banque allemande
DATE: 2014-02-14
URL: http://www.latribune.fr/entreprises-finance/services/transport-logistique/20140214trib000815430/airbus-se-paie-une-banque-allemande.html
PRINCIPAL: 172859
TEXT:
Airbus se paie une banque allemande
Le groupe Airbus veut racheter la Salzburg M�nchen Bank pour en faire son propre �tablissement bancaire. (DR)
latribune.fr �|�
14/02/2014, 9:03
�-� 201 �mots
Airbus Group a annonc� vendredi la conclusion d'un accord en vue d'acqu�rir la banque allemande Salzburg M�nchen Bank AG dans le but de cr�er son propre �tablissement bancaire.
sur le m�me sujet
L'avionneur europ�en Airbus Group veut sa banque de groupe. Dans un communiqu�, l'entreprise annonce son intention de racheter la banque allemande Salzburg M�nchen Bank pour "�largir les capacit�s de financement du groupe".
Airbus Group Bank
La Salzburg M�nchen Bank, d�tenue � 100 % par le Raiffeisenverband Salzburg, est une banque bas�e � Munich, disposant d'une licence bancaire � part enti�re et servant une client�le de PME ainsi qu'une client�le priv�e, explique le groupe Airbus dans un communiqu� .
Une fois l'op�ration boucl�e, la banque sera renomm�e Airbus Group Bank et aura pour fonction de fournir des solutions de financement � toutes les activit�s du groupe europ�en d'a�rospatiale et de d�fense.
Meilleurs d�lais en 2014
"Airbus Group entend finaliser l'accord dans les meilleurs d�lais en 2014", indique la soci�t� dans le communiqu�, en pr�cisant que la transaction doit encore recevoir l'accord des autorit�s comp�tentes.
"L'acquisition de la Salzburg M�nchen Bank nous offre une base de lancement solide pour notre projet de banque de groupe", a d�clar� Harald Wilhelm, directeur financier d'Airbus Group,�"� l'avenir, l'ensemble du groupe b�n�ficiera ainsi d'une plus grande flexibilit� financi�re".
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Commentaires
Doute �a �crit le 14/02/2014                                     � 22:25 :
Airbus viendrait il � racheter cette banque pour �ponger les dettes et �viter une d�faillance de l'organisme ? La question est ouverte.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
nicolas �a �crit le 14/02/2014                                     � 11:08 :
Soci�t� de droit Hollandais
Airbus est de moins en moins fran�ais...
Ben �a r�pondu le 14/02/2014                                                 � 11:19:
Cela en a toute les apparences, effectivement : diminution de la participation accept�e par l'Etat fran�ais, (qui ne semble donc plus avoir aucun pouvoir d�cisionnaire au sein de la soci�t�), acquisition d'une banque allemande - donc vraisemblablement augmentation du nombre d'actionnaire allemand au capital par �change de parts, soci�t� de droit hollandais... Voil� qui est vraiment tr�s peu rassurant pour notre pauvre pays.
Die Welt �a r�pondu le 14/02/2014                                                 � 13:20:
C'est quand m�me la premi�re banque Autrichienne en Allemagne, au c�ur de Munich sa sp�cialit� transactions bancaires germano/Autrichienne Est ce en pr�vision d'une future taxes de 10 % sur les comptes bancaires�? That is the question�??Robindesbois80// OK nous sommes en train de nous faire d�passer
par d'autres investisseurs, c'est la loi du commerce, Nous nous pr�f�rons taxer, et taxer toujours, notre gestion ressemble de plus en plus � un e d�mocratie populaire
yvan �a r�pondu le 14/02/2014                                                 � 14:25:
"ressemble de plus en plus � un e d�mocratie populaire" Celle-l�, on me l'avait pas encore faite... L�, faut le faire..  J'en reste pantois. Si vous voyez une d�mocratie populaire quelque part sur terre, envoyez-moi un fax, l�. Non, mais, il faut le lire pour pas le croire. Y'en a qui ont tout, mais mal rang�. Vous �tes s�r d'avoir compris un truc, ou c'est tout l'ensemble, qui vous �chappe..??
Die Welt �a r�pondu le 14/02/2014                                                 � 14:58:
Yvan @ c'est de l'humour au premier degr�, les d�mocraties populaires sont tr�pass�es, et j'aime ce sursaut de Airbus pour l'avenir. Dans la vie il faut savoir
rire. Bien que j'ose dire dans la presse �trang�re, que la France est une terre rouge
� esprit f�odal. C est mon point de vue.
@ nicolas �a r�pondu le 14/02/2014                                                 � 18:30:
Airbus n'a jamais �t� fran�ais. Vous oubliez que les Allemands, les Anglais et les Espagnols sont �galement pr�sents. C'est typiquement Fran�ais que de penser que nous sommes le centre du Monde, voire de l'Univers!
Le paysan �a r�pondu le 15/02/2014                                                 � 17:23:
A Yvan, il y a bien un social lib�ral � la t�te du pays .
dd91 �a r�pondu le 15/02/2014 � 19:21:
mais que fait Arnault?? on est bien en train de se faire doubler par le biais de la finance...
Helmut �a r�pondu le 15/02/2014                                                 � 22:51:
L'Allemagne est en train de vampiriser EADS/AIRBUS mais la France d'en haut s'en fout. Le r�veil sera douloureux....
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
thucyd �a �crit le 14/02/2014                                     � 9:58 :
Si une tr�s grande entreprise s'engage dans cette voie, ne serait ce pas un signe de plus que les banques classiques ne font pas leur travail concernant l'�conomie r�elle ?
yvan �a r�pondu le 14/02/2014                                                 � 10:11:
Elles n'ont aucune raison de faire leur "travail" dans le sens o� il rapporte peu. Et puis... elles savent ce qui va se passer.
oui �a r�pondu le 14/02/2014                                                 � 10:27:
oui c'est aussi le signe qu'il faut arr�ter de faire des banques li�es aux �tats, il faut laisser fusionner les banques pour qu'elles aient la taille des industries.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
