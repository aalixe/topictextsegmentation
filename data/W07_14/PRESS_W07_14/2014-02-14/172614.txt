TITRE: Oscar Pistorius sort du silence le jour anniversaire de la mort de sa petite amie - RTL info
DATE: 2014-02-14
URL: http://www.rtl.be/info/monde/international/1069293/oscar-pistorius-sort-du-silence-le-jour-anniversaire-de-la-mort-de-sa-petite-amie
PRINCIPAL: 172611
TEXT:
?
Oscar Pistorius sort du silence le jour anniversaire de la mort de sa petite amie
Alors qu'il ne s'�tait pas exprim� depuis la mort de sa petite amie il y a tout juste un an, Oscar Pistorius a r�agi sur son site internet. Les parents de Reeva Steenkamp, la petite amie, veulent "tourner la page".
14 F�vrier 2014 06h44
R�agir (3)
Tout juste un an apr�s le d�c�s de sa petite amie Reeva Steenkamp, le champion paralympique sud-africain Oscar Pistorius est sorti de son silence. Il qualifie dans une r�action sur son site internet la mort de Reeva Steenkamp d'"accident d�vastateur", �crivent plusieurs m�dias �trangers.
�
Il a toujours affirm� que c'�tait un accident
Oscar "Blade Runner" Pistorius est accus� d'avoir assassin� sa petite amie, Reeva Steenkamp le jour de la Saint-Valentin 2013. Il aurait tir� sur elle � travers la porte de sa salle de bain. Oscar Pistorius a toujours affirm� avoir tu� son amie par accident, pensant qu'elle �tait un cambrioleur.�����
�
"Je porterai en moi la perte de Reeva et le traumatisme de cette journ�e pour le restant de ma vie"
Apr�s un long silence, l'athl�te a � nouveau r�agi � propos de l'affaire. Peu apr�s 01h00, il a post�, pour la premi�re fois en un an, un message sur Twitter, qui renvoyait � une communication sur son site. "Aucun mot ne peut exprimer d'une mani�re adapt�e mes sentiments � propos de l'accident d�vastateur qui a caus� tant de souffrances � tous ceux qui aimaient Reeva - et l'aiment toujours", �crit-il. "La douleur et le chagrin, surtout pour les parents, les proches et les amis de Reeva, me peinent beaucoup. Je porterai en moi la perte de Reeva et le traumatisme de cette journ�e pour le restant de ma vie."�����
Depuis sa lib�ration sous caution le 22 f�vrier 2013, Pistorius n'a pas fait de grandes d�clarations publiques sur l'affaire. Son proc�s se tiendra � Pretoria du 3 au 20 mars.�
