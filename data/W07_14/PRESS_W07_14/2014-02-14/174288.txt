TITRE: Les protagonistes de l�Arche de Zo� condamn�s � du sursis en appel | La-Croix.com
DATE: 2014-02-14
URL: http://www.la-croix.com/Actualite/France/Les-protagonistes-de-l-Arche-de-Zoe-condamnes-a-du-sursis-en-appel-2014-02-14-1106630
PRINCIPAL: 0
TEXT:
petit normal grand
Les protagonistes de l�Arche de Zo� condamn�s � du sursis en appel
Le pr�sident de L'Arche de Zo� et sa compagne ont �t� condamn�s en appel vendredi 14 f�vrier � deux ans de prison avec sursis.
14/2/14 - 16 H 38
PASCAL GUYOT/AFP
Eric Breteau et Emilie Lelouch, de l�ONG l�Arche de Zo�,�en d�cembre 2007�au Tchad.�
PASCAL GUYOT/AFP
Eric Breteau et Emilie Lelouch, de l�ONG l�Arche de Zo�,�en d�cembre 2007�au Tchad.�
Avec cet article
�Les familles d'accueil, autres victimes de l'Arche de Zo�. Les familles d'accueil, autres victimes de l'Arche de Zo�
Ils avaient tent� en 2007 d'exfiltrer du Tchad vers la France cent trois enfants pr�sent�s comme des orphelins du Darfour.
�ric Breteau et �milie Lelouch ne retourneront pas derri�re les barreaux. Si la cour d'appel les bel et bien a condamn�s, vendredi 14 janvier, pour � escroquerie � et � exercice illicite de l'activit� d'interm�diaire � l'adoption �, elle n�a pas retenu la � tentative d'aide � l'entr�e ou au s�jour de mineurs en situation irr�guli�re �.
La peine prononc�e aujourd�hui est donc sensiblement plus l�g�re que celle de premi�re instance (trois ans de prison, dont deux ferme). Les juges ont par ailleurs relax� le logisticien de l'association, Alain P�ligat.
Le d�part vers la France
L'�vacuation des enfants, affubl�s de faux pansements, avait �t� stopp�e net le 25 octobre 2007 lorsque les b�n�voles de l'association avaient �t� arr�t�s en route vers l'a�roport d'Ab�ch�, dans l'est du Tchad.
C�est l� qu�ils devaient faire embarquer les enfants � bord d'un avion. En France, de nombreuses familles les attendaient. L'association avait par ailleurs cach� aux autorit�s tchadiennes le but ultime de l'op�ration. Elle esp�rait par ailleurs, une fois sur le sol fran�ais, faire obtenir aux enfants le statut de r�fugi�.
Des enfants qui n��taient pas orphelins
La plupart des enfants venaient de villages de la r�gion frontali�re entre le Tchad et le Soudan, autour des localit�s tchadiennes d'Adr� et de Tin�. Les enqu�tes men�e par plusieurs ONG � l��poque ont �tabli que la quasi-totalit� des enfants avaient au moins un parent ou un adulte qu'ils consid�raient comme tel.
Tout comme trois autres b�n�voles, �ric Breteau, �milie Lelouch et Alain P�ligat avaient �t� condamn�s au Tchad � huit ans de travaux forc�s pour tentative d'enl�vement d'enfants. La peine avait �t� commu�e en ann�es de prison en France, avant que le pr�sident tchadien Idriss Deby ne prononce une gr�ce en leur faveur .
La-Croix avec AFP
