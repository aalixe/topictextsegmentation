TITRE: Justice : l'�pineux probl�me des drones
DATE: 2014-02-14
URL: http://www.linformatique.org/justice-lepineux-probleme-des-drones/
PRINCIPAL: 175115
TEXT:
Accueil � Technologie � Justice : l��pineux probl�me des drones
Justice : l��pineux probl�me des drones
Publi� par : Hicham ALAOUI 14 f�vrier 2014 dans Technologie
Voter pour ce post
V�ritable succ�s commercial lors des derni�res f�tes de fin d�ann�e, l�utilisation des drones est soumise � une r�glementation stricte, c�est ce que vient de d�couvrir � ses d�pens un lyc�en de Nancy.
Les drones sont aujourd�hui devenus tr�s communs � toute pointe de vue, pour un usage militaire ou civil, mais aussi aupr�s des particuliers. Les drones ont d�ailleurs �t� un v�ritable succ�s commercial lors des derni�res f�tes de fin d�ann�e. Mais voil�, on n�est pas forc�ment libre de faire ce qu�on veut avec un drone, c�est ce que vient de d�couvrir un lyc�en de 18 ans de Nancy.
C�est apr�s avoir mis en ligne une vid�o du survol de la ville de Nancy, vue plus de 400 000 fois, qu�un lyc�en de 18 ans a �t� poursuivi pour � mise en danger de la vie d�autrui �. Convoqu� par le tribunal de la ville, le jeune homme s�est fait informer qu�il avait viol� deux arr�t�s en mati�re d�utilisation d�a�ronefs pilot�s, un qui pr�voit une formation similaire � celle des pilotes d�avion pour les utilisateurs et un autre qui pr�voit une demande pr�alable avant le survol d�un espace urbain.
� cela, il faut bien �videmment encore ajouter toute la probl�matique de la violation de la vie priv�e.
Gr�ce � cette affaire, on d�couvre que l�utilisation des drones est tr�s r�glement�e, ce qui justifierait la poursuite de la proc�dure que le jeune lyc�en.
En attendant sa comparution devant le tribunal correctionnel dans les mois � venir, la vraie question qui se pose est de savoir si toutes les autres personnes qui ont des drones connaissent la r�glementation en vigueur.
Un jeune homme devant la justice pour avoir film� Nancy depuis un drone
Share the post "Justice : l��pineux probl�me des drones"
