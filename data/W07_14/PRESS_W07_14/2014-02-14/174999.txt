TITRE: Le #Foodporn au restaurant, � �viter pour la Saint-Valentin - France-Monde - La Voix du Nord
DATE: 2014-02-14
URL: http://www.lavoixdunord.fr/france-monde/le-foodporn-au-restaurant-a-eviter-pour-la-saint-valentin-ia0b0n1919346
PRINCIPAL: 174998
TEXT:
Le journal du jour � partir de 0,49 �
Saint-Valentin synonyme de restaurant en amoureux ? Souvent oui. Pour ce 14 f�vrier au resto, il va falloir s�abstenir de sortir son smartphone de sa poche pour prendre son plat en photo, et plut�t profiter pleinement de ce moment pass� � deux. Des chefs ont fait savoir qu�ils ne voulaient plus de cette mode, appel�e #Foodporn sur les r�seaux sociaux, notamment Alexandre Gauthier, chef dans le Pas-de-Calais.
Fini le #Foodporn, place aux repas les yeux dans les yeux. PHOTO AFP
- A +
Le ph�nom�ne a pris le nom de #Foodporn sur les r�seaux sociaux. Aucune obsc�nit� dans la pratique, il s�agit tout simplement de prendre en photo un plat concoct� maison, ou au restaurant, et le publier sur les r�seaux sociaux, Facebook ou Twitter. Ce terme, �trange, d�signait � l�origine � la mani�re quasi-�rotique dont les aliments �taient esth�tis�s dans les publicit�s, ou des l�gumes avec des courbes sensuelles� Aujourd�hui le terme � porn � est utilis� dans diff�rents contextes, g�n�ralement pour d�signer des images all�chantes traitant d�un sujet �, explique le blog Emakina .
Mais la mode �nerve particuli�rement les chefs, qui ont pour certains fait savoir, peu avant la Saint-Valentin, qu�ils ne souhaitaient plus que leurs plats soient pris en photo. Cela pour plusieurs raisons.
�
� Vero ??? (@ka0nashi) 10 F�vrier 2014
Propri�t� intellectuelle
Le chef trois �toiles de l�auberge du Vieux-Puits � Fontjoncouse (Aude) Gilles Goujon, explique dans Midi-Libre qu�il est question de propri�t� intellectuelle : � On enl�ve aussi un peu ma propri�t� intellectuelle, on peut �tre copi�. Sans compter qu�une photo prise avec un smartphone pas terrible est rarement bonne. �a ne donne pas la meilleure image de notre travail. C�est emb�tant. �
Difficile de d�connecter
Ce qui agace davantage le chef nordiste Alexandre Gauthier du restaurant la Grenouill�re, � La Madelaine-sous-Montreuil, c�est qu�� o n n�arrive pas � d�connecter les gens �, mais il assure qu�il ne s�agit que d�une minorit� de clients.
� Avant, ils faisaient des photos de famille, de la grand-m�re, et maintenant on fait des photos de plats �, note le jeune chef de 34 ans. � C�est gratifiant, mais nous sommes une maison o� il y a peu d��clairage, donc il faut le flash. Et puis, si � chaque plat, c�est du Stop, on arr�te tout ou Il faut refaire la photo trois fois� On tweete, on like, on commente, on r�pond. Et le plat est froid �
Et l�autre pendant ce temps ?
� Si vous �tes avec votre amoureux, soit �a le fait marrer soit il fait la gueule �, note justement Alexandre Gauthier. � Il y a des moments pour tout. On essaie de cr�er une parenth�se dans la vie de nos clients. Pour �a, il faut d�connecter du portable. �
Quelles solutions ?
� Beaucoup de gens � prennent des photos, � c�est compliqu� d�interdire �, confie Gilles Goujon. � Je cherche une phrase � �crire sur le menu, mais je n�ai pas encore trouv� la bonne formule, qui ne soit pas choquante. �
Alexandre Gauthier a lui repr�sent� un appareil photo barr� sur sa carte. � Les photos ne sont pas interdites, mais je veux cr�er l�interrogation. �
Les chefs fran�ais ne seraient pas les seuls � se plaindre.
Dans le New York Times, des chefs des �tats-Unis d�non�aient r�cemment l�attitude de certains clients, debouts sur leur chaise pour prendre la meilleure photo possible, qui utilisent le flash, voire des tr�pieds en plein restaurant. Cons�quence : quelques-uns interdisent aux clients de prendre des clich�s.
Un apport de pub
Mais � on ne va pas cracher dans la soupe. On est conscient de ce que �a apporte �, rel�ve le chef.
� Les chefs, plus on parle d�eux, mieux c�est �, juge de son c�t� St�phane Riss, du blog Cuisiner en ligne. � Les photos, c�est un acc�l�rateur de visibilit� et donc de chiffre d�affaires. � Les r�seaux sociaux, c�est � une publicit� gratuite � pour les chefs, estime le blogueur, pr�sent sur Facebook, Twitter, Instagram, Google +, Pinterest.
� Il faut vivre avec son temps ! �, se r�signe quant � lui David Toutain, chef chouchou des critiques, qui a ouvert son restaurant en d�cembre � Paris. � Je pense que les r�seaux sociaux m�ont aid� au d�but et m�aident toujours. �a nous fait de la pub �.
�
