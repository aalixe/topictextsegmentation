TITRE: Bourse de Paris : France-Nice Matin int�resse un fonds d'investissement suisse
DATE: 2014-02-14
URL: http://www.zonebourse.com/actualite-bourse/France-Nice-Matin-interesse-un-fonds-dinvestissement-suisse--17951497/
PRINCIPAL: 174704
TEXT:
Bourse de Paris : France-Nice Matin int�resse un fonds d'investissement suisse
14/02/2014 | 18:55
Recommander :
0
Le fonds d'investissement suisse GXP Capital, associ� � un �lu local ni�ois, va prendre la t�te du groupe Nice-Matin en difficult� financi�re, a-t-on appris vendredi aupr�s de l'actuel propri�taire, le groupe Hersant M�dia (GHM).
GXP Capital est notamment pr�sent dans l'h�tellerie de luxe, la finance et la construction navale.
"L'arriv�e de ce nouvel investisseur va permettre de financer et mener � bien la relance des titres de Nice-Matin et d'assurer ainsi leur p�rennit�. Elle marque �galement l'ach�vement de la restructuration de GHM", a d�clar� Dominique Bernard, pr�sident de Nice-Matin, dans un communiqu�.
Le fonds d'investissement, repr�sent� par Gilles P�rin en tandem avec le conseiller g�n�ral et conseiller municipal de Nice Jean Icart, devrait r�cup�rer dans un premier temps 80% des actions du groupe jusque-l� d�tenues par GHM.
Le nouvel actionnaire majoritaire, baptis� "Nice Morning", a l'intention d'apporter un soutien financier de 20 millions d'euros, selon GHM.
Cette somme permettra de "r�aliser le plan de sauvegarde de l'emploi (PSE) que nous avons sign�, de financer les indemnit�s des 127 d�parts tous volontaires et de proc�der aux investissements n�cessaires", pr�cise l'intersyndicale de Nice-Matin/Var-Matin dans un communiqu�.
Selon GHM, l'accord final avec GXP Capital est "en cours de finalisation" avec une "signature en fin de mois".
L''arriv�e d'un nouvel actionnaire majoritaire a �t� approuv�e � l'unanimit� par les �lus et repr�sentants syndicaux de l'entreprise lors de la r�union du comit� d'entreprise, a pour sa part d�clar� l'intersyndicale.
Jean Icart est un farouche opposant du d�put�-maire de Nice Christian Estrosi (UMP). Il s'�tait lanc� dans la course � la mairie de Nice mais a annonc� la semaine derni�re sa volont� de d�missionner prochainement de tous ces mandats �lectoraux afin de se consacrer � la direction du journal.
Le groupe Nice-Matin �dite les quotidiens r�gionaux Nice-Matin (90.000 exemplaires) et Var-Matin (65.000 exemplaires). Il emploie 1.250 salari�s.
Nice-Matin d�tient 50% du capital de la soci�t� Corse Presse, qui �dite Corse-Matin, selon des informations communiqu�es par GHM.    (Matthias Galante, �dit� par Emmanuel Jarry)
Recommander :
