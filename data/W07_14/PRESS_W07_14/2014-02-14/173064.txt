TITRE: "Neknomination" sur Facebook : "C'est comme boire dans une soirée" - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-14
URL: http://tempsreel.nouvelobs.com/vu-sur-le-web/20140213.OBS6339/neknomination-sur-facebook-c-est-comme-boire-dans-une-soiree.html
PRINCIPAL: 0
TEXT:
Publication de Neknomination - France .
"C�est comme une soir�e �tudiante"
Derri�re cette page, il y a Pierre, 19 ans, et J�r�my, 17 ans, tous deux r�sidents de Charente-Maritime. "Quand j�ai vu appara�tre le ph�nom�ne sur Facebook, je me suis dit qu�il y avait quelque chose � faire. Il fallait d�mocratiser �a,�� la mani�re du Harlem Shake ", explique J�r�my. Lui-m�me "nomin�", on peut le voir en chemise grise � carreaux, avec des �carteurs d�oreille et une casquette � l�envers viss�e sur la t�te, en train de boire un "bon shooter" et un verre de whisky-coca .��
"Depuis ce jeudi matin, nous avons re�u une bonne centaine de vid�os [� 14h30, NDLR]�, indique Pierre.
Quant au fait que les vid�os soient diffus�es sans aucune protection de la vie priv�e, Pierre se contente de r�torquer :�
C�est comme lorsqu�on boit devant des centaines de personnes pendant une soir�e �tudiante".�
Les comp�res avouent avoir essuy� �norm�ment de critiques lorsqu�ils ont publi� la vid�o d�un gar�on de 20 ans vidant enti�rement une bouteille de rhum. Comme c�est la publication qui a "fait d�coller la page", ils ne l�ont pas supprim�e. Mais depuis, ils censurent certains contenus, comme les vid�os de personnes qui boivent au volant ou qui ingurgitent des quantit�s d�raisonnables.
Ils avouent recevoir quotidiennement des messages priv�s leur adressant des reproches ou leur faisant la morale. Qu�importe. J�r�my, habitu� du gameplay [commentaire de jeux vid�o sur YouTube , NDLR], ne pr�te plus attention aux commentaires des internautes depuis longtemps. Selon lui, "pour quelqu�un de cens�, la Neknomination n�est pas du tout dangereuse".�
Devant le�risque d'escalade, des pages "Ban Neknomination"
Le risque r�side dans l�escalade de d�fis de plus en plus recherch�s. Le ph�nom�ne aurait d�j� entra�n� la mort de quatre personnes, dont deux le week-end dernier, �g�s de 20 et 29 ans, � Londres et Cardiff, selon les m�dias britanniques . Plus t�t dans le mois, deux irlandais de 19 et 22 ans y avaient succomb� .��
En Irlande, Alcohol Action Ireland a demand� au gouvernement d�agir pour rem�dier au probl�me. Pat Rabbitte, ministre charg� de la communication, a appel� � une interdiction de l�ensemble des vid�os de neknomination. Son appel a �t� rejet� par Facebook. "Nous ne tol�rons pas les contenus directement n�fastes, comme ceux qui sont intimidants, mais un comportement controvers� ou offensant ne va pas n�cessairement � l�encontre de notre r�glement", s�est justifi� un porte-parole du r�seau social aupr�s de " The Independent" .�
Une page Facebook protestant contre ce jeu, intitul�e "Ban Neknomination" , rassemble d�j� plus de 26.000 personnes.�Un groupe fran�ais, "Neknomination - STOP" �a le m�me objectif, mais rencontre pour l�instant un succ�s moindre, avec 300 abonn�s. Elle rapporte notamment l�histoire de Niki Hunter , qui a trouv� son fils de 19 ans inconscient apr�s avoir bu trois bouteilles de spiritueux pour une neknomination. Cette maman a partag� la photo de son enfant couvert de vomi afin d'alerter les parents et de dissuader les gens de participer.�
"On va faire boire un SDF contre de la nourriture"�
Mais Pierre et J�r�my sont bien d�cid�s � "faire le buzz". En bons enfants de la g�n�ration Y, ils savent que la neknomination "sera oubli�e dans un mois". En attendant que la mode passe, ils ne sont pas en manque d�id�es douteuses.
Lorsque "le Nouvel Observateur" les a contact�s par t�l�phone, ils cherchaient � r�aliser une vid�o o� ils feraient boire un verre � un SDF contre la promesse de nourriture. "Je pense que �a va faire une grosse pol�mique", affirme J�r�my.
D�autres ont trouv� le moyen de tirer le meilleur de la Neknomination. Le Sud-africain Brent Lindeque a profit� de sa nomination pour offrir un d�jeuner � un sans-abri, puis a incit� ses amis � faire de m�me. Dans la description de sa vid�o qui a d�j� r�colt� plus de 540.000 vues sur YouTube , il �crit : "Imaginez si nous exploitions la puissance des m�dias sociaux pour effectuer une vraie diff�rence dans la vie des gens". CQFD.
Partager
