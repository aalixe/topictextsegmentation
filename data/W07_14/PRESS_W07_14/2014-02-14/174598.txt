TITRE: Lille : Corchia retourne � Sochaux ! - France Info
DATE: 2014-02-14
URL: http://www.franceinfo.fr/football/lille-corchia-retourne-a-sochaux-1318713-2014-02-14
PRINCIPAL: 174593
TEXT:
Imprimer
sochaux corchia � Panoramic
La conciliation devant le CNOSF dans l'affaire du transfert de S�bastien Corchia vers Lille est all�e dans le sens de la DNCG. Le LOSC ne devrait pas aller devant les tribunaux pour contester cette d�cision.
On ne verra pas tout de suite S�bastien Corchia sous le maillot lillois. En effet, apr�s�le refus de l�homologation du contrat du lat�ral droit chez les Dogues et l��chec de l�appel devant la FFF, c�est au tour de la conciliation devant le CNOSF d��tre en d�faveur du club lillois. En effet, apr�s avoir entendu les repr�sentants du club lillois et S�bastien Corchia pendant plus d�une heure jeudi matin, la Commission de Conciliation du CNOSF a rendu un avis d�favorable � l�homologation de l'engagement du joueur par la DNCG. Dans un communiqu�, le LOSC annonce prendre acte de la d�cision du CNOSF :
� Saisi par le LOSC suite au refus d�homologation du contrat de S�bastien Corchia par la DNCG, le CNOSF a rendu un avis d�favorable.
Apr�s avoir �cout� les arguments des dirigeants lillois, le Comit� National Olympique et Sportif Fran�ais a d�abord soulign� l�effort consenti par le LOSC dans son entreprise de r�duction de sa masse salariale. Il n�a toutefois pas souhait� s�opposer � la d�cision de principe rendue par la DNCG le 5 f�vrier.�Le LOSC prend donc acte de cet avis et le regrette profond�ment, en premier lieu pour le joueur, S�bastien Corchia, premi�re victime de la rigidit� extr�me des institutions. �
Selon L�Equipe, alors qu�un ultime recours en justice devant le Tribunal Administratif �tait une solution �voqu�e par l��tat-major du club lillois, ce ne sera finalement pas le cas. Michel Seydoux a d�clar� : � On s�arr�te l�. Le joueur appartient � Sochaux �. L�affaire est donc close. Sauf �ni�me revirement de situation,�S�bastien Corchia terminera la saison dans le Doubs et non pas dans le Nord. Communiqu� de Sochaux
Le FC Sochaux-Montb�liard prend acte de l�avis du Comit� National Olympique et Sportif Fran�ais, sollicit� dans le cadre de l�homologation du contrat de S�bastien Corchia en faveur du LOSC, qui confirme la d�cision de la DNCG (Direction Nationale du Contr�le de Gestion). Le FCSM est entr� en contact avec les dirigeants du LOSC Lille afin d�envisager rapidement la suite � donner et trouver une solution qui puisse convenir aux deux clubs et au joueur.
