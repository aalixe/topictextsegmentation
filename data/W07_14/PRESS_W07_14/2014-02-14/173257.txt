TITRE: Pourquoi Docteur
DATE: 2014-02-14
URL: http://www.pourquoidocteur.fr/Parfums---Bruxelles-s-attaque-aux-allergies-5413.html
PRINCIPAL: 0
TEXT:
Parfums : Bruxelles s'attaque aux allergies
publi� le 14 F�vrier 2014 par Philippe Berrebi
Les g�ants de la cosm�tique ont trembl�. Un peu � l�image des agences de notation qui menacent de d�grader la note d�un pays pour ses mauvaises performances �conomiques, la Commission europ�enne s�est attaqu�e � l�industrie de la parfumerie. ��Bruxelles vient de lancer un processus d�interdiction de certaines substances, qui va obliger les parfumeurs � revoir la composition de certaines de leurs fragances��, explique le site du quotidien les Echos. Mais que l�on se rassure, Chanel pourra continuer � afficher dans le monde sa meilleure note�: N�5.
1 % � 3 % de la population europ�enne est victime de r�actions allergiques apr�s utilisation de produits de beaut�. Il y une dizaine d�ann�es, d�j�, les fabricants de parfum ont �t� contraints de signaler sur les �tiquettes les 26 substances susceptibles de provoquer des allergies d�s que leur proportion �tait sup�rieure � 0,01 %.
Plus r�cemment, une �tude men�e par la Comit� pour la s�curit� des consommateurs ��avait provoqu� une vent de panique parmi les grands fabricants de parfums��, rappelle le site des Echos, qui craignaient de devoir revoir la quasi-totalit� de leurs fragances. Parmi les mesures envisag�es, plafonner la concentration de 12 substances dont celles qui ont fait � la fois l�histoire de la parfumerie et l�identit� des plus grandes marques.
Bruxelles a finalement succomb� aux effluves des industriels. Trois ingr�dients seulement jugement hautement allerg�nes (le synth�tique HICC et deux essences naturelles extraites de mousses) seront interdits. A charge pour la maison Chanel de trouver la recette magique pour filtrer ces essences de mousse sans enlever la note bois�e de son parfum.
Sur le m�me th�me
