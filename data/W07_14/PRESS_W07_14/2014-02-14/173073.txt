TITRE: Courchevel croit � l'or pour Pinturault - BFMTV.com
DATE: 2014-02-14
URL: http://www.bfmtv.com/sport/courchevel-croit-a-lor-pinturault-710974.html
PRINCIPAL: 173070
TEXT:
Courchevel croit � l'or pour Pinturault
La r�daction
r�agir
Pr�sident du club des sports de Courchevel, o� s�entra�ne Alexis Pinturault, Michel Raffin croit aux chances du Fran�ais de d�crocher la m�daille d�or du super-combin� � l�issue de la manche de slalom (12h30) : � Le contexte est bon, il n�a pas pris trop de temps de retard sur ses principaux concurrents. Il a fait une bonne descente avec une ou deux petites erreurs. Il aurait sans doute pr�f�r� aller plus vite mais on ne va pas revenir l�-dessus. Il est en position de chasseur, une position qu�il aime bien aussi. Je ne me fais pas de soucis pour lui, il a largement les moyens et le ski n�cessaires pour y arriver. Le plus gros danger, c�est lui-m�me. J�ai pronostiqu� qu�il allait gagner aujourd�hui, je suis toujours persuad� qu�il va le faire. � Pinturault a termin� 23e de la descente � 2�44�� du Norv�gien Kjetil Jansrud.�
Toute l'actu Sport
