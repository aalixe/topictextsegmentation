TITRE: Le super-combin� en direct - JO 2014 - Sports.fr
DATE: 2014-02-14
URL: http://www.sports.fr/jo-2014/ski-alpin/articles/super-combine-revivez-la-course-1009819/
PRINCIPAL: 174388
TEXT:
14 février 2014 � 06h42
Mis à jour le
14 février 2014 � 14h28
Suivez en direct le slalom du super-combiné des JO de Sotchi. Alexis Pinturault, Adrien Théaux et Thomas Mermillod-Blondin sont en lice mais sont distancés après la descente.
13h24: Kjetil Jansrud, ultime concurrent à s'élancer n'est que quatrième, à 1"06 de Sandro Viletta. A 23 ans, le Suisse est donc sacré champion olympique du super combiné devant Kostelic et Innerhofer !
13h14: En difficulté sur le haut du tracé, le Croate se reprend bien et récupère du temps en bas de la piste. Ce n'est toutefois pas suffisant, puisqu'il franchit la ligne d'arrivée avec 0"34 de retard sur Viletta ! Il prend la deuxième place provisoirement.
13h05: Si la déception primait chez Alexis Pinturault après sa sortie dans le slalom du super-combiné, le skieur de Courchevel se tourne déjà vers la suite des JO. "Je suis forcément déçu. Je me suis bien mis dans le rythme, j'ai rattrapé du temps sur le haut du tracé. Malheureusement, je passe trop près d'un piquet, il me tape la chaussure et ça me fait enfourcher. Il ne faut pas s'affoler, on verra pour la suite. Ça reste du sport, ça sourit un jour et pas l'autre. Maintenant il faut se reposer, il me reste encore le géant et le slalom. On va doucement se remettre à l'entraînement", a réagi à chaud le Français sur France Televisions.
13h: Bode Miller, proche de la sortie de route, parvient malgré tout à arriver jusque dans l'aire d'arrivée et s'empare de la troisième place, chassant Natko Zrncic-Dim du podium (1"40).
12h59: Après un manche réussie, en avance lors de tous les intermédiaires, Sandro Viletta déloge Adam Zampa de la première place, et de quelle manière ! Le Suisse compte 1"14 d'avance sur le skieur slovaque.
12h52: Le dernier Français engagé, Adrien Théaux, pas spécialiste du slalom, en termine. Il est provisoirement sixième, à 2"32 de Zampa.
12h50: Prétendant au titre, Ted Ligety, en tête après les deux premiers intermédiaires, craque sur la fin du parcours et termine finalement avec 1"05 de retard sur Zampa, leader provisoire.
12h39: Après un très bon haut de parcours, en tête à tous les intermédiaires, Pinturault part à la faute et quitte le tracé ! C'est terminé pour le Français et pour les chances de médaille tricolore...
12h38: Très à l'aise sur le haut du tracé, Mermillod Blondin donne tout et même trop, puisque le Français part à la faute et chute. Le Français passe la ligne d'arrivée sur les fesses...
12h30: Trois Français sont en lice et présents dans les 30 premiers skieurs: Thomas Mermillod Blondin (+2"99), Adrien Théaux (+1"76) et surtout Alexis Pinturault (+2"44). Le skieur de Courchevel est la seule véritable chance de médaille mais sa descente très moyenne de ce matin pourrait bien l'empêcher d'être sacré champion olympique. Il faudra qu'il réalise une très grande manche de slalom pour reprendre du temps sur les meilleurs et notamment sur Svindal et Kostelic, qui partiront avec 1"74 et 1"51 d'avance sur Pinturault.
12h30: La température, bien loin d'être hivernale, pourrait bien être un facteur décisif lors de cette manche de slalom. Il fait actuellement 12°C en Russie, où la course débutera à 15h30 (heure locale). Le tracé risque bien de se dégrader fortement au fil des passages et les premiers partants, comme Thomas Mermillod Blondin (dossard 4) et Alexis Pinturault (dossard 8), pourraient bien être avantagés à ce niveau-là par rapport aux meilleurs temps de la descente.
08h30: Alexis Pinturault, meilleure chance de médaille parmi les trois Français engagés, affiche 2"44 de retard sur le Norvégien Kjetil Jansrud. Mais surtout, il compte 1"51 de retard sur Ivica Kostelic, auteur d'une énorme performance lors de cette descente et désormais grand favori à la victoire finale, mais également 2"30 sur Ondrej Bank, provisoirement deuxième. Le skieur de Courchevel devra réaliser une grande manche de slalom pour espérer accrocher une médaille. Les deux autres Français, Adrien Théaux (+1"76) et Thomas Mermillod Blondin (+2"99) sont respectivement 17e et 25e. Le slalom débutera à 12h30, heure française.
07h59: Adrien Théaux n'y est pas. 1"76 de retard pour le descendeur, 16e. Dur, dur...
07h53: Auteur d'une grosse faute dans le haut du tracé, Bode Miller réalise une performance plutôt moyenne (+1"43). Cela semble insuffisant pour pouvoir espérer conserver son titre ...
07h48: Ligety devant Pinturault. L'Américain est 14e à 1"93, soit avec une demi-seconde d'avance sur Pinturault.
07h46: Kostelic fait une grosse perf'. Le vice-champion olympique termine à moins d'une seconde de Jansrud : 7e à 0"93 et prend une seconde et demie d'avance sur Pinturault. Inquiétant alors que la manche de slalom sera en plus tracée par son père. Ne cherchez plus le prétendant numéro 1 au podium !
07h44: Auteur d'une grosse faute dans le haut du tracé, le Français Thomas Mermillod-Blondin affiche un retard conséquent de 2"99 dans l'aire d'arrivée. Le slalomeur devra cravacher à midi.
07h42: Le Croate Natko Zrncic-Dim, skieur polyvalent candidat à la médaille olympique, termine avec 2"02 de retard sur Jansrud. Une bonne nouvelle pour ... Pinturault.
07h37: Pinturault en termine à 2"44. Il est 13e. L'écart est conséquent mais pas insurmontable... Tout dépend ce que feront Ligety, Miller ou Kostelic.
07h34: Alexis Pinturault s'élance ! Le Français va tenter de limiter la casse avant le slalom.
07h31: Peter Fill ne jouera certainement pas pour la médaille aujourd'hui. L'Italien affiche un retard de 1"74 après cette descente. 
07h29: Belle performance du jeune Matthias Mayer. Quelques jours après son titre olympique en descente, l'Autrichien de 23 ans prend la troisième place provisoire avec seulement 0"37 de retard sur la tête de la course, toujours occupée par Jansrud.
07h25: Baumann finit loin. L'Autrichien est 9e à 2"12.
07h24: Svindal déçoit. Il n'est que cinquième à 70 centièmes de son compatriote Jansrud.
07h22: Aksel Lund Svindal au départ. Moins à l'aise qu'avant en slalom, le Norvégien doit creuser l'écart ce matin en descente.
07h22: Médaillé d'argent dimanche, Christof Innerhofer ne parvient pas à rééditer la même performance ce vendredi matin et compte 1"06 de retard à l'arrivée sur Jansrud et son nouveau temps de référence.
07h18: Kjetil Jansrud prend les commandes. Le troisième de la descente dimanche dernier devance Bank de 14 centièmes.
07h16: Maciej Bydlinski, sorti plusieurs fois de la trajectoire, perd près de quatre secondes (+3"98) sur Ondrej Bank, toujours leader provisoire de ce super-combiné.
07h15:Martin Vrablik finit très loin de Bank, à 2"98. 
07h13: Max Franz, le premier des trois Autrichiens à s'élancer, arrive dans l'aire d'arrivée avec 0"55 de retard sur Bank. Déception pour ce pur descendeur !
07h11: Ondrej Bank, meilleur temps de l'entraînement de jeudi, prend la première place provisoire, avec 0"47 d'avance sur Kilde. Le Tchèque, même s'il n'a pas terminé de slalom en Coupe du monde depuis 2011, est un outsider à la victoire finale.
07h08: Alexander Khoroshilov, un autre skieur ayant plutôt un profil de slalomeur, en termine, avec 2"18 de retard sur Kilde.
07h04: Adam Zampa s'élance. Le Tchèque, plus à l'aise en slalom, finit à 2"38 de Kilde.
07h: C'est parti avec le Norvégien Aamodt Kilde, le premier à s'élancer sur la piste olympique. 1'53"85 pour le tout jeune Norvégien qui s'est fait peur sur quelques sauts...
06h39: Le départ est prévu à 7h. Le premier ouvreur partira à 6h50. 41 portes sont à passer puisque le départ est situé plus bas que lors de la descente de dimanche. Le portillon est à 1947 mètres d'altitude, l'arrivée à 970 mètres pour un dénivelé de 977 mètres sur une longueur de 3219 mètres. Il fait beau et chaud à Rosa Khutor puisque la température est déjà de sept degrés. Neige de printemps au programme donc !
06h38: On rappelle le format de cette épreuve, une descente suivie d'une manche de slalom.
06h35: Voici les dossards des autres favoris. Le Norvégien Aksel Lund Svindal partira avec le n°12, le Croate Natko Zrncic-Dim avec le dossard n°18. Son compatriote Ivica Kostelic suivra avec le 21, juste devant l'Américain Ted Ligety (22). Bode Miller a hérité du n°24.
06h32: En raison de la chaleur, les organisateurs ont décidé d'avance l'heure de la descente du super-combiné messieurs des Jeux Olympiques de Sotchi. Prévue à 11h heure locale (8h en France), celle-ci débutera finalement à 10h (7h heure locale) pour maintenir un état équitable de la piste aux 50 concurrents engagés. Trois Français seront au départ, Alexis Pinturault (dossard 17), Thomas Mermillod-Blondin (dossard 19) et Adrien Théaux (dossard 26).
06h21: Le dernier entraînement de la descente du Super-combiné s’est tenu jeudi matin. Le Tchèque Ondrej Bank en a profité pour réaliser le meilleur chrono devant l’Italien Dominik Paris et l’Américain Bode Miller. Premier Français, Adrien Théaux s’est classé neuvième à plus d'une seconde pendant que Thomas Mermillod-Blondin (17e à 2"22) et Alexis Pinturault (12e à 1"80) prenaient leurs marques sur la piste de vitesse de Rosa Khutor. La descente est prévue vendredi matin avant le slalom à 12h30.
