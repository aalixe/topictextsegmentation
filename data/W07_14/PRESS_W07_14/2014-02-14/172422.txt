TITRE: Saint-Valentin : croyez-vous aux rencontres sur Internet ?
DATE: 2014-02-14
URL: http://www.leparisien.fr/societe/saint-valentin-croyez-vous-aux-rencontres-sur-internet-13-02-2014-3587975.php
PRINCIPAL: 0
TEXT:
Saint-Valentin : croyez-vous aux rencontres sur Internet ?
OUI
R�agir
5490 votants
Malgr� ce qu'affirment les sites de rencontres, pour lesquels l'engouement n'a jamais �t� aussi important, la qu�te pour d�couvrir l'algorithme secret de l'amour n'a malheureusement pas encore abouti.�Et si certains restent optimistes, les c�libataires en qu�te de l'�me s�ur sur internet risquent d'�tre d��us car pour beaucoup, la formule math�matique du coup de foudre n'existe tout simplement pas.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Pourtant, une autre �quipe de chercheurs men�e par Kang Zhao � l'Universit� de l'Iowa (Etats-Unis) aurait tout r�cemment d�couvert une m�thode qui am�liore largement les chances de trouver en ligne un partenaire. Une m�thode�bas�e sur les recommandations effectu�es par les utilisateurs ou leurs go�ts, et pas simplement sur des profils personnels remplis par les abonn�s en qu�te de l'�me s�ur, qui peuvent �tre impr�cis ou incomplets.
Cette d�couverte intervient alors que l'int�r�t pour les sites de rencontres n'a jamais �t� aussi important: selon une �tude de l'Universit� de Chicago, plus d'un mariage sur trois c�l�br� aux Etats-Unis entre 2005 et 2012 �tait issu d'une rencontre en ligne. Certains experts ont toutefois mis en doute les r�sultats de cette recherche, subventionn�e par eHarmony.com, l'un des plus importants sites de rencontres am�ricain.
Et vous, pensez-vous qu'il est possible de rencontrer l'�me s�ur sur la Toile?
LeParisien.fr
R�agir avec mon compte You / le Parisien
Identifiant
Votre e-mail* (ne sera pas visible)
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
D�connexion
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
R�ponse Signaler un abus
lebill 16/02/2014 - 23h54
Un jour, au d�tour d'une rue, en tenant une porte, en se heurtant comme par hasard ou quand deux regards se rencontrent et les yeux se noient dans ceux de l'autre .
R�ponse Signaler un abus
AudreyWB 16/02/2014 - 21h59
Oui je crois aux rencontres sur internet.cela va faire 11 ans que je suis avec mon Mari dont pr�s de 3 ans de mariage. Il faut simplement apprendre � conna�tre les gens avant de se lancer t�te baiss�e dans une relation.
R�ponse Signaler un abus
L'inqui�tude 16/02/2014 - 08h51
Oui je suis pour les rencontres sur le Net et par petites annonces ! sinon aujourd'hui j'aurai fait le grand saut de 105 m�tres !! Quelque temps apr�s la disparition de mon �pouse ne pouvant rest� seul j'ai essay� de trouver une femme compr�hensive attentionn�e comme je l'�tais. Cela a dur� pr�s de 3 ans et 3 �checs mais aujourd�hui cela va faire 11 ans que je vis avec celle qui n�a jamais remplac� l�amour de ma vie (pendant 32 ans) et qui m�a donn� 2 enfants qui eux m�ont apport�s 3 petits enfants que malheureusement elle n�a jamais connu ! A 50 ans apr�s 13 ans de lute contre un cancer elle nous a quitt�e et nous sommes encore tous tr�s tristes et toujours aussi malheureux. Mais voil� nous sommes encore en vie et appr�cions les bons cot�s de la vie alors fallait il ne rien essayer pour rester pour vivre ? Je parle pour moi ! Avez-vous fr�quent�s des discoth�ques ou dancing de � Vieux � ou quand vous rentrez vous avez un long couloir ou toutes ces dames sont assises et vous habillent... (Pardon vous d�shabillent plut�t et font leurs choix ! ! ) Vous avez cela dans la r�gion de Grenoble et pour une personne, homme ou femmes  qui viennent ici pour discuter, faire connaissance et qui ne pense pas qu�au sexe ! C�est d�une tristesse et rien ne vaux les annonces ou les rencontres sur le Web. J�ai vu dans ces lieux des individus qui vraiment n�inspiraient pas confiances et je n�ai jamais remis les pieds dans ces lieux.
annebzh 15/02/2014 - 09h24
Il n'y a pas que les sites de rencontres payants pour trouver quelqu'un. Je suis plut�t d'accord avec Kawa, les bonnes rencontres se font surtout sur des sites o� les personnes partagent d�j� un int�r�t commun. J'ai rencontr� mon mari sur un site de tchat gratuit car je ne voulais pas payer un site de rencontres (apr�s chacun ses choix). Il est clair qu'il y a beaucoup plus de d�ceptions que de bonnes rencontres mais dans un bar ou ailleurs c'est la m�me chose.
duanyer.r 15/02/2014 - 08h09
J'ai trouv� l'amour sur un site de rencontre, cela fait maintenant 10 ans et jusque l� je sortais et je n'avais rencontr� que des, des tordus, des malades, des pervers, alors je dis oui on peut rencontrer l'amour sur internet.
ajcal2011 14/02/2014 - 22h33
Je ne suis pas certain que l'on puisse trouver l'�me s�ur sur un site de rencontre, de mon point de vue, cela se rapproche plus d'une �tale de super march� qu'� autre chose. Ayant quelques notions du fonctionnement des sites internet, je suis convaincu qu'ils sont plus orient�s business que social. Je ne dis pas que ce soit impossible pour autant mais je pense que c'est peu probable. Par contre, sur des sites li�s � nos centres d'int�r�t o� l'on sera � m�me de c�toyer des gens ayant les m�mes go�ts et les m�mes aspirations j'en suis une d�monstration flagrante. Ma petite exp�rience : J'ai rencontr� ma douce sur le net (mais pas sur un site de rencontre). Depuis nous sommes mari�s et avons deux bambins de 5 et 7 ans. Tous les jours depuis maintenant 10 ans nous vivons sur un nuage. D'�vidence nous �tions faits l'un pour l'autre. Elle est mon Elea tout comme je suis son Pa�kan.  Ainsi donc, � tous ceux et celles qui cherchent quelqu'un, je donnerai les conseils suivant : 1/ Faites le point sur vos passions ou vos centres d�int�r�ts, sans vous mentir ou vous surestimer ; 2/ Trouvez un site internet qui rel�ve de ce th�me (c�est pas difficile il y en a des milliers) ; 3/ Communiquez avec les intervenants en toute transparence. Faites lui confiance, cupidon fera son office.  Il n�y a pas de secret, tout est dans la communication, la transparence.
kawa 14/02/2014 - 17h50
J'y crois mais pas sur les sites de rencontre, plut�t sur des sites o� les gens se retrouvent autour d'un int�r�t commun: site de litt�rature, de jeu vid�o, de cin�ma, peu importe... Des sites o� les gens commencent par discuter en plus ou moins grand nombre d'un sujet et o�, au fil du temps finissent par �changer en priv�, se d�couvrir ou non d'autres affinit�s, et �voluent � partir de l�. Les sites de rencontre sont � mon avis un upgrade de tous les chats qui fleurissaient il y a quelques ann�es. Sous couvert de faire des rencontres s�rieuses on s�lectionne en fait un physique qui nous plaise gr�ce aux profils (enfin, quand la photo est bien celle de la personne concern�e et ne remonte pas 15 ans en arri�re) pour passer une ou deux tr�s agr�ables soir�es sportives, mais pas plus.
R�ponse Signaler un abus
charly 14/02/2014 - 16h56
.je pense q'il y a plus de dechets ou beaucoup de d�ceptions a vouloir rencontrer l'amour par �cran interpos� . pour ceux qui ont trouv�s leur bonheur par internet , tant mieux!! . mais je pense aussi le danger que cela peut procurer , sur qui va t'on tomber ? une personne de ma famille en as fait l'am�re exp�rience .
PaTChevaliers 14/02/2014 - 16h49
Trois copines sont pass�es par ses sites, et � chaque fois elles tombaient sur de v�ritables tocards... Mauvais en tout et bon � rien, bref pour des rencontres vraiment durable faut �viter absolument le net...
