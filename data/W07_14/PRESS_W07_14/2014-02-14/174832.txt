TITRE: Ukraine: Lib�ration des manifestants arr�t�s - DH.be
DATE: 2014-02-14
URL: http://www.dhnet.be/actu/monde/ukraine-liberation-des-manifestants-arretes-52fe5c153570c16bb1ccef9f
PRINCIPAL: 174826
TEXT:
Vous �tes ici: Accueil > Actu > Monde
Ukraine: Lib�ration des manifestants arr�t�s
AFP Publi� le
vendredi 14 f�vrier 2014 � 19h10
- Mis � jour le
mardi 18 f�vrier 2014 � 14h33
...
Le pirate de l'air ukrainien voulait "sauver" Ianoukovitch, "otage" de Poutine
Monde
Tous les 234 manifestants ukrainiens arr�t�s ont �t� lib�r�s, mais les poursuites contre eux sont pour l'instant maintenues, a annonc� vendredi le procureur g�n�ral Viktor Pchonka.
"234 personnes ont �t� arr�t�es entre le 26 d�cembre et le 2 f�vrier. Aujourd'hui, plus aucune d'entre elles n'est en d�tention", a d�clar� le procureur selon un communiqu� disponible sur le site internet du parquet. Il a soulign� que les poursuites seraient abandonn�es si ces personnes remplissaient les conditions fix�es par la loi d'amnistie.
"Si les conditions de la loi sont remplies, (...) les poursuites contre elles seront abandonn�es dans un d�lai d'un mois", � compter du 18 f�vrier, a-t-il soulign�.
La loi d'amnistie vot�e en janvier par la majorit� favorable au pouvoir au Parlement, fait d�pendre le sort des manifestants interpell�s de la lib�ration des lieux publics et des b�timents administratifs occup�s par l'opposition.
Sur le m�me sujet :
