TITRE: Fourcade: &quot;Tr�s diff�rent&quot; - JO 2014 - Sports.fr
DATE: 2014-02-14
URL: http://www.sports.fr/jo-2014/biathlon/articles/martin-fourcade-veut-enchainer-1009814/
PRINCIPAL: 0
TEXT:
Avec déjà deux titres olympiques décrochés à Sotchi, Martin Fourcade marche sur les traces d'un Jean-Claude Killy, triplé médaillé d'or aux JO de Grenoble en 1968. (ICON SPORT)
Interview
Par Raphaëlle Peltier , à Sotchi
Publi� le
13 février 2014 � 18h45
Mis à jour le
14 février 2014 � 11h16
Martin Fourcade, champion olympique jeudi sur l'individuelle (en 49'31"7), explique pourquoi il ne vit pas ce titre de la même façon que celui obtenu lundi en poursuite (33'48"6). Sans détour, il assure aussi qu'entrer dans la légende ne l'intéresse pas.
Martin, elle vous fait quel effet cette deuxième médaille d'or ?
C'est un sentiment très différent d'il y a deux jours. Moins d'excitation, moins de pression, mais pas moins de bonheur. Je suis incroyablement heureux, c'est merveilleux. C'est une course individuelle, donc il y a beaucoup moins de tension, moins d'excitation que sur les courses en masse ou en poursuite. Je l'ai abordée vraiment sur un autre registre, mais je ne suis pas moins heureux. On ne va pas commencer à faire la gueule au bout de deux titres olympiques !
Elle a été plus dure à aller chercher ?
Non, je savais ce que j’avais à faire. L'individuelle, c'est une course qui me plaît énormément. Je crois que je n'avais pas perdu depuis les Mondiaux de Ruhpolding (en 2012, ndlr). J'étais conscient de ça avant le départ, c'était une vraie force. Je savais que même avec un tour de pénalité, ça allait le faire. J'ai failli tomber par contre, c'est ce qui aurait pu me coûter un peu cher. Mais la règle du jeu, c'est de rester debout. Et je l'ai fait.
"Je ne me suis jamais battu pour entrer dans l'histoire"
Vous commencez à entrer dans l'histoire du biathlon et du sport français, c'est quelque chose qui compte pour vous ?
Non. C'est beaucoup de bonheur et de fierté d'être comparé à Ole Einar Bjoerndalen ou à Jean-Claude Killy, je suis un vrai fan de sport et je sais ce que ça représente... Mais je ne me suis jamais battu pour entrer dans l'histoire, pour faire partie des livres. Je me bats uniquement pour être heureux et partager ce que je fais avec mes copains, mon équipe. On vit une aventure humaine incroyable et me faire plaisir, c'est vraiment ma seule motivation. J'aime mon métier, je pense que c'est pour ça que je suis bon dans ce que je fais.
Et désormais ?
Wait and see... C'est sûr qu'il y a beaucoup d'attentes. Il reste encore trois courses et j'ai envie de bien faire, comme j'avais envie de bien faire là. Je l'ai dit, ce n'est pas le fait d’avoir un titre olympique qui allait me rendre moins compétiteur. Ça fait partie de mon ADN. Quand je prends le départ d'une course, c'est pour gagner et ça sera toujours pour gagner, quel que soit mon palmarès. Il reste trois belles chances de médaille mais je sais aussi que le compteur peut rester bloqué à deux. Je connais les règles de mon sport, donc je vais continuer à le prendre comme ça. Je pense surtout aux relais, aux courses avec les copains. J'ai envie de me dépasser pour eux.
