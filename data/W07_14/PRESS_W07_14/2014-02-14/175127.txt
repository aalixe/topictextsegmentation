TITRE: Stromae remporte la Victoire du vid�o-clip pour &quot;Formidable&quot; | La-Croix.com
DATE: 2014-02-14
URL: http://www.la-croix.com/Culture/Actualite/Stromae-remporte-la-Victoire-du-video-clip-pour-Formidable-2014-02-14-1106836
PRINCIPAL: 175120
TEXT:
A Evian, retour aux sources pour la Grange au Lac
Stromae a remport� la Victoire du vid�o-clip de l'ann�e pour "Formidable", vendredi lors de la 29e �dition de la c�r�monie.
Stromae �tait nomm� deux fois dans la cat�gorie vid�o-clip, pour "Formidable", mais aussi pour "Papaoutai". Il �tait en lice avec Woodkid pour "I love you".
Le Belge est le grand favori de cette 29e �dition des Victoires avec six nominations, dont trois dans les cat�gories reines : artiste masculin et chanson de l'ann�e ("Formidable" et "Papaoutai").
Film� en cam�ra cach�e par J�r�me Guiot, "Formidable" montre le chanteur qui feint d��tre "fort bourr�", titubant dans les rues de Bruxelles.
Les r�actions des passants vont de l�indiff�rence au voyeurisme primaire, avec une intervention fort aimable de la police.
Stromae a post� la vid�o sur internet plusieurs mois avant la publication de l'album "Racine carr�e", entretenant le flou sur son statut : fiction ou r�alit� ?
Visionn� plus de 52 millions de fois, le clip a contribu� � cr�er un formidable buzz autour du disque.
Les vid�os ont toujours jou� un r�le central dans l'univers de Stromae qui s'est fait conna�tre � ses d�buts par des "le�ons" post�es sur la Toile et dans lesquelles il montrait comment fabriquer une chanson sous forme de pastilles humoristiques.
La soir�e des Victoires de la musique, en direct du Z�nith de Paris, s'est ouverte par une prestation de Stromae avec "Ta f�te".
Zaz, Indochine, Christophe Ma�, Indochine notamment doivent jouer en direct. En revanche, Bertrand Cantat, invit� et nomm� dans la cat�gorie album rock, n'est pas pr�sent.
AFP
