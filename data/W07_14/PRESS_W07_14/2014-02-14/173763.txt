TITRE: Profil Facebook : extension du domaine de lutte des genres
DATE: 2014-02-14
URL: http://www.itespresso.fr/profil-facebook-extension-domaine-lutte-genres-72763.html
PRINCIPAL: 173761
TEXT:
Vous �tes ici : Accueil / March� IT / Profil Facebook : extension du domaine de lutte des genres
Profil Facebook : extension du domaine de lutte des genres
Facebook s�ouvre davantage � la diversit� des genres et des orientations sexuelles avec une option de profil homme, femme ou ��autre��.
Donnez votre avis
Avec Facebook, vous pouvez d�sormais �tre un homme, une femme ou ��autre��. Et il y a une cinquantaine d�hypoth�ses alternatives avec la nouvelle option pour affiner votre profil (��transexuel��, ��bisexuel��, ��intersexuel���). Le r�seau social justifie cette nouveaut� au nom du respect � la diversit� et des modes de vie.
Dans le prolongement, vous pouvez �galement modifier votre pronom personnel en cas de sollicitation sur les contributions. Les classiques d�clinaisons ��lui/il�� et ��elle�� demeurent mais il est d�sormais possible d�opter pour un ��on�� (��they/their�� en anglais) cens� refl�ter une dimension renforc�e de la neutralit�. En l��tat actuel, seule la d�clinaison anglophone est concern�e par cette extension des genres.
Cette nouvelle formule de profil entre clairement dans une initiative militante. ��Si, pour beaucoup, ces changements ont peu d�importance, pour ceux qui en ont souffert, c�est quelque chose de significatif��, �crit Facebook dans une contribution blog sur sa page Diversit�. C�est clairement un geste en faveur de la communaut� homosexuelle et transgenre. Tout en revendiquant le droit d�affirmer ses orientations sexuelles sans complexe.
Le r�seau social a discut� avec des associations de d�fense des droits des lesbiennes, gays, bisexuels et transgenre pour cr�er ces nouvelles options qui peuvent �tre trouv�es dans la cat�gorie ��autres��. ��Nous voyons ces �volutions comme un moyen suppl�mentaire pour faire de Facebook un lieu o� les gens peuvent exprimer librement leur identit頻, clame le r�seau social que l�on a connu plus pudibond dans des affaires de censure de contenus.
Lib�ration.fr rappelait dans un article d�octobre 2013 les tergiversations de Facebook dans ce domaine avec un filtrage parfois abusif de contenus tout � fait acceptables.
Mais cette nouvelle option servant � affiner son profil sur Facebook doit �tre accompagn�e d�un contr�le rigoureux de la confidentialit� des donn�es soumises au r�seau social. On peut �galement se demander dans quelle mesure ces nouvelles informations personnelles serviront les int�r�ts de la publicit� cibl�e.
R�dacteur en chef ITespresso.fr Lire mes autres articles
Autres articles sur ce sujet
