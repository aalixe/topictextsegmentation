TITRE: Zone euro: reprise timide fin 2013, la prudence reste de mise - 14 février 2014 - Le Nouvel Observateur
DATE: 2014-02-14
URL: http://tempsreel.nouvelobs.com/topnews/20140214.AFP0081/zone-euro-la-croissance-fin-2013-devrait-refleter-une-reprise-timide.html
PRINCIPAL: 0
TEXT:
Actualité > TopNews > Zone euro: reprise timide fin 2013, la prudence reste de mise
Zone euro: reprise timide fin 2013, la prudence reste de mise
Publié le 14-02-2014 à 07h31
Mis à jour à 15h35
A+ A-
Bruxelles (AFP) - L'économie de la zone euro a connu une légère accélération fin 2013, une bonne nouvelle qui doit être accueillie avec prudence dans une Europe rongée par le chômage et menacée par la déflation.
D'octobre à décembre, le Produit intérieur brut (PIB) de la zone euro a progressé de 0,3%, après une hausse de 0,1% le trimestre précédent, indique Eurostat. C'est mieux que prévu: les analystes tablaient sur 0,2%.
"Pour la première fois en trois ans, les six premières économies de la zone euro voient leur activité économique progresser, les Pays-Bas en tête (+0,7%) suivis par l'Allemagne et la Belgique (+0,4% chacun)", se réjouit Martin Van Vliet, économiste pour la banque ING.
Deuxième économie de la zone euro, la France a affiché une croissance de 0,3%. En Italie, le rebond a été plus limité (+0,1%), mais le pays a renoué avec une modeste croissance, la première après huit trimestres consécutifs de contraction et un trimestre de stagnation.
Autre signe encourageant, la récession a été moins forte en Grèce (-2,6%), le pays le plus lourdement affecté par la crise.
Cette amélioration générale est principalement due à la bonne tenue des exportations, soutenues par l'Allemagne. Sur les trois derniers mois de 2013, "l'impulsion positive est venue avant tout du commerce extérieur", a confirmé vendredi l'office allemand des statistiques.
- Le moteur de l'investissement -
Mais il semble aussi que "le moteur de l’investissement se soit enfin rallumé, laissant ainsi présager d’une véritable reprise cyclique en zone euro", avance Jean-Christophe Caffet, de Natixis.
"C'est le schéma d'une reprise classique: d'abord le rebond des exportations, ensuite les investissements des entreprises et si tout va bien, la consommation des ménages suit", renchérit son confrère de ING.
La consommation est restée très limitée fin 2013 et a peu de chances de s'améliorer à court terme, compte tenu du niveau du chômage -qui touche 12% de la population- et des politiques d'assainissement budgétaire, souligne Tom Rogers, économiste chez Ernst & Young.
Sur l'ensemble de l'année 2013, la zone euro a été en récession avec un PIB en baisse de 0,4%, un chiffre conforme aux prévisions faites par la Commission européenne à l'automne. Mais elle reste nettement à la traîne par rapport aux Etats-Unis, qui ont affiché une croissance de 0,8% au quatrième trimestre et de 1,9% sur toute l’année 2013.
Même constat avec le Royaume-Uni, qui affiche une santé insolente avec une croissance de 0,7% fin 2013 et de 1,9% sur l'ensemble de l'année. Pour 2014, la Banque d'Angleterre (BoE) vient même de relever sa prévision de croissance et table désormais sur une hausse de 3,4%. Loin des quelque 1% attendus pour la zone euro.
- Menace de déflation -
En outre, de nombreux risques pèsent sur la qualité de la reprise: les turbulences sur les marchés émergents, la vigueur de l'euro et le processus d’évaluation de la qualité des bilans bancaires en zone euro, énumèrent les analystes.
Autre menace: la déflation, synonyme de baisse des prix, des salaires et au final, de l'activité. "Avec un PIB près de 3% en-deçà de son pic de 2008", le risque est important, "en particulier dans les pays les plus fragiles", estime Jonathan Loynes de Capital Economics.
Moins spectaculaire que l' inflation galopante, la déflation est tout aussi dangereuse car elle crée un cercle vicieux dont il est difficile de sortir: face à des prix qui baissent, les consommateurs diffèrent leurs achats, les entreprises réduisent leur production et finissent par baisser les salaires voire supprimer des emplois. Résultat: la machine économique est complètement grippée.
La directrice générale du Fonds monétaire international (FMI), Christine Lagarde, a récemment mis en garde contre "des risques croissants de déflation qui pourraient être désastreux pour la reprise", affirmant que cet "ogre doit être combattu".
Mais cette analyse n'est pas partagée par tous. Le président de la Banque centrale européenne (BCE), Mario Draghi, vient de récuser ce scénario. "Il n'y a pas de déflation en zone euro", a-t-il assuré la semaine dernière.
Partager
