TITRE: La police tha�landaise �vacue des sites occup�s par les manifestants | Mediapart
DATE: 2014-02-14
URL: http://www.mediapart.fr/journal/international/140214/la-police-thailandaise-evacue-des-sites-occupes-par-les-manifestants
PRINCIPAL: 173091
TEXT:
La lecture des articles est r�serv�e aux abonn�s.
Des milliers de policiers anti-�meutes d�ploy�s dans Bangkok ce vendredi ont d�gag� plusieurs sites occup�s par les manifestants de l'opposition, apr�s des semaines d'affrontements qui ont fait plus de 10 morts.
Les policiers ont repris facilement les zones autour du si�ge du gouvernement et de trois autres minist�res, que la plupart des manifestants semblaient d�j� avoir quitt�, selon des journalistes de l'AFP.Police also at the government complex at Chaeng Wattana (Via @Nalinee_PLE) pic.twitter.com/xILDePtQD8 #Bangkok� Richard Barrow (@RichardBarrow) 14 F�vrier 2014
La premi�re ministre Yingluck Shinawatra et son gouvernement, cibles des manifestations qui bloquent une partie du pays, n'ont pu, depuis pr�s de deux mois, utiliser le si�ge du gouvernement, Government ...
