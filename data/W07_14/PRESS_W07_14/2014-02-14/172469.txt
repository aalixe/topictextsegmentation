TITRE: 49�% des Fran�ais trouvent les objets connect�s encore trop chers
DATE: 2014-02-14
URL: http://www.lesnumeriques.com/49-francais-trouvent-objets-connectes-encore-trop-chers-n33217.html
PRINCIPAL: 172468
TEXT:
Nous soutenir, navigation sans publicit� Premium : 2�/mois + Conseil personnalis� Premium+ : 60�/an
49�% des Fran�ais trouvent les objets connect�s encore trop chers
Une enqu�te r�v�le des pistes � suivre
�
Publi� le: 13 f�vrier 2014 17:16
Par Christophe S�frin
Tweet
Les sondages autour des objets connect�s se suivent, et ne contredisent pas la tendance�: les Fran�ais placent beaucoup d�espoirs dans les objets connect�s, mais ils les trouvent trop chers...
Encore un sondage sur les objets connect�s�! Le dernier en date a �t� r�alis� les 6 et 7�f�vrier par BVA pour le Syntec num�rique*. D�apr�s l�enqu�te, pr�s d�un quart des Fran�ais utiliseraient d�j� des objets connect�s, tels des thermom�tres, des balances, des cam�ras de surveillance, des bracelets fitness etc. 23�% s'en serviraient pour faciliter leur vie quotidienne, 21�% pour surveiller leur sant�, 20�% pour se d�placer, 14�% pour se prot�ger et 9�% pour suivre leurs performances sportives.
�
Envoyez-moi un mail lorsque le prix descend sous :
�
�
�
Mais lorsque l�on interroge les Fran�ais sur les fonctionnalit�s qu�ils souhaiteraient le plus voir se d�velopper autour des objets connect�s, deux cat�gories d�usage soul�vent�principalement l�enthousiasme. Nous serions ainsi 64�% � placer de nombreux espoirs dans les objets connect�s autour de la sant�, et 60�% autour de la s�curit�. Dans une moindre mesure, la vie quotidienne "chez soi" est �voqu�e par 30�% des personnes interrog�es, suivie par la mobilit� (22�%) et le sport (6�%).
Les derni�res donn�es concernent la perception des objets connect�s. ��20�%, les Fran�ais voient en eux un progr�s, � 15�%, un progr�s mais entra�nant des difficult�s d�usage, et � 49�% un progr�s mais avec des prix trop �lev�s. Seuls 12�% de nos compatriotes y voient un danger�
* Sondage r�alis� en partenariat avec 20�Minutes, O1Business et BFM�Business, aupr�s d�un �chantillon repr�sentatif de 973�personnes �g�es de 18�ans et plus.
