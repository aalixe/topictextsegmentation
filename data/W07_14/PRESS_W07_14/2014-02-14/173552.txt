TITRE: Veolia-Pas d'intervention dans le choix des dirigeants-Matignon - LExpress.fr
DATE: 2014-02-14
URL: http://lexpansion.lexpress.fr/high-tech/veolia-pas-d-intervention-dans-le-choix-des-dirigeants-matignon_428933.html
PRINCIPAL: 173551
TEXT:
Veolia-Pas d'intervention dans le choix des dirigeants-Matignon
Par Reuters, publi� le
14/02/2014 � 12:11
, mis � jour � 12:11
Selon le site de l'hebdomadaire Le Point, le PDG de Veolia, dont le mandat arrive � �ch�ance en avril prochain, serait menac� d'�tre remplac� � la t�te du groupe de services collectifs par David Azema, un haut fonctionnaire du minist�re de l'Economie.�
Le groupe familial Dassault, qui d�tient une participation d'environ 6% dans le capital de Veolia, serait parvenu � convaincre certains administrateurs de le remplacer et l'Etat aurait d�cid� de ne pas s'y opposer, selon l'hebdomadaire.�
"Matignon, agac� par l'intransigeance du PDG dans l'�pineux dossier du sauvetage de la compagnie de ferry SNCM, pourrait laisser faire", �crit Le Point.�
"Cette citation n'a aucun fondement", a-t-on d�clar� � Reuters dans les services du Premier ministre. "Matignon n'intervient pas dans le choix des dirigeants des entreprises priv�es", a-t-on ajout�.�
Le groupe Dassault n'a pas fait de commentaire dans l'imm�diat. (Julien Ponthus avec la contribution de Jean-Michel B�lot, �dit� par Matthieu Protard)�
Par
