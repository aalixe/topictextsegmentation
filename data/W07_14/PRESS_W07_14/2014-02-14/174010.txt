TITRE: La meilleure �quipe pour le meilleur r�sultat :: Lensois.com
DATE: 2014-02-14
URL: http://www.lensois.com/2014/02/14/63891-la-meilleure-equipe-pour-le-meilleur-resultat
PRINCIPAL: 174006
TEXT:
La meilleure �quipe pour le meilleur r�sultat
vendredi 14 f�vrier 2014 � 09h04
R�action(s)
Ce jeudi, en pr�ambule du 8e de finale de Coupe de France opposant le RC Lens � Lyon au stade de Gerland (1-2 ap), nous vous demandions quel type de formation devait aligner Antoine Kombouar� pour cette affiche. Bien que la priorit� des Sang et Or r�side cette saison dans le championnat et la mont�e en Ligue 1, vous esp�riez � une majorit� de 57% voir le meilleur onze possible. Vous avez �t� exhauss�s�! Et en plus avec un exploit � la cl�!
Ce vendredi, revenons sur cette qualification mais surtout le quart de finale programm� le mardi 25 ou mercredi 26 mars prochain. Pensez-vous que le RC Lens puisse se qualifier � Monaco�? Oui�? Ce n�est pas impossible�? Non�?
Si vous souhaitez participer � notre sondage, rendez-vous en haut � droite de notre page d�accueil. En revanche, si vous souhaitez discuter de ce sujet, connectez-vous en cliquant sur l�onglet � IntenseDebate � ou utilisez vos profils � Twitter � ou � Facebook �. Sinon, le cas �ch�ant, � inscrivez-vous � si vous n'�tes pas encore inscrit.
Rejoignez Lensois.com sur Facebook en cliquant ici
