TITRE: Air France autorise les appareils �lectroniques pendant le vol
DATE: 2014-02-14
URL: http://www.igen.fr/0-apple/air-france-autorise-les-appareils-electroniques-pendant-le-vol-110123
PRINCIPAL: 174113
TEXT:
Air France autorise les appareils �lectroniques pendant le vol
?
14/02/2014 - 16:30 - Nicolas Furno
Bonne nouvelle pour tous les habitu�s des trajets en avion sur les lignes Air France : la compagnie a�rienne autorise � partir d�aujourd�hui l�utilisation de tous les appareils �lectroniques personnels pendant tout le vol. Jusque-l�, il fallait �teindre son smartphone ou sa tablette pendant les phases de d�collage et d�atterrissage, ce qui �tait toujours un petit peu p�nible quand on �coutait de la musique ou regardait un film.
Cette autorisation s�accompagne de quelques contraintes malgr� tout�: les appareils en question devront �tre en mode avion et ne pas communiquer avec le r�seau cellulaire naturellement, mais il faudra aussi couper le Wi-Fi et le Bluetooth. Et en cas de probl�me, les passagers auront toujours comme consigne d��teindre totalement leurs appareils.
Ce changement n�est pas r�serv� � Air France. Comme le rappelle Le Monde , la compagnie allemande Lufthansa autorisera aussi l�utilisation des appareils �lectroniques � partir du premier mars. Et comme le note un lecteur, la compagnie a�rienne suisse Swiss acceptera aussi les appareils �lectroniques dans le courant de l'ann�e.
vache folle [14.02.2014 - 16:35] via MacG Mobile
Tout comme Swiss (20min.ch)
"La compagnie a�rienne Swiss va autoriser ses passagers � utiliser leurs tablettes et smartphones pendant le vol, ainsi qu'au d�collage et � l'atterrissage."
diegue [14.02.2014 - 16:36]
Pour ce qui est de mon iPhone 4S, vu que je dois maintenant le charger 2 fois par jour il est plus s�rieux de le fermer sur un vol transatlantique quand on veut t�l�phoner � l'arriv�e !
Bon c'est une bonne nouvelle pour l'iPad.
azgard [14.02.2014 - 16:55]
Moi j'en profite au contraire pour recharger mes appareils grace a la prise usb situ�e sur le siege.
flapy [14.02.2014 - 16:43]
Bonne nouvelle !
Je ne compte plus le nombre de fois o� je me suis fait sermonner par les h�tesses parce que j'�coutais de la musique pendant le d�collage !!
@flapy :
Normalement la musique �a n'as rien a voir, c'est que l'on doit �couter les consignes de s�curit�.
Par contre dans les films am�ricains ils ont du Wi-fi dans l'avion
eseldorm [14.02.2014 - 16:47]
"La premi�re priorit� en termes de s�curit� des vols est que les passagers soient attentifs aux annonces et aux consignes des �quipages"
utilisation en mode avion au sol "OUI, en respectant les consignes de s�curit�"
bref, a coup sur, il faudra que ce soit eteint pendant l'ennonciation des consignes de securite. et que l'utilisation de l'appareil sera conditionne a la non utilisation de la tablette du siege pendant le decollage et l'atterissage.
ce qui est etrange c'est l'autorisation du casque pendant les phases les plus critiques de decollage et d'atterissage. avec un casque (voir anti bruit), les bouchons d'oreilles (pas electronique) et les deux combines (le pied !), le passager ne risque pas d'entendre grand chose si il y a une annonce !
tout ca pour dire : mon telephone n'est pas toujours en mode avion quand il est dans le coffre a baggage... et que j'ai toujours ecoute de la musique des que les PNC sont attaches.
D0D [14.02.2014 - 16:55]
Ben pour ma part, du vol Nice - Lyon de Vendredi dernier, les casques et �couteurs ont �t� interdits pendant l'atterrissage.
Du coup, on doit donc arr�ter d'�couter la musique ou son film.
Ceci dit, je comprends quand m�me: Le fil peut retenir ou s'emm�ler en cas de probl�me.
Logique, c'est une d�cision des autorit�s a�riennes Europ�enne et am�ricaine datant de Novembre dernier.
Pour info, c'est le cas aussi chez British Airways depuis d�but Janvier.
M�me si je ne pige toujours pas le pourquoi c'est possible maintenant et pas avant
Il faut bien dire que ce n'�tait pas une contrainte dramatique
Link1993 [14.02.2014 - 17:52] via MacG Mobile
@alan63 :
A l'origine pour les ondes, c'�tait parce que certaines compagnies avaient de vieux avions qui ne supportaient pas ou mal les ondes �mise par n'importe quoi. Un avion maintenant est capable de supporter ceux �mit par un orage (enfin, th�oriquement). Ensuite lors de la phase de d�collage et d'atterrissage, c'�tait surtout pour comme dit plus haut, que l'on soit plus attentif lors de ces phases a risque ;)
Ali Baba [14.02.2014 - 18:06] via MacG Mobile
@alan63 :
Parce que les avions sont tous blind�s maintenant. Ce n'�tait pas encore le cas il y a dix ans.
oomu [14.02.2014 - 18:33]
�a ne reposait pas sur grand chose de technique
disons que c'�tait plus culturel, un h�ritage d'avant que �a se g�n�ralise et qu'on tienne compte de ces usages.
Cela fait depuis de nombreuses ann�es que l'on savait que c'�tait sans aucune cons�quence mais on n'avait gu�re de raisons de s'en pr�occuper.
Mais wifi, en r�alit�, on commen�ait � le trouver en usage depuis 10 ans. M�me une console de jeu faisait du wifi. Si un avion �tait tomb� chaque fois qu'un gamin oubliait sa console ben...
C'est pas tant une contrainte mais plut�t un archa�sme,  �a �vitera aux h�tesses une corv�e.
-
y a 3 mois on m'a demand� en plein vol si ce n'�tait pas dangereux que je lise Le Monde sur un ipad.
Le discours du "boh en fait c'est pas dangereux, mais faut pas l'allumer pendant le d�collage, mais apr�s oui, m�me si c'�tait pas dangereux avant hein, et aussi pendant l'atterrissage, m�me si on craint rien hein !" , �a laissait les gens dans la confusion et l'angoisse.
Donc, on arr�te les conneries, on dit que �videmment un appareil �lectronique ne d�range pas et on le met en mode avion: �a �conomise la batterie, �a �vite de saturer le �ventuel WIFI de l'AVION (ben vi) et basta!
discours : "zero soucis, coupez votre r�seau, bon vol". simple et clair.
Ralph_ [14.02.2014 - 17:36]
toujours mis en mode avion...apr�s j'enlevais les �couteurs et basta.
Au moins, ils nous emb�teront plus et je pourrais m'endormir en musique avant m�me le d�collage :)
