TITRE: Megan Fox a accouch�, elle est maman pour la deuxi�me fois - Voici
DATE: 2014-02-14
URL: http://www.voici.fr/news-people/actu-people/megan-fox-a-accouche-elle-est-maman-pour-la-deuxieme-fois-521275
PRINCIPAL: 173211
TEXT:
Publi� le vendredi 14 f�vrier 2014 � 10:02                              par M-A.K.
Megan Fox a accouch�, elle est maman pour la deuxi�me fois
Encore un gar�on !
En direct des US : Megan Fox est enceinte, John Travolta se travestit en femme
24/05/2012
Et de deux pour Megan Fox. L��pouse de Brian Austin Green a donn� naissance � son second fils.�
Cette fois-ci, le secret n�aura pas �t� aussi bien gard� que pour sa premi�re grossesse�: Megan Fox a accouch� de son deuxi�me gar�on, a r�v�l� le site Too Fab . Mais pour le moment, aucune autre information n�a filtr�, ni la date de naissance exacte, ni le pr�nom du petit bout de chou.
Lors de la naissance de Noah, le premier enfant de Megan Fox et Brian Austin Green , le 27 septembre 2012, le faire-part de naissance nous �tait parvenu avec trois semaines de retard. Vingt jours pendant lesquels le couple avait ��eu la chance de profiter de quelques semaines de tranquillit� � la maison ��, avait �crit l�heureuse maman sur Facebook.
Moins d�un an apr�s son accouchement, la star de 27 ans confirmait, via son porte-parole, qu� elle �tait de nouveau enceinte . En novembre dernier, � trois mois de son terme, elle avait r�v�l� � ABC News que son premier trimestre avait �t� ���prouvant � . ��Mais une fois que vous �tes entr� dans le deuxi�me, �a va beaucoup mieux��, avait-elle assur�. Contrairement � la premi�re fois, elle n�a pas eu le temps de profiter de sa grossesse�:���Quand vous avez un enfant d�un an qui court partout et que vous travaillez seize heures par jour, vous n�avez pas vraiment de temps pour vous��, avait-elle r�v�l�.
� 40 ans, Brian Austin Green devient ainsi papa pour la troisi�me fois. Il a d�j� un gar�on de onze ans, Kassius, n� de sa relation avec l�actrice Vanessa Marcil.�
Mots-cl�s :
