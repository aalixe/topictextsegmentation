TITRE: Facebook : de nouvelles options pour d�finir le genre
DATE: 2014-02-14
URL: http://www.boursier.com/actualites/economie/facebook-de-nouvelles-options-pour-definir-le-genre-23027.html
PRINCIPAL: 174826
TEXT:
Facebook : de nouvelles options pour d�finir le genre
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � Facebook veut donner la possibilit� � ses utilisateurs - anglophones dans un premier temps - d'�tre "eux-m�mes"... Sur sa page "diversit�" aux couleurs du drapeau arc-en-ciel, symbole du mouvement LGBT, le r�seau social a annonc� jeudi l'inclusion de nouvelles options pour caract�riser le genre.
S'exprimer en toute s�curit�
Le groupe de Mark�Zuckerberg va plus loin encore puisqu'il donne la possibilit� � ses abonn�s de pr�ciser le pronom avec lequel ils seront interpell�s dans les messages post�s sur leur mur (il/elle/leur...). Les utilisateurs garderont le contr�le sur ces informations et pourront d�cider de choisir les personnes avec lesquelles ils souhaitent les partager.
Dans un communiqu�, le " transgender law center " s'est f�licit� de cette annonce. "C'est une mise � jour bienvenue qui permer aux utilisateurs d'�tre authentiques et de s'exprimer en toute s�curit�". Son concurrent Google Plus n'avait pas �t� aussi loin proposant seulement, "autre", � c�t� "d'homme" ou de "femme".
Marianne Davril � �2014, Boursier.com
