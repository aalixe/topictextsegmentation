TITRE: Diplomatie | Centrafrique : rencontre Hollande-Déby Itno ce vendredi à Paris | Jeuneafrique.com - le premier site d'information et d'actualit� sur l'Afrique
DATE: 2014-02-14
URL: http://www.jeuneafrique.com/Article/ARTJAWEB20140214111933/
PRINCIPAL: 173537
TEXT:
Le nom de votre ami *
Adresse Email de votre ami *
Message *
Tous les champs marqu�s * sont obligatoires
François Hollande le 3 février 2014 à l'Elysée à Paris. � AFP
Le pr�sident fran�ais Fran�ois Hollande et son homologue tchadien Idriss  D�by Itno �voqueront la situation en Centrafrique lors d'une rencontre,  vendredi, � Paris.
La crise en Centrafrique sera au centre d'un entretien vendredi 14 f�vrier � Paris entre le pr�sident fran�ais Fran�ois Hollande et son homologue tchadien Idriss Deby Itno.
"Cette rencontre, au caract�re inopin� et annonc�e � quelques heures de sa tenue, est pr�vue � 17H00 GMT", a pr�cis� la pr�sidence dans un communiqu�. Elle aura lieu apr�s un Conseil de d�fense consacr� � la situation en Centrafrique, r�unissant autour de Fran�ois Hollande dans la matin�e responsables militaires et ministres concern�s (Affaires �trang�res, D�fense).
>> � lire aussi : Hollande demande � l'ONU d'acc�l�rer l'envoi de Casques bleus
Cette r�union intervient apr�s que le chef de l'�tat a demand� jeudi � Ban Ki-moon "d�acc�l�rer la pr�paration d�une op�ration de maintien de la paix" de l'ONU en Centrafrique, lors d'un entretien t�l�phonique avec le secr�taire g�n�ral des Nations unies.
Paris dispose de 1 600 militaires sur le terrain, qui �paulent une force de l'Union africaine de plus de 5 000 hommes, et jusqu'� pr�sent refuse d'augmenter son contingent. Le Tchad de son c�t� comprend aussi un fort contingent en Centrafrique mais rechigne � voir la pr�sence militaire africaine se transformer en op�ration de maintien de la paix de l'ONU.
