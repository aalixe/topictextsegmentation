TITRE: Arche de Zo� : deux ans de prison avec sursis en appel pour Breteau et Lelouch
DATE: 2014-02-14
URL: http://www.parisdepeches.fr/2-Societe/128-75_Paris/9071-Arche_Zoe_Deux_prison_avec_sursis_appel_pour_Breteau_Lelouch.html
PRINCIPAL: 0
TEXT:
Arche de Zo� : deux ans de prison avec sursis en appel pour Breteau et Lelouch
Publi� le 14/02/2014�Par Roxane Bayle
Toucanradio - Flickr
Les deux responsables de l'Arche de Zo�, Emilie Lelouch et Eric Breteau ont �t� condamn�s � deux ans de prison avec sursis, pour escroquerie, exercice illicite de l'activit� d'interm�diaire � l'adoption et tentative d'aide � l'entr�e ou au s�jour en situation irr�guli�re.
C'�tait en 2007 : Eric Breteau, sa compagne Emilie Lelouch et les membres de l'association l'Arche de Zo�, avaient tent� d'amener en France 103 enfants du Tchad pr�sent�s comme des orphelins du Darfour. Selon des organisations, ces enfants avaient presque tous au moins un parent encore en vie. Ils avaient �t� arr�t�s et condamn�s � huit ans de travaux forc�s pour tentative d'enl�vement d'enfants. Condamnation qui avait �t� commu�e en peine de prison en France avant que le pr�sident Idriss Deby ne les gracie.
Pour Eric Breteau, l'op�ration �tait "parfaitement l�gale"
En France, les deux responsables de l'association l'Arche de Zo� ont boycott� leur premier proc�s devant le tribunal correctionnel de Paris, au terme duquel ils avaient �t� condamn�s � deux ans de prison ferme. Lors du proc�s en appel, l'avocat g�n�ral Etienne Madranges avait requis une peine de deux � trois ans de prison sans qu'ils ne retournent en prison. Il avait �galement �voqu� l'obligation de rembourser les familles d'accueil qui avaient financ� l'op�ration et m�me un suivi psychologique.
Les magistrats ont donc suivi les recommandations de l'avocat g�n�ral en condamnant Emilie Lelouch et son compagnon � deux ans de prison avec sursis aujourd'hui. Si Eric Breteau se dit "100% responsable" de l'�chec de l'association, il clame encore son innocence pour son op�ration, qu'il qualifie de "parfaitement l�gale".
La Cour d'appel a relax� le logisticien de l'association, Alain P�ligat.
�
Tweeter
R�agir
Si vous souhaitez voir votre commentaire appara�tre directement sur le site sans attendre la validation du mod�rateur, veuillez vous identifier ou cr�er un compte sur le site Paris D�p�ches.
