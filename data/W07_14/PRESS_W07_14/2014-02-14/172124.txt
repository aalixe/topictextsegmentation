TITRE: Angry Birds Stella annonc�, disponible d�s cet automne | iPhoneAddict.fr
DATE: 2014-02-14
URL: http://iphoneaddict.fr/post/news-119454-angry-birds-stella-annonce-disponbile-automne
PRINCIPAL: 172121
TEXT:
Angry Birds , Jeu
Rovio avait annonc� la couleur sur son compte Twitter en annon�ant l�arriv�e d�un nouvel �pisode d�Angry Birds. Peu � si ce n�est aucun d�tail n�avait �t� partag�. Depuis, le d�veloppeur est un peu plus bavard. Il annonce Angry Birds Stella, un �pisode assez orient� vers le public f�minin. Il ne s�en cache pas et le reconnait sur son blog . Cet �pisode mettra en avant Stella, un oiseau f�minin qui peut s�av�rer m�chant selon les circonstances. Elle sera accompagn�e de ses amies qui sont aussi ses ennemies.
Pour l�instant, Rovio ne d�voile que ce morceau de l�histoire et une image pour faire patienter. Il promet cependant qu�Angry Birds Stella va ouvrir les portes d�un tout nouvel univers non encore exploit� dans la s�rie Angry Birds. Pour l�instant, la majorit� des �pisodes est rest�e identique, si ce n�est le dernier en date, Angry Birds Go! qui se veut �tre un jeu de kart.
Angry Birds Stella �tait annonc� pour arriver bient�t. Au final, il ne sera disponible qu�� partir de l�automne prochain. Bien �videmment, aucun prix n�a �t� communiqu� en l��tat.
Int�ressant ? Partagez la news !
