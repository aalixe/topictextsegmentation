TITRE: 50 Shades of Grey : Dakota Johnson trouve Anastasia Steele ennuyeuse | melty.fr
DATE: 2014-02-14
URL: http://www.melty.fr/50-shades-of-grey-dakota-johnson-trouve-anastasia-steele-ennuyeuse-a253167.html
PRINCIPAL: 172188
TEXT:
il y a 55 jours
Publi� le 13/02/2014 15:36
Humour inappropri� ou v�rit� ? Dakota Steele n'a pas l'air emball�e par son r�le d'Anastasia Steele dans 50 Shades of Grey. melty.fr vous dit tout.
Katie Cassidy a r�pondu � la rumeur pour son r�le de Leila dans 50 Shades of Grey , comme vous le relatait melty.fr. Aujourd'hui c'est � Dakota Johnson de prendre la parole dans Vanity Fair et le moins que l'on puisse dire, c'est qu'elle va cr�er la pol�mique : "Ce r�le consiste beaucoup � entrer dans la t�te d'Ana, avant qu'elle rencontre Christian, donc je dois beaucoup lire, ce que j'adore, mais elle �tudie la litt�rature anglaise, donc c'est plut�t ennuyeux". Un avis que ne partagent pas les millions de lecteurs de la saga �rotique. Dakota Johnson �tait-elle un bon choix pour incarner Anastasia Steele, si elle ne prend pas au s�rieux le personnage ?
Plus d'actu sur 50 Shades of Grey 50 Shades of Grey : Katie Cassidy r�pond � la rumeur sur son r�le de Leila
Le casting sera-t-il � la hauteur ?
Mais Dakota Johnson, en photos sur le tournage de 50 Shades of Grey , est plus enthousiaste pour ce qui concerne le tournage : "C'est une exp�rience cool, fun et cr�ative. Quand je me suis engag�e, j'avais pleins de peurs mais aucune ne s'est r�alis�e. On s'entend tous tr�s bien et l'�quipe est g�niale. On essaie de rendre ce projet le meilleur possible". Reste � voir quel sera le r�sultat une fois que le film sera mis en bo�te.
Que pensez-vous de ces d�clarations ?
