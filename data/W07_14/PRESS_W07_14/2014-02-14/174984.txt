TITRE: Municipales 2014 : � Ch�teauroux, le candidat FN au tatouage nazi exclu de la liste | Planet
DATE: 2014-02-14
URL: http://www.planet.fr/politique-municipales-2014-a-chateauroux-le-candidat-fn-au-tatouage-nazi-exclu-de-la-liste.553468.29334.html
PRINCIPAL: 174981
TEXT:
Municipales 2014 : � Ch�teauroux, le candidat FN au tatouage nazi exclu de la liste
Publi� par Sacha Liezer le Vendredi 14 F�vrier 2014 � 16h57
R�agissez ! - 83 commentaires
Bastien Durocher, colistier FN � Ch�teauroux (Indre) pour les �lections municipales de mars prochain, a �t� �vinc� de la liste du parti pr�sid� par Marine Le Pen. Cette exclusion est d�e � des clich�s diffus�s sur Internet d'un de ses tatouages repr�sentant un blason nazi.
Capture d'�cran Rafi
Publicit�
Marine Le Pen a d�cid� d'exclure des listes municipales�Bastien Durocher, un candidat Front national (FN) � la municipale de Ch�teauroux (Indre) suite � la pol�mique n�e du tatouage de ce dernier. Dans un communiqu�, la t�te de la liste FN a justifi� ce choix � cause de "la l�gitime �motion suscit�e en interne" par cette affaire. Depuis quelques jours, des photos circulent de lui sur Internet. On y voit le bras gauche de Bastien Durocher, 25 ans, avec un tatouage repr�sentant le blason de la Division SS Charlemagne .
Si Matthieu Colombier, la t�te de la liste FN � Ch�teauroux, a dans premier temps maintenu sa "confiance" en Bastien Durocher, �voquant notamment "des conneries de gamin", le parti a �finalement pris la d�cision l'�carter. "C'est quelqu'un de valeur, de profond�ment humain, sur lequel on peut compter et qui a le sens de l'amiti�. En cons�quence, je ne l'exclus pas de la liste", avait-il affirm� avant la diffusion d'un communiqu� excluant le vingtenaire.
"Il ne faut pas prendre tout au premier degr�"
Dans l'�dition du quotidien r�gional La Nouvelle R�publique parue vendredi, Bastien Durocher a expliqu� pourquoi il avait fait ce tatouage. D'apr�s l'ex-colistier du FN, ce serait "une erreur de jeunesse". Il a aussi assur� avoir "un rendez-vous le 4 mars prochain pour faire enlever ce tatouage" qu'il a fait � 19 ans.
Les photos ont �t� diffus�es par le site Internet du groupe R�sistance antifasciste de l'Indre (Rafi). Le mouvement a aussi d�nonc� les liens de Bastien Durocher avec le groupe d'extr�me droite du Renouveau fran�ais (RF). De plus, ses r�actions approbatrices sur Facebook � des opinions racistes ont �t� relev�es par la Rafi. "Il ne faut pas prendre tout au premier degr�", s'est d�fendu le militant FN.
A voir �galement sur le th�me du FN : 34% des Fran�ais adh�rent aux id�es du FN
