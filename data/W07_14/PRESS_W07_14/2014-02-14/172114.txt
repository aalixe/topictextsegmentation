TITRE: Nouveau cap d�cisif dans la lutte contre le secret bancaire
DATE: 2014-02-14
URL: http://www.boursorama.com/actualites/nouveau-cap-decisif-dans-la-lutte-contre-le-secret-bancaire-887b2a6ab8a6f9379ee888fdcbd78add
PRINCIPAL: 0
TEXT:
Nouveau cap d�cisif dans la lutte contre le secret bancaire
Le Figaro le 13/02/2014 � 19:24
Imprimer l'article
L'OCDE a pr�sent� sa nouvelle norme sur l'�change automatique d'informations. 42��tats sont pr�ts � l'adopter.
L'�tau se resserre sur les paradis fiscaux. La lutte contre le secret bancaire a franchi une �tape d�cisive avec la pr�sentation, ce jeudi, par l'OCDE d'une nouvelle norme instaurant l'�change automatique d'informations. L'organisation internationale qui m�ne le combat depuis des ann�es a re�u mandat des dirigeants du G20 pour d�finir un nouveau standard. �Cette initia�tive va changer les r�gles du jeu�, se f�licite son secr�taire g�n�ral, Angel Gurria.
Jusqu'� pr�sent, lorsqu'une administration soup�onne un cas d'�vasion fiscale, elle adresse une demande d�taill�e et argument�e au fisc du pays concern�. Lequel r�pond avec plus ou moins de diligence. Cela donne r�guli�rement lieu � quelques frictions entre Paris et Berne, Bercy se plaignant du manque de c�l�rit� des autorit�s helv�tes. Du reste, la Suisse ne fait pas partie des 42��tats - dont l...
