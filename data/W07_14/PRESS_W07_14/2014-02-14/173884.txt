TITRE: Matteo Renzi � la t�te de l'Italie, est-ce une bonne nouvelle pour l'Europe?
DATE: 2014-02-14
URL: http://www.latribune.fr/actualites/economie/union-europeenne/20140214trib000815460/matteo-renzi-a-la-tete-de-l-italie-est-ce-une-bonne-nouvelle-pour-l-europe.html
PRINCIPAL: 173842
TEXT:
�>� �conomie �>� union europ�enne
Matteo Renzi � la t�te de l'Italie, est-ce une bonne nouvelle pour l'Europe?
Matteo Renzi n'a pas jug� bon de convoquer des �lections p�ur confirmer sa nomination comme pr�sident du conseil.
Romaric Godin �|�
14/02/2014, 12:35
�-� 1011 �mots
Le nouveau pr�sident du conseil italien a pr�f�r� pour appuyer sa "politique de r�formes" la r�volution de palais plut�t que la sanction d�mocratique. Une le�on pour l'Europe.
sur le m�me sujet
Italie : Enrico Letta n'a pas confiance dans sa capacit� � former un gouvernement
Ce week-end, Matteo Renzi va prendre ses p�nates au palais Chigi, le si�ge de la pr�sidence du conseil italienne. Sans doute, tout ce que l'Europe compte de belles �mes ne manquera pas de sa p�mer devant ce jeune homme press� et d�cid� � faire - enfin�! - les fameuses ��r�formes structurelles�� dont le pays aurait si imp�rieusement besoin.
Popularit� d'acteur de cin�ma
Il est vrai que celui qui n'est encore que maire de Florence a tout pour plaire aux m�dias, aux march�s et aux dirigeants europ�ens. Sa r�putation soigneusement entretenue de ��Tony Blair italien�� lui assure un capital sympathie dans les salons. Matteo Renzi a, il est vrai, su �lever le marketing politique au rang d'art de la guerre. Mais, s'appuyant pr�cis�ment sur sa popularit� d'acteur de cin�ma, on e�t �t� en droit d'attendre de sa part qu'il s'appuy�t sur la sanction populaire de son programme de r�forme.
Une r�volution de palais plut�t qu'une �lection
Au lieu de cela, nous avons eu une simple r�volution de palais qui fera de Matteo Renzi le troisi�me pr�sident du conseil cons�cutif qui n'aura pas �t� choisi par les �lecteurs de la P�ninsule. Nul doute que tout son r�gne sera d�sormais marqu� par le sceau de cette naissance.
Renzi fera-t-il mieux que Letta�?
Matteo Renzi veut imposer ses choix � l'Italie, mais il devra le faire avec la m�me majorit� �troite et instable qu'Enrico Letta. Et ces alli�s, lorsqu'ils le voudront, pourront rappeler le berceau dans lequel est n� le pr�sident du conseil. Lequel ne pourra gu�re se pr�valoir que des sondages de popularit� personnelle ou des 2 millions de voix obtenus lors des primaires du Parti d�mocrate (Pd), soit moins que le nombre de voix obtenus aux �lections de f�vrier 2013 par le parti de Mario Monti Choix Civique qui a glan� 8,3 % des voix. Autrement dit, le refus de la sanction d�mocratique n'est pas un bon choix pour Matteo Renzi lui-m�me. Il fait de lui un simple Enrico Letta, le charisme en plus. C'est peu pour imposer des choix � une classe politique italienne dont on conna�t la capacit� � r�sister aux choix des chefs de gouvernements. Il y a donc fort � parier que Matteo Renzi m�ne une politique � la Berlusconi visant surtout � s'assurer son maintien au pouvoir plut�t que le ��big bang�� qu'il promet.
Ni urgence, ni instabilit�
Pourquoi donc ce jeune homme ambitieux a-t-il recul� devant la sanction d�mocratique�? N'avait-il pas lui-m�me b�ti une r�forme �lectorale dont il avait n�goci� les termes avec Silvio Berlusconi afin de rendre enfin l'Italie gouvernable�? Jeudi, il a repouss� d'un revers de main l'int�r�t de cette r�forme et s'est cach� derri�re l'argument incertain de l'urgence et de la stabilit�. Or, le gouvernement Letta n'�tait pas moins stable que ne le sera le sien et il n'y avait en ce mois de f�vrier 2014 aucune urgence.
L'Italie a commenc� des r�formes douloureuses
Non, si Matteo Renzi a refus� de franchir le rubicond �lectoral, c'est pour deux raisons essentielles. L� premi�re, c'est que son programme de r�forme, qui vise � achever celui de Mario Monti et � condamner en creux ��l'immobilisme�� d'Enrico Letta, n'est pas majoritaire dans l'opinion italienne. Car contrairement � ce qu'on entend dans les salons europ�ens, particuli�rement allemands, l'Italie n'est pas rest�e immobile depuis 2010, elle a connu une phase aigu� d'aust�rit� et de r�forme qui l'ont plong� dans une r�cession profonde. Le PIB italien a cr� au dernier trimestre 2013 de 0,1 %, mais a recul� de 9 % en trois ans. Un coup d'�il sur les graphiques de l'Italie montre l'�preuve qu'ont connu les habitants de la troisi�me �conomie europ�enne qui, de surcro�t, a vu son potentiel industriel - jadis une de ses forces - se r�duire comme peau de chagrin.
Pas de majorit� pour les r�formes
On comprend que cette politique que Matteo Renzi promet de poursuivre et d'approfondir, ne soit gu�re populaire. Les sondages promettent ainsi au PD un score inf�rieur � la popularit� du maire de Florence. M�me avec la r�forme �lectorale, l'alliance de centre-gauche ne serait pas assur�e d'avoir la prime majoritaire accord�e � partir de 35 % des voix. En revanche, le Mouvement 5 Etoiles eurosceptique et tr�s antipolitique de Beppe Grillo maintient ses positions aux alentours de 20 � 25 % des voix et Silvio Berlusconi demeure aux alentours de 20 % avec Forza Italia.
Voici le dernier sondage r�alis� par SWG :
#Italy - SWG #EP2014 elections poll: PD 32.2% (27 seats) M5S 24.5% (21) Forza Italia 20% (17) Lega 4.7% (4) NCD 4.2% (3)
�
Plus simple de r�aliser un putsch
Matteo Renzi ne pouvait donc pas prendre le risque de soumettre ses r�formes tant vant�es sur les march�s et � l'�tranger � la sanction populaire. Il �tait plus simple de r�aliser un putsch interne et d'obtenir la b�n�diction de Paris, Berlin et Bruxelles ensuite. Il savait, de plus, que rien n'effraie plus les dirigeants et les march�s concernant l'Italie qu'une nouvelle �lection g�n�rale.
Le probl�me d�mocratique de l'Europe
En choisissant d'�carter brutalement Enrico Letta, Matteo Renzi a mis � jour un des grands probl�mes de l'Europe d'apr�s-crise�: la peur de la d�mocratie. La fracture entre les choix des dirigeants et le peu d'assise qu'ont ces choix dans la population am�ne � un contournement de la pratique d�mocratique. Le cas fran�ais du tournant de Fran�ois Hollande et le putsch de Matteo Renzi viennent de l'illustrer. Le probl�me, c'est qu'en cette ann�e d'�lections europ�ennes, plus on �loigne les �lecteurs des choix qu'ils doivent faire et plus ils voudront faire conna�tre leur mauvaise humeur. C'est ce cercle vicieux qu'il�faudrait briser et que Matteo Renzi � renoncer � briser ce jeudi. C'est pourquoi son arriv�e au palais Chigi est une mauvaise nouvelle pour l'Europe.
�����������������������������������������������������������������������������������������������������
