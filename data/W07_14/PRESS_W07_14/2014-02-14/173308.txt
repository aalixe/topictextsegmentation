TITRE: Corées: fin des pourparlers sur les familles séparées et les manoeuvres militaires | Site mobile Le Point
DATE: 2014-02-14
URL: http://www.lepoint.fr/monde/corees-fin-des-pourparlers-sur-les-familles-separees-et-les-manoeuvres-militaires-14-02-2014-1791654_24.php
PRINCIPAL: 173306
TEXT:
14/02/14 à 10h16
Corées: fin des pourparlers sur les familles séparées et les manoeuvres militaires
Les deux Corées ont mené vendredi un second tour de table en vue de trouver un accord permettant la réunion de familles séparées par la guerre malgré des exercices militaires à venir entre le Sud et les Etats-Unis qui irritent Pyongyang.
Le ministère sud-coréen de l'Unification, en charge des relations avec la Corée du Nord , a confirmé vendredi après-midi la fin des discussions après deux courtes sessions, sans autre précision.
Un premier contact avait eu lieu mercredi qui n'avait pas donné de résultat tangible mais clairement fait ressortir les points de discorde.
Le Sud exige du Nord qu'il garantisse la prochaine réunion de familles séparées par la guerre (1950-53) prévue du 20 au 25 février dans la station du mont Kumgang, en territoire nord-coréen.
La Corée du Nord insiste de son côté sur le report du début des manoeuvres annuelles entre les forces armées sud-coréennes et américaines, prévu le 24 février.
Cette question devait être au coeur des pourparlers vendredi dans le village frontalier de Panmunjom, où fut signé l'armistice de 1953.
Pyongyang considère ces exercices comme un entraînement à une invasion de son territoire. Séoul et Washington affirment qu'il s'agit de manoeuvres "défensives".
Séoul a pour le moment opposé une fin de non-recevoir à cette demande, arguant que les manoeuvres et les réunions des familles ne pouvaient dépendre l'une de l'autre.
- Travaux sur le site nucléaire nord-coréen -
Ces discussions, les premières à ce niveau de représentation depuis 7 ans, font suite aux récentes déclarations des dirigeants des deux pays --le Nord-Coréen Kim Jong-Un et la Sud-Coréenne Park Geun-Hye-- plaidant pour une amélioration des relations bilatérales.
Un accord ouvrirait potentiellement la voie à l'examen d'autres sujets épineux, selon Robert Carlin, un ancien diplomate américain qui collabore au site "38 North" consacré à la Corée du Nord.
"Quand elles le veulent -- ce qui malheureusement ne se produit pas souvent -- les deux parties sont capables d'apporter des solutions ingénieuses à des problèmes réputés inextricables", affirme-t-il.
Le fait que le Nord demande un simple report des exercices militaires est en soi un progrès, souligne-t-il, alors que Pyongyang a toujours réclamé leur suppression définitive.
De passage à Séoul jeudi, le secrétaire d'Etat américain John Kerry a apporté son soutien aux efforts de Park Geun-Hye pour établir un climat de confiance avec le Nord.
Il a toutefois appelé le régime nord-coréen à ne pas prendre en otage les familles attendant de revoir leurs proches en faisant du report ou de l'annulation des manoeuvres une condition à leur réunion.
M. Kerry se trouvait vendredi à Pékin, où il devait encourager la Chine à "s'appuyer sur l'influence unique dont elle dispose" pour convaincre Pyongyang de prouver sa volonté réelle de redémarrer les négociations à Six (les deux Corées, la Russie, le Japon, la Chine et les Etats-Unis).
Ces négociations, interrompues fin 2008, visent à convaincre la Corée du Nord d'abandonner son programme nucléaire en échange d'une aide, notamment énergétique.
Le site 38 North a révélé vendredi que la Corée du Nord avait accéléré "de façon significative" les travaux d'excavation sur son principal site nucléaire de Punggye-ri.
"Il n'y a aucun signe indiquant qu'un essai est en préparation", a cependant ajouté le site.
La Corée du Nord a procédé l'an dernier à son troisième essai nucléaire souterrain, défiant les résolutions du Conseil de sécurité des Nations unies.
Les experts internationaux estiment qu'elle détient suffisamment de matériaux fissiles --du plutonium notamment-- pour fabriquer entre six et dix bombes nucléaires.
M. Kerry a insisté jeudi sur le fait que les Etats-Unis n'accepteraient jamais que la Corée du Nord devienne une puissance nucléaire et qu'il refuserait de discuter avec Pyongyang uniquement "pour le plaisir de discuter".
