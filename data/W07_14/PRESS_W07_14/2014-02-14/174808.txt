TITRE: Brian Joubert termine 13e de la comp�tition pour ses derniers Jeux olympiques
DATE: 2014-02-14
URL: http://www.francetvinfo.fr/sports/jo/jo-de-sotchi/jo-patinage-artistique-brian-joubert-termine-13e-de-la-competition-pour-ses-derniers-jeux-olympiques_530339.html
PRINCIPAL: 174806
TEXT:
Tweeter
Brian Joubert termine 13e de la comp�tition pour ses derniers Jeux olympiques
Le Fran�ais de 29 ans a confirm� mettre un terme � sa carri�re. Il est l'athl�te le plus titr� du patinage artistique fran�ais.�
Le patineur Fran�ais Brian Joubert sur la patinoire de Sotchi (Russie), le 14 f�vrier 2014. (DAVID GRAY / REUTERS)
, publi� le
14/02/2014 | 20:12
Le Fran�ais Brian Joubert a termin� 13e de la comp�tition de patinage artistique des� Jeux olympiques de�Sotchi �(Russie),�vendredi 14 f�vrier. Il s'agissait de�ses derniers�JO.�C'est le Japonais�Yzuru Hanyu, 19 ans,�qui a d�croch� la m�daille d'or. Le podium est compl�t� par le Canadien Patrick Chan et le Kazakh Denis Ten.
A l'issue de son programme libre, le patineur fran�ais ne s'�tait class� que 4e. Voici sa derni�re prestation.
(FRANCE TELEVISIONS)
Peu apr�s son passage, Brian Joubert a accord� une interview � France 2, confirmant la fin de sa carri�re. Submerg� par l'�motion, il n'a pu contenir ses larmes�:�"Je suis assez �mu. Le public russe a toujours �t� exceptionnel. Je pense � ma famille, qui m'a toujours soutenue."
Cette vid�o n'est plus disponible
