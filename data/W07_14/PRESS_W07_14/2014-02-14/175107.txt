TITRE: Ministry of Economy and Finance of the French Repu : Pierre MOSCOVICI et Fleur PELLERIN - Faire de la France le pays pionnier du financement participatif
DATE: 2014-02-14
URL: http://www.zonebourse.com/actualite-bourse/Ministry-of-Economy-and-Finance-of-the-French-Repu--Pierre-MOSCOVICI-et-Fleur-PELLERIN-Faire-de-l--17952089/
PRINCIPAL: 175106
TEXT:
MINISTRE DE L'ECONOMIE ET DES FINANCES
FLEUR PELLERIN MINISTRE DELEGUEE
AUPRES DU MINISTRE DU REDRESSEMENT PRODUCTIF, CHARGEE DES PETITES ET MOYENNES ENTREPRISES, DE L'INNOVATION ET DE L'ECONOMIE NUMERIQUE
Communiqu� de presse Com muniqu� de presse
www.economie.gouv.fr www.redressement-productif.gouv.fr
Paris, le 14 f�vrier 2014
N� 1096/899
Faire de la France le pays pionnier du financement participatif
Pierre MOSCOVICI, ministre de l'�conomie et des finances et Fleur PELLERIN, ministre d�l�gu�e charg�e des Petites et Moyennes Entreprises, de l'Innovation et de l'Economie num�rique ont annonc�, vendredi 14 f�vrier, le nouveau cadre juridique assoupli qui s'appliquera au financement participatif.
Fleur PELLERIN a pr�sent� cette r�forme � l'occasion d'une apr�s-midi de rencontres et de d�bats intitul�e � Faire de la France le pays pionnier du financement participatif �.
� Je suis convaincue que les Fran�ais veulent �tre des acteurs de notre �conomie et nous
devons leur permettre de financer les projets auxquels ils croient �, a-t-elle soulign�.
En 2013, la collecte mondiale par des plateformes de financement participatif a d�pass� les
3 milliards de dollars, et les montants lev�s doublent tous les ans. En France, le financement participatif a �galement commenc� � s'imposer comme un formidable levier de
croissance pour les projets de chacun, pour les PME et pour les start-up de la French Tech.
Avec ces mesures, Pierre MOSCOVICI et Fleur PELLERIN souhaitent contribuer davantage encore � son essor et montrer que la France est pleinement engag�e en faveur du d�veloppement de l'�conomie participative.
Fruit d'un an de travail collectif entre les plateformes, l'Autorit� des March�s Financier, l'Autorit� de Contr�le Prudentiel et de R�solution et la Direction G�n�rale du Tr�sor, cette r�forme a pour ambition d'acc�l�rer la dynamique du financement participatif tout en prot�geant les citoyens. Elle repose sur trois valeurs fondamentales :
-�? L'innovation, avec la suppression des barri�res � l'entr�e pour faciliter les nouveaux projets,
-�? La confiance, qui doit �tre totale avec la transparence, sur les risques, les frais et les projets pour prot�ger les donateurs et les �pargnants, et qui se mat�rialisera par un label signalant les plateformes en conformit� avec la r�glementation,
-�? L'inclusion, avec l'acc�s � tous, sans limitation de patrimoine ou de revenus, au contraire des pratiques internationales.
� Les valeurs qui fondent cette r�forme, l'innovation, la confiance, l'inclusion, sont essentielles. Ce sont les valeurs de la � start-up r�publique � dans laquelle la dynamique du financement participatif prend tout son sens �, a ainsi d�clar� Fleur PELLERIN.
Depuis 2012, le gouvernement a engag� un ensemble coh�rent d'actions pour que les start-up et les entreprises innovantes r�alisent tout leur potentiel de croissance et de cr�ation d'emplois, avec notamment les mesures issues des Assises de l'entrepreneuriat, de la Nouvelle Donne pour l'innovation, et de l'Initiative French-Tech, dans le but de faire � faire de la France la � start-up r�publique de l'Europe � �.
Contact presse :
Cabinet de Pierre MOSCOVICI : 01 53 18 45 13
Cabinet de Fleur PELLERIN : 01 53 18 41 00
distribu� par
