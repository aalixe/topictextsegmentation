TITRE: Bison Futé voit orange samedi sur les routes de Rhône-Alpes
DATE: 2014-02-14
URL: http://www.lyonmag.com/article/62429/bison-fute-voit-orange-samedi-sur-les-routes-de-rhone-alpes
PRINCIPAL: 172914
TEXT:
Voir le dossier �
Tops / Flops Lyon
�Najat Vallaud-Belkacem : avec Arnaud Montebourg, Laurent Fabius et S�gol�ne Royal, elle fait partie des ministres ou personnalit�s politiques promus lors de la formation du gouvernement de Manuel Valls. En gardant les Droits des Femmes mais en acqu�rant la Ville, la Jeunesse et les Sports, NVB poursuit sa progression, m�me si elle se r�jouit d'avoir perdu le porte-parolat qui l'exposait trop � la critique.
�Le LHC : Quatorze ans apr�s, les hockeyeurs lyonnais retrouvent l'�lite. En finissant premiers de D1, et en battant facilement Bordeaux en finale des play-offs, ils ont conclu une saison d�j� parfaite en acc�dant � la ligue Magnus.
�Les Verts : Si les �cologistes ne feront pas partie de l'ex�cutif villeurbannais, il restait un chance de les voir int�grer l'�quipe de G�rard Collomb. Mais face au manque d'entente sur le programme socialiste, les Verts ont d�cid� de mettre un terme � leur alliance et ne deviendront pas adjoints au maire.
�Michel Havard : Alors qu'il aurait du rebondir imm�diatement apr�s sa d�faite aux municipales, l'ancien d�put� a pr�f�r� disparaitre de la circulation. Pas une interview, un message de remerciement sur Facebook plusieurs jours apr�s, Havard pr�pare bien mal sa prise de leadership pour les 6 ans d'opposition qui l'attendent.
Circulation � Lyon
