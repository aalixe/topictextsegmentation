TITRE: Croissance : Hollande "salue la confiance retrouv�e des acteurs �conomiques" - RTL.fr
DATE: 2014-02-14
URL: http://www.rtl.fr/actualites/info/economie/article/croissance-hollande-salue-la-confiance-retrouvee-des-acteurs-economiques-7769715047
PRINCIPAL: 0
TEXT:
Le logo de l'Insee � Lille, le 14 septembre 2010
Cr�dit : AFP / PHILIPPE HUGUEN
La croissance en France est meilleure qu'attendue pour l'ann�e 2013 (0,3%), mais moins bonne qu'attendue pour le quatri�me trimestre (0,3% aussi).  Fran�ois Hollande a salu� "la confiance retrouv�e" en France.
La croissance en France a �t� de 0,3% sur             l'ensemble de 2013, un taux un peu meilleur qu'attendu, selon             la premi�re estimation de l'Institut national de la             statistique et des �tudes �conomiques publi�e             vendredi 14 f�vrier.  L'Insee pr�voyait en d�cembre une croissance moyenne du produit  int�rieur brut sur l'an dernier de 0,2% mais elle a revu en hausse  l'�volution du PIB aux premier et troisi�me trimestres, d'une  contraction de 0,1% � une stagnation. Le gouvernement pr�voyait quant �  lui une croissance d'ensemble de 0,1%.
Fran�ois Hollande a salu� dans la foul�e de cette annonce "la confiance retrouv�e par les acteurs �conomiques" en France, a d�clar� la porte-parole du gouvernement Najat Vallaud-Belkacem. Lors du conseil des ministres vendredi matin, "le pr�sident est (...) revenu sur les chiffres de l'�conomie" et "a salu� "la confiance retrouv�e par les acteurs �conomiques", a rapport� Mme Vallaud-Belkacem en rendant compte des travaux du conseil. "C'est en effet gr�ce � la reprise de l'exportation d'une part, de l'investissement de l'autre et de la consommation que l'�conomie repart", a-t-elle ajout�. "C'est de bon augure pour l'ann�e qui vient", a poursuivi la porte-parole du gouvernement. En 2014, "nous devrions pouvoir atteindre les 0,9%, voire les 1% sans trop de difficult�s", a-t-elle pr�cis�.
La croissance fran�aise en 2013
Cr�dit : AFP
"Pas de quoi crier victoire"
Alain Vidalies, le ministre des relations avec le Parlement, s'est r�joui vendredi sur RTL de la r�vision l�g�rement � la hausse par l'Insee de la croissance en 2013, "un bon acquis" qui ne donne cependant "pas de quoi crier victoire." "C'est plut�t mieux que ce que l'on esp�rait, plut�t mieux que ce qui �tait pr�vu au d�but de l'ann�e, y compris au mois de juin. En m�me temps, il n'y a pas de quoi crier victoire", a d�clar� Alain Vidalies.
Le ministre de l'Economie, Pierre Moscovici, a estim� quant � lui que la croissance en 2013, m�me meilleure que pr�vue, n�cessitait d'"aller plus loin" pour "faire reculer le ch�mage". "Je ne me satisfais pas, je dis: 'faisons plus' ", a d�clar� le ministre sur France 2, quelques minutes apr�s la publication du chiffre officiel arr�t� par l'Insee. "Il faut aller plus loin pour avoir des cr�ations d'emplois, pour faire reculer le ch�mage", a-t-il martel�.
Croissance allemande en 2013 de 0,4%
Les chiffres de la croissance de l'Allemagne sont aussi tomb�s, et sont tr�s l�g�rement meilleurs � ceux de la France. Au quatri�me trimestre de 2013, la croissance a �t� de 0,4%, gr�ce � une hausse plus marqu�e que pr�vu des exportations, a indiqu� vendredi Destatis, qui en janvier avait donn� une premi�re estimation � "environ 0,25%". L'Office f�d�ral allemand des statistiques a confirm� le chiffre de 0,4% de croissance du Produit int�rieur brut (PIB) allemand pour l'ensemble de l'ann�e.
Croissance : Pierre Moscovici partage l'enthousiasme du pr�sident de la R�publique
Cr�dit : Ya�l Goosz
