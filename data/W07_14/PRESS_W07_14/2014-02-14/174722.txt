TITRE: Ken Loach. Le r�alisateur britannique soutient le NPA
DATE: 2014-02-14
URL: http://www.ouest-france.fr/ken-loach-le-realisateur-britannique-soutient-le-npa-1930982
PRINCIPAL: 174720
TEXT:
Ken Loach. Le r�alisateur britannique soutient le NPA
Angleterre -
Achetez votre journal num�rique
Le r�alisateur Ken Loach encourage � contribuer � une souscription du Nouveau Parti anticapitaliste (NPA) dans une vid�o de soutien en ligne.
Le r�alisateur britannique Ken Loach connu pour ses films engag�s sur la classe ouvri�re est un alli� fid�le du Nouveau Parti Anticapitaliste (NPA). � la demande de ses militants, la palme d'or � Cannes de 2006 a post� une vid�o sur Youtube pour appeler les internautes � souscrire au parti.
�"Rendre sa voix forte et intelligible"
"J'esp�re que le NPA r�ussira � collecter l'argent n�cessaire pour rendre sa voix forte et intelligible", d�clare en anglais le cin�aste engag� dans une vid�o de 1 minute 41 secondes mise en ligne mercredi sur YouTube et diffus�e par le site internet du NPA.  Il y d�nonce les "programmes d'aust�rit�", le "capitalisme" et les "attaques contre les plus pauvres et les plus vuln�rables".�Ken Loach "est en train de tourner un film en Angleterre. On a appel� son �quipe pour lui parler de notre souscription. Quelqu'un de son �quipe a film� la s�quence et nous l'a envoy�e", a racont� Alain Krivine, membre du comit� politique national du NPA.
360 000�euros r�colt�s
La lev�e de fonds du NPA "a commenc� en octobre-novembre avec l'objectif d'atteindre un million d'euros pour nous aider � �tre pr�sents aux �lections europ�ennes et � financer nos activit�s quotidiennes. On en est � environ 360 000�euros", a d�taill� l'ancien candidat de la Ligue communiste r�volutionnaire (LCR) aux pr�sidentielles de 1969 et de 1974.
"Le parti ne touche plus d'argent de l'�tat parce qu'il n'a pas obtenu de scores suffisants aux derni�res l�gislatives", a ajout� M. Krivine.
Le r�alisateur du film "Le vent se l�ve", Palme d'or � Cannes en 2006, a d�j� apport� son soutien au NPA � plusieurs reprises, notamment pendant la promotion de son film "Looking for Eric" en 2009.�
Lire aussi
