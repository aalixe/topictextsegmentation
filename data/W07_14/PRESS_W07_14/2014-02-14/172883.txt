TITRE: Mister Univers arr�t� et plac� en garde � vue - news t�l�
DATE: 2014-02-14
URL: http://www.programme-tv.net/news/people/48682-mister-univers-arrete-place-garde-vue/
PRINCIPAL: 172880
TEXT:
Mister Univers arr�t� et plac� en garde � vue
Mister Univers arr�t� et plac� en garde � vue
Arr�t� en d�but de semaine par la Brigade des Stup�fiants, Alexandre Piel, alias Mister Univers, met fin � sa carri�re politique.
Il s'�tait engag� il y a quelques mois dans les municipales, et c'est d�j� termin� !
Selon France Bleu, Alexandre Piel, �lu Mister Univers 2014, a �t� arr�t� en d�but de semaine � Rouen, et plac� en garde � vue. La raison ? La Brigade des Stup�fiants a intercept� un colis contenant des produits dopants provenant de Tha�lande, adress� � son nom. Entendu par les enqu�teurs, ce dernier a �t� consid�r� comme �tant en bout de cha�ne, c'est-� dire simple acheteur. L'homme, qui n'est autre qu'un ancien garde du corps de Fran�ois Hollande, a �t� lib�r� mardi soir apr�s son audition. Pour le moment, aucune poursuite judicaire n'a �t� engag�e contre lui.
Ce qui sucite l'�tonnement, c'est qu'il y a encore deux semaines, Alexandre Piel (qui est culturiste) affirmait cat�goriquement ne prendre "aucun produit hormis des compl�ments alimentaires autoris�s".
L'homme a �t� m�diatis� ces derniers mois pour sa candidature aux �lections municipales en Normandie sur la liste de Laurent Bonnaterre, qui se dit aujourd'hui "d��u" : "On s'est vus longuement. Il m'a propos� de d�missionner, j'ai accept�." a-t-il d'ailleurs confi� au Parisien.�
I.M. - vendredi 14 F�vrier 2014 � 09:37
A lire aussi
