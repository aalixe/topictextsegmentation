TITRE: FRANCE - Arche de Zo� : �ric Breteau et �milie Lelouch ne retourneront pas en prison - France 24
DATE: 2014-02-14
URL: http://www.france24.com/fr/20140214-france-tchad-arche-zoe-proces-appel-justice-decision-emilie-lelouche-eric-breteau-orphelins/
PRINCIPAL: 173706
TEXT:
Vid�o par Jonathan WALSH
Derni�re modification : 14/02/2014
La cour d'appel de Paris a condamn�, vendredi, �ric Breteau et �milie Lelouch � deux ans de prison avec sursis. Les deux responsables de l'association l'Arche de Zo� �taient accus�s d'avoir tent� d'exfiltrer 103 enfants du Tchad en 2007.
�ric Breteau et �milie Lelouch ne retourneront pas derri�re les barreaux. La cour d'appel de Paris les a condamn�s, vendredi 14 f�vrier, � deux ans de prison avec sursis. En premi�re instance, le fondateur de l'association humanitaire l�Arche de Zo� et sa compagne avaient �t� condamn�s � trois ans de prison, dont deux fermes, et 50�000 euros d�amende.
Tous deux, ainsi que le logisticien Alain P�ligat, avaient �t� jug�s en appel au cours du mois de novembre 2013, pour escroquerie, exercice illicite de l'activit� d'interm�diaire � l'adoption et tentative d'aide � l'entr�e ou au s�jour de mineurs en situation irr�guli�re.
L�affaire de l�Arche de Zo� remonte � l�automne 2007. Les membres de l�association se trouvaient alors au Tchad, pour sauver, affirmaient-ils, des orphelins du Darfour, une province soudanaise en proie � une guerre civile. Leur objectif�: exfiltrer�103 enfants vers la France, o� ils devaient �tre confi�s � des familles d�accueil. Mais � la derni�re minute, juste avant que les enfants soient embarqu�s, les autorit�s tchadiennes ont interpell� les membres de l�organisation, soup�onn�s d�enl�vement. La plupart des enfants n��taient en r�alit� ni orphelins, ni originaires du Darfour,�mais venaient du Tchad o� l�un, au moins, de leurs parents, �tait encore en vie.
R�fugi�s en Afrique du Sud
Les six membres de l�Arche de Zo� arr�t�s en octobre 2007 ont d�abord �t� condamn�s � huit ans de travaux forc�s au Tchad, puis rapatri�s en France en d�cembre 2007 pour qu�ils y purgent leur peine, commu�e en peine de prison. Finalement graci�s par le pr�sident tchadien et lib�r�s, �milie Lelouch et �ric Breteau sont partis vivre en Afrique du Sud, alors qu�en France, une proc�dure �tait lanc�e contre eux ainsi que contre quatre autres membres de l�association. Tous ont �t� renvoy�s en correctionnelle.
�ric Breteau, qui s�occupait de convoyer des touristes en Afrique du sud, et� �milie Lelouch, qui avait fond� une �cole de cirque, ne sont revenus dans l�Hexagone que pour l�annonce des jugements de premi�re instance, le 12 f�vrier 2012. Ils ont �t� arr�t�s dans la foul�e, puis �crou�s. Ils ont �t� lib�r�s deux mois plus tard, le 18 avril, pour pouvoir pr�parer leur d�fense en vue du proc�s en appel, qui s�est tenu en novembre 2013.
Le couple a comparu avec le logisticien Alain P�ligat, 62 ans, qui a �t� condamn� � six mois de prison avec sursis en premi�re instance. Les trois autres pr�venus, condamn�s en premi�re instance � des peines de six mois � un an de prison avec sursis, n'ont quant � eux pas fait appel.
�
