TITRE: Lens a mang� du Lyon - Coupe de France - Football - Sport.fr
DATE: 2014-02-14
URL: http://www.sport.fr/football/lens-a-mange-du-lyon-339952.shtm
PRINCIPAL: 173018
TEXT:
Lens a mang� du Lyon
Vendredi 14 f�vrier 2014 - 18:00
Lens a cr�� la surprise au Stade Gerland, en s'imposant contre Lyon lors des 8e de finale de la Coupe de France (1-2, ap). Les Lyonnais pourront s'en vouloir d'avoir voulu g�rer en deuxi�me p�riode, alors qu'ils avaient le match en main.
Il n?y aura pas de choc entre l?Olympique Lyonnais et l?AS Monaco lors des quarts de finale de la Coupe de France . Oppos�s au RC Lens , les Lyonnais se sont content�s de prendre l?avantage en premi�re p�riode par Jimmy Briand , avant de g�rer, et m�me de se faire rattraper en deuxi�me p�riode. Trop timor�s lors de l?entame de la rencontre, les Art�siens ont trop respect� les Gones en premi�re p�riode pour esp�rer quelques chose. C?est fort logiquement, apr�s un premi�re alerte de Grenier (8e), que Briand, seul au second poteau, ouvrait la marque sur une centre parfait de Gonalons (1-0, 9e).
Si les septuples champions de France en titre ont largement domin� la premi�re p�riode, c?est en partie gr�ce au travail de Gonalons et Grenier, tr�s pr�cieux dans la construction du jeu. Positionn� en meneur de jeu, pour pallier l?absence de Yoann Gourcuff , Cl�ment Grenier a fait �tal de toute sa palette technique, que ce soit dans les transmissions ou bien dans ses dribbles courts. Pour sa part Gonalons, souvent laiss� seul par les Lensois, s?est trouv� � la base de toutes les offensives.
Lyon s?est vu trop beau
Mais si Gomis avait l?occasion de doubler la mise par deux fois (15e, 16e), les hommes de R�mi Garde ne parvenaient pas � saisir leur chance de faire le break. De quoi laisser les visiteurs esp�rer. D?autant que dans le deuxi�me acte, les locaux baissaient sensiblement de rythme. Par trois fois sur corner (54e, 67e, 89e), Lens �tait � deux doigts de revenir au score. Mais c?est finalement dans les arr�ts de jeu que Zeffane offrait un penalty au RCL. Valvidia se chargeait de le transformer et d?amener les deux �quipes en prolongations (1-1, 90+3).
Prolongation dans laquelle les Lensois prenaient l?avantage par Gbamin sur corner, suite � sortie totalement rat�e de Vercoutre (1-2, 92e). Incapable de faire la diff�rence par la suite, l?OL ne r�agissait que dans les tous derniers instants de la partie. Mais Kon� (119e) et Lacazette (120e) manquaient l?occasion d?emmener les leurs aux tirs au but. A vouloir trop g�rer, l?OL s?est fait punir. Et ce sera une bonne le�on � retenir pour la fin de la saison.
Dossiers associ�s
