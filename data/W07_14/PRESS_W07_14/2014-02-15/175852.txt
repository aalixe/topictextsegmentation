TITRE: ETATS-UNIS: Une Barbie dans <i>Sports Illustrated</i> fait pol�mique -   Vivre - 24heures.ch
DATE: 2014-02-15
URL: http://www.24heures.ch/vivre/barbie-sports-illustrated-polemique/story/15544596
PRINCIPAL: 175851
TEXT:
Votre adresse e-mail*
Votre email a �t� envoy�.
L'apparition de Barbie dans le traditionnel num�ro sp�cial maillots de bain du magazine Sports Illustrated a suscit� une pol�mique aux Etats-Unis, o� certains ont critiqu� la pr�sence de la poup�e au milieu des photos sexy.
A la veille de son 55e anniversaire, la c�l�bre poup�e longiligne en plastique s'affichait sur une fausse une du magazine, devant un grand ciel bleu, v�tue d'un maillot de bain en tricot noir et blanc, � c�t� du grand titre �La poup�e par laquelle tout a commenc�.
�le mod�le inatteignable de toutes les femmes�
�Le num�ro sur les maillots de bain n'est pas pour les enfants, comme Barbie n'est pas pour les adultes non plus�, a critiqu� Eve Vawter, du site parental www.mommyish.com , qui se pr�sente sur son compte Twitter comme une f�ministe, et qui va jusqu'� comparer Barbie � une �poup�e gonflable�.
�Vous avez envie de vous exciter avec des femmes sexy, � peine v�tues dans un magazine de sports pour hommes cette ann�e encore? Eh bien voil�. En voil� une en plastique. Voil� le premier mod�le inatteignable de toutes les femmes�, a de son c�t� d�nonc� Mary Eliz, sur le site d'information salon.com .
�Douteux, d'un point de vue �thique�
Lindsey Feitz, sp�cialiste des questions de femmes et de genre � l'Universit� de Denver, dans le Colorado, consid�re comme �douteux, d'un point de vue �thique�, d'utiliser une poup�e destin�e aux petites filles pour sexualiser l'image de la femme dans un magazine surtout lu par des hommes.
�Nous sommes pass�s des pin-ups � l'encre des ann�es 40 � des mannequins en une dont les corps ont �t� num�riquement alt�r�s�, explique Lindsey Feitz dans un email. �Et maintenant le mannequin est remplac� par une repr�sentation en plastique d'une fille-femme sexy. C'est ironique�.
Une silhouette irr�aliste
Barbie, dont la marque est �valu�e � 3 millions de dollars, suscite r�guli�rement la controverse. Elle a notamment �t� accus�e par le pass� d'incarner une silhouette irr�aliste pour les jeunes filles.�Depuis ses d�buts, le maillot de bain a transmis un message (...) de force et de beaut�, explique de son c�t� le r�dacteur en chef de Sports Illustrated en charge de cette �dition sp�ciale, M.J. Day.
�Et nous sommes ravis que Barbie c�l�bre ces valeurs de mani�re unique�.La v�ritable couverture du magazine, qui doit para�tre mardi, affiche une photo de trois mannequins en bikini, fesses bronz�es bien en �vidence, les pieds dans l'eau turquoise. (afp/Newsnet)
Cr��: 15.02.2014, 03h31
