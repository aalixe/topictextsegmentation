TITRE: Nicolas Bedos condamn� � trois mois de prison avec sursis et 800 euros d'amende - 20minutes.fr
DATE: 2014-02-15
URL: http://www.20minutes.fr/people/1299702-20140214-nicolas-bedos-condamne-a-trois-mois-prison-sursis-800-euros-amende
PRINCIPAL: 175658
TEXT:
Nicolas Bedos en octobre 2013 PJB/SIPA
FAITS DIVERS - Apr�s une chute � scooter, en �tat d'ivresse, l'humoriste avait pris � partie des policiers...
L'humoriste Nicolas Bedos  a �t� condamn� vendredi � trois mois de prison avec sursis et 800 euros  d'amende pour conduite en �tat d'ivresse, outrage et menace de mort  envers des policiers apr�s une chute � scooter.
Le tribunal correctionnel de Paris a suivi les r�quisitions  du  parquet et a retenu l'�tat de r�cidive l�gale, l'humoriste ayant d�j� �t� condamn� en janvier 2010 pour conduite en �tat d'�bri�t�. Nicolas  Bedos  n'�tait pas pr�sent � l'audience. Il �sait donner des le�ons de  courage  (...) aux policiers quand il passe � la t�l� mais quand il  s'agit de  venir s'expliquer devant eux, il n'en a plus beaucoup�, a  tacl� l'avocat  des deux policiers, Me J�r�me Andrei. Une r�f�rence � une ancienne  chronique au vitriol de l'humoriste sur la police, qui lui  avait d�j� valu une condamnation pour injure publique en juillet 2012.
�Je te retrouverai et je te tuerai�
Dans la nuit du 24 septembre 2013, dans le centre de Paris,  deux  policiers avaient aper�u un homme circulant avec difficult�s sur un   deux-roues, zigzaguant et faisant des embard�es avant de chuter.  Lorsqu'ils �taient arriv�s � la hauteur de Nicolas Bedos, le ton �tait  rapidement mont�.
Selon les deux policiers, l'humoriste, ivre, les avait pris a   partie, parlant de leur �boulot de merde� notamment. A l'un d'eux, il   aurait dit: �Je te retrouverai et je te tuerai�. La repr�sentante du  parquet a fustig� un �comportement  inadmissible� et des �propos  d�testables�, d�plorant qu'il ne soit venu  au tribunal �pr�senter des  excuses�.
Bedos �doublement coupable�
Entendu par la police lorsqu'il avait recouvr� ses esprits, Nicolas  Bedos  avait dit ne pas se souvenir avec pr�cision des propos tenus,  mais il  avait ni� toute forme de menace. Et selon lui, l'un des deux  policiers  l'avait abord� en lui lan�ant: �Vous faites moins le fier  qu'� la t�l�.  Ce que le fonctionnaire de police a ni�. Nicolas Bedos a   en outre �t� condamn� � verser au total 750 euros de dommages et   int�r�ts aux fonctionnaires et 300 euros pour les frais de justice.
Son avocat, Me Vincent Toledano, a d�plor� que, dans ce  dossier, son  client soit �doublement coupable: du comportement habituel  d'un  ivrogne�, mais aussi de sa notori�t�. Il a renouvel� les excuses de   l'humoriste aux policiers.
Avec AFP
