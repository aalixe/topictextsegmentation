TITRE: Au bonheur de Fenninger! - JO 2014 - Sports.fr
DATE: 2014-02-15
URL: http://www.sports.fr/jo-2014/ski-alpin/articles/fenninger-s-adjuge-le-super-g-1010518/?sitemap
PRINCIPAL: 0
TEXT:
15 février 2014 � 09h16
Mis à jour le
15 février 2014 � 12h47
Sur un tracé exigeant, Anna Fenninger a remporté le Super-G olympique samedi matin à Sotchi. L’Autrichienne s’est imposée en 1’25"52 devant Maria Hoefl-Riesch (+0"55) et Nicole Hosp (+0"66). Marie-Archand-Arvier a dû abandonner, comme onze autres skieuses…
Vu l’hécatombe qui a touché les concurrentes de ce Super-G olympique, on s’est longtemps demandé si terminer la course n’allait pas suffire pour décrocher une médaille. Sept filles sur les huit premières à s’élancer ont effectivement mordu la poudreuse. Mais en réalité – et heureusement pour le spectacle –, il en a fallu plus, beaucoup plus, pour monter sur la boîte samedi matin à Sotchi. Et parmi toutes les skieuses en lice, Anna Fenninger est celle qui a le mieux allié vitesse et finesse.
L’Autrichienne, qui avait signé trois podiums dans la spécialité cette saison, a parfaitement maîtrisé les lignes d’un tracé dessiné par son entraîneur et s’est imposée en 1’25"52, tenant ses principales rivales à distance, notamment Maria Hoefl-Riesch (+0"55) et Nicole Hosp (+0"66). "Je n’arrive pas à y croire, a-t-elle réagi sur France 3 après la course. Dans la descente, j’avais manqué de précision et vitesse mais aujourd’hui, c’était mon jour. C’était vraiment un rêve qui se réalise."
Du chocolat pour Gut
Pour Lara Gut, grandissime favorite de ce Super-G du haut de ses trois victoires en cinq épreuves de Coupe du monde dans la discipline depuis le début de l’hiver, ce rêve est passé. La Suissesse a dû se contenter de la quatrième place, la plus cruelle (+0"73), devant une autre déçue, Tina Maze (+0"76). Dur à encaisser pour la Slovène, qui aura sûrement très à cœur de se rattraper d’ici la fin de ces Jeux. Des Jeux qui, pour Marie Marchand-Arvier, ont définitivement tourné au cauchemar.
Ce sont des moments qui ne sont pas faciles dans une carrière...
Marie Marchand-Arvier
Après sa chute en descente, déjà compliquée à avaler, la Française a fait partie des douze à être tombées dans le piège d’un tracé exigeant. La pilule de trop. "Quand on est dans les premiers dossards, on sert souvent de crash test. On peut aussi être tenté de faire sa propre ligne. Mais pour moi, c’était une erreur technique…", a-t-elle regretté, sans trop savoir quelle suite donner à sa carrière. Remontera-t-elle sur ses skis la saison prochaine ? Pas sûr.
"Ce sont des moments qui ne sont pas faciles dans une carrière, a-t-elle ajouté. J’avais préparé ces Jeux de manière très professionnelle. Après, parfois ça sourit, parfois non. Il faut faire avec et repartir de l’avant, toujours avec le sourire. C’est toujours plus facile de célébrer des victoires mais bon, je suis loin d’être à terre. Je suis très soutenue." Si elle veut rebondir, "MMA" devra ranger son album de Sotchi quelque part au fond d’un tiroir. Et ne plus jamais l’ouvrir.
