TITRE: Ukraine: les opposants lib�r�s, le pr�sident demande des "concessions" - Flash actualit� - Monde - 14/02/2014 - leParisien.fr
DATE: 2014-02-15
URL: http://www.leparisien.fr/flash-actualite-monde/ukraine-l-opposition-maintient-la-pression-russes-et-europeens-polemiquent-14-02-2014-3591087.php
PRINCIPAL: 0
TEXT:
Ukraine: les opposants lib�r�s, le pr�sident demande des "concessions"
Publi� le 14.02.2014, 14h09
Les autorit�s ukrainiennes ont annonc� vendredi avoir lib�r� tous les manifestants interpell�s en deux mois, le pr�sident Viktor Ianoukovitch appelant en retour l'opposition � "faire aussi des concessions", tandis qu'une nouvelle manifestation est pr�vue dimanche. | Yury Kirnichny
1/4
R�agir
Les autorit�s ukrainiennes ont annonc� vendredi avoir lib�r� tous les manifestants interpell�s en deux mois, le pr�sident Viktor Ianoukovitch appelant en retour l'opposition � "faire aussi des concessions", � deux jours d'une nouvelle manifestation pr�vue dimanche.
Les 234 manifestants lib�r�s sont assign�s � r�sidence et les accusations pesant sur eux ne sont pas abandonn�es, a toutefois pr�venu le procureur g�n�ral ukrainien, Viktor Pchonka.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
"234 personnes ont �t� arr�t�es entre le 26 d�cembre et le 2 f�vrier. Aujourd'hui, plus aucune d'entre elles n'est en d�tention", a d�clar� le procureur, ajoutant que les poursuites, qui pourraient valoir aux opposants de lourdes peines de prison, seraient abandonn�es dans le mois � venir si les conditions fix�es par la loi d'amnistie �taient remplies.
En retour, le pr�sident Viktor Ianoukovitch a interpell� l'opposition pro-europ�enne qui conteste le r�gime ukrainien depuis deux mois.
"Nous avons les moyens de remettre n'importe qui � sa place, mais nous ne voulons pas que des innocents souffrent. (...). Je ne veux pas faire la guerre. Je veux sauvegarder l'Etat et reprendre un d�veloppement stable", a d�clar� le pr�sident ukrainien. "Nous nous adressons � l'opposition pour qu'elle accepte aussi de faire des concessions (...). Les appels � une lutte sans merci, � prendre les armes , c'est dangereux", a-t-il ajout�, en allusion � des groupes radicaux au sein de l'opposition.
Dans le centre de Kiev, sur la place de l'Ind�pendance -le Ma�dan-, occup�e depuis novembre et entour�e de barricades, le "conseil" improvis� du mouvement de contestation a soulign� que les manifestants lib�r�s restaient menac�s de prison.
Une loi d'amnistie avait �t� vot�e en janvier, avec pour condition l'�vacuation des lieux publics et b�timents officiels occup�s par les contestataires, dont la mairie de la capitale. Ce d�lai expire lundi.
L'opposition exigeait la lib�ration sans conditions de toutes les personnes incarc�r�es et la lev�e des poursuites.
L'une des opposantes, l'ancienne Premi�re ministre emprisonn�e Ioulia Timochenko, r�affirme dans un entretien � para�tre samedi dans l'hebdomadaire Dzerkalo Tyjmia que "le seul sujet de n�gociation avec Ianoukovitch, ce sont les conditions de son d�part et des garanties pour sa famille".
- Manifestation dimanche -
En signe de bonne volont�, les opposants ont toutefois promis de d�bloquer "en partie" la rue Grouchevski o� se trouvent le gouvernement et le parlement, th��tre de heurts violents fin janvier.
Pour la 11e fois depuis le d�but de la contestation, n�e de la volte-face de Kiev renon�ant � un rapprochement avec l'Union europ�enne pour se tourner vers la Russie, les manifestants se r�uniront dimanche � midi (10H00 GMT) sur le Ma�dan.
Pr�s de 70.000 personnes s'�taient rassembl�es le 9 f�vrier.
Le mouvement de contestation s'est transform� au fil des semaines en un rejet pur et simple du r�gime du pr�sident Ianoukovitch, et ni la d�mission du gouvernement ni les n�gociations engag�es apr�s les affrontements qui ont fait quatre morts et plus de 500 bless�s fin janvier, n'ont r�gl� le conflit.
Apr�s la d�mission fin janvier du Premier ministre Mykola Azarov, son successeur n'a toujours pas �t� d�sign�, les alli�s du pr�sident, majoritaires au Parlement, faisant savoir qu'ils ne soutiendraient pas un candidat d'opposition.
Les d�bats sur une �ventuelle r�forme constitutionnelle r�clam�e par l'opposition semblent aussi au point mort.
La crise �tait vendredi au centre de discussions � Moscou entre les ministres russe et allemand des Affaires �trang�res. Sergue� Lavrov a accus� l'UE de chercher � �tendre sa zone d'"influence" � l'Ukraine en soutenant l'opposition. Il avait jug� la veille que les relations entre la Russie et l'UE, qui se sont d�t�rior�es ces derni�res ann�es, �taient arriv�es � un "moment de v�rit�" avec le diff�rend sur l'Ukraine.
L'Allemand Frank-Walter Steinmeier a r�torqu� que la crise en Ukraine n'�tait "pas un jeu d'�checs g�opolitique" et a soulign� que "personne n'avait int�r�t � un envenimement de la situation".
Quant aux Etats-Unis, tr�s impliqu�s aux c�t�s de l'opposition pro-europ�enne, ils ont salu� dans un communiqu� "la retomb�e des tensions", tout en exhortant Kiev � "cesser toutes les enqu�tes, arrestations, mises en d�tention et poursuites" contre les manifestants. Le d�partement d'Etat plaide aussi pour la "formation d'un gouvernement technique multipartite".
