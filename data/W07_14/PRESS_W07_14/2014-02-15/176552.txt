TITRE: Plac�: un d�part des �cologistes du gouvernement n�aurait �aucun sens� - Lib�ration
DATE: 2014-02-15
URL: http://www.liberation.fr/politiques/2014/02/15/place-un-depart-des-ecologistes-du-gouvernement-n-aurait-aucun-sens_980480
PRINCIPAL: 176551
TEXT:
Jean-Vincent Plac�, pr�sident du groupe EELV au S�nat, en septembre � Angers.  (Photo Alain Jocard. AFP)
EELV compte deux ministres dans le gouvernement de Jean-Marc Ayrault, C�cile Duflot (logement) et Pascal Canfin (d�veloppement).
Sur le m�me sujet
Municipales : Jean-Vincent Plac� candidat dans l'Essonne
Jean-Vincent Plac�, le chef de file des s�nateurs EELV, a estim� samedi qu�un d�part des �cologistes du gouvernement n�aurait aujourd�hui �strictement aucun sens�.
�Il y a un d�bat tr�s vif au sein du mouvement, la faiblesse des �l�ments �cologiques de la politique men�e et les orientations nettement social-lib�rales plaisent de moins en moins � nos militants et � nos cadres�, a-t-il d�clar� sur Europe 1.
Interrog� sur un �ventuel remaniement minist�riel en mai prochain, le s�nateur EELV a estim� que �dans la p�riode actuelle et sur les grands enjeux� - loi de transition �nerg�tique, sur la biodiversit�, fiscalit� �cologique... - �dans ce temps extr�mement complexe, y compris d��lections, quitter le gouvernement n�aurait strictement aucun sens�.
�On est dans une position d�un groupe minoritaire dans un ex�cutif, c�est tr�s difficile. Dans la s�quence qui vient, municipales, europ�ennes, sortir �a n�a tout simplement aucun sens�, a-t-il r�p�t�: �Ce serait per�u comme tr�s, tr�s politicien, voire politicard, par les Fran�aises et les Fran�ais�.
EELV compte deux ministres dans le gouvernement de Jean-Marc Ayrault, C�cile Duflot (logement) et Pascal Canfin (d�veloppement).
A propos des chiffres de la croissance (+0,3% en 2013), Jean-Vincent Plac� a estim� qu�il �faut aujourd�hui une politique plus dynamique, notamment des pouvoirs publics, pour relancer en particulier l�investissement et un peu aussi la consommation�.
Interrog� sur les municipales de mars, il a indiqu� que, s�agissant des villes de plus de 30.000 habitants qui constituent leur �coeur de cible�, les �cologistes �taient �� moiti� en alliance avec le PS, au tiers en listes autonomes EELV� et �� 1,5% avec le Parti de gauche�.
dch/ed
