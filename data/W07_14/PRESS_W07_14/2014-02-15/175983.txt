TITRE: 20 Minutes Online - L espionne qui s amourache de sa cible - Insolite
DATE: 2014-02-15
URL: http://www.20min.ch/ro/news/insolite/story/23739245
PRINCIPAL: 175975
TEXT:
Cette histoire rappelle le sc�nario alambiqu� de la s�rie �Homeland�. (photo: Keystone)
14 f�vrier 2014 17:41
Canada
L'espionne qui s'amourache de sa cible
Une agente des services canadiens de surveillance des menaces terroristes a �t� suspendue en raison de sa liaison amoureuse avec un individu qu'elle �tait charg�e de suivre.
Cette jeune femme faisait partie des Equipes int�gr�es de la s�curit� nationale (EISN), un groupe cr�� en 2002 pour pr�venir les menaces terroristes et regroupant des membres de la gendarmerie et de la police, des douanes, ainsi que du Service canadien du renseignement de s�curit� (SCRS),.
�Hey ch�ri! J'ai un petit quelque chose que je voulais te donner pour No�l (...) je t'aime et je m'ennuie de toi�. Ce message a �t� adress� en 2011 par cette agente depuis son t�l�phone � un homme d'affaires montr�alais d'origine iranienne, selon �La Presse� .
C'est ce message amoureux destin� � l'homme qu'elle �tait charg�e de surveiller, qui a �t� � l'origine d'une enqu�te interne menant � sa suspension. Le ministre de l'Immigration, Chris Alexander, a fait savoir par sa porte-parole, Alexis Pavlich, que le gouvernement prenait cette affaire �au s�rieux�.
�Victime de jalousie�
Cette porte-parole a d�livr� un message strictement identique vendredi � celui qu'elle a livr� au quotidien montr�alais: �Aussit�t inform� de ces all�gations, le ministre Alexander a demand� aux fonctionnaires de prendre des actions rapides et s�rieuses pour s'assurer que la s�curit� et l'int�grit� du programme n'ont pas �t� compromises�.
De son c�t�, l'homme d'affaires canado-iranien s'est dit �victime de jalousie� car ��a ne plaisait pas � tout le monde que je couche avec elle. C'est pour �a que j'ai �t� fich� comme un espion iranien!�.
Le m�dia canadien rapproche cette affaire bien r�elle de la s�rie t�l�vis�e am�ricaine �Homeland�, dans laquelle une agente de la CIA tombe amoureuse d'un ancien soldat am�ricain soup�onn� de pr�parer un attentat contre les Etats-Unis.
(afp)
