TITRE: Nouvelle réforme judiciaire controversée en Turquie  - 7SUR7.be
DATE: 2014-02-15
URL: http://www.7sur7.be/7s7/fr/1505/Monde/article/detail/1794273/2014/02/15/Nouvelle-reforme-judiciaire-controversee-en-Turquie.dhtml
PRINCIPAL: 176681
TEXT:
15/02/14 -  10h39��Source: Belga
Le Premier ministre turc Recep Tayyip Erdogan � afp.
Le parlement turc a adopt� samedi un projet de loi tr�s controvers� destin� � renforcer le contr�le politique sur les nominations de magistrats, a annonc� une source parlementaire.
La loi a �t� vot�e � l'initiative du gouvernement qui se d�bat en plein scandale politico-financier, apr�s des �changes houleux entre d�put�s du parti au pouvoir et ceux de l'opposition.
Avant le vote, le Premier ministre Recep Tayyip Erdogan avait annonc� le gel des articles les plus controvers�s du texte r�formant le Haut-conseil des juges et magistrats (HSYK), apr�s plusieurs jours de vives tensions entre la majorit� et ses adversaires.
Le d�bat avait d�g�n�r� � deux reprises en pugilat entre d�put�s rivaux.
L'opposition a d�nonc� ce projet qui donne le dernier mot au ministre de la Justice en mati�re de nomination de magistrats, affirmant qu'il est contraire � la Constitution et qu'il a pour seul but de permettre au gouvernement d'�touffer les enqu�tes qui le menacent.
Pour entrer en vigueur la loi doit encore �tre sign�e par M. Erdogan.
Lire aussi
