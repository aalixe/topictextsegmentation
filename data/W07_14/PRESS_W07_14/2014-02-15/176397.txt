TITRE: Intermittents : Malaise avec Zaz aux Victoires de la musique
DATE: 2014-02-15
URL: http://www.ozap.com/actu/intermittents-malaise-avec-zaz-aux-victoires-de-la-musique/451707
PRINCIPAL: 176393
TEXT:
Intermittents : Malaise avec Zaz aux Victoires de la musique
11H13 Le 15/02/14 Zapping 155
publi� par Benoit Daragon
Intermittents : Malaise avec Zaz aux Victoires de la musique
Petit moment de malaise hier aux Victoires de la musique. Zaz a refus� de lire un texte sur les intermittents du spectacle que Virginie Guilhaume lui soumettait.
Tweet
Retrouvez aussi l'actu m�dias sur notre page Facebook
Les Victoires peuvent dire merci � Stromae ! Hier soir, le triomphe du chanteur belge, qui a vendu 1,3 million d'exemplaires � ce jour de son album "Racine Carr�", a permis � la c�r�monie de r�aliser sa meilleure audience depuis 2010 ! En effet, la soir�e pr�sent�e par Virginie Guilhaume et retransmise en direct sur France 2 a d�pass� les 3,1 millions de t�l�spectateurs , soit 15,8% de parts d'audience.
Si la soir�e a �t� marqu�e par les trois prix de Stromae et par le sacre de Vanessa Paradis , elle a aussi �t� travers�e par un moment de g�ne. A l'issue de sa prestation sur sa chanson intitul�e "Gamine", la chanteuse Zaz , qui concourait dans la cat�gorie "artiste interpr�te f�minine" a �t� tr�s surprise par une question de Virginie Guilhaume. L'animatrice a demand� � la chanteuse son avis sur le dossier br�lant des intermittents du spectacle. "Alors, Zaz, on a parl� tout � l'heure des intermittents du spectacle. C'est vrai qu'on tient vraiment � dire un mot pour eux et je crois que vous aviez envie de dire quelque chose", a lanc� l'animatrice.
"Tu veux que je parle de �a ?"
"Euh pour les intermittents du spectacle ?", a r�p�t� la jeune chanteuse visiblement tr�s surprise par la question. "Bah c'est super important d�j� qu'on soit reconnu...", a-t-elle bafouill� sans convaincre. "Tu veux que je parle de �a ? Je dois dire quelque chose que tu me dis de dire", s'est agac�e Zaz en d�couvrant qu'un texte d�filait sur un prompteur. "Non je pensais que vous vouliez le dire mais on peut le dire ensemble si vous voulez !", lui a expliqu� Virginie Guilhaume. "Bah non vas y, dis-le, alors", s'est amus�e l'interpr�te de "Je Veux".
C'est donc finalement Virginie Guilhaume seule qui a lu un texte appelant � une manifestation le 27 f�vrier pour d�fendre le r�gime sp�cifique des intermittents que le MEDEF a propos� de supprimer. "J'adore son caract�re", s'est amus�e l'animatrice avant de faire promettre � la chanteuse de tout de m�me participer au rassemblement.
-             Toutes les news : actus TV
Qu'en pensez vous ?
6 photos
LANCER LE DIAPORAMA
Veuillez d�sactiver votre bloqueur de pub (Adblock) pour ce site afin d'afficher l'int�gralit� des commentaires et en publier.
karpis
Pas tr�s pro de la part de Guilhaume de mettre quelqu'un dans cette position...
Surtout pour une c�r�monie qui a �t� r�p�t�e la journ�e.
arthur alexandre
comment se fait il qu'on soit le seul pays ou il y a ce systeme d'intemittents du spectacle?   ces intermittents constiuent je crois environ 0,3 pour cent des chomeurs et consomment pres de 25 ou 30 pour cent de l'aregent du chomage!!......c'est �norme!!!  il faut obligatoirement r�former cela
Fr�d�ric Baussart
Solution : on supprime les Intermittents et ils deviennent tous travailleurs ind�pendants ou auto-entrepreneurs et g�rent le risque li� � leur travail comme un commer�ant par exemple... Car finalement tout ce�i n'est qu'un commerce.
intermittent et alors
surtout, il faudrait arreter de debattre de sujet que vous ne connaissez pas, plutot que de gober tout ce que nous disent les media, complices du medef et des dirigeants!! "les intermittents", c est vrai que tout les maux viennent de leur fait!
si on regarde les autres metiers, les ministres qui vont toucher des salaires on ne sait combien de temps meme si ils ont �t� ministre pendant 1 an, la prime de charbon pour les cheminots, en 2014 la seule locomotive que j ai vu rouler au charbon, c est peut etre a la mer de sable! Alors plutot que de nous cracher � la gueule et de vous laisser manipuler et diviser par tout ces hauts fonctionnaires bien pensants, il faudrait peut etre se rassembler et faire corps contre tout �a, mais quand le  principal interet des fran�ais, c est dieudonn� ou les manifs pour tous ou anti- mariage gay! on va pas s en sortir! et le jour il ne se passera plus grand chose culturellement, il faudra pas venir pleurer!!
SLG29
Effectivement, mais il n'a pas non plus �t� cr�e pour faire des d�ficits abyssaux.
Roxy
Sinon �a ne choque personne qu'une autre artiste interpretait la chanson Gamine depuis 2011 sur des centaines de concerts dont des premi�res partie de ZAZ ???
Voici les vid�os:
L'UNEDIC n'a pas �t� cr��e pour faire du profit...
GEO 92
Ah oui �a ne vous d�range pas de vous divertir toute l'ann�e avec le travail des intermittents, de profiter de leurs spectacles, films, concerts, programmes TV...mais vous leurs interdisez de prendre la parole dans leur �mission, sur leur sc�ne, bref dans leur travail...C'est sur vous devez �tre sacr�ment docile dans votre entreprise si vous tenter de r�gler les probl�mes de celle-ci partout sauf au bureau! Et puis comment osez-vous cataloguer les intermittents de citoyens privil�gi�s?En bon fran�ais que vous �tes, vous n'y connaissez rien, mais comme la t�l� vous dit que ce n'est pas normal, ben alors vous vous mettez � r�ler, parce que si la t�l� et les m�dias le disent, c'est que �a doit �tre vrai! Vous vous arr�tez � votre petit horizon de petit fran�ais d��u et triste et vous recrachez ce que l'on vous a pr�-mach� en vous croyant dans le coup et cultiv�...Avec ce genre de r�flexion, vous d�montrez une nouvelle fois l'existence d'un courant fran�ais individualiste qui se permet de juger le travail des autres sans jamais lever son cul de son canap� et qui croit apprendre aux autres en gueulant devant sa TV comment il faut faire et ce que c'est que la vie... Path�tique et terriblement d�cevant.
Deepimpact
Je suis intermittent et oui il y a de l'abus mais c'est une minorit� qui agace �normement les intermittents qui ne profitent pas du syst�me. Les intermittents cherchent qu'une chose avoir du boulot, ce statut pr�caire n'est pas un choix pour tous. Moi je connais certaines personnes qui vivent du RSA sans r�ellement vouloir travailler. Eux oui ils ne cherchent pas � travailler. Il m'arrive de multiplier les cachets et travailler 48h par semaine de nuit parfois �a use �norm�ment et c'est toujours plus que 35h. Je suis technicien et sans arr�t je dois me mettre au niveau sur toutes les avanc�es du domaine ce qui demande �galement du temps � l'exterieur. Je suis d�sol� je fais plus de 45h/ semaine je touche 1600 euros par mois. Non je ne vis pas sur le syst�me si on me proposais un CDI de suite je le prendrai les yeux ferm�s. J'ai 25 ans pas d'appart je suis chez mes parents car mon statut m'en emp�che. Faut parfois arr�ter de stigmatiser, je suis passion� par mon m�tier, mais l'intermittence c'est souvent un choix par d�faut. C'est un rythme compliqu� qui vous use dans la mesure o� vous ne profitez pas.
Avant de cibler la pr�carit� ciblons nos politiques qui eux ne toucheront jamais � leur avantages car il y a plus a prendre de leur cot� que du c�t� des intermittents.
Mais bon si on supprime ce statut, des festivals seront annul�s et ce genre de choses rapportes quand m�me pas mal de fric � l'�tat. On prend du pognon mais on en rapporte aussi pas mal. J'attend de voir... Qui vivra verra.
Phper
Petite correction, avec mes excuses.
Le MEDEF et ses amis de la Finances et de
la politique lancent un rideau de fum�e
pour diviser tous les Fran�ais et �a marche. On pointe du doigt les r�gimes
sp�ciaux, les fonctionnaires, les ch�meurs, etc.
� La France est en d�ficit et c'est la
crise, il faut r�former � et qui doit payer ?
Tel est le discours que l'on mart�le �
longueur de journ�e aux classes moyennes (ce qu�il en reste) et autres
(soi-disant assist�es) c'est � eux de se serrer la ceinture.
Pendant ce temps on ne parle pas des
milliards d'euros dans les paradis fiscaux, on ne nous dit pas comment on va
les r�cup�r�, et les grandes entreprises qui excellent en mati�re
d'optimisation fiscale. Le march� du luxe ne cesse de cro�tre ainsi que les
grandes fortunes sans parler de l'Empire de la Finance qui sp�cule sur les
mati�res premi�res pendant que les autres s'appauvrissent de plus en plus.
Alors � qui profite la crise ?
PS Avant de subir les quolibets de certains, je pr�cise que je suis apolitique
mais que je m'efforce d'avoir un regard le plus large possible sur le monde
dans lequel je vis.
Sur ce, je vous laisse � vos r�flexions.
Phper
Le MEDEF et ses amis de la finances et de la politique lancent un rideaux de fum�e
pour diviser tous les Fran�ais et �a marche. On pointe du doit les r�gimes sp�ciaux, les fonctionnaires, les ch�meurs, etc. La France est en d�ficit et c'est la crise il faut
r�former, et qui doit payer ? Le discours que l'on mart�le � longueur de journ�e aux classes moyennes (ce qui l'en reste) et autres (soi-disant assist�es) c'est � eux de se serrer la ceinture. Pendant ce temps on ne parle pas des milliards d'euros dans les paradis fiscaux, on ne nous dit pas comment on vas les r�cup�r�, et les grandes entreprises qui excellent en mati�re d'optimisation fiscale. Le march� du luxe ne cesse de cro�tre ainsi que les grandes fortunes sans parl� de l'empire de la Finance pendant que les autres s'appauvrissent de plus en plus. Alors � qui profite la crise ?
PS avant de subir les quolibets de certains, je pr�cise que je suis apolitique mais que je m'efforce d'avoir un regard le plus large possible du monde dans lequel je vis.
Sur ce, je vous laisse � vos r�flexions.
norbertgabriel
""Le parcours de star est un long travail maxillaire sur le si�ge arri�re d'une voiture de producteur, avant tout."
Depardieu a fait �a ?  Gabin aussi ??  Belmondo ??? Clint Eastwood ???
karpis
Ah les intermittents. Ceux qui ne sont pas pris parce que ... ben finalement ils ne sont pas si bon que �a, peut-�tre, ou qu'ils n'ont pas couch� avec les bonnes personnes ou le Pr�sident Lemou.
Le parcours de star est un long travail maxillaire sur le si�ge arri�re d'une voiture de producteur, avant tout.
Bref les intermittents, c'est la bonne conscience des autres, des �lus qui se font eux, du fric. Et ont raison de le faire, mais ne veulent pas pour autant partager. La bont� a des limites, on est dans le showbiz quand m�me.
Le faux-cul � la fran�aise port� en pendentif.
Dans la m�me cat�gorie :
