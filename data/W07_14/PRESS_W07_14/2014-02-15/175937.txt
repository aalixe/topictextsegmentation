TITRE: 20 Minutes Online - �10 centi�mes l autre jour, maintenant c est 7� - Actualitesinternationales
DATE: 2014-02-15
URL: http://www.20min.ch/ro/jo2014/actualitesinternationales/story/Lara-Gut-au-pied-du-podium-26769404
PRINCIPAL: 175934
TEXT:
Sotchi - Lara Gut 4e du super-G
�10 centi�mes l'autre jour, maintenant c'est 7�
La Tessinoise est rentr�e les mains vides du super-G des JO de Sotchi. Elle a �chou� au pied du podium, lors d'une course remport�e par l'Autrichienne Anna Fenninger.
Elle visait l�or, elle a obtenu du chocolat! Un d�nouement forc�ment amer pour Lara Gut. Priv�e mercredi du titre en descente pour dix centi�mes (3e rang), elle a termin� samedi � ce qu'il est coutume d'appeler la pire des places, � seulement sept centi�mes de la m�daille de bronze d'une autre Autrichienne, Nicole Hosp. �Dix centi�mes l�autre jour , maintenant c�est 7�, a-t-elle l�ch� au micro de la RTS apr�s la course.
Sur un parcours au trac� vicieux, la Tessinoise �tait pourtant bien partie. Mais une l�g�re correction de trajectoire en fin de course lui a co�t� un temps pr�cieux, l'emp�chant de conserver sa place sur le podium.
Si une m�daille semblait dans ses cordes, il n'y avait en revanche rien � faire face � Anna Fenninger, l'une de ses meilleures amies sur le circuit. Profitant du trac� de son entra�neur Florian Winkler, l'Autrichienne a fait une belle d�monstration, ne tremblant que lors du passage de l'Allemande Maria H�fl-Riesch, finalement class�e au 2e rang � 0''55.
De son c�t�, Fabienne Suter s'est class�e au 7e rang, glanant une �ni�me place d'honneur aux Jeux. Entre les joutes de Vancouver 2010 et celles de Sotchi, c'est la cinqui�me fois que la Schwytzoise termine dans le top 7, mais sans jamais monter une seule fois sur le podium.
Partie avec le dossard no 9, la skieuse de Sattel avait pourtant eu l'intelligence d'adapter sa course aux circonstances. Apr�s avoir vu sept des huit skieuses parties avant elle se faire �liminer (!), elle a effectivement pu passer toutes les portes, mais en se montrant un brin trop prudente pour viser le podium.
Trois jours apr�s son titre en descente, Dominique Gisin est, elle, redescendue de son nuage. Comme beaucoup d'autres, l'Obwaldienne a �t� �limin�e, victime du trac� de Florian Winkler qui, d�cid�ment, ne se sera pas fait beaucoup d'amies - les Autrichiennes except�es - samedi.
Quatri�me Suissesse en lice, Fr�nzi Aufdenblatten a cr�nement jou� sa chance pour la derni�re course olympique de sa carri�re. Et alors qu'elle n'�tait pas attendue aux avant-postes, la Valaisanne a bluff� son monde en terminant au 6e rang.
Avec trois Suissesses parmi les sept premi�res, le bilan de l'�quipe entra�n�e par Hans Flatscher serait excellent en Coupe du monde. Las, ce sont les JO et seules les m�dailles ont vraiment de la valeur...
(20 minutes/si/afp)
