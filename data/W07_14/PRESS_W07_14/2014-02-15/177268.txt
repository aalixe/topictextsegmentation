TITRE: Victoires de la musique. Audience en hausse pour 29e �dition
DATE: 2014-02-15
URL: http://www.ouest-france.fr/victoires-de-la-musique-audience-en-hausse-pour-29e-edition-1933675
PRINCIPAL: 177266
TEXT:
Victoires de la musique. Audience en hausse pour 29e �dition
France -
Achetez votre journal num�rique
Les 29�me Victoires de la Musique 2014 vendredi soir sur France 2 ont attir� 3 millions de t�l�spectateurs, soit 15,8% de part d'audience.
Il s'agit du meilleur score des Victoires de la Musique depuis mars 2010 en audience et part d'audience, a soulign� France 2. Les Victoires n'avaient en effet plus d�pass� la barre des 3 millions depuis 2010.
Les Victoires de la Musique �taient aussi en t�te des audiences sociales avec 100 000 tweets, selon les chiffres de Mesagraph cit�s par France 2.
Les Victoires ont cependant �t� devanc�es par "Qui veut gagner des millions", sur TF1 (5,1 millions de t�l�spectateurs, 23% de part d'audience) et par le premier �pisode in�dit de la s�rie "Elementary" sur M6 (3,9 millions), ont indiqu� les deux cha�nes.
Cette longue c�r�monie musicale, pr�sent�e par Virginie Guilhaume et Bruno Guillon, qui a dur� de 20h30 � minuit, �maill�e de quelques incidents techniques, a couronn� sans surprise Stromae, qui a rafl� trois troph�es dont celui d'artiste masculin, tandis que Vanessa Paradis a �t� sacr�e artiste f�minine de l'ann�e pour la troisi�me fois de sa carri�re.
