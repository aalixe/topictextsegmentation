TITRE: Rising Stars Challenge : Andre Drummond conduit la Team Hill � la victoire
DATE: 2014-02-15
URL: http://basket-infos.com/2014/02/15/rising-stars-challenge-andre-drummond-conduit-la-team-hill-a-la-victoire/
PRINCIPAL: 176059
TEXT:
Accueil � Infos � Rising Stars Challenge : Andre Drummond conduit la Team Hill � la victoire
Rising Stars Challenge : Andre Drummond conduit la Team Hill � la victoire
Publi� par : Christophe Brouet f�vrier 15, 2014 , 8:28 dans Infos , R�sum�s et highlights NBA
Avec 30 points et surtout un record de 25 rebonds (dont 14 offensifs) au�Rising Stars Challenge, Andre Drummond a men� la Team Hill a la victoire 142-136 face � la Team Webber. Le Piston a logiquement �t� �lu MVP et il succ�de � Kenneth Faried qui avait lui aussi r�alis� un gros match l�an pass� (40 points et 10 rebonds). En plus de �a il a m�me rentr� ses lancers avec un joli 6/8 alors qu�il tourne � 41% en saison r�guli�re dont des pr�cieux en toute fin de rencontre pour assurer la victoire des siens.
Mais outre cette domination d�Andre Drummond on a eu droit � un superbe duel entre Dion Waiters�(Team Hill) et Tim Hardaway Jr auteurs respectivement de 31 et 36 points (7 tirs � trois points). Dans la Team Hill Bradley Beal n�a pas �t� en reste �galement avec 21 points.
En face pour la Team Webber outre Tim Hardaway Jr,��Michael Carter-Williams compile 17 points, 9 passes et 6 rebonds alors que Damian Lillard a �t� plus discret avec 13 points et 5 passes. Mason Plumlee ajoute lui 20 points.
Un match dont l�issue s�est d�cid�e en toute fin de rencontre puisque � 4 minutes du terme le score �tait de 118 partout. Le duel Waiters � Hardaway Jr a continu� et les deux joueurs ont �chang� les tirs � trois points et c�est finalement le Cav qui a mis les siens devant 126-124 gr�ce � 8 points de suite avant qu�Andre Drummond ne donne 4 points d�avance � la Team Hill � 2 minutes. Puis Bradley Beal a fait le break sur un shoot � trois points pour prendre 5 points d�avance � 90 secondes de la fin. Le reste s�est jou� aux lancers et Beal, Drummond et Barnes n�ont pas craqu�.
