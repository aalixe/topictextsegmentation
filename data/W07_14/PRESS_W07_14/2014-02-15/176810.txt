TITRE: Comment les comptes de la SNCF sont pass�s au rouge sur l'exercice 2013
DATE: 2014-02-15
URL: http://www.boursier.com/actualites/economie/comment-les-comptes-de-la-sncf-ont-vire-au-rouge-l-an-dernier-23009.html?sitemap
PRINCIPAL: 176809
TEXT:
Comment les comptes de la SNCF sont pass�s au rouge sur l'exercice 2013
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � La SNCF a publi� jeudi des r�sultats dans le rouge pour l'ann�e 2013... Le groupe affiche affiche une perte nette de 180 millions d'euros, contre un b�n�fice de 376 millions d'euros un an plus t�t. Une mauvaise performance qui s'explique notamment par "une d�pr�ciation comptable des rames TGV de 1,4 milliard d'euros", explique la SNCF.
Le chiffre d'affaires a atteint 32,232 milliards d'euros, en l�g�re progression de +0,5% �� p�rim�tre et change constants par rapport � 2012. La marge op�rationnelle ressort � 2,804 milliards d'euros, soit 8,7% du chiffre d'affaires,�dop�e notamment par les aide du CICE (Cr�dit imp�t comp�titivit�) et le r�sultat net r�current 582 millions d'euros.
Plongeon du TGV
Concernant le TGV, la SNCF explique que le plongeon de l'activit�, en France et en Europe, s'est aggrav� en 2013. L'augmentation des p�ages ferroviaires, la faiblesse des trafics li�e � la crise �conomique et � la concurrence des avions low-cost et du co-voiturage ont notamment pes�. "La valeur des actifs TGV n'�tant pas couverte par les esp�rances de flux de tr�sorerie futurs d�gag�s par l'activit�, le groupe a d� constater dans ses comptes, une perte de valeur de 1.400 millions d'euros sur le mat�riel roulant TGV", d�taille la soci�t�, ce qui a fait passer le r�sultat net dans le rouge.
Excellence 2020
Le cash-flow libre de 464 millions d'euros est positif pour la 3�me ann�e cons�cutive, atteignant un nouveau record. Enfin, conform�ment au projet strat�gique 'Excellence 2020', l'endettement net est stabilis� sous les 7,5 milliards d'euros. La SNCF a recrut� plus de 10.000 nouveaux collaborateurs en 2013, tout en r�alisant plus de 225 millions d'�conomies sur les frais de structure de la maison m�re (frais g�n�raux, Achats et informatique), d�passant ainsi l'objectif du plan de performance transverse...
Claire Lemaitre � �2014, Boursier.com
