TITRE: JO 2014: la cinqui�me fois fut la bonne pour Alla Tsuper - Ski - Sports - Arcinfo - site de L'Express, L'Impartial et de Canal Alpha
DATE: 2014-02-15
URL: http://www.arcinfo.ch/fr/sports/ski/jo-2014-la-cinquieme-fois-fut-la-bonne-pour-alla-tsuper-570-1262114
PRINCIPAL: 175522
TEXT:
JO 2014: la cinqui�me fois fut la bonne pour Alla Tsuper
Ski Freestyle
Alla Tsuper vole au-dessus du podium.
Cr�dit: KEYSTONE
Tous les commentaires (0)
Alla Tsuper d�croche la m�daille d'or en saut acrobatique. La Bi�lorusse de 34 ans en est � sa cinqui�me participation aux Jeux olympiques.
La Chinoise Xu Mengtao, championne du monde en titre et vice-championne du monde 2009 et 2011, a pris la m�daille d'argent alors que celle de bronze est revenue � l'Australienne Lydia Lassila, m�daill�e d'or il y a quatre ans � Vancouver. Lassila s'entra�ne avec les Suissesses depuis des ann�es.
Li Nina, vice-championne olympique en 2006, championne du monde 2009 et actuelle leader de la Coupe du monde, a �galement particip� � la derni�re phase de la finale, mais a pris la 4e place apr�s une chute � la r�ception de son saut.
En 1998, Tsuper avait pris la 5e place des Jeux de Nagano sous les couleurs de l'Ukraine, son pays de naissance. Elle avait quitt� ce pays cette m�me ann�e pour suivre son entra�neur au Belarus et avait particip� aux JO en 2002 (9e), en 2006 (10e), en 2010 (8e) pour son pays d'adoption.
La Zurichoise Tanja Sch�rer ne s'est pas qualifi�e pour la finale des douze meilleures, malgr� son statut d'outsider. Une chute a plomb� sa prestation. Il ne lui a manqu� que 0,35 point pour se qualifier.
Source: ATS
