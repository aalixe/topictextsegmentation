TITRE: Skicross: La Russe Komissarova gri�vement bless�e � la colonne vert�brale - DH.be
DATE: 2014-02-15
URL: http://www.dhnet.be/sports/sotchi2014/skicross-la-russe-komissarova-grievement-blessee-a-la-colonne-vertebrale-52ff4d493570c16bb1cd7e30
PRINCIPAL: 176420
TEXT:
Le Suisse Cologna vainqueur du 15 km classique
Sotchi 2014
La Russe Maria Komissarova s'est gri�vement bless�e � la colonne vert�brale samedi lors d'un entra�nement dans le parcours de skicross des jeux Olympiques de Sotchi et a d� �tre imm�diatement op�r�e, selon la F�d�ration russe et l'agence russe R-sport.
"L'athl�te a �t� victime d'un traumatisme s�rieux, a indiqu� la F�d�ration russe dans un communiqu�, sans pr�ciser la nature de la blessure. Elle a �t� �vacu�e d'urgence (du Parc extr�me de Rosa Khoutor) et transport�e � l'h�pital N.8 de Krasna�a Poliana, construit sp�cialement pour les JO. Les m�decins ont effectu� les examens n�cessaires et ont pris la d�cision d'op�rer l'athl�te imm�diatement sur place."
Komissarova souffrirait d'une fracture au niveau de la colonne vert�brale, avec d�placement, selon l'agence russe R-Sport, qui ajoute que la nature de sa blessure ne permet pas de la transporter � Moscou pour y �tre op�r�e.
La Russe de 23 ans, qui ne fait pas partie des cadors de la discipline (1 podium en Coupe du monde en 2012), ne pourra pas d�fendre ses chances aux jeux Olympiques, a ajout� sa F�d�ration.
Selon sa biographie r�dig�e par les services de presse des jeux Olympiques, sa devise est "Celui qui ne prend pas de risques ne boit pas de champagne".
Sur le m�me sujet :
