TITRE: France Monde | Anders Breivik juge sa Playstation trop vieille et menace de faire grève de la faim  - Le Bien Public
DATE: 2014-02-15
URL: http://www.bienpublic.com/actualite/2014/02/15/le-tueur-anders-breivik-juge-sa-playstation-trop-vieille-et-menace-de-faire-greve-de-la-faim
PRINCIPAL: 176343
TEXT:
France Monde
Anders Breivik juge sa Playstation trop vieille et menace de faire grève de la faim
Norvège - Terrorisme Anders Breivik juge sa Playstation trop vieille et menace de faire grève de la faim
Notez cet article :
le 15/02/2014      à 08:55
Photo archives AFP
L'auteur de la tuerie de l'île Utoeya en Norvège, Anders Breivik, menace de faire grève de la fim s'il n'obtient pas, entre autres, une nouvelle console de jeux vidéo.
Partager
Envoyer à un ami
Comme l'explique le Parisien , le terroriste a rédigé un courrier à l'administration carcérale. Il fait état de 12 demandes. Il exige ainsi le remplacement de sa Playstation 2 par une Playstation 3, avec des jeux pour adulte. Il veut également remplacer sa chaise par un fauteuil ou un sofa. Le détenu le plus célèbre de Norvège met en avant sa « bonne conduite » pour justifier ses demandes. Il explique également que ses exigences portent sur des droits fondamentaux qui rendront son séjour « conforme à la réglémentation européenne ».
Rappelons qu'Anders Breivik a été condamné à 21 ans de prison après avoir été reconnu coupable de la tuerie de l'île d'Utoeya qui, en 2012, avait fait plus de 80 victimes.
Vos commentaires
