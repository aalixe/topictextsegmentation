TITRE: Foot - Angleterre - Fulham - Magath nouvel entra�neur
DATE: 2014-02-15
URL: http://www.lequipe.fr/Football/Actualites/Magath-nouvel-entraineur/441469
PRINCIPAL: 0
TEXT:
a+ a- imprimer RSS
Fulham annonce vendredi l'arriv�e sur le banc de F�lix Magath , �avec effet imm�diat�, sans pr�ciser ce qu'il advient de l'entra�neur en poste, le N�erlandais Ren� Meulensteen. L'Allemand, qui a sign� un contrat de dix-huit mois, d�barque dans un club en grande difficult�, dernier de la Premier League, � quatre points du premier non-rel�gable (WBA). L'ancien jouuer du HSV (60 ans) �tait sans club depuis son d�part de Wolfsburg en octobre 2012.
Le propri�taire des Cottagers, le milliardaire am�ricano-pakistanais Shahid "Shad" Khan, salue en Magath un entra�neur �accompli�. �Je suis impressionn� en particulier par la r�putation que Felix a de reprendre des clubs dans des moments difficiles, souvent tard dans la saison, et d'en tirer tout le potentiel et m�me au-del�.�
Trois fois champion en Bundesliga
Felix Magath a conduit le Bayern Munich � deux titres d'affil�e en 2005 et 2006, avant de r�aliser la m�me performance avec Wolfsburg en 2009. Quant � Meulensteen, c'est la seconde fois cette saison qu'il est remerci�, apr�s son renvoi par les Russes d'Anzhi Makhachkala, en ao�t, 16 jours seulement apr�s sa prise de fonction. Cette fois, il aura tenu deux mois.
Felix Magath sera le premier entra�neur allemand � officier en Premier League (Opta).
Fulham have announced the hiring of Felix Magath as First Team Manager, effective immediately. http://t.co/5ytybDdMjG pic.twitter.com/1fQWSjF7gI
