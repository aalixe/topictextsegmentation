TITRE: Orange: annonce le lancement commercial d'Orange Cash. - L'Express avec Votre Argent
DATE: 2014-02-15
URL: http://votreargent.lexpress.fr/bourse-de-paris/orange-annonce-le-lancement-commercial-d-orange-cash_359606.html
PRINCIPAL: 176716
TEXT:
Suivant
Interviews de la lettre de la bourse
Jean Pierre Roturier, pr�sident d'Euromedis Groupe: "Nous nous consid�rons plus comme un pr�dateur que comme une cible."
Le patron de cet acteur de l'industrie des dispositifs m�dicaux et de l'assistance m�dicale � domicile revient sur la publication des comptes tr�s solides du premier semestre de l'exercice 2013/2014. Il nous explique comment sa soci�t� peut viser un chiffre d'affaires de 100 millions d'euros et une marge op�rationnelle comprise entre 7% et 7,5% � l'horizon 2015/2016.
Dominique Henri, pr�sident directeur g�n�ral d'Heurtey Petrochem : "Notre priorit� est de saisir des opportunit�s dans l'univers du gaz"
Nous avons saisi l'occasion de la publication des comptes annuels de cet acteur d'ing�nierie p�troli�re et gazi�re pour faire le point avec son pr�sident, Dominique Henri, sur le dernier exercice, les perspectives pour cette ann�e et le projet d'appel au march�.
