TITRE: Crise politique en Italie - Poursuite des consultations avant la nomination de Matteo Renzi - DH.be
DATE: 2014-02-15
URL: http://www.dhnet.be/dernieres-depeches/belga/crise-politique-en-italie-poursuite-des-consultations-avant-la-nomination-de-matteo-renzi-52ff44cd3570516ba0bc4012
PRINCIPAL: 176343
TEXT:
Vous �tes ici: Accueil > Derni�res d�p�ches
Crise politique en Italie - Poursuite des consultations avant la nomination de Matteo Renzi
Publi� le
15 f�vrier 2014 � 11h41
BRUXELLES (Belgique)
Le pr�sident italien Giorgio Napolitano a repris samedi � 10h00 ses consultations apr�s la d�mission � la t�te du gouvernement d'Enrico Letta vendredi. La classe politique italienne attend maintenant la nomination de son rempla�ant, le jeune maire de Florence Matteo Renzi.
Le premier � franchir la porte du palais pr�sidentiel du Quirinal a �t� Daniel Alfreider, repr�sentant de la minorit� linguistique du Sud Tyrol. Il devait �tre suivi par Albert Laniece et Rudi Franco Marguerettaz, de la province francophone du Val d'Aoste. D'autres micro-partis seront re�us par le pr�sident avant que les choses s�rieuses ne d�butent samedi apr�s-midi, avec la venue des repr�sentants du "Nouveau centre droit" (NCD), la branche rebelle de Forza Italia (FI), le parti de Silvio Berlusconi entr� dans l'opposition apr�s avoir soutenu les premiers mois du gouvernement Letta. Leader du NCD et ex-ministre de l'Int�rieur d'Enrico Letta, Angelino Alfano a annonc� dans un entretien au quotidien "Messaggero" qu'il attendait que Matteo Renzi lui pr�sente un "programme pr�cis". Le pr�sident Napolitano ayant refus� la tenue de nouvelles �lections, Matteo Renzi devra composer avec la formation actuelle du Parlement, qui n'avait d�j� pas d�gag� de majorit� nette au printemps dernier. En �change de son appui au futur ex�cutif du secr�taire du Parti d�mocrate (PD, gauche), Angelino Alfano devrait conserver son poste au minist�re de l'Int�rieur. Enfin, � 18h30, Silvio Berlusconi sera re�u par M. Napolitano en tant que pr�sident de Forza Italia. Les consultations se termineront en fin de journ�e par la r�ception de deux repr�sentants du PD. Matteo Renzi n'�tait pas attendu au Quirinal samedi. Sa d�signation en tant que chef du gouvernement devrait intervenir vraisemblablement dimanche ou lundi. (Belga)
� 2014 Belga. Tous droits de reproduction et de repr�sentation r�serv�s. Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par Belga. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, rediffus�e, traduite, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de Belga.
Derni�res d�p�ches
