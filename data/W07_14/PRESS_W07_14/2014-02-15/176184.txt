TITRE: Le PSG a bien pr�par� Leverkusen - Paris SG - Homes Clubs - Ligue 1 - Football -
DATE: 2014-02-15
URL: http://sport24.lefigaro.fr/football/ligue-1/Homes-Clubs/paris-sg/actualites/le-psg-a-bien-prepare-leverkusen-679208
PRINCIPAL: 0
TEXT:
Le PSG a bien pr�par� Leverkusen
Par Flavien Chailleux, 14-02-2014
Tweeter
-  Panoramic
Avant de retrouver la Ligue des Champions, le Paris SG a facilement dispos� de Valenciennes (3-0). Monaco, en d�placement samedi � Bastia, est provisoirement rel�gu� � huit points des Parisiens.
Paris SG-Valenciennes 3-0
Paris SG : Lavezzi (19e), Ibrahimovic (50e), csc Kagelmacher (52e)
A quatre jours de son 8e de finale aller � Leverkusen, le Paris SG a atteint son objectif du week-end en championnat en dominant facilement Valenciennes en ouverture de la 25e journ�e (3-0). Un succ�s qui lui permet de repousser provisoirement Monaco � huit longueurs, en attendant le d�placement des joueurs du Rocher samedi � Bastia. De plus, les Parisiens se sont montr�s plut�t convaincants dans le jeu et dans l�intention, eux qui �taient scrut�s apr�s plusieurs r�centes prestations en demi-teinte. Parmi les satisfactions, Laurent Blanc pourra relever la bonne performance de ses joueurs habituellement abonn�s au banc de touche, dont figure d�sormais Yohan Cabaye, titularis� pour la premi�re fois sous le maillot du PSG. Bien qu�oppos� � un onze remani�, la formation d�Ariel Jacobs choisissait de faire le dos rond avec pas moins de huit joueurs au profil d�fensif pour prot�ger au mieux la cage de Penneteau.
Mais la strat�gie ultra-d�fensive du technicien valenciennois affichait rapidement ses faiblesses. Pris sur les longs ballons, battus sur les couloirs, la d�fense de VA n�avait d�autres choix que de s�en remettre � Penneteau. Ce dernier maintenait son �quipe � flot en s�interposant devant Thiago Silva (9e), Lavezzi (24e) et Ibrahimovic (36e). Malgr� un gardien adverse en verve, Lavezzi parvenait � ouvrir la marque tel un renard des surfaces (1-0, 19e). Un court avantage qui se r�v�lait par moment fragile pour les Parisiens, mis en danger sur une frappe de Ducourtioux qui flirtait avec leur cadre (31e). Les locaux attendaient finalement le d�but de la deuxi�me p�riode pour se mettre � l�abri. Ibrahimovic prenait ses responsabilit�s en doublant la mise d�une vol�e smash�e du droit (2-0, 50e) avant de mettre sous pression Kagelmacher qui marquait contre son camp (3-0, 52e). L�affaire �tait pli�e pour le PSG qui s�est rassur� avant son d�placement en Allemagne dans quatre jours. Un rendez-vous qui en dira un peu plus sur les pr�tentions europ�ennes du champion de France en titre�
Les joueurs du match
Ezequiel Lavezzi aura v�cu une semaine particuli�re. Endeuill� apr�s la perte brutale de son oncle, l�Argentin �tait bien au rendez-vous contre Valenciennes. Titulaire, il a mis toute son �nergie dans la rencontre, se procurant de nombreuses occasions m�me s�il n�en a concr�tis� qu�une seule, celle de l�ouverture du score. Pour sa premi�re titularisation, Yohan Cabaye s�est montr� � son aise dans l�entrejeu. L�international fran�ais a notamment affich� une entente vite �vidente avec Thiago Motta. Comme � son habitude, ce dernier a donn� l'impression de survoler son sujet.
On n�a pas aim�
Si Ariel Jacobs avait annonc� la couleur en ne titularisant que deux joueurs offensifs (Waris, Melikson), il ne dispose visiblement pas de l�effectif pour verrouiller une armada offensive de qualit�. A l�image de Gary Kagelmacher, tout proche de marquer un csc grossier en d�but de match� avant d�en conc�der finalement un en deuxi�me p�riode. Align� � sa gauche, Saliou Ciss a gagn� trop peu de duels tandis que Rudy Mater a pris l�eau sur son couloir face aux perc�es r�p�t�es de Lavezzi et Digne.
R�sultat de la 25e journ�e�:
Vendredi 14 f�vrier,
