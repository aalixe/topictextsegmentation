TITRE: JO/hockey: les Etats-Unis battent la Russie aux tirs au but | Site mobile Le Point
DATE: 2014-02-15
URL: http://www.lepoint.fr/sport/jo-hockey-les-etats-unis-battent-la-russie-aux-tirs-au-but-15-02-2014-1792109_26.php
PRINCIPAL: 177292
TEXT:
15/02/14 à 16h53
JO/hockey: les Etats-Unis battent la Russie aux tirs au but
Les Etats-Unis ont pris une option sur la qualification directe en quarts de finale du tournoi olympique après leur victoire sur le fil aux dépens de la Russie 3-2 aux tirs au but, samedi à Sotchi, dans le match au sommet du groupe A.
En cas de succès dimanche contre la Slovénie , classée au 17e rang, les Américains seraient assurés de terminer premiers du groupe A.
T.J. Oshie a inscrit les quatres tirs au but des Etats-Unis au cours d'une séance à suspense de huit tirs par équipe.
De son côté, la Russie, grâce au point pris après sa défaite aux tirs au but, pourrait prendre la place de meilleur deuxième, qualificative directement pour les quarts. Mais elle doit battre la Slovaquie dimanche.
