TITRE: REPETE: Minerals Technologies d�pose une contre-offre sur AMCOL face � Imerys | Zone bourse
DATE: 2014-02-15
URL: http://www.zonebourse.com/MINERALS-TECHNOLOGIES-INC-13637/actualite/REPETE-Minerals-Technologies-depose-une-contre-offre-sur-AMCOL-face-a-Imerys-17950819/
PRINCIPAL: 175713
TEXT:
REPETE: Minerals Technologies d�pose une contre-offre sur AMCOL face � Imerys
14/02/2014 | 16:22
Recommander :
0
(Actualisation: confirmation de la contre-offre de Minerals Technologies, d�clarations du pr�sident de Minerals Technologies, r�action en Bourse et annonce de Moody's sur la note d'Imerys.)
Le groupe de traitement des min�raux Minerals Technologies ( >>�Minerals Technologies Inc ) a lanc� une offre de 42 dollars par action sur le sp�cialiste am�ricain de la bentonite AMCOL ( >>�AMCOL International Corporation ), d�j� convoit� par le fran�ais Imerys ( >>�IMERYS ), a-t-il annonc� vendredi dans un communiqu�, confirmant des informations du Wall Street Journal.
L'offre de Minerals Technologies est sup�rieure d'un dollar par action � celle d'Imerys, qui a annonc� mercredi avoir propos� 41 dollars par action pour reprendre AMCOL.
"Notre proposition est clairement plus �lev�e que l'accord existant pour AMCOL, et nous attendons avec impatience de travailler avec AMCOL pour mettre en oeuvre cette transaction sup�rieure dans les meilleurs d�lais possibles", a d�clar� le pr�sident du conseil d'administration de Minerals Technologies, Joseph C. Muscari, cit� dans le communiqu�.
Contact�e par Dow Jones Newswires, une porte-parole d'Imerys n'a pas souhait� r�agir.
Avec un chiffre d'affaires de plus d'un milliard de dollars en 2013, AMCOL est le leader mondial de la bentonite, un min�ral utilis� dans de nombreux domaines comme l'automobile et les machines-outils, ou encore dans la consommation courante.
Vendredi, l'action AMCOL a ouvert en hausse de 6,8% � 44,15 dollars sur le New Nork Stock Exchange, donnant une capitalisation boursi�re de 1,3 milliard de dollars. Le titre valait 36,72 dollars � la cl�ture mardi, avant l'annonce de l'OPA amicale d'Imerys.
De son c�t�, le titre Minerals Technologies progressait de 0,8% � 53,50 dollars. A la Bourse de Paris, l'action Imerys grappillait 0,02% � 63,95 euros, apr�s avoir recul� de plus de 3% dans la matin�e.
Minerals Technologies a envoy� jeudi une lettre au conseil d'administration d'AMCOL, lui exposant les modalit�s de son offre. D'apr�s une source proche du dossier, les discussions entre les deux soci�t�s avaient d�but� l'an dernier, mais se sont arr�t�es lorsqu'AMCOL a entam� des n�gociations exclusives avec Imerys.
Selon les lois en vigueur dans l'Etat du Delaware, o� les statuts d'AMCOL sont enregistr�s, les conseils d'administration des soci�t�s cot�es qui ont donn� leur accord � une offre d'achat en num�raire doivent g�n�ralement accepter l'offre la plus �lev�e.
Importantes synergies pour Imerys
De son c�t�, Imerys avait annonc� mercredi la signature d'un accord en vue du rachat d'AMCOL pour un montant d'environ 1,6 milliard de dollars, dette financi�re incluse. Lors de l'annonce, les deux groupes avaient indiqu� que le projet avait �t� "unanimement" approuv� par leur conseil d'administration respectif. Cela comprend les membres de la famille actionnaire d'AMCOL, qui contr�le encore pr�s de 20% du capital du groupe am�ricain.
Le rachat d'AMCOL permettrait � Imerys de d�gager "d'importantes synergies commerciales et op�rationnelles", sans que le groupe n'ait toutefois apport� de pr�cisions chiffr�es.
A la suite de cette annonce, l'agence Moody's a plac� vendredi la note de cr�dit long terme "Baa2" d'Imerys sous surveillance n�gative, indiquant que l'op�ration devrait faire ressortir un ratio flux de tr�sorerie sur dette nette inf�rieur � ses crit�res pour sa note actuelle. L'agence reconna�t toutefois que l'acquisition d'AMCOL "pr�sente un r�el int�r�t strat�gique" pour Imerys et qu'une �ventuelle d�gradation de la note serait limit�e � un cran, sur la base d'une offre � 41 dollars par action.
-Dana Mattioli et Everdeen Mason, The Wall Street Journal
(Version fran�aise Blandine H�nault)
