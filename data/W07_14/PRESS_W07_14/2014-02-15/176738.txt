TITRE: Attention au zéro pointé pour le ski français - Europe1.fr - Multisports
DATE: 2014-02-15
URL: http://www.europe1.fr/Sport/Multisports/Articles/Attention-au-zero-pointe-pour-le-ski-francais-1802539/
PRINCIPAL: 176734
TEXT:
Attention au z�ro point� pour le ski fran�ais
Par Victor Dhollande-Monnier
Tweet
Thomas Mermillod Blondin a chut� juste avant l'arriv�e lors du super-combin�, vendredi. � REUTERS
FIASCO - Le ski alpin a rat� sa premi�re semaine des Jeux olympiques de Sotchi. Attention aux derni�res �preuves.
Encore bredouille. Il y a quatre ans � Vancouver, le ski fran�ais �tait revenu sans aucune m�daille. Apr�s une semaine de comp�tition et plusieurs d�sillusions, la menace du z�ro point� place encore. Et ce n'est pas la nouvelle sortie de piste de Marie Marchand-Arvier, samedi lors du super-G , qui devrait rassurer l'�quipe de France...
