TITRE: Ski alpin - JO (F) - Le super-G �tait-il trop dur ?
DATE: 2014-02-15
URL: http://www.lequipe.fr/Ski/Actualites/Un-super-g-trop-dur/441637
PRINCIPAL: 176627
TEXT:
Marie Marchand-Arvier, une des victimes du jour. (L'Equipe)
Le tra�age
Le tirage au sort avait d�sign� l�entra�neur autrichien Florian Winkler pour dessiner le super-G sur cette montagne qui offrait un mur d�entr�e, une partie plus plate avec des mouvements de terrain et un mur final. Le coach y a dessin� une course au rythme assez vari� : tournant sur le premier secteur, plus fluide ensuite et empil� pour finir. � Je voulais tracer une course pour les bonnes skieuses. En Autriche, nous avons de bonnes techniciennes, comme Anna (Fenninger), Nicole (Hosp) et Liz (G�rgl), explique Winkler. Je ne voulais pas le faire trop dur mais il fallait r�fl�chir. �� A l�arriv�e, il s�est dit � tr�s surpris � du taux d��chec chez les skieuses .
L'inspection
En raison des conditions de neige assez molles pr�s de l�aire d�arriv�e, les entra�neurs n�ont pas pu accompagner les skieuses lors de l�inspection avant la course. �Elles se sont retrouv�es toutes seules et certaines se sont tromp�es sur l�appr�ciation de l�entr�e dans le mur final, analyse Thomas Stauffer, entra�neur de l��quipe d�Allemagne. Apr�s le saut, la premi�re porte �tait tr�s serr�e, et ensuite la neige �tait tr�s molle et le ski r�pondait mal sur le pied. � �Cela nous arrangeait car une fille comme Anna (Fenninger) est toujours tr�s bonne � l�inspection. Elle a toujours le bon feeling pour choisir la bonne ligne�, se f�licite Winkler.
La course
Les premi�res portes du super-G font plusieurs victimes comme Marie Marchand-Arvier ou la Canadienne Marie-Mich�le Gagnon mais c�est surtout le mur final qui �cr�me la start-list. M�me une g�antiste comme Jessica Lindell-Vikarby se fait pi�ger dans l�empilement des portes et d�rape. �Les informations sont remont�es assez rapidement mais c��tait clairement un d�savantage de partir avec un petit dossard�, raconte Stauffer dont la skieuse Maria Riesch prendra l�argent avec le num�ro 22. M�me si je suis sortie en descente, je savais que je pouvais aller vite, s�est f�licit�e la nouvelle championne olympique Anna Fenninger. �La derni�re section �tait le passage cl� et les coaches ont fait du tr�s bon boulot en faisant remonter l�information. � Fenninger, Riesch, Hosp, Gut et Maze dans le top 5 : � Les bonnes skieuses sont devant �, r�sume le coach fran�ais Nicolas Burtin.
A. T.-C., � Rosa Khutor
