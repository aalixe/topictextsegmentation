TITRE: BBM se transforme en r�seau social
DATE: 2014-02-15
URL: http://fr.canoe.ca/techno/mediassociaux/archives/2014/02/20140214-102802.html
PRINCIPAL: 175429
TEXT:
BBM se transforme en r�seau social
�Photo Reuters
Tweeter
14-02-2014 | 10h28
Avec sa derni�re mise � jour, l'application BBM se transforme en un  v�ritable r�seau social, avec ses statuts et ses �changes group�s.
� la mani�re d'un Facebook, le nouveau BBM (2.0) permet � ses  utilisateurs de se cr�er un profil, d'aimer (pouce lev�) et de commenter  des statuts, d'�changer du contenu multim�dia avec un ou plusieurs  contacts (photos, vid�os, etc.), de participer � des groupes de  discussions ou encore de s'inscrire � des �chaines� (BBM Channels)  d�di�es � des marques, des �v�nements ou des personnalit�s.
Dans tous les cas, les contacts doivent se valider mutuellement pour pouvoir communiquer ensemble.
Depuis septembre 2013, l'application BBM est � t�l�charger sur Google Play (Android) et l' App Store (iOS).
