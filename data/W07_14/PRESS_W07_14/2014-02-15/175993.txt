TITRE: Temp�te Ulla : 1 500 passagers bloqu�s en gare de Rennes. Info - Rennes.maville.com
DATE: 2014-02-15
URL: http://www.rennes.maville.com/actu/actudet_-tempete-ulla-1-500-passagers-bloques-en-gare-de-rennes_fil-2491893_actu.Htm
PRINCIPAL: 175992
TEXT:
Google +
Les passagers sont pris en charge par la SNCF.� Ouest-France.
Branle-bas de combat en gare de Rennes. Plusieurs TGV en provenance de Paris et � destination de Brest et Saint-Brieuc ont d� faire demi-tour.
Ils sont plusieurs centaines de passagers de TGV actuellement en gare de Rennes. Leurs TGV, en provenance de Paris, n'ont pu atteindre Saint-Brieuc ou Brest. La temp�te Ulla mena�ant la s�curit� du trafic ferroviaire.
Pour �viter que des TGV ne se retrouvent bloqu�s entre deux gares, la SNCF a pr�f�r� les rapatrier � Rennes.
Les passagers ont �t� pris en charge par la SNCF, la S�curit� civile ainsi que par la Croix-Rouge. La plupart des voyageurs vont passer la nuit dans des TGV transform�s en dortoirs.�
Samuel Nohra�� Ouest-France��
