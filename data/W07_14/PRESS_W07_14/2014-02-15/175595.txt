TITRE: Ligue 2 | Ligue 2 : Dijon grimpe sur le podium !
DATE: 2014-02-15
URL: http://www.le10sport.com/football/ligue2/ligue-2-dijon-grimpe-sur-le-podium-136060
PRINCIPAL: 175591
TEXT:
Envoyer � un ami
Belle op�ration pour Dijon ce soir dans le cadre de la 24e journ�e de Ligue 2. Les Bourguignons se sont impos�s � domicile face au CA Bastia (3-0) gr�ce � des buts de Philippoteaux (18e), Tavares (72e) et Babit (85e). Au classement, Dijon monte provisoirement sur le podium en attendant les matches d�Angers (samedi) et de Lens (lundi).
�
Les r�sultats de la 24e journ�e�:
�
