TITRE: Affaire Lambert. Une décision avant l'été - France - Le Télégramme
DATE: 2014-02-15
URL: http://www.letelegramme.fr/france/affaire-lambert-une-decision-avant-l-ete-15-02-2014-10036471.php
PRINCIPAL: 0
TEXT:
Affaire Lambert. Une décision avant l'été
15 février 2014
Après un accident de voiture et un coma artificiel pour tenter de limiter les dégâts d'un traumatisme crânien massif, Vincent Lambert, un infirmier, s'est réveillé tétraplégique dans un état de conscience limitée à des informations sensorielles primaires.. Photo L'Union de Reims
Le Conseil d'État, réuni en formation collégiale, a décidé, hier... de ne pas décider, sur le cas Vincent Lambert. Pour l'instant du moins. Il ordonne une nouvelle expertise médicale et se prononcera après en avoir reçu les résultats, avant l'été.
Les trois médecins qui conduiront l'expertise médicale devront « se prononcer sur le caractère irréversible » des lésions cérébrales de Vincent Lambert et sur le pronostic clinique. Ils devront déterminer « si ce patient est en mesure de communiquer, de quelque manière que ce soit, avec son entourage » et devront apprécier si les réactions éventuellement décelées « peuvent être interprétées comme un rejet de soins, une souffrance » ou, au contraire, comme « un souhait que ce traitement soit prolongé », a expliqué hier le Conseil d'État.
Un rapport rendu au bout de deux mois
Les médecins devront rendre « leur rapport dans un délai de deux mois à compter de leur désignation ». Le Conseil d'État a également décidé de demander à l'Académie nationale de médecine, au Comité consultatif national d'éthique et au Conseil national de l'Ordre des médecins, des « observations écrites » de nature à « l'éclairer utilement sur l'application des notions d'obstination déraisonnable et de maintien artificiel de la vie ». « L'assemblée du contentieux, la plus haute formation de jugement du Conseil d'État, se prononcera avant l'été », a déclaré le vice-président du Conseil d'État, Jean-Marc Sauvé, dans une déclaration à la presse prononcée après la décision. L'épouse de Vincent Lambert, Rachel, et son neveu François, ainsi que le CHU de Reims où il est soigné, avaient saisi le Conseil d'État pour qu'il annule le jugement du tribunal administratif de Châlons-en-Champagne du 16 janvier ayant décidé le maintien en vie de Vincent Lambert, à la demande des parents, contre l'avis des médecins.
