TITRE: Archives Actualit�. Archives information en ligne : photos, media - Actu Orange
DATE: 2014-02-15
URL: http://actu.orange.fr/une/tempete-ulla-le-calme-revient-apres-un-deces-50-000-foyers-sans-electricite-afp_2832426.html
PRINCIPAL: 177032
TEXT:
utilisez un cr�dit pour consulter cet article pendant 7 jours.
Temp�te Ulla: le calme revient apr�s un d�c�s, 30.000 foyers sans �lectricit�
La temp�te Ulla, la plus forte de l'hiver selon M�t�o-France, s'est �loign�e progressivement samedi de la Bretagne et du Cotentin en direction de la mer du Nord, apr�s avoir fait un mort sur un paquebot et laiss� encore 30.000 foyers sans �lectricit� en d�but de soir�e.
les articles li�s
07/04/14
Kerry et Lavrov �voquent une prochaine r�union Ukraine-Russie-Etats-Unis-UE : Les chefs des diplomaties am�ricaine et russe, John Kerry et Sergue� Lavrov, ont discut� lundi d'une r�union d'ici dix jours entre les Etats-Unis, la Russie, l'Union europ�enne et l'Ukraine pour tenter de r�gler la crise ukrainienne, a indiqu� le d�partement d'Etat.
lire la suite
06/04/14
S�gol�ne Royal joue la montre sur Notre-Dame-des-Landes et l'�cotaxe : La ministre de l?�cologie, S�gol�ne Royal, a jou� la montre sur le projet sensible d'a�roport de Notre-Dame-des-Landes, o� elle attendra l'issue des recours en justice, comme sur l'�cotaxe, dimanche lors du Grand Jury RTL/LCI/Le Figaro.
