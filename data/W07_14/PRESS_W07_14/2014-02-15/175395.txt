TITRE: Ukraine: les opposants lib�r�s, la rue maintient la pression - La Libre.be
DATE: 2014-02-15
URL: http://www.lalibre.be/dernieres-depeches/afp/ukraine-les-opposants-liberes-la-rue-maintient-la-pression-52fe74443570c16bb1ccf04d
PRINCIPAL: 175389
TEXT:
Vous �tes ici: Accueil > Derni�res d�p�ches
Ukraine: les opposants lib�r�s, la rue maintient la pression
Publi� le
14 f�vrier 2014 � 20h51
Kiev (AFP)
Les autorit�s ukrainiennes ont annonc� vendredi avoir lib�r� tous les manifestants interpell�s en deux mois, le pr�sident Viktor Ianoukovitch appelant en retour l'opposition � "faire aussi des concessions", tandis qu'une nouvelle manifestation est pr�vue dimanche.
Les 234 manifestants lib�r�s sont assign�s � r�sidence et les accusations pesant sur eux ne sont pas abandonn�es, a indiqu� dans un communiqu� le procureur g�n�ral ukrainien, Viktor Pchonka.
"234 personnes ont �t� arr�t�es entre le 26 d�cembre et le 2 f�vrier. Aujourd'hui, plus aucune d'entre elles n'est en d�tention", a d�clar� le procureur, ajoutant que les poursuites, qui pourraient valoir aux opposants de lourdes peines de prison, seraient abandonn�es dans le mois � venir si les conditions fix�es par la loi d'amnistie �taient remplies.
"On m'a pouss� et on continue de me pousser � diff�rentes solutions pour r�gler la crise. Je ne veux pas faire la guerre. Je veux sauvegarder l'Etat et reprendre un d�veloppement stable", a d�clar� vendredi soir le pr�sident Viktor Ianoukovitch, dans un entretien t�l�vis�.
"Nous nous adressons � l'opposition pour qu'elle accepte aussi de faire des concessions", a-t-il ajout�.
Dans le centre de Kiev, sur la place de l'Ind�pendance - le Ma�dan -, occup�e  depuis novembre et entour�e de barricades, le "conseil" improvis� du mouvement de contestation a soulign� que, pour l'heure, les manifestants lib�r�es restaient menac�es de prison.
Une loi d'amnistie avait �t� vot�e en janvier, avec pour condition l'�vacuation des lieux publics et b�timents officiels occup�s par les contestataires, dont la mairie de la capitale. Ce d�lai expire lundi.
L'opposition exigeait de son c�t� la lib�ration sans conditions de toutes les personnes incarc�r�es, et la lev�e des poursuites.
L'une des responsable de l'opposition, l'ancienne Premi�re ministre emprisonn�e Ioulia Timochenko, a r�affirm� dans un entretien � para�tre samedi dans l'hebdomadaire Dzerkalo Tyjmia que "le seul sujet de n�gociation avec Ianoukovitch, c'est les conditions de son d�part et des garanties pour sa famille".
- Manifestation dimanche -
En signe de bonne volont� apr�s l'annonce du procureur, les opposants ont promis de d�bloquer "en partie" la rue Grouchevski o� se trouvent le gouvernement et le parlement, th��tre de heurts violents fin janvier, pour y permettre la circulation automobile.
"Cela ne veut pas dire qu'on lib�re les locaux (occup�s) ou qu'on enl�ve les barricades", a soulign� un repr�sentant des contestataires, Andri� Dzyndzia.
Pour la onzi�me fois depuis le d�but fin novembre de la contestation, n�e de la volte-face du pouvoir qui a renonc� � un rapprochement avec l'Union europ�enne pour se tourner vers la Russie, les manifestants se r�uniront dimanche � midi (10H00 GMT) sur le Ma�dan.
Le pr�c�dent rassemblement, dimanche 9 f�vrier, avait r�uni pr�s de 70.000 personnes.
Le mouvement de contestation s'est transform� au fil des semaines en un rejet pur et simple du r�gime du pr�sident Viktor Ianoukovitch, et ni la d�mission du gouvernement ni les n�gociations engag�es apr�s les affrontements qui ont fait quatre morts et plus de 500 bless�s fin janvier, n'ont r�gl� le conflit.
Apr�s la d�mission fin janvier du Premier ministre Mykola Azarov, son successeur n'a toujours pas �t� d�sign�, les alli�s du pr�sident, majoritaires au Parlement, faisant savoir qu'ils ne soutiendraient pas un candidat d'opposition.
Les d�bats sur une �ventuelle r�forme constitutionnelle r�clam�e par l'opposition semblent aussi au point mort.
- D�termination intacte -
Sur la place, les manifestants ne d�sarment pas.
"Tout est calme pour le moment, mais si la police vient, nous nous d�fendrons et la repousserons !", dit Anna Lazarenko, 20 ans, membre d'une unit� d'autod�fense sur le Ma�dan.
La jeune fille est casqu�e, encagoul�e et �quip�e d'un gilet de protection.
Dans la mairie de Kiev occup�e, m�me d�termination chez Rouslan Andre�ko, 27 ans, le "commandant" sur place.
"La seule condition pour que nous quittions les lieux, c'est la d�mission du pr�sident (Viktor) Ianoukovitch, suivie d'une �lection pr�sidentielle anticip�e", dit-il.
La crise �tait vendredi au centre de discussions � Moscou entre les ministres russe et allemand des Affaires �trang�res.
Le Russe, Sergue� Lavrov, a accus� l'Union europ�enne de chercher � �tendre sa zone d'"influence" � l'Ukraine en soutenant l'opposition.
Il avait jug� la veille que les relations entre la Russie et l'UE, qui se son d�t�rior�es ces derni�res ann�es, �taient arriv�es � un "moment de v�rit�" avec le diff�rend sur l'Ukraine.
Le ministre allemand, Frank-Walter Steinmeier, a r�torqu� que la crise en Ukraine n'�tait "pas un jeu d'�checs g�opolitique" et a soulign� que "personne n'avait int�r�t � un envenimement de la situation" en Ukraine.
� 2014 AFP. Tous droits de reproduction et de repr�sentation r�serv�s. Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par l'AFP. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, rediffus�e, traduite, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de l'AFP.
NewsLetter
Je d�sire recevoir des informations et offres de La Libre.be.
Valider
