TITRE: Turquie : heurts entre la police et des manifestants kurdes - � la une - Actualit�s sur orange.fr
DATE: 2014-02-15
URL: http://actu.orange.fr/une/turquie-heurts-entre-la-police-et-des-manifestants-kurdes-afp-s_2832998.html
PRINCIPAL: 177047
TEXT:
15/02/2014 � 17:50
Turquie : heurts entre la police et des manifestants kurdes
Des heurts ont oppos� la police turque et des Kurdes qui manifestaient pour r�clamer la lib�ration d'Adbullah �calan, le chef du parti PKK, � Diyarbakir (sud-est de la Turquie).
r�agir 0 �
photo : MEHMET ENGIN, AFP
Alors que le rassemblement devenait violent, la police anti�meutes turque a utilis� samedi des gaz lacrymog�nes et un canon � eau contre des Kurdes qui appelaient � la lib�ration du chef du parti PKK Adbullah �calan � Diyarbakir, ville � majorit� kurde du sud-est de la Turquie.
Quelque 100 manifestants kurdes ont lanc� des pierres et des cocktails Molotov en direction de la police, � l'issue d'un rassemblement marquant l'anniversaire de l'arrestation d'Abdullah �calan par les autorit�s en 1999. La police a ripost� avec des gaz lacrymog�nes et un canon � eau.
Des manifestations semblables ont eu lieu dans d'autres villes du sud-est, la police intervenant pour disperser les groupes. Une grande manifestation rassemblant 30.000 personnes selon les organisateurs, 9.000 selon la police, a aussi eu lieu en France, � Strasbourg.
Abdullah �calan purge sa peine de r�clusion � perp�tuit� sur l'�le-prison d'Imrali (nord-ouest).
voir (0)��
