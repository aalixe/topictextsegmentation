TITRE: Andre Drummond embrase le Rising Star Challenge - NBA 2013-2014 - Basketball - Eurosport
DATE: 2014-02-15
URL: http://www.eurosport.fr/basketball/nba/2013-2014/andre-drummond-embrase-le-rising-star-challenge_sto4136889/story.shtml
PRINCIPAL: 0
TEXT:
Andre Drummond embrase le Rising Star Challenge
le 15/02/2014 � 09:33, mis � jour le 15/02/2014 � 11:59
30 points et 25 rebonds : c�est un Andre Drummond monumental qui a men� la Team Hill � la victoire (142-136) lors du Rising Star Challenge. Le jeune Piston est logiquement �lu MVP d�un match serr� de bout en bout.
�
AFP
�
Comme chaque ann�e, le Rising Star Challenge a donn� lieu � un match ultra-spectaculaire au cours duquel les jeunes pousses de la ligue ont assur� le show. Des dunks en pagaille, une pluie de paniers � 3-points, la rencontre opposant rookies et sophomores a donn� le ton en vue du match des �toiles qui aura lieu demain soir. Dans une partie ultra-serr�e,�Andre Drummond�auteur de�30 points et 25 rebonds (nouveau record pour un Rising Star Challenge) a fait un chantier un �norme pour s�offrir un troph�e de MVP qui, pour la petite histoire, lui a �t� remis cass�.
�
Dans une fin de match compl�tement folle durant laquelleTim Hardaway Jr (36 pts) et Dion Waiters (31 pts, 7 passes) se sont r�pondus coup pour coup � longue distance, c�est finalement la Team Hill qui s�est d�tach�e dans les derni�res minutes pour l�emporter 142-136 dans le sillage du jeune int�rieur de Detroit.
�
Retrouvez plus d�articles sur BasketSession
�
