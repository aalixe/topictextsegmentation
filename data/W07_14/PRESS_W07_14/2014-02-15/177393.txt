TITRE: Reims frappe un grand coup en battant Bordeaux  - France Info
DATE: 2014-02-15
URL: http://www.franceinfo.fr/football/reims-frappe-un-grand-coup-en-battant-bordeaux-1320287-2014-02-15
PRINCIPAL: 177389
TEXT:
Reims frappe un grand coup en battant Bordeaux
le Samedi 15 F�vrier 2014 � 22:17
Imprimer
reims depreville � Panoramic
Transfigur� en seconde mi-temps face � des Bordelais qui avaient domin� le match pendant une heure, Reims a d�croch� une victoire tr�s importante qui lui permet d'assurer son maintien et de rester en course pour l'Europe.
Le debrief Apr�s deux d�faites de suite sur sa pelouse, contre Lyon et Montpellier, Reims a d�croch� samedi sa premi�re victoire � domicile en 2014. Au vu de la physionomie du match en premi�re mi-temps, on ne voyait pourtant pas trop comment les R�mois pourraient venir � bout de Bordelais constamment dominateurs. Pendant 45 minutes, les Girondins ont profit� de l�apathie d'en face pour r�cup�rer la plupart des ballons et encha�ner les vagues offensives sur le but de Johnny Placide. Mais sans marquer... Un d�tail essentiel puisqu�il permettait � Reims de rester parfaitement dans le coup. A condition de se r�veiller ! Boug�s par leur entra�neur Hubert Fournier, les R�mois sont revenus des vestiaires survolt�s. Beaucoup plus conqu�rants et pr�sentant un bloc d�fensif compact, les prot�g�s de Jean-Pierre Caillot n�ont pas tard� � trouver la r�compense � leur sursaut d�orgueil sur une belle frappe de De Pr�ville. Reims passait m�me tout pr�s de tuer le match sur l�engagement. Toujours est-il qu�il fallait de longues minutes avant de voir les Bordelais retrouver leurs esprits. Peut-�tre �tonn�s eux aussi de la r�volte r�moise ? En fin de match, Bordeaux a retrouv� sur certains coups de sa superbe. Mais Placide, en �tat de gr�ce dans le dernier quart d�heure, a sorti le match qu�il fallait dans sa cage. Et conserv� jusqu�au bout ce pr�cieux but d�avance obtenu peu apr�s l�heure de jeu. Assur� d�sormais de son maintien, Reims poss�de autant de points que l�OM avant le choc de dimanche soir � Saint-Etienne. Les R�mois reprennent m�me provisoirement deux points d�avance sur Lyon. On appelle cela une magnifique op�ration. Le classement de Ligue 1 | Le classement des buteurs | Les r�sultats de la journ�e Le film du match 10eme minute
Hoarau profite d�un mauvais renvoi de la d�fense r�moise pour enrouler une frappe tr�s vicieuse qui passe tout pr�s de la lucarne gauche de Placide. 13eme minute
C�est maintenant au tour de Faubert de prendre sa chance de loin, mais l� non plus, la tentative bordelaise ne trouve pas le cadre. 20eme minute
A la lutte avec Weber � l�entr�e de la surface, Hoarau reprend de la t�te un ballon en cloche qui lui �tait adress�. Cette fois, le ballon meurt nettement � gauche du but r�mois. 28eme minute
Placide revient � temps dans son but pour emp�cher in extremis le ballon de rentrer sur un corner frapp� directement par Sertic c�t� gauche. 31eme minute
Bien d�cal� par Oniangu�, Mandi prend sa chance de pr�s de trente m�tres � droite d�une frappe flottante � mi-hauteur qui oblige Carrasso � une belle parade. 36eme minute
Nguemo reprend le ballon d�une frappe crois�e dans l�axe sur une remise de Faubert c�t� droit. Placide se couche. 41eme minute
Un centre fort de Maurice-Belay sur l�aile gauche transperce toute la d�fense r�moise, Faubert reprend le ballon mais ne cadre pas. 61eme minute
Olimpa se couche sur une frappe lointaine d�Albaek � ras de terre. 64eme minute (1-0)
Albaek s�arrache aux abords du rond central et trouve dans la profondeur De Pr�ville, qui fixe Henrique en s�avan�ant et d�clenche une superbe frappe crois�e du droit � mi-hauteur qui se loge dans le petit filet d�Olimpa avec l�aide du poteau. 65eme minute
Reims r�cup�re tout de suite le ballon sur l�engagement. Mandi, lanc� sur l�aile droite, centre dans la surface pour Atar, qui voit sa frappe bloqu�e par Olimpa. 67eme minute
Pas attaqu� sur un bon service de Maurice-Belay, Saivet enroule sa frappe sur le poteau gauche de Placide, qui n�avait pas boug�. 70eme minute
Sur un coup-franc venu de la droite, Oniangu� coupe la trajectoire de la t�te au premier poteau. Olimpa est vigilant. 76eme minute
Placide se couche bien sur une reprise de vol�e lointaine os�e mais bien r�alis�e par Nguemo. 78eme minute
Placide s�envole et d�tourne magnifiquement un coup-franc de Sertic qui prenait la direction de la lucarne. 79eme minute
Placide r�ussit une nouvelle parade extraordinaire sur une t�te plongeante � bout portant d�Hoarau, servi par Saivet. 82eme minute
Nouvel arr�t d�cisif de Placide, qui ne se laisse pas surprendre sur une frappe lointaine de Sertic qui avait rebondi juste devant lui. Les joueurs � la loupe Reims
Placide (8) : Sauv� par son poteau gauche sur une frappe de Saivet, il a termin� le match en feu et a sauv� son �quipe sur deux parades incroyables.
Mandi (6) : Avec Signorino, il a �t� l�un des seuls R�mois � ne jamais l�cher. Beaucoup d�activit� dans son couloir et cette belle frappe qui aurait pu surprendre Carrasso.
Weber (6) : Beaucoup de travail face � Hoarau, tr�s bon dans le domaine a�rien et constamment recherch� par les Bordelais. Un match compliqu� mais dont il est sorti vainqueur.
Tacalfred (7) : D�cisif en d�but de match avec cette belle anticipation devant Hoarau, il a lui aussi �t� mis � rude �preuve dans la surface. Mais cela ne lui fait pas peur. Quel match !
Signorino (6) : S�il y a bien un joueur auquel Hubert Fournier n�a pas pu reprocher son manque d�agressivit� en premi�re mi-temps, c�est lui. Omnipr�sent dans les duels, il a en plus souvent aid� sa d�fense centrale.
Krychowiak (6) : Il s�est souvent retrouv� en inf�riorit� num�rique en premi�re mi-temps. La m�tamorphose de son �quipe lui a permis de faire de nouveau la loi.
Oniangu� (5) : Sur quelques situations, son engagement et sa technique ont fait beaucoup de bien. Mais il a connu aussi des difficult�s par moments en premi�re mi-temps.
Albaek (6) : De retour dans le onze de d�part apr�s deux matchs sur le banc, il s�est signal� sur une belle action en fin de premi�re mi-temps et a ensuite lanc� intelligemment De Pr�ville sur le but apr�s un bon travail � la r�cup�ration. Remplac� par Devaux (77eme).
Fortes (5) : Bien pris par la d�fense bordelaise, il a eu longtemps beaucoup de mal � se cr�er des situations avant de prendre progressivement le dessus en seconde mi-temps. A l�origine du but r�mois.
Atar (4) : Il n�est pas connu pour ses gros efforts d�fensifs et l�a encore prouv� samedi. En oubliant (refusant ?) de revenir d�fendre, il a plusieurs fois mis son �quipe en danger. Il a en plus manqu� une tr�s belle occasion de mettre Bordeaux KO. Remplac� par Ayit� (71eme), qui a amen� beaucoup de rythme.
De Pr�ville (6) : Pr�f�r� � Charbonnier, il a souvent but� sur la d�fense en premi�re p�riode. Mais il n�a jamais abandonn� et a �t� r�compens� sur cette superbe frappe dans la course. Son troisi�me but cette saison.
Bordeaux
Carrasso (non not�) : Quelques minutes apr�s avoir sauv� son �quipe sur une frappe vicieuse de Mandi, il a �t� contraint de sortir sur blessure � une cuisse. Remplac� par Olimpa (40eme - 5). Un style moins rassurant que son pr�d�cesseur mais beaucoup d�efficacit� sur ses interventions.
Mariano (5) : Tr�s en vue sur son c�t� droit, il a tent� r�guli�rement d�amener des solutions mais s�est heurt� � Signorino.
Henrique (5) : Pas vraiment g�n� par les attaquants r�mois, il a fait preuve de solidit� jusqu�� cette action sur le but r�mois o� il semble ne pas savoir quoi faire.
San� (6) : Rapidement averti, il n�a rien perdu de sa vigilance, ni de son sens du placement pour autant. De bonnes interventions.
Pellenard (6) : Tr�s int�ressant dans l�utilisation du ballon apr�s des premi�res minutes h�sitantes, il a aussi montr� de tr�s belles choses dans l�engagement et le travail d�fensif.
Nguemo (6) : Solidement positionn� devant sa d�fense centrale, il a bien orient� le jeu, r�cup�r� de nombreux ballons et aurait pu marquer avec davantage de r�ussite. Remplac� par Traor� (84eme).
Sertic (7) : Il a obtenu de bons coup-francs sur lesquels il a lui-m�me amen� le danger. Sans deux superbes arr�ts de Placide, il aurait m�me �galis� en fin de match. Un match s�rieux au milieu comme souvent.
Faubert (5) : Son activit� permanente en premi�re mi-temps lui a permis d�adresser de bons centres. Tout pr�s de marquer �galement. Remplac� par C.Diabat� (68eme), qui a besoin de reprendre ses marques devant.
Maurice-Belay (6) : Tr�s en jambes sur son aile gauche, il a apport� beaucoup mais n�a pas toujours trouv� preneur sur ses centres.
Saivet (6) : Malheureux avec cette frappe sur le poteau, il a beaucoup tent� pour essayer de trouver la solution dans une d�fense r�moise aux abois en premi�re mi-temps.
Hoarau (5): Pour sa premi�re titularisation avec Bordeaux en L1, il a beaucoup g�n� les d�fenseurs r�mois dans le jeu a�rien. R�guli�rement utilis� comme point de fixation devant, il a manqu� d�ourvrir le score pour quelques centim�tres. En fin de match, il a but� sur un Placide en grande forme. Monsieur l�arbitre au rapport Match s�rieux de M.Jaffredo qui, comme souvent, n�a pas fait parler de lui. Et on ne demande que cela � un arbitre. �a s�est pass� en coulisses� - Interrog� dans la semaine sur le site Scapulaire.com, Gr�gory Sertic, le milieu de terrain bordelais, y faisait l��loge de son ancien co�quipier et aujourd�hui joueur du Stade de Reims, Grzegorz Krychowiak, qu�il verrait bien en Bundesliga ou en Premier League. � Grzegorz, je l�ai connu en jeunes, o� nous avions une tr�s belle relation en milieu de terrain. C�est un mec qui ne l�che rien ! Tu peux vraiment aller � la guerre avec lui, c�est quelqu�un sur qui tu peux compter. (�) Je pense qu�en fin d�ann�e, il partira soit dans le championnat anglais, soit dans l�allemand. L�anglais, parce qu�il casse beaucoup, l�allemand, parce qu�il frappe de loin. � - A�ssa Mandi, l�arri�re droit du Stade de Reims, semblait attendre avec impatience ses retrouvailles avec Nicolas Maurice-Belay. Sur le site officiel des Girondins, l�international alg�rien avoue que le Bordelais fait partie de ses joueurs pr�f�r�s en Ligue 1. En tout cas � son poste. � Pour moi, il est une des r�f�rences de la Ligue 1. C�est l�un des meilleurs milieux gauche du championnat. C�est une sorte de d�fi personnel. J�aurai en tout cas un tr�s bon duel � livrer contre un joueur de qualit�. � - Francis Gillot f�tait samedi soir � Reims sa 100eme apparition en Ligue 1 sur le banc des Girondins de Bordeaux. Avec un bilan positif pour l�ancien entra�neur de Sochaux et de Lens, puisqu�il pr�sentait avant ce d�placement dans la Marne un total de 38 victoires, 36 matchs nuls et seulement 25 d�faites. La feuille de match  L1 (25eme journ�e) / REIMS � BORDEAUX : 1-0 Stade Auguste-Delaune (13 702 spectateurs)
Temps froid - Pelouse correcte
But : De Pr�ville (64eme) pour Reims
Avertissement : L.San� (11eme) pour Bordeaux
Expulsion : Aucune
Reims
Placide (8) � Mandi (6), Weber (6), Tacalfred (cap) (7), Signorino (6) � Krychowiak (6), Oniangue (5), Albaek (6) puis Devaux (77eme) - Fortes (5), De Pr�ville (6) puis Charbonnier (84eme), Atar (4) puis F.Ayit� (71eme)
N'ont pas particip� : I.Gassama (g), Turan, Ghisolfi, Courtet
Entra�neur : H.Fournier Bordeaux
C.Carrasso (cap) (non not�) puis Olimpa (40eme, 5) � Mariano (5), Henrique (5), L.San� (6), Pellenard (6) � Nguemo (6) puis Ab.Traor� (84eme), Sertic (7), Maurice-Belay (6), Faubert (5) puis C.Diabat� (68eme) - Saivet (6), Hoarau (5)
N'ont pas particip� : Olimpa (g), Chalm�, Planus, Poundj�, Poko
Entra�neur : F.Gillot L1, L2, National, CFA, CFA2, Allemagne, Angleterre, Espagne, Italie� Tous les r�sultats et les classements en un coup d�oeil
