TITRE: Syrie : Obama veut accro�tre la pression sur Assad
DATE: 2014-02-15
URL: http://www.lemonde.fr/ameriques/article/2014/02/15/obama-propose-une-aide-a-la-jordanie-voisine-de-la-syrie_4367141_3222.html
PRINCIPAL: 0
TEXT:
Syrie : Obama veut accro�tre la pression sur Assad
Le Monde |
� Mis � jour le
15.02.2014 � 12h41
Les Etats-Unis envisagent des mesures pour faire davantage pression sur le r�gime syrien, a d�clar�vendredi 14 f�vrier le pr�sident Barack Obama en recevant le roi de Jordanie , Abdallah II. ��Nous ne nous attendons pas � r�gler cela � aucun moment sur le court terme, alors il va y avoir des mesures imm�diates que nous allons devoir prendre pour aider la situation humanitaire l�-bas��, a d�clar� M. Obama lors d'une rencontre avec le roi au sujet de la guerre civile qui dure depuis pr�s de trois ans en Syrie . ��Il y aura des mesures interm�diaires que nous pourrons prendre pour mettre davantage de pression sur le r�gime d'Assad��, a ajout�, sans fournir de pr�cisions, le pr�sident am�ricain.
Selon des responsables am�ricains, la Maison Blanche envisage notamment de fournir des armes aux rebelles syriens pour inciter le r�gime de Damas � se montrer davantage enclin � n�gocier .
Ces propositions interviennent au moment o� les� discussions entre opposition et gouvernement syriens men�es par le m�diateur de l'ONU, Lakhdar Brahimi, devraient reprendre , samedi � Gen�ve, en d�pit de l'impasse dans laquelle elles se trouvent, un appel aux parrains russes et am�ricains de la conf�rence n'ayant produit aucun effet.
Barack Obama a propos� un milliard de dollars en garanties de pr�ts � la Jordanie, ainsi que le renouvellement d'un accord de coop�ration repr�sentant 660 millions de dollars par an. Ces fonds sont en partie destin�s � aider ce pays � faire face � l'afflux continu de r�fugi�s qui fuient la guerre civile en Syrie, ainsi qu'� compenser la perte de ressources en gaz en provenance d' Egypte . La Jordanie h�berge plus de 600�000 r�fugi�s syriens.
