TITRE: Pour vous souvenir de vos r�ves, r�veillez-vous souvent !
DATE: 2014-02-15
URL: http://www.ici-c-nancy.fr/index.php/actus/actualites/sante/item/6831-pour-vous-souvenir-de-vos-r%25C3%25AAves-r%25C3%25A9veillez-vous-souvent
PRINCIPAL: 0
TEXT:
Pour vous souvenir de vos r�ves, r�veillez-vous souvent !
samedi, 15 f�vrier 2014 06:13
�crit par� ICI-C-NANCY.fr
SANT�. Une �quipe de chercheurs fran�ais de l'Inserm s'est pench�e sur nos songes en analysant�l'activit� c�r�brale des r�veurs. R�sultat ? Les �grands r�veurs� se distingueraient par une plus grande aptitude � percevoir les stimulations ext�rieures.
Le sommeil objet d'�tude d'une �quipe de scientifiques fran�ais�
Certaines personnes se souviennent de leurs r�ves tous les matins alors que d'autres s�en souviennent rarement. Fort de ce constat, l'�quipe de Perrine Ruby, charg�e de recherche Inserm, au sein du centre de recherche en neurosciences de Lyon (Inserm / CNRS / Universit� Claude Bernard Lyon 1) a �tudi� l'activit� c�r�brale de ces r�veurs afin de comprendre ce qui les diff�rencient. Les premiers �l�ments de r�ponse ont �t� publi�s dans la revue Neuropharmacology, dans laquelle les chercheurs soulignaient que "la jonction temporo-pari�tale", un carrefour du traitement de l'information dans le cerveau, �tait plus active chez les grands r�veurs.
Les grands r�veurs plus sensibles aux stimulations ext�rieures
Elle induirait donc toujours selon l'�quipe de chercheurs "une plus grande r�activit� aux stimulations ext�rieures" facilitant ainsi le r�veil au cours du sommeil et donc la m�morisation des r�ves.
En janvier 2013, dans des travaux publi�s dans la revue C�r�bral Cortex, l��quipe de scientifiques d�couvrait que les �grands r�veurs� comptabilisaient deux fois plus de phases de r�veil pendant le sommeil que les �petits r�veurs� et leur cerveau est plus r�actif aux stimuli de l�environnement. En r�sum�, cette sensibilit� expliquerait une augmentation des �veils au cours de la nuit et permettrait ainsi une meilleure m�morisation des r�ves lors de cette br�ve phase d��veil.
Des observations pour lesquelles l��quipe de recherche a mis en place une �tude clinique en analysant�le sommeil de 41 r�veurs volontaires en m�langeant��grands r�veurs� et �petits r�veurs� rapportant en moyenne 2 r�ves par mois.
Cela explique pourquoi les grands r�veurs r�agissent plus aux stimuli de l�environnement et se r�veillent plus au cours de leur sommeil que les petits r�veurs, et ainsi pourquoi ils m�morisent mieux leurs r�ves. En clair, le cerveau endormi n�est pas capable de m�moriser une nouvelle information en m�moire, il a besoin de se r�veiller pour pouvoir faire �a� explique, Perrine Ruby, charg�e de recherche � l�Inserm.
