TITRE: Bundesliga : Leverkusen s'incline devant Schalke (1-2) avant d'affronter le PSG - Bundesliga 2013-2014 - Football - Eurosport
DATE: 2014-02-15
URL: http://www.eurosport.fr/football/bundesliga/2013-2014/bundesliga-leverkusen-s-incline-devant-schalke-1-2-avant-d-affronter-le-psg_sto4137941/story.shtml
PRINCIPAL: 177295
TEXT:
Rib�ry: "Gagner tous les matches pour Uli"
15/03
Le Bayern Munich bat Leverkusen 2-1 et prend 23 points d'avance sur Dortmund
15/03
Karl Hopfner succ�dera � Hoeness � la t�te du Bayern
14/03
Huntelaar propulse Schalke sur le podium
14/03
Guardiola: "Uli Hoeness restera mon ami"
14/03
Vibrant hommage du Bayern � son pr�sident condamn� et d�missionnaire
14/03
Allemagne - Hanovre: fin de saison pour Diouf op�r� d'une �paule
14/03
Bundesliga
Uli Hoeness ne fait pas appel de sa condamnation et d�missionne de son poste au Bayern
13/03
Uli Hoeness (Bayern Munich) condamn� � trois ans et demi de prison
13/03
Fraude fiscale : Cinq ans et demi de prison requis contre Uli Hoeness
12/03
Schalke toujours priv� de Farfan � Augsbourg
10/03
Le patron du Bayern Munich reconna�t une fraude fiscale de plus de 18 millions
10/03
Kroos : "La Premier League est une opportunit�"
09/03
Vainqueur de Fribourg (0-1), Dortmund rel�gue Leverkusen � quatre points
09/03
Stuttgart se s�pare de son coach
08/03
Le Bayern Munich affole encore les compteurs
08/03
