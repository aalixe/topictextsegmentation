TITRE: Nicolas Bedos condamn� pour avoir insult� des policiers - La Libre.be
DATE: 2014-02-15
URL: http://www.lalibre.be/light/people/nicolas-bedos-condamne-pour-avoir-insulte-des-policiers-52fe79553570c16bb1ccf07d
PRINCIPAL: 175297
TEXT:
Vous �tes ici: Accueil > Light > People
Nicolas Bedos condamn� pour avoir insult� des policiers
R�daction en ligne Publi� le
vendredi 14 f�vrier 2014 � 21h20
- Mis � jour le
lundi 24 f�vrier 2014 � 14h29
...
Fran�ois Hollande couronn� roi du selfie
People D�j� condamn� en janvier 2010 pour conduite en �tat d'ivresse, le fiston de Guy Bedos n'a donc pas �t� �pargn� par le tribunal correctionnel de Paris qui a retenu l'�tat de r�cidive l�gal.
Cette fois-ci, l'acteur fran�ais � la langue bien pendue s'est fait attrap� pour pour conduite en �tat d'ivresse, outrage et menace de mort envers des policiers apr�s une chute � scooter .
Ma�tre J�r�me, l'avocat des deux policiers, n'a pas h�sit� � critiquer l'absence de Nicolas Bedos lors de l'audience. "Donner des le�ons de courage aux policiers � la t�l�, il ne s'en prive pas. Par contre, lorsqu'il s'agit de venir s'expliquer devant eux, il n'y a plus personne", d�clarait l'homme de loi en r�f�rence � une ancienne chronique de Bedos, comme le rappelle le Huffington Post.
Pour rappel, le 24 septembre 2013, deux policiers avaient appr�hend� le natif de Neuilly apr�s une chute en deux roues. Rapidement, la situation s'�tait envenim�e et les insultes �taient de sortie. Bedos junior aurait �voqu� le "boulot de merde" des agents avant de carr�ment dire � l'un d'eux : "Je te retrouverai et te tuerai".
Le parquet a regrett� que le chroniqueur ne vienne pr�senter ses excuses mais aussi le "comportement inadmissible" et les "propos d�testables" �mis par le trentenaire.
Sur le m�me sujet :
