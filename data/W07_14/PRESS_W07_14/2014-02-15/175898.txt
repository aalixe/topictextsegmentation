TITRE: Ukraine: les opposants libérés, la rue maintient la pression - 15 février 2014 - Le Nouvel Observateur
DATE: 2014-02-15
URL: http://tempsreel.nouvelobs.com/monde/20140214.AFP0115/ukraine-l-opposition-maintient-la-pression-russes-et-europeens-polemiquent.html
PRINCIPAL: 0
TEXT:
Actualité > Monde > Ukraine: les opposants libérés, la rue maintient la pression
Ukraine: les opposants libérés, la rue maintient la pression
Publié le 14-02-2014 à 14h15
Mis à jour le 15-02-2014 à 14h55
A+ A-
L'opposition ukrainienne a appelé vendredi à se mobiliser pour une nouvelle "offensive pacifique" ce week-end, alors que les négociations avec le pouvoir sont dans l'impasse et que Russes et Européens s'accusent mutuellement de visées géopolitiques. (c) Afp
Kiev (AFP) - Les autorités ukrainiennes ont annoncé vendredi avoir libéré la totalité des 234 manifestants interpellés en deux mois, mais sans pour autant abandonner les accusations pesant sur eux, tandis que l'opposition appelle à une nouvelle mobilisation dimanche.
Le procureur général ukrainien, Viktor Pchonka, a fait cette annonce dans un communiqué.
"234 personnes ont été arrêtées entre le 26 décembre et le 2 février. Aujourd'hui, plus aucune d'entre elles n'est en détention", a déclaré le procureur, ajoutant que les poursuites, qui pourraient valoir aux opposants de lourdes peines de prison, seraient abandonnées dans le mois à venir si les conditions fixées par la loi d'amnistie étaient remplies.
Dans le centre de Kiev, sur la place de l'Indépendance - le Maïdan -, occupée depuis novembre et entourée de barricades, le "conseil" improvisé du mouvement de contestation a souligné que, pour l'heure, les personnes libérées restaient assignées à résidence et menacées de prison.
Une loi d'amnistie avait été votée en janvier, avec pour condition l'évacuation dans les deux semaines des lieux publics et bâtiments officiels occupés par les contestataires, dont la mairie de la capitale. Ce délai expire lundi.
L'opposition exigeait de son côté la libération sans conditions de toutes les personnes incarcérées, et la levée des poursuites.
- Manifestation dimanche -
Les opposants ont promis en signe de bonne volonté de débloquer "en partie" la rue Grouchevski où se trouvent le gouvernement et le parlement, théâtre de heurts violents fin janvier, pour y permettre la circulation automobile.
"Cela ne veut pas dire qu'on libère les locaux (occupés) ou qu'on enlève les barricades", a souligné un représentant des contestataires, Andriï Dzyndzia.
Pour la onzième fois depuis le début fin novembre de la contestation, née de la volte-face du pouvoir qui a renoncé à un rapprochement avec l'Union européenne pour se tourner vers la Russie, les manifestants se réuniront dimanche à midi (10H00 GMT) sur le Maïdan.
Le parti de l'opposante emprisonnée Ioulia Timochenko a appelé à se mobiliser.
Le précédent rassemblement, dimanche 9 février, avait réuni près de 70.000 personnes.
Le mouvement de contestation s'est transformé au fil des semaines en un rejet pur et simple du régime du président Viktor Ianoukovitch, et ni la démission du gouvernement ni les négociations engagées après les affrontements qui ont fait quatre morts et plus de 500 blessés fin janvier, n'ont réglé le conflit.
Après la démission fin janvier du Premier ministre Mykola Azarov, son successeur n'a toujours pas été désigné, les alliés du président, majoritaires au Parlement, faisant savoir qu'ils ne soutiendraient pas un candidat d'opposition.
L'un des leaders de l'opposition, Arseni Iatseniouk, qui s'était vu proposer le poste de Premier ministre, a souligné qu'il ne pourrait l'accepter que si l'opposition obtenait des postes clé au gouvernement, et après une réforme constitutionnelle réduisant les pouvoirs présidentiels au profit du gouvernement et du Parlement. Les débats sur cette réforme semblent au point mort .
- Détermination intacte -
Sur la place, les manifestants ne désarment pas.
"Tout est calme pour le moment, mais si la police vient, nous nous défendrons et la repousserons !", dit Anna Lazarenko, 20 ans, membre d'une unité d'autodéfense sur le Maïdan.
La jeune fille est casquée, encagoulée et équipée d'un gilet de protection.
Dans la mairie de Kiev occupée, même détermination chez Rouslan Andreïko, 27 ans, le "commandant" sur place.
"La seule condition pour que nous quittions les lieux, c'est la démission du président (Viktor) Ianoukovitch, suivie d'une élection présidentielle anticipée", dit-il.
La crise était vendredi au centre de discussions à Moscou entre les ministres russe et allemand des Affaires étrangères.
Le Russe, Sergueï Lavrov, a accusé l'Union européenne de chercher à étendre sa zone d'"influence" à l'Ukraine en soutenant l'opposition.
Il avait jugé la veille que les relations entre la Russie et l' UE , qui se sont détériorées ces dernières années, étaient arrivées à un "moment de vérité" avec le différend sur l'Ukraine.
Le ministre allemand, Frank-Walter Steinmeier, a rétorqué que la crise en Ukraine n'était "pas un jeu d'échecs géopolitique" et a souligné que "personne n'avait intérêt à un envenimement de la situation" en Ukraine .
La chancelière allemande Angela Merkel recevra lundi à Berlin deux chefs de file de l'opposition, Vitali Klitschko et Arseni Iatseniouk, a annoncé un porte-parole du gouvernement allemand.
Partager
