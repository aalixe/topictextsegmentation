TITRE: Rugby/Top 14 - Castres: Rory Kockott annonce vouloir rester - Tout le sport - www.lunion.presse.fr
DATE: 2014-02-15
URL: http://www.lunion.presse.fr/tout-le-sport/rugbytop-14-castres-rory-kockott-annonce-vouloir-rester-ia0b0n300747
PRINCIPAL: 176758
TEXT:
Le journal du jour � partir de 0,79 �
Le demi de m�l�e Rory Kockott a exprim� sa volont� de rester � Castres dans un documentaire diffus� sur Canal +, en d�pit d'un pr�-contrat sign� avec Toulon. "Finalement, j'ai d�cid� de rester � Castres", affirme le Sud-Africain de 27 ans au micro de l'�mission Int�rieur Sport. "C'est toujours agr�able d'avoir plusieurs propositions et je ressens beaucoup de gratitude pour cela, ajoute-t-il. J'ai pens� que l'une �tait meilleure que l'autre."
AFP
