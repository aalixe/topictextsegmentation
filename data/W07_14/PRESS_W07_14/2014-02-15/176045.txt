TITRE: Football Ligue 1 - Le PSG en totale maitrise face � VA - Paris - Valenciennes - Foot 01
DATE: 2014-02-15
URL: http://www.foot01.com/ligue1/le-psg-en-totale-maitrise-face-a-va,136307
PRINCIPAL: 176044
TEXT:
Le PSG en totale maitrise face � VA
Photo Icon Sport
Publi� Vendredi 14 F�vrier 2014 � 22h25 Dans�: Ligue 1 , PSG , Valenciennes .
Le PSG a totalement �touff� Valenciennes de la premi�re � la derni�re minute pour s�imposer sans trembler 3-0. Plus que jamais en t�te de la Ligue 1, les Parisiens ont fait tourner et id�alement pr�par� leur match de Ligue des Champions.�
Apr�s le nul � Monaco, le PSG n�a pas perdu de temps pour r�enclencher la marche avant. Comme il en a d�sormais pris l�habitude, le club de la capitale acc�l�rait d�entr�e de jeu pour litt�ralement asphyxier son adversaire. Au bout d�un quart d�heure, il fallait d�j� plusieurs miracles de Penneteau, un sauvetage de Kagelmacher et une certaine maladresse d�Ibrahimovic et de Lavezzi pour que le score soit toujours de 0-0. Cela ne durait pas et, sur un coup-franc en force d�Ibrahimovic contr� par le mur, le ballon revenait � Lavezzi qui marquait de pr�s d�un tir plein centre (1-0, 19e). Ce but ne changeait rien au sc�nario du match, avec une nette domination parisienne, encore d�autres occasions, mais toujours un seul but au compteur. En face, VA ne sortait quasiment pas, mais sur sa seule occasion, Ducourtioux passait tout pr�s de l��galisation d�un tir crois� qui filait � ras du poteau (31e).�
Certainement un peu recadr�s par Laurent Blanc sur leur manque d�efficacit�, les Parisiens retenaient la le�on et se mettaient � l�abri apr�s le repos. Sur un ballon naviguant dans la surface, Ibrahimovic concluait une reprise �cras�e de Cabaye par une vol�e piqu�e (2-0, 50e). Dans la foul�e, sur un d�bordement de Lavezzi tout en rapidit�, le centre en retrait de l�Argentin �tait repris par Kagelmacher, qui marquait contre son camp (3-0, 52e). Le maigre suspense avait disparu et Laurent Blanc entrait en mode gestion avec plusieurs changements. Les joueurs en faisaient de m�me, ne for�ant pas pour conserver cet avantage qui leur permet de prendre huit points d�avance sur Monaco, en attendant le d�placement des dauphins de la Ligue 1 � Bastia ce samedi. Pour Valenciennes, l�op�ration maintien passera par un autre chemin que le Parc des Princes.
Samedi 15 F�vrier 2014 � 04h32 R�pondre
Cocotte
Bon match en g�n�ral m�me si apr�s les sorties de Motta et Zlatan �a a �t� un peu plus compliqu�. M�nez qu'il d�gage, il n'apporte rien m�me quand il ne loupe pas son match ! Digne a fait un bon match, c'est bien pour lui et nous. Maintenant il faut penser LDC.
Vendredi 14 F�vrier 2014 � 23h25 R�pondre
Harkhange
j'aurais bein voulu voir lavezzi -- pastore-- lucas
Vendredi 14 F�vrier 2014 � 23h12 R�pondre
Grenierninho
On parle du mur a 13 m sur le 1er but parisien ou pas ?
Vendredi 14 F�vrier 2014 � 23h06 R�pondre
Tartopom
pur�e pas 3 jours d'anciennet� et �a commence d�j�.
Y a pas � dire les lyonnais ont le meilleur centre de formation..........de trolls ^^
Mais oui parlons en. qu'as tu � dire ?
Samedi 15 F�vrier 2014 � 08h27 R�pondre
M_lamakina
Vendredi 14 F�vrier 2014 � 23h13 R�pondre
Harkhange
non non, le mur �tait bien � 12.5m
Vendredi 14 F�vrier 2014 � 23h15 R�pondre
M_lamakina
ouais enfin tu  m apprends rien c est juste que je comprend pas trop ve que se com vient faire la
Vendredi 14 F�vrier 2014 � 23h25 R�pondre
Cocotte
Ouais parce que le joueur le plus pr�s chausse du 54^^
Vendredi 14 F�vrier 2014 � 23h19 R�pondre
Harkhange
12.5 ^^ mais bon ils ont que ce qu'ils m�ritent, � la force de tricher voil� ce qui arrive.
Vendredi 14 F�vrier 2014 � 23h10 R�pondre
Grenierninho
Oui sa emp�che pas la sup�riorit� de Paris ce soir si il ya 10-0 c'est pas illogique mdr :) !!
Vendredi 14 F�vrier 2014 � 23h11 R�pondre
ParisestMagik2
Vendredi 14 F�vrier 2014 � 22h59 R�pondre
Dodo_La_Saumure
Surtout qu'il met une frappe super d�tendu, limite comme � la fin d'un entrainement.
Par contre cette coupe... D�cid�ment ces loutres sur la t�te, je pourrai jamais m'y faire.
Samedi 15 F�vrier 2014 � 00h06 R�pondre
M_lamakina
c est le but de l ann�e
Vendredi 14 F�vrier 2014 � 23h33 R�pondre
dealoff
j'ai vu �a �norme et sa c�l�bration est encore meilleure en mode Cantona
Vendredi 14 F�vrier 2014 � 23h25 R�pondre
cedric9375
Enorme match de marquinhos, pastore et digne.
Vendredi 14 F�vrier 2014 � 22h45 R�pondre
ParisestMagik2
Digne il  m'a fait tellement plaisir sur la premiere mi temps !
Vendredi 14 F�vrier 2014 � 22h49 R�pondre
Abdoulaye Mopit
Et bravo Marquinhos pour son fran�ais.
Vendredi 14 F�vrier 2014 � 22h35 R�pondre
kress93
Match propre, on prend pas de but, une bonne chose pour le match qui
arrive mardi, on a eu droit a un grand Pastore, a un Lavezzi survolter,
qui met son but, et j'suis bien content pour lui, bref, bravo a toutes
l'�quipe, les d�fenseurs, les milieux, et les attaquants, tout le monde a
fait le taf, m�me si on aurait pu gagner 6-0, et �a aurait choquer
personne allez Paris, j'ai h�te pour le match du Mardi !
Vendredi 14 F�vrier 2014 � 22h35 R�pondre
pauletic
oblig� de rajouter que menez faut arreter mais vraiment!!
Vendredi 14 F�vrier 2014 � 22h53 R�pondre
JosipSk
Le jardinier aussi! La pelouse commence � �tre merdique! Faut dire � l'Emir d'aller d�baucher le nouveau meilleur jardinier de 1er League 2014!^^
Vendredi 14 F�vrier 2014 � 22h57 R�pondre
ParisestMagik2
Tu le vois en arri�re  plan sur Be In  faire des gestes avec ses bras parce que sa pelouse est d�fonc�   ^^
Vendredi 14 F�vrier 2014 � 22h58 R�pondre
Abdoulaye Mopit
Et avec une �quipe mixte ^^
Vendredi 14 F�vrier 2014 � 22h37 R�pondre
Abdoulaye Mopit
Faut f�licit� Valenciennes qui � pas fermer le jeu, malgr� la d�faite. Paris tr�s propre avec un bon Pastore dans un poste plus reculer, c'est the bonne nouvelle pour vous, son retour en forme. Et Menez bon match aussi faut le soulign�.
Vendredi 14 F�vrier 2014 � 22h32 R�pondre
pauletic
bon match?? le mec rate tout ce qu'il tente, tout ce qu'il veut!
Vendredi 14 F�vrier 2014 � 22h54 R�pondre
Abdoulaye Mopit
Il a eu des choix curieux, mais il a pas �t� d�gueu non plus... Je l'ai trouv� bien plus concern� que d'habitude, et beaucoup moins nonchalant. Apr�s il est perfectible dans son jeu, et son intelligence de d�placement par rapport � ses partenaires avec la passe qui peut faire � Ibra sur un contre en 2�me.
Vendredi 14 F�vrier 2014 � 23h00 R�pondre
pauletic
mais ya plus � etre perfectible il nous montre qu'il ne le fera pas, il est naz et il s'en fout!
Vendredi 14 F�vrier 2014 � 23h25 R�pondre
Abdoulaye Mopit
Il serais naze il serais pas dans ton club !!
Vendredi 14 F�vrier 2014 � 23h26 R�pondre
pauletic
rien � voir �a... on a cru qub'il�tait bon parce qu'pon croit �ternellment en lui mais il a rien ce type puisqu'il fra
Samedi 15 F�vrier 2014 � 10h51 R�pondre
Cocotte
Gomis fait des biens meilleurs matchs et vous lui crachez tous dessus, incompr�hensible lyonnais !
Vendredi 14 F�vrier 2014 � 23h21 R�pondre
Abdoulaye Mopit
Parce que des fois il est aga�ant il loupe des actions toute fa�tes comme hier ! apr�s pour ma part j'aime bien Gomis y'a pas de souci, mais c'est pas l'attaquant ou le style de joueur que je pr�f�re !
Vendredi 14 F�vrier 2014 � 23h26 R�pondre
Regalus
Menez, il est rarement � la bonne place. Souvent il a fait doublon avec VdW qui se retrouve oblig� de monter. Et comme Menez ne sait pas d�fendre, ca nous d�couvre sur l'aile...
Samedi 15 F�vrier 2014 � 00h45 R�pondre
Abdoulaye Mopit
Je suis d'accord mais de dire qu'il a �t� bidon comme dit Cocote c'est n'importe quoi.
Samedi 15 F�vrier 2014 � 00h50 R�pondre
pauletic
BIDON une purge ce type!! jallet est meilleur sans talent
Samedi 15 F�vrier 2014 � 11h17 R�pondre
Shadow27
Merde je voulais mettre une petite vanne lyonnaise mais ton analyse pleine d'objectivit� m'a couper la chique....  ;)
J'me la met sur l'oreille!
Vendredi 14 F�vrier 2014 � 22h42 R�pondre
Abdoulaye Mopit
Lol tu la fera � un autre alors :p
Vendredi 14 F�vrier 2014 � 22h47 R�pondre
�
