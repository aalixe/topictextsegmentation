TITRE: Perche : Lavillenie bat le record du monde - Europe1.fr - Multisports
DATE: 2014-02-15
URL: http://www.europe1.fr/Sport/Multisports/Flashs/Perche-Lavillenie-bat-le-record-du-monde-1802605/
PRINCIPAL: 176881
TEXT:
Renaud Lavillenie a battu le record du monde de la perche, samedi, � Donetsk. � REUTERS
ATHL�TISME - Le perchiste a effac� le record du monde de Sergue� Bubka en franchissant 6,16 m.
Il ne pouvait r�ver plus bel endroit pour battre le record du monde : Donetsk, en Ukraine, la ville de Sergue� Bubka, l� o� le "tsar" de la perche avait �tabli son dernier record du monde en 1993 (6,15 m). Samedi, Renaud Lavillenie a am�lior� cette marque qui tenait depuis 21 ans en franchissant 6,16 m* ! Et ce, d�s le premier essai !
Lavillenie franchit 6,16 m � son premier essai :
La performance de samedi est le point d'orgue d'un hiver incroyable avec un saut � 6,04 m � Rouen puis � 6,08 m le 31 janvier dernier � Bydgoszcz, en Pologne. Samedi, � Donestk, Lavillenie a d� s'employer � 6,01 m, barre qu'il n'a effac�e qu'� son troisi�me essai. Avant de demander � ce qu'on place la barre quinze centim�tres plus haut...
RECORD DU MONDE!!!!!!!! 6m16 au premier essai!!!� C'est incroyable, je suis encore dans les airs ...
� Renaud Lavillenie (@airlavillenie) February 15, 2014
"C'�tait totalement incroyable ! Je n'ai jamais utilis� cette perche pour 6,16 m", a confi� le nouveau recordman du monde. "Pour ma premi�re tentative, je souhaitais r�aliser le meilleur saut possible. J'ai demand� 6,16 m parce que c'est le meilleur endroit pour battre le record de Sergue� Bubka, 21 ans apr�s son record."
Conscient qu'il avait ce record dans les jambes, Lavillenie avait chang� son planning de meetings afin d'�tre pr�sent sur les terres de Bubka, samedi. "C'est une performance incroyable, dans ma ville", a reconnu au micro de BFM TV le "tsar", dont le meeting de Donestk est "son" meeting. "Je suis tr�s content et heureux pour lui. Je lui souhaite de continuer � �crire l'histoire de la perche."
Bubka a �t� le premier � f�liciter Lavillenie :
L'histoire de la perche, Lavillenie l'�crit depuis plusieurs saisons d�j�. Champion d'Europe en salle en 2009, il franchit les 6 m pour la premi�re fois cette ann�e-l�. En 2010, il devient champion d'Europe en plein air. Il obtient la cons�cration en 2012, en devenant champion olympique � Londres avec un saut � 5,97 m. L'an dernier, il manque une troisi�me fois le titre de champion du monde en plein air, seul grand titre qui manque � son palmar�s, qui compte d�sormais un incroyable record du monde. Un record qu'il a tout de suite tent� d'am�liorer sur un essai, sans succ�s, � 6,21 m, se blessant m�me l�g�rement au pied droit...
*D�sormais, un record du monde en salle est consid�r� comme record du monde tout court.
