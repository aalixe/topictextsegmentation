TITRE: Le Liban a un nouveau gouvernement apr�s dix mois de blocage - rts.ch - info - monde
DATE: 2014-02-15
URL: http://www.rts.ch/info/monde/5616214-le-liban-a-un-nouveau-gouvernement-apres-dix-mois-de-blocage.html
PRINCIPAL: 176543
TEXT:
Le Liban a un nouveau gouvernement apr�s dix mois de blocage
15.02.2014 15:01
Le pr�sident libanais Michel Sleiman (centre), avec le porte-parle du Parlement Nabih Berri (gauche) et le nouveau Premier ministre Tammam Salam [Keystone]
Le Liban s'est dot� d'un nouveau gouvernement, form� de personnalit�s issues des deux camps rivaux, les Hezbollah chiite et la coalition de l'ex-Premier ministre Saad Hariri.
Le Liban s'est dot� samedi d'un gouvernement de compromis apr�s un blocage de pr�s d'un an entre camps rivaux, exacerb� par le conflit en Syrie voisine qui divise profond�ment le pays. "C'est un gouvernement rassembleur", a d�clar� le nouveau Premier ministre Tammam Salam. Les alli�s du Hezbollah h�ritent des Affaires �trang�res.
"Apr�s dix mois d'efforts, de patience (...), un gouvernement pr�servant l'int�r�t national est n�", a affirm� Tammam Salam apr�s l'annonce de la liste des 24 ministres. "C'est la meilleure formule pour permettre au Liban de faire face aux d�fis", a-t-il ajout�.
Deux camps r�unis
Le gouvernement, form� dans un contexte de violences intermittentes qui secouent le pays, r�unit pour la premi�re fois depuis 3 ans les deux camps rivaux: celui du Hezbollah chiite, qui combat les rebelles de Syrie aux c�t�s du r�gime de Bachar al-Assad, et la coalition men�e par l'ancien Premier ministre Saad Hariri, qui soutient, elle, l'opposition syrienne.
ats/rber
