TITRE: Lait maternel : plus riche pour les gar�ons que pour les filles, les m�res s�adaptent au sexe de l�enfant - France-Monde - La Voix du Nord
DATE: 2014-02-15
URL: http://www.lavoixdunord.fr/france-monde/lait-maternel-plus-riche-pour-les-garcons-que-pour-les-ia0b0n1922143
PRINCIPAL: 0
TEXT:
Le journal du jour � partir de 0,49 �
Le lait des m�res a une composition diff�rente selon qu�elles donnent naissance � un gar�on ou � une fille, a r�v�l� une �tude publi�e vendredi. Les biologistes de l�universit� de Harvard ont d�couvert que le lait produit pour un gar�on est plus riche que celui pour les filles.
Le lait maternelle a une composition diff�rente selon le sexe de l�enfant.
- A +
� Les m�res produisent des recettes biologiques diff�rentes pour un gar�on et pour une fille �, a expliqu� Katie Hinde, une biologiste de l�Universit� de Harvard. Des �tudes sur des humains, des singes et d�autres mammif�res ont r�v�l� des diff�rences dans le contenu du lait et la quantit� produite.
Du lait plus riche pour les gar�ons
Les petits gar�ons ont du lait plus riche en graisse et en prot�ines, donc �nerg�tique, tandis que les petites filles obtiennent de plus grande quantit� de lait.
Plusieurs th�ories ont �t� avanc�es pour expliquer ce ph�nom�ne. Chez les singes par exemple, la femelle aurait tendance � produire plus de calcium dans le lait destin� � des prog�nitures femelles qui h�ritent du statut social de leur m�re.
� Cela permet aux m�res de donner plus de lait � leurs filles ce qui va permettre d�acc�l�rer leur d�veloppement pour commencer � se reproduire plus jeune �, a expliqu� la biologiste de l��volution.
Les m�les n�ont pas besoin de parvenir � la maturit� sexuelle aussi vite que les femelles. La seule limite de leur reproduction d�pendant du nombre de femelles qu�ils peuvent conqu�rir.
Les femelles chez les singes sont nourries au lait maternel plus longtemps que les m�les. Ils passent plus de temps � jouer et ont donc besoin d�un lait plus �nerg�tique.
Une programmation du lait, d�s le ventre de la m�re
Mais on ne sait pas vraiment encore pourquoi chez les humains les m�res produisent des laits diff�rents pour leurs nourrissons selon leur sexe, admet la scientifique.
Il y a des indications montrant que tout est d�j� programm� quand le b�b� est encore dans le ventre de sa m�re.
Une �tude de Katie Hinde publi�e la semaine derni�re montre que le sexe du f�tus influence la production de lait des vaches longtemps apr�s la s�paration de leurs veaux, le plus souvent dans les heures apr�s avoir mis bas.
Cette recherche men�e sur 1,49 million de vaches a montr� qu�au cours de deux cycles de lactation de 305 jours, elles ont produit en moyenne 445 kilos en plus de lait quand elles donnaient naissances � des femelles comparativement � des m�les.
Ces chercheurs n�ont pas non plus constat� de diff�rences dans le contenu de prot�ines ou de graisse dans le lait produit pour une prog�niture femelle ou m�le.
Adapter le lait dans les h�pitaux
Comprendre les diff�rences dans le lait maternel humain et l�impact sur le d�veloppement de l�enfant pourrait aider � am�liorer les formules de lait pour enfant, destin�es aux m�res incapables d�allaiter.
� Si la valeur nutritionnelle du lait maternel est bien reproduite dans les formules, les facteurs favorisant l�immunit� du nourrisson ainsi que les signaux hormonaux sont absents �, a expliqu� la chercheuse.
Pouvoir mieux comprendre comment le lait est � personnalis� � selon chaque enfant permettrait �galement d�aider les h�pitaux � trouver du lait provenant du sein donn� pour aider � mieux nourrir des enfants malades et n�s pr�matur�ment, a-t-elle ajout�.
Cet article vous a int�ress� ? Poursuivez votre lecture sur ce(s) sujet(s) :
