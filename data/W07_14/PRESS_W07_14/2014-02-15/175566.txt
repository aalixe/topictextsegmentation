TITRE: LEAD 2-Wall Street finit en hausse, peu influenc�e par les stats | Reuters
DATE: 2014-02-15
URL: http://fr.reuters.com/article/frEuroRpt/idFRL5N0LJ3CS20140214
PRINCIPAL: 0
TEXT:
LEAD 2-Wall Street finit en hausse, peu influenc�e par les stats
vendredi 14 f�vrier 2014 23h40
�
* Le Dow a gagn� 0,79%, le S&P 500 0,48%, le Nasdaq 0,08%%
* Sur la semaine, le Dow et le S&P gagnent 2,3%, le Nasdaq 2,9%
* Des statistiques impact�es par les intemp�ries ignor�es   (Actualis� avec des d�tails et des pr�cisions)
par Chuck Mikolajczak et Ryan Vlastelica
NEW YORK, 14 f�vrier (Reuters) - Wall Street et ses trois grands indices ont inscrit vendredi une deuxi�me semaine dans le vert d'affil�e, les investisseurs ne s'en laissant pas compter par des statistiques qui ont subi l'impact n�gatif des intemp�ries.
C'est le cas de la production manufacturi�re qui a subi en janvier une baisse inattendue de 0,8%, la plus forte depuis mai 2009, � cause de la vague de froid qui a touch� une bonne partie des Etats-Unis. (voir )
En revanche, les prix � l'exportation ont augment� pour le troisi�me mois cons�cutif en janvier, un signal favorable pour la demande et pour les entreprises am�ricaines.
Derni�re statistique du jour, le moral des m�nages am�ricains est rest� stable durant les premiers jours de f�vrier, l'optimisme sur les perspectives futures �tant temp�r� par les pr�occupations li�es � leur situation financi�re actuelle, suivant les premiers r�sultats de l'enqu�te mensuelle Thomson Reuters-Universit� du Michigan.
L'indice Dow Jones a gagn� 126,80 points (0,79%) � 16.154,39. Le S&P-500, plus large, a pris 8,80 points, soit 0,48%, � 1.838,63. Le Nasdaq Composite a avanc� de son c�t� de 3,35 points (0,08%) � 4.244,03. � Suite...
