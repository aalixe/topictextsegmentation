TITRE: Un gouvernement formé au Liban après dix mois d'impasse - 15 février 2014 - Challenges
DATE: 2014-02-15
URL: http://www.challenges.fr/monde/20140215.REU1202/un-gouvernement-forme-au-liban-apres-dix-mois-d-impasse.html
PRINCIPAL: 176792
TEXT:
Challenges > Monde > Un gouvernement formé au Liban après dix mois d'impasse
Un gouvernement formé au Liban après dix mois d'impasse
Publié le 15-02-2014 à 15h51
A+ A-
Le POremier ministre libanais, Tammam Salam. Les autorités libanaises ont officiellement annoncé samedi la formation d'un nouveau gouvernement, après plus de dix mois d'impasse politique. /Photo prise le 7 avril 2013/REUTERS/Jamal Saidi (c) Reuters
BEYROUTH (Reuters) - Les autorités libanaises ont officiellement annoncé samedi la formation d'un nouveau gouvernement, après plus de dix mois d'impasse politique.
Le pays fonctionnait avec un cabinet de transition depuis la démission du Premier ministre Najib Mikati en mars 2013, due aux rivalités politiques entre le bloc du Hezbollah chiite et un bloc rival à dominante sunnite, celui du 14-Mars. Leur lutte pour le pouvoir a été exacerbée par leur soutien à des camps rivaux de la guerre civile en Syrie voisine.
"Un gouvernement a été formé dans l'intérêt de la nation(...)", a déclaré le Premier ministre, Tammam Salam, dans une allocution à la télévision, en disant son espoir que l'avènement du gouvernement permette au pays d'organiser une élection présidentielle dans les délais prévus et de finir par tenir des élections législatives.
"Ce gouvernement large est la meilleure formation qui puisse représenter le Liban à un moment où il est confronté à des défis d'ordres politique, sécuritaire, économique et social" a déclaré Tammam Salam.
Celui-ci a été choisi comme Premier ministre par le Parlement dès avril 2013 mais il n'avait pas été en mesure de former un gouvernement à cause des rivalités entre le bloc du 8-Mars, dominé par le Hezbollah, et le bloc du 14-Mars, dominé par le Courant du Futur, le parti de Saad Hariri.
L'ancien ministre de l'Energie Gebran Bassil, du bloc du 8-Mars, est nommé ministre des Affaires étrangères et l'ancien ministre de la Santé Ali Hassan Khalil, également du bloc du 8-Mars, devient ministre des Finances.
Le député Nouhad Machnouk, du bloc du 14-Mars, obtient quant à lui le portefeuille de l'Intérieur.
Le Liban est confronté depuis des mois à des violences découlant de la guerre civile en Syrie, où le Hezbollah chiite libanais soutient militairement le régime de Damas.
Vendredi, prenant l'exemple de son père Rafic Hariri, dont il commémorait le neuvième anniversaire de l'assassinat, le 14 février 2005 à Beyrouth, l'ex-Premier ministre Saad Hariri avait souhaité que la modération l'emporte face aux risques de contagion du conflit syrien et face à l'impasse politique qui a privé le Liban d'un gouvernement ces derniers mois.
Laila Bassam, Eric Faye pour le service français
Partager
