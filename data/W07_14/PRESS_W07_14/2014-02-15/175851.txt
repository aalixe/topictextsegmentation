TITRE: Etats-Unis: Barbie en maillot de bain dans Sports Illustrated enflamme la toile - 20minutes.fr
DATE: 2014-02-15
URL: http://www.20minutes.fr/medias/1299798-etats-unis-barbie-en-maillot-de-bain-dans-sports-illustrated-enflamme-la-toile
PRINCIPAL: 0
TEXT:
Twitter
La poup�e de Mattel, qui f�te ces 55 ans cette ann�e, s'affiche sur cette fausse une du traditionnel num�ro sp�cial maillots du magazine Sports Illustrated. DR
MAILLOT - La campagne de pub conjointe de la poup�e et du magazine n�est pas du go�t de tous�
L'apparition de Barbie dans le traditionnel num�ro sp�cial maillots de bain du magazine Sports Illustrated a suscit� une pol�mique aux Etats-Unis , o� certains ont critiqu� la pr�sence de la poup�e au milieu des photos sexy.
A la veille de son 55e anniversaire, la c�l�bre poup�e longiligne en plastique s'affichait sur une fausse une du magazine, devant un grand ciel bleu, v�tue d'un maillot de bain en tricot noir et blanc, � c�t� du grand titre��La poup�e par laquelle tout a commenc�. Une op�ration commerciale qui a mis le feu � la toile .
�Mod�le inatteignable de toutes les femmes�
�Vous avez envie de vous exciter avec des femmes sexy, � peine v�tues dans un magazine de sports pour hommes cette ann�e encore?�Eh bien voil�. En voil� une en plastique. Voil� le premier mod�le inatteignable de toutes les femmes�, a d�nonc� Mary Elizabeth Williams, sur le site d'information salon.com .
Lindsey Feitz, sp�cialiste des questions de femmes et de genre � l'Universit� de Denver, dans le Colorado, consid�re comme��douteux, d'un point de vue �thique�, d'utiliser une poup�e destin�e aux petites filles pour sexualiser l'image de la femme dans un magazine surtout lu par des hommes.
�Ironique�
�Nous sommes pass�s des pin-ups � l'encre des ann�es 40 � des mannequins en une dont les corps ont �t� num�riquement alt�r�s�, explique Mme Feitz � l'AFP dans un email.��Et maintenant le mannequin est remplac� par une repr�sentation en plastique d'une fille-femme sexy. C'est ironique�. Barbie, dont la marque est �valu�e � 3 millions de dollars, suscite r�guli�rement la controverse. Elle a notamment �t� accus�e par le pass� d'incarner une silhouette irr�aliste pour les jeunes filles.
�Depuis ses d�buts, le maillot de bain a transmis un message (...) de force et de beaut�, explique de son c�t� le r�dacteur en chef de Sports Illustrated en charge de cette �dition sp�ciale, M.J. Day.��Et nous sommes ravis que Barbie c�l�bre ces valeurs de mani�re unique�.
La v�ritable couverture du magazine, qui doit para�tre mardi, affiche une photo de trois mannequins en bikini, fesses bronz�es bien en �vidence, les pieds dans l'eau turquoise.
M.C. avec AFP
