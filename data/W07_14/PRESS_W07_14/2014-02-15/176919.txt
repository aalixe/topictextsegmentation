TITRE: Sayyed s'en prend au 8-Mars apr�s la nomination de Rifi au minist�re de la Justice - L'Orient-Le Jour
DATE: 2014-02-15
URL: http://www.lorientlejour.com/article/854756/sayyed-sen-prend-au-8-mars-apres-la-nomination-de-rifi-au-ministere-de-la-justice.html
PRINCIPAL: 176917
TEXT:
Abonnez-vous
L'ex-directeur g�n�ral de la S�ret� g�n�rale libanaise, le g�n�ral Jamil Sayyed, �crou� pendant quatre ans apr�s le meurtre en 2005 de Rafic Hariri, a critiqu� samedi la nomination de�l'ancien directeur des Forces de s�curit� int�rieure (FSI), le g�n�ral Achraf Rifi, comme ministre de la Justice au sein du nouveau gouvernement, dont la formation a �t� annonc�e ce matin.
Dans un communiqu� diffus� par son bureau, Jamil Sayyed a affirm� que "tous les partis politiques ont le droit de choisir la personnalit� de leur choix pour les repr�senter au sein du gouvernement, et le Courant du Futur a le droit de nommer le g�n�ral Achraf Rifi. Mais il n'existe aucune raison morale qui justifierait le compromis que les forces du 8-Mars ont fait au sujet du minist�re de la Justice, d'autant plus que la justice et la s�curit� ne font qu'un aux yeux de la soci�t�". "Celui qui a �t� jug� mal plac� pour g�rer la s�curit� ne devrait pas �tre charg� de la justice", a-t-il ajout�.
Le gouvernement de Tammam Salam a failli ne pas voir le jour en raison de la nomination par le Courant du Futur du g�n�ral Rifi comme ministre de l'Int�rieur, une candidature contest�e par le Hezbollah et Amal en raison de l'hostilit� affich�e par l'ancien directeur g�n�ral des FSI envers le r�gime syrien.
S'adressant aux dirigeants du Hezbollah et d'Amal, M. Sayyed a affirm� que "ce n'est pas la premi�re fois que les deux plus importants mouvements chiites libanais commettaient une erreur aussi grave � l'�gard de leur public". Il a de ce fait annonc� qu'il se voyait oblig� "avec regret" de couper "toute consultation" avec le 8-Mars, en raison de cette "erreur injustifiable".
