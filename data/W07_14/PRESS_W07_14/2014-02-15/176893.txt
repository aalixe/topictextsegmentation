TITRE: Municipales � Angers. Jean-Luc Rotureau veut faire rayonner la ville
DATE: 2014-02-15
URL: http://www.ouest-france.fr/municipales-angers-jean-luc-rotureau-veut-faire-rayonner-la-ville-1928361
PRINCIPAL: 176891
TEXT:
Municipales � Angers. Jean-Luc Rotureau veut faire rayonner la ville
Angers -
13 F�vrier
Jean-Luc Rotureau, entour� de Aleksandra Paulo, son adjointe � l��conomie, et de Claude Eas, adjoint � l�action culturelle.�|�C�line Mont�cot
Facebook
Achetez votre journal num�rique
Le candidat divers gauche � la mairie d�veloppe le premier axe de son programme, sur la visibilit�, l'attractivit� et l'accueil d'Angers.
" Angers � des atouts aux niveaux national et international ", et Jean-Luc Rotureau, candidat divers gauche � la mairie, compte exploiter ces atouts et entend faire rayonner Angers.�
Soutenir et dialoguer avec les entreprises
En d�veloppant la plateforme de l'agence Angers Loire d�veloppement, en personnalisant l'accueil des entreprises et en les aidant � s'y retrouver dans le labyrinthe des aides qui leur sont propos�es, par exemple.�
Renforcer les partenariats avec les villes jumel�es et les villes partenaires d'Angers.
�" Un volet sous exploit� ", selon Aleksandra Paulo, adjointe � l��conomie, l'emploi et le commerce, si Jean-Luc Rotureau est �lu. " Les entreprises doivent profiter au maximum de ce jumelage en se d�pla�ant dans ces villes. "
Un festival a port�e " nationale "
Parmi les projets, la cr�ation d'un festival " Angers en mouvement ", ax� sur la danse et qui aura " une port�e culturelle nationale ",�selon Claude Eas, adjoint � l'action culturelle.�
Mais aussi avec la mise en place d'un lieu, en centre-ville, " La Fabrique " qui accueillera des ateliers artistiques, des expositions, de la commercialisation d'art et des productions angevines.
Tags :
