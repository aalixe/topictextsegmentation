TITRE: Foot - Italie - 24e j. - "Super Mario" est de retour !
DATE: 2014-02-15
URL: http://www.lequipe.fr/Football/Actualites/-super-mario-est-de-retour/441551
PRINCIPAL: 175639
TEXT:
a+ a- imprimer RSS
Mario Balotelli a inscrit contre Bologne un but merveilleux ! (AFP)
Il n'avait plus marqu� depuis le 26 janvier dernier et une victoire � Cagliari. Il avait pleur� samedi dernier , lorsque Clarence Seedorf l'avait sorti pr�matur�ment dans un match � oublier et une d�faite � Naples (1-3) . Vendredi soir, Mario Balotelli a d�livr� le Milan, bouscul� par Bologne, sur un geste qui explique pourquoi l'Italie aime l'appeler "Super Mario" ( 1-0 ).
Il restait une poign�e de minutes � San Siro, lorsque l'international italien a d�clench� une frappe, quasiment sans �lan, de 25 m�tres, qui a fil� sous la barre du gardien bolognais (87e). La pure merveille de son fantasque attaquant a donn� raison � son entra�neur qui l'a laiss� sur le pr� jusqu'au terme d'un match que le Milan aurait pu perdre.
Certes dominateurs, les Lombards se sont montr�s peu dangereux. A l'inverse, Bologne s'est procur� une s�rie d'occasions autour de l'heure de jeu par Cristaldo (60e), Christodoulopoulos (61e) et Rolando Bianchi (63e). Ce Milan-l� pourrait �tre trop court mercredi contre l'Atl�tico Madrid en 8e de finale aller de la Ligue des champions. Sauf si un certain Balotelli...
http://medias.lequipe.fr/img-stade:football-jpg/stade//0-665-0-80/
