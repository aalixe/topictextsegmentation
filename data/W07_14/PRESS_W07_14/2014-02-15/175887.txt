TITRE: Guerre en Syrie: Obama �tudie des moyens de pression sur le pr�sident syrien -   Monde - lematin.ch
DATE: 2014-02-15
URL: http://www.lematin.ch/monde/obama-etudie-moyens-pression-president-syrien/story/20288304
PRINCIPAL: 0
TEXT:
Obama �tudie des moyens de pression sur le pr�sident syrien
Guerre en Syrie
�
Le pr�sident am�ricain a dit vendredi �tudier de nouveaux moyens de pression sur son homologue syrien Bachar al-Assad lors d'un entretien avec le roi Abdallah de Jordanie.
Mis � jour le 15.02.2014 25 Commentaires
� Imprimer
1/141 Le pr�sident syrien Bachar al-Assad (� droite) a d�clar� qu'il ne comptait pas suivre l'exemple du chef d'Etat d�chu ukrainien Viktor Ianoukovitch et qu'il ne quitterait pas le pouvoir,  rapport� l'ex-Premier ministre et ancien chef de la Cour des comptes Sergue� Stepachine, lors d'une conf�rence de presse � Moscou, � son retour de Damas. ( Lundi 7 avril 2014)
Bild: AFP
Le deuxi�me round de la conf�rence de Gen�ve 2 sur la Syrie va reprendre  lundi 10 f�vrier. Objectif: mettre fin � 2 ans et demi de guerre civile  en cr�ant une autorit� de transition.
Articles en relation
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Dans sa r�sidence de Sunnyland, en Californie, le pr�sident am�ricain a dit au souverain jordanien qu'il ne s'attendait pas � ce que le conflit se termine rapidement. Il y a donc �des mesures imm�diates que nous devons prendre pour faciliter l'aide humanitaire�, a-t-il pr�cis�.
�Il y a des mesures interm�diaires que nous pouvons prendre pour imposer davantage de pression sur le r�gime d'Assad et nous continuerons � travailler avec toutes les parties concern�es pour essayer de parvenir � une solution diplomatique�, a-t-il ajout�.
Barack Obama a propos� un milliard de dollars en garanties de pr�ts � la Jordanie.
Armer les rebelles
Barack Obama n'a pas pr�cis� la nature de ces nouvelles mesures � l'�tude. Selon des responsables am�ricains, la Maison-Blanche envisage notamment de fournir des armes aux rebelles syriens pour inciter le r�gime de Damas � se montrer davantage enclin � n�gocier.
Le pr�sident am�ricain a par ailleurs indiqu� que les Etats-Unis entendaient fournir � la Jordanie des garanties de pr�ts d'un milliard de dollars pour l'aider � faire face � l'afflux de r�fugi�s syriens - elle en h�berge plus de 600'000 - et d'autres difficult�s �conomiques.
Il a �galement annonc� que Washington allait renouveler pour cinq ans l'accord qui fixe l'aide vers�e chaque ann�e � la Jordanie. Au terme de l'accord en cours, qui expirera en septembre, Amman re�oit 660 millions de dollars par an. (ats/afp/Newsnet)
Cr��: 15.02.2014, 08h46
