TITRE: Importantes inondations en Corse-du-Sud - France 3 Corse ViaStella
DATE: 2014-02-15
URL: http://corse.france3.fr/2014/02/10/importantes-inondations-porto-en-corse-du-sud-412529.html
PRINCIPAL: 0
TEXT:
+  petit
Voitures sous les eaux � Porto
Le d�partement de la Corse-du-Sud a �t� travers� lundi par un �pisode pluvio-orageux qui a�entra�n� d'importantes pr�cipitations. A Porto, le fleuve est sorti de son lit, inondant les berges du port.�
Les premiers d�g�ts font �tat d'une dizaine de voitures, qui stationn�es sur les berges � la sortie du port de plaisance, se sont retrouv�es en partie noy�es sous les eaux.
Surpris par l'importance du ph�nom�ne, une �quipe de reportage de�France 3 Corse ViaStella�a d� abandonner son v�hicule route de la plage, pour trouver refuge sur les hauteurs.
Inondations et gros d�g�ts � Porto
Fr�d�ric Leca, lieutenant des pompiers; Pierre Mateo, sinistr�; Antoine, habitant de Porto
Eboulements et routes coup�es?
De nombreux �boulements se sont �galement produits, sur la route du col de Vergio, � hauteur du Paesolu d�Aitone�et dans les secteurs d�Ota, Marignane�ont indiqu� les pompiers de Corse-du-Sud.�
A Ajaccio, une route s'est effondr�e rendant l'acc�s au lotissement la Confina II�impossible. A l'a�roport, les locaux de la compagnie a�rienne Air Corsica��taient menac�s par la mont�e des eaux.
A Sagone, une station service a �t� inond�e et des immeubles se trouvant en bord de mer �taient �galement menac�s d�inondation en fin d'apr�s-midi. La Gravona avait �galement franchi sa cote d'alerte et mena�ait de couper le pont de Cuttoli, sur la rive sud du golfe.
�
