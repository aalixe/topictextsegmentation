TITRE: Sotchi : "Je me suis battu comme un lion", dit Brian Joubert - RTL.fr
DATE: 2014-02-15
URL: http://www.rtl.fr/actualites/sport/article/sotchi-je-me-suis-battu-comme-un-lion-dit-brian-joubert-7769732902
PRINCIPAL: 176188
TEXT:
Les Dossiers de RTL.fr - Sotchi 2014
Sotchi : "Je me suis battu comme un lion", dit Brian Joubert
Par Ryad  Ouslimani | Publi� le 15/02/2014 � 10h31
Brian Joubert aux Jeux de Sotchi
Cr�dit : YURI KADOBNOV / AFP
R�ACTION - Brian Joubert, seulement 13e des Jeux Olympiques en patinage artistique pour sa derni�re comp�tition, se dit fier de sa prestation avant de prendre sa retraite.
Vendredi soir, les larmes de Brian Joubert ont certainement �mu les t�l�spectateurs et les fans de patinage artistique. En effet, au terme d'une prestation honorable bien qu'insuffisante (il termine 13e), le Poitevin a tout donn� pour sa derni�re sortie en comp�tition officielle, apr�s plus de 10 ans de carri�re au plus haut niveau.
Une partie de la vie de Brian Joubert s'est termin�e vendredi soir, sur la patinoire de Sotchi , � la fin d'un programme libre ex�cut� avec panache. "Je me suis battu comme un lion, maintenant je sais ce que c'est que les Jeux Olympiques ", a-t-il d�clar� � la fin de la comp�tition.
Je ne voulais pas transmettre une mauvaise image
Brian Joubert, patineur fran�ais
D�sormais retrait�, il va tout d'abord sillonner le monde pour se produire au sein de la troupe de Evgueni Plushenko, apr�s avoir port� le patinage fran�ais pendant 10 ans . "Je ram�ne quand m�me 16 m�dailles � mon pays, des titres de champion d'Europe, un titre de champion du monde", rappelle celui qui avait �t� raill� par les Guignols de l'info, malgr� un palmar�s XXL.
"Je suis rest� � Poitiers dans mon coin, j'ai fait pas mal de bonnes choses", analyse Joubert, qui a toujours refus� de sortir de son cocon et de son environnement. Mais le champion refuse de passer � autre chose et veut continuer � donner au patinage et aux Jeux Olympiques . "J'esp�re revenir en tant qu'entra�neur, et justement en tant qu'entra�neur je ne voulais pas transmettre une mauvaise image des Jeux � mes athl�tes", a-t-il confi�.
Brian Joubert fier de la fin
Cr�dit : J-M Rascol
