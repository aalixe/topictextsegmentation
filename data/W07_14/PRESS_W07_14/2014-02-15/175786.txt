TITRE: Adaptation au vieillissement :  le calendrier se pr�cise - Logement - LeMoniteur.fr
DATE: 2014-02-15
URL: http://www.lemoniteur.fr/145-logement/article/actualite/23677420-adaptation-au-vieillissement-le-calendrier-se-precise
PRINCIPAL: 175785
TEXT:
� Yves Malenfer/Matignon
Remise � Jean-Marc Ayrault des trois rapports sur l'adaptation de la soci�t� au vieillissement
En pr�sence de Marisol Touraine, ministre des  Affaires sociales et de la Sant� et de Mich�le Delaunay, ministre  d�l�gu�e aupr�s de la ministre des Affaires sociales et de la Sant�,  charg�e des Personnes �g�es et de l'Autonomie, Jean-Pierre Aquino, pr�sident du Comit� "Avanc�e en �ge", Luc Broussy (au centre),  conseiller g�n�ral du Val-d�Oise et maire adjoint de Goussainville, et Martine  Pinville, d�put�e de la Charente, ont remis leurs rapports sur  l�adaptation de la soci�t� au vieillissement, � Jean-Marc Ayrault.
La ministre de la Sant�, Marisol Touraine et la ministre d�l�gu�e charg�es des personnes �g�es, Mich�le Delaunay ont communiqu� en Conseil des ministres vendredi 14 janvier les grandes �tapes du projet de loi pour l�adaptation de la soci�t� au vieillissement.
SUR LE M�ME SUJET
Le vieillissement de la population ouvre un march� colossal aux professionnels de l'adaptation de l'habitat
Si l'on connaissait d�j� les grandes lignes du projet de loi pour l'adaptation de la soci�t� au vieillissement - elles ont �t� d�taill�es par Jean-Marc Ayrault jeudi 13 f�vrier � Angers : voir notre article - le calendrier l�gislatif a �t� pr�cis� en Conseil des ministres vendredi 14 f�vier par la ministre de la Sant�, Marisol Touraine et la ministre d�l�gu�e charg�es des personnes �g�es, Mich�le Delaunay.
La projet de loi d�orientation et de programmation qui doit inscrire la totalit� de la politique de l��ge "dans un programme pluriannuel et transversal", sera soumis au Conseil �conomique, social et environnemental (CESE). Apr�s examen au CESE, le projet de loi sera inscrit � l�ordre du jour du conseil des ministres du 9 avril, ce qui devrait permettre au Parlement de l�examiner au printemps en vue d�une adoption d�finitive d�ici la fin de l�ann�e 2014.
Concr�tement, la mise en oeuvre de cette future loi, financ�e par les 645 M� de la contribution additionnelle de solidarit� pour l�autonomie (CASA) cr��e en 2013, se fera ensuite en deux �tapes.
La premi�re, op�rationnelle d�s 2015, se consacrera � la  pr�vention du vieillissement et � l�adaptation du domicile (avec notamment un plan national d�adaptation de 80.000 logements d�ici � 2017 avec des  moyens d�di�s accord�s � l�ANAH) ; tandis que  la seconde, programm�e pour la deuxi�me partie du quinquennat, sera  d�di�e aux �tablissements sp�cialis�s.
�
