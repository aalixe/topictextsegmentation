TITRE: Apr�s 10 mois de blocage, le Liban a enfin un gouvernement - 15/02/2014 - LaD�p�che.fr
DATE: 2014-02-15
URL: http://www.ladepeche.fr/article/2014/02/15/1819299-liban-un-gouvernement-forme-apres-dix-mois-de-blocage.html
PRINCIPAL: 177330
TEXT:
Apr�s 10 mois de blocage, le Liban a enfin un gouvernement
Publi� le 15/02/2014 � 12:52
,
Mis � jour le 15/02/2014 � 19:53
Photo transmise par l'agence libanaise Dalati et Nohra montrant le nouveau gouvernement du Liban et le pr�sident Michel Sleimane, au palais pr�sidentiel de Baabda � Beyrouth
Le Liban s'est dot� samedi d'un gouvernement de compromis r�unissant les deux blocs rivaux, apr�s un blocage de pr�s d'un an exacerb� par le conflit en Syrie voisine qui divise profond�ment le pays.
Le nouveau Premier ministre, Tammam Salam, d�sign� depuis avril 2013, aura la lourde t�che de diriger un gouvernement dans un pays profond�ment divis� quant � l'implication du puissant mouvement chiite Hezbollah dans les combats en Syrie aux c�t�s des forces du r�gime de Bachar al-Assad.
R�percussion de la guerre chez le voisin, le pays est secou� par une vague d'attentats sanglants, visant surtout le Hezbollah, et accueille des centaines de milliers de r�fugi�s syriens.
Ce gouvernement devrait cependant �tre de courte dur�e en raison de l'�lection pr�sidentielle pr�vue au printemps apr�s laquelle un nouveau cabinet doit �tre form�, selon la constitution.
"C'est un gouvernement rassembleur et c'est la meilleure formule pour permettre au Liban de faire face aux d�fis", a affirm� Tammam Salam, apr�s l'annonce de la liste du gouvernement. Il a toutefois affirm� que "le chemin est plein d'emb�ches".
La France a appel� la communaut� internationale � pr�ter assistance � ce nouveau gouvernement, tandis que la Grande-Bretagne a promis son "soutien total" en vue de la "paix et de la prosp�rit� au Liban".
Les Etats-Unis ont �galement salu� la formation du gouvernement, rappelant leur "engagement fort � la souverainet�, la s�curit� et la stabilit� du Liban".
Pour la premi�re fois depuis trois ans, le gouvernement r�unit les deux camps rivaux: celui du Hezbollah et la coalition dite du "14-mars" de l'ex-Premier ministre, le sunnite Saad Hariri, qui soutient l'opposition syrienne.
Gr�ce � un compromis � l'arrach�, le gouvernement de 24 ministres accorde huit portefeuilles au camp du Hezbollah dont deux pour des membres du parti, huit au 14-mars et huit � des ministres proches du pr�sident Michel Sleimane, consid�r� comme neutre, et du leader druze Walid Joumblatt, consid�r� comme centriste.
Selon cette formule, aucun des deux principaux rivaux ne peut bloquer les d�cisions gouvernementales.
Selon des sources proches du 14-mars, M. Hariri a fait une grande concession en acceptant de participer � ce gouvernement avec le Hezbollah, un parti qu'il accuse d'�tre derri�re l'assassinat de son p�re, le dirigeant Rafic Hariri.
Concessions
M. Hariri avait justifi� sa d�cision d'y prendre part en affirmant que c'�tait pour sauver le pays de l'instabilit�.
Il a d� cependant retirer la candidature au poste cl� de l'Int�rieur de son favori, le g�n�ral � la retraite Achraf Rifi, ancien chef influent de la police libanaise et b�te noire du parti chiite qui a oppos� son veto.
Le choix pour l'Int�rieur s'est fix� sur Nouhad al-Machnouk, un d�put� du Courant du Futur de M. Hariri, et M. Rifi a �t� nomm� ministre de la Justice.
En outre, le mouvement de l'alli� chr�tien du Hezbollah, Michel Aoun, obtient le poste convoit� des Affaires �trang�res, attribu� � son gendre Gebrane Bassil, et celui de l'Energie, pour lequel est nomm� un Arm�nien, Arthur Nazarian.
Contrairement au gouvernement pr�c�dent, d�pourvu de femme, une seule a �t� nomm�e au cabinet, Alice Chabtini, une magistrate de premier plan qui s'est vu confier le portefeuille des d�plac�s.
Le principal courant chr�tien alli� de M. Hariri et farouche opposant au Hezbollah, Les Forces libanaises, a refus� de participer au gouvernement.
La formation d'un gouvernement au Liban doit tenir compte de l'�quilibre confessionnel dans un pays o� coexistent 18 communaut�s religieuses musulmanes et chr�tiennes.
Ainsi, les postes au Parlement et au gouvernement sont attribu�s � parit� aux chr�tiens et aux musulmans, bien que les premiers soient minoritaires.
M. Salam a affirm� qu'il oeuvrerait pour "renforcer la s�curit� (...) faire face � toute formes de terrorisme, traitera les probl�mes socio-�conomiques �pineux, notamment celui de l'augmentation des r�fugi�s fr�res syriens et ce que cela suppose comme fardeau".
Selon l'ONU, le nombre de r�fugi�s syriens a atteint plus de 900.000 personnes au Liban, soit pr�s d'un cinqui�me de sa population.
La formation du gouvernement intervient alors que le pays est pris dans un cycle de violences et d'attentats li�s au conflit syrien qui a exacerb� les divisions politiques mais aussi les tensions entre sunnites, en majorit� pro-opposition, et chiites, qui soutiennent pour la plupart Damas.
� 2014 AFP
