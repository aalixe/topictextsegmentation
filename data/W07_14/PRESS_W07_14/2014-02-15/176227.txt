TITRE: Centrafrique : un chef de milice �chappe � l'op�ration de d�sarmement � Bangui
DATE: 2014-02-15
URL: http://www.leparisien.fr/international/centrafrique-vaste-operation-de-desarmemement-engagee-a-bangui-15-02-2014-3593975.php
PRINCIPAL: 0
TEXT:
Centrafrique : un chef de milice �chappe � l'op�ration de d�sarmement � Bangui
Publi� le 15.02.2014, 10h11                                             | Mise � jour :                                                      11h37
Tweeter
Appuy�e par les soldats de la Misca, l'arm�e fran�aise a engag�, t�t samedi matin, une op�ration de d�sarmement dans un quartier de Bangui. Objectif notamment : le domicile de Patrice Edouard Ngaissona, qui se pr�sente comme le �coordonnateur politique� des anti-balaka et �est prot�g� par une douzaine d'hommes arm�s�.
| AFP / Fred Dufour
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Centrafrique : �Ce sera plus long que pr�vu� selon Le Drian
�L'arm�e fran�aise et la force de l'Union africaine � Boy Rabe ont investi ensemble un quartier chr�tien de Bangui.
L'op�ration de ratissage, la plus grosse depuis le d�but de l'op�ration Sangaris en d�cembre, a d�but� peu avant 6 heures sur un large p�rim�tre de ce quartier, fief banguissois des milices anti-balaka, r�guli�rement accus�es d'exactions envers les civils musulmans. Lors des op�rations de fouille, qui ont �t� faites maison par maison, quelques armes automatiques, grenades, armes blanches, ainsi qu'un grand nombre de munitions ont �t� d�couverts.
�Toutes les personnes chez qui des armes ont �t� saisies ont �t� identifi�es et seront remises � la gendarmerie�, a affirm� le capitaine Bolo, un gendarme camerounais de la force africaine Misca, soulignant qu'�une quinzaine d'officiers de police judiciaire de la Misca sont int�gr�s au dispositif�.
Le �gros poisson� a �t� manqu�
Avant 9heures, des �l�ments de la Misca ont pris position autour du domicile de Patrice Edouard Ngaissona, qui se pr�sente comme le �coordonnateur politique� des anti-balaka, et qui �est prot�g� par une douzaine d'hommes arm�s�. Autour de son domicile, d�tonations d'arme automatique et de grenades, d'origine encore inconnue, se sont fait entendre.
L'op�ration s'est termin�e vers 10 heures, sans que Ngaissona soit arr�t�.� �Ils n'ont pas r�ussi � me prendre, j'�tais sorti. Il faut qu'on me dise pourquoi on me cherche�, a-t-il r�agi, affirmant que cinq de ses proches avaient, eux, �t� arr�t�s dans la matin�e.
Ngaissona, �c'�tait le gros poisson qu'il fallait prendre�, a simplement comment� le procureur de la R�publique de Bangui Ghislain Grezenguet, qui avait remis aux hommes de la Misca une liste d'individus � interpeller. Les v�hicules des Sangaris et de l'Union africaine sont repartis sous les hu�es des habitants qui scandaient notamment: �"Cassez-vous ou on va s'occuper de vous !�.
Jeudi, dans un camp de Bangui, ce sont treize corps qui avaient �t� d�couverts dans un puits dans un �tat de putr�faction avanc�e.�Les premi�res constatations laissaient � penser, selon le procureur de la R�publique de la capitale centrafricaine, qu'ils avaient �t� jet�s l� vivants. Dans ce camps sont cantonn�s, sous surveillance de la Misca, des combattants de l'ex-S�l�ka, les rebelles musulmans.
Les milices dans le collimateur des autorit�s
Apparus comme des milices luttant contre la r�bellion S�l�ka, � dominante musulmane, qui avait pris le pouvoir en mars 2013 et qui pers�cutait la communaut� chr�tienne, les anti-balaka ont rapidement sem� la terreur dans Bangui et en province. Apr�s le d�part du pr�sident Michel Djotodia, contraint � la d�mission le 10 janvier 2014, et le d�sarmement et le cantonnement des �l�ments de la S�l�ka men�s par les soldats fran�ais de l'op�ration Sangaris, les anti-balaka s'en sont pris syst�matiquement aux civils musulmans, multipliant lynchages et pillages.
La pr�sidente centrafricaine Catherine Samba Panza a promis mercredi �la guerre� � ces milices, faisant �cho aux d�clarations mena�antes faites ces derniers jours par le ministre fran�ais de la D�fense Jean-Yves Le Drian et les commandants des contingents fran�ais et africain en Centrafrique, qui visaient directement les miliciens et les pillards s�vissant en toute impunit�.
