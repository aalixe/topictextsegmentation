TITRE: Bastia-ASM : les compos - BFMTV.com
DATE: 2014-02-15
URL: http://www.bfmtv.com/sport/bastia-asm-compos-711980.html
PRINCIPAL: 0
TEXT:
r�agir
Eric Abidal ne figure pas sur la feuille de match de Bastia-Monaco (17h). Le capitaine de l�ASM figurait pourtant dans le groupe de Claudio Ranieri et devait logiquement �tre titulaire au centre de la d�fense mon�gasque mais il ne sera m�me pas sur le banc. C�est Nicolas Isimat-Mirin qui sera align� en d�fense centrale aux cot�s de Ricardo Carvalho, sa premi�re titularisation cette saison. C�t� bastiais, Ryad Boudebouz est sur le banc.
Le onze bastiais�: Leca � Harek, Modesto, Squillaci, Palmieri � Yatabar�, Cahuzac, Romaric, Diakit� � Ilan, Ciss�.
Le onze mon�gasque�: Subasic - Fabinho, Isimat-Mirin, Carvalho, Kurzawa � Moutinho, Toulalan, Kondogbia, James - Rivi�re, Germain.
Toute l'actu Sport
