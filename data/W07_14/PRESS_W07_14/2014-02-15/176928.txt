TITRE: Croissance: Hollande salue &quot;la confiance retrouv�e&quot; | La-Croix.com
DATE: 2014-02-15
URL: http://www.la-croix.com/Actualite/France/Croissance-Hollande-salue-la-confiance-retrouvee-2014-02-14-1106472
PRINCIPAL: 176927
TEXT:
Pour le FMI, la politique budg�taire doit aider la croissance autant que possible
Fran�ois Hollande a salu� "la confiance retrouv�e par les acteurs �conomiques" en France, apr�s les chiffres de la croissance publi�s vendredi, a d�clar� la porte-parole du gouvernement Najat Vallaud-Belkacem.
Lors du conseil des ministres vendredi matin, "le pr�sident est (...) revenu sur les chiffres de l'�conomie" et "a salu� la confiance retrouv�e par les acteurs �conomiques", a rapport� Mme Vallaud-Belkacem en rendant compte des travaux du conseil.
"C'est en effet gr�ce � la reprise de l'exportation d'une part, de l'investissement de l'autre et de la consommation que l'�conomie repart", a-t-elle ajout�.
"C'est de bon augure pour l'ann�e qui vient", a poursuivi la porte-parole du gouvernement. En 2014, "nous devrions pouvoir atteindre les 0,9%, voire les 1% sans trop de difficult�s", a-t-elle pr�cis�.
Mais selon elle, "on ne va pas se contenter de ce constat", "c'est le moment d'acc�l�rer nos politiques publiques de soutien � la croissance". Mme Vallaud Belkacem a rappel� deux rendez-vous: un conseil de l'attractivit� lundi � l'Elys�e et la rencontre entre partenaires sociaux � la fin du mois sur le Pacte de responsabilit�.
La croissance �conomique en France a �t� de 0,3% en 2013, un taux un peu meilleur que pr�vu, a annonc� vendredi l'Institut national de la statistique et des �tudes �conomiques (Insee).
"C'est une bonne nouvelle", a comment� de son c�t� Jean-Marc Ayrault, dans une interview � France Bleu Sud Lorraine. Le Premier ministre �tait attendu dans la journ�e � Nancy et Neuves-Maisons (Meurthe-et-Moselle) pour promouvoir l'engagement associatif.
AFP
