TITRE: Paris SG, Blanc salue le "travail bien fait" - Goal.com
DATE: 2014-02-15
URL: http://www.goal.com/fr/news/29/ligue-1/2014/02/14/4620655/paris-sg-blanc-salue-le-travail-bien-fait
PRINCIPAL: 175700
TEXT:
Paris SG, Blanc salue le "travail bien fait"
S�lectionn�
0
14 f�vr. 2014 23:57:00
Paris s�est facilement d�fait de Valenciennes ce vendredi (3-0). Laurent Blanc, son entraineur, a f�licit� les joueurs pour la prestation fournie.
Le PSG a confort� ce soir sa place en t�te du classement de la Ligue 1. A domicile, le club de la capitale a vaincu tranquillement Valenciennes (3-0) . A quatre jours d�un d�placement � Leverkusen, c�est exactement le genre de prestation dont Ibrahimovic et ses co�quipiers avaient besoin pour se mettre en confiance. Et Laurent Blanc , leur entraineur, n�a rien trouv� � y redire (ou presque) apr�s la partie : � La r�p�tition avant Leverkusen consistait avant tout � gagner ce match. Les joueurs pensaient � ce match de Ligue des Champions mais ils ont fait abstraction et ont fait le job. On a eu des occasions qu'on n'a pas su concr�tiser. Oui, on aurait pu �tre efficaces, mais marquer trois buts et ne pas en encaisser, c'est pas mal �.
Relatifs
