TITRE: Actualit�s Nautisme : l'actualit� du nautisme au quotidien avec Figaro Nautisme
DATE: 2014-02-15
URL: http://nautisme.lefigaro.fr/actualites-nautisme/les-news-meteo-11/2014-02-14-22-02-15/au-coeur-de-la-tempete-ulla-13333.php
PRINCIPAL: 0
TEXT:
Mots cl?s : bretagne , Temp?te , Atlantique , Manche , submersions , crues , inondations
Par Figaro Nautisme
La temp�te Ulla a s�vi vendredi soir et la nuit derni�re du Cotentin au Nord-Pas de Calais, et surtout en Angleterre. M�me si cette derni�re est pass�e rapidement sur nos c�tes, cinq d�partements restent en communiqu� sp�cial pour des risques de crues et de submersions par les services de M�t�o Consult : le Finist�re, le Morbihan, la Loire-Atlantique et l'Ille-et-Vilaine.
Cr?dits photo : JEAN-SEBASTIEN EVRARD / AFP
C�est le Finist�re qui a connu les plus fortes rafales hier soir, avec l�un des �pisodes temp�tueux les plus importants de l�hiver. Les rafales ont souffl� � pr�s de 150 km/h en bord de mer avec notamment 152 km/h � Ouessant. La ville de Brest a connu une pointe � 133 km/h (battant l'ancien record de f�vrier 1990). Sur les autres d�partements bretons et les secteurs expos�s du Cotentin, le vent a lui aussi d�pass� le seuil de la temp�te, fix� � 100 km/h.
Les vents violents d'ouest � sud-ouest� se sont ensuite d�cal�s en milieu et fin de nuit vers les c�tes du Pas-De-Calais (r�gion de Boulogne sur mer), avec des pointes entre 120-130 km/h sur les caps et autour de 100 km/h pr�s du littoral, alors que dans les terres les rafales �taient moins fortes (entre 80 et 100 km/h en moyenne).
�
Attention aux submersions marines
Ce samedi matin, la temp�te Ulla nourrit �galement de tr�s fortes vagues en mer d'Iroise, avec des hauteurs moyennes de 7-8 m�tres sur les secteurs les plus expos�s et pr�s de 15 m�tres en hauteur maximale. Le vent de secteur sud-ouest entrave aussi l'�coulement pluvial en Bretagne Sud. Le risque de submersion marine sur la c�te Atlantique est fort. "Il faudra rester tr�s vigilant jusqu'� la derni�re mar�e haute de samedi soir", avertit Emmanuel Streby, pr�visionniste pour M�t�o Consult.  Avec un coefficient sup�rieur � 80, le risque de submersion se maintient jusqu'en d�but de soir�e, avec une accalmie rapide, d'autant que les vents se sont d�j� essouffl�s ce matin, ne soufflant plus qu'entre 40 et 80 km/h entre les c�tes bretonnes et normandes, avec quelques rafales r�siduelles de 90 km/h entre Calais et Dunkerque et m�me 115 km/h � 9 heures au cap Gris Nez.
�
Les principales rafales de vent enregistr�es au passage de la temp�te Ulla :
152 km/h � Ouessant (29)
143 km/h � Plovan (29)
139 km/h � Plougonvelin (29)
134 km/h � Landivisiau (29)
130 lm/h � Morlaix (29)
123 km/h � Cap Griz Nez (62)
114 lm/h � Quimper (29)
112 km/h � Lannion (22)
107 km/h � Gonneville (50)
�
