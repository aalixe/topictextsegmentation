TITRE: Politique | Le fil d�??une vie entre les mains d�??experts
DATE: 2014-02-15
URL: http://www.ledauphine.com/politique/2014/02/15/le-fil-d-une-vie-entre-les-mains-d-experts
PRINCIPAL: 175732
TEXT:
Skichrono
REPèRES - �?thique - La loi Leonetti difficilement interprétable devant le Conseil d�??�?tat Le fil d�??une vie entre les mains d�??experts
Le vice-président du Conseil d�??�?tat, Jean-Marc Sauve, a demandé  aux experts qu�??ils se prononcent sur « le caractère irréversible »  des lésions cérébrales de Vincent Lambert.  Photo AFP
La vie de Vincent Lambert reste en suspens. Hier, le Conseil d�??�?tat a décidé d�??attendre les résultats d�??une nouvelle expertise médicale pour rendre sa décision, avant l�??été.
Cinq ans après un accident de la route qui a laissé Vincent Lambert tétraplégique et dans un état végétatif, ses proches devront encore patienter avant de connaître la décision définitive du Conseil d�??�?tat sur son maintien en vie ou pas. La réponse est promise avant l�??été.
La plus haute juridiction administrative française a suivi les préconisations du rapporteur public, Rémi Keller, qui avait prôné jeudi la prudence, recommandant que trois nouveaux médecins spécialisés se penchent sur l�??état de santé du patient. Ces experts, auxquels le député UMP Jean Leonetti, l�??auteur de la loi de 2008 interdisant l�??acharnement thérapeutique, devrait être associé, se prononceront « sur le caractère irréversible » des lésions cérébrales de Vincent Lambert et sur son pronostic clinique pour autoriser une euthanasie passive, prévue dans la loi Leonetti. L�??équipe du CHU de Reims avait entamé début 2013 ce processus avant d�??être stoppé par deux décisions de justice à la demande des parents du patient.
Une mesure exceptionnelle
Les experts devront déterminer « si ce patient est en mesure de communiquer, de quelque manière que ce soit, avec son entourage » et devront apprécier si les réactions éventuellement décelées « peuvent être interprétées comme un rejet de soins, une souffrance » ou, au contraire, comme « un souhait que ce traitement soit prolongé ». Cette mesure est « exceptionnelle dans une procédure d�??urgence », a commenté le vice-président du Conseil d�??�?tat, Jean-Marc Sauvé, mais elle est « indispensable pour que le Conseil d�??�?tat puisse pleinement remplir sa mission de juge administratif suprême ».
Le Docteur Eric Kariger, qui dirige le service de soins palliatifs de l�??hôpital de Reims, a déclaré hier se « tenir à la disposition des experts pour leur donner tous les moyens de porter un avis éclairé et impartial sur l�??état de Vincent ». « Le Conseil d�??�?tat a démontré la pertinence de notre analyse en affirmant que la loi Leonetti s�??appliquait bien au cas de Vincent et que nous l�??avons respectée à la lettre », s�??est félicité le médecin.
Respecter les volontés
« Si c�??est le temps nécessaire pour que les volontés de Vincent soit respectées, il faut attendre », a de son côté réagi son épouse, Rachel. « J�??ai pleinement confiance dans les médecins », a-t-elle ajouté.
De son côté, l�??Association pour le droit à mourir dans la dignité (ADMD) a regretté, neuf ans après le vote de la loi Leonetti, que le Conseil d�??�?tat soit obligé de se faire expliquer son contenu et ses conditions d�??application.
Le Conseil d�??�?tat a également décidé hier de demander à l�??académie nationale de médecine, au Comité consultatif national d�??éthique et au Conseil national de l�??Ordre des médecins, des « observations écrites » de nature à « l�??éclairer utilement sur l�??application des notions d�??obstination déraisonnable et de maintien artificiel de la vie », comme le préconisait le rapporteur public.
Alors que le cas de Vincent Lambert était débattu au Conseil d�??�?tat, la liberté de choisir sa fin de vie était examinée jeudi au Sénat.
Le président de la République François Hollande a annoncé en janvier un projet de loi sur le sujet qui sera déposé d�??ici à l�??été.
29 septembre 2008 : Vincent Lambert, 38 ans, est victime d�??un accident de la route qui lui cause un traumatisme crânien. Son coma végétatif va évoluer en état pauci-relationnel, c�??est-à-dire « de conscience minimale plus ».
Début 2013 : l�??équipe de soins palliatifs du CHU de Reims décèle une opposition aux soins du patient lui faisant interpréter un refus de vivre. Avec l�??accord de la femme de Vincent Lambert et après une procédure collégiale de réflexion prévue par la loi Leonetti, le docteur Eric Kruger décide l�??arrêt de la nutrition artificielle.
11 mai 2013 : le juge des référés du tribunal administratif de Châlons-en-Champagne, saisi par les parents de Vincent Lambert qui refusent une euthanasie passive, demande à l�??hôpital de Reims de rétablir l�??alimentation et l�??hydratation normales.
16 janvier 2014 : le tribunal administratif de Châlons-en-Champagne enjoint une deuxième fois le CHU de Reims de poursuivre le traitement de Vincent Lambert.
�?té 2014 : le Conseil d�??�?tat se prononcera sur la poursuite ou pas des soins.
Par Patrice BARR�?RE (avec AFP) | Publié le 15/02/2014 à 06:00
Vos commentaires
Connectez-vous pour laisser un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
