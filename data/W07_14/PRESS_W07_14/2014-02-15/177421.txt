TITRE: Sochaux y croit encore  - Ligue 1 - Football.fr
DATE: 2014-02-15
URL: http://www.football.fr/ligue-1/scans/sochaux-y-croit-encore-522400/?sitemap
PRINCIPAL: 177415
TEXT:
15 février 2014 � 21h51
Mis à jour le
Réagir 5
Et si Sochaux réussissait l'exploit de se maintenir ? L'hypothèse est encore peu probable mais les Lionceaux se sont donné les moyens d'y croire en s'imposant samedi, à l'occasion de la 25e journée, face à Guingamp (1-0). C'est le Zambien Sunzu qui a libéré Bonal en fin de match (85e). Au classement, les hommes de Hervé Renard reviennent à trois longueurs de Valenciennes et à cinq points d'Evian-Thonon-Gaillard, le premier non-relégable, qui reçoit Lille dimanche.
