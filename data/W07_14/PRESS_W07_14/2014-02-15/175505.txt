TITRE: Les Inrocks - De La Soul offre sa discographie en t�l�chargement gratuit
DATE: 2014-02-15
URL: http://www.lesinrocks.com/2014/02/14/musique/de-la-soul-offre-sa-discographie-entiere-en-telechargement-gratuit-11474111/
PRINCIPAL: 175503
TEXT:
De La Soul offre sa discographie en t�l�chargement gratuit
14/02/2014 | 18h06
A l�occasion de l�anniversaire de �3 Feet High and Rising�, De la Soul propose l�int�gralit� de son catalogue en t�l�chargement gratuit pendant quelques heures.
Non, vous ne r�vez pas, le l�gendaire trio hip-hop a d�cid� de mettre � disposition l�int�gralit� de son catalogue (cela inclut remixes, raret�s et autres) � disposition de tous. Mais, attention ! ledit s�same ne sera disponible que pour une dur�e de 25 heures, � compter d�aujourd�hui 17 heures, jusqu�� demain 18 heures.
L�op�ration intervient � l�occasion des 25 ans de la parution de leur classique 3 Feet High and Rising. N�y voyez pas un acc�s fulgurant d�altruisme de la part du combo, n�anmoins : leur musique n�a jamais r�ellement �t� disponible sur les plateformes de t�l�chargement, principalement � cause de probl�mes de samples et de copyright. Voyez donc ceci comme un geste symbolique.
Quoiqu�il en soit, cela ne sera disponible que pour une dur�e limit�e, donc pr�cipitez-vous � cette adresse .
