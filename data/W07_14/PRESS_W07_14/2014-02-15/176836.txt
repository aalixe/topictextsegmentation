TITRE: Hockey - Russie-Etats-Unis : c��tait le feu sur la glace ! � metronews
DATE: 2014-02-15
URL: http://www.metronews.fr/sport/hockey-russie-usa-c-etait-le-feu-sur-la-glace/mnbo!3FtAkoW5Pdcvg/
PRINCIPAL: 176832
TEXT:
Mis � jour  : 16-02-2014 17:12
- Cr�� : 15-02-2014 16:38
Hockey - Russie-Etats-Unis : c��tait le feu sur la glace !
JEUX OLYMPIQUES � Les Etats-Unis se sont impos�s dans la rencontre au sommet du premier tour du tournoi de hockey, aux tirs aux buts, face � la Russie. Devant une foule en folie, le suspens a �t� � son comble, dans ce qui restera l�un des grands moments de ces JO.
Tweet
�
Les Am�ricains peuvent se tomber dans les bras, ils viennent de remporter leur match face � la Russie.� Photo :�JONATHAN NACKSTRAND / AFP
Huit. Il aura fallu huit tirs aux buts pour que la Russie s�incline face aux �tats-Unis lors de la phase de poule du tournoi de hockey sur glace (2-2 apr�s la prolongation). Alors que la plan�te enti�re attendait le match entre deux des meilleures �quipes sur la glace, les deux nations ont r�pondu pr�sentes dans un match d�anthologie.
Devant un palais Bolcho� plein � craquer, et sous l��il de Vladimir Poutine, les deux �quipes ont mis peu de temps � mettre le feu � la glace, en venant aux mains au bout d�� peine une minute de jeu. Se rendant coups pour coups, les �quipes marquent tour � tour deux buts sur des lancers de Pavel Datsyuk pour les Russes et Cam Fowler et Joe Pavelski pour les Etats-Unis.
Oshie, h�ros des Etats-Unis
Dans la tension, sous les hourras de la foule, Russes et Am�ricains ne se d�partageaient pas et en venaient � la prolongation, malgr� un but dans les derni�res secondes de la Russie, finalement refus�. Patrick Kane, la star nord-am�ricaine avait bien un face � face � jouer durant ce temps suppl�mentaire, mais les deux �quipes ne trouvaient pas la solution en prolongation non plus.
Les tirs aux buts en arrivaient � la mort subite. Les c�urs Russes et Am�ricains s�arr�taient de battre et sur la glace Kovalchuk/Quick d�un c�t� et Oshie/Bobrovski de l�autre c�t� se livraient � des duels des deux c�t�s de la piste. La paire am�ricaine sortait vainqueur au bout huit face � face, offrant la t�te du groupe aux USA. Et surtout la victoire dans ce match � l�immense rivalit�.�
Yann Butillon
