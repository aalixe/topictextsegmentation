TITRE: Un quart des Am�ricains ignorent que la Terre tourne autour du Soleil
DATE: 2014-02-15
URL: http://www.lemonde.fr/ameriques/article/2014/02/15/un-quart-des-americains-ignorent-que-la-terre-tourne-autour-du-soleil_4367120_3222.html
PRINCIPAL: 0
TEXT:
Un quart des Am�ricains ignorent que la Terre tourne autour du Soleil
Le Monde |
� Mis � jour le
15.02.2014 � 17h56
Un quart des Am�ricains (26�%) ignorent que la Terre tourne autour du Soleil et plus de la moiti� (52�%) ne savent pas que l'homme a �volu� � partir d'esp�ces pr�c�dentes d'animaux. C'est ce que r�v�le une enqu�te aux r�sultats �difiants, men�e�aupr�s de 2�200 personnes par la Fondation nationale des sciences am�ricaine et publi�e vendredi 14 f�vrier.
Sur neuf questions portant sur des connaissances �l�mentaires en physique et en biologie , le score moyen des r�ponses exactes a �t� de seulement 6,5. Malgr� ces r�sultats tr�s m�diocres concernant les connaissances scientifiques, plus de 90�% des Am�ricains interrog�s se sont d�clar�s ��tr�s int�ress�s�� ou ��mod�r�ment int�ress�s�� par les nouvelles d�couvertes m�dicales.
Pourtant, les Etats-Unis paraissent �tre relativement bien plac�s en ce qui concerne ��l'�ducation scientifique informelle��. Ainsi, pr�s de 60�% des Am�ricains sont d�j� all�s dans un zoo, un aquarium, un mus�e d'histoire naturelle ou des sciences et des technologies .
��POUR LE BIEN DE L'HUMANITɠ�
Par ailleurs, pr�s de 90�% des participants � cette enqu�te pensent que les bienfaits de la science d�passent tout danger potentiel. Enfin, un tiers des r�pondants estiment que la science et la technologie devraient b�n�ficier de plus de financements.
Selon l'enqu�te, plus de 90�% des Am�ricains estiment que les scientifiques ��aident � r�soudre des probl�mes difficiles�� et qu'��ils travaillent pour le bien de l'humanit頻, souligne John Besley, professeur adjoint de relations publiques � l'universit� de l'Etat du Michigan.
Cette enqu�te est effectu�e tous les deux ans et figure dans un rapport sur la science et l'ing�nierie que le Conseil national am�ricain de la science remet r�guli�rement au pr�sident et au Congr�s. Le professeur Besley a pr�sent� les r�sultats de cette enqu�te � la conf�rence annuelle de l'Association am�ricaine pour l'avancement de la science r�unie � Chicago du 12 au 17 f�vrier.
Soci�t� am�ricaine
