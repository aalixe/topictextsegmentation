TITRE: Syrie: échec total à Genève, Londres et Paris condamnent le régime | Site mobile Le Point
DATE: 2014-02-15
URL: http://www.lepoint.fr/monde/syrie-echec-total-a-geneve-londres-et-paris-condamnent-le-regime-15-02-2014-1792135_24.php
PRINCIPAL: 0
TEXT:
15/02/14 à 18h42
Syrie: échec total à Genève, Londres et Paris condamnent le régime
Quinze jours après un premier échec , une deuxième session de négociations à Genève entre l'opposition et le gouvernement syriens n'a permis aucune avancée, l'avenir de ces pourparlers étant désormais en question.
Le médiateur de l'ONU, Lakhdar Brahimi s'est dit "tout à fait désolé" et s'est excusé "auprès du peuple syrien dont les espoirs étaient si grands".
Il a mis fin aux discussions, dans l'impasse depuis trois semaines, et n'a fixé aucune date pour une reprise.
"Je pense qu'il est préférable que chaque partie rentre et réfléchisse à ses responsabilités, et (dise) si elle veut que ce processus continue ou non", a déclaré M. Brahimi à la presse.
Il était prévu que ce deuxième cycle de discussions, commencé lundi dernier, s'achève samedi mais le médiateur en accord avec les deux délégations devait fixer une date pour une nouvelle réunion.
Le conflit en Syrie a fait plus de 140.000 morts depuis près de trois ans, a rapporté samedi une ONG syrienne.ue un "sérieux revers" et "la responsabilité en incombe directement au régime d'Assad", a estimé le chef de la diplomatie britannique Willam Hague, soulignant "son plein soutien à Lakhdar Brahimi". Même sentiment pour son homologue français Laurent Fabius qui "condamne l'attitude du régime syrien qui a bloqué toute avancée".
Après le refus d'appliquer l'ordre du jour par la délégation du gouvernement syrien M. Brahimi a choisi de renvoyer tout le monde sans date de retour pour donner à chacun un temps de réflexion.
La dernière réunion à Genève entre l'opposition et le régime avait également échoué avec toutefois une avancée notable puisque pour la première fois les ennemis s'étaient parlés.
Cette fois aucun progrès n'a été enregistré après des discussions particulièrement difficiles.
- "Une perte de temps" -
Rendant compte de l'ultime rencontre, le médiateur a expliqué que les deux parties avaient campé sur leur position.
"Le gouvernement considère que la question la plus importante est le terrorisme, l'opposition considère que la question la plus importante est l'autorité gouvernementale de transition", a-t-il dit ajoutant qu'il avait proposé d'évoquer d'abord "la violence et le terrorisme" pour passer ensuite au problème de "l'autorité gouvernementale".
"Malheureusement le gouvernement a refusé, provoquant chez l'opposition le soupçon qu'ils ne veulent absolument pas parler de l'autorité gouvernementale de transition", a ajouté le médiateur.
"J'espère que les deux parties vont réfléchir un peu mieux et reviendront pour appliquer le communiqué de Genève", adopté en juin 2012 par les grandes puissances comme plan de règlement politique de ce conflit qui dure depuis près de trois ans.
"J'espère que ce temps de réflexion conduira en particulier le gouvernement à rassurer l'autre partie (sur le fait) que quand ils parlent d'appliquer le communiqué de Genève ils comprennent que l'autorité gouvernementale transitoire doit exercer les pleins pouvoirs exécutifs. Bien sûr combattre la violence est indispensable", a ajouté M. Brahimi.
L'exercice des "pleins pouvoirs exécutifs" reviendrait à priver le président Bachar al Assad de ses pouvoirs, même si cela n'est pas écrit explicitement dans le communiqué, d'où le blocage de Damas sur ce point.
Un troisième round de discussions avec le gouvernement syrien sans parler de transition politique serait "une perte de temps", a estimé le porte-parole de la délégation de l'opposition, M. Louai Safi.
"Le régime n'est pas sérieux (...) nous ne sommes pas ici pour négocier le communiqué de Genève mais pour l'appliquer", a-t-il ajouté à propos du plan de règlement politique en Syrie adopté par les grandes puissances en 2012.
- Les Russes doivent faire beaucoup plus -
"Nous devons être sûr que le régime veut une solution politique et pas des tactiques pour gagner du temps", a encore affirmé M. Safi à propos de ces négociations sous médiation de l'ONU entamées le 22 janvier sous la pression de la communauté internationale, en particulier les parrains russe et américain de la Conférence.
Côté gouvernemental, le chef des négociateurs, l'ambassadeur syrien auprès de l'ONU Bachar Al-Jafari s'en est tenu à accuser l'opposition "de ne pas respecter l'agenda", affirmant qu'il fallait d'abord conclure "par une vision commune" sur le premier point, la lutte contre la violence et le terrorisme avant de passer à un autre.
M. Brahimi a indiqué qu'il allait rendre compte à New York au Secrétaire général  Ban Ki-moon et qu'il espérait que se tiendrait une réunion avec lui et les deux chefs de la diplomatie russe et américaine Sergei Lavrov et John Kerry.
Un appel jeudi aux parrains russes et américains de la Conférence n'a produit aucun effet et les Etats-Unis interpellent désormais publiquement la Russie pour lui demander d'en faire plus pour obtenir de la flexibilité de la part de son allié, le régime syrien.
"Nous demandons aux Russes, très franchement, d'en faire beaucoup plus, parce qu'il n'y a pas beaucoup de pays qui peuvent avoir une influence sur le régime", a souligné vendredi la porte-parole adjointe du Département d'Etat, Marie Harf.
Le président Barak Obama, qui a reçu vendredi en Californie le roi Abdallah II de Jordanie a exprimé sa frustration et annoncé sa décision de faire davantage pression sur le régime syrien.
