TITRE: Photos- G�rard Depardieu extatique au carnaval de Nice - Gala
DATE: 2014-02-15
URL: http://www.gala.fr/l_actu/news_de_stars/photos-_gerard_depardieu_extatique_au_carnaval_de_nice_308715
PRINCIPAL: 176944
TEXT:
Le 05/01/2014 G�rard Depardieu, plus heureux loin de planches
Amateur de bonne bouffe et tr�s proche de ses fans, notre G�g� national �tait hier soir � Nice, non loin du maire de la ville Christian Estrosi pour inaugurer le carnaval annuel qui met � l�honneur la gastronomie, du 14 f�vrier au 4 mars.
Foulant le pav� et improvisant quelques pas de danse sur un tube de Britney Spears, G�rard Depardieu a une nouvelle fois cr�� la surprise , hier soir � Nice. La star de cin�ma s�est m�l� � la foule dense du Carnaval de la ville, prenant le temps de plaisanter tour � tour avec ses voisins. Et pour cause, � l�occasion du d�but de ses manifestations traditionnelles, il se tenait sur l�un des 18 chars qui d�filaient devant 600.000 spectateurs.
�
Un show survolt� qui a vraisemblablement r�veill� son �me d�enfant et de blagueur. � Merci � vous et merci � votre joie � s�est-il exclam�. Muni d�une bombe de mousse, l�interpr�te de Cyrano �tait d�cha�n� face aux cam�ras du Parisien. D�autant que c��tait la premi�re fois que ce c�l�bre expatri� assistait aux festivit�s.
�
Une occasion qu�il n�aurait manqu�e pour rien au monde. Ainsi, il a expliqu� dans les colonnes du quotidien : � C�est la premi�re fois que je participe au carnaval �. Et d�ajouter � Cette ann�e, c'est particulier, c'est la gastronomie qui est f�t�e, croyez-moi je ne rate en rien une occasion pareille �.
�
Au bras de l�ancienne pr�sentatrice t�l� Denise Fabre, l�acteur �tait l�invit� de Christian Estrosi, d�put�-maire de Nice. Ce dernier s�est m�me autoris� une plaisanterie de circonstance devant les cam�ras des journalistes, expliquant : � Je lui ai donn� un passeport de Nice pour qu�il ait une double nationalit� : la nationalit� russe et la nationalit� nicoise �. Mais loin de la pol�mique du d�part de l'idole pour la Russie , hier � Nice, l�heure �tait sans conteste � la bonne humeur.
A lire aussi :
