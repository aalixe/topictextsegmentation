TITRE: A Gen�ve, les n�gociations sur la Syrie se terminent sans aucune avanc�e - rts.ch - info - monde
DATE: 2014-02-15
URL: http://www.rts.ch/info/monde/5616034-a-geneve-les-negociations-sur-la-syrie-se-terminent-sans-aucune-avancee.html
PRINCIPAL: 177032
TEXT:
A Gen�ve, les n�gociations sur la Syrie se terminent sans aucune avanc�e
15.02.2014 20:32
Le repr�sentant permanent de la Syrie aupr�s des Nations Unies, Bashar al-Jaafari, lors de la r�union du 14 f�vrier � Gen�ve. [Keystone]
Les n�gociations sur la Syrie qui se tenaient � Gen�ve depuis trois semaines se sont termin�es samedi sans aucune avanc�e. Aucune nouvelle date n'a �t� fix�e pour un prochain rendez-vous.
Le m�diateur de l'ONU, Lakhdar Brahimi, a mis fin samedi aux discussions entre l'opposition et le gouvernement syriens qui �taient dans l'impasse depuis trois semaines � Gen�ve et n'a fix� aucune date pour une reprise.
"Je pense qu'il est pr�f�rable que chaque partie rentre et r�fl�chisse � ses responsabilit�s, et (dise) si elle veut que ce processus continue ou non", a d�clar� Lakhdar Brahimi � la presse.
Temps de r�flexion
Il �tait pr�vu que ce deuxi�me cycle de discussions, commenc� lundi dernier, s'ach�ve samedi. Le m�diateur en accord avec les deux d�l�gations devait fixer une date pour une nouvelle r�union.
Apr�s le rejet de l'ordre du jour par la d�l�gation du gouvernement syrien Lakhdar Brahimi a choisi de renvoyer tout le monde sans date de retour pour donner � chacun un temps de r�flexion.
afp/rber
Paris et Londres condamnent
La France "condamne l'attitude du r�gime syrien qui a bloqu� toute avanc�e", a d�clar� samedi le ministre des Affaires �trang�res, Laurent Fabius, apr�s l'�chec des n�gociations.
"Je condamne l'attitude du r�gime syrien qui a bloqu� toute avanc�e sur l'�tablissement d'un gouvernement de transition et multipli� les violences et les actes de terreur a l'encontre des populations civiles", a-t-il affirm� dans un communiqu�.
"L'impossibilit� de s'entendre sur le programme des prochaines sessions de n�gociations repr�sente un s�rieux revers en vue de trouver la paix en Syrie, et la responsabilit� en incombe directement au r�gime d'Assad", a affirm� de son c�t� samedi le ministre britannique des Affaires �trang�res, William Hague.
vid�os et audios
