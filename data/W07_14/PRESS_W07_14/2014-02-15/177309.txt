TITRE: Renaud Lavillenie (6,16m) : Je quitte le sol� et je me dis�: qu�est-ce qui t�arrive�? - Athl�tisme - Eurosport
DATE: 2014-02-15
URL: http://www.eurosport.fr/athletisme/renaud-lavillenie-616m-je-quitte-le-sol.-et-je-me-dis-quest-ce-qui-tarrive_sto4138058/story.shtml
PRINCIPAL: 0
TEXT:
�
Athl�tisme�-�Record du monde de saut � la perche
Renaud Lavillenie (6,16m) : "Je quitte le sol� et je me dis�: qu�est-ce qui t�arrive�?"
Par�Fran�ois-Xavier RALLET (avec S.V.)�le 15/02/2014 � 21:57, mis � jour le 16/02/2014 � 13:52 @FX_Rallet
Renaud Lavillenie a mis de longues minutes pour r�aliser l'exploit qu'il a accompli en fixant le record du monde � 6,16m. Il raconte LE saut.
�
�
�
Pour Renaud Lavillenie, les �toiles n�ont jamais paru aussi proches. A Donetsk, le perchiste tricolore de 27 ans est parvenu � battre un record du monde que personne ne s�imaginait voir tomber un jour. La barre l�gendaire de 6,15 m�tres et Serguei Bubka�? De l�histoire ancienne, et le fait de le relater ici s�av�re irr�el en soi. Lui-m�me va devoir se poser et prendre quelques minutes, heures, jours pour r�aliser l�exploit accompli. "Il va me falloir du temps pour redescendre r�ellement sur terre c�est s�r, parce que c'est incroyable. C'est un record du monde tellement mythique", a-t-il confi� � nos confr�res de BFM juste apr�s son impensable performance.
A Donetsk, sur les terres du mythe Bubka, apr�s dix jours d�entra�nement de r�ve, Lavillenie a v�cu une journ�e id�ale et comme il ne savait pas que c��tait impossible, il l�a fait. Un concours exemplaire devant le tsar ukrainien lui-m�me. Sans le moindre tourment, si ce n�est cette coupure au pied survenue � sa seule tentative � 6,21 m�tres. Le chiffre est surr�aliste. "Je savais qu'en arrivant l�, j'�tais dans de bonnes dispositions, poursuit-il. Mon �chauffement a �t� id�al. C'est la premi�re fois que je commence aussi haut. C�est parti direct." C�est � 5,76 m�tres que le champion olympique a lanc� son concours. Puis il a franchi 5,91 m�tres d�s sa premi�re tentative. Les deux �checs � 6,01 m�tres n�ont jamais �branl� sa confiance�: "Je sentais que j'avais vraiment les moyens de faire quelque chose, peut-�tre pas dans cette facilit�-l�. Mais, � aucun moment je n'ai dout�."
� J�ai saut� 19 centim�tres de plus qu�aux JO�
Et puis, il y a cette r�ussite � 6,16 m�tres. Du premier coup, avec une nouvelle perche et une facilit� d�concertante. Entre Lavillenie et cette barre inaccessible depuis 21 ans, quatre, cinq, six centim�tres�? Qu�importe. Le Clermontois d�adoption raconte�: "Je quitte le sol, je me retrouve au-dessus de la barre, et l� je me dis�: 'Qu'est-ce qui t'arrive ?' Il y a un bruit de dingue, tout le monde est en train de se tenir dans les bras." Et quand on lui demande de classer son record du monde dans la hi�rarchie de ses exploits pass�s, lui s�y refuse�: "Le titre olympique �tait �norme. C��tait un concours o� il y avait de la pression. Mais l�, c�est juste incroyable. J�ai saut� 19 centim�tres de plus qu�aux JO. Je me rends compte que je suis pass� dans une autre dimension. Je n�y crois pas."
�
Incr�dule, Jean Galfione l�est tout autant�: "Je suis sid�r�. Qu�il l�ait fait aussi vite apr�s ses 6,08 m�tres, c�est bluffant." Adversaire de Bubka en son temps, l�ancien recordman de France s�est longtemps imagin� devenir le dauphin de l�Ukrainien. A l��poque, il �tait difficile d�esp�rer plus. "Renaud, lui, fait 6,08 m�tres et derri�re, il bat le record du monde, poursuit Galfione. Faire mieux que Bubka, c�est comme aller plus vite que Bolt, avoir de meilleures stats que Michael Jordan. Il d�tr�ne une l�gende."� D�mesur�e cette comparaison avec le sprinteur jama�cain�? "Peut-�tre que maintenant non, juge Lavillenie. Apr�s, Usain Bolt est encore sur une autre plan�te, � mon avis. Mais je me rends compte de l�ampleur de la performance mine de rien."
Comme l�avance Galfione, Lavillenie n�a jamais �t� "le plus fort, le plus grand, le plus rapide", mais le champion olympique de Londres, loin d�avoir un physique hors-norme, a pour lui de "ne pas conna�tre ses limites." C�est l� toute sa force. "Je me suis entra�n� super dur pour �a. J'ai fait des choix, des strat�gies, des changements pour �a, juge simplement l�Auvergnat. Et l�, il y a tout qui paie." Obs�d� par ce record du monde, Lavillenie va devoir se trouver d�autres exploits � accomplir. A ce jour, et sauf erreur de notre part, Bubka reste le meilleur r�alisateur d'un saut � la perche en plein air (6,14m). C�est dans ses cordes maintenant.
�
A propos de l'auteur
Fran�ois-Xavier RALLET (avec S.V.)�-�Eurosport
Entr� � Eurosport en 2003, Fran�ois-Xavier Rallet nourrit une passion ancestrale pour le basket mais c�est sur le cyclisme, les sports olympiques et les sports blancs qu�il trempe le plus souvent sa plume. Co-r�dacteur du blog Roue Libre, il �tait l�envoy� sp�cial de la r�daction aux JO de Londres. @FX_Rallet 117175470403475089823
�
