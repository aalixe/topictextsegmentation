TITRE: Serie A. Un miracle de Balotelli sauve l'AC Milan
DATE: 2014-02-15
URL: http://www.ouest-france.fr/serie-un-miracle-de-balotelli-sauve-lac-milan-1931314
PRINCIPAL: 175686
TEXT:
Serie A. Un miracle de Balotelli sauve l'AC Milan
Italie -
Mario Balotelli a offert la victoire � Milan gr�ce � un but lumineux.�|�Photo : Reuters
Facebook
Achetez votre journal num�rique
Un but lumineux de Mario Balotelli � 4 minutes de la fin a sauv� l'AC Milan contre Bologne (1-0), mais le jeu fut bien pauvre.
Balotelli, en crise depuis quelques mois, a ressorti sa frappe de mule au meilleur moment (84'), en coin de 30 m sous la barre. Ce but masque la tr�s triste performance du Milan, de mauvais augure � cinq jours de son 8e de finale aller de Ligue des champions contre l'Atletico Madrid.�
"Elle est bien partie, j'ai eu de la r�ussite", a dit Balotelli � la cha�ne Sky Sport, avant d'admettre que le match de son �quipe n'�tait "pas bon du tout".�
Sans id�e, sans puissance ni changement de rythme, les "Rossoneri" ont produit un p�le spectacle � San Siro.�
Clarence Seedorf n'a pas encore trouv� de solution apr�s un mois sur le banc, d�crochant sa troisi�me victoire en six matches, mais la deuxi�me dans les derni�res minutes, comme � Cagliari (2-1).�
Le N�erlandais avait pourtant choisi un 4-2-3-1 th�oriquement fort offensif, avec Riccardo Montolivo en r�cup�rateur plus joueur de balle aux c�t�s de Nigel De Jong, et un milieu tr�s cr�ateur Keisuke Honda-Kaka-Adel Taarabt en soutien de Balotelli.�
Avant le but, le Milan n'avait gu�re vibr� que sur un ballon amen� dans la surface par Giampaolo Pazzini (78'), entr� � la place de Honda, sorti sous les sifflets.�
C'est m�me Bologne qui s'est montr� le plus dangereux, notamment � l'heure de jeu par l'Argentin Jonathan Cristaldo (60'), double buteur une semaine plus t�t au Torino (2-1), et le Grec Lazaros Christodoulopoulos (61').�
La meilleure nouvelle est peut-�tre le r�veil de "Balo". "On s'occupe un peu trop de ma vie priv�e', a-t-il dit, allusion � sa fille Pia, qu'il a reconnue r�cemment apr�s un test de paternit�. 'Je conseillerais � tout le monde de me laisser tranquille, et je donnerai tout sur le terrain".�
Le Milan en aura besoin contre l'Atletico.�
Lire aussi
