TITRE: Angers accroch�, Brest se relance - Football - Eurosport
DATE: 2014-02-15
URL: http://www.eurosport.fr/football/angers-accroche-brest-se-relance_sto4137416/story.shtml
PRINCIPAL: 176748
TEXT:
Angers accroch�, Brest se relance
Par�Alexandre REIGNOUX�le 15/02/2014 � 15:56, mis � jour le 15/02/2014 � 15:56
Angers a �t� tenu en �chec sur sa pelouse Laval (1-1) alors que Brest a battu Auxerre (1-0). Angers repasse deuxi�me du classement � quatre points de Metz . Angers compte 41 points alors que Laval tombe � la 19�me. De son c�t� Brest se relance avec une victoire face � Auxerre. Les Bretons remontent � la 18e place � 3 points de N�mes, premier non rel�guable.
�
Jung et Drmic � Leverkusen ?
13:24
M�nchengladbach � la recherche un d�fenseur central
13:04
Officiel : Adrian Ramos signe � Dortmund
12:29
Stambouli et Cabella sur le d�part
11:37
Klinsmann et Vogts mettent la pression sur L�w
11:24
OM : Thauvin � nouveau forfait contre Montpellier ?
08:14
OM : Le nouvel entra�neur bient�t connu
01:00
Trop facile pour la France ?
00:04
Matuidi : "A ce niveau on manque encore un peu d'exp�rience"
00:00
Hazard forfait "15 jours minimum"
08/04
Klopp : "Aucun reproche � faire � personne"
08/04
Blanc : "Pas mal de choses n'ont pas fonctionn�"
08/04
Le PSG �limin� en quart de finale apr�s sa d�faite � Chelsea (2-0)
08/04
Le Real Madrid se qualifie en demi-finales malgr� une d�faite 2-0 � Dortmund
08/04
Averti face � Chelsea, Edinson Cavani (PSG) ne jouerait pas la demi-finale aller
08/04
En direct : Eden Hazard (Chelsea) sort sur blessure � la 18e minute
08/04
La Juve retrouve ses forces, pas l'OL
08/04
Ronaldo sur le banc, Benzema en pointe
08/04
Chelsea aligne Samuel Eto'o d�s le coup d'envoi
08/04
Le PSG avec un trio Lucas - Lavezzi - Cavani, Pastore est rempla�ant
08/04
Jacobs: "Le probl�me, c'est qu'on commence la rencontre � 5 ou 6"
08/04
Patrice Lair va quitter l'OL
08/04
