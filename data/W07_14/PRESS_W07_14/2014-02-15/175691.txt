TITRE: La grippe s�installe en France - Le journal du Week-end - Replay
DATE: 2014-02-15
URL: http://videos.tf1.fr/jt-we/2014/la-grippe-s-installe-en-france-8365852.html
PRINCIPAL: 0
TEXT:
Sciences - 1min 56s -
Le 14 f�vr. � 20h35
Si la gastro est moins pr�sente, l'�pid�mie de grippe s'installe au contraire en France. Les m�decins et les h�pitaux enregistrent une augmentation des cas depuis quelques semaines, et les enfants et personnes �g�es sont les populations les plus vuln�rables. Les r�gions les plus touch�es par le virus sont la Provence-Alpes-C�te d'Azur et le Languedoc.
