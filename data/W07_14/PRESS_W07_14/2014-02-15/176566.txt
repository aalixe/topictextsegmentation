TITRE: La conférence de paix sur la Syrie n'aura rien changé - 7SUR7.be
DATE: 2014-02-15
URL: http://www.7sur7.be/7s7/fr/16921/Syrie/article/detail/1794330/2014/02/15/La-conference-de-paix-sur-la-Syrie-n-aura-rien-change.dhtml
PRINCIPAL: 176561
TEXT:
15/02/14 -  13h16��Source: AFP
� epa.
MISE � JOUR Le m�diateur de l'ONU, Lakhdar Brahimi, a mis fin samedi aux discussions entre l'opposition et le gouvernement syriens qui �taient dans l'impasse depuis trois semaines � Gen�ve et n'a fix� aucune date pour une reprise.
"Je pense qu'il est pr�f�rable que chaque partie rentre et r�fl�chisse � ses responsabilit�s, et (dise) si elle veut que ce processus continue ou non", a d�clar� M. Brahimi � la presse. Il �tait pr�vu que ce deuxi�me cycle de discussions, commenc� lundi dernier, s'ach�ve samedi mais le m�diateur, en accord avec les deux d�l�gations, devait fixer une date pour une nouvelle r�union.
Apr�s le rejet de l'ordre du jour par la d�l�gation du gouvernement syrien, M. Brahimi a choisi de renvoyer tout le monde sans date de retour pour donner � chacun un temps de r�flexion. "Le gouvernement consid�re que la question la plus importante est le terrorisme, l'opposition consid�re que la question la plus importante est l'autorit� gouvernementale de transition (...) nous avons sugger� que le premier jour nous parlions de la violence et de combattre le terrorisme, et le second jour de l'autorit� gouvernementale, �tant bien clair qu'une journ�e sur chaque sujet ne saurait suffire", a expliqu� M. Brahimi.
"Malheureusement le gouvernement a refus�, provoquant chez l'opposition le soup�on qu'ils ne veulent absolument pas parler de l'autorit� gouvernementale de transition", a ajout� le m�diateur. M. Brahimi s'est dit "tout � fait d�sol�" et s'est excus� "aupr�s du peuple syrien dont les espoirs �taient si grands".
"J'esp�re que les deux parties vont r�fl�chir un peu mieux et reviendront pour appliquer le communiqu� de Gen�ve", adopt� en juin 2012 par les grandes puissances comme plan de r�glement politique de ce conflit qui dure depuis pr�s de trois ans.
Lire aussi
