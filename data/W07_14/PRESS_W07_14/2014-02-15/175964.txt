TITRE: Temp�te Ulla : quatre d�partements toujours en vigilance orange | Atlantico
DATE: 2014-02-15
URL: http://www.atlantico.fr/pepites/tempete-ulla-quatre-departements-toujours-en-vigilance-orange-982985.html
PRINCIPAL: 175958
TEXT:
ATLANTICO BUSINESS avec JM Sylvestre CULTURE Le gouvernement Valls Disparition du vol MH370 de Malaysia Airlines Fran�ois Hollande change son gouvernement �lections municipales 2014 NSA : le scandale des �coutes am�ricaines Affaire des �coutes de Nicolas Sarkozy La tribune de Nicolas Sarkozy Ukraine : Kiev s'embrase La pollution � Paris Sarkoleaks, les enregistrements pirates Election pr�sidentielle de 2017 Accident de Michael Schumacher
Temp�te Ulla : quatre d�partements toujours en vigilance orange
Le Finist�re, le Morbihan, la Loire-Atlantique et l'Ille-et-Vilaine risquent toujours des inondations, alors que 90.000 foyers sont encore priv�s d'�lectricit�s.
Ulla-la !
RSS
�
L'Ouest de la France se r�veille groggy apr�s le passage de la temp�te Ulla, cette nuit. Le vent a souffl� jusqu'� 185 km/h hier � Crozon et les rafales ont d�pass� 110 voire 130 km/h � Quimper, Brest ou Morlaix. De nombreuses routes, des ponts et�des�voies de chemin de fer�ont subi ces chutes d'arbres en s�rie tandis que le transport maritime et a�rien s'arr�tait d'un coup. La SNCF estimait tard hier soir � 2.000�le�nombre de personnes bloqu�es dans�les�gares de Rennes, Guingamp et Saint-Brieuc. Le trafic est toujours interrompu entre Brest et Rennes, mais a �t� r�tabli entre Rennes et Saint-Brieuc.
C�t� �lectricit�,�90.000 foyers sont toujours dans le noir ce matin ; ils �taient 120.000 au plus fort de la temp�te. Il faudra sans doute attendre la fin du weekend pour un retour complet � la normale, pr�vient Bernard Laurans, directeur d'ERDF Bretagne, interrog� par France Info .
Si�les rafales ne d�passeront plus les 80 km/heure pr�s du littoral dans l'apr�s-midi et que les nuages devraient simplement donner quelques averses,�M�t�o France reste vigilante. Le Finist�re, le Morbihan, la Loire-Atlantique et l'Ille-et-Vilaine restent en alerte orange pour les risques d'inondations et le littoral est maintenu en alerte vague-submersion.�
