TITRE: Recherche: Le lait maternel est diff�rent pour les gar�ons et les filles -  News Savoirs: Sant� - tdg.ch
DATE: 2014-02-15
URL: http://www.tdg.ch/savoirs/sante/lait-maternel-different-garcons-filles/story/27862934
PRINCIPAL: 177422
TEXT:
Le lait maternel est diff�rent pour les gar�ons et les filles
Mis � jour le 15.02.2014 9 Commentaires
Le lait des m�res a une composition diff�rente selon qu'elles donnent naissance � un gar�on ou � une fille, r�v�le une recherche publi�e vendredi.
Des recherches prouvent que la composition du lait maternel diff�re selon le sexe du b�b�.
Image: Archives/photo d'illustration/AFP
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Signaler une erreur
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Des �tudes sur des humains, des singes et d'autres mammif�res ont r�v�l� une vari�t� de diff�rences dans le contenu du lait et la quantit� produite.
�Les m�res produisent des recettes biologiques diff�rentes pour un gar�on et pour une fille�, a expliqu� Katie Hinde, une biologiste de l'Universit� de Harvard.
Ainsi les petits gar�ons ont du lait plus riche en graisse et en prot�ines donc �nerg�tique tandis que les petites filles obtiennent de plus grande quantit�s de lait.
Plusieurs th�ories ont �t� avanc�es pour expliquer ce ph�nom�ne, a relev� Katie Hinde lors d'une pr�sentation � la conf�rence annuelle de l'Association am�ricaine pour l'avancement de la science (AAAS) r�unie � Chicago du 13 au 17 f�vrier.
Plus de lait aux filles
Chez les singes rh�sus par exemple, la femelle a tendance � produire plus de calcium dans son lait destin� � des prog�nitures femelles qui h�ritent du statut social de leur m�re.
�Cela permet aux m�res de donner plus de lait � leurs filles ce qui va permettre d'acc�lerer leur d�veloppement pour commencer � se reproduire plus jeune�, a expliqu� la biologiste de l'�volution.
Les m�les n'ont pas besoin de parvenir � la maturit� sexuelle aussi vite que les femelles car leur seule limite sur la fr�quence de leur reproduction d�pend du nombre de femelles qu'ils peuvent conqu�rir.
Les femelles chez les singes sont nourries au lait maternel plus longtemps que les m�les qui passent plus de temps � jouer et qui ont de ce fait besoin d'un lait plus �nerg�tique.
Mais on ne sait pas vraiment encore pourquoi chez les humains les m�res produisent des laits diff�rents pour leur nourrissons selon leur sexe, admet la scientifique. Il y a des indications montrant que tout est d�j� programm� quand le b�b� est encore dans le ventre de sa m�re.
Influenc� par le sexe du foetus
Une �tude de Katie Hinde publi�e la semaine derni�re montre que le sexe du foetus influence la production de lait des vaches longtemps apr�s la s�paration de leurs veaux, le plus souvent dans les heures apr�s avoir mis bas.
Cette recherche men�e sur 1,49 million de vaches a montr� qu'au cours de deux cycles de lactation de 305 jours, elles ont produit en moyenne 445 kilos en plus de lait quand elles donnaient naissance � des femelles comparativement � des m�les.
Ces chercheurs n'ont pas non plus constat� de diff�rences dans le contenu de prot�ines ou de graisse dans le lait produit pour une prog�niture femelle ou m�le.
Comprendre les diff�rences dans le lait maternel humain et l'impact sur le d�veloppement de l'enfant pourrait aider � am�liorer les formules de lait pour enfant destin�es aux m�res qui ne peuvent allaiter.
�Si la valeur nutritionnelle du lait maternel est bien reproduite dans les formules, les facteurs favorisant l'immunit� du nourrisson ainsi que les signaux hormonaux sont absents�, a expliqu� la chercheuse.
Pouvoir mieux comprendre comment le lait est �personnalis� selon chaque enfant permettrait �galement d'aider les h�pitaux � trouver du lait provenant du sein donn� pour aider � mieux nourrir des enfants malades et n�s pr�matur�ment, a-t-elle ajout�. (afp/Newsnet)
Cr��: 15.02.2014, 12h05
