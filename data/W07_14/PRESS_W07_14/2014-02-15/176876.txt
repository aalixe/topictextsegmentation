TITRE: Cette femme va vous donner une bonne le�on de courage. Elle assume son corps sans probl�me
DATE: 2014-02-15
URL: http://www.ohmymag.com/danse/cette-femme-va-vous-donner-une-bonne-lecon-de-courage-elle-assume-son-corps-sans-probleme_art78349.html
PRINCIPAL: 0
TEXT:
Vid�o
A voir aussi
Publi� par J�r�my Garandeau , le
10 f�vrier 2014
Atteinte d'un d�r�glement hormonal, Whitney Thore a pris �norm�ment de poids en l'espace de quelques ann�es. Une surcharge pond�rale qui l'a oblig�e � mettre de c�t� sa grande passion pour la danse. Aujourd'hui, elle foule � nouveau le parquet pour nous livrer une incroyable danse.
Quand Whitney Thore a vu son corps changer et les kilos s'engendrer, cette jeune femme a totalement perdu confiance en elle.�Pourtant, d�s son plus jeune �ge, Whitney Thore �tait une fille �nergique et passionn�e par la danse.
A l'�ge de 4 ans, Whitney Thore enflammait d�j� les parquets de salles de danse. A l'�ge de 16 ans, elle donnait des cours dans un club. La reine du dancefloor, c'�tait elle. Mais, un d�r�glement hormonal scientifiquement appel� le Syndrome de Stein-Leventhal - plus commun�ment connu sous le nom des "ovaires polykystiques" ou "SOPK" - a eu des effets incontr�lables sur son anatomie.
De 58 kilos, la jeune femme a atteint le poids de 158 kilos. Une surcharge pond�rale qui a totalement min� la confiance de Whitney Thore. Elle a alors mis de c�t� sa passion. Mais pas question de se�laisser abattre plus longtemps. Apr�s un cours passage en Cor�e o� la pression sociale �tait moindre, la jeune femme a �t� appel�e par un animateur radio, Jared Pike. Ce dernier lui a propos� de faire partie de son projet, "Fat�Girl Dancing". La vid�o de ce projet a �t� partag�e sur�YouTube. On y�aper�oit Whitney Thore et Jared Pike danser sur le dernier tube de Jason Derulo, Talk�Dirty To Me. Et, comme on peut�le voir, Whitney Thore n'a rien perdu de sa superbe sur les parquets et serait bien plac�e pour donner des cours � un grand nombre d'entre nous !�
Suivez-nous sur Facebook
Vous �tes d�j� abonn� ? Ne plus afficher
Suivre
