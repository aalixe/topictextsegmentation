TITRE: France - Monde | Il surgit dans un bar, tire plusieurs balles et tue la gérante
DATE: 2014-02-15
URL: http://www.estrepublicain.fr/actualite/2014/02/14/il-surgit-dans-un-bar-tire-plusieurs-balles-et-tue-la-gerante
PRINCIPAL: 175874
TEXT:
- Publié le 14/02/2014
PARIS Il surgit dans un bar, tire plusieurs balles et tue la gérante
Un homme armé a été interpellé vendredi en début de soirée à la sortie d�??un bar à Paris après y être entré et avoir ouvert le feu à plusieurs reprises, tuant la gérante de l�??établissement, a-t-on appris de source proche de l�??enquête.
Vers 18h00, cet homme est entré dans le bar L�??Aiglon, avenue de la Grande-Armée (XVIIe), «il a sorti une arme et a tiré à plusieurs reprises», a expliqué cette source.
La gérante de l�??établissement a été touchée par trois projectiles «au niveau du thorax» et est décédée quelques instants plus tard, selon cette source.
Peu après, des policiers ont aperçu un homme sortant du bar «avec une arme à la main», et sont parvenus à l�??interpeller, a assuré la source.
AFP
Poster un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
