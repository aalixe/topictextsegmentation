TITRE: Sauts (D): Tsuper sacr�e, Lassila bronz�e - JO 2014 - Sports.fr
DATE: 2014-02-15
URL: http://www.sports.fr/jo-2014/ski-acrobatique/scans/sauts-d-tsuper-sacree-lassila-bronzee-1010350/
PRINCIPAL: 0
TEXT:
La vasque olympique est allumée... Elle le sera jusqu'au 23 février. (Reuters)
Publi� le
14 février 2014 � 19h54
Mis à jour le
15 février 2014 � 01h32
En finale de l'épreuve de sauts féminine ce vendredi soir à Sotchi, Alla Tsuper a été sacrée championne olympique, alors que Lydia Lassila, tenante du titre, est allée chercher le bronze.
La Biélorusse a réalisé une excellente finale, qui se déroulait en trois temps. Avec un run évalué à 98.01 points lors de son dernier essai, Tsuper a terminé largement devant ses concurrentes, alors qu'elle avait été créditée de 99.18 points lors de sa première tentative.
La Chinoise Mengtao Xu pointe loin derrière mais prend la médaille d'argent (83.50 points) tandis que l'Australienne Lydia Lassila, dernière titrée dans cette discipline, à Vancouver en 2010, repartira de Sotchi avec le bronze (72.12).
