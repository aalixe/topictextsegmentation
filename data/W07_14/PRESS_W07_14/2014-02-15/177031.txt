TITRE: Turquie: le parlement adopte une réforme judiciaire controversée - 16 février 2014 - Le Nouvel Observateur
DATE: 2014-02-15
URL: http://tempsreel.nouvelobs.com/monde/20140215.AFP0173/turquie-le-parlement-adopte-une-reforme-judiciaire-controversee.html
PRINCIPAL: 177028
TEXT:
Actualité > Monde > Turquie: le parlement adopte une réforme judiciaire controversée
Turquie: le parlement adopte une réforme judiciaire controversée
Publié le 15-02-2014 à 10h15
Mis à jour le 16-02-2014 à 11h25
A+ A-
Ankara (AFP) - Le parlement turc a adopté samedi un projet de loi très controversé destiné à renforcer le contrôle politique sur les nominations de magistrats, a annoncé une source parlementaire.
La loi a été votée à l'initiative du gouvernement qui se débat en plein scandale politico-financier, après des échanges houleux entre députés du parti au pouvoir et ceux de l'opposition.
Avant le vote, le Premier ministre Recep Tayyip Erdogan avait annoncé le gel des articles les plus controversés du texte réformant le Haut-conseil des juges et magistrats (HSYK), après plusieurs jours de vives tensions entre la majorité et ses adversaires.
Le débat avait dégénéré à deux reprises en pugilat entre députés rivaux.
L'opposition a dénoncé ce projet qui donne le dernier mot au ministre de la Justice en matière de nomination de magistrats, affirmant qu'il est contraire à la Constitution et qu'il a pour seul but de permettre au gouvernement d'étouffer les enquêtes qui le menacent.
Pour entrer en vigueur la loi doit encore être signée par M. Erdogan.
Partager
