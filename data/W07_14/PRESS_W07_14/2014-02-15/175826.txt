TITRE: Tempête Ulla : 90.000 foyers privés d'électricité en Bretagne - 15 février 2014 - Le Nouvel Observateur
DATE: 2014-02-15
URL: http://tempsreel.nouvelobs.com/societe/20140215.OBS6488/tempete-ulla-100-000-foyers-prives-d-electricite-en-bretagne.html
PRINCIPAL: 175824
TEXT:
Publié le 15-02-2014 à 07h38
Mis à jour à 16h47
A+ A-
Des vents �150 km/h ont �t� enregistr�s dans la nuit. Un homme est mort � la suite d'une chute � bord d'un paquebot.
La tempête Ulla a frappé la Bretagne où les perturbations se succèdent depuis le début de l'hiver. (JEAN-SEBASTIEN EVRARD)
Un octog�naire est d�c�d� vendredi � bord d'un paquebot naviguant au large de la Bretagne � la suite d'une chute provoqu�e par les mauvaises conditions m�t�orologiques frappant le Nord-Ouest du pays, tandis que 90.000 foyers �taient toujours priv�s d'�lectricit� en Bretagne o� des vents � 150 km/h ont �t� enregistr�s dans la nuit.
Une femme � bord du m�me paquebot a �t� �vacu�e par h�licopt�re � la suite �galement d'une chute, a indiqu� la pr�fecture maritime de l'Atlantique.�
"Les deux personnes ont chut� sans aucun doute en raison des conditions climatiques particuli�rement difficiles", a indiqu� le capitaine de corvette Karine Foll, de la pr�fecture maritime, pr�cisant que le vent soufflait sur zone en rafales � plus de 70 noeuds (120 km/h).
Trois d�partements de l'Ouest de la France - Finist�re, Loire-Atlantique, Morbihan- �taient toujours plac�s en vigilance orange "vagues-submersion", selon le bulletin publi� � 4h15 par M�t�o-France.
Accalmie attendue � partir de 10 heures
La fin de l'�v�nement pour ces trois d�partements est pr�vue pour ce samedi 10 heures.�L'alerte a �t� lev�e, en revanche, pour les C�tes d'Armor et la Manche.
La vigilance orange "vent violent" a �t� lev�e sur les d�partements du Finist�re, des C�tes d'Armor, du Morbihan et de la Manche.
Dans la nuit "les valeurs maximales relev�es durant l'�pisode temp�tueux ont �t� de l'ordre de 140 � 150 km/h � la Pointe de Bretagne et de 130 � 135 km/h au nord d'une ligne Brest-Morlaix; la partie du Finist�re plus au sud de cette ligne, a connu des valeurs comprises entre 102 et 110 km/h", selon M�t�o France.
Le vent violent qui avait commenc� � souffler dans la journ�e, a plong� dans le noir pr�s de 100.000 foyers en Bretagne, selon Electricit� R�seau distribution France (ERDF). Quelque 50.000 foyers �taient concern�s � 19H30 dans le Finist�re, 40.000 dans les C�tes d'Armor, 4.000 dans le Morbihan et 1.000 en Ille-et-Vilaine. Dans la matin�e de samedi, ils �taient encore 90.000.
Le trafic ferroviaire entre Rennes et Brest a �t� interrompu, selon la SNCF, qui a estim� � quelque 2.000 le nombre de voyageurs touch�s par cette mesure de pr�caution.�
En outre, la quasi-totalit� des vols ont �t� annul�s dans les a�roports de Brest, Lorient et Quimper. Dans le Finist�re, particuli�rement touch�, les pompiers avaient men� en d�but de soir�e plus de 500 interventions, essentiellement en raison de la chute d'arbres.�
La pr�fecture maritime de l'Atlantique a signal� la perte de 70 conteneurs au large de la Bretagne par un navire de la compagnie danoise Maersk.�
40 cm d'eau � Quimperl�
Il y a avait en d�but de soir�e 40 cm d'eau sur les quais de Quimperl�, selon la pr�fecture du Finist�re, et de l�gers d�bordements � Morlaix et Landerneau. En outre, de nombreuses routes ont �t� coup�es en raison d'inondations. A Brest il est tomb� depuis le 15 d�cembre 636 mm de pluie, un record, selon M�t�o France.
A 10 heures, trois d�partements �taient encore en vigilance orange inondation : la Loire-Atlantique, le Morbihan et l'Ille-et-Vilaine.
Vents violents, trombes d'eau, gr�le, houle, inondations, coupures de courant, d�g�ts le long du littoral : dans les Finist�re, tout particuli�rement touch�, les intemp�ries ne laissent aucun r�pit aux habitants depuis le d�but de l'hiver.�
"Il pleut tous les jours, y en a marre", se plaignait dans l'apr�s-midi Armande, une retrait�e de 70 ans r�sidant � Guengat, petite commune de 1.600 habitants � quelques kilom�tres de Quimper.�
"On n'en sort pas du mauvais temps, �a bouleverse la vie", t�moignait de son c�t� Mich�le Lescoat, une habitante de Quimperl�, o� les crues de la La�ta se succ�dent. "On dirait qu'il n'y a pas de fin", constatait la sexag�naire, r�cemment rest�e plusieurs jours sans chauffage ni eau chaude � cause des inondations � r�p�tition.
"On a eu une succession de perturbations depuis le d�but de l'hiver avec des �pisodes un peu temp�tueux", reconna�t Fr�d�ric Nathan, pr�visionniste � M�t�o -France, soulignant l'intensit� de "Ulla", la perturbation actuelle. "Nous avons l�, sur la pointe de la Bretagne , la temp�te la plus forte depuis le d�but de l'hiver", assure-t-il, avant d'annoncer pour la semaine prochaine un certain "r�pit".
Partager
