TITRE: Recul de l'âge moyen de départ à la retraite en 2013 - 15 février 2014 - Le Nouvel Observateur
DATE: 2014-02-15
URL: http://tempsreel.nouvelobs.com/topnews/20140215.REU1166/recul-de-l-age-moyen-de-depart-a-la-retraite-en-2013.html
PRINCIPAL: 0
TEXT:
Actualité > TopNews > Recul de l'âge moyen de départ à la retraite en 2013
Recul de l'âge moyen de départ à la retraite en 2013
Publié le 15-02-2014 à 15h50
A+ A-
L'âge moyen de départ à la retraite est revenu à 62,1 ans en France en 2013, contre 62,2 en 2012, l'assouplissement des conditions de départ anticipé pour les carrières longues décidé par le gouvernement ayant pour conséquence d'interrompre la hausse constatée depuis la réforme de 2010. /Photo d'archives/REUTERS/Andrew Winning (c) Reuters
PARIS (Reuters) - L'âge moyen de départ à la retraite est revenu à 62,1 ans en France en 2013, contre 62,2 en 2012, l'assouplissement des conditions de départ anticipé pour les carrières longues décidé par le gouvernement ayant pour conséquence d'interrompre la hausse constatée depuis la réforme de 2010.
Par comparaison, il se situait à 61,9 ans en 2011, première année d'application de la réforme votée sous Nicolas Sarkozy, et 61,4 ans en 2010.
Selon les données publiées vendredi par la Cnav (Caisse nationale d'assurance-vieillesse), 847.484 salariés affiliés au régime général (secteur privé) sont partis en retraite l'an passé, soit 13,3% de plus qu'en 2012.
Les seules retraites anticipées longue carrière ont représenté 147.208 personnes, contre 86.975 un an plus tôt.
La réforme de 2010 avait relevé à 62 ans l'âge légal de départ à la retraite.
Dès son arrivée au pouvoir à l'été 2012, le gouvernement de François Hollande a assoupli par décret les conditions dans lesquelles les salariés ayant commencé à travailler jeunes peuvent faire valoir leurs droits à taux plein dès 60 ans.
Yann Le Guernigou, édité par Sophie Louet
Partager
