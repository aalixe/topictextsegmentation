TITRE: VIDEO. Rolling Stones : une heure de show pour 27 fans � Bondy - Concerts Musique - 15/02/2014 - leParisien.fr
DATE: 2014-02-15
URL: http://www.leparisien.fr/musique/concerts/video-rolling-stones-une-heure-de-show-pour-27-fans-a-bondy-15-02-2014-3593311.php
PRINCIPAL: 176727
TEXT:
VIDEO. Rolling Stones : une heure de show pour 27 fans � Bondy
Michel Valentin |                     Publi� le 15.02.2014, 06h00                                             | Mise � jour :                                                      07h28
Bondy (Seine-Saint-Denis), vendredi 14 f�vrier 2014. Mick Jagger, � son arriv�e, salue les fans qui se pressent aux portes du studio Planet Live.
1/2
R�agir
Le 14 f�vrier a beau �tre la Saint-Valentin, c�est plut�t un gros cadeau de No�l qu�ont fait vendredi les Rolling Stones � 27 de leurs fans les plus endurcis : un peu plus d�une heure de concert intime dans le studio Planet Live, � Bondy (Seine-Saint-Denis). Une ultime r�p�tition avant un concert la semaine prochaine � Abu Dhabi (Emirats arabes unis).
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
La compagne de Mick Jagger retrouv�e pendue
� J��tais l� depuis le matin, raconte un fan. Vers 14 h 30, un membre de l�entourage des Stones est sorti, et nous a tous �crit un num�ro au marqueur sur la main. A cette heure-l�, nous n��tions que 27. Tous sont rentr�s, pas ceux arriv�s plus tard. �
Classiques et raret�s
Bien que le technicien leur ait annonc� qu�ils n�auraient droit d�assister qu�� � trois ou quatre chansons �, c�est bien � la quasi-int�gralit� d�un vrai show qu�ont eu droit les heureux �lus, � la moyenne d��ge plut�t �lev�e, rarement au dessous de la quarantaine. � En plus, on nous a dit qu�on pouvait se laisser aller, s�enthousiasme encore le porteur du num�ro 10. On �tait tous sur une rambarde, et Mick Jagger nous a dit � un moment donn� de nous calmer, en rigolant, puis plus s�rieusement ! �
Entr�s en plein milieu de � Doom and Gloom �, nouveaut� extraite de la r�cente compilation � Grrr ! �, les fans, aux anges, ont eu droit aux classiques, tels � Miss You �, � Midnight Rambler �, � Paint it Black �, � Tumbling Dice �, � Honky Tonk Women � ou � Sympathy for the Devil �, mais aussi au beaucoup plus rare � Silver Train �, rarement jou� ces derni�res ann�es. � Keith Richards (NDLR : guitariste) a aussi chant� ses deux morceaux, Slipping Awayet Before They Make Me Run, mais c�est surtout Mick Taylor (NDLR : deuxi�me guitariste des Stones, entre 1969 et 1974) qui m�a impressionn�. On a beau le surnommer le Panda, parce qu�il a beaucoup grossi, son toucher reste toujours aussi fluide �, souligne un admirateur qui se frotte encore les yeux � la sortie.
� Qu�est-ce que vous voulez que je vous dise ? r�sume Jeanne, venue plusieurs jours d�affil�e avant de d�crocher le pr�cieux s�same. Comme on dit, it�s only rock�n�roll, mais ce sont les Stones les meilleurs, et il faut absolument aller les voir sur sc�ne ! �
VIDEO. Les Rolling Stones donnent le plus petit concert de leur histoire � Bondy
�
