TITRE: Berlin 2014: La Chine triomphe avec Black Coal, Thin Ice - Studio Cin� Live
DATE: 2014-02-15
URL: http://www.lexpress.fr/culture/cinema/berlin-2014-la-chine-triomphe-avec-black-coal-thin-ice_1324582.html
PRINCIPAL: 177353
TEXT:
Black Coal, Thin Ice, ours d'or au Festival de Berlin 2014.
DR
Notre chouchou (en m�me temps, vu la disette qualitative il n'y avait pas besoin de se greffer une nouvelle main pour compter sur ses doigts les bons films pr�sent�s cette ann�e en comp�tition) le chinois Black Coal, Thin Ice de Diao Yinan repart non seulement avec l'Ours d'or et celui de l'interpr�te masculin Liao Fan. Tout cela �tant on ne peut plus justifi� pour cette merveille de noirceur et cette r�ussite de mise en sc�ne. �
Evidemment nous nous r�jouissons du prix Alfred Bauer attribu� � Aimer, boire et chanter d' Alain Resnais , certes r�compense au parfum de pis aller mais d�finie comme �tant r�serv�e � un film qui ouvre de nouvelles perspectives cin�matographiques. Formule seyant comme un gant � Resnais qui repart aussi avec le Prix Fipresci attribu� par la presse internationale.
�
On ne dira pas de mal non plus � propos de l'ours d'argent qui r�compense la fantaisie baroque de Wes Anderson pour son The Grand Budapest Hotel.
Voil� c'est fini pour les bonnes nouvelles car il faut aussi annoncer le prix de la mise en sc�ne (rires) au soap interminable de trois heures de Richard Linklater , Boyhood r�alis� sur douze ans mais dont le film ne comporte pas plus de quatre valeurs tr�s banales de cadre. Mais Linklater a la carte et contre cela aucune lutte possible.�
La japonaise Haru Kuroki repart avec l'ours de la meilleure com�dienne pour sa prestation dans The Little House, le film d'antiquaire un rien poussi�reux de Yoji Yamada . �
Enfin le prix du sc�nario va � l'autrichien Dietrich Br�ggemann et son Kreuzweg, film ambigu mais ma�tris�, �ni�me �mule -en moins puissant tout de m�me- de Michael Haneke ou de Ulrich Seidl , cin�astes auxquels il est difficile de ne pas penser tout le long de la projection.�
Le bilan en deux mots: petite ann�e mais Ours d'or impeccable.�
