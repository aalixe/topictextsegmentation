TITRE: Trois jeunes soeurs meurent dans l'incendie de l'appartement familial - SudOuest.fr
DATE: 2014-02-15
URL: http://www.sudouest.fr/2014/02/15/trois-jeunes-soeurs-meurent-dans-l-incendie-de-l-appartement-familial-1463194-7.php
PRINCIPAL: 176267
TEXT:
�Trois soeurs ont trouv� la mort vendredi dans un incendie qui s'est d�clar� au huiti�me �tage d'un immeuble de Sevran (Seine-Saint-Denis), et huit personnes, dont cinq pompiers, ont �t� l�g�rement bless�es
Le quartier de la cit� basse, � Sevran. � Photo
AFP
Publicit�
T
rois s�urs, une jeune femme de 18 ans et deux fillettes de 8 et 11 ans, sont d�c�d�es dans l'incendie qui a ravag� un appartement, dans une tour du quartier Montceleux, rue de la Bo�tie, � Sevran (Seine-Saint-Denis) vendredi soir, rapporte le Parisien .� Leurs trois fr�res et s�urs, �galement pr�sents dans l'appartement familial au moment du sinistre, ont �t� intoxiqu�s et transport�s � l'h�pital mais leur jours ne sont pas en danger.
Publicit�
Les parents, qui n'�taient pas l� lorsque l'incendie a �clat�, ont reconnu les corps et ont d� �tre hospitalis�s en �tat de choc.
"Les six enfants sont de la m�me famille", a confirm�, � Sevran, le commandant des pompiers Gabriel Plus. Les trois survivants "ont �t� sauv�s par les pompiers", a-t-il ajout�, pr�cisant que le feu ne s'�tait pas propag� aux autres appartements de la tour.
"L'incendie a d�marr� vers 19h45 dans un appartement situ� au 8�me �tage d'un immeuble de 16 �tages", a indiqu� la pr�fecture de Seine-Saint-Denis, confirmant une information de BFMTV . La famille occupait cet appartement de 80m�.
Le feu a �t� ma�tris�. L'origine du sinistre n'est pas encore connue mais un "accident domestique" est probable, a pr�cis� cette source pr�fectorale. L'enqu�te a �t� confi�e � la police judiciaire.
Parmi les sept bless�s, l�g�rement intoxiqu�s, quatre sapeurs-pompiers souffrent de br�lures l�g�res.
Pr�s de 180 pompiers et une trentaine de camions sont intervenus au pied de cette tour du quartier de Montcelleux.�
�
