TITRE: Mars : le myst�re de la roche apparue pr�s d'Opportunity r�solu | Atlantico
DATE: 2014-02-15
URL: http://www.atlantico.fr/atlantico-light/mars-mystere-roche-apparue-pres-opportunity-resolu-983072.html
PRINCIPAL: 176327
TEXT:
Commentaires
Nos articles sont ouverts aux commentaires sur une p�riode de 7 jours.
Face � certains abus et d�rives, nous vous rappelons que cet espace a vocation � partager vos avis sur nos contenus et � d�battre mais en aucun cas � prof�rer des propos calomnieux, violents ou injurieux. Nous vous rappelons �galement que nous mod�rons ces commentaires et que nous pouvons �tre amen�s � bloquer les comptes qui contreviendraient de fa�on r�currente � nos conditions d'utilisation.
Par
LeditGaga
- 15/02/2014 - 17:22 - Signaler un abus ... � propos du vent sur Mars
La surface de Mars a une faible inertie thermique, ce qui signifie qu'elle chauffe rapidement quand le Soleil l'�claire. Sur Terre, le vent se cr�e l� ou il y a des changements brutaux d'inertie thermique, tel que de la mer vers la terre. Il n'y a pas de mers sur Mars, mais il y a des r�gions o� l'inertie thermique du sol change, cr�ant des vents matinaux et du soir apparent�s � la brise marine terrestre. Le projet Antares Mars Small-Scale Weather (MSW) a r�cemment d�couvert quelques faiblesses dans le mod�le climatique actuel d� au param�tre des sols. Ces faiblesses sont en train d'�tre corrig�es et devraient conduire � des �valuations plus pr�cises.
.
� basses latitudes, la circulation de Hadley domine et est presque le m�me processus qui, sur Terre, g�n�re les aliz�s. � hautes latitudes, une s�rie de r�gions de hautes et basses pressions, appel�es ondes de pression baroclines, domine le temps. Mars est plus s�che et plus froide que la Terre, en cons�quence la poussi�re soulev�e par ces vents tend � rester dans l'atmosph�re plus longtemps que sur Terre puisqu'il n'y a pas de pr�cipitations pour la rabattre (sauf la neige de CO2).
.
La suite sur Wikip�dia, "atmosph�re de Mars".
Par
- 15/02/2014 - 16:47 - Signaler un abus vent
Pas de vent sur Mars ??? et �a se dit journaliste ...
C ' est pourtant bien la m�me Nasa qui a film�
des tourbillons de sable sur Mars , pouss�s par le ... vent  !
En savoir plus
