TITRE: Lyon sans Gourcuff - Goal.com
DATE: 2014-02-15
URL: http://www.goal.com/fr/news/29/ligue-1/2014/02/15/4621656/lyon-sans-gourcuff
PRINCIPAL: 176654
TEXT:
0
15 f�vr. 2014 14:05:00
R�mi Garde, l'entra�neur de l'OL, a retenu un groupe de 18 joueurs pour le match de dimanche contre Ajaccio. Gourcuff (adducteurs) est forfait.
Comme il l'avait laiss� entendre cette semaine, R�mi Garde n'a pas convoqu� Yoann Gourcuff pour le match que doit jouer Lyon, dimanche, contre Ajaccio. Le joueur pointe � l'infirmerie en compagnie de Dabo et Benzia. Bahlouli et F�kir ont �t� appel�s en renfort.
Le groupe de Lyon : A.Lopes, Vercoutre � M.Lopes, B.Kon�, Bisevac, Umtiti, Bedimo � Gonalons, Tolisso, Mvuemba, Bahlouli, Fekir, Ferri, Grenier, G.Fofana � Briand, Lacazette, B.Gomis.
Relatifs
