TITRE: Tizen : 15 nouveaux partenaires, 0 smartphone
DATE: 2014-02-15
URL: http://www.journaldugeek.com/2014/02/15/tizen-15-nouveaux-partenaires-0-smartphone/
PRINCIPAL: 176486
TEXT:
Par Micka�l , 15 f�vrier 2014 � 08:23 Logiciels, Apps , T�l�phonie 1 avis
Sortira, sortira pas� ? On attend toujours des nouvelles du premier smartphone Tizen de Samsung, m�me si le constructeur a d�j� lanc� un appareil-photo, le NX300M, en Cor�e du Sud en fin d�ann�e derni�re.
C�est d�ailleurs durant le Sommet des d�veloppeurs Tizen, qui s�est tenu � S�oul mi-d�cembre, que l�on a appris que les promoteurs de la plateforme avait recrut� 36 partenaires dans leur association , parmi lesquels Konami, eBay, HERE (Nokia) ou encore Panasonic.
Plus on est de fous, plus on rit : tout ce petit monde est rejoint depuis peu par 15 nouveaux membres. On trouve des d�veloppeurs de jeu, des fabricants d�objets connect�s, des constructeurs de smartphones� Allons-y pour la liste : AccuWeather, Ixonos, Acrodea, Nomovok, Baidu, Piceasoft, CloudStreet, Red Bend Software, Cyberlightning, SoftBank Mobile, DynAgility, Sprint, Gamevil, ZTE et Inside Secure.
Il faut de tout pour faire un monde et Tizen semble avoir de s�rieux soutiens dans tous les secteurs qui comptent. Il ne manque plus gu�re qu�une vraie gamme de produits mais pour le moment, il va falloir prendre son mal en patience. Le MWC sera t-il l�occasion pour Samsung de marquer le coup ? Rien n�est moins s�r, puisque le constructeur y lancera le Galaxy S5.
�
