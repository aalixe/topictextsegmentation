TITRE: Les fr�missements de croissance remontent le moral de l'�lys�e - 15/02/2014 - La Nouvelle R�publique France-Monde
DATE: 2014-02-15
URL: http://www.lanouvellerepublique.fr/France-Monde/Actualite/24-Heures/n/Contenus/Articles/2014/02/15/Les-fremissements-de-croissance-remontent-le-moral-de-l-Elysee-1797391
PRINCIPAL: 175713
TEXT:
Les fr�missements de croissance remontent le moral de l'�lys�e
15/02/2014 05:35
(Dessin Deligne) - (Dessin Deligne)
Fran�ois Hollande a salu� ��la confiance retrouv�e par les acteurs �conomiques�� en France, apr�s les chiffres de la croissance publi�s vendredi.
La Bourse de Paris a boucl� une neuvi�me s�ance de hausse cons�cutive vendredi (+�0,63�%) et retrouv� un sommet qu'elle n'avait plus atteint depuis septembre�2008, gr�ce � des chiffres de croissance encourageants dans la zone euro.
Lors du Conseil des ministres vendredi matin, Fran�ois Hollande est ��revenu sur les chiffres de l'�conomie�� et ��a salu�la confiance retrouv�e par les acteurs �conomiques��, a rapport� Najat Vallaud-Belkacem en rendant compte des travaux du conseil.
�" La bonne surprise, c'est l'investissement "
��C'est un bon d�but, une bonne surprise, une croissance plus �quilibr�e��, commente pour sa part Frederik Ducrozet, �conomiste du Cr�dit Agricole, dans une allusion � la contribution en fin d'ann�e des trois moteurs cit�s par l'Insee (lire par ailleurs).
��La bonne surprise c'est clairement l'investissement��, estime le directeur des �tudes �conomiques de Natixis Asset Management, Philippe Waechter.
Sur France 2, le ministre de l'�conomie, Pierre Moscovici, a estim� que le chiffre finalement arr�t� par l'Insee, m�me meilleur que pr�vu, n�cessitait d'��aller plus loin�� pour ��faire reculer le ch�mage��.
Le gouvernement attend officiellement pour 2014 un taux de croissance de 0,9 %, m�me si Pierre�Moscovici a plusieurs fois dit qu'il jugeait possible de faire plus.
Selon l'Insee, le PIB devrait cro�tre de 0,2 % au premier trimestre comme au deuxi�me trimestre. ��On est d�sormais un peu au-dessus du produit int�rieur brut du d�but 2008, fait remarquer Philippe�Waechter, mais en termes de produit int�rieur brut par habitant, on n'a pas encore retrouv� le niveau d'avant crise, c'est l� pour moi la prochaine �tape��.
rep�res
>�La croissance �conomique en France a �t� de 0,3 % en 2013, une bonne surprise marqu�e en fin d'ann�e par un rebond esp�r� depuis longtemps de l'investissement et un soutien du commerce ext�rieur.
>�L'Institut national de la statistique (Insee) pr�voyait en d�cembre une croissance moyenne du produit int�rieur brut (PIB) pour 2013 de 0,2 % tandis que la pr�vision officielle du gouvernement �tait de 0,1 %.
>�Au quatri�me trimestre, la croissance a �t� de 0,3 %, selon la premi�re estimation rendue publique vendredi par l'Insee : l�g�rement inf�rieur � sa pr�vision de d�cembre (+�0,4�%), ce chiffre r�sulte d'une progression � la fois de la consommation, de l'investissement�� qui se faisait attendre depuis sept trimestres �, et du commerce ext�rieur.
>�La France suit le rythme de la zone euro, laquelle a �galement affich� une croissance de 0,3 % en fin d'ann�e.
