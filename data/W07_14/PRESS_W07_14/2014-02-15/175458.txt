TITRE: L2 / 24EME JOURNEE - Metz cale � nouveau, Dijon remonte
DATE: 2014-02-15
URL: http://www.football365.fr/france/ligue-2/metz-cale-a-nouveau-dijon-remonte-1103588.shtml
PRINCIPAL: 175454
TEXT:
L2 / 24EME JOURNEE - Publi� le 14/02/2014 � 22h18 -  Mis � jour le : 14/02/2014 � 22h36
14
Metz cale � nouveau, Dijon remonte
Auteur de sa premi�re victoire de l'ann�e la semaine derni�re, Metz a de nouveau cal� en ne faisant pas mieux que le match nul � N�mes (0-0). Large vainqueur du CA Bastia, Dijon fait la bonne affaire de la soir�e en prenant la deuxi�me place provisoire du classement.
Apr�s avoir sign� sa premi�re victoire de l�ann�e face � Caen la semaine derni�re, le FC Metz n�est pas parvenu � encha�ner et � capitaliser sur ce succ�s. En effet, les Messins ne ram�nent que le point du match nul de leur d�placement � N�mes ( 0-0 ). Ce r�sultat permet aux Crocos de prendre provisoirement quatre points d�avance sur Laval, qui joue ce samedi � Angers (qui peut se rapprocher � deux points du leader messin). Mais l��quipe qui fait la bonne affaire de cette soir�e, c�est Dijon. Les Dijonnais s�impose sans coup f�rir face au dernier du classement, le CA Bastia ( 3-0 ), et remonte tr�s provisoirement � la deuxi�me place du classement, � �galit� avec Angers et Lens qui recevra Le Havre en cl�ture lundi).
Niort revient sur Nancy
Juste derri�re la triplette Dijon-Angers-Lens, on retrouve d�sormais Nancy et Niort. Les Chamois, qui s�imposent � Clermont ( 0-1 ), reviennent � �galit� de points avec l�ASNL, qui a �t� accroch�e sur sa pelouse de Marcel-Picot ( 2-2 ), malgr� le sixi�me but en trois matchs de Jeff Louis. Avec sa courte victoire face � Arles-Avignon ( 1-0 ), acquise en tout d�but de rencontre gr�ce au douzi�me but de Mathieu Duhamel (9eme), Caen passe devant Tours, qui s�incline � Istres ( 1-0 ). Dernier match de la soir�e, Ch�teauroux s�impose face � Troyes ( 2-1 ). Ce qui permet � la Berrichonne de remonter � la 14eme place, avec six points d�avance sur le premier rel�gable, Laval. Si Brest-Auxerre se joue comme pr�vu ce samedi, les Brestois pourraient revenir � trois points du premier non-rel�gable, N�mes, avec un match encore � jouer.
L2 / 24EME JOURNEE
N�mes � Metz : 0-0
Nancy � Cr�teil : 2-2
Buts : Jeannot (45eme sp) et Louis (75eme) pour Nancy - Lafon (22eme) et Andriatsima (54eme) pour Cr�teil
Dijon � CA Bastia : 3-0
But : Philippoteaux (17eme), Tavares (71eme) et Babit (88eme) pour Dijon
Clermont � Niort : 0-1�� �
But : F.Martin (42eme) pour Niort
Istres � Tours : 1-0
But : N.Ke�ta (43eme) pour Istres
Caen � Arles-Avignon : 1-0
But : Duhamel (9eme) pour Caen
Ch�teauroux � Troyes : 2-1
But : Tait (18eme) et Kinkela (85eme) pour Ch�teauroux - Bonnefoi (90eme csc) pour Troyes
Samedi 15 f�vrier 2014
