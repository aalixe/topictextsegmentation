TITRE: Centrafrique. L�intervention fran�aise, plus longue que pr�vue - Paris Match
DATE: 2014-02-15
URL: http://www.parismatch.com/Actu/International/L-intervention-francaise-plus-longue-que-prevue-548576
PRINCIPAL: 0
TEXT:
L�intervention fran�aise, plus longue que pr�vue
Centrafrique
L�intervention fran�aise, plus longue que pr�vue
Des soldats fran�ais en Centrafrique. � REUTERS/Luc Gnago
Le 15 f�vrier 2014 | Mise � jour le 15 f�vrier 2014
C.R avec Reuters
Tweeter
Le ministre de la D�fense, Jean-Yves Le Drian, a annonc� samedi que l�intervention fran�aise en Centrafrique allait �tre plus longue que pr�vue.�
�Je pense que �a sera plus long que pr�vu parce que le niveau de haine et de violence est plus important que celui qu'on imaginait�, a d�clar� samedi Jean-Yves Le Drian sur France Inter. Le ministre de la D�fense a fait savoir que l�intervention en Centrafrique de l�arm�e fran�aise allait se poursuivre au-del� de d�lais annonc� au d�part. �Une op�ration militaire ne se d�cr�te pas comme du papier � musique, il faut s'adapter�, a-t-il fait savoir. Cette d�claration intervient alors que vendredi, le gouvernement fran�ais a annonc� l'envoi de 400 soldats suppl�mentaires pour renforcer les 1600 hommes de l'op�ration Sangaris et tenter de faire cesser les massacres dans ce pays.
La d�cision a �t� annonc�e par l'Elys�e � l'issue d'un conseil de d�fense restreint pr�sid� par Fran�ois Hollande, qui a appel� �la communaut� internationale � une solidarit� accrue�. �Le conseil a pris en consid�ration l'appel du secr�taire g�n�ral des Nations unies, M. Ban Ki-moon, � une mobilisation de la communaut� internationale, ainsi que la d�cision de l'Union europ�enne d'engager une op�ration militaire en Centrafrique�, lit-on dans le communiqu� de la pr�sidence fran�aise. Le chef de l'Etat a d�cid� d'y r�pondre en portant temporairement � 2000 les effectifs militaires fran�ais d�ploy�s en Centrafrique.
"Renforcer la mobilit� de la force Sangaris"
�Cet effort suppl�mentaire de 400 hommes comprend le d�ploiement anticip� de forces de combat et de gendarmes fran�ais qui participeront ensuite � l'op�ration militaire de l'Union Europ�enne d�s son d�ploiement�, pr�cise l'Elys�e. Selon le porte-parole de l'�tat-major des arm�es, le colonel Gilles Jaron, cette d�cision se traduira �dans les jours � venir� par l'engagement d'unit�s pr�positionn�es en Afrique.
Ces renforts comprendront des unit�s de combat d'infanterie, des h�licopt�res de transport et des moyens logistiques et de commandement, a-t-il pr�cis�. �Il s'agit de renforcer la mobilit� de la force Sangaris pour augmenter sa capacit� � s'engager en province et acc�l�rer le d�ploiement de la Misca (Mission africaine en Centrafrique)�, a ajout� le colonel Jaron. La France appelle par ailleurs l'UE � acc�l�rer le d�ploiement de la mission Eufor, y compris la Force de gendarmerie europ�enne, ajoute la pr�sidence fran�aise.
A lire
