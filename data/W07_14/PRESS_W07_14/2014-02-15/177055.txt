TITRE: Chez Volkswagen, ces ouvriers qui refusent de créer un syndicat, Actualités
DATE: 2014-02-15
URL: http://www.lesechos.fr/entreprises-secteurs/auto-transport/actu/0203319229780-chez-volkswagen-ces-ouvriers-qui-refusent-de-creer-un-syndicat-650756.php
PRINCIPAL: 177054
TEXT:
Chez Volkswagen, ces ouvriers qui refusent de cr�er un syndicat
15/02 | 17:39 | mis � jour � 18:06
Sous la pression des syndicats allemands, le constructeur avait ouvert la porte de son usine de Chattanooga, aux Etats-Unis, au puissant syndicat am�ricain UAW. Mais ses employ�s ont dit non.
1.338 employ�s de l�usine Volkswagen de Chattanooga, soit 89% du personnel, ont particip� au scrutin. - Sipa
C�est un revers majeur pour le mouvement syndical am�ricain�: les ouvriers d�une usine am�ricaine de Volkswagen, � Chattanooga dans le sud du pays, ont refus� de cr�er un syndicat au cours d�un vote tr�s surveill� aux Etats-Unis. Ils ont rejet� par 712 voix contre 626 la cr�ation d�une antenne de l�organisation United Auto Workers (UAW), selon des r�sultats publi�s dans la nuit de vendredi � samedi.
L�UAW n�a jusqu�� pr�sent jamais r�ussi � rassembler en syndicat les employ�s d�une usine d�un constructeur automobile �tranger aux Etats-Unis et un � oui � � l�usine Volkswagen de Chattanooga aurait constitu� une victoire historique. Les efforts du syndicat s��taient heurt�s � une forte opposition des �lus locaux, qui affirmaient qu�une victoire de l�UAW d�couragerait la cr�ation d�emplois dans le Tennessee. Ils avaient m�me menac� de suspendre les aides fiscales au secteur.
Au total, 1.338 employ�s de l�usine Volkswagen de Chattanooga, soit 89% du personnel, ont particip� au scrutin � bulletin secret organis� sur trois jours et surveill� par le National Labor Relations Board, l�autorit� f�d�rale en charge des relations sociales.
Soutien tacite de la direction
� Nous sommes scandalis�s par les ing�rences des hommes politiques et des groupes d�int�r�ts dans ce processus qui touche au droit �l�mentaire des employ�s � former un syndicat, nous sommes fiers que ces employ�s aient eu le courage de voter, malgr� les pressions �, a d�clar� un responsable de l�UAW, Dennis Williams. � Nous esp�rons que ceci va provoquer un d�bat sur le droit des travailleurs � s�organiser �, a-t-il ajout�.
Volkswagen avait ouvert la porte � l�UAW l�an dernier, sous la pression des syndicats allemands d�sireux de donner � l�usine du Tennessee un si�ge dans le comit� d�entreprise du groupe, ce qui aurait permis aux employ�s d�avoir leur mot � dire dans la gestion. En d�pit du soutien tacite de la direction de Volkswagen, convaincre des ouvriers du sud profond des Etats-Unis de payer des cotisations syndicales n�est pas une mince affaire, surtout depuis que l�UAW s�est vu accuser d��tre � l�origine de la ruine des constructeurs automobiles de Detroit.
Fin des espoirs d�expansion pour l�UAW
Ce vote intervient en outre � un moment o� le mouvement syndical am�ricain se bat pour sa survie. Le taux de syndicalisation aux Etats-Unis a chut� � son plus bas niveau depuis les ann�es 1930, atteignant tout juste les 11,3%. La seule organisation UAW a vu son nombre de membres plonger de 1,5 million en 1979 � 383.000 aujourd�hui.
La direction de Volkswagen � semblait neutre, voire positive � � l��gard de l�UAW , et pourtant le syndicat � a �chou� �, a comment� Jack Nerad, un expert du Kelley Blue Book, un cabinet sp�cialis� du secteur connu pour sa publication similaire � l�Argus automobile. � C�est un grave revers � pour le syndicat.
� Les tentatives de l�UAW de s�implanter dans d�autres usines automobiles non-syndiqu�es aux Etats-Unis ont bien peu de chance d��tre accueillies favorablement par les autres constructeurs automobiles �, contrairement � Volkswagen, a-t-il ajout�. � Cela pourrait bien marquer la fin des espoirs du syndicat de gagner du terrain dans ces usines du sud � des Etats-Unis.
Le pr�sident de l�UAW, Bob King, a indiqu� que le syndicat allait �valuer ses options dans les prochains jours, notamment � la lumi�re des menaces des �lus locaux.
Vers un comit� d�entreprise
Le directeur de l�usine de Chattanooga, Frank Fischer, n�a pas exclu la cr�ation d�un comit� d�entreprise. � Nos employ�s n�ont pas pris la d�cision qu�ils refusaient un comit� d�entreprise �, a-t-il soulign�. Un comit� d�entreprise � l�allemande se rapproche d�un syndicat � l�am�ricaine dans le sens o� ses membres aident � repr�senter les employ�s, mais c�est une organisation maison, non une structure ext�rieure comme le syndicat.
L�usine du Tennessee est la seule usine de Volkswagen en dehors de la Chine � ne pas avoir de comit� d�entreprise local et � ce titre, ne si�ge pas au comit� d�entreprise mondial du constructeur allemand.
Volkswagen, qui y fabrique notamment la Passat, cherche � se d�velopper aux Etats-Unis dans le cadre de ses efforts pour devenir le premier constructeur automobile mondial d�ici 2018.
source AFP
