TITRE: Temp�te Ulla. 2 000 voyageurs et 40 trains touch�s par la temp�te
DATE: 2014-02-15
URL: http://www.ouest-france.fr/tempete-ulla-2-000-voyageurs-et-40-trains-touches-par-la-tempete-1933759
PRINCIPAL: 177006
TEXT:
Temp�te Ulla. 2 000 voyageurs et 40 trains touch�s par la temp�te
Rennes -
Achetez votre journal num�rique
Le trafic ferroviaire est revenu � la normale ce samedi apr�s-midi en Bretagne. Mais la nuit a �t� longue pour pr�s de 2 000 passagers.
Directeur de crise au PC de la SNCF, Emmanuel Clochet a tr�s peu dormi la nuit derni�re. C'est � lui et � ses �quipes qu'est revenue la t�che de g�rer les cons�quences de la temp�te Ulla sur le trafic ferroviaire breton.
En tout, une quarantaine de trains ont vu leur circulation perturb�e ou stopp�e, hier en fin de journ�e et pr�s de 2 000 voyageurs ont �t� pris en charge � Rennes, Saint-Brieuc et Morlaix. Dans des TGV reconditionn�s en train-lit ou dans des salles municipales.
