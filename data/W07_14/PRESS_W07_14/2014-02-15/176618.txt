TITRE: Deux-S�vres : 100 kilos de drogue dans un camion accident� - M6info by MSN
DATE: 2014-02-15
URL: http://news.fr.msn.com/m6-actualite/faits-divers/deux-sevres-100-kilos-de-drogue-dans-un-camion-accidente
PRINCIPAL: 176615
TEXT:
INFO M6 - De "gros ca�ds" interpell�s dans une s�rie de cambriolages
Tout commence mercredi matin, lorsqu'un camion-benne se couche au travers de la chauss�e, sur l'A10, dans le sens Paris-Bordeaux. L'accident provoque un important bouchon, puis la coupure de la circulation, le temps pour une grue de d�gager le poids lourd. Le chauffeur, de nationalit� espagnole, n'a pas �t� bless� dans l'accident. Mais son comportement �trange interpelle les gendarmes. Il l'attribue au choc. L'homme est tout de m�me hospitalis� par pr�caution pour des examens. Des analyses qui vont r�v�ler qu'il conduisait sous l'emprise de stup�fiants.
Mais ce n'est pas tout : au cours du d�gagement du camion, des sachets de poudre blanche ont �t� d�couverts dans la cargaison de r�sidu de pneus recycl�s. Une fouille a alors permis de trouver environ 100 kilos de drogue de synth�se. Une partie a �t� conserv�e � des fins d'analyse et pour les besoins de l'enqu�te, l'autre doit �tre incin�r�e.
De son c�t�, le chauffeur, qui nie �tre au courant d'un trafic, a �t� plac� en garde � vue.
Lire aussi :
