TITRE: Bourse de Wall Street : Wall Street finit en hausse, peu influenc�e par les stats
DATE: 2014-02-15
URL: http://www.zonebourse.com/actualite-bourse/Wall-Street-finit-en-hausse-peu-influencee-par-les-stats--17952177/
PRINCIPAL: 175414
TEXT:
Bourse de Wall Street : Wall Street finit en hausse, peu influenc�e par les stats
14/02/2014 | 23:36
* Le Dow a gagn� 0,79%, le S&P 500 0,48%, le Nasdaq 0,08%%
* Sur la semaine, le Dow et le S&P gagnent 2,3%, le Nasdaq 2,9%
* Des statistiques impact�es par les intemp�ries ignor�es   (Actualis� avec des d�tails et des pr�cisions)
par Chuck Mikolajczak et Ryan Vlastelica
Wall Street et ses trois grands indices ont inscrit vendredi une deuxi�me semaine dans le vert d'affil�e, les investisseurs ne s'en laissant pas compter par des statistiques qui ont subi l'impact n�gatif des intemp�ries.
C'est le cas de la production manufacturi�re qui a subi en janvier une baisse inattendue de 0,8%, la plus forte depuis mai 2009, � cause de la vague de froid qui a touch� une bonne partie des Etats-Unis. (voir )
En revanche, les prix � l'exportation ont augment� pour le troisi�me mois cons�cutif en janvier, un signal favorable pour la demande et pour les entreprises am�ricaines.
Derni�re statistique du jour, le moral des m�nages am�ricains est rest� stable durant les premiers jours de f�vrier, l'optimisme sur les perspectives futures �tant temp�r� par les pr�occupations li�es � leur situation financi�re actuelle, suivant les premiers r�sultats de l'enqu�te mensuelle Thomson Reuters-Universit� du Michigan.
L'indice Dow Jones a gagn� 126,80 points (0,79%) � 16.154,39. Le S&P-500, plus large, a pris 8,80 points, soit 0,48%, � 1.838,63. Le Nasdaq Composite a avanc� de son c�t� de 3,35 points (0,08%) � 4.244,03.
Sur l'ensemble de la semaine, le Dow a gagn� 2,3%, le S&P-500 2,3% �galement et le Nasdaq Composite 2,9%.
Le Nasdaq a termin� � son niveau le plus �lev� depuis 2000 et neuf des 10 grands secteurs de l'indice S&P-500 ont fini en hausse. Le seul secteur a �tre dans le rouge, celui des t�l�coms  est consid�r� comme d�fensif. Celui de l'�nergie , qui suit de pr�s la croissance �conomique, a inscrit le gain le plus appr�ciable, de 1,5%.
Wall Street est � port�e de ses records et les investisseurs veulent des preuves que la valorisation du march� est justifi�e. "Le march� n'est pas particuli�rement cher mais je ne le trouve pas non plus sp�cialement bon march�", commente Jeff Morris (Standard Life Investments). "Le march� va avoir du mal � faire une perc�e convaincante � partir de ses nouvelles bases".
Quant aux r�sultats de soci�t�s, sur les 398 valeurs du S&P-500 qui ont publi� jusqu'� pr�sent, 66,3% ont d�pass� le consensus, soit plus que la moyenne historique de 63%, selon Thomson Reuters. Plus de 64% ont d�pass� le consensus pour le chiffre d'affaires, au-dessus de la moyenne de long terme de 61%.
Aux valeurs, le groupe de pr�t-�-porter masculin Jos. A Bank Clothiers a annonc� l'achat du distributeur Eddie Bauer  pour 825 millions de dollars au fonds de capital investissement Golden Gate Capital. L'action Jos A. Bank a gagn� 0,36%. Men's Wearhouse, qui avait lanc� une offre sur Jos. A Bank, a recul� de 5,3%.
American International Group a c�d� 1,2%. L'assureur a pourtant publi� jeudi un b�n�fice meilleur que pr�vu au titre du quatri�me trimestre et a relev� son dividende.
Weight Watchers International a plong� de pr�s de 28%, le groupe de di�t�tique ayant projet� un b�n�fice annuel largement inf�rieur aux attentes.
A l'inverse, LCA-Vision a grimp� de 28%, la soci�t� sp�cialis�e dans les lasers de correction de la vue, ayant accept� d'�tre rachet�e par PhotoMedex. L'action de cette derni�re a gagn� 1,3%.     (Wilfrid Exbrayat pour le service fran�ais)
