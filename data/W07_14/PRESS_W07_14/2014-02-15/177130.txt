TITRE: France/Monde | Les sympathisants UMP veulent une primaire avec Sarkozy
DATE: 2014-02-15
URL: http://www.ledauphine.com/france-monde/2014/02/15/les-sympathisants-ump-veulent-une-primaire-avec-sarkozy
PRINCIPAL: 0
TEXT:
SONDAGE Les sympathisants UMP veulent une primaire avec Sarkozy
Nicolas Sarkozy lors d�??un meeting de soutien à Nathalie Kosciusko-Morizet, le 10 février.  AFP
73% des sympathisants UMP souhaitent que Nicolas Sarkozy participe à la primaire de son parti s�??il souhaite être candidat à la présidentielle de 2017, selon un sondage CSA publié samedi par Nice-Matin.
68% des mêmes sympathisants souhaitent que Nicolas Sarkozy soit le candidat de l�??UMP en 2017, alors qu�??ils ne sont que 9% à vouloir Alain Juppé, tout comme François Fillon.
Selon ce sondage, 40% des Français disent regretter Nicolas Sarkozy comme président, 54% ne le regrettant pas.
Le sondage a été réalisé par internet du 11 au 13 février auprès d�??un échantillon représentatif de 1.000 personnes. Le nombre de sympathisants UMP au sein de cet échantillon et la marge d�??erreur ne sont pas précisées dans la fiche technique accompagnant le sondage.
Par AFP | Publié le 15/02/2014 à 15:55
Vos commentaires
Connectez-vous pour laisser un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
