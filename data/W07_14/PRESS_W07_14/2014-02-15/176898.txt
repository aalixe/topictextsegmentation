TITRE: Ecotaxe : 250 Bonnets rouges manifestent en Bretagne - France - Actualit�s sur orange.fr
DATE: 2014-02-15
URL: http://actu.orange.fr/france/ecotaxe-250-bonnets-rouges-manifestent-en-bretagne-afp-s_2832918.html
PRINCIPAL: 176894
TEXT:
15/02/2014 � 16:05
Ecotaxe : 250 Bonnets rouges manifestent en Bretagne
Une manifestation pr�s du portail �cotaxe de Brec'h, dans le Morbihan, a rassembl� environ 250 Bonnets rouges.
r�agir 33 �
photo : JEAN-SEBASTIEN EVRARD, AFP
Environ 250 Bonnets rouges, selon la gendarmerie, ont manifest� samedi apr�s-midi � proximit� du portique �cotaxe de Brec'h, entre Vannes et Lorient (Morbihan). Ils ont commenc� en d�but d'apr�s-midi � lancer des fus�es de d�tresse et des oeufs sur les forces de l'ordre emp�chant l'acc�s au portique. Les gendarmes ont lanc� des grenades lacrymog�nes.
La circulation a �t� interrompue pr�ventivement sur la Nationale 164.
Les Bonnets rouges avaient annonc� depuis quelque temps cette manifestation qu'ils voulaient "avant tout pacifique", selon l'appel post� sur leur page Facebook. Ils avaient pr�vu de se rendre � partir de midi � proximit� du portique, o� �taient pr�vu un pique-nique et des prises de parole. Mais les forces de l'ordre ont pris les devants en interrompant la circulation sur la 2X2 voies d�s 10H00 et en mettant en place des d�viations, bousculant l'organisation pr�vue.
voir (0)��
