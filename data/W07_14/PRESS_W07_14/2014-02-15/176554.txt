TITRE: Italie: dernière ligne droite avant le nouveau gouvernement - 16 février 2014 - Le Nouvel Observateur
DATE: 2014-02-15
URL: http://tempsreel.nouvelobs.com/topnews/20140215.AFP0162/italie-le-president-poursuit-ses-consultations-pour-former-le-gouvernement.html
PRINCIPAL: 0
TEXT:
Actualité > Monde > Italie: dernière ligne droite avant le nouveau gouvernement
Italie: dernière ligne droite avant le nouveau gouvernement
Publié le 15-02-2014 à 07h31
Mis à jour le 16-02-2014 à 10h35
A+ A-
Le président italien Giorgio Napolitano devait poursuivre samedi ses consultations en vue de désigner un nouveau chef de gouvernement, au lendemain de la démission d'Enrico Letta, poussé vers la sortie par son rival et probable successeur, le maire de Florence Matteo Renzi. (c) Afp
Rome (AFP) - Le président italien Giorgio Napolitano a achevé samedi soir ses consultations à Rome en vue de confier à Matteo Renzi la formation du nouveau gouvernement, qui verra Silvio Berlusconi rester dans l'opposition.
"Nous restons dans une opposition constructive", a confirmé devant la presse l'ancien chef du gouvernement, à l'issue de l'entrevue.
M. Berlusconi a également assuré avoir fait part au chef de l'Etat de "sa préoccupation et stupéfaction pour cette crise opaque qui s'est ouverte en dehors du Parlement et au sein d'un seul parti".
Le Cavaliere visait l'inhabituel vote de défiance par lequel la direction du Parti démocrate (PD), sous la pression de son chef Matteo Renzi, le jeune maire de Florence de 39 ans, a fait chuter jeudi soir le gouvernement d'Enrico Letta, pourtant numéro deux du parti jusqu'à il y a quelques mois.
En revanche, l'ex-lieutenant de Silvio Berlusconi, Angelino Alfano, vice-Premier ministre sortant et chef du parti Nouveau centre droit (NCD), dont les 30 sénateurs sont nécessaires pour assurer la majorité gouvernementale de la coalition gauche-droite, s'est déclaré prêt à participer à un éventuel gouvernement Renzi, à deux conditions.
D'abord, il faut que "l'axe de l'actuelle coalition +anormale+ droite-gauche ne se déplace pas vers la gauche". "Nous dirions non à un tel gouvernement", a assuré M. Alfano.
La deuxième condition est de "faire les choses en grand" pour sortir le pays de la crise, en ciblant "la classe moyenne". "Mais pour faire les choses en grand, il faut du temps. On ne peut pas conclure un accord (avec Matteo Renzi, ndlr) en 48 heures et une fois l'accord conclu il faudra l'inscrire noir sur blanc", a averti M. Alfano, qui se méfie de toute évidence du chef du PD.
Les dirigeants du PD ont fermé le bal dans la soirée, confirmant qu'ils avaient proposé à M. Napolitano "de mettre au service du pays toutes les forces dont ils disposent, à commencer par la personne du secrétaire Matteo Renzi".
Pas moins d'une quinzaine de délégations ont défilé dans la journée chez le chef de l'Etat. Un véritable marathon pour le président Napolitano, âgé de 88 ans, qui, lors d'une brève allocution devant la presse, a qualifié ces rencontres d'"intenses", mais n'a pas précisé quand il dévoilerait le nom du successeur de M. Letta, sans doute dimanche ou lundi.
- "Totoministri" -
Silvio Berlusconi, toujours aux commandes de son parti Forza Italia, après sa destitution du Sénat consécutive à sa condamnation pour fraude fiscale , s'est refait une légitimité politique avec sa visite à M. Napolitano.
majorité Accueilli par les sifflets d'une poignée de manifestants, il s'offrait ainsi une sorte de revanche : il avait franchi la même porte dans l'autre sens, lorsque, selon lui, le chef de l'Etat l'avait poussé à démissionner en novembre 2011 pour céder la place à Mario Monti.
Le futur chef du gouvernement devra composer avec la formation actuelle du Parlement, qui n'avait pas dégagé de majorité claire au printemps dernier, pour former un gouvernement de coalition gauche-droite, comme son prédécesseur.
La Repubblica avance le chiffre de 18 ministres, "dont la moitié de femmes". L'économiste Lucrezia Reichlin pourrait remplacer Fabrizio Saccomani à l' Economie , un ministère scruté de près par les partenaires européens de la troisième économie de la zone euro.
D'autant que, malgré un rebond de 0,1% du PIB au quatrième trimestre 2013, après deux ans de récession économique, l' Italie est loin d'être tirée d'affaire, avec une dette publique s'élevant à 127% de son PIB et un chômage à 13%.
"Emploi et fisc" devront dans tous les cas être les deux priorités du nouvel exécutif, selon le Corriere della Sera.
Au jeu du "toto-ministri" (néologisme dont les Italiens ont le secret et qui se réfère aux paris sur les matches de foot), un autre nom circule: celui de Luca Cordero di Montezemolo, président de Ferrari et chantre du "Made in Italy". Emma Bonino, radicale, ancienne commissaire européenne, pourrait conserver son portefeuille aux Affaires étrangères.
Partager
