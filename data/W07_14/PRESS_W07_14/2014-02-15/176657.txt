TITRE: OL: Sans Gourcuff contre Ajaccio - Football - Sports.fr
DATE: 2014-02-15
URL: http://www.sports.fr/football/ligue-1/scans/ol-sans-gourcuff-contre-ajaccio-1010636/
PRINCIPAL: 176654
TEXT:
15 février 2014 � 14h00
Mis à jour le
15 février 2014 � 14h46
Pour la réception de l'AC Ajaccio dimanche (14h) au stade de Gerland, dans le cadre de la 25e journée de Ligue 1, l'Olympique Lyonnais sera privé de Yoann Gourcuff (adducteurs), Mouhamadou Dabo et Yassine Benzia.
L'ancien milieu de terrain de l' AC Milan , l'ex-latéral de l'AS Saint-Étienne et l'attaquant de l'équipe de France espoirs sont tous les trois en phase de reprise après diverses blessures. Gueïda Fofana et Milan Bisevac , absents face à Lens jeudi en Coupe de France, sont par contre de retour dans le groupe de Rémi Garde.
Le groupe de l'OL : A. Lopes, R. Vercoutre - Bedimo, Bisevac, Umtiti, B. Koné, M. Lopes - Fofana, Tolisso, Mvuemba, Fekir, Ferri, Gonalons, Bahlouli, Grenier - Lacazette, Briand, Gomis.
