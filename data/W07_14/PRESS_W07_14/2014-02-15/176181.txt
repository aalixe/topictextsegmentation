TITRE: JO: l'or pour l'Autrichienne Anna Fenninger en Super G dames | Site mobile Le Point
DATE: 2014-02-15
URL: http://www.lepoint.fr/fil-info-reuters/jo-l-or-pour-l-autrichienne-anna-fenninger-en-super-g-dames-15-02-2014-1792028_240.php
PRINCIPAL: 176172
TEXT:
15/02/14 à 10h26
JO: l'or pour l'Autrichienne Anna Fenninger en Super G dames
ROSA KHOUTOR, Russie ( Reuters ) - L'Autrichienne Anna Fenninger a maintenu la domination de son pays en Super G dames lors de jeux olympiques, en remportant samedi la médaille d'or de la spécialité aux J.O de Sotchi.
L'Allemande Maria Höfl-Riesch, victorieuse lundi dans le super combiné, obtient l'argent et le bronze revient à une autre Autrichienne, Nicole Hosp, déjà médaille d'argent dans le super combiné.
Les trois derniers titres olympiques de super G dames, en incluant celui conquis samedi, ont été remportés par des Autrichiennes.
Le début de l'épreuve, samedi matin, a tourné à la bérézina pour les concurrentes et seule une des huit premières en lice a terminé ce Super G à Rosa Khoutor.
La Française Marie Marchand-Arvier a ainsi manqué une porte, et l'épreuve a été fatale par la suite à la Suissesse Dominique Gisin, la 11e à se lancer.
Alan Baldwin; Eric Faye pour le service français
