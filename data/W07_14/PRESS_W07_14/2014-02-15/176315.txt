TITRE: LIGA / REAL MADRID  - Real Madrid : Benzema vers la prolongation
DATE: 2014-02-15
URL: http://www.mercato365.com/etranger/espagne/real-madrid-benzema-vers-prolongation-1103700.shtml
PRINCIPAL: 0
TEXT:
LIGA / REAL MADRID - Publi� le 15/02/2014 � 11h40
20
Real Madrid : Benzema vers la prolongation
Selon Marca, Karim Benzema, l'attaquant du Real Madrid, aurait �t� approch� pour prolonger son engagement qui expire en juin 2015. Des n�gociations auraient �t� lanc�es avec l'entourage de l'international fran�ais.
Auteur de treize r�lisations cette saison en Liga, Karim Benzema (26 ans), l�attaquant du Real Madrid , aurait �t� approch� pour prolonger son engagement qui expire en juin 2015. Des n�gociations auraient �t� lanc�es avec l�entourage de l�international fran�ais qui pourrait parapher un nouveau bail de trois (2018) ou cinq ans (2020). Avec le d�part de l�Argentin Gonzalo Higuain � Naples, l�ancien Lyonnais a su prendre ses responsabilit�s aux c�t�s de Cristiano Ronaldo et de Gareth Bale. M�me si les noms de Diego Costa (Atletico Madrid), de Luis Suarez (Liverpool) ou encore de Radamel Falcao (Monaco) continuent de circuler du c�t� de La Maison Blanche.
Toujours soutenu par Zin�dine Zidane et Florentino Perez, Karim Benzema est aujourd�hui un homme de base du dispositif de Carlo Ancelotti, le successeur de Jos� Mourinho sur le banc des Merengue. Selon Marca, qui en fait sa Une ce samedi, les Madril�nes souhaiteraient ainsi blinder le contrat du Tricolore, qui est notamment suivi par le Paris Saint-Germain . Auteur d�un doubl� contre Villarreal (4-2, 23eme journ�e de Liga), le Fran�ais a �gal� samedi dernier, avec 104 buts, le bilan madril�ne du Br�silien Ronaldo, �poque � Galactiques �.
