TITRE: Retour � la normale sur la route des stations alpines - france - Directmatin.fr
DATE: 2014-02-15
URL: http://www.directmatin.fr/france/2014-02-15/premieres-difficultes-sur-la-route-des-stations-alpines-658067
PRINCIPAL: 176610
TEXT:
Retour � la normale sur la route des stations alpines
Photo
ci-dessus
V�hicules sur la route des vacances le 16 f�vrier 2013 sur l'A43 entre Chamb�ry et Albertville
[Jean-Pierre Clatot / AFP/Archives]
Le trafic revenait � la normale samedi apr�s-midi sur la route des stations alpines, apr�s un pic de bouchon de 90 km vers midi, pour cette premi�re journ�e de vacances class�e orange en Rh�ne-Alpes et en Bourgogne dans le sens des d�parts.
"Le trafic est toujours charg�, mais il n'y a plus de grosse perturbation, �a roule correctement", a indiqu� � l'AFP un responsable du Centre r�gional d'information et de coordination routi�res (CRICR) Rh�ne-Alpes et Auvergne.
Les principales perturbations se sont concentr�es sur l'A43 entre Lyon et Chamb�ry, o� le trafic �tait toujours dense dans l'apr�s-midi entre St-Genix-sur-Guiers et le tunnel de Dullin, avec 4 km de bouchons � 16H00, selon le CRICR.
Dans la r�gion grenobloise, la circulation �tait encore bouch�e sur 1 km sur l'A480, entre Seyssins et Pont de Claix.
Il y avait en outre 3 km de bouchons entre Pont de Claix et Vizille (Is�re) sur la RN85, en direction de l'Alpe d'Huez, des Deux-Alpes et des stations des Hautes-Alpes.
Class�e orange dans le sens des d�parts en Bourgogne et en Rh�ne-Alpes, la journ�e de samedi �tait pr�vue comme la plus charg�e du week-end sur les axes menant aux massifs montagneux et sur les routes d�acc�s aux stations de sports d�hiver, selon Bison Fut�.
La circulation devait redevenir compl�tement fluide sur les axes des r�gions concern�es � partir de 18H00.
Dimanche, la circulation devrait rester normale tout au long de la journ�e, � l�exception de quelques retenues enregistr�es en fin d�apr�s-midi aux abords des grands centres urbains, notamment au sud de l'agglom�ration grenobloise, ainsi qu�� Lyon au passage du tunnel de Fourvi�re, selon la m�me source.
Les vacances scolaires d'hiver ont d�but� samedi et s'�talent jusqu'au 17 mars selon les trois zones, la premi�re concern�e �tant celle des acad�mies de Bordeaux, Cr�teil, Paris et Versailles.
�
