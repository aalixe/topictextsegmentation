TITRE: � Julie Gayet porte plainte pour : mise en danger de la vie d�autrui  Stars Actu
DATE: 2014-02-15
URL: http://www.stars-actu.fr/2014/02/julie-gayet-porte-plainte-pour-mise-en-danger-de-la-vie-dautrui/
PRINCIPAL: 175975
TEXT:
Tweeter
� Bernard Barbereau / Mercredi films
Parce qu�elle se sent d�sormais harcel�e par les paparazzi, la com�dienne fran�aise Julie Gayet aurait, selon des informations parues dans ��Le Monde��, d�pos� plainte pour ��mise en danger de la vie d�autrui�� .
Une information confirm�e � l�AFP par une source judiciaire qui pr�cise que le parquet de Paris a confi� l�enqu�te � la brigade de r�pression de la d�linquance aux personnes (BRDP).
L�occasion de rappeler que de son c�t� le parquet de Nanterre a lanc� une autre enqu�te pour ��atteinte � l�intimit� de la vie priv�e�� et ce apr�s la plainte d�pos�e au p�nal par l�actrice.
En cause la parution le 17  janvier dernier dans le magazine ��Closer�� d�une photo  la montrant au volant de sa voiture.
Une photo parue une semaine seulement apr�s que le m�me magazine n��voque une liaison suppos�e de l�actrice avec le Pr�sident fran�ais.
Quand � Fran�ois Hollande, il a finalement renonc� � poursuivre l�hebdomadaire.
A lire aussi
