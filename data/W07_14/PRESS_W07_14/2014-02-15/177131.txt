TITRE: Tempête au Royaume-Uni et en Bretagne: deux morts - 16 février 2014 - Le Nouvel Observateur
DATE: 2014-02-15
URL: http://tempsreel.nouvelobs.com/topnews/20140215.AFP0182/tempete-au-royaume-uni-deux-morts-140-000-foyers-prives-d-electricite.html
PRINCIPAL: 0
TEXT:
Actualité > TopNews > Tempête au Royaume-Uni et en Bretagne: deux morts
Tempête au Royaume-Uni et en Bretagne: deux morts
Publié le 15-02-2014 à 13h20
Mis à jour le 16-02-2014 à 09h01
A+ A-
Deux morts, plus de 140.000 foyers privés d'électricité, fortes perturbations dans les transports ferroviaires et routiers, des centaines d'arbres déracinés, restaurant évacué, glissement de terrain: la dernière tempête en date à s'abattre cet hiver sur le Royaume-Uni provoquait le chaos samedi. (c) Afp
Londres (AFP) - Deux morts, l'un à Londres, l'autre sur un bateau en mer, 115.000 foyers privés d'électricité entre le Royaume-Uni et la France, fortes perturbations dans les transports, glissements de terrain: la tempête Ulla a frappé durement samedi.
Une femme a été tuée et un homme blessé vendredi soir à Londres par l'effondrement partiel d'un immeuble sur leur véhicule.
Un octogénaire, qui effectuait une croisière, a péri quand un hublot de son paquebot a volé en éclats sous la force des vagues, dans la Manche, au large du Finistère. Plusieurs autres personnes parmi les 735 passagers du Marco Polo, essentiellement des Britanniques, ont aussi été blessées. Le paquebot, attendu dimanche à Tilbury, dans le sud-est de l'Angleterre, revient d'une croisière dans les Caraïbes.
Une trentaine de personnes ont également dû être évacuées vendredi soir d'un restaurant du front de mer à Milford on Sea (sud), après l'explosion de fenêtres causée par des galets transportés par des vents extrêmement violents. L'eau s'est ensuite engouffrée dans le restaurant où des couples célébraient la Saint-Valentin, nécessitant l'intervention des services de secours et de l'armée.
Deux randonneurs portés disparus en Ecosse ont finalement été retrouvés sains et saufs samedi.
Dans l'après-midi, quelque 85.000 foyers étaient encore privés d'électricité au Royaume-Uni, contre 140.000 samedi matin, selon la compagnie Energy Networks Association (ENA).
En France, environ 30.000 restaient sans courant samedi soir en Bretagne, selon Electricité Réseau Distribution France (ERDF), qui avait réussi à le rétablir chez quelque 85.000 usagers initialement plongés eux aussi dans le noir.
La ligne de chemin de fer Brest-Quimper devait rester fermée tout le week-end, des dizaines d'arbres étant tombés sur la voie.
En Grande-Bretagne, les transports ferroviaires et routiers ont également été fortement perturbés à cause d'arbres déracinés et de glissements de terrain, même si la situation s'améliorait dans la journée.
"La nuit a été violente, avec la chute de plus de 120 arbres, bloquant des dizaines de voies dans le sud de l'Angleterre", a déclaré un porte-parole du réseau ferroviaire Network Rail.
A l'aéroport international londonien d'Heathrow, 51 vols, essentiellement des court-courriers, ont été annulés sur un total d'environ 1.300 pour la journée.
Le Premier ministre David Cameron, toujours en déplacement dans les zones inondées, a prévenu que le niveau de l'eau allait encore monter, alors qu'une partie du Royaume-Uni a déjà les pieds dans l'eau. La Tamise pourrait atteindre localement ce week-end des niveaux record.
Les intempéries "frappent de façon dramatique les communautés les unes après les autres, semaine après semaine, (...) et nous ferons tout ce qui est en notre pouvoir pour que les gens s'en remettent", a déclaré à la presse M. Cameron dans le Surrey (sud-est). Interrogé une nouvelle fois sur le retard pris par les autorités pour intervenir, il a assuré qu'il allait "tirer les leçons" de la crise le moment venu.
Des rafales de vent, jusqu'à 130 km/h, et de fortes pluies se poursuivaient samedi, avec des précipitations pouvant atteindre jusqu'à 40 mm dans le sud-ouest de l' Angleterre et le sud du Pays de Galles. Une accalmie est prévue dimanche.
Cette tempête est la dernière en date d'une longue série à s'abattre ces dernières semaines sur le Royaume-Uni, qui connaît l'un de ses hivers les plus pluvieux. Deux autres personnes avaient été tuées plus tôt cette semaine lors d'une précédente vague d'intempéries.
Partager
