TITRE: Un homme poss�dant 3.000 images p�dopornographiques condamn� � 2 ans de prison - France - Actualit�s sur orange.fr
DATE: 2014-02-15
URL: http://actu.orange.fr/france/un-homme-possedant-3-000-images-pedopornographiques-condamne-a-2-ans-de-prison-afp-s_2833056.html
PRINCIPAL: 177193
TEXT:
15/02/2014 � 18:39
Un homme poss�dant 3.000 images p�dopornographiques condamn� � 2 ans de prison
La justice a condamn� � deux ans de prison, dont un avec sursis un homme qui poss�dait pr�s de 3.000 fichiers � caract�re p�dopornographique sur son ordinateur.
r�agir 45 �
photo : JOEL SAGET, AFP
Un homme interpell� jeudi pour avoir t�l�charg� pr�s de 3.000 fichiers � caract�re p�dopornographique sur son ordinateur a �t� condamn� � deux ans d'emprisonnement, dont un avec sursis, a-t-on appris samedi aupr�s de la gendarmerie du Val-d'Oise.
Le domicile de cet homme �g� d'un quarantaine d'ann�es, r�sidant � Ecouen (Val-d'Oise), a �t� perquisitionn� apr�s une enqu�te des gendarmes de la cellule "Nouvelles technologies", sp�cialis�e dans la cybercriminalit�.
"L'analyse de son ordinateur a permis la d�couverte d'environ 3.000 fichiers � caract�re p�dopornographique, que l'homme avait t�l�charg�s via une plateforme et qu'il partageait avec d'autres utilisateurs", a d�clar� � l'AFP une source � la gendarmerie.
voir (0)��
