TITRE: Guide des maisons de retraite : Cl�ture de la concertation sur le projet de loi d'orientation et de programmation pour l'adaptation de la soci�t� au vieillissement
DATE: 2014-02-15
URL: http://www.capgeris.com/allocation-apa-319/cloture-de-la-concertation-sur-le-projet-de-loi-d-orientation-et-de-programmation-pour-l-adaptation-de-la-societe-au-vieillissement-a29075.htm
PRINCIPAL: 175785
TEXT:
Allocation - APA
Cl�ture de la concertation sur le projet de loi d'orientation et de programmation pour l'adaptation de la soci�t� au vieillissement
Le Premier Ministre se rend � Angers, � Ville Amie des A�n�s �, pour pr�senter les principales mesures du projet de loi.
Zoom
Le 29 novembre 2013, le Premier ministre, aux c�t�s de Marisol Touraine, ministre des affaires sociales et de la sant�, et de Mich�le Delaunay, ministre d�l�gu�e aux personnes �g�es et � l'autonomie, a officiellement ouvert la concertation destin�e � pr�parer la loi d'orientation et de programmation pour l'adaptation de la soci�t� au vieillissement :
� C'est un des grands chantiers du quinquennat que nous lan�ons aujourd'hui. Non seulement parce qu'il concerne chaque Fran�ais, non seulement parce qu'il nous touche dans notre vie familiale, mais parce qu'il est au coeur du mod�le de soci�t� que nous voulons construire. �
Deux mois de d�bats et d'�changes constructifs ont permis d'enrichir et de finaliser le texte du projet de loi d'orientation et de programmation pour l'adaptation de la soci�t� au vieillissement.
Il sera dans les tout prochains jours transmis au Conseil Economique, Social et Environnemental (CESE), pour une pr�sentation en Conseil des ministres d�but avril et un d�p�t au Parlement au printemps.
Ainsi le projet de loi pourra-t-il �tre adopt� avant la fin de l'ann�e 2014, conform�ment � l'engagement qui a �t� pris devant les Fran�ais.
Au moment o� la concertation prend fin, le Premier ministre, accompagn� de la ministre des affaires sociales et de la sant� et de la ministre d�l�gu�e aux personnes �g�es et � l'autonomie, s'est rendu � Angers, � Ville Amie des A�n�s � au sens de l'Organisation Mondiale de la Sant�, pour illustrer la n�cessit� de pr�parer d�s maintenant la soci�t� toute enti�re au vieillissement de la population.
Il a � cette occasion pr�sent� les principales mesures du projet de loi, qui repose sur une mobilisation de la soci�t� tout enti�re autour du d�fi in�dit que constitue la � r�volution de l'�ge �.
En 2060, un tiers des Fran�ais aura plus de 60 ans.
Et les personnes �g�es de plus de 85 ans seront pr�s de 5 millions, contre 1,4 million aujourd'hui.
La � r�volution de l'�ge � impose de revoir notre vision de l'�ge : elle n'est pas la marque d'un d�clin, mais bien au contraire le signe d'un progr�s consid�rable pour la soci�t� fran�aise.
Elle g�n�re de nouvelles exigences de solidarit� et valorise le lien social et interg�n�rationnel.
Elle constitue aussi une opportunit� �conomique r�elle, susceptible de cr�er des milliers d'emplois dans les prochaines ann�es.
Marisol Touraine et Mich�le Delaunay ont, dans le prolongement de ce d�placement du Premier ministre, pr�sent� de fa�on d�taill�e les apports de la concertation et les grandes mesures du futur projet de loi dans l'h�micycle du CESE devant pr�s de 300 acteurs de la politique en direction des personnes �g�es.
Ce choix symbolise un � passage de t�moin � entre le Gouvernement et les repr�sentants du corps social dans son ensemble.
Deux mois de mobilisation
Pendant deux mois, pas moins de 80 r�unions ont �t� organis�es, rassemblant plus de 500 participants, tandis qu'une trentaine de contributions �crites ont �t� vers�es au d�bat.
Les ministres ont particip� en personne � tout ou partie de ces r�unions de concertation.
Le Gouvernement a tenu � associer �troitement l'Assembl�e des D�partements de France � ces �changes en raison du r�le essentiel des conseils g�n�raux dans la mise en oeuvre des politiques d'autonomie.
L'ensemble du secteur m�dico-social, les repr�sentants des �g�s et de leurs familles, les collectivit�s territoriales et les partenaires sociaux ont �galement �t� partie prenante de cette concertation.
Celle-ci a �t� �tendue - pour la premi�re fois dans le champ de l'�ge - aux repr�sentants des secteurs de l'habitat, des transports, de l'urbanisme, de la vie associative et citoyenne, des sports et de cette nouvelle fili�re d'industrie et de services qu'est la silver �conomie.
Le Premier ministre tient � saluer l'engagement de chacun, � la hauteur de l'extraordinaire attente de la soci�t� face aux enjeux de la transition d�mographique.
Les propositions issues de la concertation sont d'ores et d�j� valoris�es, et de fa�on tr�s concr�te : retranscrites dans un rapport de restitution, elles ont permis d'enrichir et de nourrir le projet de loi d'orientation et de programmation pour l'adaptation de la soci�t� au vieillissement.
Le calendrier
14 f�vrier 2014 : Communication en Conseil des ministres
Mi-f�vrier 2014 : Transmission au Conseil �conomique, social et environnemental
D�but mars 2014 : Lancement du groupe de travail sur les �tablissements
D�but avril 2014 : Adoption en Conseil des ministres
Fin 2014 : Adoption de la loi au Parlement
2015 : Entr�e en vigueur de la loi d'orientation et de programmation pour l'adaptation de la soci�t� au vieillissement
Le projet de loi d'orientation et de programmation pour l'adaptation de la soci�t� au vieillissement
Zoom
Le projet de loi d�orientation et de programmation pour l�adaptation de la soci�t� au vieillissement
Le Gouvernement a fait le choix d�une loi d�orientation et de programmation, inscrivant la totalit� de la politique de l��ge dans un programme pluriannuel et transversal, embrassant toutes les dimensions de la prise en compte de l�avanc�e en �ge et confortant le choix d�un financement solidaire de l�accompagnement de la perte d�autonomie.
Ce projet se veut porteur d�un projet global pour les ann�es � venir, avec des actions de court et de moyen terme.
Il s�adresse aussi bien au retrait� actif, qui souhaite continuer � s�investir dans la vie de la cit�, qu�� la personne �g�e qui commence � ressentir une fragilit� et � la personne en perte d�autonomie qui doit pouvoir b�n�ficier de la solidarit� nationale.
Ce projet de loi est fond� sur quelques principes essentiels :
Le choix de la solidarit� et de l��quit�
Dans un contexte budg�taire contraint, le Gouvernement a d�cid� de conforter les principes de solidarit� nationale inscrits dans la loi de 2001 qui a cr�� l�APA (Allocation Personnalis�e d�Autonomie). Universalit�, financement solidaire, �galit� de traitement des citoyens sur tout le territoire resteront les fondements de la prise en charge du risque de perte d�autonomie.
Le choix d�une vision positive de l��ge, au b�n�fice de toutes les g�n�rations.
Notre soci�t� doit s�adapter pour donner toute leur place aux �g�s, v�ritable colonne vert�brale de la coh�sion familiale, sociale et citoyenne.
La loi propose un mod�le de soci�t� plus fraternelle, plus apais�e et r�concili�e avec les plus fragiles. Ainsi comporte-t-elle une dimension �thique et soci�tale majeure.
Le choix de la co-construction et du partenariat
Le projet de loi mobilise tous les acteurs du m�dico-social, au premier rang desquels les conseils g�n�raux, dont le r�le de � chef de file � des politiques de l�autonomie � l��chelle du d�partement est confort�, mais aussi les agences r�gionales de sant�, les caisses de retraite, les communes et intercommunalit�s (via notamment leurs centres d�action sociale), les acteurs de l�aide � domicile et des �tablissements, les compl�mentaires sant�, les mutuelles, les institutions de pr�voyance.
La loi associe, en outre, les acteurs du secteur du logement, des transports, du sport, de la culture, mais aussi du monde �conomique.
Le choix de concevoir une politique de l��ge au plus pr�s des personnes et de leurs familles, pour qu�elle soit lisible, coh�rente et accessible � tous.
Des conseils d�partementaux pour la citoyennet� et l�autonomie seront cr��s, qui auront pour vocation d�am�liorer la coordination entre les acteurs et de renforcer la participation des personnes �g�es et en situation de handicap.
Le choix de la participation des �g�s � l��laboration d�une politique de l��ge inter-minist�rielle.
La cr�ation d�un Haut Conseil de l��ge, directement plac� sous l�autorit� du Premier ministre et r�unissant l�ensemble des acteurs des politiques concern�es par l�avanc�e en �ge (logement, culture, urbanisme transports�), concr�tise cette ambition.
Les principales mesures de la loi
Le projet de loi qui va �tre transmis au CESE repose sur trois piliers indissociables : l�anticipation, pour pr�venir la perte d�autonomie de fa�on individuelle et collective, l�adaptation de notre soci�t� tout enti�re � l�avanc�e en �ge, et l�accompagnement de la perte d�autonomie, avec pour priorit� de permettre � ceux qui le souhaitent de rester � domicile dans de bonnes conditions le plus longtemps possible.
Il constitue une premi�re �tape l�gislative.
Une seconde �tape portera sur l�accompagnement et la prise en charge des personnes �g�es dans les �tablissements et int�grera notamment des mesures permettant de r�duire le poids financier de l�h�bergement pour les �g�s qui r�sident en maisons de retraite m�dicalis�es et pour leurs familles.
Un groupe de travail sera mis en place d�s le mois de mars pour d�finir ces mesures qui seront mises en oeuvre dans la seconde partie du quinquennat.
Pour cette premi�re �tape l�gislative, le financement de la loi reposera exclusivement sur la contribution additionnelle de solidarit� pour l�autonomie (CASA), vot�e pour financer la loi d�adaptation de la soci�t� au vieillissement, et dont le rendement est estim� � 645 millions d�euros par an.
Ainsi, la CASA r�pondra bien � sa vocation et sera pleinement affect�e � la politique de l��ge d�s 2015.
Pr�vention
L��ge est un facteur d�acc�l�ration d�in�galit�s sociales et de sant� qui entra�nent un risque accru de perte d�autonomie.
Pr�venir tout au long de la vie la perte des capacit�s physiques et psychiques, rep�rer les facteurs de risque sont des priorit�s.
La r�forme permettra, d�une part, de proposer, chaque fois que n�cessaire, des programmes de pr�vention adapt�s et, d'autre part, de faciliter le recours aux aides techniques pour retarder la perte d�autonomie.
Pour notre soci�t�, il s�agit d�anticiper, au lieu de subir, le vieillissement de nos concitoyens, dont les effets sur l'autonomie ne sont pas une fatalit� : la priorit� � la pr�vention de la perte d�autonomie est partag�e.
Inscrire un volet � personnes �g�es � dans la strat�gie nationale de sant�
La sant� des personnes �g�es figure parmi les grandes priorit�s de sant� publique faisant l�objet d�une approche int�gr�e identifi�es dans la feuille de route de la Strat�gie nationale de sant� pr�sent�e par Marisol Touraine en septembre 2013.
S�agissant de la meilleure coordination des interventions des �quipes soignantes et sociales, le projet PAERPA (Personnes Ag�es En Risque de Perte d�Autonomie), d�ploy� depuis septembre 2013, va permettre aux huit r�gions pilotes de s�lectionner les premiers porteurs de projet.
PAERPA financera l�exp�rimentation de nouvelles pratiques professionnelles pour optimiser le parcours de sant� des personnes �g�es de plus de 75 ans dont l��tat de sant� est susceptible de s�alt�rer pour des raisons d�ordre m�dical et / ou social.
Am�liorer l�acc�s aux aides techniques et aux actions collectives de pr�vention
Am�nager son domicile et recourir � la t�l�assistance et � la domotique sont des moyens - souvent simples - de pr�venir chutes et perte d�autonomie.
Or, le soutien au domicile des �g�s repose aujourd�hui presque exclusivement sur l�aide humaine et ne permet pas suffisamment de d�velopper des projets individuels de pr�vention.
Par ailleurs, de nombreuses actions collectives de pr�vention pour les �g�s sont organis�es, mais elles restent dispers�es, peu lisibles et trop m�connues.
La loi organise des programmes coh�rents de pr�vention pr�voyant notamment la promotion d�une activit� sportive et d�une meilleure nutrition, des r�gles de bon usage du m�dicament, des actions de lutte contre le suicide, une mobilisation contre l�isolement.
Elle apporte �galement des moyens financiers pour mieux solvabiliser l�acc�s aux aides techniques et aux actions collectives de pr�vention, en ciblant les m�nages les plus modestes, quel que soit leur niveau de perte d�autonomie.
Il s�agit de permettre un acc�s plus large aux aides techniques et de d�velopper les actions collectives de pr�vention.
140 millions seront ainsi consacr�s � ce volet pr�vention, par des moyens d�l�gu�s aux � conf�rences d�partementales des financeurs de la pr�vention de la perte d�autonomie �.
Cr��es par la loi, ces organisations nouvelles associeront, sous la pr�sidence du conseil g�n�ral, les caisses de retraite, l�agence r�gionale de sant� (ARS) et les autres acteurs volontaires (mutuelles, etc.).
Au sein de ces Conf�rences, les caisses de retraite, organis�es � leur initiative en inter-r�gimes, joueront un r�le central pour garantir un continuum de pr�vention et de prise en charge.
Adaptation
D�velopper des politiques de l�habitat et de l�urbanisme prenant mieux en compte l�avanc�e en �ge
Il s�agit d�int�grer syst�matiquement dans les programmes locaux de l�habitat (PLH) comme dans les sch�mas g�rontologiques, un volet relatif � l�habitat des personnes �g�es (adaptation des logements, d�veloppement d�une offre adapt�e et diversifi�e).
La dynamique Villes amies des a�n�s permettra �galement de mieux mobiliser les outils d�urbanisme.
Lancer un Plan national d�adaptation de 80 000 logements d�ici 2017
L�adaptation de la soci�t� au vieillissement, comme la priorit� donn�e au domicile, imposent de conduire un effort tout particulier dans le domaine du logement des �g�s et de soutenir l�effort d�adaptation des logements priv�s.
Celui-ci sera notamment port� par l�Agence nationale de l�habitat (ANAH) et la Caisse nationale d�assurance-vieillesse (CNAV), dans le cadre d�un plan autonomie pour l'adaptation de 80 000 logements priv�s sur la p�riode 2014-2017.
Pour permettre d�atteindre cet objectif, le budget de l�Agence nationale de l�habitat (ANAH) sera abond� en 2015 et 2016 d�un versement de la Caisse nationale de solidarit� pour l�autonomie (CNSA) de 40 millions d�euros.
Ce plan permettra �galement de mieux d�tecter les besoins des �g�s, d�accompagner les propri�taires, et d�am�liorer les financements conjoints de l�Agence nationale de l�habitat (ANAH) et de la Caisse nationale d�assurance-vieillesse (CNAV).
Donner un nouveau souffle aux foyers-logements, rebaptis�s R�sidences Autonomie
Les logements dits � interm�diaires � sont une bonne solution pour les �g�s qui ont besoin d�un accompagnement pour pr�server leur autonomie, tout en souhaitant rester � � domicile �.
Les foyers logements font partie de cette palette de r�ponses propos�es aux �g�s qu�il faut continuer de d�velopper et diversifier, et pour certains mieux adapter.
La loi entend donc donner un nouveau souffle aux foyers-logements, rebaptis�s � R�sidences Autonomie �, gr�ce � la cr�ation d�un � forfait autonomie �, qui permettra de renforcer leurs actions de pr�vention, pour un montant de 40 millions d�euros.
Par ailleurs - d�marche in�dite - l��tat participera � la r�novation des foyers-logement, via un Plan exceptionnel d�aide � l�investissement de 40 millions d�euros qui s�ajouteront aux 10 millions d�euros d�j� d�gag�s en 2014.
Promouvoir une � silver mobilit� � pour les �g�s dans les politiques locales de transport via notamment les plans de d�placement urbain (PDU)
Cr�er un volontariat civique senior, qui valorise l�engagement r�publicain b�n�vole des �g�s
D�velopper la cohabitation interg�n�rationnelle, qui permet de faire se rencontrer la solitude d�un �g� et le besoin en logement d�un �tudiant.
Affirmer les droits et libert�s des �g�s, en r�affirmant fortement les principes fondamentaux que sont la libert� d�aller et venir ou le respect du consentement lors de l�entr�e en �tablissement, et en les prot�geant des exc�s du march� et des abus auxquels ils sont particuli�rement expos�s.
Accompagnement
Poser un acte II de l�APA � domicile
Pr�s de 1,2 millions de personnes b�n�ficiaient de l�APA fin 2011, dont pr�s de 700 000 � domicile (60%).
L�APA permet d�accompagner les plus d�pendants (GIR 1) mais aussi, et c�est essentiel, de pr�server l�autonomie des �g�s en situation de fragilit� (GIR 4).
Pourtant, cette prestation conna�t aujourd�hui des limites : en 2011, un plan d�aide sur quatre �tait satur� (c�est-�-dire atteignait son plafond).
C��tait m�me le cas pour pr�s de la moiti� des plans d�aide pour les plus d�pendants.
Par ailleurs, de nombreux b�n�ficiaires y renoncent en partie pour des raisons financi�res.
Enfin, la qualit� de l�intervention peut �tre am�lior�e par de meilleures conditions de travail des professionnels.
Une APA plus g�n�reuse
Les plafonds d�aide mensuels de l�APA seront revaloris�s de 400 � en GIR 1, de 250 � en GIR 2, de 150 � en GIR 3 et de 100 � en GIR 4.
Cette revalorisation touchera donc tous les b�n�ficiaires de l�APA, quel que soit leur degr� de d�pendance.
Exemple : Madame X souffre d�une lourde perte d�autonomie. Elle est en GIR 1. La revalorisation du plafond de son APA lui permettra de b�n�ficier de pr�s d�une heure de plus par jour d�aide � domicile.
Sa voisine, Madame Y, a la chance d��tre plus autonome. En GIR 4, elle pourra b�n�ficier jusqu�� une heure de plus par semaine d�aide � domicile.
Une APA plus accessible gr�ce � la diminution du reste � charge qui p�se aujourd'hui sur les �g�s et leurs familles.
Pour la part du plan d�aide comprise entre 350 et 550 euros, le ticket mod�rateur, c�est-�-dire ce qu�il reste � la personne �g�e � payer, pourra baisser jusqu�� 60 %.
Pour la part du plan d�aide allant au-del� de 550 euros, la baisse pourra atteindre 80 %.
De plus, aucun b�n�ficiaire de l�allocation de solidarit� aux personnes �g�es (ex minimum vieillesse) n�acquittera plus, d�sormais, de ticket mod�rateur.
Exemple : Monsieur V dispose de 1 000 � de ressources mensuelles. En GIR 1, la diminution du reste � charge de son APA repr�sentera une �conomie annuelle d�environ 1000 euros. S�il �tait plus autonome, en GIR 4, son �conomie annuelle atteindrait 250 � par an.
En alliant le b�n�fice de l�augmentation des plafonds et de l�all�gement du reste � charge, le � gain � mensuel pour une personne disposant de 1 500 � de ressources pourra aller jusqu�� pr�s de 150 � pour les plus autonomes (GIR 4), et pourra atteindre plus de 600 � pour les moins autonomes (GIR 1).
Une APA plus qualifi�e, gr�ce � la professionnalisation des aides � domiciles, � l�am�lioration de leurs conditions de travail, � la lutte contre la pr�carisation des salari�s et � une meilleure prise en compte de leurs frais professionnels.
25 millions d�euros y seront consacr�s chaque ann�e.
Il s�agira notamment d�am�liorer la situation financi�re des intervenants � domicile pour les plus bas salaires et de mieux valoriser leurs d�placements, qui font partie int�grante de leur activit�.
Au total, 375 millions d�euros suppl�mentaires seront consacr�s chaque ann�e � l�APA � domicile.
Simplifier les d�marches des �g�s et de leurs familles
La loi am�liore les dispositifs locaux et nationaux d�information et d�orientation sur les droits, notamment via le futur portail internet port� par la Caisse nationale de solidarit� pour l�autonomie (CNSA).
Ce portail regroupera des informations sur les �tablissements et les services utiles � l�accompagnement de la perte d�autonomie pour les �g�s et leurs aidants.
Il permettra � chaque personne d�acc�der � une information claire et accessible, et de pouvoir choisir son �tablissement en connaissant ses droits et les aides qu�elle pourra mobiliser.
Pour rendre possible sur ce site la comparaison des prix � prestation donn�e, la loi pr�voit de normaliser la d�finition des prestations couvertes par les tarifs.
Seront aussi favoris�s l�utilisation du ch�que emploi service universel (CESU) pour l�APA et le tiers payant pour les services d�aide � domicile.
Reconna�tre le r�le des aidants en cr�ant une aide au � r�pit �
4,3 millions de personnes aident r�guli�rement au moins un de leurs proches �g� de 60 ans ou plus � domicile en raison d�une sant� alt�r�e ou d�un handicap.
20 % des aidants ont des sympt�mes de fatigue morale ou physique, avec des effets sur leur sant� : 40 % des aidants dont la charge est la plus lourde se sentent d�pressifs, 29 % d�clarent consommer des psychotropes.
En outre, les aidants renoncent fr�quemment � des soins, faisant passer la sant� de l�aid� avant leur propre sant�.
Une aide au � r�pit � est cr��e afin de permettre � l'aidant d�une personne tr�s peu autonome de s'absenter quelques jours en garantissant que le relais sera pris aupr�s de l�aid� (heures d'aide suppl�mentaires � domicile voire pr�sence continue ou accueil exceptionnel en accueil de jour ou en h�bergement temporaire).
D�un montant qui pourra aller jusqu�� 500 euros annuels au-del� du plafond de l�APA, cette aide permettra par exemple de financer sept jours de s�jour dans un h�bergement temporaire.
Elle est cibl�e sur les aidants dont les besoins de soutien sont les plus importants.
De plus, un dispositif d�urgence est mis en place pour accompagner les aid�s dont l'aidant est hospitalis�.
Pr�s de 80 millions d�euros par an seront consacr�s � ces deux dispositifs.
En outre, les moyens de la CNSA sont renforc�s pour contribuer au financement d�actions de soutien et d'accompagnement des aidants (par exemple en d�veloppant des � caf�s des aidants �), � hauteur de 5 millions d�euros par an.
Angers, � ville amie des a�n�s �
Zoom
Angers, � ville amie des a�n�s �
Angers, � ville amie des a�n�s �, est consciente de la n�cessit� de se pr�parer d�s maintenant au vieillissement de sa population et a su mobiliser tous ses acteurs (notamment les bailleurs sociaux) pour d�velopper des logements et services adapt�s aux plus �g�s au coeur de la ville, tout en privil�giant l�approche interg�n�rationnelle.
Angers est �galement investie dans la Silver �conomie avec le g�rontop�le Autonomie Long�vit�, et le Centre d�expertise national des technologies de l�information et de la communication pour l�autonomie.
Enfin, la ville contribue � l��panouissement de ses �g�s avec l'Universit� Angevine du Temps Libre et � un nouveau regard entre les g�n�rations, pour avoir d�velopp� l'engagement des �tudiants contre l'isolement des �g�s.
Le Premier ministre et les ministres, lors de leur d�placement du 12 f�vrier, ont visit� le Logement Evolutif pour une Nouvelle Autonomie (le LENA) et l�Espace territorial du Bien vieillir Robert Robin (p�le territorial g�rontologique), repr�sentatifs de ces deux derni�res solutions.
Visite du LENA
Le Logement Evolutif pour une Nouvelle Autonomie est int�gr� au Centre d�expertise national des technologies de l�information et de la communication pour l�autonomie et la sant� (Centich), soutenu par la Caisse Nationale de Solidarit� pour l�Autonomie (CNSA) depuis 2009.
Au sein du Square des Ages, ce complexe propose appartements pour personnes �g�es et �tudiants handicap�s.
Le LENA est un appartement adapt�, �volutif et int�grant les technologies de l�information et de la communication pour l�autonomie.
Con�u pour �tre un espace d�accueil, d�information et de conseil pour l�adaptation du logement et le maintien � domicile, il propose un parcours de pr�vention et de sensibilisation pour bien am�nager son logement.
Espace territorial du Bien vieillir Robert Robin, p�le territorial g�rontologique
Il s�agit d�un lieu ouvert sur le quartier, avec de nombreux services accessibles aux plus de 60 ans : un guichet unique d�informations sur le bien vieillir, centre local d�information et de coordination et Maison pour l�Autonomie et l�Int�gration des malades Alzheimer (MAIA), anim� par l�association Pass��ge, un restaurant de 150 couverts ouvert � tous (�g�s ou non), un caf�-rencontre, des animations, une permanence du Service de Soutien � Domicile, un foyer-logement de quatre-vingt-douze appartements, six logements pour personnes handicap�es et quatre h�bergements temporaires.
Publi� le 14 f�vrier 2014
R�AGISSEZ, COMMENTEZ, PARTAGEZ !
Twitter
DANS LA M�ME RUBRIQUE
