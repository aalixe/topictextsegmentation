TITRE: Coming out d'Ellen Page: la vid�o de son beau discours | Slate.fr
DATE: 2014-02-15
URL: http://www.slate.fr/culture/83541/coming-out-ellen-page-video-discours
PRINCIPAL: 176157
TEXT:
2
Ellen Page � Hollywood le 28 mai 2013, REUTERS/Mario Anzuoni
L'actrice Ellen Page vient de faire son coming out avec un discours �mouvant et �loquent vendredi 14 f�vrier lors d'une conf�rence LGBT de l'ONG Human Rights Campaign � Las Vegas. �Aimer d'autres personnes commence par s'aimer soi-m�me et s'accepter� a d�clar� Page, visiblement nerveuse. �Je suis ici aujourd'hui parce que je suis gay.� Apr�s une br�ve standing ovation, elle a continu�:
�Je peux peut-�tre faire une diff�rence pour aider d'autres � passer un moment plus facile et optimiste. Je le fais aussi de mani�re �go�ste parce que je suis fatigu�e de me cacher et je suis fatigu�e de mentir par omission. J'ai souffert pendant des ann�es parce que j'avais peur de le dire. Mon esprit a souffert. Ma sant� mentale a souffert. Mes relations amoureuses ont souffert. Je suis ici aujourd'hui avec vous tous de l'autre c�t� de cette douleur.�
Qu'une actrice ayant jou� � la fois dans des blockbusters ( Inception ) et des succ�s ind�pendants ( Juno ) parle publiquement de son orientation sexuelle est en soi remarquable, mais c'est d'autant plus louable qu'elle l'a fait � cette conf�rence, devant un parterre de personnes travaillant en faveur des jeunes LGBT. Voici la vid�o du discours touchant de Page:
Dan Kois
