TITRE: Les djihadistes de l'EIIL ex�cutent 21 personnes en Syrie-OSDH
DATE: 2014-02-15
URL: http://www.zonebourse.com/actualite-bourse/Les-djihadistes-de-lEIIL-executent-21-personnes-en-Syrie-OSDH--17951626/
PRINCIPAL: 176677
TEXT:
Les djihadistes de l'EIIL ex�cutent 21 personnes en Syrie-OSDH
14/02/2014 | 19:45
Recommander :
0
Les djihadistes de l'Etat islamique en Irak et au Levant (EIIL) ont ex�cut� au moins 21 personnes, dont des membres de groupes rebelles rivaux, pr�s d'Alep, la grande ville du nord de la Syrie, rapporte vendredi l'Observatoire syrien des droits de l'homme (OSDH).
A Haritan, au nord-ouest d'Alep, ce sont 17 hommes qui ont �t� tu�s, dont des combattants rebelles qui avaient �t� captur�s par les djihadistes. Leurs corps ont �t� jet�s dans un puits.
A la suite d'un combat � Azaz, pr�s de la fronti�re turque au nord d'Alep, les hommes de l'EIIL ont aussi d�capit� quatre miliciens appartenant � des factions rivales, pr�cise l'ONG proche de l'opposition syrienne.
Le mois dernier, plusieurs groupes insurg�s, dont des islamistes, ont uni leurs forces pour tenter de chasser l'EIIL de ses bastions dans le nord et l'est de la Syrie. Les combats ont fait des centaines de morts.   (Tom Perry, Guy Kerivel pour le service fran�ais)
Recommander :
