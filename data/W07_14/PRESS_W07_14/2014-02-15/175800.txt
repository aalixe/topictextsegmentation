TITRE: Intemp�ries en Bretagne : un octog�naire meurt dans la temp�te - SudOuest.fr
DATE: 2014-02-15
URL: http://www.sudouest.fr/2014/02/15/intemperies-en-bretagne-un-octogenaire-meurt-dans-la-tempete-1463136-4971.php
PRINCIPAL: 175798
TEXT:
Il est tomb� d'un paquebot de croisi�re au large de la Bretagne. Quatre d�partements du Nord Ouest de la France sont toujours en vigilance orange
Les vagues touchant le port du Guilvinec, dans le Finist�re � Photo
AFP JEAN-SEBASTIEN EVRARD
Publicit�
Un octog�naire est d�c�d� vendredi � bord d'un paquebot naviguant au large de la Bretagne � la suite d'une chute provoqu�e par les mauvaises conditions m�t�orologiques frappant le Nord-Ouest du pays, tandis que 100.000 foyers �taient priv�s d'�lectricit� en Bretagne o� des vents � 150 km/h ont �t� enregistr�s dans la nuit.�
Publicit�
Une femme � bord du m�me paquebot a �t� �vacu�e par h�licopt�re � la suite �galement d'une chute, a indiqu� la pr�fecture maritime de l'Atlantique. "Les deux personnes ont chut� sans aucun doute en raison des conditions climatiques particuli�rement difficiles", a indiqu� le capitaine de corvette Karine Foll, de la pr�fecture maritime, pr�cisant que le vent soufflait sur zone en rafales � plus de 70 noeuds (120 km/h).
Quatre d�partements de l'Ouest de la France - Finist�re, Ille-et-Vilaine, Loire-Atlantique, Morbihan - �taient toujours plac�s en vigilance orange "vagues-submersion", selon le bulletin publi� � 6h par M�t�o-France.
� Photo Document M�t�o France
Dans la nuit "les valeurs maximales relev�es durant l'�pisode temp�tueux ont �t� de l'ordre de 140 � 150 km/h � la Pointe de Bretagne et de 130 � 135 km/h au nord d'une ligne Brest-Morlaix; la partie du Finist�re plus au sud de cette ligne, a connu des valeurs comprises entre 102 et 110 km/h", selon M�t�o France. �Le vent violent qui avait commenc� � souffler dans la journ�e, a plong� dans le noir pr�s de 100.000 foyers en Bretagne, selon Electricit� R�seau distribution France (ERDF). Quelque 50.000 foyers �taient concern�s � 19H30 dans le Finist�re, 40.000 dans les C�tes d'Armor, 4.000 dans le Morbihan et 1.000 en Ille-et-Vilaine.
Le trafic ferroviaire entre Rennes et Brest a �t� interrompu, selon la SNCF, qui a estim� � quelque 2.000 le nombre de voyageurs touch�s par cette mesure de pr�caution. �En outre, la quasi-totalit� des vols ont �t� annul�s dans les a�roports de Brest, Lorient et Quimper. Dans le Finist�re, particuli�rement touch�, les pompiers avaient men� en d�but de soir�e plus de 500 interventions, essentiellement en raison de la chute d'arbres.
Il y a avait en d�but de soir�e 40 cm d'eau sur les quais de Quimperl�, selon la pr�fecture du Finist�re, et de l�gers d�bordements � Morlaix et Landerneau. En outre, de nombreuses routes ont �t� coup�es en raison d'inondations. A Brest il est tomb� depuis le 15 d�cembre quelque 636 mm de pluie, un record, selon M�t�o France.
