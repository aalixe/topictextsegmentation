TITRE: Manifestation �cotaxe : Journ�e de bras de fer sous le portique de Brech. Info - Vannes.maville.com
DATE: 2014-02-15
URL: http://www.vannes.maville.com/actu/actudet_-manifestation-ecotaxe-bras-de-fer-sous-le-portique-de-brech_une-2492183_actu.Htm
PRINCIPAL: 177039
TEXT:
11
Les manifestants d�termin�s. � Photo : Thierry Creux / Ouest-France
Un pique-nique contre l'�cotaxe �tait pr�vu samedi midi sous le portique de Brech. La journ�e s'est termin�e par des affrontements avec les CRS.
Les opposants � l'�cotaxe comptaient organiser un pique-nique � Brech, sous le portique �cotaxe, ce samedi midi. La journ�e a tourn� au bras de fer avec les forces de l'ordre. Il y a eu 6 bless�s.
Les gendarmes ont d�velopp� leurs moyens pour bloquer l'acc�s au portique. Des d�viations ont �t� mises en place dans les deux sens � hauteur d'Auray dans le sens Vannes-Lorient et de Landaul dans le sens Lorient-Vannes. La RN 165 a �t� coup�e.
Le fil de la journ�e :
13:00. Barrages de CRS et gendarmes mobiles
Le cort�ge est parti de Landaul vers 13h sur la voie rapide, o� la circulation �tait interdite. � deux cent m�tres du portique, trois barrages de CRS et gendarmes mobiles bloquent l'acc�s au portique.
�14:20. La tension monte
La tension est mont�e entre CRS et opposants � l'�cotaxe, alors que ces derniers cherchent � s'approcher du portique de Brech. Les manifestants ont lanc� des �ufs, les CRS ont r�pliqu� avec des gaz lacrymog�nes.
Les manifestants lancent des �ufs sur les forces de l'ordre :
Les manifestants d�termin�s.�|�Photo : Thierry Creux / Ouest-France
15:00. Des sc�nes de gu�rilla
Les manifestants continuent de vouloir approcher le portique �cotaxe tenu par les forces de l'ordre. Dans un concert de bidons et dans une �paisse fum�e, les �chauffour�es se poursuivent avec les CRS.
16:00 Les affrontements ont cess�
Les affrontements ont cess� depuis une demie heure mais les manifestants et les CRS sont toujours face � face. Les pompiers sont sur place. Plusieurs personnes ont �t� bless�es, dont une aurait �t� touch�e par un tir de flash-ball.
18:30 Dispersion
Les manifestants, qui �taient entre 500 et 1 000, se sont dispers�s aux alentours de 18h30.
Au final, les affrontements ont fait 6 bless�s, dont un membre des forces de l'ordre. Trois personnes ont �t� bri�vement interpell�es, avant d'�tre rel�ch�es.
Ouest-France��
