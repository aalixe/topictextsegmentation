TITRE: Fatima Benbraham : "Les essais nucl�aires fran�ais au Sahara, pires que ce que l'on croyait"
DATE: 2014-02-15
URL: http://www.lemag.ma/Fatima-Benbraham-Les-essais-nucleaires-francais-au-Sahara-pires-que-ce-que-l-on-croyait_a80616.html
PRINCIPAL: 0
TEXT:
Fatima Benbraham : "Les essais nucl�aires fran�ais au Sahara, pires que ce que l'on croyait"
APS - Lemag - publi� le Samedi 15 F�vrier 2014 � 11:22
Paris - La juriste alg�rienne, Fatima Benbraham, a exprim� sa "stup�faction" d'apprendre que les retomb�es radioactives des essais nucl�aires fran�ais dans le Sahara oriental sont "pires que ce que l'on croyait", affirmant que la France "doit rendre des comptes" sur cette question.
A proximit�
D�put�e islamiste : La MINURSO propage le SIDA au Sahara
"Nous savons depuis longtemps que les cons�quences des essais nucl�aires dans le Sahara sont beaucoup plus graves que ce qu en disait la France, mais l�, on d�couvre que c'est toute l'Alg�rie qui a �t� touch�e et m�me tous les pays de l'ancienne Afrique de l'Ouest coloniale", a-t-elle dit dans un entretien accord�e au journal Le Parisien.
Ce quotidien fran�ais a publi� en exclusivit� la carte class�e jusque-l�"Secret-d�fense" par l arm�e fran�aise, sur les retomb�es radioactives des essais nucl�aires en Alg�rie.
Aujourd'hui, a dit Me Benbraham, "la France doit rendre des comptes",soulignant que "le mythe de la 'bombe propre' est fini" et que "c'est un crime continu que nous subissons depuis cinquante ans".
Elle a indiqu�, �galement, que "contrairement � ce que disait la France,les zones de tirs des bombes �taient loin d �tre inhabit�es", ajoutant qu aujourd'hui,elle d�fend des gens expos�s � l �poque, mais aussi leurs enfants, les nomades qui ont travers� ces zones depuis des d�cennies, et les jeunes qui ont fait leur service militaire dans ces lieux, d�comptant, ainsi, "des dizaines de milliers de victimes potentielles".
Dans le Grand Sud, a-t-elle ajout�, le diab�te, la leuc�mie, la st�rilit�,les affections de la thyro�de, qui sont des maladies caract�ristiques des radiations,"ont explos� ces derni�res ann�es".
Me Benbraham a relev�, par ailleurs, que lorsque l'arm�e fran�aise a quitt� les sites de tirs, en 1967, elle a emport� toutes les archives avec elle,"ce qui rend compliqu� de savoir quelles sont les zones touch�es par ces essais".
Elle a rappel�, � ce propos, qu en partant, les militaires fran�ais ont enfoui des tonnes de mat�riels radioactifs, sans laisser la cartographie et que depuis, "les habitants se sont servi, ont r�cup�r� du m�tal, du cuivre,au p�ril de leur vie".
"La loi d indemnisation des v�t�rans fran�ais exclut les populations alg�riennes. C'est un scandale", s est-elle offusqu�e, soutenant que cette loi"doit �tre ouverte � toutes les victimes".
"L arm�e fran�aise doit nous donner les archives et cesser de se prot�ger derri�re le Secret-d�fense", a encore d�clar� Me Benbraham, ajoutant, en outre,que la France "doit nous aider � d�contaminer les montagnes de d�chets et soutenir des programmes de soin".
Elle a soulign�, � ce sujet, que les Alg�riens victimes de cancers dans le Grand Sud, doivent se rendre � Alger pour se soigner.
Me Fatima Benbraham, avocate � la Cour d Alger, agr��e � la Cour supr�me et au Conseil d Etat, est l auteur de nombreuses contributions dans diff�rents s�minaires et forums internationaux sur les aspects juridiques relatifs aux cons�quences des explosions nucl�aires fran�aises au Sahara alg�rien.
Lors d un colloque international, ayant pour th�me "L'impact humanitaire des armes nucl�aires", organis� r�cemment dans la capitale fran�aise, la juriste alg�rienne avait d�clar� que les exp�riences et explosions nucl�aires au Sahara alg�rien "demeurent un parfait crime contre l humanit� et de ce fait restera imprescriptible donc vou� � contraindre leurs auteurs � reconnaitre d'abord puis � r�parer les dommages constat�s".
Elle a �galement relev� le caract�re "s�lectif et limitatif" de la loi Morin relative � la reconnaissance et l indemnisation des victimes des essais nucl�aires, dans la mesure o� il est fait abstraction des victimes alg�riennes et des d�g�ts environnementaux caus�s au Sahara alg�rien.
