TITRE: La 64e Berlinale met l'Asie à l'honneur et la Chine en particulier - 16 février 2014 - Le Nouvel Observateur
DATE: 2014-02-15
URL: http://tempsreel.nouvelobs.com/topnews/20140215.AFP0175/berlinale-boyhood-de-linklater-parmi-les-favoris-pour-l-ours-d-or.html
PRINCIPAL: 0
TEXT:
Actualité > TopNews > La 64e Berlinale met l'Asie à l'honneur et la Chine en particulier
La 64e Berlinale met l'Asie à l'honneur et la Chine en particulier
Publié le 15-02-2014 à 11h00
Mis à jour le 16-02-2014 à 13h20
A+ A-
Berlin (AFP) - La 64e édition du Festival du film de Berlin, la Berlinale, a mis samedi l'Asie à l'honneur et la Chine en particulier en décernant l'Ours d'or et le prix du meilleur acteur au thriller chinois "Black coal, thin ice".
Il a fallu un long moment au réalisateur Diao Yinan pour commencer à s'exprimer. "C'est dur de croire qu'un rêve devienne réalité", a-t-il dit avant de remercier toute son équipe avec laquelle il a tenu à partager le prix.
"Black coal, thin ice", à l'esthétique particulièrement soignée, débute en 1999 avec la découverte de plusieurs corps découpés en morceaux. Le policier Zhang Zili (Liao Fan, récompensé samedi soir de l'Ours d'argent du meilleur acteur) enquête avec des collègues jusqu'à une fusillade dont il est le seul survivant.
Détruit, il devient alcoolique et survit en étant gardien. Jusqu'à ce que quelques années plus tard, les meurtres recommencent et cette fois semblent tous reliés à une femme qui tient une teinturerie et dont il va tomber amoureux.
L'acteur Liao Fan a remercié la Berlinale en révélant qu'elle lui faisait "un merveilleux cadeau pour son 40e anniversaire" avec le prix d'interprétation.
La Chine a raflé encore l'Ours d'argent de la meilleure contribution artistique pour "Blind massage" de Lou Ye, sur la vie quotidienne de masseurs aveugles. Le réalisateur est venu recevoir son prix accompagné d'une de ses actrices aveugles.
La Japonaise Haru Kuroki s'est vue décerner l' Ours d'argent de la meilleure actrice pour "The little house" du vétéran japonais Yoji Yamada.
Elle est apparue en costume traditionnel, comme un lien avec son rôle d'épouse discrète dans ce film que le cinéaste a réalisé pour "que les jeunes générations n'oublient pas les atrocités de la Seconde guerre mondiale", avait-il dit devant la presse.
L'Ours d'argent du meilleur réalisateur est allé sans trop de surprise à l'Américain Richard Linklater pour "Boyhood", ode au temps qui passe.
Le film reste surtout une expérience unique dans le cinéma de fiction: il a été tourné en 39 jours sur une période de douze ans avec les mêmes acteurs. Patricia Arquette et Ethan Hawke interprètent un couple de parents, Ellar Coltrane et Lorelei Linklater, leurs enfants, que les spectateurs voient grandir, comme s'ils étaient eux-mêmes devenus des membres de cette famille américaine.
Les Etats-Unis repartent encore avec le prix spécial du jury pour Wes Anderson et son très loufoque "Grand Budapest hotel", film d'ouverture de la Berlinale. L'histoire d'un concierge ultra stylé d'un grand hôtel de la République fictive de Zubrowka, soumise aux affres de l'histoire.
La France a reçu un prix décerné à un "film qui ouvre de nouvelles perspectives" au cinéma, grâce au nouvel opus d'Alain Resnais "Aimer, boire et chanter".
"Quelle plus belle récompense pouvions nous espérer" a dit sur scène le producteur du film Jean-Louis Livi qui a qualifié Alain Resnais de "champion de l'innovation".
"Aimer boire et chanter" -
"Aimer boire et chanter" est un savoureux mélange de théâtre, cinéma et bande dessinée comme le vétéran français, Ours d'argent à Berlin en 1993 pour "Smoking/No smoking", en a le secret.
Deux jeunes Français Caroline Poggi et Jonathan Vinel ont reçu pour leur part l'Ours d'or du meilleur court métrage pour "Tant qu'il nous reste des fusils à pompe". L'Ours d'argent dans cette catégorie est revenu à un autre Français, Guillaume Cailleau, qui vit en Allemagne , pour "Laborat".
L'an dernier, l'Ours d'or avait été remporté par "Child's Pose" du Roumain Calin Peter Netzer, histoire d'une mère de famille aisée qui cherche à protéger son fils responsable d'un accident de la route mortel.
Le jury était présidé cette année par le producteur américain James Schamus ("Tigre et dragon", "Le secret de Brokeback Mountain") et avait dans ses rangs les acteurs autrichiens Christoph Waltz ou chinois Tony Leung ainsi que le réalisateur français Michel Gondry et la coproductrice des James Bond, Barbara Broccoli.
Au cours de cette 64e Berlinale, un hommage a été rendu au réalisateur britannique Ken Loach, 77 ans, qui s'est vu attribué un Ours d'or d'honneur pour l'ensemble de son oeuvre.
Partager
