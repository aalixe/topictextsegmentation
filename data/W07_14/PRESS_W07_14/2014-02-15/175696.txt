TITRE: Faits divers | Nouvelle tempête sur la Bretagne
DATE: 2014-02-15
URL: http://www.ledauphine.com/faits-divers/2014/02/15/nouvelle-tempete-sur-la-bretagne-kjbo
PRINCIPAL: 175695
TEXT:
Skichrono
intempéries - Ulla souffle à l�??ouest Nouvelle tempête sur la Bretagne
Un homme promenant ses chiens hier, au Guilvinec (Finistère).  Météo-France annonçait des pointes à 130 km/h sur le littoral.  Photo AFP
Météo-France a de nouveau placé hier en vigilance orange six départements du Nord-Ouest. Des rafales à plus de 100 km/h étaient attendues à l�??intérieur des terres, faisant craindre des submersions sur les parties exposées du littoral, ainsi que de nouvelles crues.
La quasi-totalité des vols ont été annulés dans les aéroports de Brest, Lorient ou Quimper.
Près de 100 000 foyers étaient privés d�??électricité hier soir dans les quatre départements bretons, dont la moitié dans la Finistère, suite aux coupures de lignes provoquées par la tempête.
Cette fois la responsable s�??appelle Ulla, dernière tempête en date de la longue série endurée par la Bretagne ces dernières semaines. Vents violents, trombes d�??eau, grêle, houle, inondations, coupures de courant, dégâts le long du littoral : dans le Finistère, tout particulièrement touché, les intempéries n�??ont laissé quasiment aucun répit aux habitants. �? Brest, le record de précipitations a été battu : 636 mm de pluie depuis le 15 décembre, du jamais vu selon Météo-France.
« On a eu une succession de perturbations depuis le début de l�??hiver avec des épisodes un peu tempétueux », reconnaît Frédéric Nathan, prévisionniste à Météo-France, qui assure cependant que ce type d�??épisode n�??est pas inédit. « �?a fait partie de la variabilité du climat », estime-t-il, tout en soulignant l�??intensité d�??Ulla, qui se trouve actuellement au-dessus de l�??Irlande.
Les agriculteurs mobilisés
�?galement touchés par ces intempéries à répétition, les agriculteurs de la FNSEA se sont mobilisés hier dans les deux tiers des départements français pour réclamer le droit d�??entretenir les fossés et cours d�??eau, meilleur rempart selon eux pour éviter les inondations.
« On dénonce le fait que l�??agriculteur a perdu la compétence de curer les cours d�??eau. Aujourd�??hui, nous continuons à le faire pour éviter que les champs soient inondés, mais on est systématiquement pénalisés pour cela », dénonce Damien Greffin, président de la FNSEA Ile-de-France.
L�??Office national de l�??eau et des milieux aquatiques (Onema) conteste toutefois en partie ces revendications. Selon lui, les agriculteurs ont bien le droit d�??entretenir les cours d�??eau, mais doivent demander depuis 2006 une autorisation pour procéder à des curages avec intervention mécanique.
Publié le 15/02/2014 à 06:00
Vos commentaires
