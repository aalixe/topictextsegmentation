TITRE: Serie A: Balotelli délivre l'AC Milan  - Italie - Football.fr
DATE: 2014-02-15
URL: http://www.football.fr/italie/scans/serie-a-balotelli-delivre-l-ac-milan-521988/
PRINCIPAL: 175773
TEXT:
14 février 2014 � 23h07
Mis à jour le
Réagir 5
Grâce à un but exceptionnel de Balotelli – une frappe des 30 mètres décochée à la 86e minute – l’AC Milan s’est imposée sur le fil devant Bologne ce vendredi soir en ouverture de la 24e journée de Serie A (1-0). Une dixième réalisation cette saison qui permet aux Rossoneri de se replacer provisoirement dans la première partie de tableau. Les Bolognais, eux, restent scotchés au 16e rang.
