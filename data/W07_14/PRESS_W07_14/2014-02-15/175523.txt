TITRE: Sotchi 2014: la B�larusse Tsuper s�empare de l�or au saut acrobatique | JO - lesoir.be
DATE: 2014-02-15
URL: http://www.lesoir.be/468342/article/sports/jo/2014-02-14/sotchi-2014-belarusse-tsuper-s-empare-l-or-au-saut-acrobatique
PRINCIPAL: 175522
TEXT:
Sotchi 2014: la B�larusse Tsuper s�empare de l�or au saut acrobatique
R�daction en ligne
vendredi 14 f�vrier 2014, 22 h 13
Sa 5�me participation aux Jeux olympique aura donc �t� la bonne.
Sur le m�me sujet
Notre dossier sur les JO
La B�larusse Alla Tsuper a remport� vendredi la m�daille d�or du saut acrobatique, une des disciplines du ski freestyle, � l��ge de 34 ans et � sa cinqui�me participation aux jeux Olympiques.
En 1998, Tsuper avait pris la 5e place des Jeux de Nagano sous les couleurs de l�Ukraine, son pays de naissance.
Elle avait quitt� ce pays la m�me ann�e pour suivre son entra�neur au Belarus et avait ensuite particip� pour son pays d�adoption aux JO en 2002 (9e), ann�e o� elle a remport� le classement g�n�ral de la Coupe du monde, en 2006 (10e) et en 2010 (8e).
�Lors de mes premiers Jeux (en 1998), je n��tais pas nerveuse du tout. Mais � chaque fois, �a devenait plus dur. Alors cette ann�e, j�ai d�cid� de faire comme si ces Jeux de Sotchi �taient mes premiers et je n��tais pas du tout nerveuse�, a indiqu� celle qui est devenue � 34 ans et 304 jours la plus �g�e des m�daill�s olympiques de ski freestyle.
Alla Tsuper n��tait pas favorite
A Sotchi, Tsuper �tait loin de porter un costume de favorite: elle n�est revenue sur les tremplins que cette saison apr�s une pause de deux ans pour fonder une famille, elle n�avait encore jamais remport� de m�daille en grands championnats puisqu�elle a fait chou blanc lors de ses cinq participations aux Mondiaux, en plus des quatre pr�c�dents Jeux, et sa derni�re victoire en Coupe du monde remonte � janvier 2009, � Lake Placid (Etats-Unis).
La Chinoise Xu Mengtao, championne du monde en titre, a pris la m�daille d�argent � cause d�une main pos�e sur la neige � la r�ception de son saut. La Chine, une superpuissance de la discipline, n�a encore jamais d�croch� un titre olympique en saut acrobatique f�minin mais compte quatre m�dailles d�argent et une de bronze.
La 3e place est revenue � Lydia Lassila, m�daill�e d�or il y a quatre ans � Vancouver (Canada). L�Australienne a tent� le saut le plus difficile du concours mais n�a pas r�ussi � se r�ceptionner correctement.
Li Nina (4e), vice-championne olympique en 2010 et 2006, a �galement particip� � la derni�re phase de la finale mais n�a pas r�ussi � accrocher une troisi�me r�compense olympique. La championne du monde 2009 et actuelle leader de la Coupe du monde a chut� � la r�ception de son saut final, le moins difficile des quatre concurrentes restant en lice.
