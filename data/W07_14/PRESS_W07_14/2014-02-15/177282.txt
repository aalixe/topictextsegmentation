TITRE: Ligue 1. Rennes et Nantes accroch�s, Lorient et Guingamp s'inclinent
DATE: 2014-02-15
URL: http://www.ouest-france.fr/ligue-1-rennes-et-nantes-accroches-lorient-et-guingamp-sinclinent-1933845
PRINCIPAL: 177161
TEXT:
Ligue 1. Rennes et Nantes accroch�s, Lorient et Guingamp s'inclinent
France -
Les Nantais ont marqu� un bon point � Nice. �|�Photo : Philippe Ch�rel / Ouest-France
Facebook
Achetez votre journal num�rique
Aucun vainqueur � l'Ouest, Rennes et Nantes ont pris un point face � Montpellier et Nice. Lorient et Guingamp ont perdu.
Le Stade Rennais a bien cru tenir un succ�s pr�cieux face � Montpellier mais c'�tait sans compter sur Souleymane Camara qui a �galis� dans les derni�res minutes du match. Et sans un Costil d�cisif sur ce m�me Camara deux minutes plus tard l'addition aurait pu �tre plus sal�e.�
Lorient ne pouvait pas�
R�duits � 10, les Lorientais ont pourtant ouvert le score gr�ce � Lautoa. Mais les Toulousains ont su r�agir pour inscrire 3 buts et s'offrir un beau succ�s.
Nantes a ramen� un bon point de son d�placement � Nice, aucun but dans ce match.�
Reims bat Bordeaux�
Gr�ce � Nicolas De Pr�ville, les R�mois ont pris le meilleur sur des Bordelais bien timides. Une victoire important pour les hommes d'Hubert Fournier.�
Guingamp coule � Sochaux
La d�faite des Guingampais � Sochaux (1-0) est inqui�tante. Les joueurs de Jocelyn Gourvennec n'ont plus connu la victoire depuis le 30 novembre dernier.�
Les r�sultats :
