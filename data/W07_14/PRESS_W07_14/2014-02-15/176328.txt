TITRE: Avancement des fonctionnaires : Ayrault écrit aux syndicats - 15 février 2014 - Le Nouvel Observateur
DATE: 2014-02-15
URL: http://tempsreel.nouvelobs.com/social/20140215.OBS6495/avancement-des-fonctionnaires-ayrault-ecrit-aux-syndicats.html
PRINCIPAL: 176327
TEXT:
Le Premier ministre a adress� une lettre aux syndicats de fonctionnaires dans laquelle il confirme qu'il n'y aura aucune mesure de gel.
Jean-Marc Ayrault, le 18 décembre 2013. (Sebastien Muylaert/Wostok Press/Maxppp)
Dans une lettre adress�e aux f�d�rations syndicales de fonctionnaires, Jean-Marc Ayrault assure qu'"aucune mesure" ne serait prise pour geler l'avancement des fonctionnaires, a indiqu� l'Unsa, samedi 15 f�vrier :
Je vous confirme ce que j'ai d�clar� publiquement, � savoir qu'il n'y aura pas de baisse du pouvoir d'achat des fonctionnaires et qu'aucune mesure ne sera prise qui aurait pour objet de geler l'avancement des agents publics"
Jeudi dernier, le Premier ministre avait d�j� d�menti l'hypoth�se d'un gel des primes et de l'avancement des fonctionnaires pour r�duire les d�penses publiques, mais les syndicats avaient demand� un engagement par �crit .
Dans sa lettre, Jean-Marc Ayrault affirme �galement "le besoin de r�nover notre mod�le de fonction publique". "J'ai bien not� que les organisations repr�sentatives des fonctionnaires �taient pr�tes � mener une discussion sur l'avenir de notre fonction publique dans le contexte de r�tablissement de nos comptes publics", �crit-il.
Ouverture de n�gociations
Le Premier ministre rappelle qu'il a demand� � la ministre de la Fonction publique, Marylise Lebranchu, d'ouvrir avec les syndicats cette n�gociation "qui doit reposer sur des engagements r�ciproques, car dans la situation budg�taire que nous connaissons la fonction publique doit participer � l'effort de redressement du pays". "Nous pouvons y parvenir par des �conomies et des r�organisations", affirme-t-il.
La F�d�ration des fonctionnaires de l'Unsa se r�jouit de la r�ponse du Premier ministre , qui "sauvegarde l'avancement des fonctionnaires" et "met un terme aux rumeurs sur un �ventuel gel de leur d�roulement de carri�re".
Cette lettre "est un signal d'apaisement" et "est de nature � permettre un retour � un dialogue social plus serein", estime le syndicat.
"Victimes d'un blocage du point d'indice depuis quatre ans, travaillant dans des conditions souvent d�grad�es, cibles d'un ' fonctionnaire bashing' trop souvent convenu, les fonctionnaires n'auront donc pas � subir une injustice suppl�mentaire", se r�jouit Unsa fonction publique .
Partager
