TITRE: Les compositions : avec Isimat, sans Abidal -  Planete-ASM.fr - Toute l'actualit� de l'AS Monaco
DATE: 2014-02-15
URL: http://www.planete-asm.fr/actualite/13563-les-compositions-avec-isimat-sans-abidal.html
PRINCIPAL: 176755
TEXT:
Le programme des entra�nements
07/04/14 | Abdoulkader Thiam participera au Tournoi de Montaigu en Vend�e avec la France U16 du 13 au 22 avril prochain.
07/04/14 | L1 J32 (dimanche) : Saint-Etienne 1-1 Nice // Valenciennes 1-2 Lyon. L'ASSE et l'OL ont respectivement 55 et 51 points.
05/04/14 | L1 J32 (samedi, suite) : Toulouse 1-2 Lille. Le LOSC revient provisoirement � trois points de l'AS Monaco.
05/04/14 | L1 J32 (samedi, suite) : Bastia 2-2 Sochaux // Bordeaux 2-2 Rennes // Guingamp 1-2 Montpellier // Lorient 1-1 �vian TG
05/04/14 | L1 J32 (samedi) : Victoire 3-0 du Paris Saint-Germain contre le Stade de Reims. Le PSG prend provisoirement 16 points d'avance sur l'ASM.
04/04/14 | L1 J32 (vendredi) : L'Olympique de Marseille s'est impos� 3-1 contre l'AC Ajaccio et compte d�sormais 48 points.
