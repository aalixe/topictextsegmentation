TITRE: Arnaud Montebourg et Elsa Zylberstein seraient en couple - L'Express
DATE: 2014-02-15
URL: http://www.lexpress.fr/styles/vip/arnaud-montebourg-et-elsa-zylberstein-seraient-en-couple_1324107.html
PRINCIPAL: 175633
TEXT:
Voter (10)
� � � �
Selon l'hebdomadaire "Paris Match", le ministre Arnaud Montebourg et l'actrice Elsa Zylberstein fileraient le parfait amour.
Reuters
Apr�s Fran�ois Baroin et Mich�le Laroque puis, bien s�r, Fran�ois Hollande et Julie Gayet , c'est au tour d' Arnaud Montebourg d'�tre en couple avec une com�dienne. Selon l'hebdomadaire Paris Match , le ministre du Redressement productif filerait le parfait amour avec Elsa Zylberstein.�
Le magazine explique qu'Arnaud Montebourg, 50 ans, et Elsa Zylberstein, 44 ans, se sont rencontr�s chez l'�crivain Marek Halter , le 8 septembre 2013, lors d'une f�te de la Fraternit� r�publicaine pour c�l�brer Roch hachana, le nouvel an juif. De nombreux invit�s �taient pr�sents, dont Yamina Benguigui , Manuel Valls et... Audrey Pulvar , l'ex-compagne d'Arnaud Montebourg. C'est lors du d�ner que les tourtereaux se seraient d�couverts. Ils seraient ins�parables depuis.�
�
