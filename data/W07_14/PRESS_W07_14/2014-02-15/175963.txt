TITRE: DIRECT. Temp�te Ulla : 70 000 foyers sans courant en Bretagne, deux morts au Royaume-Uni
DATE: 2014-02-15
URL: http://www.francetvinfo.fr/meteo/tempete/direct-tempete-ulla-90-000-foyers-toujours-prives-d-electricte-quatre-departements-restent-en-vigilance-orange_530647.html
PRINCIPAL: 175958
TEXT:
DIRECT. Temp�te Ulla : 70 000 foyers sans courant en Bretagne, deux morts au Royaume-Uni
La temp�te balaie le nord-ouest de la France et le Royaume-Uni.�
Une vague s'�crase sur le phare du Guilvinec (Finist�re), lors du passage de la temp�te Ulla, le 14 f�vrier 2014. (JEAN-SEBASTIEN EVRARD / AFP)
, publi� le
15/02/2014 | 10:12
Trois d�partements (Ille-et-Vilaine, Morbihan et Loire-Atlantique)�sont toujours plac�s en vigilance orange par M�t�o France samedi 15 f�vrier pour risques d'inondations, alors que la temp�te Ulla a balay� dans la nuit la nord-ouest du pays.
Francetv info fait le point sur les cons�quences de cette nouvelle d�pression hivernale.�
� Des coupures de courant. Quelque 70 000 foyers sont� toujours priv�s d'�lectricit� samedi matin , selon�ERDF. Parmi eux :�43�000 sont dans le Finist�re, 24�000 dans les C�tes-d'Armor, 2�000 dans le Morbihan et un millier en Ille-et-Vilaine. Le courant devrait �tre r�tabli au fil du week-end, a-t-on pr�cis�.
� Un mort au large de la Bretagne.�Un octog�naire est mort apr�s avoir lourdement chut� � bord d'un paquebot qui faisait route au large de la�Bretagne.
� Des perturbations dans les transports. Le trafic ferroviaire reprend progressivement samedi matin. Le trafic entre Rennes et Saint-Brieuc, ainsi qu'entre Rennes et�Quimper�a pu �tre r�tabli.�En revanche, celui entre�Brest�et�Quimper�et entre Saint-Brieuc�et�Brest�reste interrompu dans les deux sens. La SNCF�a pris en charge quelque 800 voyageurs dans la nuit.
� Chaos au Royaume-Uni. La France n'est pas le seul pays � �tre frapp� par Ulla. Outre-Manche, la temp�te a d�j� tu� deux personnes et prive 140�000 foyers d'�lectricit�. Les services m�t�o ont mis en garde contre de nouvelles inondations, alors qu'une partie du Royaume-Uni a d�j� les pieds dans l'eau. La Tamise pourrait atteindre des niveaux record.
Retrouvez ici l'int�gralit� de notre live #METEO
22h54 : �Apr�s le passage de la temp�te, l'heure des vid�os souvenir. France 3 Bretagne a compil� quelques vid�os publi�es par des internautes , t�moignant de la violence des vents�d'Ulla, comme ici dans le Finist�re Sud.
22h21 : �La temp�te Ulla continue de faire des d�g�ts. France 3 revient sur la� situation au Royaume-Uni , o� deux personnes ont �t� tu�es.
(JULIEN GASPARUTTO - FRANCE 3)
21h43 : �#Ulla #ERDF #Bretagne Retour � la normale pr�vu ce soir pour le #Morbihan et l'#IlleEtVilaine. Les agents restent mobilis�s
21h43 : �#Ulla #ERDF (2/3)  Ce soir il restera 20 000 clients coup�s ds le #Finist�re et 10 000 en #CotesDArmor.
21h43 : �#Ulla #ERDF (1/2) 75% des foyers bretons coup�s, sont r�tablis ce soir. Il restera 30 000 clients sans �lectricit� ce soir en #Bretagne.
21h42 : �@anonyme : L'�lectricit� revient progressivement dans les chaumi�res bretonnes, comme le signale ERDF sur Twitter. C�t� m�t�o, le Morbihan, l'Ille-et-Vilaine et la Loire-Atlantique sont en vigilance orange "inondation" jusqu'� demain 16 heures.
21h42 : �Comment �volue actuellement la situation en Bretagne ?
20h56 : #METEO�La temp�te Ulla a touch� le Royaume-Uni, comme ici � Tendy, au Pays de Galles. Une photo � retrouver dans notre� diaporama �sur les�intemp�ries.
(REBECCA NADEN / REUTERS)
18h48 : �Environ 50 000 clients sont toujours priv�s d'�lectricit� � cause de la temp�te Ulla, selon ERDF. Le Finist�re est le plus touch� avec 33 000 clients sans courant, devant les C�tes-d'Armor (15 000) ainsi que le Morbihan et l'Ille-et-Vilaine.�L'alimentation devrait �tre r�tablie d'ici � la fin du week-end.
17h38 : �Avec les temp�tes la plage de Sainte Marie de R� est en ce moment un d�potoir et un cimeti�re. https://t.co/saOHuL7uFz �
17h37 : �Les temp�tes ne cessent de causer des d�g�ts, notamment sur l'�le de R� (Charente-Maritime). Un journaliste de Lib�ration�a d�couvert "un d�potoir et un cimeti�re"�en se promenant sur une plage.
16h55 : �Le Royaume-Uni se mobilise face aux intemp�ries, jusqu'au sommet de l'Etat. Le Premier ministre David Cameron et le prince William sont venus braver la pluie pour soutenir les sinistr�s. Des photos � retrouver dans� notre diaporama sur la temp�te Ulla �en Bretagne et en Grande-Bretagne.
(MAXPPP - PAUL HACKETT/REUTERS)
15h51 : �Apr�s le grand blocage de la nuit derni�re, le r�seau de train des C�tes-d'Armor a retrouv�, depuis 13 heures, un fonctionnement normal, rapporte� Ouest-France . 24 000 foyers encore priv�s d'�lectricit� d'apr�s le dernier rapport d'ERDF.�
15h36 : �Ulla, la plus forte temp�te de l'hiver, laisse derri�re elle de l'�cume jusqu'aux cuisses dans Saint-Gu�nol�, et des arbres et cat�naires � terre � Quimper (Finist�re).
�MATHIEU BOISSEAU - FRANCE 2 et FRANCE 3
15h06 : �La situation est alarmante et les inondations sont d�j� historiques� en Grande-Bretagne . La Tamise, ce week-end pourrait atteindre par endroit son plus haut niveau depuis 60 ans. L'arm�e est mobilis�e pour faire face � ces intemp�ries hors-normes.
�
�CLEMENT LE GOFF et THOMAS DONZEL - FRANCE 2 LONDRES�
15h02 : �Brest n'avait pas connu une telle temp�te depuis 24 ans. Des vents de�133 km/h enregistr�s hier soir en ville. Pour rappel, en 1987, le vent �tait mont� � 148 km/h, rapporte� Ouest-France .�
13h58 : �La temp�te Ulla a d�j� tu� deux personnes au Royaume-Uni et prive en ce moment 140�000 foyers d'�lectricit�. Les services m�t�o ont mis en garde contre de nouvelles inondations, alors qu'une partie du pays a d�j� les pieds dans l'eau. La Tamise pourrait atteindre des niveaux record. Toutes les derni�res informations sont � retrouver� dans notre direct .�
13h16 : �Selon un dernier bilan d'ERDF, 70 000 foyers sont encore priv�s d'�lectricit�.�Les trains circulent de nouveau entre Lorient et Quimper, et devaient reprendre � 13 heures entre Saint-Brieuc et Brest, selon un porte-parole de la SNCF.
13h06 : #METEO�La� temp�te Ulla �a fait des d�g�ts outre-manche, comme ici � Londres o� une partie d'un immeuble s'est effondr�e sur un voiture, tuant�une personne.�
�JUSTIN TALLIS / AFP
11h48 : �Dans la nuit les vents de� la temp�te Ulla �ont atteint les 140 � 150 km/h � la Pointe de Bretagne et de 130 � 135 km/h au nord�d'une ligne Brest-Morlaix. C�t� pluie, �Brest, il est tomb� 636 mm depuis le 15 d�cembre, un record, selon M�t�o-France.
10h01 : �70 conteneurs, de couleur grise, rouge et orange, qui ne transportent aucune mati�re dangereuse, ont �t� perdus hier soir par un cargo. Le navire, battant pavillon danois, �tait alors en sortie descendante du rail�d�Ouessant, � environ 60 nautiques (100 km) de Brest.�Un avis urgent aux navigateurs a �t� �mis par la pr�fecture maritime de l�Atlantique.
