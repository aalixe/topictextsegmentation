TITRE: Florent Pagny: �On me fait passer pour un ringard dans The Voice� (vid�o) - sudinfo.be
DATE: 2014-02-15
URL: http://www.sudinfo.be/936597/article/fun/tele/the-voice/2014-02-15/florent-pagny-on-me-fait-passer-pour-un-ringard-dans-the-voice-video
PRINCIPAL: 177149
TEXT:
Pourquoi les coaches de �The Voice� ont toujours la m�me tenue?
Photo News
Dans une interview accord�e � T�l� Loisirs , Florent Pagny a confi� que le montage de TF1 dans The Voice avait ��vex� sa femme�� car ��il passait pour un ringard��. Malheureusement, le chanteur a affirm� n�avoir ��aucun regard dessus��.
��C�est vrai que ma femme �tait un peu vex�e en regardant les d�buts des auditions � l�aveugle. Elle trouvait qu�on me montrait un peu comme le ringard des coachs. Je lui ai dit que je n�y pouvais rien et qu�ils montaient l��mission dans l�ordre qu�ils voulaient en fonction de l�histoire qu�ils ont envie de raconter��, a-t-il expliqu�. ��Moi, je peux vous dire que le premier jour des enregistrements � l�aveugle, j�avais d�j� cinq talents dans mon �quipe. J��tais m�me le coach qui en avait le plus��, a-t-il conclu.
Les + lus
