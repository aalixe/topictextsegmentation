TITRE: Monuments Men: la tourn�e promotionnelle de George Clooney f�che - L'Express
DATE: 2014-02-15
URL: http://www.lexpress.fr/culture/cinema/monuments-men-la-tournee-promotionnelle-de-george-clooney-fache_1324408.html
PRINCIPAL: 177064
TEXT:
Voter (0)
� � � �
Le r�alisateur et acteur George Clooney a estim� samedi � Berlin que tout ce qui contribuait � faire parler des oeuvres d'art vol�es par les nazis et de leur restitution comme dans son dernier film "Monuments men" �tait "une bonne chose".
afp.com/Johannes Eisele
George Clooney vit pleinement son personnage. Et m�me apr�s le tournage, il continue de se prendre pour un justicier de l'art. Dans son dernier film, Monuments Men, George Clooney fait partie d'une brigade charg�e de r�cup�rer les oeuvres d'art vol�es par les nazis. Un r�le qui l'a visiblement profond�ment marqu� puisque depuis qu'il est en tourn�e de promotion, il demande aux pays visit�s de bien vouloir rendre leurs oeuvres phares � leur v�ritables propri�taires.�
C'est lors de la pr�sentation du film � la 64e Berlinale que l'acteur-r�alisateur am�ricain a commenc� � vouloir mettre de l'ordre dans les oeuvres d'art. En tourn�e � Berlin le 8 f�vrier, George Clooney a propos� que les frises du Parth�non conserv�es au British Museum soient rendues � Ath�nes, comme le rapporte France TV Info . Ce "serait une bonne id�e", a d�clar� l'acteur, avant d'assurer aux Grecs qu'ils avaient "le droit de (leur c�t�)".�
�
�
Le maire Londres compare Clooney � Hitler
Une proposition salu�e par les Grecs, qui consid�rent que ces frises ont �t� vol�es par les Anglais. "Au nom de tous les Grecs, je vous adresse un grand merci pour votre d�claration ", a �crit le ministre de la Culture, Panos Panagiotopoulos. Et en guise de remerciement, il a invit� George Clooney � venir "passer quelques jours en Gr�ce [...] pour voir une multitude d'antiquit�s grecques conserv�es sous le soleil m�diterran�en. Et, bien s�r, visiter le nouveau mus�e de l'Acropole, en face du rocher sacr�, o� une place attend le retour des marbres du Parth�non en exil involontaire."�
Mais Londres, qui ne parle pas des frises du Parth�non mais des "marbres d'Elgin", ne l'entend pas du tout de la sorte. John Whitingdale , ancien d�put� responsable d'une commission sur la culture a affirm� que George Clooney ne connaissait "pas l'histoire des marbres d'Elgin et le droit des Anglais dessus". Pire, le maire de Londres, Boris Johnson , a accus� George Clooney d'avoir des objectifs dignes d'Hitler . Pour sa part, le British Museum, qui conserve les frises, s'est content� de r�affirmer que les marbres "font partie de leurs collections"�
Nouveau pays, nouvelle pol�mique. A Milan, c'est Paris qui a subi les foudres du justicier Clooney. D'apr�s Le Guardian , l'acteur a estim� que la France devrait rendre � l'Italie La Joconde. De nombreuses p�titions italiennes ont d�j� demand� le retour de la fameuse oeuvre d'art, mais la France a toujours refus�, estimant que le tableau �tait trop d�licat � transf�rer. La France r�agira-t-elle � cette demande de l'acteur? �
�
