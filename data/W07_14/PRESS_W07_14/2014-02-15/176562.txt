TITRE: SYRIE. Paris et Londres condamnent "l'attitude du régime syrien" - 15 février 2014 - Le Nouvel Observateur
DATE: 2014-02-15
URL: http://tempsreel.nouvelobs.com/guerre-en-syrie/20140215.OBS6500/syrie-ne-pas-parler-de-la-transition-serait-une-perte-de-temps.html
PRINCIPAL: 176561
TEXT:
Actualité > Guerre en Syrie > SYRIE. Paris et Londres condamnent "l'attitude du régime syrien"
SYRIE. Paris et Londres condamnent "l'attitude du régime syrien"
Publié le 15-02-2014 à 14h46
Mis à jour à 18h50
L'opposition syrienne refuse elle de nouvelles n�gociations avec le r�gime s'il refuse de parler de transition politique. Le m�diateur de l'ONU n'a fix� aucune date pour une reprise des discussions.
Le ministre des Affaires étrangères Laurent Fabius à Genève. (Sipa)
La France "condamne l'attitude du r�gime syrien qui a bloqu� toute avanc�e", a d�clar� samedi 15 f�vrier le ministre des Affaires �trang�res, Laurent Fabius, apr�s l'�chec des n�gociations sur la Syrie � Gen�ve .
"La deuxi�me session de n�gociations vient de s'achever sans succ�s � Gen�ve", a confirm� Laurent Fabius dans un communiqu�.
Je condamne l'attitude du r�gime syrien qui a bloqu� toute avanc�e sur l'�tablissement d'un gouvernement de transition et multipli� les violences et les actes de terreur a l'encontre des populations civiles", a-t-il ajout�.
Laurent Fabius "salue le courage et le sens des responsabilit�s de la Coalition nationale syrienne qui a adopt� une position constructive tout au long des n�gociations".�"Ceux qui exercent une influence sur le r�gime doivent l'amener au plus vite � respecter les demandes de la communaut� internationale", a poursuivi le ministre dans une claire allusion � la Russie.
"Un s�rieux revers"
M�me r�action de Londres. Le ministre britannique des Affaires �trang�res, William Hague, a jug� responsable le r�gime de Bachar-al-Assad de l'�chec des n�gociations.
L'impossibilit� de s'entendre sur le programme des prochaines sessions de n�gociations �[...] repr�sente un s�rieux revers en vue de trouver la paix en Syrie, et la responsabilit� en incombe directement au r�gime d'Assad", a d�clar� William Hague dans un communiqu�.
Les commentaires du m�diateur de l'ONU, Lakhdar Brahimi, lors de sa conf�rence de presse samedi matin "mettent clairement en �vidence que le r�gime a refus� de discuter d'une autorit� gouvernementale de transition, un sujet qui est au coeur des n�gociations et qui repr�sente un moyen essentiel de mettre fin au conflit", a poursuivi le chef de la diplomatie britannique.
L'�chec de ces n�gociations "peut marquer la fin de la route. Mais avec la guerre en Syrie qui, chaque jour, fait toujours plus de morts et cause toujours plus de d�g�ts, nous avons le devoir, vis-�-vis du peuple syrien, de tout faire pour enregistrer des progr�s en vue d'une solution politique", a-t-il encore dit, apportant son "plein soutien � Lakhdar Brahimi".
Londres a par ailleurs estim� qu'il �tait d�sormais encore "plus urgent de parvenir � s'entendre sur une r�solution du Conseil de s�curit� de l'ONU pour r�pondre � la souffrance humanitaire consternante en Syrie". "Les habitants des r�gions syriennes assi�g�es et de nombreuses zones du pays qui ne re�oivent pas d'aide ne peuvent pas attendre", a estim� William Hague.
"Que chaque partie r�fl�chisse � ses responsabilit�s"
Apr�s le refus, selon Lakhdar Brahimi, de la d�l�gation du gouvernement syrien d'appliquer l'ordre du jour, le m�diateur de l'ONU a mis fin samedi aux discussions inter-syriennes qui �taient dans l'impasse depuis trois semaines � Gen�ve . Il�n'a fix� aucune date pour une reprise,�pour donner � chacun un temps de r�flexion.
Je pense qu'il est pr�f�rable que chaque partie rentre et r�fl�chisse � ses responsabilit�s, et [dise] si elle veut que ce processus continue ou non", a d�clar�le m�diateur de l'ONU�� la presse.
Rendant compte de l'ultime rencontre, le m�diateur a expliqu� que les deux parties avaient adopt� des positions difficilement conciliables :�"Le gouvernement consid�re que la question la plus importante est le terrorisme, l'opposition consid�re que la question la plus importante est l'autorit� gouvernementale de transition", a-t-il dit ajoutant qu'il avait propos� d'�voquer d'abord "la violence et le terrorisme" pour passer ensuite au probl�me de "l'autorit� gouvernementale".
Malheureusement le gouvernement a refus�, provoquant chez l'opposition le soup�on qu'ils ne veulent absolument pas parler de l'autorit� gouvernementale de transition", a ajout� le m�diateur.�
"J'esp�re que les deux parties vont r�fl�chir un peu mieux et reviendront pour appliquer le communiqu� de Gen�ve", adopt� en juin 2012 par les grandes puissances comme plan de r�glement politique de ce conflit qui dure depuis pr�s de trois ans.�"J'esp�re que ce temps de r�flexion conduira en particulier le gouvernement � rassurer l'autre partie (sur le fait) que quand ils parlent d'appliquer le communiqu� de Gen�ve ils comprennent que l'autorit� gouvernementale transitoire doit exercer les pleins pouvoirs ex�cutifs.", a ajout� Lakhdar�Brahimi.�
L'exercice des "pleins pouvoirs ex�cutifs" reviendrait � priver le pr�sident Bachar al Assad de ses pouvoirs, m�me si cela n'est pas �crit explicitement dans le communiqu�, d'o� le blocage de Damas sur ce point.�
Lakhdar Brahimi s'est par ailleurs dit "tout � fait d�sol�" et s'est excus� "aupr�s du peuple syrien dont les espoirs �taient si grands".
"Une perte de temps"
"Un troisi�me round sans parler de la transition serait une perte de temps", a estim� le porte-parole de la d�l�gation de l'opposition � Gen�ve, Louay Safi.
"Le r�gime n'est pas s�rieux [...], nous ne sommes pas ici pour n�gocier le communiqu� de Gen�ve mais pour l'appliquer", a-t-il ajout� � propos du plan de r�glement politique en Syrie adopt� par les grandes puissances.
Nous devons �tre s�r que le r�gime veut une solution politique et pas des tactiques pour gagner du temps", a encore affirm� Louay�Safi aux journalistes.
"Je suis d�sol� de dire qu'il n'y a rien de positif que nous puissions retenir", a-t-il estim� � propos de ces n�gociations sous m�diation de l'ONU entam�es le 22 janvier sous la pression de la communaut� internationale, en particulier les parrains russe et am�ricain de la Conf�rence.
C�t� gouvernemental, le chef des n�gociateurs, l'ambassadeur syrien aupr�s de l'ONU Bachar Jafari s'en est tenu � accuser l'opposition "de ne pas respecter l'agenda", affirmant qu'il fallait d'abord conclure "par une vision commune" sur le premier point, la lutte contre la violence et le terrorisme avant de passer � un autre.
Un appel jeudi aux parrains russes et am�ricains de la Conf�rence n'a produit aucun effet et les Etats-Unis interpellent d�sormais publiquement la Russie pour lui demander d'en faire plus pour obtenir de la flexibilit� de la part de son alli�, le r�gime syrien :�"Nous demandons aux Russes, tr�s franchement, d'en faire beaucoup plus, parce qu'il n'y a pas beaucoup de pays qui peuvent avoir une influence sur le r�gime", a soulign� vendredi la porte-parole adjointe du D�partement d'Etat, Marie Harf.
Le conflit syrien, qui dure depuis trois ans, a fait plus de 140.000 morts, selon l'Observatoire syrien des droits de l'homme (OSDH), une organisation non-gouvernementale syrienne qui s'appuie sur un large r�seau de sources m�dicales et de militants � travers le pays.
Partager
