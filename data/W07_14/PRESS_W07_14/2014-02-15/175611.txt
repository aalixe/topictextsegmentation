TITRE: Une femme abattue avenue de la Grande Arm�e, dans le 8e arrondissement de Paris
DATE: 2014-02-15
URL: http://www.francetvinfo.fr/faits-divers/une-femme-abattue-avenue-de-la-grande-armee-dans-le-8e-arondissement-de-paris_530365.html
PRINCIPAL: 175609
TEXT:
Tweeter
Une femme abattue avenue de la Grande Arm�e, dans le 8e arrondissement de Paris
Selon les informations de France 2, il s'agit de la g�rante du bistrot L'Aiglon. Elle a �t� abattue par son ex-mari, qui a �t� interpell�.
Capture d'�cran de Google Stret View du�bistrot "L'Aiglon", avenue de la Grande Arm�e,�� Paris (16e arrondissement). (  FRANCETV INFO )
, publi� le
14/02/2014 | 18:59
"Un crime passionnel" commis le jour de la Saint-Valentin.�Une femme de 42 ans a �t� abattue, vendredi 14 f�vrier en fin d'apr�s-midi,�avenue de la Grande Arm�e, dans le 16e arrondissement de Paris. Selon les informations de France 2 qui cite une source proche de l'enqu�te, la victime est la g�rante du bistrot L'Aiglon, situ� pr�s de l'Arc de triomphe. Elle�aurait �t� tu�e par son ex-mari. L'homme de 45 ans�a �t� interpell�.� Le Parisien �pr�cise que "trois coups de feu ont �t� tir�s".�
Vers 18 heures, cet homme est entr� dans le bar, a racont� � l'AFP une�source proche de l'enqu�te. "Il a sorti une arme et a tir� � plusieurs reprises" sur la g�rante de l'�tablissement. La quadrag�naire�a �t� touch�e par trois projectiles "au niveau du thorax". Elle est morte quelques instants plus tard.
Peu apr�s, des policiers ont aper�u un homme sortant du bar "avec une arme � la main", et sont parvenus � l'arr�ter. L'homme est connu des services de police pour "des violences conjugales et des violences sur concubine". Le 1er district de la police judiciaire parisienne a �t� saisie de l'affaire.
Des t�moins ont publi� sur Twitter des photos prises sur le lieu du drame. Les abords du restaurant ont �t� interdits d'acc�s par la police.
#rtl avenue de la Grande Arm�e - Paris 16�me...... que se passe-t-il ces Messieurs de la Police refusent de le dire.. pic.twitter.com/pwino6hWEG
