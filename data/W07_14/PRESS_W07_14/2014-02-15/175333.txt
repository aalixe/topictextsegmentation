TITRE: La candidature de l'�pouse du maire in�ligible de Propriano fait pol�mique - � la une - Actualit�s sur orange.fr
DATE: 2014-02-15
URL: http://actu.orange.fr/une/la-candidature-de-l-epouse-du-maire-ineligible-de-propriano-fait-polemique-afp-s_2831266.html
PRINCIPAL: 175327
TEXT:
14/02/2014 � 15:57
La candidature de l'�pouse du maire in�ligible de Propriano fait pol�mique
L'interview embarrassante de Caroline Bartoli, l'�pouse de l'actuel maire de Propriano, en Corse, frapp� d'in�ligibilit�, a braqu� les projecteurs sur cette candidate aux municipales dont le seul but est de garder la place pour son mari.
r�agir 40 �
photo : PASCAL POCHARD CASABIANCA, AFP
La candidature de l'�pouse du maire (PRG) de la station baln�aire corse Propriano, en lieu et place de son mari in�ligible, serait peut-�tre pass�e inaper�ue, sans une prestation t�l�vis�e calamiteuse de la candidate.
"Ma femme n'est pas un pantin" : le gros titre barrait vendredi la Une de Corse-Matin, soulignant l'embarras du maire sortant Paul-Marie Bartoli, qui a demand� � son �pouse de conduire la liste avant de se repr�senter lui-m�me en mai, au terme de sa p�riode d'in�ligibilit�.
Attaquant pour mieux se d�fendre, en stigmatisant "une pol�mique scandaleuse qui salit une m�re de famille", Paul-Marie Bartoli tente, comme l'�crit le journal r�gional, "de d�samorcer le 'buzz' cr�� par l'interview de son �pouse" Caroline � France 3 Corse.
voir (0)��
Corse : attentat contre un caf� dans la ville de Corte
Le 6 f�vrier, Caroline Bartoli est interrog�e sur le plateau de Corsica Sera, le journal t�l�vis� du d�but de soir�e de la cha�ne r�gionale, qui re�oit chaque jour durant cinq minutes un candidat aux municipales dans l'�le. L'exercice tourne rapidement au fiasco. T�tanis�e, Mme Bartoli, qui n'a aucune exp�rience, parvient � peine � r�pondre, en lisant laborieusement des fiches et se bornant � dire, embarrass�e, qu'elle poursuivrait la politique de son mari.
L'ampleur du malaise est att�nu� par le professionnalisme et m�me la bienveillance de l'intervieweur, Jean-Vitus Albertini, l'un des journalistes les plus chevronn�s de Corse.
Depuis, Paul-Marie Bartoli accuse France 3 d'avoir "pi�g�" son �pouse et lui-m�me, qui est officiellement directeur de campagne, Jean-Vitus Albertini r�pliquant que le maire avait "assist� � l'enregistrement de l'interview et n'a jamais dit qu'il souhaitait qu'on la refasse ou qu'elle ne soit pas diffus�e".
L'�lu, par ailleurs membre du Conseil ex�cutif de la Collectivit� territoriale de Corse, o� il occupe le poste sensible de pr�sident de l'Office des transports, a �t� d�clar� en 2013 in�ligible pour un an par le Conseil constitutionnel, qui n'a pas valid� ses comptes de campagne. Le Conseil lui reproche notamment de ne pas avoir justifi� de quelques milliers d'euros de frais de bouche.
Paul-Marie Bartoli, ne pouvant se repr�senter avant le 25 mai, envisage de briguer un troisi�me mandat au printemps, apr�s la d�mission, d�j� annonc�e, de sa conjointe.
Depuis l'�cho rencontr� par l'entretien t�l�vis�, largement visionn� par les internautes, il ne d�col�re pas. "C'est scandaleux de s'en prendre comme cela � une m�re de famille. On la ridiculise, on la salit", s'insurge-t-il dans Corse-Matin avant de d�noncer une "pol�mique due � de l'acharnement m�diatique". "Nous ne sommes pas les Balkany", dit-il, en r�f�rence au couple Isabelle et Patrick Balkany, maire UMP de Levallois-Perret (Hauts-de-Seine) r�guli�rement pl�biscit� en d�pit de ses ennuis judiciaires.
"Mon seul juge sera le corps �lectoral", dit encore l'�lu proprianais. "Nous allons l'emporter avec plus de 66,66% des voix", assure-t-il.
En attendant, une chape d'embarras s'est abattue sur le petit port de la c�te occidentale de l'�le, qui fonctionne au ralenti en attendant les beaux jours. Tout le monde se conna�t dans ce gros village de 4.000 habitants et rares sont ceux qui souhaitent commenter ces p�rip�ties �lectorales. Des voix s'�l�vent toutefois pour d�noncer la pi�tre image des femmes que renvoie cet �pisode.
Mais pour le principal opposant de Paul-Marie Bartoli, le nationaliste Jacques Luciani, "personne n'est dupe". Ancien premier adjoint du maire sortant, ce m�decin, qui a d�missionn� pour pr�senter sa propre liste, stigmatise tout autant "le mode de fonctionnement archa�que et anti-d�mocratique et la d�rive autocratique et carri�riste" de Paul-Marie Bartoli, que sa politique d'encouragement de la sp�culation immobili�re et de la multiplication des r�sidences secondaires � Propriano.
