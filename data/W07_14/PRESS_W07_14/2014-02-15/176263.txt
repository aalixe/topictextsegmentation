TITRE: En Bretagne, 70 000 foyers restent priv�s d��lectricit� apr�s la temp�te Ulla� | La-Croix.com
DATE: 2014-02-15
URL: http://www.la-croix.com/Actualite/France/En-Bretagne-90-000-foyers-restent-prives-d-electricite-apres-la-tempete-Ulla-2014-02-15-1107032
PRINCIPAL: 176259
TEXT:
petit normal grand
En Bretagne, 70 000 foyers restent priv�s d��lectricit� apr�s la temp�te Ulla�
Le passager d�un navire voguant au large de la Bretagne est d�c�d� d�une mauvaise chute, � cause des vents ayant atteint les 150�km/h
15/2/14 - 11 H 44
- Mis � jour le 15/2/14 - 18 H 21
Mots-cl�s :
R�agir 0
JEAN-SEBASTIEN EVRARD / AFP
Les agents de ERDF r�parent une ligne �lectrique endommag�e par la temp�te Ulla, � Landevennec, en Bretagne ( AFP PHOTO / JEAN-SEBASTIEN EVRARD)
JEAN-SEBASTIEN EVRARD / AFP
Les agents de ERDF r�parent une ligne �lectrique endommag�e par la temp�te Ulla, � Landevennec, en Bretagne ( AFP PHOTO / JEAN-SEBASTIEN EVRARD)
Avec cet article
Un hiver 2013-2014 marqu� par une m�t�o exceptionnelle
La temp�te Ulla qui a touch� la Bretagne a provoqu� d�importantes coupures d��lectricit� dans le Finist�re et les C�tes d�Armor et l�arr�t du trafic de trains
Le trafic ferroviaire reprend progressivement ce samedi 15�f�vrier en Bretagne, apr�s les perturbations dues � la temp�te Ulla qui a balay� tout l�ouest de la France. ��Les trains ont �t� stopp�s en gare par mesure de s�curit�, pour �viter qu�ils se retrouvent en pleine voie pour toute la nuit��, a expliqu� la SNCF.
En cons�quence, quelque 500 voyageurs ont d� �tre h�berg�s pour la nuit du vendredi au samedi dans des rames s�curis�es en gare de Rennes. � Saint-Brieuc, ce sont 300 personnes qui ont �t� prises en charge par la SNCF, dont 200 ont pass� la nuit dans des rames s�curis�es et 100 dans une salle des f�tes ouverte par la ville.
Des vents � 150�km/h ont �t� enregistr�s dans la nuit. Les mauvaises conditions m�t�o ont provoqu� la chute mortelle d�un passager � bord d�un paquebot naviguant au large de la Bretagne, vendredi soir. Une femme � bord du m�me paquebot a �t� �vacu�e par h�licopt�re � la suite �galement d�une chute, a indiqu� la pr�fecture maritime de l�Atlantique.
Coupures d��lectricit� et inondations
Le vent violent qui avait commenc� � souffler dans la journ�e de vendredi, a plong� dans le noir pr�s de 100�000 foyers en Bretagne, selon �lectricit� R�seau distribution France (ERDF). Quelque 70�000 foyers �taient toujours priv�s de courant, samedi � la mi-journ�e, principalement dans le Finist�re et les C�tes-d�Armor.
Les intemp�ries se sont �galement traduites par des inondations. Il y a eu jusqu�� 40�cm d�eau sur les quais de Quimperl�, selon la pr�fecture du Finist�re, et de l�gers d�bordements � Morlaix et Landerneau. En outre, de nombreuses routes ont �t� coup�es en raison d�inondations. � Brest, il est tomb�, depuis le 15�d�cembre, 636�mm de pluie, un record, selon M�t�o France.
��On a eu une succession de perturbations depuis le d�but de l�hiver avec des �pisodes un peu temp�tueux��, reconna�t Fr�d�ric Nathan, pr�visionniste � M�t�o France, soulignant l�intensit� de ��Ulla��, la perturbation actuelle. ��Nous avons l�, sur la pointe de la Bretagne, la temp�te la plus forte depuis le d�but de l�hiver��, assure-t-il, avant d�annoncer pour la semaine prochaine un certain ��r�pit��.
La Croix (avec AFP)
