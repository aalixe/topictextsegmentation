TITRE: PSG | PSG : Lavezzi et M�nez, les deux cibles de Pierre M�n�s
DATE: 2014-02-15
URL: http://www.le10sport.com/football/ligue1/psg/psg-lavezzi-et-menez-les-deux-cibles-de-pierre-menes136093
PRINCIPAL: 176304
TEXT:
�
��MALADROIT, COMME D�HAB��
Sur son compte Twitter, le journaliste n�a as h�sit� � critiquer la finition et le manque d�efficacit� de certains attaquants du PSG�: ��Je le r�p�te ce PSG rate beaucoup trop d'occasions. C'est toujours par l� que �a p�che (�) Lavezzi�? il a �t� maladroit comme d'hab��, explique Pierre M�n�s. Pourtant, l�international argentin n�avait plus livr� une telle prestation avec le PSG depuis un certain temps.
�
M�NEZ PAS PLUS CONVAINCANT�
Dans une autre conversation sur Twitter�: un "follower" de Pierre M�n�s lui demande�: ��Comment s'est comport� Menez sur ce match, lui qui a �t� titularis� enfin?��, avant que le journaliste ne lui lance un ��Bof�� pour le moins symbolique. Siffl� par le public du Parc des Princes, J�r�my M�nez a d�ailleurs plut�t mal v�cu cette situation�: ��C�est dur, �a ne fait jamais plaisir, on ne va pas se le cacher. Mais dans une carri�re c�est comme �a. L�ann�e derni�re il n�y avait pas de sifflets, cette ann�e il y en a, c�est comme �a. Il faut savoir faire avec et travailler encore plus pour montrer aux gens qu�on est capable de mieux��, a souffl� l�attaquant du PSG.
�
� Pierre M�n�s (@PierreMenes) 14 F�vrier 2014
�
