TITRE: Sortie de piste de MMA � Sotchi : "Ca fait partie du sport" - Sport - MYTF1News
DATE: 2014-02-15
URL: http://lci.tf1.fr/sport/sortie-de-piste-de-mma-a-sotchi-ca-fait-partie-du-sport-8365894.html
PRINCIPAL: 176172
TEXT:
jeux olympiques , ski
SportC'�tait la seule Fran�aise engag�e en vitesse. La fran�aise Marie Marchand-Arvier a termin� ses jeux Olympiques par une sortie de piste en super-G, samedi matin � Rosa Khoutor, apr�s avoir d�j� chut� en descente mercredi. En bas de la piste, la jeune championne ne cachait pas sa d�ception.
Sa descente aura dur� 11 secondes, trois portes. Avant qu'un cri de rage ne�d�chire le calme blanc.�La fran�aise�Marie Marchand-Arvier, seule Fran�aise engag�e en vitesse, a termin� ses jeux Olympiques par une sortie de piste en super-G, samedi matin � Rosa Khoutor.�"Mes jeux, je les ai pr�par� avec beaucoup d'envie, d'ambition et de prrofessionnalisme. C'est d'autant plus dur de les r�ter, a d�clar� sur LCI la skieuse, visiblement tr�s �mue � son arriv�e en contrebas de la piste. �Maitenant, �a fait partie du sport, avec des �motions tr�s fortes qui vont dans les deux sens. Ben l�, c'est pas forc�ment des �motions tr�s joyeuses", conclut-elle la voix �trangl�e par la d�ception.
Vice-championne du monde de la discipline en 2009 � Val d'Is�re, la skieuse des Contamines-Montjoie n'a pas �t� la seule � se faire pi�ger sur le trac� dessin� par l'entra�neur autrichien Florian Winkler. En fait, des dix premi�res concurrentes � s'�lancer, seule trois skieuses ont pass� la ligne d'arriv�e. Au final, c'est l'Autrichienne Anna Fenninger qui,�� 24 ans, remporte�son premier titre olympique en s'imposant sur la ligne d'arriv�e� avec un chrono de 1 min 25 sec 52/100e, devant l'Allemande Maria H�fl-Riesch et une autre Autrichienne Nicole Hosp, respectivement 2e � 55/100e et 3e � 66/100e.
Commenter cet article
Ecrire votre commentaire ici ...
Vous devez �crire un avis
matchproduguido : Elle est sp�cialiste des gamelles comme tous ces athl�tes qui ne se pr�parent pas assez. Rien ne sert de s'entrainer longtemps si ce n'est pas pour s'entrainer durement (dans les conditions de course). L'entraineur de l'�quipe de France a raison de dire qu'on ne peut tenter le hold-up sur chaque comp�tition parce qu'on a pas voulu faire l'effort avant.
Le 15/02/2014 � 17h10
