TITRE: Cyclisme - Tour M�diterran�en: Cummings leader | francetv sport
DATE: 2014-02-15
URL: http://www.francetvsport.fr/tour-mediterraneen-cummings-leader-206983
PRINCIPAL: 177087
TEXT:
Cyclisme - Tour M�diterran�en: Cummings nouveau leader
Publi� le 15/02/2014 | 18:50, mis � jour le 15/02/2014 | 18:51
Le Britannique Stephen Cummings (AFP - Jaime Reina)
Le Britannique Stephen Cummings (BMC/Racing Team) a endoss� le maillot de leader du Tour M�diterran�en apr�s avoir remport� samedi la 4e �tape, un contre-la-montre de 18,6 km trac� autour de Saint-R�my de Provence.  Il a devanc� l'Autrichien Riccardo Zoidl (Trk Factory Racing) de 4 secondes, et le Fran�ais Sylvain Chavanel (IAM Cycling) de 10 secondes.
Apr�s les premi�res �tapes disput�es au sprint qui n'avaient pas permis de creuser d'�cart, 56 coureurs �taient class�s dans le m�me temps au g�n�ral derri�re l'ancien leader John�Degenkolb. Il �tait �crit que ce contre-la-montre allait rebattre les cartes, et que la situation allait forc�ment se d�canter.
Tout reste ouvert d�sormais, au moins entre les dix premiers�du classement, sur un parcours davantage destin� aux rouleurs passant bien les difficult�s, avant la derni�re �tape dimanche entre Bandol et Toulon, avec le Mont Faron au programme.�
Classement de la 4e �tape
1. Stephen Cummings (ENG/BMC Racing Team) les 18,6 km en� 24:27. (moyenne:� 44,639 km/h)
2. Riccardo Zoidl (AUT) � �� �4.
3. Sylvain Chavanel (FRA) � � �10.
4. Jean-Christophe P�raud (FRA) � �15.
5. Eduardo Sepulveda (ARG) � � �34.
6. Tobias Ludvigsson (SWE) �� �35.
7. Bob Jungels (LUX) � �� �37.
8. Ben Hermans (BEL � � �43.
9. J�r�my Roy (FRA) � � �44.
10. Thomas Degand (BEL) � � �46.
Classement g�n�ral
1. Stephen Cummings (ENG/BMC Racing Team) 11 h 33:55
2. Riccardo Zoidl (AUT)��� � 4.
3. Sylvain Chavanel (FRA)�� � 10.
4. Jean-Christophe P�raud (FRA) � 15.
5. Eduardo Sepulveda (ARG)�� � 34.
6. Tobias Ludvigsson (SWE)� � 35.
7. Bob Jungels (LUX)��� � 37.
8. Ben Hermans (BEL�� � 43.
9. J�r�my Roy (FRA)�� � 44.
10. Thomas Degand (BEL)�� � 46.
