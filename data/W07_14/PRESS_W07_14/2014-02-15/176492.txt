TITRE: Star Wars : la saison 6 in�dite de Clone Wars sur Netflix !
DATE: 2014-02-15
URL: http://www.cinemovies.fr/actu/star-wars-la-saison-6-inedite-de-clone-wars-sur-netflix/26586
PRINCIPAL: 176491
TEXT:
13H45 Le 15/02/14 S�ries 0 publi� par Nicolas Germain
Star Wars : la saison 6 in�dite de Clone Wars sur Netflix !
Apr�s l'annulation de Star Wars : The Clone Wars, une nouvelle s�rie de la saga intergalactique sera propos�e � la fin de l'ann�e. Mais Clone Wars va pourtant revenir pour une saison 6 in�dite, que proposera Netflix en exclusivit�.
Yoda combat le c�t� obscur de la Force gr�ce � Netflix ! � Lucasfilm
Tweet
Retrouvez aussi l'actu cin�ma sur notre page Facebook
Il y a un an, le rachat de Lucasfilm par Disney pr�cipitait la fin de Star Wars: The Clone Wars diffus�e sur Cartoon Network, rapidement remplac�e par une nouvelle s�rie anim�e , Star Wars Rebels . Pourtant, 13 �pisodes de Star Wars: The Clone Wars �taient encore dans les tiroirs, et ceux-ci vont finalement �tre propos�s aux t�l�spectateurs.
En effet, Netflix ( d�j� sauveuse de The Killing ), a annonc� la mise � disposition le 7 mars prochain de cette saison 6 in�dite intitul�e "The Lost Missions". Mais ce n'est pas tout, car le service de VOD proposera �galement les cinq saisons pr�c�dentes, incluant plusieurs �pisodes au format director's cut, jamais vus � la t�l�vision.
Dans ces derniers �pisodes de Clone Wars, "les plus grands myst�res du conflit entre les c�t�s lumineux et obscurs de la Force sont r�v�l�s, tandis qu'un clone intr�pide d�couvre un secret choquant. La relation d'Anakin Skywalker avec la personne la plus proche de lui est test�e jusqu'� sa limite, et ce que d�couvre Ma�tre Yoda en enqu�tant sur la disparition d'un Jedi pourrait changer � tout jamais l'�quilibre du pouvoir dans la galaxie".
Rappelons que Star Wars Rebels sera quant � elle diffus�e � la fin de l'ann�e sur Disney XD et se d�roulera entre entre Star Wars III : la revanche des Sith et Star Wars IV : un nouvel espoir .
Plus sur
Star Wars: The Clone Wars
1 article
