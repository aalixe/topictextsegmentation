TITRE: Supercombin�: Viletta emp�che le sacre de Kostelic - L'Orient-Le Jour
DATE: 2014-02-15
URL: http://www.lorientlejour.com/article/854658/supercombine-viletta-empeche-le-sacre-de-kostelic.html
PRINCIPAL: 175454
TEXT:
Abonnez-vous
En enlevant l'or du supercombin� aux Jeux de Sotchi, le Suisse Sandro Viletta a emp�ch� le sacre du Croate Ivica Kostelic, qui n'a pu ramener, pour la quatri�me fois, que l'argent dans la collection familiale lanc�e par sa petite s�ur Janica.
Comme sa compatriote Dominique Gisin en descente mercredi, Viletta a d�jou� les pronostics et sorti la manche de slalom de sa vie pour apporter � la Suisse son second titre en ski alpin en quatre �preuves.
��C'est incroyable !�� a l�ch� le Suisse, n'en revenant pas lui-m�me de son coup. Son seul fait d'arme jusque-l� �tait une victoire en Coupe du monde, lors du super-g de Beaver Creek en d�cembre 2011.
Viletta, 28 ans, est dernier bourreau de la famille Kostelic, dont il vient de tuer probablement � jamais le r�ve du vieux p�re. Ante Kostelic, qui a enseign� le ski � ses deux enfants sur les hauteurs de Zagreb, aurait tant voulu voir son a�n� lui aussi champion olympique.
��Les Am�ricains disent que le second est le premier perdant. Bien s�r que je voulais l'or, je croyais vraiment avoir ma chance aujourd'hui, mais malheureusement, il y en a un qui a �t� tr�s inspir� aujourd'hui��, a soulign� Kostelic en d�signant du regard Viletta.
10e m�daille des Kostelic
Parmi tous les pr�tendants, le Croate, 34 ans, semblait en effet le mieux plac� au terme de la descente en matin�e. Non seulement le champion du monde de slalom 2003 avait conc�d� moins d'une seconde aux cadors de la vitesse, mais il pouvait aussi compter sur une manche de slalom trac� sur mesure par papa.
��Je sais que certains critiquent sa mani�re de tracer. C'est un peu la vieille �cole, a soulign� Kostelic. Je pr�f�re cela non pas parce que c'est mon p�re, mais parce que ce sont des slaloms qui vous obligent � skier avec intelligence et qui ne donnent jamais un vainqueur par accident.��
Pour la petite histoire, Ivica est pass� � 34/100e de l'or, douze ans jour pour jour apr�s le tout premier des quatre titres de sa petite s�ur Janica, qu'elle avait d�croch� en combin�, � Salt Lake City. ��Les Jeux olympiques inspirent profond�ment ma famille. Aujourd'hui, c'est notre dixi�me m�daille, et cela, j'en suis tr�s fier��, a insist� le d�sormais quadruple vice-champion olympique.
Quelle que soit la couleur, l'Italien Christof Innerhofer rafle tout ce qu'il peut. Le champion du monde de super-g 2011 s'est offert, avec le bronze, sa seconde m�daille en deux courses apr�s l'argent de la descente dimanche.
��Au d�part du slalom, je ne pensais � rien car je croyais n'avoir aucune chance��, a racont� l'Italien, �pat� lui-m�me d'avoir sign� le m�me chrono entre les piquets qu'un sp�cialiste comme Kostelic.
��C'�tait juste mon r�ve d'arriver � faire une m�daille ici, maintenant j'en ai deux, c'est incroyable��, a ajout� le triple m�daill� des Mondiaux 2011, qui, avec le super-g dimanche, a de grandes chances de poursuivre sa collection.
Si l'Italien ne croyait pas avoir la moindre chance, ceux qui pensaient en avoir de grandes sont bien pass�s � la trappe.
� commencer par le jeune Fran�ais Alexis Pinturault qui, en enfourchant en slalom, a rat� sa premi�re course olympique. L'Am�ricain Bode Miller avait hypoth�qu� ses chances avec une grosse faute dans la descente, mais avec une 6e place, il fait mieux que son compatriote Ted Ligety, le champion du monde (12e).
�AFP�
