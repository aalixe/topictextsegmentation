TITRE: OL: Gourcuff encore forfait
DATE: 2014-02-15
URL: http://www.lyonpremiere.com/OL-Gourcuff-encore-forfait_a4286.html
PRINCIPAL: 176993
TEXT:
Le milieu offensif de l'OL, Yoann Gourcuff a de nouveau d�clar� forfait pour le match OL-Ajaccio de la 25e journ�e de Ligue 1, dimanche au stade de Gerland.
�
Gourcuff souffre des adducteurs et �tait d�j� indisponible jeudi pour le match de coupe de France perdu contre Lens (2-1 apr�s prolongation).
�
Apr�s avoir aussi �t� forfait contre Rennes, en championnat, et Troyes en coupe de la Ligue, les 2 et 5 f�vrier, il avait jou� dimanche durant une heure � Nantes.
�
Par ailleurs, le milieu Steed Malbranque est lui aussi forfait pour le match contre Ajaccio en raison d'une fatigue aux adducteurs, lui aussi.
�
