TITRE: Var : un adolescent poignard� chez lui � Six-Fours-les-Plages - 15/02/2014 - leParisien.fr
DATE: 2014-02-15
URL: http://www.leparisien.fr/provence-alpes-cote-d-azur/var-un-adolescent-poignarde-chez-lui-a-six-fours-les-plages-15-02-2014-3594669.php
PRINCIPAL: 0
TEXT:
Une page Facebook a �t� cr��e pour rendre hommage � l'adolescent poignard�.
| CAPTURE D'ECRAN
R�agir
Diego, un adolescent de 17 ans a �t� retrouv� mort vendredi soir � son domicile de Six-Fours-les-Plages (Var). Ce samedi, le parquet de Toulon a indiqu� que toutes les pistes restaient ouvertes.
L'adolescent vivait seul depuis l'hospitalisation de son p�re et c'est sa s�ur qui, venue lui rendre visite, a d�couvert le corps, a indiqu� le procureur de la R�publique de Toulon, Xavier Tarabeux.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Selon les premiers �l�ments de l' enqu�te , l'adolescent a �t� tu� de plusieurs coups de couteau. �Les investigations men�es dans la nuit de vendredi � ce samedi autour n'ont pas permis de retrouver cette arme�, pr�cise �Var-Matin� . Une autopsie devrait �tre pratiqu�e lundi � Marseille.
Le jeune homme avait quelques ant�c�dents judiciaires et une proc�dure �tait en cours pour possession de stup�fiants, outrage et r�bellion, a pr�cis� le parquet. L'enqu�te a �t� confi�e � l'antenne de la police judiciaire de Toulon.
Quelques heures apr�s l'annonce du d�c�s du jeune homme, une page Facebook a �t� cr��e en sa m�moire . Une marche est pr�vue ce dimanche. Elle d�butera devant le coll�ge Reynier de Six-Fours, o� le jeune homme �tait scolaris� avant d'int�grer le CFA de Beausset, selon le quotidien .
LeParisien.fr
