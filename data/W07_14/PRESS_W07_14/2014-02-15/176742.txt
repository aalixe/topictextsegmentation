TITRE: Berger-Sabbatel croise les doigts pour Komissarova - BFMTV.com
DATE: 2014-02-15
URL: http://www.bfmtv.com/sport/berger-sabbatel-croise-doigts-komissarova-711952.html
PRINCIPAL: 176739
TEXT:
r�agir
Victime d�une chute d�une extr�me gravit�, ce samedi lors de l�entra�nement de skicross, Maria Komissarova souffrirait d�une fracture de la colonne vert�brale. Pr�sente sur les lieux de l�accident, Marielle Berger-Sabbatel, qui fait partie de l��quipe de France de skicross s�inqui�te pour l�athl�te russe�: ��J�ai appris �a apr�s l�entra�nement. J�ai d� partir quelques minutes avant elle. Le temps que je remonte, elle avait d�j� �t� �vacu�e. Je n�ai pas vu l�arr�t de course et je ne savais pas qu�elle �tait tomb�e. J�esp�re que �a va aller pour elle. On attend des nouvelles. Je sais qu�elle a d� se faire op�rer sur place car elle n��tait pas transportable, ce n�est pas bon signe. J�esp�re qu�elle va se remettre vite et qu�il n�y a rien de trop grave.�D�s que �a touche le dos, ce n�est pas cool. Je croise les doigts pour que �a se passe bien.��
Toute l'actu Sport
