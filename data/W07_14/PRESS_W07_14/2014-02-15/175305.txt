TITRE: La France va envoyer 400 soldats suppl�mentaires en Centrafrique - France - RFI
DATE: 2014-02-15
URL: http://www.rfi.fr/afrique/20140214-rca-soldats-europeens-peut-etre-bientot-casques-bleus-bangui/
PRINCIPAL: 175284
TEXT:
Modifi� le 14-02-2014 � 23:05
La France va envoyer 400 soldats suppl�mentaires en Centrafrique
Francisco Soriano, dirigeant de l'op�ration fran�aise Sangaris, le 5 f�vrier 2014 � Bangui.
REUTERS/Siegfried Modola
R�publique centrafricaine
Paris va renforcer sa pr�sence en R�publique centrafricaine, en d�p�chant sur le terrain 400 hommes suppl�mentaires, pour parvenir � 2000 hommes. Cette d�cision a �t� prise lors d'un conseil de d�fense � Paris, ce vendredi 14 f�vrier 2014.
Paris a longtemps h�sit� pour finalement s'y r�soudre. 400 hommes suppl�mentaires vont partir en RCA, officiellement pour r�pondre � l'appel du secr�taire g�n�ral de l'ONU, Ban Ki-moon, qui demandait mardi � la France d'envoyer des renforts. L'op�ration Sangaris va ainsi passer de 1�600 �l�ments, � 2�000.
Ce nouveau d�ploiement concerne � la fois des troupes de combats et des gendarmes, qui participeront ensuite � l'op�ration militaire europ�enne. Paris souhaite que cette op�ration se mette en place rapidement. La France a exhort� Bruxelles d'acc�l�rer le d�ploiement de l'Eufor et de la force de gendarmerie europ�enne, nouvellement cr��e.
D'apr�s la d�claration de la chef de la diplomatie de l'UE, Catherine Ashton, l'Union envisagerait finalement de d�ployer un millier de soldats sur place. L'Europe s'�tait engag�e initialement sur l'envoi d'une force de 500 hommes.
Des casques bleus � Bangui ?
Les Vingt-Huit pourraient commencer � d�ployer des troupes d�s le mois prochain. L'urgence de la situation, la crise g�n�ralis�e dans l'ensemble du pays, font que les 1�600 hommes de l'op�ration Sangaris et les 5�400 soldats de la Misca, la force africaine, ne suffisent pas. Les experts estiment qu'il faudrait au bas mot 10�000 hommes en RCA pour stabiliser la situation.
Jeudi 13 f�vrier, Fran�ois Hollande a demand� � l'ONU d'acc�l�rer l'envoi de casque bleus � Bangui. Mais une telle op�ration, bien qu'envisag�e dans la r�solution adopt�e � New York en d�cembre, se heurte aux r�ticences de plusieurs pays, comme les Etats-Unis mais aussi le Congo et le Tchad.
