TITRE: Incendie � Sevran: trois soeurs meurent dans l�appartement familial - Lib�ration
DATE: 2014-02-15
URL: http://www.liberation.fr/societe/2014/02/15/incendie-a-sevran-trois-soeurs-meurent-dans-l-appartement-familial_980451
PRINCIPAL: 176806
TEXT:
Un quartier de la ville de Sevran le 7 janvier 2014 (Photo Patrick Kovarik. AFP)
ARTICLE + VID�OS
Trois autres enfants, �galement pr�sents dans l�appartement familial, ont �t� transport�s � l�h�pital mais leur jours ne sont pas en danger.
Trois jeunes soeurs ont trouv� la mort vendredi dans un incendie qui s�est d�clar� au huiti�me �tage d�un immeuble de Sevran (Seine-Saint-Denis), et huit personnes, dont cinq pompiers, ont �t� l�g�rement bless�es, a-t-on appris de sources concordantes.
Les trois soeurs d�c�d�es �taient �g�es de 8, 11 et 18 ans, a affirm� � l�AFP le parquet de Bobigny, dont un repr�sentant s�est rendu sur place dans la soir�e. Leurs trois fr�res et soeurs, �galement pr�sents dans l�appartement familial au moment du sinistre, ont �t� intoxiqu�s et transport�s � l�h�pital mais leur jours ne sont pas en danger.
�
Incendie meurtrier � Sevran: des voisins... par liberationafp
�Les six enfants sont de la m�me famille�, a confirm�, � Sevran, le commandant des pompiers Gabriel Plus. Les trois survivants �ont �t� sauv�s par les pompiers�, a-t-il ajout�, pr�cisant que le feu ne s��tait pas propag� aux autres appartements de la tour.
Les parents, en revanche, n��taient pas pr�sents lorsque le feu s�est d�clar�. Egalement transport�s � l�h�pital, �ils sont en �tat de choc�, a d�clar� sur place le maire de Sevran St�phane Gatignon.
�L�incendie a d�marr� vers 19H45 dans un appartement situ� au 8e �tage d�un immeuble de 16 �tages�, a indiqu� � l�AFP la pr�fecture de Seine-Saint-Denis, confirmant une information de BFMTV. La famille occupait cet appartement de 80m�.
Le feu a �t� ma�tris�. L�origine du sinistre n�est pas encore connue mais un �accident domestique� est probable, a pr�cis� cette source pr�fectorale. L�enqu�te a �t� confi�e � la police judiciaire.
Parmi les sept bless�s, l�g�rement intoxiqu�s, quatre sapeurs-pompiers souffrent de br�lures l�g�res.
Pr�s de 180 pompiers et une trentaine de camions sont intervenus, a constat� une journaliste de l�AFP au pied de cette tour du quartier de Montcelleux. Le pr�fet de Seine-Saint-Denis s�est rendu sur place.
