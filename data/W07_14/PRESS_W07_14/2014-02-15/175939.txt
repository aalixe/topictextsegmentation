TITRE: JO 2014: pas de m�daille suisse au Super-G f�minin, l'or � Anna Fenninger - Dossiers - La C�te - Journal r�gional l�manique
DATE: 2014-02-15
URL: http://www.lacote.ch/fr/dossiers/jo-sotchi-2014/jo-2014-lara-gut-a-ete-sortie-du-podium-par-fenninger-hoefl-riesch-et-hosp-2769-1262081
PRINCIPAL: 175938
TEXT:
15.02.2014, 09:18 - JO Sotchi 2014
Actualis� le 15.02.14, 09:19
JO 2014: pas de m�daille suisse au Super-G f�minin, l'or � Anna Fenninger
Direct
La Tessinoise Lara Gut n'a pas encore atteint son objectif de d�crocher l'or olympique. Elle a encore �chou� en prenant la quatri�me place ce samedi en Super-G.
Cr�dit: KEYSTONE
Tous les commentaires (0)
Quatre Suissesses �taient engag�es dans le Super-G f�minin ce samedi. Mais la meilleure d'entre-elles, Lara Gut termine au pied du podium. L'Autrichienne Anna Fenninger remporte l'or devant l'Allemande Maria Hoefl-Riesch et l'Autrichienne Nicole Hosp.
Lara Gut, quatri�me au pied du podium doit commencer de r�aliser l'importance de sa m�daille de bronze d�croch�e en descente. "Oui, c'�tait 1 dixi�me l'autre jour, maintenant c'est 73 centi�mes sur Anna Fenninger et je rate surtout� le podium pour 7 centi�mes aujourd'hui (samedi). Je suis d��ue, mais c'est ainsi", l�chait la Tessinoise un brin d�pit�e.
C'est donc l'Autrichienne Anna Fenninger qui d�croche l'or de ce Super-G devant l'Allemande Maira Hoefl-Riesch pour 55 centi�mes et une autre Autrichienne, Nicole Hosp qui accusait 66 centi�mes de retard sur la gagnante.
Mais avec les 4e place de Lara Gut, la sixi�me de Fr�nzi Aufdenblatten et la septi�me de Fabienne Suter, les Suissesses, m�me bredouilles peuvent �tre satisfaites de leurs prestations sur ce difficile trac� o� m�me la championne olympique en titre, l'Autrichienne Elizabeth G�rgl est partie � la faute. Quant � la quatri�me Suissesse engag�e, la toute r�cente championne olympique de la descente, Dominique Gisin elle a elle aussi d� se d�clarer battue sur cette piste qui ne pardonne aucune erreur. Elle fait partie des douze concurrentes qui ont �t� contraintes � l'abandon.
9:04 Et c'est au tour de la Zermattoise Fr�nzi Aufdenblatten de s'�lancer sur une piste qui marque un peu dans le haut. Mais la Haut-Valaisanne de 33 ans qui s'en sortait bien sur premier tron�on perdait quelque peu le fil plus bas. Elle prend une jolie sixi�me place avec un chrono de 1:26.79 juste devant Fabienne Suter.
�8:46 L'Allemande Maria H�fl-Riesch pourra-t-elle d�loger Anna Fenninger? Elle prend la deuxi�me place derri�re Anna Fenninger et devant Nicole Hosp. Lara Gut n'est plus sur le podium. La d�ception de la Tessinoise doit �tre immense. L'Autricheinne Anna Fenninger est en train de r�aliser qu'elle devrait �tre en mesure de d�crocher l'or.
8:44 Elisabeth G�rgl (Aut) a tout risqu� dans l'enchainement de virages apr�s la bosse. A mauvais escient. Elle est aussi sortie de piste et est �limin�e. Lara Gut conserve pour le moment le bronze.
8:41 Lara Gut, qui doit faire attention de ne pas trop en faire, passe parfaitement le passage avant d'entamer une ligne parfaite dans le dernier mur. Mais ses efforts ne suffisent pas pour br�ler la politesse � Anna Fenninger. Elle prend la troisi�me place provisoire derriere les deux Autrichiennes.
8:39 La championne olympique en titre de la descente avec Dominique Gisin, la Slov�ne Tina Maze est � fond dans l'enchainement de virages mais commet deux petits drifts r�dhibitoires qui la placent � la troisi�me place finale. Fabienne Suter� a �t� �ject�e du podium.
8:36 L'Autrichienne Anna Pfenninger qui fait partie des grandes favorites de la discipline skie de mani�re tr�s concentr�e pour terminer sa course avec un nouveau temps de r�f�rence en 1:25 52. Un chrono qui devrait �tre difficile � aller chercher.
8.34 On entre dans le vif du sujet avec les monstres sacr�s de la discipline. L'Autrichienne Nicole Hosp est � �galit� avec Fabienne Suter � mi-parcours. Elle laisse bien aller ses skis sans paniquer. Elle s'installe logiquement en t�te de liste avec un chrono de 1:26.18
8:32� Quinze skieuses se sont �lanc�es sur ce difficile super-G, c'est toujours Fabienne Suter qui tient la baraque devant Julia Mancuso de 15 centi�mes et l'Allemande Viktoria Rebensburg pour 19 centi�mes.
8:28 L'Am�ricaine Julia Mancuso, qui avait remport� la descente du super-combin� devant Lara Gut, est venue "dedans" dans les virages de fin de parcours apr�s la fameuse bosse et s'installe � la deuxi�me place provisoire derri�re Fabienne Suter.
8:24 L'Italienne Verena Stuffer, qui s'est qualifi�e au tout dernier moment pour ces JO, parvient elle aussi en fin de course. En fin de parcours, la piste marque un peu. Mais l'Italienne prend la troisi�me place provisoire derri�re Suter, et sa compatriote Franchini.
8:21 C'est la championne olympique de descente, la Suissesse Dominique Gisin qui s'�lance dans ce super-G. Elle accuse un l�ger retard sur le haut. Elle �tait bien plac�e avec deux drifts dans le mur, mais elle bascule un peu trop � l'int�rieur... et chute � son tour. Nouvelle �limination.
8:17 Fabienne Suter s'�lance. Elle passe loin devant l'Am�ricaine au premier et au deuxi�me pointage. Mais ce qu'il faut c'est terminer! Elle n�gocie du mieux qu'elle peut la suite de virages et parvient enfin, deuxi�me skieuse de neuf, dans l'aire d'arriv�e avec une seconde et demi d'avance sur Leanne Smith en 1:26.89
8:17 Le fameux Solitary Jump laissera un bien mauvais souvenir aux skieuses.
8:13 Laurenne Ross (EU) d�bute avec beaucoup de r�serve. Lorsque toutes les concurrentes devant soi ont �t� �limin�es �a ne laisse pas en confiance. Et hop. Elle sort aussi de sa ligne. Elle est �limin�e!
8:12 C'est l'hecatombe en ce d�but de course. La Su�doise Jessica Lindell-Vikarby ma�trise mal une bosse et... sort de piste. M�me sort pour la Fran�aise�Marie-Mich�le Gagnon et la Canadienne Marie Marchand-Arvier. Une seule skieuse a pour le moment rejoint l'aire d'arriv�e sur six coureuses.
8:06� Daniela Merighetti, d�j� �limin�e � la descente, l'Italienne trop passive ne parvient pas � lutter pour chercher la derni�re porte et manque un virage. Elle est �limin�e elle aussi.�
8:04: C'est donc � l'Am�ricaine Leanne Smith d'�tablir le premier chrono. Elle passe le deuxi�me chrono interm�diaire en 34.66 et le 3e en 54.53. Elle termine difficilement dans les derniers virages et termine son run en 1:28.38
8:00 C'est parti avec l'Espagnole Carolina Ruiz Castillo qui s'entra�ne avec le groupe France qui ouvre les feux. Elle qui �tait d�j� sortie en descente, elle a vu sa chaussure toucher et partir � la faute apr�s quelques hectom�tres seulement! Elle est �limin�e!
7:58 La m�t�o de Sotchi est au beau fixe avec des temp�ratures tr�s �lev�es. Des conditions qui ne favoriseront pas forc�ment les skieuses. C'est l'Espagnole Carolina Ruiz-Castillo qui �tablira le premier chrono de r�f�rence sur le trac� de l'Autrichien Thomas Winkeler.
7:55 Parmi les quatre Suissesses en lice dans le super-G f�minin ce samedi matin sur la piste de Rosa Khutor, Lara Gut sera ind�niablement l'une des favorites. La Tessinoise sera la troisi�me concurrente helv�tique � s'�lancer avec son dossard num�ro 20.
Fabienne Suter passera le portillon de d�part en neuvi�me position, deux places devant la nouvelle championne olympique de descente, Dominique Gisin. La quatri�me repr�sentante helv�tique qui en d�coudra sur ce super-G, Fr�nzi Aufdenblatten partira avec le dossard n� 30. Suivez la course en live sur notre site et nos applications mobiles.
�
