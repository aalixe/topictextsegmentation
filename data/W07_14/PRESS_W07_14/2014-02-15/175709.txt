TITRE: Bain de soleil et d'amour sur la place Saint-Pierre - 7SUR7.be
DATE: 2014-02-15
URL: http://www.7sur7.be/7s7/fr/15736/Saint-Valentin/article/detail/1793645/2014/02/14/Bain-de-soleil-et-d-amour-sur-la-place-Saint-Pierre.dhtml
PRINCIPAL: 0
TEXT:
14/02/14 -  17h00��Source: Belga
� epa.
Vingt mille fianc�s ont commenc� � se rassembler vendredi matin place Saint-Pierre sous un beau soleil, � l'invitation du pape qui les a convoqu�s pour c�l�brer leur engagement dans le mariage � l'occasion de la Saint-Valentin.
� epa.
� reuters.
� afp.
Le Conseil pontifical de la famille, qui avait propos� cette initiative in�dite au pape, a �t� surpris par le nombre de participants: plus de 20.000 fianc�s issus de 25 pays - m�me s'ils proviennent en majorit� d'Italie. A l'issue d'une messe et de t�moignages, le pape devait les rejoindre et s'adresser � eux.
Les couples pr�sents suivent une pr�paration au mariage catholique. Ils ont tous re�u un petite coussinet blanc en soie sur lequel �taient imprim�es les armoiries du Vatican et la signature de Fran�ois.
Cette c�l�bration par le Vatican de la f�te des amoureux, une f�te la�que m�me si elle se r�f�re � un �v�que et martyr du IIIe si�cle, a �t� largement comment�e, y compris au Vatican.
"Le succ�s de l'initiative d�montre qu'il y a des jeunes � contre-courant qui d�sirent que leur amour dure pour toujours, m�me si le monde contemporain ne croit pas que les liens durent", s'est f�licit� le pr�sident du Conseil pontifical, l'Italien Vincenzo Paglia.
Oppos� au mariage gay, le pape argentin met l'accent sur le couple et la famille traditionnels, fondements selon lui de la promotion de l'homme et de la stabilit� sociale. Il y consacrera un consistoire et deux synodes en l'espace d'un peu plus d'un an.
