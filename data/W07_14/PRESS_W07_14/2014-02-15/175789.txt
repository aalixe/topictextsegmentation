TITRE: France Alzheimer Bigorre : �R�former la d�pendance� - 14/02/2014 - LaD�p�che.fr
DATE: 2014-02-15
URL: http://www.ladepeche.fr/article/2014/02/14/1818378-france-alzheimer-bigorre-reformer-la-dependance.html
PRINCIPAL: 175785
TEXT:
France Alzheimer Bigorre : �R�former la d�pendance�
Publi� le 14/02/2014 � 08:50
France Alzheimer Bigorre : �R�former la d�pendance�
France Alzheimer Bigorre veut mobiliser sur la question cruciale de la r�forme de la d�pendance. Trois questions � son pr�sident Francis Salas.
Comment allez-vous mobiliser les familles ?
Nous invitons � signer la p�tition nationale sur notre site lemanifeste.francealzheimer.org pour appuyer notre manifeste qui contribuera � �laborer la prochaine loi d�orientation et de programmation pour l�adaptation de la soci�t� au vieillissement annonc�e pour fin 2014. Nous comptons recueillir au moins 100.000 signatures.
Quel est ce manifeste ?
Il exprime le fait qu�il n�est pas normal que la d�pendance soit support�e par les familles. Il reprend sept mesures parmi lesquelles l�in�galit� territoriale devant l�Allocation personnalis�e d�autonomie (Apa), la suppression de la barri�re d��ge � 60 ans et la diminution du reste � charge � domicile (1.000 � en moyenne selon une �tude de notre association) comme en �tablissement (2.200 � en moyenne).
En effet, les familles sont doublement p�nalis�es par la perte d�autonomie : en plus de la charge humaine et de la fatigue, c�est une charge financi�re.
Pouvez-vous pr�senter votre association ?
France Alzheimer est n�e d�ailleurs du besoin des familles de se r�unir. Aujourd�hui, il y a une centaine d�associations en France dont France Alzheimer Bigorre, f�d�r�es � l�union nationale. Mais nous nous battons aussi pour toutes les familles touch�es par la perte d�autonomie.
Il y a 4.700 b�n�ficiaires de l�Apa dans notre d�partement. Selon l�Insee, ces chiffres doubleront en 2040.
Propos recueillis par Cyrille Marqu�
�|�
