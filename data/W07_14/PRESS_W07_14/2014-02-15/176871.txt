TITRE: Angers manque le coche, Brest respire - Ligue 2 - Football - Sport.fr
DATE: 2014-02-15
URL: http://www.sport.fr/football/ligue-2-angers-manque-le-coche-brest-respire-340272.shtm
PRINCIPAL: 176866
TEXT:
Angers manque le coche, Brest respire
Samedi 15 f�vrier 2014 - 16:23
Le SCO d'Angers a manqu� le coche � domicile contre Laval (1-1), lors de la 24e journ�e de Ligue 2 . C'est m�me Laval qui aurait pu l'emporter, si Toudic avait converti un penalty � la 86e minute de jeu. Renouard avait ouvert la marque pour les Tangos (22e), mais Socrier avait �galis� dans la foul�e (23e). Angers ne compte plus qu'un point d'avance sur Lens, qui jouera lundi contre le Havre.
Dans le m�me temps, Brest s'est donn� un peu d'air gr�ce � sa courte mais pr�cieuse victoire contre l'AJ Auxerre (1-0). Une victoire acquise gr�ce � une r�alisation de Verdier (33e). Le club icaunais se trouve d�sormais au 13e rang du championnat, alors que Brest, toujours 19e avec un match de retard, est � 3 points du premier non-rel�gable.
