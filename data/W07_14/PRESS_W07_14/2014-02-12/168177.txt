TITRE: Aucun problème avec Crédit agricole (Noyer)
DATE: 2014-02-12
URL: http://www.lefigaro.fr/flash-eco/2014/02/12/97002-20140212FILWWW00361-aucun-probleme-avec-credit-agricole-noyer.php
PRINCIPAL: 0
TEXT:
le 12/02/2014 à 19:15
Publicité
Il n'y a aucun problème avec le niveau de fonds propres du Crédit agricole et aucun problème avec les banques françaises, a déclaré mercredi le gouverneur de la Banque de France, Christian Noyer.
"Il n'y aucun problème avec le Crédit agricole, il n'y a aucun problème avec les banques françaises", a-t-il déclaré sur BFM Business. Le Crédit agricole ayant une structure différente des autres grandes banques, des interrogations s'élèvent régulièrement sur une éventuelle sous-capitalisation comme dans une étude récente d'universitaires contestée par la "banque verte" .
"Nous n'avons aucune inquiétude", a ajouté Christian Noyer, qui est également membre du conseil des gouverneurs de la Banque centrale européenne.
"L'étude qui a été réalisée qui citait le Crédit agricole parmi d'autres est une étude qui est bourrée d'erreurs, qui ne comprend rien à la façon dont les groupes mutualistes sont organisés, qui (...) regardait les fonds propres d'une petite partie avec l'ensemble des crédits de la totalité du groupe", a-t-il poursuivi. "C'était évidemment faux."
