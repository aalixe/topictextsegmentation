TITRE: Twitter r�ve-t-il de ressembler � Facebook ?
DATE: 2014-02-12
URL: http://www.zdnet.fr/actualites/ui-twitter-reve-t-il-de-ressembler-a-facebook-39797759.htm
PRINCIPAL: 167059
TEXT:
RSS
Twitter r�ve-t-il de ressembler � Facebook ?
Technologie : Confront� � un ralentissement du nombre de ses utilisateurs, Twitter a promis de nouvelles fonctionnalit�s. Une interface refondue est en pr�paration et elle reprend des �l�ments de l�interface de Facebook.
Par La r�daction de ZDNet.fr |
Mercredi 12 F�vrier 2014
Suivre @zdnetfr
Twitter est-il en train de plancher sur une refonte tr�s en profondeur de l�interface de son service en ligne ? Selon Mashable , dont un des journalistes a eu acc�s � une nouvelle page de profil Twitter, la r�ponse est oui.
Oui et Twitter tendrait, au travers de cette refonte graphique , � se rapprocher du design de page appliqu� par Facebook sur sa propre plate-forme avec par exemple une biographie plac�e dans le coin sup�rieur gauche et surtout une grande image s��tirant sur toute la longueur.
Simplifier l'arriv�e des nouveaux utilisateurs
Selon Mashable, cette refonte n�affecte pour le moment que la version PC de Twitter. Elle s�accompagne en outre� de messages, tweets, plus grands. Une fa�on d�accorder plus d�importance aux contenus. Et accessoirement de rendre les messages sponsoris�s plus visibles ?
Confront� � un ralentissement de l�inscription de nouveaux abonn�s, Twitter s�efforce peut-�tre avant tout de rendre son interface plus accueillante et plus lisible, notamment aupr�s d�utilisateurs d�autres services, comme Facebook.
Conscient des difficult�s rencontr�es par les nouveaux utilisateurs, Twitter annon�ait r�cemment sa volont� d�y r�pondre par l�ajout de nouvelles fonctionnalit�s destin�es � am�liorer l�exp�rience.
�
