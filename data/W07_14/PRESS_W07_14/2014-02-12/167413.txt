TITRE: Une garantie contre les pensions alimentaires impay�es exp�riment�e dans 20 d�partements - Lib�ration
DATE: 2014-02-12
URL: http://www.liberation.fr/societe/2014/02/12/une-garantie-contre-les-pensions-alimentaires-impayees-experimentee-dans-20-departements_979696
PRINCIPAL: 0
TEXT:
Lire sur le readerMode zen
Une maman r�conforte son enfant, le 29 ao�t 2006 � l'�cole Malfil�tre d'H�rouville-Saint-Clair, jour de rentr�e scolaire. (Photo Mychele Daniau. AFP)
Alors qu�environ 40 % des pensions alimentaires ne sont pas pay�es, ou partiellement, cette exp�rimentation ouvrira un droit � une pension alimentaire minimale pour les m�res isol�es.
Sur le m�me sujet
Vers une garantie publique contre les impay�s de pension alimentaire
Vers un droit � la pension alimentaire minimale ? Une garantie publique contre les impay�s de pensions alimentaires sera exp�riment�e dans 20 d�partements � partir du 1er juillet, ont pr�cis� mercredi les ministres Najat Vallaud-Belkacem (Droits des femmes) et Dominique Bertinotti (Famille).
Alors qu�environ 40�% des pensions alimentaires ne sont pas pay�es, ou ne le sont que partiellement, cette exp�rimentation ouvrira un droit � une pension alimentaire minimale pour les m�res isol�es, �quivalente au montant de l�allocation de soutien familial (ASF). Si la pension alimentaire est inf�rieure � l�ASF, les CAF verseront la diff�rence.
� lire aussi Mini-miss, IVG, ce que le projet de loi sur l��galit� homme-femme peut changer
Vers une saisies des prestations familiales des mauvais payeurs
L�ASF, qui est de 90 euros par mois et par enfant actuellement, doit passer � �95 euros au moins� au 1er avril et, comme annonc� pr�c�demment, �tre port�e � 120 euros d�ici 2018, ont pr�cis� les deux ministres dans un communiqu�, � l�occasion d�un d�placement � la Caisse d�Allocations familiales (CAF) de Seine-et-Marne. Par ailleurs, les moyens de recouvrement des CAF � l��gard des parents mauvais payeurs seront am�lior�s. Elles pourront ainsi effectuer des saisies sur les prestations familiales des d�biteurs.
Le nombre des d�partements retenus pour l�exp�rimentation a �t� port� � 20 contre 14 envisag�s pr�c�demment�: Ain, Aube, Charente, Corr�ze, C�tes d�Armor, Finist�re, Haute-Garonne, H�rault, Indre-et-Loire, Loire-Atlantique, Haute-Marne, Meurthe-et-Moselle, Morbihan, Nord, Rh�ne, Sa�ne-et-Loire, Paris, Seine-et-Marne, Territoire de Belfort, La R�union.
Un nouveau simulateur en ligne permet �galement aux familles de calculer un montant indicatif de pension alimentaire devant �tre vers�e.
