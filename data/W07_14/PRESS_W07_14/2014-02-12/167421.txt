TITRE: Caroline Bartoli, la candidate aux municipales qui fait le buzz - Infos - Replay
DATE: 2014-02-12
URL: http://videos.tf1.fr/infos/2014/caroline-bartoli-la-candidate-aux-municipales-qui-fait-le-buzz-8364247.html
PRINCIPAL: 167420
TEXT:
� 17h11
31 secondes
Luc Chatel, vice-pr�sident de l'UMP et Marc-Philippe Daubresse, secr�taire g�n�ral adjoint de l'UMP r�agissent � la nomination d'Harlem D�sir, nouveau secr�taire d'Etat aux Affaires europ�ennes.
� 17h00
32 secondes
Suite au rebondissement dans le proc�s Agnelet pour le meurtre d'Agn�s Leroux, une enqu�te a �t� ouverte en Italie. LCI a r�ussi � joindre le chef des carabiniers de Casinon charg� de l'enqu�te. Il affirme que ses hommes sont d'ores et d�j� � la recherche d'ossements.
� 16h57
3min 26s
Pierre-Ren� Lemas, le secr�taire g�n�ral sortant de l'Elys�e a annonc� mercredi apr�s-midi la liste des 14 secr�taires d'Etat du gouvernement Valls.
� 16h01
1min 06s
La Franklin Regional Senior High School de Murrysville a �t� boucl�e mercredi matin, heure locale, apr�s qu'un �l�ve a poignard� une vingtaine de ses camarades.
� 15h51
27 secondes
Dans son discours, Manuel Valls a pr�conis� de "r�duire de moiti� le nombre de r�gions dans l'Hexagone", avec une nouvelle carte des r�gions le 1er janvier 2017. Voici � quoi la France ressemblerait.
� 14h17
16 secondes
"Histoire spatiale", c'est le nom de la vente aux ench�res des accessoires ayant particip� � la conqu�te de l'espace. Cette vente est organis�e par la maison Bonhams � New York.
� 14h12
57 secondes
Toyota rappelle 6,39 millions de voitures au garage, dont 110 000 en France. Pas moins de vingt sept mod�les du constructeur japonais seraient concern�s par divers probl�mes techniques.
� 13h57
51 secondes
Deux coll�giennes de 13 ans qui avaient planifi� l'assassinat de la famille de l'une d'elles ont �t� stopp�es alors qu'elles avaient commenc� � mettre leur projet � ex�cution en blessant gri�vement le petit fr�re.
� 13h45
1min 53s
Ayutthaya a donn� mercredi le coup d'envoi du festival de l'eau de Songkran. Des centaines de personnes et des douzaines d'�l�phants ont pris part � l'�v�nement
� 13h43
1min 27s
Lors de leur deuxi�me conseil, les ministres ont �t� pri�s de laisser leur portable hors de la salle pour �viter des fuites quant � la nomination des secr�taires d'Etat.
� 13h40
4min 44s
En fin de 3�me, les �l�ves font face � une question importante pour leur avenir. Ils doivent choisir entre la voie g�n�rale ou la voie professionnelle notamment avec l'apprentissage.
� 13h40
2min 34s
Les entreprises de chaudronnerie et les industries m�caniques ont chang� d'image ces derni�res ann�es. Aujourd'hui, leur image est bonne. Elles ont fait des efforts pour attirer les jeunes. Le moins que l'on puisse dire, c'est que les besoins sont �normes dans ce secteur qui recrute.
