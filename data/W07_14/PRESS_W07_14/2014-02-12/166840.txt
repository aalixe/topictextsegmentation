TITRE: Criteo : le chiffre d�affaires s�envole (+63%), la part du mobile en nette progression | FrenchWeb.fr
DATE: 2014-02-12
URL: http://frenchweb.fr/criteo-le-chiffre-daffaires-senvole-63-la-part-du-mobile-en-nette-progression/141667
PRINCIPAL: 166838
TEXT:
Actualit� Publicit�
Le cap des 5 000 clients a �t� franchi.�Le titre a cl�tur� � +1,2% hier � Wall Street.
��Nous avons d�livr� un trimestre record et une ann�e record en 2013, d�passant nos attentes �. C�est ainsi que Jean-Baptiste Rudelle, co-fondateur et pr�sident de�Criteo, annonce clairement la couleur.
Quatre mois apr�s son introduction au Nasdaq, la saga se poursuit. L��toile montante issue de l��cosyst�me start-up fran�ais a publi� d�excellents r�sultats financiers avec une hausse de 57% de son chiffre d�affaires au quatri�me trimestre 2013, � 135,9 millions de dollars � contre 86,6 millions un an plus t�t sur la m�me p�riode -�pour un r�sultat net trimestriel de 0,8 millions contre une perte de 4,7 millions un an plus t�t.
Sur l�ann�e enti�re, le sp�cialiste du reciblage publicitaire conna�t une croissance sup�rieure � 63% avec 444 millions de dollars de revenus pour un b�n�fice fix� � 1,4 million de dollars, contre 0,8 millions en 2012.�Mais outre l�exploitation, les signaux d�autres signaux sont au vert. Ainsi, la contribution du mobile dans l�activit� a quadrupl� en passant de 2,5% � 10% de septembre � d�cembre, et le cap des 5 000 clients a �t� franchi d�octobre � d�cembre, soit plus de 50% par rapport � 2012.
��Nous avons lanc� plusieurs nouveaux produits et nous sommes particuli�rement satisfaits de la forte lancement de notre offre mobile au quatri�me trimestre. Avec la mont�e en puissance de nos nouveaux produits sur ??de nouveaux �crans et appareils, nous attendons avec enthousiasme�2014 et nos�futures opportunit�s de croissance �� d�taille M. Rudelle. Le titre a cl�tur� � +1,2% hier � Wall Street.
