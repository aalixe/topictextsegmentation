TITRE: La réunion intercoréenne de haut niveau débute à Panmunjom
DATE: 2014-02-12
URL: http://french.yonhapnews.co.kr/northkorea/2014/02/12/0500000000AFR20140212001500884.HTML
PRINCIPAL: 165433
TEXT:
2014/02/12 10:52 KST
La r�union intercor�enne de haut niveau d�bute � Panmunjom
SEOUL, 12 f�v. (Yonhap) -- Les deux Cor�es ont d�but� ce matin vers 10 heures leur r�union de haut niveau au pavillon sud-cor�en � Panmunjom, dans la Zone d�militaris�e (DMZ). Il s�agit de la premi�re rencontre intercor�enne de haut niveau depuis sept ans et celle-ci vise � discuter de sujets li�s � la p�ninsule cor�enne sans sujet d�termin� � l�avance.
Cette r�union a �t� propos�e par la Cor�e du Nord le 8 f�vrier dernier et le gouvernement de S�oul l�a officialis�e la tenue de la rencontre hier soir. S�oul a expliqu� que �le temps �tait limit� pour d�finir les sujets de discussion. On se rencontrera avec chacun ses sujets � proposer�.
Avant le d�part vers le Nord, le chef de la d�l�gation sud-cor�enne, Kim Kyou-hyun, le premier adjoint du directeur du Bureau de la s�curit� nationale � Cheong Wa Dae a expliqu� qu��on part avec un esprit ouvert pour trouver l�opportunit� d�ouvrir une nouvelle �re sur la p�ninsule cor�enne�.
En ce qui concerne les sujets des discussions, Kim a r�pondu aux journalistes en disant que �les sujets n�ont pas �t� d�termin�s comme vous le savez bien�. �Les actualit�s intercor�ennes feront partie de l�essentiel des discussions mais nous voulons nous focaliser sur le bon d�roulement des r�unions de familles s�par�es.�
Le chef de la d�l�gation nord-cor�enne est l�adjoint du directeur du D�partement du front uni du Parti du travail, Won Ton-yon, un grand sp�cialiste des affaires intercor�ennes du r�gime nord-cor�en.
Le Nord pourrait demander au Sud l�arr�t des exercices militaires conjoints Key Resolve et Foal Eagle qui d�buteront le 24 f�vrier, la reprise du tourisme intercor�en au mont Kumgang puis la lev�e des sanctions du 24 mai 2010. De son c�t�, le Sud devrait insister sur la d�nucl�arisation et le bon d�roulement des r�unions de familles s�par�es pr�vues entre les 20 et 25 f�vrier tout en demandant une organisation r�guli�re.
La d�l�gation sud-cor�enne avant son d�part vers Panmunjom, ce matin � S�oul
