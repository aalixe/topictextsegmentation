TITRE: La mort d'un caméraman émeut le Brésil - Europe1.fr - International
DATE: 2014-02-12
URL: http://www.europe1.fr/International/La-mort-d-un-cameraman-emeut-le-Bresil-1797883/
PRINCIPAL: 165314
TEXT:
La mort d'un cam�raman �meut le Br�sil
Par Charles Carrasco avec AFP
Publi� le 11 f�vrier 2014 � 12h27 Mis � jour le 11 f�vrier 2014 � 19h52
Le journal Correio Braziliense a fait sa "Une" sur cette affaire. � Capture
Santiago Ilidio Andrade a �t� mortellement bless� lors d'une manifestation contre la hausse du prix des transports.
L'INFO. Avec l'augmentation du prix des transports, le contexte social est particuli�rement tendu au Br�sil depuis plusieurs mois. Une nouvelle affaire a suscit� une vive �motion jusqu'au sommet de l��tat. Santiago Ilidio Andrade, 49 ans, cam�raman de la cha�ne de t�l�vision br�silienne Bandeirantes, bless� jeudi, est mort lundi apr�s avoir �t� diagnostiqu� en �tat de "mort c�r�brale".
A quatre mois du coup d'envoi du Mondial de football au Br�sil, il s'agit du premier journaliste mortellement bless� lors d'une des violentes manifestations qui ont secou� le pays pendant et apr�s la fronde sociale massive de juin 2013.
Que s'est-il pass� ? Le journaliste avait �t� frapp� jeudi en pleine t�te par une fus�e d'artifice allum�e par un manifestant en direction de la police. Au moment de l'impact, il ne portait ni casque, ni masque � gaz, ni gilet pare-balles en caoutchouc. "Mon mari est en train de partir. Ils ont d�truit une famille qui �tait unie, tr�s unie", a r�agi l'�pouse du cam�raman dans un entretien � la cha�ne de t�l�vision Globo, avant l'annonce de sa mort. La famille a pr�cis� que plusieurs organes de la victime avaient �t� donn�s. Santiago Ilidio Andrade sera incin�r� jeudi � Rio de Janeiro.
Roussef critique ceux qui "n'ont aucun respect pour la vie". Une trentaine de journalistes ont rendu hommage lundi soir � leur coll�gue. Posant leurs appareils et cam�ras au sol, ils ont fait cercle en silence autour de la tache de sang impr�gn�e dans le bitume, � l'endroit o� �tait tomb� leur coll�gue pendant une premi�re manifestation contre l'augmentation du prix du ticket de bus � Rio de Janeiro.
"Il n'est pas admissible que les manifestations d�mocratiques soient d�natur�es par ceux qui n'ont aucun respect pour la vie", a r�agi la pr�sidente Dilma Rousseff sur son compte Twitter.
N�o � admiss�vel que os protestos democr�ticos sejam desvirtuados por quem n�o tem respeito por vidas humanas
� Dilma Rousseff (@dilmabr) 10 F�vrier 2014
Le maire de Rio, Eduardo Paes, a quant � lui estim� que "la soci�t� doit r�fl�chir sur les limites entre le droit de manifester et les exc�s qui d�coulent du vandalisme et de la violence".
Batman parle "d'irresponsables". Un Carioca devenu tr�s c�l�bre � Rio pour avoir particip� � toutes les manifestations d�guis� en Batman, s'est joint au concert de r�probations. "C'est une fatalit� commise par des irresponsables qui ont sali la r�putation des manifestants" en tuant "un h�ros qui risquait sa vie", a-t-il d�clar�, brandissant une pancarte avec l'inscription : "En deuil pour Santiago".
