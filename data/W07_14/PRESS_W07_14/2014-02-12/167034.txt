TITRE: Berlin revoit sa pr�vision de croissance � la hausse � 1,8%
DATE: 2014-02-12
URL: http://www.latribune.fr/actualites/economie/union-europeenne/20140212trib000814988/berlin-revoit-sa-prevision-de-croissance-a-la-hausse-a-18.html
PRINCIPAL: 0
TEXT:
Berlin revoit sa pr�vision de croissance � la hausse � 1,8%
Le gouvernement table sur une croissance de 2%, selon les premi�res estimations
latribune.fr �|�
12/02/2014, 13:56
�-� 237 �mots
Apr�s un exc�dent commercial record en 2013, le pays a revu � la hausse l'estimation de croissance de son PIB.
sur le m�me sujet
Allemagne : le taux de ch�mage inchang�, � 6,8%
Berlin revoit ses pr�visions de croissance � la hausse.�Dans son nouveau rapport �conomique annuel, le gouvernement allemand table � pr�sent sur une progression du Produit int�rieur brut (PIB) de 1,8% en 2014, contre 1,7% annonc� l'automne dernier et a livr� pour la premi�re fois une pr�vision de croissance pour 2015 de 2%.
"L'�conomie allemande est sur le chemin d'une reprise stable et reposant sur une base large", qui s'appuie aussi "sur le march� int�rieur", a comment� le ministre de l'Economie et vice-chancelier, Sigmar Gabriel, dans un communiqu�.
Le pays doit instaurer progressivement au niveau national un salaire minimum de 8,50 euros par heure, qui pourrait aussi peser positivement sur la consommation.
R�duction de l'exc�dent commercial attendu
La premi�re puissance europ�enne a �t� vivement critiqu�e ces derniers mois tant par les Etats-Unis que par Bruxelles sur l'ampleur de son exc�dent commercial qui nourrit un exc�dent courant jug� excessif de nature � p�naliser ses principaux partenaires. Elle devrait voir cette ann�e l'exc�dent de sa balance commerciale se r�duire, apr�s un niveau record exc�dentaire en 2013, proche de 200 milliards d'euros .�
"Les importations vont cette ann�e davantage augmenter que les exportations", explique le rapport du gouvernement, assurant que par leurs importations et leurs investissements � l'�tranger, les entreprises allemandes "alimentent la reprise �conomique en Europe".
La croissance des exportations est attendue � 4,1% en 2014, alors que les importations devraient progresser de 5%.
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Etienne �a �crit le 13/02/2014                                     � 10:56 :
l'Allemagne peut dire un grand merci � l'euro comme chaque ann�e.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
walter �a �crit le 13/02/2014                                     � 9:59 :
sortons de l'euro, l'euro ne favorise la croissance que dans un pays l'allemagne et g�ne la croissance des autres. Quand allons nous sortir de cette situation absurde ?
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
J'ajoute... �a �crit le 12/02/2014                                     � 17:56 :
...qu'aujourd'hui la banque d'Angleterre a revue sa pr�vision de croissance pour l'ann�e 2014 � 3,4%.
franc �a r�pondu le 13/02/2014                                                 � 9:55:
vive l'euro !!!!!!!!!
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Fran�ois �a �crit le 12/02/2014                                     � 16:01 :
les allemands ach�tent allemand et cela leur permet d'avoir de la croissance. Les fran�ais n'ach�tent pas assez fran�ais.
frontalier �a r�pondu le 12/02/2014                                                 � 17:55:
en tant que frontalier travaillant en Allemagne, je ne peux que confirmer...
pedro �a r�pondu le 12/02/2014                                                 � 22:20:
en tant que Fran�ais travaillant en Allemagne je ne peut qu'infirmer.
papa fox �a r�pondu le 13/02/2014                                                 � 17:05:
en tant que Francais vivant en Allemagne je ne peux qu�affirmer le contraire.
Dans ma rue , voici le parc  d� environ  9  familles:
1 Peugeot  308 CC
