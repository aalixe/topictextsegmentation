TITRE: Jean Dujardin est ins�parable de George Clooney et Matt Damon - Terrafemina
DATE: 2014-02-12
URL: http://www.terrafemina.com/societe/buzz/articles/37850-jean-dujardin-est-inseparable-de-george-clooney-et-matt-damon.html
PRINCIPAL: 165995
TEXT:
Publi� le 11 f�vrier 2014
Jean Dujardin, George Clooney et Matt Damon
��Porta Stefano/ANSA/ABACA
�
�
En pleine tourn�e de promotion du film � Monuments Men �, Jean Dujardin ne quitte pas George Clooney et Matt Damon, ses partenaires � l��cran. Apr�s la Berlinale, l�acteur oscaris� �tait en Italie, o� il a amus� la galerie.
Jean Dujardin ne quitte plus d�une semelle George Clooney et Matt Damon, auxquels ils donne la r�plique dans le film Monuments Men. A la Berlinale, la star de The Artist a fait le show en compagnie de ses coll�gues dans une ambiance festive. George Clooney, Matt Damon, Jean Dujardin , Hugh Bonneville, John Goodman, Bill Murray et Bob Balaban n�ont d�ailleurs pas h�sit� � faire la chenille devant les photographes.�L�avant-premi�re et le photocall du film avaient des airs de bringue entre potes plut�t que de pr�sentation dans un festival de cin�ma.
La c�l�bre imitation du chameau
Rebelote � Milan en Italie. Venu pr�senter le film en compagnie de ses comp�res Georges  Clooney et Matt Damon , le frenchie s�est donn� en spectacle sur le plateau de l��mission Che Tempo Che Fa � Milan. Jean Dujardin s�est en effet lanc� dans son c�l�bre num�ro d�imitation du chameau, pour le plus grand plaisir de ses acolytes qui ont �clat� de rire.
Jean Dujardin devrait revenir sur la terre natale � l�occasion de l�avant-premi�re de Monuments Men le 12 f�vrier prochain. Toujours accompagn� de ses nouveaux amis c�l�bres;
�
