TITRE: L�Audi S1 Quattro en fuite | le blog auto
DATE: 2014-02-12
URL: http://www.leblogauto.com/2014/02/laudi-s1-quattro-en-fuite.html
PRINCIPAL: 165267
TEXT:
Publi� par Christian Conde le
11 f�vrier 2014
dans Audi , Berlines , Nouveaux mod�les , Sportive | 1063 lectures  | 14 r�ponses
Bien que l�annonce officielle ne soit pr�vue que dans quelques heures, quelques images de l�Audi S1 Quattro ont pris la fuite sur Internet.
L�A1 y gagne un bouclier avant plus agressif et des garnitures en forme de maille sur les entr�es d�air lat�rales du spoiler, alors que les feux avant � LED se parent d�un fond de couleur rouge, ce qui semble donner un regard inject� de sang qui fera son effet dans les r�troviseurs.
La compacte adopte �galement des jupes lat�rales ainsi que des jantes noires sp�cifiques avec pneus � profil bas. Un sticker ��Quattro�� vient prendre place sur les portes arri�re. La face arri�re re�oit un imposant aileron de coffre et quatre sorties d��chappement int�gr�es dans le diffuseur arri�re.
A l�int�rieur, les si�ges avant semblent remplac�s par des si�ges avant sport en cuir. On note la pr�sence d�une coque entourant le levier de vitesses (manuelle � 6 rapports) qui rappelle le coloris de la teinte ext�rieure (jaune pour le mod�le pr�sent� sur les images. Peu d�informations ont filtr� concernant la motorisation mais certaines sources annoncent un moteur 2l TFSI de 230ch. L�annonce officielle dans quelques heures viendra confirmer cette hypoth�se dans quelques heures.
