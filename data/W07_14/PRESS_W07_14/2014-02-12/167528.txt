TITRE: Arsenal-Wenger: "Un simple accident" - Angleterre - Football.fr
DATE: 2014-02-12
URL: http://www.football.fr/angleterre/scans/arsenal-wenger-un-simple-accident-520454/
PRINCIPAL: 0
TEXT:
11 février 2014 � 13h10
Mis à jour le
Réagir 5
Quatre jours après la lourde défaite concédée sur la pelouse de Liverpool (5-1), Arsène Wenger était présent en conférence de presse. Le manager d'Arsenal en a profité pour dédramatiser ce revers, considérant cette défaite comme un simple "accident."
"C'est toujours une déception de perdre un match mais je pense que c'était un accident parce que nous avons été très stables défensivement nous devons le prendre comme cela", a déclaré le coach des Gunners. Avant de poursuivre : "Nous sortons tous juste d'une série de dix matchs sans défaite, vous ne pouvez pas considérer un match comme une tendance, le symbole de notre saison."
Arsène Wenger espère une réaction de la part de ses joueurs mercredi, à l'occasion du choc de la 26e journée de Premier League face à Manchester United.
