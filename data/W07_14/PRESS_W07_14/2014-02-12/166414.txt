TITRE: Audi S1 Sportback : restylage et 231 ch en prime ! | - Actu Automobile
DATE: 2014-02-12
URL: http://www.actu-automobile.com/2014/02/12/audi-s1-sportback/
PRINCIPAL: 166410
TEXT:
Accueil > ACTUS > Audi S1 Sportback : restylage et 231 ch en prime !
Audi S1 Sportback : restylage et 231 ch en prime !
Publi� le 12 f�vrier 2014 | Par S�bastien Rabatel
Audi S1 Sportback
Pour le restylage de l�A1, voici une version in�dite : l� Audi S1 Sportback. Elle est aussi accompagn�e d�une S1 3 portes dont les photos n�ont pas encore �t� d�voil�e.
M�caniquement, cette nouvelle Audi S1 accueille un g�n�reux 2.0 TSI d�veloppant une puissance de 231 ch, avec un couple de 370 Nm. C�est moins que les 256 ch de l� Audi A1 Quattro vendue en petite s�rie en 2012, mais ce niveau de puissance est suffisant pour que la S1 devienne la sportive la plus puissante de la cat�gorie. Les Clio RS et 208 GTI se situent � 200 ch, mais la prochaine Mini JCW pourrait bien d�passer les 220 ch et se rapproche de la nouvelle S1.
La transmission int�grale Quattro est de rigueur, comme sur tous les mod�les S de la firme aux anneaux. Audi annonce un 0 � 100 km/h r�alis� en 5,8 s pour la S1, et en 5,9 s pour la S1 Sportback. Les deux carrosseries pourront atteindre les 250 km/h en vitesse de pointe�
Avec une consommation mixte de 7,0 et 7,1 L, ces Audi S1 n��chapperont pas au malus �cologique en France avec un niveau d��missions d�passant les 160 g.
Audi profite du lancement des S1 et S1 Sportback pour mettre � jour son A1 avec un restylage. Esth�tiquement, on remarque d�entr�e de jeu les nouveaux blocs optiques Xenon Plus ac�r�s, dans l�esprit de la derni�re A3 . A l�arri�re les feux sont revus dans leur dessin, avec des LED et une nouvelle ligne horizontale. Le kit carrosserie de la nouvelle S1 comprend �galement les r�troviseurs argent�s, des jantes alliage 18 pouces de s�rie en France, et deux doubles sorties d��chappement � l�arri�re. En option, un kit carrosserie Quattro ajoute des �l�ments comme le grand spoiler de toit.
Le prix de la nouvelle Audi S1 Sportback est d�j� annonc�, il est fix� � 34800 euros. La version S1 3 portes sera vendue 33900 euros. L��quipement de s�rie est complet avec les projecteurs X�non Plus, l�aide au stationnement, le GPS couleur avec commande MMI, le r�gulateur de vitesse, Cobra Track by Audi, radio Concert, et les r�troviseurs escamotables.
Pr�sentation en premi�re mondiale au Salon de Gen�ve, et arriv�e en concessions d�s le mois de mai 2014.
Audi S1 Sportback
