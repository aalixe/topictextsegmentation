TITRE: Le Conseil d'Etat suspend le d�cret autorisant l'ouverture des magasins de bricolage le dimanche
DATE: 2014-02-12
URL: http://www.lemonde.fr/emploi/article/2014/02/12/le-conseil-d-etat-suspend-le-decret-autorisant-l-ouverture-des-magasins-de-bricolage-le-dimanche_4365168_1698637.html
PRINCIPAL: 0
TEXT:
Le Conseil d'Etat suspend le d�cret autorisant l'ouverture des magasins de bricolage le dimanche
Le Monde |
� Mis � jour le
12.02.2014 � 18h21
Le d�cret autorisant temporairement les magasins de bricolage � ouvrir le dimanche a �t� suspendu, mercredi 12 f�vrier, par le Conseil d'Etat. Dans son ordonnance , celui-ci a estim� qu'il ��existait un doute s�rieux sur la l�galit頻 de ce d�cret du 30 d�cembre . Le gouvernement a imm�diatement indiqu� qu'il pr�parait un nouveau texte qui sera publi� � dans les plus brefs d�lais��.
Ce d�cret, directement inspir� du� rapport Bailly remis � Matignon d�but d�cembre, autorise, depuis le d�but de l'ann�e, l'ouverture des magasins de bricolage le dimanche jusqu'au 1er juillet 2015, le temps de plancher sur une loi qui mettrait de l'ordre dans le maquis des d�rogations en vigueur.
� DROIT CONSTITUTIONNEL�� DES SALARI�S
Dans un communiqu� , le Conseil d'Etat met cependant l'accent sur deux points�probl�matiques du d�cret. Le premier concerne son caract�re temporaire : ��L'autorisation pr�vue courait jusqu'au 1er juillet 2015, alors qu'une telle d�rogation doit normalement avoir un caract�re permanent, dans la mesure o� elle a vocation � satisfaire des besoins p� rennes du public��, note la plus haute juridiction administrative.
Le deuxi�me concerne le fond m�me du texte. Le Conseil d'Etat a ainsi estim� que l'ouverture des �tablissements le dimanche ��est de nature � porter une atteinte grave et imm�diate aux int�r�ts d�fendus par les organisations syndicales��, alors ��que le principe d'un repos hebdomadaire est l'une des garanties du droit constitutionnel au repos reconnu aux salari�s et que ce droit s'exerce en principe le dimanche��.
� PAS UN BESOIN DE PREMI�RE N�CESSITɠ�
Le Conseil d'Etat avait �t� saisi en r�f�r� � la demande des syndicats CGT, FO, SUD et SECI, qui estiment qu'��aller s' acheter un meuble ou un marteau le dimanche n'est pas un besoin de premi�re n�cessit頻.
Ils soulignaient ainsi le fait que ce d�cret contrevenait au code du travail, qui donne d�rogation aux seuls �tablissements ��dont le fonctionnement ou l'ouverture est rendue n�cessaire par les contraintes de la production, de l'activit� ou les besoins du public��. C'est cette d�rogation qui permet, par exemple, aux h�tels, aux restaurants et d�bits de boisson, ou encore aux commerces de d�tails alimentaire d' �tre ouverts le dimanche, mais uniquement jusqu'� 13 heures pour ces derniers.
Pour obtenir ce d�cret, le patronat du secteur avait promis de n�gocier un accord de branche avec les organisations syndicales et d' ouvrir le dimanche uniquement 178 magasins o� l'ouverture avait d�j� �t� constat�e.
