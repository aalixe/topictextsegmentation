TITRE: Gattaz ne veut pas que le pacte de responsabilit� soit "un pacte de contrainte" - BFMTV.com
DATE: 2014-02-12
URL: http://www.bfmtv.com/economie/pacte-responsabilite-gattaz-nexlut-pas-engagements-chiffres-709368.html
PRINCIPAL: 168221
TEXT:
> Emploi, Social
Gattaz ne veut pas que le pacte de responsabilit� soit "un pacte de contrainte"
Le pr�sident du Medef a d�clar�, ce mercredi 12 f�vrier, que sur les contreparties du pacte de responsabilit�, il allait rester "ferme sur les prix". Et il n'exclut pas des engagements chiffr�s.
D. L. avec AFP
r�agir
Apr�s le couac du d�but de semaine, Pierre Gattaz met un peu d'eau dans son vin, ce mercredi 12 f�vrier. Le pr�sident du Medef Pierre Gattaz n'a pas "exclu" des "engagements chiffr�s" en mati�re de cr�ation d'emplois dans le cadre du pacte de responsabilit� . Mais, a-t-il pr�cis� � la presse � bord de l'avion du pr�sident fran�ais Fran�ois Hollande, entre Washington et San Francisco, sur la base "d'objectifs" et "d'estimations".
Mais il ajoute tout de m�me: "sur les contreparties du pacte de responsabilit�", le patron des patrons entend rester "ferme sur les prix". "On ne pourra pas faire n'importe quoi", a-t-il encore estim�. "Je ne pourrai jamais prendre un engagement juridique dans un environnement, instable, mondialis� et tr�s concurrentiel", a-t-il fait valoir, r�affirmant que ce pacte de responsabilit� ne devait pas �tre transform� en un "pacte de contrainte".
Mais pour Pierre Gattaz, le "niveau appropri�", �voqu� mardi par le pr�sident Hollande � Washington, des engagements qui seraient pris par les entreprises en �change d'all�gements de charges "est une bonne id�e".
"Il y aura des engagements de mobilisation sur les apprentis et les emplois � la condition que les choses bougent et que certains verrous se d�bloquent", assure-t-il, "l'enjeu" �tant "la cr�ation d'un million d'emplois nets en cinq ans auquel je crois".
"Pour moi, le pacte de responsabilit� reposera sur deux piliers, le retour de la comp�tivit� des entreprises fran�aises avec la restauration de leurs marges et l'instauration de ce climat de confiance", ajoute-t-il, "l'emploi et l'investissement en seront les cons�quences".
Najat Vallaud Belkacem a imm�diatement r�agi sur BFM TV en d�clarant : "Je me r�jouis que M. Gattaz ait retrouv� ses esprits. (...). Je pense que ses premiers propos ont d�pass� sa pens�e".
"Le dialogue social ne peut pas reposer sur des oukazes"
Lundi, il avait d�clar�: "aujourd'hui, quand j'entends parler de  contrepartie dans ce pacte, j'entends aussi des gens qui me disent: 'on  va vous contraindre, on va vous obliger, on va vous mettre des p�nalit�s  si vous ne le faites pas, vous allez �tre punis'".
Le pacte de responsabilit� tel qu'il est pr�sent� ne lui convient pas. Pierre Gattaz attend plus d'efforts sur les baisses de charges et la baisse de la fiscalit�. Il demande surtout plus de garanties sur le calendrier de ces baisses. Pour le "patrons des patrons", c'est un pr�alable. Il ne veut pas s'engager sans cela.
Des propos qui avaient provoqu�s les ires du Premier ministre . "Le  dialogue social ne peut pas reposer sur des oukazes", avait d�clar� Jean-Marc Ayrault.�
Et il avait ajout� : "je souhaite que M. Gattaz,  quand il sera revenu en France, rencontre le plus vite possible les  organisations syndicales comme c'�tait pr�vu pour engager le dialogue  social vraiment sur le pacte qui est tr�s attendu". Ajoutant: "je pense  que le d�calage horaire, parfois, peut poser des probl�mes".
Fran�ois Hollande avait, lui, d�clar� lors de sa conf�rence de presse: "des engagements, les entreprises doivent �galement en prendre au niveau appropri� pour cr�er de l'emploi, am�liorer la formation professionnelle, localiser des activit�s, d�velopper les investissements".
A lire aussi
