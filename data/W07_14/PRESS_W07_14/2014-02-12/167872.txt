TITRE: Vins & spiritueux : stabilisation des exportations fran�aises en 2013
DATE: 2014-02-12
URL: http://www.boursier.com/actualites/economie/vins-spiritueux-stabilisation-des-exportations-en-2013-23000.html
PRINCIPAL: 0
TEXT:
Vins & spiritueux : stabilisation des exportations fran�aises en 2013
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � Apr�s une ann�e 2012 record, le chiffre d'affaires des exportations de vins et spiritueux s'est stabilis� l'an dernier, d'apr�s le bilan de la FEVS publi� ce mercredi... Cependant, "les chiffres globaux cachent des situations contrast�es entre les diff�rents produits", pr�cise la f�d�ration. Les vins et spiritueux conservent�toutefois leur place de deuxi�me poste exc�dentaire dans la balance commerciale de la France...
Le Bordeaux, le Champagne et le Cognac � l'honneur
Dans le d�tail, en valeur, les exportations de vins se sont �tablies � 7,6 milliards d'euros sur l'ann�e, en recul de 8 millions d'euros, dont 4 milliards pour les "vins tranquilles AOC/AOP" (-2%). Les vins de Bordeaux comptent toujours pour l'essentiel (2,1 milliards d'euros), malgr� un repli des exportations de 6,7%, devant le Bourgogne (716,8 millions d'euros, +1,1%) et le Beaujolais (112,3 millions, -7,3%). La France a par ailleurs export� pour 2,3 milliards d'euros de Champagne, soit une progression de +1,3% en 2013. Du c�t� des spiritueux, le chiffre d'affaires s'est �lev� � 3,5 milliards d'euros (-1,1%), dont 2,4 milliards uniquement pour le Cognac (67%, -1,7%).
L'Europe, premier client
Par zone g�ographique, l'Europe demeure la premi�re destinataire des exportations de vins & spiritueux (4,5 milliards d'euros, +0,3%), devant l'Asie du Nord - Chine, Hong Kong, Japon, Singapour, Ta�wan (2,7 milliards d'euros, -5,9%). Ce repli� s'explique par "la politique de r�duction du train de vie de l'Etat et de lutte contre la corruption mise en oeuvre par le nouveau gouvernement chinois (...) Le march� reste toutefois structurellement porteur, avec une classe moyenne qui se d�veloppe�", souligne la F�d�ration. *
En troisi�me position, on retrouve l'Am�rique du Nord� (2,4 milliards d'euros, -0,1%), les taux de change n'ayant pas contribu� favorablement aux �changes. Enfin, les exportations continuent de progresser avec les Emirats Arabes Unis et l'Australie.
Prudence de mise
C�t� perspectives,� le pr�sident de la FEVS, Louis Fabrice Latour appelle � la prudence. "Le maintien de notre niveau de performance en 2013 ne doit n�anmoins pas cacher la d�gradation continue de nos parts de march� en volume sur les principaux march�s �trangers (...) L'heure n'est donc pas au satisfecit mais � la mobilisation de tous pour conserver notre capacit� de croissance, d'investissement et d'innovation dans les ann�es � venir...", a-t-il d�clar�.
Marianne Davril � �2014, Boursier.com
