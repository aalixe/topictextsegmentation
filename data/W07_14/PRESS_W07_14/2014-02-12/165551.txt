TITRE: Barclays�pourrait�supprimer�jusqu'�12�000�emplois
DATE: 2014-02-12
URL: http://french.peopledaily.com.cn/Economie/8533291.html
PRINCIPAL: 165544
TEXT:
Barclays pourrait supprimer jusqu'� 12 000 emplois
(     le Quotidien du Peuple en ligne   )
12.02.2014 � 08h15
La banque britannique, qui compte 140 000 personnes au total, a d�clar� avoir annonc� les suppressions d'emplois � environ la moiti� du personnel affect�.
Les compressions ont Barclays a dit qu'il avait augment� le montant total qu'elle a pay�e sur les primes du personnel l'ann�e derni�re.
La somme globale affect�e aux primes de la banque pour 2013 a augment� de 10 % � 2,38 milliards de Livres, contre 2,17 milliards de livres sterling en 2012. Ces chiffres si �lev�s suscitent l'incompr�hension chez ceux qui ne sont pas familiers de l'industrie, sachant que la Barclays n'a pas �t� sans jouer un certain r�le dans l'�pop�e de la quasi- destruction du capitalisme financier mondial il n'y a pas si longtemps.
� Nous devons �tre comp�titifs sur les salaires et nous devons payer pour la performance �, a dit � la BBC Antony Jenkins, directeur g�n�ral, qui a renonc� � son bonus annuel. � Nous intervenons dans de nombreux pays � travers le monde... nous sommes en concurrence pour les talents sur les march�s mondiaux �, a-t-il ajout�.
Barclays a dit que les suppressions d'emplois concerneraient 820 postes de hauts dirigeants, dont 220 directeurs g�n�raux et 600 directeurs. Environ 400 des suppressions d'emplois de haut niveau toucheront la banque d'investissement. Barclays a dit qu'il esp�rait qu'il pourrait obtenir la majorit� de ces suppressions par des d�parts volontaires. 7 650 autres postes avaient d�j� �t� supprim�s l'ann�e derni�re.
Articles recommand�s:
