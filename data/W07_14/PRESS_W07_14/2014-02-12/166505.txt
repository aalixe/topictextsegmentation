TITRE: Fran�ois Hollande : seul, mais bien entour� au d�ner de la Maison-Blanche � metronews
DATE: 2014-02-12
URL: http://www.metronews.fr/info/francois-hollande-seul-mais-bien-entoure-au-diner-de-la-maison-blanche/mnbl!UW0e1xTwwN6/
PRINCIPAL: 166501
TEXT:
Mis � jour  : 12-02-2014 11:18
- Cr�� : 12-02-2014 07:15
Hollande � la Maison Blanche : seul, mais bien entour�
AGAPES - Il s'agissait de l'un des principaux temps forts de la visite de Fran�ois Hollande aux Etats-Unis. Mardi soir, le chef de l'Etat, en c�libataire, ainsi qu'une d�l�gation fran�aise compos�e d'une trentaine de membres, �tait l'invit� du couple Obama pour un d�ner de gala � la Maison Blanche.
Tweet
�
� la Maison Blanche, le petit personnel peut souffler un ''ouf'' de soulagement. Jusque dans les moindres d�tails, y compris l'�pineuse question du plan de table, le d�ner propos� mardi soir � l'invit� fran�ais s'est d�roul� sans fausse note. Arriv� en pr�sident c�libataire aux �tats-Unis, Fran�ois Hollande a finalement pris place entre Michelle et Barack Obama. Et � la gauche du chef de l'Etat am�ricain - place qui aurait d� �tre celle de la Premi�re dame fran�aise - se trouvait une certaine Thelma Golden, directrice d'un mus�e d'art contemporain de Harlem, � New York.
Mary J. Blige chante Brel
Un temps pressentie pour cette place de choix, la ministre Fleur Pellerin, selon certaines sources sur place, aurait �t� consid�r�e comme ''trop jeune'' pour un tel honneur. R�sultat des courses, Fran�ois Hollande �tait le seul Fran�ais � avoir d�n� � la table des Obama. Les autres membres de la d�l�gation ''frenchie'' - une trentaine de convives, dont Christine Lagarde, Pierre Gattaz ou Jean-Paul Huchon - �tant r�partis parmi les 300 ''happy few'' du soir.
Comme annonc�, le menu proposait un subtil tour d'horizon de mets typiquement am�ricains : pas de hamburgers au programme, mais du caviar de l'Illinois, une salade directement cueillie dans le potager de Michelle Obama, du boeuf du Colorado et, pour finir, une ganache au chocolat, sp�cialit� de Hawa�, �le natale du premier pr�sident noir des Etats-Unis. Cerise sur le g�teau, la chanteuse soul   Mary J. Blige a conclu en beaut� ce d�ner hors du commun, interpr�tant m�me du Jacques Brel en l'honneur de l'invit� fran�ais.
Nicolas Moscovici
