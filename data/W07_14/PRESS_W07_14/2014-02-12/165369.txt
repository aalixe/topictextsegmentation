TITRE: Hollande aux �tats-Unis : accueilli avec faste � la Maison Blanche pour un d�ner d'Etat  - RTL.fr
DATE: 2014-02-12
URL: http://www.rtl.fr/actualites/info/international/article/hollande-aux-etats-unis-accueilli-avec-faste-a-la-maison-blanche-pour-un-diner-d-etat-7769660288
PRINCIPAL: 0
TEXT:
Barack Obama et Fran�ois Hollande lors de leur conf�rence de presse commune, mardi 11 f�vrier � la Maison Blanche (Washington)
Cr�dit : AFP
Fran�ois Hollande a �t� accueilli avec tous les honneurs � la Maison Blanche pour un d�ner d'Etat luxueux.
Fran�ois Hollande a �t� accueilli en grande pompe par les �poux Obama � la Maison Blanche mardi 11 f�vrier au soir pour participer � un d�ner d'Etat, au  deuxi�me jour d'une visite qui voit les deux pr�sidents afficher leur  bonne entente. Le pr�sident fran�ais est arriv� seul peu apr�s  19h00 et a �t� chaleureusement accueilli par Barack et Michelle Obama,  qui portait une longue robe bleu et un bustier noir.
Apr�s avoir  pris la pause sur le parvis nord, Fran�ois Hollande et les �poux Obama  sont entr�s dans la Maison Blanche o� devait se tenir un d�ner d'Etat  rassemblant environ 300 personnes.
Les hommes politiques am�ricains et fran�ais se m�langent au d�ner
A leur table prendront place  avec eux des membres de la soci�t� civile am�ricaine. Fran�ois Hollande  devait d�ner entre ses deux h�tes, et Thelma Golden, directrice d'un  mus�e d'art contemporain � Harlem, occupera l'autre si�ge au c�t� de  Barack Obama, � une place o� est parfois assise la Premi�re Dame du pays  invit�. Le com�dien Stephen Colbert partagera �galement son d�ner avec  les �poux Obama et Fran�ois Hollande.
Parmi les autres invit�s au  d�ner figurent de nombreux hommes politiques am�ricains, comme le  secr�taire d'Etat John Kerry ou le vice-pr�sident Joe Biden, et  fran�ais, comme les ministres Fleur Pellerin, Arnaud Montebourg, Pierre  Moscovici ou Laurent Fabius, ainsi que des personnalit�s.
Des stars et un menu de luxe
Guillaume  Pepy, pr�sident de la SNCF, Anne Lauvergeon, l'ex-patronne d'Areva,  Jean-Yves Le Gall, pr�sident du CNES, faisaient ainsi partie de la  d�l�gation fran�aise. La patronne du FMI Christine Lagarde �tait  �galement pr�sente. L'acteur - francophone - Bradley Cooper et la  chanteuse Mary J. Blige, qui devait chanter en fin de repas, avaient  �galement �t� convi�s.
Le menu proposait notamment du "caviar  osci�tre am�ricain" de l'Illinois et un velout� de pommes de terre avec  oeufs de caille avant une "petite salade d'hiver" aux l�gumes du jardin  que Michelle Obama a plant�s � la Maison Blanche. Ensuite, un  faux-filet de boeuf du Colorado avec �chalotes et blettes brais�es  devait �tre servi avant une glace � la vanille avec mandarines de  Floride et ganache au chocolat de Hawa�, l'Etat o� est n� le pr�sident  am�ricain. Le tout devait �tre arros� de vins de Californie, de l'Etat de Washington et d'un Blanc de Chardonnay de Virginie.
La r�daction vous recommande
