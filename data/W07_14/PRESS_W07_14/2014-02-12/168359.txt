TITRE: Fonctionnaires : le gel des primes et avancements est bien envisag�
DATE: 2014-02-12
URL: http://www.leparisien.fr/politique/le-gel-des-primes-et-avancements-des-fonctionnaires-est-bien-envisage-12-02-2014-3584371.php
PRINCIPAL: 0
TEXT:
Fonctionnaires : le gel des primes et avancements est bien envisag�
Publi� le 12.02.2014, 17h39                                             | Mise � jour :                                                            13.02.2014,                                                  08h27
�Je serai attentif aux fonctionnaires qui sont aujourd'hui les plus modestes, et notamment les fonctionnaires de cat�gorie C�, a assur� Bruno Le Roux, le pr�sident du groupe PS � l'Assembl�e nationale.
| (LP/Jean-Baptiste Quentin.)
R�agir
Le pr�sident du groupe PS � l' Assembl�e nationale Bruno Le Roux a affirm� ce mercredi que le gel des primes et avancements des fonctionnaires �tait bien �sur la table�, avant de corriger un peu plus tard ses propos.
Interrog� � Questions d'Info (LCP/Le Monde/France Info/AFP) sur cette proposition attribu�e par la presse � Vincent Peillon, Bruno Le Roux a d'abord rappel� que le ministre de l'�ducation avait �d�menti� l'avoir faite.
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Primes gel�es pour les fonctionnaires : un �casus belli� pour FO
�Je sais que Bernard Cazeneuve envisage ces mesures�
�C'est sur la table. Bernard Cazeneuve le propose aux ministres�, lui objectent les journalistes. �Je sais que c'est sur la table. Je sais que Bernard Cazeneuve aujourd'hui envisage ces mesures, pour ensuite nous faire la proposition, au Premier ministre, au pr�sident de la R�publique et � la majorit� (...)�, a r�pondu Bruno Le Roux.
Un peu plus tard, le patron des d�put�s PS a affirm� avoir voulu dire qu'il y �a des sujets qui sont sur la table, dont peut-�tre celui-l�. En tout �tat de cause, si cette hypoth�se de gel �tait propos�e �j'y serais d�favorable�, a poursuivi Bruno Le Roux.
�Je dis, d'ores et d�j� ici, que dans la fonction publique, dans toutes les fonctions publiques, celle de l'�ducation comme toutes les autres, je serai attentif aux fonctionnaires qui sont aujourd'hui les plus modestes, et notamment les fonctionnaires de cat�gorie C�, avait-il dit � Questions d'Info.
53 Mds� d'�conomies � r�aliser en trois ans
La pol�mique avait fait rage la semaine derni�re, quand les �chos et le Figaro avaient attribu� � Vincent Peillon la proposition de geler l'avancement et les promotions de l'ensemble des fonctionnaires . L'int�ress� avait aussit�t d�menti ces propos, suivi du Premier ministre.
Le gouvernement est � la recherche de 53 Mds� d'�conomies en trois ans. Et un conseil strat�gique de la d�pense publique est conduit directement par le pr�sident de la R�publique. Les diff�rents ministres ont commenc� � d�filer � Bercy afin de pr�senter leurs pistes d��conomies.
Mais geler les primes et l'avancement des fonctionnaires risque d'�tre tr�s d�licat. D'autant que le point d'indice, autre composante importante du salaire des fonctionnaires, n'a pas �t� revaloris� depuis 2010.
Le chantier est sensible sur le plan politique. �Deux fonctionnaires sur trois ont apport� leur voix � Fran�ois Hollande lors du second tour des �lections pr�sidentielles de mai 2012�, rappelait L'Express en janvier 2013 dans un article intitul� �Le PS doit-il craindre de perdre les fonctionnaires ?�
LeParisien.fr
R�agir avec mon compte You / le Parisien
Identifiant
Votre e-mail* (ne sera pas visible)
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
D�connexion
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
> Cliquez ici pour lire les 180 r�actions � l�article
banque 15/02/2014 - 09h28
Je travaille dans une banque, 17 mois de salaires au total en incluant primes int�ressement participation, ce , mutuelle ch�ques cadeaux etc... Il n y a pas que dans le public que cela existe, il y a des abus c est clair mais n en faisons pas une majorit�! Tous les fonctionnaires n ont pas une place planqu�e! Et les concours sont ouverts! Soyons � minima reconnaissant envers ces personnes qui travaillent pour nous!
R�ponse Signaler un abus
kiwi 83 15/02/2014 - 07h21
moi je vais pas bosser pour la gloire si ils tuent encore plus mon salaire je risque de lever le pied !!!! RCP on va pas s'excuser d'�tre des fonctionnaire chacun dans la vie a des chances et des oportunit�s toi tu dois certainement en avoir des differentes et personne te dis rien !!!!
R�ponse Signaler un abus
Seb 14/02/2014 - 15h27
Gel des salaires des fonctionnaires.  Comme d'habitude cela fait 5 ans que ?a dure.  Que le gouvernement 0 en se plut?t ? arr?ter les aides sociaux des profiteurs qui co?te beaucoup plus cher ? l'?tat. Ces gens l? reste chez eux et gagnent plus qu'un employ? que ce soit du priv? ou du public.
R�ponse Signaler un abus
Gramuchon 14/02/2014 - 14h23
Il faut que toutes les familles fran�aises puissent b�n�ficier des emplois de fonctionnaires, cela ne doit pas �tre r�serv� � ceux qui "passent" le concours. Il faut faire des roulements pour que tout le monde en profite, c'est cela l'�galit� !
R�ponse Signaler un abus
H�l�ne de Biare 14/02/2014 - 14h21
Il ne suffit d'annoncer que l'on va faire 50 milliards d'euros d'�conomies, il faut les faire ! Sans taper dans le budget des aides sociales c'est impossible. On attend quoi ?
R�ponse Signaler un abus
azozo 14/02/2014 - 13h59
Il bosse ou dans le priv� rcp ? Il n'a pas de CE , de 13e mois , de tickets restaurants , de villages vacances .... ? Car les fonctionnaires pas ! On change ?
rcp 14/02/2014 - 12h19
A Balkys@ du 13/02 � 12h59 Vous �tes encadreur? C'est un beau m�tier f�licitations. Et bonne journ�e
rcp 14/02/2014 - 12h13
En r�ponse @lolo9501. Moi je ne critique pas, je pleure sur le sort de ces pauvres fonctionnaires qui vont �tre oblig� de faire un effort apr�s avoir profit� � outrance d'un syst�me injuste et in�gal par rapport aux salari�s du priv�.  Bonne journ�e et au boulot!!
R�ponse Signaler un abus
risbet 14/02/2014 - 11h52
c'est la gauche qui devrait faire les reformes pour redresser le pays car c'est souvent son eletorat qui a bloqu� le pays quand la droite etait aux pouvoirs  si la gauche n'y arrive pas alors  personne ne pourra
R�ponse Signaler un abus
lise 13/02/2014 - 22h26
Ils cherchent juste ? nous monter les uns contre les autres et pendant ce temps l? personne ne s'occupe des vrais privil?gi?s : les membres de notre gouvernement dont le salaire n'a jamais ?t? revu  ? lz baisse comme promis et dont les avantages plombent le budget de l'?tat. .. voil? o? faire des ?conomies.   Arr?tons la gueguerre public priv?.
