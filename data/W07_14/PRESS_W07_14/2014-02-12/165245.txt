TITRE: Reprise des n�gociations pour la r�unification de Chypre - L'Orient-Le Jour
DATE: 2014-02-12
URL: http://www.lorientlejour.com/article/854189/reprise-des-negociations-pour-la-reunification-de-chypre.html
PRINCIPAL: 165236
TEXT:
Moyen Orient et Monde
Reprise des n�gociations pour la r�unification de Chypre
Le pr�sident de la R�publique de Chypre, Nicos Anastasiad�s, la responsable des Nations unies � Chypre, Lisa Buttenheim, et le dirigeant de la R�publique turque de Chypre Nord, Dervis Eroglu, lors de leur entrevue hier sur l�a�roport de Nicosie. Yiannis Kourtoglou/AFP
Crise
Les dirigeants grec et turc tentent de parvenir � une solution ��aussi vite que possible��.
OLJ
Abonnez-vous
Les dirigeants chypriote-grec et chypriote-turc ont affirm� leur volont� de parvenir � un r�glement ��aussi vite que possible�� pour une r�unification de l'�le, lors de la reprise hier des n�gociations favoris�es par la perspective d'une exploitation des r�serves gazi�res.
Le pr�sident de la R�publique de Chypre, Nicos Anastasiad�s (chypriote-grec), et le dirigeant de la R�publique turque de Chypre Nord (RTCN, autoproclam�e), Dervis Eroglu, se sont rencontr�s pendant une heure et demie dans les locaux de l'ONU sur l'a�roport d�saffect� de Nicosie, en pr�sence de la responsable des Nations unies � Chypre, Lisa Buttenheim. ��J'esp�re qu'aujourd'hui est le d�but de la fin d'une situation ind�sirable et inacceptable qui a maintenu notre �le et notre peuple divis�s pendant quarante ans��, a dit M. Anastasiad�s au terme des discussions, les premi�res depuis deux ans. En Turquie, le Premier ministre Recep Tayyip Erdogan a estim� que l'on ��se dirige vers un nouveau processus � Chypre, et si Dieu le veut, il n'y aura pas de marche arri�re et le probl�me chypriote sera r�gl頻.
Le pays est ainsi entr� divis� dans l'Union europ�enne en 2004, apr�s l'�chec d'un premier accord de r�unification largement approuv� lors d'un r�f�rendum par les Chypriotes-Turcs mais rejet� par les Chypriotes-Grecs. Des discussions poussives avaient repris en 2008 puis suspendues en 2012. Apr�s des mois d'�pres discussions, les deux dirigeants se sont mis d'accord sur une d�claration conjointe pr�par�e par l'ONU pour fixer le cadre des pourparlers. � l'issue de la rencontre, Mme Buttenheim a lu cette d�claration devant la presse. ��Les dirigeants auront pour objectif de parvenir � une solution aussi vite que possible et d'organiser ensuite des r�f�rendums distincts��, annonce le texte. La responsable onusienne a pr�cis� que la prochaine rencontre des n�gociateurs �tait pr�vue ��cette semaine��.
Le communiqu� commun ���tablit une base solide pour la reprise des n�gociations en vue d'un r�glement global juste et durable��, a pour sa part r�agi l'Union europ�enne. Il devrait aider les deux parties ��� s'attaquer le plus vite possible aux questions de substance pour parvenir rapidement � des r�sultats��.
Nouvelle donne
M�me si les n�gociations chypriotes n'ont jusqu'� pr�sent rien donn�, la r�cente d�couverte de gisements gaziers au large de l'�le comme pr�s des c�tes isra�liennes a chang� la donne. Pour Hubert Faustmann, professeur d'histoire et de sciences politiques � l'Universit� de Nicosie, il s'agit de ��la plus grande opportunit� pour la paix depuis 2004��. ��Washington a mis beaucoup de poids dans ce dernier effort de paix parce que le gaz et le p�trole changent le jeu (...). C'est une situation gagnant-gagnant pour tout le monde��, estime-t-il. Isra�l, qui r�fl�chit � exporter son gaz via un gazoduc passant dans les eaux chypriotes et la Turquie ou � investir dans une usine de liqu�faction de gaz sur l'�le m�diterran�enne, ��ne donnera pas son gaz � Chypre � moins qu'il y ait une solution��, note M. Faustmann.
(Source�: AFP)
