TITRE: Airbus : une nouvelle commande de 20 appareils A380
DATE: 2014-02-12
URL: http://www.boursorama.com/actualites/airbus--une-nouvelle-commande-de-20-appareils-a380-fbbc0da5eea569ea33cbc27ec2af1db7
PRINCIPAL: 166832
TEXT:
Imprimer l'article
Airbus : une nouvelle commande de 20 appareils A380
Airbus a annonc� mercredi la commande ferme de vingt de ses tr�s gros porteurs A380 par la compagnie Amedeo, ex-Doric Lease Corp, sp�cialis�e dans le financement d'achats d'avions pour le compte des compagnies a�riennes (leasing).�Le prix catalogue pour cette commande, sign�e lors du salon a�ronautique de Singapour, est de 8,3 milliards de dollars US. Un protocole d'accord avait �t� sign� au salon du Bourget (France) en juin dernier.�
�Cette commande ferme de la part de Amedeo souligne clairement les qualit�s � long terme de l'appareil aux yeux du march�, a d�clar� le directeur commercial du constructeur, John Leahy.�Le constructeur a un objectif d'�une trentaine de commandes au total pour l'A380 cette ann�e�, a ajout� le directeur commercial.�En novembre dernier, le directeur financier Harald Wilhelm, avait �voqu� �plus de 30 commandes� pour 2014, et 30 pour 2015.
�Une machine imbattable�
�Le trafic a�rien mondial double tous les 15 ans, contrairement aux infrastructures des a�roports et au nombre de cr�neaux de d�collage et d'atterrissage. L'A380 est donc la meilleure solution pour capturer la croissance� du trafic, a soulign� Mark Lapidus, le patron d'Amedeo, une soci�t� sp�cialis�e dans le leasing d'appareils aux compagnies.
�Plac�e sur les liaisons ad�quates, lors des p�riodes de pic de trafic, c'est une machine imbattable � g�n�rer des b�n�fices pour les compagnies�, a assur� Mark Lapidus.
Des A320 et A321 pour VietJetAir
Le tr�s gros porteur A380, un avion dot� d'un �tage, est le plus gros appareil au monde pour le transport des personnes, avec une capacit� de 500 passagers.�Les appareils command�s seront livr�s entre 2016 et 2020.�Depuis son entr�e en service en 2007, quelque 120 A380 sont op�rationnels au sein d'une dizaine de compagnies.
A l'ouverture du salon de Singapour, la compagnie vietnamienne VietJetAir avait pass� une commande de 63 Airbus, des A320 ceo et A321 ...
