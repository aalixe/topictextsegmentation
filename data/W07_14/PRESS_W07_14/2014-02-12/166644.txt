TITRE: Patch Tuesday f�vrier 2014 : Microsoft rajoute des correctifs critiques pour IE et Windows - Le Monde Informatique
DATE: 2014-02-12
URL: http://www.lemondeinformatique.fr/actualites/lire-patch-tuesday-fevrier-2014-microsoft-rajoute-des-correctifs-critiques-pour-ie-et-windows-56546.html
PRINCIPAL: 166642
TEXT:
Patch Tuesday f�vrier 2014 : Microsoft rajoute des correctifs critiques pour IE et Windows
Cr�dit Photo: D.R
Petite surprise pour la livraison du Patch Tuesday de f�vrier avec l'int�gration en derni�re minute de deux mises � jour critiques suppl�mentaires pour IE et Windows.
Finalement ce n'est pas cinq bulletins de s�curit� que Microsoft a publi� lors de son Patch Tuesday du mois de f�vrier, mais sept. En effet, la firme de Redmond a ajout� deux mises � jour critiques suppl�mentaires couvrant IE et Windows. Aucune explication n'a �t� fournie par Microsoft pour cette int�gration tardive, hormis le fait que les tests avaient �t� finalis�s. L'ann�e derni�re, la soci�t� avait rappel� 23 patchs en raison notamment de tests incomplets. Les sp�cialistes de la s�curit� saluent cette prudence et les efforts pour proposer des correctifs efficients.
IE et Windows corrig�s
Sur les deux bulletins compl�mentaires et class�s comme critiques, le premier vise IE et corrige 24 failles dans le navigateur, y compris une d�j� connue du public. La plus grave de ces vuln�rabilit�s autorise l'ex�cution de code � distance via la visite d'une page web compromise. L'autre mise � jour critique porte sur des failles dans Windows dont une localis�e dans le moteur de script VBScript et une autre dans le logiciel d'acc�l�ration mat�rielle Direct2D.
Pour m�moire, les autres correctifs critiques visaient les derni�res versions de Windows et le logiciel Forefront associ� au serveur Exchange.
