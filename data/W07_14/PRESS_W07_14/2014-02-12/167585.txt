TITRE: Centrafrique: la pr�sidente promet "la guerre" aux anti-balaka, Paris exclut toute partition - RTL info
DATE: 2014-02-12
URL: http://www.rtl.be/info/monde/international/1068907/centrafrique-la-presidente-promet-la-guerre-aux-anti-balaka-paris-exclut-toute-partition
PRINCIPAL: 167583
TEXT:
R�agir (0)
La pr�sidente centrafricaine Catherine Samba Panza a promis mercredi "la guerre" aux miliciens anti-balaka, qui multiplient les exactions contre les civils musulmans au risque de conduire le pays � une partition que Paris, principal alli� de Bangui, juge inacceptable.
"Les anti-balaka (milices d'autod�fense � dominante chr�tienne), on va aller en guerre contre eux. (Ils) pensent que parce que je suis une femme, je suis faible. Mais maintenant les anti-balaka qui voudront tuer seront traqu�s", a d�clar� Mme Samba Panza en sango, la langue nationale, devant les habitants de Mba�ki (80 km au sud-ouest de Bangui), lors d'une visite en compagnie du ministre fran�ais de la D�fense Jean-Yves Le Drian.
Ces propos offensifs font �cho aux d�clarations mena�antes tenues ces derniers jours par M. Le Drian et les commandants des contingents fran�ais et africain en Centrafrique, qui visaient directement les miliciens et les pillards s�vissant en toute impunit�.
"Les anti-balaka ont perdu le sens de leur mission. Ce sont eux qui tuent, qui pillent, qui volent. Est-ce que c'est normal?", a martel� la pr�sidente, tout en r�cusant le terme de "nettoyage ethnique" utilis� par Amnesty International pour d�crier la crise. "Je ne pense pas qu'il y ait d'�puration confessionnelle ou ethnique. Il s'agit d'un probl�me d'ins�curit�", a-t-elle estim�.
Le risque de la partition
La Centrafrique a sombr� dans le chaos depuis le coup d'Etat en mars 2013 de Michel Djotodia, chef de la coalition rebelle S�l�ka � dominante musulmane.
Devenu pr�sident, il a �t� contraint � la d�mission par la communaut� internationale le 10 janvier pour son incapacit� � emp�cher les tueries entre ex-S�l�ka et milices anti-balaka, qui ont entra�n� un exode de civils musulmans, essentiellement vers le Tchad et le Cameroun voisins.
Le secr�taire g�n�ral de l'ONU Ban Ki-moon s'est �mu mardi du risque d'une possible partition du pays, apr�s des mois de violences interreligieuses. "La brutalit� sectaire est en train de changer la d�mographie du pays, la partition de facto de la RCA (R�publique centrafricaine) est un risque av�r�", a-t-il dit.
D'anciens responsables de la S�l�ka avaient d�j� fait �tat publiquement de leur volont� s�cessionniste depuis les confins nord-est du pays, majoritairement peupl�s de musulman, qui �chappent depuis des ann�es au contr�le d'un Etat centrafricain en �tat de quasi-faillite permanente.
"Personne n'acceptera quelque partition que ce soit. Il faut absolument l'emp�cher", a r�pliqu� le ministre fran�ais de la D�fense � Mbaiki. "Pour la France, il n'y a et il n'y aura qu'une seule Centrafrique, qu'une seule chef de l'Etat. Toute tentative de penser autrement rencontrera l'opposition de la France et celle de la communaut� internationale", a poursuivi M. Le Drian.
De son c�t�, Mme Samba Panza a fait �tat de sa "ferme volont� de ne pas c�der un seul pouce du territoire centrafricain, qui a toujours �t� uni et la�c".
Pont a�rien de l'ONU
C'est la troisi�me fois que M. Le Drian se rend en Centrafrique depuis le d�clenchement de l'op�ration militaire fran�aise "Sangaris", le 5 d�cembre.
Mardi � Brazzaville, il avait durci le ton contre les milices s�vissant en Centrafrique, affirmant que les forces internationales �taient pr�tes � mettre fin aux exactions "par la force".
"Il faut que l'ensemble des milices qui continuent aujourd'hui � mener des exactions, � commettre des meurtres, arr�tent", avait averti M. Le Drian.
Le contingent fran�ais et la force de l'Union africaine en Centrafrique (Misca) agissent sous l'�gide de l'ONU, qui leur a donn� en d�cembre un mandat autorisant l'emploi de la force en cas de menace directe sur la population civile.
Jusqu'� pr�sent, les militaires �trangers ne sont pas parvenus � mettre fin aux violences meurtri�res et aux pillages.
La Misca a actuellement 5.400 hommes - sur les 6.000 pr�vus - sur le terrain, soutenus par 1.600 soldats fran�ais.
Selon Amnesty International, les exactions des anti-balaka rel�vent d�sormais du "nettoyage ethnique".
"Les soldats de la force internationale de maintien de la paix ne parviennent pas � emp�cher le nettoyage ethnique des civils musulmans dans l'ouest de la R�publique centrafricaine", ass�ne Amnesty, appelant la communaut� internationale � "faire barrage au contr�le des milices anti-balaka et � d�ployer des troupes en nombre suffisant dans les villes o� les musulmans sont menac�s".
Selon l'ONU, 1,3 million de personnes, soit plus d'un quart de la population de Centrafrique, ont besoin d'une assistance alimentaire imm�diate, en particulier dans les camps de d�plac�s o� s'entassent plus de 800.000 personnes, dont plus de la moiti� � Bangui.
Le Programme alimentaire mondial (PAM) a lanc� mercredi un pont a�rien entre Douala (Cameroun) et Bangui pour acheminer des vivres pour 150.000 personnes pendant un mois, mais cela restera insuffisant face � l'ampleur de la crise.
Galerie - Centrafrique: la pr�sidente promet "la guerre" aux anti-balaka, Paris exclut toute partition
