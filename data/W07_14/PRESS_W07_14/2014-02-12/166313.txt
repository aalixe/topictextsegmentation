TITRE: En Espagne, la future loi sur l'avortement passe une �tape cruciale
DATE: 2014-02-12
URL: http://www.lemonde.fr/europe/article/2014/02/12/en-espagne-la-future-loi-sur-l-avortement-passe-une-etape-cruciale_4364726_3214.html
PRINCIPAL: 166311
TEXT:
En Espagne, la future loi sur l'avortement passe une �tape cruciale
Le Monde |
� Mis � jour le
12.02.2014 � 10h40
Le Parti socialiste espagnol (PSOE) n'est pas parvenu � faire obstacle au projet de loi controvers� d�fendu par les conservateurs au pouvoir , limitant le droit � l'avortement. Le projet de loi supprimant le droit � l'avortement a pass� un premier obstacle au Congr�s des d�put�s, mercredi 12�f�vrier, qui a rejet�, � bulletins secrets, une proposition de retirer le texte. Cent quatre-vingt-trois d�put�s ont vot� contre la motion du PSOE, qui r�clamait ��le retrait imm�diat�� du texte, et 151 d�put�s ont vot� pour. Six se sont abstenus.
Le Parti populaire (PP), qui dispose de la majorit� absolue avec 185�si�ges, semble avoir quasiment fait le plein des voix. Il est le seul avec Unio, le petit parti des d�mocrates-chr�tiens catalans, � soutenir le projet de loi.
Le porte-parole du PP du chef du gouvernement Mariano Rajoy, Alfonso Alonso, s'�tait auparavant dit certain que la discipline de vote serait observ�e dans les rangs du parti. S'il est adopt�, le projet de loi fera de l' Espagne l'un des pays europ�ens le plus restrictifs en la mati�re. La l�gislation actuelle autorise l'IVG jusqu'� la 14e�semaine de grossesse .
Regarder la carte : En Europe, les femmes in�gales face � l'avortement
��L'IN�GALIT� VA PROGRESSER��
Pourtant, le texte a �galement suscit� de fortes r�ticences au sein de la droite. L'une des premi�res voix dissonantes fut celle de la premi�re vice-pr�sidente du Congr�s, Celia Villalobos, qui a demand� la libert� de vote sur ce texte. Plusieurs pr�sidents de r�gions PP, comme de l'Estr�madure Jos� Antonio Monago ou de Galice, Alberto Nu�ez Feijoo, un fid�le de Mariano Rajoy, ont demand� sa suspension.
��Si c'est adopt�, le nombre d'avortements en Espagne continuera � augmenter , et beaucoup seront plus dangereux pour les femmes, a estim� Elena Valenciano, secr�taire g�n�rale adjointe du PSOE, lors du d�bat qui a pr�c�d� le vote. L'in�galit� va progresser , et les Espagnoles seront une fois plus divis�es en deux cat�gories : celles qui peuvent se rendre � l'�tranger pour avorter en toute s�curit� et celles qui ne peuvent pas.��
��Nous sommes pr�ts au dialogue��, a r�pondu Marta Torrado de Castro, au nom du PP. Le texte, rejet� par une tr�s large majorit� des Espagnols, a �t� approuv� en d�cembre par le gouvernement de Mariano Rajoy, qui, pour beaucoup, cherchait ainsi � apaiser l'aile droite de sa formation . Le PP a depuis perdu du terrain dans les intentions de vote.
