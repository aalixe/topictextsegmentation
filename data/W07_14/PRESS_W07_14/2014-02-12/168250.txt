TITRE: Attention aux faux Flappy Bird qui abritent des malware
DATE: 2014-02-12
URL: http://www.presse-citron.net/le-retrait-de-flappy-bird-profite-aux-malwares-et-aux-arnaqueurs
PRINCIPAL: 168246
TEXT:
Jeu vid�o - 12 f�vrier 2014 :: 16:00 :: Par Setra
Le retrait de Flappy Bird profite aux malwares et aux arnaqueurs
Apr�s le retrait de Flappy Bird, des versions fake sont apparues sur des stores d�applications alternatives. Celles-ci peuvent repr�senter des menaces.
print
La saga de Flappy Bird, le jeu qui rend fou, n�est pas encore termin�e. Hier encore, nous avons �voqu� les explications que le d�veloppeur de ce jeu a fournies au magazine Forbes , par rapport � sa d�cision de retirer Flappy Bird. Selon lui, le jeu est devenu trop addictif et ceci repr�sentait un probl�me. Il a donc estim� qu�il �tait plus sage de le retirer. Sur Twitter, il a n�anmoins affirm� qu�il continuera � d�velopper des jeux.
Mais Flappy Bird est un jeu tr�s populaire, et le buzz caus� par son retrait (et toutes les histoires qui ont tourn� autour de celui-ci) l�a rendue encore plus connu. De ce fait, m�me si vous ne trouverez plus de Flappy Bird sur Google Play et dans l�App Store, il est possible que vous tombiez sur des ��Flappy Bird�� que l�on vous proposera de t�l�charger et d�installer ��manuellement��.
Sur Android, il s�agit g�n�ralement �de fichiers .apk que vous pouvez installer sur votre terminal en activant l�installation d�applications de sources inconnues. C�est simple, pas de Jailbreak ou ni que ce soit d�autre.
Mais l�ennui, c�est que ces fichiers que vous ramassez un peu n�importe o�, vous ne savez pas d�o� ils viennent. Vous ne connaissez pas la personne ou l�organisation qui les a publi�s, et vous ne savez pas si des �l�ments du code de l�application ont �t� modifi�s pour vous nuire.
Au pire, vous installez une appli qui ne marche m�me pas et au mieux, vous avez une application qui fonctionne comme le vrai. Mais dans tous les cas, celle-ci peut vous voler des informations secr�tement. Donc, il faut se m�fier de tout ce qui ne sort pas de Google Play ( m�me l�, on se m�fie un peu ).
Mais pour les utilisateurs les moins conscients, les avertissements ne sont peut-�tre pas suffisants pour emp�cher de t�l�charger et installer un .apk venu de nulle part. De ce fait, de nombreux m�dias rapportent des ��Flappy Bird�� un peu louches disponibles sur des sites de t�l�chargement d�applications alternatifs.
Il y aurait par exemple une version ��Demo�� qui vous demande ensuite de payer pour poursuivre le jeu. Une autre fonctionnerait de la m�me mani�re que le Flappy Bird original mais r�cup�rerait aussi des informations de contact sur le smartphones.
On notera que les menaces cit�es ci-dessus sont encore mineures et assez courantes. Mais votre addiction � Flappy Bird pourrait vous co�ter encore plus cher. N�oublions pas qu�un smartphone peut contenir des donn�es professionnelles ou une application de banque.
