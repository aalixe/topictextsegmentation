TITRE: SPORT - Schumacher souffre d'une infection pulmonaire, selon un tablo�d allemand - France 24
DATE: 2014-02-12
URL: http://www.france24.com/fr/20140212-schumacher-souffre-infection-pulmonaire-repond-pas-stimuli-bild-coma/
PRINCIPAL: 167735
TEXT:
Texte par FRANCE 24 Suivre france24_fr sur twitter
Derni�re modification : 12/02/2014
Dans le coma depuis fin d�cembre, l'�tat de sant� de Michael Schumacher ne montre toujours pas d'�volution positive. Selon le journal allemand "Bild", il ne r�pondrait pas aux stimuli des m�decins et aurait contract� une infection pulmonaire.
Les nouvelles ne sont pas bonnes du c�t� du CHU de Grenoble. L'�tat de sant� de Michael Schumacher , plong� dans le coma depuis le 29 d�cembre apr�s un grave accident de ski, ne pr�sente pas de signe d'am�lioration.
Comme le rappporte le quotidien allemand "Bild " dans son �dition du mercredi 12 f�vrier, le septuple champion du monde ne r�agit pas aux diff�rents stimuli pratiqu�s par l'�quipe du professeur Payen, chef du service de r�animation. Alors que les m�decins ont lanc� il y a deux semaines une phase de r�veil progressif , l'ancien pilote aurait m�me contract� une infection pulmonaire.
L'entourage de Michael Schumacher n'a toutefois pas confirm� ces informations. "Nous ne commentons pas les sp�culations", a d�clar� Sabine Kehm, la porte-parole du sportif allemand � "Bild". La semaine derni�re, la famille du champion de Formule 1 avait �galement d� d�mentir des rumeurs annon�ant la mort du pilote de course sur les r�seaux sociaux.
Depuis sa chute le 29 d�cembre dans la station de ski de Courchevel, sa femme Corinna est quotidiennement rest�e aupr�s de Michael Schumacher. Au moment de son admission au CHU de Grenoble, ce dernier souffrait de l�sions cr�niennes diffuses et s�rieuses et avait �t� plong� dans un coma artificiel.
Avec AFP
