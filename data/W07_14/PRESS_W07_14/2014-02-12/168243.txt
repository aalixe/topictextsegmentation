TITRE: BoE-Une hausse des taux peut-�tre en 2015, devises forex - Les Echos Bourse
DATE: 2014-02-12
URL: http://bourse.lesechos.fr/forex/infos-et-analyses/boe-une-hausse-des-taux-peut-etre-en-2015-950603.php
PRINCIPAL: 168242
TEXT:
BoE-Une hausse des taux peut-�tre en 2015
12/02/14 � 12:07
- Reuters 0 Commentaire(s)
LONDRES, 12 f�vrier (Reuters) - La Banque d'Angleterre ( BoE ) a laiss� entendre mercredi qu'une premi�re hausse des taux pourrait n'intervenir qu'en 2015 et a fortement revu en hausse ses pr�visions de croissance pour les trois ann�es � venir.
Dans son rapport trimestriel sur l'inflation, la BoE indique que les march�s partent sur l'hypoth�se d'une premi�re hausse des taux au deuxi�me trimestre 2015.
La banque centrale souligne que le rel�vement des taux sera progressif et qu'il s'arr�tera sans doute bien en de�� de 5%, la moyenne pr�valant avant la crise financi�re.
"Malgr� la forte baisse du ch�mage , il reste possible d'absorber des capacit�s inemploy�es avant d'augmenter les taux", �crit la BoE dans son rapport.
"Lorsque le taux d'intervention commencera vraiment � monter, la trajectoire appropri�e pour �liminer les points de faiblesse (de l'�conomie) dans les deux � trois ann�es � venir et conserver une inflation proche de l'objectif sera sans doute progressive".
Pour 2014, la BoE a revu � 3,4% sa pr�vision de croissance contre 2,8% anticip�s en novembre. Elle pr�voit 2,7% de croissance en 2015 (2,3% en novembre) et 2,8% en 2016 (2,5%).
L'institut d'�mission a d� pr�ciser quand et de quelle mani�re il entendait relever les taux apr�s une baisse exceptionnellement forte du ch�mage depuis l'inauguration de la pratique de communication avanc�e ("forward guidance") en ao�t.
Peu apr�s son arriv�e � la t�te de la BoE, le gouverneur Mark Carney s'�tait engag� � observer le statu quo sur les taux tant que le taux de ch�mage ne serait pas retomb� � 7%.
La BoE pensait alors qu'il faudrait trois ans pour cela. A peine six mois plus tard, il est � 7,1% et il baisserait encore � 6,5% d'ici le d�but 2015, selon la banque centrale.
Pour l'inflation, la BoE estime qu'elle tombera � 1,7% en mars avant de revenir pr�s de l'objectif de 2% dans les deux prochaines ann�es. (David Milliken et Ana Nicolaci da Costa, Wilfrid Exbrayat pour le service fran�ais, �dit� par Marc Joanny)
Laisser un commentaire
