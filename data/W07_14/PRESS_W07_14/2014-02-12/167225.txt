TITRE: La justice ordonne � Dieudonn� de retirer deux passages d�une vid�o sur internet - France-Monde - La Voix du Nord
DATE: 2014-02-12
URL: http://www.lavoixdunord.fr/france-monde/la-justice-ordonne-a-dieudonne-de-retirer-deux-passages-ia0b0n1914341
PRINCIPAL: 167174
TEXT:
La justice a ordonn� mercredi � l�humoriste controvers� Dieudonn� de retirer deux passages de la vid�o � 2014 sera l�ann�e de la quenelle � diffus�e sur son compte YouTube.
Par ses quenelles, Dieudonn� a provoqu� un remous m�diatique et politique important. AFP PHOTO PATRICK KOVARIK
Saisi en r�f�r� (proc�dure d�urgence), le tribunal de grande instance de Paris a notamment estim� que l�un des passages constitue une contestation de crimes contre l�Humanit� et a �cart� l�humour invoqu� par Dieudonn�.
En cons�quence, deux passages de son spectacle � 2014 sera l�ann�e de la quenelle � devront �tre supprim�s.
L�Union des �tudiants juifs de France (UEJF), � l�origine de cette action visant � obtenir le retrait de cette vid�o, compte � pr�sent saisir le parquet � pour que les poursuites p�nales qui s�imposent soient engag�es � son initiative �, a d�clar� l�avocat de l�association, St�phane Lilti.
D�autre part, l�UEJF va demander � YouTube de � se rapprocher des associations pour voir dans quelles conditions ils respecteront � l�avenir leurs obligations l�gales �.
Dans la vid�o litigieuse, Dieudonn� d�clarait notamment : � Je suis n� en 66, donc j��tais pas n� (...) moi les chambres � gaz j�y connais rien, si tu veux vraiment je peux t�organiser un rencard avec Robert �, en allusion � l�historien n�gationniste Robert Faurisson. Ce passage reprenait un extrait de son spectacle � Le Mur �, interdit dans plusieurs villes fran�aises en raison notamment de ses passages jug�s antis�mites.
Rappelant le � contexte plus g�n�ral des d�clarations � de Dieudonn�, � dont certaines lui ont valu des condamnations pour injure, diffamation et provocation � la haine antis�mite �, le juge Marc Bailly a estim� qu�il � ressort bien de cette formulation (...) une contestation � de crime contre l�Humanit�.
Dans son ordonnance, le magistrat consid�re que � l�humour invoqu� � par la d�fense de Dieudonn� � n�appara�t que comme le moyen de v�hiculer publiquement des convictions en
testant les limites de la libert� d�expression
� en l�esp�ce d�pass�es � et non comme le ressort d�un sketch comique et provocateur dont les exc�s pourraient �tre admis �.
La d�cision pr�voit une astreinte de 500 euros par jour de retard constat� pour chacun des passages si Dieudonn� ne les retire pas dans un d�lai de cinq jours � compter de la signification de l�ordonnance.
Cet article vous a int�ress� ? Poursuivez votre lecture sur ce(s) sujet(s) :
