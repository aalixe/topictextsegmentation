TITRE: Tennis/viols : peine aggrav�e pour R�gis de Camaret, condamn� en appel � 10 ans (VIDEO) - Tennis - La Voix du Nord
DATE: 2014-02-12
URL: http://www.lavoixdunord.fr/sports/tennisviols-peine-aggravee-pour-regis-de-camaret-ia233b0n1913428
PRINCIPAL: 165651
TEXT:
L�ancien entra�neur de tennis vedette R�gis de Camaret, rejug� en appel � Draguignan pour les viols aggrav�s de deux anciennes �l�ves mineures a vu sa peine aggrav�e, �copant de 10 ans de prison.
R�gis de Camaret a vu sa peine aggrav�e en appel. PHOTO AFP
- A +
En novembre 2012, en premi�re instance � Lyon, il avait �t� condamn� � huit ans de prison. R�gis de Camaret a sembl� sonn� � l�enonc� du verdict. Ses nombreuses victimes se sont elles longuement �treintes.
La championne de tennis Isabelle Demongeot, qui avait �t� � l�origine de la proc�dure en 2005 pour des viols prescrits, s�est exprim�e au c�t� des deux seules parties civiles du proc�s, Karine et St�phanie, qui esquissaient enfin de timides sourires. � Les parties civiles et les prescrites sont indisociables �, a lanc� la championne, ajoutant :� Dix ans c�est juste. Ce verdict est apaisant �. � C�est d�finitif �, s�est r�jouie Karine.
� J�ai honte et je demande pardon, c�est tout �
Apr�s s��tre mur� dans le silence pendant sept jours, R�gis de Camaret avait d�clar� � la surprise g�n�rale avoir honte au dernier jour du proc�s. � Cette petite prise de conscience, on ne s�y attendait pas du tout, �a fait du bien �, a affirm� St�phanie, avant de quitter le tribunal. De Camaret avait simplement consenti � evoquer des erreurs, en �tant trop familier avec des jeunes filles qui auraient mal interpr�t� ses gestes, disait-il, puis menti, voire particip� � un complot.
Isabelle Demongeot a confi� avoir eu � le souffle coup� � en entendant ce laconique pardon. � C�est un soulagement, c�est bon quand m�me �, a-t-elle dit. A Lyon, R�gis de Camaret avait seulement demand� pardon � St�phanie. La championne a r�p�t� durant le proc�s comment de Camaret lui avait� tout enlev� �, en la violant durant neuf ans � partir de l��ge de 13 ans.
Attouchements, sodomies sur des enfants de 10 � 14 ans
Parmi toutes ses anciennes �l�ves, entendues en tant que simples t�moins pour des faits prescrits et aujourd�hui �g�es de 37 � 50 ans, dix ont �voqu� des viols ou des tentatives de viols et douze des attouchements sexuels. Leurs t�moignages ont visiblement choqu� les membres du jury. Marjolaine a racont� comment l�entra�neur l�avait sodomis�e et bless�e de mani�re bestiale � 12 ans. La fillette n�avaient alors pas �t� crue par ses parents. Marion a d�taill� comment il lui avait impos� des attouchements sexuels d�s ses 10 ans, avant de la violer � 14 ans. Elle avait �t� la premi�re � d�poser une plainte en 2002. Une plainte sans suite pour des faits prescrits.
L�avocat g�n�ral Philippe Guemas, qui avait r�clam� de 12 � 15 ans de prison, a estim� que l�appel de l�ex-entra�neur �tait assez sordide, car son prix �tait la douleur des victimes qui ont d� �tre replong�es dans le cauchemar. � C�est tr�s difficile au cr�puscule de sa vie de reconna�tre qu�on est un salaud �, a dit Me Eric Dupond-Moretti. � Jugez-le le moins mal possible �, a demand� aux jur�s le t�nor du barreau de Lille, sans solliciter l�acquittement.
Cet article vous a int�ress� ? Poursuivez votre lecture sur ce(s) sujet(s) :
