TITRE: Corse : une candidate aux municipales moquée sur le Web - Elle
DATE: 2014-02-12
URL: http://www.elle.fr/Societe/News/Corse-une-candidate-aux-municipales-moquee-sur-le-Web-2667355
PRINCIPAL: 0
TEXT:
Hélène Guinhut @heleneguinhut
[ Tous ses articles ]�
Depuis plusieurs jours, Caroline Bartoli, candidate aux municipales de Propriano en Corse et novice en politique, est devenue la risée du Web. A l’origine de ces moqueries, une interview donnée le 6 février dernier sur le plateau de France 3. Invitée pour évoquer son programme, elle a livré une prestation plutôt médiocre. Sur le plateau, elle enchaîne réponses hésitantes, improvisations et longs silences, tout en gardant le regard fixé sur ses fiches.� « J’ai cru à un fake », « totalement délirant », « interview surréaliste », « ce n’est pas un sketch », ont commenté les internautes sur Twitter.
Il faut dire que la candidature de Caroline Bartoli est atypique. Si elle est élue maire, elle s’est engagée à démissionner au profit de son mari. Son époux, Paul-Marie Bartoli, est en effet inéligible jusqu’en mai 2014, suite à l’invalidation de ses comptes de campagne 2012.
Son mari dénonce un « acharnement médiatique scandaleux »
Au cours de l’interview, Caroline Bartoli répète inlassablement, « je continuerai ce qu’a commencé mon mari ». Mardi, Paul-Marie Bartoli a réagi sur le site « Corse Net Infos », refusant de commenter les moqueries dirigées contre sa femme. « Il est inenvisageable que je puisse alimenter une polémique due à un acharnement médiatique scandaleux ! Il est totalement exclu que nous répondions à des commentaires qui consistent à salir une mère de famille ! », s’est-il emporté. « Cela ne nous intéresse pas du tout de nous mettre à ce niveau, c’est-à-dire au niveau du caniveau. Nous ne répondrons pas à ces calomniateurs », a-t-il ajouté, avant de conclure : « Nous n’avons, nous, qu’un seul juge : c’est le corps électoral ! »
 
