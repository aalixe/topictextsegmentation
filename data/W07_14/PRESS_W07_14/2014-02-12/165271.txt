TITRE: Audi S1, la voil�
DATE: 2014-02-12
URL: http://www.caradisiac.com/Audi-S1-la-voila-92351.htm
PRINCIPAL: 165267
TEXT:
Audi S1, la voil�
Ecrit par Patrick Garcia le 11 F�vrier 2014
Elle a mis du temps � arriver mais elle a quand m�me r�ussi � se montrer avant l'heure officielle � pr�vue pour demain matin. L'Audi S1 qui sera pr�sent�e � Gen�ve se montre rev�tue d'un joli jaune poussin qui n'arrive pas � la faire passer pour gentille.
C'est avec quelques heures d'avance que l'Audi S1 tant attendue arrive sur la Toile par le biais de 5 images en fuite. Cette S1 qui sera pr�sent�e � Gen�ve ne r�v�le encore rien de sa m�canique mais les stickers sur les flancs sons sans ambigu�t�s, ce sera bien une quattro ! Depuis la sortie de l'extraterrestre A1 quattro � m�canique de TTS, nous savions que lorsque la S1 viendrait, elle se positionnerait entre l'A1 de 185 ch et la quattro de 256 ch, on peut donc imaginer une puissance tournant autour de 220/230 ch.
Normalement, la bo�te S-Tronic ne devrait pas �tre de la partie, tout du moins en s�rie, alors que le look adopt� par ce mod�le aux belles couleurs jaune et noir reste d'une sobri�t� typiquement Audi. On remarque �galement la nouvelle calandre et les naseaux aux angles plus aiguis�s que l'on devrait retrouver sur les A1 restyl�es ainsi que les nouveaux optiques avant bien plus vindicatif dans leur dessin int�rieur.
Les jantes qui cachent des �triers rouges � logo S1 paraissent de bon diam�tre, probablement du 18 pouces comme sur l' A1 quattro qui lui aura �galement donn� sa transmission int�grale ainsi que toutes les modifications ch�ssis qu'elles imposaient. Le reste des d�tails sera disponible d�s demain.
