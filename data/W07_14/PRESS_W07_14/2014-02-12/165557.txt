TITRE: Orange, SFR et Bouygues T�l�com ��lamin�s�� par Free
DATE: 2014-02-12
URL: http://www.universfreebox.com/article/24181/Orange-SFR-et-Bouygues-Telecom-lamines-par-Free
PRINCIPAL: 165552
TEXT:
Cat�gorie Br�ves
, publi� le 10 f�vrier 2014 � 01h15 par Fouzi Habibi
Le magazine Challenges a publi� un comparatif concernant l?�volution du chiffre d?affaires (tendance sur les neuf premiers mois de l?ann�e 2013), mais aussi de l?�volution l?EBITDA  (autrement dit, l?exc�dent brut d?exploitation) et de la capitalisation boursi�re des quatre grands op�rateurs fran�ais�: Free, Orange, SFR et Bouygues�T�l�com (sur deux ans).
�
En observant ces chiffres, on peut constater que les trois op�rateurs historiques r�gressent nettement. Si le Trublion garde le cap et affiche de tr�s bons r�sultats dans tous les domaines, avec un chiffre d?affaires en hausse de 82%, un EBITDA de + 337 millions d?euros et une progression de sa capitalisation boursi�re de 4,5 milliards d?euros, on ne peut pas dire la m�me chose de SFR, Orange et Bouygues T�l�com.
�
La plus grosse baisse en mati�re de chiffre d?affaires revient � Bouygues T�l�com avec une chute vertigineuse de 18,5%.�L?EBITDA�n?est pas �pargn� puisqu?il est �galement en recul (-303 millions d?euros). Seule la capitalisation du groupe Bouygues est dans le vert avec une progression d?1,6 milliard d?euros. Pour SFR, le constat est similaire.�Le chiffre d?affaires accuse une forte baisse de 17% sur les trois premiers trimestres de l?ann�e 2013. L?EBITDA chute �galement (- 865 millions d?euros) mais la capitalisation du groupe Vivendi progresse de 6,1 milliards d?euros. Enfin pour Orange, tout est dans le rouge.�Le chiffre d?affaires et l?EBITDA�sont en baisse (-10% pour le premier et un recul de 1560 millions d?euros pour le second). La capitalisation boursi�re du groupe Orange connait �galement un repli de 7,3 milliards d?euros.
�
