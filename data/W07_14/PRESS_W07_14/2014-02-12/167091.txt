TITRE: VIDEO - Bande-annonce plus intense de Transcendence avec Johnny Depp
DATE: 2014-02-12
URL: http://www.premiere.fr/Cinema/News-Cinema/Video/VIDEO-Bande-annonce-plus-intense-de-Trenscendence-avec-Johnny-Depp-3951599
PRINCIPAL: 167090
TEXT:
VIDEO - Bande-annonce plus intense de Transcendence avec Johnny Depp
VIDEO - Bande-annonce plus intense de Transcendence avec Johnny Depp
12/02/2014 - 12h50
Br���m ! Le chef-op�rateur d'Inception met en sc�ne son premier film, o� il est une nouvelle fois question d'esprit.
�
La premi�re bande-annonce de� Transcendence insistait sur la transformation de l'homme en machine . La nouvelle, beaucoup plus intense, s'int�resse aux cons�quences d'une telle exp�rience scientifique. Une fois que son esprit est transf�r� dans l'ordinateur, le chercheur incarn� par Johnny Depp devient surpuissant. Au point de d�mat�rialiser les hommes ? Et de se r�incarner ? La vid�o propose plusieurs pistes, mais il est difficile d'en comprendre toutes les images. Logique, puisqu'elle� est cens�e intriguer les spectateurs sans trop d�voiler de d�tails.
La bande-annonce est dans la veine des pr�c�dents films de Christopher Nolan . Ce n'est pas une surprise, puisque ce "techno-thriller" est mis en sc�ne par son fid�le chef-op�rateur, Wally Pfister . Avec ses sons vibrants et son montage tr�s rapide, le trailer rappelle �trangement la promotion d' Inception , qui s'int�ressait d�j� au cerveau humain.
Transcendence, �galement port� par Rebecca Hall , Paul Bettany et Morgan Freeman , sortira en avril. Voici sa premi�re bande-annonce, si vous voulez comparer les deux :
�
