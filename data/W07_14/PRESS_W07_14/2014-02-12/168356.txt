TITRE: France/Monde | Blois : profanation à la mosquée avec une tête de porc
DATE: 2014-02-12
URL: http://www.dna.fr/actualite/2014/02/12/blois-profanation-a-la-mosquee-avec-une-tete-de-porc
PRINCIPAL: 168351
TEXT:
Blois : profanation à la mosquée avec une tête de porc
- Publié le 12/02/2014
LOIR-ET-CHER Blois : profanation à la mosquée avec une tête de porc
La mosquée Sunna de Blois a été profanée dans la nuit de mardi à mercredi par des inconnus qui ont jeté dans son enceinte une tête de porc ainsi que trois autres morceaux de cette viande considérée comme impure par les musulmans.
A leur arrivée à 6 h ce mercredi matin, les fidèles ont découvert dans la cour et sur le toit d�??un des bâtiments, quatre morceaux de viande de porc dont une tête de cochon jetés depuis la rue.
La porte d�??entrée avait été recouverte de l�??inscription «MLE France», un sigle inconnu des services de police.
Une enquête a été ouverte et le maire de Blois, Marc Gricourt, ainsi que le directeur du cabinet du préfet, Frédéric Doué, sont venus en milieu de journée pour constater les faits et témoigner leur soutien à la communauté musulmane.
Boujama Hannou, président de l�??association cultuelle marocaine de la mosquée a appelé au calme.
AFP
Poster un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
