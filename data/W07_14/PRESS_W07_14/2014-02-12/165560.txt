TITRE: Comme on se retrouve** - 12/02/2014 - leParisien.fr
DATE: 2014-02-12
URL: http://www.leparisien.fr/espace-premium/culture-loisirs/comme-on-se-retrouve-12-02-2014-3581635.php
PRINCIPAL: 0
TEXT:
Ev�nement
Comme on se retrouve**
Les Inconnus L�gitimus, Campan et Bourdon reviennent dans les salles dix-neuf ans apr�s avec �les Trois Fr�res: le Retour�.
Pierre Vavasseur |  Publi� le 12 f�vr. 2014, 07h00
Pascal L�gitimus, Bernard Campan et Didier Bourdon r�alisentet interpr�tent leur grand retouren tant que trio au cin�ma. (Prod.)
Mon activit� Vos amis peuvent maintenant voir cette activit�
Tweeter
Com�die fran�aise de Didier Bourdon et Bernard Campan , avec Didier Bourdon, Bernard Campan, Pascal L�gitimus... Dur�e�: 1 h 46
Cette fois, �a y est. Apr�s une strat�gie promotionnelle orchestr�e dans les moindres d�tails (voir encadr�), les Inconnus vont conna�tre aujourd'hui le verdict du public � l'�gard de leurs retrouvailles sur les �crans. Il fallait en effet avoir pass� les derniers mois au fin fond de la banquise pour ne pas savoir que le trio d'humoristes revenait avec une suite aux ��Trois Fr�res��, 7�millions d'entr�es en 1995, sobrement et efficacement baptis�e ��les Trois Fr�res�: le Retour��, qui sort sur 701�copies. Dix-neuf ans apr�s, les fr�res Latour, qui n'ont gu�re brill� par leur r�ussite et dont deux d'entre eux se d�testent cordialement, se retrouvent chez le notaire pour r�cup�rer les cendres de leur m�re et... un reliquat de dette qu'elle leur a laiss�.
Capital sympathie record
D'un budget de 12 �, cette production tr�s attendue devra au moins occuper le terrain jusqu'au 26�f�vrier, date � laquelle un autre blockbuster comique fran�ais viendra s'emparer des �crans�: ��Supercondriaque��, avec Dany Boon aux manettes. Au vu de l'attente que provoque ce ��Retour��, d'un accueil enthousiaste lors des avant-premi�res et, sondages � l'appui (voir nos �ditions de dimanche dernier), d'un capital sympathie record, cette reformation ne semble avoir gu�re de risque de se crasher.
L'ensemble tient la route et le trio n'a rien perdu de son efficacit� de jeu. Structur� sur un sc�nario semblable au premier, le film en r��dite l'humour jusqu'� des s�quences en forme de clins d'oeil appuy�s sur des p�rip�ties mythiques. Les trois fr�res se retrouvent ainsi de nouveau aux prises avec les paradis artificiels, suscitant des dialogues et des situations qui ne font pas dans la dentelle, m�me si l'un d'entre eux est repr�sentant en lingerie coquine. Pourquoi s'en priveraient-ils ? Les Fran�ais aiment les com�dies... bien � la fran�aise.
VIDEO. La bande-annonce des "Trois fr�res, le retour"
