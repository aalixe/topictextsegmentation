TITRE: Michel Gondry revient au clip pour Metronomy - News films Vu sur le web - AlloCin�
DATE: 2014-02-12
URL: http://www.allocine.fr/article/fichearticle_gen_carticle%3D18630746.html?utm_source%3Dfeedburner%26utm_medium%3Dfeed%26utm_campaign%3DFeed%253A%2Bac%252Factualites%2B(AlloCine%2B-%2BRSS%2BNews%253A%2BCinema%2B%2526%2BSeries)
PRINCIPAL: 0
TEXT:
Michel Gondry revient au clip pour Metronomy
Toutes les news Vu sur le web
�
Michel Gondry revient au clip pour Metronomy
mercredi 12 f�vrier 2014 - News - Vu sur le web
Retour � la case d�part pour Michel Gondry, qui n�avait quasiment plus fait de clips depuis 2011. Il revient � ses premi�res amours pour le groupe anglais Metronomy...
Trop occup� par le cin�ma, il avait abandonn� le monde du clip avec une vid�o (Crystalline) pour Bj�rk dont il fut le metteur en sc�ne presque exclusif pendant presque 20 ans. Michel Gondry a d�cid� de repasser derri�re la cam�ra pour mettre en image la musique qu'il aime.
�
Ce sont les chanceux anglais de Metronomy qui profitent donc de sa science du trompe-l�oeil et du bricolage pour le clip de "Love Letters", aussi malin qu�ing�nieux, o� sa cam�ra tourne autour du groupe dans un plan s�quence plein de po�sie.
�
A NE PAS MANQUER EN CE MOMENT
Paul Walker : la surprenante affiche de son dernier film Brick Mansions
En savoir plus : Michel Gondry
A voir aussi
Encore plus de news Vu sur le web
The Amazing Spider-Man 2 : l'affiche qui rend hommage aux comics
mercredi 9 avril 2014 - News - Vu sur le web
Alors qu'une affiche IMAX tr�s graphique a �galement �t� r�v�l�e, "The Amazing Spider-Man 2" rend hommage � ses origines, � travers un poste...
"The Raid 2": incarnez le h�ros du film gr�ce au jeu en 8-bit
mercredi 9 avril 2014 - News - Vu sur le web
"The Raid 2" ne sort que le 23 juillet sur nos �crans. Que les fans impatients se rassurent, gr�ce au jeu en 8-bit, "The Raid 2 Arcade Editi...
Beno�t Brisefer : la bande-annonce de l'adaptation cin� !
mercredi 9 avril 2014 - News - Vu sur le web
Manuel Pradal adapte la c�l�bre bande dessin�e de Peyo, avec G�rard Jugnot et Jean Reno au casting. La sortie salles est pr�vue le 10 d�cemb...
Toutes les news Vu sur le web
Haut de page
Vous devez vous connecter pour �crire un commentaire
Hunnam29
Et m�ga styl� ce r�alisateur !
Ils se sont bien trouv�s.
JimBo Lebowski
Le clip est tr�s sympa, bien Gondrien.
Par contre le morceau est �nervant ... Pourtant Metronomy j'aime assez.
Kao-BB
J'ai d�couvert ce groupe avec The English Riviera, j'aime beaucoup! Vivement leur nouvel album!
Top Bandes-annonces
