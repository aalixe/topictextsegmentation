TITRE: Insolite : Sony vend un walkman �tanche dans une bouteille d'eau
DATE: 2014-02-12
URL: http://www.clubic.com/insolite/actualite-618548-insolite-sony-vend-walkman-etanche-bouteille-eau.html
PRINCIPAL: 0
TEXT:
Insolite : Sony vend un walkman �tanche dans une bouteille d'eau
Partager cette actu
Publi�e                 par Audrey Oeillet le Mercredi 12 Fevrier 2014
Vendre un Walkman �tanche dans une bouteille, une id�e originale qu'� eu Sony en Nouvelle-Z�lande : l'occasion de refaire parler d'un produit commercialis� depuis d�j� un an.
Le NWZ-W273 n'est pas nouveau : ce Walkman �tanche est propos� � la vente chez Sony depuis d�j� pr�s d'un an. Mais ce n'est pas une raison pour la firme de laisser de c�t� ce mod�le destin� aux sportifs, et m�me aux nageurs, puisqu'il r�siste � une immersion jusqu'� 2 m�tres de profondeur.
Pour valoriser son produit et attirer le regard des curieux, Sony a donc entrepris de le vendre dans une bouteille d'eau. The Bottled Walkman, soit le Walkman en bouteille, est vendu dans des distributeurs automatiques propos�s dans certaines salles de sport d'Auckland, en Nouvelle-Z�lande.
Dans la vid�o qui met en avant cette d�marche commerciale, on peut ainsi constater que le Walkman en bouteille est propos� au milieu d'autres boissons �nerg�tiques. Si la question du prix n'est pas �voqu�e, il est �vident qu'il faut plus que quelques pi�ces de monnaie pour acqu�rir le lecteur de musique, commercialis� en France aux environs de 60 euros.
