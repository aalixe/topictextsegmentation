TITRE: Cancer du sein: une nouvelle étude relance la polémique sur les mammographies - LaRepubliquedesPyrenees.fr
DATE: 2014-02-12
URL: http://www.larepubliquedespyrenees.fr/2014/02/12/cancer-du-sein-une-nouvelle-etude-relance-la-polemique-sur-les-mammographies,1179164.php
PRINCIPAL: 167350
TEXT:
Par AFP
Publié le 12/02/2014 à 15h40
Une macrobiopsie par mammotome réalisée le 8 mars 2006 à l'Institut Curie à Paris (AFP/Archives - Joel Saget)
Une mammographie, dans un centre de radiologie de Bayeux le 15 mai 2001 (AFP/Archives - Mychele Daniau)
1 / 2
1 / 2
Une macrobiopsie par mammotome réalisée le 8 mars 2006 à l'Institut Curie à Paris (AFP/Archives - Joel Saget)
2 / 2
Une mammographie, dans un centre de radiologie de Bayeux le 15 mai 2001 (AFP/Archives - Mychele Daniau)
La pratique de mammographies annuelles ne permet pas de réduire la mortalité par cancer du sein, selon une étude canadienne qui relance la polémique autour de l'intérêt des campagnes de dépistage organisé.
Réalisée sur près de 90.000 femmes âgées de 40 à 59 ans, suivies pendant 25 ans, l'étude a montré que les femmes qui avaient subi des mammographies annuelles pendant cinq ans n'avaient pas moins de risque de mourir d'un cancer du sein que celles ayant seulement bénéficié d'un examen physique.
Au bout de 25 ans, 500 décès par cancer du sein étaient survenus chez les 44.925 femmes suivies par mammographies contre 505 décès chez les 44.910 femmes du groupe témoin.
Les femmes avaient été assignées dans les deux groupes par tirage au sort.
Les tumeurs du sein détectées étaient en revanche plus nombreuses dans le 1er groupe, soit 3.250 au total contre 3.133 dans le second à la fin de l'étude.
Le déséquilibre était déjà net au bout de cinq ans, avec 666 cancers détectés chez les femmes sous mammographies contre 524 dans le groupe témoin, soit un "excédent" de 142 tumeurs.
Cet "excédent" était encore de 106 tumeurs au bout de 15 ans, ce qui, selon les auteurs, "signifie que 22% des cancers diagnostiqués dans le premier groupe ont été surdiagnostiqués".
Les tumeurs étaient de surcroît plus petites (1,4 cm dans le premier groupe contre 2,1 cm dans le second) au moment du diagnostic.
Dépistage organisé et surdiagnostic
Le surdiagnostic fait référence à la détection de très petites tumeurs qui n'auraient pas eu d'impact du vivant de la personne concernée.
En se fondant sur des études montrant une baisse de la mortalité, de nombreux pays occidentaux ont mis en place des programmes de dépistage organisé du cancer du sein. En France par exemple, le programme s'adresse à toutes les femmes âgées de 50 à 74 ans qui sont invitées à faire des mammographies tous les deux ans, prises en charge à 100% par l'Assurance maladie.
Mais d'autres études sont plus contradictoires : selon une étude de la Collaboration Cochrane, publiée pour la première fois en 2000 et régulièrement réévaluée depuis, le taux de mortalité des femmes dépistées ne serait guère différent de celui des autres femmes.
Une étude britannique publiée en 2012 avait au contraire estimé que le dépistage organisé du cancer du sein sauvait des vies mais entraînait un surdiagnostic estimé à près de 20% des cancers dépistés.
S'exprimant en septembre dernier, le Dr Jérôme Viguier, le directeur du Pôle santé publique et soins de l'Institut national du cancer (INCa-France) avait pour sa part estimé que la controverse était "scientifiquement réglée". Il avait ajouté que selon les dernières études, les programmes de dépistage organisé avaient permis de réduire la mortalité par cancer du sein de 15 à 21% et d'éviter 150 à 300 décès pour 100.000 femmes participant de manière régulière au dépistage pendant 10 ans.
"Nos résultats rejoignent les vues de certains commentateurs qui estiment que les politiques de dépistage par  mammographies devraient être revues" dans les pays développés, estiment les auteurs de l'étude canadienne.
Mais selon eux, la chose ne sera pas facile "parce que les gouvernements, les organismes de financement de la recherche, les chercheurs et les médecins peuvent avoir intérêt à poursuivre des activités qui sont bien établies".
Source : AFP
