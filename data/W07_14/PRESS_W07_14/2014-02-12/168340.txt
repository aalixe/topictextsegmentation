TITRE: Les �lecteurs pourront voter blanc � partir des �lections europ�ennes  - France-Monde - La Voix du Nord
DATE: 2014-02-12
URL: http://www.lavoixdunord.fr/france-monde/les-electeurs-pourront-voter-blanc-a-partir-des-ia0b0n1915432
PRINCIPAL: 168337
TEXT:
- A +
Les �lecteurs pourront voter blanc d�s les �lections europ�ennes de mai, mais pas aux municipales de mars, le Parlement ayant adopt� mercredi une proposition de loi centriste le reconnaissant � partir du 1er avril.
En votant conforme un texte d�j� adopt� par les d�put�s, les s�nateurs ont ainsi fait un pas, mais un pas seulement, vers la prise en compte du vote blanc, v�ritable serpent de mer de la vie politique fran�aise puisque 30 textes parlementaires ont �t� d�pos�s en 20 ans sur le sujet. L'un d'eux avait �t� adopt� � l'Assembl�e nationale en 2003 avant de voir son parcours l�gislatif interrompu dans une navette au S�nat.
Chaque �lecteur pourra voter "blanc" soit en introduisant dans l'enveloppe un bulletin blanc, soit en laissant cette enveloppe vide. Jusqu'� pr�sent, bulletins blancs et bulletins nuls �taient m�lang�s lors du d�pouillement, et comptabilis�s ensemble. L�, ils seront compt�s s�par�ment, sans toutefois �tre comptabilis�s dans les suffrages exprim�s, ce qui enl�ve une grande port�e � ce vote.
En outre,  ce vote blanc ne sera reconnu ni aux �lections pr�sidentielles, ni aux referendums locaux parce qu'il faudrait une loi organique. "J'appelle ce texte de mes voeux avant la prochaine �ch�ance pr�sidentielle", a d�clar� le rapporteur, Fran�ois Zocchetto (UDI-UC).
"La reconnaissance du vote blanc est intimement li�e � la notion de d�mocratie repr�sentative", a plaid� Alain Vidalies, le ministre des relations avec le Parlement, en soutenant le texte d�pos� par des d�put�s centristes. Il a notamment rappel� que Fran�ois Hollande en avait fait la demande lors de ses voeux aux bureaux des assembl�es, le 21 janvier. "+Les �lecteurs assez sophistiqu�s pour voter blanc ne doivent pas �tre comptabilis�s en vrac avec les paresseux ou les imb�ciles+", a-t-il dit en citant le constitutionnaliste Guy Carcassonne, disparu il y a un an.
Interrogations sur l'utilit� du texte
Il a aussi regrett� que sa mise en oeuvre d�s les prochaines municipales "pose des probl�mes pratiques, d'ordre informatique notamment" et ne soit pas possible.
Cette adoption constitue "une avanc�e dans la transparence de la vie d�mocratique et r�pond aux attentes de nombreux Fran�ais depuis de nombreuses ann�es", s'est f�licit� apr�s le vote M. Zocchetto, pr�sident du groupe centriste, dans un communiqu� commun avec l'auteur de la proposition � l'Assembl�e, Fran�ois Sauvadet (UDI).
"L'absence de reconnaissance de la voix de l?�lecteur qui se d�place pour accomplir son devoir civique �tait choquante en d�mocratie", a-t-il soulign� � la tribune.
Contrairement � l'Assembl�e, le S�nat avait consid�r� en premi�re lecture qu'une enveloppe vide ne saurait �tre assimil�e � un bulletin blanc. Les d�put�s estimaient notamment que "mettre des bulletins blancs � la disposition des �lecteurs serait co�teux et inciterait � voter blanc". "La commission des lois s'est finalement ralli�e � la position simple et pragmatique de l'Assembl�e nationale", a indiqu� M. Zocchetto.
"Le vote blanc est un acte d�lib�r� et positif, contrairement le plus souvent au vote nul, qui proc�de d'une maladresse ou d'un rejet de l'offre politique", a argument� C�cile Cukierman (Communiste, r�publicain et citoyen, CRC), se r�jouissant que "le consensus semble pr�valoir".
"Le vote blanc n'est pas qu'une vague fantaisie mais un thermom�tre de la d�mocratie", a estim� Pierre Charon (UMP). "Nous devons entendre la d�tresse de nos concitoyens, pour mieux leur r�pondre".
"Mieux vaut voter blanc que bleu marine", a lanc� de son c�t� l'�cologiste H�l�ne Lipietz.
"Ce texte n'est pas mauvais mais on peut se demander s'il est utile", a relev� Fran�ois Fortassin (RDSE, � majorit� PRG).
Le vote blanc et nul avait attir� plus de deux millions d'�lecteurs aux �lections l�gislatives de 1993 et au r�f�rendum de 1972 sur l'�largissement de la communaut� europ�enne. Lors du second tour de la pr�sidentielle de 1995, aucun des deux candidats n'aurait eu la majorit� absolue si les bulletins blancs avaient �t� comptabilis�s. Jacques Chirac, qui l'avait emport� face � Lionel Jospin, n'aurait obtenu que 49,5% des suffrages.
R�agir � l'article
