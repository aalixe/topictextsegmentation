TITRE: Ces Françaises qui choisissent de vivre sans enfant - Elle
DATE: 2014-02-12
URL: http://www.elle.fr/Societe/News/Ces-Francaises-qui-choisissent-de-vivre-sans-enfant-2667266
PRINCIPAL: 166870
TEXT:
Ces Françaises qui choisissent de vivre sans enfant
Créé le 12/02/2014 à 12h41
© Getty
Hélène Guinhut @heleneguinhut
[ Tous ses articles ]�
Dans une société où la famille est régulièrement au cœur des débats, faire le choix de vivre sans enfant reste marginal. Dans une étude publiée ce mercredi et réalisée en 2010 auprès de 5 275 femmes et 3 373 hommes, l’Ined (Institut national d’étude démographique) se penche sur ce phénomène d’« infécondité volontaire ».
En France, seulement 6,3% des hommes et 4,3% des femmes déclarent ne pas avoir d’enfant et ne pas en vouloir. « Il est sans doute moins stigmatisant pour les hommes d’assumer ce choix que pour les femmes, étant donné les rôles encore assignés à chacun des sexes », expliquent Charlotte Debest et Magali Mazuy, les deux auteures de l'étude. Elles constatent qu’« une pression, diffuse, s'exerce sur les couples », pour avoir un enfant. En conséquence, seulement 3% des femmes en couple et 5% des hommes en couple revendiquent le fait de vivre sans enfant.
Trop libres et trop âgés pour devenir parents
Les chercheuses se penchent sur les motivations de ces personnes sans enfant. « Huit fois sur dix, femmes (79%) et hommes (83%), déclarent être bien sans enfant », observe l’étude. Le sentiment de liberté et le fait d’avoir d’autres priorités, sont souvent évoqués. Chez les femmes, une autre raison ressort : l’âge. « Le fait de se considérer trop âgé est un argument très souvent mis en avant après 40 ans, notamment pour les femmes, qui sont, plus que les hommes, confrontées à la pression biologique », détaillent les auteures. En allant à contre-courant des normes, ceux qui sont surnommés les « no-kids » affirment « un choix de vie positif et épanouissant », conclue l’Ined.
