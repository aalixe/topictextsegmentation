TITRE: Hollande dans la Silicon Valley pour une offensive de charme - 13 février 2014 - Le Nouvel Observateur
DATE: 2014-02-12
URL: http://tempsreel.nouvelobs.com/societe/20140212.AFP9899/hollande-dans-la-silicon-valley-pour-une-offensive-de-charme.html
PRINCIPAL: 0
TEXT:
Actualité > Société > Hollande dans la Silicon Valley pour une offensive de charme
Hollande dans la Silicon Valley pour une offensive de charme
Publié le 12-02-2014 à 13h10
Mis à jour le 13-02-2014 à 06h40
A+ A-
Le président français François Hollande achève mercredi sa visite aux Etats-Unis par une escale de quelques heures à San Francisco où il entend mener une offensive de charme au profit des start-up françaises et auprès des géants américains de l'internet, tout en évitant les sujets qui fâchent. (c) Afp
Washington (AFP) - Le président français François Hollande achève mercredi sa visite aux Etats-Unis par une escale de quelques heures à San Francisco où il entend mener une offensive de charme au profit des start-up françaises et auprès des géants américains de l'internet, tout en évitant les sujets qui fâchent.
Après deux jours d'une visite d'Etat à Washington dominée par les enjeux diplomatiques et économiques, il sera le premier chef d'Etat français en exercice à fouler le sol de la Californie depuis François Mitterrand, il y a trente ans.
François Hollande doit y rencontrer les tycoons de l'internet, Eric Schmidt (Google), Sheryl Sandberg (Facebook), Jack Dorsey (Twitter), Mitchell Baker (Mozilla Foundation) et Tony Fadell. Ce dernier est emblématique des "success stories" de la Silicon Valley. Concepteur de l'iPod chez Apple, il vient de céder sa start-up d'objets connectés, Nest, pour 3,2 milliards de dollars à Google.
Mardi, lors d'une conférence de presse commune avec son homologue français, le président américain Barack Obama a annoncé le "lancement d'un nouveau dialogue économique pour développer le commerce, accroître la compétitivité de nos entreprises, stimuler l'innovation et encourager les nouveaux entrepreneurs".
"La visite du président Hollande dans la Silicon Valley souligne notre engagement en faveur de nouvelles collaborations scientifiques et technologiques", a-t-il enchaîné.
Jeunes pousses françaises
Selon le président Obama, François Hollande a "entrepris des réformes structurelles difficiles qui (...) vont aider (la France) à être plus compétitive à l'avenir". Ce sont ces réformes que le président français entend vanter mercredi face aux investisseurs américains tout comme les jeunes pousses de l'économie française réunies au sein d'un French Tech Hub.
"La France est l'un des pays au monde qui reçoit le plus d'investissements venant de l'étranger, l'un des pays au monde les plus ouverts aux capitaux extérieurs", a fait valoir mardi le président français, chantre de "l'attractivité" de son pays.
Les relations de la France avec les géants américains de l'internet sont cependant parfois houleuses, qu'il s'agisse de la protection des données personnelles ou de leurs pratiques d'"optimisation fiscale". Ces pratiques ne sont "pas acceptables", a souligné François Hollande avant de s'envoler pour les Etats-Unis, assurant être "d'accord" avec Barack Obama pour produire un "effort d'harmonisation fiscale".
Selon des informations de presse, Google, dont il retrouvera le patron à déjeuner mercredi, se serait vu infliger un redressement d'un milliard d'euros par le fisc français. Yahoo vient pour sa part d'annoncer que ses services en Europe, Afrique et Moyen-Orient seraient désormais gérés par une seule entité basée en Irlande tout en soutenant que "cela ne change rien" pour le groupe en matière d'impôts.
Réflexes protectionnistes
Vu des Etats-Unis en revanche, les freins posés par le gouvernement français au développement en France de Dailymotion, Netflix et Uber témoignent de réflexes protectionnistes incompatibles avec un esprit d'innovation.
François Hollande, dont la délégation compte plusieurs patrons de start-up françaises, n'entend pas aller à la rencontre des géants de l'internet en "inspecteur des impôts", a rétorqué la ministre française déléguée à l'économie numérique, Fleur Pellerin, qui l'accompagne dans son périple américain.
En deux jours, lundi et mardi, Barack Obama et François Hollande auront passé près d'une dizaine d'heures ensemble, à Washington ainsi que sur le domaine de Monticello, bâti par le troisième président américain, Thomas Jefferson, affichant leur identité de vues mardi sur les grandes questions géopolitiques et économiques du moment.
Se donnant du "François" et du "Barack", ils ont souligné le chemin parcouru depuis le refus français d'une intervention en Irak en 2003 qui avait ouvert une ère de glaciation dans les relations entre les deux pays.
La coopération franco-américaine actuelle aurait été "inimaginable il y a seulement dix ans", a relevé le président Obama tandis que son homologue français a salué lors du somptueux dîner d'Etat offert en son honneur à la Maison Blanche mardi soir une relation désormais parvenue à "un degré exceptionnel de proximité et de confiance".
Partager
