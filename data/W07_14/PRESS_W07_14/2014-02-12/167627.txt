TITRE: Dieudonné condamné à modifier une vidéo YouTube
DATE: 2014-02-12
URL: http://www.numerama.com/magazine/28395-dieudonne-condamne-a-modifier-une-video-youtube.html
PRINCIPAL: 167618
TEXT:
Ces documents sont int�ressants. Dans celui du 10/05/90, le passage que retient Faurisson est :
"Au lendemain du 17 avril, la commission des
lois se met au travail. Le rapporteur du projet,
Fran�ois Asensi (PCF), et le pr�sident de la
commission, Michel Sapin (PS), d�couvrent que
la proposition Gayssot est une coquille vide.
� C'�tait un tract �, explique Sapin. En huit jours,
il faut b�tir un texte qui se tienne."
puis :
inclut aussi dans le texte une proposition de loi PS
contre le r�visionnisme."
Le texte du Nouvel Obs pr�cise aussi que Asensi et Sapin incorporent dans le projet de loi la proposition d'Areski Dahmani de priver de droits civiques les auteurs de d�lits racistes (!)
Un autre point marrant est la droite qui refuse de voter au pr�texte que le texte vient des communistes
Le monde n'a pas chang�
Ce que pr�tend Faurisson ( http://robertfauriss...e-courouve.html ) c'est que la coquille vide de 1990 a �t� remplie avec la proposition de loi Sarre du 2 avril 1988 ( http://www.assemblee...iers/881247.asp ), qui elle m�me provenait de Fabius qui aurait d�clar� avoir "personnellement d�pos� une loi contre les n�gateurs".
Dans l'autre Nouvel Obs, celui du 17/05, il n'y a en fait rien de bien int�ressant concernant notre sujet, cela parle surtout d'extr�misme juif, j'en conseille toutefois la lecture � ceux qui sont int�ress�s par l'histoire de ce ph�nom�ne. Je cite juste une petite perle montrant qu'en 1990 on ne parlait encore beaucoup de sionisme :
"22 ans, n� au Maroc, Richard est sioniste, c'est-�-dire qu'il compte partir s'installer en Isra�l d�s la fin de ses �tudes de m�decine."
J'ai aussi lu ceci, page 114 :
"Sous l�impulsion de Laurent Fabius, alors pr�sident de l�Assembl�e Nationale, un texte est finalement pr�sent� par le d�put� communiste, Jean-Claude Gayssot."
J'ai lu quelques d�bats parlementaires, en fait tout est en ligne c'est assez balaise :
http://archives.asse...-ordinaire2.asp
Mais cela demande pas mal de lecture pour trouver quelque chose. J'ai surtout vu pour le moment que la seule opposition v�h�mente au texte vient de Marie-France Stirbois (FN). L'opposition semble aussi s'�tonner de cet alliance de circonstance entre PS et PC. Rien de suffisamment majeur ou synth�tique pour que je te donne les r�f�rences, mais il me reste beaucoup � lire.
Par contre sur le r�le de Fabius en tant que pr�sident de l'AN, on a un an plus tard Toubon qui dit :
"[La loi Gayssot] constitue en r�alit� une loi de circonstance, et je le regrette
beaucoup. Une ann�e s' est �coul�e . Nous ne sommes pas �
un mois des �v�nements de Carpentras. Nous n'avons pas �
examiner un texte que la conf�rence des pr�sidents avait, je
le rappelle, inscrit � l'ordre du jour en toute h�te, quarante huit
heures apr�s son d�p�t, et qui avait �t� discut� imm�diatement
parce que le pr�sident de l 'Assembl�e, M . Fabius,
avait d�cid� personnellement son inscription ."
http://archives.asse...inaire2/101.pdf (page 27 du PDF, 3571 du JO)
Cela me semble assez clair sur ce point.
Sur la campagne m�diatique, j'ai au moins une source (mais je n'ai pas le doc) : Le Monde, 26/27 mars 1989, p. 18, qui apparemment montrerait Fabius et Chirac d�fendant une loi m�morielle.
Un truc amusant que j'ai d�couvert en passant, c'est que cette loi est pass�e dans la foul�e de la profanation du cimeti�re de Carpentras, et � lire la page Wikipedia d�di�e � cet �v�nement, j'ai cru r�ver :
https://fr.wikipedia...f_de_Carpentras
"34 s�pultures juives sont profan�es�: st�les renvers�es et bris�es, sans inscriptions antis�mites "
"Le corps, extrait du cercueil, est pos� nu face contre terre sur une tombe voisine. Un mat de parasol est retrouv� � c�t� de lui�: on parle d'un ��simulacre d'empalement��."
En fait il ne semble y avoir aucune preuve que l'acte �tait antis�mite, et cette histoire d'empalement (utilis�e par Fabius lors des d�bats) flaire bon la quenelle salut nazi symbole de sodomisation des victimes de la Shoa !
Jugement tout � fait personnel bien s�r.
r�pondre
