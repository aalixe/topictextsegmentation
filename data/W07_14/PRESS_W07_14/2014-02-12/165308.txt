TITRE: Le nuage du d�faut de paiement s'�loigne de Washington | Ivan Couronne | �tats-Unis
DATE: 2014-02-12
URL: http://www.lapresse.ca/international/etats-unis/201402/11/01-4737884-la-chambre-approuve-le-relevement-du-plafond-de-la-dette.php
PRINCIPAL: 165306
TEXT:
>�Le nuage du d�faut de paiement s'�loigne de Washington�
Le nuage du d�faut de paiement s'�loigne de Washington
Les chefs de file r�publicains ont jur� ne plus tenter l'impossible, et de se distancer de la faction intransigeante des affili�s au Tea Party.
Photo: AFP
Agence France-Presse
Washington
Sauf coup de th��tre, les �tats-Unis ne feront pas d�faut cette ann�e: les adversaires r�publicains de Barack Obama, dans une capitulation surprise � la Chambre, ont laiss� voter un texte autorisant les �tats-Unis � emprunter jusqu'au 15 mars 2015, sans conditions.
Le vote marque la fin de trois ann�es de confrontation et de blocage au Capitole, et a �t� d�ment c�l�br� par le camp de Barack Obama.
�Le vote de ce soir est une �tape positive pour s'�carter de la politique de la corde raide, qui freine inutilement notre �conomie�, s'est f�licit� le porte-parole de la Maison-Blanche, Jay Carney, en appelant le Congr�s � coop�rer sur d'autres sujets comme le salaire minimum. �Le pr�sident a h�te de travailler avec les deux partis�.
La mesure, d�pos�e par les dirigeants r�publicains contre l'avis de leurs troupes, a �t� approuv�e mardi gr�ce � l'appui quasi-unanime des d�mocrates de la Chambre, seulement 28 des 232 r�publicains votant �oui�. Le S�nat, dont l'aval est indispensable, devrait commencer l'examen du texte mercredi, a indiqu� le chef de la majorit� d�mocrate Harry Reid.
Difficile de comprendre le revirement des dirigeants r�publicains, mardi, sans revenir au mois d'octobre dernier. � l'issue de plusieurs semaines de bras de fer, ils avaient �chou� � extraire des concessions budg�taires de la part des d�mocrates en �change du rel�vement n�cessaire du plafond de la dette.
Les chefs de file avaient alors jur� de ne plus tenter l'impossible, et de se distancer de la faction intransigeante des affili�s au Tea Party. La premi�re �tape de ce nouvel esprit de coop�ration fut l'adoption � une large majorit� des budgets 2014 et 2015, en d�cembre.
La limite l�gale de la dette, 17 211 milliards de dollars, a �t� r�activ�e vendredi dernier, et le Tr�sor a pr�venu qu'il avait besoin d'une nouvelle autorisation du Congr�s pour pouvoir emprunter au-del� du 27 f�vrier, de fa�on � ce que les �tats-Unis paient leurs obligations: pensions de retraites, d�penses courantes mais aussi les int�r�ts pay�s aux cr�anciers internationaux.
Ne pas relever la limite de la dette �serait catastrophique�, avait averti mardi la nouvelle pr�sidente de la Banque centrale am�ricaine, Janet Yellen. Risquer un d�faut �serait extr�mement destructif du point de vue de notre �conomie, de nos march�s financiers, des march�s financiers mondiaux�, avait-elle dit.
Les d�mocrates crient victoire
�Les r�publicains sont les seuls qui reconnaissent l'existence d'une crise de la dette et ont essay� � plusieurs reprises de renverser la dangereuse tendance de d�penses � Washington�, a toutefois d�clar� Eric Cantor, le num�ro deux r�publicain de la Chambre.
Le vote illustre les divisions internes dans le camp r�publicain, entre les mod�r�s et les plus conservateurs, qui exigeaient un accord de baisse des d�penses en �change du rel�vement du plafond, une tactique qui avait fonctionn� � l'�t� 2011 mais pas en octobre dernier.
John Boehner, pr�sident de la Chambre, avait depuis publiquement d�clar� qu'il ne provoquerait pas de crise de la dette am�ricaine, et il a vot� �oui� mardi.
�Ce n'est pas marrant pour lui, c'est le moins qu'on puisse dire�, a admis le r�publicain James Lankford.
Les d�mocrates ont quant � eux cri� victoire: depuis des mois, ils exigeaient un rel�vement sans condition de la limite de la dette.
�J'esp�re que la tactique consistant � menacer d'un d�faut pour des questions budg�taires est termin�e, ne sera plus envisag�e ni employ�e�, a affirm� Gene Sperling, le principal conseiller �conomique de Barack Obama.
L'adoption par le S�nat d�pendra de la coop�ration de la minorit� r�publicaine. Certains s�nateurs pourraient tenter de retarder l'examen du texte.
�La plupart des r�publicains partout dans le pays pensent que nous ne devrions pas �crire de ch�que en blanc au pr�sident, et que nous ne pouvons pas continuer � emprunter de l'argent sans contrainte sur les d�penses�, a dit le s�nateur r�publicain Rand Paul.
Partager
