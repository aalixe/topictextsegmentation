TITRE: Technologie : les ventes de produits "high tech" ont recul� de 2% en France en 2013
DATE: 2014-02-12
URL: http://www.boursier.com/actualites/economie/les-ventes-de-produits-high-tech-en-baisse-de-2-en-france-en-2013-22999.html
PRINCIPAL: 0
TEXT:
Technologie : les ventes de produits "high tech" ont recul� de 2% en France en 2013
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � L'engouement des Fran�ais pour les produits "high tech" perd de son �lan... D'apr�s l'institut d'�tudes GfK, qui publie mercredi son bilan pour l'ann�e 2013, les d�penses en la mati�re ont recul� de 2%, pour atteindre�15,4 milliards d'euros. Les smartphones et les tablettes sont sans surprise les deux vainqueurs de l'ann�e derni�re, avec 23,6 millions d'unit�s vendues - les smartphones comptant pour 15,8 millions d'unit�s. La 4G, le ph�nom�ne des "phablettes", ces t�l�phones dot�s de grands �crans, et la sortie de nombreux mod�les de smartphones sous les 150 euros expliquent cette bonne sant�...
La TV en baisse
Les ventes informatiques se sont �galement bien comport�es, avec un nouveau record � pr�s de 11 millions de mat�riels vendus en 2013. "L'ann�e aura �t� marqu�e par le formidable essor des tablettes qui ont s�duit pr�s de 6,2 millions de consommateurs. La facilit� d'utilisation, la largeur d'offre et des prix en chute libre (240 euros, soit- 26%) ont incit� les Fran�ais � s'�quiper en masse cette ann�e", note GfK. Les notebook, les webcams PC et les souris ont dop� ce r�sultat. "2014 sera en progression en volume pour les ventes de mat�riel informatique", �crit l'institut, avec 7,5 millions d'unit�s attendues.
TV en berne
D'autres segments s'en sortent nettement moins bien... C'est le cas de la t�l�vision qui accuse un repli de un million de pi�ces pour s'�tablir � 5,7 millions d'�crans vendus sur 2013. "La copie 2013, un peu morose de prime abord, masque une inflexion de cette tendance baissi�re sur le dernier trimestre qui laisse esp�rer une stabilisation des ventes d�s 2014", souligne Michael Mathieu, Directeur Image et T�l�com chez GfK Consumer Choices France.
Chute de la photo
Le mat�riel audio enregistre pour sa part une contraction de 6% de ses ventes. "C'est pourtant l'effervescence qui caract�rise actuellement ce secteur puisque l'�volution des modes d'�coute musicale par le grand public bouleverse profond�ment l'offre de mat�riels", note encore GfK. La connectivit� sans-fil notamment est tr�s pris�e des consommateurs, avec 1,4 million d'appareils vendus en 2013, soit le double de la performance de 2012. "Elle p�se d�j� pour plus du tiers du chiffre d'affaires de la hifi et audio-vid�o r�unis et sera vraisemblablement majoritaire d'ici 2015", �crivent les auteurs de cette �tude.
Photo flout�e
Autre produit d�laiss�, l'appareil photo,�avec un recul de 18% des volumes pour un march� � 3,7 millions d'unit�s en 2013. La concurrence des smartphones fait donc des ravages... "La photo tente (...) de r�pondre en misant sur la qualit� de ses produits (capteur, basse lumi�re, High zoom), l� o� le Smartphone peine encore... Mais c'est surtout en se connectant que le compact tente de conserver une taille de march� acceptable. 20% des volumes et d�j� 30% du chiffre d'affaires 2013 de ce segment, le smart cam�ra s'est impos� dans les lin�aires cette ann�e. Il faut maintenant confirmer !", analyse GfK.
Claire Lemaitre � �2014, Boursier.com
