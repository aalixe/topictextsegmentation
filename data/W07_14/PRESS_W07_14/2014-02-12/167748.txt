TITRE: Poker en ligne : 9 crit�res de jeu probl�matique | PsychoM�dia
DATE: 2014-02-12
URL: http://www.psychomedia.qc.ca/psychologie/2014-02-12/poker-en-ligne-risque-de-jeu-problematique
PRINCIPAL: 167662
TEXT:
Soci�t�
Les joueurs de poker en ligne sont plus � risque de jeu probl�matique que les autres joueurs en ligne, selon une �tude de l'Observatoire fran�ais des drogues et des toxicomanies (OFDT) et de l'Observatoire des jeux (ODJ) publi�e sur le site de l'ODJ.
Le poker occupe la deuxi�me place des jeux en ligne derri�re les jeux de tirage / grattage ; 19,2% des joueurs sur Internet ont jou� au poker au cours des 12 derniers mois. Parmi ces derniers, 43% ne jouent qu�au poker.
Les joueurs de poker sont 20,9 % � jouer de mani�re quasi quotidienne contre 10,9 % pour les autres joueurs en ligne (jeux de tirage et de grattage, paris sportifs et hippiques) et ils d�pensent davantage : 778 euros par an en moyenne contre 627 euros.
Ils obtiennent des scores plus �lev�s � 9 crit�res de jeu probl�matique :
Miser plus d'argent que pr�vu : 17,4% contre 14,7%
Miser plus d'argent pour avoir la m�me excitation : 20,1% contre 14,7%
Rejouer pour r�cup�rer ses mises : 42,7% contre 26,8%
Vendre ou emprunter pour jouer : 7,9% contre 6,3%
Percevoir un probl�me de jeu : 16,2% contre 13,2%
Jeu, cause de stress : 10,2% contre 9,6%
Pratiques de jeu critiqu�es par l'entourage : 18,9% contre 12,1%
Difficult�s financi�res suite au jeu : 10,2% contre 7,8%
Sentiment de culpabilit� : 20,2% contre 15,3%
66,4% des joueurs de poker en ligne sont des hommes (56,4% pour les autres jeux). Ils sont un peu plus nombreux, soit 58,5% contre 52,5% � avoir un niveau d'�ducation �lev� (dipl�me sup�rieur au bac).
Voyez �galement:
