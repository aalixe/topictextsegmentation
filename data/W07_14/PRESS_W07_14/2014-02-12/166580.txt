TITRE: R�forme des rythmes scolaires: Les trois raisons de la d�ception des profs - 20minutes.fr
DATE: 2014-02-12
URL: http://www.20minutes.fr/societe/1296974-20140212-reforme-rythmes-scolaires-trois-raisons-deception-profs
PRINCIPAL: 0
TEXT:
Twitter
La r�forme permet de d�gager du temps pour les activit�s p�riscolaires. M.LIBERT/20MINUTES
EDUCATION - La mise en �uvre de la r�forme des rythmes scolaires leur pose probl�me, comme le r�v�le une �tude du SNUipp d�voil�e ce mercredi�
Une impression de g�chis . Voil� le sentiment qui pr�domine lorsqu�on interroge les enseignants sur la mise en �uvre de la r�forme des rythmes scolaires dans les 20% de communes qui ont fait le choix de l�appliquer d�s 2013. En t�moigne une enqu�te* men�e par le SNUipp-FSU , premier syndicat des professeurs du premier degr�, d�voil�e ce mercredi. 20 Minutes revient sur les trois principaux points de crispation des profs � l��gard des nouveaux rythmes.
1.��� Les conditions d�apprentissage des �l�ves ne se sont pas am�lior�es
La r�forme des rythmes scolaires a �t� pr�sent�e par Vincent Peillon comme un levier important pour am�liorer la r�ussite des �l�ves .�Or, seulement 22% des enseignants estiment que les conditions d�apprentissage des �l�ves se sont am�lior�es avec les nouveaux rythmes. Et alors que le minist�re pensait que la r�forme allait permettre un meilleur �quilibre de la semaine scolaire, seulement 25% des enseignants consid�rent que l�organisation de tous les domaines d�enseignement dans la journ�e et la semaine des �l�ves est plus satisfaisante.
Quand les activit�s p�riscolaires sont pr�vues sur la pause m�ridienne, les enseignants estiment qu�elles engendrent de l�excitation et qu�il est tr�s difficile ensuite de mener des activit�s scolaires l�apr�s-midi. Et lorsque les activit�s p�riscolaires sont programm�es l�apr�s-midi, elles entra�nent des contraintes d�emploi du temps plus nombreuses, car les activit�s longues (natation, sorties culturelles�) doivent �tre programm�es sur les matin�es.
2.��� Les conditions de travail des enseignants se sont d�grad�es
75% des profs interrog�s estiment que leurs conditions de travail se sont d�grad�es . Les profs pointent notamment le d�sagr�ment que leur cause le partage des salles de classes avec les animateurs, les cr�neaux horaires des activit�s p�dagogiques compl�mentaires qui leur sont impos�es par certaines organisations municipales� Ils �voquent aussi le surco�t en frais de garde d�enfant engendr� par la demi-journ�e de classe suppl�mentaire et l�augmentation de leur temps de trajet. Certains expliquent aussi que les r�unions d�animation p�dagogiques entre enseignants sont d�sormais plac�es le mercredi apr�s-midi, au d�triment de leur vie personnelle.
3.��� Le sentiment d��tre laiss�s pour compte
Pour beaucoup d�enseignants, la mise en �uvre de la r�forme s�est faite sans eux. 60% d�entre eux estiment en effet que l�avis du conseil d��cole n�a pas �t� suivi dans l��laboration des projets de nouveaux rythmes scolaires par les communes. Or, ils auraient appr�ci� �tre consult�s sur les transitions scolaires et p�riscolaires, sur l�utilisation des salles de classes, sur l�organisation des nouveaux rythmes en maternelles� Ce qui aurait permis selon eux d��viter des tensions et d��laborer des projets suscitant davantage d�adh�sion.
*Enqu�te men�e en ligne du 20 janvier au 10 f�vrier 2014 aupr�s de 3.568 enseignants des �coles dont l��tablissement a appliqu� les nouveaux rythmes d�s 2013.
Delphine Bancaud
