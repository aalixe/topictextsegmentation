TITRE: Neknomination : Un dangereux jeu alcoolis� se r�pand sur Facebook. Info - Saint-Malo.maville.com
DATE: 2014-02-12
URL: http://www.saint-malo.maville.com/actu/actudet_-neknomination-un-dangereux-jeu-alcoolise-se-repand-sur-facebook_fil-2489243_actu.Htm
PRINCIPAL: 165990
TEXT:
Twitter
Google +
Une vague de "Neknominations" d�ferle sur Facebook. Un jeu qui encourage la consommation d'alcool et peut se r�v�ler dangereux.� Photo AFP.
Lanc�e en Australie d�but janvier, cette pratique � risque se r�pand rapidement en Europe. Elle pourrait avoir entra�n� la mort de 4 personnes.
Vous avez peut-�tre vu passer une ou plusieurs de ces vid�os sur votre fil Facebook, ces derniers jours. Le ph�nom�ne, qui encourage la consommation abusive d'alcool, d�ferle d�sormais en France, sur le r�seau social.
Boire en cha�ne
Le jeu, nomm� Neknomination (neck your drink signifie boire son verre cul sec, en anglais), consiste � se filmer, face cam�ra, en buvant un verre d�alcool d�une traite, puis � nommer trois personnes afin qu�elles fassent de m�me dans les prochaines 24 heures� �
La dangereuse cha�ne, lanc�e au d�but de l�ann�e en Australie , prend aujourd�hui une bien plus large ampleur.
Quatre d�c�s auraient �t� entra�n�s par cette pratique
Outre-Manche, elle aurait entra�n� la mort de deux personnes, de 20 et 29 ans, ce week-end � Londres et Cardiff, selon les m�dias anglais .
En Irlande aussi, deux hommes seraient d�c�d�s apr�s y avoir pris part la semaine derni�re, rapporte l�Irish Times . Le premier, un DJ dublinois, a �t� retrouv� mort, chez lui, alors que le second a �t� rep�ch� dans une rivi�re du comt� Carlow , o� il s�est jet� apr�s avoir ingurgit� une pinte d�une traite. Plusieurs enqu�tes sont en cours.
En Irlande du Nord, des investigations ont �galement �t� lanc�es � l�encontre d�un juge , qui a particip� en famille � ce jeu explosif.
"J'esp�re que ces �v�nements vont y mettre fin"
La pratique a vivement �t� condamn�e par une association de pr�vention majeure au pays au tr�fle, Alcohol Action Ireland . "Alors que certains voient cela comme un jeu, boire de grandes quantit�s d�alcool sur une courte dur�e peut avoir des cons�quences r�elles sur la sant�", a rappel� l'organisation dans un communiqu�.
"J'esp�re que ces �v�nements vont y mettre fin", a ajout� la responsable de l'association, Suzanne Costello dans les colonnes de l�Irish Times .
Une demande d'interdiction de ces vid�os sur Facebook
Les autorit�s irlandaises n�ont pas attendu avant de r�agir. Le ministre charg� de la communication a d�abord appel� � une interdiction de l�ensemble des vid�os Neknominations afin de mettre fin � cette mode. Pat Rabbitte a �t� suivi par de nombreux internautes.�
Mais Facebook a rejet� son appel . "Nous ne tol�rons pas les contenus directement n�fastes, comme ceux qui sont intimidants, mais un comportement controvers� ou offensant ne va pas n�cessairement � l�encontre de notre r�glement", s�est justifi� un porte-parole du r�seau au journal The Independant .�
Apr�s un contact avec l�entreprise, la ministre de l�enfance, Frances Fitzgerald a quant � elle pr�cis� que Facebook supprimera n�anmoins toute vid�o au comportement abusif. "Cela reste potentiellement mortel", a-t-elle d�clar�, insistant sur la pr�vention pour y faire face.
La riposte s'organise aussi sur les r�seaux sociaux
En attendant, sur les r�seaux sociaux, la riposte s�organise. Les proches de certaines victimes irlandaises et anglaises ont demand� le boycott de cette pratique.
Une page Facebook, � Ban Neknomination � ("pour l'interdiction des Neknominations") a �galement vu le jour. Elle rassemble actuellement pr�s de 25 000 personnes. �Pas de quoi emp�cher, pour le moment, cette dangereuse pratique de se r�pandre sur le net.
Ouest-France��
