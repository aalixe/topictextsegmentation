TITRE: Sotchi: un militant �colo condamn� � 3 ans de camp - BFMTV.com
DATE: 2014-02-12
URL: http://www.bfmtv.com/international/sotchi-un-militant-ecolo-condamne-a-3-ans-camp-708978.html
PRINCIPAL: 0
TEXT:
La justice russe a confirm� mercredi en appel la condamnation � trois ans de camp d'un militant �cologiste qui d�non�ait l'impact sur l'environnement des travaux de pr�paration des jeux Olympiques d'hiver de Sotchi, qui battent actuellement leur plein.
�� �
Le tribunal r�gional de Krasnodar a rejet� l'appel d'Evgueni Vitichko, g�ologue et membre d'une association r�gionale de d�fense de l'environnement du Caucase du Nord (EWNC).
�� �
Le militant avait �t� condamn� en 2012, avec un autre de ses compagnons de lutte, Suren Gazarian, � une peine de trois ans avec sursis pour avoir perc� une ouverture dans une cl�ture install�e dans une zone prot�g�e.
De la peinture sur une cl�ture
Sur la cl�ture, les militants �cologistes avaient aussi inscrit � la bombe de peinture "C'est  notre for�t" et "Sania est un voleur", utilisant le diminutif du  pr�nom du gouverneur. Suren Gazarian a fui la Russie et r�side d�sormais en Estonie.
�� �
Evgueni Vitichko, lui, a comparu mercredi par liaison vid�o � partir de la prison  de Touapse, une ville voisine de Sotchi o� il est maintenu en d�tention.  Une quinzaine de militants �taient dans la salle du  tribunal de Krasnodar, pour lui apporter leur soutien.
"Je n'ai rien fait d'ill�gal",  a-t-il d�clar�. Il a admis ne pas   s'�tre pr�sent�, par inadvertance, � deux reprises - sur six au total -   aux autorit�s p�nitentiaires, mais a  rejet� l'accusation d'avoir   syst�matiquement viol� les r�gles qui lui �taient impos�es.
Six militants plac�s en d�tention
"L'affaire  Vitichko est depuis le d�part fond�e  sur des mobiles politiques", a  d�nonc� Ioulia Gorbounova, de l'ONG Human  Rights Watch, cit�e dans un  communiqu�.
"Quand les  autorit�s ont continu� � le  harceler, il est devenu clair qu'elles  essayaient de r�duire au silence  et de punir certaines personnes  critiquant avec persistance les  pr�paratifs des Jeux olympiques",  a-t-elle ajout�.
Au moins six militants d'EWNC ont �t� plac�s en d�tention, jusqu'� quinze jours pour certains, depuis novembre.
A lire aussi
