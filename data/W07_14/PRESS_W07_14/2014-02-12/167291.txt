TITRE: How I Met Your Dad : le spin-off de How I Met Your Mother a trouv� son actrice principale
DATE: 2014-02-12
URL: http://www.cinemovies.fr/actu/how-i-met-your-dad-le-spin-off-de-how-i-met-your-mother-a-trouve-son-actrice-principale/26546
PRINCIPAL: 167290
TEXT:
� S�ries
14H01 Le 12/02/14 S�ries 0 publi� par Nicolas Germain
How I Met Your Dad : le spin-off de How I Met Your Mother a trouv� son actrice principale
A l'heure o� le dernier �pisode de How I Met Your Mother s'appr�te � �tre tourn�, son spin-off How I Met Your Dad fait un grand pas en avant avec l'annonce de l'arriv�e de Greta Gerwig dans le r�le principal.
Greta Gerwig d�croche le r�le principal de How I Met Your Dad, le spin-off de How I Met Your Mother � DR
Tweet
Retrouvez aussi l'actu cin�ma sur notre page Facebook
Dans un mois et demi, How I Met Your Mother prendra fin apr�s neuf saisons. Mais les cr�ateurs Carter Bays et Craig Thomas travaillent activement au spin-off How I Met Your Dad, qui aura cette fois un personnage principal f�minin.
C'est l'actrice Greta Gerwig qui vient de d�crocher le r�le principal, celui de Sally, une femme immature qui ne sait pas quoi faire de sa vie et qui r�alise qu'elle n'a rien en commun avec son mari �pous� il y a moins d'un an. Alors que la s�paration est in�vitable, Sally se tourne vers ses amis et sa famille.
C'est la premi�re fois que Greta Gerwig participe � une sitcom et a d'ailleurs fait tr�s peu d'apparition � la t�l�vision. On la conna�t surtout pour ses r�les dans des films ind�pendants tels que Baghead et Frances Ha , et d'autres plus grands publics comme Sex Friends et To Rome With Love .
Dans How I Met Your Dad, Greta Gerwig agira �galement comme productrice et participera � l'�criture aux c�t�s de Carter Bays, Craig Thomas et Emily Spivey. Rappelons que les personnages du spin-off seront introduits dans le final d'une heure de How I Met Your Mother qui sera diffus� le 31 mars sur CBS.
Plus sur
How I Met Your Dad
0 article
