TITRE: JO de Sotchi : grosse d�ception pour Jason Lamy-Chappuis en combin� nordique
DATE: 2014-02-12
URL: http://www.francetvinfo.fr/sports/jo/jo-de-sotchi/jo-combine-nordique-grosse-deception-pour-le-porte-drapeau-francais-jason-lamy-chappuis-35e-en-petit-tremplin_528181.html
PRINCIPAL: 167303
TEXT:
JO de Sotchi : grosse d�ception pour Jason Lamy-Chappuis en combin� nordique
Le porte-drapeau fran�ais, champion olympique en titre,�a termin� 35e du petit tremplin.
Le Fran�ais Jason Lamy Chappuis � la r�ception de son saut du combin� nordique (petit tremplin), le 12 f�vrier 2014, � Krasnaya Polyana (Russie). (  MAXPPP)
Par Francetv info avec AFP
Mis � jour le
, publi� le
12/02/2014 | 13:58
Il n'a pas r�ussi � conserver son titre olympique.� Jason Lamy-Chappuis �a �chou� � la 35e place du combin� nordique (petit tremplin) des� JO de Sotchi , mercredi 12 f�vrier. Une grosse d�ception, quatre ans apr�s avoir remport� la m�daille d'or aux Jeux de Vancouver (Canada).
Le porte-drapeau de la d�l�gation fran�aise a termin�� plus de 2 min 37 sec du vainqueur, son adversaire �ternel, l'Allemand Eric Frenzel, nouveau champion olympique devant le Japonais Akita Watabe et le Norv�gien Magnus Krog.
Probl�me de mat�riel ou "jour sans" ?�
Le Fran�ais �tait huiti�me � l'issue du saut. Parti avec 31 secondes de handicap pour le parcours de 10 km en ski de fond, il a explos� peu apr�s la mi-course, coll� sur la neige molle, r�chauff�e par la douceur printani�re. Une contre-performance qu'il a analys� au micro de France T�l�vision s � l'issue de sa course.�
Cette vid�o n'est plus disponible
"D�s le d�but, apr�s un tour, je sentais que les jambes ne r�pondaient pas. Plus �a allait, moins la glisse allait non plus. C'�tait un calvaire de finir cette course. Je me suis fait doubler par des gars � qui je mets une minute trente d'habitude."
"Je ne pense pas que ce soit un probl�me de 'fart'", r�pond-il.�"C'est un m�lange de physique et de mauvais choix tactique au niveau du ski".�"Un jour comme �a, cela ne veut rien dire sur mon �tat de forme g�n�ral", assure-t-il.�
Encore deux �preuves pour se rattraper�
Lors des championnats du monde de 2011, le skieur�avait aussi fait une contre-performance, avant d'�tre sacr� champion du monde sur grand tremplin.�En sera-t-il de m�me au JO ? R�ponse, d�s mardi 18 f�vrier, avec le grand tremplin et, deux jours plus tard, dans l'�preuve de combin� nordique par �quipes.
Jason Lamy-Chappuis�abordait ce nouveau rendez-vous partag� entre les r�sultats mitig�s tout au long de l'hiver et une confiance quasi-in�branlable, nourrie par ses r�sultats lors des grandes comp�titions.�En 2013, il avait d�croch� trois titres mondiaux, en�petit tremplin, en �preuve par �quipes et en sprint par �quipes.
