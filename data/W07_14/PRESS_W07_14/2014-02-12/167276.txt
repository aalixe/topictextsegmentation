TITRE: Audi S1 : 231 ch et une traction int�grale  - La Revue Auto
DATE: 2014-02-12
URL: http://www.larevueautomobile.com/News/Audi_S1-Sportback_7034.actu
PRINCIPAL: 167274
TEXT:
�
Audi S1 : 231 ch et une traction int�grale
La l�gendaire AUDI S1 qui dominait le championnat du monde de Rallye dans les ann�es 80, offre son patronyme � la nouvelle petite bombinette d?Audi, les S1 et S1 Sportback. Mais il n?est pas question pour la marque aux anneaux de corrompre cette appellation l�gendaire pour rien. Les nouvelles S1 d?Audi prennent la t�te de leur cat�gorie gr�ce 231 canassons qui se trouve sous le capot.
Sous le capot
Les Audi S1 et S1 Sportback re�oivent un quatre cylindres 2.0 litres turbo � injection directe d�veloppant une puissance de 231 chevaux et un couple de 370 Nm. Le nouveau bolide flanqu� du S est capable de se taper un 0 � 100 km/h en 5,8 secondes (et 5,9 secondes pour la Audi S1 Sportback). La cavalerie est brid�e �lectroniquement � la vitesse maximum de 250 km/h. En cycle mixte, le moteur � la sonorit� sportive ne consomme que 7,0 l/100 km ou 7,1 l/100 km (162g CO2/km pour Audi S1).
La preuve par 4
Tous les mod�les de la gamme S d�Audi sont �quip�s de la transmission int�grale quattro. La nouvelle Audi S1, n�y fera pas exception ! Elle y rajoutera, un blocage transversale �lectronique avec gestion roue par roue du couple, un embrayage multidisque, un ch�ssis revu en profondeur, une direction assist�e �lectrom�canique et des freins � disque aux dimensions g�n�reuses (310 millim�tres � l'avant). Les deux compactes sportives disposent en s�rie de jantes 18 pouces avec des pneus 225/35 R18.
Ta le look coco
Pas de doute. Au premier coup d��il, les badauds reconnaitront les Audi S1 et S1 Sportback tant ses mod�les marque leur empreinte sportive par rapport au retse de la gamme. Pour les identifi�s facilement, il vous suffira de rep�rer les feux arri�re � LED, les pare-chocs avant et arri�re, les bas de caisse et le syst�me d'�chappement qui ont tous �t� redessin�s. Le pack style ext�rieur quattro, propos� en option, accentue encore l'allure dynamique, notamment avec un grand spoiler de toit.
L�excellence � un prix
Les Audi S1 et S1 Sportback seront disponibles en France en mai 2014 aux prix respectifs de 33 900 � et 34 800 �. Pour le march� fran�ais, l'�quipement est enrichi : jantes alliage de 18 pouces, r�troviseurs ext�rieurs escamotables �lectriquement, r�troviseur int�rieur automatique jour/nuit, aide au stationnement, r�gulateur de vitesse, accoudoir central, MMI GPS couleur, radio Concert, Cobra Track by Audi. Les projecteurs X�non plus sont �galement en s�rie.
Benoit Alves - Publication :                          12/02/2014 � 13:00:53
Photos
Audi S1 Sportback
ACTU AUTOMOBILE
