TITRE: Lille monte dans le quart tout au bout du suspense - 20minutes.fr
DATE: 2014-02-12
URL: http://www.20minutes.fr/lille/1296674-lille-monte-quart-tout-bout-suspense
PRINCIPAL: 165854
TEXT:
La joie des lillois apr�s leur victoire contre Caen en 8�me de finale de la Coupe de France. M.Libert/20 Minutes
FOOTBALL - Le Losc a �limin� Caen (3-3) apr�s la s�ance des tirs aux buts...
Le Losc en a vu de toutes les couleurs. Qualifi� mardi en quart de finale de la Coupe de France apr�s les tirs aux buts (3-3, 6-5 t.a.b) face � Caen, pourtant modeste huiti�me de Ligue 2, le club nordiste est pass� tout pr�s du gouffre. L'honneur est sauf, l'aventure continue m�me si des interrogations demeurent.
Si la Coupe de France est��un objectif�, comme l'a affirm� Ren� Girard cette semaine, pourquoi avoir fait tourner son effectif dans de telles proportions?�Avec pas moins de huit changements par rapport � l'�quipe qui a battu Sochaux samedi (2-0), le coach lillois a pris un gros risque qui a failli se retourner contre lui apr�s une premi�re mi-temps cauchemardesque pour Steve Elana. Le gardien, habituel rempla�ant d'Enyeama est pass� compl�tement � c�t�. Sur un d�gagement au pied rat�, le portier nordiste a offert, malgr� lui, une passe d�cisive � Koita tout heureux de marquer dans le but vide (14e).
L'attaquant caennais profitera ensuite de l'apathie de la d�fense lilloise pour s'offrir un doubl� (29e) Si Rodelin a r�duit l'�cart (33e), Elana n'en avait pas fini avec sa sale soir�e. En manque total de confiance, le gardien a d� aussi subir les moqueries les 6000 spectateurs du stade Pierre Mauroy � chaque ballon touch�. Heureusement, Lille a r�agi en deuxi�me p�riode gr�ce � Delaplace (77e) puis Roux (83e) qui pensait bien offrir la qualification � son �quipe.
Mais sur un ultime coup franc caennais, Rodelin marque contre son camp (90e) et pousse les deux �quipes � la prolongation puis aux tirs aux buts o� le rat� d'Autret qualifie le Losc au bout de la nuit.
