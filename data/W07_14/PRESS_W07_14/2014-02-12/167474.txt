TITRE: Les smartphones et les tablettes tirent le march� des biens techniques - Flash actualit� - High-tech- 12/02/2014 - leParisien.fr
DATE: 2014-02-12
URL: http://www.leparisien.fr/flash-actualite-high-tech/les-smartphones-et-les-tablettes-tirent-le-marche-des-biens-techniques-12-02-2014-3583995.php
PRINCIPAL: 167469
TEXT:
Les smartphones et les tablettes tirent le march� des biens techniques
Publi� le 12.02.2014, 13h29
Tweeter
La consommation de biens techniques en France s'est de nouveau contract�e de 2% en 2013, � 15,4 milliard d'euros, mais la d�ferlante des smartphones et tablettes semble le pr�mice d'un retour � la croissance en 2014, a indiqu� mercredi l'institut GfK. | Martin Bureau
R�agir
La consommation de biens techniques en France s'est de nouveau contract�e de 2% en 2013, � 15,4 milliard d'euros, mais la d�ferlante des smartphones et tablettes semble le pr�mice d'un retour � la croissance en 2014, a indiqu� mercredi l'institut GfK.
Les ventes de mat�riel high-tech ont de nouveau �t� touch�es en 2013 par le repli des �crans de t�l�visions, mais "la forte croissance des t�l�phones et des tablettes a compens� le d�clin du march� de la t�l�vision", qui dure depuis 2010, a indiqu� Julien Jolivet, analyste chez GfK, lors d'une conf�rence de presse.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
23,7 millions de t�l�phones et smartphones ont �t� vendus en France en 2013 contre 22,9 millions l'ann�e pr�c�dente. Les ventes non subventionn�es ont explos�, repr�sentant une vente sur quatre, et un mobile sur deux s'est vendu en-dessous de 150 euros.
"Les smartphones et tablettes ont concentr� la demande des m�nages, mais les autres march�s se sont appuy�s sur la connectivit�, et d'autres viennent enrichir cette offre", comme les appareils photos connect�s, a-t-il ajout�.
Ainsi, les ventes de t�l�viseurs ont recul� de 15% en volume, � 5,7 millions d'unit�s, l'an dernier, mais un quart des ventes ont �t� consacr�es � la Smart TV (connect�e).
Les tablettes ont permis au secteur informatique d'atteindre un nouveau record avec 11 millions d'unit�s vendues en 2013 (PC, notebooks, imprimantes et tablettes) dont 6,2 millions de tablettes. Ce succ�s du tactile a toutefois eu un impact sur les ventes de PC fixe, de netbooks, et des accessoires: webcams, souris et claviers.
Quant au march� de la photo, il a fortement souffert en 2013 avec un recul des volumes de 18% � 3,7 millions d'unit�s, et une chute de 13% du chiffre d'affaires. Les ventes d'appareils compacts ont notamment �t� touch�es par l'essor des smartphones qui ont tous une fonction appareil photo.
GfK pr�voit pour 2014 une l�g�re reprise de la croissance de 0,7% des biens techniques � 15,6 milliards d'euros, principalement tir� par le secteur t�l�coms.
