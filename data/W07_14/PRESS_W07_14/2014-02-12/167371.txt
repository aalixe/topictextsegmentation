TITRE: Bradley Cooper et Mary J. Blige... Des V.I.P au d�ner de gala � la Maison Blanche - People - MYTF1News
DATE: 2014-02-12
URL: http://lci.tf1.fr/people/bradley-cooper-et-mary-j-blige-des-v-i-p-au-diner-de-gala-a-la-maison-8364428.html
PRINCIPAL: 167369
TEXT:
Bradley Cooper et Mary J. Blige... Des V.I.P au d�ner de gala � la Maison Blanche
Dans l'actualit� r�cente
12 f�vrier 2014 � 14h49
Temps de lecture
fran�ois hollande , bradley cooper
PeopleBradley Cooper, Mary J. Blige, Julia Louis-Dreyfus... de nombreuses stars �taient pr�sentes au d�ner de gala � la Maison Blanche, organis� pour la venue de Fran�ois Hollande, aux Etats-Unis, ce mardi.
Ils �taient plus de 350 invit�s � la Maison Blanche, ce mardi, lors du d�ner de gala organis� � l'occasion de la venue de Fran�ois Hollande aux Etats-Unis . Parmi eux, de nombreux hommes politiques - Christine Lagarde, John Kerry, Fleur Pellerin, entre autres - mais aussi des stars du cin�ma et de la musique. Bradley Cooper , h�ros de Very Bad Trip n'aurait loup� cet �v�nement pour rien au monde. L'acteur, qui parle couramment le fran�ais, est arriv� au bras de sa compagne, Suki Waterhouse, �g�rie Burberry parfums, avec qui il partage sa vie depuis f�vrier 2013. On pouvait �galement compter sur la pr�sence de J.J. Abrams, le producteur de la s�rie Lost et r�alisateur de Mission Impossible 3.
Autres arriv�es remarqu�es, celles de Julia Louis-Dreyfus, star des s�ries 30 Rock et Seinfield, Mindy Kalling, l'actrice de 40 ans, toujours puceau et de La nuit au Mus�e 2 et de la chanteuse Mary J. Blige. Cette derni�re a, d'ailleurs, fait le show. Elle a, en effet, cl�tur� la soir�e en reprenant le titre de Jacques Brel "Ne me quitte pas", sous les yeux du Pr�sident de la R�publique fran�aise, Fran�ois Hollande . Un moment fort pour tous les convives. "Les Am�ricains avaient mis les petits plats dans les grands. Ils ont d�roul� tous les tapis rouges" a assur� Beno�t Thieulin, pr�sident du conseil national du num�rique (CNNUM) et invit� du d�ner de gala.
�
