TITRE: Huit voitures aval�es par un "sinkhole" au National Corvette Museum
DATE: 2014-02-12
URL: http://www.caradisiac.com/Huit-voitures-avalees-par-un-sinkhole-au-National-Corvette-Museum-92389.htm
PRINCIPAL: 168538
TEXT:
Huit voitures aval�es par un "sinkhole" au National Corvette Museum
Ecrit par Patrick Garcia le 12 F�vrier 2014
Le ph�nom�ne d�nomm� ��sinkhole�� constitue la source des photos les plus spectaculaires que l'on peut trouver sur la Toile. Ces immenses crat�res qui se creusent de l'int�rieur et engloutissent ce qui se trouve en surface lorsque s'effondrent les derniers m�tres de terre sont terrifiants � bien des �gards car ils peuvent se produire n'importe o�. Le dernier en date a tout simplement englouti 8 voitures du National Corvette Museum dans le Kentucky.
Le ph�nom�ne des sinkholes ou dolines est absolument terrifiant puisqu'il s'agit de crat�res qui se cr�ent non pas lorsqu�un objet frappe le sol mais par effondrement du sous-sol. Le dernier en date a �t� r�pertori� dans le Kentucky au centre m�me du Mus�e National de la Corvette. La cavit� de plus de 9m de profondeur et 12m de circonf�rence s'est creus�e exactement sous le toit du Sky Dome qui abrite le mus�e avec comme cons�quence spectaculaire, un effondrement de la dalle qui a laiss� appara�tre ce trou gigantesque dans lequel ont �t� englouties 8 voitures du mus�e.
Il s'agit d'une Spyder ZR-1 de 1993, d'une ZR1 Blue Devil de 2009 appartenant � General Motors et d'un mod�le 1962, une version PPG Pace Car de 1984, la millioni�me Corvette de 1992, la 40e anniversaire Ruby Red de 1993, une Corvette Z06 Mallet Hammer de 2001 et la 1,5 millioni�me Corvette datant de 2009, toutes propri�t�s du mus�e. Par chance, l'incident qui a eu lieu dans la matin�e de mercredi n'a fait aucun bless�.
