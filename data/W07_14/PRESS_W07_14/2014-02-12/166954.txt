TITRE: A la Une | Epidémie de grippe en Lorraine
DATE: 2014-02-12
URL: http://www.estrepublicain.fr/actualite/2014/02/12/epidemie-de-grippe-en-lorraine
PRINCIPAL: 166950
TEXT:
- Publié le 12/02/2014
Epidémie de grippe en Lorraine
En France métropolitaine, 13 régions sont en situation d�??épidémie pour la grippe, dont la Lorraine.
photo d'archives ER.
Selon les critères définis par le Réseau des GROG , (groupes régionaux d'observation de la grippe) la grippe est épidémique depuis trois semaines au niveau national.
En France métropolitaine, 13 régions sont en situation d�??épidémie : Auvergne, Bourgogne, Champagne-Ardenne, Ile-de-France, Languedoc-Roussillon, Limousin, Lorraine, Midi-Pyrénées, Pays de la Loire, Picardie, Poitou-Charentes, PACA et Rhône-Alpes.
Au cours des deux dernières semaines, plus d�??un prélèvement sur deux fait par les vigies du Réseau des GROG et analysé en laboratoire était positif pour la grippe.
Vos commentaires
