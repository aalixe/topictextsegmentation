TITRE: Les idées du Front national ne cessent de progresser | Site mobile Le Point
DATE: 2014-02-12
URL: http://www.lepoint.fr/politique/plus-d-un-tiers-des-francais-approuve-le-front-national-12-02-2014-1790788_20.php
PRINCIPAL: 165728
TEXT:
12/02/14 à 07h35
Les idées du Front national ne cessent de progresser
INFOGRAPHIE. 34 % des Français affirment adhérer aux "idées du Front national" et 58 % jugent Marine Le Pen "capable de rassembler au-delà de son camp".
La présidente du Front national, Marine Le Pen. FAYOLLE PASCAL / SIPA
Le Point.fr (avec AFP)
Ils sont désormais plus d'un tiers des Français (34 %) à adhérer aux "idées du Front national", selon le baromètre d'image du FN réalisé par TNS Sofres pour Le Monde, France Info et Canal+. Certes, cela signifie qu'a contrario ils sont près de six sur dix (59 %) à ne pas adhérer à ses idées, mais le niveau d'adhésion n'a cessé de croître ces dernières années, selon cette étude, rendue publique mercredi et réalisée chaque année depuis 1984.
Ils étaient 22 % en 2011, lors de la prise de fonction de Marine Le Pen à la tête du parti, puis 31 % en 2012 et 32 % en 2013. La part de personnes interrogées qui n'adhèrent "ni aux critiques ni aux solutions de Marine Le Pen" reste prépondérante à 43 %, mais elle est en recul de trois points par rapport à 2013. Elles sont 14 % (+ 2 points) à adhérer aux "critiques et solutions" et 35 % (=) à souscrire seulement aux critiques formulées par Marine Le Pen, mais pas à ses solutions.
Une droite "patriote"
Cette progression de l'adhésion aux idées semble traduire la performance personnelle de Marine Le Pen et le succès de sa "dédiabolisation". L'image de la présidente du FN recueille de plus en plus d'opinions favorables : 58 % jugent qu'elle est "capable de rassembler au-delà de son camp" (+ 5), 56 % qu'elle "comprend les problèmes quotidiens des Français" (+ 7) et 40 % qu'elle "a de nouvelles idées pour résoudre les problèmes de la France" (+ 5). Ils sont une majorité (46 %) à considérer Marine Le Pen comme "plutôt la représentante d'une droite patriote attachée aux valeurs traditionnelles" (+ 2), contre 43 % qui la rattachent à "une extrême droite nationaliste et xénophobe". Pour 54 %, le FN reste un parti protestataire. Mais, pour 35 % des personnes interrogées, il pourrait au contraire participer à un gouvernement.
Deux points-clés du programme du FN sont cependant rejetés à une très large majorité : la sortie de l'euro (64 % y sont opposés contre 29 %) et la préférence nationale en matière d'emploi (72 % la rejettent contre 24 %).
