TITRE: Orange pr�voit l'ouverture de la 4G dans 24 agglom�rations suppl�mentaires Orange Op�rateurs Mobile - Echos du Net
DATE: 2014-02-12
URL: http://www.echosdunet.net/dossiers/dossier_13151_orange%2Bprevoit%2Bouverture%2B4g%2Bdans%2B24%2Bagglomerations%2Bsupplementaires.html
PRINCIPAL: 165979
TEXT:
Orange pr�voit l'ouverture de la 4G dans 24 agglom�rations suppl�mentaires
Publi� par achtungbaby dans la cat�gorie Op�rateurs Mobile / Orange le 10/02/2014
Lu 956 fois - 0 commentaire
Orange continue de d�ployer la 4G � un rythme soutenu pour tenter de rattraper son retard sur Bouygues Telecom. N'ayant pas l'avantage du refarming de la bande des fr�quences 800 Mhz, Orange installe de nouvelles antennes-relais sur la bande 2600 Mhz. 24 nouvelles agglom�rations sont pr�vues au programme de l'op�rateur en ce d�but d'ann�e 2014.
Malgr� un ralentissement durant les f�tes de fin d'ann�e, Orange est rest� l'op�rateur qui a le plus d�ploy� d'antennes-relais ces derniers mois avec 454 nouvelles antennes en janvier. Ainsi, l'op�rateur comptait 4699 antennes d�clar�es en service d�but f�vrier, et 4760 demandes aupr�s de l'ANFR. Orange ne l�che pas la pression et pr�voit d'ajouter 24 agglom�rations � son tableau de chasse en ce d�but d'ann�e 2014.
Depuis la fin 2013, Orange a d�ploy� son r�seau 4G � Charleville-M�zi�res, Dijon et Lourdes. Les prochaines agglom�rations � basculer en 4G sont Saint Omer, Cambrai, Abbeville, Laon, Meru, Chantilly, Verdun, Epernay, Longwy, Vitry-le-Fran�ois, Chaumont, Saint-Die-Des-Vosges, Colmar, Oyonnax, Draguignan, Calvi, Porto-Vecchio, Foix, Cognac, Saintes, Challans, Montaigu, Foug�res et Lisieux.
On notera un �cart entre les chiffres de l'ANFR de ce d�but de mois ( Observatoire de la 4G en France ), et ce qu'annonce Orange. L'ANFR a re�u 4699 d�clarations d'antennes actives par Orange, mais ce dernier annonce quant � lui 5728 antennes actives sur son site. Toutefois, il se contente d'annoncer une couverture de 50% de la population, la m�me qu'en fin d'ann�e 2013.
D�but janvier, Orange annon�ait 1 millions de clients 4G. L'op�rateur a d�cid� de toucher un plus grand nombre en �largissant ses offres pouvant b�n�ficier de la 4G. Depuis le 6 f�vrier 2014, Orange propose la 4G sur tous ses forfaits Origami (avec Data), d�s Origami Zen.
Les forfaits Origami Zen sont disponibles � partir de 19,99 �/mois et permettent de b�n�ficier :
des appels & SMS/MMS illimit�s en France et DOM
du cloud d'Orange avec 50 Go (uniquement Zen 500 mo)
de 500 Mo d'enveloppe Data en 4G, avec d�bit r�duit au del� (Zen 500 mo)
Les forfaits Origami Play sont disponibles � partir de 32,99 �/mois et permettent de b�n�ficier :
des appels & SMS/MMS illimit�s en France et DOM
des appels vers les fixes de 100 destinations sur la version 7 Go
de 3 ou 7 Go d'enveloppe Data en 4G, avec d�bit r�duit au del�
de 7 ou 14 jours par an d' internet mobile avec 1 ou 2 Go en Europe et DOM
du cloud d'Orange avec 100 Go (non d�duit de l'enveloppe data)
de l'acc�s � Deezer+ Premium
de l'option Multi-SIM offerte
et de la Ligue 1 Saison 2012-2014
Les forfaits Origami Jet sont d�sormais d�compos�s en deux forfaits : Jet Voyageur et Jet Grand Voyageur, et incluent �galement l'acc�s � Deezer+ Premium, 100 Go dans le Cloud d'Orange (non d�duit de l'enveloppe data), l'option Multi-SIM offerte et la Ligue 1 Saison 2012-2014.
C�t� quadruple-play, Orange propose toujours son forfait Edition Open Play 4G .
Pour finir, depuis d�but janvier, Orange propose un acc�s � la 4G depuis son offre sans engagement, le forfait Sosh 5 Go 4G/H+ .
