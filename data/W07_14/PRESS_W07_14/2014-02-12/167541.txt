TITRE: Cancer du sein: et si les mammographies ne servaient � rien? - BFMTV.com
DATE: 2014-02-12
URL: http://www.bfmtv.com/societe/cancer-sein-polemique-mammographies-relancee-708942.html
PRINCIPAL: 167350
TEXT:
Non, la pratique de mammographies annuelles ne permet pas de r�duire la mortalit� par cancer du sein, affirme une �tude canadienne.
�� �
R�alis�e sur pr�s de 90.000 femmes �g�es de 40 � 59 ans, suivies pendant 25 ans, l'�tude conclut que les femmes qui avaient subi des mammographies annuelles pendant cinq ans n'avaient pas moins de risques de mourir d'un cancer du sein que celles ayant seulement b�n�fici� d'un simple examen physique.
�� �
Au bout de 25 ans, 500 d�c�s par cancer du sein �taient survenus chez les 44.925 femmes suivies par mammographies, contre 505 d�c�s chez les 44.910 femmes du groupe t�moin. Les femmes avaient �t� assign�es dans les deux groupes par tirage au sort.
Des cancers "sur-diagnostiqu�s"
Les tumeurs du sein d�tect�es �taient en revanche plus nombreuses dans  le premier groupe, soit 3.250 au total, contre 3.133 dans le second � la fin  de l'�tude.
�� �
Le d�s�quilibre �tait d�j� net au bout de  cinq ans, avec 666 cancers d�tect�s chez les femmes sous mammographies,  contre 524 dans le groupe t�moin, soit un "exc�dent" de 142 tumeurs.  Cet "exc�dent" �tait encore de 106 tumeurs au bout de 15 ans, ce qui,  selon les auteurs, "signifie que 22% des cancers diagnostiqu�s dans le  premier groupe ont �t� surdiagnostiqu�s".
�� �
Les tumeurs �taient de surcro�t plus petites (1,4 cm dans le premier groupe contre  2,1 cm dans le second) au moment du diagnostic. Le surdiagnostic fait r�f�rence � la d�tection de tr�s petites tumeurs    qui n'auraient pas eu d'impact du vivant de la personne concern�e.
Une pol�mique de longue dur�e
En se fondant sur des �tudes montrant une baisse de la mortalit�, de   nombreux pays occidentaux ont mis en place des programmes de d�pistage   organis� du cancer du sein. En France par exemple, le programme   s'adresse � toutes les femmes �g�es de 50 � 74 ans qui sont invit�es � faire des mammographies tous les deux ans, prises en charge � 100% par   l'Assurance maladie.
�� �
Mais d'autres �tudes sont plus   contradictoires: selon une �tude de la Collaboration Cochrane, publi�e   pour la premi�re fois en 2000 et r�guli�rement r��valu�e depuis, le  taux  de mortalit� des femmes d�pist�es ne serait gu�re diff�rent de  celui  des autres femmes.
�� �
Une �tude britannique publi�e  en 2012  avait au contraire estim� que le d�pistage organis� du cancer  du sein  sauvait des vies mais entra�nait un surdiagnostic estim� � pr�s  de 20%  des cancers d�pist�s.
A lire aussi
