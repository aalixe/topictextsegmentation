TITRE: Roms. A Bobigny, un enfant retrouv� mort dans un camp apr�s un incendie
DATE: 2014-02-12
URL: http://www.ouest-france.fr/roms-bobigny-un-enfant-retrouve-mort-dans-un-camp-apres-un-incendie-1925239
PRINCIPAL: 165892
TEXT:
Roms. A Bobigny, un enfant retrouv� mort dans un camp apr�s un incendie
Seine-Saint-Denis -
Achetez votre journal num�rique
Un incendie d'origine inconnue s'est d�clar�e dans un camp de Roms de Bobigny. Un enfant  trouv� la mort.
Un enfant a �t� retrouv� mort mercredi par les pompiers � Bobigny (Seine-Saint-Denis) apr�s l'�vacuation matinale d'un camp de Roms � la suite d'un incendie, a-t-on appris de source pr�fectorale.
"Le corps sans vie d'un enfant a �t� retrouv� dans une cabane � l'int�rieur du camp", a indiqu� cette source� pr�cisant qu'un "incendie d'origine inconnue s'est d�clar� dans le camp vers 5h40 et a men� � l'�vacuation de 200 personnes".
Tags :
