TITRE: Sondage : 80% des maires sont heureux
DATE: 2014-02-12
URL: http://www.boursorama.com/actualites/sondage--80-des-maires-sont-heureux-27d1e20bb834b697c2153810351303c9
PRINCIPAL: 0
TEXT:
Sondage : 80% des maires sont heureux
Le Figaro le 12/02/2014 � 12:09
Imprimer l'article
Ce bonheur est plus marqu� dans les grandes villes que dans les petites communes, r�v�lent un sondage CSA pour l'Observatoire du bonheur � quelques semaines des �lections municipales.
L'�tude bouscule une id�e largement r�pandue: celles du maire qui, d�go�t�, choisit de ne pas de repr�senter. Nos �lus seraient en r�alit� �panouis dans leurs fonctions. Pas moins de 80 % �lus municipaux, qu'ils soient maires ou adjoints, se d�clarent heureux, d'apr�s un sondage CSA* pour l'Observatoire du bonheur publi� ce mercredi. �Le r�sultat nous a m�me assez surpris�, conc�de Jean-Pierre Ternaux, directeur de recherche au CNRS et coordonnateur de l'observatoire cr�� par Coca-Cola.
Toutefois, ce sentiment d'accomplissement varie d'une situation � une autre. Les maires des petites communes se sentent par exemple moins bien lotis que ceux des plus grandes communes. Ainsi, dans les municipalit�s de moins de 2000 habitants, les �lus ne sont que 18 % � se d�clarer tr�s heu...
