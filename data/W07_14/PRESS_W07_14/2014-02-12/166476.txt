TITRE: Foot - Coupe - Nice - De la revanche dans l'air
DATE: 2014-02-12
URL: http://www.lequipe.fr/Football/Actualites/De-la-revanche-dans-l-air/440375
PRINCIPAL: 0
TEXT:
a+ a- imprimer RSS
Etrill� en L1 par Monaco, Nice veut se venger en Coupe. (L'Equipe)
C��tait le 3 d�cembre dernier. Pour le premier derby Nice-Monaco � l�Allianz Riviera, devant pr�s de 30 000 spectateurs, l�ASM avait d�chiquet� une �quipe ni�oise ajour�e et impuissante avec notamment deux buts sign�s James Rodriguez et Rivi�re dans une premi�re demi-heure � sens unique ( 3-0 au final ). Ce mercredi, m�me derby, m�me endroit, cette fois en huiti�mes de finale de Coupe de France. Nice est toujours en course apr�s avoir surpris Nantes ( 2-0 , 32e de finale, le 5 janvier) et surtout Marseille ( 5-4 , 16e, le 21) chez eux, ce qui est fort. Il serait dommage de s�arr�ter en si bon chemin mais Monaco est toujours aussi fort, aussi.
Bodmer : �Ils nous avaient march� dessus�
�En Championnat, ils nous avaient march� dessus, se rem�more le d�fenseur ni�ois Mathieu Bodmer. Ils nous avaient domin� � la fois dans le jeu et physiquement avec une grosse pression d�entr�e pour nous emp�cher de relancer de derri�re, et ils �taient quasiment ressortis vainqueurs de chaque duel. Ensuite, ils avaient mis en place leur jeu et on sait qu�ils ont des joueurs de ballon qui peuvent faire la diff�rence � tout moment.� Bodmer, qui a jou� deux finales de Coupe de France avec Lyon et le PSG, ne veut pas revivre �a : �Il faut d�abord r�pondre dans l�impact et ensuite jouer avec nos qualit�s, avec notre fonds de jeu qu�on peut d�velopper aussi. C�est un match de Coupe contre une grande et belle �quipe. Il faudra vraiment �tre � 100 % dans tous les compartiments du jeu si on veut avoir une chance de passer.�
Et surtout ne pas ressembler � l��quipe qui a perdu samedi � Valenciennes ( 1-2 ). �On est pass� au travers, regrette Bodmer. On a donc des choses � se faire pardonner, on doit se racheter. Contre Monaco, on a une revanche � prendre et �a passe par un gros gros match.� Nice sera priv� de deux �l�ments majeurs, Digard (mollet) et Pejcinovic (adducteurs), mais pourra peut-�tre compter sur la forme ou les retours en forme de Maupay, Bosetti, Bauth�ac ou Pied.
J.Ri.
