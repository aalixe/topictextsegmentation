TITRE: Alg�rie: 3 jours de deuil apr�s le crash d'un avion militaire qui a fait 77 morts - Flash actualit� - Monde - 12/02/2014 - leParisien.fr
DATE: 2014-02-12
URL: http://www.leparisien.fr/flash-actualite-monde/algerie-3-jours-de-deuil-apres-le-crash-d-un-avion-militaire-qui-a-fait-77-morts-12-02-2014-3582811.php
PRINCIPAL: 166074
TEXT:
Alg�rie: 3 jours de deuil apr�s le crash d'un avion militaire qui a fait 77 morts
Publi� le 12.02.2014, 06h28
Un deuil national de trois jours d�bute mercredi en Alg�rie apr�s le crash d'un avion militaire dans l'est du pays, qui a fait 77 morts, l'une des pires catastrophes a�riennes en Alg�rie.
1/4
R�agir
Un deuil national de trois jours d�bute mercredi en Alg�rie apr�s le crash d'un avion militaire dans l'est du pays, qui a fait 77 morts, l'une des pires catastrophes a�riennes en Alg�rie.
"Le crash a fait 77 victimes et un rescap�, gri�vement bless�, a �t� transf�r� � l'h�pital militaire r�gional de Constantine", selon le minist�re alg�rien de la D�fense cit� par l'agence APS.
Vos amis peuvent maintenant voir cette activit� Supprimer X
Un survivant souffrant d'un traumatisme cr�nien a �t� d�couvert par la protection civile.
En d�but de soir�e , 76 corps, dont quatre femmes, avaient �t� r�cup�r�s par les secouristes, selon un bilan de la protection civile.
L'avion, un Hercules C-130, qui assurait la liaison entre la pr�fecture de Tamanrasset (2.000 km au sud d'Alger) et Constantine (450 km � l'est d'Alger), transportait des militaires et des familles de militaires, selon la m�me source.
L'appareil s'est �cras� alors qu'il survolait le mont Fortas dans la wilaya (pr�fecture) d'Oum El Bouaghi (500 km � l'est d'Alger) vers midi (11H00 GMT).
La t�l�vision a diffus� des images des lieux montrant la carcasse de l'appareil reposant dans un environnement d�sert, rocailleux et vert, dans une zone montagneuse.
Le minist�re de la D�fense a indiqu� que l'avion transportait 74 passagers et quatre membres de l'�quipage.
La radio alg�rienne avait d'abord annonc� "une centaine de morts" en fin d'apr�s-midi parlant de 103 personnes � bord.
"Les conditions m�t�orologiques tr�s d�favorables avec un orage accompagn� de chutes de neige seraient � l'origine de ce crash", a annonc� le minist�re de la D�fense dans un communiqu�.
Le crash se serait produit au moment des man?uvres d'approche de l'a�roport de Constantine.
"A la suite de cet accident, le plan de recherches et de sauvetage a aussit�t �t� d�clench� et les unit�s de secours relevant de l'Arm�e nationale populaire (ANP) et de la Protection civile se sont d�plac�es sur les lieux pour apporter les premiers secours", selon le minist�re de la D�fense.
Commission d'enqu�te
Pr�s de 250 secouristes de la protection civile, sont affect�s au site du crash rendu difficile d'acc�s en raison du mauvais temps et de l'escarpement du lieu, a annonc� la radio alg�rienne.
Un haut responsable militaire, le colonel Lahmadi Bouguern, a fait �tat � l'agence APS de fortes rafales de vents depuis quelques jours dans cette r�gion et d'un manque de visibilit�.
Le pr�sident alg�rien Abdelaziz Bouteflika a d�cr�t� trois jours de deuil national et a pr�sent� ses condol�ances aux familles des victimes.
"Les soldats qui ont p�ri dans le crash de l'avion militaire sont des martyrs du devoir, aussi nous d�cr�tons un deuil national de trois jours � partir de mercredi", a �crit M. Bouteflika dans un message de condol�ances diffus� par l'APS.
Le pr�sident a �galement d�cid� que "la journ�e de vendredi sera consacr�e au recueillement � leur m�moire".
"Une commission d'enqu�te a �t� cr��e et d�p�ch�e sur les lieux pour d�terminer les causes et les circonstances exactes de ce tragique accident", selon le communiqu� minist�riel.
Le site en ligne du journal francophone El Watan affirme qu'une des deux bo�tes noires a �t� retrouv�e.
Le vice-ministre de la D�fense nationale, Ahmed Ga�d Salah, devait se rendre sur les lieux.
Le pr�c�dent accident le plus meurtrier date de mars 2003. Un Boeing 737-200 de la compagnie publique alg�rienne Air Alg�rie avait fait 102 morts et un bless� en s'�crasant peu apr�s son d�collage de Tamanrasset.
D�but d�cembre 2012, deux avions militaires, se livrant � des entra�nements, se sont percut�s en plein vol � Tlemcen, dans l?extr�me ouest alg�rien, provoquant la mort des pilotes.
En novembre 2012, un bimoteur militaire de type CASA C-295, qui transportait une cargaison de papier fiduciaire pour la fabrication de billets pour la Banque d'Alg�rie s'�tait �cras� en Loz�re. Les cinq militaires � bord et le repr�sentant de la banque d'Alg�rie ont p�ri dans le crash.
En 2003, un Hercule C-130 de l?arm�e s'�tait �cras� sur un quartier r�sidentiel � Boufarik (banlieue d'Alger), faisant 20 morts -les quatre membres de l?�quipage, huit passagers et huit personnes au sol suite � l?effondrement d?une habitation.
