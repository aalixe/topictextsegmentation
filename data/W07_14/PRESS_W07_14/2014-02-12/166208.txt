TITRE: Cin�ma: "Abus de faiblesse", une histoire vraie, un roman, un proc�s, un film - LExpress.fr
DATE: 2014-02-12
URL: http://www.lexpress.fr/actualites/1/culture/cinema-abus-de-faiblesse-une-histoire-vraie-un-roman-un-proces-un-film_1322984.html
PRINCIPAL: 166206
TEXT:
Cin�ma: "Abus de faiblesse", une histoire vraie, un roman, un proc�s, un film
Par AFP, publi� le
12/02/2014 � 08:48
, mis � jour � 11:11
Paris - Son histoire, la r�alisatrice Catherine Breillat l'a d�j� racont�e dans un livre. Puis il y a eu le temps du proc�s au cours duquel "l'escroc des stars" a �t� condamn�. Le roman est maintenant un film �ponyme , "Abus de faiblesse", en salles mercredi.
Son histoire, la r�alisatrice Catherine Breillat l'a d�j� racont�e dans un livre. Puis il y a eu le temps du proc�s au cours duquel "l'escroc des stars" a �t� condamn�. Le roman est maintenant un film �ponyme , "Abus de faiblesse", en salles mercredi.
afp.com
Catherine Breillat, actrice, romanci�re et cin�aste ("Tapage nocturne", Romance" ou "36 Fillette") a �t� victime d'un accident vasculaire c�r�bral en 2005.�
Deux ans plus tard, alors qu'elle souffre encore d'h�mipl�gie et de crises d'�pilepsie, elle entre en contact avec Christophe Rocancourt, qui a d�j� purg� cinq ans de prison aux Etats-Unis pour avoir escroqu� le tout-Hollywood sous divers pseudonymes. Il la fascine. �
Elle veut lui proposer un r�le dans un projet de film, "Bad Love", qu'elle doit tourner avec Naomi Campbell. Elle lui confie �galement l'�criture d'un sc�nario intitul� "La vie amoureuse de Christophe Rocancourt". �
A la signature de ce dernier contrat, elle lui remet 25.000 euros. Suivent, en l'espace d'un an et demi, douze autres ch�ques d'un montant total de 703.000 euros.�
Catherine Breillat a fini par l'accuser d'avoir profit� de son �tat. Christophe Rocancourt a �t� condamn� en f�vrier 2012 � 16 mois de prison dont huit ferme, ainsi qu'� 578.000 euros de dommages et int�r�ts pour abus de faiblesse au pr�judice de la r�alisatrice.�
"Abus de faiblesse" est une oeuvre de fiction, insiste pourtant Catherine Breillat dans les notes de production. �
"Ce film n'est pas plus autobiographique que mes autres longs m�trages", insiste-t-elle, terroris�e � l'id�e qu'on pense qu'elle exploite sa propre histoire.�
Le film "n'a rien d'un exutoire pour moi (...) je n'ai pas besoin de cela". D'ailleurs elle dit avoir mis plus de deux ans � �crire ce sc�nario alors qu'habituellement, elle arrive � une premi�re version au bout de trois semaines. "Mais il y a une raison �vidente derri�re tout cela : j'avais peur de faire ce film", dit-elle encore.�
"Transfert affectif"�
Qu'importe. A l'�cran, Isabelle Huppert incarne jusqu'au bout des doigts cramp�s par la maladie Maud qui se r�veille un matin avec un corps � moiti� mort. Femme d�termin�e, elle se bat pour r�cup�rer de la vie dans ses membres. Cin�aste jusqu'au bout, elle veut mener � bien malgr� tout un projet de film, surtout depuis qu'elle a aper�u � la t�l�vision Vilko, arnaqueur de c�l�brit�s. Elle le veut.�
Kool Shen, ex-partenaire du groupe NTM avec JoeyStarr devenu un acteur confirm�, fait ici ses premiers pas devant la cam�ra, incarnant un Vilko animal. �
Si Catherine Breillat a imm�diatement pens� � Isabelle Huppert pour camper une femme aux allures de poup�e d�glingu�e, la cin�aste cherchait "un corps de rappeur" pour incarner l'arnaqueur.�
"Je ne connaissais rien de ce courant musical mais, instinctivement, il me semblait que s'en d�gageaient l'�nergie et la violence que je recherchais pour le personnage", poursuit-elle.�
Progressivement, Maud se laisse diriger, par le personnel m�dical mais surtout par Vilko qui l'entoure de plus en plus, sait se rendre indispensable. Il lui prend tout, tout en lui donnant une impression de chaleur familiale.�
Le ton entre les deux personnages devient cependant insensiblement plus dur. Surtout si Maud r�siste quand il lui demande de nouveaux ch�ques.�
"C'�tait moi, c'�tait pas moi", dit Maud/Isabelle Huppert, ruin�e.�
C'est � l'h�pital que tout a commenc�, se souvient Catherine Breillat: "parce que je faisais un transfert affectif sur toute personne qui me prenait en charge. �a n'a fait que continuer".�
Au proc�s, o� elle �tait apparue tr�s affaiblie et lourdement handicap�e, elle avait racont� que "Christophe Rocancourt �tait devenu (son) meilleur ami, celui qui disait qu'il s'occuperait de moi".�
Par
