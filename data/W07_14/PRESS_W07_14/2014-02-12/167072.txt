TITRE: iWatch : Apple pr�voirait un��cran incassable gr�ce au�saphir�? - Linternaute.com High-tech
DATE: 2014-02-12
URL: http://www.linternaute.com/hightech/mobile/iwatch-un-ecran-incassable-grace-au-saphir-0214.shtml
PRINCIPAL: 167071
TEXT:
Toute l'actualit� High-tech
Apple aurait command� une grande quantit� de ce mat�riau. Il ne s'agirait pas de prot�ger l'�cran de l'iPhone�6, comme cela a pu �tre indiqu�, mais celui de l'iWatch. Un site sp�cialis� pense qu'il s'agira du premier produit � b�n�ficier de cette technologie chez Apple.
La marque � la pomme voudrait lutter contre les �crans cass�s et proposer � ses clients des surfaces plus solides, moins susceptibles d'�tre ray�es ou fendill�es. C'est pourquoi Apple a command� une grande quantit� de cristaux de saphir, selon des sites am�ricains sp�cialis�s. En clair, l'utilisation de ce mat�riau pour la fabrication des �crans permettrait de les rendre presque incassables, ou en tout cas moins cassables. Le site " G for games " avance que cette technologie serait d'abord utilis�e pour l'iWatch, cette future montre connect�e.
Toujours selon ce site, ce n'est finalement pas l'iPhone�6 qui b�n�ficierait de cet �cran en cristal de saphir , contrairement aux rumeurs qui ont circul� ces derniers jours sur le Web � ce sujet. L'utilisation de ce mat�riau pour le prochain smartphone de la marque ferait peut-�tre trop grimper son prix�? La surface de l'�cran de l'iWatch �tant plus petit, le co�t de production avec du cristal de saphir pourrait ainsi �tre plus raisonnable pour ce produit.
EN VIDEO � En 2013, Apple a annonc� une baisse de son b�n�fice annuel, pour la premi�re fois en 11�ans.
Apple / A la une
