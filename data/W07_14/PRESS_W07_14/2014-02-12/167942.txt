TITRE: JO/Half-pipe: Mirabelle Thovex rejoint Sophie Rodriguez en finale - LExpress.fr
DATE: 2014-02-12
URL: http://www.lexpress.fr/actualites/1/sport/jo-snowboard-sophie-rodriguez-directement-en-finale-du-half-pipe_1323098.html
PRINCIPAL: 0
TEXT:
JO/Half-pipe: Mirabelle Thovex rejoint Sophie Rodriguez en finale
Par AFP, publi� le
12/02/2014 � 12:30
, mis � jour � 17:30
Rosa Khoutor (Russie) - La Fran�aise Mirabelle Thovex a rejoint sa compatriote Sophie Rodriguez en finale de l'�preuve olympique de snowboard half-pipe, qui va avoir lieu mercredi � 21h30 locales (18h30 fran�aises).
La Fran�aise Mirabelle Thovex a rejoint sa compatriote Sophie Rodriguez en finale de l'�preuve olympique de snowboard half-pipe, qui va avoir lieu mercredi � 21h30 locales (19h30 fran�aises). a rejoint sa compatriote Sophie Rodriguez en finale de l'�preuve olympique de snowboard half-pipe, qui va avoir lieu mercredi � 21h30 locales (19h30 fran�aises).
afp.com/Franck Fife
Au Parc extr�me de Rosa Khoutor, Thovex s'est qualifi�e pour la finale (douze concurrentes) en prenant la 5e place des demi-finales alors que Rodriguez, m�daill�e de bronze des Mondiaux-2013, avait d�croch� son billet d�s le stade des qualifications en prenant la 3e place de sa s�rie.�
En finale, les deux tricolores auront fort � faire face aux favorites les Am�ricaines Kelly Clark et Hannah Teter, � l'Australienne Torah Bright, championne olympique en titre, et � l'Espagnole Queralt Castelet, qui ont toutes marqu� au moins 92 points lors des qualifications.�
Mais Rodriguez, 25 ans, pourra compter sur sa grande exp�rience, elle qui a d�j� gagn� une m�daille aux Mondiaux (bronze en 2013) et aux X Games (3e place en 2010) et qui avait fini 5e aux JO-2010.�
La troisi�me Fran�aise en lice, Cl�mence Grimal s'est fait mal au dos et aux fesses � l'entra�nement juste avant les qualifications et a pris la 14e place de l'�preuve, parvenant � entrer en demi-finales avant de buter � ce stade.�
"C'est d�cevant car je n'ai pas pu montrer ce que je sais faire, a dit Grimal, 19 ans. J'ai un petit go�t amer. Mais ce n'est pas grave, je suis jeune, j'ai le temps. Si j'avais +plaqu�+ mon +run+, je pense que j'aurais pu aller en finale. Mais je vais arr�ter de broyer du noir et encourager les autres Fran�aises en finale, qui ont bien rid�."�
Par
