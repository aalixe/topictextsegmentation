TITRE: La doyenne des �toiles a 13,6 milliards d'ann�es, un record - GinjFo
DATE: 2014-02-12
URL: http://www.ginjfo.com/actualites/science-et-technologie/la-doyenne-des-etoiles-137-milliards-dannees-un-record-20140212
PRINCIPAL: 0
TEXT:
Home / Actualit�s / Science et technologie / La doyenne des �toiles a 13,6 milliards d�ann�es, un record
La doyenne des �toiles a 13,6 milliards d�ann�es, un record
Publi� par : J�r�me Gianoli dans Science et technologie 12/02/2014 0 4,787 Lectures
La d�couverte de la fuite des galaxies a transform� notre vision de l�univers. Son statut d�immuabilit� a vol� en �clat avec l�id�e qu�un commencement a eu lieu. Une d�couverte �tonnante d�voile la plus ancienne �toile jamais observ�e par l�homme, son age�? 13,6 milliards d�ann�e.
Edwin Powell Hubble est � l�origine de deux grandes d�couvertes. La premi�re est l�observation d�autres galaxies montrant ainsi que la Voie Lact�e n�a rien d�extraordinaire. La seconde est encore plus percutante puisqu�elle est � l�origine d�une transformation radicale de notre compression de l�univers.
La fuite des galaxies donne naissance au Big Bang.
Edwin Powell Hubble a observ� le d�calage vers le rouge du spectre de plusieurs galaxies et il a d�montr� que celles-ci s��loignaient les unes des autres et ce de fa�on proportionnelle � leur distance. Il faut bien comprendre ici, qu�il ne s�agit pas d�un d�placement physique des galaxies mais d�un accroissement de la trame de l�espace en 3 dimensions.
L�expansion de l�Univers a �t� d�couvert et ses implications ont chamboul� notre vision du monde.� En remontant le scenario � l�envers, les objets qui sont aujourd�hui distants, ont �t�, par le pass� forcement plus proches et ce jusqu�au point o�, il y a 13,9 milliards d�ann�es, l�Univers n��tait qu�une singularit�. Les astronomes ont alors conclu qu�une ��explosion�� (le terme est mal choisi car une explosion a lieu dans quelque chose) a du donner naissance aux premi�res briques de mati�re, � la lumi�re, aux �toiles, aux galaxies et�la vie.
La doyenne de l�Univers � d�sormais 13,6 milliards d�ann�e.
Des chercheurs astronomes de l�Universit� nationale d�Australie viennent d�annoncer avoir observ�s une �toile bien particuli�re. SMSS J031300.36-670839.3, c�est son petit nom, serait la doyenne de l�Univers observ� jusqu�� pr�sent avec un �ge plus qu�honorable de 13,6 milliards d�ann�es. Sa naissance remonterait ainsi � seulement deux cent millions d�ann�es apr�s le Big Bang. Elle a �t� d�couverte gr�ce au t�lescope SkyMapper.
La diversit� de la mati�re qui compose notre univers n�a pas �t� produite au commencement. Il s�agit du fruit de plusieurs g�n�rations d��toiles qui par leur fonctionnement utilisent des atomes l�gers (hydrog�ne, helium) pour les transformer en atome lourd comme le Fer. Les �toiles vieilles �se pr�sentent donc avec une concentration en fer tr�s faible. Cette propri�t� est un outil pour dater l��ge d�une �toile.
SMSS J031300.36-670839.3 aurait une concentration en fer un million de fois moindres que notre soleil, et 60 fois moins que dans n�importe quelle autre �toile, ce qui la place en t�te des �toiles les plus �g�es observ�es. Elle est de plus assez proche de la Terre. Situ�e dans la Voie Lact�e, sa distance est de seulement 6.000 ann�es-lumi�re.
En octobre 2013, une �tude a r�v�l� que la galaxie z8_GND_5296 �tait la plus veille jamais observ�e� (13,1 milliards d�ann�es). L��ge de notre univers est estim� entre 13,7, 13,8 milliards d�ann�es.
-----------------------------------------------------
Vos recherches qui ont men� � cet article :
smss j031300 36-670839 3
