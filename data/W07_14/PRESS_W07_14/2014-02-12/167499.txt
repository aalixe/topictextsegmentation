TITRE: George Clooney pour le retour des marbres du Parth�non dans leur pays - Gala
DATE: 2014-02-12
URL: http://www.gala.fr/l_actu/news_de_stars/george_clooney_pour_le_retour_des_marbres_du_parthenon_dans_leur_pays_308427
PRINCIPAL: 167498
TEXT:
Gala > L'actu > News de stars > George Clooney pour le retour des marbres du...
George Clooney pour le retour des marbres du Parth�non dans leur pays
L'acteur prend position en faveur des Grecs
Publi� le 12 f�vrier 2014 � 10h35 par           Saeptem
Le 27/02/2014 George Clooney: une nouvelle amoureuse?
Les marbres d'Elgin sont l'objet d'une vieille querelle entre la Gr�ce et l'Angleterre. Extraits du Parth�non par Lord Elgin au d�but du XIXe si�cle et rapatri�s � Londres pour y �tre expos�s, ils sont depuis r�clam�s par les Grecs � sans succ�s. � l'occasion du festival du film de Berlin, George Clooney a pris position lors d'une interview : il faut rendre ses sculptures � la Gr�ce !
"Je pense que ce serait juste, que ce serait une bonne chose. Ce serait m�me la bonne chose � faire." Quand un reporter grec demande � George Clooney ce qu'il pense d'un possible rapatriement des marbres d'Elgin sur le sol hell�nique, l'acteur cesse de faire le clown . Sensibilis� au sujet par son prochain film The Monuments Men (qui relate l'histoire de soldats sauvant des �uvres d'art de la destruction pendant la Seconde Guerre mondiale ), le beau George se prononce en faveur d'une certaine justice historique. Il pr�f�re voir ces sculptures � leur place originelle plut�t qu'au British Museum.
�
L'acteur est habitu� � prendre position : l'�t� dernier, il rappelait son engagement contre Omar el-B�chir , pr�sident de la R�publique du Soudan. Une volont� qui a bien failli lui co�ter la vie ; en voyage au Darfour, il avait �t� menac� par un enfant de 10 ans arm� d'une Kalachnikov .
�
George Clooney a donc cette fois choisi un sujet moins dangereux, bien que tr�s �pineux. Toutefois, ses amis acteurs l'ont tous dit : George est capable de tout . Son charme l�gendaire suffira-t-il � appuyer la cause grecque�?
A lire aussi :
