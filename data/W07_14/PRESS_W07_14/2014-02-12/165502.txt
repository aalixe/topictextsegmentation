TITRE: Coupe du Roi: le Real en finale, rideau pour l'Atletico - LExpress.fr
DATE: 2014-02-12
URL: http://www.lexpress.fr/actualites/1/sport/coupe-du-roi-le-real-en-finale-rideau-pour-l-atletico_1322943.html
PRINCIPAL: 165496
TEXT:
Coupe du Roi: le Real en finale, rideau pour l'Atletico
Par AFP, publi� le
11/02/2014 � 23:20
, mis � jour � 23:20
Madrid - Deux penalties de Cristiano Ronaldo ont permis mardi au Real Madrid d'�carter de la Coupe du Roi l'Atletico, tenant du titre, avec un succ�s 2-0 en demi-finale retour qui permettra au finaliste 2013 de briguer � nouveau le troph�e.
Le Real Madrid s'est facilement qualifi� pour la finale de la Coupe du Roi (coupe d'Espagne) en battant 2-0 l'Atletico, pourtant coleader de Liga avec le Real et le FC Barcelone, mardi, en demi-finales retour
afp.com
Malgr� un match houleux sur le terrain mais surtout en tribunes, avec notamment un briquet lanc� au visage de "CR7", les joueurs de Carlo Ancelotti ont confirm� leur large succ�s de l'aller (3-0).�
Et ils pourraient se voir offrir en finale mi-avril un clasico contre le FC Barcelone si ce dernier confirme mercredi (21h00 GMT) sur la pelouse de la Real Sociedad son avantage de deux buts, acquis la semaine derni�re (2-0).�
En deux derbys, le Real a fait vasciller l'�pouvantail "Atleti": d'abord en lui infligeant il y a une semaine sa premi�re d�faite depuis plus de trois mois, puis, mardi soir, en lui �tant son invincibilit� � domicile cette saison. �
Le stade Vicente Calderon, bouillant face � l'ennemi merengue, n'en a pas reconnu son �quipe. Certains spectateurs sont m�me sortis de leurs gonds: Ronaldo, qui rentrait aux vestiaires � la pause, a re�u un briquet au visage et une bagarre a �clat� en tribune en seconde p�riode.�
Il faut dire que les "Colchoneros" se sont senti flou�s par deux d�cisions arbitrales qui ont permis au Real de prendre l'ascendant.�
- Varane de retour comme titulaire -�
Au bout de seulement cinq minutes de jeu, Cristiano Ronaldo a d�boul� sur la gauche de la surface adverse et �t� accroch� par Javier Manquillo, conduisant l'arbitre � siffler. L'attaquant portugais, qui doit encore purger deux matches de suspension en Liga mais est libre de jouer en Coupe, s'est fait une joie de transformer (7e).�
Huit minutes plus tard, rebelote, avec cette fois une faute sur Gareth Bale. Ronaldo a r�cidiv� (16e), tuant dans l'oeuf toute esquisse de r�volte.�
La gifle est rude pour les "Colchoneros" qui ont "perdu" en trois jours la premi�re place du Championnat d'Espagne, qu'ils co-d�tiennent � �galit� avec le Bar�a et le Real, et leurs chances de conserver leur troph�e en Coupe.�
A leur d�charge, l'entra�neur Diego Simeone ne semblait gu�re croire � un retournement de situation, puisqu'il avait mis au repos des cadres comme Diego Godin, Arda Turan ou Gabi, avec �galement l'absence du gardien Thibaut Courtois (dos) et de l'attaquant Diego Costa (suspendu).�
D'ailleurs, seul Raul Garcia, qui a trouv� le poteau sur une frappe de l'entr�e de la surface, a vraiment inqui�t� Iker Casillas (11).�
Comme � l'aller, le match a �t� physique, et comme � l'aller, l'Atletico a �t� pris � son propre jeu, celui de l'engagement et de l'agressivit�. Sur un choc avec Ronaldo, Insua est d'ailleurs tr�s mal retomb� et a d� c�der sa place, ajoutant � la tension ambiante.�
Faute de beau football, cette rencontre a au moins permis � Karim Benzema de souffler sur le banc des rempla�ants et � Rapha�l Varane de faire son retour comme titulaire avec le Real.�
Le d�fenseur international fran�ais, remis de ses probl�mes � un genou, a su livrer une partie propre dans un match heurt�, disputant une rencontre dans son int�gralit� pour la premi�re fois depuis mi-novembre.�
Par
