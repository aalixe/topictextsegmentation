TITRE: Le constructeur automobile Mia Electric encore au bord de la faillite - 12 février 2014 - Challenges
DATE: 2014-02-12
URL: http://www.challenges.fr/entreprise/20140212.CHA0354/le-constructeur-automobile-mia-electric-a-nouveau-au-bord-de-la-faillite.html
PRINCIPAL: 0
TEXT:
Challenges > Entreprise > Le constructeur automobile Mia Electric encore au bord de la faillite
Le constructeur automobile Mia Electric encore au bord de la faillite
Publié le 12-02-2014 à 18h43
Mis à jour à 18h59
Soutenu par la r�gion Poitou-Charentes, le constructeur automobile a �t� plac� en redressement judiciaire mercredi.
La Mia Electric, du constructeur du même nom (ex-Heuliez) (C) SIPA
Le constructeur automobile Mia Electric a �t� plac� mercredi 12 f�vrier en redressement judiciaire par le tribunal de commerce de Niort, alors que les difficult�s s'accumulent, en d�pit d'un march� plus porteur et de l'implication de la r�gion Poitou-Charentes.
Mercredi matin, la soci�t� de quelque 200 salari�s bas�e � Cerizay (Deux-S�vres) a obtenu que ce redressement soit assorti d'une longue p�riode d'observation de six mois, pendant laquelle Mia sera encore pilot�e par l'actuelle pr�sidente Michelle Boos, mais avec un administrateur judiciaire.
"La mise en redressement judiciaire est la moins mauvaise solution et va nous permettre de mettre � plat les comptes et la strat�gie de l'entreprise", a r�agi S�gol�ne Royal, pr�sidente de la r�gion, actionnaire � hauteur de 12% du capital de Mia, reprise en juin 2013 par un consortium d'investisseurs, Focus Asia.
"On ne baisse pas les bras", a insist� S�gol�ne�Royal. "La bataille industrielle pour la voiture �lectrique 100% fran�aise continue", a-t-elle ajout�, en soulignant que cela permettrait notamment d'exiger de Michelle Boss qu'elle tienne "ses engagements sur les investissements" promis.
Liquidation d'Heuliez en 2010
Mia Electric, qui appartenait jusqu'en juin aux groupes allemands ConEnergy et Kohl, avait �t� cr�e en 2010 pour y concentrer l'activit� de fabrication de voitures �lectriques de l'�quipementier Heuliez, �galement soutenu par la r�gion et dont les difficult�s ont finalement conduit � la liquidation fin octobre 2013.
La production en s�rie des Mia avait d�but�, mais les objectifs de vente n'ont pas �t� atteints. En 2012, 337 voitures ont �t� vendues, selon la soci�t�. Le chiffre de 2013 n'�tait pas disponible.
Michelle Boos l'a reprise pour un montant jamais r�v�l�. A l'�poque, Focus Asia s'�tait, selon elle, engag� � injecter 36 millions d'euros dans l'entreprise, de quoi la faire tourner jusqu'� ce que les ventes d�collent. En juillet, Mme Boss s'�tait montr�e tr�s optimiste, assurant � l'AFP avoir "deux � cinq v�hicules command�s par jour", esp�rant en vendre "200 par mois" d�s 2014, et atteindre la rentabilit� cette ann�e-l�.
Michelle Boos comptait r�duire les co�ts de 35% en ren�gociant les contrats avec les fournisseurs, vendre des services d'ing�nierie et faire venir de nouveaux investisseurs.
Mais, d�s septembre, le paiement des salaires est devenu al�atoire. Les employ�s ont �t� pay�s en quatre fois pour leur salaire de septembre, en cinq pour octobre, et mi-d�cembre pour celui de novembre. Mardi soir, encore quatre salari�s sur dix attendaient leur paye de janvier, selon une source syndicale.
Un march� de l'�lectrique en hausse
Pourtant, sous l'impulsion d'entreprises et collectivit�s locales, le march� fran�ais s'est envol� en 2013: +55% avec 8.779 unit�s vendues selon l'Association nationale pour le d�veloppement de la mobilit� �lectrique.
Selon syndicats et direction, le carnet de commandes comptait mi-janvier 274 voitures command�es et factur�es, mais la situation s'est tendue car les fournisseurs refusent de livrer les pi�ces n�cessaires � la production, faute de paiement de leurs factures.
Selon le syndicaliste CFE-CGC, Christophe Klein, depuis la mi-d�cembre, aucune voiture n'est sortie de la cha�ne de montage. "On ne vend plus de voitures depuis deux mois", a-t-il d�plor� mercredi: "Je n'ai jamais connu �a. C'est tr�s dur � vivre et tr�s �trange" a-t-il ajout�, pr�cisant n'avoir aucune information pr�cise sur les fonds r�ellement inject�s dans l'entreprise, ni m�me l'origine des sommes ayant permis le versement des salaires.
Le syndicaliste a toutefois pr�cis� que les salari�s souhaitaient le maintien de la Franco-cor�enne Michelle Boos � la t�te de la soci�t�, esp�rant qu'elle pourrait amener, comme promis, des commandes internationales.
Mi-janvier, Michelle Boos avait d�clar� qu'en 2013, le chiffre d'affaires avait atteint "8,5 millions d'euros" gr�ce � des contrats de licence de commercialisation en Cor�e du Sud et au Mexique. Elle n'�tait pas joignable mercredi apr�s-midi.
"Nous sommes en attente d'une action du gouvernement qui devrait agir plus vite et plus fort pour les v�hicules �lectriques", a pour sa part souhait� S�gol�ne Royal en assurant que des d�marches �taient aussi en cours "pour trouver de nouveaux investisseurs".
(Avec AFP)
