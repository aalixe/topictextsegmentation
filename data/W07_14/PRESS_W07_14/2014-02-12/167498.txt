TITRE: George Clooney gagne un voyage officiel en Grèce
DATE: 2014-02-12
URL: http://www.lefigaro.fr/culture/2014/02/12/03004-20140212ARTFIG00224-george-clooney-gagne-un-voyage-officiel-en-grece.php
PRINCIPAL: 0
TEXT:
Publié
le 12/02/2014 à 15:24
Le ministre de la Culture grec a invité George Clooney à passer «quelques jours en Grèce». Crédits photo : Giuseppe Aresu/ASSOCIATED PRESS
Répondant à une journaliste grecque lors de la Berlinale, l'acteur a estimé que ce serait «une bonne idée» de rendre à Athènes les �uvres de l'Antiquité éparpillées en Europe. Reconnaissant, le ministre de la Culture l'a alors invité à venir visiter l'Acropole.
Publicité
George Clooney ne s'attendait probablement pas à faire la une des journaux grecs sur ce sujet. En répondant poliment, à Berlin, à la question d'une journaliste sur le rapatriement des marbres du Parthénon, l'acteur a gagné la reconnaissance de tout un peuple ainsi qu'un voyage officiel au pays de l'Acropole. Il présentait samedi à la Berlinale son dernier film, Monuments Men , sur un sujet davantage en résonance avec l'histoire allemande qu'avec l'Antiquité grecque: les vols d'�uvres d'art par les nazis et le travail des alliés pour les récupérer.
C'était sans compter sur le patriotisme d'une journaliste grecque. Elle a saisi l'occasion pour demander à l'acteur et réalisateur s'il pensait que les antiquités grecques exposées dans les musées étrangers - et notamment les frises du Parthénon conservées au British Museum - devaient être rendues à Athènes. «Oui, ce serait une bonne idée, vous avez le droit de votre côté», a simplement réagi Clooney, selon la presse. Par cette réponse de moins de quinze mots, la star hollywoodienne s'est attiré un élan de reconnaissance au pied de l'Acropole, illustré par des pages entières voire des unes, consacrées par la presse grecque à cette déclaration.
«Un grand merci» à George Clooney
La gratitude nationale a culminé lundi soir avec une lettre du ministère de la Culture, transmise à la presse, invitant officiellement l'acteur à venir passer «quelques jours en Grèce». «Au nom de tous les Grecs, je vous adresse un grand merci pour votre déclaration», a écrit Panos Panagiotopoulos. «J'espère que vous accepterez cette invitation à passer quelques jours en Grèce. Pour voir une multitude d'antiquités grecques conservées sous le soleil méditerranéen. Et, bien sûr, visiter le nouveau musée de l'Acropole, en face du rocher sacré, où une place attend le retour des marbres du Parthénon en exil involontaire.» La Grèce bataille depuis des décennies pour le retour de la frise longue de 75 m, emportée en 1803 par un diplomate britannique auprès de l'empire ottoman, Lord Elgin. George Clooney volera-t-il au secours des Grecs?
