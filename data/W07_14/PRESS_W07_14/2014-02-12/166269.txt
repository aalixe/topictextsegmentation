TITRE: VIDÉO. Des milliers de couples se disent oui lors d'un mariage collectif de la secte Moon | Site mobile Le Point
DATE: 2014-02-12
URL: http://www.lepoint.fr/insolite/des-milliers-de-couples-se-disent-oui-lors-d-un-mariage-collectif-de-la-secte-moon-12-02-2014-1790855_48.php
PRINCIPAL: 0
TEXT:
AFP - Publié le
12/02/14 à 09h55
VIDÉO. Des milliers de couples se disent oui lors d'un mariage collectif de la secte Moon
2 500 couples mariés en une seule cérémonie. Pour l'Église de l'unification, cet exploit n'a rien d'inhabituel : le mouvement, plus connu en France sous le nom de secte Moon, pratique les mariages en commun depuis plus d'un demi-siècle.
Des milliers de membres de l'Eglise de l'unification, connue sous le nom de secte Moon, se sont unis mercredi en Corée du Sud lors d'une cérémonie de mariages collectifs, la deuxième de ce type depuis la mort du fondateur de cette organisation.
Quelque 2.500 couples, les hommes vêtus d'un costume noir identique les uns aux autres, et les femmes en longue robe blanche avec pour beaucoup un voile sur la tête, se sont unis lors de cette cérémonie qui se déroulait dans les locaux de l'organisation, à Gapyeong, à l'est de Séoul.
Plusieurs couples n'avaient fait connaissance que quelques jours auparavant. Les jeunes marié(e)s étaient sud-coréens mais aussi étrangers, dont plusieurs Occidentaux.
Ces unions collectives, dont certaines se déroulaient dans des stades sportifs et rassemblaient des dizaines de milliers de participants, sont traditionnels au sein de ce mouvement religieux que ses critiques associent à une secte.
Sun Myung Moon, le fondateur de l'organisation, est mort en septembre 2012 à l'âge de 92 ans. C'est sa veuve, Hak Ja Han, 71 ans, qui présidait la cérémonie de mercredi.
Mariages arrangés par Moon
En 1997, Moon avait uni 30.000 couples à Washington. Deux ans plus tard, 21.000 couples emplissaient le stade olympique de Séoul.
La plupart de ces mariages étaient arrangés par Moon, qui professait que l'amour romantique conduisait au libertinage, aux couples mal assortis et à une société malade.
Beaucoup s'unissaient quelques heures à peine après s'être vu pour la première fois. Moon avait une prédilection pour les unions multi-culturelles, ce qui signifiait que les nouveaux conjoints pouvaient ne pas parler la même langue.
Ces dernières années, les couples sont formés par les parents des jeunes gens. Mais certains ont préféré s'en remettre aux choix de l'organisation. Les fiancés doivent alors jurer sous serment qu'ils sont vierges et ne pas consommer leur union avant 40 jours.
Kim Jeong-Rae, jeune marié sud-coréen, reconnait que la communication avec son épouse, philippine et anglophone, est pour le moment très limitée. "Je ne sais dire que quelques mots d'anglais (...). Je l'écoute et je m'exprime bribe par bribe", dit-il.
L'Eglise de l'Unification est qualifiée par ses détracteurs de secte pratiquant le lavage de cerveau sur ses adeptes. Elle revendique trois millions de fidèles, mais les experts estiment que leur nombre est bien plus faible.
REGARDEZ la vidéo du mariage géant au sein de la secte Moon :
