TITRE: Finist�re: le cadavre d'un nouveau-n� d�couvert dans un jardin - BFMTV.com
DATE: 2014-02-12
URL: http://www.bfmtv.com/societe/finistere-cadavre-dun-nouveau-ne-decouvert-un-jardin-709378.html
PRINCIPAL: 168481
TEXT:
Le corps sans vie d'un nourrisson de sexe masculin a �t� retrouv� mercredi dans le jardin d'un maison d'Audierne, a indiqu� le parquet de Quimper.
�� �
Le corps, envelopp� dans un sac plastique, a �t� d�couvert par un voisin dans le jardin d'une r�sidence secondaire inoccup�e dans un quartier r�sidentiel de la ville.
�� �
Une autopsie sera pratiqu�e jeudi. Le d�c�s remonterait a quelques jours, et le corps aurait �t� abandonn� quelques heures apr�s la naissance. La gendarmerie a lanc� un appel a t�moins.
