TITRE: "La Belle et la B�te" : "aucune comparaison avec le film de Cocteau" selon Vincent Cassel - Cin�ma - MYTF1News
DATE: 2014-02-12
URL: http://lci.tf1.fr/cinema/news/vincent-cassel-personne-ne-peut-rivaliser-christophe-gans-sur-8363992.html
PRINCIPAL: 166420
TEXT:
vincent cassel
News Cin�-S�ries INTERVIEW. Vincent Cassel joue la B�te dans la nouvelle version de "La belle et la b�te", r�alis�e par Christophe Gans, des ann�es apr�s Jean Marais chez Cocteau, ce mercredi dans les salles.
Vincent Cassel joue la b�te dans la nouvelle adaptation du conte de Jeanne-Marie Leprince de Beaumont, r�alis�e par Christophe Gans, coproduite par Path� et Eskwad. L'histoire de Belle (Lea Seydoux), fille d'un marchand tyrannis�e par ses deux s�urs, qui va tomber amoureuse d'un monstre reclus dans son ch�teau.
Christophe Gans retrouve Vincent Cassel onze ans apr�s "Le pacte des loups". L'acteur, confirmant l'inspiration f��rique de Hayao Miyazaki, assure : "Le tournage de "La belle et la b�te" a �t� une exp�rience exceptionnelle. Je n'avais encore jamais travaill� avec un corps de b�te o� je suis cens� faire 400 kilos. Apprendre � donner vie � ce personnage, c'�tait tr�s int�ressant. En tant qu'acteur, je ne cherche pas forc�ment le d�fi. Je me retrouve dans des situations o� � chaque fois, il y a quelque chose � d�fendre. Ce qui m'int�resse, c'est comment je vais y arriver." Rencontre.
MYTF1News : Que ce soit chez Cocteau ou chez Gans, "La belle et la b�te" fait appel au merveilleux. Quels films vous ont �merveill�, enfant?
Vincent Cassel : A sept ans, mon p�re m'avait emmen� au Kino Panorama, le seul grand cin�ma de l'�poque � Paris, et gr�ce � lui j'ai d�couvert deux films : Orfeu Negro, la descente d'Orph�e aux enfers transpos�e au carnaval de Rio - �a m'avait tellement marqu� que je me suis d�sormais install� � Rio - et West Side Story. Deux claques. Il y avait quelques points communs entre les deux films : beaucoup de musique et la m�me id�e du conte transpos� dans des univers inhabituels avec la r�f�rence � Romeo et Juliette pour West Side Story.
MYTF1News : Comment avez-vous d�couvert l'univers de Jean Cocteau ?
Vincent Cassel : Je l'ai d�couvert avec La belle et la b�te lorsque j'�tais enfant. A l'�poque, �a m'avait impressionn�. Des ann�es plus tard, j'ai achet� le Laserdisc Criterion. Et quand je l'ai revu, je me suis endormi. Il y avait une po�sie mais que je trouve d�suete aujourd'hui. Beaucoup de gens vont faire la comparaison entre les deux versions, mais � tort, je pense. Et pour �tre franc, je trouve la version de Cocteau vraiment longue...
MYTF1News : Christophe Gans est r�put� pour sa cin�philie.
Vincent Cassel : Oui, il est biberonn� aux films de genre, poss�de une vraie culture et c'est d'ailleurs ce qui me plait chez lui. On voit bien toutes les r�f�rences � Miyazaki : Princesse Mononok�, Le ch�teau dans le ciel... Mais il ne se cache pas�derri�re une cin�philie vorace. Il ne va pas me dire ce que je dois voir. Ce n'est pas un int�griste. Cela lui arrive de me vanner sur certains films, comme Dobermann de Jan Kounen (1996). Il me r�p�te qu'il trouve �a atroce depuis je ne sais plus combien de temps. Tout en convenant, comme moi, que �a avait le m�rite d'exister � l'�poque. Vous savez, c'est un ancien critique, il a des avis tranch�s, comme tous les critiques qui ne se sentent pas respect�s. Autrement, Christophe est le seul capable d'univers d�mesur�s dans ses films. Et je ne pense pas que demain il puisse faire un film d'auteur fran�ais. Honn�tement, je vois pas qui d'autre est capable de �a en France. Il y a bien eu Pitof avec Vidocq (2001) mais bon... son film a vieilli encore plus vite que La belle et la b�te de Cocteau.
Propos recueillis par Romain Le Vern
Commenter cet article
Vous devez �crire un avis
jeanpft : Effectivement, rien a voir...
Le 12/02/2014 � 10h50
