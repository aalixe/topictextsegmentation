TITRE: Obama et Hollande passent � table et trinquent � l'amiti� franco-am�ricaine
DATE: 2014-02-12
URL: http://www.francetvinfo.fr/monde/ameriques/hollande-aux-etats-unis/obama-et-hollande-passent-a-table-et-trinquent-a-l-amitie-franco-americaine_527819.html
PRINCIPAL: 0
TEXT:
Tweeter
Obama et Hollande passent � table et trinquent � l'amiti� franco-am�ricaine
Le pr�sident fran�ais �tait accueilli � la Maison Blanche pour un fastueux d�ner d'Etat, pour lequel 300 personnes avaient �t� rassembl�es sous une vaste tente dress�e dans les jardins.
Les pr�sidents Barack Obama et Fran�ois Hollande trinquent lors d'un d�ner d'Etat organis� � la Maison Blanche, � Washington (Etats-Unis), le 11 f�vrier 2014. (BRENDAN SMIALOWSKI / AFP)
Par Francetv info avec AFP
Mis � jour le
, publi� le
12/02/2014 | 06:09
Les probl�mes de protocole ont finalement �t� r�solus. Fran�ois Hollande est arriv� seul � la Maison Blanche mardi 11 f�vrier, chaleureusement accueilli par Barack et Michelle Obama pour un fastueux d�ner d'Etat , au deuxi�me jour de sa visite outre-Atlantique .
Apr�s avoir pris la pose sur le parvis nord, le pr�sident fran�ais et le couple Obama se sont dirig�s vers les salons de la Maison Blanche, avant de rejoindre les 300 invit�s sous une vaste tente dress�e dans les jardins.�
(MARYSE BURGOT - LAURENT DESBOIS / FRANCE 2)
A la droite d'Obama, place habituellement d�volue aux Premi�res dames des pays invit�s, se tenait Thelma Golden, directrice d'un mus�e d'art contemporain � New York.
Echanges d'amabilit�s lors des toasts
Les deux chefs d'Etat se sont pr�t�s � quelques plaisanteries lors de leurs toasts. Barack Obama a ri de "la multitude de choses que les Am�ricains sont capables de se fourrer dans l'estomac". Dans un sourire, il a remarqu� que ses concitoyens aimaient tout ce qui est fran�ais, "les films, la nourriture, le vin... surtout le vin". Mais "avant tout, nous aimons nos amis fran�ais parce qu'ils sont rest�s � nos c�t�s pour notre libert� pendant plus de deux cents ans".
Fran�ois Hollande accueilli par Barack et Michelle Obama � la Maison Blanche, � Washington (Etats-Unis), dans le cadre d'un d�ner d'Etat, le 11 f�vrier 2014. (KEVIN LAMARQUE / REUTERS)
En r�ponse, Fran�ois Hollande a une nouvelle fois soulign� que la France et les Etats-Unis "partagent des valeurs universelles mais aussi de grands sentiments l'un pour l'autre". "Vous aimez la France, vous ne le dites pas toujours parce que vous �tes timides, les Am�ricains, vous vous retenez, a-t-il plaisant�, mais nous aimons les Etats-Unis". Des toasts dans la lign�e des �changes d'amabilit�s dont rivalisent les deux pr�sidents depuis le d�but de la visite.
Mary J. Blige pour conclure
Parmi les invit�s figuraient de nombreux hommes politiques et chefs d'entreprise fran�ais et am�ricains, mais aussi quelques c�l�brit�s. L'acteur francophile et francophone Bradley Cooper �tait ainsi de la partie, tout comme le com�dien Stephen Colbert, habitu� � railler la France dans certains de ses sketchs.
La soir�e s'est conclue par un petit show de la chanteuse Mary J. Blige, �galement convi�e � la soir�e. Si Michelle Obama s'est lev�e pendant ce mini-spectacle, Barack Obama et Fran�ois Hollande ont pr�f�r� rester sagement assis, comme le montre cette courte vid�o diffus�e par un invit�.
Yeah �a ne finit pas forc�ment comme un State diner "normal" ! ;-) #obama #hollande #prusa https://t.co/vv8SxuqC3P
� Beno�t Thieulin (@thieulin) February 12, 2014
"J'ai v�cu beaucoup de choses dans ma vie,�mais un d�ner d'Etat avec l'Elys�e et la Maison Blanche qui dansent sur Mary J. Blige, c'est quelque chose", a comment� la journaliste fran�aise�Laurence Ha�m, invit�e du repas, sur son compte Twitter .
