TITRE: Le vote blanc adopté par le Parlement
DATE: 2014-02-12
URL: http://www.lefigaro.fr/flash-actu/2014/02/12/97001-20140212FILWWW00259-le-vote-blanc-adopte-par-le-parlement.php
PRINCIPAL: 167761
TEXT:
le 12/02/2014 à 16:28
Publicité
Les électeurs pourront désormais voter blanc, le parlement ayant définitivement adopté mercredi une proposition de loi centriste en ce sens, une mesure qui entrera en vigueur après les municipales.
Les sénateurs ont voté conforme le texte déjà adopté en deuxième lecture par l'Assemblée nationale. Son adoption est donc définitive.
Cette proposition de loi, qui avait été déposée par les députés centristes, prévoit qu'à partir du 1er avril 2014 les bulletins nuls seront décomptés séparément des bulletins blancs. Les électeurs pourront donc se saisir de cette possibilité dès le prochain scrutin européen en juin. Chaque électeur pourra voter "blanc" soit en introduisant dans l'enveloppe un bulletin blanc, soit en laissant cette enveloppe vide.
Cette adoption constitue "une avancée dans la transparence de la vie démocratique répond aux attentes de nombreux Français depuis de nombreuses années", a fait valoir le rapporteur, François Zocchetto (UDI-UC). "L'absence de reconnaissance de la voix de l'électeur qui se déplace pour accomplir son devoir civique était choquante en démocratie", a-t-il ajouté. Pour Philippe Kaltenbach (PS), "si l'abstention peut généralement être comprise comme une marque de désintérêt pour la vie politique, le vote blanc doit être vu comme une attente non satisfaite qui peut traduire une forme d'espérance. C'est un choix tout aussi respectable que les autres".
"Mieux vaut voter blanc que bleu marine", a lancé de son côté l'écologiste Hélène Lipietz.
LIRE AUSSI:
