TITRE: JO/Ski: deux reines, Maze et Gisin, au bout d'une descente palpitante - 12 février 2014 - Le Nouvel Observateur
DATE: 2014-02-12
URL: http://tempsreel.nouvelobs.com/topnews/20140212.AFP9871/jo-ski-la-suissesse-gisin-et-la-slovene-maze-championnes-de-descente.html
PRINCIPAL: 0
TEXT:
Actualité > TopNews > JO/Ski: deux reines, Maze et Gisin, au bout d'une descente palpitante
JO/Ski: deux reines, Maze et Gisin, au bout d'une descente palpitante
Publié le 12-02-2014 à 09h40
Mis à jour à 16h20
A+ A-
La Suissesse Dominique Gisin et la Slovène Tina Maze se sont parées toutes deux de l'or olympique de la descente mercredi aux Jeux de Sotchi, une première dans l'histoire du ski alpin. (c) Afp
Rosa Khoutor (Russie) (AFP) - La Suissesse Dominique Gisin et la Slovène Tina Maze se sont parées toutes deux de l'or olympique de la descente mercredi aux Jeux de Sotchi, pour une grande première dans l'histoire du ski alpin.
Les deux dames, de la même génération et au physique similaire, sont montées main dans la main sur le podium, se sachant liées à jamais au palmarès olympique alors qu'elles n'avaient pas grand chose en commun jusqu'à présent, mis à part leur passion pour la glisse.
Car l'une est issue d'un pays d'Europe centrale qui n'avait jamais obtenu le moindre titre olympique aux Jeux d'hiver. Tandis que l'autre porte les couleurs d'une des plus grandes nations du ski alpin.
Mais si Tina Maze passait pour l'une des grandes prétendantes au titre, elle qui avait dominé outrageusement la saison de Coupe du monde 2013, Dominique Gisin faisait figure de surprise potentielle.
La Suissesse avait obtenu sa place dans le portillon de départ que la semaine dernière, au vu de ses bons résultats à l'entraînement. Comme son compatriote Didier Défago, aux Jeux de Vancouver, qui s'était lui aussi adjugé l'or de l'épreuve reine quelques jours après sa sélection.
- Gut en bronze -
La carrière de Gisin était marquée plus par les opérations aux genoux, une bonne dizaine, que par les victoires, trois seulement en Coupe du monde.
C'est plutôt Lara Gut, qui passait pour le principal espoir de médaille de l'équipe suisse. Mais la demoiselle, 22 ans, se mordait les doigts d'avoir laisser filer 10 centièmes sur cette piste olympique, qui la laissait avec la médaille de bronze.
Dominique Gisin a su profiter pleinement de la chance que lui offrait un bon dossard, le N.8, pour signer le temps de référence en 1 min 41 et 57/100e que seule Tina Maze, avec le N.21, est parvenue à égaler.
La couronne olympique de la descente qu'avait mise aux enchères la star du ski alpin féminin, l'Américaine Lindsey Vonn, avec son forfait pour les Jeux, a donc trouvé non pas une mais deux preneuses au terme d'une course palpitante.
Julia Mancuso, sa dauphine pensait en profiter, elle qui avait dominé la manche de vitesse lundi du super-combiné, mais la croqueuse de médailles a été privée cette fois du festin (8e, à près d'une seconde).
Gisin et Maze illuminent leur carrière chacune à leur façon.
Il ne manquait plus que cela à la Slovène, déjà double médaillée d'argent aux Jeux de Vancouver et sextuple médaillée mondiale, pour finir d'écrire sa légende.
"J'ai réalisé mon rêve. Je n'avais pas d'autre but que remporter l'or olympique de la descente", a souligné Maze.
'Un grand honneur'
Si l'an dernier elle avait battu pas mal de records en Coupe du monde, dont celui du nombre de points amassés en une saison (2414) et du nombre de podiums (24), la fonceuse slovène avait laissé les premiers rôles à d'autres cet hiver, avec une seule victoire sur le circuit.
"C'est un grand honneur de partager l'or avec elle. C'est une telle skieuse ! J'ai partagé ma première victoire en Coupe du monde avec Anja Paerson, alors je suis ravie de partager ce titre avec Tina", a fait valoir la Suissesse, en faisant référence à l'ogresse suédoise qui dominait le ski alpin dans les années 2000.
Dominique Gisin peut être fière: elle la première Suissesse couronnée en descente depuis le sacre de la grande Michaëla Figini à Sarajevo en 1984.
L'Italienne Daniela Merighetti pouvait se frapper la tête dans la neige, elle qui a fait une grande partie de la course en tête avant de perdre de vue le podium sur un saut et prendre la 4e place (+27/100e).
Chasse à l'or oblige, les coureuses ont poussé les risques à la limite, au risque de chuter. Marie Marchand-Arvier, la seule Française en lice, a ainsi fini sa course dans le filets.
Partager
