TITRE: Chine. S�isme de magnitude 6,8
DATE: 2014-02-12
URL: http://www.ouest-france.fr/chine-seisme-de-magnitude-68-1925273
PRINCIPAL: 0
TEXT:
Chine. S�isme de magnitude 6,8
Chine -
12 F�vrier
Un tremblement de terre de magnitude 6,8 a frapp� mercredi la r�gion du Xinjiang, en Chine, a annonc� l'institut am�ricain de g�ophysique.�|�Epa/MaxPPP
Facebook
Achetez votre journal num�rique
Un tremblement de terre de magnitude 6,8 a frapp� mercredi la r�gion du Xinjiang, en Chine, a annonc� l'institut am�ricain de g�ophysique.
D'abord �valu�e � 7, la magnitude du s�isme a �t� ensuite annonc�e � 7,3 par les m�dias et les autorit�s chinoises.�
La secousse tellurique a �branl� une zone recul�e du Xinjiang, immense r�gion en partie d�ertique situ�e aux confins occidentaux de la Chine.�
L'�picentre du s�isme a �t� localis� � 270 kilom�tres de la ville de Hotan, selon l'USGS.�
Les r�gions montagneuses de l'ouest de la Chine sont r�guli�rement touch�es par des tremblements de terre.�
Le Sichuan, l'une des provinces les plus peupl�es de Chine avec 80 millions d'habitants, avait �t� endeuill� en mai 2008 par un tremblement de terre d�vastateur qui avait fait 87 000 morts et disparus.
Lire aussi
