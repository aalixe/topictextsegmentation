TITRE: Int�gration : "Ce que Thierry Mariani appelle le pire, c'est les �trangers", dit Vallaud-Belkacem - RTL.fr
DATE: 2014-02-12
URL: http://www.rtl.fr/actualites/info/article/integration-ce-que-thierry-mariani-appelle-le-pire-c-est-les-etrangers-dit-vallaud-belkacem-7769662638
PRINCIPAL: 166088
TEXT:
Najat Vallaud-Belkacem, invit�e de RTL, mercredi 12 f�vrier 2014
Cr�dit : Nicolas Briquet / Abaca / RTL.fr
INVIT�E RTL - La porte-parole du gouvernement est revenu sur la feuille de route pr�sent�e par Jean-Marc Ayrault mardi 11 f�vrier pour l'int�gration des immigr�s et la lutte contre les discriminations.
Vallaud-Belkacem "extr�mement choqu�e" par le "comportement" de Cop�
Cr�dit : RTL
Najat Vallaud-Belkacem �tait mardi 11 f�vrier � Matignon o� Jean-Marc Ayrault a plaid� pour une int�gration "apais�e" en pr�sentant sa feuille de route , un plan a minima en faveur des immigr�s et de leurs descendants. En r�ponse � Thierry Mariani, qui a d�clar� mardi soir que "le pire a �t� �vit�" , Najat Vallaud-Belkacem estime que "ce que M. Mariani, appelle le pire, c'est les �trangers tout court".
Pour la ministre des Droits des femmes, il s'agit de "la reprise d'un travail s�rieux et continu sur la question de l'Int�gration, interrompu par la cr�ation inutile d'un minist�re de l'Identit� nationale". "On reprend ce travail interminist�riel, dans lequel les �trangers ne sont pas stigmatis�s mais au contraire des sujets de politique transversale et de droit commun", affirme-t-elle au micro de RTL.
Les discriminations au travail � l'ordre du jour des partenaires sociaux en juillet
Le suivi de ce plan, qui comporte 28 mesures, dont certaines �taient d�j� connues, sera assur� par un d�l�gu� interminist�riel plac� sous l'autorit� du Premier ministre. Parmi les mesures figurent notamment le renforcement de l'apprentissage du fran�ais pour les primo-arrivants. Des mesures "pour mieux accompagner les �trangers lorsqu'ils arrivent sur le territoire", insiste la porte-parole du gouvernement. "Pendant 5 ans, ils vont avoir droit � un accompagnement � l'apprentissage de la langue fran�aise qui va leur permettre d'obtenir des dipl�mes extr�mement pr�cieux ensuite pour s'ins�rer par le travail", continue-t-elle.
�
"Il est pr�vu qu'� la grande conf�rence sociale de juillet prochain nous mettions � l'ordre du jour des partenaires sociaux les actions collectives en mati�re de discriminations, ce qui va permettre lorsque les �trangers se sentent discrimin�s sur le march� du travail de pouvoir porter ensemble les actions en Justice", poursuit la ministre, ajoutant que le gouvernement envisageait s�rieusement de d�ployer "les recrutements par simulation ou sans CV" pour les personnes "qui ne pratiquent pas bien le fran�ais ou qui peuvent �tre victimes de discrimination li�es � leur origine ou � leur nom".
La r�daction vous recommande
