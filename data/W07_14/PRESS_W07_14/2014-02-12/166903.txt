TITRE: Star Wars Rebels pr�sente son h�ros, Kanan | SyFantasy.fr
DATE: 2014-02-12
URL: http://www.syfantasy.fr/18054-Star_Wars_Rebels_presente_son_heros_Kanan
PRINCIPAL: 166902
TEXT:
Star Wars Rebels Serie-tv�2014
partager cet article Tweet
Apr�s avoir d�voil� Chopper , la caution dro�de de la s�rie, Star Wars Rebels revient aujourd'hui pour pr�senter son h�ros et le commandant du Ghost, Kanan.
C'est � travers une vid�o making-of d�voil�e par USA Today que les �quipes de Dave Filoni, qui travaillent d'arrache-pied sur la s�rie TV Star Wars Rebels, ont d�cid� de pr�senter le premier h�ros dont nous suivrons les aventures alors que l'Alliance rebelle vit ses premi�re heures pour combattre Dark Vador et l'Empereur Palpatine.
Kanan, doubl� par Freddie Prinze Jr. , est un ancien chevalier Jedi qui a d�cid� de laisser son sabre laser de c�t� apr�s la chute de la R�publique mais qui sera oblig� rapidement oblig� de reprendre ses vieilles habitudes pour le bien de la galaxie. Dave Filoni l'a surnomm� le "Jedi Cowboy" et a annonc� qu'il sera un personnage tr�s pr�tentieux, plus proche de Han Solo que de Luke Skywalker.
Star Wars Rebels est pr�vue pour cet �t� sur Disney Channel.
Galerie Photo
