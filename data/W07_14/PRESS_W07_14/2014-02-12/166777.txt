TITRE: USA: accus� du meurtre d'un ado qui "�coutait la musique trop fort" - La Libre.be
DATE: 2014-02-12
URL: http://www.lalibre.be/actu/international/usa-accuse-du-meurtre-d-un-ado-qui-ecoutait-la-musique-trop-fort-52fb060a3570516ba0b95e15
PRINCIPAL: 166771
TEXT:
Les "anti-Hollande" poursuivent leur ennemi jusqu'� la Maison Blanche
International
Un habitant de Floride accus� d'avoir tu� un adolescent noir � qui il reprochait d'�couter de la musique trop fort dans sa voiture, a plaid� mardi la l�gitime d�fense, comme le meurtrier de Trayvon Martin lors de son proc�s.
Le proc�s de Michael Dunn, un homme blanc de 47 ans, rappelle celui de George Zimmerman, acquitt� du meurtre de cet adolescent de 17 ans, sur lequel il avait tir� lors d'une altercation en f�vrier 2012 alors qu'il effectuait une ronde de surveillance dans son quartier, �galement en Floride.
L'affaire avait fait grand bruit dans le pays, Barack Obama affirmant m�me qu'il aurait pu �tre Trayvon Martin et regrettant la persistance de pr�jug�s racistes aux Etats-Unis.
Mardi, Michael Dunn a expliqu� devant un jury qu'il avait demand� � des adolescents noirs de baisser la musique de leur voiture stationn�e � une station service et que craignant pour sa vie, il avait ouvert le feu sur le v�hicule. "Je me suis dit 'Mon Dieu, pourquoi tant d'hostilit�?' ", a-t-il dit, affirmant avoir vu un des passagers s'emparer de quelque chose ressemblant � un pistolet.
La police n'a trouv� aucune trace d'arme dans la voiture et les trois autres adolescents survivants ont affirm� n'avoir jamais menac� M. Dunn.
L'accus� a insist� sur le fait qu'il "avait �t� pris de panique" lorsque l'un des jeunes �tait sorti de la voiture et que c'�tait pour cette raison qu'il avait d�gain� son pistolet de la bo�te � gant.
"Je me suis dit, c'est le moment o� il vient pour me tuer, pour me battre", a-t-il d�clar�. "Il a clairement fait comprendre ses intentions".
Il a affirm� avoir continu� � tirer sur la voiture qui s'�loignait car il avait peur pour sa fianc�e et pour lui-m�me. Il a expliqu� ensuite �tre parti de la station service de peur que les adolescents ne reviennent.
Il ignorait avoir tu� Jordan Davis jusqu'� ce que, de retour � l'h�tel, il ne d�couvre la mort du jeune homme par le biais d'une alerte m�dias sur son t�l�phone. Il s'est justifi� de n'avoir pas contact� imm�diatement la police car il voulait d'abord avoir les conseils d'un de ses voisins sur une affaire, qui � ses yeux, ne pouvait �tre que de la l�gitime d�fense.
De retour � l'h�tel, il avait command� une pizza et une boisson, pour calmer sa fianc�e, s'est-il justifi�.
Ce proc�s met de nouveau en lumi�re la loi particuli�rement laxiste en mati�re d'armes � feu en Floride.
"Quand vous avez le droit de porter une arme partout et que vous perdez votre calme, le risque est qu'une banale altercation se transforme en un homicide", a not� Kristen Rand, partisane d'un contr�le plus strict de la l�gislation des armes � feu et directrice juridique du Violence Policy Center.
"La Floride semble avoir le plus de probl�mes en la mati�re mais ce genre de choses se produit partout dans le pays", a-t-elle encore soulign�.
La proportion des m�nages am�ricains propri�taires d'armes � feu est tomb�e � 37%, contre 50% dans les ann�es 1960, selon un sondage Gallup. Un d�clin dans les armes de chasse et une urbanisation plus importante ont contribu� � ce ph�nom�ne.
Cependant, les propri�taires d'armes � feu en poss�dent en g�n�ral plusieurs. Des estimations �valuent le nombre d'armes � feu en circulation aux Etats-Unis entre 270 et 310 millions, pour une population d'environ 315 millions d'habitants.
Les armes � feu tuent 30.000 personnes chaque ann�e aux Etats-Unis, dont 20.000 suicides, et en blessent en moyenne 81.000.
Sur le m�me sujet :
