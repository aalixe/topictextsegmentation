TITRE: France: d�faillances d'entreprises en hausse de 5,3% /Coface - Le Figaro Bourse
DATE: 2014-02-12
URL: http://bourse.lefigaro.fr/indices-actions/actu-conseils/france-defaillances-d-entreprises-en-hausse-de-5-3-coface-980841
PRINCIPAL: 167233
TEXT:
France: d�faillances d'entreprises en hausse de 5,3% /Coface
Cercle Finance | Publi� le 12/02/2014 � 12:03 | Mise � jour le 12/02/2014 � 12:03 | R�actions (0)
En 2013, les d�faillances d'entreprises fran�aises ont d�pass� le pic enregistr� en 2009, selon Coface.
En un an, s'est produite une augmentation de 5,3% du nombre de d�faillances et de 10,4% de leur co�t financier.
Ainsi, au total, 63.452 entreprises fran�aises ont �t� touch�es en 2013.
Le co�t financier associ� aux d�faillances atteint 4,82 milliards d'euros.
Pour 2014, Coface anticipe une stabilisation des d�faillances en France � un niveau �lev�. Le mois de janvier donne de timides signes d'optimisme, avec une baisse du nombre de d�faillances de -7,7% et de leur co�t de -6% par rapport � janvier 2013.
Mais pour que les d�faillances baissent de mani�re significative, il faudrait une croissance d'au moins 1,6%. Or cette ann�e, la croissance fran�aise n'atteindra pas ce rythme (pr�vue � +0,6% par Coface), contrairement � d'autres �conomies avanc�es.
La situation est plus favorable pour les entreprises en Allemagne et aux Etats-Unis, o� le minimum de croissance requis (1,7% et 2,4% respectivement) devrait �tre atteint cette ann�e pour permettre une am�lioration notable sur le front des d�faillances.
Copyright (c) 2014 CercleFinance.com. Tous droits r�serv�s.
