TITRE: Pacte de responsabilit� : le psychodrame entre le patron du Medef et Hollande en quatre actes
DATE: 2014-02-12
URL: http://www.francetvinfo.fr/politique/pacte-de-responsabilite/pacte-de-responsabilite-gattaz-n-exclut-pas-des-engagements-chiffres-sur-des-objectifs_528609.html
PRINCIPAL: 168371
TEXT:
Tweeter
Pacte de responsabilit� : le psychodrame entre le patron du Medef et Hollande en quatre actes
En pleine visite d'Etat du pr�sident fran�ais aux Etats-Unis, Pierre Gattaz a critiqu� l'id�e d'une contrepartie en �change de 30 milliards d'all�gements de charges pour les entreprises. Avant de reculer, recadr� par l'ex�cutif.�
Le pr�sident fran�ais, Fran�ois Hollande, et le patron du Medef, Pierre Gattaz, � l'Elys�e, � Paris, le 21 janvier 2014.� (PHILIPPE WOJAZER / AP / SIPA)
Par Francetv info avec AFP
Mis � jour le
, publi� le
12/02/2014 | 20:49
Les discussions entre le gouvernement et le patronat autour du pacte de responsabilit� vont-elles s'apaiser ? Alors qu'il se trouvait dans la d�l�gation fran�aise aux c�t�s de Fran�ois Hollande aux Etats-Unis, le patron des patrons, Pierre Gattaz, a affirm� qu'il refusait que le patronat s'engage � cr�er des emplois en �change de 30 milliards d'all�gements de charges pour les entreprises. Le chef de l'Etat et le gouvernement l'ont vertement recadr�. Si bien que Pierre Gattaz a fini par reculer. Mercredi 12 f�vrier, le pr�sident du Medef a �voqu� la possibilit� d'"engagements chiffr�s".�
Retour sur ce psychodrame en quatre actes, en pleine� visite d'Etat�de Fran�ois Hollande aux Etats-Unis .
Acte 1 : la sortie de Pierre Gattaz
Au premier jour de la visite de Fran�ois Hollande aux Etats-Unis, plac�e sous le signe de l'offensive �conomique, le pr�sident du Medef s�me le trouble . Il r�p�te la position affich�e en France par le patronat : les all�gements de charges promis par l'ex�cutif aux entreprises ne doivent pas s'accompagner de "contrainte" pour les entreprises.
"Il faut arr�ter de g�rer par la contrainte. Aujourd'hui, quand on parle de contreparties, j'entends aussi des gens qui disent : 'on va vous obliger, vous contraindre, vous mettre des p�nalit�s, si vous ne le faites pas, on va vous punir'", l�che-t-il.
Acte 2 : le rappel � l'ordre de l'ex�cutif
Le gouvernement go�te peu la sortie du pr�sident du patronat. D'autant plus qu'elle survient au cours d'une visite diplomatique de premi�re importance. Le patron des patrons, invit� dans la d�l�gation fran�aise, avait annul� une conf�rence de presse � Washington, � la demande de l'Elys�e. Il a n�anmoins choisi de rencontrer des journalistes et de leur faire cette confidence.
Fran�ois Hollande, suivi par son Premier ministre�et plusieurs ministres, rappelle donc Pierre Gattaz � l'ordre . "Des engagements, les entreprises doivent �galement en prendre au niveau appropri� pour cr�er de l'emploi", lance le chef de l'Etat,�mardi soir, au cours d'une conf�rence de presse � la Maison blanche. Il faut "�tre capable de mobiliser l'ensemble du pays pour un objectif", insiste le pr�sident socialiste.
Mercredi, Didier Migaud, pr�sident de la Cour des comptes, l'organisme charg� du contr�le des d�penses publiques, tente d'�teindre l'incendie en souhaitant que "chacun se retrousse les manches et ait un comportement constructif".
Acte 3 : le recul du patron des patrons
Lors du vol entre�Washington�et�San�Francisco avec�Fran�ois Hollande, mercredi, Pierre Gattaz se confie de nouveau � la presse. Et change de ton :�"Je n'exclus pas des engagements chiffr�s qui seraient des objectifs � partager sur la base d'estimations."�Il r�affirme cependant que le pacte de responsabilit� ne doit pas �tre transform� en un�"pacte de contrainte".
Pour autant, "sur les contreparties du pacte de responsabilit�", il entend rester "ferme sur les prix".�"On ne pourra pas faire n'importe quoi.�Je ne pourrai jamais prendre un engagement juridique dans un environnement instable, mondialis� et tr�s concurrentiel", fait-il valoir.
Mais pour Pierre Gattaz, le�"niveau appropri�" des engagements qu'a��voqu� le pr�sident � Washington�"est une bonne id�e". "Il y aura des engagements de mobilisation sur les apprentis et les emplois � la condition que les choses bougent et que certains verrous se d�bloquent", assure-t-il. "L'enjeu [est]�la cr�ation d'un million d'emplois nets en cinq ans auquel je crois".
Acte 4 : les piques de Fran�ois Hollande�
Malgr� ce relatif apaisement,�le chef de l'Etat a tout de m�me d�coch� quelques piques � Pierre Gattaz devant quelque 3 000 Fran�ais de San Francisco,�ironisant sur "l'avantage" de l'avoir eu au sein de sa d�l�gation pendant le s�jour. "Vous pouvez l'applaudir", a-t-il lanc�, d�cha�nant les rires de la salle. "Il sera m�me dit que j'ai fait applaudir le pr�sident du Medef", a-t-il alors plaisant� avant d'encha�ner sur le m�me registre moqueur : "Je ne doute pas qu'il me rendra la pareille le moment venu, �a fait partie du pacte de responsabilit�, je l'avoue."
(POOL)
Le prochain round entre l'ex�cutif et le Medef�doit avoir lieu le 28 f�vrier, pour les premi�res discussions sur le pacte de responsabilit� entre le patronat et des syndicats.
