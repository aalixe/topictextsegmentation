TITRE: Centrafrique : les anti-balaka mis en garde, Amnesty d�nonce un �nettoyage ethnique�
DATE: 2014-02-12
URL: http://www.leparisien.fr/international/centrafrique-le-drian-en-visite-dans-un-pays-en-proie-au-chaos-12-02-2014-3583115.php
PRINCIPAL: 0
TEXT:
Centrafrique : les anti-balaka mis en garde, Amnesty d�nonce un �nettoyage ethnique�
E.Pe, G.L |                     Publi� le 12.02.2014, 07h39                                             | Mise � jour :                                                      20h30
Tweeter
Le ministre fran�ais de la D�fense, Jean-Yves Le Drian, a durci le ton mardi � Brazzaville contre les milices qui s�vissent en Centrafrique, affirmant que les forces internationales �taient pr�tes � mettre fin aux exactions �si besoin par la force�. | (AFP/Fred Dufour.)
R�agir
A l'occasion de la visite du ministre de la D�fense Jean-Yves Le Drian en Centrafrique ce mercredi, Catherine Samba Panza a hauss� le ton contre les anti-balaka.�La pr�sidente centrafricaine de transition veut �aller en guerre� contre ces milices d'autod�fense chr�tiennes, coupables de nombreuses exactions contre la communaut� musulmane. �(Ils) pensent que parce que je suis une femme je suis faible.
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Centrafrique : l'ONU donne le feu vert � l'intervention d'une force europ�enne
Mais maintenant les anti-balaka qui voudront tuer seront traqu�s�, a ass�n� Catherine Samba Panza devant les habitants de Mba�ki (80 km au sud-ouest de Bangui).
Un avertissement qui fait �cho aux propos du ministre de la d�fense , ce mardi. Jean-Yves Le Drian, en tourn�e en Afrique centrale, s'�tait montr� ferme .��La mission (de la France , ndlr) est de faire que la s�curit� revienne, que le d�sarmement (des milices) ait lieu et se fasse de fa�on impartiale�. Les exactions des anti-balaka menacent par ailleurs le pays de partition, une issue jug�e �inacceptable� par la France .
Nettoyage ethnique et exode des musulmans
Selon Amnesty International, les soldats de la force internationale de maintien de la paix sont impuissants face au nettoyage ethnique qui se trame dans l'ouest du pays. Les civils musulmans, accus�s d'�tre complices des rebelles de la S�l�ka, sont les cibles des anti-balaka. L'ONG constate un exode musulman �sans pr�c�dent�. Les centrafricains de confession musulmane, qui composent 15% de la population -contre 50% pour les chr�tiens-, fuient notamment vers le Tchad ou le Cameroun.
D�s le d�but de l'intervention fran�aise �Sangaris�, d�but d�cembre, Peter Bouckaert, le directeur de la division Urgence de l'ONG de d�fense des droits de l'Homme Human Rights Watch,�d�clarait au Parisien que �l'arm�e fran�aise, si elle s'�tait bien pr�par�e au d�sarmement des S�l�ka , n'avait pas anticip� les attaques des anti-balaka, tr�s nombreuses. Le probl�me des anti-balakas, est qu'il s'agit d'une entit� sans structure commune et donc tr�s difficilement identifiable�.
Au total, pr�s d'un million de personnes, musulmans ou chr�tiens, sont d�plac�es ou r�fugi�es depuis le d�but du conflit, selon l'ONU. Amnesty International affirme pour sa part que��la r�ponse� de la communaut� internationale reste �trop timor�e� et note que les troupes internationales de maintien de la paix �se montrent r�ticentes � faire face aux milices anti-balaka et ne sont pas assez r�actives pour prot�ger la minorit� musulmane menac�e�.
Joanne Mariner,�conseill�re d�Amnesty International pour les situations de crise, se trouve actuellement � Bangui
Survivor of grenade attack in #Bangui 's Muslim PK 12 neighborhood - area under continual threat. #CARcrisis pic.twitter.com/sZEPJ7zhvv
� joanne mariner (@jgmariner) February 12, 2014
Lancement d'un pont a�rien entre Douala et Bangui
Face au risque de crise alimentaire qui menace les quelque 4,5 millions de Centrafricains, la filiale de l'ONU pour l'alimentation et l'agriculture (FAO) a lanc� ce mercredi un appel d'urgence pour obtenir 22,2 millions d'euros afin d'aider les agriculteurs en Centrafrique. �Des semences et outils essentiels doivent �tre livr�s d'urgence aux agriculteurs de Centrafrique en vue de la campagne des semis qui d�marrera en mars afin de conjurer une crise alimentaire et nutritionnelle g�n�ralis�e�, indique l'organisation dans un communiqu�.
Le Programme alimentaire mondial (PAM) a �galement lanc� un pont a�rien entre Douala (Cameroun) et Bangui pour acheminer des vivres pour 150 000 personnes pendant un mois. Un premier avion-cargo charg� de 80 tonnes de riz s'est pos� en d�but d'apr�s-midi � Bangui Il devrait �tre suivi de 24 rotations quotidiennes sur la capitale de la Centrafrique, o� 1,3 million de personnes, soit plus d'un quart de la population du pays, a besoin d'une assistance alimentaire.���a va nous faire un ballon d'oxyg�ne, mais �a ne r�glera pas le probl�me � terme�, a pr�venu le porte-parole du PAM, Alexis Masciarelli. �Vu l'ampleur des besoins, cette op�ration va nous permettre de mieux r�pondre � l'urgence mais pas de refaire nos stocks. Il faudrait pour cela que la route (entre la fronti�re camerounaise et Bangui, ndlr) soit rouverte de mani�re r�guli�re�, dit-il.
Here's the cargo plane that landed in #Bangui w 82t @WFP rice, yards from displaced ppl camp. Airlifts to last 1month pic.twitter.com/Zp74iHntmD
� Alexis Masciarelli (@amasciarelli) February 12, 2014
LeParisien.fr
