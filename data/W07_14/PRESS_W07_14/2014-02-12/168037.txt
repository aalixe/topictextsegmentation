TITRE: Dix ans de prison pour le mari qui torturait sa femme - L'Express
DATE: 2014-02-12
URL: http://www.lexpress.fr/actualite/societe/dix-ans-de-prison-pour-le-mari-qui-torturait-sa-femme_1323294.html
PRINCIPAL: 168033
TEXT:
Dix ans de prison pour le mari qui torturait sa femme
Par LEXPRESS.fr, publi� le
12/02/2014 �  18:15
Les jur�s de la Cour d'assises des Bouches-du-Rh�ne ont condamn� Ren� Schembri, enseignant retrait�, � dix ans de prison pour actes de tortures et de barbarie sur sa femme. Les faits auront dur� 32 ans.�
Voter (0)
� � � �
Pour le parquet, Ren� Schembri, comme mu par une "volont� d'an�antir sa femme" par la "perversion, la perversit� a d�traqu�, d�mont� sa personnalit�".
afp.com/Bertrand Langlois
Ren� Schembri , enseignant retrait� �g� de 71 ans, a �t� condamn� ce mercredi devant les assises des Bouches-du-Rh�ne � 10 ans de prison actes de tortures et de barbarie sur sa femme . A l'unanimit� des jur�s, Ren� Schembri a �t� reconnu coupable de violences volontaires, ayant entra�n� "infirmit� permanente et mutilation", sur son ex-femme Colette, au cours des 32 ans qu'aura dur� leur vie commune. �
Les jur�s ont retenu les circonstances aggravantes, les violences �tant le fait du conjoint.�
Quinze ans d'emprisonnement avaient �t� requis par la repr�sentante du parquet, Martine Assonian , qui d�crivait "un dossier exceptionnel, extraordinaire dans les violences des actes de tortures et de barbarie, les mutilations".�
"Elle �tait pire qu'une esclave"
"Comment un mari peut-il imposer pendant trente ans de telles violences � son �pouse?," s'�tait-elle demand�, en r�sumant "l'isolement financier complet, les violences incessantes, les d�gradations de son corps".�
"J'�tais oblig�e d'accepter, si je n'acceptais pas c'�tait pire", a t�moign� son ex-�pouse, d�taillant ses "tortures": coups de b�tons sur le sexe, cuisses br�l�es, l�vres g�nitales cousues et puis perc�es d'un trombone, paralysie des cordes vocales, perte d'un oeil, l�vre mutil�e...�
Si Colette Renault n'a d�pos� plainte pour tortures que quatre ans apr�s son divorce, en 2009, c'est "parce qu'elle �tait sous l'emprise" de ce "manipulateur" qui a su couper cette femme, qu'il avait aussi "isol�e financi�rement", de ses liens "les plus proches, sa famille", avait-elle ajout�. Mme Renault "ne sait pas dire non, ne peut pas dire non, on ne lui a pas appris tant par son milieu familial que par sa personnalit�". "Elle �tait pire qu'un esclave... Pour elle la soumission est la normalit�", a poursuivi la magistrate. "Elle ne pouvait pas s'�chapper, elle �tait dans ce carcan moral et financier".�
>> Lire aussi: Retrait� accus� de torture: "Je regrette d'avoir �t� stupide" �
Elle a d�crit en revanche son mari, Ren� Schembri, comme mu par une "volont� d'an�antir sa femme" par la "perversion, la perversit� et qui durant des ann�es a d�traqu�, d�mont� la personnalit� de sa femme". Au cours du proc�s, "on attendait une r�ponse de la part de M. Schembri," a dit la magistrate pour qui "les dires" de l'accus� "ne sont pas des aveux". "C'est le d�ni, c'est le d�ni le plus complet", a-t-elle martel�, en dressant le portrait d'un "tortionnaire" sous l'apparence "d'un homme � l'allure insignifiante".�
Deux tentatives de fuite
Par deux fois, Colette avait tent� de fuir, d�s 1971, quatre mois apr�s son mariage, puis en 1977, �tant � chaque fois rattrap�e par son mari, avant de r�ussir � d�finitivement s'�vader, en 2002. L'enseignant retrait�, qui niait les tortures et actes de barbarie sur son ex-femme, avait fait volte-face mardi, lui demandant "pardon" pour des violences sexuelles affirmant toutefois qu'elle en avait �t� "complice". Entendu � nouveau mercredi, M. Schembri a r�it�r� sa demande de pardon et exprim� "ses regrets sinc�res", assurant ne "plus �tre cet homme qu'� pr�sent il maudit". "Par ma faute , toute une famille est bris�e. Devant un tel d�sastre, je n'ai qu'une chose � faire, c'est m'en remettre � votre d�cision", a-t-il dit aux six jur�s.�
Son avocat, Me Fr�d�ric Monneret , avait plaid� l'acquittement pour une parties des faits incrimin�s : les coups de b�ton, la l�sion de la l�vre et la perte de l'oeil gauche, apr�s qu'un expert eut estim� "hasardeux", d'�tablir un lien de causalit�, 23 ans apr�s les faits, avec des coups. Pour le reste, "il y a eu des coups, il y a eu violences, elles existent (...) Vous �tes une victime", a-t-il dit � l'adresse de Mme Renault, estimant que dans leur relation "de type sado-masochiste, cette femme n'avait pas tout son libre arbitre et lui �tait dans une spirale infernale".�
Les deux filles du couple ont livr� mardi des t�moignages contradictoires, la cadette prenant la d�fense de son p�re et l'a�n�e celle de sa m�re.�
Avec
