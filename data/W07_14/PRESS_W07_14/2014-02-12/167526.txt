TITRE: Tottenham : Lloris d�ment pour Arsenal
DATE: 2014-02-12
URL: http://www.topmercato.com/79511,1/tottenham-lloris-dement-pour-arsenal.html
PRINCIPAL: 167523
TEXT:
Bourges invit� du dernier carr� Lire
Braillard secr�taire d'Etat aux Sports Lire
Hamilton dans son jardin Lire
Actu Sport.fr
�
Tottenham : Lloris d�ment pour Arsenal
Les derni�res d�clarations d' Olivier Giroud au sujet de l'avenir du gardien de Tottenham Hugo Lloris (27 ans) ont beaucoup fait parler en Angleterre. Alors que l'attaquant des Gunners affirmait r�cemment que le gardien des Spurs souhaitait le rejoindre � Arsenal , le portier de l'�quipe de France a d� s'expliquer ce mardi.
"Je pense que certains mots ont �t� mal interpr�t�s mais c'est embarrassant. Je ne pense pas � �a et je suis concentr� sur les ambitions de Tottenham. Je suis ravi de jouer pour les Spurs. J'ai trop de respect pour le club et les fans pour penser une chose comme �a. Bien s�r que je le connais tr�s bien car nous sommes �quipiers en s�lection nationale, comme avec Laurent Koscielny , mais je suis un joueur des Spurs et je suis tr�s fier d'�tre ici", a rappel� le portier des Bleus sur le site officiel du club londonien.
