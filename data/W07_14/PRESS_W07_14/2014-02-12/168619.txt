TITRE: Contrairement � la gastro, la grippe progresse et touche fortement Rh�ne-Alpes
DATE: 2014-02-12
URL: http://www.mlyon.fr/113017-contrairement-a-la-gastro-la-grippe-progresse-et-touche-fortement-rhone-alpes.html
PRINCIPAL: 168618
TEXT:
RSS 2.0
Contrairement � la gastro, la grippe progresse et touche fortement Rh�ne-Alpes
L?institut de Veille Sanitaire (InVS) a rendu public un nouveau rapport concernant la progression des �pid�mies de grippe et de gastro. La premi�re continue de s?�tendre avec 274 000 consultations en deux semaines pour des sympt�mes de grippe. C?est d?ailleurs la deuxi�me semaine cons�cutive que le seuil �pid�mique est franchi.
La r�gion Rh�ne-Alpes n?est pas �pargn�e puisque, la semaine derni�re, elle �tait la troisi�me la plus touch�e avec 369 cas pour 100 000 habitants. Elle �tait devanc�e par Provence-Alpes-C�te d?Azur � 467 cas et Languedoc-Roussillon � 450 cas. Selon l?Institut, 13 personnes sont d�j� d�c�d�es de la grippe depuis le d�but du mois de novembre.
Au contraire, la gastro tend � reculer apr�s une avanc�e importante. Elle impacte �galement moins Rh�ne-Alpes puisque, la semaine derni�re, les r�gions les plus touch�es �taient la Franche-Comt�, la Basse-Normandie et PACA.
R�dig� dans Sant� le 12/02/2014 � 17h48
Laissez un commentaire :
