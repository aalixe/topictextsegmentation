TITRE: D�pendance : un projet de loi ax� sur le maintien � domicile
DATE: 2014-02-12
URL: http://www.boursier.com/actualites/economie/dependance-jean-marc-ayrault-devoile-son-projet-de-loi-axe-sur-le-maintien-a-domicile-22998.html
PRINCIPAL: 167353
TEXT:
D�pendance : un projet de loi ax� sur le maintien � domicile
Abonnez-vous pour
(Boursier.com) � A l'occasion d'un d�placement � Angers mercredi, le Premier ministre Jean-Marc Ayraut a pr�sent� les grandes lignes du projet de loi d'orientation et de programmation "pour l'adaptation de la soci�t� au vieillissement", fruit d'une concertation lanc�e en novembre dernier...
650 millions d'euros mobilis�s
Les mesures annonc�es seront financ�es d�s l'ann�e 2015 par la contribution additionnelle de solidarit� pour l'autonomie (CASA), soit 650 millions d'euros par an. Mise en place le 1er avril 2013, il s'agit d'un pr�l�vement � hauteur de 0,3% sur les pensions de retraite, de pr�retraite (pour les salari�s et non salari�s) et sur les pensions d'invalidit�. Elle s'applique sur toutes les pensions des r�gimes de base (Cnav, MSA, RSI...), et compl�mentaires (Agirc, Arrco...).
Le maintien � domicile
L'un des axes forts de ce projet est de maintenir les personnes �g�es en difficult� � domicile. Pour cela, le gouvernement compte revaloriser l'allocation personnalis�e d'autonomie (APA) pour un montant suppl�mentaire de 375 millions d'euros par an. "Pour une personne en perte lourde d'autonomie, l'augmentation des plafonds d'aide pourrait aller jusqu'� augmenter de pr�s d'une heure par jour l'aide � domicile", a indiqu� Jean-Marc Ayrault. Le projet pr�voit par ailleurs que les "petits retrait�s" n'aient plus rien � payer pour avoir acc�s � l'aide � domicile, et ce, afin d'�viter que certains y renoncent pour des questions financi�res.
Autre mesure visant � favoriser le maintien � domicile : une "aide au r�pit", pour les personnes s'occupant au moins d'un de leurs proches �g� de 60 ans ou plus � domicile. "D'un montant qui pourra aller jusqu'� 500 euros annuels au-del� du plafond de l'APA, cette aide permettra par exemple de financer sept jours de s�jour dans un h�bergement temporaire", a d�taill� Jean-Marc Ayrault.
Adapter les logements
Le projet de loi entend par ailleurs adapter "la soci�t� toute enti�re au vieillissement".� Un plan national d'adaptation des logements (�quipements domotiques, technologiques) va ainsi �tre lanc�, l'objectif �tant de construire 80.000 habitations d'ici 2017. "Nous allons aussi soutenir le logement interm�diaire, entre le domicile et l'�tablissement, et donner un nouveau souffle aux foyers logements, rebaptis�s R�sidences Autonomie, gr�ce � la cr�ation d'un forfait autonomie destin� � am�liorer la pr�vention dans ces lieux de vie", a ajout� Jean-Marc Ayrault. Les villes sont aussi concern�es puisque les besoins des personnes �g�es devront �tre pris en compte dans les programmes locaux de l'habitat (PLH) et les plans de d�placement urbain (PDU).
Le projet sera transmis dans les prochains jours au Conseil �conomique, social et environnemental (CESE) puis pr�sent� en Conseil des ministres au printemps prochain, le but �tant que la loi entre en vigueur d�s l'an prochain...
Marianne Davril � �2014, Boursier.com
