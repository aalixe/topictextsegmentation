TITRE: Soci�t� G�n�rale : la banque se dit en ordre de bataille pour "saisir les opportunit�s de croissance"
DATE: 2014-02-12
URL: http://www.boursier.com/actions/actualites/news/societe-generale-la-banque-se-dit-en-ordre-de-bataille-pour-saisir-les-opportunites-de-croissance-566089.html
PRINCIPAL: 166362
TEXT:
Mon compte Je suis d�j� client
D�couvrir Je souhaite recevoir une documentation gratuite
Soci�t� G�n�rale : la banque se dit en ordre de bataille pour "saisir les opportunit�s de croissance"
Abonnez-vous pour
moins de 1� par jour !
Le 12/02/2014 � 08h09
(Boursier.com) � La Soci�t� G�n�rale a livr� une publication plut�t solide ce matin, avec des ratios prudentiels bien positionn�s. La direction s'estime en ordre de bataille pour reprendre la croissance et atteindre un objectif de rentabilit� des capitaux de 10% � l'horizon 2015.
La banque a publi� un produit net bancaire de 5,782 milliards d'euros pour le compte de son 4�me trimestre 2013, en progression de 12,7% en donn�es publi�es et de 20,1% en comparable, pour un r�sultat brut d'exploitation de 1,297 milliard d'euros (+8,6% en oubli�, +15,2% en comparable) et un b�n�fice net de 322 millions d'euros part du groupe. Hors �l�ments non-r�currents, il aurait atteint 928 millions d'euros. Le consensus misait sur un produit net bancaire de 5,87 milliards d'euros, un r�sultat brut d'exploitation de 1,394 milliard d'euros et un b�n�fice net part du groupe de 152 millions d'euros, comprenant une charge de 446 millions d'euros li�e � l'amende Libor.
L'�tablissement �voque la fin d'une "phase de transformation profonde de son bilan", qui lui a permis d'atteindre un ratio CET1 de 10% sur la base du r�f�rentiel B�le III. L'objectif de d�but d'ann�e est ainsi d�pass�. La r�duction du portefeuille d'actifs g�r�s en extinction a continu�, si bien qu'il ne repr�sentait plus que 709 millions d'euros et n'aura plus d'impact sur les r�sultats � partir de cette ann�e. Le conseil d'administration proposera � l'assembl�e g�n�rale le versement d'un dividende de 1 euro par action en num�raire.
"L'ann�e 2013 apporte la confirmation de la solidit� du mod�le de banque universelle de Soci�t� G�n�rale, avec une croissance des revenus dans un environnement toujours difficile. Parall�lement la transformation structurelle du bilan est achev�e, se traduisant par des ratios de capital et de liquidit� tr�s solides. Le niveau de provisionnement des risques a �galement �t� significativement renforc� au cours de l'ann�e �coul�e. Ainsi le groupe est en position, en 2014 et au-del�, de saisir les opportunit�s de croissance en s'appuyant sur un mod�le et des activit�s focalis�s sur la satisfaction des clients et l'innovation", a comment� le directeur g�n�ral Fr�d�ric Oud�a, qui donne rendez-vous au march� le 13 mai prochain pour une conf�rence de pr�sentation des �l�ments qui permettront � sa banque d'atteindre une rentabilit� sur capital de 10% d'ici la fin 2015.
Anthony Bondain � �2014, Boursier.com
