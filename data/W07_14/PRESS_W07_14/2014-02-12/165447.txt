TITRE: La Cour des comptes � Didier Robert : � irr�aliste ��(...) -  Economie
DATE: 2014-02-12
URL: http://www.temoignages.re/la-cour-des-comptes-a-didier-robert-irrealiste,74432.html
PRINCIPAL: 0
TEXT:
# Impasse du mod�le , # Didier Robert , # A la Une de l�actu
Une fr�quentation en retrait par rapport � Maurice, des d�penses de communication loin de correspondre aux r�sultats, une strat�gie touristique dat�e, des billets d�avion subventionn�s qui font monter les prix� La Cour des comptes juge tr�s s�v�rement la politique touristique conduite par Didier Robert � la R�gion. Voici des extraits du rapport annuel publi� hier, relatif au tourisme dans notre ile, avec des intertitres de "T�moignages"�:
Avec une telle politique, les touristes ne sont pas pr�s de venir en masse� (photo C.F.)
Dans l�oc�an indien, La R�union est en retrait par rapport � ses concurrents, Maurice (965.000 touristes en 2012) et les Maldives (958.000 touristes), qui ont connu une croissance sup�rieure � 30% ces dix derni�res ann�es. (�)
Les strat�gies touristiques des conseils r�gionaux de la Martinique et de La R�union s�inscrivent dans des plans anciens, datant de la fin des ann�es 90 et du d�but des ann�es 2000.
Outre leur caract�re dat�, ces documents affichent des objectifs g�n�raux, nombreux et non hi�rarchis�s, sans pr�cision de calendrier ni de moyens, notamment financiers. La plupart d�entre eux sont fond�s sur des hypoth�ses irr�alistes, comme l�objectif de parvenir � 600 000 touristes d�ici 2015 � La R�union, l�offre d�h�bergement �tant incapable d�absorber un tel flux. (�)
Atouts pas valoris�s
Face � la palette tr�s riche de son offre touristique (destination de montagne, de volcan, de tourisme de nature, offre baln�aire, diversit� culturelle), La R�union h�site entre le ��bleu�� (tourisme baln�aire) et le ��vert�� (tourisme de nature), transmettant aux client�les �trang�res un message peu lisible.
Le parc national de La R�union, qui repr�sente pr�s de 40% du territoire, a �t� class� en 2010 au patrimoine mondial de l�humanit� par l�Unesco pour ses cirques, ses pitons et ses remparts. La strat�gie touristique r�unionnaise, �labor�e avant ce classement, reste silencieuse face � un produit d�appel et de notori�t� majeur.
��Situation de mono-client�le��
Les �conomies touristiques des Antilles et de La R�union sont largement orient�es vers la m�tropole, au risque de devenir des produits m�connus du march� mondial. La client�le m�tropolitaine repr�sente plus de 80% des touristes.
Cette situation de mono-client�le constitue un double handicap pour les destinations de la Guadeloupe, de la Martinique et de La R�union. Le premier est une grande sensibilit� � la conjoncture �conomique fran�aise. Le second tient au caract�re fortement affinitaire. Or le tourisme affinitaire apporte moins de recettes que le tourisme d�agr�ment ou le tourisme d�affaires, les d�penses li�es � l�h�bergement et � la restauration �tant moindres.
R�sultats pas � la hauteur des d�penses
Aux Antilles et � La R�union, les conseils r�gionaux, via les comit�s r�gionaux de tourisme, ont engag� des actions visant � diversifier les client�les touristiques en ciblant prioritairement les pays d�Europe du Nord et les client�les �trang�res voisines. Les r�sultats ne sont pas � la hauteur des enveloppes financi�res engag�es annuellement (plus de 7 M� � La R�union et 6 M� � la Guadeloupe). La part des client�les europ�ennes reste stable, repr�sentant en moyenne 5% des touristes accueillis.
Au-del� des facteurs ext�rieurs li�s notamment � la conjoncture internationale et � la faible implication des acteurs priv�s dans la promotion touristique, les politiques promotionnelles conduites par les acteurs publics locaux portent, en elles-m�mes, des faiblesses qui expliquent l�absence de r�sultat.
Et les �les Vanille�?
� La R�union, les actions de promotion, qui prennent la forme d��v�nementiels organis�s en m�tropole et � l��tranger, sont peu �valu�es alors qu�elles entra�nent des frais importants li�s au transport a�rien, au fret et � l�h�bergement des participants sans avoir d�effet structurant imm�diat. (�)
La R�union participe depuis 2010, avec les �les de la zone (Maurice, Seychelles, Madagascar, Mayotte, Comores, Maldives), � une nouvelle strat�gie promotionnelle destin�e � s�duire les client�les �trang�res, particuli�rement les touristes europ�ens et chinois, en leur proposant des offres combin�es inter-�les. Fin 2013, ce concept des ���les vanille�� se traduit essentiellement par la vente d�offres combin�es entre La R�union et Maurice et peine encore � se mat�rialiser dans les taux de fr�quentation en raison notamment des diff�rences dans les r�glementations relatives aux visas et au fait que les partenaires sont �galement des concurrents.
Conclusion
�laborer un plan strat�gique, actualis�, fond� sur une observation du secteur, fixant des orientations prioris�es � moyen terme.
Air France privil�gie Maurice
��Air France et Corsair desservent La R�union depuis Orly. Seule la compagnie Air Austral fait d�coller ses avions depuis Roissy Charles de Gaulle. Air France ne partage aucun code share avec Air Austral, compagnie qu�elle a contribu� � cr�er, alors qu�elle est engag�e dans un partenariat de cette nature avec la compagnie mauricienne Air Mauritius qui a arr�t� la majorit� de ses liaisons europ�ennes. Cet accord facilite le transport des touristes europ�ens vers Maurice. Les tensions qui ont pu exister dans les relations entre Air France et Air Austral, li�es � la d�cision de la compagnie r�gionale r�unionnaise, en 2003, de desservir Paris, ne favorisent pas les int�r�ts touristiques de l��le.��
Les 13 millions d�euros de la continuit� territoriale font monter les prix
Au moment o� la R�gion vote une d�pense de 13 millions d�euros pour subventionner des billets d�avion, voici ce qu��crit la Cour des Comptes�:
��Les dispositifs de continuit� territoriale (aide au transport en faveur des r�sidents) et de cong�s bonifi�s (prise en charge des frais de voyage des fonctionnaires tous les trois ans) peuvent avoir un caract�re inflationniste sur les tarifs a�riens.��
��Supprimer les d�fiscalisations��
Voici la seule recommandation de la Cour des Comptes � l�Etat.
Supprimer les d�fiscalisations ��Girardin�� en faveur des investissements productifs et les remplacer par d�autres modes d�intervention, moins co�teux pour le budget de l��tat et plus efficaces.
mod�ration a priori
Ce forum est mod�r� a priori�: votre contribution n�appara�tra qu�apr�s avoir �t� valid�e par un administrateur du site.
Qui �tes-vous�?
