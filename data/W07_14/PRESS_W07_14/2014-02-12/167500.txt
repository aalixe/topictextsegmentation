TITRE: George Clooney veut le retour des marbres du Parth�non, ce qui d�frise Londres | Geopolis
DATE: 2014-02-12
URL: http://geopolis.francetvinfo.fr/george-clooney-veut-le-retour-des-marbres-du-parthenon-ce-qui-defrise-londres-30195
PRINCIPAL: 167498
TEXT:
George Clooney veut le retour des marbres du Parth�non, ce qui d�frise Londres
Par   Pierre Magnan | Publi� le 11/02/2014 � 16H38, mis � jour le 11/02/2014 � 16H45
George Clooney � Berlin pour la pr�sentation de son film��'The Monuments Men' � CLEMENS NIEHAUS/GEISLER-FOTOPRES / GEISLER-FOTOPRESS / PICTURE-ALLIANCE/AFP
C�est � Berlin que l�acteur-r�alisateur am�ricain George Clooney a fait plaisir � la Gr�ce en affirmant, � propos des frises du Parth�non conserv�es au British Museum, que ce �serait une bonne id�e� qu�elles soient rendues � Ath�nes.
La sc�ne se passait samedi � la 64e Berlinale, le festival du film de Berlin. George Clooney pr�sentait son film Monument men�sur les vols d��uvres d�art par les nazis, quand il s�est fait interroger par une journaliste grecque sur la question des frises du Parth�non, consid�r�es en Gr�ce comme vol�es par les Anglais.
Copie du journal grec �Enet�. � dr
�Oui, ce serait une bonne id�e, vous avez le droit de votre c�t�, a simplement r�pondu l'acteur, selon la presse grecque. Par cette r�ponse de moins de quinze mots, la star hollywoodienne s'est attir� un �lan de reconnaissance au pied de l'Acropole, illustr� par des pages enti�res voire des Unes, consacr�es par la presse grecque � cette d�claration depuis le week-end.�
Clooney invit� en Gr�ce
Encore mieux, le minist�re grec de la Culture s�est fendu d�une lettre � l�acteur, transmise � la presse, l�invitant � passer �quelques jours en Gr�ce� (au frais du contribuable grec exsangue?). �Au nom de tous les Grecs, je vous adresse un grand merci pour votre d�claration�, �crit le ministre de la Culture, Panos Panagiotopoulos. �J'esp�re que vous accepterez cette invitation � passer quelques jours en Gr�ce. Pour voir une multitude d'antiquit�s grecques conserv�es sous le soleil m�diterran�en. Et, bien s�r, visiter le nouveau mus�e de l'Acropole, en face du rocher sacr�, o� une place attend le retour des marbres du Parth�non en exil involontaire.�
Copie du journal anglais �The Independent�. � DR
A Londres, on parle des �marbres d'Elgin�
C�t� anglais, ce n�est pas une lettre d�invitation qu�a re�ue l�acteur. What else, alors? Une vol�e de bois vert. Ainsi, dans The Independent,�John Whitingdale,�un ex-d�put� conservateur responsable d�une commission sur la culture et les m�dias, a �estim� qu�il �ne conna�t pas l�histoire des marbres d�Elgin et le droit des Anglais dessus�. Quant au British Museum, il a r�affirm� que les marbres �font partie de leurs collections�.
La pol�mique n'est pas nouvelle et rebondit r�guli�rement, partisans et opposants au retour des frises du Parth�non se d�chirant sur la question.�
Les marbres du Parth�non ont �t� r�alis�s entre 447 et 432 avant J�sus Christ. Sur le site de l�ambassade de Gr�ce en France,�la suite est ainsi racont�e�: �Au d�but du si�cle dernier, peu avant la guerre d�ind�pendance et la lib�ration de la Gr�ce du joug ottoman, lord Elgin, ambassadeur de Grande-Bretagne � Constantinople, a d�pouill� le Parth�non.�
C'est en 1980 que la Gr�ce, par la voix de Melina Mercouri, alors ministre de la Culture, r�clama officiellement la restitution des frises du Parth�non, le temple d�di� � Athena qui domine Ath�nes. En vain, pour l�instant.
Partager sur Facebook Partager sur Twitter Partager sur Google Partager sur Tumblr Partager par e-mail Imprimer cet article
Europe,��Gr�ce
