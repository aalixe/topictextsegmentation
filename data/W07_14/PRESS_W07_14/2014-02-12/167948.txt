TITRE: Laporte : Toulon fait appel - Fil Info - Top 14 - Rugby -
DATE: 2014-02-12
URL: http://sport24.lefigaro.fr/rugby/top-14/fil-info/laporte-toulon-fait-appel-678781
PRINCIPAL: 167946
TEXT:
Laporte : Toulon fait appel
12/02 17h33 - Rugby, Top 14
Le RCT a imm�diatement fait savoir qu�il allait faire appel de la suspension de 13 semaines inflig�e � son manager, suite � ses propos injurieux envers un arbitre. �En la mati�re, la commission de discipline a bafou� le droit, car lorsque Bernard Laporte a tenu les propos qu'on lui reproche, il �tait sur l'antenne de RMC Sport, radio pour laquelle il intervenait comme consultant et non comme entra�neur du RC Toulon. Deux jours plus tard, il a certes tenu d'autres propos, mais c'�tait en marge d'un match de H Cup contre Cardiff, rencontre organis�e sous l'�gide de l'ERC et non pas de la F�d�ration fran�aise et de la Ligue nationale de rugby�, a d�clar� le pr�sident Mourad Boudjellal � La Provence. Et d�ajouter�: �Le probl�me, c'est qu'� l'instar de l'arbitrage, que l'on souhaiterait davantage professionnel, il faudrait �galement avoir affaire � une commission de discipline compos�e de juristes professionnels. Mais non, cette commission est un �tat de non-droit.�
