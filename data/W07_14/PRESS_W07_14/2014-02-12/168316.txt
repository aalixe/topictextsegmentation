TITRE: En 2011, l'UMP �voquait le "genre" dans son programme - leJDD.fr
DATE: 2014-02-12
URL: http://www.lejdd.fr/Politique/En-2011-l-UMP-evoquait-le-genre-dans-son-programme-652764
PRINCIPAL: 0
TEXT:
Tweet
En 2011, l'UMP �voquait le "genre" dans son programme
En 2014, l'UMP fait tout pour lutter contre la "th�orie du genre". Mais en juin 2011, � l'issue d'une convention sur l'�galit� hommes-femmes, le parti de Jean-Fran�ois Cop� proposait que, d�s la maternelle, il fallait aider "filles et les gar�ons � percevoir positivement leur genre et celui du sexe oppos�".
L'opposant farouche � la "th�orie du genre", c'est lui. Apr�s ses sorties contre le gouvernement, voil� pourtant un document g�nant pour Jean-Fran�ois Cop�. Alors que les opposants � cette "th�orie" ne cessent de fouiller Internet � la recherche de document prouvant que les socialistes veulent l'instaurer, un internaute a us� mardi de la m�me technique pour mettre la main sur un texte de l'UMP �voquant le "genre". Pas n'importe quel texte, puisqu'il s'agit du compte rendu d'une convention du parti (d�j� dirig� par Cop�) sur l'�galit� hommes-femmes, tenue en juin 2011 dans le cadre de ses Etats g�n�raux. A l'�poque, il s'agissait de pr�parer le futur programme pr�sidentiel.
Il y a d'abord un constat :�
"D�apr�s le rapport de Brigitte Gr�sy sur l�image de la femme dans les m�dias, les hommes sont plus souvent pr�sent�s comme des 'experts' ou des 'hommes d�action' et les femmes plus souvent comme des 't�moins' ou des 'victimes' rattach�es � la sph�re du foyer. Les m�dias recr�ent un monde binaire, voire archa�que, et transmettent des repr�sentations li�es au genre presque r�gressives, loin du monde actuel!", explique le document.
Ensuite, il y a des propositions , qui vont, alors que Jean-Fran�ois Cop� s'en inqui�te aujourd'hui, jusqu'� sugg�rer de lutter contre ces st�r�otypes d�s "la maternelle".
Mais l'UMP allait alors plus loin. Elle proposait de lutter contre ces st�r�otypes au sein de l'entreprise �galement :
Et aussi dans le service public, quotas � la clef :
Ce n'�tait pas jean-Fran�ois Cop� qui pilotait cette convention, mais Marie-Jo Zimmerman. Mais le patron de l'UMP s'�tait felicit� de ce travail , et il avait exprim� l'envie d'"aller au fonds des choses" sur ce dossier de l'�galit� hommes-femmes. Il qualifiait alors le travail de "plus approfondi" de toutes les familles politiques.
Un revirement qu'a ironis� Najat Vallaud-Belkacem , ministre des Droits des femmes :�
Merci � l'UMP 2011 de soutenir si ardemment les ABCD de l'�galit� qu'elle conspue aujourd'hui. pic.twitter.com/AR40z39EP1
� Najat Belkacem (@najatvb) 12 F�vrier 2014
V.V. - leJDD.fr
Jean-Fran�ois Cop� avait � l'�poque un travail "approfondi" sur la question. (Reuters)
Et aussi
Connectez-vous ou inscrivez-vous pour laisser un message
Message
Saississez ce code de s�curit� :
Les plus populaires Les plus r�cents Les plus anciens
� Pour valider votre �valuation, saississez ce code de s�curit� :
9 4 Par Carmina44
Post� le 13/02/2014 � 07h46 - Signalez un abus Bravo amedh pour votre r�ponse ... rien � voir entre Cop� et les lobbies des assos LGBT qui exigent de tout organiser en fonction de leurs propres particularit�s li�es � la sexualit� ... sans tenir compte des voeux du plus grand nombre des fran�ais qui eux ne veulent pas voir �duquer leurs enfants selon les volont�s de cette poign�e de totalitaires !.. Pour Cop�, il s'agit de la "repr�sentation de la femme" toujours malmen�e (voir certaines publicit�s et surtout certains clips musicaux destin�s � la jeunesse !...) et non "d'�ducation sexuelle" .... mais la plupart des commentaires ici tombent dans le panneau, n'ayant lu qu'en diagonale (ou n'ayant lu que le titre) et surtout trouvant l� une occasion suppl�mentaire d'attaquer une fois de plus la droite et Cop� .... il faut dire aussi que le titre de l'article a �t� mis � bon escient pour installer la confusion dans les esprits ... merci encore une fois aux journalistes "bien" intentionn�s !!! R�pondre                                     - 3 r�ponses
Connectez-vous ou inscrivez-vous pour laisser un message
Message
Saississez ce code de s�curit� :
� Pour valider votre �valuation, saississez ce code de s�curit� :
12 8 Par amedh
Post� le 12/02/2014 � 23h54 - Signalez un abus Je ne vois pas le rapport. Dans ces propositions, l'UMP veut s'attaquer � l'�galit� homme-femme mais en aucun cas ne parle d'identit� sexuelle. Aucune initiative  pour faire croire aux gar�ons qu'ils peuvent devenir des filles et vice versa, aucune promotion de l'homosexualit�, aucune intention de faire intervenir des associations LGBT dans les �coles. On ne parle quand m�me pas de la m�me chose , m�me si le mot genre est �voqu�. R�pondre                                     - 1 r�ponse
Connectez-vous ou inscrivez-vous pour laisser un message
Message
Saississez ce code de s�curit� :
� Pour valider votre �valuation, saississez ce code de s�curit� :
7 8 Par jean-camille
Post� le 12/02/2014 � 21h30 - Signalez un abus La presse toujours en campagne pour le PS et Hollande ?... Il semble pourtant  qu'hollande aussi change d'avis,mais ,lui ,continue de ruiner la France . R�pondre                                     - 2 r�ponses
Connectez-vous ou inscrivez-vous pour laisser un message
Message
Saississez ce code de s�curit� :
� Pour valider votre �valuation, saississez ce code de s�curit� :
12 7 Par effiat
Post� le 12/02/2014 � 23h18 - Signalez un abus M.Cop�. Suite � la r�surgence d'un document , dat� de juin 2011 et pr�nant, au niveau de l'Education Nationale, l'introduction "d�s la maternelle de s�ances consacr�es � l'�galit� homme:femme", sans la vid�o et le texte complet de votre discours, j'ai l'impression d'avoir affaire � un pantin.Lorsque vous parliez comme �a, on disait :"il a du culot, c'est vraiment un homme moderne". Maintenant que la gauche suit votre lanc�e, vous retournez votre veste : et pas n'importe comment, en essayant de faire peur aux fran�ais (heureusement que les archives vous concernant sont l�,que ce soit � la t�l� ou � la radio). Bref, ne le prenez pas mal, ce n'est qu'une petite caricature � la hauteur de ce que  vous valez:  les petites filles du PS en rose, les petits gar�ons de l'UMP en bleu! Si un PS veut mettre du bleu � sa fille et un UMP, du rose � son fils, �a va poser probl�me: on ne saura plus les distinguer! Ni dans un sens, ni dans un autre. Enfin, vous r�coltez ce que vous avez sem�, et la moindre des choses, c'est de vous faire tout petit! R�pondre                                     - 2 r�ponses
Connectez-vous ou inscrivez-vous pour laisser un message
Message
Saississez ce code de s�curit� :
� Pour valider votre �valuation, saississez ce code de s�curit� :
2 1 Par matinvert
Post� le 13/02/2014 � 17h29 - Signalez un abus Les difficult�s de compr�hension d'un certain nombre d'internautes (Najat Belkacem, elle, fait seulement semblant, pour entretenir la confusion) est inqui�tante. Il ne s'agit pas de th�orie du genre qui pr�ne l'indiff�renciation, au contraire! Voil� par exemple une phrase: "Il faut aider les filles et les gar�ons � percevoir positivement leur genre et celui du sexe oppos�". Tout le contraire de "tous pareils" ! Au lieu de survoler l'article, il vaudrait mieux le lire...Ici on parle d'�galit�! Alors �videmment cela ressemble un peu � ce qui est pr�conis� maintenant � l'�cole primaire et qui a �t� �dulcor� par rapport � ce qui �tait dans les cartons !!!!! R�pondre
Connectez-vous ou inscrivez-vous pour laisser un message
Message
Saississez ce code de s�curit� :
� Pour valider votre �valuation, saississez ce code de s�curit� :
10 6 Par gavrinis
