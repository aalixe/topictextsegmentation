TITRE: Art. Le march� mondial ne conna�t pas la crise
DATE: 2014-02-12
URL: http://www.ouest-france.fr/art-le-marche-mondial-ne-connait-pas-la-crise-1925389
PRINCIPAL: 168055
TEXT:
Art. Le march� mondial ne conna�t pas la crise
Monde -
12 F�vrier
Le produit des ventes aux ench�res d'oeuvres d'art dans le monde a augment� de 13,17% en 2013, � 12,05 milliards de dollars, un record historique�|�AFP
Facebook
Achetez votre journal num�rique
Le produit des ventes aux ench�res d'oeuvres d'art dans le monde a augment� de 13,17% en 2013, � 12,05 milliards de dollars, un record historique
Avec des ventes de 4,078 milliards de dollars (+21%), la Chine arrive en t�te pour la quatri�me ann�e cons�cutive mais d'extr�me justesse, devant les Etats-Unis qui affichent un produit de 4,016 milliards de dollars (+20%), a indiqu� le num�ro un mondial des donn�es sur le march� de l'art, la soci�t� fran�aise Artprice.�
Le produit des ventes aux ench�res d'oeuvres d'art dans le monde s'est �tabli � 12,05 milliards de dollars.�
La Chine en t�te
Ce rebond des ventes de "Fine art" (peintures, sculptures, dessins, photographies, estampes) survient apr�s une baisse de plus de 9% en 2012 � 10,64 milliards de dollars, li�e � une forte contraction du march� chinois alors que les Etats-Unis progressaient.�
En 2013, avec des ventes de 4,078 milliards de dollars (+21%), la Chine arrive en t�te pour la quatri�me ann�e cons�cutive mais d'extr�me justesse, devant les Etats-Unis. Ces derniers affichent un produit de 4,016 milliards de dollars, en hausse de 20%, a d�taill� le num�ro un mondial des donn�es sur le march� de l'art, dont le rapport annuel sera rendu public dans une dizaine de jours.�
Ces grandes puissances repr�sentent � elles seules les deux tiers du march� de l'art (33,84% de part de march� pour la Chine, 33,33% pour les Etats-Unis).�
La France toujours en quatri�me position
Le Royaume-Uni arrive en troisi�me position avec des ventes de 2,11 milliards de dollars et une part de march� 17,57%.�
La France conserve sa quatri�me position, avec des ventes de 549 millions de dollars et une part de march� de 4,56%.�
L'Allemagne est cinqui�me (207 millions de dollars) devant la Suisse (159 millions de dollars).
Lire aussi
