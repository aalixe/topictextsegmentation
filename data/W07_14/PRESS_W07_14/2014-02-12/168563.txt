TITRE: Le contr�leur g�n�ral veut autoriser les portables en prison
DATE: 2014-02-12
URL: http://www.lemonde.fr/societe/article/2014/02/12/le-controleur-general-veut-autoriser-les-portables-en-prison_4365285_3224.html
PRINCIPAL: 168562
TEXT:
Le contr�leur g�n�ral veut autoriser les portables en prison
Le Monde |
12.02.2014 � 18h48
Un ��facteur consid�rable d'apaisement�� de la d�tention. C'est ainsi que Jean-Marie Delarue, le contr�leur g�n�ral des lieux de privation de libert�, plaide pour l'autorisation du t�l�phone portable en prison.
D�j� int�gr�e � son avis du 10 janvier 2011 , cette proposition a �t� r�p�t�e mercredi 12 janvier par M. Delarue, auditionn� par la commission des lois de l'Assembl�e nationale. ��Je crois que, le jour venu, il faudra autoriser les t�l�phones portables en d�tention��, a-t-il d�clar�. En 2011, le contr�leur plaidait pour l'ouverture d'une r�flexion, en pr�cisant qu'une �ventuelle autorisation devrait se faire dans un cadre de s�curit� et de contr�le s�r et l�gitime. Il �voque d�sormais la piste d'un contr�le des appels � �ch�ance r�guli�re.
T�L�PHONES SAISIS, MAIS PAS EXPLOIT�S
Aujourd'hui, les t�l�phones portables sont interdits aux d�tenus, qui ne peuvent utiliser librement que les t�l�phones fixes accessibles dans les �tablissements p�nitentiaires. Les conversations sur ces postes fixes sont susceptibles d' �tre �cout�es par le personnel p�nitentiaire pour raisons de s�curit�.
��Nous �puisons les personnels � la recherche de t�l�phones portables, qui est un puits sans fond��, a regrett� mercredi M. Delarue, rappelant qu'aux Baumettes � Marseille environ 900 t�l�phones mobiles �taient saisis chaque ann�e. D'autant que, selon lui, les portables saisis ne sont pas exploit�s par les services enqu�teurs.
Le contr�leur a dit s' �tre entretenu du sujet avec des personnels p�nitentiaires afin de conna�tre leur sentiment. ��Ils m'ont dit�: �a ne changera rien, mais �a apaisera consid�rablement la d�tention��, a-t-il expliqu�. A titre d'illustration, il a �voqu� la r�cente mutinerie survenue � Argentan (Orne) , d�but d�cembre. Une quinzaine de d�tenus s'�taient retranch�s dans une aile du b�timent et s'�taient livr�s � des actes de vandalisme. Or , la veille, avait eu lieu, dans cette m�me aile, une fouille g�n�rale au cours de laquelle on avait saisi une vingtaine de t�l�phones portables. ��Les personnes d�tenues � Argentan sont loin de tout. Ce qu'il leur reste, c'est le t�l�phone portable��, a-t-il expliqu�.
