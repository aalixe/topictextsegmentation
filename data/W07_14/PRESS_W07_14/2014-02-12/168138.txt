TITRE: (8eme) : C'est fini pour l'Ile-Rousse, �limin� par Guingamp - France Info
DATE: 2014-02-12
URL: http://www.franceinfo.fr/football/8eme-c-est-fini-pour-l-ile-rousse-elimine-par-guingamp-1316091-2014-02-12
PRINCIPAL: 168137
TEXT:
Imprimer
guingamp mathis � Panoramic
L'En Avant Guingamp a d�croch� mercredi son billet pour les quarts de finale de la Coupe de France en �liminant sur la pelouse du stade Fran�ois-Coty d'Ajaccio le petit poucet de la comp�tition, l'Ile-Rousse (CFA2).
Ce n'est pas encore cette ann�e qu'un club corse inscrira de nouveau son nom au palmar�s de la Coupe de France. Dernier repr�sentant de l'Ile de Beaut� encore en course dans l'�preuve, l'Ile-Rousse (CFA2) s'est fait sortir mercredi � Ajaccio par Guingamp. Tombeur notamment sur son parcours h�ro�que de son voisin du GFC Ajaccio et surtout de Bordeaux aux tirs au but au tour pr�c�dent, en 16emes de finale, le petit poucet de cette Coupe de France � ce stade des 8emes, a bien r�sist� sur la pelouse de Fran�ois-Coty. Mais les hommes du tandem d'entra�neurs Graziani-Gennarielli, valeureux, ont chut� sur deux coups de pied arr�t�s en faveur de Guingampais beaucoup plus exp�riment�s. Sur un corner de Thibaut Giresse, Gr�gory Cerdan a tout d'abord devanc� de la t�te la mauvaise sortie de Menozzi, le gardien de l'Ile-Rousse. Un but marqu� juste avant la pause et suivi d'un deuxi�me, peu de temps apr�s en d�but de seconde p�riode. Cette fois sur un coup-franc. Avec une t�te gagnante � l'arriv�e de Moustapha Diallo, tout heureux lui aussi que le gardien corse soit un peu pass� au travers. Une prestation contraire � celle du gardien guingampais, Mamadou Samassa. Impeccable toute la partie, l'international malien a notamment remport� deux face � face tr�s importants devant Santelli puis Ventura. " Quand on joue en Coupe comme �a, on sait que �a va �tre difficile. Une �quipe bien organis�e qui joue bien et est bien organis�e, mais on a �t� pragmatique, efficace. On est tr�s heureux de passer ", savourait l'entra�neur guingampais Jocelyn Gourvennec, qui n'oubliait pas de tirer son chapeau � son gardien et dont l'objectif �tait simple : " �tre au tirage ". Mission accomplie : Guingamp d�fendra ses chances en quarts de finale.
COUPE DE FRANCE � 8EMES DE FINALE
Mardi 11 f�vrier 2014
Cannes (CFA) � Montpellier : 1-0 (ap)
But : Zobiri (119eme) pour Cannes
Angers (L2) � CA Bastia (L2) : 4-2
Buts : Ayari (57eme), Diers (105eme +1), M.Yattara (113eme) et Blayac (117eme) pour Angers - Mba (31eme et 105eme) pour le CA Bastia
Lille � Caen (L2) : 3-3 (6-5, tab)
Buts : Rodelin (34eme), Delaplace (77eme) et Roux (83eme) pour Lille - Koita (14eme et 29eme), Rodelin (90eme +2 csc) pour Lille
Mercredi 12 f�vrier 2014
L'�le Rousse (CFA2) � Guingamp : 0-2
Buts : Cerdan (40eme) et M.Diallo (58eme) pour Guingamp
18h45 : Auxerre (L2) � Rennes (sur France 4)
19h30 : Moulins (CFA) � S�te (CFA2)
20h55 : Nice � Monaco (sur France 3)
Jeudi 13 f�vrier 2014
