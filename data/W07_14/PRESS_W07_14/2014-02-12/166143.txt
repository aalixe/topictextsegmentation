TITRE: Ces Français qui ne veulent pas d'enfant - Europe1.fr - Société
DATE: 2014-02-12
URL: http://www.europe1.fr/Societe/Ces-Francais-qui-ne-veulent-pas-d-enfant-1798767/
PRINCIPAL: 166142
TEXT:
> Suivez l'info Europe 1 en continu sur , et r�agissez sur
4 Commentaires
Par cestmoi08 � 23:39 le 12/02/2014
Clich�s...
Et voil� le retour du pseudo argument "ne pas vouloir d'enfant, c'est �go�ste" (et les parents, ils font des enfants par altruisme, peut-�tre ?). Je suis infiniment heureuse de pouvoir choisir de ne pas avoir d'enfants et j'emmerde les moralisateurs aux petites id�es scl�ros�es...
Par interniet � 23:26 le 12/02/2014
les retrait�s font de vieux os...
Je pense que ceux qui enfantent ne vont pas voir venir la retraite, et vont finir sous les ponts... Le syst�me �conomique et politique est tel qu'avoir un enfant est pour moi une pure criminalit�, engendrer dans une monde en d�liquescence, est ce vraiment utile ? De toute fa�on, un "�go�ste" sans enfant finira mal... L��go�sme c'est plut�t le statut social pour moi...
Par albizzi � 19:09 le 12/02/2014
Ne pas vouloir d'enfant...
...est assez �go�ste, surtout lorsque les retraites sont constitu�es par r�partition, les enfants actifs des retrait�s payant indiff�remment pour leurs parents et pour ceux qui n'ont pas eu d'enfants, soit par impossibilit� physique, ce qui est bien s�r excusable, soit par volont� d�lib�r�e, ce qui l'est moins.
Tous les commentaires
Les commentaires sont d�sactiv�s pour cet article.
En ce moment sur Europe 1
Un gouvernement resserr� oui, mais sur le fil...
Politique      09/04/2014 - 18:07:37
INFOGRAPHIE - Le gouvernement Valls compte 30 membres. Depuis le d�but de la Ve R�publique, les gouvernements fran�ais en ont compt� en moyenne 35,5.
R�forme territoriale�: "ce sera encore plus de d�penses"
Politique      09/04/2014 - 17:25:53
VOTRE CHOIX D�ACTU DU 9 AVRIL � Le Premier ministre a propos� une r�duction du nombre de r�gions et la disparition des conseils d�partementaux.
R�ouverture du zoo de Vincennes : �a s'active
Culture      09/04/2014 - 17:25:46
J-2 - Le compte � rebours a commenc�. Samedi, le nouveau zoo de Vincennes ouvrira ses portes, apr�s six ans de travaux.
Politique      09/04/2014 - 14:39:14
PORTRAIT - Homme de r�seau et expert de l'Europe, Jean-Pierre Jouyet vient d'�tre nomm� secr�taire g�n�ral de l'Elys�e.
International      09/04/2014 - 14:27:16
APPEL - Ils ont lanc� une p�tition pour d�noncer "l'immobilisme du gouvernement" et exiger "une action politique forte".
007�: quel m�chant de James Bond �tes-vous�? �
Cin�ma      09/04/2014 - 14:00:32
TOP 5 - Chiwetel Ejiofor, l�acteur de 12 years a slave, est pressenti pour incarner le nouveau m�chant de James Bond. Et parmi les ennemis de 007, lequel est votre pr�f�r� ?
Proc�s Agnelet�: "ce qui tue, plus que la v�rit�, c�est le secret"
France      09/04/2014 - 12:54:32
A L�AUDIENCE - La cour d�assises a entendu les deux fils et l�ex-femme de Maurice Agnelet. Une famille bris�e.
Jean-No�l Gu�rini cr�e son propre parti
Politique      09/04/2014 - 12:03:04
ENTREPRENEUR POLITIQUE - L�ex-cacique du PS des Bouches-du-Rh�ne a quitt� le navire socialiste pour fonder son propre parti.
Interruption du JT : France 2 ouvre une enqu�te
M�dias-T�l�      09/04/2014 - 11:49:27
QUE S'EST-IL PASSE ? � Le plateau du journal t�l�vis� de David Pujadas a �t� envahi par des intermittents mardi soir.
La nouvelle arme anti-fraude de la SNCF
Economie      09/04/2014 - 11:17:41
DIVISE PAR HUIT - La SNCF a d�cid� de faire passer la limite de validit� d'un ticket de train de deux mois � une semaine.
20h16
