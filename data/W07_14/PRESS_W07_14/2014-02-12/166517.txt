TITRE: Apr�s un saut de 98,5 m, Jason Lamy Chappuis reste confiant avant les 10 km de fond - France 3 Franche-Comt�
DATE: 2014-02-12
URL: http://franche-comte.france3.fr/2014/02/12/apres-un-saut-de-985-m-jason-lamy-chappuis-reste-confiant-avant-les-10-km-de-fond-413813.html
PRINCIPAL: 166514
TEXT:
Sotchi 2014
Apr�s un saut de 98,5 m, Jason Lamy Chappuis reste confiant avant les 10 km de fond
Rien est perdu, rien est gagn�, tout reste � faire ! Arriv� 8eme � l'issue de l'�preuve de saut du combin� nordique, Jason Lamy Chappuis va devoir sortir tout ce qu'il a pour tenter un podium. A Bois d'Amont, les supporters restent confiants.
Isabelle Brunnarius
Publi� le 12/02/2014 | 11:51, mis � jour le 12/02/2014 | 18:24
� ps
+  petit
Fran�ois Braud partira 24 eme, Sebastien Lacroix 28 eme et Maxime Laheurte 16 eme.
Ce matin � Bois d'Amont, village de S�bastien Lacroix et Jason Lamy Chappuis, les employeurs �taient plut�t sympas avec leurs salari�s : pas de probl�me pour se rendre � la salle polyvalente assister � la retransmission en direct de l'�preuve de saut sur le tremplin K90. Les petits du centre de loisirs sont m�me aux premi�res loges ! Les medias nationaux sont aussi au rendez-vous dans ce berceau du combin� nordique.
Cr�celles, drapeaux, maquillages, les Bois d'Amoniers ont ressorti leur panoplie porte-bonheur. Un signe : d�j� en 1992, Fabrice Guy �tait porte drapeau de la d�l�gation fran�aise ! lls �taient l� aussi en 2010 pour f�ter la m�daille d'or de Jason � Vancouver.
Et si jamais Jason, Sebastien, Maxime et Fran�ois avaient eu une liaison directe dans leur casque avec la petite salle polyvalente, ils auraient entendu une clameur digne des grands jours :
Ambiance � Bois d'Amont pendant le saut de Jason Lamy Chappuis
