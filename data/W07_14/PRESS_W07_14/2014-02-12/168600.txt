TITRE: Premier League: Arsenal neutralis� par United  - Football - Sports.fr
DATE: 2014-02-12
URL: http://www.sports.fr/football/angleterre/scans/premier-league-arsenal-neutralise-par-united-1009391/
PRINCIPAL: 0
TEXT:
12 février 2014 � 22h35
Mis à jour le
12 février 2014 � 23h09
Giflé à Liverpool (5-1), samedi dernier, Arsenal n’est pas parvenu à dominer Manchester United (0-0), mercredi soir, dans le cadre de la 26e journée de Premier League. Szczesny a même sauvé les pensionnaires de l'Emirates Stadium en repoussant une tête de Van Persie à 10 minutes de la fin de la partie.
Les Gunners ne font pas néanmoins pas une mauvaise opération. Chelsea , leader à un point, avait été tenu en échec contre West Brom (1-1), mardi soir, tandis que City, désormais à 2 longueurs, a vu son match contre Sunderland reporté par de fortes pluies.
Les Red Devils, eux, restent 7e à désormais 8 unités de Tottenham , 5e. Les Spurs ont infligé une correction à Newcastle (0-4), grâce à un doublé d’Adebayor (19e) et des réalisations de Paulinho (53e) et Chadli (88e).
