TITRE: Bourgogne : l'�pid�mie de grippe est bien install�e  - France 3 Bourgogne
DATE: 2014-02-12
URL: http://bourgogne.france3.fr/2014/02/12/bourgogne-l-epidemie-de-grippe-est-bien-installee-414087.html
PRINCIPAL: 167749
TEXT:
+  petit
Le seuil �pid�mique��(qui est de 176 cas pour 100 000 personnes)�est franchi pour la deuxi�me semaine cons�cutive en France m�tropolitaine. L'Institut de veille sanitaire (InVS) souligne que l'�pid�mie de grippe est d�sormais "confirm�e". Elle est marqu�e par une augmentation du nombre de consultations pour syndromes grippaux. Les hospitalisations et les admissions en r�animation pour grippe sont aussi en hausse.
Quatre d�c�s ont �t� observ�s la semaine derni�re, ce qui porte � 13 le nombre total des d�c�s par grippe observ�s depuis le 1er novembre dernier.�Par ailleurs, 46 nouveaux cas graves de grippe ont �t� admis en r�animation la semaine derni�re, ce qui porte � 165 le nombre de cas graves depuis le 1er novembre.
Selon le r�seau de surveillance Sentinelles-Inserm , 274 000 personnes auraient consult� un m�decin pour des sympt�mes li�s � la grippe au cours des deux derni�res semaines en France.
La Bourgogne est en premi�re ligne
Du 3 au 9 f�vrier 2014, le taux d'incidence des consultations pour syndromes grippaux � partir des donn�es du R�seau Unifi� (donn�es conjointes du R�seau des Grog et Sentinelles) est de 375/100 000 .�Les r�gions Bourgogne, Aquitaine, Languedoc-Roussillon, Rh�ne-Alpes et Champagne-Ardennes avaient les taux d�incidence les plus �lev�s, sup�rieurs � 500/100 000 .
L�an dernier, la grippe �tait arriv�e beaucoup plus t�t, d�s le mois de novembre 2012 et le virus avait s�vi pendant 13 semaines. Cela s��tait traduit par 4 millions et demi de consultations, plus de 28 000 passages aux urgences� Son bilan s��tait �lev� � 153 d�c�s en r�animation, dont 15 en Bourgogne et 9 en Franche-Comt�.
