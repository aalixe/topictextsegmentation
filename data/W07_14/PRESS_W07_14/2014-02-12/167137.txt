TITRE: Football Monaco - L'AS Monaco donne des nouvelles de Falcao - Ligue 1 - Foot 01
DATE: 2014-02-12
URL: http://www.foot01.com/equipe/monaco/l-as-monaco-donne-des-nouvelles-de-falcao,136073
PRINCIPAL: 167136
TEXT:
L'AS Monaco donne des nouvelles de Falcao
Publi� Mercredi 12 F�vrier 2014 � 13h01 Dans�: Monaco , Ligue 1 .
S�rieusement bless� au genou apr�s un tacle pris lors du match de Coupe de France entre Monaco et Chasselay, Falcao a �t� op�r� le 25 janvier. Ce mercredi, le club mon�gasque a donn� quelques nouvelles de l�attaquant colombien. � Radamel Falcao a aujourd�hui fini avec succ�s la premi�re phase de la r�cup�ration suite � l�op�ration de son genou gauche.L�AS Monaco FC et le joueur tiennent � remercier chaleureusement toutes les personnes qui se sont occup�es de Falcao � Porto, notamment le service m�dical du Docteur Jose Carlos Noronha et le club du FC Porto. Radamel Falcao va poursuivre � partir de ce jeudi la deuxi�me phase de sa r�cup�ration � la clinique sp�cialis�e du sport de Madrid en �troite collaboration avec le service m�dical de l�AS Monaco FC �, pr�cise le club de la Principaut� dans un communiqu�.
