TITRE: Taxis : le médiateur appelle au 'calme'
DATE: 2014-02-12
URL: http://www.lefigaro.fr/flash-eco/2014/02/12/97002-20140212FILWWW00076-taxis-le-mediateur-appelle-au-calme.php
PRINCIPAL: 166158
TEXT:
le 12/02/2014 à 09:35
Publicité
Le député PS Thomas Thévenoud, nommé médiateur par le gouvernement pour apaiser le conflit entre les taxis et les voitures avec chauffeur (VTC), a appelé aujourd'hui toutes les parties "au calme" et à se mettre autour de la table pour trouver une solution. "J'appelle au calme, j'appelle tout le monde à venir me voir autour de la table pour discuter, pour s'entendre, pour passer des compromis et trouver un nouveau système", a déclaré le député de Saône-et-Loire sur RMC et BFM-TV. "Le gouvernement précédent (...) a décidé d'ouvrir le marché aux VTC. Aujourd'hui on voit bien qu'il a mis le feu aux poudres", a estimé le député.
Selon lui, la législation n'est pas "viable" et "il faut inventer un nouveau système où chacun puisse vivre de son travail". "Il ne faut pas s'interdire de faire évoluer le système, en même temps il faut respecter les chauffeurs de taxis qui ont payé parfois très cher" leur licence, a-t-il estimé.
LIRE AUSSI :
