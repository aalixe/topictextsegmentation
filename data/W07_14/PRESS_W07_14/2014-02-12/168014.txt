TITRE: Libert� de la presse : RSF tacle le Maroc, El Khalfi r�pond
DATE: 2014-02-12
URL: http://www.medias24.com/A-suivre/9089-Liberte-de-la-presse-RSF-tacle-le-Maroc-El-Khalfi-repond.html
PRINCIPAL: 168012
TEXT:
R�agir Classer PDF Imprimer
Selon Reporters sans fronti�res, ��les autorit�s confondent volontiers journalisme et terrorisme depuis l�affaire Ali Anouzla.�� Le ministre de la Communication r�agit vivement au classement qui place le Maroc en 136e position du 180 pays.
Globalement, le rapport�r�v�le une d�gradation importante de la situation dans des pays aussi divers que les �tats-Unis, la R�publique centrafricaine et le Guatemala, et � contrario des am�liorations notables en Bolivie, en Equateur et en Afrique du Sud. Pour sa part, le Maroc demeure un mauvais �l�ve.
Le principal argument avanc� par l�ONG concerne l�affaire Anouzla. Pour elle, �cette affaire illustre l�inqui�tant amalgame que font les autorit�s marocaines entre travail journalistique et incitation � l�ex�cution d�acte terroristes �.
Le gouvernement emmen� par le PJD n�est pas �pargn�. � Les autorit�s marocaines, plac�es sous la houlette des islamistes depuis les �lections de 2011, tardent � concr�tiser les promesses de r�formes annonc�es depuis le r�f�rendum constitutionnel de 2011 � poursuit RSF.
En r�action, le porte-parole du gouvernement et ministre de la Communication n�a pas manqu� de r�agir via un communiqu�. ��Quoique le rapport de RSF n'ait pas class� le Maroc parmi les pays ayant connu une r�gression, la conclusion g�n�rale qui en d�coule demeure �trange, manque de pr�cision et ne refl�te pas la r�alit� de la pratique journalistique au Maroc��, estime-t-il.
Il ajoute que ��ce rapport se base sur une seule affaire, c�est pourquoi nous estimons que ce rapport se fonde sur des impressions plut�t que sur des indicateurs concrets. Il suffit de noter que plusieurs pays qui ont connu des guerres et o� les journaux se voient confisqu�s sont mieux class� que le Maroc��.
�
