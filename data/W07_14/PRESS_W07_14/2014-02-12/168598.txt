TITRE: Nice - Monaco (0-1), le résumé du match, Coupe de France - 8es de finale , Football - L'Equipe.fr
DATE: 2014-02-12
URL: http://www.lequipe.fr/Football/match/314061
PRINCIPAL: 0
TEXT:
120
�
Claudio Ranieri savait pourquoi il voulait Berbatov : � moins de six minutes du terme de la seconde prolongation, l'attaquant bulgare surgit et envoie Monaco en quart de finale de la Coupe de France. R�duit � 10, Nice sort la t�te haute.
118
�
Les Mon�gasques font tranquillement tourner le ballon en cette fin de match. Il leur reste deux minutes � tenir.
116
�
Apr�s s'�tre fait chiper le ballon par Raggi dans la surface mon�gasque, Amavi tacle violemment le lat�ral italien. L'arbitre siffle logiquement en faveur de l'AS Monaco.
114
�
But de Dimitar Berbatov ! Depuis la droite, � 30 m�tres du but d'Hassen, Ocampos adresse un centre au premier poteau. Berbatov surgit et pousse le ballon du bout du pied dans le but ni�ois.
112
�
La fatigue gagne les organismes. Raggi n'arrive pas � aller au bout de sa course pour emp�cher un ballon mon�gasque de fuser en touche.
110
�
La parade de Hassen ! Servi dans la surface, Berbatov remet en retrait pour James, aux 18 m�tres plein axe. Le Colombien croise sa frappe du gauche mais le portier azur�en plonge bien.
108
�
Claude Puel proc�de � son troisi�me et dernier changement : Neal Maupay est remplac� par Christian Br�ls
106
�
L'arbitre de la rencontre, Monsieur Rainville, siffle le coup d'envoi de la seconde p�riode de la prolongation.
105
�
Au terme de la premi�re mi-temps de la prolongation, les Ni�ois ont l'air compl�tement cuit. Si les Mon�gasques n'ont pas encore forc� la d�cision, les entr�es de Berbatov, Rodriguez et Rivi�re ont fait du bien aux hommes de Ranieri
103
�
Sur le banc, Claudio Ranieri est indign� par la prestation de ses joueurs. Le coach italien fustige le d�chet technique grandissant qui alt�re le jeu de son �quipe.
102
�
Ocampos, encore lui, est servi sur la gauche avant de centrer au premier poteau. Berbatov surgit mais sa tentative n'est pas assez appuy�e. Hassen parvient � s'emparer du ballon.
101
�
Sur le c�t� gauche, � l'angle de la surface ni�oise, Ocampos provoque deux ni�ois avant de centrer au second poteau. Genevois, sur la trajectoire, exp�die le ballon en corner. Lequel ne donne rien.
99
�
Sur son coup-franc, James enroule du pied gauche mais ne l�ve pas assez son ballon. Sa tentative est contr�e par le mur ni�ois
98
�
Mendy fauche James, � 20 m�tres du but de Hassen, plein axe. Excellent coup-franc � venir pour les Mon�gasques.
97
�
Sur coup-franc, � 37 m�tres du but de Romero l�g�rement sur la droite, Bauth�ac cherche une t�te ni�oise au second poteau mais le portier argentin est plus prompt sur le ballon.
95
�
Aux abords de la surface ni�oise, sur la gauche, Ocampos d�pose le ballon sur la cr�ne de Berbatov mais la t�te du Bulgare est d�vi�e par un Ni�ois en corner. Lequel ne donne rien.
93
�
Sur la gauche, Elderson lance Rivi�re dans la profondeur mais Romain Genevois, imp�rial depuis le d�but de la rencontre, lui chaparde le ballon dans les pieds.
91
L'arbitre de la rencontre, Monsieur Ranville, siffle le coup d'envoi de la premi�re prolongation.
90+2
�
Pourtant en sup�riorit� num�rique, les Mon�gasques, trop impr�cis dans le dernier geste, n'ont jamais r�ussi � prendre en d�faut une courageuse formation ni�oise. Il leur reste 30 minutes pour forcer la d�cision.
90+1
�
La reprise de James ! Le Colombien, servi au point de penalty, ex�cute une merveille de reprise acrobatique, mais Hassen, id�alement plac�, d�tourne la frappe de l'ancien joueur du FC Porto.
90
�
Sur la droite, depuis le camp mon�gasque, James lance Berbatov dans la profondeur mais le Bulgare est siffl� en position de hors-jeu.
88
�
Toulalan tire Maupay au sol, aux abords de la surface mon�gasque, sur la droite. Curieusement, l'arbitre ne bronche pas.
86
�
Bruins h�rite d'un ballon mal renvoy� par la d�fense mon�gasque. Le N�erlandais tente un lob sur Romero, mais sa tentative est d�vi�e et fr�le la transversale du portier argentin.
84
�
Sur coup-franc, James d�pose le ballon sur la t�te de Berbatov, au point de penalty. Le Bulgare, qui s'�tait d�fait du marquage de Genevois, ne parvient pas � cadrer sa tentative.
83
�
Toulalan acc�l�re sur la gauche et est accroch� � 35 m�tres du but de Hassen. Bon coup-franc � venir pour les Mon�gasques.
81
�
Claudio Ranieri veut en finir : Anthony Martial, qui a consid�rablement g�n� la d�fense ni�oise par sa vitesse, est remplac� par Emmanuel Rivi�re
79
�
Sur coup-franc, � 35 m�tres plein axe, Bauth�ac enroule sa frappe du pied gauche mais trouve directement les gants de Romero.
78
�
Carton jaune pour J�r�my Toulalan, coupable d'un tacle par derri�re sur Pied, qui amor�ait une contre-attaque.
77
�
La parade de Romero ! A la suite d'un corner, Bodmer h�rite du ballon dans les 5,5 m�tres, dans la surface mon�gasque. �tonnement seul, l'ancien Parisien cadre sa frappe mais Romero ex�cute une parade magnifique.
75
�
Si les Mon�gasques sont en sup�riorit� num�rique, les Ni�ois font jeu �gal avec les hommes de Ranieri. L'expulsion de Kolodziejczak � la 47e ne se fait presque pas sentir.
73
�
Alert� par Martial dans la surface ni�oise, sur la droite, Berbatov veut remettre pour l'ancien Lyonnais au point de penalty, mais Amavi contre le ballon de la main. L'arbitre ne bronche pas.
71
C�t� Ni�ois, J�r�my Pied est remplac� par Luigi Bruins
70
Ranieri proc�de � son deuxi�me changement : Yannick Ferreira Carrasco est remplac� par James Rodriguez
69
�
Quel geste de Bodmer. Press� par Martial et Berbatov, l'ancien Parisien parvient � s'en sortir en ex�cutant un r�teau parfait.
67
�
Martial est servi � 40 m�tres du but, plein axe. L'ancien Lyonnais acc�l�re, �limine Mendy et tente le grand pont sur Genevois. Mais pour le coup, a �t� un peu gourmand. Le d�fenseur ni�ois met le pied sur le ballon.
65
�
A 30 m�tres, plein axe, Berbatov tente de lancer Martial dans la surface ni�oise, mais Genevois coupe la trajectoire et �carte le danger.
63
Claudio Ranieri proc�de � son premier changement : Mounir Obbadi est remplac� par Dimitar Berbatov.
62
�
A 30 m�tres, plein axe, Kondogbia tente un peu d�sesp�r�ment une frappe du droit, laquelle est beaucoup trop �cras�e pour inqui�ter Hassen.
61
�
La t�te d'Ocampos ! Depuis la droite, � 30 m�tres, Raggi d�pose un centre du droit sur la cr�ne du jeune argentin, qui avait surgi au premier poteau. La t�te d�crois�e d'Ocampos flirte avec le montant gauche de Hassen.
59
�
A l'issue d'un une-deux parfaitement ex�cut� aux abords de la surface ni�oise, plein axe, Obbadi d�coche une frappe s�che du droit, mais sa tentative fr�le le montant gauche de Hassen.
57
�
La t�te de Maupay ! Sur un coup-franc frapp� du gauche par Bauth�ac, � 35 m�tres sur la gauche, Pied, aux 18 m�tres d�vie de la t�te pour Maupay au second poteau. La t�te du jeune ni�ois n'est pas assez appuy�e.
56
�
Beaucoup de tension entre les formations depuis le retour du vestiaire. Le match est hach� par de nombreuses fautes.
55
Carton jaune pour J�r�my Pied, �galement sanctionn� pour un tacle par derri�re sur Kondogbia.
54
Carton jaune pour Mounir Obbadi, coupable d'un tacle par derri�re sur Obbadi.
53
�
L'expulsion de Kolodziejczak para�t s�v�re. Profitant de ce petit geste d'humeur, Ricardo Carvalho a clairement exag�r� sa r�action.
51
�
Son �quipe r�duite � 10, Claude Puel proc�de � un r�ajustement tactique : Alexy Bosetti est remplac� par Jordan Amavi
49
�
Carrasco lance Ocampos sur la droite, mais la passe du belge est mal dos�e. La transmission du belge finit en touche.
47
�
Thimoth�e Kolodziejczak est expuls� ! Le lat�ral ni�ois a mis une claque Ricardo Carvalho, lequel venait de pousser Maupay dans la surface mon�gasque.
46
�
L'arbitre de la rencontre, Monsieur Rainville, siffle le coup d'envoi de la deuxi�me p�riode. Ce sont les Ni�ois qui engagent.
45
�
Dominateurs mais trop impr�cis dans leurs transmissions, les Mon�gasques ont trop souvent p�ch� dans la derni�re passe et n'ont quasiment pas eu d'opportunit�s. Le pressing des Ni�ois, agressifs et g�n�reux dans l'effort, a g�n� les hommes de Ranieri.
44
�
Aux abords de la surface mon�gasque, sur la gauche, Bosetti adresse un centre de gauche au second poteau. Pied, qui arrivait lanc�, manque de faucher Romero, lequel s'�tait empar� de ballon.
43
�
Servi � 30 m�tres du but gard� par Hassen, plein axe, Ferreira Carrasco n'est pas attaqu� et a tout le temps d'armer sa frappe. Le jeune belge d�coche un tir sec du droit mais le portier ni�ois se couche bien.
41
�
Servi sur la droite par Ferreira Carrasco � 30 m�tres, Martial repique dans l'axe et frappe du droit aux abords de la surface ni�oise. La tentative de l'ancien Lyonnais est trop �cras�e pour inqui�ter Hassen.
39
�
Depuis la gauche, Obbadi parvient � servir Raggi � l'entr�e de la surface ni�oise, qui arrivait en trombe sur la droite. Le lat�ral italien est semble t-il fauch� par Bodmer, mais l'arbitre ne bronche pas.
37
�
Anthony Martial vient de prendre une soufflante par son coach, Claudio Ranieri. L'entra�neur italien lui reproche notamment de courir dans tous les sens.
35
�
Le festival de Kolodziejczak ! Depuis la gauche, � 38 m�tres, le lat�ral ni�ois slalome trois Mon�gasques avant de servir Maupay, aux 18 m�tres. Le jeune attaquant remet aussit�t pour ''Kolo'' au point de penalty, n'appuie pas assez sa frappe du droit
33
�
Compos� de Berbatov, James et Moutinho, le banc mon�gasque a fi�re allure. Germain, Abidal et Kurzawa regardent le match depuis les tribunes.
32
�
A 40 m�tres, Kondogbia alerte Martial dans la profondeur, sur la gauche. A l'angle de la surface ni�oise, le joueur de 18 ans provoque Mendy, mais l'ancien Mon�gasque ex�cute un tacle parfait.
30
�
Depuis le rond central, Kondgbia remet en retrait pour Carvalho mais n'appuie pas assez sa passe. Il s'en est fallu de peu pour que Maupay, � l'aff�t, s'empare du ballon. Le d�fenseur portugais parvient � rattraper l'erreur commise par son co�quipier.
28
�
Sur la gauche, Elderson sert Ocampos dans la surface ni�oise. L'Argentin tente d'�liminer Bodmer, mais l'ancien Lyonnais ne se jette pas et d�vie en corner. Lequel ne donne rien pour les Mon�gasques.
27
�
Sur la gauche, Elderson lance Ferreira Carrasco, mais le jeune belge est repris par Puel avant la ligne de sortie de but. L'ailier mon�gasque a du mal � exister depuis le d�but de la rencontre.
25
�
Sur la droite, Gr�goire Puel profite du boulevard laiss� par Elderson et acc�l�re. A 30 m�tres, il adresse un centre du droit dans la surface mon�gasque mais Romero sort bien et capte le ballon.
23
�
Ocampos et Raggi tente un une-deux sur le c�t� droit, � 35 m�tres, mais Kolodziejczak intercepte le ballon.
21
�
Elderson Echiejile, le lat�ral mon�gasque, a d�j� jou� une finale de Coupe de France. C'�tait avec Rennes, en 2009. Les Bretons s'�taient inclin�s face � Guingamp sur le score de 1-2.
19
�
Obbadi alerte Ocampos dans la profondeur, mais Genevois coupe la trajectoire. Les Mon�gasques p�chent encore dans la derni�re passe.
17
�
Martial se faufile dans la surface ni�oise, sur la droite. L'international espoir �limine deux joueurs dans un mouchoir de poche avant de frapper du droit en angle ferm�. Hassen stoppe sa tentative.
16
�
Alert� sur la gauche, Elderson acc�l�re jusqu'au poteau de corner ni�ois et adresse un centre au second poteau. Ocampos est � la r�ception, mais il commet une faute sur Genevois.
15
�
Les chiffres confirment la domination mon�gasque, toutefois encore st�rile. Les hommes de Ranieri poss�dent le ballon 60% du temps.
13
�
Frapp� du pied droit par Ferreira Carrasco, le coup-franc ne donne rien. Le ballon est all� heurter le mur ni�ois.
12
�
Au tour de Martial d'�tre irr�guli�rement stopp� � 35 m�tres du but de Hassen, plein axe. Bon coup-franc � venir pour les Mon�gasques.
11
�
Sur le c�t�, 38 m�tres, Bosetti parvient � passer entre deux mon�gasques mais est finalement fauch�. Le coup-franc, frapp� du gauche par Bauth�ac, ne donne rien.
9
�
Servi par Martial aux abords de la surface ni�oise l�g�rement sur la gauche, Carrasco se met sur sur pied droit et d�coche une frappe puissante qui ne trouve pas le cadre.
7
�
A 30 m�tres, plein axe, Kondogbia alerte Elderson dans la surface ni�oise sur la gauche. Le lat�ral mon�gasque centre fort du gauche devant le but mais Ocampos, qui avait surgi au second poteau, est trop court.
5
�
Mendy adresse une merveille de passe transversale pour Kolodziejczak, sur la gauche. Le lat�ral ni�ois, � proximit� de la surface mon�gasque, veut remettre en retrait mais il jauge mal sa passe. Les hommes de Ranieri amorcent une contre-attaque.
3
�
A 35 m�tres, plein axe, Obbadi lance Raggi dans la surface ni�oise, sur la droite. Un d�fenseur ni�ois coupe la trajectoire.
3
�
Les Mon�gasques n'ont pas l�ch� le ballon depuis le d�but de la rencontre. Les joueurs de Ranieri semblent d�cid�s � faire le jeu.
1
�
L'arbitre de la rencontre, Monsieur Rainville, siffle le coup d'envoi de la premi�re p�riode. Ce sont les Mon�gasques qui engagent.
0
�
Un peu plus de 20000 personnes garnissent les trav�es de l'Allianz Riviera. La pelouse, chang�e � deux reprises depuis le d�but de la saison, est parfaite.
0
�
La derni�re confrontation entre nos deux adversaires du soir remonte au 3 d�cembre dernier. Les Mon�gasques s'�taient facilement impos�s sur le score de 3-0, gr�ce � des buts de Rivi�re, Ocampos et James Rodriguez.
0
�
C�t� Mon�gasque, Isimat-Mirin, Elderson, Obbadi, Martial et Ferreira Carrasco, sur le banc contre le PSG, sont align�s par Claudio Ranieri.
0
�
Pour Monaco comme pour Nice, la priorit� va au championnat. Pour preuve : Puel et Ranieri profite de cette confrontation pour faire massivement tourner leur effectif. C�t� Ni�ois, Pied, Maupay et Hassen sont titularis�s.
0
�
Les deux clubs sudistes �voluent dans des sph�res �conomiques compl�tement antagonistes. Le budget de l'opulent club mon�gasque est �valu� � 130 millions d'euros, tandis que celui de l'OGCN est estim� � 32. Soit une diff�rence de 98 millions d'euros.
0
�
Solide 2e en L1, Monaco reste sur une performance convaincante face au leader parisien. A l'inverse, les Azur�ens ont fourni une prestation inqui�tante samedi dernier face � Valenciennes, 18e du championnat (2-1). L'OGC Nice pointe � la 13e place.
0
�
Au tour pr�c�dent, les hommes de Ranieri s'�taient ais�ment impos�s � Chasselay. Falcao, bless� pour 6 mois, y avaient laiss� des plumes. Les Ni�ois, eux, avaient triomph� de l'OM au Stade V�lodrome, au terme d'une rencontre riche en buts (5-4).
0
�
Bonsoir et bienvenue � tous pour suivre en direct le d�placement des Mon�gasques sur la pelouse de l'OGC Nice, pour le compte des 8e de finale de la Coupe de France.
