TITRE: Affaire Dassault: l'UMP soutient son s�nateur - L'Express
DATE: 2014-02-12
URL: http://www.lexpress.fr/actualite/politique/affaire-dassault-l-ump-soutient-son-senateur_1323322.html
PRINCIPAL: 168202
TEXT:
Voter (0)
� � � �
Le bureau du S�nat a lev� mercredi sans surprise l'immunit� parlementaire du s�nateur et avionneur Serge Dassault, demand�e par la justice dans le cadre d'une enqu�te sur des achats pr�sum�s de voix � Corbeil-Essonnes.
afp.com/Eric Piermont
La lev�e de l'immunit� de Serge Dassault a �t� vot�e ce mercredi par les s�nat eurs de gauche. De gauche seulement, car leurs coll�gues de l'opposition ont "refus� de participer au vote". Ils se sont fendus d'un communiqu� de presse pour expliquer leur attitude. �
En boycottant le vote, les s�nateurs UMP assurent avoir voulu d�noncer la "manoeuvre inspir�e par le pr�sident du S�nat" . Selon eux, la "raison r�elle" de la r�union de ce mercredi n'�tait pas tant de d�cider la lev�e de l'immunit� de Serge Dassault , qu'il avait lui-m�me demand�e, mais "de changer le mode de scrutin du bureau en rempla�ant le vote � bulletin secret par un vote public ". Ils estiment qu'� travers ce changement de vote, "la tradition fran�aise est mise � mal".�
Pour Marie-H�l�ne Des Esgaulx , s�natrice UMP membre du bureau du S�nat contact�e par L'Express, cette d�cision de Jean-Pierre Bel de passer au vote � main lev�e est "un recul du pouvoir l�gislatif". "Le parlementaire doit pouvoir d�cider en toute libert� et en son �me et conscience". Or, elle estime que ce nouveau mode de vote va obliger les parlementaires "� �couter ce que dit leur parti" et donc � suivre un conseil de vote. "On est en train de nous b�illonner", r�sume-t-elle.�
Les s�nateurs, et Dassault lui-m�me, sont confiants
Apr�s deux refus de lever l'immunit� du s�nateur , l'issue du troisi�me vote de ce mercredi ne faisait pas de doute. Alors les s�nateurs UMP "prennent acte de la d�cision du bureau du S�nat", tout en assurant soutenir leur coll�gue.�
"Nous tenons � assurer � notre coll�gue et ami Serge Dassault de toute notre confiance et de notre profond attachement ", ont �crit les s�nateurs dans un communiqu� de presse. Et de flatter Serge Dassault, "un homme qui contribue au rayonnement industriel de la France dans le monde entier". �
Autant de soutien pour un s�nateur qui ne semble pas en avoir besoin. Apr�s l'annonce de la lev�e de son immunit�, Serge Dassault a r�agi dans une vid�o visible sur son site. " Le S�nat vient de lever mon immunit�, c'est exactement ce que j'ai demand� ", commence le s�nateur qui pourra d�sormais �tre entendu en tant que gard� � vue dans des affaires d'achats de voix � Corbeil-Essonnes. "Je suis tr�s content et je vais enfin pouvoir dire la v�rit� aux juges, et dire ce que je n'ai pas fait". �
