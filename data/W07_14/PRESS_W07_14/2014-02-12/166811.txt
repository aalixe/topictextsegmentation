TITRE: Pr�sidence de Radio France: les six candidats retenus - L'Express
DATE: 2014-02-12
URL: http://www.lexpress.fr/actualite/medias/presidence-de-radio-france-les-six-candidats-retenus_1323109.html
PRINCIPAL: 166810
TEXT:
Voter (0)
� � � �
Le CSA a d�voil� mercredi le nom des candidats pr�-s�lectionn�s pour briguer la pr�sidence de Radio France � partir de mai, date de la fin du mandat de l'actuel pr�sident Jean-Luc Hees.
afp.com/Bertrand Guay
Le  Conseil sup�rieur de l'audiovisuel (CSA) a retenu six candidats, sur 12  au d�part, pour briguer la pr�sidence de Radio France � partir de mai,  date de la fin du mandat de l'actuel pr�sident Jean-Luc Hees .�
Les six candidats sont Jean-Luc Hees, candidat � sa propre  succession , Martin Ajdari, le secr�taire g�n�ral de France T�l�visions, Mathieu Gallet , pr�sident de l'INA, Anne Durupty , directrice g�n�rale  d'Arte France, Anne Brucy , ex-directrice de France Bleu, et Philippe  Gault, pr�sident du Sirti (Syndicat des radios et t�l�visions  ind�pendantes).�
Vote � bulletin secret le 7 mars
Apr�s audition de ces six pr�tendants � huis clos, les neuf membres  du coll�ge du CSA choisiront le patron des radios publiques par un vote �  bulletin secret le 7 mars. Le Conseil retrouvera ainsi pour la premi�re fois le pouvoir de  nomination des patrons de l'audiovisuel public qu'il avait perdu sous la  pr�sidence Sarkozy. Une r�forme d�cid�e par le gouvernement Hollande qui a modifi� fin  2013 la loi qui pr�c�demment confiait ce choix directement au pr�sident  de la R�publique.�
Avec
