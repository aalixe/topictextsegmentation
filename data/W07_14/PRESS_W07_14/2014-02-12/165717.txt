TITRE: Eric Frenzel, le rival num�ro un - Jeux paralympiques Sotchi 2014
DATE: 2014-02-12
URL: http://www.francetvsport.fr/les-jeux-olympiques/combine-nordique/eric-frenzel-le-rival-numero-un-202461
PRINCIPAL: 0
TEXT:
Les jeux olympiques - Eric Frenzel, le rival num�ro un
Publi� le 12/02/2014 | 07:15, mis � jour le 12/02/2014 | 16:22
Eric Frenzel (PIERRE TEYSSOT / AFP )
Pour faire le doubl� et d�crocher un second sacre olympique en combin� nordique, le porte-drapeau tricolore Jason Lamy-Chappuis aura fort � faire. Il devra en effet surpasser Eric Frenzel, qui survole la discipline depuis plus d�un an. L�Allemand sera-t-il aussi dominateur � Sotchi qu�en Coupe du monde ? Pas si s�r�
Et si Jason Lamy-Chappuis, en plus d��tre un skieur hors-pair, �tait un bluffeur d�exception ? L�an pass�, davantage en retrait en Coupe du monde que les ann�es pr�c�dentes, le Fran�ais avait �bloui les championnats du monde de ski nordique � Val di Fiemme � dont il avait fait son principal objectif � en raflant quatre m�dailles en autant de courses, dont trois en or. Cette saison, actuel quatri�me du classement g�n�ral de la Coupe du monde, Jez se montre encore plus discret. Un peu comme s�il attendait son heure.
Mais s�il s�est sans doute r�serv� pour son grand moment, � Sotchi, o� il tentera ce mercredi de conserver sa couronne olympique sur petit tremplin, la mission s�annonce tr�s compliqu�e. La faute � l'explosion d'Eric Frenzel, qui apr�s avoir succ�d� au skieur fran�ais en remportant le dernier gros globe de cristal, multiplie les performances... au point d'�tre devenu le nouveau patron du combin� nordique.
L'Allemand tentera de profiter des difficult�s de Jez au saut
A 25 ans, toujours impeccable au saut et imp�rial en fond, l�Allemand aura � c�ur de faire mieux qu�en 2010. Encore trop tendre � Vancouver, il n�avait d�croch� qu�une m�daille de bronze (par �quipes), en terminant loin du podium � l�issue des �preuves individuelles (10e sur petit tremplin, 40e sur grand tremplin). Seulement, depuis le d�but de l�ann�e 2013, le ph�nom�ne fait preuve d�une constance exceptionnelle. Seul athl�te capable de bousculer Lamy-Chappuis aux derniers Mondiaux (sacr� sur grand tremplin), Frenzel est encore mont� en puissance depuis.
Cette saison, il rel�gue ainsi tous ses concurrents, parmi lesquels figurent le Japonais Akito Watabe et les Norv�giens Haavard Klemetsen, Magnus Krog et Mikko Kokslien, � plus de 300 points au classement g�n�ral de la Coupe du monde. Si Jez confirme ce mercredi�ses difficult�s au saut, c�est un boulevard vers l�or qui s�ouvrira pour l�Allemand. Faut-il encore qu'il supporte la pression des Jeux. Ce que Lamy-Chappuis, homme des grands rendez-vous, fait mieux que quiconque .
�
