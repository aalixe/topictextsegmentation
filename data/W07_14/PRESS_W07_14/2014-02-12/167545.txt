TITRE: Silicon Valley : les dossiers br�lants que n'abordera pas Fran�ois Hollande
DATE: 2014-02-12
URL: http://www.lemonde.fr/technologies/article/2014/02/12/silicon-valley-les-dossiers-brulants-de-m-hollande_4364829_651865.html
PRINCIPAL: 0
TEXT:
Silicon Valley : les dossiers br�lants que n'abordera pas Fran�ois Hollande
Le Monde |
| Par Sarah Belouezzane
Apr�s deux jours � Washington o� il �tait en visite d'Etat, Fran�ois Hollande s'est rendu mercredi 12 f�vrier dans la Silicon Valley, en Californie. Une premi�re depuis la visite de Fran�ois Mitterand dans l'Etat le plus innovant d'Am�rique en 1984.
Le chef de l'Etat doit rencontrer quelques personnalit�s phares de la Valley, fondateurs et patrons d'ex start-up devenus aujourd'hui mastodontes de l'�conomie num�rique : parmi eux, Jack Dorsey, cofondateur du r�seau social Twitter , Sheryl Sandberg, directrice op�rationnelle de Facebook ou encore Eric Schmidt, pr�sident ex�cutif de Google . Mais aussi Mitchell Baker, pr�sidente de la fondation Mozilla, la fondation qui g�re le c�l�bre navigateur Internet �ponyme, et Elon Musk, le milliardaire, confondateur du g�ant de paiement en ligne PayPal et propri�taire de Tesla, le fabricant de voitures �lectriques.
Bien que Fran�ois Hollande et Barack Obama aient salu�, dans une tribune commune publi�e lundi 10 f�vrier dans Le Monde , � un mod�le de coop�ration internationale�� entre les deux pays, les dissensions entre les deux pays sur le num�rique sont multiples. Les sujets qui f�chent, s'ils sont nombreux, ne devraient cependant pas �tre abord�s frontalement mercredi.
LA FISCALIT� DES G�ANTS DU NET
En visite surprise, jeudi 6 f�vrier, dans les locaux du site de vente en ligne VentePriv�e.com, Fran�ois Hollande a d�clar� que l'optimisation fiscale pratiqu�e par les g�ants du Net n'�tait � pas acceptable��. Les GAFA (Google, Amazon , Facebook , Apple ), de mani�re l�gale, y ont largement recours, par exemple en facturant des services � vendus en France depuis l' Irlande , pays fiscalement plus favorable aux entreprises .
Les grands groupes ne d�clarent alors au fisc fran�ais qu'une partie du chiffre d'affaires r�alis� en France. Et paient donc un imp�t sur les soci�t� s bien inf�rieur � ce qu'ils devraient payer . A en croire l'h�bdomadaire Le Point, Google se serait d'ailleurs vu notifier un redressement fiscal de un milliard d'euros.
Le dernier en date � avoir choisi l'Irlande est le g�ant mauve Yahoo! qui a d�cid� mercredi 5 f�vrier de facturer la quasi-totalit� des services vendus en France depuis son si�ge de Dublin.
Lire aussi :� La r�organisation de Yahoo! coupe l'herbe sous le pied des futurs contr�les fiscaux �
La plupart des g�ants am�ricains font aujourd'hui l'objet d'un contr�le fiscal en France. Mais Fran�ois Hollande n'entend pas aller � la rencontre des g�ants de l'internet en ��inspecteur des imp�ts ��, a soulign� la ministre fran�aise d�l�gu�e � l'�conomie num�rique, Fleur Pellerin, qui l'accompagne dans son p�riple am�ricain.�������������������������������
LA PROTECTION DES DONN�ES PRIV�ES
Cela fait d�j� quelque temps que la France et la Commission europ�enne se battent contre l'usage que font les�� g�ants du Net des donn�es personnelles de leurs utilisateurs, sans les en informer .
La Commission de l'informatique et des libert�s ( CNIL ) a r�cemment condamn� le moteur de recherche Google � 150 000 euros d'amende pour non respect des droits de la vie priv�e de ses utilisateurs fran�ais. L'Autorit� reproche � Google une nouvelle politique de gestion des donn�es personnelles, mise en place au printemps 2012. Le g�ant de l'Internet s'autorise � croiser toutes les informations de ses utilisateurs, quel que soit le service qu'ils utilisent (Youtube, le moteur de recherche, la cartographie). Des proc�dures qui pourraient aboutir � des condamnations similaires sont en cours dans la plupart des pays europ�ens et devraient aboutir dans quelques mois.
L'AFFAIRE PRISM
La question de la protection des donn�es est d'autant plus sensible depuis la r�v�lation par Edward Snowden, ex-agent de la NSA, de l'affaire Prism. Un syst�me qui permet � l'agence de s�curit� am�ricaine d' espionner les internautes du monde entier en acc�dant directement et sans autorisation aux serveurs des g�ants du Net.
Pour t�moigner de leur bonne foi, ceux-ci ont r�cemment demand� aux autorit�s am�ricaines la possibilit� de communiquer sur le nombre de demandes d'informations qui leur sont faites par la NSA.
Lireaussi : Les g�ants du Net publient le nombre de requ�tes secr�tes de la NSA
LA PROTECTION DE L'EXCEPTION CULTURELLE FRAN�AISE
Parmi les th�mes probablement �voqu�s par M. Hollande, il y a aura le sujet sensible de la culture . Celle-ci ne fait pas partie des discussions sur l'accord de libre-�change entre l' Union europ�enne et les Etats-Unis. Mais avec les rumeurs de l'arriv�e prochaine de Netflix en France, elle se pose aujourd'hui avec insistance. En effet, s'il s'installe dans l'hexagone, le site de Vid�o � la demande par abonnement (SVOD) va-t-il financer la production fran�aise ou choisira-t-il d' op�rer depuis un pays voisin pour y �chapper?
