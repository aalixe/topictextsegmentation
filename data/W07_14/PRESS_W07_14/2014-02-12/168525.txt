TITRE: Gisin-Maze, un duo dans l'histoire - JO 2014 - Sports.fr
DATE: 2014-02-12
URL: http://www.sports.fr/jo-2014/ski-alpin/articles/jo-descente-gisin-et-maze-entre-dans-l-histoire-1008992/
PRINCIPAL: 168523
TEXT:
12 février 2014 � 13h19
Mis à jour le
12 février 2014 � 19h11
Tina Maze et Dominique Gisin partagent la médaille d’or de la descente après avoir réalisé le même chrono mercredi à Sotchi. Si ce n’est pas une première dans l’histoire des Jeux Olympiques, le ski alpin ne regorge pas de tels précédents. C’est même historique dans le cas d’une première place...
Tina Maze et Dominique Gisin ont réalisé le même chrono ce mercredi matin lors de la descente des Jeux Olympiques de Sotchi: 1’41’57. La Slovène et la Suissesse se partagent donc l’or olympique devant la Suissesse Lara Gut troisième.
L'histoire des JO d’Hiver recense 25 podiums avec ex aequo depuis 1924, dont huit désormais pour un titre olympique. Trois précédents concernent le patinage de vitesse, imité par la luge à une reprise, le bobsleigh à une reprise et le ski de fond une fois également en 2002. Ils sont rejoints par le résultat des couples en patinage artistique en 2002 mais cela résultait du reclassement des Canadiens Salé-Pelletier suite au scandale de notation qui avait favorisé le duo russe Berezhnaya-Sikharulidze à Salt Lake City.
Ce résultat ex-aequo n’est pas une première dans l’histoire olympique pour le ski alpin mais il s’agit en revanche de la première fois qu’il concerne l’octroi d’une médaille d’or dans cette discipline. Ce scénario n’était en plus jamais arrivé lors d'une descente dames. Les précédents concernent la médaille de bronze de la descente messieurs en 1948 (partagée entre deux Suisses, Karl Molitor et Rolf Olinger), la deuxième place du slalom géant dames en 1964 (partagée entre la Française Christine Goitschel et l’Américaine Jean Saubert), la deuxième place du slalom géant dames en 1992 à Albertville (entre l’Autrichienne Anita Wachter et l’Américaine Diann Roffe) et la deuxième place du Super-G messieurs de Nagano en 1998 (entre le Suisse Didier Cuche et l’Autrichien Hans Knauss).
