TITRE: Ayrault veut faire du vieillissement de la France une "chance" - Capital.fr
DATE: 2014-02-12
URL: http://www.capital.fr/a-la-une/actualites/ayrault-veut-faire-du-vieillissement-de-la-france-une-chance-910287
PRINCIPAL: 167353
TEXT:
Ayrault veut faire du vieillissement de la France une...
Ayrault veut faire du vieillissement de la France une "chance"
Source : Reuters
Tweet
Jean-Marc Ayrault a pr�sent� mercredi les contours de la future loi d'adaptation de la soci�t� fran�ais au vieillissement de la population, qui privil�gie le maintien � domicile des personnes d�pendantes sans transfert vers les assurances priv�es. L'avant-projet de loi d'orientation et de programmation fera l'objet d'une communication vendredi en conseil des ministres. /Photo d'archives/REUTERS/Charles Platiau
Jean-Marc Ayrault a pr�sent� mercredi les contours de la future loi d'adaptation de la soci�t� fran�ais au vieillissement de la population, qui privil�gie le maintien � domicile des personnes d�pendantes sans transfert vers les assurances priv�es.
L'avant-projet de loi d'orientation et de programmation, qui fera l'objet d'une communication vendredi en conseil des ministres, entend r�pondre aux besoins d'un pays qui compte aujourd'hui 15 millions de personnes �g�es de plus de 60 ans.
Elles seront 20 millions en 2030, 24 millions en 2060.
"La priorit� des priorit�s est donn�e au maintien � domicile", a dit le Premier ministre lors de la visite d'une r�sidence publique pour personnes �g�es � Angers (Maine-et-Loire).
Jean-Marc Ayrault a annonc� � cette fin une r�forme de l'Allocation personnalis�e d'autonomie (Apa) "qui garantisse une prestation plus g�n�reuse et plus facile d'acc�s".
Le gouvernement entend ainsi d�bloquer 375 millions d'euros suppl�mentaires chaque ann�e pour l'Apa � domicile, avec un objectif : "Plus d'heures, mieux d'heures, et des heures moins co�teuses pour le b�n�fice des �g�s et des professionnels".
La r�forme de l'Apa pr�voit une "aide au r�pit" pour les proches de personnes �g�es d�pendantes, notamment les malades d'Alzheimer, qui se verront accorder une aide leur permettant de financer un h�bergement temporaire pour la personne d�pendante.
"Dans ce projet de loi, nous avons �cart� sans ambigu�t� le transfert - que certains pr�conisaient - vers des assurances priv�es", a expliqu� Jean-Marc Ayrault.
"Ce n'est pas contre les assurances priv�es, mais elles doivent rester facultatives et subsidiaires", a-t-il ajout�. "Dans une p�riode de contrainte financi�re, c'�tait un choix vraiment d�licat, mais que nous avons d�cid� d'assumer."
"INVENTER LA MAISON DE RETRAITE DE DEMAIN"
La loi de programmation et d'orientation - qui doit �tre examin�e en avril au conseil des ministres - inclut des mesures de soutien aux professionnels de l'aide � domicile dont les frais de d�placement seront, par exemple, mieux pris en charge.
Le texte entend aussi "inventer collectivement la maison de retraite de demain", des �tablissements "plus ouverts sur la cit�, qui jouent un r�le structurant dans le parcours des personnes �g�es et accessibles � tous les Fran�ais", a dit Jean-Marc Ayrault.
Un Haut Conseil de l'�ge et des Conseils d�partementaux de la citoyennet� et de l'autonomie seront �galement cr��s pour faciliter la coordination des diff�rentes politiques publiques (logement, transports, urbanisme...) relatives � "l'adaptation de la soci�t� au vieillissement".
Un Plan national d'adaptation du logement sera lanc� pour am�nager d'ici 2017 quelque 80.000 logements en r�ponse aux besoins particuliers des personnes �g�es.
Jean-Marc Ayrault a aussi lanc� un appel � investir dans la "Silver economy" pour exploiter les opportunit�s d'affaires g�n�r�es par le vieillissement de la population fran�aise.
"Cette nouvelle fili�re industrielle est un vrai gisement d'emplois", a dit le Premier ministre.
Pour Jean-Marc Ayrault, la future loi sera applicable d�s 2015 "pour montrer aux Fran�ais que nous ne vivons pas cette nouvelle �tape de l'histoire de nos soci�t�s comme une charge, comme un frein, mais comme une chance".
Edit� par Yves Clarisse
