TITRE: AS Monaco : Ramadel Falcao transf�r� � Madrid... pour sa convalescence � metronews
DATE: 2014-02-12
URL: http://www.metronews.fr/sport/monaco-ramadel-falcao-transfere-a-madrid-pour-sa-convalescence/mnbl!EARvolD9y510Y/
PRINCIPAL: 168147
TEXT:
Mis � jour  : 13-02-2014 06:53
- Cr�� : 12-02-2014 14:55
Monaco : Falcao transf�r� � Madrid...
CONVALESCENCE - Radamel Falcao continue de se remettre de sa rupture du ligament crois� du genou gauche. Apr�s Porto, l'attaquant colombien va rejoindre Madrid � partir de jeudi pour entamer "la deuxi�me phase de sa r�cup�ration", informe mercredi le club de la Principaut�.
�
Malgr� sa blessure, Radamel Falcao garde le sourire. Photo :�MIGUEL RIOPA / AFP
Mais au fait, comment va Radamel Falcao ? Plut�t bien � en croire son employeur, l'AS Monaco. Op�r� le 25 janvier d'une rupture du ligament crois� ant�rieur du genou gauche, l'attaquant colombien de 27 ans poursuit sa convalescence. Le club princier indique mercredi via un communiqu� publi� sur son site officiel que le "Tigre" a aujourd�hui fini "avec succ�s la premi�re phase de la r�cup�ration suite � l�op�ration de son genou gauche."
Ranieri veut croire au "miracle" du Mondial
Le buteur de 27 ans, recrut� contre 60 millions d'euros par l'ASM l'�t� dernier � l'Atl�tico Madrid, va ainsi quitter Porto, o� il a subi une intervention chirurgicale pratiqu�e par le docteur Jose Carlos Noronha, pour retrouver la capitale espagnole. "Radamel Falcao va poursuivre � partir de ce jeudi la deuxi�me phase de sa r�cup�ration � la clinique sp�cialis�e du sport de Madrid en �troite collaboration avec le service m�dical de l�AS Monaco FC", pr�cise le dauphin du PSG en Ligue 1.
Touch� lors du succ�s mon�gasque contre Ludovic Giuly et les amateurs de Chasselay (3-0) en 16e de finale de Coupe de France, le 22 janvier dernier, Falcao est lanc� depuis dans une course contre la montre dans l'optique du Mondial. Malgr� son �tat pr�caire, l'ancien joueur de River Plate veut croire � son infime chance de voir le Br�sil avec la Colombie. "Je pense qu'il va jouer la Coupe du monde.  Ce sera un miracle mais il est tr�s concentr�", a estim� la semaine derni�re en conf�rence de presse son entra�neur sur le Rocher, Claudio Ranieri. Les Colombiens et les amateurs de football ne demandent pas mieux.
R�my de Souza
