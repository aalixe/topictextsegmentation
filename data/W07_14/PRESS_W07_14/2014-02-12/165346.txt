TITRE: Salon de Gen�ve 2014 - Skoda y lancera son Octavia Scout
DATE: 2014-02-12
URL: http://www.caradisiac.com/Salon-de-Geneve-2014-Skoda-y-lancera-son-Octavia-Scout-92341.htm
PRINCIPAL: 165340
TEXT:
Salon de Gen�ve 2014 - Skoda y lancera son Octavia Scout
Ecrit par Antoine Dufeu le 11 F�vrier 2014
La Skoda Octavia Scout fera sa premi�re apparition publique � l�occasion du salon de Gen�ve, d�but mars. Cette nouvelle version de l�Octavia , propos�e sur la carrosserie break, adopte un look de ��baroudeur�� tout en �tant �quip�e d�une transmission int�grale.
La nouvelle Octavia Scout est bas�e sur l�Octavia Combi et se distingue par son apparence inspir�e par les attributs des v�hicules tous-terrains. Les faces avant et arri�re int�grent des boucliers noirs associ�s � des inserts argent�s sp�cifiques. Les bas de caisses et les passages de roues sont �galement peints en noir, de m�me que les baguettes de protection lat�rales. La garde au sol a �t� rehauss�e de 33 mm compar�e � une Octavia standard. Des jantes en alliage de 17 pouces font �galement partie de l��quipement de s�rie. A l�int�rieur on retrouve un volant en cuir trois branches multifonctions, sp�cialement dessin� pour la version Scout.
La transmission int�grale est confi�e � un coupleur Haldex 5. Par rapport � la version pr�c�dente Octavia Scout, le poids tract� a �t� augment� de 25�% pour atteindre jusqu�� deux tonnes. Selon son constructeur, les capacit�s de traction et d�ascension de cette Octavia ont �t� ��grandement am�lior�es��.
Deux motorisations Diesel et une essence sont disponibles m�me si Skoda n�en d�voile pas encore les caract�ristiques. De m�me, pour le moment, le constructeur tch�que ne communique pas encore sur les tarifs.
Rappelons que Skoda profitera aussi du salon de Gen�ve pour d�voiler un in�dit concept VisionC .
