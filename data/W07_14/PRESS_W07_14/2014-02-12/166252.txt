TITRE: Camaret condamn� en appel � 10 ans de prison  - BFMTV.com
DATE: 2014-02-12
URL: http://www.bfmtv.com/societe/regis-camaret-condamne-a-10-ans-appel-708390.html
PRINCIPAL: 166248
TEXT:
> Proc�s
Camaret condamn� en appel � 10 ans de prison
Reconnu coupable de viols sur deux de ses anciennes stagiaires, l'entra�neur de tennis a �cop� d'une peine plus lourde que celle prononc�e en premi�re instance, en novembre 2012.
M.G. avec  AFP
Mis � jour le 12/02/2014 �  8:47
- +
r�agir
L'entra�neur de tennis R�gis de Camaret a �t� condamn� mardi en appel par la cour d'assises du Var � 10 ans de prison pour le viol de deux pensionnaires mineures de son club de Saint-Tropez, il y a vingt-cinq ans.
Plus t�t dans la journ�e, l'avocat g�n�ral avait r�clam� "12 � 15 ans" de prison. Cette peine est plus lourde que celle prononc�e en premi�re instance, en novembre 2012 par les assises du Rh�ne � Lyon, o� il avait alors �cop� de huit ans de prison.
Un "verdict apaisant"
L'ancien entra�neur est apparu sonn� dans le box. Les nombreuses victimes de R�gis de Camaret, aujourd'hui m�re de famille pour la plupart, se sont longuement �treintes avec �motion.
�� �
La championne de tennis Isabelle Demongeot, qui avait initi� la proc�dure en 2005, est venue s'exprimer devant les cam�ras aux c�t� des deux seules parties civiles retenues par la justice, Karine et St�phanie. "Les parties civiles et les prescrites sont indisociables", a comment� Isabelle Demongeot ajoutant : "Dix ans c'est juste. Ce verdict est apaisant".
�� �
"C'est d�finitif", s'est r�jouie Karine. "Cette petite prise de conscience on ne s'y attendait pas du tout, �a fait du bien", a affirm� St�phanie, commentant le pardon tardif de l'entaineur.
�� �
"J'ai honte et je demande pardon, c'est tout", avait d�clar� � la surprise g�n�rale R�gis de Camaret, en cl�ture du proc�s, apr�s s'�tre mur� dans le silence pendant sept jours.
�� �
L'accus� de 71 ans a ni� durant le proc�s avoir viol� les deux femmes qui �taient alors �g�es d'une dizaine d'ann�es.
Des t�moignages choquants
Parmi toutes ses anciennes �l�ves, aujourd'hui �g�es entre 37 et 50 ans  et entendues en tant que simples t�moins pour des faits prescrits, dix  ont �voqu� des viols ou des tentatives de viols et douze des  attouchements sexuels.
�� �
Les t�moignages d'anciennes joueuses se sont encha�n�s pendant le proc�s, choquant les membres du jury.
�� �
Marjolaine  a racont� comment son entra�neur l'avait sodomis�e et bless�e de  mani�re bestiale � 12 ans en la plaquant dans les vestiaires contre une  armoire. La fillette n'avait alors pas �t� crue par ses parents. �
�� �
Marion  a d�taill� comment il lui avait impos� des attouchements sexuels d�s  ses 10 ans, avant de la violer � 14 ans. Elle avait �t� la premi�re � d�poser plainte en 2002. Une plainte sans suite car prescrite.
A lire aussi
