TITRE: Six mois de prison ferme pour deux supporteurs de Montpellier - Coupe de France 2013-2014 - Football - Eurosport
DATE: 2014-02-12
URL: http://www.eurosport.fr/football/coupe-de-france/2013-2014/six-mois-de-prison-ferme-pour-deux-supporteurs-de-montpellier_sto4133504/story.shtml
PRINCIPAL: 168299
TEXT:
Six mois de prison ferme pour deux supporteurs de Montpellier
12/02
Bodmer : "Une revanche � prendre"
12/02
Courbis : "Niang ne devait pas tirer le p�nalty"
12/02
Girard : "�a montre aux jeunes le chemin qu�il reste � accomplir"
11/02
Coupe de France
Lille se qualifie pour les quarts de finale en battant Caen aux tirs au but (3-3, 6/5)
11/02
Les amateurs de Cannes �liminent Montpellier (L1) en 8es de finale (1-0)
11/02
Lyon: Lopes de retour, Gourcuff et Bisevac incertains
10/02
Niang sanctionn�, sur le banc
23/01
Lens, "un bon souvenir" pour Bernard Lacombe
23/01
Nice - Monaco � l'affiche des huiti�mes de finale
23/01
Auxerre bat Dijon au bout du suspense (3-2)
23/01
Blanc : "Une d�ception, pas une d�sillusion"
22/01
Courbis : "Un �v�nement et en m�me temps un accident"
22/01
Paris �limin� d�s les 16es de finale par Montpellier (1-2)
22/01
Les amateurs de Moulins (CFA) �liminent Toulouse (2-1) en 16e de finale
22/01
Soner Ertek est "d�pit�" apr�s son tacle sur Falcao
22/01
Coupe de France
Les amateurs de l'�le  Rousse (CFA 2) �liminent Bordeaux (0-0, 4 tab � 3) en 16e de finale
22/01
Monaco corrige Chasselay (0-3) et se qualifie pour les 8es de finale
22/01
Falcao, touch� au genou gauche, sort sur civi�re lors de Chasselay - Monaco
22/01
Mandanda : "C�est honteux ce que nous avons fait"
21/01
L'OGC Nice �carte l'OM apr�s un match fou (4-5)
21/01
Guingamp �limine Concarneau (2-3) en prolongations
21/01
Le RC Lens fait une nouvelle victime en L1 en dominant Bastia apr�s prolongation (2-1)
21/01
Le SCO Angers fait tomber Sochaux (1-0) d�s les 16es de finale
21/01
Coupe de France
Battu par Caen (0-2), Ajaccio est la premi�re �quipe de L1 sortie lors des 16es de finale
21/01
Lavezzi au repos contre Montpellier
21/01
Abidal et James Rodriguez absents contre Chasselay
21/01
OM, Monaco, Lyon... : 3 jours de Coupe en direct sur Eurosport Player et Eurosport
21/01
