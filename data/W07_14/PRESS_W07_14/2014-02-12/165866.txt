TITRE: JO Sotchi 2014 : le bronze pour Coline Mattel en saut � skis (VIDEO) - Divers Sports - La Voix du Nord
DATE: 2014-02-12
URL: http://www.lavoixdunord.fr/sports/jo-sotchi-2014-le-bronze-pour-coline-mattel-en-saut-a-ia2250b0n1913180?xtor%3DRSS-2
PRINCIPAL: 165864
TEXT:
Coline Mattel a apport� � la France sa troisi�me m�daille des jeux Olympiques 2014 en terminant troisi�me du premier concours de saut � skis f�minin de l�histoire, ce mardi � Rosa Khoutor.
Coline Mattel d�croche le bronze. AFP PHOTO / PETER PARKS
RETROUVEZ EN VIDEO LA COMPETITION ET LA REACTION DE COLINE MATTEL
La troisi�me m�daille fran�aise
Coline Mattel, 18 ans, a �t� devanc�e par l�Allemande Carina Vogt, premi�re championne olympique de saut � skis, et l�Autrichienne Daniela Iraschko-Stolz. La Haut-Savoyarde rejoint dans le camp des m�daill�s fran�ais � Sotchi les biathl�tes Martin Fourcade et Jean-Guillaume B�atrix, respectivement champion olympique et m�daill� de bronze de la poursuite lundi.
Elle a fr�l� la m�daille d'or
Le tremplin de Rosa Khoutor r�ussit bien � Mattel qui s�y �tait impos�e en Coupe du monde lors de l�hiver 2012-13. Deuxi�me apr�s la premi�re manche avec un saut � 99,5 m, elle a r�trograd� d�un rang apr�s la seconde manche o� elle s�est repos�e � 97,5 m pour un total de 254,2 points.
Une pionni�re
L��closion de Mattel, entra�n�e par Jacques Guillard, entra�neur historique du combin� nordique fran�ais, �tait annonc�e, tant cette adolescente originaire des Contamines Montjoie (Haute-Savoie), au caract�re tr�s affirm�, a domin� son sport dans les cat�gories de jeunes. Elle a en effet �t� championne du monde junior en 2011, vice-championne du monde en 2010 et avait particip� � ses premiers championnats du monde seniors � l��ge de 13 ans en 2009 � Liberec (R�p. tch�que).�
Cet article vous a int�ress� ? Poursuivez votre lecture sur ce(s) sujet(s) :
