TITRE: Front national : un tiers des Fran�ais adh�re � ses id�es - RTL.fr
DATE: 2014-02-12
URL: http://www.rtl.fr/actualites/info/politique/article/front-national-un-tiers-des-francais-adhere-a-ses-idees-7769661114
PRINCIPAL: 165600
TEXT:
Marine Le Pen a annonc� que le FN se maintiendra partout au second tour des municipales (Archives).
Cr�dit : AFP / Martin Bureau
Plus d'un tiers des Fran�ais dit adh�rer aux id�es du parti d'extr�me-droite. Un chiffre en constante hausse selon un barom�tre TNS Sofres.
34% des Fran�ais affirment adh�rer "aux id�es du  Front national", selon le barom�tre  d'image du FN r�alis� par TNS Sofres pour Le Monde, France Info et Canal+. A contrario, ils sont pr�s de six sur dix (59%) � ne pas  adh�rer aux id�es du parti d'extr�me-droite , selon cette �tude r�alis�e chaque ann�e depuis 1984.
Le niveau d'adh�sion n'a cess�  toutefois de cro�tre ces derni�res ann�es. Il �tait de 22% en 2011, lors  de la prise de fonction de Marine Le Pen � la t�te du parti. Il a  atteint 31% en 2012 et 32% en 2013. La part de personnes  interrog�es qui n'adh�rent "ni aux critiques ni aux solutions de Marine  Le Pen" reste pr�pond�rante � 43% mais en recul de trois points par  rapport � 2013. Elles sont 14% � adh�rer aux "critiques et  solutions" et 35%� � souscrire seulement aux critiques formul�es par  Marine Le Pen mais pas � ses solutions.
L'image de Marine Le Pen positive
L'image de la pr�sidente du  FN recueille de plus en plus d'opinions favorables : 58% jugent qu'elle  est "capable de rassembler au-del� de son camp", 56% qu'elle  "comprend les probl�mes quotidiens des Fran�ais" et 40% qu'elle "a  de nouvelles id�es pour r�soudre les probl�mes de la France". Pour  46%, Marine Le Pen est "plut�t la repr�sentante d'une droite patriote attach�e aux valeurs traditionnelles", contre 43% qui la rattachent  � "une extr�me droite nationaliste et x�nophobe".
Deux  points cl�s du programme du FN sont cependant rejet�s � une tr�s large  majorit� : 64% des Fran�ais sont oppos�s � la sortie de l'euro et 72% rejettent la  pr�f�rence nationale en mati�re d'emploi.
La r�daction vous recommande
