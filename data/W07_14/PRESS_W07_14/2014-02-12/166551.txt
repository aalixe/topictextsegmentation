TITRE: Crash d'avion en Algérie : Sa Majesté adresse un message de condoléances à Bouteflika
DATE: 2014-02-12
URL: http://www.lavieeco.com/news/actualites/crash-d-avion-en-algerie-sa-majeste-adresse-un-message-de-condoleances-a-bouteflika-28430.html
PRINCIPAL: 166549
TEXT:
Actualit�
Crash d'avion en Alg�rie : Sa Majest� adresse un message de condol�ances � Bouteflika
SM le Roi Mohammed VI a adress� un message de condol�ances et de compassion au pr�sident alg�rien Bouteflika, suite au crash d'un avion militaire dans la r�gion d'Oum El Bouaghi, qui a fait plusieurs victimes.
Dans ce message, le Souverain affirme avoir appris avec une grande affliction et une profonde peine la tragique nouvelle du crash d'un avion militaire dans la r�gion d'Oum El Bouaghi (Est), faisant de nombreuses victimes.
En cette douloureuse circonstance, SM le Roi a fait part au pr�sident Bouteflika, et � travers lui, aux familles afflig�es et au peuple alg�rien fr�re, de Ses vives condol�ances et Sa sinc�re compassion, implorant le Tout-Puissant d'avoir les victimes en Sa Sainte Mis�ricorde et d'accorder consolation et r�confort � leurs familles.
Le Souverain a, �galement, pri� le Tr�s Haut, de pr�server le pr�sident et le peuple alg�riens de tout malheur, et d'accorder � M. Bouteflika sant�, bien �tre et longue vie.
MAP
