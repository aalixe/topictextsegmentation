TITRE: Michael Schumacher: toujours le grand flou - Gala
DATE: 2014-02-12
URL: http://www.gala.fr/l_actu/news_de_stars/michael_schumacher_toujours_le_grand_flou_308428
PRINCIPAL: 166962
TEXT:
Le 07/03/2014 Un journal italien affirme que Schumacher respire sans assistance
Hospitalis� depuis le 29 d�cembre, Michael Schumacher est toujours dans le coma et son �tat ne va pas en s�am�liorant. Les quelques nouvelles qui filtrent affirment qu�il ne r�pond pas aux stimulis des m�decins. Pire, il aurait contract� une infection pulmonaire.
Les derni�res nouvelles en provenance du CHU de Grenoble ne se montrent gu�re rassurantes quant � l��tat de sant� de Michael Schumacher, toujours hospitalis� suite � son accident de ski . Une phase de r�veil progressif a �t� entam�e il y a maintenant quinze jours, mais elle ne donne pas les r�sultats escompt�s. Le quotidien allemand Bild affirme que l�ancien coureur automobile ne r�agirait pas aux stimulis des m�decins. Aucune r�action physique ni sensorielle n�a en effet �t� observ�e. Il a �t� demand� � Corinna, son �pouse pr�sente tous les jours � son chevet, de beaucoup lui parler en esp�rant que cela provoque une r�action chez Michael Schumacher.
Bild, souvent bien inform�, affirme m�me que le champion de F1 a contract� une infection pulmonaire la semaine derni�re, sans pr�ciser quelles cons�quences cela avait eu sur son �tat g�n�ral. Des informations qui n�ont pas �t� confirm�es par l�h�pital ou son entourage proche. Sabine Kehm, la porte-parole de Schumacher qui tente de maintenir un minimum de communication , a d�clar�: �Nous ne commentons pas les sp�culations�. Mais devant la multiplication des rumeurs, dont une annon�ait la semaine derni�re le d�c�s du sportif, et des informations non confirm�es, une mise au point devrait �tre organis�e la semaine prochaine.
A lire aussi :
