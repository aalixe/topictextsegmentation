TITRE: Sotchi: Le r�cap de mardi - JO 2014 - Sports.fr
DATE: 2014-02-12
URL: http://www.sports.fr/jo-2014/articles/jo-2014-white-le-patron-s-ecroule-1008738/?sitemap
PRINCIPAL: 0
TEXT:
11 février 2014 � 23h30
Mis à jour le
11 février 2014 � 23h30
La journée de mardi aux Jeux Olympiques a été dominée par l'absence de médaille pour Shaun White. Il s'est classé quatrième en halfpipe (90,25) alors que le Suisse Iouri Podladtchikov s'est imposé (94,75).
Si les Jeux font d’inattendus heureux, ils livrent aussi leur lot d’étranges déçus. Shaun White est ainsi tombé de très haut, seulement quatrième du halfpipe alors qu’il était bien sûr attendu avec l’or autour du cou. Après avoir renoncé au slopestyle, il s’agissait de la seule chance de breloque pour l’ex-chevelu américain, seule star mondiale ou presque de ces Jeux de Sotchi en l’absence de Lindsey Vonn. Après être tombé sur le premier saut, il n’a pas réussi à se rattraper suffisamment sur le deuxième.
Pourtant, White avait réussi un 95,75 en qualifications, soit 1 point de mieux que le passage du Suisse Iouri Podladtchikov, meilleur score en finale. Deux Japonais, Ayumu Hirano et Taku Hiraoka, complètent le podium. Et pour la petite - et incroyable - histoire, Hirano est le plus jeune médaillé de l’histoire à 15 ans et 74 jours… Côté français, il y a aussi eu quelques déceptions. La plus importante est peut-être pour Anais Bescond, partie cinquième de la poursuite mais qui n’a réussi que le 12e temps après 2 fautes au tir debout. Elle était pourtant deuxième après le couché…
Mattel: "Ça a été difficile"
Mais on retiendra bien sûr la belle médaille de bronze de Coline Mattel, pour les grands débuts du saut à ski féminin aux Jeux Olympiques d’hiver. Ce n’était pas gagné d’avance au vu de son début de saison… et de ses assiettes: "Ça a été difficile pour y arriver, de se tirer sur la gueule à l'entraînement, de se tirer sur la gueule devant un plat de spaghettis... Je dois faire gaffe à ce que je mange comme n'importe quel sportif. Sauf que c'est encore plus important pour moi, parce que je n'ai pas une physiologie qui fait que je perds facilement du poids."
L’occasion de tacler gentiment l’encadrement: "C'est moins manger, absolument pas grignoter, sortir de table quand les coaches se servent des assiettes monstrueuses ou des desserts... Aux repas de famille, à Noël, au Nouvel an, c'est dire: 'Je rentre me coucher là, je ne peux pas rester...'" Le résultat est incontestable, même si Mattel a pu croire au sommet puisqu’elle était deuxième après le premier saut. Ce sera beaucoup, beaucoup plus compliqué pour Vanessa James et Morgan Cipres, classés en 10e position après le programme court des couples.
