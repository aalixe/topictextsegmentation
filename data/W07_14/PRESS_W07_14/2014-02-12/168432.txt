TITRE: Limitations de vitesse � 80 km/h : rien de concret pour l'instant - L'argus
DATE: 2014-02-12
URL: http://www.largus.fr/actualite-automobile/limitations-de-vitesse-rien-de-concret-pour-linstant-3707382.html
PRINCIPAL: 168428
TEXT:
. par Arnaud Murati
Limitations de vitesse � 80 km/h : rien de concret pour l'instant
L'entourage du pr�sident du Conseil national de la s�curit� routi�re (CNSR) d�ment avec fermet� le fait que des sc�narios seraient d�j� arr�t�s concernant la baisse de la limitation de vitesse sur le r�seau secondaire.
Et si vous rouliez � 80 km/h partout en France ?
Ce sont nos confr�res d'Europe 1 qui ont allum� la m�che. La radio a en effet annonc� le 12 f�vrier 2014 que "deux propositions" �taient sur la table ce m�me jour et qu'elles aspiraient � �tre "tranch�es".
De quoi s'agit-il ? "La premi�re piste est d'abaisser de 90 � 80 km/h la vitesse partout en France. L'autre proposition entend abaisser la vitesse dans seulement la moiti� des d�partements fran�ais" peut-on lire sur le site Internet de la station.
Et de citer le point de vue de Claude Got, m�decin expert en s�curit� routi�re connu pour ses positions hostiles envers l'automobile.
L'entourage d 'Armand Jung, le pr�sident du Conseil national de la s�curit� routi�re, n'a pas tard� � d�mentir. "C'est faux, totalement faux et cela vient d'un Claude Got en mal de notori�t�. Aujourd'hui se tient une r�union normale du comit� des experts du CNSR, le rapport sur les limitations de vitesse sera remis le 16 mai en assembl�e pl�ni�re."
Rien ne serait donc�grav� dans le marbre�pour l'instant !
