TITRE: THE FLASH : John Wesley Shipp l'ex Flash ainsi que d'autres au casting
DATE: 2014-02-12
URL: http://www.lyricis.fr/cinema-serie-tv/flash-john-wesley-shipp-lancien-flash-rejoint-le-casting-ainsi-que-dautres-99219/
PRINCIPAL: 167082
TEXT:
7
La chaine The CW qui diffuse actuellement ARROW et qui s�appr�te � lancer prochainement la s�rie THE FLASH avec Grant Gustin vient d�annoncer une tr�s bonne nouvelle pour les v�t�rans de la s�rie Flash des ann�es 90� : l�acteur principal de cette derni�re, John Wesley Shipp, apparaitra dans la nouvelle s�rie !
La chaine n�a pour le moment pas souhait� s�exprimer concernant le r�le de John Wesley Shipp qui est tenu secret, mais son personne serait apparemment l�un des personnages r�currents de la s�rie. Je trouve �a vraiment cool.
D�autres acteurs ont aussi rejoint la s�rie, c�est le cas de Tom Cavanagh (Ed) qui incarnera une sorte de rock-star des scientifiques � l�origine de l�acc�l�rateur de particules de S.T.A.R. Labs qui suite � une explosion dans son labo aura lui aussi comme Flash, la facult� de courir � une tr�s grande vitesse.
Nous avons aussi Candice Patton (The Game) qui rejoint le casting en tant qu�Iris West, la meilleure amie de Barry Allen et fille de l�inspecteur West incarn� par Jesse L. Martin.
Finissons avec Carlos Valdes qui incarnera Cisco Ramon, l�un des ing�nieurs et plus jeune membre de l��quipe de scientifiques � la t�te de S.T.A.R. Labs.
La s�rie THE FLASH sera dirig�e par Greg Berlanti, Andrew Kreisberg et Geoff Johns, et � la r�alisation du pilote nous retrouverons David Nutter qui a aussi r�alis� le pilote d� Arrow .
Tu veux partager ?
