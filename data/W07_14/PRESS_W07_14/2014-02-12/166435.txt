TITRE: Greta Gerwig, star de How I met your dad
DATE: 2014-02-12
URL: http://www.ecranlarge.com/article-details-27422.php
PRINCIPAL: 166433
TEXT:
Tweet
Alors que la c�l�bre� How I met your mother �s'ach�ve dans un m�lange d'indiff�rence et de profonde lassitude, son spin-off How I met your dad se dessine plus clairement, gr�ce � l'embauche inattendue de� Greta Gerwig �dans le r�le principal. Si le projet de raconter une nouvelle fois cette histoire de rencontre, de dragouillage et de copinades n'est pas � premi�re vue des plus excitant et se voit r�guli�rement critiqu�e sur les r�seaux sociaux, l'arriv�e d'une com�dienne telle que Gerwig pourrait changer (un peu) les choses.
Car le moins que l'on puisse dire, c'est que l'on n'attendait pas l'une des coqueluches d'un certain cin�ma ind�pendant am�ricain dans la continuation de� How I met your mother . Pour m�moire,� Greta Gerwig �avait �t� l'an pass� au centre de toutes les attention gr�ce � Frances Ha . Reste � savoir si l'actrice (qui travaillera aussi sur le sc�nario) saura instiller un grain de folie dans ce qui s'annonce d�j� comme une m�canique un peu trop bien huil�e.
�
