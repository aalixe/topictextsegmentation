TITRE: Premi�res s�ances : "Les trois fr�res" s'inclinent face � "Mr Peabody" et "La Belle et la B�te"
DATE: 2014-02-12
URL: http://www.ozap.com/actu/premieres-seances-les-trois-freres-s-inclinent-face-a-mr-peabody-et-la-belle-et-la-bete/451659
PRINCIPAL: 167700
TEXT:
Premi�res s�ances : "Les trois fr�res" s'inclinent face � "Mr Peabody" et "La Belle et la B�te"
16H14 Le 12/02/14 Business 14
publi� par Charles Decant
Premi�res s�ances : "Les trois fr�res" s'inclinent face � "Mr Peabody" et "La Belle et la B�te"
Ce mercredi, "Mr Peabody et Sherman" s'est impos� en t�te des premi�res s�ances parisiennes, devan�ant "La Belle et la B�te" et "Les trois fr�res, le retour".
"Les trois fr�res" d�marre derri�re "Mr Peabody et Sherman" et "La Belle et la B�te"
Tweet
Retrouvez aussi l'actu m�dias sur notre page Facebook
Surprise en t�te des premi�res s�ances parisiennes cet apr�s-midi ! Alors que les deux plus grosses sorties de la semaine sont ind�niablement "Les trois fr�res, le retour" et la nouvelle version de "La Belle et la B�te" sign�e Christophe Gans, c'est le film d'animation "Mr Peabody & Sherman : Les voyages dans le temps" qui s'est empar� de la pole position. Destin� � un public familial, et donc aux enfants accompagn�s, le film a r�uni 1.918 spectateurs dans les 18 salles de la capitale qui le proposaient. Il devance ainsi "La Belle et la B�te", port� par Vincent Cassel et L�a Seydoux . Projet� dans 22 salles, le film n'est pas tr�s loin derri�re avec 1.803 billets vendus, pour une moyenne honorable de 82 spectateurs par salle.
C'est donc en troisi�me position que se hisse "Les trois fr�res, le retour". Le film, qui marque la premi�re collaboration � trois des Inconnus sur grand �cran depuis "Les rois mages" il y a plus de douze ans, �tait propos� dans une combinaison de 24 salles et a attir� 1.506 spectateurs, pas effray�s par les critiques d�sastreuses . Il s'agit du plus mauvais d�marrage de Bernard Campan, Pascal L�gitimus et Didier Bourdon � trois, mais de peu : en 2001, "Les rois mages" avait s�duit 2.150 personnes lors des premi�res s�ances parisiennes, tandis que 1.734 billets avaient �t� vendus au lancement des "Trois fr�res" en 1995.
"Les trois fr�res, le retour" a failli se faire chiper la troisi�me place par "Ida", co-production dano-polonaise qui a r�uni 1.502 spectateurs dans 21 salles. Derri�re ce quatuor de t�te, le nouveau film de Catherine Breillat, "Abus de faiblesse", fait pale figure. Le film, dans lequel Isabelle Huppert donne la r�plique au rappeur Kool Shen, n'�tait projet� que dans huit salles et a s�duit 328 spectateurs, soit 41 par salle.
-             Toutes les news : actus TV
Qu'en pensez vous ?
D�primant0
Rien � faire0
Veuillez d�sactiver votre bloqueur de pub (Adblock) pour ce site afin d'afficher l'int�gralit� des commentaires et en publier.
sepieter
180.000 premier jour france le coef paris province doit �tre enorme ! car c'est un bon chiffre ! donc je pense que le film va faire une semaine a 1 millions apr�s c'est vrai il y a l'autre film qui nous a gave en promo le film de dany boon donc on verra en plus ce sont les vacances ! on melange tous �a et on verra dans 1 mois !lol
ifbh.bleu
"La belle et la b�te" est vraiment � voir mais Pistouille recommande aussi American Bluff (que je n'ai pas vu).
En tout cas je reste sur ma d�ception concernant "les 3 fr�res".
A vous de choisir et bon cin� ce we.
Aurelien Python
Je voulais le voir, mais la bande-annonce m'avait pas beaucoup emball�, et maintenant avec les critiques d�sastreuses, je pense que j'irai plut�t voir "La belle et la b�te" ce week-end.
pistouille
moi je vous recommande un super film avec des acteurs g�niaux, AMERICAN BLUFF
spy01
la bande annonce ne m'a meme pas fait sourire l� je crains le pire quand j'irais le voir...
sextape
Oui exact. �� fera comme " le volcan "
pfff ;)
Nous �tions 3 spectateurs dans mon cin� en Province. Tr�s mal barr� pour tes 6M ^^
La t�l� intelligemment
Ceci �tant le Boon / M�rad m'a l'air du m�me (cas) niveau !
sextape
Ceux qui ont vu le premier en salles ne payent plus 10 euros pour voir des films fran�ais.
Les moins de 20 ans s'en fichent royalement.
Un Danyboon / Kad Merad dans 2 semaines...
D�j� si ils arrivent � 2 millions avec toutes les sorties de vacances, ils seront chanceux...
eyes-blue3
Une aussi grande d�ception que la votre ou j'attendais un grand retour des inconnus et du trio.
Pour moi c'est un vrai "Nanar" ou mes zygomatiques �taient en gr�ve (comme l'a bien dit un journaliste).
Franchement d�cevant !
philippedelacroix
Je suis all� le voir � 13h, c'est tr�s d�cevant franchement, j'ai trouv� le temps long mais bon... J'ai eu la m�me impression que pour les bronz�s 3, le film de trop, ils auraient du partir sur une toute autre id�e et pas faire une suite
eyes-blue3
Je vous recommande vraiment "La belle et la b�te".
jules-marechel
On s'en fou c'est Paris. �a fera 6 millions d'entr�e minimum.
Stargazer
