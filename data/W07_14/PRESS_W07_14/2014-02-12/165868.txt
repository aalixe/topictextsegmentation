TITRE: Coupe de France. Guingamp se m�fie du Petit Poucet, L'Ile-Rousse
DATE: 2014-02-12
URL: http://www.ouest-france.fr/coupe-de-france-guingamp-se-mefie-du-petit-poucet-lile-rousse-1922727
PRINCIPAL: 0
TEXT:
Coupe de France. Guingamp se m�fie du Petit Poucet, L'Ile-Rousse
Guingamp -
Yatabar� et les Guingampais, tombeurs de Concarneau au tour pr�c�dent.�|�DAVID ADEMAS
Facebook
Achetez votre journal num�rique
Guingamp joue son ticket pour les quarts de finale cet apr�s-midi � Ajaccio, � 16 h 45, face � L'Ile-Rousse (CFA 2).
En Avant prend tr�s au s�rieux son huiti�me de finale face aux amateurs de L�Ile-Rousse (CFA 2), tombeurs de Bordeaux au tour pr�c�dent (0-0, 4-3 aux tirs au but). � � Guingamp, on n�a pas l�habitude de prendre les matches � la l�g�re, indique l�entra�neur Jocelyn Gourvennec qui s�attend � un match difficile en terre corse. Malgr� les quatre divisions qui nous s�parent, cette �quipe poss�de des joueurs exp�riment�s qui se connaissent bien. On a les a supervis�s comme on supervise nos adversaires de Ligue 1. On a vu deux de leurs matches, � Vaulx-en-Velin et � N�mes, on s�est beaucoup renseign� sur eux. Ils ont d�j� r�ussi un exploit, on est pr�venu. � nous de bien rentrer dans la partie, de jouer tous les coups � fond et de faire respecter la hi�rarchie. �
Tags :
