TITRE: Un gros coup de �Cannes� - 20minutes.fr
DATE: 2014-02-12
URL: http://www.20minutes.fr/montpellier/1296602-gros-coup-cannes
PRINCIPAL: 165708
TEXT:
Un gros coup de �Cannes� J.-C. Magnenet / AFP
FOOTBALL Le MHSC a �t� battu (1-0) en 8es de finale de la coupe de France
Une qualification au bout du suspense. Le stade Pierre-de-Coubertin, plein � craquer mardi soir, a explos� de joie au coup de sifflet final, � la fin de la deuxi�me mi-temps de la prolongation. Mardi soir, les Cannois ont r�ussi l'exploit de se qualifier contre une �curie de Ligue�1 en huiti�mes de finale de la Coupe de France. Apr�s Saint-Etienne, c'est face � Montpellier que les Azur�ens n'ont rien l�ch� et prouv� tout leur courage. Alors que les deux �quipes partaient pour une s�ance de tirs au but, l'attaquant Belkacem Zobiri permit aux Cannois de continuer leur aventure en Coupe d'un tir crois� dans la surface de r�paration.
Un penalty sur le poteau
��C'est la victoire de tout un groupe, tout une famille. On n'a rien � enlever � notre qualification, elle est amplement m�rit�e��, a r�agi l'unique buteur de la rencontre au micro d'Eurosport. Solides en d�fense et g�n�reux dans l'effort, les Cannois ont pourtant cru voir la qualification s'�chapper lorsque l'arbitre siffla un penalty pour les H�raultais (101e). Mais c'est le poteau qui sauva les Azur�ens. Les Cannois ont alors redoubl� d'effort devant le but de Laurent Pionnier. L'AS Cannes file en quart de finale et se met � r�ver du Stade de France.
Jean-Alexis Gallien-Lamarche
? Des incidents
Le d�but du match a �t� marqu� par des incidents entre supporters montp�lli�rains et les CRS.  Un homme a �t� l�g�rement bless� � la jambe et �vacu� � l'h�pital  de Cannes, selon les pompiers.
