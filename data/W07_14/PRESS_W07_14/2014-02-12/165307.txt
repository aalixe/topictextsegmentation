TITRE: USA : La Chambre rel�ve le plafond de la dette, Infos march�s - Les Echos Bourse
DATE: 2014-02-12
URL: http://bourse.lesechos.fr/infos-conseils-boursiers/actus-des-marches/infos-marches/usa-la-chambre-releve-le-plafond-de-la-dette-950470.php
PRINCIPAL: 165306
TEXT:
USA : La Chambre rel�ve le plafond de la dette
11/02/14 � 23:41
- Reuters 0 Commentaire(s)
WASHINGTON, 11 f�vrier (Reuters) - La Chambre des repr�sentants, � majorit� r�publicaine, a vot� mardi en faveur du rel�vement pour un an du plafond de l'endettement am�ricain.
Par 221 voix contre 201, la Chambre a adopt� ce texte, se conformant � la demande exprim�e par Barack Obama de ne pas y attacher de condition particuli�re.
Le S�nat, contr�l� par les d�mocrates doit encore se prononcer. Il pourrait commencer l'examen du texte d�s mercredi. (Richard Cowan, Nicolas Delame pour le service fran�ais)
Laisser un commentaire
