TITRE: Sapin appelle Pierre Gattaz à la 'responsabilité'
DATE: 2014-02-12
URL: http://www.lefigaro.fr/flash-actu/2014/02/11/97001-20140211FILWWW00373-sapin-appelle-pierre-gattaz-a-la-responsabilite.php
PRINCIPAL: 0
TEXT:
conifère en un mot ou en deux ...con n'y flaire ...
Le 12/02/2014 à 09:12
CER203
GAttaz a enfin compris que les 6 milliards que devraient recevoir les entreprises vont être prélevés sur les clients des entreprises, que leur chiffre d'affaire va baisser d'autant, et qu'il n'est donc pas question de créer d'emplois dans ces conditions.
Pauvre Sapin, Plus les impôts monteront, moins les consommateurs iront de l'avant, tout le monde serre les boulons, la spirale récessioniste est en marche. Il faut dire qu'ils sont bien formatés par l'idéologie de la baisse des dépenses publiques qui fout les pétoches à tout le monde, fonctionnaires, entreprises du bâtiment, investisseurs immobiliers, etc...
Le 11/02/2014 à 22:57
