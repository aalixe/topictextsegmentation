TITRE: L'�quipe du CICR disparue au Mali enlev�e par des djihadistes - rts.ch - info - monde
DATE: 2014-02-12
URL: http://www.rts.ch/info/monde/5602324-l-equipe-du-cicr-disparue-au-mali-enlevee-par-des-jihadistes.html
PRINCIPAL: 165173
TEXT:
L'�quipe du CICR disparue au Mali enlev�e par des djihadistes
11.02.2014 09:45
Un mouvement djihadiste a revendiqu� mardi l'enl�vement en f�vrier au Mali d'une �quipe du Comit� international de la Croix-Rouge (CICR). L'ONG �tait sans nouvelle d'eux depuis leur disparition.
Un responsable du Mouvement pour l'unicit� et le djihad en Afrique de l'Ouest (MUJAO) a affirm� mardi avoir enlev� dans le nord du Mali une �quipe du Comit� international de la Croix-Rouge (CICR). L'ONG avait indiqu� lundi �tre sans nouvelle de ces cinq personnes depuis le 8 f�vrier.
"Nous avons pris gr�ce � l'aide de Dieu un (v�hicule) 4x4 des 'ennemis de l'islam' avec leurs complices", a d�clar� dans un bref entretien t�l�phonique � l'AFP un responsable du MUJAO. A la question de savoir s'il s'agissait de l'�quipe du CICR, il a r�pondu: "Oui". "Ils sont en vie et en bonne sant�", a-t-il dit, sans plus de d�tails.
Disparus depuis f�vrier
Les cinq personnes sont quatre membres du CICR et un v�t�rinaire d'une autre organisation humanitaire, tous des Maliens, et leur disparition remonte au 8 f�vrier, alors que le v�hicule effectuait le trajet entre Kidal (extr�me nord-est) et Gao (nord-est), d'apr�s un porte-parole du CICR.
agences/jvia
