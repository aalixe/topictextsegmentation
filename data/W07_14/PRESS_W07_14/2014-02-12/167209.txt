TITRE: Patrick Buisson enregistrait Nicolas Sarkozy � son insu | Mediapart
DATE: 2014-02-12
URL: http://www.mediapart.fr/journal/france/120214/patrick-buisson-enregistrait-nicolas-sarkozy-son-insu
PRINCIPAL: 0
TEXT:
La lecture des articles est r�serv�e aux abonn�s.
L'ancien conseiller et sp�cialiste des sondages, Patrick Buisson, a r�alis� plusieurs heures d'enregistrements clandestins de ses conversations avec l'ancien pr�sident de la R�publique, Nicolas Sarkozy, dans les bureaux de l'�lys�e, selon une information du Point.
Les enregistrements ont �t� effectu�s � l'aide d'un dictaphone dissimul� dans la veste de M. Buisson. La police, qui a effectu� des perquisitions dans les bureaux du conseiller sp�cial, dans le cadre de l'enqu�te sur les sondages de l'�lys�e, s'int�resserait � ces bandes.
Lire sur le site du Point
LES SONDAGES EN QUESTION
