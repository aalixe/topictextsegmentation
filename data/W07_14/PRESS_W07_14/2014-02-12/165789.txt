TITRE: Le message de la Silicon Valley � Hollande - 12/02/2014 - La Nouvelle R�publique France-Monde
DATE: 2014-02-12
URL: http://www.lanouvellerepublique.fr/France-Monde/Actualite/24-Heures/n/Contenus/Articles/2014/02/12/Le-message-de-la-Silicon-Valley-a-Hollande-1792981
PRINCIPAL: 165784
TEXT:
Le message de la Silicon Valley � Hollande
12/02/2014 05:27
Paris - Washington : la nouvelle alliance
Fran�ois Hollande termine ce mercredi sa visite aux �tats-Unis par San Francisco, en Californie, o� il va rencontrer des dirigeants d'entreprises de la Silicon Valley. Le dernier pr�sident fran�ais � y avoir mis les pieds, c'�tait voici trente ans, Fran�ois Mitterrand. Depuis, le monde a chang�. La Silicon Valley aussi.
Guillaume Dumortier , la trentaine un peu tass�e, est un de ces french-entrepreneurs dont on loue l�-bas le talent technologique en les appelant ��les po�tes du code��. Il y a sept ans, il posait ses valises dans cet univers o� on ne peut pas subsister sans faire rentrer au moins 100.000�dollars � l'ann�e. Depuis, il est devenu mari et p�re, a connu des succ�s et des plantages. Comme tout le monde dans la Silicon Valley. ��Un �chec, c'est une r�ussite qui a mal tourn�. Il faut avoir foir� pour r�ussir. Ici, nous avons cette culture : faillir n'est pas une infamie. C'est �a que Fran�ois Hollande doit comprendre��, r�sume-t-il.
Un univers en perp�tuel mouvement
Son boulot ? Marketeur. Il construit des moteurs de croissance pour les cr�ations num�riques gr�ce au Net et aux r�seaux sociaux. Une vie loin des clich�s : ��N'allez surtout pas croire qu'on est ici pour se dorer la pilule. Nous vivons dans une sorte de championnat du monde permanent de l'innovation. On �volue dans un univers en perp�tuel mouvement, o� on ne peut survivre qu'en s'adaptant constamment. Dans le techbuziness (les affaires technologiques), on peut lever des millions de dollars sur un projet, se crasher et recommencer ensuite. La France ne peut pas imiter ce mod�le, mais elle doit cr�er des passerelles avec lui.��
Comment rendre l'Europe ��sexy�� pour ces entrepreneurs ? En abolissant en tout ou partie les contraintes administratives et fiscales. En tout cas, pr�cise Guillaume Dumortier, ��en phase d'amor�age, quand on travaille sur des technologies qui ont � peine deux ans d'incubation, une fiscalit� est difficilement imaginable. Dans l'ordre des choses, avant de payer des imp�ts, il faut avoir trouv� un march� et des revenus��.
Qu'est-ce qui pourrait le convaincre de rentrer au bercail ? ��Avoir la certitude que la France, et pas seulement les grands groupes, se lance � fond dans l'�conomie num�rique et sentir que je vais pouvoir lui �tre utile dans cette aventure��, r�pond Guillaume Dumortier.
- - -
Si vous avez des questions ou des remarques, n'h�sitez pas � en faire part � l'auteur de cet article par ici .
Christophe Colinet
