TITRE: Levée de l'immunité parlementaire de Serge Dassault au Sénat | Site mobile Le Point
DATE: 2014-02-12
URL: http://www.lepoint.fr/fil-info-reuters/levee-de-l-immunite-parlementaire-de-serge-dassault-au-senat-12-02-2014-1790950_240.php
PRINCIPAL: 0
TEXT:
12/02/14 à 13h28
Levée de l'immunité parlementaire de Serge Dassault au Sénat
PARIS ( Reuters ) - Le bureau du Sénat a levé mercredi l'immunité parlementaire du sénateur UMP Serge Dassault, patron du groupe aéronautique éponyme, a annoncé le président de la haute assemblée, Jean-Pierre Bel.
Le sénateur de l'Essonne avait annoncé lundi son intention de demander lui-même la levée de son immunité afin de "démontrer sa totale innocence" dans l'enquête sur des achats présumés de voix dans la ville de Corbeil-Essonnes, dont il fut maire durant 14 ans (1995-2009).
Sur 24 participants, la levée de l'immunité a été décidée par "14 voix pour, les autres refusant de participer au vote", a précisé Jean-Pierre Bel à la presse. Deux membres du bureau, qui compte au total 26 élus, étaient absents, a-t-il ajouté.
"Le Sénat a tranché ce matin de manière positive en faveur de la levée de l'immunité", a-t-il souligné.
Le bureau du Sénat avait rejeté le 8 janvier par 13 voix contre 12 et une abstention une première demande, ce qui avait provoqué un tollé, les sénateurs de gauche y étant majoritaires.
Jean-Pierre Bel a annoncé en outre que, comme il l'avait proposé le 9 janvier, le bureau avait décidé que les votes se feraient désormais à main levée et non plus à bulletins secrets.
"Il nous était apparu qu'il était nécessaire que chaque parlementaire assume son choix", a expliqué le président du Sénat lors d'une conférence de presse à l'issue de la réunion.
Il a précisé par la suite dans un communiqué que le vote sur la demande de levée d'immunité parlementaire de Serge Dassault s'était déroulé à main levée.
Le groupe des sénateurs communistes et apparentés du Sénat se félicite dans un communiqué d'"une étape vers la transparence".
"Cette décision n'est en rien une décision de justice et ne présume pas de la culpabilité du ou de la parlementaire concerné(e). Il s'agit de permettre à la justice de faire son travail en toute indépendance et de pleinement respecter le principe républicain de séparation des pouvoirs", peut-on lire.
La levée de l'immunité parlementaire de l'élu doit permettre aux magistrats du pôle financier de Paris de le placer éventuellement en garde à vue. L'immunité n'empêchait pas les magistrats d'effectuer des perquisitions.
Serge Dassault, qui est âgé de 88 ans, a été maire de Corbeil-Essonnes de juin 1995 à juin 2009. C'est un de ses proches collaborateurs, Jean-Pierre Bechter, qui est actuellement maire de cette commune de la banlieue parisienne.
En juillet dernier, le Sénat avait rejeté une demande de levée de l'immunité de Serge Dassault dans une autre affaire, celle d'une tentative de meurtre perpétrée en février à Corbeil-Essonnes.
Emile Picy, édité par Sophie Louet
