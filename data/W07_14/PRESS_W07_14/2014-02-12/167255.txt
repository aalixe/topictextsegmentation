TITRE: Mia Electric plac� en redressement judiciaire
DATE: 2014-02-12
URL: http://www.boursier.com/actualites/reuters/mia-electric-place-en-redressement-judiciaire-151272.html?sitemap
PRINCIPAL: 167251
TEXT:
Mia Electric plac� en redressement judiciaire
Abonnez-vous pour
Cr�dit photo ��Reuters
BORDEAUX (Reuters) - Le fabricant de voitures �lectriques Mia Electric (ex-Heuliez) a �t� plac� en redressement judiciaire mercredi par le tribunal de commerce de Niort (Deux-S�vres) avec une p�riode d'observation jusqu'au 12 ao�t, a-t-on appris de source syndicale.
Mia Electric, situ� � Cerizay, avait �t� assign� au tribunal de commerce par une saisine du parquet, inquiet de la situation �conomique de l'entreprise. Il avait requis le 22 janvier dernier le placement en redressement judiciaire.
Un probl�me de tr�sorerie a provoqu� un retard dans le versement des salaires de certains des 200 employ�s, au point qu'ils ont saisi le tribunal des prud'hommes de Thouars en r�f�r�.
Les salaires de janvier n'ont pas �t� vers�s, a-t-on appris mercredi de source syndicale. Selon cette m�me source, la direction avait garanti que l'ensemble des salaires seraient vers�s avant mardi soir.
L'usine tourne actuellement au ralenti, selon une source interne. Seule la production de ch�ssis se poursuit mais la construction des voitures est � l'arr�t faute de pi�ces.
Mia Electric avait annonc� en octobre dernier l'entr�e � son capital d'une soci�t� d'investissement pr�sid�e par le Cor�en Mark Rho dans le cadre de son premier contrat d�croch� en Asie.
Le nouvel actionnaire s'�tait engag� � racheter 10% des actions Mia pour 5,3 millions d'euros. Apr�s cette op�ration, le principal actionnaire de Mia Electric, Focus Asia GmbH, devait passer de 88% � 78% du capital, tandis que la r�gion Poitou-Charentes devait conserver ses 12%.
Cette augmentation de capital n'est toujours pas concr�tis�e.
Mia Electric est n� de la reprise de l'activit� v�hicule �lectrique du groupe Heuliez par l'investisseur allemand Edwin Kohl en 2010. Le groupe a �t� rachet� par Focus Asia GmbH en 2013.
Claude Canellas, �dit� par Sophie Louet
click here for restriction
