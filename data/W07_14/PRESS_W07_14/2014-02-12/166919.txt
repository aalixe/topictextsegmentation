TITRE: Gisin-Maze, pas une premi�re - Fil info - Sotchi 2014 - Jeux Olympiques -
DATE: 2014-02-12
URL: http://sport24.lefigaro.fr/jeux-olympiques/sotchi-2014/fil-info/gisin-maze-pas-une-premiere-678682
PRINCIPAL: 166916
TEXT:
Gisin-Maze, pas une premi�re
12/02 11h57 - JO 2014
La Suissesse Dominique Gisin et la Slov�ne Tina Maze, sacr�es ex aequo championnes olympiques de descente mercredi � Sotchi, sont le huiti�me cas dans l'histoire des JO d'hiver de m�dailles d'or partag�es. Gisin et Maze sont les premi�res en ski alpin � ne pas pouvoir �tre d�partag�es et � donc se partager le titre olympique de la descente. Mais avant elles, le cas s'�tait produit en luge, en ski de fond, en bobsleigh, en patinage artistique et trois fois en patinage de vitesse (dont deux fois pour un m�me patineur, le Russe�Yevgeny Grishin).
