TITRE: La Chine p�se sur les exportations de vins et spiritueux fran�ais
DATE: 2014-02-12
URL: http://www.boursier.com/actualites/reuters/la-chine-pese-sur-les-exportations-de-vins-et-spiritueux-francais-151288.html?sitemap
PRINCIPAL: 0
TEXT:
La Chine p�se sur les exportations de vins et spiritueux fran�ais
Abonnez-vous pour
Cr�dit photo ��Reuters
par Pascale Denis
PARIS (Reuters) - Apr�s avoir atteint des niveaux record en 2012, les exportations de vins et spiritueux fran�ais ont marqu� le pas en 2013 sous l'effet d'une forte baisse des ventes de cognac et de bordeaux � destination de la Chine.
Le chiffre d'affaires du secteur r�alis� � l'export, qui repr�sente le deuxi�me poste exc�dentaire de la balance commerciale fran�aise, a totalis� 11,12 milliards d'euros l'an dernier, en baisse de 0,4% par rapport � 2012.
"Il s'agit tout de m�me d'une belle performance apr�s une ann�e historique en 2012 et malgr� la sur�valuation de l'euro", a d�clar� mercredi � la presse Louis Fabrice Latour, pr�sident de la F�d�ration des exportateurs de vins et spiritueux de France (FEVS).
Pour 2014, il s'est dit "inquiet" compte tenu d'une r�colte encore tr�s limit�e dans toutes les appellations viticoles hormis celle du Languedoc.
"Le probl�me des volumes s'est amplifi�. Il aura un impact important sur les prix et il n'est pas certain que la fili�re puisse passer les hausses voulues", a-t-il estim�.
La vigueur de l'euro a frein� l'an dernier les ventes aux Etats-Unis (+0,2%), premier march� d'exportation de la fili�re, tandis qu'elles ont recul� de 1,8% au Royaume-Uni, leur deuxi�me march�.
Surtout, les exp�ditions vers la Chine ont plong� de 18%, avec un d�crochage de 21,5% pour les spiritueux et de 12% pour les vins dits "tranquilles".
Les mesures prises par P�kin il y a 18 mois visant � bannir la consommation ostentatoire ont lourdement p�nalis� les acteurs du cognac, principal contributeur aux exportations fran�aises (21% du total) avec le champagne (20%) et le bordeaux (19%).
R�my Cointreau, le plus expos� � la Chine, a �t� contraint au profit warning apr�s un violent d�crochage des ventes de son cognac R�my Martin, tandis que Pernod Ricard, qui publiera ses r�sultats semestriels jeudi, pourrait r�viser en nette baisse sa pr�vision pour l'exercice qui sera clos fin juin.
Hennessy, propri�t� du groupe LVMH, a lui aussi ralenti la cadence, tout en disant avoir compens� la baisse de la demande chinoise par de solides ventes aux Etats-Unis.
DYNAMISME EN ALLEMAGNE ET � SINGAPOUR
"Ce qui a disparu ne reviendra pas dans les canaux de distribution touch�s par ces mesures", a pr�cis� Christophe Navarre, PDG de Mo�t Hennessy, faisant r�f�rence aux pratiques chinoises des cadeaux ou des banquets.
Il faut donc que les acteurs du cognac s'organisent pour "capter les opportunit�s d'un march� qui reste par ailleurs tr�s dynamique", a-t-il dit, en �voquant la consommation dans les bars ou les karaok�s.
La lutte contre les cadeaux d'affaires ou les banquets arros�s d'eaux-de-vie haut de gamme a aussi touch� les exportations de vins, qui ont par ailleurs souffert des tensions commerciales apparues en juin entre l'Union europ�enne et la Chine, avec une enqu�te anti-dumping ouverte par P�kin en r�ponse � l'instauration de taxes europ�ennes sur les panneaux solaires chinois.
"Nous sommes toujours dans une phase de dialogue, mais il n'y a pas encore d'avanc�es significatives", a pr�cis� Louis Fabrice Latour.
La France est de loin la plus touch�e, avec une baisse de 12,5% de ses exportations de vins vers la Chine, premier importateur de bordeaux et cinqui�me importateur de vins fran�ais toutes appellations confondues. A l'inverse, l'Italie a vu ses ventes y gagner 9%, tandis que celles du Chili y ont grimp� de 13%.
Les exportations fran�aises de vins et spiritueux ont en revanche �t� soutenues en Allemagne (+4,3%) et � Singapour (+5,1%), respectivement troisi�me et quatri�me importateurs.
Edit� par Dominique Rodriguez
