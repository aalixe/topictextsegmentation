TITRE: Russie: trois ans de camp pour un �cologiste critique des JO de Sotchi | Environnement
DATE: 2014-02-12
URL: http://www.lapresse.ca/environnement/201402/12/01-4737998-russie-trois-ans-de-camp-pour-un-ecologiste-critique-des-jo-de-sotchi.php
PRINCIPAL: 167173
TEXT:
Agence France-Presse
KRASNODAR, Russie
La justice russe a confirm� mercredi en appel la condamnation � trois ans de camp d'un militant �cologiste qui d�non�ait les impacts sur l'environnement des travaux de pr�paration des Jeux olympiques d'hiver de Sotchi, qui battent actuellement leur plein.
Le tribunal r�gional de Krasnodar (sud) a rejet� l'appel d'Evgueni Vitichko, g�ologue et membre d'une association r�gionale de d�fense de l'environnement du Caucase du Nord (EWNC), a constat� une correspondante de l'AFP.
M.�Vitichko avait �t� condamn� en 2012, avec un autre militant, Suren Gazarian, � une peine de trois ans avec sursis pour avoir cr�� une ouverture dans une cl�ture dress�e dans une zone prot�g�e. Sa peine a �t� commu�e en prison ferme fin�2013.
Partager
