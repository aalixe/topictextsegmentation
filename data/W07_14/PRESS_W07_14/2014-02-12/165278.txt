TITRE: Ida, initiation à l'histoire polonaise par les démons
DATE: 2014-02-12
URL: http://www.lefigaro.fr/cinema/2014/02/11/03002-20140211ARTFIG00277--ida-initiation-a-l-histoire-polonaise-par-les-demons.php
PRINCIPAL: 165276
TEXT:
Ida (Agata Trzebuchkowska) s'apprête à devenir religieuse. Crédits photo : Memento Films
Une jeune religieuse voit pour la première fois sa tante juive et communiste.
Publicité
Pawel Pawlikowski s'est fait un nom dans le cinéma britannique avec Transit Palace et My Summer of Love (Bafta du meilleur film 2005). Après un détour par Paris, où il a réalisé La Femme du Ve , il revient dans sa Pologne natale, quittée à l'adolescence. Son voyage personnel est devenu un film de voyage, en noir et blanc, d'une étrange lenteur et d'un esthétisme recherché.
Au début des années 1960, en Pologne, deux femmes font route à travers la campagne, s'enfoncent dans le passé. Elles ne se connaissent pas, appartiennent à des générations différentes, ont des personnalités et des comportements aussi opposés que possible. Mais elles sont de la même famille, et les souvenirs tragiques de Wanda sont l'héritage inconscient de la jeune Ida.
Orpheline élevée dans un couvent, Ida s'apprête à devenir religieuse, mais avant, la supérieure a tenu à ce qu'elle rencontre sa seule parente, la s�ur de sa mère. Visage pur sous son voile de novice, Ida (Agata Trzebuchkowska) tombe sur une femme à la beauté abîmée mais à la sensualité affirmée, qui boit et fume trop, aime les jeux de la séduction. Wanda (Agata Kulesza) brûle sa vie avec un désespoir provocant, une ironie noire.
Sarabande jazzy
Elle apprend à Ida ses origines juives et revient avec elle vers le village familial, les temps de ténèbres et de massacres. Pour Ida, innocente et ignorante, c'est une initiation à l'histoire et à la vie. Pour Wanda, une sorte d'enquête policière vengeresse. Les hommes sont aussi contrastés que les femmes: lourd paysan fermé sur des secrets meurtriers, ou jeune auto-stoppeur musicien qui charme et trouble la solitude des deux femmes.
Ida est une sorte de sarabande jazzy (deux grands thèmes de John Coltrane), où passent les démons polonais. On peut regretter le maniérisme excessif de la mise en scène, avec ses plans décadrés, quelque chose de trop étudié qui nuit parfois à l'émotion. Mais la thématique est riche, et les deux actrices remar­quables.
