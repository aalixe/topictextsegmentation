TITRE: JO 2014 / SKI DE FOND - Manificat ne fera pas le 15km
DATE: 2014-02-12
URL: http://www.sport365.fr/jo-2014-sotchi/les-francais/manificat-ne-fera-pas-15km-1102766.shtml
PRINCIPAL: 167715
TEXT:
JO 2014 / SKI DE FOND - Publi� le 12/02/2014 � 16h47
0
Manificat ne fera pas le 15km
Maurice Manificat a renonc� � prendre le d�part du 15 kilom�tres en ski de fond, vendredi.
Selon L�Equipe, Maurice Manificat a d�cid� de faire l�impasse sur le�15 kilom�tres�en ski de fond vendredi. Le Fran�ais, neuvi�me de la poursuite dimanche, a d�cid� de se pr�server pour le relais 4x10km (le 16), le team sprint (le 19) et le 50km libre (le 23). Jean-Marc Gaillard et Adrien Backscheider seront les deux repr�senteront fran�ais sur le 15km classique.
