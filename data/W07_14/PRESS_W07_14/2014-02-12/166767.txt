TITRE: Syrie: reprise des op�rations d'aide et d'�vacuation des civils � Homs - 12/02/2014 - LaD�p�che.fr
DATE: 2014-02-12
URL: http://www.ladepeche.fr/article/2014/02/12/1816707-syrie-reprise-homs-operations-aide-evacuation-civils.html
PRINCIPAL: 166765
TEXT:
Syrie: reprise des op�rations d'aide et d'�vacuation des civils � Homs
Publi� le 12/02/2014 � 11:07
,
Mis � jour le 12/02/2014 � 15:12
Des Syriens attendent d'�tre �vacu�s des quartiers assi�g�s de Homs, lors d'une op�ration humanitaire. Bassel Tawil �/�AFP
Les op�rations d'acheminement d'aide humanitaire et d'�vacuation de civils ont pu reprendre mercredi � Homs, une ville syrienne assi�g�e depuis plus d'un an et demi o� des centaines de personnes restent bloqu�es manquant cruellement de nourriture.
Dans le m�me temps, � Gen�ve, des repr�sentants du r�gime et de l'opposition devaient poursuivre leurs discussions visant � trouver une solution politique au conflit sous l'�gide de l'ONU pour le troisi�me jour cons�cutif. Le m�diateur de l'ONU Lakhdar Brahimi a cependant admis mardi que ce nouveau cycle de discussions �tait aussi "laborieux" que le premier en janvier, et un ministre syrien les a jug�es en l'�tat "vou�es � l'�chec".
Les op�rations humanitaires � Homs ont �t� la seule note relativement positive relev�e par M. Brahimi m�me si la plupart des civils �vacu�s �taient dans un �tat de grande faiblesse, � cause surtout d'un manque de nourriture et de m�dicaments en raison du si�ge impos� par l'arm�e depuis juin 2012.
"Il y a des enfants l�-bas et c'est vraiment d�chirant, c'est la premi�re fois qu'ils voient une banane", a indiqu� le chef des op�rations du Croissant rouge Khaled Erksoussi � l'AFP, parlant des personnes �vacu�es.
"Nos �quipes de soutien psychologique sont l�-bas pour tenter de g�rer les situations au cas par cas, mais � un moment ces �quipes auront elles aussi besoin d'aide psychologique parce que la situation est tr�s �motionnelle", a-t-il ajout�.
Apr�s une suspension mardi de l'op�ration pour des probl�mes "logistiques", le gouverneur de Homs Talal Barazi a annonc� que de la nourriture a pu entrer (mercredi) � 11H00 (09H00 GMT), ajoutant que "les v�hicules qui ont livr� cette assistance devaient �vacuer un groupe de civils dont 20 chr�tiens du quartier Bustan al-Diwan".
Des enfants fr�les et �maci�s
Depuis vendredi, en vertu d'un accord entre r�gime et rebelles n�goci� par l'ONU, quelque 1.200 personnes, en grande majorit� des enfants, des femmes et des hommes �g�s, ont pu �tre �vacu�es des quartiers de la Vieille ville tenue par les rebelles, selon le Croissant-Rouge.
Au moins 500 enfants figurent parmi les civils �vacu�s, selon Fonds des Nations unies pour l'enfance (Unicef) qui estimait � plus de 1.000 le nombre d'enfants coinc�s � Homs avant le d�but de l'op�ration. Ils �taient "fr�les et �maci�s", a indiqu� Tarek Hefnawy, un membre de l'Unicef.
Le personnel de l'Unicef leur a imm�diatement donn� des suppl�ments tr�s nutritifs.
Sur les personnes �vacu�es, figurent �galement "336 hommes, �g�s de plus de 15 ans et de moins de 55 ans, (qui) ont �t� interpell�s (...) pour �tre interrog�s", a d�clar� Melissa Fleming, porte-parole du Haut-commissariat des r�fugi�s (HCR).
Quarante-deux d'entre eux ont �t� rel�ch�s, et les autres sont toujours entre les mains des autorit�s syriennes, selon le HCR.
Par ailleurs, 310 paquets de nourriture ont pu �tre donn�s ces derniers jours pour les familles restant dans la ville ainsi que 1,5 tonne de farine de bl�, "de quoi nourrir 1.550 personnes pendant un mois", selon le Programme alimentaire mondial (PAM).
Ces op�rations humanitaires ont �t� rendues possibles gr�ce � un cessez-le-feu entr� en vigueur vendredi et �tendu jusqu'� mercredi. Il a cependant �t� viol� � plusieurs reprises.
Alors que le conflit a fait en pr�s de trois ans plus de 136.000 morts et pouss� � la fuite des millions de personnes, les n�gociations de Gen�ve pi�tinaient.
"Nous n'accomplissons pas beaucoup de progr�s", a reconnu mardi M. Brahimi.
A Damas, le ministre syrien de la R�conciliation nationale Ali Haidar a jug� que les n�gociations �taient "vou�es � l'�chec en l'�tat actuel des choses", et pr�conis� la mise en place d'"un processus politique interne sans lien avec l'�tranger".
Les deux d�l�gations se sont bien assises � la m�me table mardi avec le m�diateur mais chacune a maintenu ses priorit�s, la lutte antiterroriste pour le gouvernement, et pour l'opposition avancer vers l'autorit� gouvernementale de transition demand�e par les grandes puissances dans Gen�ve I en 2012.
Les discussions doivent se poursuivre jusqu'� vendredi, jour d'une r�union trilat�rale de M. Brahimi avec des repr�sentants russe et am�ricain.
� 2014 AFP
