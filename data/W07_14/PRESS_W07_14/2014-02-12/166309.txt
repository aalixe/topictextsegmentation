TITRE: Un avion de transport militaire s'�crase sur le mont Fertas (Oum El Bouaghi) - Libert� Alg�rie , Quotidien national d'information
DATE: 2014-02-12
URL: http://www.liberte-algerie.com/actualite/un-avion-de-transport-militaire-saecrase-sur-le-mont-fertas-oum-el-bouaghi-215607
PRINCIPAL: 166304
TEXT:
Charte d'utilisation de
www.liberte-algerie.com
-          L�espace des commentaires sur liberte-algerie.com est ouvert � toute personne s�engageant � respecter les pr�sentes conditions g�n�rales d'utilisation par l�Internaute
-          L�internaute est pri� de prendre connaissance le plus r�guli�rement possible des conditions G�n�rales d'utilisation du site. Il faut savoir que liberte-algerie.com est libre de les modifier � tout moment sans notification pr�alable afin de les adapter aux �volutions du site et des lois et r�glements en vigueur.
-          La citation de la marque liberte-algerie.com sur un site Internet tiers ne signifie pas que liberte-algerie.com assume une quelconque garantie et responsabilit� quant au contenu du site Internet tiers ou de l'usage qui peut en �tre fait.
-         Tout internaute peut r�agir sur le contenu du site
-         Les informations fournies � liberte-algerie.com par un Utilisateur lors l�envoi de son commentaire sont communiqu�es sous la seule responsabilit� de cet Utilisateur.
-          En envoyant ses commentaires � liberte-algerie.com, l�utilisateur :
-      d�clare �tre autoris� � envoyer ses commentaires ;
-      autorise liberte-algerie.com � reproduire et repr�senter, int�gralement ou partiellement et � adapter les commentaires sur les services de communication �lectroniques �dit�s par liberte-algerie.com dans le monde entier.
-          liberte-algerie.com a le droit de retirer ou d�interdire l�acc�s � tout Commentaire contrevenant ou risquant de contrevenir aux lois et r�glements en vigueur ou qui ne serait pas conforme aux r�gles �ditoriales du site.
-          L'utilisateur est seul responsable de toute utilisation personnelle du contenu ou des commentaires publi�s sur le Site.
-          liberte-algerie.com n'exerce aucun contr�le quant au contenu des sites Internet tiers. L'existence d'un lien hypertexte entre le site et un site internet tiers ne signifie pas que liberte-algerie.com assume une quelconque garantie et responsabilit� quant au contenu du site Internet tiers ou de l'usage qui peut en �tre fait.
-          Dans l'hypoth�se o� l�utilisateur est une personne physique mineure, il d�clare et reconnait avoir recueilli l'autorisation aupr�s de ses parents ou du (ou des) titulaire(s) de l'autorit� parentale le concernant pour utiliser le site. Le (ou les) titulaire(s) de l'autorit� parentale a (ont) accept� d'�tre garant(s) du respect de l'ensemble des dispositions de la charte.
-          Les propos incitants � la haine raciale ou religieuse, les appels � la violence, l�utilisation de termes r�gionalistes, et tout message litigieux ne sont pas accept�s par liberte-algerie.com.
-          Veuillez �viter l�exc�s de propos de type belliciste, morbide ou guerroyeur.
-          Les commentaires o� on y constate de l�agressivit�, de la vulgarit� ou de la violence excessive dans le ton, ne sont pas autoris�es � �tre publi�s sur le site. Egalement les insultes personnelles entre participants.
-          Pas de pornographie, p�dophilie, obsc�nit�s et grossi�ret�s.
-          La r�p�tition de messages identiques ou tr�s voisins n�est pas autoris�e
-          Tout lien dirigeant vers un site raciste, islamophobe, ou que la r�daction jugera non conforme,  sera effac�.
-          Les  sp�culations ou r�v�lations � propos de l'identit� de tel ou tel participant ne sont pas tol�r�es.
-          Les tentatives d'usurpation d'un pseudonyme d�j� employ� ne sont pas tol�r�es.
-          Les messages personnels �chang�s entre participants ou post�s sur d�autres sites ne doivent pas �tre diffus�s sur le site.
-          L�usage normal pour publier les commentaires requiert un seul pseudonyme.
-          Veuillez poster vos sujets dans les rubriques appropri�es du site
-          En cas de malentendu persistant avec le m�diateur, vous �tes pri� de le contacter par email (l�adresse : journal.liberte.dz@gmail.com)
Nom
