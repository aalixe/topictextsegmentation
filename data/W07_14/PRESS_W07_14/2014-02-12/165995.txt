TITRE: 'Trésor nazi': une loi sur les oeuvres spoliées?
DATE: 2014-02-12
URL: http://www.lefigaro.fr/flash-actu/2014/02/12/97001-20140212FILWWW00043-tresor-nazi-une-loi-sur-les-uvres-spoliees.php
PRINCIPAL: 0
TEXT:
le 12/02/2014 à 07:51
Publicité
Ebranlée par la révélation de la découverte d'un "trésor nazi" , l'Allemagne souhaite se doter d'une loi facilitant la restitution d'oeuvres d'art volées sous le IIIe Reich pour que justice soit rendue, presque 70 ans après la fin de la guerre.
Ce projet de loi dont la discussion doit commencer vendredi a été élaboré après l'annonce en novembre de la découverte de 1.406 oeuvres, en partie probablement issues de pillages nazis chez des juifs, au domicile d'un octogénaire, Cornelius Gurlitt, à Munich.
Et l'affaire est loin d'être terminée puisque mardi encore, était annoncée la découverte de 60 oeuvres , dont des Monet, Manet et Renoir dans la maison de Salzbourg (Autriche) de M. Gurlitt, fils d'un marchand d'art au passé trouble sous le Troisième Reich.
Baptisée "Lex Gurlitt" par les médias, le projet doit être détaillé par l'Etat régional de Bavière devant le Bundesrat, la chambre haute du parlement qui représente les Länder.
Il propose notamment d'abolir la prescription de 30 ans au-delà de laquelle la propriété d'une oeuvre d'art ne peut plus être contestée, si le détenteur est considéré comme de "mauvaise foi", c'est-à-dire s'il connaissait la provenance de l'objet au moment de son acquisition.
"En principe ce projet de loi est un signal positif. Cela montre que les consciences politiques sont en train de se réveiller en Allemagne après les manquements du passé. Le cas Gurlitt a fait bouger les choses", a estimé auprès de l'AFP Markus Stötzel, avocat des héritiers d'Alfred Flechtheim, juif allemand et grand marchand d'art du XXe siècle.
