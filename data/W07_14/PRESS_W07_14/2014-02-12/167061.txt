TITRE: Twitter : une nouvelle interface � la Facebook
DATE: 2014-02-12
URL: http://www.reponseatout.com/reponse-conso/telephonie-internet/twitter-nouvelle-interface-facebook-a1012173
PRINCIPAL: 167059
TEXT:
Twitter : une nouvelle interface � la Facebook
Le 12/02/2014 � 12:48:43
Vues : 375 fois JE REAGIS
Certains �l�ments de la nouvelle interface de Twitter font penser � Facebook et Pinterest. | � DR
Twitter revoit le design des pages � profil �. Mise en avant des photos et vid�os, apparition d�une image de couverture sous forme de bandeau� Le r�seau social se serait-il inspir� de Facebook pour concevoir sa nouvelle interface ?
Il y a quelques semaines, Twitter modifiait l�apparence de son interface � l�image de ses applications iOS et Android. Nouveau changement en perspective : le r�seau social r�alise un lifting de la page profil de ses membres. Certains �l�ments font penser � Facebook et Pinterest .  Pas s�r que les twittos appr�cient.
Nouvelle interface Twitter : les photos et les vid�os mises en valeur
Dans sa version 2014, Twitter pr�sente la photo et la biographie de l�internaute � gauche, dans un espace plus cons�quent. Appara�t aussi, tout en haut, une image de couverture type Facebook, qui s��tend en bandeau sur toute la largeur de la page. La colonne centrale a �galement subi quelques changements. Les contenus sont dispos�s sous forme de petites vignettes ind�pendantes les unes des autres, un peu comme c�est le cas sur Pinterest. Par ailleurs, les photos - affich�es dans une taille plus importante - et les vid�os sont davantage mises en avant et regroup�es dans un nouvel onglet. L�espace � favoris � vient lui aussi s�ajouter � la liste des rubriques.
Twitter joue la carte de la simplicit� pour gagner des membres
Twitter compte 241 millions d�utilisateurs actifs. Selon le site sp�cialis� Re/code , plus d�un milliard de personnes se seraient en r�alit� inscrites sur le site depuis sa cr�ation en 2006. Ainsi, le taux de r�tention (clients qui restent) atteint seulement les 25 %. La raison de cet abandon massif s�explique essentiellement par la complexit� du site, difficilement accessible au premier abord, contrairement � Facebook. Par ce nouveau design, Twitter esp�re gagner en simplicit�. Le risque : d�cevoir les habitu�s, qui pourraient prendre la fuite.
Voici la nouvelle interface Twitter :
Capture d��cran r�alis�e par Matt Petronzio, journaliste pour le site am�ricain Mashable
