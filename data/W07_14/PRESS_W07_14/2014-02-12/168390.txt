TITRE: C � Vous : Les Inconnus jugent les critiques sur leur film "normales"
DATE: 2014-02-12
URL: http://www.gentside.com/les-inconnus/c-a-vous-les-inconnus-jugent-les-critiques-sur-leur-film-normales_art59138.html
PRINCIPAL: 168384
TEXT:
Publi� par Basile Caillaud , le
12 f�vrier 2014
Invit�s de C � Vous ce mercredi 12 f�vrier, Les Inconnus ont ainsi pu r�agir aux critiques concernant leur film sorti ce m�me jour. Des critiques parfois dures avec Les Trois Fr�res Le Retour mais que les trois humoristes jugent "normales".
N�gatif. C'est sans doute l'adjectif qui qualifierai le mieux la journ�e qu'ont d� vivre Les Inconnus ce mercredi 12 f�vrier, date de la sortie nationale de leur film, Les Trois Fr�res, Le Retour. Il faut dire que les critiques de la presse sur ce retour ont �t� l�gion. Pire certains analysent cela comme une avalanche de critiques cinglantes.
"Il faut se rendre � l'�vidence : aucune sc�ne ni aucune r�plique  de cette suite ne resteront dans les annales", regrette par exemple la journaliste du Point. "Les blagues se cantonnent au graveleux, les bons mots ne font pas vraiment mouche. R�sultat : l�humour ne passe plus la barre", estime de son c�t� Europe 1. M�me son de cloche au Nouvel Observateur qui juge ce "come-back triste et p�teaux".
"Le temps, la meilleure des critiques"
Hier, Christophe Carri�re, chroniqueur dans Touche pas � mon poste, n'�tait pas pr�sent dans l'�mission qui avait Les Inconnus comme invit�s. Il faut dire qu'il avait �t� �galement tr�s s�v�re vis-�-vis du dernier opus du trio comique. Au final, seul Le Parisien-Aujourd'hui en France s'est autoris� � relater une critique positive sur le film. Une bonne notation qui peut cependant s'expliquer par le fait que le journal est partenaire de la production...
Apr�s ce toll�, Les Inconnus �taient invit�s ce mercredi soir de C � Vous sur France 5. Un rendez-vous suppl�mentaire pour la promotion de leur nouveau b�b� mais aussi et surtout, l'occasion pour eux de r�agir aux propos, parfois violents, de la presse sp�cialis�e. Et alors qu'Anne-Sophie Lapix et ses coll�gues avaient pr�par�s des r�actions de spectateurs � la sortie du cin�ma (de bonnes r�actions par ailleurs), Les Inconnus se livraient.
"Les critiques, c'est normal, c'est bon signe", estime Pascal Legitimus avant de poursuivre. "On peut pas plaire � tout le monde, d'une part. Et puis ils (les critiques cin�ma, ndlr) n'ont pas le m�me regard que le peuple". Des propos tenus avant que la pr�sentatrice de C � Vous ne leur demande : "Elles vous atteignent pas du tout ces critiques ?". "Si", r�pondaient en ch�ur les trois humoristes. Mais comme le pr�cise Didier Bourdon, "c'est le temps qui est la meilleure des critiques".
Suivez Gentside M�dia sur Facebook
Vous �tes d�j� abonn� ? Ne plus afficher
Suivre
