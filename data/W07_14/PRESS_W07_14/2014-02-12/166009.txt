TITRE: JO 2014 / SKI ALPIN - Descente (F) : Maze-Gisin, deux filles en or
DATE: 2014-02-12
URL: http://www.sport365.fr/jo-2014-sotchi/descente-f-maze-gisin-deux-filles-en-or-1102626.shtml
PRINCIPAL: 166008
TEXT:
JO 2014 / SKI ALPIN - Publi� le 12/02/2014 � 08h58 -  Mis � jour le : 12/02/2014 � 11h31
0
Descente (F) : Maze-Gisin, deux filles en or
La Slov�ne Tina Maze et la Suissesse Dominique Gisin se partagent le titre olympique de descente en signant exactement le m�me temps. Lara Gut, une autre Suissesse, compl�te le podium.
PANORAMIC
Deux m�dailles d�or pour le prix d�une�! La descente olympique f�minine a offert la premi�re place � deux skieuses�: Tina Maze et Dominique Gisin, ce mercredi. Partie en huiti�me sur la piste, la Suissesse a profit� d�une piste en meilleur �tat pour signer le temps de r�f�rence de la discipline que personne n�a finalement battu. La Slov�ne Tina Maze, auteur d�un excellent d�part, a tout de m�me r�ussi � �galer la performance de Gisin pour se parer d�or � son tour. Lara Gut, troisi�me � 0��10, compl�te le podium d�une course plac�e sous le signe de la croix helv�tique puisque Fabienne Suter se classe cinqui�me devant des sp�cialistes comme l�Am�ricaine Julia Mancuso (8eme), m�daill�e d�argent � Vancouver il y a quatre ans, l�Autrichienne Nicole Hosp, 9eme, et l�Allemande Maria Hoefl-Riesch, sacr�e en super-combin� lundi, mais seulement 13eme aujourd�hui.
JO 2014 - DESCENTE (F)
1- Tina Maze (SLO) en 1�41��57
- Dominique Gisin (SUI) mt
3- Lara Gut (SUI) � 0�10��
4- Daniela Merighetti (ITA) � 0��27
5- Fabienne Suter (SUI) � 0��37
6- Lotte Smiseth Sejersted (NOR) � 0''44
7- Edit Miklos (HON) � 0��71
8- Julia Mancuso (USA) � 0��99
9- Nicole Hosp (AUT) � 1��05
10- Iika Stuhec (SLO) � 1''08
11- Laurenne Ross (USA) � 1��11
12- Elena Fanchini (ITA) � 1��13
13- Maria Hoefl-Riesch (ALL) � 1��17
...
