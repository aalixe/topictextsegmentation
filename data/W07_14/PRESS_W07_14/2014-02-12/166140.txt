TITRE: Dassault fix� sur la lev�e de son immunit� parlementaire ce mercredi - RTL.fr
DATE: 2014-02-12
URL: http://www.rtl.fr/actualites/info/politique/article/dassault-fixe-sur-la-levee-de-son-immunite-parlementaire-ce-mercredi-7769661267
PRINCIPAL: 166139
TEXT:
Accueil > Toutes les Actualit�s info > Dassault fix� sur la lev�e de son immunit� parlementaire ce mercredi
Dassault fix� sur la lev�e de son immunit� parlementaire ce mercredi
L'industriel Serge Dassault � Paris le 17 octobre 2013.
Cr�dit : AFP / FRANCOIS GUILLOT
Le bureau du S�nat se prononce ce mercredi 12 f�vrier sur la lev�e de l'immunit� parlementaire de l'homme d'affaires, qu'il a lui-m�me r�clam�e.
Le bureau du S�nat devrait lever sans surprise ce mercredi 12 f�vrier l'immunit�  parlementaire de Serge Dassault. Cela permettra aux magistrats  du p�le financier de Paris de  placer le s�nateur de l'Essonne de 88 ans en garde � vue dans le cadre d'une affaire  d'achats pr�sum�s de voix � Corbeil-Essonnes (Essonne). "Je demande la lev�e de mon immunit� parlementaire" , avait annonc� lundi 10 f�vrier l'industriel et propri�taire du Figaro. Par deux fois en effet, l'ancien maire de  Corbeil-Essonnes a de justesse �chapp� � la mesure. La premi�re fois, il  s'agissait d'un probl�me de proc�dure.
La derni�re, le 8 janvier,  avait d�clench� un toll�: les 26 membres du bureau du S�nat, 14 de  gauche et 12 de droite, avaient rejet� la lev�e par 13 voix contre 12,  et une abstention. Il avait donc manqu� deux voix de gauche. Jean-Marc Ayrault s'�tait lui-m�me dit "choqu�" par ce  vote. "La justice peut, si elle le souhaite, tr�s vite, c'est-�-dire  demain, faire une nouvelle demande (de lev�e d'immunit�) et l�, je crois  que le contexte aura chang� car on ne peut continuer avec ce genre de  pratiques qui portent atteinte � la d�mocratie", avait-il ajout�.
Serge Dassault demande � se "d�fendre" et montrer sa "totale innocence"
"C'est  la pire journ�e que j'aie v�cue au S�nat depuis mon �lection � sa  pr�sidence", avait reconnu de son c�t� le pr�sident PS de la Haute  Assembl�e, Jean-Pierre Bel, membre de droit du bureau, en proposant de  modifier le mode de scrutin pour les lev�es d'immunit� parlementaire.
�
"M�me  si cette lev�e d'immunit� provoque mon placement en garde � vue, je  suis pr�t � affronter cette �preuve", avait affirm� lundi Serge Dassault. "Je pourrai de ce fait avoir acc�s � la  proc�dure" et "pouvoir me d�fendre contre ces accusations". "Je pourrai  d�montrer ma totale innocence de ces soi-disant achats de votes,  accusations invent�es de toutes pi�ces par certains de mes adversaires  politiques", avait-il ajout�.
Soup�ons d'achat de vote � Corbeil-Essonnes
Dans  cette instruction ouverte depuis mars pour achat de votes, corruption,  blanchiment et abus de biens sociaux, les magistrats s'int�ressent aux  �lections municipales organis�es en 2008, 2009 et 2010 �  Corbeil-Essonnes, remport�es par Serge Dassault, puis par son bras droit,  Jean-Pierre Bechter. Les juges s'int�ressent aussi � d'importants mouvements de fonds depuis le Liban vers la France.
En  annulant le scrutin de 2008, le Conseil d��tat avait tenu pour  "�tablis" des dons d'argent aux �lecteurs, sans se prononcer sur leur  ampleur et bien que des t�moins se soient r�tract�s. Mi-septembre,  les avocats de l'industriel avaient estim� que leur client �tait  "l'objet, depuis plusieurs ann�es, de demandes pressantes de remise  d'argent par divers individus qui avaient �t� inform�s de sa  g�n�rosit�". Il lui est arriv� "d'accorder un soutien financier, mais  toujours en dehors de toute d�marche �lectorale", avaient-ils affirm�.
La r�daction vous recommande
