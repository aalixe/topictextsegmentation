TITRE: BRETAGNE  : L'ARRÊTé DE CATASTROPHE NATURELLE VA ÊTRE éTENDU, Actualité
DATE: 2014-02-12
URL: http://patrimoine.lesechos.fr/patrimoine/assurance/actu/0203309331448-bretagne-l-arr-te-de-catastrophe-naturelle-va-tre-etendu-649664.php
PRINCIPAL: 0
TEXT:
Aide
Tous droits r�serv�s - Les Echos 2014
Conform�ment � la loi Informatique et Libert� n� 78-17 du 6 janvier 1978 relative � l'informatique, aux fichiers et aux libert�s, nous nous engageons � informer les personnes qui fournissent des donn�es nominatives sur notre site de leurs droits, notamment de leur droit d'acc�s et de rectification sur ces donn�es nominatives. Nous nous engageons � prendre toutes pr�cautions afin de pr�server la s�curit� de ces informations et notamment emp�cher qu'elles ne soient d�form�es, endommag�es ou communiqu�es � des tiers.
