TITRE: VIDEO. S�nat: l'immunit� parlementaire de Serge Dassault a �t� lev�e
DATE: 2014-02-12
URL: http://www.leparisien.fr/politique/immunite-de-serge-dassault-le-senat-etudie-sa-levee-12-02-2014-3583497.php
PRINCIPAL: 166725
TEXT:
VIDEO. S�nat: l'immunit� parlementaire de Serge Dassault a �t� lev�e
�
Tweeter
Apr�s l'avoir refus� deux fois, le bureau du S�nat devrait lever, ce mercredi, l'immunit� parlementaire de Serge Dassault, le s�nateur UMP et homme d'affaires l'ayant lui-m�me demand� lundi alors que la majorit� s�natoriale de gauche est d�termin�e � l'obtenir. | AFP / Fred Dufour
L'issue �tait attendue.�Le bureau du S�nat a lev�, ce mercredi, l'immunit� parlementaire de Serge Dassault, s�nateur UMP de l'Essonne. La r�union du bureau du S�nat faisait suite � une troisi�me demande formul�e par la justice dans le cadre d'une enqu�te sur des achats pr�sum�s de voix � Corbeil-Essonnes ,
Le r�sultat de la r�union, d�but�e � 9h30 ne faisait gu�re de myst�re, Serge Dassault ayant lui-m�me demand� la lev�e de son immunit� pour d�montrer, a-t-il dit, qu'il n'avait �rien � se reprocher�.
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Il est d�sormais susceptible d'�tre plac� en garde � vue.
Vote � main lev�e et non plus � bulletin secret
Avant de se prononcer, le bureau a adopt� une proposition du pr�sident du S�nat, le socialiste Jean-Pierre Bel, de modifier le mode de vote lors des demandes d'immunit�: � main lev�e, et non plus � bulletin secret. Par deux fois en effet, l'ancien maire de Corbeil-Essonnes a de justesse �chapp� � la mesure. Une premi�re fois, en raison d'un probl�me de proc�dure. La seconde, le 8 janvier, avait d�clench� un toll� : les 26 membres du bureau du S�nat, 14 de gauche et 12 de droite qui s'exprimaient � bulletin secret, avaient rejet� la lev�e par 13 voix contre 12 et une abstention. Il avait donc manqu� deux voix de gauche.
VIDEO. Le S�nat a lev� l'immunit� parlementaire de Serge Dassault
Ce mercredi, les 14 s�nateurs de gauche, membres du bureau, ont donc vot� � main lev�e pour la lev�e de l'immunit� de l'industriel et s�nateur UMP, �le reste refusant de participer au vote�, soit les s�nateurs de droite pr�sents, a pr�cis� Jean-Pierre Bel. Deux membres de droite du bureau, dont l'ancien Premier ministre Jean-Pierre Raffarin, �taient absents.
Dans cette instruction ouverte depuis mars pour achat de votes, corruption, blanchiment et abus de biens sociaux, les magistrats s'int�ressent aux �lections municipales organis�es en 2008, 2009 et 2010 � Corbeil-Essonnes, remport�es par Serge Dassault, puis par son bras droit, Jean-Pierre Bechter. En annulant le scrutin de 2008, le Conseil d'Etat avait tenu pour ��tablis� des dons d'argent aux �lecteurs, sans se prononcer sur leur ampleur et bien que des t�moins se soient r�tract�s.
Pour ses avocats, l'ancien maire de Corbeil-Essonnes est �l'objet, depuis plusieurs ann�es, de demandes pressantes de remise d'argent par divers individus qui avaient �t� inform�s de sa g�n�rosit�. Il lui est arriv� �d'accorder un soutien financier, mais toujours en dehors de toute d�marche �lectorale�, avaient-ils affirm�.
LeParisien.fr
R�agir avec mon compte You / le Parisien
Identifiant
Votre e-mail* (ne sera pas visible)
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
D�connexion
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
> Cliquez ici pour lire les 25 r�actions � l�article
hibou 13/02/2014 - 12h38
Je suis indign� : 1 ) Un s�nateur ose dire : < avec le vote � main lev�e cela DEVIENT un vote politique>.... Il n'a pas compris que c��tait d�j� un vote politique alors que ce devrait �tre un vote de citoyen dans l�int�r�t de son pays. 2 ) C'est devenu un vote politique et int�ress�....mais le s�nateur repr�sente les citoyens de sa circonscription alors, il est bien naturel que ceux ci  veulent conna�tre le vote de leur s�nateur pour savoir < pour qui il roule >.  3) La r�ponse est claire.  il roule g�n�ralement pour son parti et non pas pour ses �lecteurs.  Certains roulent selon leur int�r�t personnel  ( idem pour les maires.)  et quelquefois pour les lobbies ce qui est une trahison de leurs �lecteurs.. Ce qui est choquant c'est qu'un s�nateur n'ait pas le courage de voter et par des arguments fallacieux, veuille tenir secret son vote.  Il y a l� un commencement de l�chet� contre lequel il va falloir lutter en gardant le vote secret pour les citoyens et en rendant obligatoire la divulgation des votes des leur pr�tendus repr�sentants . Certains �lus repr�sentent des lobbies..... Cela est tenu secret �videmment. Et quand un journaliste montre ces liens, vous pouvez voir comment il est trait� ..
ko20q 12/02/2014 - 23h12
en demandant lui m�me cette lev�e, il n'a fait qu'enlever une �pine du pied des s�nateurs ump!
tempo1813 12/02/2014 - 18h52
La proc�dure va prendre suffisamment de temps pour que le suspect meure de vieillesse. Au final, un second couteau paiera l'addition.
R�ponse Signaler un abus
philippe 12/02/2014 - 18h32
En r�ponse @Bernie. EH  OUI   !!!!  c ' est  �a  le  probl�me  !!!  Mais  les  enqu�teurs  sont  malins  et  tenaces  .  il  y  a  quand  m�me  un  mort  et  l ' affaire  est  tr�s  grave  !!!!!  Ce  fric  !!!!  c ' est  une  maladie  �  droite  .
R�ponse Signaler un abus
bob 12/02/2014 - 17h19
Quel bande de faux-culs et de girouettes ces s�nateurs  surtout ceux de droite et mention speciale � M Raffarin pour son absence courage fuyons.Supprimons au plus vite le s�nat par mesure d'�conomie.
gavroche500 12/02/2014 - 17h11
Une fois de plus les nostalgiques de la droite arrogante pr�f�re ne pas voir l'immoralit� de ses �lus qui se refusent � lever l'immunit� de l'un des leur, que cela soit pour des soup�ons de prises d'int�r�ts ou m�me d�assassinat, rien n'y fait il faut prot�ger l'�lite d�linquante mais tol�rance "0" pour la France d'en bas, quelle soit de souche ou d'adoption. Tout est dit  d�s les 5 premi�res r�actions.  Quand � mickl91 il feint de nous prendre pour des idiots, comme s'il ne savait pas que Dassault n'avait pas d'autre choix, cette-fois �i ! Difficiele d'acheter des votes ... quand c'est � main lev�. Malheureusement pour ceux qu'il soutient, c'est FINI. Vote � main lev� = je sais ce que tu as vot� !
R�ponse Signaler un abus
machin 12/02/2014 - 14h07
Quand les enqu�teurs de la police vont se pointer, il y aura quelqu'un pour leur dire : "prenez les patins; je viens de faire le m�nage..."
R�ponse Signaler un abus
Gayel 12/02/2014 - 14h02
@52fb62d145944:  1/ Enfin, en vote secret si son immunit� � �t� pr�serv�e, c'est gr�ce � des s�nateurs de gauche... 2/Il � demand� � ce que son immunit� soit lev�e. On supposera qu'il � fait cela pour couper l�herbe sous le pied de la gauche. 3/Prot�ger ses brebis galeuses est une chose: Changer les r�gles d'un processus d�mocratique pour changer les r�sultats est bien pire!
R�ponse Signaler un abus
Bernie 12/02/2014 - 13h35
Que de foin pour qu'un citoyen se voit traiter � l'�gal d'un autre devant la justice ! L'immunit� parlementaire ne doit pas prot�ger certains d'enqu�tes de justice, on n'est plus au temps de la royaut� ni dans un milieu de malfrats o� on a le temps de faire dispara�tre les preuves.
R�ponse Signaler un abus
Logik 12/02/2014 - 13h34
En r�ponse @Ben . Choquant? Non, juste r�aliste: comment g�rer les pressions que le vote � main lev�e g�n�re, vous �tes oblig� de vous d�clarer, de vous justifier et vous pouvez subir des brimades. Ce qui s'est sans doute pass� pour changer le r�sultat!
