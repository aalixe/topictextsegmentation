TITRE: Les deux Cor�es auront des n�gos �� haut niveau�
DATE: 2014-02-12
URL: http://fr.canoe.ca/infos/international/archives/2014/02/20140211-070906.html
PRINCIPAL: 165433
TEXT:
Les deux Cor�es auront des n�gos �� haut niveau�
La r�union se tiendra dans le village frontalier de Panmunjom. �Photo AFP
Derni�res nouvelles
Tweeter
11-02-2014 | 07h09
S�OUL - Les Cor�es du Nord et du Sud vont entreprendre des n�gociations � un haut niveau d�s mercredi, a annonc� mardi le minist�re sud-cor�en de l'Unification, avant la reprise pr�vue des r�unions des familles s�par�es par la guerre de Cor�e il y a 60 ans.
La r�union se tiendra dans le village frontalier de Panmunjom, lieu habituel de rencontre des d�l�gations des deux voisins, a pr�cis� le prote-parole du minist�re, Kim Eui-Do.
De hauts responsables des minist�res sud-cor�ens de la D�fense, de l'Unification et du Bureau pr�sidentiel y participeront.
Jusqu'� r�cemment, S�oul disait �tre d'accord pour des n�gociations � haut niveau seulement si Pyongyang s'engageait fermement � abandonner son programme d'armement nucl�aire.
Les discussions de mercredi porteront sur des �sujets importants�, dont la reprise du programme des r�unions des familles s�par�es par la guerre de Cor�e (1950-1953), a ajout� le porte-parole sud-cor�en.
Ces rencontres, qui visent � r�unir pour quelques jours les fr�res, soeurs, enfants et parents s�par�s depuis plus de 60 ans, ont �t� suspendues depuis trois ans, en raison d'un regain de tensions sur la p�ninsule cor�enne.
Des r�unions pr�vues pour septembre dernier avaient �t� annul�es � la derni�re minute par Pyongyang. Un nouveau train de rencontres est pr�vu entre les 20 et 25 f�vrier, mais plusieurs experts, c�t� sud-cor�en, pr�voient qu'elles soient elles aussi annul�es au dernier moment.
D'autant que se d�rouleront � partir de fin f�vrier les manoeuvres annuelles am�ricano-cor�ennes, que Pyongyang critique chaque ann�e violemment, consid�rant qu'il s'agit d'une r�p�tition pour une invastion du nord de la p�ninsule par S�oul et son alli� am�ricain.
Le Nord n'a rien annonc� de son c�t� mais le minist�re sud-cor�en de l'Unification a indiqu� que Pyongyang �tait � l'origine de la rencontre de mercredi.
