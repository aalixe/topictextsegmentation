TITRE: Algérie: 3 jours de deuil après le crash d'un avion militaire qui a fait 77 morts - 12 février 2014 - Le Nouvel Observateur
DATE: 2014-02-12
URL: http://tempsreel.nouvelobs.com/topnews/20140212.AFP9846/algerie-3-jours-de-deuil-apres-le-crash-d-un-avion-militaire-qui-a-fait-77-morts.html
PRINCIPAL: 0
TEXT:
Procès Agnelet : son ex-femme menace de se suicider
Actualité > TopNews > Algérie: 3 jours de deuil après le crash d'un avion militaire qui a fait 77 morts
Algérie: 3 jours de deuil après le crash d'un avion militaire qui a fait 77 morts
Publié le 12-02-2014 à 06h31
Mis à jour à 12h20
A+ A-
Un deuil national de trois jours débute mercredi en Algérie après le crash d'un avion militaire dans l'est du pays, qui a fait 77 morts, l'une des pires catastrophes aériennes en Algérie. (c) Afp
Alger (AFP) - Un deuil national de trois jours débute mercredi en Algérie après le crash d'un avion militaire dans l'est du pays, qui a fait 77 morts, l'une des pires catastrophes aériennes en Algérie.
"Le crash a fait 77 victimes et un rescapé, grièvement blessé, a été transféré à l'hôpital militaire régional de Constantine", selon le ministère algérien de la Défense cité par l'agence APS.
Un survivant souffrant d'un traumatisme crânien a été découvert par la protection civile.
En début de soirée, 76 corps, dont quatre femmes, avaient été récupérés par les secouristes, selon un bilan de la protection civile.
L'avion, un Hercules C-130, qui assurait la liaison entre la préfecture de Tamanrasset (2.000 km au sud d'Alger) et Constantine (450 km à l'est d'Alger), transportait des militaires et des familles de militaires, selon la même source.
L'appareil s'est écrasé alors qu'il survolait le mont Fortas dans la wilaya (préfecture) d'Oum El Bouaghi (500 km à l'est d'Alger) vers midi (11H00 GMT).
La télévision a diffusé des images des lieux montrant la carcasse de l'appareil reposant dans un environnement désert, rocailleux et vert, dans une zone montagneuse.
Le ministère de la Défense a indiqué que l'avion transportait 74 passagers et quatre membres de l'équipage.
La radio algérienne avait d'abord annoncé "une centaine de morts" en fin d'après-midi parlant de 103 personnes à bord.
"Les conditions météorologiques très défavorables avec un orage accompagné de chutes de neige seraient à l'origine de ce crash", a annoncé le ministère de la Défense dans un communiqué.
Le crash se serait produit au moment des manœuvres d'approche de l'aéroport de Constantine.
"A la suite de cet accident, le plan de recherches et de sauvetage a aussitôt été déclenché et les unités de secours relevant de l'Armée nationale populaire (ANP) et de la Protection civile se sont déplacées sur les lieux pour apporter les premiers secours", selon le ministère de la Défense.
Commission d'enquête
Près de 250 secouristes de la protection civile, sont affectés au site du crash rendu difficile d'accès en raison du mauvais temps et de l'escarpement du lieu, a annoncé la radio algérienne.
Un haut responsable militaire, le colonel Lahmadi Bouguern, a fait état à l'agence APS de fortes rafales de vents depuis quelques jours dans cette région et d'un manque de visibilité.
Le président algérien Abdelaziz Bouteflika a décrété trois jours de deuil national et a présenté ses condoléances aux familles des victimes.
"Les soldats qui ont péri dans le crash de l'avion militaire sont des martyrs du devoir, aussi nous décrétons un deuil national de trois jours à partir de mercredi", a écrit M. Bouteflika dans un message de condoléances diffusé par l'APS.
Le président a également décidé que "la journée de vendredi sera consacrée au recueillement à leur mémoire".
"Une commission d'enquête a été créée et dépêchée sur les lieux pour déterminer les causes et les circonstances exactes de ce tragique accident", selon le communiqué ministériel.
Le site en ligne du journal francophone El Watan affirme qu'une des deux boîtes noires a été retrouvée.
Le vice-ministre de la Défense nationale, Ahmed Gaïd Salah, devait se rendre sur les lieux.
Le précédent accident le plus meurtrier date de mars 2003. Un Boeing 737-200 de la compagnie publique algérienne Air Algérie avait fait 102 morts et un blessé en s'écrasant peu après son décollage de Tamanrasset.
Début décembre 2012, deux avions militaires, se livrant à des entraînements, se sont percutés en plein vol à Tlemcen, dans l’extrême ouest algérien, provoquant la mort des pilotes.
En novembre 2012, un bimoteur militaire de type CASA C-295, qui transportait une cargaison de papier fiduciaire pour la fabrication de billets pour la Banque d'Algérie s'était écrasé en Lozère. Les cinq militaires à bord et le représentant de la banque d'Algérie ont péri dans le crash.
En 2003, un Hercule C-130 de l’armée s'était écrasé sur un quartier résidentiel à Boufarik (banlieue d'Alger), faisant 20 morts -les quatre membres de l’équipage, huit passagers et huit personnes au sol suite à l’effondrement d’une habitation.
Partager
