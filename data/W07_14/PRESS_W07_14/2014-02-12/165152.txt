TITRE: Etats-Unis : les r�publicains renoncent � rejouer la carte du plafond de la dette
DATE: 2014-02-12
URL: http://www.lemonde.fr/ameriques/article/2014/02/12/etats-unis-la-chambre-vote-le-relevement-du-plafond-de-la-dette-jusqu-en-2015_4364659_3222.html
PRINCIPAL: 165151
TEXT:
Etats-Unis : les r�publicains renoncent � rejouer la carte du plafond de la dette
Le Monde |
� Mis � jour le
12.02.2014 � 14h01
La Chambre des repr�sentants a approuv� mardi 11 f�vrier le rel�vement du plafond de la dette des Etats-Unis jusqu'au 15 mars 2015 sans conditions, une mesure que le S�nat doit encore ent�riner pour �carter d�finitivement tout risque de d�faut de paiement.
La mesure a �t� adopt�e par 221 voix contre 201, gr�ce � l'appui de la quasi-totalit� des d�mocrates et de seulement 28 r�publicains. Le Congr�s a jusqu'au 27 f�vrier pour adopter d�finitivement le texte.
Les r�publicains, qui dominent la Chambre des repr�sentants, avaient d�clar� plus t�t mardi renoncer � exiger des contreparties au rel�vement du plafond de la dette , une annonce surprise assimil�e � une capitulation face au pr�sident Barack Obama . Accul�s, ils ont laiss� proc�der au vote, bien que l'immense majorit� des r�publicains aient vot� contre.
��L'AM�RIQUE N'A PAS D'AUTRE CHOIX QUE DE PAYER SES FACTURES��
La limite l�gale de la dette, suspendue en octobre apr�s une rude bataille parlementaire, a �t� r�activ�e vendredi � hauteur de 17�211 milliards de dollars.� Le Tr�sor a pr�venu qu'il ne pourrait tenir que jusqu'au 27 f�vrier sans une nouvelle autorisation d' emprunter de la part du Congr�s�: au-del�, le d�faut de paiement menacerait.
En renon�ant � la confrontation, les r�publicains ferment le chapitre douloureux des trois derni�res ann�es. A chaque �ch�ance sur la dette, ils avaient insist� pour obtenir des concessions sur la r�duction des d�penses ou contre la r�forme du syst�me de sant� de Barack Obama. ��L'Am�rique n'a pas d'autre choix que de payer ses factures��, a d�clar� le d�mocrate Steny Hoyer mardi.
Economie am�ricaine
