TITRE: Schumacher victime d'une pneumonie ? - F1 - Sports.fr
DATE: 2014-02-12
URL: http://www.sports.fr/f1/articles/schumacher-une-pneumonie-qui-inquiete-1008861/
PRINCIPAL: 0
TEXT:
Michael Schumacher souffrirait désormais d'une pneumonie. (Reuters)
Par François Kulawik
12 février 2014 � 09h06
Mis à jour le
12 février 2014 � 12h38
L’inquiétude grandit autour de Michael Schumacher au CHU de Grenoble. Dans le coma depuis son accident de ski, il y a un mois et demi, l’ancien pilote allemand souffrirait également d’une infection pulmonaire.
Plus de six semaines sont passées depuis le 29 décembre dernier et l’accident de ski dont a été victime Michael Schumacher à Méribel, et la situation du septuple champion du monde, toujours hospitalisé dans le coma à Grenoble, semble désormais s’aggraver. Et pas seulement parce que l’ancienne icône de Ferrari ne semble pas réagir aux stimuli des médecins.
Alors que l’équipe du Professeur Payen, chef du service de réanimation à l’hôpital isérois, a en effet entamé une phase de réveil progressif depuis plusieurs jours, une pneumonie également été diagnostiquée, selon le quotidien allemand Bild, qui explique que de puissants antibiotiques ont été administrés à Michael Schumacher, sans savoir si la phase de réveil a pu être dès lors poursuivie.
Et le Professeur Heinz Peter Moecke, responsable des urgences à l’hôpital de Hambourg d’expliquer la nature des complications. "La pneumonie est une maladie grave. Et elle est d’autant plus dangereuse dans le cas de Michael Schumacher que le corps bénéficie de moins d’oxygène alors qu’il est par ailleurs affaibli par l’infection", a ainsi expliqué le spécialiste allemand.
Contacté par le quotidien allemand, Sabine Kehm, la porte-parole du clan Schumacher, a néanmoins refusé de commenter «des spéculations». 
