TITRE: La California de Ferrari passe au turbo  - La Revue Auto
DATE: 2014-02-12
URL: http://www.larevueautomobile.com/News/Ferrari_California-T_7037.actu
PRINCIPAL: 168267
TEXT:
�
La California de Ferrari passe au turbo
Avec son � petit � V8 atmosph�rique la California �tait devenue depuis peu, le parent pauvre de la gamme Ferrari. Pour ce nouveau mill�sime, la marque au cheval cabr� a d�cid�e de lui offrir une cure de jeunesse aussi bien m�caniquement qu?esth�tiquement parlant.
C�est le salon de l�auto de Gen�ve qui verra l�apparition en premi�re mondiale de la toute nouvelle Ferrari California T. Sous le capot moteur, le V8 de 3,8 litres re�oit l�aide d�un turbocompresseur pour faire grimper la puissance � 560 ch, soit 70 ch de plus que la derni�re �volution du V8 4.3 l de la California � moteur atmosph�rique. Mais le plus bluffant est que le couple gagne 250 Nm pour atteindre 755 Nm � 4.750 tr/min.
Avec cette cavalerie, la California T explose le 0 � 100 km/h en 3,6 s (contre 3,8 s pour la version atmosph�rique) et la vitesse de pointe de 316 km/h. Ferrari annonce une baisse de consommation de 15 % soit une moyenne mixte de 10,5l aux 100km (250gr de co2).�
Le coup�-cabriolet, California T, profite �galement de quelques retouches esth�tiques. Le plus notable est la nouvelle calandre encadr�e par de nouvelles optiques effil�es et l'implantation des sorties d'�chappement d�sormais horizontale.
Dans l�habitacle, c�est toujours le r�gne du cuir, mais il re�oit un nouveau volant et un cadran de contr�le du turbo sur la console centrale.
Le prix de la Ferrari California T devrait flirter avec les 200.000�
Benoit Alves - Publication :                          12/02/2014 � 19:37:26
Photos
