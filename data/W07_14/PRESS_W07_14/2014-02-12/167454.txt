TITRE: Coca-Cola lance une nouvelle marque de boissons en France
DATE: 2014-02-12
URL: http://www.lefigaro.fr/societes/2014/02/12/20005-20140212ARTFIG00171-coca-cola-lance-une-nouvelle-marque-de-boissons-en-france.php
PRINCIPAL: 167449
TEXT:
Publié
le 12/02/2014 à 13:18
La marque Finley sera commercialisée d'abord en France, à partir d'avril, avec de nouveaux formats dont une canette de 25 cl. Crédits photo : COCO COLA COCO COLA/COCO COLA COCO COLA
Avec Finley, Coca-Cola espère séduire les jeunes adultes qui ne consomment pas de sodas. C'est son lancement le plus ambitieux depuis Coca-Cola Zéro en 2007.
Publicité
Coca-Cola part à la conquête des adultes. Le géant d'Atlanta a lancé en grande pompe ce mercredi matin une nouvelle marque, Finley, destinée à séduire les adultes qui ne boivent pas de sodas . Commercialisée d'abord en France à partir d'avril, Finley proposera une gamme de trois boissons pétillantes aux fruits (orange-cranberry, citron-fleur de sureau, pamplemousse-orange sanguine) ainsi qu'un tonic faiblement caloriques (20 calories pour 100 ml).
«C'est le lancement le plus ambitieux de Coca-Cola depuis Coca-Cola Zéro en 2007», promet Ilan Ouanounou, vice-président commercial de Coca-Cola Entreprise . Avec cette nouvelle boisson, dotée d'un logo noir, l'entreprise espère séduire les 25-45 ans qui ne sont que 10% à boire des «soft drinks» lorsqu'ils font une pause, leur préférant café, thé ou eau. Elle entend bien imposer le «réflexe» Finley «à tous les moments de la journée».
Coca-Cola vient marcher sur les plates-bandes d'Orangina-Schweppes
Coca-Cola n'avait jusque-là aucune marque grand public spécialement conçue pour les adultes à l'exception de Vitaminwater, positionnée haut de gamme, et de deux marques de niche lancées ces deux dernières années, la boisson maltée Tumult et la limonade Limon & Nada. Du coup, Finley sera résolument positionnée sur le c�ur de marché (1,65 euro la bouteille de 1,5l), avec de nouveaux formats dont une canette de 25 cl.
En débarquant sur cette nouvelle catégorie, les boissons aux fruits gazeuses, Coca-Cola vient marcher sur les plates-bandes d' Orangina-Schweppes , leader du marché avec sa célèbre boisson jaune (24,7% de parts de marché) et son tonic (21,8%). «Aujourd'hui, seul Schweppes a un positionnement adulte, reconnaît Ilan Ouanounou. C'est une grande marque, bien installée. Nous sommes convaincus que sur cette catégorie, il y a encore de la place et du potentiel pour une deuxième boisson pour adultes».
Coca-Cola, qui ne comptait que Fanta (16,1% du marché) sur cette catégorie, fait donc bien figure de challenger (16,1%). Mais le jeu en vaut la chandelle. Il s'agit du deuxième segment du marché des «soft drinks» en volume derrière les colas, mais qui affiche la plus forte croissance.
