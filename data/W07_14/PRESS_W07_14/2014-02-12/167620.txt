TITRE: Toulouse. L'ex-gendarme condamn� � perp�tuit� pour meurtre
DATE: 2014-02-12
URL: http://www.ouest-france.fr/toulouse-30-ans-requis-contre-un-ex-gendarme-pour-homicide-1925297
PRINCIPAL: 167618
TEXT:
Toulouse. L'ex-gendarme condamn� � perp�tuit� pour meurtre
Toulouse -
Achetez votre journal num�rique
Un ancien gendarme accus� d'avoir tu� une vieille dame de 97 ans pour lui voler ses bijoux a �t� condamn� � perp�tuit�.
Daniel Bedos, 57 ans, qui a fait l'essentiel de sa carri�re de gendarme entre 1978 et 1992 en Haute-Garonne, est accus� d'avoir tu� Suzanne Blanc � son domicile le 18 ao�t 2010 � Toulouse pour lui voler ses bijoux.
Daniel Bedos, jug� depuis lundi, a ni� les faits. Il a reconnu avoir revendu les bijoux du crime d�s le lendemain des faits. Mais il dit les avoir re�us d'un autre homme, qui les lui aurait c�d�s pour 170 euros l'apr�s-midi de l'agression.
Des explications qui n'ont pas convaincu la cour d'assises, qui l'ont reconnu coupable de l'ensemble des chefs d'accusation ainsi que des faits de subornation de t�moin, et sont all�s au-del� des r�quisitions de l'avocat g�n�ral qui avait r�clam� contre lui trente ann�es de r�clusion criminellle.
Lire aussi
