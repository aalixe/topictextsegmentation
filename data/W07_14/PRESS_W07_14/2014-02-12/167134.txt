TITRE: Le CFA, première  - Coupe de France - Football.fr
DATE: 2014-02-12
URL: http://www.football.fr/coupe-de-france/scans/le-cfa-premiere-520764/?sitemap
PRINCIPAL: 167131
TEXT:
12 février 2014 � 08h11
Mis à jour le
Réagir 5
Pour la première fois depuis sa création (saison 1993-1994), le Championnat de France amateur va placer au minimum deux de ses clubs en quart de finale de la Coupe de France.
Cannes, auteur d’un nouvel exploit mardi contre Montpellier (1-0), sera rejoint par le vainqueur de Moulins (CFA)-Sète (CFA2), programmé ce mercredi à 19h30.
Les Corses de l’Ile Rousse, autres pensionnaires de CFA2, peuvent également rêver de faire partie du Top 8, opposés à Guingamp.
