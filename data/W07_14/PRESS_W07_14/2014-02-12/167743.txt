TITRE: JO-2014 - Patinage - Bart Swings 23e du 1000 m, l'or pour le N�erlandais Stefan Groothuis - DH.be
DATE: 2014-02-12
URL: http://www.dhnet.be/dernieres-depeches/belga/jo-2014-patinage-bart-swings-23e-du-1000-m-l-or-pour-le-neerlandais-stefan-groothuis-52fb99003570516ba0b9a4df
PRINCIPAL: 167737
TEXT:
Vous �tes ici: Accueil > Derni�res d�p�ches
JO-2014 - Patinage - Bart Swings 23e du 1000 m, l'or pour le N�erlandais Stefan Groothuis
Publi� le
12 f�vrier 2014 � 16h52
SOTCHI (Russie)
Bart Swings s'est class� 23e, sur 40 engag�s, du 1000 m de patinage de vitesse des jeux Olympiques de Sotchi, mercredi en Russie. Le N�erlandais Stefan Groothuis a d�croch� la m�daille d'or
Stefan Groothuis s'est impos� en 1:08.39 devant le Canadien Denny Morrison (1:08:43) et le N�erlandais Michel Muller (1:08.74). Bart Swings a pris la 23e place le jour de son 23e anniversaire. Le Louvaniste a patin� en 1:10.14, � 14 centi�mes de son record personnel (1:10.00). Premier r�serviste, Swings n'a appris que mardi soir qu'il pourrait prendre le d�part du 1000 m, o� il s'alignait sans r�elle ambition. Il vise surtout le 1500 m, sa distance de pr�dilection, qui sera disput� samedi. (Belga)
� 2014 Belga. Tous droits de reproduction et de repr�sentation r�serv�s. Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par Belga. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, rediffus�e, traduite, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de Belga.
Derni�res d�p�ches
