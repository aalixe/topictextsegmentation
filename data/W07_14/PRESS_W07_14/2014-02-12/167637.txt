TITRE: Bricolage : l'autorisation d'ouverture le dimanche suspendue - 12 février 2014 - Le Nouvel Observateur
DATE: 2014-02-12
URL: http://tempsreel.nouvelobs.com/economie/20140212.OBS6032/bricolage-l-autorisation-d-ouverture-le-dimanche-suspendue.html
PRINCIPAL: 167558
TEXT:
Publié le 12-02-2014 à 16h58
Mis à jour à 23h00
En r�action � la d�cision du�Conseil d'Etat, le gouvernement fait savoir qu'il pr�pare "dans les plus brefs d�lais" un nouveau d�cret.
Rassemblement du Collectif de salariés "Les bricoleurs du dimanche", le 17 avril 2013 (PRM/SIPA). PRM/SIPA
Le Conseil d'Etat a suspendu mercredi 12 f�vrier le d�cret autorisant temporairement les magasins de�bricolage�� ouvrir le dimanche, a-t-il annonc� dans un communiqu�.
Le Conseil d'Etat a estim� qu'il "existait un doute s�rieux sur la l�galit�" du d�cret du 30 d�cembre autorisant temporairement les �tablissements de commerce de d�tail du bricolage�� d�roger � la r�gle du repos dominical. A la demande des syndicats, il a d�cid� de "suspendre l'ex�cution du d�cret".
Premi�re anomalie : la limitation dans le temps.�"Le juge des r�f�r�s a relev� que l�autorisation pr�vue courait jusqu�au 1er juillet 2015, alors qu�une telle d�rogation doit normalement avoir un caract�re permanent, dans la mesure o� elle a vocation � satisfaire des besoins p�rennes du public", indique le communiqu�.�
Deuxi�me anomalie�: "Il a �galement relev� que l�ouverture des �tablissements le dimanche, alors que le principe�d'un repos hebdomadaire est l'une des garanties du droit constitutionnel au repos reconnu�aux salari�s et que ce droit s'exerce en principe le dimanche, est de nature � porter une�atteinte grave et imm�diate aux int�r�ts d�fendus par les organisations syndicales."
Le gouvernement va pr�parer "dans les plus brefs d�lais" un nouveau d�cret pour autoriser l'ouverture le dimanche des magasins de bricolage, "qui n'aura pas de limitation dans le temps", a aussit�t fait savoir le�minist�re du Travail.
Partager
