TITRE: Centrafrique: un �nettoyage ethnique� selon Amnesty, nouvelle visite de Le Drian - France-Monde - La Voix du Nord
DATE: 2014-02-12
URL: http://www.lavoixdunord.fr/france-monde/centrafrique-un-nettoyage-ethnique-selon-amnesty-ia0b0n1914171
PRINCIPAL: 166758
TEXT:
Selon Amnesty International, les exactions des milices anti-balaka contre la minorit� de civils musulmans rel�vent d�sormais du � nettoyage ethnique �.
� Les soldats de la force internationale de maintien de la paix ne parviennent pas � emp�cher le nettoyage ethnique des civils musulmans dans l�ouest de la R�publique centrafricaine �, �crit Amnesty, appelant la communaut� internationale � � faire barrage au contr�le des milices anti-balaka et � d�ployer des troupes en nombre suffisant dans les villes o� les musulmans sont menac�s �.
Centaines de victimes
Amnesty cite ainsi le cas de Bossempt�l� (ouest) o� une attaque d�anti-balaka a fait � plus de 100 victimes parmi la population musulmane � le 18 janvier.
� Les milices anti-balaka m�nent des attaques violentes dans le but de proc�der au nettoyage ethnique des musulmans en R�publique Centrafricaine �, �crit Amnesty qui critique � la r�ponse trop timor�e de la communaut� internationale �, en jugeant que � les troupes internationales de maintien de la paix se montrent r�ticentes � faire face aux milices anti-balaka �.
Crise alimentaire
Selon l�ONU, 1,3 million de personnes, soit plus d�un quart de la population de Centrafrique, ont besoin d�une assistance alimentaire imm�diate, en particulier dans les camps de d�plac�s o� s�entassent plus de 800 000 personnes, dont plus de la moiti� � Bangui.
Face � l�ampleur de la crise, un pont a�rien du Programme alimentaire mondial (PAM) a �t� mis en place. C�est � un ballon d�oxyg�ne, mais �a ne r�glera pas le probl�me � terme �, a pr�venu l�organisation.
Le pont a�rien entre Douala (Cameroun) et Bangui va acheminer 1 800 tonnes de vivres. De quoi alimenter 150 000 personnes pendant un mois. Une op�ration qui restera toutefois insuffisante face � l�ampleur de la crise alimentaire et humanitaire.
Troisi�mes visites du ministre de la D�fense
C�est la troisi�me fois que Jean-Yves Le Drian se rend en Centrafrique depuis le d�clenchement de l�op�ration Sangaris le 5 d�cembre.
Mardi � Brazzaville, le ministre fran�ais avait durci le ton contre les milices qui s�vissent en Centrafrique, affirmant que les forces internationales �taient pr�tes � mettre fin aux exactions, � si besoin par la force �.
� Il faut que l�ensemble des milices qui continuent aujourd�hui � mener des exactions, � commettre des meurtres, arr�tent �, avait d�clar� Jean-Yves Le Drian, en demandant aux forces fran�aises et africaines en Centrafrique d�� appliquer les r�solutions des Nations unies, si besoin par la force �.
Le pays a sombr� dans le chaos depuis le coup d��tat en mars 2013 de Michel Djotodia, chef de la coalition rebelle S�l�ka � dominante musulmane. Devenu pr�sident, il a �t� contraint � la d�mission le 10 janvier pour son incapacit� � emp�cher les tueries entre ex-S�l�ka et milices d�autod�fense anti-balaka, majoritairement chr�tiennes.
Anti-balaka<UN>: � principaux ennemis de la paix �
Jusqu�� pr�sent, elles ne sont pas parvenues � mettre fin aux violences meurtri�res et aux pillages, qui se traduisent par un exode de civils musulmans fuyant des r�gions enti�res du pays pour chercher refuge essentiellement au Tchad et au Cameroun voisins.
La Misca compte actuellement 5 400 hommes � sur les 6 000 pr�vus � et l�op�ration Sangaris 1 600 soldats.
D�s samedi, le commandant de la Misca, le g�n�ral camerounais Martin Tumenta Chomu, leur avait adress� une s�v�re mise en garde. Lundi le commandant de l�op�ration Sangaris, le g�n�ral Francisco Soriano a �t� encore plus direct en qualifiant les anti-balaka de � principaux ennemis de la paix � en Centrafrique, qui seront trait�s comme des � bandits �.
R�agir � l'article
