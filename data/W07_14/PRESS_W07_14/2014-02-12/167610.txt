TITRE: Le vote blanc désormais comptabilisé aux élections, Actualités
DATE: 2014-02-12
URL: http://www.lesechos.fr/economie-politique/politique/actu/0203312447448-le-vote-blanc-desormais-reconnu-aux-elections-649935.php
PRINCIPAL: 167549
TEXT:
Le vote blanc d�sormais comptabilis� aux �lections
Par Les Echos | 12/02 | 16:43 | mis � jour � 18:55
Le Parlement a d�finitivement adopt� ce mercredi une proposition de loi centriste visant � faire reconna�tre le vote blanc aux �lections. La mesure entrera en vigueur apr�s les municipales.
Le Parlement a d�finitivement adopt� ce mercredi une proposition de loi centriste visant � faire reconna�tre le vote blanc aux �lections. - AFP
Les �lecteurs pourront d�sormais voter blanc. Le Parlement a d�finitivement adopt� ce mercredi apr�s-midi une proposition de loi centriste en ce sens. La mesure n'entrera toutefois en vigueur qu'� partir du 1er avril 2014. Les �lecteurs pourront donc se saisir de cette possibilit� non pas pour les �lections municipales mais � partir des �lections europ�ennes du 25 mai prochain. L�UDI avait souhait� initialement que cela soit possible d�s les municipales, soutenu en cela par l�UMP, mais la majorit� socialiste en a repouss� l�entr�e en vigueur apr�s ce scrutin local.
Bulletin blanc ou enveloppe vide
Actuellement, lors des scrutins, les bulletins blancs ne sont pas diff�renci�s des bulletins nuls. Ils sont comptabilis�s ensemble, sont mentionn�s dans les r�sultats mais ne sont non pris en compte dans les suffrages exprim�s. Avec cette nouvelle loi, ils seront compt�s s�par�ment. Mais les bulletins blancs ne seront toujours pas pris en compte pour les suffrages exprim�s.
Chaque �lecteur pourra voter � blanc � soit en introduisant dans l'enveloppe un bulletin blanc, soit en laissant cette enveloppe vide.
� Un choix tout aussi respectable que les autres �
Cette adoption constitue � une avanc�e dans la transparence de la vie d�mocratique r�pond aux attentes de nombreux Fran�ais depuis de nombreuses ann�es �, a fait valoir le rapporteur, Fran�ois Zocchetto (UDI-UC). � L�absence de reconnaissance de la voix de l��lecteur qui se d�place pour accomplir son devoir civique �tait choquante en d�mocratie �, a-t-il ajout�.
Pour Philippe Kaltenbach (PS), � si l�abstention peut g�n�ralement �tre comprise comme une marque de d�sint�r�t pour la vie politique, le vote blanc doit �tre vu comme une attente non satisfaite qui peut traduire une forme d�esp�rance. C�est un choix tout aussi respectable que les autres �.
� Mieux vaut voter blanc que bleu marine �, a lanc� de son c�t� l��cologiste H�l�ne Lipietz.
D'autres vid�os � voir sur le web
Vos derniers commentaires
quiberonnais le  12/02/2014 � 17:21
cela  ne sert a rien  les compter et c'est tout   , ils doivent �tre pris en compte  par exemple au 2� tour d'une pr�sidentielle  si les bulletins blancs d�passent 50% les deux pr�tendants doivent �tre ...
lheritier le  12/02/2014 � 17:50
