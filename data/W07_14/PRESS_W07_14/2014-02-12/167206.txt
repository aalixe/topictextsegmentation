TITRE: Une m�re, morte cliniquement, d�branch�e apr�s avoir donn� naissance � son fils (+ vid�o) - nordeclair.be
DATE: 2014-02-12
URL: http://www.nordeclair.be/932989/article/actualite/monde/2014-02-11/une-mere-morte-cliniquement-debranchee-apres-avoir-donne-naissance-a-son-fils-vi
PRINCIPAL: 167202
TEXT:
Une m�re, morte cliniquement, d�branch�e apr�s avoir donn� naissance � son fils (+ vid�o)
Afp
Une Canadienne morte cliniquement depuis fin d�cembre a donn� ce week-end naissance � un petit gar�on par c�sarienne avant d��tre d�branch�e.
Tweet
�Samedi soir, mon �tonnant et merveilleux fils Iver Cohen Benson est n� (...). Le dimanche nous avons d� malheureusement dire adieu � la plus merveilleuse et courageuse femme que j�ai jamais rencontr�e�, a �crit lundi soir Dylan Benson sur son blog et sa page Facebook.
Le cas de cette famille a suscit� l��motion au Canada et une vague de solidarit� s�est manifest�e au tout d�but du mois quand ce Canadien de 32 ans a d�cid� de m�diatiser sa situation et lanc� un appel aux dons sur un blog.
Il y avait expliqu� que son �pouse Robyn Benson, du m�me �ge que lui, avait �t� victime d�une h�morragie c�r�brale le 21 d�cembre alors qu�elle �tait enceinte de 22 semaines. Cette jeune femme avait alors �t� maintenue dans le coma sous assistance respiratoire dans les services de r�animation de l�h�pital de Victoria, dans la province de Colombie-Britannique.
A l�origine, les m�decins voulaient maintenir les fonctions vitales de Robyn Benson jusqu�� la 34e semaine de grossesse avant de proc�der � un accouchement par c�sarienne. Mais finalement, le b�b� est n� samedi soir, apr�s 28 semaines de gestation.
Dylan Benson a post� une photo o� on le voit dans une salle de soins intensifs avec le fr�le nourrisson dans les bras. Dans ce dernier billet intitul� �immens�ment triste mais incroyablement fier�, il annonce � la fois la naissance de son fils et le d�c�s de sa femme. Mardi, les dons atteignaient plus de 152.000 dollars canadiens (103.000 euros). La somme avait litt�ralement d�coll� la semaine derni�re avec les r�cits des m�dias canadiens et l�objectif initial des 32.000 dollars atteint en quelques heures.
Les + lus
