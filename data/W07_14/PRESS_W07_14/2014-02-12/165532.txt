TITRE: Accessibilit� : Poitiers gagne des places - 12/02/2014 - La Nouvelle R�publique Vienne
DATE: 2014-02-12
URL: http://www.lanouvellerepublique.fr/Vienne/Actualite/24-Heures/n/Contenus/Articles/2014/02/12/Accessibilite-la-ville-gagne-des-places-1792915
PRINCIPAL: 0
TEXT:
Vienne -                     Poitiers -                                     Vie de la cit�
Accessibilit� : Poitiers gagne des places
12/02/2014 05:46
En th�orie, la totalit� des lieux publics doit �tre accessible au 1er�janvier 2015. Ici, la gare SNCF.
Le barom�tre APF de l�accessibilit� met la ville en cinqui�me position des quatre-vingt-seize chefs-lieux d�partementaux. Bien, mais peut mieux faire.
Publi� hier par l'association des paralys�es de France (APF), le 5e�barom�tre de l'accessibilit� 2013�pointe le retard pris dans la mise en accessibilit� des 96�chefs-lieux de d�partements. Son constat est sans appel : ��Plus que pr�occupant !�� Et l'association de demander que ��cet enjeu primordial soit inscrit dans les programmes des candidats aux �lections municipales � venir.�� Avec une note globale de 17,4�sur 20�cette ann�e - soit 4,5�points de plus qu'en 2012�- Poitiers entre dans le top 5�des villes les plus accessibles, derri�re Grenoble, Nantes, Caen et Lyon. La capitale r�gionale semble avoir satisfait aux trois principaux crit�res : un cadre de vie adapt�, des �quipements municipaux accessibles, une politique locale volontariste.
�" Travailler autour du handicap psychique "
Ce classement plut�t flatteur satisfait bien �videmment Abderrazak Halloumi, le conseiller municipal d�l�gu� � l'accessibilit�, au handicap et � la s�curit� des �tablissements recevant du public :���C'est une reconnaissance du travail qui est r�alis� mais beaucoup de choses restent � faire. Des efforts �normes ont �t� faits sur l'espace public, sur le plateau et dans les quartiers mais tout ne sera pas pr�t en 2015. Les normes exig�es par la loi sont int�ressantes mais il y a des difficult�s au niveau des b�timents anciens. Il nous faut concilier accessibilit� et s�curit� des monuments historiques.�� De plus, tous les types de handicaps doivent �tre pris en compte. Handicap moteur mais aussi handicap psychique. ��Les messages vocaux dans les bus pour les personnes non voyantes sont int�ressants, d�taille l'�lu, mais ils peuvent perturber les autistes. Il faut continuer � travailler ces th�mes en interne, avec les usagers, la population, par exemple dans le cadre des conseils ou des comit�s de quartier.��
��La question de l'accessibilit� est toujours prise en compte quand on fait des trottoirs ou des zones partag�es, poursuit �liane Rousseau, adjointe en charge de la voirie, on est �galement attentifs � ce que dans chaque quartier, au moins une � deux voies soient totalement accessibles.�� Les rues Henri-Dunant et Coubertin aux Couronneries, la rue Condorcet ont par exemple d�j� �t� trait�es selon ce principe, tout comme plus r�cemment la rue des Feuillants. Mais si Poitiers fait aujourd'hui figure de bonne �l�ve, elle peut encore s'amender. Tout n'est pas fini. La ville poss�de 220�km de voirie.
en savoir plus
>�La loi sur le handicap de f�vrier�2005�donnait dix ans aux collectivit�s et commer�ants pour mettre aux normes d'accessibilit� tous les lieux de passage.
�>�En 2009. Poitiers a rassembl� ses priorit�s dans un plan de mise en accessibilit� de la voirie et des espaces publics (Pave).
�>�Le barom�tre APF de l'accessibilit� est en ligne sur le site www.apf.asso.fr
Jean-Michel Gouin
