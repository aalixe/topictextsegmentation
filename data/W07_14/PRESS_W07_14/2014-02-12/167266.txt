TITRE: Twitter exp�rimente un nouveau profil sur le web
DATE: 2014-02-12
URL: http://www.macg.co/ailleurs/2014/02/twitter-experimente-un-nouveau-profil-sur-le-web-79850
PRINCIPAL: 167263
TEXT:
Twitter exp�rimente un nouveau profil sur le web
par K�vin Gavant le 12 f�vrier 2014 � 14:00
11
Twitter a visiblement de nouvelles id�es d'interface. Un employ� de Mashable a remarqu� lors de sa connexion au r�seau social que sa page de profil sur le web �tait totalement diff�rente. � en voir la capture d'�cran de Matt Petronzio, Twitter se dirige vers ce que propose Facebook ou encore Google+.
L'interface est �pur�e, �tendue et donne plus d'importance au contenu. La photo de profil ainsi que la biographie de l'utilisateur sont mises de c�t�, les tweets et les mentions prenant la plus grande partie de l'espace disponible. On remarque que les tweets ne sont pas affich�s dans une liste verticale, mais sous forme de cartes organis�es horizontalement. Le mois dernier, Twitter a appliqu� une nouvelle interface � son site web inspir�e par les applications mobiles.
Le redesign du site web d�ploy� aupr�s de tous les utilisateurs ces derni�res semaines.
Twitter, qui est r�cemment entr� en bourse, inqui�te d�j� les march�s financiers � cause d' un ralentissement de sa croissance et d'un b�n�fice toujours inexistant. Cette nouvelle interface, si elle est retenue, parviendra-t-elle � amener des utilisateurs ?
Cat�gorie :�
Amener des utilisateurs peut-�tre mais en faire partir s�rement. Twitter n'est pas Facebook et Facebook n'est pas twitter.
Pourquoi cet oiseau bleu ne peut pas le comprendre ?
Connectez-vous ou inscrivez-vous pour publier un commentaire
�a m'a l'air int�ressant, du moment quoi ne deviennent pas un Facebook 2.0
Connectez-vous ou inscrivez-vous pour publier un commentaire
iRobot 5S 12/02/2014 - 14:32 via iGeneration pour iPhone
C'est vrai que le site web �tait moche
Connectez-vous ou inscrivez-vous pour publier un commentaire
marc_os 12/02/2014 - 14:54
� La photo de profil ainsi que la biographie de l'utilisateur sont mises de c�t� �
Et la photo de face, qu'en font-ils ?
;-)
Connectez-vous ou inscrivez-vous pour publier un commentaire
Ali Ibn Bachir, Le Gros 12/02/2014 - 23:49
Ils vont la mettre de l'autre c�t� :-)
Connectez-vous ou inscrivez-vous pour publier un commentaire
Ludavid21 12/02/2014 - 16:15
Ah non! Pas cette interface style timeline aussi sur twitter, c'est d�sagr�able � souhait.
Connectez-vous ou inscrivez-vous pour publier un commentaire
Pas mal et je pr�f�re comme �a que celui de maintenant!
Connectez-vous ou inscrivez-vous pour publier un commentaire
Bof. Petit � petit ils se rejoignent tous...
Connectez-vous ou inscrivez-vous pour publier un commentaire
Peut-�tre un peu plus joli mais bien moins pratique pour lire le fil d'actualit� � mon avis... Bien que ce soit peut-�tre juste une question d'habitude
Connectez-vous ou inscrivez-vous pour publier un commentaire
chrisann 12/02/2014 - 22:41
Perso, c'est p'�tre mon c�t� conservateur, mais je trouve que cette nouvelle interface serait une perte d'identit� visuelle pour la marque � l'oiseau bleu... Et comme dit plus haut, Facebook est Facebook, Twitter est Twitter. Et si je n'utilise pas Facebook alors que j'utilise Twitter, ce n'est pas pour que ce dernier se transforme en ce premier.
Connectez-vous ou inscrivez-vous pour publier un commentaire
8enoit 12/02/2014 - 23:34 via MacG Mobile
Je n'utilise pas Facebook, � dessein. Que Twitter se rapproche de FB dans l'esprit ou dans la forme ne m'int�resse mais alors l� PAS DU TOUT.
Le site actuel est tr�s bien. Il manque juste quelques options genre "citer ce tweet" et "copier le lien du tweet", ainsi que les brouillons. Fonctionnalit�s qui existent sur l'app iPhone.
Connectez-vous ou inscrivez-vous pour publier un commentaire
