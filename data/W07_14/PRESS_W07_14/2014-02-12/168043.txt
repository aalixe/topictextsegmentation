TITRE: En 2011, l'UMP proposait des cours en maternelle sur "le genre" - L'Express
DATE: 2014-02-12
URL: http://www.lexpress.fr/actualite/politique/ump/en-2011-l-ump-proposait-des-cours-en-maternelle-sur-le-genre_1323290.html
PRINCIPAL: 168033
TEXT:
En 2011, l'UMP proposait des cours en maternelle sur "le genre"
Floriane Dumazert, publi� le
12/02/2014 �  18:20
, mis � jour �  20:00
Jean-Fran�ois Cop� s'insurge contre les livres v�hiculant la "th�orie du genre", mais c'est bien son parti, l'UMP, qui en 2011 voulait "amener les enfants � se sentir autoris�s � adopter des conduites non st�r�otyp�es".�
Voter (0)
� � � �
En 2011, l'UMP proposait "d'introduire, d�s la maternelle, des s�ances consacr�es � la mixit� et au respect hommes/femmes". (Capture d'�cran de la page du site de l'UMP)
DR
L'UMP n'assumerait-elle pas ses anciennes positions concernant la "th�orie du genre" ? En milieu d'apr�s-midi, un tweet alertait: au moment o� la pol�mique du "gender" gonfle, le parti aurait supprim� une page qui �tait consacr�e � l'�galit� femmes-hommes d�s la maternelle.�
-- Laurent (@lrtblt) 12 F�vrier 2014
�
Et en effet, quand nous avons tent� de la consulter, la page en question n'�tait plus accessible. Ci-dessous, la capture d'�cran de la page.�
Le 12 f�vrier, la page du site de l'UMP consacr�e � l'introduction de "s�ances consacr�es � la mixit�" n'�tait plus accessible. (Capture d'�cran du site de l'UMP)
DR
�
Sauf que, quelques minutes plus tard, la page �tait de nouveau consultable . L'UMP a-t-elle voulu la supprimer avant de changer d'avis pour ne pas �tre victime de l'effet Streisand -quand la censure fait finalement une publicit� monstrueuse � ce qu'on voulait cacher? Contact� par L'Express, le charg� de communication du parti n'a pas encore donn� suite. �
On apprend en tout cas sur cette page qu'en juin 2011, date de l'�dition de la page, l'UMP parlait "d'introduire, d�s la maternelle des s�ances consacr�es � la mixit� et au respect hommes/femmes". Ces cours qui font aujourd'hui hurler les membres de l'opposition .�
Une position que la porte-parole du gouvernement, Najat Vallaud-Belkacem a aussit�t identifi�e, sur son compte Facebook, comme un manque de "coh�rence" de la part du parti.�
Publication de Najat Vallaud-Belkacem .
�
Comme le rappelle Le Monde , au printemps 2011, l'UMP, d�j� dirig�e par Jean-Fran�ois Cop� , avait organis� des Etats g�n�raux du parti, cens�s �tablir des premi�res propositions en attendant l'entr�e en campagne de Nicolas Sarkozy�
En 2011, l'UMP proposait "d'introduire, d�s la maternelle, des s�ances consacr�es � la mixit� et au respect hommes/femmes". (Capture d'�cran de la page du site de l'UMP)
DR
Sur cette page, le concept du genre est explicitement �voqu�. L'UMP proposait ainsi "d'aider les filles et les gar�ons � percevoir positivement leur genre et celui du sexe oppos�e". L'objectif de ces "s�ances" �tait lui aussi clairement affich�. Il s'agissait "d'amener les enfants � se sentir autoris�s � adopter des conduites non st�r�otyp�es". �
En 2011, le parti formulait �galement ses propositions concernant "la place des femmes dans la soci�t�" , dont on peut retrouver l'int�gralit� ici .�
Outre ses propositions pour la maternelle, le parti soulignait entre autres qu'il fallait "aider les managers � remettre en cause les st�r�otypes de genre"... L'UMP aurait-elle perdu la m�moire?�
�
