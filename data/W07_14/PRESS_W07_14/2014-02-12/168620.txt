TITRE: Une pub avec Cantona censurée en Angleterre - Angleterre - Football.fr
DATE: 2014-02-12
URL: http://www.football.fr/angleterre/articles/cantona-censure-en-angleterre-520806/?sitemap
PRINCIPAL: 168618
TEXT:
12 février 2014 � 09h33
Mis à jour le
Réagir 5
La dernière publicité pour Kronenbourg, qui met en scène Eric Cantona, ne sera jamais diffusée en Angleterre. Et pour cause, elle est considérée comme mensongère.
Après avoir appris aux Anglais à aimer les footballeurs français, Eric Cantona continue de défendre le "Made In France". L'ancienne icône de Manchester United est en effet la vedette du dernier spot publicitaire de la marque Kronenbourg. Dans ce petit film -par ailleurs assez génial-, on y voit "The King" vanter les mérites des agriculteurs alsaciens, chargés de récolter le fameux houblon, et qui sont de véritables stars locales, comme le sont les footballeurs en Angleterre.
Problème, la bière en question n'est pas brassée en France mais bien... à Manchester. Et l'Advertising Standards Authority, qui a jugé cette publicité "trompeuse", en a fait interdire sa diffusion, rapporte le Daily Mirror. La mention "Brewed in UK" ("Brassée au Royaume-Uni") n'a pas suffi...
