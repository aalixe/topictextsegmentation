TITRE: Huit Corvette aval�es par un trou b�ant dans un mus�e du Kentucky - Lib�ration
DATE: 2014-02-12
URL: http://www.liberation.fr/monde/2014/02/12/huit-corvette-avalees-par-un-trou-beant-dans-un-musee-du-kentucky_979809
PRINCIPAL: 168538
TEXT:
Huit Corvette aval�es par un trou b�ant dans un mus�e du Kentucky
12 f�vrier 2014 � 20:32
La Corvette DP de S�bastien Bourdais, Joao Barbosa et Christian Fittipaldi lors des 24 heures de Daytona Beach (Floride), le 26 janvier 2014 (Photo Chris Trotman. AFP)
HISTOIRE
Les causes de l'apparition de ce trou b�ant, de 12 m�tres de circonf�rence, sont inconnues.
Un grand trou s�est ouvert mercredi dans le sol d�un hall d�exposition du Mus�e National Corvette, dans le Kentucky (centre-est des Etats-Unis), dans lequel sont tomb�es huit voitures de collection, un spectacle d�solant pour les amateurs de belles carrosseries.
Ce trou b�ant, d�environ 12 m�tres de circonf�rence et de 8�� 9 m�tres de profondeur, a �t� d�couvert quand une alarme s�est d�clench�e au petit matin. Les pompiers ont rapidement s�curis� les lieux et les responsables du mus�e tentaient de rep�cher les huit Corvette tomb�es en contrebas.
Deux d�entre elles �taient pr�t�es au mus�e par General Motors, qui construit ces voitures de sport embl�matiques depuis plus de 60 ans. Parmi les voitures de collection endommag�es, une Corvette de 1962, la millioni�me Corvette sortie des usines en 1992 ou une Corvette de 1993 c�l�brant le 40e anniversaire de la marque.
Le mus�e est situ� dans la ville de Bowling Green, la seule o� sont construites ces voitures de l�gende.�Aucune explication n�avait �t� donn�e dans l�imm�diat sur les causes de l�apparition de ce trou b�ant.
les plus partag�s
