TITRE: Michel Gondry ressort le carton-p�te pour Metronomy - Lib�ration
DATE: 2014-02-12
URL: http://next.liberation.fr/musique/2014/02/11/michel-gondry-ressort-le-carton-pate-pour-metronomy_979368
PRINCIPAL: 166660
TEXT:
Lire sur le readerMode zen
Le groupe anglais a fait appel au r�alisateur fran�ais pour mettre en sc�ne �Love Letters�, deuxi�me extrait tir� de son nouvel album.
Apr�s un concert de chauffe � Paris , les Anglais de Metronomy viennent de publier le clip du morceau-titre de leur quatri�me album,�Love Letters, attendu pour le 10 mars.
Pour cette deuxi�me vid�o annon�ant son album (apr�s I'm Aquarius en d�cembre), le groupe de Joseph Mount a fait appel au r�alisateur Michel Gondry, sp�cialiste du genre, qui a sign� des clips rest� marquants pour Bj�rk (Human Behaviour,�Army Of me,�Bachelorette...), Daft Punk (Around the World) ou Kanye West (Heard 'Em Say)...
Comme souvent dans ses clips, le vid�aste fran�ais utilise une id�e simple et une imagerie bric-�-brac �galement pr�sente dans ses films (Eternal Sunshine of the Spotless Mind,�l�Ecume des jours). Le clip de Love Letters,�color� et baign� d'ambiance sixties, risque toutefois de bien moins marquer les esprits que le travail de Michel Gondry avec les Daft Punk ou Bj�rk.
