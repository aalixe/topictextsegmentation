TITRE: Les instits mécontents de la réforme des rythmes selon leur premier syndicat - 12 février 2014 - Le Nouvel Observateur
DATE: 2014-02-12
URL: http://tempsreel.nouvelobs.com/education/20140212.AFP9883/les-instits-mecontents-de-la-reforme-des-rythmes-selon-leur-premier-syndicat.html
PRINCIPAL: 166580
TEXT:
Actualité > Education > Les instits mécontents de la réforme des rythmes selon leur premier syndicat
Les instits mécontents de la réforme des rythmes selon leur premier syndicat
Publié le 12-02-2014 à 11h15
Mis à jour à 16h15
A+ A-
PARIS, 12 fév 2014 (AFP) - Les professeurs des écoles sont en général "mécontents" de la mise en ouvre de la réforme des rythmes scolaires, selon une enquête publiée mercredi par leur premier syndicat, le SNUipp-FSU, qui demande la suspension de la généralisation de la réforme et une réécriture du décret.
La réforme des rythmes, qui marque le retour à la semaine de 4,5 jours en primaire, est effective depuis cinq mois dans 17% des communes et doit s'appliquer partout ailleurs à la rentrée 2014.
Le SNUipp-FSU a lancé un questionnaire et a analysé deux types de réponses, celles de 3.568 professeurs des écoles dont l'école applique la semaine de 4,5 jours depuis septembre 2013, et celles de 3.906 écoles parmi les 39.000 qui doivent l'appliquer à la rentrée 2014.
Le syndicat en a tiré un "contre-rapport", en réponse à un rapport du Comité de suivi de la réforme (dans lequel le SNUipp-FSU est représenté) qui stipule que les acteurs des communes pionnières "expriment le plus souvent leur satisfaction et un large accord sur l'intérêt de la réforme", selon le texte dont l'AFP a eu copie.
Selon le "contre-rapport" du syndicat, 75% des enseignants estiment que leurs conditions de travail se sont dégradées, proportion qui monte à 84% quand le conseil d'école n'a pas été consulté sur la construction de l'emploi du temps de la semaine. Parmi les griefs: "partage non concerté des salles de classe" avec les animateurs des activités péricolaires, "multiplication des réunions pour réguler les transitions périscolaires" sur leur temps personnel, temps de concertation entre enseignants déplacés au mercredi après-midi ou en soirée...
Seuls 22% des enseignants qui ont répondu voient une amélioration pour l'apprentissage des élèves.
Un "sentiment de gâchis prédomine", selon le syndicat, pour lequel "les leçons de 2013 n'ont pas été tirées" pour 2014.
Le syndicat demande la suspension de la généralisation de la réforme prévue à la rentrée 2014 et "une réécriture totale du décret", avec la possibilité de dérogations comme deux mercredis sur trois travaillés, "4 jours avec rattrapage sur les vacances d'été" ou "5 jours avec un nombre d'heures identiques par jour".
Lundi, la fédération de parents d'élèves Peep avait aussi souhaité dans un communiqué "une réécriture du texte prenant en considération la diversité des territoires français et la volonté des acteurs de terrain".
Partager
