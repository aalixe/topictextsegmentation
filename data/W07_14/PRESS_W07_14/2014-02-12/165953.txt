TITRE: Toyota : pr�s de 2 millions de v�hicules rappel�s � cause d�un probl�me de logiciel
DATE: 2014-02-12
URL: http://www.latribune.fr/entreprises-finance/industrie/automobile/20140212trib000814931/toyota-pres-de-2-millions-de-vehicules-rappeles-a-cause-d-un-probleme-de-logiciel.html
PRINCIPAL: 165943
TEXT:
Toyota : pr�s de 2 millions de v�hicules rappel�s � cause d�un probl�me de logiciel
Le constructeur nippon a �t� averti d'un peu plus de 400 cas d'arr�t de v�hicules.
latribune.fr �|�
12/02/2014, 8:48
�-� 299 �mots
Le dernier mod�le hybride de la gamme Prius rencontre des difficult�s avec un logiciel, pouvant provoquer une perte de puissance puis l�arr�t du v�hicule.
sur le m�me sujet
Le japonais Toyota conserve sa couronne de num�ro un mondial de l'automobile
Le constructeur d'automobiles japonais Toyota a annonc� mercredi le rappel de 1,9 million de Prius dans le monde entier, en raison d'un probl�me li� au syst�me hybride qui peut entra�ner l'arr�t de la voiture.�
Ce rappel massif concerne le dernier mod�le de la gamme Prius, pr�cis�ment les v�hicules fabriqu�s entre 2009 et 2014 de cette s�rie � succ�s fonctionnant avec un syst�me hybride (motorisation � essence et �lectricit�) dont Toyota est le pionnier.
Aucun accident
"Dans le pire des cas, la voiture peut s'arr�ter pendant la conduite. Nous consid�rons ceci comme un possible probl�me de s�curit�, ce qui explique ce rappel", a pr�cis� un porte-parole de Toyota.
Averti d'un peu plus de 400 cas au Japon et aux Etats-Unis, le constructeur nippon a indiqu� qu'aucun accident n'�tait � d�plorer.
Le rappel concerne notamment pr�s d'un million de v�hicules au Japon, 700.000 aux Etats-Unis et 130.000 en Europe, dont quelque 15.600 en France. Il s'agit du plus important rappel jamais effectu� pour la Prius, dont le mod�le concern� est le troisi�me de cette s�rie lanc�e en 1997.
Avarie d'un logiciel
Toyota a expliqu� que ce probl�me provenait du logiciel du module de contr�le de l'inverseur du syst�me hybride.
Lorsqu'il est fortement sollicit�, le "logiciel peut faire entrer le v�hicule en mode de s�curit�, limitant la puissance disponible pour la conduite. Dans de rares cas le module de contr�le pourrait se r�initialiser, mettant hors service le syst�me hybride" et entra�nant l'arr�t du v�hicule, a d�taill� le constructeur.
"L'intervention consiste � reprogrammer le module de contr�le de l'inverseur. D'une dur�e d'environ 40 minutes, cette intervention sera enti�rement gratuite pour le client", a-t-il �galement d�clar�.
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Commentaires
GGG �a �crit le 19/02/2014                                     � 18:59 :
Demandez aux taxis parisien ce qu'ils pensent de leurs Prius et vous verez � quel point c'est fiable.
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
Toyota moins fiable qu'avant ... �a �crit le 12/02/2014                                     � 21:59 :
mais Volkswagen et Audi, c'est catastrophiquement fiable, il ne faut pas l'oublier !  Nos fran�aises s'en sortent tr�s bien finalement !
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
pm �a �crit le 12/02/2014                                     � 11:18 :
40 mn pour un "flashage" (qui doit prendre 40s...) !
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
L�on �a �crit le 12/02/2014                                     � 9:16 :
Tout les 3 mois Toyota charrie ses casseroles. Il n'y a pas de marque dans le monde qui l�che dans la nature autant de voitures non fiable qu'il faut rattraper en catastrophe, parfois apr�s de tr�s graves accident, comme dans le cas de la p�dale d'acc�l�rateur qui restait coinc�e.
pm �a r�pondu le 12/02/2014                                                 � 11:23:
Il semble que votre expertise contredise ce qu'annoncent tous les sondages: Toyota serait un des leaders en mati�re de fiabilit�. Courage ! vous allez bien r�ussir � convaincre... le monde entier.
godrev �a r�pondu le 12/02/2014                                                 � 12:40:
@l�eon Oui, voyez Volkswagen group a rappel� la moiti� de sa production en 2013 -notamment pour ses bv auto DSG  au d�veloppement loup�-, Toyota est au cinqui�me de sa production et pour un flashage de l'ordi, sensiblement mieux, n'est-il pas ???
lyria �a r�pondu le 12/02/2014                                                 � 13:33:
@leon
Roulez-vous au moins en Toyota pour en parler ??? A moins d'une jalousie.... Pour ma part j'ai poss�d� plusieurs mod�les de cette marque et jamais de soucis : 99,9 % de fiabilit� !!!!
Ok avec les propos de Godrev.
cridel �a r�pondu le 12/02/2014                                                 � 13:48:
Fin 2009 : 9 millions de voitures rappel�es � cause d'un probl�me du syst�me de freinage.
Octobre 2010 : 1,5 millions de voitures rappel�es � cause d'une fuite de liquide de freinage.
Juin 2011 : 110 000 voitures rappel�es � cause d'un probl�me d'inverseur du syst�me hybride.
Octobre 2012 : 7,43 millions de voitures rappel�es pour cause de risque d'incendie.
Janvier 2013 : probl�me d'airbag sur 900 000 voitures.
Juin 2013 : probl�me de freinage sur 242 000 voitures.
Et aujourd'hui, ce probl�me de logiciel.
Donc je ne sais pas si d'autres constructeurs ont d�j� fait pire ou pas, mais les statistiques montrent que lorsqu'on ach�te une Toyota, on a de bonnes chances d'avoir un probl�me technique grave.
pm �a r�pondu le 12/02/2014                                                 � 15:59:
Ce que "les statistiques montrent", c'est que Toyota, devant des "potentialit�s" de dysfonctionnement, anticipe. Vu leur diffusion dans certains pays "proc�s-compatibles", ce n'est pas idiot.
Par ailleurs, "probl�me sur 242000"... �a repr�sente combien de jours de production ?
On aimerait que certains constructeurs anticipent; particuli�rement en France o� la culture c'est "faisons le mort"; tant que le client ne demande rien, c'est bien; et s'il demande, � lui d'apporter la preuve. Il est vrai que nous nous distinguons particuli�rement par des dol�ances compl�tement farfelues... �a n'aide pas.
PS jamais eu de Toyota
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
