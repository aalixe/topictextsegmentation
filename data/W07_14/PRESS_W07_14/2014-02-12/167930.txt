TITRE: Cin�ma : le h�ros de �True Blood� dans Tarzan en 3D - La Parisienne
DATE: 2014-02-12
URL: http://www.leparisien.fr/laparisienne/actu-people/personnalites/cinema-le-heros-de-true-blood-dans-tarzan-en-3d-12-02-2014-3584325.php
PRINCIPAL: 0
TEXT:
Cin�ma : le h�ros de �True Blood� dans Tarzan en 3D
Alain Grasset |               12 f�vr. 2014, 17h00              | MAJ : 17h48
r�agir
4
L�acteur am�ricain Alexander Skarsg�rd a �t� choisi pour incarner Tarzan en 3D, dont la sortie est pr�vue en 2016. askarsgard.com
Mon activit� Vos amis peuvent maintenant voir cette activit�
Tweeter
Fin du suspense : c�est l�acteur am�ricain Alexander Skarsg�rd que l�on a vu dans la s�rie �True Blood� et le film �Battleship� qui a �t� choisi pour incarner Tarzan, viennent d'annoncer les studios Warner Bros.
Ce blockbuster dont la sortie aux Etats-Unis est d�j� fix�e au 1 er juillet 2016 aura aussi d'autres stars � l�affiche : Samuel L. Jackson, la blonde Margot Robbie (la r�v�lation du �Loup de Wall Street� de Martin Scorsese ) dans le r�le de Jane Porter, la femme dont s��prend Tarzan, ainsi que le com�dien autrichien Christoph Waltz (�Inglorious Basterds� et �Django Unchained� de Quentin Tarantino , nomin� deux fois � l�Oscar).
Cette nouvelle adaptation de �Tarzan� sera r�alis� par le britannique David Yates qui a sign� les quatre derniers �pisodes de la saga �Harry Potter�. Le sc�nario est �crit par David Beatty ( �Australia� � I, Frankenstein�) et Craig Brewer (�Footlose 2011�) et suivra le parcours du h�ros imagin� par Edgar Rice Burroughs depuis son enfance sauvage dans la jungle jusqu�� son retour en Angleterre et sa d�couverte d�une autre jungle, urbaine cette fois.
Dan Fellman, le patron de la distribution chez Warner Bros s'est dit tr�s tr�s satisfait : �Nous avons r�uni des acteurs des quatre coins du monde pour raconter cette histoire magnifique. Warner Bros collabore depuis longtemps avec David Yates et Jerry Weintraub (�Ocean�s Twelve�) et nous avons h�te de d�couvrir ce que toute l��quipe va nous concocter pour ce r�cit atemporel�.
Et d'ajouter : �Tarzan est un personnage � la fois �nigmatique et �ternel de la litt�rature et du cin�ma depuis plus d�un si�cle. L�aventure de cet homme, pris en �tau entre deux mondes, captive lecteurs et spectateurs de tous �ges. Autant dire que nous sommes heureux de proposer une nouvelle adaptation du livre de Burroughs � la g�n�ration actuelle�.
