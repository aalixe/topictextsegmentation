TITRE: L'offensive de charme de Fran�ois Hollande dans la Silicon Valley
DATE: 2014-02-12
URL: http://www.lemonde.fr/politique/article/2014/02/12/hollande-un-president-mefiant-d-internet-dans-la-silicon-valley_4365327_823448.html
PRINCIPAL: 168368
TEXT:
L'offensive de charme de Fran�ois Hollande dans la Silicon Valley
Le Monde |
� Mis � jour le
13.02.2014 � 15h05
La visite de Fran�ois Hollande dans la Silicon Valley, mercredi�12�f�vrier, a constitu� un moment-cl� du p�riple am�ricain du chef de l'Etat. Il en a profit� pour proclamer sa foi dans les entreprises innovantes et les start-up, mais aussi sa disposition � accueillir en France les g�ants am�ricains de l'Internet.
Symbole de cette lune de miel�: le hug (� accolade��) qu'il a accord� � Carlos Diaz, un entrepreneur fran�ais � l'origine du mouvement des � pigeons � , une r�volte de patrons contre la hausse un temps envisag�e de la taxation des plus-values sur les cessions d'entreprises.
Cet acte de r�conciliation, une � preuve d'amour�� aux entrepreneurs, selon la ministre d�l�gu�e charg�e des PME, de l'innovation et de l'�conomie num�rique, Fleur Pellerin, a servi de tremplin � M. Hollande, qui a ensuite fait une s�rie d'annonces devant le French Tech Hub, un incubateur d'entreprises financ� notamment par le conseil r�gional d' Ile-de-France .
��PASSEPORTS TALENTS�� ET ��FINANCEMENT PARTICIPATIF��
��La France doit reconna�tre le dynamisme de ses entrepreneurs�� et favoriser ��l'esprit d'initiative��, a notamment lanc� le pr�sident devant quelques dizaines de patrons de start-up fran�aises.
Il a promis ��une nouvelle impulsion�� au ��financement participatif��, avec l'adoption, ��le mois prochain��, d'une ordonnance pour que ce dispositif devienne en France ��aussi incitatif qu'aux Etats-Unis��. ��Un projet pourra recueillir jusqu'� 1�million d'euros de pr�t sur une plateforme de financement participatif�� pour la cr�ation d'entreprises, a-t-il d�taill�.
Lire aussi l'entretien : Marc Rougier, patron de start-up, boycottera la tourn�e de Hollande
Le chef de l'Etat a �galement appel� le pr�sident du Medef, Pierre Gattaz, qui l'accompagnait, � explorer avec les grands groupes fran�ais la ��piste�� d'offres d'embauche similaires � celles propos�es en Californie pour trois ans aux �tudiants en fin de cycle. Elles leur permettraient de ��disposer d'une s�curit� pour d�velopper leur propre entreprise��, a-t-il expliqu�.
Parmi les id�es lanc�es, celle de ��passeports talents�� donnerait aux cr�ateurs, innovateurs et entrepreneurs �trangers la possibilit� de recevoir plus facilement un visa fran�ais. ��Entre�5�000 et 10�000�personnes�� pourraient en b�n�ficier chaque ann�e, a-t-il pr�cis�.
Observant que pour attirer les talents dans les start-up il fallait aussi les r�mun�rer � hauteur de ce qu'offrent les grands groupes, ��ce qui n'est pas simple quand une entreprise se cr�e��, M.� Hollande a �voqu� l'id�e ��d' am�liorer le r�gime des attributions gratuites d'actions et de bons de souscription de parts de cr�ateurs d'entreprises��.
�
� SUR INTERNET, QU'EST-CE QUI EST VRAI ?��
S'il a lou� l'esprit des entrepreneurs du num�rique, M. Hollande n'en reste pas moins sceptique lorsqu'il s'agit du Web. Comme l'a rappel� Le Parisien jeudi, il reste � un homme de l'�crit�� qui cultive une v�ritable m�fiance vis-�-vis des nouveaux m�dias .�
��Il y a une forme d'addiction � tout �a dont il faut absolument se pr�munir��, aurait-il d�clar�. Fran�ois Hollande aurait �galement d�nonc� la���violence�� et la���propagation des rumeurs�� en ligne.�� Sur Internet, qu'est-ce qui est vrai ? Le pire, ce sont les attaques priv�es� � Ces d�clarations n'avaient pas �chapp� � la presse am�ricaine qui, � l'image du Washington Post , pr�disait des � discussions tendues�� entre Fran�ois Hollande et de nombreux� tycoons de l'Internet avec qui il a d�jeun�, dont Eric Schmidt ( Google ), Sheryl Sandberg ( Facebook ), Jack Dorsey ( Twitter ) ou Mitchell Baker (Mozilla Foundation).
M. Hollande leur a lanc� le message suivant�:
��Nous n'avons peur de rien, pas peur de mettre nos meilleures entreprises dans la Silicon Valley, pas peur non plus d' attirer des talents ou des investisseurs �trangers dans notre pays (�) Nous devons accepter une �mulation favorable � l' emploi dans notre pays, c'est pour �a que j'ai dit � ces grands groupes�: �Venez investir en France, venez cr�er des emplois, (�) venez aussi soutenir les start-up fran�aises.���
Il n'a toutefois pas abord� avec eux la question qui f�che�: leurs pratiques d'��optimisation fiscale�� qu'il avait jug� ��pas acceptables � � quelques jours de son d�part pour les Etats-Unis.
