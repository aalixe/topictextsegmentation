TITRE: Crash d'un avion en Alg�rie : trois jours de deuil national d�cr�t� � metronews
DATE: 2014-02-12
URL: http://www.metronews.fr/info/crash-d-un-avion-en-algerie-trois-jours-de-deuil-national-decrete/mnbl!veZLdYkVFIXz/
PRINCIPAL: 166514
TEXT:
Cr�� : 12-02-2014 09:08
Crash en Alg�rie : trois jours de deuil national
ACCIDENT - Au lendemain du crash d'un avion militaire en Alg�rie qui a fait 77 morts, trois jours de deuil national ont �t� d�cr�t�s mercredi. Le bilan fait �tat d'un seul survivant.
�
Un orage accompagn� de chutes de neige expliquerait l'accident. Photo :�Mohamed Ali/AP/SIPA
Un pays entier plong� dans la douleur. Apr�s le crash meurtrier d'un avion militaire mardi dans l'est de l'Alg�rie, un deuil national de trois jours a d�but� mercredi. L'accident, l'une des pires catastrophes a�riennes de l'histoire alg�rienne, a fait 77 morts et laiss� la vie sauve � une seule personne.  L'avion, un Hercules C-130, qui assurait la liaison entre la pr�fecture de Tamanrasset (2.000 km au sud d'Alger) et Constantine (450 km � l'est d'Alger), transportait des militaires et des familles de militaires.
Crash � l'approche de l'a�roport
La catastrophe s'est produite vers midi. Les conditions m�t�orologiques sont mauvaises, un orage accompagn� de chutes de neige �clate dans cette zone montagneuse d'Oum El Bouaghi, � 500 km � l'est d'Alger o� la visibilit� est mauvaise. Et l'avion s'�crase, il n'en reste qu'une carcasse montr�e par les images des cha�nes locales. "Les soldats qui ont p�ri dans le crash de l'avion militaire sont des martyrs du devoir", a r�agi le pr�sident Bouteflika qui a pr�sent� ses condol�ances aux familles.
Pr�s de 250 secouristes de la protection civile, sont affect�s au site du crash rendu difficile d'acc�s en raison du mauvais temps et de l'escarpement du lieu, a annonc� la radio alg�rienne. "Une commission d'enqu�te a �t� cr��e et d�p�ch�e sur les lieux pour d�terminer les causes et les circonstances exactes de ce tragique accident", selon le communiqu� minist�riel. Le pr�c�dent accident le plus meurtrier date de mars 2003. Un Boeing 737-200 de la compagnie publique alg�rienne Air Alg�rie avait fait 102 morts et un bless� en s'�crasant peu apr�s son d�collage de Tamanrasset.
Ozal Emier
