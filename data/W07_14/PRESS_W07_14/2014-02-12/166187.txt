TITRE: Yahoo! ach�te la startup Wander
DATE: 2014-02-12
URL: http://www.leparisien.fr/high-tech/yahoo-achete-la-startup-wander-12-02-2014-3583227.php
PRINCIPAL: 166185
TEXT:
Yahoo! ach�te la startup Wander
Publi� le 12.02.2014, 08h28
R�agir
Le groupe internet am�ricain Yahoo !, qui cherche � relancer sa croissance, a annonc� mardi une nouvelle acquisition, celle de la start-up Wander.
Wander avait lanc� en mars 2013 une application baptis�e Days, permettant de cr�er un journal visuel en rassemblant des photos prises un jour donn�. Ses �quipes vont rejoindre celles de Yahoo! � New York.
Les modalit�s financi�res de la transaction ne sont pas divulgu�es, mais le site sp�cialis� dans les informations technologiques TechCrunch �voque un montant de plus de 10 millions de dollars.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Yahoo! a d�j� achet� une trentaine d'entreprises depuis l'arriv�e aux commandes � l'�t� 2012 de la directrice g�n�rale Marissa Mayer, qui cherche ainsi � recruter des talents mais aussi � am�liorer les services du groupe.
Elle a fait des services adapt�s aux appareils mobiles, et notamment aux smartphones, sa priorit�.
Lors d'une intervention mardi � une conf�rence organis�e par la banque Goldman Sachs en Californie, elle n'a pas exclu � l'avenir d'utiliser des informations disponibles sur ces appareils, par exemple sur la localisation des gens ou leurs habitudes, pour am�liorer les r�sultats de leurs recherches. "Nous pouvons offrir beaucoup (...) au-del� de la recherche de base", a-t-elle estim�.
Yahoo! avait �t� un pionnier de la recherche en ligne mais est aujourd'hui largement distanc� par le rival Google . Mme Mayer a pour mission de relancer sa croissance, en panne depuis plusieurs ann�es, et de rajeunir son public, avec des r�sultats jusqu'ici mitig�s.
Certains remaniements de services centraux du groupe, comme sa messagerie Yahoo!Mail, ont �t� accueillis assez ti�dement par les utilisateurs. Et les r�sultats 2013 du groupe, publi�s fin janvier, montraient encore une baisse de 6% du chiffre d'affaires, � 4,7 milliards de dollars.
