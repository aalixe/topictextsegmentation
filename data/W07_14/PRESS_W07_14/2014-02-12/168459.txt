TITRE: La moiti� des immeubles d'Ath�nes priv�s de chauffage central - Lib�ration
DATE: 2014-02-12
URL: http://www.liberation.fr/monde/2014/02/12/la-moitie-des-immeubles-d-athenes-prives-de-chauffage-central_979826
PRINCIPAL: 0
TEXT:
Lire sur le readerMode zen
Des demandeurs d'emplois patientent devant l'agence grecque pour l'emploi (OAED), � Ath�nes, en d�cembre 2013 (Photo Louisa Gouliamaki. AFP)
A cause de la crise, 44% des immeubles renoncent � leur consommation de mazout cet hiver. Ce pourcentage est en hausse par rapport � l�ann�e derni�re.
Pr�s de la moiti� (44%) des immeubles d�Ath�nes �quip�s de chaudi�re centrale au mazout se passent de chauffage cet hiver en raison du prix de ce combustible et des baisses de pouvoir d�achat, a indiqu� mercredi la soci�t� de distribution de gaz EPA.
Ce pourcentage est en hausse par rapport � l�ann�e derni�re o� il �tait de 33%, selon le directeur de l�EPA en Attique, la r�gion d�Ath�nes, Christos Balaskas, cit� par l�agence de presse Ana (semi-officielle).
A Ath�nes et sa banlieue, qui regroupent environ un tiers des onze millions de Grecs, les immeubles anciens sont souvent �quip�s de chauffage central au mazout et depuis le d�but de la crise de la dette qui a fait plonger le niveau de vie de la population, les copropri�taires sont toujours plus nombreux � renoncer � cette d�pense.
Pollution atmosph�rique
L�hiver 2013-2014 est �galement particuli�rement doux � Ath�nes avec des temp�ratures r�guli�rement sup�rieures � 15�.�L�alignement en 2012 de la taxation du fioul domestique sur celle de l�essence, dans le cadre de la cure de rigueur administr�e au pays surendett�, a encore davantage gr�v� les d�penses de chauffage.
En revanche le bond enregistr� par le chauffage au bois a fait augmenter la pollution atmosph�rique devenue, lors des soir�es les plus fra�ches, respirable et visible sous la forme d�une brume flottant sur les principales villes du pays.
