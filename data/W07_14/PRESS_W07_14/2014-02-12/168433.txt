TITRE: Comment exploiter les inefficiences des march�s actions
DATE: 2014-02-12
URL: http://www.latribune.fr/opinions/tribunes/20140210trib000814511/comment-exploiter-les-inefficiences-des-marches-actions.html
PRINCIPAL: 0
TEXT:
Comment exploiter les inefficiences des march�s actions
Hugues Le Maire �|�
10/02/2014, 11:55
�-� 789 �mots
Les profits des entreprises devraient globalement progresser en 2014. M�me si la Bourse am�ricaine a beaucoup progress�, des opportunit�s y subsistent, � condition de jouer sur les inefficiences de march�. par Hugues Le Maire, co-fondateur de Diamant Bleu Gestion
Dans un environnement �conomique en am�lioration, les march�s boursiers regorgent d'actifs sous-valoris�s, m�me apr�s cinq ann�es de hausse continue aux Etats-Unis. Pour profiter de cet important potentiel de rattrapage, une approche s�lective par � cas d'investissement � bien
sp�cifique est indispensable.
Plus de profits en 2014
M�me si la croissance du PIB de la zone euro restera modeste cette ann�e (estim�e � 1,0%, contre 3,7% � l'�chelle mondiale selon le FMI), la reprise �conomique devrait jouer en faveur d'une
am�lioration des profits des soci�t�s. Les prochains mois devraient permettre � celles-ci de
concr�tiser leurs efforts de r�organisation par de meilleurs niveaux de rentabilit�, tout en amor�ant
une croissance de leurs ventes (� top line growth �) plus soutenue. Un double ph�nom�ne que les
march�s actions n'ont pas encore totalement int�gr�. Aux �tats-Unis, le march�s est, dans
l'ensemble, mieux valoris�. La situation �conomique et budg�taire du pays est bien plus avantageuse et les march�s actions l'ont anticip�. Les indices actions am�ricains se traitent dans leur ensemble � des niveaux de valorisation qui sont plut�t de l'ordre de 15 � 20 fois les profits actuels.
Les actions am�ricaines ont beaucoup mont�, mais...
Est-ce � dire qu'il ne faut pas investir aux Etats-Unis car le march� est trop cher ? Qu'il faut
concentrer les portefeuilles sur la zone euro, dont les actions sont meilleur march� ? Pas tout � fait.
Nous observons plut�t que les march�s de part et d'autre de l'Atlantique regorgent d'actifs encore
mal valoris�s. Exploiter ces inefficiences de valorisation n�cessite une approche s�lective et
discriminante des titres, non pas sectorielle, th�matique ou g�ographique - des approches
d�pass�es et inadapt�es au contexte globalis� des march�s -, mais par � cas d'investissement�.
Trois cat�gories d'actions en Europe
En Europe, on distingue aujourd'hui trois cat�gories d'actions : les valeurs ayant bien perform� et
plut�t ch�res ; les valeurs bon march� pour de bonnes raisons (qu'on a l'habitude d'appeler � value traps �) ; enfin, les valeurs bon march� pour de mauvaises raisons. C'est pr�cis�ment dans ce troisi�me groupe que les id�es d'investissement sont les plus int�ressantes. Il s'agit tout
particuli�rement de certains titres du secteur bancaire europ�en, pour lesquels le retard de
valorisation est souvent cons�quent et plus vraiment l�gitime.
La banque portugaise Espirito Santo en est un tr�s bon exemple. Longtemps sous pression en raison des risques syst�miques associ�s � la qualit� d'emprunteur du pays - le cours de bourse a trait� jusqu'� 25% des actifs -, le titre a toutes les raisons de poursuivre son redressement dans les mois � venir. Comme d'autres banques des pays p�riph�riques (Banco Santander en Espagne, Intesa Sanpaolo en Italie), Espirito Santo va continuer � tirer parti d'un environnement de march� en am�lioration et de la reconstitution de ses fonds propres.
Jouer la reprise du march� automobile mondial
Certains segments de l'industrie sont �galement bien positionn�s. Le red�marrage de l'activit�
�conomique et en particulier la reprise du march� automobile mondial ont d�j� largement profit�
aux soci�t�s impliqu�es dans le secteur comme Duerr, sp�cialiste � divers niveaux des cha�nes de montage automobile. Depuis le d�but de l'ann�e 2009, le cours de l'indice Eurostoxx Autos a
progress� de 200% alors que le cours de Duerr a progress� de 1400%.
Le constat d'une reprise cyclique est identique pour certains acteurs comme Arcelor ou Eramet. Le ralentissement de la croissance en Chine, notamment, semble d�j� bien int�gr� dans la valorisation tr�s faible de ces titres. Et structurellement, le r��quilibrage du mod�le �conomique chinois, davantage tourn� vers la consommation domestique dans les ann�es � venir, permettra tout au moins au cours des mati�res premi�res de se stabiliser. Cela devrait repr�senter un facteur de soutien pour ces titres.
Le retard de certaines "mega cap"
La situation est diff�rente mais pas moins int�ressante aux Etats-Unis. Si les march�s sont, dans leur ensemble mieux valoris�s, un certain nombre de m�ga-caps comme General Electric ou Caterpillar ;accusent un vrai retard de revalorisation. Ces valeurs sont rest�es � l'�cart du cycle haussier des march�s US, engag� en mai 2009. Par ailleurs, les th�matiques sous-jacentes au pays telles que � l'ind�pendance �nerg�tique �, � la reprise du cycle de l'�quipement � ou encore � le r�le strat�gique des terres rares � pr�sentent autant d'opportunit�s d'investissement � plus long terme.
S'il est exact que des march�s chroniquement sous-�valu�s comme ceux de la zone euro pr�sentent structurellement plus d'opportunit�s, on ne saurait se limiter � cette seule zone g�ographique et les Etats-Unis, le Japon, bient�t certains march�s �mergents, pr�sentent d'innombrables opportunit�s d'investissement.
R�agir
Recevoir un email si quelqu'un r�pond � mon commentaire.
Envoyer commentaire
Commentaires
aaa �a �crit le 10/02/2014                                     � 19:40 :
Les actions, c'est le cancer de notre �conomie? Tous les entreprises qui entre en actions finissent toujours t�t ou tard par d�localiser pour que leur actions ne chute pas ! Si je ne me trompe pas bien s�r...
Michel �a r�pondu le 10/02/2014                                                 � 22:37:
L'inaction c'est tellement plus efficient...
R�pondre
Suivre
Pour �tre alert� par email d'une r�action � ce commentaire, merci de                                             renseigner votre adresse email ci-dessous :
