TITRE: JO/Ski: deux reines, Maze et Gisin, au bout d'une descente palpitante - LExpress.fr
DATE: 2014-02-12
URL: http://www.lexpress.fr/actualites/1/sport/jo-ski-la-slovene-maze-et-la-suissesse-gisin-championnes-de-descente_1323000.html
PRINCIPAL: 166689
TEXT:
JO/Ski: deux reines, Maze et Gisin, au bout d'une descente palpitante
Par AFP, publi� le
12/02/2014 � 09:22
, mis � jour � 13:14
Rosa Khoutor (Russie) - La Suissesse Dominique Gisin et la Slov�ne Tina Maze se sont par�es toutes deux de l'or olympique de la descente mercredi aux Jeux de Sotchi, pour une grande premi�re dans l'histoire du ski alpin.
La Suissesse Dominique Gisin et la Slov�ne Tina Maze se sont par�es toutes deux de l'or olympique de la descente mercredi aux Jeux de Sotchi, une premi�re dans l'histoire du ski alpin.
afp.com/Alexander Klein
Les deux dames, de la m�me g�n�ration et au physique similaire, sont mont�es main dans la main sur le podium, se sachant li�es � jamais au palmar�s olympique alors qu'elles n'avaient pas grand chose en commun jusqu'� pr�sent, mis � part leur passion pour la glisse.�
Car l'une est issue d'un pays d'Europe centrale qui n'avait jamais obtenu le moindre titre olympique aux Jeux d'hiver. Tandis que l'autre porte les couleurs d'une des plus grandes nations du ski alpin.�
Mais si Tina Maze passait pour l'une des grandes pr�tendantes au titre, elle qui avait domin� outrageusement la saison de Coupe du monde 2013, Dominique Gisin faisait figure de surprise potentielle. �
La Suissesse avait obtenu sa place dans le portillon de d�part que la semaine derni�re, au vu de ses bons r�sultats � l'entra�nement. Comme son compatriote Didier D�fago, aux Jeux de Vancouver, qui s'�tait lui aussi adjug� l'or de l'�preuve reine quelques jours apr�s sa s�lection.�
- Gut en bronze -�
La carri�re de Gisin �tait marqu�e plus par les op�rations aux genoux, une bonne dizaine, que par les victoires, trois seulement en Coupe du monde.�
C'est plut�t Lara Gut, qui passait pour le principal espoir de m�daille de l'�quipe suisse. Mais la demoiselle, 22 ans, se mordait les doigts d'avoir laisser filer 10 centi�mes sur cette piste olympique, qui la laissait avec la m�daille de bronze. �
Dominique Gisin a su profiter pleinement de la chance que lui offrait un bon dossard, le N.8, pour signer le temps de r�f�rence en 1 min 41 et 57/100e que seule Tina Maze, avec le N.21, est parvenue � �galer.�
La couronne olympique de la descente qu'avait mise aux ench�res la star du ski alpin f�minin, l'Am�ricaine Lindsey Vonn, avec son forfait pour les Jeux, a donc trouv� non pas une mais deux preneuses au terme d'une course palpitante.�
Julia Mancuso, sa dauphine pensait en profiter, elle qui avait domin� la manche de vitesse lundi du super-combin�, mais la croqueuse de m�dailles a �t� priv�e cette fois du festin (8e, � pr�s d'une seconde).�
Gisin et Maze illuminent leur carri�re chacune � leur fa�on.�
Il ne manquait plus que cela � la Slov�ne, d�j� double m�daill�e d'argent aux Jeux de Vancouver et sextuple m�daill�e mondiale, pour finir d'�crire sa l�gende. �
"J'ai r�alis� mon r�ve. Je n'avais pas d'autre but que remporter l'or olympique de la descente", a soulign� Maze.�
'Un grand honneur'�
Si l'an dernier elle avait battu pas mal de records en Coupe du monde, dont celui du nombre de points amass�s en une saison (2414) et du nombre de podiums (24), la fonceuse slov�ne avait laiss� les premiers r�les � d'autres cet hiver, avec une seule victoire sur le circuit.�
"C'est un grand honneur de partager l'or avec elle. C'est une telle skieuse ! J'ai partag� ma premi�re victoire en Coupe du monde avec Anja Paerson, alors je suis ravie de partager ce titre avec Tina", a fait valoir la Suissesse, en faisant r�f�rence � l'ogresse su�doise qui dominait le ski alpin dans les ann�es 2000.�
Dominique Gisin peut �tre fi�re: elle la premi�re Suissesse couronn�e en descente depuis le sacre de la grande Micha�la Figini � Sarajevo en 1984.�
L'Italienne Daniela Merighetti pouvait se frapper la t�te dans la neige, elle qui a fait une grande partie de la course en t�te avant de perdre de vue le podium sur un saut et prendre la 4e place (+27/100e).�
Chasse � l'or oblige, les coureuses ont pouss� les risques � la limite, au risque de chuter. Marie Marchand-Arvier, la seule Fran�aise en lice, a ainsi fini sa course dans le filets.�
Par
