TITRE: Un ancien gendarme condamné à la perpétuité pour le meurtre d'une vieille dame
DATE: 2014-02-12
URL: http://www.lefigaro.fr/actualite-france/2014/02/12/01016-20140212ARTFIG00402-un-ancien-gendarme-condamne-a-la-perpetuite-pour-le-meurtre-d-une-vieille-dame.php
PRINCIPAL: 0
TEXT:
Publié
le 12/02/2014 à 20:05
Le tribunal de Toulouse a prononcé mercredi la réclusion criminelle à perpétuité à l'encontre de Daniel Bedos. Crédits photo : JACQUES DEMARTHON/AFP
Responsable de la mort d'une femme de 97 ans, Daniel Bedos avait tenté de faire accuser un innocent.
Publicité
«Avec tout le respect que je vous dois, je n'ai rien à faire dans ce box», lançait Daniel Bedos devant les assises de la Haute-Garonne , à l'ouverture de son procès ce lundi. Pourtant, la justice aura estimé ce mercredi que cet ex-gendarme, qui ne cesse de clamer son innocence depuis trois ans, est bien responsable du meurtre de Suzanne Blanc. Alors que l'avocat général avait requis trente ans de prison, le tribunal est allé au-delà en prononçant la réclusion criminelle à perpétuité.
Le 18 août 2010, Daniel Bedos pénétrait chez Suzanne Blanc, 97 ans, pour lui dérober bijoux et argent. Après avoir fracassé la tête de la vielle dame à coups de poings, il avait laissé sa victime agonisante tandis qu'il fouillait la maison. Découverte par son infirmière, Suzanne Blanc est morte de ses blessures le lendemain. Daniel Bedos est arrêté après avoir déposé les bijoux de la victime chez un revendeur. Mais alors que tout l'accable, il accuse une lointaine connaissance, Jean-Claude Durandeu, rencontré dans un PMU. Ce dernier lui aurait cédé les bijoux pour 170 euros le jour de l'agression.
Alcoolique et dépressif, Jean-Claude Durandeu apparaît comme le coupable idéal ; il a déjà passé neuf ans en prison pour un homicide lors d'une bagarre d'ivrognes. Placé en détention provisoire, puis sous contrôle judiciaire, il a été mis hors de cause. Daniel Bedos a finalement été trahi par les aveux de son ex-compagne à qui il avait demandé de témoigner en sa faveur.
La jurés auront dû s'en remettre à leur intime conviction. En effet, l'agresseur de Suzanne Blanc n'a laissé ni traces ADN, ni empreintes digitales. Pour Me Catala, avocat de la famille de la victime, cette étonnante absence de preuve matérielle a pour origine le passé de gendarme de l'accusé. Officier de police judiciaire entre 1978 et 1992, Daniel Bedos a notamment été décrit par l'avocat général comme précautionneux, «manipulateur et sans affect».
