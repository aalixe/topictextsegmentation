TITRE: Lamy Chappuis, la t�te dans le sac - BFMTV.com
DATE: 2014-02-12
URL: http://www.bfmtv.com/sport/lamy-chappuis-tete-sac-709322.html
PRINCIPAL: 0
TEXT:
> JO
Lamy Chappuis, la t�te dans le sac
Champion olympique il y a 4 ans � Vancouver, Jason Lamy Chappuis a termin� � une triste 35e place ce mercredi sur l��preuve du petit tremplin de Sotchi. Une d�sillusion pour le skieur de Bois d�Amont, rest� longtemps sous le choc.
P.Taisne � Sotchi
r�agir
Jason Lamy Chappuis a tent� de faire bonne figure devant les micros et face aux cam�ras. Une indigne 35e place sur le concours du petit tremplin du combin� nordique � 2�37 de l�Allemand Frentzen. Dans le camp fran�ais, c�est l�incompr�hension. Au moment de passer la ligne, ��JLC�� a pos� les mains sur les genoux avant de se diriger vers un technicien. Au centre de la discussion, les skis que le skieur de Bois d�Amont montre � plusieurs reprises. Pas question pour autant d�incriminer qui que ce soit devant la presse.
��Est-ce que c�est physique ? Le fart ? Les  structures ? Il faudra d�briefer avec les coaches et techniciens��,  livre le skieur � chaud. Comme � son habitude, Lamy Chappuis s�ex�cute  face � la presse. Sans sourciller. L�attitude est positive. Le triple  vainqueur de la Coupe du monde de la discipline fait bonne figure, mais  le mal semble plus profond. Au moment de retrouver son entra�neur, il  semblait perdu. ��Je suis d�go�t�, je voyais passer les autres � c�t�,  je ne comprenais pas pourquoi��, l�che-t-il � Etienne Gouy.
Des mines d�confites
Le coach encha�ne�: ���C�est un jour sans, on avait du mat�riel pas super. On avait fait un choix, �a n�est pas pass�.�� Lui aussi a le regard dans le vide et semble touch�. De retour � la cabine de fartage, Lamy Chappuis arrive le dernier. Les mines sont d�confites . Certaines personnes de l�entourage ne retiennent pas leurs larmes. Sans mettre les techniciens en cause, il se plaint une fois de plus de ses skis. Quinze minutes apr�s avoir d�barqu�, il quitte les lieux. Quant aux techniciens, ils souhaitent tester les skis, mais refus de l�organisation en raison de la fermeture des pistes.
Auteur de son pire classement depuis le 16 d�cembre 2006,  Lamy Chappuis devra d�sormais attendre mardi prochain pour prendre sa  revanche sur grand tremplin. D�ici l�, deux jours de repos. ���a m�a  fait mal au c�ur de le doubler, d�taille son co�quipier Maxime Laheurte.  Ce n�est pas sa place, �a ne refl�te pas son niveau. Ce n��tait pas un  bon jour, mais je ne me fais pas de soucis. �a ira beaucoup mieux la  semaine prochaine.��
Toute l'actu Sport
