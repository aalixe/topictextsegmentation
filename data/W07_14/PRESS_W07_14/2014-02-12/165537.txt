TITRE: Une jambe flottait dans la Seine - 12/02/2014 - leParisien.fr
DATE: 2014-02-12
URL: http://www.leparisien.fr/espace-premium/hauts-de-seine-92/une-jambe-flottait-dans-la-seine-12-02-2014-3581971.php
PRINCIPAL: 165516
TEXT:
Une jambe flottait dans la Seine
Publi� le 12 f�vr. 2014, 07h00
Mon activit� Vos amis peuvent maintenant voir cette activit�
Tweeter
On ignore le propri�taire du membre. Hier apr�s-midi, des passants ont aper�u une jambe humaine qui d�rivait dans la Seine au niveau du quai du Pr�sident -Carnot, � Saint-Cloud. Ils ont aussit�t pr�venu la police. Les plongeurs de la brigade fluviale ont r�cup�r� le membre en �tat de d�composition avanc�e. Les enqu�teurs vont maintenant se lancer � la recherche du reste du corps. Selon les premiers �l�ments du dossier, il s'agirait d'une personne � la peau blanche, son sexe et son �ge restant encore inconnus. Des pr�l�vements ADN devraient �tre effectu�s. Les policiers vont �galement v�rifier les disparitions signal�es dans le secteur. Selon un proche du dossier, la mutilation constat�e pourrait avoir �t� provoqu�e par l'h�lice d'une p�niche.
