TITRE: How I Met Your Dad: d�couvrez le visage de l�actrice qui obtient le r�le principal du spin-off d�How I Met Your Mother (photos) - sudinfo.be
DATE: 2014-02-12
URL: http://www.sudinfo.be/933484/article/fun/tele/2014-02-12/how-i-met-your-dad-decouvrez-le-visage-de-l-actrice-qui-obtient-le-role-principa
PRINCIPAL: 167290
TEXT:
How I Met Your Dad: d�couvrez le visage de l'actrice qui obtient le r�le principal du spin-off d'How I Met Your Mother (photos)
R�daction en ligne
Les producteurs d�How I Met Your Dad, le spin-off d�How I Met Your Mother, viennent de recruter l�actrice qui incarnera le r�le principal. C�est Greta Gerwig qui incarnera la version f�minine de Ted Mosby.
Tweet
Photo News
Un �pisode pilote command� pour la fin d�ann�e et Greta Gerwig dans le r�le principal, le spin-off d�How I Met Your Mother, How I Met Your Dad, commence tout doucement � prendre forme�!
En plus d�avoir �t� choisie pour incarner l��quivalent f�minin de Ted Mosby, la jolie blonde, aper�ue r�cemment dans To Rome with Love ou Frances Ha, �crira �galement plusieurs �pisodes.
Dans ce spin-off, Greta Gerwig interpr�tera Sally, une jeune femme qui vient de divorcer et qui ne sait pas quoi faire de sa vie. Si Ted, Marshall, Lily, Robin et Barney devaient th�oriquement dispara�tre du petit �cran fin mai 2014, le pub o� ils se rencontrent, le Mac Laren, sera toujours pr�sent. Qui sait, les acolytes reviendront peut-�tre prendre une derni�re tourn�e en 2015�?
Les + lus
