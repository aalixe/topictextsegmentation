TITRE: Top 14: Laporte, suspendu 13 semaines, Toulon fera appel  - Flash actualit� - Sports - 12/02/2014 - leParisien.fr
DATE: 2014-02-12
URL: http://www.leparisien.fr/flash-actualite-sports/top-14-laporte-suspendu-13-semaines-pour-injure-a-un-arbitre-12-02-2014-3584305.php
PRINCIPAL: 168123
TEXT:
Le manager de Toulon Bernard Laporte a �t� interdit mercredi de diriger son �quipe en match pendant 13 semaines, soit jusqu'� la fin de la saison r�guli�re de Top 14 , une sanction lourde apr�s ses propos insultants envers un arbitre.
Le club varois a imm�diatement annonc� qu'il faisait appel de cette d�cision.
Les membres de la commission de discipline, qui avait auditionn� l'ancien secr�taire d'Etat aux Sports mercredi dernier, s'�taient donn�s une semaine de r�flexion pour statuer sur une �ventuelle sanction.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Ils ont finalement d�cid� d'interdire l'ancien entra�neur du XV de France "d'acc�der au terrain, aux vestiaires (des �quipes et des arbitres) ainsi qu?aux couloirs d?acc�s � ces zones lors des matches officiels (y compris avant et apr�s les matches) pendant 13 semaines cons�cutives, soit jusqu?au 13 mai 2014 inclus".
Laporte n'aura donc le droit de revenir qu'en cas de qualification de son �quipe, championne d'Europe en titre et vice-championne de France, pour les demi-finales de Top 14 pr�vues les 16 et 17 mai.
Ses propos avaient fait l'effet d'une bombe dans le monde du rugby. Le 9 janvier, sur la radio RMC pour laquelle il est consultant, il �tait revenu sur la d�faite (22-21) de son �quipe � domicile le week-end pr�c�dent face � Grenoble, qualifiant l'arbitre de la rencontre, Laurent Cardona, de "nul, d'"incomp�tent" et de "pipe" qui "vole � chaque fois" son �quipe.
- R�cidiviste -
Ces paroles ont d'autant plus choqu� qu'elle ont �t� tenues � froid, cinq jours apr�s la rencontre, et non dans un de ces coups de col�re qui ont valu � Laporte le surnom de "Bernie le dingue".
Le manageur avait d'ailleurs r�it�r� ses propos deux jours plus tard, � l'issue d'un match de Coupe d'Europe contre Cardiff.
"Tu te dis que ce sera toujours pareil quand tu es tributaire d'une pipe", avait-il lanc�, avant d'ajouter: "C'est comme pour Dieudonn�, � un moment donn�, il faut que quelqu'un dise stop."
Laporte avait �galement d�plor� que l'arbitre ne lui ait pas directement r�pondu au t�l�phone mais ait pr�f�r� communiquer par message �crit. "Cardona essaie d'�tre ami avec moi mais ses textos, il peut se les carrer dans le c...".
L'ancien demi de m�l�e et ex-entra�neur du XV de France est un r�cidiviste en la mati�re. Il avait d�j� �t� interdit de banc et de vestiaire 60 jours en 2012 pour des propos injurieux envers l'arbitre Romain Poite, quelques mois apr�s la suspension de son pr�sident provocateur Mourad Boudjellal pour 130 jours pour avoir affirm� avoir subi "une sodomie arbitrale" lors d'un match de Top 14.
Le club de Toulon a d'ailleurs �t� condamn� � 10.000 euros d'amende pour les propos de son manager.
Avant sa comparution, Laporte avait affirm� sur RMC ne pas craindre une �ventuelle sanction: "Je ne joue pas, moi. Je suis en tribune, ils me mettront sur le toit de la tribune".
En attendant, ses adjoints Pierre Mignoni (arri�res) et Jacques Delmas (avants) seront donc ses relais durant les matches. En esp�rant que son �quipe se qualifie pour les demi-finales du Top 14.
