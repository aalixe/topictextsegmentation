TITRE: Centrafrique : � Bangui, Le Drian tape du poing sur la table contre les milices - Monde - MYTF1News
DATE: 2014-02-12
URL: http://lci.tf1.fr/monde/afrique/centrafrique-a-bangui-le-drian-martele-le-message-francais-8364245.html
PRINCIPAL: 166292
TEXT:
amnesty international , jean-yves le drian , centrafrique , ong
AfriqueAlors que les milices continuent � faire r�gner la terreur, le ministre de la D�fense est � Bangui ce mercredi pour affirmer que la France ne laissera pas les exactions continuer.
Depuis le 5 d�cembre et le d�but de l'intervention fran�aise, c'est la 3e fois que Jean-Yves Le Drian se rend en Centrafrique . Objectif : montrer la d�termination de la France � stopper les violences interconfessionnelles, qui n'ont pas cess� malgr� le d�ploiement des forces internationales.
�
Avant son arriv�e, le ministre de la D�fense a affirm� �tre pr�t � mettre fin aux violences, "si besoin par la force". "Il faut que l'ensemble des milices qui continuent aujourd'hui � mener des exactions, � commettre des meurtres, arr�tent", a-t-il mardi lors d'une visite au Congo. Dans cette optique, il autorise donc les forces fran�aises � "appliquer les r�solutions des Nations unies, si besoin par la force". Son message est identique pour les soldats africains.
Le cri d'alarme d'Amnesty
�
L' ONG souligne que les forces internationales ne "parviennent pas � emp�cher" ces vengeances, cons�cutives aux massacres commis par les anciens rebelles de la S�l�ka entre mars et d�cembre 2013.
�
