TITRE: L'�tat de Michael Schumacher, atteint d'une infection pulmonaire, s'aggrave - 12/02/2014 - LaD�p�che.fr
DATE: 2014-02-12
URL: http://www.ladepeche.fr/article/2014/02/12/1816676-etat-michael-shumacher-atteint-infection-pulmonaire-aggrave.html
PRINCIPAL: 166236
TEXT:
L'�tat de Michael Schumacher, atteint d'une infection pulmonaire, s'aggrave
Publi� le 12/02/2014 � 09:20
,
Mis � jour le 12/02/2014 � 09:42
| 28
Michael Schumacher en 2006 lors d'un entra�nement Jose Jordan �/�AFP/Archives
Les ennuis continuent pour Michael Schumacher... D'apr�s le quotidien allemand Bild, le champion de Formule 1 aurait contract� la semaine derni�re une infection pulmonaire. Cette aggravation de son �tat intervient juste apr�s la tentative de r�veil effectu�e par les m�decins de l'h�pital de Grenoble.
Le soutien de sa femme
Michael Schumacher irait de mal en pis. Quinze jours apr�s le d�but de la phase de r�veil progressif, ses m�decins ne constatent aucune am�lioration. Il ne r�pond pas aux stimuli du personnel hospitalier, bien que sa femme passe ses journ�es � ses c�t�s � lui parler dans l'espoir d'obtenir une r�action. Apr�s six semaines bloqu� dans un coma artificiel, le champion de formule 1 voit aujourd'hui son �tat s'aggraver � cause d'une infection aux poumons.
Une clarification attendue
�L'ancien m�decin chef de la F1, Gary Hartstein, se montre tr�s pessimiste dans son blog sur la sant� de Michael Schumacher. Il pense que le champion va rester dans un �tat v�g�tatif, malgr� l'absence d'informations �manant du CHU. Quant � Sabine Kehm, porte-parole de la famille Schumacher, elle a annonc� qu'elle clarifierait les rumeurs qui circulent en d�but de semaine prochaine.
LaD�p�che.fr
