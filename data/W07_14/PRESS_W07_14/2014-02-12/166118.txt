TITRE: Un 'nettoyage ethnique' frappe la Centrafrique
DATE: 2014-02-12
URL: http://www.lefigaro.fr/flash-actu/2014/02/12/97001-20140212FILWWW00019-un-nettoyage-ethnique-frappe-la-centrafrique.php
PRINCIPAL: 0
TEXT:
le 12/02/2014 à 06:37
Publicité
L'ONG Amnesty International a interpellé mercredi dans un communiqué l'opinion internationale sur le "nettoyage ethnique" de civils musulmans qui se déroule selon elle dans l'ouest de la Centrafrique. "Les soldats de la force internationale de maintien de la paix ne parviennent pas à empêcher le nettoyage ethnique des civils musulmans dans l'ouest de la République centrafricaine", écrit l'ONG dans un communiqué , appelant la communauté internationale à "faire barrage au contrôle des milices anti-balaka et déployer des troupes en nombre suffisant dans les villes où les musulmans sont menacés".
Preuves d'épuration ethnique en RCA par Amnesty_France
La situation en Centrafrique s'est détériorée après le renversement du président François Bozizé en mars 2013 par Michel Djotodia et la rébellion Séléka, qui avaient pris les armes fin 2012. Depuis le départ forcé de M. Djotodia en janvier, dont le mouvement, à majorité musulmane, a été accusé de nombreuses exactions a l'égard des populations chrétiennes, le pays a sombré dans une spirale infernale de violences interconfessionnelles, avec l'apparition des "anti-balaka", des milices d'autodéfense paysannes à dominante chrétienne, décidées à se venger de la Séléka comme des civils musulmans.
