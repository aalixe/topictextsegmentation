TITRE: Twitter : vers un nouveau design de profil | Gossy
DATE: 2014-02-12
URL: http://www.gossymag.fr/twitter-vers-un-nouveau-design-de-profil-art55784.html
PRINCIPAL: 166629
TEXT:
Nouveau look pour une nouvelle vie ? Twitter aurait pris la d�cision de changer le design de ses pages de profil. Actuellement en phase de test, la nouvelle pr�sentation semble plus moderne et adapt�e aux publicit�s.
Nouveau look proche de Facebook ?
Les r�seaux sociaux ont parfois besoin d�un relooking extr�me pour ne pas se d�moder. En ce moment c�est le petit oiseau bleu de Twitter qui d�cide de tester un nouveau design plus moderne pour ses pages de profil. Cette nouvelle version tr�s diff�rente de la pr�c�dente se rapproche dr�lement du design de Facebook ou d�un Google+ et inspir� par celui de Pinterest.
Twitter teste avant d�agir
Il n�y a encore pas si longtemps, Twitter revisitait son portail web. Avant de changer d�finitivement le design de son r�seau, le groupe pr�f�re effectuer des tests sur un petit nombre chanceux de twittos. Cette version n�est qu�un essai et n�a pas encore �t� confirm�e par le r�seau. Pour les curieux, Mashable publie une capture d��cran qui d�voile le nouveau look de Twitter.
On remarque donc que ce nouveau design met en avant les images et les ��tuiles�� d�informations. Au lieu de lire de haut en bas ou de bas en haut, les utilisateurs peuvent avoir acc�s � leur ��timeline�� enti�rement gr�ce � une disposition en ��puzzle��. Comme son concurrent Facebook, Twitter a mis en place une photo de couverture en haut de la page du profil, et la photo de profil est plac�e � gauche. Parmi les nouveaut�s, il y a un acc�s rapide au nombre de photos ou vid�os partag�es ou encore au nombre de favoris qui s�ajoute � celui du nombre d�abonnements, d�abonn�s et au nombre de tweets.
Un lifting adapt� aux pubs ?
Twitter a sans doute pens� marketing avant esth�tique. Ce nouveau design semble effectivement beaucoup plus adapt� aux encarts publicitaires. Ainsi, l�objectif premier serait de renforcer le visionnage de la publicit� pour tenter de rattraper les derniers r�sultats financiers plut�t critiques.
Les r�actions � chaud
