TITRE: Michael Schumacher: la porte-parole Sabine Kehm ne confirme pas l'infection pulmonaire - Formule 1 - Eurosport
DATE: 2014-02-12
URL: http://www.eurosport.fr/formule-1/michael-schumacher-la-porte-parole-sabine-kehm-ne-confirme-pas-l-infection-pulmonaire_sto4132581/story.shtml
PRINCIPAL: 166236
TEXT:
Michael Schumacher: la porte-parole Sabine Kehm ne confirme pas l'infection pulmonaire
le 12/02/2014 � 09:40, mis � jour le 12/02/2014 � 12:38
Michael Schumacher , toujours hospitalis� � l'h�pital de Grenoble aurait �t� victime d'une infection pulmonaire selon le quotidien allemand Bild. Sa porte-parole, Sabine Kehm �voque des "sp�culations".
�
AFP
�
Michael Schumacher, aurait contract� une infection pulmonaire la semaine derni�re � l'h�pital de Grenoble, o� il est soign� depuis son accident de ski le 29 d�cembre, rapporte le quotidien allemand Bild mercredi. Les cons�quences pour l'�tat de sant� du septuple champion de F1 �g� de 45 ans, toujours dans le coma, ne sont pas pr�visibles, pr�cise Bild qui ne cite pas ses sources.
De son c�t�, la porte-parole de Michael Schumacher, Sabine Kehm n'a pas confirm�: "Comme toujours dans ce genre de cas, ma r�ponse est: les annonces sur l'�tat de sant� de Schumacher, qui ne sont pas faites par ses m�decins ou par sa manager (elle, en l'occurrence) doivent �tre trait�s comme des sp�culations", a d�clar� Kehm � l'agence allemande de presse DPA.
Fin janvier, les m�decins avaient entam� le processus de r�veil de l'ancien sportif de haut niveau, une phase qui pourrait durer longtemps. Mais l'h�pital de Grenoble avait d� d�mentir jeudi des rumeurs de la mort du champion diffus�es via les r�seaux sociaux.
Le pilote est hospitalis� � Grenoble depuis un accident de ski � M�ribel le 29 d�cembre. Sa t�te avait violemment heurt� un rocher en skiant en compagnie de son fils et d'un groupe d'amis. Lors de son admission � l'h�pital, Schumacher souffrait de l�sions cr�niennes "diffuses et s�rieuses" et avait �t� plong� dans un coma artificiel.
AFP
