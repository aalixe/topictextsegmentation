TITRE: Des �ruptions solaires comme vous n�en avez jamais vues - Sciences - Actualit� - LeVif.be
DATE: 2014-02-12
URL: http://www.levif.be/info/actualite/sciences/des-eruptions-solaires-comme-vous-n-en-avez-jamais-vues/article-4000526574199.htm
PRINCIPAL: 168384
TEXT:
Le Vif � Actualit� � Sciences � Des �ruptions solaires comme vous n�en avez jamais vues
Des �ruptions solaires comme vous n'en avez jamais vues
Source:�R�ponse � tout
mercredi 12 f�vrier 2014 � 16h40
Cela fait quatre ans que la NASA a mis en orbite SDO (Solar Dynamics Observatory), son observatoire solaire. A cette occasion, l�Administration nationale de l'a�ronautique et de l'espace am�ricaine a compil� les principales �ruptions solaires de l�ann�e 2013 dans une vid�o impressionnante.
�  Image Globe
Prouesses technologiques pour la captation de ces images
Il a fallu utiliser diff�rentes longueurs d�ondes et coloriser certaines images pour obtenir ce r�sultat. La vid�o montre le passage presque incessant des flammes sur la surface du Soleil. Mais c�est surtout la tr�s haute qualit� de r�solution des images qui rend cette vid�o spectaculaire. Les derni�res images sont consacr�es aux m�tamorphoses d�une immense tache solaire (plus de 200.000 kilom�tres) qui a pu �tre vue d�but 2014.
La mission de l�observatoire solaire am�ricain est d�une importance capitale, car ces �ruptions solaires peuvent causer des probl�mes, notamment ici-bas, sur la terre ferme. Une �ruption solaire est une �tape essentielle de l�activit� du Soleil qui se calcule en cycle de 11,2 ans en moyenne. Une �ruption sur cet astre qui fait�cent fois la taille de la terre peut perturber certains outils de t�l�communications, les vols d�avions ou encore�des missions spatiales.
Les infos du Vif aussi via Facebook
