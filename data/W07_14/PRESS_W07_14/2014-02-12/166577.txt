TITRE: Immigration : le gouvernement est "terroris�" par ces questions, juge Rama Yade - RTL.fr
DATE: 2014-02-12
URL: http://www.rtl.fr/actualites/info/politique/article/immigration-le-gouvernement-est-terrorise-par-ces-questions-juge-rama-yade-7769666634
PRINCIPAL: 166576
TEXT:
Rama Yade, ici � Paris le 29 janvier 2014, juge le gouvernement "terroris�" sur les questions d'int�gration.
Cr�dit : AFP / JOEL SAGET
La vice-pr�sidente de l'UDI a jug� que la feuille de route gouvernementale sur l'immigration �tait une "provocation � l'endroit des enfants immigr�s".
Pour Rama Yade, le gouvernement  est "terroris�" sur les questions d'int�gration. La vice-pr�sidente de l'UDI r�agissait � la pr�sentation mardi 11 f�vrier d'une feuille de route sur le sujet. "Sur la  forme, c'est un dossier qui se d�gonfle comme une baudruche, un rapport  creux et squelettique qui est la manifestation d'un gouvernement  terroris� par le sujet de l'int�gration dans un contexte explosif avec  la mont�e du Front National, le retrait de la loi sur la famille, le  r�f�rendum en Suisse", a indiqu� Rama Yade sur RFI.
"Dans le fond,  c'est une provocation. Les enfants d'immigr�s ne demandent pas l'aum�ne,  ils demandent l'�galit� r�publicaine", a-t-elle poursuivi. "Quand  on sait l'ampleur, l'ambition du programme de Fran�ois Hollande au  moment de la campagne pr�sidentielle, et (de voir) ces 20 pages de  rapport o� rien n'est dit, rien de substantiel n'est propos�, c'est une  provocation � l'endroit des enfants d'immigr�s et de ceux qui se battent  pour l'�galit� r�publicaine", a insist� l'ex-secr�taire d��tat du  gouvernement Fillon.
La "feuille  de route" du gouvernement pour l'int�gration des immigr�s et la lutte contre les  discriminations ne comprend en effet aucune annonce spectaculaire, ni moyen  suppl�mentaire, dans l'espoir d'�viter toute controverse.
La r�daction vous recommande
