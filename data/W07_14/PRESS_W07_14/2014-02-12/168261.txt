TITRE: Des attaques par d�ni de service d�une ampleur in�gal�e secouent le web
DATE: 2014-02-12
URL: http://www.01net.com/editorial/614026/des-attaques-par-deni-de-service-d-une-ampleur-inegalee-secouent-le-web/
PRINCIPAL: 168260
TEXT:
? Benjamin Sonntag (@vincib) 10 F�vrier 2014
La puissance de ces attaques est, en tous les cas, exceptionnelle. � Plusieurs centaines de Gbits, c�est �quivalent au trafic g�n�r� par un pays de taille moyenne. C�est suffisant pour faire dispara�tre un op�rateur ou un h�bergeur de la carte l�Internet��, explique Matthieu Texier, ing�nieur-conseil s�curit� chez Arbor Networks, qui a �galement d�tect� ces attaques sur ses radars, et bien d'autres encore ces derni�res semaines. � On assiste � une v�ritable recrudescence de ce type de malveillance �, souligne-t-il.
Mais comment les attaquants ont-ils pu atteindre un tel d�bit�? Ils ont proc�d� � des attaques dites ��par r�flexion�� ou ��par amplification��, en utilisant non pas des serveurs DNS (comme pour l�attaque Spamhaus) mais des serveurs Network Time Protocol (NTP). Ces derniers sont utilis�s par n�importe quels ordinateurs et serveurs pour conna�tre l�heure et pouvoir se synchroniser.
agrandir la photo
Le probl�me, c�est que ces serveurs sont souvent mal prot�g�s. Il est relativement facile de se faire passer pour quelqu�un d�autre (usurpation de l�adresse IP cible) et de leur envoyer plein de requ�tes. Les serveurs NTP vont alors transmettre leurs r�ponses � l�adresse IP cible. L�astuce, c�est qu�une r�ponse NTP est plus grande que la requ�te, d�o� l�effet d�amplification. Selon St�phane Bortzmeyer, ing�nieur r�seau, avec un paquet de 234 octets, il est possible d�obtenir ��des dizaines de milliers d�octets en r�ponse, donc un facteur d�amplification qui d�passe largement celui du DNS��, qui est ��aux alentours de 45��.
Par ailleurs, les attaques r�centes sont loin d��tre improvis�es, car il a fallu au pr�alable r�f�rencer un grand nombre de serveurs NTP vuln�rables. ��Ce n�est pas compliqu�, mais cela prend du temps��, souligne Matthieu Texier. Reste � savoir quel est le but de toute cette op�ration malveillante.�� Les attaques par d�ni de service peuvent avoir plusieurs raisons d��tre�: le vandalisme, l�activisme, l�extorsion, etc. Dans le cas pr�sent, il est probable qu�il s�agissait d�une d�monstration de force��, ajoute l�ing�nieur conseil. L�ann�e 2014 sera peut-�tre celle du d�ni de service distribu�.�
Source:
Articles de Bortzmeyer sur le facteur d'amplification DNS et les attaques DDoS par NTP
envoyer
