TITRE: 2014 : encore plus de tablettes que de PC en France
DATE: 2014-02-12
URL: http://www.igen.fr/ipad/2014-encore-plus-de-tablettes-que-de-pc-en-france-110084
PRINCIPAL: 167469
TEXT:
2014 : encore plus de tablettes que de PC en France
?
12/02/2014 - 16:42 - Florian Innocente
2014 devrait assoir en France la supr�matie des ventes de tablettes sur celle des PC. Dans son dernier bilan des march�s et biens techniques [ PDF ], GFk a un mot pour cette cat�gorie.
Sur l'ann�e en cours, il se vendrait 7,5 millions de tablettes dans l'hexagone, loin devant les portables PC (3,7 millions d'unit�s) et plus encore les PC de bureau (700�000). Entre les deux formats d'ordinateurs, la cat�gorie des hybrides PC/tablette joue des coudes et passerait en un an de 130�000 unit�s � presque un demi-million.
Le rouleau compresseur des tablettes a vraiment acc�l�r� l'an dernier, o� leurs ventes ont �t� pour la premi�re fois largement sup�rieures � celles de PC (lire aussi Apple veut profiter du d�clin du PC pour imposer le Mac ).
Une autre �tude, de Nielsen cette fois, s'est focalis�e sur les usages des consommateurs am�ricains. Elle montre qu'entre 2012 et 2013, le temps pass� chaque mois sur des apps ou un navigateur web avec des t�l�phones ou tablettes a d�pass� les 34h (+10 heures en un an) contre 27h pour le PC (-2 heures).
Les loisirs devant la TV reculent aussi, avec quasiment 134 heures mensuelles soit presque 3h de moins compar� � 2012. Enfin, 29% de ces sond�s ont une tablette contre seulement 5% il y a deux ans ( via ).
Apple 33,8% soit 14,1 millions d'iPad
Samsung 9,7 millions
Microsoft et ses tablettes Surface sont en revanche invisibles.
Source IDC - via ZDNet.fr/c...
DrLektroluv [12.02.2014 - 17:56]
Tablettes = inutile pour la plupart des quidams � part pour des usages sp�cifiques (d�marcheurs avec une pr�sentation, m�decin avec une liste de m�doc,... etc)...
Franchement, autant se payer un pc portable plut�t qu'une tablette... Parce que si c'est pour surfer + bureautique: pc > tablette
Le pc a encore de belles ann�es devant lui!
http://www.pcworld.fr/pc-portable/actualites,pc-portable-marche-2014-pre...
"Les fabricants d'ordinateurs portables viennent de revoir leurs pr�visions de ventes � la baisse pour l'ann�e 2014, expliquant ce pessimisme ambiant par deux principaux facteurs : la concurrence des tablettes et le report des processeurs Broadwell d'Intel. Cela concerne la plupart des grands acteurs du march�, d'Asus � HP en passant par Lenovo, Samsung, Sony ou Toshiba, sans oublier Apple. Selon les chiffres du cabinet NPD DisplaySearch, les neuf principales marques d'ordinateurs portables (qui cumulent 82% des ventes) devraient livrer 134 millions de machines en 2014, au lieu des 152 millions pr�c�demment estim�s.
Ainsi, comme ce fut le cas en 2013, il est fortement pressenti que les ventes d�clinent encore sur ce march� en 2014. Si en 2012 il s'�tait vendu 214 millions de PC portables dans le monde au total, toutes marques confondues, ce chiffre est tomb� � 180 millions en 2013 et pourrait descendre � 163 millions en 2014."
http://www.journaldunet.com/solutions/systemes-reseaux/marche-des-pc-fra...
D�clin du march� PC : la France limite la casse en Europe
"Apr�s IDC, c'est au tour du cabinet d'�tudes Gartner de publier ses chiffres sur le volume de PC �coul�s dans la zone Europe Moyen-Orient Afrique. Des donn�es qui tiennent compte du nombre de postes de travail en circulation dans les r�seaux de vente et chez les distributeurs, et non pas des PC achet�s par les clients finaux.
Au global, 14,82 millions de PC ont �t� �coul�s en zone EMEA, contre 16,73 millions un an auparavant. Ce qui repr�sente une baisse de 11,4% d'une ann�e sur l'autre.
Mais, tous les fournisseurs n'ont pas �t� touch�s de la m�me fa�on par le recul de la demande. Alors que chez Acer, la baisse du nombre de PC livr�s atteint 45,1%, chez Dell elle est de 10% contre 7,5% pour HP. Seuls Asus et Apple ont pu tir� leur �pingle du jeu avec une progression respective de 20,3% et 19,6% du nombre d'unit�s �coul�es.
Le march� des PC professionnels en progression de 10% en France
"Le march� des PC mobiles a �t� particuli�rement difficile avec une chute de 12,6%, tir�e vers le bas par une baisse e 40% du volume des livraisons de mini netbooks au troisi�me trimestre 2011", note le Gartner.
Le march� des PC professionnels en progression de 10% en France
Une tendance loin d'�tre cependant homog�ne dans tous les pays. En particulier en France o� le segment de march� de l'ultra mobilit� a baiss� de 21% contre -11% pour celui des postes de travail fixes. Malgr� tout, le march� des PC mobiles (comprenant les portables, les netbooks et les ultra portables) a progress� de 3% en volume. Un march� qui repr�sente aujourd'hui 69% de l'ensemble des PC �coul�s en France, et dont le dynamisme doit beaucoup au segment professionnel.
...
En d�pit d'un contexte �conomique d�favorable, le march� des PC fran�ais est donc parvenu � limiter la casse par rapport � ses voisins europ�ens. Notamment l'Allemagne et surtout le Royaume-Uni qui affichent un nombre d'unit�s livr�es en recul respectivement de 7,9% et de 10,8%."
Barbababar [12.02.2014 - 21:27] via iGeneration pour iPad
@DrLektroluv
Je suis un quidam pratiquement Power-User. Je viens de finaliser l'enregistrement d'un morceau sur mon iPad et je n'ai qu'un iPad. �crire des textes, livres, web, musique, youtube, cours... Je fais tout avec.
J'enregistre ma guitare et ma basse, je cr�e les batteries, j'ai toute une pl�thore d'effet int�gr�es.
N'importe quel PC a plus de possibilit�s. Un Mac est sacr�ment plus puissant.
Mais rien n'�galera l'attractivit� et l'aisance de mon iPad. Et c'est pour cela que je ne le regrette pas.
Ducletho [12.02.2014 - 23:52]
D'accord avec toi.
Pour le d�clin du PC, dans les foyers : il est  moins utilis�s pour le surf quand il y a une tablette, mais il reste un �l�ment n�cessaire dans un foyer, pour g�rer les albums photos et autres fichiers.
Le d�lai renouvellement a consid�rablement augment�. Mais il n'y a pas moins de PC en service dans le monde qu'en 2012 ou 2011...
Le taux d'�quipement a t'il baiss� ? Je pense pas, par contre il a d� arriver � son apog�e.
Apr�s dans le monde professionnel, un renouvellement de parc : 300 postes � changer par 200 postes >>> moins de personnel donc moins de PC dans les entreprises : Une des principales raisons.
Mon hypoth�se :
Une fois que le march� des smartphones sera orient�  grand format de t�l�phone + montre connect�e, la tablette sera l'objet qui sera sacrifi�e.
Le PC portable prendra la place de la tablette,car tactile et plus  autonome, au moment du renouvellement.
philoo34 [13.02.2014 - 01:49] via iGeneration pour iPad
@Ducletho
"Une fois que le march� des smartphones sera orient� grand format de t�l�phone + montre connect�e, la tablette sera l'objet qui sera sacrifi�e.
Le PC portable prendra la place de la tablette,car tactile et plus autonome, au moment du renouvellement."
Ben moi je pense tout l'inverse .
Une fois que la tablette 3G sera le mod�le de base avec les abonnements data pour que dalle , le t�l�phone reprendra son form factor initial , qui ne servira plus qu'� t�l�phoner , ou alors juste un p�riph�rique pour t�l�phoner comme un autre pour la tablette 7" par exemple . Du coup pour seulement  t�l�phoner on y gagnera en discr�tion .
Quand aux portables qui remplaceront les tablettes , c'est oublier l'�volution des tablettes et consid�rer le march� comme boucl�, ce qui �videmment est faux .
Faire une hypoth�se sur du mat�riel qui n'existe pas encore .. Mouaih ..
Le portable n'�voluera plus, la tablette par contre � encore beaucoup de chose � nous montrer .
