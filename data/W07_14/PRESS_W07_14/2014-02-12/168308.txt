TITRE: Canoe ? Infos ?  La pol�mique sur les mammographies relanc�e
DATE: 2014-02-12
URL: http://fr.canoe.ca/sante/archives/2014/02/20140212-112056.html
PRINCIPAL: 168307
TEXT:
�
Photo Fotolia
La pratique de mammographies annuelles ne permet pas de r�duire la mortalit� par cancer du sein, selon une �tude canadienne qui relance la pol�mique autour de l'int�r�t des campagnes de d�pistage organis�.
R�alis�e sur pr�s de 90 000 femmes �g�es de 40 � 59 ans, suivies pendant 25 ans, l'�tude a montr� que les femmes qui avaient subi des mammographies annuelles pendant cinq ans n'avaient pas moins de risque de mourir d'un cancer du sein que celles ayant seulement b�n�fici� d'un examen physique.
Au bout de 25 ans, 500 d�c�s par cancer du sein �taient survenus chez les 44 925 femmes suivies par mammographies contre 505 d�c�s chez les 44 910 femmes du groupe t�moin.
Les femmes avaient �t� assign�es dans les deux groupes par tirage au sort.
Les tumeurs du sein d�tect�es �taient en revanche plus nombreuses dans le 1er groupe, soit 3 250 au total contre 3 133 dans le second � la fin de l'�tude.
Le d�s�quilibre �tait d�j� net au bout de cinq ans, avec 666 cancers d�tect�s chez les femmes sous mammographies contre 524 dans le groupe t�moin, soit un �exc�dent� de 142 tumeurs.
Cet �exc�dent� �tait encore de 106 tumeurs au bout de 15 ans, ce qui, selon les auteurs, �signifie que 22% des cancers diagnostiqu�s dans le premier groupe ont �t� surdiagnostiqu�s�.
Les tumeurs �taient de surcro�t plus petites (1,4 cm dans le premier groupe contre 2,1 cm dans le second) au moment du diagnostic.
D�pistage organis� et surdiagnostic
Le surdiagnostic fait r�f�rence � la d�tection de tr�s petites tumeurs qui n'auraient pas eu d'impact du vivant de la personne concern�e.
En se fondant sur des �tudes montrant une baisse de la mortalit�, de nombreux pays occidentaux ont mis en place des programmes de d�pistage organis� du cancer du sein. En France par exemple, le programme s'adresse � toutes les femmes �g�es de 50 � 74 ans qui sont invit�es � faire des mammographies tous les deux ans, prises en charge � 100% par l'Assurance maladie.
Mais d'autres �tudes sont plus contradictoires: selon une �tude de la Collaboration Cochrane, publi�e pour la premi�re fois en 2000 et r�guli�rement r��valu�e depuis, le taux de mortalit� des femmes d�pist�es ne serait gu�re diff�rent de celui des autres femmes.
Une �tude britannique publi�e en 2012 avait au contraire estim� que le d�pistage organis� du cancer du sein sauvait des vies mais entra�nait un surdiagnostic estim� � pr�s de 20% des cancers d�pist�s.
S'exprimant en septembre dernier, le Dr J�r�me Viguier, le directeur du P�le sant� publique et soins de l'Institut national du cancer (INCa-France) avait pour sa part estim� que la controverse �tait �scientifiquement r�gl�e�. Il avait ajout� que selon les derni�res �tudes, les programmes de d�pistage organis� avaient permis de r�duire la mortalit� par cancer du sein de 15 � 21% et d'�viter 150 � 300 d�c�s pour 100 000 femmes participant de mani�re r�guli�re au d�pistage pendant 10 ans.
�Nos r�sultats rejoignent les vues de certains commentateurs qui estiment que les politiques de d�pistage par mammographies devraient �tre revues� dans les pays d�velopp�s, estiment les auteurs de l'�tude canadienne.
Mais selon eux, la chose ne sera pas facile �parce que les gouvernements, les organismes de financement de la recherche, les chercheurs et les m�decins peuvent avoir int�r�t � poursuivre des activit�s qui sont bien �tablies�.
