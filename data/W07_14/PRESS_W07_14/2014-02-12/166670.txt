TITRE: L�a Seydoux et Vincent Cassel racontent La Belle et la B�te - L'Express
DATE: 2014-02-12
URL: http://www.lexpress.fr/styles/vip/conte-d-acteurs_1323041.html
PRINCIPAL: 166669
TEXT:
L�a Seydoux et Vincent Cassel dans La Belle et la B�te.
Luciana Val et Franco Musso pour L'Express Styles
La Belle et la B�te croise, comme on le sait, les th�mes de la vertu, de la sexualit�, des d�mons int�rieurs...�
L�a Seydoux : Oui, une jeune femme perd son innocence, sa virginit�...�
Vincent Cassel: En tombant amoureuse d'un prince charmant maudit. Il a �t� puni de son avidit�, de son �go�sme.�
L. S.: La Belle voit l'humain en lui. La beaut� int�rieure triomphe.�
V. C.: La beaut� du diable. Car lui est dangereux, il la maltraite, l'emp�che de sortir. Mais il lui fait des cadeaux. Et finalement, elle craque.�
Malgr� toutes les r�volutions f�ministes, le mythe du prince charmant n'a �trangement jamais vacill�?�
L. S.: Les petites filles ont toujours envie d'�tre sauv�es par l'amour et par un homme. La r�alit� leur apprend que ce prince charmant n'existe pas, ou seulement pour quelques heures, quelques mois, quelques ann�es.�
C'est l'histoire d'une nana de bonne famille qui tombe amoureuse d'un monstre, en plus c'est son ge�lier. C'est compl�tement tordu, quand on y r�fl�chit�
V. C.: Que derri�re lui se cache parfois un monstre, pr�t � jaillir. Et vice versa. C'est la chanson de Claude Nougaro , Une petite fille: "Seulement y a le temps/Et le moment fatal o� le vilain mari/Tue le prince charmant..." La Belle et la B�te, c'est l'histoire d'une nana de bonne famille qui tombe amoureuse d'un monstre, en plus c'est son ge�lier. C'est compl�tement tordu, quand on y r�fl�chit.�
Se transformer en Belle, c'est jouer � la princesse?�
L. S.: Oh oui ! J'ai eu l'id�e de placer des bijoux dans mes cheveux. J'ai donn� mon avis sur les costumes pour raconter l'attitude de Belle envers la B�te . Son allure se transforme peu � peu. Au d�but, sa robe est serr�e � la taille, ensuite elle en porte des moins rigides, de style Empire. De toute fa�on, tous les films sont en costumes.�
V. C.: Moi, il me suffisait d'enfiler ma tenue de B�te, comme un acteur le ferait pour Hulk . Ensuite, c'�tait le travail des effets sp�ciaux. Gr�ce au d�guisement, on se laisse aller. Je n'ai pas toujours pens� ainsi. Pendant des ann�es, j'ai discut� de la moindre cravate. Aujourd'hui, je ne trouve pas inint�ressant de me sentir dans une position inconfortable avec un v�tement. Je suis plus mall�able.�
Juste pour les cravates ou pour le reste aussi?�
V. C. : Pour le reste, il faut demander � mon entourage. En tout cas, maintenant, pendant un tournage, j'accepte les remarques qui viennent de l'ext�rieur. J'ai appris cela avec ma femme. Tout m'�nervait, elle rien. Monica (Bellucci) m'a dit: "Arr�te de tout prendre au s�rieux. Moi, quoi qu'il se passe, je me sentirai bien". Elle avait raison. A la fois, je garde la capacit� de claquer une porte, de tout p�ter, mais ce n'est pas mon but.�
Un plateau est un microcosme fragile�
L. S. : Je suis quelqu'un de facile. Je trouve qu'il vaut mieux garder son �nergie pour le travail. Et apprendre � g�rer l'ego de chacun. Vincent, tu m'as fait tellement rire entre les prises, et m�me pendant. Tu �tais le Kiki de tous les Kiki. Mais, c'est vrai, il ne faut pas t'emb�ter!�
V. C. : Un plateau est un microcosme fragile. C'est tr�s simple de faire monter le stress, on �l�ve la voix et tout le monde s'inqui�te. Le stress doit �tre utile, il ne doit pas aider � se rassurer. Apr�s, certains ont besoin de souffrir et de faire souffrir autour d'eux.�
�
>>> A lire aussi, le diaporama: L�a Seydoux et Vincent Cassel rejouent La Belle et la B�te�
Luciana Val et Franco Musso pour L'Express Styles
�
Comment vous d�finiriez- vous l'un l'autre?�
V. C. : Tout ce que fait L�a est naturel, on dirait qu'elle ne joue pas. Elle n'est jamais fausse, on y croit toujours. C'est une qualit� qui lui est propre. �
L. S. : J'ai beaucoup de mal � tricher au cin�ma. V. C. : Cela veut dire que tu mens dans la vie? L. S. (Elle rit): Ni au cin�ma ni dans la vie. �
V. C. : Tu verras, tu apprendras! �
L. S.:Vincent pratique beaucoup l'autod�rision. ... /...J'aime bien son c�t� myst�rieux. C'est un com�dien tr�s connu dont on ne sait presque rien. �
V. C. : C'est mon c�t� Nuit de l'homme (NDLR : il a �t� l'ambassadeur du parfum Yves Saint Laurent ). Myst�rieux, je ne sais pas. J'ai l'impression qu'on en sait d�j� trop sur moi. Beaucoup sont persuad�s que je suis quelqu'un d'agressif et un peu m�chant. Mais on est responsable de son image, du personnage que l'on a cr�� autour de soi.�
Dans mon quartier, personne ne va voir mes films. Sinon, je continue � prendre le m�tro�
Etes- vous d'accord, L�a ?�
L. S. : Oui, il y a toujours une forme de v�rit� dans la fa�on dont on vous voit. Les gens ne te tapent pas dans le dos, Vincent, non?�
V. C. : Lorsque l'on interpr�te des mafieux, des personnes violentes, d�sagr�ables, personne ne vient vers nous. Mais on continue � me lancer: "C'est � moi que tu parles, encul�!" Si l'on se rappelle des r�pliques qui remontent � presque vingt ans (La Haine), alors tout va bien. Toi non plus, L�a, les gens ne t'abordent pas dans la rue.�
L. S. : Dans mon quartier, personne ne va voir mes films. Sinon, je continue � prendre le m�tro...�
V. C. : Au cin�ma tu as une image glamour, mais dans la rue tu te planques, tu portes un manteau, un bonnet. Tu ne projettes pas ta f�minit�.�
L. S. : Je pourrais, mais cela m'amuse de ne pas le faire. Ce qu'il y a de bien avec toi, c'est que tu dis toujours ce que tu penses.�
V. C. : Le probl�me, c'est que, souvent, je ne pense pas grand-chose.�
Vous non plus L�a, vous n'�tes pas du genre langue de bois.�
V. C. : Parfois, tu ferais mieux d'attendre six mois (NDLR : il fait allusion � la pol�mique sur La Vie d'Ad�le).�
L. S. : Enfin, ce qui est fait est fait...�
Vincent, dans la pr�face de A mes amours, l'autobiographie de Jean- Pierre Cassel parue en 2005, vous �criviez: "L'acteur est un animal �trange, tour � tour puissant et faible, f�minin et masculin, g�n�reux et pingre..."�
V. C. : J'ai observ� chez mon p�re, et aussi chez moi et d'autres, cette aptitude � �tre g�n�reux de soi, et en m�me temps � se retrouver �conome de soi, car tellement concentr� sur son implication, sur son risque. �
L. S. : C'est l'engagement de l'acteur. Le fait d'�tre compl�tement avec l'autre.�
Mon p�re ne comprenait pas mes choix, et j'adorais cela�
Votre p�re disait aussi que vous lui faisiez penser � Patrick Dewaere?�
V. C. : Il se trompait. Je ne me sens pas du tout ainsi. Mon p�re ne comprenait pas mes choix, et j'adorais cela. J'ai tellement essay� de me d�marquer de lui! Par exemple, il ne savait pas quoi penser d' Irr�versible ou de Sheitan. Un jour, il m'a demand�: "Mais pourquoi fais-tu ce genre de films?" Et je lui ai r�pondu: "Mais pourquoi ne les as-tu pas faits?" Apr�s sa mort, j'ai trouv� un texte de lui, dans lequel il parlait des hommes en col�re. Et j'ai compris qu'il aurait aim� en �tre un.�
L'homme en col�re, c'�tait vous ?�
V. C. : Oui, et aussi mon fr�re, Mathias (NDLR : le leader du groupe de rap Assassin).�
L�a, aviez- vous aussi cette col�re en vous?�
L. S. : Une certaine col�re, oui, quand j'�tais plus jeune.�
Vous avez re�u l'an pass� la palme d'or pour La Vie d'Ad�le. Cette reconnaissance �tait- elle importante pour vos parents?�
L. S. : Je ne sais pas. C'est davantage par rapport � moi-m�me. Jouer la com�die est venu d'un instinct de survie assez fort. Gr�ce au cin�ma, j'ai trouv� une forme d'expression. Et une envie d'exister.�
�
