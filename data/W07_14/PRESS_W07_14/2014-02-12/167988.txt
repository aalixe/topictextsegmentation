TITRE: Sotchi 2014: Pas de m�daille en half-pipe pour Sophie Rodriguez... Le calvaire de Lamy-Chappuis... La cinqui�me journ�e des JO � revivre - 20minutes.fr
DATE: 2014-02-12
URL: http://www.20minutes.fr/article/1296798/20140212-sotchi-2014-suivez-5e-journee-jeux-olympiques-live-comme-a-la-maison
PRINCIPAL: 0
TEXT:
JEUX OLYMPIQUES - Le retour du ski alpin...
Les principaux r�sultats du jour
Descente dames: La Slov�ne Maze et la Suissesse Gisin championnes olympiques de descente, dans le m�me temps.
Patinage artistique: Les Russes Volosozhar/Trankov sacr�s en couples
Half-pipe snowboard dames : Kaitlyn Farrington titr�e, Sophie Rodriguez 7e
Patinage de vitesse: le N�erlandais Stefan Groothuis sacr� sur 1000 m
Luge biplace: le titre pour les Allemands Tobias Wendl et Tobias Arlt.
20h30: Allez, c'est fini pour aujourd'hui � Sotchi. On ferme ce live, merci d'avoir suivi cette journ�e des JO avec nous. BONNE SOIREE.
20h01: Bonne id�e pour les prochains JO.
Rugby in Antarctica!! Cool... (literally!) pic.twitter.com/SlGaIFeXw4
� IRB Total Rugby (@IRBTotalRugby) 12 F�vrier 2014
20h: Rediffusion du programme libre des couples en patinage artistique. On vous spoile le truc, ce sont les Russes qui gagnent � la fin.
19h56: Ca d�bat qualit� de neige sur France T�l�visions. Plut�t int�ressant pour le coup.
19h50: La m�daill�e de bronze veut devenir com�dienne et vient de nous faire un peu de Muriel Robin. Ce sketch l�.
19h46: Sinon J�r�me Alonzo r�agit � la victoire de Coline Mattel. Non, c'est tout.
19h34: Coline Mattel sur le plateau de France TV. On vous tient au courant de ce qu'il se dit.
19h27: ET CLARK QUI FINIT TROISIEME. SON COMPATRIOTE KAITLYN FARRINGTON EST CHAMPIONNE OLYMPIQUE.
19h25: Tr�s beau run de l'Australienne Torah Bright qui monte � la deuxi�me place entre les Am�ricaines Farrington et Teter. Mais il reste Kelly Clark. La patronne en th�orie.
19h19: "Je n'avais pas le niveau pour �tre sur le podium", envoie Sophie Rodriguez sur France TV. Franche au moins.
19h18: On va quand m�me continuer de suivre cette compet. On reste pros.
19h16: Je commencais � m'habituer...
Une journ�e sans m�dailles pour la d�l�gation fran�aise #Sochi2014
� Yann Bertrand (@YannBertrand) 12 F�vrier 2014
19h15: Bon je dis scandale mais j'y connais rien hein.
19h15: SCANDALE! 79.50 seulement!!! C'est officiel on n'aura pas de podium... Oh que c'est dur... Ca valait plus que �a...
19h14: On a une main qui touche la neige mais c'�tait vraiment du lourd.
19h13: Oui c'est pas mal! Oui oui oui oui...
19h13: ALLEZ SOPHIE! ALLEZ!
19h12: Farrinton a pris la t�te et elle fait la maline. Le podium est � 85.50...
19h12: La pression, la pression...
19h11: ALLEZ SOPHIE Y A MEDAILLE LA!!!
19h11: Oh ce run de la Ricaine! Solide, ultra-solide!
19h08: Allez retour de Farrinton. Elle reste sur un 87.5. Rivale pour Sophie �a, clairement.
19h07: Allez Liu Jiayu, c'est ton tour.
19h06: Et c'est fait elle tombe! Excusez moi pour ce manque de faire-play.
19h06: Allez tombe la Chinoise! Tombe!
19h06: Au tour de Shuang Li maintenant, qui a d�j� remplit son objectif: montrer la marque de ses moufles.
19h05: Allez la derni�re fille avant Sophie Rodriguez...
19h04: Et encore une chute... Elle essaie m�me pas de terminer dignement.
19h03: Ursina Haller en piste. La bise � nos amis suisses.
19h02: Raaah c'est dur, �a manque de vitesse.... C'est mort. Et elle termine sur les fesses...
19h02: ALLEZ MIRABELLE! TU JOUES TA VIE SUR CELUI-LA!!!
18h58: Et Mirabelle Thovex est 6e! Sur 12 c'est pas mal. Un miracle est n�cessaire pour le podium pour elle par contre...
18h57: Allez Sophie Rodrigues est provisoirement 4e avec 77.75. Il faudra �lever le niveau, et monter autour de 85, voire 87 ou 88 pour aller chercher le podium. Mais c'est possible.
18h56: Enorme crash de Kelly Clarke! Elle passe un 480 et se vautre dans la foul�e...
18h56: Sophie Rodriguez est actuelkement 4e... Il reste une concurrente... Et surtout un second run.
18h54: Oh elle chute l'Australienne! Elle nous fait une Shaun White!
18h53: Bon, a priori, Torah Bright devrait �craser la concurrence...
18h53: LE GIF ABSOLU DE SOTCHI
18h47: Par contre ras-le-bol de la Chinoise qui montre ses moufles pour faire de la pub.
18h47: Bon on a quand m�me du gros, gros niveau derri�re...
18h45: Allez l�, pas mal! Elle est 2e, avec 77.75... Il en reste quatre devant elle pour ce 1er run...
18h45: Et c'est pas mal �a!!! Pas tr�s fluide, mais pas de chute et des belles figures...
18h44: Elle part quand m�me hein.
18h43: Elle se vautre au d�part... UNE MARION ROLLAND!!!
18h43: SOPHIE RODRIGEZ!!! COME ON!!!!
18h41: Tr�s joli run de l'Am�ricaine Farrington... 85.75, premi�re place, en toute logique.
18h41: Question l�gitime.
� Guy Moux (@Guy_Moux) 12 F�vrier 2014
18h39: OH LE GADIN POUR UNE CHINOISE! Elle va bien, mais j'ai eu mal pour elle .
18h39: C'est fou le nombre de Chinoises dans cette finale.
18h38: Ah �a y est, une Chinoise passe devant Mirabelle.
18h36: Mirabelle Thovex est en t�te provisoirement. mais plus les concurrentes passent, meilleures elles sont...
18h35: Par ailleurs, je vous live ces JO de Sotchi sans avoir m�me jamais pass� ma 1er �toile, sachez le.
18h44: Allez �a manquait un peu de vitesse et d'amplitude parfois, mais elle repart avec un 67 points tout � fait honorable. Elle devra faire mieux apr�s, mais c'est un bon d�but.
18h33: OH LE BEAU RUN!!! Super passage pour la fran�aise!!! Ah �a fait plaisir...
18h32: Faut envoyer, pas de pression pour toi Mirabelle.
18h32: ALLEZ MIRABELLE!!!!
18h30: Allez c'est parti sur le demi tube de Sotchi. Je vous rappelle le principe: deux passages, on ne garde que le meilleur, et c'est boucl�.
18h26: Allez, le grand moment approche. C'est l'heure pour Sophie Rodrigues et Mirabelle Thovex!
18h15: Bon belle fin de programme ceci dit, mais �a ne suffira pas je pense.
18h11: Chute de Vanessa James sur le triple-triple. Dur.
18h: Le couple James/Cipres est sur la glace de Sotchi!! C'est l'heure du programme libre, apr�s avoiir fait 10es du court hier.
17h55: Rendez-vous dans quatre ans, Coline... Ce sera l'or!
La jeune Coline Mattel re�oit sa m�daille de bronze. Encore bravo. #Sochi2014 #JO2014 via @EspritGlisse pic.twitter.com/lSl2nXMMTv
� JO 2014 Sotchi (@_JeuxOlympiques) 12 F�vrier 2014
17h48: Et mais c'est l'heure du podium pour Coline Mattel! Va chercher ta breloque!
17h46: Et si on avait un d�but de pol�mique sur le rat� de Jason Lamy-Chappuis?
Nicolas #Michaud , patron du combin� nordique, a mis en cause le mat�riel de Jason Lamy Chappuis #Sotchi2014 VIDEO : http://t.co/tv0oM39qRE
� francetv sport (@francetvsport) 12 F�vrier 2014
17h43: En attendant le halfpipe, voici le programme des Fran�ais pour demain. Biathlon (20 km): Jean-Guillaume B�atrix, Alexis Boeuf, Martin Fourcade, Simon Fourcade - Ski de fond (10km): C�lia Aymonier, Aurore Jean- Ski freestyle: Antoine Adelisse, Jules Bonnaire, J�r�my Pancras - Patinage artistique (Programme court): Florent Amodio, Brian Joubert - Short track (1000 m): S�bastien Lepape, Maxime Chataignier, Thibaut Fauconnet
17h30: La doublette allemande de Tobias championne olympique de luge buplace. Tobias, pour moi, c'est surtout �a.
16h55: Sinon, on vient de m'annoncer que le fondeur Maurice Manificat avait d�clar� forfait pour le 15km de vendredi. Rassurez-vous, c'est juste pour se pr�server pour le 50km, pas de maladie, pas de vilaine blessure.
16h50: Ah on va vivre une belle petite fin de journ�e avec nos Bleues en finale. Ce sera � 18h30, et ce sera � vivre avec nous ici.
16h47: MAIS C'EST BON! Elle rejoint Sophie Rodriguez en finale...
[Halfpipe]: Belle perf de la fran�aise Mirabelle Thovex qui rejoint Sophie Rodriguez en finale #JO2014 @EspritGlisse pic.twitter.com/G9C4SmAEao
� JO 2014 Sotchi (@_JeuxOlympiques) 12 F�vrier 2014
16h46: LA CHUTE. C'est ballot, elle �tait pas mal... Fau croiser les doigts pour que son score sur le 1er run suffise...
16h45: La voil�, tout en haut du demi-tube!
16h44: Elle devrait plus tarder, Mirabelle Thovex...
16h40: A�e, a�e, a�e... Grosse chute de Cl�mence Grimal sur le 2e run. C'est mort pour la finale...
16h34: Fin du 1er run dans les 1/2 du half-pipe f�minin. Bonne nouvelle: Mirabelle Thovex est 5e (les 6 premi�res sont qualifi�es). Mauvaise nouvelle: Cl�mence Grimal est derni�re � cause de sa chute. Attention, on ne gtarde que le meilleur score sur les deux manches, tout peux changer...
16h27: Je mise sur deux Fran�aises en finale. Pas plus.
16h18: Bon, la Suissesse Ursulla Haller pique la 1ere place � notre fran�aise. On n'aura pas profit� longtemps.
16h16: Mais oui! Premi�re place provisoire pour l'enfant de La Cluzas!
16h15: Pas de chute pour la Fran�aise! Mais �a manquait un peu de vitesse et d'amplitude.
16h14: Allez Mirabelle Thovex! C'est le moment!
16h12: @MagicBamby On a tous les droits mon bon. Apr�s, en half-pipe, il se passe quand m�me des choses aujourd'hui.
16h10: Je vous rappelle que quoi qu'il arrive, il y aura bien une Fran�aise en finale, puisque Sophie Rodriguez s'est qualifi�e directement.
16h07: Et c'est parti pour les 1/2 finale du half-pipe. Et Cl�mence Grimal est tomb�e sur son 1er passage.
15h56: Et sinon, la BBC a fait la descente de Rosa Khutor avec une Go Pro. Comme si vous y �tiez.
15h43: C'est fini!!! Fin de match � haute intensit�, mais victoire finale du Canada (3-2) contre les States.
15h39: Et c'est gol! But des USA... plus qu'un de retard dans ce qu'on peut �videmment consid�rer comme un derby de la glace.
15h34: Et pendant ce temps-l�, le Canada m�ne 3-1 contre les USA au hockey. Question b�te: est-ce qu'il ya� des bagarres, comme chez les mecs?
15h28: Il est battu le Fran�ais, d'une demie seconde. Il est � 1sec50 du leader. Loin donc.
15h26: On a Benjamin Mac� qui s'�lance sur le 1000m du patinage de vitesse. Vas-y camarade!
15h20: C'est parti pour la luge � deux par ailleurs. Epreuve assez �trange, je dois bien l'avouer. Et �a ressemble � �a.
15h19: Bon, apr�s son gros fail, Jason Lamy-Chappuis est venu s'expliquer. Son interview c'est par ici.
15h12: OUCH! Nicolas Burtin, le chef du groupe vitesse chez les filles, d�zingue Marie marchand-Arvier. C'est � lire chez nos confr�res de BFM .
14h52: Alors �a se confirme, on a deux Fran�aises en 1/2 finale du half-pipe: Mirabelle Thovex et Cl�mence Grimal. Ce sera � 16h les amis.
14h47: Si vous �tes fans de hockey, on ungros match en ce moment: USA/Canada, chez les filles, 0-0 pour le moment.
14h06: C'est l'heure de la #MinuteEspritOlympique. En ski de fond, un entra�neur canadien a port� secours � un russe qui avait p�t� un ski en lui en refilant en en �tat de marche. "C'�tait comme regarder un animal pris dans un pi�ge, je ne pouvais pas rester l� et ne rien faire", a-t'il expliqu�.
14h23: Bon, au half-pipe, chez les filles, on a deux Fran�aises en course pour la qualification en 1/2 finale. Toutes les concurrentes ne sont pas encore pass�es.
14h09: Lamy-Chappuis sur France TV: "C'�tait un calvaire jusqu'au bout. Ca m'�tait d�j� arriv� mais �a ne se voit pas, c'est en Coupe du monde. Ca ne veut rien dire, ce n'est clairement pas mon niveau."
13h57: Splendide course de Jason Lamy-Chappuis qui finit... 35e. Voil�.
13h42: Pendant ce temps, on apprend que le patineur n�erlandais Sven Kramer d�j� sacr� champion olympique du 5000 m � Sotchi, ne s'alignera sur le 1500 m. On vous rassure, il y aura un autre Batave pour faire une m�daille.
13h28: Ah, elle envoie Torah Bright.
13h25: Bon, on va bient�t passer au combin� nordique avec Jason Lamy-Chappuis. Pour suivre sp�cifiquement la course, c'est par ici.
13h20: En gros, on fait repasser les concurrentes en half-pipe pour d�terminer d'autres finalistes tandis que d'autres iront en demi-finale pour tenter de les rejoindre. Euh...
13h17: On compare le half-pipe au motocross sur France TV. Et le bobsleigh c'est de la Formule 1?
13h10: Bon, ce n'est pas dit que �a rejoigne Sophie Rodriguez en finale tout �a. Mais on croise les doigts hein.
13h08: A�e, elle fait n'importe quoi. C'�tait nul, il faut le dire.
13h07: Elle prend la premi�re place provisoire. En attendant l'autre fran�aise Cl�mence Grimal. Ben qui part maintenant tiens.
13h06: Une Bleue en half-pipe, Mirabelle Thovex, s'�lance pour tenter de rejoindre Sophie Rodriguez en finale.
12h22: On vous rassure, on est toujours aussi peu passionn� par ce d�riv� de la p�tanque. Le curling, ce myst�re...
12h14: Finale � 18h30 pour Sophie Rodriguez. Pour le moment petite pause curling avant la deuxi�me manche du combin� nordique pour Jason � 13h30.
12h07: Bon c'est vrai elle peut remercier ses concurrentes qui se sont toutes cass�es la gueule derri�re. Mais pas grave, on prend quand m�me
12h05: ELLE EST EN FINALE ! Troisi�me place directement qualificative pour Sophie Rodriguez gr�ce � son premier run !
12h03: Il ne reste plus qu'une concurrente � passer ! Ca sent bon la finale cette histoire...
12h00: Pour rappel les trois premi�res sont directement qualifi�es en finale et les six suivantes passeront par les demies
11h57: Elle est toujours troisi�me Sophie Rodriguez ! La demie est quasiment assur�e et on peux commencer � r�ver d'une qualif en finale directe !
11h48: Chute sur chute dans ce deuxi�me run. En �tant totalement chauvin, �a arrange les affaires pour Sophie on va pas se plaindre
11h43: Kelly Clark, la "Shaun White au f�minin" r�alise un deuxi�me run quasi-parfait et reprend la premi�re place � l'espagnole Castellet avec une note de 95.00.
11h35: Bon elle chute sur le deuxi�me run. Esp�rons que le premier soit suffisant mais �a va �tre tendu pour la qualif directe en finale...
11h32: Deuxi�me run � venir...On pousse tous derri�re Sophie, on la veut cette finale !
11h31: Sophie Rodriguez est pour l'instant troisi�me derri�re Queralt Castellet et Kelly Clark apr�s le premier run !
11h05: Kelly Clark envoie le p�t� juste derri�re Sophie. 92.25, les bases sont pos�es !
11h01: Une note de 78.50 pour Sophie. C'�tait la premi�re partante, on va attendre les autres concurrentes et esp�rer maintenant. Bon on va pas vous le cacher, pour une place directe en finale �a semble l�ger...
11h00 : Elle a assur�, pas de prise de risque. Pas de chute au moins, on dira que c'est le principal
10h59: Sophie Rodriguez se lance sur l'�preuve du half-pipe en snowboard. On y croit !!
10h25: On va bient�t basculer sur le combin� nordique. Jason, c'est � toi. Et c'est par ici.
10h10:� Pour revoir la chute de MMA
9h54: La voil� la photo du podium des ex aequo.
L'image du matin : Dominique Gisin et Tina Maze championnes olympique de descente main dans la main sur le podium pic.twitter.com/qZfgAYDOiK
� JO 2014 Sotchi (@_JeuxOlympiques) 12 F�vrier 2014
9h48: C'est mignon ce podium de la descente avec Maze et Gisin qui se tiennent la main pour aller chercher l'or.
9h20: Bon, il y a un petit creux � Sotchi. On en profite pour petit d�jeuner. Comme d'habitude, posez nous vos questions, on y r�pondra avec toute la gentillesse qui nous caract�rise.
9h18: Au passage, �a manque de neige sur le tremplin de saut � ski, la preuve.
Heureusement, il y a un peu de neige sur le tremplin... #combin�nordique #summergames #sotchi2014 pic.twitter.com/0fYdzVS8nN
� Eric Bruna (@ebruna1973) 12 F�vrier 2014
9h15: Sinon, va falloir sortir la cr�me solaire.
Les JO de Sotchi sont le JO du printemps il fait 12 degr�s dans les montagnes aujourd'hui pic.twitter.com/rg4p8QFtxG
� Lunzenfichter Alain (@ALunzenfichter) 12 F�vrier 2014
9h04: Le grand moment de la journ�e, surtout ne le loupez pas, c'est le combin� nordique de Jason Lamy Chappuis. Premi�re manche � 10h30, le saut. Deuxi�me � 13h30, le ski de fond.
8h59: Voil� le programme du jour, car il n'y a pas que de l'alpin aujourd'hui.
Day 6 is here already! It also means 6 sets of medals are at stake. Check out the #Sochi2014 schedule for the day! pic.twitter.com/7leo3DhYYd
� Sochi 2014 (@Sochi2014) February 12, 2014
8h52: Le podium de la descente femmes pour l'instant:
1. Tina Maze & Dominique Gisin
3. Lara G�t
8h49: Et donc, pendant ce temps, on va avoir deux m�dailles sur la m�me course. Comme Stravius Lacourt, si vous vous souvenez...
8h47: Marie Marchand-Arvier est au micro de FranceT�l�: "C'est frustrant. J'ai des douleurs au mollet. Je suis tomb�e assez violemment."
8h45: Non, truc de ouf, Tina Maze arrive dans le m�me temps, exactement le MEME TEMPS que Gisin. Elles vont �tre toutes les deux championnes olympiques.
8h42:� Le pull de Luc Alphand au r�veil, c'est rude.
R�VEILLEZ-VOUS!!! Les pulls JO de France t�l�visions sont pay�s avec nos imp�ts. Rdv dimanche place de la Bastille pour une manif citoyenne
8h40: !!!! Elle termine deuxi�me � 10 centi�mes derri�re sa compatriote gisin, qui a des faux airs d'Evangeline Lilly.
8h37: Voil� l'une des favorites, peut-�tre la favorite, une autre suisesse Lara G�t
8h35: Y a combien de Suisses exactement dans cette comp�tition? Ah bah une de moins, chute pour Kauffman-Haberald.
8h30: Pas de m�daille pour Julia Mancuso, qui avait fait 2e � Vancouver. Elle est 4e provisoire et il reste un paquet de favorites.
8h26: Au bout de 26 secondes de course... cela dit, bon, il lui reste le Super-G!
Marie Marchand-Arvier. Your head and neck shouldnt turn like that #Downhill #Olympics #France pic.twitter.com/tpJrFzqCKU
� ?????? (@KennedyMLB) February 12, 2014
8h24: C'est vrai qu'il y a une sacr�e vitesse, on voit les skieuses se faire balader par la piste...
8h20: Malgr� sa chute, "MMA" va bien selon le DTN. Elle est en "deficit de confiance" explique-t-il. "La neige est extr�mement rapide, il y a des filles qui s'adaptent mieux � cette neige".
8h18: Ce sont deux Suisses qui m�nent la course, Gisin devant Suter.
8h15: Premi�re grosse mauvaise nouvelle pour le clan fran�ais puisque Marie Marchand Arvier a chut� dans cette descente!!! La gigne
8h10: Salut � tous! Bienvenue sur 20minutes.fr pour cette journ�e � Sotchi. On d�marre tout de suite avec la descente femmes!
La descente, remport�e par l'Autrichien Mayer chez les hommes, connaitra-t-elle le m�me destin chez les femmes?�Peut-�tre.�Premi�re comp�tition de la journ�e � Sotchi, l'�preuve de vitesse du ski alpin lancera � merveille ce 5e jour de jeux olympiques, qui proposera aussi l'entr�e dans le game de Jason�Lamy-Chappuis, de la luge bi-place et plein d'autres trucs.�
>> Soyez avec nous d�s 8 heures du matin pour cette journ�e...
B.V.
