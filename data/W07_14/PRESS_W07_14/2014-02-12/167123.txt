TITRE: Tottenham: Lloris d�ment un d�part � Arsenal - Football - Sports.fr
DATE: 2014-02-12
URL: http://www.sports.fr/football/transferts/scans/tottenham-lloris-dement-un-depart-a-arsenal-1008979/
PRINCIPAL: 167117
TEXT:
12 février 2014 � 12h40
Mis à jour le
12 février 2014 � 12h49
Dans un communiqué officiel publié sur le site de Tottenham , Hugo Lloris a démenti les rumeurs qui le lieraient à Arsenal depuis l'interview accordée par Olivier Giroud à nos confrères de Téléfoot dimanche dernier. Le gardien avoue avoir entendu parler de ces rumeurs : "J'en ai entendu parler. Je pense que certains mots ont été mal interprétés, et c'est gênant. Je suis concentré sur les Spurs".
Le portier des Spurs a tenu à clarifier la situation, notamment pour les fans du club londonien : "J'ai trop de respect pour le club et ses supporters pour penser à ce genre de choses. Je suis un joueur des Spurs et je suis fier de jouer ici. Tout est réuni à Tottenham pour avoir un excellent avenir. Il faut juste que les joueurs travaillent dur pour atteindre nos objectifs".
Bien sûr, la rivalité entre Tottenham et Arsenal explique en grande partie cette réponse de l'international français, les joueurs faisant le trajet entre les deux clubs ayant toujours reçu un accueil peu chaleureux du côté des supporters. On se souvient notamment de Sol Campbell, qui était passé de Tottenham à Arsenal avant la saison 2001/2002, ou, plus récemment, de William Gallas , qui avait fait le chemin inverse en août 2010.
