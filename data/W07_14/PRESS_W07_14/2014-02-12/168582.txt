TITRE: Besan�on envoie par erreur des �coliers sur des sites X - Freenews : L'actualit� des Freenautes - Toute l'actualit� pour votre Freebox Revolution
DATE: 2014-02-12
URL: http://www.freenews.fr/spip.php?article14408
PRINCIPAL: 168581
TEXT:
Besan�on envoie par erreur des �coliers sur des sites X
Y clic, y clic... et il y est
Des sites web �ducatifs propos�s par la Ville sont devenus pornographiques.
Depuis 2002, la ville de Besan�on distribue aux �l�ves de CE2 des "cartables num�riques". Il s�agit en r�alit� d�un ensemble informatique complet (unit� centrale, �cran, clavier/souris...) accompagn� de logiciels ludiques et �ducatifs destin�s aux enfants et � leurs parents.
Ces logiciels se trouvent sur une cl� USB, qui contient �galement une liste de liens vers des sites web permettant de t�l�charger des mises � jour ou des outils compl�mentaires. Ce sont ces liens qui posent probl�me�: si on peut raisonnablement supposer que les liens ont �t� v�rifi�s avant la distribution des cl�s, deux des sites list�s ont entre temps �t� transform�s en sites pornos�!
La ville a envoy� hier un courrier d�excuses aux familles concern�es et proc�dera au remplacement de toutes les cl�s USB.
A l�avenir, on ne peut que conseiller � la municipalit� de ne plus proposer de liste de liens directement dans les cl�s USB, mais plut�t d�h�berger cette liste sur son propre site afin de pouvoir la mettre au jour si n�cessaire...
Il y a 55 j
Kami78 a �crit :
Au passage, quel est le probl�me des �coliers bisontins si leurs parents ont fait leur boulot de parents ? :P
Si tes parents avaient fait leur boulot de parents, ils t'auraient appris a ne pas raconter de telles inepties� ;)
Kami78
Il y a 55 j
Merci � Fansat d'�voquer d'autres mod�les. Car nous sommes totalement � la d�rive depuis que l'informatique est venu massivement compl�ter l'enseignement traditionnel dans tous les pays d�velopp�s sauf le n�tre. L'aveuglement des syndicats d'enseignants qui refusent l'informatique et l'�conomie nous maintient dans une crasse terrible qui commence � se traduire concr�tement.
Au passage, quel est le probl�me des �coliers bisontins si leurs parents ont fait leur boulot de parents ? :P
Fansat70
Il y a 56 j
En r�alit�, notre syst�me �ducatif est retomb� en enfance... On est imm�diatement accroch� sur tout nouveau proc�d� qui "brille", alors que l'on se d�sint�resse totalement de ce qui marche - quelque fois fort bien - ailleurs!
Comment fonctionne (au "hasard") le syst�me Finlandais qui est l'exemple m�me d'anti-syst�me fran�ais, avec une n�gation compl�te du h�las trop fameux "�galitarisme", mais avec cette "petite" particularit� de mettre l'accent � fond sur ceux justement qui ont le plus de difficult�s pour "suivre"...
La culture de l'�litisme a d�montr� que la soci�t� qui en r�sultait formait d eplus en plus de pauvres et d�rivait le plus souvent sur une culture de "pens�e unique", particuli�rement dans nos soci�t�s consum�ristes...
Bref, pour ne pas �tre dans la "bonne voie", on y est! :(
MSG
Il y a 56 j
Ces programmes informatiques de distribution de mat�riel c'est avant tout le renouvellement de la nouvelle g�n�ration Microsoft / Google.
�a participe au lavage de cerveau , d�s le plus jeune �ge , afin que ces jeunes ne connaissent autre chose que les produits de ces soci�t�s .
L'Europe devrait soutenir plus les initiatives sur les logiciels libres , ce qui � la longue , favoriserait l'ind�pendance et la cr�ation chez nous et non plus uniquement dans la "Silly con valley" .
