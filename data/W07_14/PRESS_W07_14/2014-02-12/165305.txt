TITRE: France: Nicolas Sarkozy fait un pas de plus vers le retour - France - RFI
DATE: 2014-02-12
URL: http://www.rfi.fr/france/20140211-france-nicolas-sarkozy-fait-pas-plus-vers-le-retour/
PRINCIPAL: 0
TEXT:
Modifi� le 12-02-2014 � 02:01
France: Nicolas Sarkozy fait un pas de plus vers le retour
L'ancien pr�sident de la R�publique, Nicolas Sarkozy, au premier rang du meeting de Nathalie Kosciusko-Morizet, le 10 f�vrier 2014.
REUTERS/Charles Platiau
Il n'�tait pas apparu dans un meeting depuis mai 2012. Nicolas Sarkozy �tait hier, lundi, l�invit� vedette du celui qu�organisait Nathalie Kosciusko-Morizet, la candidate UMP � la mairie de Paris. Et l�ancien pr�sident a quelque peu vol� la vedette � NKM.
Nicolas Sarkozy a v�ritablement �t� accueilli en h�ros par les 2�000 sympathisants UMP r�unis lundi soir au gymnase Japy. Des militants qui n�avaient d�yeux que pour Nicolas... Son pr�nom a d�ailleurs �t� longuement scand� par une foule en liesse. L�ancien pr�sident �tait venu ��par amiti頻 pour son ancienne porte-parole, Nathalie Koscisuko-Morizet dont la campagne a du mal � d�coller.
Son arriv�e s�est faite dans une v�ritable cohue. Pour l'anecdote, Nicolas Sarkozy a fait son entr�e au moment o� Marielle de Sarnez pronon�ait un discours. Une petite vacherie savamment calcul�e. L�ancien pr�sident n�a toujours pas dig�r� le fait que le MoDem, le parti de Marielle de Sarnez, ait appel� � voter pour Fran�ois Hollande au second tour de la pr�sidentielle.
Nicolas Sarkozy a ensuite �cout� attentivement le discours de sa prot�g�e, sans r�agir. A la tribune, cette derni�re lui a rendu plusieurs fois hommage. Puis l�ancien chef de l�Etat a quitt� les lieux, en lan�ant cette phrase�:���Nathalie fera une maire de Paris formidable��.
Une nouvelle �tape vers un retour sur le devant de la sc�ne�?
L�ancien pr�sident songe � la pr�sidentielle de 2017, ce n�est plus un secret pour personne. Il envoie r�guli�rement des petites cartes postales � ses adversaires � droite pour se rappeler � leurs bons souvenirs.
Lundi soir, l�ancien pr�sident a particip� non seulement � son premier meeting depuis mai 2012 mais il a surtout pris part pour la premi�re fois depuis la pr�sidentielle � une r�union politique. C�est l� un �l�ment majeur. Il �tait le seul t�nor de l�UMP � participer � ce meeting. Fran�ois Fillon qui est d�put� de Paris, n�avait pas �t� invit� par la candidate. En mettant en sc�ne son arriv�e, Nicolas Sarkozy veut montrer � ses futurs adversaires � droite qu�il est � ce jour le seul homme capable de susciter le d�sir. D'ailleurs, une �crasante majoritr� de sympathisants souhaitent son retour.
Apparitions tous azimuts
Il ne cesse de multiplier les apparitions. A Nice, en Savoie, o� il �tait all� � la rencontre d'�lus UMP, mais aussi dans plein d'autres villes de France, o� en tant que spectateur il assiste aux concerts de sa compagne, la chanteuse Carla Bruni. A chaque fois, le rituel est le m�me�:�l'ancien pr�sident serre des mains, prend des bains de foule.
Dernier rendez-vous marquant � Chatelaillon, fin janvier. Ce jour-l�, Nicolas Sarkozy avait �voqu� en filigrane son retour. ��Parfois les vacances me paraissaient longues, par la suite �a ne s'est gu�re am�lior�e��, avait-il d'abord lanc�. Avant d'ajouter un peu plus tard�:���L� o� la mer est repass�e, elle revient...��.
L'ancien chef de l'Etat se montre de plus en plus. Pour ne pas laisser trop d'espace � ses concurrents � droite, pour parvenir � revenir sur le devant de la sc�ne, l'ancien pr�sident devra cependant surmonter deux obstacles plac�s sur sa route : d'abord, �tre blanchi dans les diff�rentes affaires dans lesquelles sont nom appara�t. Ensuite, sortir vainqueur de la primaire � droite. Une primaire dont lui et ses amis ne veulent toujours pas entendre parler.
