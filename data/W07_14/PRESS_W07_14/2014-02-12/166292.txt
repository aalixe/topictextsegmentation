TITRE: Revivez toute l'�preuve du combin� nordique
DATE: 2014-02-12
URL: http://www.lemonde.fr/jeux-olympiques/live/2014/02/12/le-saut-du-combine-nordique-en-direct_4364818_1616891.html
PRINCIPAL: 0
TEXT:
Revivez toute l'�preuve du combin� nordique
Le Monde |
12h12
C'est la fin de ce live, merci � vous de l'avoir suivi avec nous. Tr�s bon apr�s-midi.
12h11
Etre porte-drapeau est un travail (r�pondre aux interviews, ...) qui peut nuire aux bons r�sultats !
Commentaire de la part de Visiteur
12h10
"Un jour comme �a, �a ne veut rien dire sur mon �tat de forme g�n�ral."
Olivier Bertrand
12h10
Mais il n'a pas l'air abattu pour autant et prend beaucoup de temps pour r�pondre aux questions.
Olivier Bertrand
Oui, tout � fait. Il r�p�te encore le mot de "calvaire". Olivier Bertrand
12h09
Il semble qu'il soit en train de s'exprimer en direct sur France 3 ? Commentaire de la part de Visiteur
12h09
"Apr�s un tour je sentais que les jambes ne r�pondaient pas", r�agit Jason, qui parle de "calvaire". "C'est un m�lange de physique et de mauvais choix tactiques au niveau du ski. Il faudra qu'on d�briefe." Olivier Bertrand
12h07
Qui est le dernier fran�ais porte-drapeau � ne pas avoir fait de contre performance lors de JO ?    Olivier Bertrand: En 1992 � Albertville, Fabrice Guy, porte drapeau, remporte l'or sur... le combin� nordique. Commentaire de la part de Marc
12h05
N'y a t'il pas une sorte de 'mal�diction' des portes drapeaux fran�ais ?     Olivier Bertrand: Ce n'est pas faux... En 2012, � Londres, l'escrimeuse Laura Flessel, porte drapeau, ne passe qu'un tour. En 2008, � P�kin, Tony Estanguet �choue en demi-finales. En 2010, � Vancouver, Vincent Defrasne, porte drapeau, n'avait rien fait de bon... Commentaire de la part de Marc
12h02
Apparemment Jason Lamy Chappuis ne souhaite pas parler � France T�l�visions. C'est une grosse d�ception. Olivier Bertrand
11h58
Non, il ne semble pas qu'il soit tomb�. Olivier Bertrand
11h58
est-il tomb� ? Commentaire de la part de Visiteur
11h58
Jason Lamy Chappuis est 35e � 2'37 Olivier Bertrand
11h57
Je n'ai pas pu assister � la course. Pourquoi Jason s'est-il fait distancer ? Pas en forme, probl�me de mat�riel ?     Olivier Bertrand: Apparemment pas en forme... Commentaire de la part de Mopa
11h56
S�bastien Lacroix est 28e. Olivier Bertrand
11h56
On va re-regarder le final de Vancouver pour se consoler... Commentaire de la part de Hugo
11h56
Le deuxi�me Fran�ais, Fran�ois Braud, est arriv�. Olivier Bertrand
11h55
Jason Lamy Chappuis n'est toujours pas arriv�. Olivier Bertrand
11h55
Il est 17e � 1 min 22 s. Olivier Bertrand
11h55
Premier Fran�ais, Maxime Laheurte, loin du vainqueur. Olivier Bertrand
11h54
C'est finalement le Norv�gien Magnus Krog qui prend le bronze ! Olivier Bertrand
11h54
Il a le temps de lever les bras, tranquille. Olivier Bertrand
11h54
La 3e place est la vraie bataille de ce sprint ! Olivier Bertrand
11h53
Le sprint n'aura dur� que quelques secondes. La sup�riorit� de l'Allemand est �vidente. Olivier Bertrand
11h53
Frenzel attaque et file vers le titre ! Olivier Bertrand
11h53
Watabe m�ne le train mais Frenzel est juste derri�re est � l'aff�t. Olivier Bertrand
11h52
Derni�re grosse mont�e... Olivier Bertrand
11h52
L'�cart a augment� (12 secondes) et semble insurmontable avec le peloton. Olivier Bertrand
11h51
Eric Frenzel a tout gagn�... sauf les Jeux. Olivier Bertrand
11h50
Lamy Chappuis pass� par Maxime Laheurte, c'est dur pour le champion olympique en titre #combin� #JO #sotchi2014 Aur�lien Delfosse via Twitter
11h49
il n'a vraiment plus aucune chance ?     Olivier Bertrand: Non, � moins d'une chute collective de 30 coureurs. Commentaire de la part de Visiteur
11h48
Un peu moins de 2,5 km. Olivier Bertrand
11h48
Combien de km restants? Commentaire de la part de thomas
11h48
Avant les JO, il n'�tait d�j� plus au niveau ! Commentaire de la part de dodo la saumure
11h48
Pour son r�confort Jason est-il engag� sur d'autres �preuves?     Olivier Bertrand: Oui, sur grand tremplin. Commentaire de la part de Visiteur
11h47
O� est Jason ? Queue de peloton ?    Olivier Bertrand: Jason est � 48 secondes... Commentaire de la part de v�v�
11h47
Frenzel et Watabe vont avoir du mal � garder leur avance... Olivier Bertrand
11h46
Malheureusement Jason n'en fait plus partie... Olivier Bertrand
11h46
Le peloton revient � 12 secondes des deux hommes de t�te! Olivier Bertrand
11h46
Le peloton commence � s'�tirer. Et Watabe et Frenzel garde leur cadence. Olivier Bertrand
11h45
Les autres fran�ais ont il une chance ?     Olivier Bertrand: Non, les autres Fran�ais sont trop loin pour viser une m�daille. Commentaire de la part de Simon
11h44
25 minutes a ce rythme, c'est de la folie! Commentaire de la part de Rob
11h44
Jason est rel�gu� � la 17e place � pr�s de 30 secondes... Olivier Bertrand
11h44
o� sont les autres fran�ais ? Commentaire de la part de Visiteur
11h43
20 secondes en 6 km, a moins d'une chute ce n'est pas envisageable. Commentaire de la part de Visiteur
11h43
Il s'est apparemment fait distancer... Olivier Bertrand
11h43
Y a t'il encore une chance pour Jez ? Commentaire de la part de Visiteur
11h42
SI JL CHAPPUIS vise une m�daille ( le bronze), ou en sont ses adversaires directs en dehors de FRENZEL et WATABE ?     Olivier Bertrand: Ils sont dans le peloton de t�te, derri�re, � une vingtaine de secondes des deux hommes de t�te. Commentaire de la part de Lucas
11h42
Jason est bien.. il laisse les autres ramener le peloton sur les 2 hommes de t�te Commentaire de la part de Visiteur
11h42
Il reste un peu moins de 6 kilom�tres � parcourir... Olivier Bertrand
11h41
O� ils ont mis leur carabine?     Olivier Bertrand: Vous confondez avec le biathlon... Commentaire de la part de Visiteur
11h41
L'�cart se r�duit avec le peloton, � nouveau � une vingtaine de secondes. C'est du yo-yo... Olivier Bertrand
11h39
Combien de temps dure la course environ ?     Olivier Bertrand: Une course dure 25 minutes pour les meilleurs. Commentaire de la part de Frisette
11h39
Objectivement, Jason a t il une chance de rattraper 31s de retard sur les skis?     Olivier Bertrand: Ca va �tre tr�s difficile. A Vancouver, Jason Lamy Chappuis avait ski� 16 secondes plus vite qu'Eric Frenzel sur le 10 kilom�tres. L'Allemand avait termin� 10e du combin� nordique. Commentaire de la part de Visiteur
11h39
Watabe prend le relais et sa part de boulot dans son duo avec Frenzel. Olivier Bertrand
11h38
Jason est dans une mauvaise posture, coinc� dans le peloton... Olivier Bertrand
11h38
Alessandro Pittin a d�j� rejoint le peloton. Il faudra se m�fier du m�daill� de bronze � Vancouver. Olivier Bertrand
11h37
Jason est de nouveau � une trentaine de secondes du duo de t�te Frenzel-Watabe. Olivier Bertrand
11h36
Il est ou Jason ?     Olivier Bertrand: Il se situe dans un peloton tr�s compact de 22 coureurs. Commentaire de la part de Visiteur
11h36
Alessandro Pittin, l'un des meilleurs fondeurs, est derri�re, mais il devrait remonter au fil des quatre tours de 2,5 kilom�tres. Olivier Bertrand
11h35
L'�cart augmente-t-il ?    Olivier Bertrand: L'�cart entre le peloton, o� se situe Jason, et le duo de t�te est de 24 secondes, soit 7 secondes de rattrap�es. Commentaire de la part de Visiteur
11h33
Les deux hommes font la course en t�te. Olivier Bertrand
11h33
Watabe semble finalement revenir sur Frenzel. Olivier Bertrand
11h32
L'Allemand n'a pas attendu Watabe, le Japonais parti 6 secondes derri�re lui. Olivier Bertrand
11h31
Frenzel ne semble pas vouloir temporiser, il a pris un d�part tr�s rapide. Olivier Bertrand
11h31
Jason vient de partir avec un petit peloton de fondeurs. Olivier Bertrand
11h30
Eric Frenzel vient de s'�lancer ! Olivier Bertrand
11h30
Le fartage sera d�terminant... Olivier Bertrand
11h30
Les sportifs ne risquent pas d'avoir froid. Il fait quinze degr�s � Sotchi (plus qu'� Paris) et la neige est molle sur une dizaine de centim�tre de hauteur. Olivier Bertrand
11h30
Olivier Bertrand: Pour un finish � la Vancouver 2010... marina36
11h28
Dramatic photo taken by the ISS crew showing #Sochi Olympic Park. Fisht Stadium and the flame are visible. pic.twitter.com/2vA1f9iTsD Johnson Space Center via Twitter
11h27
En 2010, le Fran�ais avait termin� l'�preuve de ski de fond en 25 min 1 sec. Il avait rattrap� ses 46 secondes de retard sur le Finlandais Jane Ryyn�nen, premier de l'�preuve de saut mais qui s'�tait fait largement distancer sur l'�preuve de ski de fond. Olivier Bertrand
11h26
votre pronostic ? difficile d'aller chercher frenzel ?     Olivier Bertrand: Cela s'annonce difficile. Mais Jason Lamy Chappuis est un bon fondeur. Commentaire de la part de Visiteur
11h25
"Je suis dans un bon groupe � une trentaine de secondes. Devant ils vont essayer de se relayer, ce sera une course tr�s dure, mais ce n'est pas termin�", a-t-il expliqu� apr�s son �preuve de saut. Olivier Bertrand
11h25
Si vous �tes inquiets pour Lamy Chappuis, lui ne s'en fait pas trop, comme en t�moigne sa r�action apr�s l'�preuve de saut. Olivier Bertrand
11h23
Lamy Chappuis a r�alis� un saut � 98,5 m pour un total de 123,7 points, ce qui augure une course ind�cise en ski de fond, m�me si Frenzel et le Japonais Akito Watabe partent avec un net avantage qui pourrait �tre d�cisif. Olivier Bertrand
11h22
Il ne faut pas arriver en retard, car l'�preuve de ski de fond est tr�s rapide. Les meilleurs de la discipline bouclent g�n�ralement le 10 kilom�tres en 25 minutes, voire 24 minutes pour la cr�me de la cr�me. Olivier Bertrand
11h22
Re-bonjour � toutes et � tous. Si vous ne rejoignez, sachez que Jason Lamy Chappuis a r�alis� un saut tr�s moyen. Il s'�lancera au d�part du sprint de ski de fond � la 8e place, avec 31 s de retard sur l'Allemand Eric Frenzel > http://lemde.fr/1ntDCrC Olivier Bertrand
09h28
Bon bah...donc RDV � 13 h30 pour le fond en live comme ce matin ?     Olivier Bertrand: Merci d'avoir suivi ce live avec nous. Nous vous donnons rendez-vous � 13 h 30 pour l'�preuve de ski de fond. Commentaire de la part de Myriam
09h27
La neige mouill�e favorise les skieurs en l'occurence et non les sauteurs, car elle augmente le temps de course et la difficult� de la course de ski de fond.     Olivier Bertrand: Merci pour votre remarque. Commentaire de la part de Maxime
09h27
Et est ce que vous avez le t�l�phone de Jason, c'est pour une amie?     Olivier Bertrand: Non, d�sol�. Commentaire de la part de Visiteur
09h25
Un r�capitulatif de la position de Jason et de son "retard" sur ceux qui le precedent ?     Olivier Bertrand: Il suffit de demander @Muranil. Jason Lamy Chappuis partira en huiti�me position, � 31 secondes du premier, Eric Frenzel. L'Allemand poss�de 6 secondes d'avance sur le Japonais Watabe et 27 secondes d'avance sur le Russe Klimov. Commentaire de la part de Muranil
09h23
Le talent des commentateurs dinosaures ^^ Commentaire de la part de Visiteur
09h23
Patrick Montel compare Jason Lamy Chappuis � Marie Jo P�rec. On esp�re que ce n'est pas cellle des JO de Sydney... Olivier Bertrand
09h22
Il sourit et n'a pas l'air vraiment stress�. Olivier Bertrand
09h21
Jason Lamy Chappuis s'estime "encore dans le coup". "Tout est encore faisable". Olivier Bertrand
09h21
Vu les conditions climatiques, les profils sauteurs ne vont-ils pas �tre avantager par rapport aux profils plus ski ?    Olivier Bertrand: Oui, la neige lourde avantagera forc�ment les sauteurs. Commentaire de la part de Visiteur
09h21
Il est leader de la Coupe du monde, avec 7 podiums cette saison. Olivier Bertrand
09h20
L'Allemand Eric Frenzel a domin� la discipline cet hiver. Olivier Bertrand
09h19
Watabe partira quasiment avec Frenzel. Les deux hommes auront une avance cons�quente sur leurs poursuivants. Olivier Bertrand
09h19
103 m et 131,5 pts. Il va partir avec 31 secondes d'avance sur Jason Lamy Chappuis. Olivier Bertrand
09h18
Et le dernier favori, Eric Frenzel, fait encore mieux! Olivier Bertrand
09h18
Le Japonais Akito Watabe vient de sauter tr�s loin. Il prend la t�te et rel�gue tout le monde � plus de 20 secondes. Il va falloir aller le chercher. Olivier Bertrand
09h17
C'est grave pour la suite ?    Olivier Bertrand: La sp�cialit� de Jason Lamy Chappuis est le ski de fond. On peut esp�rer qu'il fasse un tr�s tr�s bon parcours cette apr�s-midi. Commentaire de la part de Ph.
09h16
Il reste combien de sauteurs ?     Olivier Bertrand: Il en reste deux... Klemetsen vient de sauter. Commentaire de la part de Myriam
09h16
Les jurassiens sont avec toi Jason ! Commentaire de la part de Jon
09h15
98, 5m. Il est sixi�me. Olivier Bertrand
09h14
C'est au tour de Jason ! Olivier Bertrand
09h14
Rydzek est huiti�me. Olivier Bertrand
09h14
L'Allemand Johannes Rydzek s'�lance. Le suivant ? Jason! Olivier Bertrand
09h12
Les gros favoris commencent � sauter. Le Norv�gien Magnus Moan vient de sortir un gros saut mais est p�nalis� par la technique... Il n'est que dixi�me. Olivier Bertrand
09h10
Laquelle des �preuves est le point fort de Jason ?     Olivier Bertrand: C'est l'�preuve de ski de fond. Mais sa force est d'�tre un skieur tr�s complet. Commentaire de la part de Visiteur
09h10
L'Autrichien Lukas Klapfer s'invite sur le podium provisoire. Les deux premiers restent le Russe Evgenyi Klimov et le Japonais Taihei kato. Olivier Bertrand
09h08
Combien avait �t� class� Jason lors des JO de vancouver apr�s l'�preuve du saut ?     Olivier Bertrand: Il avait �t� class� cinqui�me � l'issue de l'�preuve de saut. Commentaire de la part de Gilles
09h07
Quel temps fait il actuellement?     Olivier Bertrand: Il fait tr�s chaud � Sotchi actuellement, avec des temp�ratures de 15�C pr�vu sur les sites. (http://lemde.fr/Mbadac) Ce qui sera un probl�me pour l'�preuve de fond. Commentaire de la part de Visiteur
09h06
La taille et le poids des skis est-il impos� aux sauteurs ?     Olivier Bertrand: La longueur des skis est calcul�e en fonction de la taille et et du poids des skieurs, selon des r�gles tr�s pr�cises. Commentaire de la part de Jean
09h05
RT @ZeFredouille: Sous le soleil de #RosSkiGorki avec Jason #Lamy #Chappuis #combin� #nordique #Sotchi2014 pic.twitter.com/aGVq8Ne16P AFP Sports via Twitter
09h05
Vous devriez mettre un compteur pour savoir combien de sauteur il reste :)     Olivier Bertrand: Douze concurrents doivent encore s'�lancer sur le tremplin. Commentaire de la part de Benjamin
09h04
A noter que l'Allemand Tino Edelmann, l'un des favoris, est pour l'instant troisi�me... Olivier Bertrand
09h03
@Visiteur Dans une dizaine de minutes Olivier Bertrand
09h03
A-t-on une id�e de l'horaire de passage de Jason? Commentaire de la part de Visiteur
09h03
Quand a lieu la deuxi�me �preuve ?     Olivier Bertrand: Les 10 km de ski de fond auront lieu � partir de 13 h 30. Commentaire de la part de yolu
09h01
Peut-on savoir si Maxime Laheurte a des chances de conserver une place dans les 10?     Olivier Bertrand: �a va �tre tr�s compliqu� pour lui. Les cadors du saut ne se sont pas encore �lanc�s. Commentaire de la part de Max
09h00
Suite au saut, des points sont attribu�s pour la longueur et le style. Le d�part de la course de ski de fond s'effectue selon la m�thode Gundersen (1 point = 4 secondes), le coureur occupant la premi�re place du classement de saut s��lance en premier, et les autres s��lancent ensuite dans l�ordre fix�. Olivier Bertrand
08h59
Comment est ensuite d�termin� l'�cart temps pour le d�part du fond suite aux sauts ? Commentaire de la part de klemavalon
08h59
merci pour ce direct, c'est g�nial Commentaire de la part de Herv�
08h58
Non, il n'y a pas de concurrents "exotiques" dans cette �preuve. Olivier Bertrand
08h58
Est-ce qu'il y a des br�siliens Dan's la competition? Commentaire de la part de Fran�ois
08h57
Si vous avez manqu�, hier, la m�daille de Coline Mattel, voici le compte-rendu de notre reporter @hseckel >> http://lemde.fr/LTUZWr Olivier Bertrand
08h56
@h3Mz Tous sont qualifi�s. Leur ordre de passage est d�fini en fonction de leurs performances aux deux sauts. Olivier Bertrand
08h55
combien de sauteurs sont qualifi�s pour la phase suivante? merci Commentaire de la part de h3Mz
08h55
Evidemment, France t�l�visions a organis� un direct de Bois d'Amont, la commune jurassienne de Lamy Chappuis. Olivier Bertrand
08h54
Fran�ois Braud est 9e du classement provisoire. Olivier Bertrand
08h53
Bonjour a quel num�ro sommes nous ?     Olivier Bertrand: Nous sommes, grosso modo, � mi-chemin du concours. Commentaire de la part de Patrice
08h53
Il a atteri � 95 m�tres et poss�de 113,5 points. Olivier Bertrand
08h52
Il est un peu court... Olivier Bertrand
08h52
Fran�ois Braud s'�lance! Olivier Bertrand
08h51
Le Russe Klimov est toujours devant au classement provisoire. Olivier Bertrand
08h50
Avec 117,3 points, Maxime Laheurte est toujours 5e. Olivier Bertrand
08h48
Yoshito Watabe vient de sauter. Mais c'est son fr�re Akito qui sera le plus gros danger pour Lamy Chappuis. Olivier Bertrand
08h47
En attendant le vol de Jason, nous vous invitons � lire notre gazette des Jeux du jour > http://lemde.fr/1g622XO Olivier Bertrand
08h46
Moi qui pensais que dans combin� nordique, il y avait nordique... #Lamy-Chappuis #H-1h30 #Summergames #sotchi2014 pic.twitter.com/lAGBmNtmlL Eric Bruna via Twitter
08h46
Il a atterri � 97 points. Il est deuxi�me pour l'instant. Olivier Bertrand
08h45
Maxime Laheurte a saut�! Olivier Bertrand
08h44
Les performances des femmes sur le petit tremplin et celles des hommes semblent comparables. Peut on esp�rer tenir l� un sport o� hommes et femmes soient � �galit� en termes de potentiel de performances? Commentaire de la part de Thl
08h43
Pour parfaire son saut, Jason Lamy Chappuis a travaill� avec une �quipe de scientifiques. Voici notre article > La soufflerie au service du saut � skis (http://lemde.fr/1cvhKah) Olivier Bertrand
08h42
Pas de souci, morfaledu38, ne vous en faites pas, vous pourrez manger � midi devant Tout le monde veut prendre sa place. Sauf catastrophe, Jason Lamy Chappuis aura saut� avant midi. Olivier Bertrand
08h42
Vous pouvez m�me pas dire une fourchette pour le passage de Jason ? Pour que je puisse caler mon repas de midi. Commentaire de la part de morfaledu38
08h39
Sur combien de concurrents ?     Olivier Bertrand: Il y a 46 concurrents dans cette �preuve. Commentaire de la part de Un expat
08h38
A la suite du saut, des points sont attribu�s pour la longueur et le style. Olivier Bertrand
08h38
RT @marina36: Le casque de Jason Lamy-Chappuis. Absolument parfait. pic.twitter.com/UCRVEh6oeC ?Yoann Bouilly� via Twitter
08h37
@Atom Nous n'avons pas d'heure pr�cise, mais il s'�lancera en 43e position. Olivier Bertrand
08h37
A-t-on une heure pr�vue pour Jason Lamy-Chappuis ? Commentaire de la part de Atom
08h34
Les premiers sauteurs ne sont pas les favoris, qui s'�lanceront en derni�res positions. Olivier Bertrand
08h34
L��preuve de ski nordique en individuel a fait son apparition � l�occasion de la premi�re �dition des Jeux olympiques d'hiver de Chamonix en 1924. Olivier Bertrand
08h34
Jason Lamy Chappuis sautera en 43e position. Olivier Bertrand
08h33
Le premier Fran�ais � s'�lancer sera Maxime Laheurte, en seizi�me position. Olivier Bertrand
08h32
RT @YannBertrand: Le tremplin olympique duquel va s'�lancer Jason Lamy-Chappuis dans moins d'une heure #Sochi2014 pic.twitter.com/Vyi0VxPhB9 Bernadette Faille 4 via Twitter
08h31
[Saut � Ski] Ronan Lamy-Chappuis, 33�me, passe le premier tour de qualification sur le petit tremplin #Sotchi2014 pic.twitter.com/s8hD5f99cJ JO 2014 Sotchi via Twitter
08h31
C'est parti ! Le premier concurrent, le Russe Evgeniy Klimov, s'est �lanc�. 124, 7 points. Olivier Bertrand
08h30
L'�preuve de saut � skis avait �t� marqu�e par de mauvaises conditions m�t�orologiques qui oblig�rent le concours � �tre recommenc�. Cependant, lors du second saut, les conditions s'�taient � nouveau d�grad�es � l'occasion du passage des cinq derniers concurrents, dont Jason Lamy-Chappuis, qui avaient sign� des performances tr�s en de�� de celles de leurs sauts d'entra�nement. Olivier Bertrand
08h29
'Jez' - c'est l'un de ses surnoms - se rappelle s�rement sa m�saventure sur grand tremplin � Vancouver, o� il n'avait pris que la 18e place. Olivier Bertrand
08h29
Jason Lamy Chappuis au micro de France T�l�visions : "C'est du bon stress. Il y a la famille, les amis et je sais qu'il y a pas mal de gens qui nous regardent en France. En ce moment, il n'y a pas de vent , c'est important, et du soleil. �a ne va pas nous g�ner ce matin sur le saut mais sur le ski de fond, la neige va �tre lente." Olivier Bertrand
08h26
Mais il n'y a pas que Jason qui participe aujourd'hui � l'�preuve. Les trois autres Fran�ais engag�s : Maxime Laheurte, S�bastien Lacroix et Fran�ois Braud. Olivier Bertrand
08h24
Avant de commenter cette �preuve, nous vous proposons de (re)d�couvrir notre portrait : Jason Lamy Chappuis, l'Argonaute du nordique >> http://lemde.fr/1cv3UER Olivier Bertrand
08h23
L'�preuve va d�buter � 10 h 30. Olivier Bertrand
08h23
Jason Lamy Chappuis s'�lance sur le tremplin de Russki Gorki avec l'objectif de d�fendre son titre acquis il y a quatre ans � Vancouver. Olivier Bertrand
08h23
Bonjour et bienvenue sur notre live. Olivier Bertrand
Journaliste(s) pr�sent(s) sur le live : Yann Bouchez et Emmanuel Versace
Jeux olympiques de Sotchi 2014
