TITRE: Toyota rappelle 1,9 million de Prius hybrides - Automobile  - 12/02/2014 - leParisien.fr
DATE: 2014-02-12
URL: http://www.leparisien.fr/automobile/constructeurs/toyota-rappelle-1-9-million-de-prius-hybrides-12-02-2014-3583017.php
PRINCIPAL: 165791
TEXT:
Toyota rappelle 1,9 million de Prius hybrides
Publi� le 12.02.2014, 07h15                                             | Mise � jour :                                                      07h35
R�agir
Et si les voitures Toyota �taient maudites ? Le constructeur d'automobiles japonais a annonc� mercredi un nouveau rappel de 1,9 million de Prius dans le monde entier, en raison d'un probl�me de syst�me hybride qui peut engendrer l'arr�t du moteur. La mesure concerne notamment pr�s d'un million de v�hicules au Japon, 700 000 aux Etats-Unis et 130 000 en Europe, dont quelque 15 600 en France .
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Automobile: Toyota recrute 500 salari�s
Ces voitures, issues du dernier mod�le de la gamme Prius fabriqu�es entre 2009 et 2014, vont �tre rappel�es pour reprogrammer un logiciel de contr�le li� au syst�me hybride (motorisation � essence et �lectricit�). �Dans le pire des cas, la voiture peut s'arr�ter pendant la conduite. Nous consid�rons ceci comme un possible probl�me de s�curit�, ce qui explique ce rappel�, a expliqu� un porte-parole de l'entreprise.
Le constructeur a indiqu� avoir �t� averti de 400 cas, au Japon et aux Etats-Unis, ajoutant qu'aucun accident ni blessure n'avait �t� rapport�. Il a soulign� aussi que, dans le pire des cas, la voiture �s'arr�tait peu � peu, pas d'un seul coup�. Lorsqu'il est fortement sollicit�, le �logiciel peut faire entrer le v�hicule en mode de s�curit�, limitant la puissance disponible pour la conduite. Dans de rares cas, le module de contr�le pourrait se r�initialiser, mettant hors service le syst�me hybride� et entra�nant l'arr�t du v�hicule, a d�taill� le constructeur.
Des rappels de voitures tr�s r�guliers
Toyota a connu une crise majeure � la fin 2009 et au d�but 2010 lorsqu'il avait d� rappeler en urgence pr�s de 9 millions de voitures dans le monde, notamment aux Etats-Unis, � cause de probl�mes de p�dales d'acc�l�ration pouvant rester bloqu�es en position enfonc�e ou de frein r�agissant tardivement.
R�agir avec mon compte You / le Parisien
Identifiant
Votre e-mail* (ne sera pas visible)
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
D�connexion
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
> Cliquez ici pour lire les 20 r�actions � l�article
mp 14/02/2014 - 17h02
mieux vaut rappeler que d ' ignorer le probl�me comme certaine marque ont fait.....  ma yaris de 2002 a �t� rappel� il y a peu pour un controle d'airbag ( pour en finir pas de changement pour moi )  et je trouve que c'est bien parce que dans beaucoup d'autres marques une fois la garantie pass�e c'est la sourde oreille en cas de probl�me.  a croire que toyota ( en tete des ventes mondiales ) est en ligne de mire. quand d'autres marques rappelle ( ce qui est  plus ou moins exceptionnel ) personne n'en parle . pour r�sum� acheter francais vous n'aurez pas de rappel
R�ponse Signaler un abus
52fe145536346 14/02/2014 - 14h04
J ai jne mercedes classe s 500 de 2002 et une toyota prius de 2011.  Devinez avec laquelle il y a toujours un probleme !   La mercedes bien sur et meme que parfois meme la concession n arrive pas a trouver la panne.  Lamentable pour des voitures a ce prix.  Ma touota a 120000 kms et encore rien fait a part les vodanges.  Vraimet fiable comme boiture.
R�ponse Signaler un abus
Slunky 14/02/2014 - 05h35
J ai eu une Pruis et maintenant une Auris TS et vraiment je dois dire que Toyota a toujours ete aux petits soins de ses clients... Avant j ai eu des peugeots et c ?tait r?paration sur r?paration alors qu apr?s mes enqu?tes c ?tait souvent des d?fauts d origines, mais chez Peugeot Chut et fallait payer...
R�ponse Signaler un abus
petard 12/02/2014 - 18h54
En r�ponse @chabal. Les constructeurs qui ne rappellent jamais de vehicules... ca n'est pas parce que leurs vehicules sont parfaits, c'est parce qu'ils refusent de prendre des mesures. Les voitures d'aujourd'hui sont plus complexes il y a des problemes qui peuvent ne pas se reveler malgre des milliers d'heures de tests. Tous les constructeurs sont touches....je prefere un constructeur qui reconnait les problemes et prend des mesures quitte a faire souffrir son image marketing plutot qu'un constructeur qui fait passer son image avant ses clients.
R�ponse Signaler un abus
Julien 12/02/2014 - 13h16
@djynn62, il s'agit d'une voiture, pas d'un ordinateur/t�l�phone... Une mise � jour ne se fait pas du tout de la m�me mani�re! Il est impossible d'avoir acc�s � "l'ordinateur de bord" en passant par le lecteur CD/USB... Simple question de s�curit�! La mise � jour se fait via la fiche DIAG, � l'aide d'un ordinateur con�u sp�cialement pour d�tecter les erreurs et les corrig�es... Je trouve �galement tr�s honn�te de la part de Toyota d'assumer leurs d�fauts!
R�ponse Signaler un abus
djynn62 12/02/2014 - 12h07
Une mise a jour du firmware et du software.  Pourquoi ne pas laisser les utilisateurs faire a mise a jour eux m?me avec une cl? USB ou un CD?
R�ponse Signaler un abus
12/02/2014 - 11h55
Cela commence � bien faire , tous ces "rappels" de marques automobiles ... Ils devraient d�domager pour les inconvenients cr��s . Amener le v�hicule , rentrer , retourner , rester sans voiture etc ...
R�ponse Signaler un abus
maumaj 12/02/2014 - 11h16
Ce probl�me ne semble pas �tre li� uniquement � Toyota... J'ai rencontr� un incident identique sur une Peugeot 207 en 2011
R�ponse Signaler un abus
52fb48ef3e79b 12/02/2014 - 11h11
J en suis a ma 3�me Toyota dont 2 hybrides pas le moindre probl�me a signaler et pour un co�t d'entretien tr�s faible, apr�s �tre passe par Psa et Renault avec quelques d�boires toujours pour ma pomme et bien sal�e en plus, je resterai fid�le � Toyota pour toujours, c'est le 1 constructeur mondial qui par ailleurs fabrique des voitures en France, pour de multiple bonnes raisons , on peut d�plorer les commentaires inappropri�s de certains journalistes
