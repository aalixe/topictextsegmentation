TITRE: Est-ce vraiment la prochaine interface de Twitter ? � metronews
DATE: 2014-02-12
URL: http://www.metronews.fr/high-tech/est-ce-vraiment-la-prochaine-interface-de-twitter/mnbl!VE1nGBir3da8A/
PRINCIPAL: 0
TEXT:
Cr�� : 12-02-2014 17:05
Est-ce vraiment la prochaine interface de Twitter ?
R�SEAUX SOCIAUX � Un site am�ricain a publi� une capture d'�cran de ce qui pourrait �tre le nouveau look de Twitter. Une �volution probable pour le site qui peine encore � fid�liser ses utilisateurs.
�
Un aper�u de l'interface de Twitter rep�r�e par Mashable.� Photo :�Capture d'�cran Mashable
Twitter va-t-il profond�ment remanier l'interface de son service ? D'apr�s le site sp�cialis� Mashable , la soci�t� am�ricaine pr�parerait un lifting d'importance de sa page d'accueil. Finie la "timeline" qui fait d�filer tous les tweets de ses contacts, elle laisserait place � une mise en page se rapprochant de celle de Google+, le r�seau social du g�ant de Mountain View.
Selon Mashable, cette nouvelle interface s'est affich�e sur le profil de Matt Petronzio, un assistant de la r�daction du site. Elle met notamment en avant les photos et les "cards", ces tweets enrichis par exemple d'un aper�u du contenu d'un article de presse. Enfin, la photo de l'utilisateur est d�port�e sur la gauche et la photo d'illustration du profil s'�tend d�sormais sur l'int�gralit� de la largeur de la page.
Twitter "teste r�guli�rement de nouvelles fonctionnalit�s"
Joint par metronews, Twitter France, � l'instar de sa maison m�re ne confirme pas l'information, mais pr�cise simplement "tester r�guli�rement de nouvelles fonctionnalit�s sur certains profils". Une d�marche classique pour un grand nombre de services web qui collectent ainsi les r�actions � ces nouvelles interfaces avant de les d�ployer ou pas � tous les utilisateurs.
Cette interface pourrait donc �tre l'une pr�vue parmi d'autres. En effet, les premiers r�sultats financiers de Twitter publi�s apr�s son entr�e en bourse ont notamment montr� la difficult� qu'avait le site � fid�liser ses utilisateurs . Cet "engagement" est l'une des mesures de pr�dilection des performances de r�seaux sociaux, c'est lui qui mesure la fid�lit� des utilisateurs � un site.
Twitter doit ainsi continuer � innover pour convaincre les nouveaux inscrits de continuer � utiliser son service. Selon les chiffres publi�s par le site am�ricain Re/code il y a quelques jours, Twitter n'aurait ainsi retenu que 241�millions d'utilisateurs actifs sur le plus d'un milliard qui l'aurait essay�.
L'int�gralit� de la possible prochaine interface du site de Twitter.� Photo :�Capture d'�cran Mashable
