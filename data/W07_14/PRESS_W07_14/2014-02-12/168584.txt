TITRE: Des sites pornos proposés à des enfants par la ville de Besançon
DATE: 2014-02-12
URL: http://www.numerama.com/magazine/28393-des-sites-pornos-proposes-a-des-enfants-par-la-ville-de-besancon.html
PRINCIPAL: 168581
TEXT:
Publi� par Guillaume Champeau, le Mercredi 12 F�vrier 2014
Des sites pornos propos�s � des enfants par la ville de Besan�on
La ville de Besan�on a d� envoyer un courrier d'excuses aux parents d'�l�ves, apr�s leur avoir fourni une cl� USB de contenus ludo-�ducatifs... sur laquelle figuraient des liens vers des sites "non adapt�s au mineurs".
19
La bourde est bien involontaire, mais elle devrait inciter les professionnels de l'�ducation � la prudence lorsqu'ils �tablissent des listes de sites web recommand�s aux enfants, alors qu'une URL n'a rien de p�renne, et peut changer de propri�taire du jour au lendemain.
A Besan�on, la municipalit� a �t� alert�e par des parents choqu�s de d�couvrir, en ouvrant une cl� USB de logiciels ludo-�ducatifs distribu�e par la Communaut� d'agglom�ration du Grand Besan�on, une page HTML avec des liens menant vers diff�rents sites... dont au moins deux �taient devenus des sites pour adultes. "Leur nom de domaine a �t� repris par des soci�t�s qui h�bergent depuis des sites pornographiques", nous pr�cise un parent d'�l�ve.
Les cl�s USB ont �t� distribu�es dans le cadre de l'op�ration Besan�on Clic , � travers laquelle la municipalit� offres des " cartables num�riques " (en fait des PC de bureaux) � des �l�ves de CE2, pour les familiariser � l'outil num�rique. Dans ce cartable figure notamment une "cl� USB de 2 Go pour le transfert de logiciels �ducatifs agr��s par l�Education nationale".
Mardi, les parents concern�s ont tous re�us un courrier d'excuses sign� de la direction g�n�rale des services de la ville de Besan�on. "Sur cette cl� figurent plus d'une centaine de logiciels ludo-�ducatifs ainsi que des liens vers des sites internet de t�l�chargement compl�mentaires ou d'actualisation. A notre insu, deux de ces sites ont tr�s r�cemment chang� de propri�taire et pr�sentent d�sormais des contenus inadapt�s aux mineurs", reconna�t la ville.
"Nous vous invitons par cons�quent � ne pas utiliser les cl�s "Ordi-Classe" qui seraient en votre possession, y compris celles des ann�es pr�c�dentes et � les rapporter � l'�cole pour proc�der � leur remplacement".
"Croyez bien que nous sommes tr�s sinc�rement d�sol�s de cette situation", s'excuse la municipalit�.
"D'ores et d�j�, nous vous rappelons l'int�r�t d'activer un logiciel de contr�le parental", conclut la lettre. Mais elle se garde bien de communiquer une URL o� les t�l�charger .
