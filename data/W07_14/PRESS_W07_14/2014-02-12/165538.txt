TITRE: ariegenews.com - Les apiculteurs toujours en guerre contre les pesticides
DATE: 2014-02-12
URL: http://www.ariegenews.com/ariege/actualites_economie/2014/72596/les-apiculteurs-toujours-en-guerre-contre-les-pesticides.html
PRINCIPAL: 0
TEXT:
Les apiculteurs toujours en guerre contre les pesticides
Un apiculteur inspectant sa ruche
��AFP/Archives - Remy Gabalda
Un an apr�s le lancement d'un plan de soutien � l'apiculture, la production fran�aise de miel a encore baiss� en 2013, les professionnels accusant de nouveau certains pesticides mais aussi le mauvais temps.
"En 2013, la production fran�aise s'av�re tr�s faible et inf�rieure � 15.000 tonnes, encore en recul par rapport � 2012", a indiqu� Henri Cl�ment, porte-parole de l'Union nationale des apiculteurs fran�ais, en soulignant que des "conditions climatiques tr�s mauvaises (froid, pluie)" �taient venues "s'ajouter aux probl�mes d'intoxication".
Avec un nombre de ruches presqu'�gal (de 1,25 � 1,3 million), la production de miel a �t� plus de deux fois moins importante en 2013 qu'en 1995, date de d�but d'utilisation de certains pesticides, dont les n�onicotino�des, met en avant l'Unaf.
Le syndicat d'apiculteurs professionnels, qui bataille depuis des ann�es contre l'utilisation de pesticides sur les cultures pollinis�es par les abeilles, a de nouveau sonn� la charge contre plusieurs produits dont ceux contenant les mol�cules de thiaclopride (Prot�us, Sonido) et d'ac�tamipride (Supr�me).
Ils r�clament leur interdiction par la France et demandent � l'Europe de revoir l'�valuation de leur toxicit�.
"Ces deux substances n�onicotino�des pr�sentent les m�mes modes d'actions que ceux r�cemment suspendus" par l'Union europ�enne, explique l'Unaf. Et leurs �valuations pr�sentent les carences similaires � celles ayant conduit � l'interdiction, ajoute l'organisation professionnelle.
Au printemps dernier, Bruxelles a interdit pour deux ans et sur certaines cultures trois substances actives (clothianidine, imidaclopride et thiam�thoxame) appartenant � la famille des n�onicotino�des en raison de leur r�le dans la mortalit� accrue des abeilles (environ 30% du cheptel par an).
Jean-Marc Bonmantin, chercheur au CNRS � Orl�ans, sp�cialiste des produits neurotoxiques, a fait �tat mardi de travaux montrant que "la toxicit� chronique de l'ac�tamipride et du thiaclopride est similaire aux n�onicotino�des bannis" par Bruxelles.
Pour lui, "les diff�rentes mol�cules de la famille des n�onicotino�des sont tr�s proches et il n'est pas tr�s judicieux d'en avoir interdit seulement trois".
Sonnette d'alarme
Les mol�cules provisoirement interdites par Bruxelles sont pr�sentes dans des pesticides (Gaucho, Cruiser, Poncho, etc.) fabriqu�s par Bayer ou Syngenta, qui ont contest� en justice l'interdiction.
L'Union europ�enne a �galement interdit pour deux ans l'insecticide Fipronil sur les semences de ma�s et de tournesol.
La France avait pris les devants en bannissant le Fipronil d�s 2005, le Cruiser sur le colza (2012) et le Gaucho (sur tournesol en 1999 et sur ma�s en 2004).
Si la responsabilit� de certains pesticides dans la surmortalit� des abeilles est aujourd'hui scientifiquement confirm�e, il y a d'autres facteurs qui y contribuent: des parasites comme le Varoa, le frelon asiatique ou la perte de diversit� des cultures.
Les apiculteurs ne contestent pas cet aspect multifactoriel mais soutiennent que les pesticides en sont la principale cause.
"Le probl�me des pesticides est le probl�me des apiculteurs dans le monde entier, en France mais aussi chez nos voisins europ�ens, en Argentine, aux Etats-Unis", a affirm� Henri Cl�ment pour qui "il y a d'autres types de probl�mes, mais celui des pesticides est essentiel dans la hi�rarchie des causes".
L'Unaf a d'ailleurs d�plor� que le frelon asiatique ait �t� class� nuisible en cat�gorie 2, ce qui n'implique pas une lutte obligatoire coordonn�e par l'administration.
Henri Cl�ment s'est aussi �lev� contre la volont� du minist�re de l'Agriculture d'assouplir les d�rogations � l'interdiction de traiter les cultures en p�riode de floraison. "Nous attendons avec impatience l'avis de l'Anses (agence sanitaire) � ce sujet", a-t-il dit.
Concernant le "plan de d�veloppement durable de l'apiculture" annonc� il y a un an, l'Unaf appelle � "�tre r�aliste".
"L'objectif d'installer 2 � 300 nouveaux apiculteurs par an est ambitieux, mais il faut une coh�rence et cr�er un environnement favorable aux abeilles en retirant des pesticides sinon les jeunes vont aller au del� de d�convenues", estime Henri Cl�ment.
france
