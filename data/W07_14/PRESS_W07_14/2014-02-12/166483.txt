TITRE: Foot - Coupe - Moulins - Kamata, la boucle est boucl�e
DATE: 2014-02-12
URL: http://www.lequipe.fr/Football/Actualites/Kamata-la-boucle-est-bouclee/440364
PRINCIPAL: 0
TEXT:
a+ a- imprimer RSS
Kamata avait un �norme potentiel. Mais il n'a pas fait la carri�re qu'on lui promettait... (L'Equipe)
�Pedro Kamata �tait vif et avait d�excellentes qualit�s techniques, se souvient Daniel Rolland, ancien directeur du centre de formation d'Auxerre. Il appartenait � la g�n�ration des Mex�s, Ciss�, Kapo. Il avait son caract�re, et il fallait savoir le prendre pour en tirer le meilleur.� Rep�r� � Moulins � l�aube de l�adolescence l'AJA, Kamata avait int�gr� le centre de formation bourguignon � 13 ans. Mais plut�t que dans l�Yonne, c�est au FC Groningen (Pays-Bas) que celui qui �tait d�crit comme le possible successeur de Bernard Diom�de signe son premier contrat professionnel en 2002... �Mon caract�re? J�en avais, mais je n��tais pas le plus dur�, s�amuse-t-il.
En Italie avec Marco Simone et Antonio Conte
Apr�s avoir �volu� � Clermont et � Ch�teauroux, devenant un bon joueur de Ligue 2, celui qui fut international congolais en 2005 (une s�lection) s�est exil� en Italie pendant six ans. �D�abord � Legnano, que venait de racheter Marco Simone. C��tait en S�rie C2 (CFA) et quand je suis arriv�, j�ai flipp� en voyant les installations, de niveau PH. Mais on est mont� en C1, et j�ai sign� en 2008 � Bari, o� entra�nait Antonio Conte.� Les deux hommes se retrouveront � Sienne (2010-2011), la derni�re saison transalpine de Kamata, avant son retour en France, d�abord � Yzeure, puis � Moulins � l��t� 2012.
Il veut laisser sa place aux jeunes
Pedro Kamata n�a que 32 ans, une vie d�j� bien riche, et l�id�e de passer � autre chose en fin de saison occupe une partie de son esprit. �Cela fait presque vingt ans que je suis dans ce milieu au quotidien. Cela demande beaucoup d�efforts, et pendant que je joue, c�est une place en moins pour un jeune�, explique l�attaquant de Moulins, qui pr�pare sa reconversion : �La saison derni�re, j��tais �ducateur au club. L�, je travaille avec le service communication�. Un avenir d�j� tout trac�, mais qui attendrait bien un petit exploit en Coupe !
Alexis BILLEBAULT
