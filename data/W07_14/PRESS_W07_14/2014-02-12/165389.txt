TITRE: Municipales à Paris : Christophe Najdovski, l'homme vert de la campagne - 11 février 2014 - Le Nouvel Observateur
DATE: 2014-02-12
URL: http://tempsreel.nouvelobs.com/elections-municipales-2014/20140211.OBS5817/municipales-a-paris-christophe-najdovski-l-homme-vert-de-la-campagne.html
PRINCIPAL: 165387
TEXT:
La t�te de liste EELV, qui tient son premier grand meeting de campagne mardi soir � Paris,�tente�de se faire entendre, � l'heure o� Anne Hidalgo et NKM�affichent leurs pr�occupations �colos.
Christophe Nadjovski, candidat Europe Ecologie Les Verts à la Mairie de Paris.  (20 MINUTES/SIPA)
C'est un candidat qui, jusqu'ici, n'a pas fait de bruit. Pas assez au go�t de ses amis qui aimeraient que leur chef de file fasse plus parler de lui. Christophe Najdovski, la t�te de liste d'Europe Ecologie-Les Verts (EELV) aux municipales � Paris , est comme �a : discret, pos�, pas emport� pour un sou. "Un beau gar�on, sur les affiches il n'y a aucun souci. Mais disons qu'il a des progr�s � faire quant � ses qualit�s de communicant. Il faudrait qu'il fende l'armure, glisse un de ses colistiers". Le slogan choisi par Najdovski � Vivre mieux � Paris - est d'ailleurs � son image : sans emphase ni effets de manches ! "Mais si des gens pouvaient entendre parler de lui, ce serait quand m�me mieux", ajoute encore un camarade.
Les �cologistes font circuler une astuce mn�motechnique pour retenir le nom, d'origine mac�donienne, de leur t�te d'affiche : il faut imaginer qu'il "nage sur le dos" et vous aurez son surnom, "Najdo". Cela tombe bien : le candidat �colo promet un plan d'investissement pour les piscines dans la capitale. Une des propositions-phares de son programme, avec celle de la sortie du diesel et d'un tramway de la Seine, circulant sur les quais hauts de la Porte de Saint-Cloud � Bercy pendant que les pi�tons, en bas, gambaderaient au bord du fleuve. Un tramway qu'il veut m�me relier, � terme, au m�tro du Grand Paris afin de faire encore baisser la circulation et la pollution intra-muros.
Difficile de lui coller l'�tiquette de Khmer vert
Mais ce n'est pas lui que vous entendrez promettre l'enfer aux automobilistes - comme Yves Contassot en 2001 - ou annoncer que les imp�ts vont grimper en fl�che - comme Denis Baupin en 2008. "Il a r�ussi un petit exploit, se f�licite un cadre d' EELV , transformer l'image des Verts � Paris o� on �tait per�us comme des mecs punitifs, contre la bagnole, voulant fermer le p�rif." Difficile en effet de lui coller l'�tiquette de Khmer vert ! D'autant qu'il accueille sur sa liste, dans le 12e arrondissement, en position non �ligible, le ministre tr�s "r�alo" au D�veloppement, Pascal Canfin.
Najdo, 44 ans, a d�j�, lui, des responsabilit�s � l'H�tel de Ville : depuis 2008, ce fils d'un ouvrier de chez Renault et d'une concierge, n� dans le 20e arrondissement, est l'adjoint � la petite enfance. C'est lui qui est charg� de multiplier les places en cr�ches. Lui aussi qui a fait remplacer tous les biberons contenant du bisph�nol A en 2010.
Le seul, le vrai �colo
Ce travail avec les socialistes municipaux ne l'emp�che pas de s'opposer � de nombreux projets port�s par Bertrand Delano� hier et par Anne Hidalgo aujourd'hui : le nouveau stade de rugby Jean-Bouin, la tour Triangle dans le 15e arrondissement ou encore la transformation de l'avenue Foch en parc urbain avec des logements sociaux. "C'est inconstructible, dit-il. C'est du marketing."�Comme quand l'ex-ministre de l'�cologie NKM se targue, elle aussi, d'avoir la fibre verte.
Non, le seul, le vrai �colo dans cette �lection, c'est lui, dit Najdovski ! Qui, fid�le � lui-m�me, se fixe des ambitions modestes, mais r�alistes et � sa port�e � en croire les sondages : �tre au soir du premier tour le premier homme de l'�lection municipale. Autrement dit, se placer derri�re le duel Hidalgo- NKM , mais devant le candidat du FN � Paris.
Ma�l Thierry - Le Nouvel Observateur
Acheter Le nouvel Observateur
DOSSIER. Poutine, le tsar aux cent visages
Vincent Lambert, condamné au silence
Marseille : Gaudin peut-il perdre ?
ENQUÊTE. Barril, un barbouze au coeur du génocide
NOUVEAU !
