TITRE: Mali: un responsable du Mujao dit avoir enlev� une �quipe du CICR | Courrier international
DATE: 2014-02-12
URL: http://www.courrierinternational.com/depeche/newsmlmmd.urn.newsml.afp.com.20140211.efffb863.e281.4913.916a.e6fe2c3de37a.xml
PRINCIPAL: 165481
TEXT:
Mali: un responsable du Mujao dit avoir enlev� une �quipe du CICR
11.02.2014
| 12:06
| Par Serge DANIEL
Des soldats fran�ais en position dans une rue de Gao apr�s une alerte sur la pr�sence de membres du Mujao, le 13 avril 2013 dans le nord du Mali AFP/Archives
�
�
� Bamako (AFP)
Un responsable du Mouvement pour l'unicit� et le jihad en Afrique de l'Ouest (Mujao) a affirm� mardi � l'AFP avoir enlev� dans le nord du Mali une �quipe du Comit� international de la Croix-Rouge (CICR), dont cette ONG avait indiqu� �tre sans nouvelles depuis le 8 f�vrier.
"Nous avons pris (...) un (v�hicule) 4X4 des +ennemis de l'islam+ avec leurs complices", a d�clar� Yoro Abdoulsalam, responsable connu du Mujao, dans un bref entretien t�l�phonique avec un journaliste de l'AFP � Bamako.
A la question de savoir s'il s'agissait de l'�quipe du CICR, il a r�pondu: "Oui". "Ils sont en vie et en bonne sant�", a-t-il dit, sans plus de d�tails.
Lundi, le CICR avait annonc� avoir "perdu le contact avec un de ses v�hicules, avec cinq personnes � son bord".
Les cinq personnes sont quatre membres du CICR et un v�t�rinaire d'une autre organisation humanitaire, tous des Maliens, et leur disparition remonte au 8 f�vrier, alors que le v�hicule effectuait le trajet entre Kidal (extr�me nord-est) et Gao (nord-est), d'apr�s un porte-parole du CICR, Alexis Heeb.
Tous "�taient partis de Kidal pour regagner leur base � Gao lorsque nous avons perdu le contact", avait pr�cis� dans un communiqu� Christoph Luedi, chef de la d�l�gation du CICR au Mali, en affirmant n'�carter "aucune piste".
Selon Alexis Heeb, le CICR est en contact r�gulier avec les autorit�s maliennes, ainsi qu'avec les divers groupes arm�s op�rant dans le nord du Mali.
� AFP/Archives
Un soldat malien surveilledeux hommes suspect�s d'�tre membres du Mujao, le 22 f�vrier 2013 � Gao, dans le nord du Mali
Aucun commentaire n'avait imm�diatement pu �tre obtenu de sources officielles maliennes ou s�curitaires.
Le Mujao est un des groupes alli�s � Al-Qa�da au Maghreb islamique (Aqmi) qui ont occup� le nord du Mali en 2012 avant d'en �tre en partie chass�s par une intervention militaire internationale lanc�e d�but 2013 � l'initiative de la France, et toujours en cours. Ce mouvement �tait particuli�rement pr�sent dans la ville de Gao, et dans sa r�gion.
Retour du Mujao sur le terrain
Ces all�gations d'enl�vement interviennent sur fond de tensions dans la r�gion de Gao, o� plusieurs habitants �voquent un retour du Mujao sur le terrain.
� AFP/Archives
Des combattants du Mujao, pr�s de l'a�roport de Gao, dans le nord du Mali, le 7 ao�t 2012
Un responsable au gouvernorat de la r�gion a indiqu� que lundi, des dizaines de combattants arm�s suppos�s membres du Mujao avaient fait irruption dans la localit� de Dj�bock, � une cinquantaine de kilom�tres de Gao.
Ils �taient � la recherche d'un chef touareg absent au moment de leur passage. Apr�s deux heures de pr�sence sur les lieux sans �tre inqui�t�s, ils sont repartis, a indiqu� la m�me source, sans plus de d�tails.
Dans un communiqu� publi� lundi, le Mouvement national de lib�ration de l'Azawad (MNLA, r�bellion touareg) avait accus� le Mujao d'�tre auteur du massacre d'une trentaine de "civils touareg" le 6 f�vrier dans la localit� de Tamkoutat (nord-est de la ville de Gao), proche de Dj�bock.
Il ajoutait que "suite � ce massacre terroriste", ses troupes avaient engag� une course poursuite contre les assaillants" les 8 et 9 f�vrier, en tuant six et faisant deux prisonniers "arabes". La r�bellion a �voqu� un mort et un bless� dans ses rangs, et indiqu� que certains assaillants avaient fui et travers� la fronti�re du Niger voisin.
Un �lu et un ex-d�put� de la r�gion de Gao avaient indiqu� � l'AFP la semaine derni�re qu'au moins 30 Touareg avaient �t� tu�s le 6 f�vrier lors de violences entre communaut�s peule et touareg dans la zone de Tamkoutat.
� AFP
Carte de localisation de l'enl�vement d'une �quipe du CICR au Mali entre Kidal et Gao
Le gouvernement malien avait de son c�t� d�nonc� "des actes terroristes" - sans citer de noms de groupes - ayant tu� une trentaine de marchands revenant d'un grand march� de la m�me zone, la force de l'ONU (Minusma) �voquant 24 morts dans des "affrontements intercommunautaires". Ni le gouvernement ni la Minusma n'avaient pr�cis� la communaut� d'origine des victimes.
Le vaste Nord malien reste instable, plus d'un an apr�s l'intervention militaire internationale. Des �l�ments jihadistes ont plusieurs fois frapp� ces derniers mois dans ces zones, notamment � Kidal, chef-lieu de r�gion et fief des Touareg qui reste la ville de tous les dangers o� l'arm�e et l'administration maliennes peinent toujours � imposer leur autorit�.
�
