TITRE: Le Drian en appui de la pr�sidente de la RCA dans l�int�rieur du pays  - France - RFI
DATE: 2014-02-12
URL: http://www.rfi.fr/afrique/20140212-le-drian-centrafrique-sangaris-samba-panza-bangui-anti-balaka/
PRINCIPAL: 167805
TEXT:
Modifi� le 12-02-2014 � 17:10
Le Drian en appui de la pr�sidente de la RCA dans l�int�rieur du pays
par RFI
Le ministre fran�ais de la D�fense, Jean-Yves Le Drian, lors d'une de ses pr�c�dentes visites � Bangui.
AFP PHOTO/FRED DUFOUR
R�publique centrafricaine
Jean-Yves Le Drian est � Bangui ce mercredi alors que le Programme alimentaire mondial vient de lancer un pont a�rien entre le Cameroun et la R�publique centrafricaine. C'est la troisi�me visite du ministre fran�ais de la D�fense depuis le d�clenchement de l'op�ration Sangaris le 5 d�cembre.
Le ministre fran�ais de la D�fense est arriv� � 7 heures ce mercredi matin. Jean-Yves Le Drian s�est d�abord entretenu avec le g�n�ral Soriano et les autres responsables de l�op�ration Sangaris.
Il s�est ensuite rendu dans le quartier de Benz-Vi, dans le 5e arrondissement de Bangui, pour inaugurer�un centre d�accueil de nuit pour d�plac�s, cens� h�berger des habitants du quartier qui survivent aujourd�hui dans le camp de r�fugi�s situ� pr�s de l�a�roport avec le patron du Haut commissariat aux r�fugi�s (HCR) Antonio Guterres, le chef de la Misca, le g�n�ral Mokoko et des repr�sentants du gouvernement centrafricain.
Am�lioration de la situation � Bangui
L�objectif de ce centre d'une capacit� de 3�000 places est de d�sengorger l�a�roport. D'autres devraient voir le jour dans d�autres quartiers de la capitale. Pour le ministre fran�ais de la D�fense, c�est un signe de l�am�lioration de la situation � Bangui :��C�est la troisi�me fois que je viens � Bangui depuis le d�but de l�intervention Sangaris, mais c�est la premi�re fois que le g�n�ral Soriano me laisse sortir de Mpoko��, a ironis� Jean-Yves Le Drian, qui a insist� aussi sur la n�cessit� de r�gler en m�me temps les probl�mes s�curitaires et humanitaires.
La d�l�gation fran�aise a ensuite �t� re�ue par la pr�sidente de la transition centrafricaine Catherine Samba-Panza au palais de la Renaissance pour un entretien � huis clos. Apr�s quoi, elle s'est envol�e en h�licopt�re avec le ministre fran�ais pour Mba�ki, dans le sud-ouest du pays. Le Puma s�est pos� sur le stade municipal de cette localit� � 85 kilom�tres de Bangui. Pour la pr�sidente de la transition, c�est une premi�re en dehors de la capitale.
Catherine Samba-Panza veut ��aller en guerre�� contre les anti-balaka
L'accueil a �t� chaleureux, tout le village est sorti le long de l�art�re principale. Les Fran�ais sont arriv�s le 29 janvier dernier � Mba�ki. Ils commencent � pr�sent � passer le relais � la Misca, un contingent congolais. Il y a aussi ce mercredi des troupes rwandaises qui assurent la s�curit� rapproch�e des autorit�s.
La pr�sidente de la transition s'est exprim�e en sango devant la population. Catherine Samba-Panza  a rappel� que les milices devaient �tre d�sarm�es. ��Si au d�but quelques patriotes ont pris les armes face aux ex-Seleka, nous assistons � pr�sent � une prolif�ration de bandits de grand chemin. Cela n�est pas acceptable��, a-t-elle dit, indiquant vouloir ��aller en guerre�� contre les milices d'autod�fense anti-balaka � l'origine d'exactions envers la communaut� musulmane notamment.
La force Sangaris, elle, se d�ploie � pr�sent dans l�Ouest de la Centrafrique, � l�Ouest d�une ligne Sibut-Kaka Bandoro, alors que la Misca, elle, est pr�sente � l�est.
