TITRE: Un ex-gendarme condamné à perpétuité
DATE: 2014-02-12
URL: http://www.lefigaro.fr/flash-actu/2014/02/12/97001-20140212FILWWW00256-un-ex-gendarme-condamne-a-perpetuite.php
PRINCIPAL: 167618
TEXT:
le 12/02/2014 à 16:21
Publicité
L'ex-gendarme Daniel Bedos, 57 ans, jugé à Toulouse pour le meurtre d'une dame âgée de 97 ans, a été condamné cet après-midi à la prison à perpétuité. 
Les assises ont reconnu Daniel Bedos coupable de l'ensemble des chefs d'accusation ainsi que des faits de subornation de témoin, et sont allés au-delà des réquisitions de l'avocat général qui avait réclamé contre lui trente années de réclusion criminelle .
Le procès de l'homme, accusé d'avoir porté des coups mortels à une nonagénaire pour lui voler ses bijoux, a débuté lundi devant la cour d'assises de la Haute-Garonne , en présence du principal témoin à charge. Daniel Bedos dément catégoriquement s'être introduit chez Suzanne Blanc le 18 août 2010 à Toulouse. Le lendemain de l'agression, la victime, qui était encore autonome malgré son âge, avait été retrouvée gisant au sol, le visage tuméfié mais encore en vie. Elle succombait le jour suivant à un "traumatisme cranio-facial grave". Des bijoux avaient disparu de son domicile et c'est en remontant leur trace, dans un magasin d'achat-vente, que les enquêteurs ont identifié l'ex-gendarme, formellement reconnu par l'employé du Cash Converters, où il avait mis en vente des bijoux appartenant à Mme Blanc pour 1.231 euros.
