TITRE: Critique - "La Belle et la B�te"�: une version criarde et vulgaire - Cin�Obs
DATE: 2014-02-12
URL: http://cinema.nouvelobs.com/articles/29847-critique-critiques-la-belle-et-la-bete-une-version-criarde-et-vulgaire
PRINCIPAL: 166420
TEXT:
��Copyright�: �2014 ESKWAD � PATH� PRODUCTION � TF1 FILMS PRODUCTION ACHTE / NEUNTE / ZW�LFTE / ACHTZEHNTE BABELSBERG FILM GMBH � 120 FILMS
Mots-cl�s�: L�a Seydoux, Vincent Cassel, Christophe Gans
Fans de Hayao Miyazaki , passez votre chemin. Quoique Christophe Gans se revendique du ma�tre japonais pour cette nouvelle adaptation de " la Belle et la B�te ", la po�sie du r�alisateur de " Mon voisin Totoro " est loin d��tre au rendez-vous. Les inconditionnels de la version de Cocteau (1946) en seront eux aussi pour leurs frais�: ici, le merveilleux n�a pas cours. Sous les traits un peu vulgaires d�une cr�ature de synth�se, une t�te de lion assez moche, la B�te de Christophe Gans n�a rien de suffisamment humain pour nous convaincre (et la Belle avec nous) de la beaut� de son �me et des raisons de la d�livrer de l�enveloppe �laquelle un sort l�a condamn�e.
Tout en restant factuellement fid�le au conte de Gabrielle-Suzanne de Villeneuve, Gans le revisite �sa mani�re habituelle, bruyante, spectaculaire, tape-�-l��il et sans finesse. La profusion des effets sp�ciaux s�duira �coup s�r les 8-10�ans mais effraiera les plus jeunes. Une toute petite cible pour un si beau conte� On attend maintenant la version du Mexicain Guillermo del Toro avec Emma Watson .
?
