TITRE: Les 3 fr�res, le retour : Les 5 meilleurs sketchs des Inconnus
DATE: 2014-02-12
URL: http://www.staragora.com/news/les-3-freres-le-retour-les-5-meilleurs-sketchs-des-inconnus/479109
PRINCIPAL: 167074
TEXT:
0
Le fil "Les 3 fr�res, le retour", sort mercredi 12 f�vrier au cin�ma. l'occasion pour se rem�morer les meilleurs sketchs des inconnus qui nous ont fait mourir de rire pendant des ann�es.
Annonc�e depuis des ann�es, la suite du film Les 3 fr�res sort mercredi 12 f�vrier. Le premier volet des aventures de Pascal Legitimus , Bernard Campan et Didier Bourdon , alias Les Inconnus, qui incarnent 3 fr�res oblig�s de faire connaissance pour, remonte � �1995 et avait connu un vif succ�s au cin�ma, avec pr�s de 7 millions d'entr�es.�Vingt ans plus tard, Pascal, Didier et Bernard Latour doivent cohabiter afin de percevoir l'h�ritage de leur m�re.
�a faisait bien longtemps que l'on n'avait pas revu les Inconnus au grand complet, ensemble, pour le meilleur et pour le rire. Des ennuis judiciaires et une bataille financi�re sont pass�s par l�, mais difficile d'oubli les sketchs et l'alchimie qui unissent les 3 copains. En attendant d'aller voir�Les 3 fr�res , le retour, ce 12 f�vrier au cin�ma, payez-vous une bonne tranche de rire avec le top 5 des meilleurs sketchs des Inconnus
