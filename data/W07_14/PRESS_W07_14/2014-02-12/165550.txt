TITRE: SNCF: La Cour des comptes �pingle les billets de train gratuits pour les proches des cheminots - Economie - La Voix du Nord
DATE: 2014-02-12
URL: http://www.lavoixdunord.fr/economie/sncf-la-cour-des-comptes-epingle-les-billets-de-train-ia0b0n1912870
PRINCIPAL: 165544
TEXT:
- A +
La Cour des comptes a �pingl� au nom de l'�galit� d'acc�s au service public ferroviaire les avantages tarifaires accord�s par la SNCF aux proches de ses salari�s, tout en reconnaissant que la "sensibilit� sociale du sujet" freine toute tentative de modernisation.
Dans son rapport publi� mardi, elle rel�ve que ces "facilit�s de circulation", � savoir la gratuit� ou des tarifs pr�f�rentiels accord�s par la SNCF � ses personnels et � leurs proches, profitent d�sormais davantage aux ayants droit des cheminots qu'aux cheminots eux-m�mes, ce qui "pose probl�me au regard de l'�galit� d'acc�s au service public ferroviaire".
Accord�es par la SNCF depuis sa cr�ation en 1938 � ses personnels, ces facilit�s de circulation constituent � pr�sent "un ensemble touffu, marqu� par l'accumulation de concessions successives dans le cadre du dialogue social, et, de ce fait, rec�lant nombre d'archa�smes et d'incoh�rences", estime la Cour.
Et si elle reconna�t un effort de clarification entrepris depuis 2009, elle rel�ve que "la sensibilit� sociale du sujet freine toute tentative de simplification et a dissuad� jusqu'� pr�sent toute r�flexion sur la modernisation d'ensemble d'un dispositif vieux de 75 ans".
Au point que les cheminots en activit� ne repr�sentent plus que 21,5% des b�n�ficiaires (163.005 personnes), derri�re les retrait�s (24,3%) et largement derri�re les ayants droit, 54,1% avec 409.000 personnes b�n�ficiaires, selon la Cour.
La Cour pointe m�me que si le nombre de b�n�ficiaires a diminu� de 10% depuis 2009, passant de 838.939 � 756.576 en 2011, ces chiffres ne recensent que ceux qui b�n�ficient automatiquement du dispositif (partenaires de couple et enfants de moins de 21 ans) et ne prennent pas en compte ceux qui doivent en faire la demande, soit plus de 340.000 personnes.
Un "droit" inscrit dans le "contrat du cheminot"
"Le total des b�n�ficiaires des facilit�s de circulation s'�tablissait donc, � la fin de 2011, � plus de 1.100.000 personnes, dont seulement environ 15% de cheminots en activit�", observe la Cour.
De leur c�t�, les syndicats de la SNCF d�fendent un "droit" inscrit dans le "contrat du cheminot".
La Cour des comptes "ferait mieux de s'occuper des vraies d�penses publiques inacceptables", estime ainsi Pascal Poupat de la CGT-Cheminots (premier syndicat de la SNCF).
Selon lui, pour "chercher des �conomies de bouts de chandelle", la Cour "s'attaque � un droit" octroy� pour compenser "la mobilit� des cheminots qui font tourner le service public 24h/24 et 7 jours/7".
"On cherche � faire passer les cheminots pour des nantis", a de son c�t� r�agi Alain Cambi (SUD-Rail, troisi�me syndicat).
M�me agacement � l'Unsa (deuxi�me syndicat), pour qui la Cour "remet la pression sur la SNCF qui n'�value pas financi�rement de la m�me fa�on qu'elle le co�t de ces billets", a comment� Thierry Marty.
"Pour la Cour des comptes, pour tous les puissants et les importants c'est le salari� qui est le pel�, le galeux � qui il faut jeter des pierres", a abond� le pr�sident du Parti de Gauche Jean-Luc M�lenchon.
"Maintenant on en est aux tickets de train des employ�s de la SNCF", a-t-il poursuivi en marge d'un d�placement �lectoral � Bergerac (Dordogne). "Mais jusqu'o� on va aller comme �a? (...) Les paies de cheminots sont excessives, et bien que les membres de la Cour des comptes essayent de vivre avec des paies de cheminots, on leur donnera m�me les tickets gratuits", a-t-il ajout� avant de lancer: "Ah zut! ils les ont d�j�".
Dans son rapport, la Cour recense plusieurs m�thodes et hypoth�ses pour des "�valuations de l'impact total (...) sur les comptes de la SNCF (qui) vont d'une cinquantaine de millions d'euros � des sommes d�passant significativement 100 millions", soit nettement plus que les 9,7 millions de co�t direct et les 21 millions de manque � gagner �voqu�s par la SNCF.
Elle recommande donc une remise � plat du dispositif et notamment de supprimer les facilit�s accord�es aux ascendants et de rendre non automatiques celles accord�es aux autres ayants droit.
Dans une r�ponse � la Cour, le pr�sident de la SNCF Guillaume Pepy rel�ve que ces facilit�s "constituent un sujet identitaire, �troitement li� � l'entreprise, et plus largement � l'histoire du monde ferroviaire".
"A la suite d'un pr�c�dent rapport de la Cour, SNCF a engag� un important effort de modernisation de la gestion de ces facilit�s", ajoute-t-il en soulignant que "ces chantiers d'am�lioration et de rationalisation se poursuivent".
R�agir � l'article
