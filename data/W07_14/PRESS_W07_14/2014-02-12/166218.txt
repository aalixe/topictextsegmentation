TITRE: La saison 2 de Person of Interest au dessus des 6 millions sur TF1 - News TV - Toutelatele.com
DATE: 2014-02-12
URL: http://www.toutelatele.com/la-saison-2-de-person-of-interest-au-dessus-des-6-millions-sur-tf1-56795
PRINCIPAL: 166216
TEXT:
Mots-cl�s : tf1 �|� audiences tv �|� series �|� person of interest �|�
TF1 proposait une nouvelle soir�e Person of Interest Le mardi 11 f�vrier, compos�e de deux �pisodes in�dits de la saison 2, suivis d�une rediffusion de la premi�re saison.
D�s 21h01, dans ��Etat critique��, la Le docteur Maddy Enright �tait d�sign�e par la machine. Un odieux chantage lui �tait alors soumis�: elle devait tuer un riche industriel au cours d�une op�ration chirurgicale. Cette course contre la montre a captiv� 6.40 millions de t�l�spectateurs, soit 23.1% de part d�audience aupr�s de l�ensemble du public.
� 21h50, TF1 proposait ��Gages d�amour�� au cours duquel Reese surprenait un inconnu posant un explosif artisanal sur la voiture d�une personne d�sign�e par la machine. Finch comprenait rapidement que cette tentative d�assassinat �tait commandit�e par le mari de cette derni�re. 5.45 millions de Fran�ais �taient alors devant la cha�ne priv�e, repr�sentant 21.5% du public alors pr�sent devant son t�l�viseur.
Pour terminer la soir�e, dans ��Fen�tre sur cour��, John �tait transport� d�urgence � l�h�pital. Ce point de d�part a offert � TF1 une moyenne de 3.67 millions de Fran�ais, pour 19.1% de part d�audience sur les 4 ans et plus.
Avec Person of Interest , TF1 se place en t�te des audiences en prime time. En moyenne les deux �pisodes in�dits ont rassembl� 5.9 millions de t�l�spectateurs, soit 22.3% de part d�audience aupr�s de l�ensemble du public et 27%aupr�s desfemmes de moins de 50 ans responsables des achats.
Mardi prochain, la s�rie am�ricaine sera de retour sur TF1 d�s 20h50.
