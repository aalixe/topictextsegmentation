TITRE: Monaco : Un groupe avec Obbadi et Echiejile contre Nice - Afrik-foot.com : l'actualit� du football africain
DATE: 2014-02-12
URL: http://www.afrik.com/monaco-un-groupe-avec-obbadi-et-echiejile
PRINCIPAL: 168290
TEXT:
mercredi 12 f�vrier 2014 / 16:04
Ce mercredi soir (20h55) se d�roulera le 8e de finale de Coupe de France entre Nice et Monaco. Et pour ce court d�placement, le club du Rocher va tenter de l�emporter avec dans un premier temps un groupe de 23 joueurs qui sera par la suite r�duit � 18. Nous pouvons noter la pr�sence du Nig�rian Elderson Echiejile et des Marocains Mounir Obbadi et Nabil Dirar.
Le groupe de Monaco�: Roma, Romero, Subasic - Abidal, Carvalho, Echiejile, Fabinho, Isimat-Mirin, Kurzawa, Raggi - Dirar, Ferreira-Carrasco, Kondogbia, Moutinho, Obbadi, Ocampos, Pi, James Rodriguez, Toulalan - Martial, Berbatov, Germain, Rivi�re.
