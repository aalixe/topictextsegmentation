TITRE: Perturbations du r�seau TER�: la R�gion Midi-Pyr�n�es va au clash avec la SNCF - Toul�co
DATE: 2014-02-12
URL: http://www.touleco.fr/TER-la-Region-Midi-Pyrenees-va-au-clash-avec-la-SNCF,12772
PRINCIPAL: 165614
TEXT:
Publi� le mardi 11 f�vrier 2014 �  21h49min par La R�daction
Perturbations du r�seau TER�: la R�gion Midi-Pyr�n�es va au clash avec la SNCF
La R�gion Midi-Pyr�n�es et son pr�sident Martin Malvy tapent du poing sur la table. La collectivit� a d�cid� de suspendre ses paiements � la SNCF pour l�exploitation des trains TER. Une ��d�cision exceptionnelle�� justifi�e dans un communiqu� par ��la d�gradation du service observ�e��.  Cette mesure choc�(...)
