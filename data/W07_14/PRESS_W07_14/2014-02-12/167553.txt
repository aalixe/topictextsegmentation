TITRE: Bricolage : l'ouverture des magasins le dimanche suspendue par le Conseil d'Etat
DATE: 2014-02-12
URL: http://www.leparisien.fr/economie/bricolage-l-ouverture-des-magasins-le-dimanche-suspendue-par-le-conseil-d-etat-12-02-2014-3584227.php
PRINCIPAL: 0
TEXT:
Bricolage : l'ouverture des magasins le dimanche suspendue par le Conseil d'Etat
�
R�agir
C'est toujours la confusion concernant l'ouverture des magasins de bricolage le dimanche. Ce mercredi, le Conseil d'Etat a suspendu le d�cret autorisant leur ouverture . Il a estim� dans un communiqu� qu'il �existait un doute s�rieux sur la l�galit� de ce d�cret autorisant temporairement les �tablissements de commerce de d�tail du bricolage � d�roger � la r�gle du repos dominical.
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Travail le dimanche : les magasins de bricolage officiellement autoris�s � ouvrir
A la demande des syndicats , il a d�cid� de �suspendre l'ex�cution du d�cret�. Pour le Conseil d'Etat,��le principe d�un repos hebdomadaire est l�une des garanties du droit constitutionnel au repos reconnu aux salari�s� et �ce droit s�exerce en principe le dimanche�. Le gouvernement pr�parerait un nouveau d�cret.
Un accord entre patronat et syndicats avait �t� trouv� fin janvier
Fin d�cembre 2013, le gouvernement avait publi� ce d�cret autorisant une ouverture les dimanches �pour ces types de magasins en France et ce jusqu'au 1er juillet 2015, le temps de l�gif�rer pour mettre de l'ordre dans le maquis des d�rogations en vigueur.�Le site Internet du minist�re du Travail pr�cisait que �cette d�rogation vise � apporter, � titre transitoire, un cadre juridique stable pour les ouvertures dominicales constat�es dans ce secteur dans l'attente d'une refonte globale de nature l�gislative des d�rogations au repos dominical, qui doit rester la r�gle g�n�rale�.
Les ministres soulignaient que seuls les salari�s volontaires pourraient travailler le dimanche avec pour contreparties �le doublement au minimum de la r�mun�ration�, l'attribution d'un repos compensateur et des engagements en termes d'emploi et d'acc�s � la formation. La mesure avait provoqu� la col�re des syndicats. A l'issue de n�gociations serr�es le 23 janvier dernier, patronat et syndicats �taient parvenus � un accord sur les contreparties du travail le dimanche.
Cet accord garantissait aux salari�s �le droit au refus (de travailler les dimanches) qui ne pouvait entra�ner aucune discrimination ou sanction�, �la r�versibilit� � tout moment de l'ann�e avec un pr�avis d'un mois� et �un �gal acc�s au travail le dimanche pour les volontaires� par syst�me de �roulement�.
VIDEO. D�but des n�gociations sur le travail dominical
R�agir avec mon compte You / le Parisien
Identifiant
Votre e-mail* (ne sera pas visible)
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
D�connexion
Votre r�action*
Je d�clare avoir pris connaissance et avoir approuv� la Charte de mod�ration et j'accepte que ma r�action soit publi�e dans le Parisien / Aujourd'hui en France.*
*Champs obligatoires
R�ponse Signaler un abus
fmarie-anne 13/02/2014 - 12h12
por une fois que des salari�s veulent travailler , faut les laisser d habitude ils glandent et les cam�ras de surveillance devraient �tre autoriser pour d�busquer les tricheurs
R�ponse Signaler un abus
phil1957 13/02/2014 - 11h24
ou est le probleme dans l'industrie on travaillent le dimanche pour les postes ???? et on peut considere que les grandes surfaces sont des industries aussis en plus il y a toujour des volontaires pour travailler le dimanche  j'en connait meme qui ne travaillent que le samedi et le dimanche  et faut dire aussis que certains etudiants sont contraints economiquement de travailler le dimanche  en plus celas offre un service a ceux qui n'ont le temp de faire des courses que le le samedi ,dimanche et celas augmente le chiffre d'affaire donc les impots ?????
R�ponse Signaler un abus
larebelle 13/02/2014 - 11h03
�  ceux qui sont pour la fermeture des magasins de bricolage le dimanche, je leur conseille de demander �galement la fermeture dominicale des restaurants, des parcs d'attraction, des cin�mas, des transports en commun, ....... .
R�ponse Signaler un abus
voteur 13/02/2014 - 11h01
Compl�tement incroyable cette dictature avec le conseil d'�tat, la presse et la justice compl�tement � sa solde, n'y a t il plus de pouvoir ind�pendant voire de contre pouvoirs??? Encore une concurrence d�loyale, casto et Leroy merlin sont autorises � ouvrir le dimanche, la ou une vraie partie du �A est r�alis� et on l interdit � l un de leur concurrent qui en plus est un poids mouche � c�t� des deux. �a chauffe dans les chaumi�res.
R�ponse Signaler un abus
Kerops 13/02/2014 - 10h12
Pourquoi ceux qui ach�tent le dimanche ne bossent t'ils pas? Ceux qui bossent le dimanche, c'est en raison de leur salaires qui n'�voluent pas! Si mon patron s'habitue � que je bosse le dimanche, un jour il me demandera de bosser la nuit? Le pouvoir d'achat des acheteurs n'augmente pas si je bosse le dimanche! Si je bosse le dimanche, mon coll�gue qui ne veut pas sera "mal vu" par la direction...pour conclure, pas besoin du MEDEF, le monde salariale s'auto d�truit sans aide.....
R�ponse Signaler un abus
vachalait 13/02/2014 - 10h11
Va t on r�apprendre dans ce pays le mot AUTORISER alors que l'on entend que INTERDIRE? La France, le pays de l'interdiction!!!
R�ponse Signaler un abus
memoire 13/02/2014 - 09h57
Le probl�me du travail le dimanche est un serpent de mer...Il date des ann�es 70...et de la cr�ation des grandes surfaces en p�riph�rie des villes.Les gouvernements successifs, na�fs dans un premier temps tent�rent de r�gler le probl�me en vain. Simple au d�part , il se complique irr�m�diablement . Pourquoi? Il faut juste prendre en compte qu'il met en sc�ne DES syndicats...il n'y a pas que les syndicats des salari�s, il y a aussi les syndicats patronaux...Les syndicats du petit commerce et les syndicats des grandes surfaces...Les int�r�ts ne sont pas les m�mes .La guerre est aussi patron contre patron. En 2001 ,une loi imposa la fermeture dominicale...par corporation et surtout pour les grandes surfaces ,de ce fait des d�rogations s'en suivirent pour aboutir � l'incoh�rence la plus totale .... Cette incoh�rence devient probl�matique des l'instant o� l'emploi est devenu le probl�me majeur de la soci�t�. Les emplois sont fournis par qui???? Petits commerces ???? Grandes distributions??? La guerre se fait � 3...je dis bien � 3. Les syndicats patronaux du petit commerce sont alli�s avec les syndicats salari�s CONTRE les syndicats patronaux de la grande distribution.  A vous de juger???un choix de soci�t�??? Avons nous les moyens de ce choix??? Agraver le ch�mage. ..par qui on commence??? !!!!! A vous messieurs les politiques.Serpent de Mer !!!!!
R�ponse Signaler un abus
cigale11 13/02/2014 - 09h54
fermer les magasins le dimanche et emp�cher les gents de travailler c'est grave, surtout il y avait des condition sp�cial, garder nos avantages des aquits c'est bien, mais emp�cher les jeunes �tudiants de travailler c'est du aux syndicats
