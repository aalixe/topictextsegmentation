TITRE: Ayrault veut faire une "chance" du vieillissement de la France | Site mobile Le Point
DATE: 2014-02-12
URL: http://www.lepoint.fr/fil-info-reuters/ayrault-veut-faire-une-chance-du-vieillissement-de-la-france-12-02-2014-1790998_240.php
PRINCIPAL: 167791
TEXT:
Ayrault veut faire une "chance" du vieillissement de la France
par Guillaume Frouin
ANGERS, Maine-et-Loire ( Reuters ) - Jean-Marc Ayrault a présenté mercredi les contours de la future loi d'adaptation de la société français au vieillissement de la population, qui privilégie le maintien à domicile des personnes dépendantes sans transfert vers les assurances privées.
L'avant-projet de loi d'orientation et de programmation, qui fera l'objet d'une communication vendredi en conseil des ministres, entend répondre aux besoins d'un pays qui compte aujourd'hui 15 millions de personnes âgées de plus de 60 ans.
Elles seront 20 millions en 2030, 24 millions en 2060.
"La priorité des priorités est donnée au maintien à domicile", a dit le Premier ministre lors de la visite d'une résidence publique pour personnes âgées à Angers (Maine-et-Loire).
Jean-Marc Ayrault a annoncé à cette fin une réforme de l'Allocation personnalisée d'autonomie (Apa) "qui garantisse une prestation plus généreuse et plus facile d'accès".
Le gouvernement entend ainsi débloquer 375 millions d'euros supplémentaires chaque année pour l'Apa à domicile, avec un objectif : "Plus d'heures, mieux d'heures, et des heures moins coûteuses pour le bénéfice des âgés et des professionnels".
La réforme de l'Apa prévoit une "aide au répit" pour les proches de personnes âgées dépendantes, notamment les malades d'Alzheimer, qui se verront accorder une aide leur permettant de financer un hébergement temporaire pour la personne dépendante.
"Dans ce projet de loi, nous avons écarté sans ambiguïté le transfert - que certains préconisaient - vers des assurances privées", a expliqué Jean-Marc Ayrault.
"Ce n'est pas contre les assurances privées, mais elles doivent rester facultatives et subsidiaires", a-t-il ajouté. "Dans une période de contrainte financière, c'était un choix vraiment délicat, mais que nous avons décidé d'assumer."
"INVENTER LA MAISON DE RETRAITE DE DEMAIN"
La loi de programmation et d'orientation - qui doit être examinée en avril au conseil des ministres - inclut des mesures de soutien aux professionnels de l'aide à domicile dont les frais de déplacement seront, par exemple, mieux pris en charge.
Le texte entend aussi "inventer collectivement la maison de retraite de demain", des établissements "plus ouverts sur la cité, qui jouent un rôle structurant dans le parcours des personnes âgées et accessibles à tous les Français", a dit Jean-Marc Ayrault.
Un Haut Conseil de l'âge et des Conseils départementaux de la citoyenneté et de l'autonomie seront également créés pour faciliter la coordination des différentes politiques publiques (logement, transports, urbanisme...) relatives à "l'adaptation de la société au vieillissement".
Un Plan national d'adaptation du logement sera lancé pour aménager d'ici 2017 quelque 80.000 logements en réponse aux besoins particuliers des personnes âgées.
Jean-Marc Ayrault a aussi lancé un appel à investir dans la "Silver economy" pour exploiter les opportunités d'affaires générées par le vieillissement de la population française.
"Cette nouvelle filière industrielle est un vrai gisement d'emplois", a dit le Premier ministre.
Pour Jean-Marc Ayrault, la future loi sera applicable dès 2015 "pour montrer aux Français que nous ne vivons pas cette nouvelle étape de l'histoire de nos sociétés comme une charge, comme un frein, mais comme une chance".
Edité par Yves Clarisse
