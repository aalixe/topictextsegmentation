TITRE: MIA ELECTRIC PLACE EN REDRESSEMENT JUDICIAIRE
DATE: 2014-02-12
URL: http://news.autoplus.fr/news/1479799/Redressement-judiciaire-Tribunal-Faillite-Mia-Electric-S%25C3%25A9gol%25C3%25A8ne-Royal
PRINCIPAL: 168073
TEXT:
2
Mercredi 12 f�vrier 2014 - 17:46
Le tribunal de commerce de Niort a plac� mia electric en redressement judiciaire. Le constructeur de voitures �lectriques pr�sente une tr�sorerie exsangue.
Ce que l'on craignait est finalement arriv�. Le fabricant de voitures �lectriques mia electric , ancienne branche automobile du groupe Heuliez, a �t� plac� en redressement judiciaire mercredi 12 f�vrier par le tribunal de commerce de Niort (Deux-S�vres) avec une p�riode d'observation jusqu'au 12 ao�t, a r�v�l� S�gol�ne Royal, pr�sidente de la r�gion Poitou-Charentes, actionnaire � hauteur de 12%.
Une tr�sorerie � sec ayant conduit � un retard dans le versement des salaires de certains des 210 employ�s est � l'origine de cette situation.
"On ne vend plus de voitures"
Les ventes automobiles sont elles aussi en berne avec seulement 201 unit�s �coul�es en 2013, alors que le nouvel actionnaire majoritaire de la marque, le consortium d'investisseurs internationaux Focus Asia GmbH, tablait lui sur 700 � 900 livraisons l'an pass�. "On ne vend plus de voitures depuis deux mois (...) Je n'ai jamais connu �a", a ainsi confi� � l'AFP Christophe Klein, syndicaliste CFE-CGC.
Il y a quelques jours, Michelle Boos, pr�sidente de la soci�t� de Cerizay, assurait pourtant avoir "436 voitures � livrer" mais aussi tout faire pour trouver les 890.000� n�cessaires � la p�rennit� de l'entreprise.
La bataille continue
"La mise en redressement judiciaire est la moins mauvaise solution et va nous permettre de mettre � plat les comptes et la strat�gie de l'entreprise", a d�clar� S�gol�ne Royal. "On ne baisse pas les bras (...) La bataille industrielle pour la voiture �lectrique 100% fran�aise continue", a-t-elle ajout�.
Sources: AFP et Reuters
