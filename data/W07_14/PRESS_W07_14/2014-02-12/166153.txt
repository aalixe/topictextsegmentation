TITRE: Retraite : les Fran�ais s'inqui�tent, leur �pargne diminue - Economie Matin
DATE: 2014-02-12
URL: http://www.economiematin.fr/ecoquick/item/8551-retraite-epargne-argent-placement-sondage
PRINCIPAL: 166149
TEXT:
E-mail
58 % des plus de 50 ans reconnaissent �pargner de moins en moins pour leur retraite. ShutterStock.com
Un paradoxe ! Alors que la crise relance les inqui�tudes des Fran�ais � propos de leur retraite, ils sont de moins en moins nombreux � �pargner pour leurs vieux jours.
�
D'apr�s une �tude Cecop-CSA pour le Cercle des �pargnants, les Fran�ais restent inquiets pour leurs vieux jours mais �prouvent de plus en plus de difficult� � �pargner � ce sujet. Ce qui d�montre que la r�forme des retraites, du moins la derni�re en date, celle qui est entr�e en vigueur et publi�e par le Journal officiel le 21 janvier dernier, n'a pas r�ussi � convaincre les Fran�ais qui sont 31 % � n'accepter aucune nouvelle mesure de r�forme.
67 % des Fran�ais pr�occup�s par leur �pargne-retraite
Ainsi, la majorit� des m�nages, soit 67 % des sond�s ont pour premi�re pr�occupation sociale leur retraite. Une proportion qui reste inchang�e par rapport � 2013. Une pr�occupation qui touche principalement les ouvriers et les jeunes, dont les revenus sont plus faibles que la moyenne.
58 % des plus de 50 ans �pargnent de moins en moins pour leur retraite
Pourtant, les sond�s d�clarent �pargner de moins en moins pour leur retraite. Ainsi seulement 53 % des 35-49 ans et 58 % des plus de 50 ans g�rent leur argent en vue de leur retraite et affirment se constituer un patrimoine financer pour leurs vieux jours. L'�tude du Cercle des �pargnants argue en la mati�re que l'�pargne-retraite s'est abaiss�e de 8 % depuis 2010.
L'assurance-vie, produit financier pr�f�r� des Fran�ais pour leurs vieux jours
En mati�re de placement, l'assurance-vie reste le produit d'�pargne pr�f�r� des Fran�ais pour leur retraite suivi par le Livret A, un peu multi-t�che malgr� le fait que son rendement, de 1,25 %, ne soit pas tr�s r�mun�rateur et certainement pas adapt� pour des �conomies � long terme. �
Jean-Baptiste Le Roux
Jean-Baptiste Le Roux est journaliste. Il travaille �galement pour Radio Notre Dame o� il s'occupe du site Internet. Il a travaill� pour Jalons, Causeur et Valeurs Actuelles avec Basile de Koch avant de rejoindre Economie Matin, � sa cr�ation, en mai 2012. Il est dipl�m� de l'Institut europ�en de journalisme et membre de l'Association des Journalistes de D�fense.
Tagg� sous
