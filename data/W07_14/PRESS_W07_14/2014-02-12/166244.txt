TITRE: Michael Schumacher. Le champion a contract� une infection pulmonaire
DATE: 2014-02-12
URL: http://www.ouest-france.fr/michael-schumacher-le-champion-contracte-une-infection-pulmonaire-1925248
PRINCIPAL: 166236
TEXT:
Michael Schumacher. Le champion a contract� une infection pulmonaire
France -
12 F�vrier
Le pilote de course est hospitalis� � Grenoble depuis un accident de ski � M�ribel (Savoie) le 29 d�cembre. �|�Photo AFP
Facebook
Achetez votre journal num�rique
Schumacher a contract� une infection pulmonaire la semaine derni�re � l'h�pital de Grenoble, o� il est soign� depuis son accident de ski.
Selon le quotidien allemand Bild, le champion allemand de Formule 1 dans le coma, Michael Schumacher, a contract� une infection pulmonaire la semaine derni�re � l'h�pital de Grenoble, o� il est soign� depuis son accident de ski le 29 d�cembre .
Les cons�quences pour l'�tat de sant� du septuple champion de F1 �g� de 45 ans ne sont pas pr�visibles, pr�cise Bild qui ne cite pas ses sources.
Processus de r�veil
De son c�t�, la porte-parole de Michael Schumacher, Sabine Kehm, s'est content� d'indiquer au journal : "Nous ne commentons pas les sp�culations".�
Fin janvier, les m�decins avaient entam� le processus de r�veil de l'ancien sportif de haut niveau, une phase qui pourrait durer longtemps. Mais l'h�pital de Grenoble avait d� d�mentir jeudi des rumeurs de la mort du champion diffus�es via les r�seaux sociaux.�
Lire aussi
