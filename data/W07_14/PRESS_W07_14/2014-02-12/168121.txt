TITRE: L1 / NANTES - Affaire Tour� : Match perdu pour Nantes
DATE: 2014-02-12
URL: http://www.football365.fr/france/infos-clubs/nantes/affaire-toure-match-perdu-pour-nantes-1102804.shtml
PRINCIPAL: 168116
TEXT:
L1 / NANTES - Publi� le 12/02/2014 � 18h20 -  Mis � jour le : 12/02/2014 � 18h53
114
Affaire Tour� : Match perdu pour Nantes
Comme s'y attendaient les Nantais, la Commission des comp�titions de la Ligue a donn� match perdu ce mercredi au FC Nantes dans le cadre de l'Affaire Abdoulaye Tour�. Nantes compte donc d�sormais 30 points et Bastia 36 points.
Comme attendu, Nantes a perdu ses trois points dans le cadre de l�affaire Abdoulaye Tour� . Suite � la proposition de conciliation du CNOSF, la Commission des comp�titions de la Ligue de football professionnel s�est r�unie ce mercredi pour statuer sur le dossier en pr�sence des avocats des deux clubs, le FC Nantes et le SC Bastia. Le 10 ao�t dernier lors de la 1�re journ�e de Ligue 1, les Nantais avaient fait entrer en fin de match Abdoulaye Tour�, pourtant suspendu. Apr�s la d�faite � la Beaujoire (2-0), les Bastiais avaient donc d�pos� une r�serve sur la participation du jeune joueur adverse. La commission concern�e a d�cid� en cons�quence mercredi de donner match perdu � Nantes, qui ne compte donc plus que 30 points au classement (et une diff�rence de buts de -2) recule en 12eme position. Avec d�sormais 36 points � son compteur (-3), Bastia grimpe lui � la 8eme place. Pour tenter de conserver le b�n�fice de sa victoire, le club nantais pourrait maintenant saisir le tribunal administratif et y plaider le vice de forme.
Communiqu� de la LFP
R�unie le 12 f�vrier 2014, la Commission des Comp�titions de la LFP a donn� match perdu par p�nalit� au FC Nantes (0 pt, 0 but marqu�) pour en reporter le b�n�fice au SC Bastia (3 points, 0 but marqu�) suite au litige opposant les deux clubs � l�issue de la rencontre FC Nantes � SC Bastia (1�re journ�e).
Lors de la rencontre FC Nantes-SC Bastia du 10 ao�t 2013, le SC Bastia avait port� une r�clamation sur la participation du joueur Abdoulaye Tour� (FC Nantes), qui s��tait vu infliger une sanction d�un match de suspension ferme � compter du 24 juin 2013 par la Commission F�d�rale de Discipline.
Le FC Nantes ayant d�cid� de contester cette d�cision de la Commission F�d�rale de Discipline, la Commission des Comp�titions de la LFP avait d�cid� de surseoir � sa d�cision lors de sa r�union du 11 septembre 2013.
Tour � tour, la Commission Sup�rieure d�Appel de la FFF (d�cision du 31 octobre 2013), puis le CNOSF (d�cision du 5 f�vrier 2014) ont confirm� la sanction d�un match de suspension ferme du joueur Abdoulaye Tour�.
En cons�quence, le joueur Abdoulaye Traor� ne pouvait �tre align� lors de la rencontre face au SC Bastia.
En cons�quence, et en application de l�article 510, le match perdu par p�nalit� entra�ne le retrait des trois points au FC Nantes et l�annulation des deux buts marqu�s. Le SC Bastia est d�clar� gagnant de la rencontre sans but marqu�, l�article 510 pr�voyant que l��quipe gagnante conserve le nombre de buts marqu�s au cours de la partie (en l�occurrence 0 puisque le match s��tait achev� sur le score de 2-0 pour le FC Nantes).
En cons�quence, le FC Nantes compte d�sormais 30 points et le SC Bastia 36 points.
R�dig� par Aur�lien CANOT Suivre @toto
Plus d'Actualit�s
