TITRE: Chez Disneyland, l'assemblée générale en version fan | Site mobile Le Point
DATE: 2014-02-12
URL: http://www.lepoint.fr/insolite/chez-disneyland-l-assemblee-generale-en-version-fan-12-02-2014-1791058_48.php
PRINCIPAL: 0
TEXT:
12/02/14 à 17h47
Chez Disneyland, l'assemblée générale en version fan
Il faut le voir pour le croire: des danseurs, de la musique, des fans qui font la queue pour une photo avec le PDG comme ils le feraient avec Mickey. L'assemblée générale des actionnaires de Disneyland Paris est un spectacle unique dans le monde financier.
Dans un grand auditorium du Walt Disney Studio à Marne-la-Vallée, un bon millier d'actionnaires étaient réunis mercredi pour ce rendez-vous annuel qui donne droit à une entrée gratuite valable toute la journée sur le site.
Assise sur les genoux de sa maman, une fillette attend le début du show. "Ca va venir", lui dit la maman.
"Le spectacle fait partie intégrante de l'assemblée générale. C'est une promesse tacite. Notre AG reflète la culture de l'entreprise", explique le porte-parole d'Euro Disney, la société qui exploite Disneyland.
La potion du jour comprend bien sûr des aspects formels. Devant un parterre bigarré où les costumes sont rares, le PDG Philippe Gas évoque le chiffre d'affaires, la perte d'exploitation, la perte nette... Disneyland a perdu un million de visiteurs avec la crise en 2013, passant à un peu moins de 15 millions.
La présidente du Conseil de surveillance, Virginie Calmels, juge "décevant" qu'il n'y ait toujours pas de bénéfice net. "Je ne suis pas la Fée clochette", lance-t-elle à un actionnaire désireux de savoir quand ce jour viendra.
Le dernier exercice à l'équilibre remonte à 2008. La dette reste colossale. Les actionnaires n'ont jamais vu le début d'un dividende en deux décennies. Mais Sylvie Gavel s'en fiche. Grande fan de Mickey, Pluto, Nemo & Co, cette femme de 57 ans vient chez Disneyland "toutes les semaines depuis vingt ans" et détient des actions "depuis le début", même si cet investissement s'avère "une catastrophe financière", dit-elle.
"Mes actions ne valent plus rien mais ce n'est pas grave. On récupérera peut-être dans 50 ans", confie Sylvie à l' AFP . Ce qu'elle préfère, c'est l'attraction "Pirate des Caraïbes" qu'elle fait chaque semaine. Elle collectionne aussi les Pin's de Disney qu'elle range par thèmes dans des classeurs.
Mickey, Minnie et Ratatouille
Didier Berger, policier de 38 ans venu de Lyon , est lui aussi un fan. "J'assume", lance-t-il. Au travail, il porte volontiers ses sweatshirts Disney. Son banquier n'a pas essayé de le dissuader d'investir dans Euro Disney: "je lui ai dit +je m'en fiche du dividende, ce n'est pas un placement. Disney c'est ma jeunesse, mon patrimoine". En décembre, Didier a fêté son anniversaire chez Mickey.
Euro Disney compte plus de 60.000 petits actionnaires, dont l'essentiel sont des fans, qui profitent ainsi d'avantages. Au micro mercredi, l'un réclame une réduction sénior, l'autre une réduction fidélité.
"On va regarder cela", répond le PDG.
Devant son auditoire, il déroule le rêve Disney avec ferveur. "Cette entreprise c'est ma vie", dit-il. On le croit. Philippe Gas vante "la capacité de Disneyland à donner de l'enthousiasme et du bonheur aux visiteurs". Il souligne le demi-milliard d'euros d'investissements réalisés pour des rénovations et des innovations depuis son arrivée aux commandes de la maison en 2008 -- "On ne rigole pas avec l'investissement".
Sur les genoux de sa maman, la fillette s'impatiente. Mais son supplice prend fin.
"Le moment est venu de partir au pays fabuleux de Rémy !", le petit rat de Ratatouille, annonce le PDG. Applaudissements. Disneyland Paris inaugurera cet été un nouveau quartier sur la thématique du héros imaginé par Pixar avec une méga-attraction et un restaurant dédié - pour un coût excédant 150 millions d'euros...
Ravis, les actionnaires découvrent dans un petit film animé quelques coulisses du chantier de "rat-a-too-ee" (la prononciation de Ratatouille en anglais, précisée comme telle sur l'écran, NDLR).
La musique crache. Puis arrive sur scène une vingtaine de danseurs acrobates, chacun affublé d'une toque, en écho au rat Rémy perdu dans un grand restaurant parisien. Sur un air invoquant même le french cancan, ils sautent, dansent, arborant des couteaux géants, des carottes, des fourchettes, des aubergines... Mickey et Minnie s'invitent à la fin du show. Les fans sont en joie.
En quittant la salle, l'un des actionnaires regrette que des toques n'aient pas été distribuées. L'an dernier, ils étaient repartis avec des oreilles de Mickey clignotantes.
