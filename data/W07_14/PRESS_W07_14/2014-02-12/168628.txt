TITRE: La Banque de France rassure sur la solidit� du Cr�dit agricole
DATE: 2014-02-12
URL: http://www.lemonde.fr/economie/article/2014/02/12/la-banque-de-france-rassure-sur-la-solidite-du-credit-agricole_4365331_3234.html
PRINCIPAL: 0
TEXT:
La Banque de France rassure sur la solidit� du Cr�dit agricole
Le Monde |
12.02.2014 � 21h49
Le gouverneur de la Banque de France , Christian Noyer , a tenu mercredi 12 f�vrier � rassurer sur le niveau de fonds propres du Cr�dit agricole et des autres banques fran�aises, sur BFM Business . ��Nous n'avons aucune inqui�tude �, a-t-il assur�.
Une �tude command�e par l'Organisation de coop�ration et de d�veloppement �conomiques (OCDE), r�v�l�e le 27 janvier par un hebdomadaire allemand, mettait pourtant s�rieusement en doute la solidit� de la�� banque verte��. Selon ses auteurs, deux �conomistes n�erlandais de renom, le groupe coop�ratif fran�ais repr�senterait � lui seul 27�% des besoins en capital des banques europ�ennes, suivi par la Deutsche Bank, avec 19 milliards d'euros de d�ficit � capitalistique �.
Lire (�dition abonn�s) : Une �tude controvers�e alerte sur la fragilit� du Cr�dit agricole
Pour Christian Noyer , � l'�tude qui a �t� r�alis�e qui citait le Cr�dit agricole parmi d'autres est une �tude qui est bourr�e d'erreurs, qui ne comprend rien � la fa�on dont les groupes mutualistes sont organis�s, qui (...) regardait les fonds propres d'une petite partie avec l'ensemble des cr�dits de la totalit� du groupe��. ��C'�tait �videmment faux.�� Un argumentaire m�thodologique qui rejoint le d�menti oppos� par le Cr�dit agricole apr�s la fuite de l' enqu�te : les deux �conomistes n'auraient pas tenu compte des fonds propres r�glementaires regard�s par les r�gulateurs, c'est-�-dire du ratio de solvabilit�, qui rapporte les fonds propres d'une banque � la somme des risques enregistr�s � son bilan.
De leur c�t�, les auteurs persistent et signent. S'ils reconnaissent avoir utilis� une m�thodologie diff�rente, ils estiment que la Banque centrale europ�enne (BCE) parviendra � des conclusions tr�s voisines � l'issue de ses prochains tests de r�sistance.
��Il n'y aucun probl�me avec le Cr�dit agricole, il n'y a aucun probl�me avec les banques fran�aises��, a encore assur� M.�Noyer. Le Cr�dit agricole ayant une structure diff�rente des autres grandes banques, des interrogations s'�l�vent pourtant r�guli�rement sur une �ventuelle sous-capitalisation, comme dans une �tude r�cente d'universitaires, contest�e par le Cr�dit agricole. ��Nous n'avons aucune inqui�tude��, a affirm� Christian Noyer , qui est �galement membre du conseil des gouverneurs de la Banque centrale europ�enne.
Secteur bancaire et assurance
