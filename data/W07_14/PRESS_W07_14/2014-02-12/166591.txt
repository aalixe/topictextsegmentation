TITRE: Angers. Le Premier ministre Jean-Marc Ayrault en visite ce matin � La Roseraie | Courrier de l'Ouest
DATE: 2014-02-12
URL: http://www.courrierdelouest.fr/actualite/angers-le-premier-ministre-jean-marc-ayrault-12-02-2014-149622
PRINCIPAL: 166587
TEXT:
Le Premier ministre Jean-Marc Ayrault est en visite mercredi � Angers.
Le Premier ministre Jean-Marc Ayrault est en visite mercredi � Angers.
Arriv�e au centre Robert-Robin a La Roseraie
Rencontre avec les r�sidants.
Mich�le Delaunay et Marisol Touraine � l'espace Robert-Robin.
Jean-Marc Ayrault �change avec des conjointes de malades d'alzheimer � l'espace Robert-Robin.
Le Premier ministre Jean-Marc Ayrault est en visite mercredi � Angers. Il est arriv� ce matin et visite actuellement un logement �volutif pour personnes �g�es d�pendantes, l�espace territorial "Bien Vieillir Robert Robin", dans le quartier de la Roseraie.
Il est accompagn� de Marisol Touraine, ministre des Affaires sociales et de la Sant�, et de Mich�le Delaunay, ministre d�l�gu�e charg�e des Personnes �g�es et de l'Autonomie.
Cette visite sert � illustrer "la n�cessit� de pr�parer d�s maintenant la soci�t� toute enti�re au vieillissement de la population".
Le Premier ministre doit pr�senter les principales mesures du projet de loi qui sera transmis dans les jours suivants au Conseil �conomique, social et environnemental (CESE).
