TITRE: L'Allemagne veut l�gif�rer sur les oeuvres spoli�es par les nazis - BFMTV.com
DATE: 2014-02-12
URL: http://www.bfmtv.com/international/lallemagne-veut-legiferer-oeuvres-spoliees-nazis-708532.html
PRINCIPAL: 165995
TEXT:
L'Allemagne veut l�gif�rer sur les oeuvres spoli�es par les nazis
S. D. avec AFP
r�agir
�branl�e par la r�v�lation de la d�couverte d'un "tr�sor nazi", l'Allemagne souhaite se doter d'une loi facilitant la restitution d��uvres d'art vol�es sous le III�me Reich pour que justice soit rendue, presque 70 ans apr�s la fin de la guerre.
Ce projet de loi, dont la discussion doit commencer ce vendredi, a �t� �labor� apr�s l'annonce en novembre de la d�couverte de 1.406 �uvres, en partie probablement issues de pillages nazis chez des juifs, au domicile d'un octog�naire, Cornelius Gurlitt, � Munich. Et l'affaire est loin d'�tre termin�e puisque mardi encore, �tait annonc�e la d�couverte de 60 �uvres, dont des Monet, Manet et Renoir dans la maison de Salzbourg (Autriche) de M. Gurlitt, fils d'un marchand d'art au pass� trouble sous le Troisi�me Reich.
Baptis�e "Lex Gurlitt" par les m�dias, le projet doit �tre d�taill� par l'Etat r�gional de Bavi�re devant le Bundesrat, la chambre haute du parlement qui repr�sente les L�nder.
Il propose notamment d'abolir la prescription de 30 ans au-del� de laquelle la propri�t� d'une �uvre d'art ne peut plus �tre contest�e, si le d�tenteur est consid�r� comme de "mauvaise foi", c'est-�-dire s'il connaissait la provenance de l'objet au moment de son acquisition.
�
