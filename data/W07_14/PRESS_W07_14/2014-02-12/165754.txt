TITRE: Libert� de la presse: une "l�g�re d�gradation globale" selon RSF - L'Express
DATE: 2014-02-12
URL: http://www.lexpress.fr/actualite/monde/liberte-de-la-presse-une-legere-degradation-globale-selon-rsf_1322933.html
PRINCIPAL: 0
TEXT:
Voter (0)
� � � �
"Les pays qui se pr�valent de l'Etat de droit ne donnent pas l'exemple, loin de l�", regrette Reporter sans fronti�res (RSF), qui d�plore dans sa derni�re �tude "un recul inqui�tant des pratiques d�mocratiques". Comme aux Etats-Unis, qui d�gringolent de 13 places � la 43e.
afp.com/Ben Stansall
Pour RSF, le combat continue. En publiant un nouveau palmar�s sur la libert� de la presse, l'organisation entend maintenir la pression sur le front du droit � l'information. Dans une p�riode  marqu�e par les graves conflits en Afrique et en Syrie .�
"Le classement de certains pays, y compris des d�mocraties, est largement affect� cette ann�e par une interpr�tation trop large et abusive du concept de la protection de la s�curit� nationale. Par ailleurs, le classement refl�te l'impact n�gatif des conflits arm�s sur la libert� de l'information et ses acteurs", a indiqu� Lucie Morillon, directrice de la recherche de Reporters sans fronti�res, � l'occasion de la publication du classement mondial annuel de RSF sur la libert� de la presse, portant sur 180 pays.�
L'indice annuel du classement, qui synth�tise les atteintes � la libert� de l'information, indique une "l�g�re d�gradation globale" dans le monde depuis l'an dernier, souligne l'ONG.�
Pays jug� le plus dangereux au monde pour les journalistes par RSF, la Syrie est class�e 177�me sur 180, figurant comme l'an dernier juste devant le trio inchang�: Turkm�nistan (178e), Cor�e du Nord (179e) et Erythr�e (180e).�
Pour la quatri�me ann�e, la Finlande conserve son rang de meilleur �l�ve, suivie comme l'an dernier par les Pays-Bas et la Norv�ge.�
Entre ces deux groupes de pays, les Etats situ�s en zones de conflits font sans surprise partie des plus touch�s. Outre la Syrie, le Mali chute de 22 places au 122e rang, tandis que la R�publique centrafricaine conna�t la chute la plus drastique du classement, perdant 43 places � la 109e.�
La Chasse aux sources
Loin de ces pays, certaines d�mocraties occidentales sont aussi point�es du doigt par RSF qui juge que "l'argument s�curitaire y est utilis� abusivement pour restreindre la libert� de l'information".�
"Les pays qui se pr�valent de l'Etat de droit ne donnent pas l'exemple, loin de l�", regrette l'organisation, qui d�plore "un recul inqui�tant des pratiques d�mocratiques". C'est le cas aux Etats-Unis, qui d�gringolent de 13 places � la 43e, et o� l'association d�nonce "la chasse aux sources et aux lanceurs d'alerte".�
"L'ann�e 2013 a connu un pic en termes de pression sur les journalistes et leurs sources" dans ce pays, a estim� Lucie Morillon, lors d'une rencontre avec la presse.�
La condamnation du soldat Bradley Manning � 35 ans de prison pour avoir transmis � WikiLeaks des milliers de documents, ou la traque d' Edward Snowden , � l'origine du scandale sur les �coutes men�es par l'agence nationale de s�curit� am�ricaine NSA, "sont autant d'avertissements � ceux qui oseraient livrer des informations dites sensibles, mais d'int�r�t public av�r�, � la connaissance du plus grand nombre", estime RSF.�
Le Royaume-Uni perd de son c�t� trois places pour se classer � la 33e. Ce pays s'est notamment "illustr� au nom de la lutte contre le terrorisme par des pressions scandaleuses exerc�es sur le quotidien The Guardian", rel�ve RSF. La France, qui perd une place � la 39e, a �galement connu une ann�e 2013 "inqui�tante", selon l'organisation de d�fense des journalistes.�
"Point d'orgue de l'ann�e 2013, la d�cision prise par la justice fran�aise de faire retirer les enregistrements de l'affaire Bettencourt des publications de Mediapart et du Point: une atteinte grave � la libert� de la presse", souligne-t-elle. La Gr�ce, affect�e par la crise �conomique et les pouss�es populistes, c�de quant � elle 14 places � la 99e.�
Juste derri�re, la Bulgarie (100e, -12 places) conserve le dernier rang des pays de l'Union europ�enne, au terme d'une ann�e marqu�e notamment par des agressions de journalistes par les forces de l'ordre en marge de manifestations.�
Ce classement repose sur sept indicateurs: niveau des exactions, �tendue du pluralisme, ind�pendance des m�dias, environnement et autocensure, cadre l�gal, transparence et infrastructures.�
�
