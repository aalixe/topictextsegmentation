TITRE: White: &quot;�a ne brisera pas ma carri�re&quot; - JO 2014 - Sports.fr
DATE: 2014-02-12
URL: http://www.sports.fr/jo-2014/snowboard/articles/halfpipe-white-a-du-mal-a-digerer-sa-defaite-1008841/
PRINCIPAL: 0
TEXT:
12 février 2014 � 08h26
Mis à jour le
12 février 2014 � 08h29
L’icône est tombée ! Star du snowboard, homme qui pèse plusieurs millions de dollars aux Etats-Unis, Shaun White a perdu mardi son titre olympique en halfpipe où il avait conquis l’or en 2006 et 2010. Il revient sur cette surprise…
Quatrième mardi de la finale du halfpipe quelques jours après avoir renoncé à s’aligner en slopestyle, Shaun White repartira de Sotchi sans médailles, une sensation pour  la discipline puisque la star du snowboard restait sur deux titres olympiques dans le demi-tube.  
"C’est une déception. J’avais un plan, un run spécifique que je voulais poser et que je n’ai pas pu faire jusqu’en bas. C’est ce qui rend les choses frustrantes. Si je pose mon run et que je suis battu, j’accepte ça mais je n’ai pas eu vraiment cette possibilité ce soir, ça arrive, a expliqué l’Américain qui a chuté sur sa première tentative avant d’éprouver de nouvelles difficultés en réception lors de son deuxième passage. Il a finalement obtenu une note de 90,25 pendant que le Suisse Iouri Podladtchikov se parait d’or avec une note de 94,75.
Les Japonais Ayumu Hirano  (93,50) et  Taku Hiraoka (92,25) terminent deuxième et troisième. "Être un athlète olympique n’est pas à négliger. Je suis vraiment fier d'être ici pour représenter les États-Unis, je suis fier d'être un athlète olympique, a poursuivi White. J’ai passé une soirée difficile mais je pense avoir touché beaucoup de gens qui n’avaient jamais regardé ce sport avant. Je ne pense pas que ce soir changera ou affectera ma carrière. J’ai ridé depuis si longtemps. J’aime ce sport et il m’a donné tellement. Je suis heureux de progresser et de continuer à rider." C’est la première fois depuis l’apparition de la discipline aux JO en 1998 que les États-Unis sont absents du podium.
