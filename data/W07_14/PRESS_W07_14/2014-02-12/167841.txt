TITRE: Un ex-gendarme condamn� � perp�tuit� pour le meurtre d'une vieille dame - BFMTV.com
DATE: 2014-02-12
URL: http://www.bfmtv.com/societe/toulouse-un-ex-gendarme-condamne-a-perpetuite-meurtre-dune-vieille-dame-709118.html
PRINCIPAL: 0
TEXT:
r�agir
Les assises de la Haute-Garonne ont condamn�, ce mercredi, � Toulouse, un ancien gendarme accus� d'avoir tu� une vieille dame de 97 ans pour lui voler ses bijoux � la r�clusion criminelle � perp�tuit�. Les assises ont reconnu Daniel Bedos, 57 ans, coupable de l'ensemble des chefs d'accusation ainsi que des faits de subornation de t�moin, et sont all�s au-del� des r�quisitions de l'avocat g�n�ral qui avait r�clam� contre lui trente ann�es de r�clusion criminelle .
T�te fracass�e
Daniel Bedos, qui a fait l'essentiel de sa carri�re de gendarme entre 1978 et 1992 en Haute-Garonne, �tait jug� depuis lundi pour avoir tu� Suzanne Blanc � son domicile le 18 ao�t 2010 � Toulouse pour lui prendre ses bijoux. La t�te fracass�e, la vieille dame avait longtemps agonis� et n'avait succomb� � ses blessures que le lendemain.
Daniel Bedos nie les faits. Il reconna�t avoir revendu les bijoux du crime d�s le lendemain des faits. Mais il soutient les avoir re�us d'un autre homme, Jean-Claude Durandeu, 53 ans, qui les lui aurait c�d�s pour 170 euros l'apr�s-midi de l'agression.
Dans son r�quisitoire, l'avocat g�n�ral Jean-Louis Bec a d�crit Daniel Bedos comme un �tre "pervers, narcissique, manipulateur bien s�r, et sans affect".
A lire aussi
