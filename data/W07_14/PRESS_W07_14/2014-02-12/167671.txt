TITRE: Audi S1 : enfin une "vraie" A1 Quattro | Site mobile Le Point
DATE: 2014-02-12
URL: http://www.lepoint.fr/auto-addict/salons/geneve-2014-audi-s1-enfin-une-vraie-a1-quattro-12-02-2014-1791011_656.php
PRINCIPAL: 167666
TEXT:
12/02/14 à 16h18
Audi S1 : enfin une "vraie" A1 Quattro
GENÈVE 2014. Grâce à une grosse évolution de plateforme, la petite A1 accède enfin à la transmission Quattro pour un modèle de grande série. Explications.
Audi a remis l'ouvrage sur l'établi et sort enfin une A1 Quattro digne de ce patronyme ! DR
Par Yves Maroselli
Souvenez-vous de l'A1 Quattro. Produit à seulement 333 exemplaires à partir de la fin 2012, ce modèle très spécial était en grande partie fabriqué à la main à partir d'une caisse d'A1 prélevée sur chaîne, puis découpée pour y installer un train arrière multibras et une transmission d'Audi TTS ainsi qu'un réservoir spécifique. Résultat, un tarif stratosphérique dépassant les 50 000 euros... et une rentabilité discutable pour Audi.
Une plateforme largement retravaillée...
La nouvelle S1, programmée pour le Salon de Genève, procède d'une tout autre logique qui permet de mesurer la transformation en profondeur subie par la plateforme actuelle de l'Audi A1 et qui est aussi utilisée pour la VW Polo restylée ( lire notre article ). Largement reconçue, cette architecture est désormais compatible avec la transmission Quattro jusque-là réservée aux modèles du segment supérieur. Elle reçoit un train arrière à 4 bras spécifique, une évolution qui ouvre la voie à une future Polo R non encore confirmée mais qui devrait être dotée elle aussi d'une transmission aux 4 roues (4Motion s'agissant d'une VW).
... et 231 ch sous le pied droit
Sur le plan mécanique, la S1 reprend le 2.0 TSI de 231 ch déjà vu sous le capot de la récente Golf GTI Performance. Un moteur qui permet à l'Audi S1 d'abattre le 0 à 100 km/h en 5,8 s tout en consommant 7,0 l/100 km, soit 162 g/km de CO2 pour un malus de 2 200 euros.
Côté châssis, le système de freinage évolue pour accompagner l'augmentation des performances avec l'adoption de disques de frein avant de 310 mm de diamètre, tandis que - comme la Polo restylée ainsi que toutes les futures A1 - la S1 reçoit une assistance de direction électrique. Sur le marché français, la S1 sera équipée en série de jantes en alliage de 18 pouces chaussées en 225/35.
Ces Audi S1 et S1 Sportback (5 portes) arriveront en concessions au mois de mai aux prix respectifs de 33 900 et 34 800 euros.
à lire aussi
