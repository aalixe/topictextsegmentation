TITRE: Alg�rie : le crash d'un avion militaire fait 77 morts, 3 jours de deuil - Afrik.com : l'actualit� de l'Afrique noire et du Maghreb - Le quotidien panafricain
DATE: 2014-02-12
URL: http://www.afrik.com/algerie-le-crash-d-un-avion-militaire-fait-77-morts-3-jours-de-deuil
PRINCIPAL: 166514
TEXT:
mercredi 12 f�vrier 2014 / par Fou�d Harit
La carcasse de l�avion militaire qui s�est �cras�, mardi 11 f�vrier, en Alg�rie (Ph�: AP/Mohamed Ali)
Alors qu�un premier bilan faisait �tat d�au moins 100 morts dans le crash ce mardi d�un avion militaire en Alg�rie, le minist�re de la D�fense a �voqu� 77 morts.
Le minist�re alg�rien de la D�fense a �tabli un bilan officiel du nombre de morts dans le crash, ce mardi, d�un avion militaire�: 77 morts. La veille une source s�curitaire indiquait plus de 100 morts. Un deuil de trois jours a �t� d�cr�t� � partir de ce mercredi 12 f�vrier.
Un  survivant a cependant �t� d�couvert. Toutefois, ce dernier souffre d�un traumatisme cr�nien, affirme un officier de la protection civile. ��A la suite de cet accident, le plan de recherches et de sauvetage a aussit�t �t� d�clench� et les unit�s de secours relevant de l�arm�e nationale populaire et de la protection civile se sont d�plac�es sur les lieux pour apporter les premiers secours��, selon le minist�re de la D�fense qui indique, dans un communiqu�, que ��les conditions m�t�orologiques tr�s d�favorables avec un orage accompagn� de chutes de neige seraient � l�origine de ce crash��.
L�appareil, un Hercule C130, qui avait pour destination l�a�roport de Constantine, s�est �cras� dans une zone montagneuse de la province d�Oum El Bouaghi. ��La communication entre la tour de contr�le et l�appareil s�est interrompue au moment o� l�avion amor�ait sa descente sur l�a�roport de Constantine��, affirme la presse locale.
lire aussi
