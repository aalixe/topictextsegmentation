TITRE: 5 choses � savoir sur Greta Gerwig, la "mother" de "How I Met Your Dad" - News films Stars - AlloCin�
DATE: 2014-02-12
URL: http://www.allocine.fr/article/fichearticle_gen_carticle%3D18630757.html
PRINCIPAL: 0
TEXT:
5 choses � savoir sur Greta Gerwig, la "mother" de "How I Met Your Dad"
mercredi 12 f�vrier 2014 - News - Stars
L'�g�rie ind� Greta Gerwig a surpris son monde en acceptant de devenir l'h�ro�ne de "How I Met Your Dad", le spin-off de "How I Met Your Mother" en pr�paration pour CBS. Encore peu connue chez nous malgr� le succ�s critique de "Frances Ha" sorti l'�t� dernier, voici 5 choses � savoir sur cette jeune actrice rayonnante et prometteuse pour briller � la machine � caf� ! On devrait beaucoup parler d'elle dans les prochains mois...
� RT Features
"La meilleure actrice de sa g�n�ration"
En tout cas selon le New York Times, qui l'a qualifi�e ainsi lors de la sortie du film Greenberg en 2010, sign� Noah Baumbach , dans lequel elle tenait le r�le principal et qui est consid�r� aujourd'hui comme un v�ritable tournant dans sa jeune carri�re. Trois ans plus tard, le duo, qui est devenu entre temps un couple � la ville, r�cidive avec le fameux Frances Ha , qu'ils co-�crivent. Il fait d�couvrir ses talents � la France puisque le film est bien re�u par la critique et le public et d�passe le circuit ferm� des cin�mas d'art et d'essai.
�
�
148 millions de $
Nouvelle star du cin�ma ind�pendant, Greta Gerwig n'est pas pour autant condescendante vis � vis du Hollywood du divertissement, comme le prouve son choix de devenir l'h�ro�ne de How I Met Your Dad , mais �galement sa participation en 2011 � la com�die romantique Sex Friends (No Strings Attached) , r�unissant Ashton Kutcher et Natalie Portman . Elle y joue la meilleure amie de cette derni�re. Le film de Ivan Reitman fonctionne tr�s bien aux Etats-Unis rapportant environ 77 millions de billets verts, pour un total monde de 148 millions. Il s'agit � ce jour, et de loin, du plus gros succ�s commercial de sa carri�re.
�
�
Pas sa premi�re fois � la t�l�....
M�me si le spin-off de How I Met Your Mother sera, en cas de commande en s�rie, son premier r�le majeur � la t�l�vision, Greta Gerwig n'est pas rest�e totalement �loign�e du petit �cran. Outre un r�le dans un t�l�film en 2009 (Une aventure new yorkaise) -il faut bien payer son loyer, surtout quand on vit dans la Grosse Pomme- elle a pr�t� sa voix � la s�rie anim�e China, IL, diffus�e sur Comedy Central ( South Park ) entre 2011 et 2013.
�
Elle a �galement failli faire sensation sur HBO dans la s�rie The Corrections . Cette adaptation du best-seller de Jonathan Franzen , dont le pilote �tait r�alis� par Noah Baumbach, n'a pas convaincu la cha�ne, qui n'a pas donn� suite au projet malgr� un casting incroyable : Ewan McGregor , Rhys Ifans , Maggie Gyllenhaal , Dianne Wiest et Chris Cooper �taient aussi de la partie !
�
Dans le clip de...
La princesse ind�, danseuse � ses heures perdues, a mis � profit son talent pour une rencontre 100% musicale avec le groupe de rock Arcade Fire en 2013. Elle a en effet particip� au clip tr�s �nergique de leur morceau The Afterlife o� elle se tr�mousse comme une damn�e. Pour couronner le tout, c'est Spike Jonze qui est derri�re la cam�ra !
�
�
Fan de...
La plus grande fan de Woody Allen , c'est elle ! C'est lui qui lui a donn� envie de devenir actrice, sc�nariste et r�alisatrice, c'est lui qui lui l'a motiv�e � quitter sa Californie natale pour vivre � New York et c'est lui qui a r�alis� son r�ve en la dirigeant dans son film To Rome with Love en 2012.
�
