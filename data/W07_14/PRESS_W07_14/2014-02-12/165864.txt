TITRE: Coline Mattel : �Je suis aux anges�
DATE: 2014-02-12
URL: http://www.leparisien.fr/JO-hiver/sotchi-2014-en-direct/coline-mattel-je-suis-aux-anges-11-02-2014-3581135.php
PRINCIPAL: 0
TEXT:
Coline Mattel : �Je suis aux anges�
�
R�agir
Le poing fi�rement tendu, Coline Mattel, immense sourire aux l�vres re�oit son bouquet. A 18 ans seulement, la demoiselle des Contamines s�offre la m�daille de bronze en saut � ski, une �preuve qui chez les femmes effectuait son entr�e aux Jeux Olympiques. �Je suis contente que ce soit fini, la journ�e a �t� longue, la semaine �prouvante.
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Sotchi: deux m�dailles d'or en descente
Tout le monde a �lev� son niveau, troisi�me c�est formidable, je suis aux anges !� Alors que les techniciens de France T�l�vision la connectent aux Contamines o� est rest� son papa, elle fond en larmes. �Je t�aime papa, je ne sais pas quoi dire.� �Je suis si fier de toi�, r�torque Xavier. La prochaine fois, ce sera l�or !�.
Du haut de son m�tre soixante-deux, la demoiselle des Contamines a un mental de fer. �J�ai un sale caract�re ! J�ai du mal � prendre sur moi alors, souvent, je vais au clash � se plait-elle a r�p�ter. En ce mardi soir, Coline n�a pas trembl�. Elle avait promis de grimper sur le podium olympique, elle a tenu parole, se glissant derri�re l�Allemande Vogt et l�Autrichienne Irashko-Stolz, mais devant la Japonaise Takanaski, leader de la Coupe du monde et seulement 4 e.
Une t�te bien pleine�
Coline Mattel c�est un petit bout de femme dynamique � la t�te bien faite (elle a d�croch� son bac scientifique avec mention tr�s bien), une passionn�e de voyages (elle r�ve de traverser la Mongolie � dos de cheval) et de th��tre. � J��tais toute petite lors de mon premier saut sur un tremplin de 30 m, nous racontait-elle. Les autres me regardaient et je ne voulais pas montrer que j�avais peur, se souvient-elle. J�ai chut� � la r�ception mais j�ai vite voulu recommencer pour aller plus loin. � Une d�termination qui ne l�a jamais quitt�e. � Ce qu�on ressent sur un tremplin est indescriptible. Pendant une poign�e de secondes, on a l�impression de voler. C�est fabuleux�, pr�cise-t-elle. �Quand elle monte sur un tremplin, ce n�est pas pour essayer, c�est pour gagner�, signale son papa.
En f�vrier 2011, � seulement 15 ans, elle avait d�j� offert � la France sa premi�re m�daille en saut. Ce mardi soir, c�est une premi�re m�daille olympique qu�elle a apport�e. �Je n�ai pas la vie des filles de mon �ge mais j�ai celle que j�ai toujours voulu avoir �, assurait-elle avant de partir. La voil� plus heureuse que jamais.
VIDEO. Coline Mattel avant les Jeux.�
�
