TITRE: Municipales � Perpignan: Louis Aliot progresse mais reste loin du compte
DATE: 2014-02-12
URL: http://www.huffingtonpost.fr/2014/02/12/municipales-perpignan-louis-aliot-progresse-reste-difficulte_n_4772844.html
PRINCIPAL: 167233
TEXT:
Municipales � Perpignan: Louis Aliot progresse mais reste loin du compte
Le HuffPost �|� Par Alexandre Boudet
Publication: 12/02/2014 13h41 CET��|��Mis � jour: 12/02/2014 13h42 CET
Municipales � Perpignan: Louis Aliot progresse mais reste loin du compte.         | FN
Recevoir les alertes:
Louis Aliot , Front National , Front National , Municipales 2014 , Perpignan , �lections municipales , �lections Municipales 2014 , Actualit�s
MUNICIPALES - Perpignan n'est pas que la pr�fecture des Pyr�n�es-Orientales. C'est aussi la plus grande ville que le Front national esp�re secr�tement faire tomber dans son escarcelle le mois prochain. Pour cela, le FN compte sur Louis Aliot, vice-pr�sident du parti et compagnon de Marine Le Pen.
Mais les ambitions du mouvement frontiste pourraient �tre douch�es par les r�sultats d'un sondage publi� ce mercredi 12 f�vrier . R�alis�e par TNS Sofres pour RTL et Le Nouvel Observateur, l'enqu�te place bien Louis Aliot deuxi�me au premier tour mais il reste tr�s loin de la victoire au second tour.
Aliot multiplie son score par trois depuis 2009
Dans le d�tail, le candidat FN recueille 28% des intentions de vote. Cela repr�sente une importante progression par rapport � la derni�re �lection de 2009, quand il n'avait obtenu que 9,4% des voix. De quoi conforter l'id�e que le Front national poursuit sa normalisation et qu'il s'implante fortement dans le sud du pays.
Avec ce score, Louis Aliot arrive certes derri�re l'UMP Jean-Marc Pujol (35%) mais il devance nettement le PS Jacques Cresta (18%). Arrivent ensuite l'�cologiste Jean Codognes (11%) et la liste sans �tiquette conduite par Clotilde Ripoull (8%).
Tout irait donc bien pour Louis Aliot s'il parvenait � confirmer ces bons scores au second tour. Or, il est confront� au probl�me rencontr� par de nombreux candidats frontistes: o� trouver les r�serves de voix qui lui permettrait d'atteindre la majorit�. Le sondage confirme ces difficult�s puisqu'il place le FN en troisi�me position.
Un moins bon score au deuxi�me tour
Si l'�lection avait lieu dimanche, la victoire reviendrait � l'UMP Jean-Marc Pujol avec 42%, devant le PS Jacques Cresta 33%. Quant � Louis Aliot, il n'est cr�dit� que de 25%, un score inf�rieur � celui du premier tour. Cela pourrait s'expliquer par un report des voix en faveur de l'UMP pour assurer la victoire de la droite.
Retrouvez les articles du HuffPost sur notre page Facebook .
Pour suivre les derni�res actualit�s en direct, cliquez ici .
Loading Slideshow
Pr�f�rence nationale
Incitation des entreprises � privil�gier les Fran�ais dans l'acc�s � l'emploi. Priorit� aux Fran�ais dans l'acc�s au logement social. Les allocations familiale sont r�serv�es aux familles "dont un parent au moins est Fran�ais ou europ�en". Les emplois de service public "dans le domaine r�galien" sont r�serv�s aux seuls Fran�ais.
Cr�ation d'une Garde nationale
Organisation d'une Garde Nationale de 50.000 r�servistes sur l�ensemble du territoire (Outre-Mer compris) "mobilisable dans un bref d�lai".
Suppression du droit du sol et du regroupement familial
R�duction drastique du nombre de demandeurs d�asile admis � rester en France. Remise en cause des accords de Schengen sur la libre circulation des personnes. Suppression du droit du sol et r�forme en profondeur du code de la nationalit� fran�aise. Suppression de l�Aide M�dicale d�Etat.
Diviser par 20 l'immigration
R�duction en 5 ans de l�immigration l�gale de 200 000 entr�es par an � 10 000 entr�es par an. Suppression, dans le droit fran�ais, de la possibilit� de r�gulariser des clandestins et interdiction du droit de manifester pour les clandestins.
R�tablissement de la peine de mort
Organisation d'un r�f�rendum proposant soit le r�tablissement de la peine de mort soit la r�clusion � perp�tuit� r�elle (sanction d�finitive et irr�versible).
Une justice refinanc�e et musel�e
Revalorisation 25% en cinq ans du budget de la justice. Cr�ation de 40.000 nouvelles places de prison. Interdiction pour les magistrats d��tre syndiqu�s, de s�engager politiquement ou d��tre candidat. Responsabilit� p�nale accrue pour tous les mineurs de plus de 13 ans. Renvoi "chez eux" des d�linquants �trangers condamn�s.
Natalit�: lutter contre l'IVG
Le FN pr�ne "le libre choix pour les femmes" qui "doit pouvoir �tre aussi celui de ne pas avorter": meilleure pr�vention et information , responsabilisation des parents, mise en place de l�adoption pr�natale
D�sengagement de l'Union europ�enne
R�tablissement la primaut� du droit national sur le droit europ�en, r�appropriation de la monnaie et de la politique mon�taire, promotion d�une Europe des Nations, ren�gociation des trait�s afin de rompre avec la construction europ�enne.
Abandon de la Politique agricole commune
Abandon de la PAC europ�enne et cr�ation d'une Politique agricole fran�aise (PAF). Mise en place du "patriotisme agricole". Lutte contre les ententes entre centrales d�achat et les abus de position dominante de la grande distribution.
Proportionnelle int�grale et septennat
R�tablissement du septennat non renouvelable. Une loi organique instaurera le scrutin proportionnel � toutes les �lections, nationales ou locales, directes ou indirectes. R�vision constitutionnelle uniquement par r�f�rendum. Instauration du r�f�rendum d'initative populaire.
Loading Slideshow
Abbey Road fa�on Front de Gauche
Une affiche rep�r�e <a href="http://municiplol2014.tumblr.com/post/79445443485/abbey-road-version-front-de-gauche#.UyMCPPl5Orm" target="_blank">par le blog municiplol2014</a>.
Les d�tournements d'affiche de Puteaux
Cindy Lopes, de Secret Story candidate dans le 9-4
Quelques ann�es apr�s sa participation � Secret Story, Cindy Lopes a d�cid� de se lancer en politique. Elle est candidate en 4e position sur une liste divers-droite � Villeneuve-le-Roi, dans le Val de Marne.
L'ex-batteur de Noir D�sir s'engage dans les Landes
L'ancien batteur du groupe Noir D�sir, Denis Barthe, a d�cid� de se pr�senter dans son village des Landes. A Sore, commune d'un millier d'habitants, il est candidat sur la seule liste men�e par un exploitant agricole.
Une cl�mentine pour Sevran
Voici un tract de Cl�mentine Autain, t�te de liste du Front de Gauche � Sevran.
Christophe Lambert soutient le parti communiste � Ivry
L'acteur Christophe Lambert a apport� son soutien au maire communiste sortant d'Ivry. "<a href="http://www.leparisien.fr/val-de-marne-94/municipales-a-ivry-christophe-lambert-soutient-pierre-gosnat-pc-10-03-2014-3659403.php" target="_blank">Pierre Gosnat est un personnage extraordinaire</a>, qui a fait de sa ville un mod�le d'int�gration des populations", a expliqu� Highlander au <em>Parisien</em>.
Gr�goire candidat � Senlis
Le chanteur Gr�goire, rendu c�l�bre par son tube "Toi + Moi" en 2009, a rejoint la liste du maire sortant de Senlis (Oise), <a href="http://www.huffingtonpost.fr/2014/03/01/gregoire-candidat-municipales-oise-liste-maire_n_4880869.html?1393694077" target="_blank">Pascale Loiseleur (sans �tiquette), pour les �lections municipale</a>s.
L'affiche de Gr�gory Berthault candidat � Rueil-Malmaison
Une affiche d�voil�e le 16 janvier <a href="https://www.facebook.com/rueil2014" target="_blank">sur la page Facebook de ce candidat</a> qui pose en compagnie de sa furette "L�onie". Son parti: le Mouvement Citoyen pour la Protection Animale.  Cette affiche (qui cite Lamartine) a �t� rep�r�e <a href="http://municiplol2014.tumblr.com/" target="_blank">par le blog municiplol2014.tumblr.com</a>.
L'UDI en Seine-Saint-Denis, c'est Game of Thrones
La m�re de Dany Boon se pr�sente dans le Nord
<a href="http://www.huffingtonpost.fr/2014/01/09/dany-boon-mere-candidate-ump-nord-municipales_n_4566644.html?utm_hp_ref=france" target="_blank">La m�re de Dany Boon, Dani�le Hamidou-Ducatel</a>, se pr�sente en 2e position sur la liste du candidat UMP de Armenti�res, pr�s de Lille, Michel Plouy.
Les frites de Le�la Chaibi
Le 16 janvier, la t�te de liste du Front de Gauche dans le 14e arrondissement de Paris a invit� les citoyens � prendre une frite dans le camion-snack � bord duquel elle fait campagne.
Le programme en Bitstrips d'Yves Urieta
Candidat � Pau, <a href="http://www.yvesurieta.fr/" target="_blank">Yves Urieta</a> a d�cid� d'utiliser l'application Bitstrips (une appli ph�nom�ne qui permet de transformer votre quotidien en BD) pour d�crire son programme.
La femme de Fran�ois Fillon candidate
Penelope Fillon reprend le flambeau de son mari dans la Sarthe. La femme de l'ancien Premier ministre sera candidate pour un poste de conseiller municipal � Solesmes.
La maire de Beauvais se repr�sente avec Bitstrips
Avant lui, la s�natrice-maire UMP de Beauvais, Caroline Cayeux, a annonc� sa candidature en utilisant Bitstrips. Une premi�re utilisation de cette appli en politique qui en a inspir� plus d'un.
Plus ou moins
L'actrice de "Sous le Soleil" aurait du �tre candidate
A 43 ans, Adeline Blondieau, ex-madame Hallyday, devait se pr�senter sur la liste UMP de Nicole Gou�ta � Colombes, dans les Hauts-de-Seine. <a href="http://www.huffingtonpost.fr/2014/02/27/adeline-blondieau-finalement-pas-candidate-municipales-colombes_n_4867234.html" target="_blank">Mais elle a du d�clarer forfait quelques jours plus tard</a> en raison d'"un projet professionnel qui s'est acc�l�r�"...
Les candidats des bisous
<a href="https://twitter.com/MaudeML/status/424596637236080641" target="_blank">Rep�r�e par Maude Milekovic sur Twitter</a>, cette vid�o du comit� de soutien de V�ronique Fenoll et Alain Tanton (candidats � Bourges) est pleine d'amour: des bisous, des c�urs avec les mains et Gr�goire en bande-son.  Apr�s le pr�sident des bisous, les maires ?
Viser juste
Non, Bernard Ansart n'est pas candidat au parti du Plaisir mais t�te de liste PS � Plaisir dans les Yvelines!  Ce dernier a d�voil� plusieurs affiches de campagne<a href="http://www.bernard-ansart2014.fr/" target="_blank"> sur son site officiel</a>, parmi lesquelles celle-ci, peu conventionnelle, mais qui colle parfaitement � son slogan: viser juste.
Le nouveau Delano�
"Un nouveau Delano�! Plus jeune. Plus frais. Plus performant!" C'est ainsi que <a href="http://www.gasparddelanoe2014.com/" target="_blank">Gaspard Delano�</a> se pr�sente dans cette vid�o diffus�e pour la premi�re fois en septembre 2013.  Ce "bel hidalgo pour Paris" est l'artiste anarcho-fantaisiste de ces municipales parisiennes. Candidat du Parti faire un tour dans le 10e arrondissement, il n'a pas fini de faire parler de lui avec ses id�es comme recouvrir la tour Eiffel avec un pr�servatif, d�truire la tour Montparnasse, l'op�ra Bastille et le sacr� coeur, et remplacer le m�tro par la montgolfi�re.
La municipale de Colombes en GIFs
� Colombes, deux clans s'opposent gaiement sur Tumblr. Deux militants, l'un PS, l'autre UDI, racontent le quotidien de la campagne avec des GIFs sur leurs blogs.  Le premier - <a href="http://plusdecolombes.tumblr.com/" target="_blank">plusdecolombes.tumblr.com</a> - est sign� Matthieu Brun qui soutient le candidat UDI Samuel Metlas.   Le second - <a href="http://colombien92.tumblr.com/" target="_blank">colombien92.tumblr.com</a> est l'�uvre de Philippe Claude, sympathisant du maire PS Philippe Sarre.
Le candidat myst�re...
Un lecteur nous a fait parvenir ce tract, distribu� dans les bo�tes aux lettres de Villefontaine (Is�re) en novembre dernier.
... c'est lui
Au verso, les �lecteurs ont pu d�couvrir Fr�d�ric Hugon, candidat PS.
La chanson de campagne de Thierry Robert
<a href="http://www.thierryrobert.re/" target="_blank">Thierry Robert</a>, d�put�-maire de Saint-Leu (La R�union) a une chanson de campagne bien entra�nante.
De la Star Ac aux municipales
Apr�s le ch�teau, la mairie? Mathieu Johann, ancien pensionnaire de la "Star Academy", est aujourd'hui candidat � Saint-L�, dans la Manche. <a href="http://www.huffingtonpost.fr/2014/01/21/municipales-mathieu-johan-star-academy-saint-lo-insolite_n_4637587.html?ncid=edlinkusaolp00000003" target="_blank">En lire plus ici</a>.
Le JT d�cal� de Vaison-la-Romaine
Pour faire campagne, Jean-Fran�ois Perilhou et son �quipe de Vaison-la-Romaine (Vaucluse) ont eu l'id�e d'un "JT d�cal�".   Voici le premier num�ro qui nous a �t� envoy� par une lectrice. Les autres sont <a href="http://www.youtube.com/channel/UC_AaDnwnn5bZTeenRRCmvMg?feature=watch" target="_blank">sur sa cha�ne YouTube</a>.
Mister Univers 2014 sur une liste PS
Alexandre Piel, Mister Univers 2014 et ancien garde du corps de Fran�ois Hollande se pr�sentait avant qu'il retire sa candidature pour une affaire de dopage.
Vincent Lagaf' s'engage � Cavalaire-sur-Mer
Le pr�sentateur du Juste Prix figure sur la liste d'un candidat sans �tiquette dans le Var. <a href="http://www.huffingtonpost.fr/2014/01/29/municipales-vincent-lagaf-candidat-cavalaire_n_4686074.html?1390996135" target="_blank">En lire plus ici.</a>
Une vid�o de campagne sur la bande-son d'Inception
Intense!  Une p�pite d�nich�e par <a href="http://municiplol2014.tumblr.com/post/74941478993/jamais-la-tension-na-ete-aussi-forte-pour-une#.UukblflKG70" target="_blank">municiplol2014.tumblr.com</a>
"Merci Monsieur le Maire!" version FN
"Au revoir Monsieur Adolphe": une signature collector.
L'actrice Corinne Masiero candidate Front de gauche
L'actrice Corinne Masiero, r�v�l�e par le film "Louise Wimmer" fin 2011, sera pr�sente sur la liste Front de gauche (FG) pour les �lections municipales � Roubaix (Nord),
Le message de Saint-Valentin d'Anne Hidalgo
C'est du second degr�, mais c'est quand m�me de la politique. Retrouvez le meilleur des <a href="http://www.huffingtonpost.fr/2014/02/14/sain-valentin-municipales-emparent-fete-amoureux_n_4786660.html?utm_hp_ref=france" target="_hplink">messages de Saint-Valentin de ces municipales 2014 ici</a>.
La VoteNomination de Samuel Metias
Le candidat UDI de Colombes s'est inspir� du jeu � boire Neknomination pour lancer la VoteNomination.
"Get Lucky" des Daft Punk, version cr�ole
Candidat UMP � Saint-Denis sur l'�le de La R�union, Ren�-Paul Victoria a choisi le c�l�bre titre Get Lucky de Daft Punk comme porte-bonheur, avec cette version r��crite en cr�ole.
Contribuer � cet article:
