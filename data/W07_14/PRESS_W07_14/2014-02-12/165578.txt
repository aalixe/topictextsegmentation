TITRE: Mattel, un saut pour l'histoire - 12/02/2014 - leParisien.fr
DATE: 2014-02-12
URL: http://www.leparisien.fr/espace-premium/sports/mattel-un-saut-pour-l-histoire-12-02-2014-3582055.php
PRINCIPAL: 0
TEXT:
Saut � ski.
Mattel, un saut pour l'histoire
La petite Fran�aise a r�ussi une grande performance en d�crochant le bronze pour le premier concours f�minin de la sp�cialit� aux JO.
David Charpentier |  Publi� le 12 f�vr. 2014, 07h00
Russkie Gorki (Russie), hier. Avec cette belle m�daille de bronze, la jeune Coline Mattel est r�compens�e des nombreux sacrifices qu�elle s�est impos�s. (AP/Charlie Riedel.)
Mon activit� Vos amis peuvent maintenant voir cette activit�
Tweeter
Hier, on retiendra que trente jeunes filles ont �crit une page de la riche histoire de l'olympisme en participant au premier concours de saut � ski f�minin. Et parmi ces pionni�res, Coline Mattel, un petit bout de femme plein de vitalit� de 1,65 m, n�e � Sallanches mais �lev�e sur les pentes des Contamines-Montjoie.
La Fran�aise de 18 ans s'est invit�e sur la troisi�me marche du podium . Jusque-l� r�serv�e aux hommes, la discipline spectaculaire du saut � ski a gagn� hier soir ses galons de respectabilit�. � Je suis juste super fi�re d'en faire partie et d'�crire un peu l'histoire. L�, on a toutes �crit l'histoire �, a confi� Coline, des tr�molos dans la voix.
C'est que le chemin de la gloire olympique n'a pas toujours �t� rectiligne pour la jeune femme qui a d�cid� � l'�ge de 7 ans de se lancer sur un tremplin enneig�. Pr�sente hier dans les tribunes, la maman n'avait pas encore eu le temps de serrer sa prog�niture dans les bras. Mais elle s'est rappel�e de la premi�re fois.
� La premi�re fois qu'elle a saut�, je n'ai rien dit, tout simplement parce que je ne le savais pas. Je m'en suis rendue compte deux jours plus tard car elle s'�tait bien fait mal. Mais comme elle n'aime pas rester sur un �chec, elle a continu�. �
Du caract�re et de la volont�
Une anecdote qui montre toute l'abn�gation de la prodige. Une volont� qui a aussi touch� son entra�neur, encore pr�sent sur la rampe hier apr�s-midi pour lui donner le signal, et persuad� qu'il avait entre les mains une athl�te capable d'accomplir les plus belles choses.
En 1992 � Albertville, Jacques Gaillard avait d�j� fa�onn� le destin olympique de Fabrice Guy, premier Fran�ais champion olympique de combin� nordique. La nouvelle collaboration avec celle qui est encore une adolescente trouve un premier accomplissement en 2011, quand elle d�croche une m�daille de bronze aux Championnats du monde � Oslo.
Hier, au pied du stade de saut de Ruski Gorki, Gaillard a litt�ralement saut� dans les bras de son �quipe avant de raconter � sa � Coline. � C'est vrai qu'elle a un gros caract�re mais elle a un bon caract�re , un gros mental��, admet le technicien.
Au fil d'une ann�e qui ne se d�roulait pas de mani�re id�ale, l'�quipe autour de Coline a su faire corps avec la championne et fixer l'objectif des Jeux. � On ne dira pas qu'on n'a pas dout� mais en tout cas on n'a rien l�ch�. Pas un entra�neur ne la mettait sur le podium, sauf nous. �
