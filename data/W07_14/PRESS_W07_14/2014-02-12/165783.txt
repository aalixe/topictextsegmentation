TITRE: Municipales � Paris : voici l�affiche de campagne d�Anne Hidalgo - Municipales 2014
DATE: 2014-02-12
URL: http://www.leparisien.fr/municipales-2014/paris-75/municipales-a-paris-voici-l-affiche-d-anne-hidalgo-11-02-2014-3581165.php
PRINCIPAL: 0
TEXT:
Municipales � Paris : 6 mois ferme pour les agresseurs des colleurs d'affiches du PS
En s�adressant � H5, Hidalgo a aussi opt� pour un parti-pris assez radical. Car l�agence -�qui s�est fait connaitre en 1996 avec la pochette jaune fluo Super Discount de la compil des nouveaux groupes musicaux de la French Touch ( Daft Punk , Air, Cassius...) - aime se d�marquer aussi bien dans le champ graphique qu�esth�tique. En 2010, son film d�animation Logorama a remport� � Hollywood l�Oscar du meilleur court-m�trage d�animation et un C�sar l�ann�e suivante � Paris.
Contact� par l��quipe de la candidate socialiste, le collectif de la rue du Faubourg-Poissonni�re (Paris 9�) a r�agi au quart de tour. D�autant qu�il venait de produire une exposition � La Ga�t� Lyrique confrontant communication politique et communication de marques dans la soci�t�. �Il a �t� extr�mement facile pour nous de travailler avec une candidate qui a un propos tr�s clair, qui n�a rien � cacher�, explique Charlotte Camille, une des chevilles ouvri�res de H5.
Moderniser les codes de l'affiche politique
Regard direct de la candidate capt� par la photographe In�s Dieleman, cadrage serr� avec en arri�re-plan la ville en flou. �La photo s�est impos�e � nous comme une �vidence�, commente Jean-Louis Missika, le co-directeur de la campagne d�Hidalgo. �Elle a �t� tr�s peu retravaill�e, souligne-t-on chez M5. En bas, nous avons choisi pour le nom de la candidate et le slogan Paris qui ose une typographie massive, tr�s frontale et affirmative�. La marque de fabrique de l�agence.
R�alis� sous la direction artistique de Rachel Cazadamont, le document entend moderniser les codes en vigueur de l�affiche politique traditionnelle, jug�e �trop plan-plan, ringarde ou ultra-informative�. �Un message, une candidate, une ville, nous on n�est pas dans la d�coration�, proclame Charlotte Camille.
Ou comment montrer du doigt la communication de la concurrente NKM qui, �telle une love-marque sur-joue la sympathie avec un logo en forme de coeur.� H5, qui pour la premi�re fois travaille directement sur une campagne politique, produit aussi des vid�os visibles sur le site d�Anne Hidalgo. Tir�e � 30 000 exemplaire, l�affiche, elle, est destin�e prioritairement aux panneaux �lectoraux et aux annonces des �v�nements de la campagne.
LeParisien.fr
