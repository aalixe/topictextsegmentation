TITRE: COUPE DE FRANCE  (8EMES DE FINALE) / LILLE - CAEN : 3-3 (6-5, TAB) - Lille joue � se faire peur mais se qualifie
DATE: 2014-02-12
URL: http://www.football365.fr/france/coupe-de-france/lille-joue-a-se-faire-peur-mais-se-qualifie-1102570.shtml
PRINCIPAL: 0
TEXT:
COUPE DE FRANCE  (8EMES DE FINALE) / LILLE - CAEN : 3-3 (6-5, TAB) - Publi� le 12/02/2014 � 00h06 -  Mis � jour le : 12/02/2014 � 09h13
23
Lille joue � se faire peur mais se qualifie
Apr�s une entame de match catastrophique, Lille a su se reprendre pour arracher sa qualification pour les quarts de finale de la Coupe de France face � de valeureux Caennais.
Le debrief
Qui aurait pari� sur une qualification de Lille �au terme des 20 premi�res minutes ? Sans doute personne ! Mais les Lillois ont su se sortir d�une torpeur inhabituelle pour r�duire l��cart avant la mi-temps. Apr�s avoir r�int�gr� certains cadres au gr� des changements, Lille est m�me parvenu � retourner le match pour esp�rer se qualifier. Mais une erreur de Rodelin en toute fin de temps additionnel a permis � Caen d�arracher la prolongation. Aucune d�cision n�est faite au bout de 120 minutes et la s�ance de tirs au but est in�vitable. Pr�t� par Lorient, Mathias Autret envoie le ballon dans les tribunes au sixi�me tir au but et offre le billet pour les quarts aux Lillois.
Le film du match
14eme minute (0-1)
Suite � une passe en retrait maladroite de Souar�, Elana manque son d�gagement dans l�axe. Koita est � la r�ception de cette relance maladroite et frappe des 20 m�tres. Elana n�est pas replac� sur sa ligne et est trop court pour sortir le ballon du cadre � ras-de-terre, il ne peut que l�effleurer. Avec le poteau, le ballon entre dans la cage lilloise.
19eme minute
Delaplace est bien d�cal� sur la droite de la surface et tente sa chance d�une frappe crois�e du pied droit mais elle passe au-dessus de la cage de Bosmel.
19eme minute
Kodjia acc�l�re sur l�aile droite et efface Sidib�. Il centre au premier poteau pour la t�te de Nangis mais il d�croise trop sa frappe qui passe devant la cage d�Elana.
28eme minute (0-2)
Raineau perd le ballon face aux Lillois I.Gueye et M��t�, mais ces derniers oublient d�en reprendre le contr�le. Face � cette apathie, Kant� le r�cup�re et acc�l�re sur l�aile droite. Il lance Koita sur la droite de la surface qui place une frappe crois�e sur laquelle Elana ne parvient pas � se coucher.
30eme minute
Timide r�action lilloise avec Rodelin qui prend sa chance d�une frappe de l�entr�e de la surface mais il d�visse totalement sa frappe sous les sifflets du maigre public du Stade Pierre-Mauroy.
33eme minute (1-2)
Sur un coup-franc depuis l�aile droite, Ruiz trouve la t�te de Basa � hauteur du point de p�nalty qui remet de la t�te vers Rodelin. L�attaquant lillois se jette et place le plat du pied droit pour surprendre Bosmel et r�duire l��cart.
38eme minute
Sur une bonne ouverture plein axe, Delaplace se d�tend pour d�vier le ballon du pied droit face � la sortie de Bosmel mais le milieu lillois ne parvient pas � attraper le cadre, le ballon passant � droite de la cage caennaise.
41eme minute
Koita passe dans le dos de Basa et est � la retomb�e d�une ouverture en profondeur. La t�te de l�attaquant caennais passe � gauche de la cage lilloise.
41eme minute
Sur une acc�l�ration et un centre d�Origi sur l�aile droite, Ruiz parvient � remettre de la t�te sur Rodelin mais la balle est trop haute. La reprise de l�attaquant lillois est d�viss�e et passe largement au-dessus de la cage de Bosmel.
54eme minute
Surt un coup-franc aux 30 m�tres, Ruiz centre au point de p�nalty pour Rodelin qui croch�te un d�fenseur caennais mais ne peut trouver le cadre face � Bosmel.
58eme minute
Sur corner, Ruiz centre au point de p�nalty mais la d�fense caennaise se d�gage sur Balmont qui tente la reprise de vol�e mais sa frappe � ras de terre est contr�e.
77eme minute (2-2)
Sur une frappe des 20 m�tres, Origi ne trouve que le poteau. Le ballon rebondit sur Bosmel qui remet sur un opportuniste Delaplace qui pousse le ballon dans le but vide.
83eme minute (3-2)
Nolan Roux se signale par un encha�nement contr�le-frappe du pied droit � l�entr�e de la surface plein axe. Bosmel ne peut rien faire sur cette frappe lourde qui donne l�avantage au LOSC.
93eme minute (3-3)
Sur un coup-franc lointain tendu de Calv�, Rodelin est le premier � mettre sa t�te mais il ne parvient qu�� d�vier le ballon dans ses propres filets pour permettre � Caen d�arracher la prolongation.
Les joueurs � la loupe
Lille
Elana (4) : Une entame de match tr�s difficile avec une erreur de relance incroyable. Un match difficile pour le rempla�ant d�Enyeama.
Soumaoro (4) : Quelques mont�es int�ressantes mais une prestation d�fensive d�cevante.
Rozehnal (4) : Inconstant, il n�a pas �t� � son meilleur niveau, mais il n��tait pas le seul ce soir.
Basa (cap) (4) : Des absences coupables pour la capitaine du soir, qui s�est repris par la suite.
Sidib� (5) : Quelques fulgurances int�ressantes mais pas grand-chose de plus � signaler.
Delaplace (5) : Apr�s un d�but de match difficile, il a �t� pr�sent � la relance et opportuniste en attaque sur son but.
Gueye (5) : Un oubli de relance sur le deuxi�me but lillois mais rien d�autre � signaler ensuite.
Meit�(5) : Un match int�ressant de sa part, mais pas brillant non plus. Remplac� par Balmont (57eme) qui a encore une fois d�montr� ses qualit�s.
Rodelin(5) : Quelques impr�cisions devant le but mais il a su relancer les Lillois quand ils �taient au plus mal. Un erreur �vitable en fin de match.
Mendes (4) : Pas � son aise, il n�a pas eu l�influence qu�on pouvait attendre de lui ce soir. Remplac� par Origi (56eme) qui a su se mettre en avant.
Ruiz (5) : Int�ressant dans le jeu offensif, il a su cr�er des br�ches dans la d�fense caennaise, qui n�ont pas �t� suffisamment utilis�es. Remplac� par Roux (72eme) qui n�a pas tard� se mettre en avant.
Caen
Bosmel (5) : Un match int�ressant de la part du gardien m�me s�il n�a pu faire grand chose sur les buts lillois.
Appiah (4) : Un placement int�ressant mais un manque d�agressivit� d�fensive.
Pierre (5) : Solide en premi�re p�riode, moins bien dans la suite du match.
Saad (5) : Quelques relances propres, mais pas d�erreurs � lui imputer.
Calv� (5) : Quelques mont�es int�ressantes et une passe d�cisive � son compteur. Moins brillant d�fensivement.
N.Kant� (5) : Pr�sent � la r�cup�ration et � la relance, pr�cieux pour son �quipe. Remplac� par l�exp�riment� Seube (60eme).
Agouazi (cap) (5) : Une prestation de bon niveau pour le capitaine caennais.
Raineau (5) : Quelques pertes de balles dispensables
Kodjia (5) : Int�ressant par son placement, mais il aurait pu faire mieux.
Koita (7) : Double buteur, il a beaucoup pes� sur la d�fense lilloise. Remplac� par Duhamel (56eme) qui n�a pas brill�.
Nangis (4): Il n�a pas su se faire remarquer durant le match, mais a �t� pr�sent sur son aile. Remplac� par Autret (69eme) qui a �t� malheureux.
Monsieur l�arbitre au rapport
Un match sans grande histoire pour M.Jaffredo. Rien � signaler le concernant.
�a s�est pass� en coulisses�
- Pour ce match, Ren� Girard devait se passer de l�absent de longue date Franck B�ria, touch� � une �paule, mais aussi de Marvin Martin, op�r� du genou droit la semaine pass�e. Deux incertitudes planaient encore juste avant le coup d�envoi sur Simon Kjaer, qui a du se faire op�rer des dents de sagesse, et de Pape Souar�, touch� � un talon.
- C�t� caennais, Patrice Garande devait se passer d�Aur�lien Montaroup et Jos� Saez, tous deux suspendus pour cette rencontre. N�anmoins, l�entra�neur normand r�cup�rait Nicolas Seube, absent depuis d�but janvier pour blessure. De plus, trois joueurs ont fait leur retour dans le groupe : Jonathan Kodjia, Kyung-Jung Kim et Thomas Lemar.
- En raison de la m�t�o sur le Nord de la France, ce huiti�me de finale de la Coupe de France s�est jou� dans un Stade Pierre-Mauroy compl�tement ferm�. Le toit r�tractable a permis de prot�ger la pelouse.
La feuille de match
COUPE DE FRANCE (8EMES DE FINALE) / LILLE - CAEN : 3-3�(6-5 TAB)
Stade Pierre-Mauroy (10 000 spectateurs environ)
Temps froid - Pelouse correcte
Arbitre : M.Jaffredo (5)
Buts : Rodelin (33eme), Delaplace (77eme) et Roux (83eme) pour Lille � Koita (14eme et 29eme) et Rodelin (93eme csc) pour Caen
Avertissements : Aucun
Expulsion : Aucune
Lille
Elana (4) - Soumaoro (4), Rozehnal (4), Basa (cap) (4), D.Sidib� (4) - Delaplace (5), I.Gueye (5), M�it� (5) puis Balmont (57eme) - Rodelin (5)- Mendes (4) puis Origi (56eme), Ruiz (5) puis Roux (72eme)
N'ont pas particip� : Mouko (g), Jeanvier, Mavuba, Halucha
Entra�neur : R.Girard
Caen
Bosmel (5) - Appiah (4), Pierre (5), Saad (5), Calv� (5) - N.Kant� (5) puis Seube (60eme), Agouazi (cap) (5), Raineau (5) - Kodjia (5), Koita (7) puis Duhamel (56eme), Nangis (4) puis Autret (69eme)
N'ont pas particip� : Perquis (g), Kim, Lemar, Fajr
Entra�neur : P.Garande
