TITRE: Fran�ois Hollande aux Etats-Unis : Stephen Colbert, dr�le d'invit� � la Maison Blanche � metronews
DATE: 2014-02-12
URL: http://www.metronews.fr/info/stephen-colbert-drole-d-invite-a-la-maison-blanche/mnbl!DbskWUFxVYyK/
PRINCIPAL: 166304
TEXT:
Cr�� : 12-02-2014 10:46
Stephen Colbert, dr�le d'invit� � la Maison Blanche
HUMOUR � Mardi, lors du d�ner d'Etat organis� par le couple Obama en l'honneur de Fran�ois Hollande, un homme assis aux c�t�s de Michelle a fait jaser. Il s'agit de Stephen Colbert, un humoriste politique bien connu � la t�l�vision am�ricaine et auteur, il y a quelques jours, d'une sortie mordante contre... le pr�sident fran�ais.
�
Stephen Colbert se paye Fran�ois Hollande. Photo :�Capture Comedy Central
On le sait, le plan de table �tait soigneusement pens�, mardi soir � la Maison blanche, pour la r�ception du ''pr�sident c�libataire'', Fran�ois Hollande . Et pourtant. A la droite de Michelle Obama, et donc tout proche du pr�sident fran�ais, avait pris place un dr�le d'invit� : Stephen Colbert.
Le nom ne dira rien au public fran�ais. En revanche, aux Etats-Unis, cet humoriste, pass� ma�tre dans l'art de la satire politique, est une star. Son show parodique, The Colbert Report, diffus� sur la cha�ne c�bl�e Comedy Central est un v�ritable ph�nom�ne, � tel point que le v�n�rable Time se demandait en 2012 si l'humoriste n'�tait carr�ment pas ''le nouveau�Socrate'' .
Ce n'est que de ''l'humour''
Or, le 15 janvier dernier , Stephen Colbert a d�laiss� la politique am�ricaine pour s'int�resser de pr�s �... Fran�ois Hollande et ses diff�rentes conqu�tes, r�elles et suppos�e : S�gol�ne Royal, Val�rie Trierweiler et, bien s�r, Julie Gayet. Avec, comme point commun entre toutes, leur beaut� et leur �l�gance. Tout le contraire, a poursuivi le pol�miste, du pr�sident fran�ais, le comparant au Quasimodo de Victor Hugo , alias le bossu ''qui sonne les cloches � Notre-Dame !''
En conviant Stephen Colbert � la table des grands mardi soir, la Maison Blanche a-t-elle commis un impair ? Si tel est le cas, personne ne s'en est publiquement offusqu�. Pr�sents au d�ner mardi soir, Arnaud Montebourg ou Jean-Paul Huchon n'y ont rien vu � redire, soulignant surtout ''l'humour'' qui caract�rise � la fois les Etats-Unis et le locataire de l'Elys�e.
> Voir ici le sketch de Stephen Colbert sur Fran�ois Hollande
The Colbert Report
