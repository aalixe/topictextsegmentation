TITRE: Seine-et-Marne : il s'�vade du commissariat et vole une voiture de la mairie - Soci�t� - MYTF1News
DATE: 2014-02-12
URL: http://lci.tf1.fr/france/faits-divers/seine-et-marne-il-s-evade-du-commissariat-et-vole-une-voiture-de-8364154.html
PRINCIPAL: 0
TEXT:
seine-et-marne , meaux
Faits diversLe d�tenu, �g� de 25 ans, �tait incarc�r� � la prison de Meaux-Chauconin, d'o� il avait �t� extrait pour �tre auditionn� par des policiers venus sp�cialement de l'Is�re pour une affaire de vol de voitures.
Un d�tenu qui devait �tre auditionn� au �commissariat de Meaux ( Seine-et-Marne ) a tromp� mardi la surveillance des �policiers et s'est fait la belle en d�robant une voiture de la mairie gar�e � �proximit�. "L'homme a r�ussi � quitter le commissariat. Il a travers� la rue et aurait �vol� une voiture de la mairie qui �tait gar�e sur le parking en face", a �d�clar� � l'AFP cette source, confirmant une information du journal Le Parisien .
�
"Il venait de parler � son avocat et se trouvait dans la zone d'attente du �commissariat quand il a r�ussi � s'enfuir", a ajout� cette source, pr�cisant �que pour l'heure seul un "rapprochement" �tait fait entre l'�vasion et le vol �d'un v�hicule des services techniques de la municipalit�.
Un fugitif consid�r� comme "particuli�rement dangereux"
�
"La disparition du v�hicule nous a �t� rapport�e quelques minutes apr�s la disparition du d�tenu. Nous pensons qu'il est parti � son bord et il est activement recherch�", a-t-elle encore dit. Incarc�r� depuis 2007 pour tentative d'homicide, l'homme n'est pas signal� �comme particuli�rement dangereux par l'administration p�nitentiaire.
�
La Direction d�partementale de la s�curit� publique (DDSP) de �Seine-et-Marne a �t� charg�e d'une enqu�te administrative pour d�terminer les �circonstances de l'�vasion.
Commenter cet article
Vous devez �crire un avis
toutatis45 : Surtout pas menott�, cet individu aurait pu �tre traumatis� !!!!!!!!!!!!!!!!!!!!!!!!!!!!
Le 12/02/2014 � 09h00
saneman : Bon, en m�me temps s'il avait �t� menott� aux poignets et aux chevilles, comme aux Etats Unis, �a ne serait pas arriv� !!! Vive la France !!!
Le 12/02/2014 � 08h08
talcor : Un fugitif consid�r� comme "particuli�rement dangereux" l'homme n'est pas signal� comme particuli�rement dangereux par l'administration p�nitentiaire.
Le 12/02/2014 � 07h49
