TITRE: Coupe du Roi. Ronaldo et le Real sans piti� face � l'Atletico
DATE: 2014-02-11
URL: http://www.ouest-france.fr/coupe-du-roi-demi-finale-retour-atletico-real-madrid-en-direct-1922595
PRINCIPAL: 165107
TEXT:
Coupe du Roi. Ronaldo et le Real sans piti� face � l'Atletico
Espagne -
Cristiano Ronaldo a fini le travail chez le voisin madril�ne.�|�Photo : Reuters
Facebook
Achetez votre journal num�rique
Deux penalties de Cristiano Ronaldo ont permis au Real d'�carter de la Coupe du Roi l'Atletico, tenant du titre (2-0).
Malgr� un match houleux sur le terrain mais surtout en tribunes, avec notamment un briquet lanc� au visage de "CR7", les joueurs de Carlo Ancelotti ont confirm� leur large succ�s de l'aller (3-0).�
Et ils pourraient se voir offrir en finale mi-avril un clasico contre le FC Barcelone si ce dernier confirme mercredi (22 h) sur la pelouse de la Real Sociedad son avantage de deux buts, acquis la semaine derni�re (2-0).�
Le stade Vicente Calderon, bouillant face � l'ennemi merengue, n'en a pas reconnu son �quipe. Certains spectateurs sont m�me sortis de leurs gonds: Ronaldo, qui rentrait aux vestiaires � la pause, a re�u un briquet au visage et une bagarre a �clat� en tribune en seconde p�riode.�
Il faut dire que les "Colchoneros" se sont senti flou�s par deux d�cisions arbitrales qui ont permis au Real de prendre l'ascendant.�
Varane de retour comme titulaire
Au bout de seulement cinq minutes de jeu, Cristiano Ronaldo a d�boul� sur la gauche de la surface adverse et �t� accroch� par Javier Manquillo, conduisant l'arbitre � siffler. L'attaquant portugais, qui doit encore purger deux matches de suspension en Liga mais est libre de jouer en Coupe, s'est fait une joie de transformer (7e).�
Huit minutes plus tard, rebelote, avec cette fois une faute sur Gareth Bale. Ronaldo a r�cidiv� (16e), tuant dans l'oeuf toute esquisse de r�volte.�
Faute de beau football, cette rencontre a au moins permis � Karim Benzema de souffler sur le banc des rempla�ants et � Rapha�l Varane de faire son retour comme titulaire avec le Real. Le d�fenseur international fran�ais, remis de ses probl�mes � un genou, a su livrer une partie propre dans un match heurt�, disputant une rencontre dans son int�gralit� pour la premi�re fois depuis mi-novembre.�
ATLETICO MADRID - REAL MADRID : 0-2 (0-2)
BUTS. Ronaldo (7' sp, 16' sp).
Lire aussi
