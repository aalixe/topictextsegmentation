TITRE: 35 ans apr�s la r�volution islamique, l'Iran poursuit ses n�gociations nucl�aires - BFMTV.com
DATE: 2014-02-11
URL: http://www.bfmtv.com/international/35-ans-apres-revolution-islamique-liran-poursuit-negociations-nucleaires-707984.html
PRINCIPAL: 164625
TEXT:
35 ans apr�s la r�volution islamique, l'Iran poursuit ses n�gociations nucl�aires
Des dizaines de personnes c�l�brent l'anniversaire de la r�volution islamique � T�h�ran.
A.D. avec AFP
Les autorit�s iraniennes f�tent ce mardi le 35e anniversaire de la r�volution islamique. Une comm�moration marqu�e par des avanc�es dans les n�gociations nucl�aires avec les grandes puissances, dont l'ennemi historique am�ricain.
�� �
En d�but de matin�e, plusieurs dizaines de milliers de personnes affluaient autour de la place Azadi, au centre de T�h�ran, o� le pr�sident Hassan Rohani devait prononcer un discours.
���
La journ�e du 11 f�vrier, le 22 Bahman dans le calendrier iranien, est l'occasion de d�fil�s hauts en couleurs dans tout le pays pour rappeler l'arriv�e au pouvoir de l'imam Khomeiny et la chute du r�gime du Chah.
Rapprochement avec la communaut� internationale
Depuis son �lection, le pr�sident Rohani , qualifi� de mod�r�, a entam� une politique de rapprochement avec la communaut� internationale,  essentiellement pour mettre fin aux sanctions occidentales d�cr�t�es  contre T�h�ran en raison de son programme nucl�aire controvers� .
Les discussions sur un accord global doivent reprendre le 18 f�vrier � Vienne, en Autriche. Hassan Rohani b�n�ficie du soutien du guide supr�me iranien,  l'ayatollah Ali Khamenei, pour mener ces n�gociations � travers son  charismatique chef de la diplomatie, Mohammad Javad Zarif.
�� �
Mais ce dialogue est d�nonc� par les conservateurs, qui estiment trop  importantes les concessions faites aux Occidentaux et d�noncent les  rencontres trop nombreuses � leurs yeux de Mohammad Javad Zarif avec des  responsables am�ricains.
Pour l'Iran, il n'est encore pas question de renouer des relations  diplomatiques avec le "Grand Satan", rompues en 1980 apr�s la prise  d'otages de l'ambassade am�ricaine � T�h�ran par des �tudiants  islamistes.
A lire aussi
