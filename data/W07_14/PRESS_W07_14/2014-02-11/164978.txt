TITRE: ��La Belle et la B�te��, Christophe Gans au pays du conte | La-Croix.com
DATE: 2014-02-11
URL: http://www.la-croix.com/Culture/Cinema/La-Belle-et-la-Bete-Christophe-Gans-au-pays-du-conte-2014-02-11-1104919
PRINCIPAL: 164973
TEXT:
version papier version web
�
� 2014 ESKWAD � PATH� PRODUCTION � TF1 FILMS PRODUCTION ACHTE / NEUNTE / ZW�LFTE / ACHTZEHNTE BABELSBERG FILM GMBH � 120 FILMS
� 2014 ESKWAD � PATH� PRODUCTION � TF1 FILMS PRODUCTION ACHTE / NEUNTE / ZW�LFTE / ACHTZEHNTE BABELSBERG FILM GMBH � 120 FILMS
�LA BELLE ET LA B�TE�**���de Christophe Gans�
�Film fran�ais, 1�h�52�
En 1810, un riche marchand, veuf et p�re de six enfants, voit soudain sa fortune an�antie : ses trois bateaux ont disparu en mer. Il doit vendre le peu qui lui reste et trouver refuge � la campagne. Ses trois fils et deux de ses filles se plaignent de leur train de vie perdu. Mais la cadette, Belle, trouve des satisfactions dans sa nouvelle existence : leur p�re est davantage pr�sent, Maxime, l�a�n�, n�est plus dans le sillage des mauvaises fr�quentations de la ville, la vie � la ferme offre des bonheurs simples.�
Par un retournement du sort inattendu, la famille s�appr�te � retrouver son quotidien bourgeois et citadin : un des navires a �t� retrouv� avec tout son pr�cieux chargement. Avant le retour � la ville, les deux filles a�n�es r�clament au p�re une liste de parures pour faire bonne figure ; Belle, quant � elle, ne demande qu�une rose.�
En chemin, le marchand se perd, arrive dans un somptueux et myst�rieux ch�teau qui semble � l�abandon. Il y trouve le g�te, le couvert et les produits r�clam�s par ses filles, sauf la rose qu�il cueille, provoquant la col�re du ma�tre des lieux, la B�te, qui le laisse rentrer dire adieu aux siens avant de revenir pour mourir. Mais Belle d�cide de prendre la place de son p�re�
�
�
On le voit, Christophe Gans a accord� plus d�importance que ses pr�d�cesseurs au contexte du face-�-face de Belle et de la B�te. C�est qu�il s�est r�f�r� non pas au texte de Jeanne-Marie Leprince de Beaumont qui a abr�g� et rendu c�l�bre le conte, mais � l��uvre initiale de Gabrielle-Suzanne de Villeneuve, beaucoup plus �toff�e.�
C�est pourquoi le personnage du marchand occupe plus de place, tout comme son lien �troit avec sa fille � qui explique son sacrifice �, les raisons de la disgr�ce de la B�te, dont la laideur est le tribut de sa cruaut�, et l�influence des dieux.
Sans d�cor, sur fond vert�!�
Outre ces choix sc�naristiques, l�originalit� du film de Christophe Gans tient au recours au num�rique pour les d�cors. La ferme dans laquelle Belle s�ent�te � rendre les siens heureux, la for�t enneig�e o� s��gare son p�re, le somptueux ch�teau de la B�te laiss� � l�abandon au c�ur d�une vall�e encaiss�e : tout semble sorti des pages merveilleusement illustr�es d�un livre de conte ancien.�
Et quand le palais r�v�le ses secrets et que la montagne devient titan de pierre, le meilleur de l�animation donne une dimension magnifiquement onirique � ce r�cit. Il fallait d�excellents acteurs pour jouer de mani�re convaincante sans d�cor, sur fond vert. Andr� Dussollier (le p�re), L�a Seydoux (Belle) et Vincent Cassel (la B�te) se sont montr�s � la hauteur de ce d�fi.
CORINNE RENOU-NATIVEL
