TITRE: NKM, Sarkozy, un maire de 93 ans et une épouse dévouée - 11 février 2014 - Le Nouvel Observateur
DATE: 2014-02-11
URL: http://tempsreel.nouvelobs.com/elections-municipales-2014/20140211.OBS5893/nkm-sarkozy-un-maire-de-93-ans-et-une-epouse-devouee.html
PRINCIPAL: 0
TEXT:
Actualité > Politique > Municipales 2014 > NKM, Sarkozy, un maire de 93 ans et une épouse dévouée
NKM, Sarkozy, un maire de 93 ans et une épouse dévouée
Publié le 11-02-2014 à 17h07
Mis à jour à 18h29
Et aussi : les quais de Seine pass�s au vert fluo,�un sondage � Montpellier... Voici le "Bulletin des municipales".
Le soutien : Nicolas Sarkozy le "fid�le"
Pour la premi�re fois depuis sa d�faire � l'�lection pr�sidentielle de 2012, Nicolas Sarkozy a assist� � un meeting, celui de Nathalie Kosciusko-Morizet, lundi soir � Paris. La salle lui a fait un triomphe. Et en a presque oubli� sa candidate...�> "Plut�t que d'ovationner NKM, on a fait un triomphe � Sarkozy" , reportage de Ma�l Thierry.
Il n'est pas certain que son soutien soit de nature � convaincre les �lecteurs de l'Est parisien� qui avaient massivement vot� contre Nicolas Sarkozy�� la pr�sidentielle. Il n'avait recueilli que 32% des voix au second tour dans le 11e arrondissement, o� se tenait le meeting.
Nathalie Kosciusko-Morizet elle-m�me le reconna�t : "La campagne se passe sur le terrain, dans les 20 arrondissements". Mais la d�put�e de l'Essonne veut voir dans la venue de l'ex-pr�sident "un signal fort, tr�s amical, un signal de fid�lit�". Fid�lit�, le mot est l�ch�, ce sera le mot d'ordre des commentaires � droite en ce mardi. Ainsi de Brice Hortefeux, qui salue "un t�moignage de fid�lit� dans l'amiti�".
Les commentaires sont moins am�nes � gauche. La candidate PS Anne Hidalgo a raill� ce matin sur Europe 1 "le retour de Nicolas Sarkozy soutenu par son ancienne porte-parole". Selon elle, la candidate UMP "se trompe d'�lection depuis le d�but, elle pense � faire une anticipation de la pr�sidentielle".
Tandis que "Le Figaro" �voque la venue�de�Nicolas Sarkozy sur sa "une"�
La Une: les Suisses et l'immigration, Nicolas Sarkozy soutient @nk_m , retour sur la d�mission de Beno�t XVI... pic.twitter.com/ZZqhcdgOgQ
� Le Figaro (@Le_Figaro) February 11, 2014
... un photo-montage circule sur Twitter :
�
Le sondage : Montpellier aime les dissidents
Philippe Saurel, le candidat PS dissident � Montpellier, progresse de 5 points en un mois, selon un sondage publi� ce matin dans le quotidien " Midi Libre ".�Il passe � 17% au premier tour. Ce qui le place�encore bien loin du candidat officiel du PS, Jean-Pierre Moure, soutenu par EELV et le PCF , � 31% d'intentions de vote.
Jacques Domergue, candidat UMP-UDI-Modem, perd un point � 26%, affaibli par la pr�sence d'une liste dissidente centriste. Le FN perd 3 points, � 13%, et le Front de gauche est � 10%.
- Sondage Ifop r�alis� par t�l�phone du 3 au 5 f�vrier aupr�s d'un �chantillon repr�sentatif de la population de la ville de Montpellier de 602 personnes.
L'interview : Kechiche soutient Estrosi, "le meilleur rempart contre le FN"
Abdellatif Kechiche, ouvertement de gauche, a confirm� son soutien au maire UMP sortant Christian Estrosi dans une interview accord�e dimanche � "Nice-Matin". "Estrosi est pour moi le meilleur rempart contre le Front national", a affirm� le r�alisateur � Rue89 ce mardi 11 f�vrier. Le cin�aste, qui a grandi dans la cit� des Moulins � Nice, soutient l'homme "qui s'occupe des rues", plus que le politicien.
La vid�o : je suis in�ligible, votez pour ma femme !
Le maire radical de gauche de Propriano (Corse-du-Sud), Paul-Marie Bartoli, a �t� condamn� � un an d'in�ligibilit�. Qu'� cela ne tienne, c'est sa femme Caroline Bartoli qui sera candidate pour lui. Et celle-ci ne fait m�me pas semblant de vouloir ! "Toute la Corse conna�t les raisons pour lesquelles je conduis la liste de la municipalit� sortante, nous avons agi en toute transparence", dit-elle, lisant ses notes - et tant pis si c'est � c�t� de la question pos�e.
La surprise : Kechiche de gauche soutient Estrosi de droite
Abdellatif Kechiche, r�alisateur ouvertement de gauche, a confirm� son soutien au maire UMP sortant Christian Estrosi dans une interview accord�e � "Nice-Matin" �dimanche 9 f�vrier. "Estrosi est pour moi le meilleur rempart contre le Front national", a expliqu�le r�alisateur � Rue89 ce mardi 11 f�vrier. Le cin�aste, qui a grandi dans la cit� des Moulins � Nice, soutient l'homme "qui s'occupe des rues", plus que le politicien.�
L'insolite : le plus vieux maire de France ne fera pas de 12e mandat
"J'aurai 94 ans dans trois mois, un �ge o� on peut �tre fatigu�. (�) J'aurais bien voulu continuer, mais voil�, l'�ge est l�, ma sant� est ce qu'elle est et les miens m'ont convaincu d'arr�ter !" Roger S�ni� renonce � briguer un 12e mandat de maire de La Bastide-de-Bousignac (Ari�ge). Et il n'est pas n� celui qui reprochera, apr�s 66 ann�es de mandat !
Elu pour la premi�re fois en 1947, Roger S�ni� restait un maire tr�s actif, � en croire " La D�p�che du Midi ". Au printemps 2013, ce gaulliste avait d�missionn� de son mandat pour protester contre une perte fiscale de 145.000 euros li�e au rattachement de sa commune � la communaut� de communes de Mirepoix. Il avait �t� confort� par les 330 habitants de la commune lors d'une nouvelle �lection.
Roger S�ni� reste jusqu'au scrutin le co-doyen des maires de France avec Arthur Richier. D'un an son cadet, il est �lu depuis 1947 � Faucon-du-Caire, dans les Alpes-de-Haute-Provence. Lui qui se vante d'avoir "�puis� 19�pr�fets" ne se repr�sentera pas non plus .
Le tweet : la vie en Vert sur les quais de Seine
Un dessin vaut-il mieux qu'un long discours ? Dans le doute, ce sera les deux ! Le�candidat EELV � la mairie de Paris, Christophe Najdovski , a fait sur Twitter la promotion de�son projet de parc le long de la Seine, � quelques heures de son meeting de ce mardi soir :
#VIvreMieuxAParis Je veux que les berges de Seine soient inte?gralement reconquises http://t.co/MQPdGDxtHv #mun75000 pic.twitter.com/fO8xcI54rn
� Christophe Najdovski (@C_Najdovski) February 11, 2014
Baptiste Legrand - Le Nouvel Observateur�
Partager
