TITRE: Hollande arrive aux Etats-Unis, s'envole avec Obama vers le fief de Jefferson - 11 février 2014 - Le Nouvel Observateur
DATE: 2014-02-11
URL: http://tempsreel.nouvelobs.com/topnews/20140210.AFP9747/francois-hollande-arrive-aux-etats-unis-pour-une-visite-de-trois-jours.html
PRINCIPAL: 0
TEXT:
Actualité > Monde > Hollande arrive aux Etats-Unis, s'envole avec Obama vers le fief de Jefferson
Hollande arrive aux Etats-Unis, s'envole avec Obama vers le fief de Jefferson
Publié le 10-02-2014 à 21h01
Mis à jour le 11-02-2014 à 06h45
A+ A-
Le président français François Hollande est arrivé lundi aux Etats-Unis pour une visite d'Etat de trois jours au cours de laquelle il sera reçu en grande pompe par son homologue Barack Obama. (c) Afp
Washington (AFP) - Le président français François Hollande est arrivé lundi aux Etats-Unis pour une visite d'Etat de trois jours, riche en symboles, qui débute par une excursion avec Barack Obama en Virginie, aux sources de l'amitié entre Washington et Paris.
Vol dans Air Force One, honneurs militaires, fastueux dîner: les Etats-Unis vont couvrir d'attentions M. Hollande lors du volet washingtonien de cette visite, la première du genre d'un chef d'Etat français en 18 ans.
L’Airbus présidentiel français s’est posé peu avant 14H30 (19H30 GMT) sur la base aérienne d’Andrews, à l’est de Washington. Après une cérémonie officielle, MM. Obama et Hollande devaient prendre l’avion Air Force One pour une visite de quelques heures sur le domaine de Monticello, fief de Thomas Jefferson, le plus francophile des présidents américains.
Acteur de premier plan de l'indépendance soutenue par la France contre le pouvoir colonial britannique, Jefferson fut l'un des premiers représentants diplomatiques des Etats-Unis à Paris.
L'escapade champêtre de Monticello, à 200 km au sud-ouest de Washington, permettra aux deux dirigeants de parler, de manière informelle, "des relations (franco-américaines) dans un environnement évoquant leur histoire, et des possibilités qu'elles recèlent", explique à l'AFP un haut responsable de l'administration Obama.
Le cadre sera plus solennel le lendemain à 09H00 (14H00 GMT) sur la pelouse sud de la Maison Blanche pour une arrivée ponctuée de 21 coups de canon, des hymnes nationaux et d'un passage en revue de troupes.
Les deux présidents ont lancé un appel à un accord ambitieux sur le climat, enjeu d'une conférence à Paris l'an prochain. Dans une tribune commune lundi dans Le Monde et le Washington Post, ils demandent "à tous les pays à s'associer à notre recherche d'un accord mondial ambitieux et global pour la réduction des émissions de gaz à effet de serre par des mesures concrètes".
Souhaitant "une alliance transformée" entre leurs deux pays, ils évoquent aussi leurs positions communes sur l'Iran et la Syrie, et soulignent que l'Afrique "du Sénégal à la Somalie" est le théâtre "le plus visible" du "nouveau partenariat" noué par leurs deux pays, citant le Mali, le Sahel et la Centrafrique.
La NSA, dossier épineux
MM. Obama et Hollande s'entretiendront mardi dans le bureau ovale avant une conférence de presse. Les dossiers internationaux (Syrie, Iran, Ukraine, Sahel et Libye) devraient être évoqués, ainsi que les relations économiques.
Si les Etats-Unis se sont extraits des effets de la crise de 2008, avec un taux de chômage retombé à 6,6%, la France peine à se sortir du marasme et sa croissance reste faible. M. Hollande, dont la cote de confiance oscille autour de 20%, n'a pas réussi à tenir sa promesse d'inverser la courbe du chômage en 2013.
M. Hollande doit dîner lundi soir à la résidence de France avec la directrice générale du FMI, Christine Lagarde, et le président de la Banque mondiale , l'Américain Jim Yong Kim. Il conclura sa visite mercredi par une escale à San Francisco, berceau d'entreprises informatiques de pointe.
Les liens entre Obama et Hollande reposent sur "une relation personnelle de qualité", affirme-t-on à l'Elysée, la Maison Blanche exaltant l'amitié durable entre Paris et Washington.
Ombre au tableau, les pratiques de l'agence de renseignement NSA, visant notamment les alliés européens des Etats-Unis et mises au jour par l'ancien consultant Edward Snowden en 2013.
Ces révélations ont ouvert "une période difficile, non seulement entre la France et les Etats-Unis, mais aussi entre l'Europe et les Etats-Unis ", a déclaré le président français au magazine Time.
Après un déjeuner officiel sous les auspices du chef de la diplomatie , John Kerry, et un hommage aux militaires américains, M. Hollande sera mardi soir l'invité d'honneur d'un dîner d'Etat rassemblant plusieurs centaines de convives triés sur le volet à la Maison Blanche.
Officiellement, la rupture de M. Hollande et Mme Valérie Trierweiler après la révélation d'une liaison entre le président et une actrice n'a pas eu de conséquences sur le programme. Le dirigeant français se présente seul pour cette visite d'Etat à Washington, comme l'avait fait le Chinois Hu Jintao en 2011.
Mais le New York Times affirmait ce week-end que la séparation annoncée fin janvier, très commentée par les éditorialistes américains, avait contraint la Maison Blanche à imprimer de nouveaux cartons d'invitation sans le nom de Mme Trierweiler.
Partager
