TITRE: VIDEO. �Tous � Poil� au top des ventes de livres gr�ce � Jean-Fran�ois Cop� - 20minutes.fr
DATE: 2014-02-11
URL: http://www.20minutes.fr/television/1296038-20140211-video-tous-a-poil-top-ventes-livres-grace-a-jean-francois-cope
PRINCIPAL: 164072
TEXT:
Twitter
Plateau de Mots crois�s du 10 f�vrier 2014 DR
CULTURE � Le coup de sang de Jean-Fran�ois Cop� contre l�ouvrage de jeunesse �Tous � poil� et les vives r�actions de d�fense ont boost� les ventes du livre�
Jean-Fran�ois Cop� hisse Tous � poil au top des ventes. Cet ouvrage de litt�rature jeunesse occupe selon Le Figaro la quatri�me place des meilleures ventes Amazon ce mardi.
Depuis dimanche et� l�intervention de Jean-Fran�ois Cop� sur le plateau du Grand Direct LCI Le Monde, Tous � poil est l�objet de multiples discussions, ce qui explique son statut de best-seller en devenir.
Sur le plateau, Jean-Fran�ois Cop� avait expliqu頫A poil la ma�tresse, vous voyez c'est bien pour l'autorit� des professeurs! Vous avez aussi � poil le docteur, � poil le pr�sident directeur g�n�ral��, pr�cisant que l�ouvrage��fait partie du centre de documentation p�dagogique et fait partie des listes command�es aux enseignants pour faire la classe aux enfants de primaire�. Une affirmation erron�e puisque Tous � poil n�a jamais fait officiellement partie, comme le notait Le Monde , d�une liste d�ouvrages recommand�s par les enseignants. Par ailleurs, de nombreuses voix outre celles des auteurs se sont �lev�es depuis ce week-end pour vanter les m�rites de l�ouvrage. Il a notamment fait l�objet d�une longue discussion lundi soir sur le plateau de Mots Crois�s sur France 2 entre Najat Vallaud-Belkacem, ministre des Droits des femmes et porte-parole du gouvernement, et Nadine Morano.
Vid�o : Thomas Lemoine
Sur ce m�me plateau, Nadine Morano, ex-ministre de la Famille du gouvernement Fillon,�a aussi �voqu� la n�cessit� de �nettoyer les biblioth�ques�. Un sujet sur lequel s�est exprim�e lundi soir la ministre de la Culture Aur�lie Filippetti, d�non�ant dans un communiqu� publi� par l�AFP les pressions contre �une trentaine de biblioth�ques publiques� par �des mouvements extr�mistes� qui �exigent le retrait de la consultation de tout ouvrage ne correspondant pas � la morale qu'ils pr�tendent incarner�.
Alice Coffin
