TITRE: 20 Minutes Online - Les manifestations contre la pauvret� continuent - Monde
DATE: 2014-02-11
URL: http://www.20min.ch/ro/news/monde/story/Les-manifestations-contre-la-pauvret--continuent-10305207
PRINCIPAL: 164089
TEXT:
Bosnie
Les manifestations contre la pauvret� continuent
Plusieurs centaines de protestataires ont r�clam� mardi la d�mission du gouvernement de l'entit� croato-musulmane de ce pays ethniquement divis� et en proie � une grave crise �conomique.
�D�mission! Voleurs!�, ont scand� les manifestants rassembl�s devant le b�timent de la pr�sidence du pays, incendi� dans les violences de vendredi, a constat� l'AFP. Ils ont ensuite march� vers le si�ge du gouvernement de l'entit� croato-musulmane dont il r�clament le d�part. Un important dispositif de la police anti-�meutes y �tait d�ploy�.
�Vive la fraternit� et l'unit�!�, ont scand� les manifestants, un slogan du temps de la Yougoslavie communiste dont la Bosnie a �t� l'une des six r�publiques, jusqu'� son �clatement sanglant au d�but des ann�es 1990. Une femme en pleurs a appel� les policiers � rejoindre les manifestants. �Vous devriez prot�ger les citoyens et pas les voleurs au pouvoir!�, a-t-elle lanc�.
Le Premier ministre de l'entit� croato-musulmane -- qui, avec une entit� serbe, forme la Bosnie depuis la fin de la guerre inter-communautaire (1992-95) --, Nermin Niksic, un musulman, refuse de d�missionner, affirmant que cette entit� risquerait alors d'�tre �paralys�e�. Sa formation, le Parti social-d�mocrate (SDP), qui appelle � la tenue des �lections anticip�es, a soumis mardi un amendement au code p�nal visant � rendre possible un tel scrutin huit mois avant la date pr�vue.
Mais en raison d'un syst�me politique extr�mement complexe, une telle mesure n�cessite un consensus avec les repr�sentants politiques serbes et croates. Comme ces derniers ont d�j� exprim� leur opposition � la tenue des �lections anticip�es, cette initiative a tr�s faibles chances d'�tre adopt�e par le Parlement central de ce pays ethniquement divis�. Des manifestations ont �galement eu lieu � Tulza (nord-ouest), Mostar (sud), Zenica (centre) et Brcko (nord).
(afp)
