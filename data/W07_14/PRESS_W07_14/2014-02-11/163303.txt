TITRE: Un ex-cryptographe de la marine US condamn� pour tentative d'espionnage au profit de la Russie - Derni�res infos - Soci�t� - La Voix de la Russie
DATE: 2014-02-11
URL: http://french.ruvr.ru/news/2014_02_11/Un-ex-cryptographe-de-la-marine-US-condamne-pour-tentative-despionnage-au-profit-de-la-Russie-7755/
PRINCIPAL: 0
TEXT:
Un ex-cryptographe de la marine US condamn� pour tentative d'espionnage au profit de la Russie
� Photo : RIA Novosti
Par La Voix de la Russie | Un ancien sp�cialiste des informations crypt�es de la marine am�ricaine Robert Patrick Hoffman a �t� condamn� � 30 ans de prison pour tentative de fournir des informations secr�tes � la Russie.
Ce technicien qui avait servi pendant 20 ans dans la marine des Etats-Unis et avait acc�s � des documents top-secret a pris sa retraite en novembre 2011. A la fin de 2012 il a �t� arr�t� au cours d'une op�ration secr�te du FBI.
Il a fourni � des agents du FBI qu'il croyait �tre des agents russes des donn�es secr�tes dont il avait acc�s pendant son service. Il a notamment transmis les informations permettant de d�tecter les sous-marins am�ricains et renseignant comment les Am�ricains r�p�raient des sous-marins �trangers.
