TITRE: Sarkozy attise les divisions au sein de l'UMP | Mediapart
DATE: 2014-02-11
URL: http://www.mediapart.fr/journal/france/110214/sarkozy-attise-les-divisions-au-sein-de-lump
PRINCIPAL: 164482
TEXT:
La lecture des articles est r�serv�e aux abonn�s.
Un bain de foule, trois sourires et puis s�en va. Lundi 10 f�vrier au soir, lors du premier grand meeting de Nathalie Kosciusko-Morizet, les militants UMP, r�unis au gymnase Japy (Paris XIe), ont oubli� pendant une heure la nature du rendez-vous organis� par la candidate � la mairie de Paris. Cette derni�re a eu beau discourir sur les reniements des socialistes et aborder quelques-uns des axes de son programme (ins�curit� et propret� en t�te), tous n�en avaient que pour Nicolas Sarkozy, qui apparaissait pour la premi�re fois dans un meeting politique depuis sa d�faite � la pr�sidentielle de 2012.
Arriv� apr�s l�intervention de la chef de file du MoDem, Marielle de Sarnez, l'ancien chef de l'�tat n�a pas pris la parole et s�est ...
