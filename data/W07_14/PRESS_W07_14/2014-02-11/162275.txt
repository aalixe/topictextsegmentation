TITRE: VIDÉO. "What else ?" Matt Damon ! | Site mobile Le Point
DATE: 2014-02-11
URL: http://www.lepoint.fr/medias/video-what-else-matt-damon-11-02-2014-1790407_260.php
PRINCIPAL: 0
TEXT:
11/02/14 à 07h03
VIDÉO. "What else ?" Matt Damon !
L'acteur américain est en train de chasser son aîné de la publicité pour la célèbre marque de café. Une révolution qui rend hystériques les midinettes.
Matt Damon et George Clooney ambassadeurs de Nespresso Capture d'écran YouTube
Par Albert Sebag
C'est le passage de relais le plus médiatique de l'histoire de la publicité. En novembre dernier, Matt Damon, 43 ans, a signé à son tour avec la marque Nespresso après la mythique prestation de son aîné, George Clooney, 52 ans, qui est devenu en quelques années beaucoup plus populaire que le fameux Gringo des pubs Jacques Vabre du siècle dernier.
L'année passée, on pouvait voir dans la même minute les deux acteurs dans un spot en deux actes. Sur une terrasse, Clooney s'assoit à côté d'une créature sublime qui est en train de terminer sa tasse de Volluto. George, toujours classieux, lui propose une nouvelle tasse. Ce qu'elle accepte dans la seconde. Dès que George se lève, elle se jette sur le café brûlant du sex-symbol en hurlant : "George Clooney est à l'intérieur !", provoquant l'hystérie de la dizaine de top models qui se ruent alors sur l'acteur.
Matt Damon is inside !
À un autre moment, Clooney et Damon se disent au revoir. Matt reste dans le lounge et George déguerpit tel un chat en hurlant à son tour : "Matt Damon est à l'intérieur !" Mais regardez la pub. On ne ruse pas aussi facilement avec Damon. Clooney devrait le savoir, lui qui a tourné à ses côtés dans Ocean's Eleven, Twelve et Thirteen... Et puisque George lui fait des farces, Matt s'est désormais débarrassé de lui dans la nouvelle pub Nespresso où la créature - l'ex-Miss suisse Lauriane Gilliéron, toujours aussi canon - devise avec Damon et s'étonne de ne pas le voir plus souvent sur cette terrasse Nespresso. C'est qu'il préfère commander avec son application. Plus discret, non ? Et la luciférienne de s'éclipser en hurlant devant la baie vitrée : "Matt Damon is inside !" Inutile de traduire... Tout le monde a compris. Et le spectateur averti que Mister What Else vient de trouver un digne et définitif remplaçant...
REGARDEZ la pub Nespresso avec Matt Damon :
