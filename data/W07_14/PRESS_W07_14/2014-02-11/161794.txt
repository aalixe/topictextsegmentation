TITRE: Wall Street en l�g�re hausse � la veille d'une audition de Janet Yellen - Flash actualit� - Economie - 10/02/2014 - leParisien.fr
DATE: 2014-02-11
URL: http://www.leparisien.fr/flash-actualite-economie/wall-street-ouvre-en-legere-baisse-10-02-2014-3577511.php
PRINCIPAL: 161793
TEXT:
Wall Street en l�g�re hausse � la veille d'une audition de Janet Yellen
Publi� le 10.02.2014, 15h40
Tweeter
La Bourse de New York a termin� en l�g�re hausse lundi, restant prudente avant une intervention tr�s attendue de la nouvelle patronne de la banque centrale am�ricaine mardi: le Dow Jones a grignot� 0,05% et le Nasdaq, stimul� par Apple, 0,54%. | John Moore
R�agir
La Bourse de New York a termin� en l�g�re hausse lundi, restant prudente avant une intervention tr�s attendue de la nouvelle patronne de la banque centrale am�ricaine mardi: le Dow Jones a grignot� 0,05% et le Nasdaq , stimul� par Apple , 0,54%.
Selon des r�sultats d�finitifs, le Dow Jones s'est adjug� 7,71 points � 15.801,79 points et le Nasdaq, � dominante technologique, 22,31 points � 4.
Vos amis peuvent maintenant voir cette activit� Supprimer X
148,17 points.
L'indice �largi S&P 500 a gagn� 0,16% ou 2,82 points, � 1.799,84 points.
Le Dow Jones et le S&P 500 ne sont parvenus � se hisser dans le vert qu'en fin de s�ance, le march� faisant preuve de retenue � la veille de la premi�re intervention publique de Janet Yellen depuis son arriv�e officielle � la t�te de la R�serve f�d�rale am�ricaine (Fed) le 3 f�vrier.
"La grande question en ce moment est de savoir si la reprise va continuer", a relev� Patrick O'Hare de Briefing.com. "La r�ponse � cette question est loin d'�tre �vidente puisque nombre des raisons (qui ont fait vaciller les indices en d�but d'ann�e) sont toujours pr�sentes": "les march�s �mergents font toujours face � une fuite des capitaux, les questions sur les croissances chinoise et am�ricaine restent d'actualit� et les pr�visions annonc�es par les entreprises ce premier trimestre sont majoritairement d�cevantes".
Dans ce contexte, les investisseurs se tournent de nouveau vers la Fed, qui a annonc� en d�cembre qu'elle allait progressivement mettre un terme en 2014 � son programme de rachats d'actifs. Cette mesure destin�e � vivifier l'�conomie gr�ce � l'injection massive de liquidit�s sur les march�s financiers a largement b�n�fici� � Wall Street en 2013.
"Je ne peux pas imaginer que Janet Yellen va dire quoi que ce soit qui fasse bouger les attentes sur ce programme", a avanc� Alan Skrainka de Cornerstone Wealth Management. "Ben Bernanke lui a fait une grande faveur en initiant le ralentissement" des aides de l'institution � "un rythme tr�s raisonnable et r�aliste", a-t-il ajout�.
Peter Cardillo de Rockwell Global Capital, s'attend � la m�me prudence de la part de Mme Yellen.
Le seul �l�ment sur lequel elle pourrait s'�tendre davantage est, selon lui, le taux de ch�mage qui est tomb� en janvier � 6,6%. La Fed pr�voit actuellement de commencer � remonter ses taux d'int�r�t quand il repassera en-dessous de 6,5%.
Google devant ExxonMobil
La progression du Nasdaq a aussi �t� aliment�e par la hausse de 1,79% d'Apple (� 528,99 dollars), son composant le plus important. L'investisseur Carl Icahn a indiqu� qu'il renon�ait � exiger de la soci�t� un programme de rachats d'actions plus important que celui en place actuellement.
Malgr� un repli de 0,38% � 1.172,93 dollars, le g�ant de l'internet Google a de son c�t� d�log� le groupe p�trolier ExxonMobil (-1,17% � 89,52 dollars) de la place de deuxi�me plus grosse capitalisation boursi�re au monde derri�re Apple.
Autre poids lourd de la cote, McDonald's a perdu 1,11% � 94,86 dollars apr�s des ventes d�cevantes en janvier aux Etats-Unis.
Le fabricant de jouets Hasbro s'est adjug� 4,53% � 52,36 dollars malgr� des r�sultats inf�rieurs aux attentes. Le groupe a aussi indiqu� qu'il relevait son dividende trimestriel.
Le site internet Yelp, qui propose des commentaires notamment sur les restaurants, a progress� de son c�t� de 1,90% � 91,11 dollars apr�s des informations de presse sur un partenariat avec Yahoo! (+1,42% � 37,76 dollars).
La cha�ne de librairies en difficult�s Barnes and Noble a bondi de 8,81% � 16,06 dollars. Le groupe a indiqu� qu'il allait supprimer des emplois dans la division de sa liseuse �lectronique Nook.
Le march� obligataire a termin� sur une note contrast�e. Le rendement des bons du Tr�sor � 10 ans a progress� � 2,678% contre 2,675% vendredi soir et celui � 30 ans a recul� � 3,663% contre 3,665% � la pr�c�dente cl�ture.
