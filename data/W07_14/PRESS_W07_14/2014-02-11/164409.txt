TITRE: Manger bio, est-ce bon pour la sant� ? | meltyFood
DATE: 2014-02-11
URL: http://www.meltyfood.fr/manger-bio-bon-pour-la-sante-a252360.html
PRINCIPAL: 164405
TEXT:
Manger bio, est-ce bon pour la sant� ?
6r�actions
il y a 57 jours
Publi� le 11/02/2014 17:00
La plus grande �tude jamais r�alis�e sur les b�n�fices du bio va nous permettre de conna�tre les vrais impacts de ce choix alimentaire. La r�daction de meltyFood vous en dit plus.
Une nouvelle �tude BioNutriNet, pilot�e par le Dr Emmanuelle Kess, va nous permettre de conna�tre les facteurs qui d�terminent la consommation d'aliments bio ainsi que ses effets nutritionnels et toxicologiques. Les responsables de l'�tude qui durera 5 ans cherchent 100 000 participants consommateurs et non consommateurs de produits bio. Pour s'inscrire, rendez-vous sur le site etude.nutrinet-sante.fr . Nous nous �tions d�j� demand�s si manger bio signifiait manger mieux . Une premi�re �tude r�alis�e sur un �chantillon de 54 300 fran�ais publi�e en octobre 2013 dans la revue PloS One illustrait d�j� certaines tendances. Ainsi seulement 7% de la population fran�aise mangent bio r�guli�rement. Ces personnes seraient en moyenne plus actives et plus �duqu�es que les autres . Enfin les mangeurs bio sont souvent moins sujets au surpoids m�me s'ils mangent des rations caloriques �quivalentes � celles des autres.
Alimentation bio
