TITRE: L'essentiel de l'actu du 11 f�vrier 2014: Hollande aux Etats-Unis et NKM boost�e par Sarkozy - BFMTV.com
DATE: 2014-02-11
URL: http://www.bfmtv.com/politique/lessentiel-lactu-11-fevrier-hollande-aux-etats-unis-nkm-boostee-sarkozy-707420.html
PRINCIPAL: 162114
TEXT:
r�agir
> Hollande en visite d'Etat aux Etats-Unis
Fran�ois Hollande est arriv� lundi aux Etats-Unis pour une visite d'Etat, qui a d�but� par une excursion avec Barack Obama sur le domaine de Monticello, fief de Thomas Jefferson. Dans la soir�e, le pr�sident fran�ais �tait attendu pour d�ner avec la directrice g�n�rale du FMI, Christine Lagarde, et le pr�sident de la Banque mondiale, l'Am�ricain Jim Yong Kim.
Mardi matin, Obama et Hollande s'entretiendront dans le bureau ovale avant une  conf�rence de presse. Les dossiers internationaux - Syrie, Iran,  Ukraine, Sahel et Libye -� devraient �tre �voqu�s, ainsi que les  relations �conomiques entre les deux pays.
> La campagne de NKM boost�e par Sarkozy
Nicolas Sarkozy a apport� lundi soir son soutien � Nathalie Kosciusko-Morizet , en assistant sous les ovations des militants au premier grand meeting de la candidate UMP � la mairie de Paris.
"C'est un geste d'amiti� pour Nathalie qui a �t� une porte-parole  courageuse pendant ma campagne [la pr�sidentielle 2012, NDLR]", a  d�clar� l'ancien pr�sident � BFMTV, avant son entr�e triomphale dans la  salle. Sa pr�sence, a-t-il assur�, rev�t "une seule connotation:  l'amiti� pour Nathalie, et l'admiration que j'ai pour elle".
En revanche, l'ex-chef d'Etat n'a pas pris la parole � la tribune , et a quitt� la  salle apr�s environ une demi-heure, � la fin du discours de NKM.
> Le rapport annuel de la Cour des comptes
Le premier pr�sident de la Cour des comptes Didier Migaud pr�sente mardi � 9h30 le rapport public annuel de l'Institution. A cette occasion, il donnera son avis sur les perspectives de finances publiques pour 2014, mais d�signera aussi quelques invraisemblances et abus dans les politiques publiques.
L'ann�e derni�re, la Cour des comptes avait �mis de s�rieuses r�serves quant au retour du d�ficit public � 3% en 2013. Et avait invit� le gouvernement � r�aliser davantage d'�conomies..
> Ayrault donne sa feuille de route sur l'int�gration
Deux jours apr�s les restrictions � l'immigration vot�es par la Suisse , le gouvernement pr�sente mardi sa feuille de route sur l'int�gration avec la volont� de ne pas retomber dans les pol�miques et malentendus qui avaient plomb� ce dossier en d�cembre.
�� �
La quasi totalit� des ministres sont convi�s � 17h � Matignon pour �tablir cette feuille de route. La gestion de la politique d'int�gration devrait �tre confi�e � une "instance de coordination" qui agira au niveau interminist�riel, selon l'entourage du Premier ministre.
> Affaire Bettencourt: la validation de l'enqu�te examin�e en cassation
Les avocats de plusieurs protagonistes de l'affaire Bettencourt vont contester mardi la validation de l'enqu�te dans le volet abus de faiblesse du dossier devant la Cour de cassation. Celle-ci se penchera par ailleurs sur les agendas de Nicolas Sarkozy.
L'ancien ministre du Budget Eric Woerth, le photographe Fran�ois-Marie Banier et son compagnon Martin d'Orgeval, l'ancien homme de confiance de Liliane Bettencourt Patrice de Maistre, l'avocat Pascal Wilhelm qui lui avait succ�d�, mais aussi l'homme d'affaires St�phane Courbit et l'ex gestionnaire de l'�le seychelloise d'Arros Carlos Cassina Vejarano, ont form� un pourvoi en Cassation .
M�me s'il a b�n�fici� d'un non-lieu et n'est pas renvoy� devant le tribunal, Nicolas Sarkozy a� maintenu son pourvoi. Il conteste le refus de la justice d'annuler la saisie des agendas qu'il utilisait pendant son mandat.
