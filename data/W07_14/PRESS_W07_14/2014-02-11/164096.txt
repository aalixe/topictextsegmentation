TITRE: Le "stand-up" de NKM en 4 vid�os : le culot d'une boxeuse - Politique - MYTF1News
DATE: 2014-02-11
URL: http://lci.tf1.fr/politique/le-stand-up-de-nkm-en-4-videos-le-culot-d-une-boxeuse-8363830.html
PRINCIPAL: 164043
TEXT:
NKM l'attaquante�: "qui est Hollande pour montrer l'exemple�?"�
�
"Ne vous y trompez pas, c'est une tueuse". Ceux qui l'aiment ou la d�testent � droite font tous la m�me confidence aux journalistes qui enqu�tent sur cet animal politique de moins de 40 ans, au sourire toujours photog�nique. �Nathalie Kosciusko-Morizet est une redoutable adversaire et peut l�cher des attaques terribles contre ses adversaires. Lundi soir, elle a vis� sans vergogne Fran�ois Hollande sur le terrain de l'�thique, affirmant ��sans malice�� qu'il �tait ��bien mal plac� pour montrer l'exemple��.
�
NKM la com�dienne
�
Pour son premier grand meeting, Nathalie Kosciusko-Morizet a montr� un style � fait original. Phras� heurt� et pauses pour mieux convaincre, la candidate en a �tonn� plus d'un, bluff� ou amus� par ce semblant d'improvisation. Si elle a travaill� sa voix avec une amie com�dienne, la rivale de Anne Hidalgo n'a pas encore trouv�la bonne mani�re d'occuper l'espace de la sc�ne. Entre fausses confidences et envol�es de tribun, NKM regardera dans quelques ann�es ce meeting avec le sourire.�
�
