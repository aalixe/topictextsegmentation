TITRE: Avantages à la SNCF : Guillaume Pepy répond à Yves Thréard
DATE: 2014-02-11
URL: http://www.lefigaro.fr/societes/2014/02/11/20005-20140211ARTFIG00144-avantages-a-la-sncf-guillaume-pepy-repondez-moi.php
PRINCIPAL: 0
TEXT:
Envoyer par mail
Avantages à la SNCF : Guillaume Pepy répond à Yves Thréard
Chaque jour, Yves Thréard interpelle une personnalité. Notre éditorialiste s'adresse au président de la SNCF alors que la Cour des comptes dénonce le coût des billets à tarif réduit mis à disposition des familles de cheminots.
< Envoyer cet article par e-mail
X
Séparez les adresses e-mail de vos contacts par des virgules.
De la part de :
Avantages à la SNCF : Guillaume Pepy répond à Yves Thréard
Chaque jour, Yves Thréard interpelle une personnalité. Notre éditorialiste s'adresse au président de la SNCF alors que la Cour des comptes dénonce le coût des billets à tarif réduit mis à disposition des familles de cheminots.
J'accepte de recevoir la newsletter quotidienne du Figaro.fr
Oui
Directeur adjoint de la r�daction du Figaro
Ses derniers articles
Réagir à cet article
Publicité
rené bouldouyré 1
Pas seulement les cheminots et leur famille. Comptons sur les militaires(et ils sont nombreux)paient quart de place en first. Vient ensuite un large panel tel que les hauts fonctionnaires et tous les parlementaires et  famille. pour ne citer que cela; le ménage doit être fait pour tous ou pour personne.
Nous ne sommes pas sortis de l'auberge...
Le 13/02/2014 à 10:01
Blue Smoke
YT
Je ne dis pas qu'il n'y a pas des abus mais c'est un peu facile comme article. Si on parlait des avantages des journalistes....
Le 12/02/2014 à 07:01
c'est surtout sur le prix absurde que demande la sncf pour ses grandes lignes qu'il faut interpeler pepy! c'est absolument scandaleusement prohibitif ces prix!
Le 12/02/2014 à 05:55
Ce ne sont pas des billets à tarif réduit, mais des billets gratuits ! Nuance !! Un vrai scandale auquel il faut mettre fin. Car, il parait que 20% seulement de ceux qui en profitent sont des cheminots !!
Le 11/02/2014 à 23:51
"Il paraît que"
Quelle fine analyse. Voici la mienne en tant que "cheminote":
Comment la cours des comptes peut elle chiffrer le coût de cet avantage social alors que la majorité des ces facilités de circulations est :
variable en nombre et en âge selon le lien de parenté.
Annoncer 1.1 million de bénéficiaires comme si leurs droits étaient identiques et perpétuels...
Ne pas parler des conditions d'obtention (âge, situation financière et administrative pour les enfants)
Ne pas dire qu'ils sont limités à 2 aller/retour par an pour les grands-parents.
Non cumulables pour les ayants droits lorsque plusieurs membres d'une même famille sont cheminots.
En effet dans ce cas UN SEUL membre de la famille cheminot est délégataires des facilités de circulation des ayants droits.
On s'étonne que la proportions d'actifs par rapport aux ayants droits n'est que de 15% alors que l'effectif pour exemple est passé de plus de 500 000 en 1938 à 145 000 aujourd'hui.
On ne parle pas de la structure de la pyramide des âges des effectifs de l'entreprise qui génère des départs en retraites en masse depuis plusieurs années pour expliquer cette proportion de 15%....
Pour finir
COMMENT la Cours des Comptes peut elle définir cette somme alors que la majorité des facilités de circulation sont validés par leurs porteur par l'inscription de la date du voyage et ou le compostage du billet qui de ce fait ne peut chiffrer sa valeur puisque la distance, la fréquence, la destination ne sont pas connus ?
Le 16/02/2014 à 08:26
