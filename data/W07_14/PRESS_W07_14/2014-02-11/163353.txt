TITRE: L'OREAL : L'Or�al acc�l�re au T4 et d�gage une rentabilit� record, infos et conseils valeur FR0000120321, OR - Les Echos Bourse
DATE: 2014-02-11
URL: http://bourse.lesechos.fr/infos-conseils-boursiers/infos-conseils-valeurs/infos/l-oreal-accelere-au-t4-et-degage-une-rentabilite-record-949885.php
PRINCIPAL: 163350
TEXT:
L'OREAL : L'Or�al acc�l�re au T4 et d�gage une rentabilit� record
10/02/14 � 19:09
La croissance organique d�passe l�g�rement les attentes au T4
Le luxe acc�l�re en fin d'ann�e, le grand public faiblit
Le groupe se dit confiant pour 2014
LOr�al acc�l�re au T4 et d�gage une rentabilit� record | Cr�dits photo : � L'or�al
par Pascale Denis
PARIS, 10 f�vrier (Reuters) - L'Or�al a boucl� l'ann�e 2013 sur une rentabilit� record et sur une acc�l�ration de sa croissance gr�ce � sa division de produits de luxe, alors que les investisseurs se focalisent surtout sur l'avenir des 29,5% que Nestl� d�tient dans le capital du groupe fran�ais.
Le num�ro un mondial des cosm�tiques, propri�taire de Garnier, Lanc�me ou Yves Saint Laurent Beaut�, a vu ses ventes progresser de 2,3% � 22,98 milliards d'euros, un chiffre proche du consensus Thomson Reuters I/B/E/S de 23,04 milliards, et sa croissance � donn�es comparables atteindre 5,0%.
Sur le seul quatri�me trimestre, elle a atteint 5,4%, d�passant l�g�rement les 4,5%-5,0% attendus par les analystes et marquant une acc�l�ration par rapport aux 4,9% des neuf premiers mois.
Cette progression a �t� largement tir�e par les produits de luxe (Lanc�me, Armani, Kiehl's, YSL Beaut�), dont la croissance organique a fortement acc�l�r� en fin d'ann�e � 8,4% (+6,2% sur neuf mois).
Sur l'ann�e, la division signe une performance semblable � celle des parfums et cosm�tiques de LVMH ( Dior , Guerlain, Givenchy) qui ont vu leur croissance atteindre 7%.
A l'inverse, la dynamique a nettement faibli dans les produits grand public (L'Or�al Paris, Garnier, Maybelline), premi�re division du groupe, dont la croissance a recul� � 3,7% au quatri�me trimestre, apr�s 5,3% sur neuf mois.
A quatri�me trimestre, L'Or�al a surtout redress� la barre en Am�rique du Nord (+3,7%), o� ses ventes avaient brutalement fl�chi au troisi�me trimestre, tandis qu'il a maintenu la cadence dans les nouveaux march�s (+9,4%) et a l�g�rement fl�chi en Europe de l'Ouest (+1,6%), dans un environnement peu porteur.
Le r�sultat d'exploitation, de 3,875 milliards d'euros (consensus Reuters 3,84 milliards), affiche une progression de 4,8%, tandis que la marge s'am�liore de 40 points de base pour atteindre un record � 16,9%.
CONF�RENCE MARDI
Le r�sultat net part du groupe ressort en hausse de 3,2% � 2,96 milliards, un chiffre proche du consensus de 3,08 milliards, et le dividende propos� est en augmentation de 8,7% � 2,50 euros par action.
"Les effets de change sont moins forts qu'attendu, la croissance organique plus forte", note Harold Thompson, analyste de Deutsche Bank qui salue aussi la nette hausse du dividende.
Fort de ces chiffres, L'Or�al s'est dit "confiant dans sa capacit� � surperformer le march� en 2014", sans plus de pr�cision.
L'Or�al tiendra une conf�rence de presse sur ses r�sultats mardi matin, o� l'avenir de la part de Nestl� devrait monopoliser l'attention apr�s des informations de presse laissant entendre que le groupe suisse �tudierait les voies d'une r�duction de sa part dans le groupe fran�ais.
Apr�s maints appels du pied de L'Or�al - son PDG a d'abord d�clar� que le groupe avait les moyens d'une grosse acquisition, avant d'�tre plus explicite et d'affirmer qu'un �ventuel rachat de la part de Nestl� serait relutif sur le titre l'Or�al - certains estiment que Nestl� pourrait avoir voulu tester le march� et que la faible r�action de Nestl� en Bourse lundi(-0,07%) t�moigne du peu d'entrain de ses actionnaires face � une telle �ventualit�.
La majorit� des analystes penchent toujours pour un statu quo, Nestl� n'ayant nul besoin de cash, faute de cibles d'importance s'int�grant dans sa strat�gie ax�e sur la nutrition, la sant� ou le bien-�tre.
Par ailleurs, l'op�ration serait rendue tr�s complexe par nombre d'obstacles r�glementaires, comme celui de l'�galit� de traitement de tous les actionnaires, dans le cas d'une cession de gr� � gr�, ou de la limite fix�e � 10% du capital qu'une entreprise peut racheter, et cela tous les deux ans.
La balle reste donc dans le camp de Nestl�, qui publie ses r�sultats annuels le 13 f�vrier, mais qui pourrait attendre la prise de parole de son pr�sident Peter Brabeck � l'occasion de l'assembl�e g�n�rale des actionnaires le 10 avril pour clarifier sa position.
Le pr�sident de Nestl� a fait de ce dossier une de ses principales pr�rogatives.
Le titre L'Or�al a fini en hausse de 4,45% � 129,00 euros � la Bourse de Paris lundi, dop� par la perspective d'un effet relutif d'une proc�dure d'annulation d'actions qui suivrait un rachat de la part de Nestl�. Depuis le d�but de l'ann�e, le titre grappille 1%, apr�s une progression de 21,7% l'an dernier.
Au cours de cl�ture de lundi, L'Or�al est valoris� � 77,57 milliards d'euros.
�
