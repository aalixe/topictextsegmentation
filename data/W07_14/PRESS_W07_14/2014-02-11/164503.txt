TITRE: Fraude aux allocations ch�mage : P�le emploi appel� � intensifier la lutte  - Capital.fr
DATE: 2014-02-11
URL: http://www.capital.fr/carriere-management/actualites/fraude-aux-allocations-chomage-pole-emploi-appele-a-intensifier-la-lutte-909910
PRINCIPAL: 164501
TEXT:
Tweet
� REA
P�le emploi doit faire davantage d'efforts en mati�re de lutte contre la fraude allocations ch�mage. C'est le constat de la Cour des comptes, qui a men� en 2013 un nouveau contr�le, afin de v�rifier si les recommandations r�alis�es dans le cadre de son rapport annuel de 2010 avaient �t� suivies d'effets.
Certes, des progr�s ont �t� r�alis�s, reconnaissent les Sages. P�le emploi a notamment approfondi son identification des principaux m�canismes de fraude, d�velopp� de nouveaux outils permettant de d�tecter des incoh�rences dans les dossiers trait�s et renforc� la collaboration avec les autres acteurs (Caf, police...). Les montants de fraude d�tect�e ont d'ailleurs fortement progress� ces derni�res ann�es, passant de 22,9 millions d'euros en 2009 � 76,3 millions en 2012.
Mais les actions engag�es par l'organisme public "demeurent en de�� des ambitions et des d�marches plus actives d�velopp�es par les organismes de s�curit� sociale", d�plore la Cour. En particulier concernant la mesure de la fraude : aucune �valuation du pr�judice potentiel total n'a pour le moment �t� r�alis�e.
Autre reproche : le manque d'efficacit� des sanctions. Pour gagner en rapidit�, les magistrats sugg�rent donc de transf�rer � P�le emploi le pouvoir de prononcer des sanctions administratives, actuellement d�volu aux pr�fets. Il conseillent aussi de d'augmenter le taux de saisine des juridictions p�nales, qui reste trop faible pour �tre dissuasif (7,5 % seulement en 2012, contre 13,6 % en 2009).
� Capital.fr
