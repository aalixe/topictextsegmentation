TITRE: Accessibilit� des handicap�s : Foix et Albi bons �l�ves, Cahors peut mieux faire - 11/02/2014 - LaD�p�che.fr
DATE: 2014-02-11
URL: http://www.ladepeche.fr/article/2014/02/11/1815748-accessibilite-handicapes-foix-albi-bons-eleves-cahors-peu-mieux-faire.html
PRINCIPAL: 0
TEXT:
Accessibilit� des handicap�s : Foix et Albi bons �l�ves, Cahors peut mieux faire
Publi� le 11/02/2014 � 09:51
,
Mis � jour le 11/02/2014 � 13:31
| 10
Toulouse perd 12 places par rapport � l'an dernier.
L'accessibilit� dans les transports en commun, un enjeu majeur selon l'APF.
L'Association des paralys�s de France (APF) a d�voil� ce mardi son barom�tre de l'accessibilit� pour 2013 . Force est de constater que les villes du Sud-Ouest peuvent mieux faire, m�me si elles sont toutes en progr�s par rapport � l'ann�e pr�c�dente. Foix et Albi sont les bons �l�ves de la classe, Cahors ferme la marche dans la r�gion.
Selon l'APF, l'�tude r�v�le un retard "pr�occupant" dans la mise en accessibilit� des 96 chefs-lieux d�partementaux. A tel point que l'association a d�clench� "un avis de temp�te". Bien que la moyenne nationale ait augment� continuellement de 2009, ann�e de la premi�re �tude, � 2013, passant de 10,6 � 14,14/20, APF demeure "tr�s pr�occup�e par rapport au respect concret de mise en accessibilit� de la France au 1er janvier 2015", souligne-t-elle dans un communiqu�. Selon elle, la note moyenne de 2013 est "un grave �chec de la politique de mise en accessibilit� en France". L'�tude montre notamment qu'un tiers des chefs-lieux de d�partement n'ont pas la moyenne pour l'accessibilit� de leurs �quipements municipaux. Parmi eux, figurent Tarbes (5/20), Cahors (8/20), Auch (8/20) et Carcassonne (9/20). Dans ce secteur, Rodez obtient 13,6/20 ; Agen 13,25 ; Montauban 11,3 ; Toulouse 17,2 ; Albi 14 et Foix 15.
Foix et Albi, bons �l�ves
Avec une note moyenne de 16,8/20, Foix se classe 8e de ce classement, ex aequo avec Epinal, assez loin n�anmoins du trio de t�te form� par Grenoble (18,7/20), Nantes (18/20) et Caen (17,6/20). L'APF salue particuli�rement la politique locale volontariste en lui attribuant la note de 21/21. Il faut dire que la pr�fecture de l'Ari�ge a augment� sa moyenne de 3,3 points par rapport � 2012.
Deuxi�me dans la r�gion, Albi obtient la note moyenne de 16,1/20 (+1,6 par rapport � 2012) et se classe 18e en compagnie de Ch�teauroux.
Toulouse d�gringole au classement, perdant 12 places par rapport � l'ann�e pr�c�dente, malgr� une moyenne en hausse de 0,5 point (15,5/20 contre 15 en 2012). Malgr� une politique locale jug�e volontariste (21/21) et des �quipements municipaux adapt�s (17,2/20), la Ville rose paie son cadre de vie peu adapt� aux personnes handicap�es (10/21).
Montauban est 34e au classement, avec une note moyenne de 14,9/20, en progression de 0,4 point par rapport � 2012. Selon l'APF, la pr�fecture de Tarn-et-Garonne p�che par le manque d'accessibilit� de ses �quipements municipaux (11,3/20). Agen est 43e avec une moyenne de 14,6/20 (+0,4). A la 52e position, on retrouve Auch (13,9/20, +0,4). Rodez se classe 56e avec une moyenne de 13,6/20 contre 12,9 en 2012. Suivent Carcassonne et Tarbes au 64e rang ex aequo. La seconde r�alisant une meilleure progression (+,1,3 point), Carcassonne n'ayant pris que 0,3 point. Enfin Cahors pointe � la 88e place sur 96 avec une moyenne de 11,6/20, en progression toutefois de 0,6 point.
A noter cette ann�e, qu'aucune ville n'est sous la moyenne.
LaD�p�che.fr
