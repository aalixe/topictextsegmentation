TITRE: OM, et sans Gignac? - Football - Sports.fr
DATE: 2014-02-11
URL: http://www.sports.fr/football/ligue-1/articles/om-gignac-forfait-contre-saint-etienne-1008439/
PRINCIPAL: 164375
TEXT:
APG, le maillon fort marseillais. (Reuters)
Par A.S.
11 février 2014 � 14h54
Mis à jour le
11 février 2014 � 16h05
Homme fort de l’Olympique de Marseille depuis plusieurs semaines, André-Pierre Gignac, touché à une cuisse, ne reprendra l’entraînement que jeudi. S’il devait déclarer forfait face aux Verts dimanche soir, quelles solutions José Anigo et son staff pourraient-ils trouver ?
L’OM ne rigole pas avec André-Pierre Gignac . Quand son meilleur attaquant grimace, tout est mis en œuvre naturellement pour que la douleur passe le plus vite possible. Touché aux ischio-jambiers samedi dernier contre Bastia (3-0, 24e journée), son absence dimanche prochain face aux Verts serait préjudiciable pour José Anigo et son staff. On ne remplace pas comme ça celui qui vient d’inscrire 11 buts lors de ses 11 dernières sorties, toutes compétitions confondues.
Jordan Ayew parti à Sochaux , Saber Khalifa est le seul joueur disposant d’un profil proche de celui d’un attaquant axial. Et on ne peut pas dire que l’ex-pensionnaire de l’ETG ait donné entière satisfaction. Il reste donc à Dimitri Payet de confirmer sa sortie de tunnel symbolisée par un doublé contre Bastia, et à Mathieu Valbuena et Florian Thauvin d’assurer l’intérim de « Dédé » pour marquer des buts. Le staff marseillais ne prendra aucun risque de toute façon avec Gignac avant une période charnière pour le club qui va affronter l’ASSE, Lorient , le PSG et Nice .
Placé en soins, l’ex-Merlu et Toulousain ne reprendra pas l’entraînement collectif avant jeudi, précise le club provençal. Par ailleurs, Jérémy Morel revient. Éloigné des terrains depuis plusieurs semaines en raison de problèmes dorsaux, le latéral gauche a repris mardi matin.
