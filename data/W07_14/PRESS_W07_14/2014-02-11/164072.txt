TITRE: L'auteur du livre "Tous � poil" r�pond � la pol�mique lanc�e par Cop� - Soci�t� - MYTF1News
DATE: 2014-02-11
URL: http://lci.tf1.fr/france/societe/l-auteur-du-livre-tous-a-poil-repond-a-la-polemique-lancee-par-cope-8363854.html
PRINCIPAL: 0
TEXT:
L'auteur du livre "Tous � poil" r�pond � la pol�mique lanc�e par Cop�
le
11 f�vrier 2014 � 16h43  , mis � jour le
11 f�vrier 2014 � 16h53.
Temps de lecture
jean-fran�ois cop� , �ducation
Notre soci�t�Dans une tribune publi�e sur le Plus de l'Observateur, Marc Daniau, l'illustrateur du livre "Tous � poil", a jug� "ahurissante" la pol�mique lanc�e dimanche par Jean-Fran�ois Cop� � propos de son ouvrage.
A sa sortie en avril 2011, le livre "Tous � poil" n'a pas fait l'ombre d'une vague ni de pol�mique. Quelle ne f�t donc pas la surprise de ses deux auteurs de voir Jean-Fran�ois Cop� se saisir de l'ouvrage pr�s de 3 ans apr�s pour le critiquer.
"C'est s�rement parce que le titre du livre est efficace que Jean-Fran�ois Cop� ou son �quipe de communication ont choisi de le pr�senter en exemple. Pourtant, qu'y trouve-t-on de choquant�? Ce sont des images avec tr�s peu de texte, une liste de personnages que l'on voit se d�shabiller (...)� ��voir ce qu'il en dit et comment il l'utilise dans son argumentaire, la grille lecture de Jean-Fran�ois Cop� semble donc superficielle. Surtout, il se trompe d'ennemi", a d�clar� Marc Daniau dans une tribune sur le Plus de l'Obs mardi.
Dans ce livre, les deux auteurs abordent sans complexe le th�me de la nudit�. Ils avaient eu l'id�e de cet ouvrage pour rassurer leurs quatre enfants, qui avaient � l'�poque entre 10 et 16 ans et dont le corps changeait. Ils voulaient proposer un autre regard�sur la nudit� dans ce livre vendu au rayon litt�rature jeunesse des librairies.�"Nous avons voulu montrer que nous sommes tous diff�rents, qu'il y a des gros, des petits, des maigres, des grands, des noirs, des blancs. Il n'y a aucun gros plan sur les corps, explique Marc Daniau dans cette tribune. Il�juge "ahurissante" la pol�mique�lanc�e par Jean-Fran�ois Cop�.
En attendant, le livre qui n'avait �t� vendu qu'� 350 exemplaires depuis sa sortie, cartonne depuis la fin du week-end. Il est num�ro 4 des ventes sur Amazon, toute cat�gorie confondue. Un succ�s tardif qui pourrait bien ne pas s'arr�ter l�.���
�
