TITRE: Les �oliennes modifient-elles le climat europ�en ?
DATE: 2014-02-11
URL: http://www.lemonde.fr/planete/article/2014/02/11/les-eoliennes-modifient-elles-le-climat-europeen_4364513_3244.html
PRINCIPAL: 0
TEXT:
Les �oliennes modifient-elles le climat europ�en ?
Le Monde |
| Par Audrey Garric
Les �oliennes, cens�es produire une �nergie propre, ont-elles un impact sur leur environnement et sur le climat �? Depuis quelque temps, la question�exacerbe le conflit entre pro et anti-turbines, de la m�me fa�on que le d�bat sur les effets des�gigantesques pales et m�ts sur les oiseaux .
Mardi 11 f�vrier, une �tude men�e par des chercheurs fran�ais et publi�e dans la revue Nature Communications a tranch�: le d�veloppement des fermes �oliennes en Europe modifie le climat de fa�on extr�mement faible � l'�chelle du continent, et cela restera le cas au moins jusqu'en 2020.
�TUDE � L'�CHELLE D'UN CONTINENT
En avril 2012, une �tude men�e au Texas (Etats-Unis) et publi�e dans Nature Climate Change estimait que les grandes fermes �oliennes pourraient avoir un effet de r�chauffement sur le climat local�: elle chiffrait l'augmentation des temp�ratures jusqu'� 0,72��C par d�cennie, particuli�rement la nuit, dans les zones situ�es au-dessus des champs de turbines par rapport aux zones sans.�
� De pr�c�dentes recherches montraient un effet local important, mais aucune n'avait �tudi� l'effet � l'�chelle d'un continent, avec des sc�narios r�alistes de�d�veloppement de la production �olienne��, explique Robert Vautard, sp�cialiste des simulations climatiques au Laboratoire des sciences du climat et de l'environnement (LSCE), qui a conduit l'�tude publi�e mardi.
200 GIGAWATTS INSTALL�S EN 2020
Les scientifiques fran�ais ont alors pris pour postulat de d�part l'objectif de 200�gigawatts (GW) de puissance �olienne install�e en 2020 dans l' Union europ�enne , soit un doublement par rapport aux 110�GW actuels, conform�ment aux engagements du paquet �nergie-climat adopt� en 2009 � qui pr�voit une part de 20�% d'�nergies renouvelables dans le mix �nerg�tique. Un sc�nario qui verrait un d�ploiement des turbines sur l'ensemble du continent, ainsi qu'un important d�veloppement des fermes offshore notamment en mer du Nord et dans la Baltique.
� Nous avons introduit dans un mod�le climatique l'effet de la turbulence g�n�r�e par les pales des �oliennes�: en tournant, elles dissipent de l'�nergie, poursuit Robert Vautard. Puis, nous avons compar� les simulations climatiques sans et avec l'effet des �oliennes.��
�CARTS DE TEMP�RATURES DE 0,3 �C AU MAXIMUM
R�sultat : des �carts de temp�rature atteignant au maximum 0,3��C sont possibles dans certaines r�gions, principalement en hiver. Les scientifiques ont ainsi observ� un tr�s l�ger r�chauffement dans le nord de l'Europe (sur la mer Baltique notamment) et refroidissement dans le Sud-Est, qu'ils expliquent par une l�g�re rotation des vents d'ouest vers le nord, sur l'Europe de l'Ouest. Ils notent �galement�une l�g�re baisse des cumuls de pr�cipitations saisonni�res au centre de l'Europe, de 5�% au maximum (soit 0,15�mm par jour). Mais ces diff�rences restent ��nettement plus faibles�� que les diff�rences de temp�ratures ou de pr�cipitations d'un hiver sur l'autre.
� Ces effets sont insignifiants par rapport � la variabilit� naturelle du climat � l'�chelle du continent. Finalement, les effets des �oliennes sont bien moindres que ceux du changement climatique d� � l'augmentation des gaz � effet de serre, assure Robert Vautard. Il faudrait toutefois produire de nouvelles �tudes en cas de d�ploiement plus massif de parcs.��
