TITRE: JO : pas de Fran�ais en demies - BFMTV.com
DATE: 2014-02-11
URL: http://www.bfmtv.com/sport/jo-pas-francais-demies-707934.html
PRINCIPAL: 163609
TEXT:
r�agir
Comme Renaud Jay, Cyril Miranda a pris la 3e place de sa session des quarts de finale du sprint libre des Jeux Olympiques de Sotchi. Les deux Fran�ais ne peuvent malheureusement plus esp�rer �tre rep�ch�s au temps puisque le Su�dois Marcus Hellner et le Norv�gien Petter Northug poss�dent d�j� un meilleur chrono. A noter que Cyril Gaillard a termin� dernier de son quart et n�acc�de donc pas aux demies.
Toute l'actu Sport
