TITRE: Taxis: les syndicats vont se "concerter sur les actions � venir"  - BFMTV.com
DATE: 2014-02-11
URL: http://www.bfmtv.com/societe/greve-taxis-blocages-portes-chapelle-clignancourt-707492.html
PRINCIPAL: 163159
TEXT:
r�agir
"Il n'est pas question que nous bougions", tel etait le mot d'ordre des taxis qui bloquaient encore les portes de la Chapelle et de Clignancourt sur le p�riph�rique parisien, t�t mardi matin. Le but etait alors de prolonger la contestation qui a mobilis� lundi plus d'un millier de taxis lors d'une gr�ve . A la mi-journ�e, le mot d'ordre �tait quelque peu diff�rent, puisque les syndicats des taxis parisiens ont indiqu� qu'ils allaient d�cider mardi apr�s-midi de la suite � donner � leur mouvement.
"Nous avons une intersyndicale � 15 heures pour se concerter sur ces mouvements spontan�s et les actions � venir", Abdel Ghalfi en �voquant de probables actions "dans la semaine et la semaine prochaine". Cette intersyndicale "pour d�cider des suites" a �t� confirm�e par Nordine Dahmane (FO).
Seule certitude, ils appellent � un nouvelle journ�e d'action pour le 13 mars prochain. Car � l'�vidence, le fait qu'une d�l�gation de chauffeurs de taxis ait �t� re�ue dans la soir�e � Matignon n'aura pas suffi. "Rien de concret", avait d�plor� la sortie le "conciliateur", syndicaliste de Force ouvri�re Nordine Dahmane, dans la nuit de lundi � mardi.
Retour sur les �v�nements d'un matin�e mouvement�e.
Des ralentissements sur le p�riph�rique et autour des a�roports
Cap sur Roissy. Vers 8h avait d�but� une op�ration escargot des taxis pour paralyser l'acc�s aux terminaux de l'a�roport de Roissy-Charles de Gaulle, comme le montre cette image capt�e en direct. Une centaine de taxis avaient �t� mobilis�s par cette op�ration.
L'acc�s aux terminaux 2E et 2F, avait pr�sent� des difficult�s.
Autoroutes A1 et A86. Des manifestations ont �t� signal�es sur ces axes � partir de 10 heures. Deux bouchons mobiles s'�taient form�s sur l'A86 ext�rieur � hauteur de Thiais et sur l'A1 au niveau de Garonor.
Porte de Saint-Ouen � porte de la Villette. Le p�riph�rique a �t� ferm� � la circulation dans les deux sens, de la porte de Saint-Ouen � la porte de La Villette. Selon le centre r�gional d'information routi�re d'Ile-de-France, que la circulation a �t� rouverte pour le p�riph�rique int�rieur.
Porte de Clichy. De gros ralentissements ont �t� observ�s � cause des barrage mis en place sur le p�riph�rique ext�rieur. Les trois voies de gauche sont tr�s ralenties. Il faut �viter absolument la bretelle de sortie du p�riph�rique au niveau de l�autoroute A1.
Porte de Clignancourt. A 6h, certains taxis bloquaient encore la  chauss�e ext�rieure du p�riph�rique, selon notre journaliste sur place,  avant d'�tre d�log�s par les forces de l'ordre.
���
Ailleurs dans Paris. Apr�s avoir �t� d�log�s porte de Clignancourt et porte de Saint-Ouen, les taxis envisageraient d'autres actions. Un blocage a ainsi �t� d�plac� au niveau des portes d'Asni�res et de Clichy, au nord-ouest de la capitale, selon l'AFP.
Pr�s de 200 km de bouchons en IDF, un peu plus que d�hab
�
Gros Ralentissements dus � des barrages au niveau de la porte de Clichy (periph ext�rieur). 3 voies de gauches tr�s ralenties
�
Eviter absolument la bretelle de sortie du p�riph au niveau de l�Autoroute A1
D�bat au sein de la profession et chez les automobilistes
Faut-il continuer ou pas le mouvement et faire route vers le p�riph�rique parisien avec pour id�e de le bloquer? Le d�bat avait fait rage au sein des taxis qui m�nent ce matin une op�ration escargot � Roissy o� selon un automobiliste sur place, il fallait "45 minutes pour parcourir 1,5 kilom�tre".
Certains gr�vistes font valoir que l'image de la profession pourrait �tre mise � mal.
Du c�t� des automobilistes, la compr�hension envers les taxis n'�tait pas toujours au rendez-vous et les avis restent partag�s.
Les VTC dans le viseur
Longtemps prot�g�e, la profession est de plus en plus concurrenc�e par les motos-taxis et les soci�t�s de VTC, qui ne peuvent travailler que sur r�servation mais que les taxis accusent de prendre des clients � la vol�e. "Ils racolent aux a�roports", accusent les taxis.
Autre point d'achoppement, l'enregistrement d'un v�hicule de VTC ne co�te que 100 euros alors que les licences des taxis (� 80% artisans et propri�taires de leurs licences) se n�gocient autour de 230.000 euros � Paris. L'assouplissement de la l�gislation en 2009 a favoris� la multiplication des VTC, qui ont gagn� mercredi dernier une nouvelle partie en obtenant la suspension du d�cret leur imposant un d�lai de 15 minutes entre la r�servation et la prise en charge du client. Cette d�cision provisoire du Conseil d'Etat a attis� la col�re des syndicats de taxis qui entendent bien relancer le mouvement de gr�ve et les blocages d'ici, � la fin de cette semaine.
A lire aussi
