TITRE: Sant� des ados : les in�galit�s persistent
DATE: 2014-02-11
URL: http://www.reponseatout.com/pratique/sante-bien-etre/sante-ados-inegalites-persistent-a1012160
PRINCIPAL: 0
TEXT:
Sant� des ados : les in�galit�s persistent
Le 11/02/2014 � 12:15:33
Vues : 6184 fois JE REAGIS
Les enfants de cadres seraient plus nombreux � pratiquer un sport et � prendre un petit d�jeuner �quilibr�. - cr�dit photo : Cameron Spencer - Getty Images | � ThinkStock
D�apr�s diverses enqu�tes r�alis�es entre 2004 et 2009, la sant� des coll�giens scolaris�s en classe de troisi�me tend � s�am�liorer. N�anmoins, les auteurs de l��tude pointent du doigt certaines in�galit�s sociales.
La Direction de la recherche, des �tudes, de l'�valuation et des statistiques (Drees) vient de publier une �tude sur l��tat de sant� des adolescents scolaris�s en classe de troisi�me. Ce rapport pr�sente les r�sultats des enqu�tes r�alis�es en milieu scolaire entre 2004 et 2009. Le bilan est globalement positif mais les in�galit�s sociales persistent.
Les enfants des milieux favoris�s font plus de sport et se nourrissent mieux
En 2009, 18 % des �l�ves observ�s �taient en surcharge pond�rale, dont 4 % en situation d�ob�sit�. Des chiffres encourageants puisqu�ils n�ont pas augment� depuis 2004. La Drees souligne une am�lioration g�n�rale au niveau des soins dentaires (recul du nombre de dents cari�es) et des taux de couverture vaccinale (en progression). � Ce constat favorable doit cependant �tre nuanc�, souligne l�organisme. Car ces am�liorations sont plus ou moins importantes selon le milieu social dans lequel �volue l�adolescent. �
> Lire aussi : Un Fran�ais sur cinq ne se soigne pas pour raisons financi�res
Ainsi, 22 % des �l�ves de troisi�me dont l�un des parents est ouvrier pr�sentent une surcharge pond�rale, contre 12 % des enfants de cadres (ann�e 2009). Les auteurs de l��tude constatent que les jeunes des milieux favoris�s sont plus nombreux � pratiquer un sport et � prendre un petit d�jeuner �quilibr�.
Sant� : les enfants de cadres, mieux suivis que les enfants d�ouvriers
On retrouve les m�mes nuances au niveau des probl�mes dentaires : 58 % des adolescents dont l�un des parents est ouvrier ont au moins une dent cari�e, contre 34 % des enfants de cadres. Et lorsque carie il y a, celle-ci est soign�e dans la majeure partie des cas dans les familles de cadres (88 %) et dans seulement 66 % des cas chez les ouvriers. M�me constat du c�t� de l�ophtalmologie. Les enfants d�ouvriers sont moins nombreux � porter des lunettes ou des lentilles (22 %) que les enfants de cadres (33 %) parce que leurs troubles de la vision, comme la myopie, sont souvent diagnostiqu�s plus tard.
Afin de gommer ces in�galit�s, la Drees recommande au gouvernement la mise en place d�actions de pr�vention cibl�es sur les adolescents issus des milieux sociaux les plus d�favoris�s.
