TITRE: Roaming chez Free Mobile : au tour des Pays-Bas
DATE: 2014-02-11
URL: http://www.mac4ever.com/actu/87299_roaming-chez-free-mobile-au-tour-des-pays-bas
PRINCIPAL: 162752
TEXT:
Roaming chez Free Mobile : au tour des Pays-Bas
Le 11/02/2014 � 09h20
Par arnaud
C'est au compte goutte mais r�guli�rement que Free Mobile ajoute de nouveaux pays � son offre de roaming : apr�s l'Italie, l'Allemagne, les DOM et le Portugal, c'est au tour des Pays-bas d'�tre concern�s. L'offre conserve les m�mes caract�ristiques : voix et SMS illimit�s, 3Go de data, le tout sur 35 jours par an maximum.
��Les abonn�s au Forfait mobile Free (19,99�/mois ou 15,99�/mois pour les abonn�s Freebox) ayant au moins 60 jours d�anciennet� peuvent donc d�sormais utiliser leur Forfait depuis les Pays-Bas, 35 jours par ann�e civile, sans surco�t : appels, SMS & MMS illimit�s depuis les Pays-Bas vers les Pays-Bas et vers la France m�tropolitaine, et l�Internet mobile en 3G jusqu�� 3Go (usage d�compt� de l�enveloppe Internet du Forfait Free). Il en va de m�me pour les communications re�ues (appels, SMS & MMS) qui sont incluses dans le Forfait Free��, pr�cise l'op�rateur.
