TITRE: Lancement d'un mode de paiement low-cost
DATE: 2014-02-11
URL: http://www.lefigaro.fr/flash-eco/2014/02/11/97002-20140211FILWWW00209-lancement-d-un-mode-de-paiement-low-cost.php
PRINCIPAL: 163941
TEXT:
le 11/02/2014 à 15:08
Publicité
La Financière des paiements électroniques (FPE), associée à la Confédération des buralistes , a lancé officiellement mardi auprès du grand public le "Compte nickel", son compte de paiement low-cost simplifié, disponible dans les bureaux de tabac, sans condition de dépôts ou de revenus.
Ce compte n'autorisant aucun découvert mais acceptant les achats sur internet permet une "maîtrise absolue du client sur l'ensemble de ses frais", a expliqué lors d'un point-presse le président du comité de surveillance de la FPE, Hugues Le Bret.
"Avec un compte sans découvert, les gens vont réapprendre comment dépenser leur argent", a assuré M. Boulanouar, président de la start-up.
Le "Compte nickel" concerne évidemment au premier chef les 2,5 millions d'individus interdits bancaires en France. Mais il cible aussi les étudiants, les saisonniers, les intérimaires, les colocataires, mais aussi des pratiques comme la gestion d'achats entre particuliers et les comptes éphémères liés à un divorce ou une succession.
