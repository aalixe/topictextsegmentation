TITRE: Sotchi 2014 : Coline Mattel d�croche la m�daille de bronze !
DATE: 2014-02-11
URL: http://www.leparisien.fr/JO-hiver/sotchi-2014-en-direct/en-direct-sotchi-2014-les-bleus-veulent-continuer-leur-moisson-de-medailles-11-02-2014-3580349.php
PRINCIPAL: 0
TEXT:
Sotchi 2014 : Coline Mattel d�croche la m�daille de bronze !
�
Coline Mattel vise un seul titre : celui de championne olympique du saut � ski f�minin, pr�sent aux JO pour la premi�re fois.
| AFP/Peter Parks
R�agir
Coline Mattel avait annonc� la couleur : elle venait � Sotchi pour d�crocher une m�daille. A d�faut de l'or olympique, la Fran�aise a r�ussi � se parer du bronze. Un magnifique exploit pour la jeune fille de tout juste 18 ans. Et une troisi�me m�daille pour la France apr�s celles obtenues lundi par Martin Fourcade et Jean-Guillaume Beatrix, qui avaient d�croch� l'or et le bronze dans l'�preuve de poursuite .
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
Sotchi 2014. Le �calvaire� de Jason Lamy-Chappuis en combin� nordique
Cette m�daille efface les d�ception survenues plus t�t dans la journ�e avec la 12e place d'Ana�s Bescond en biathlon et les �liminations pr�matur�es d'Arthur Longo et Johann Baisamy en snowboard, dans l'�preuve du half-pipe. Cette m�me �preuve qui a vu le double tenant du titre am�ricain Shaun White �chouer au pied du podium. Comme pour nous rappeler qu'en mati�re de sport, rien n'est jamais acquis d'avance...
Retrouvez le portrait de Coline Mattel, prodige fran�aise du saut � ski, �g�e de 18 ans.
Revivez les �v�nements de la journ�e de mardi :
20h27. �Je suis contente que ce soit fini !�, confie Coline Mattel au micro de France 3. �J'avais de la marge, mais par rapport � mon d�but de saison, c'est formidable�, ajoute-t-elle.�Avant de fondre en larmes en discutant au t�l�phone avec son p�re.
20h20. Coline Mattel est aux anges.�Elle saute de joie sur le podium de remise des fleurs. Elle a de quoi �tre fi�re. Elle est en effet la premi�re m�daill�e fran�aise de l'histoire en saut � ski !
France 3
20 heures. M�daille de bronze pour Coline Mattel ! A 2,2 points de l'or... L'Allemande Carina Vogt d�croche l'or et l'Autrichienne Iraschko-Stolz l'argent. L'ultra-favorite japonaise Sara Takanashi �choue au pied du podium.
AFP
19h57. Coline Mattel s'�lance. Elle est 2e. Elle est assur�e d'avoir une m�daille ! D'argent ou de bronze, tout d�pendra du saut de l'Allemande Carina Vogt.
19h56. La Japonaise Sara Takanashi fait un saut moyen. Elle n'est que 2e.
19h54. Enorme saut de l'Autrichienne Iraschko-Stolz. Avec 246,2 points, elle prend la t�te du concours. Il faudra aller la chercher...
19h45. Le top 10 se tient dans un mouchoir de poche. Environ 5 ou 6 filles peuvent d�crocher le titre olympique. Parmi elles, Coline Mattel.
19h37. Julia Clair passe devant sa compatriote Lea Lemare, mais est d��ue. Elle sait que son score ne lui permettra pas de d�crocher une m�daille. Tous les espoirs sont d�sormais tourn�s vers Coline Mattel.
19h34. Lea Lemare s'�lance. La Fran�aise est provisoirement premi�re, en attendant le saut des favorites.�
19h30. Retour sur le tremplin du saut � ski pour suivre Coline Mattel, plus proche que jamais d'une m�daille olympique.
19h26. D�ception pour Shaun White ! l'Am�ricain s'�lance pour son ultime saut. Il r�alise de superbes figures, mais la r�ception est al�atoire.�Il finit 4e ! Il ne sera pas le premier snowboarder am�ricain � remporter trois m�dailles d'or cons�cutives aux Jeux olympiques ! Le Suisse Youri Podlachikov d�croche l'or. Il devance Ayamu Hirano et Taku Hiraoka, deux Japonais.
19h10. Pendant ce temps l�, Shaun White, roi du snowboard est en danger en half pipe apr�s une chute sur son premier run de la finale.
AFP
19h03. Coline Mattel est 2e au classement provisoire. Le deuxi�me saut aura lieu � partir de 19h22.
� Antoine D�n�riaz (@deneriazantoine) 11 F�vrier 2014
19 heures. L'Allemande Carina Vogt prend les rennes avec un tr�s bon saut. Dans la foul�e, la Japonaise favorite est troisi�me !
18h54. Coline Mattel prend les commandes du concours. Malgr� une r�ception moyenne, elle va loin. Elle est provisoirement en t�te, mais les autres favorites ne se sont pas encore �lanc�es.
Retrouvez le portrait de Coline Mattel, prodige fran�aise du saut � ski, �g�e de 18 ans .
18h41. Julia Clair s'�lance. La Fran�aise r�alise un saut correct. Elle est provisoirement 5e, tandis que Lea Lemare a r�trograd� � la 7e place.�Coline Mattel s'�lancera parmi les derni�res, avec les favorites.
18h32. Lea Lemare est la premi�re Fran�aise � s'�lancer. Elle est 2e au classement provisoire (sur trois concurrentes).
18h30. Les sauteuses � ski sont sur le tremplin. La principale concurrente de Coline Mattel est la Japonaise Sara Takanashi, 17 ans, qui a remport� 10 des 13 �preuves d�j� disput�es en coupe du monde de saut � ski.
18h10. Dans 20 minutes, Coline Mattel sera en lice sur le saut � ski. Une �preuve que le jeune fille de 18 ans veut remporter. Elle pourrait ainsi entrer dans l'histoire puisque c'est la premi�re fois que l'�preuve est disput�e aux Jeux olympiques chez les femmes.
VIDEO. Coline Mattel peut entrer dans l'histoire
18h01. Le couple r�alise son meilleur score, avec une note de 65,36. �On est tr�s contents. On avait � coeur de se rattraper apr�s notre prestation par �quipe�, ont confi� Vanessa James et Morgan Cipr�s au micro de France 2 apr�s leur programme.
17h55. Superbe prestation de Vanessa James et Morgan Cipr�s en patinage artistique. Il s'agissait de leur programme court. Le m�me que celui pr�sent� dans l'�preuve par �quipe, mais cette fois, bien mieux ex�cut�. Le couple se classe provisoirement troisi�me derri�re les Chinois et Am�ricains. Six couples ont d�j� pr�sent� leur programme.
France 2
17h17. Martin Fourcade et Jean-Guillaume Beatrix p�n�trent dans le stade des JO pour recevoir leur m�daille.�Jean-Guillaume Beatrix semble au bord des larmes au moment de recevoir sa m�daille, gagn� par l'�motion. La Marseillaise retentit et le drapeau fran�ais s'�l�ve dans le ciel de Sotchi.
France 2
AFP
16h54. Arthur Longo chute � son tour ! Quel dommage. Le Fran�ais, comme son co�quipier Johann Baisamy, a rat� ses deux manches. Il n'ira pas en finale.
16h44. Johann Baisamy n'ira pas en finale du half-pipe. Apr�s un premier run moyen, le Fran�ais a chut� lors de sa deuxi�me tentative, hypoth�quant ses chances d'atteindre la finale.
16h31. Ana�s Bescond termine finalement 12e. Marie Dorin finit quant � elle � la 14e place, sans avoir couru une seule fois cette saison. Superbe !
16h30. Daria Domracheva s'empare d'un drapeau bi�lorusse. Elle remporte la course. Tora Berger d�croche l'argent. La Slovaque Gregorin se pare du bronze.
16h26. La Bi�lorusse Domracheva file vers la victoire.�Pour l'argent, Tora Berger, la Norv�gienne semble en bonne place. Elles sont encore trois ou quatre � se disputer le bronze.
16h25. Bescond commet une erreur sur son dernier tir. Quel dommage ! Le podium s'�loigne. Si Ana�s Bescond n'avait pas fait ce dernier tour de p�nalit�, le podium semblait assur� ! Que de regrets. Dorin et Bescond sont d�sormais au coude � coude pour finir dans le top 10.
16h17. Ana�s Bescond commet une faute ! Elle a tir� tr�s rapidement pour rattraper son erreur. Mais derri�re, plusieurs biathl�tes on fait 5 sur 5 au tir. Les affaires se compliquent pour la Fran�aise, d�sormais 7e... pour un seul tir manqu� !
France 2
16h12. Bescond fait un sans faute sur le 2e tir couch� ! Kuzmina � l'inverse commet une erreur. La Fran�aise est d�sormais deuxi�me de la course, derri�re Domracheva, apr�s les deux tirs. Il faudra faire aussi bien sur le tir debout pour d�crocher une m�daille.
16h10. Bescond n'a plus qu'une dizaine de secondes de retard sur la t�te de la course. Les tirs des concurrentes vont �tre plus que jamais d�terminants pour d�partager les biathl�tes.
16h06. Les cinq premi�res font un sans faute sur le premier tir couch�. Kuzmina reste donc en t�te. Et Ana�s Bescond reste en course pour un podium. Un peu plus loin, Marie Dorin fait aussi un sans faute sur le pas de tir.�
16 heures. C'est parti pour le biathlon. Ana�s Bescond a d�marr� une trentaine de secondes apr�s la Slovaque Kuzmina, qui a remport� le sprint il y a quelques jours.�
14h55. Chez les hommes, c'est aussi un Norv�gien qui l'emporte ! Hattestad� apporte un 4e titre � son pays gr�ce � sa large victoire sur le sprint du ski de fond. Il devance les Su�dois T. Peterson et E. Joensson.
14h40. Chez les femmes, c'est la Norv�gienne Maiken Caspersen Falla qui remporte le titre de championne olympique en sprint sur l'�preuve de ski de fond. Elle devance sa co�quipi�re Oestberg et la Slov�ne Fabjan
14h20. Aucun Fran�ais en demi-finales du ski de fond. Chez les filles, comme chez les gar�ons, il n'y a donc plus de concurrents fran�ais dans l'�preuve du sprint.
14h15. C'est fini pour Aurore Jean. La fondeuse de 28 ans termine 6e de sa demi-finale.
13h40. Renaud Jay termine 3e de sa s�rie. Il devra attendre les temps des quatre autres quarts de finale pour savoir s'il est qualifi� pour les demies.
13h27. Aurore Jean est rep�ch�e au temps. Elle disputera les demi-finales de l'�preuve de sprint du ski de fond.
13h20. Shaun White s'envole extr�mement haut. Son run est magnifique. Il prend logiquement la premi�re place provisoire de cette deuxi�me session de qualifications. Shaun White sera � coup s�r, directement qualifi� pour la finale.
13h15. Shaun White, star mondiale du snowboard s'appr�te � s'�lancer.�L'Am�ricain est ultra favori en half-pipe.�Il a m�me d�clar� forfait pour le slopestyle afin de se concentrer sur cette �preuve.
13h12. Ski de fond. La Fran�aise Aurore Jean troisi�me de sa s�rie. Elle n'a donc pas pu prendre l'une des deux premi�res places qualificatives directes pour les demies, elle devra attendre la fin des cinq s�ries pour savoir si elle fait partie des deux qualifi�es au temps.
12h38. Arthur Longo et Johann Baisamy sont qualifi�s pour les demi-finales du half-pipe. Elles se d�roulereront � 16 heures. La grande finale est programm�e pour 18h30.
12h10. Half-pipe. Les deux Fran�ais n'am�liorent pas leur premier run lors de leur deuxi�me tentative. Mais ils sont toujours 3e et 7e au classement provisoire. Arthur Longo, 3e provisoire, est n�anmoins d��u. �Les conditions ne sont pas faciles. Je ne suis pas satisfait de mes deux manches mais j'esp�re accrocher la finale�, confie-t-il au micro de France 2.
12h08. Le deuxi�me tricolore lance son run en half-pipe. Johann Baisamy avec 71.25 pts est pour le moment 5e de la manche. Le Japonais Ayumu Hirano avec 92.25 pts est toujours en t�te du classement privisoire.
11h55. Trois Fran�ais qualifi�s pour les quarts en ski de fond.�Il fallait finir dans les 30 premiers pour atteindre les quarts. Jay est 14e, Miranda 22e et Gaillard (29e). Seul Gros (40e) �choue. Quarts de finale � partir de 13h25.
Ski de fond #sprint masculin, 3 Fran�ais qualifi�s pour les 1/4 : @renaudjay 14e, Cyril Miranda 22e, Cyril Gaillard 29e #espritbleu
� France Olympique (@FranceOlympique) 11 F�vrier 2014
11h40.  Arthur Longo ouvre la comp�tition en half-pipe. Et le Fran�ais s'en sort plut�t bien.�Avec un total de 79.25 pts, il obtient la 2e place provisoire. Difficile n�anmoins de menacer Shaun White, grandissime favori de l'�preuve.
11h35. Le Canada conforte sa position de leader au classement des m�dailles. Dara Howell monte sur la plus haute marche du podium en ski slopestyle. Elle devance l'Am�ricaine Devin Logan et sa compatriote Kim Lamarre.
AFP
11h30. Aurore Jean qualifi�e pour les quarts de finale en ski de fond. La Fran�aise a r�alis� le 21e chrono des qualifications et va disputer les quarts � partir de 13h. Les deux autres Fran�aises engag�es, Marion Buillet et  C�lia Aymonier 40e sont en revanche �limin�es.
M.-C. Falla (NOR), K. Visnar (SLO) et M. Bjoergen (NOR) ont r�alis� les trois meilleurs temps
10h30. Bode Miller, roi des entra�nements.�Comme lors des entra�nements pour la descente, l'Am�ricain a  r�ussi le meilleur temps lors de l'entra�nement du Super-combin�. Le plus important est �videmment d'�tre en forme le jour J, mais la forme de l'Am�ricain est d�cid�ment �tincelante.
LeParisien.fr
