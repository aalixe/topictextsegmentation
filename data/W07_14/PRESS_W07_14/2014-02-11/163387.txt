TITRE: Free Mobile : int�gration des Pays-Bas pour l'itin�rance
DATE: 2014-02-11
URL: http://www.generation-nt.com/free-mobile-roaming-pays-bas-itinerance-actualite-1849302.html
PRINCIPAL: 163386
TEXT:
Free Mobile : int�gration des Pays-Bas pour l'itin�rance
Le
par J�r�me G. �|� 4 commentaire(s)
Pour son principal forfait mobile, Free int�gre d�sormais le roaming depuis les Pays-Bas.
La strat�gie de Free est claire, ce sera au compte-gouttes. En d�pit des annonces de ses concurrents, Free continue d'ajouter petit � petit de nouvelles destinations plut�t que de livrer une liste compl�te en une seule fois. L'assurance d�s lors de faire parler de soi quasiment toutes les semaines.
Apr�s le Portugal, la Guyane, les Antilles fran�aises (Guadeloupe, Martinique, Saint-Barth�lemy, Saint-Martin), l'Italie et l'Allemagne, c'est au tour des Pays-Bas. Pour son forfait mobile � 19,99 � par mois, Free int�gre donc les frais de roaming depuis la Hollande.
Comme � l'accoutum�e, cela concerne les abonn�s avec une anciennet� d'au moins 60 jours et pour 35 jours par an. Ils pourront utiliser leur forfait depuis les Pays-Bas vers les Pays-Bas ou vers la France m�tropolitaine pour des appels, SMS, MMS et l'Internet mobile en 3G (jusqu'� 3 Go). Faites vos jeux sur le prochain pays qui sera int�gr�...
Apr�s Orange et Bouygues Telecom (pour le 24 f�vrier) , c'est SFR qui a ripost� en d�voilant hier une offre (limit�e � 10 000 souscriptions) qui int�gre le roaming en Europe et dans les DOM sans contrainte de temps.
Compl�ment d'information
