TITRE: incroyable: La candidate � la mairie est une vraie potiche -   Monde - lematin.ch
DATE: 2014-02-11
URL: http://www.lematin.ch/monde/candidate-mairie-vraie-potiche/story/16343793
PRINCIPAL: 164492
TEXT:
La candidate � la mairie est une vraie potiche
incroyable
�
Pour sauver le poste de son mari frapp� d'in�ligibilit�, Caroline Bartoli se pr�sente aux Municipales en Corse. Elle n'y conna�t rien en politique. Devant les cam�ras, cela se voit.
Mis � jour le 12.02.2014 18 Commentaires
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
E-Mail*
Veuillez SVP entrez une adresse e-mail valide
Le maire sortant de Propriano, une localit� au sud de l'ile de beaut�, Paul-Marie Bartoli, est frapp� in�ligibilit� � la suite de l�invalidation de ses comptes de campagne. Qu'� cela ne tienne. Pour �viter de perdre son poste, il envoie au front son �pouse Caroline.
La pauvre n'y connait rien, mais alors vraiment rien en politique. A-t-elle un programme personnel? Rien. M�me pas une ch�taigne. Ce n'est pas grave, elle a celui de son mari. Qu'elle tente sans grand bonheur de d�fendre face � la cam�ra de France T�l�vision. Des grands moments de solitude.
La malheureuse candidate assume tant bien que mal, mais finalement peu importe puisque, dit-elle: �L'essentiel �tant que mon mari retrouve son poste dans trois mois, apr�s de nouvelles �lections...� L� au moins, elle a un vrai programme et on ne pourra pas dire qu'elle a cherch� � cacher la v�rit� aux �lecteurs de Propriano.
Cr��: 12.02.2014, 08h35
Votre email a �t� envoy�.
Publier un nouveau commentaire
Nous vous invitons ici � donner votre point de vue, vos informations, vos arguments. Nous vous prions d�utiliser votre nom complet, la discussion est plus authentique ainsi. Vous pouvez vous connecter via Facebook ou cr�er un compte utilisateur, selon votre choix. Les fausses identit�s seront bannies. Nous refusons les messages haineux, diffamatoires, racistes ou x�nophobes, les menaces, incitations � la violence ou autres injures. Merci de garder un ton respectueux et de penser que de nombreuses personnes vous lisent.
La r�daction
Merci de votre participation, votre commentaire sera publi� dans les meilleurs d�lais.
Merci pour votre contribution.
J'ai lu et j'accepte la Charte des commentaires.
Caract�res restants:
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
S'il vous pla�t entrer un nom valide.
S'il vous pla�t indiquez le lieu g�ographique.
Ce num�ro de t�l�phone n'est pas valable.
S'il vous pla�t entrer une adresse email valide.
Veuillez saisir deux fois le m�me mot de passe
Mot de passe*
Signup Fermer
Vous devez lire et accepter la Charte de commentaires avant de poursuivre.
Nous sommes heureux que vous voulez nous donner vos commentaires. S'il vous pla�t noter les r�gles suivantes � l'avance: La r�daction se r�serve le droit de ne pas publier des commentaires. Ceci s'applique en g�n�ral, mais surtout pour les propos diffamatoires, racistes, hors de propos, hors-sujet des commentaires, ou ceux en langues �trang�res ou dialecte. Commentaires des noms de fantaisie, ou avec des noms manifestement fausses ne sont pas publi�s non plus. Plus les d�cisions de la r�daction n'est ni responsable d�pos�e, ni en dehors de la correspondance. Renseignements t�l�phoniques ne seront pas fournis. L'�diteur se r�serve le droit �galement � r�duire les commentaires des lecteurs. S'il vous pla�t noter que votre commentaire aussi sur Google et autres moteurs de recherche peuvent �tre trouv�s et que les �diteurs ne peuvent rien et est de supprimer un commentaire une fois �mis dans l'index des moteurs de recherche.
�auf Facebook publizieren�
Veuilliez attendre s'il vous pla�t
Soumettre Commentaire
Soumettre Commentaire
No connection to facebook possible. Please try again. There was a problem while transmitting your comment. Please try again.
