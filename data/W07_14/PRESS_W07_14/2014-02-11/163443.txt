TITRE: Aliments bio: lancement d'une �tude sur les impacts sur la sant� - BFMTV.com
DATE: 2014-02-11
URL: http://www.bfmtv.com/societe/manger-bio-est-il-vraiment-bon-sante-707776.html
PRINCIPAL: 0
TEXT:
r�agir
Il y a les opportunistes, qui consomment bio "au cas o�". Et il y les consciencieux, qui y croient dur comme fer. Mais manger bio est-il vraiment bon pour la sant�? La plus grande �tude jamais conduite sur la consommation des aliments bio , est lanc�e ce mardi en France. Elle portera sur 100.000 personnes au moins et sera pilot�e par le Dr Emmanuelle Kesse, �pid�miologiste.
"BioNutriNet" aura pour objectif de mieux comprendre qui sont les consommateurs de produits bio et les liens entre la consommation de ce type d'aliments et la sant�. L'�tude pr�voit notamment de mesurer sur un sous-groupe de participants les r�sidus de pesticides.
Les pesticides et les vitamines seront mesur�s
Les responsables de l'�tude, qui se poursuivra au moins 5 ans, lancent  un appel, afin que le plus grand nombre de consommateurs (r�guliers ou  occasionnels) ou non consommateurs de produits bio, participent � ce  projet.
�� �
"Il y a d�j� eu des �tudes sur les consommateurs de bio, mais  g�n�ralement sur de petits groupes et durant seulement quelques  semaines", ajoute le Dr Kesse.
�� �
Pour cette longue �tude, il s'agit de mieux  pr�ciser ce qui d�termine la consommation ou non des aliments bio ainsi  que ses effets sur l'�tat nutritionnel (vitamines et min�raux) et  toxicologique (r�sidus de pesticides), mesur�s sur un sous-groupe de 300  personnes, gr�ce � des marqueurs sanguins et urinaires.
A terme,  BioNutriNet permettra  d'�valuer un �ventuel effet protecteur  vis-�-vis  de malades chroniques  (cancers, maladies cardiovasculaires,  ob�sit�,  diab�te, etc.). Le financement sp�cifique de BioNutriNet (700.000 euros) est assur� par l'Agence Nationale pour la Recherche.
A lire aussi
