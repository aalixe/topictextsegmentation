TITRE: Paris : un couple de Roms agress�s, un homme en garde � vue - RTL.fr
DATE: 2014-02-11
URL: http://www.rtl.fr/actualites/info/article/paris-un-couple-de-roms-agresses-un-homme-en-garde-a-vue-7769651657
PRINCIPAL: 164657
TEXT:
Une paire de menottes dans une commissariat de police d'Arras, le 20 novembre 2013
Cr�dit : AFP / PHILIPPE HUGUEN
Un homme de 40 ans a avou� avoir "asperg� le matelas" d'un couple de Roms "avec un m�lange de savon noir et d'eau de Javel".
Un homme de 40 ans �tait en garde � vue mardi 11 f�vrier, soup�onn� d'avoir asperg� mi-janvier � Paris le matelas d'un couple de Roms d'un produit caustique sans les blesser.
Convoqu� lundi matin par la police, l'homme a reconnu avoir "asperg� le matelas de ce couple avec un m�lange de savon noir et d'eau de Javel", justifiant son geste par le "d�sordre engendr� par leur pr�sence", a expliqu� une source polici�re.�Fin janvier, un couple de Roms avait d�pos� plainte au commissariat du IIIe arrondissement de Paris, affirmant avoir �t� agress� le 16 janvier place de la R�publique en d�but de soir�e par un homme.
Les d�clarations de l'homme plac� en garde � vue sont en contradiction avec les premiers t�moignages, dont celui d'un b�n�vole de l'association Autremonde, L�o Larbi, qui effectuait une mission dans le quartier ce soir-l�.�Avec une co�quipi�re, il disait "avoir constat� qu'un liquide noir tr�s corrosif commen�ait � d�truire le matelas". En ramassant un sac, sa co�quipi�re avait touch� le produit. Elle avait "�t� imm�diatement br�l�e, c'�tait extr�mement puissant et corrosif", avait expliqu� Larbi qui, apr�s discussion dans le milieu associatif, avait encourag� le couple � d�poser plainte le 27 janvier.�"Aucun pr�l�vement sur le matelas n'a pu �tre effectu� en raison du d�calage entre les faits all�gu�s et le d�p�t de plainte", a ajout� la source polici�re.�
La r�daction vous recommande
