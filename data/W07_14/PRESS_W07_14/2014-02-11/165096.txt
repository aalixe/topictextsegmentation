TITRE: PocketBook Touch Lux 2 : une liseuse de 6 pouces pratique et pas ch�re
DATE: 2014-02-11
URL: http://www.lavienumerique.com/articles/146477/pocketbook-touch-lux-2-liseuse-6-pouces-pratique-pas-chere.html
PRINCIPAL: 165087
TEXT:
Ce message sera transmis depuis l'adresse IP 193.49.124.107
PocketBook Touch Lux 2 : une liseuse de 6 pouces pratique et pas ch�re
PocketBook commercialise une liseuse de 6 pouces dot�e d'un �cran HD et d'un �clairage LED int�gr� : la PocketBook Touch Lux 2.�
Pr�sent�e par la marque comme un mod�le "haut de gamme", la PocketBook Touch Lux 2 offre a priori un bon confort de lecture gr�ce � son �cran E Ink Pearl de 6 pouces affichant une d�finition de 1024 x 768 pixels et capable d'afficher 16 nuances de gris.
Elle int�gre �galement un �clairage LED frontal permettant � la fois de ne pas �tre �bloui par le soleil ou la lumi�re r�fl�chie, et de pouvoir lire en cas de faible luminosit�.�
Elle abrite par ailleurs 4 Go de m�moire Flash (extensible par carte microSD jusqu'� 32 Go) et prend en charge jusqu'� 16 formats d'ebook sans conversion et 4 formats d'images.�
Elle propose �videmment la connectivit� WiFi et un port micro-USB pour le transfert des fichiers et le chargement de la batterie de 1500 mAh.�
Enfin, elle tourne sous Linux 2.6 et est disponible en blanc ou gris fonc� au prix de 111 euros.�
mardi 11 f�vrier 2014
