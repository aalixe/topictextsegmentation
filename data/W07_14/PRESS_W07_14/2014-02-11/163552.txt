TITRE: Pacte de responsabilit� : clash entre Gattaz et le gouvernement - Economie - MYTF1News
DATE: 2014-02-11
URL: http://lci.tf1.fr/economie/entreprise/pacte-de-responsabilite-clash-entre-gattaz-et-le-gouvernement-8363203.html
PRINCIPAL: 163550
TEXT:
fran�ois hollande , jean-marc ayrault , pierre gattaz , emploi , medef
EntreprisesApr�s que le patron du Medef, qui accompagne Fran�ois Hollande aux Etats-Unis, a tir� � boulets rouges sur le projet de l'ex�cutif, Jean-Marc Ayrault a r�pliqu� ce mardi sans langue de bois : "le d�calage horaire peut parfois poser des probl�mes", a lanc� le Premier ministre.
Le moins que l'on puisse dire, c'est que ni l'un ni l'autre n'ont cette fois fait dans la langue de bois. R�sultat : les relations entre Pierre Gattaz ��et le gouvernement en ont pris un s�rieux coup.
C'est �le pr�sident du Medef qui a ouvert le feu lundi soir. Membre de la d�l�gation �conomique fran�aise pr�sente aux Etats-Unis dans le cadre du voyage de Fran�ois Hollande , il devait essentiellement tenir lundi soir une conf�rence de presse. Celle-ci a �t� annul�e au dernier moment, pour d'obscures "raisons diplomatiques et de pr�s�ance". Sur demande de l'Elys�e, comme le pensent les observateurs ? Myst�re.
"Pas de contreparties"
Quoi qu'il en soit, Pierre Gattaz a n�anmoins rencontr� des journalistes fran�ais, mais de mani�re informelle. Et dans ce cadre moins strict, il en a profit� pour dire tout le mal qu'il pensait r�ellement du "pacte de responsabilit�" pr�sent� par le chef de l'Etat � la mi-janvier, et notamment des contreparties en terme d'embauches demand�es par le gouvernement en �change de la baisse des charges pour les entreprises.
"Il faut surtout arr�ter de g�rer par la contrainte. Je crois que c'est fondamental. Aujourd'hui, quand j'entends parler de contreparties dans ce pacte, j'entends aussi des gens qui me disent : 'On va vous contraindre, on va vous obliger, on va vous mettre des p�nalit�s. Si vous ne le faites pas, vous allez �tre puni'. Il faut arr�ter ce discours qui est insupportable", a lanc� le patron des patrons. Avant d'ajouter : "En France, je ne sais pas pourquoi, nous sommes toujours dans l'incitation n�gative.� Ca suffit ! Maintenant, j'attends du gouvernement qu'il me pr�cise en mars prochain la trajectoire de baisse de la fiscalit� sur les entreprises. Je suis extr�mement libre". Et de finir : "Il n'y a pas de contreparties".
Ayrault veut une rencontre Gattaz-syndicats
Interrog� � la mi-journ�e mardi, Jean-Marc Ayrault n'a lui non plus pas m�ch� ses mots : "Je pense que parfois, le d�calage horaire peut poser des probl�mes", a-t-il ass�n� s�chement, visiblement irrit�, voire exc�d�, par l'attitude de Pierre Gattaz. "Le dialogue social ne peut pas reposer sur des oukases", a-t-il ajout�.
Il a soulign� qu'il attendait que ce dernier, � son retour des Etats-Unis, "rencontre les organisations syndicales, comme c'�tait pr�vu, pour engager le dialogue social sur le pacte. C'est tr�s attendu". Le rendez-vous�s'annonce donc d�j� tr�s tendu...�
Commenter cet article
Ecrire votre commentaire ici ...
Vous devez �crire un avis
shooby02470 : En clair il veut le beurre + l argent du beurre + la cr�mi�re, sans rien en contrepartie. Mais bon, c'est �a aussi le medef. Le premier assist� de France, qui veulent �tre socialistes en cas de dettes (quand ils font des erreurs) tout en critiquant l'ump et lib�raux en cas de b�n�fices (quand il y a des r�ussites) et l� en critiquant le ps.
Le 13/02/2014 � 09h29
phil_grenoble : Il n'est pas question de niveau de revenu mais de comp�tences et le propre d'une d�mocratie est de permettre aux citoyens de remettre en question les gouvernants.
Le 12/02/2014 � 16h08
bretsinkler : Nos chers patrons n'ont soi-disant pas de comptes � rendre ? Ok, coupons-leur toutes les aides et contrats sponsoris�s par l'�tat alors. Et si jamais ils coulent, c'est tant pis pour eux, ce qu'on aurait d� faire pour les banques. Lib�ral que quand �a les arrange... Et vous verrez, il y aura encore des entreprises qui feront des b�n�fices, s�rement plus sainement et honn�tement.
Le 11/02/2014 � 17h59
pierrejeme : "Ce que les patrons allemands ont compris" oui OK mais aussi et surtout ce que les syndicats allemands, eux, pratiquent depuis fort longtemps !
Le 11/02/2014 � 16h54
alain-paris : Mais n'on t-ils pas promis 1 millions d'emplois en �change de l'all�gement des charges ? Ou alors c'�tait du vent ?
Le 11/02/2014 � 15h54
