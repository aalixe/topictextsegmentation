TITRE: Google devient la deuxi�me capitalisation boursi�re mondiale - rts.ch - info - economie
DATE: 2014-02-11
URL: http://www.rts.ch/info/economie/5602222-google-devient-la-deuxieme-capitalisation-boursiere-mondiale.html?wysistatpr%3Dads_rss_texte%26rts_source%3Drss_t
PRINCIPAL: 162370
TEXT:
Google devient la deuxi�me capitalisation boursi�re mondiale
11.02.2014 07:20
Google devient la deuxi�me capitalisation boursi�re mondiale, derri�re un autre g�ant informatique: Apple. [Joerg Sarbach/dapd - Keystone]
Le g�ant d'internet Google a d�log� lundi le groupe p�trolier ExxonMobil de la place de deuxi�me plus grosse capitalisation boursi�re au monde, derri�re la soci�t� informatique Apple.
Google a termin� la s�ance lundi � la Bourse de New York avec une capitalisation boursi�re de pr�s de 394 milliards de dollars, d�passant pour la premi�re fois � la cl�ture celle d'Exxon (388 milliards de dollars). Apple reste en t�te avec pr�s de 472 milliards de dollars.
Exxon, comme les autres groupes p�troliers am�ricains, voit le cours de son action reculer depuis le d�but de l'ann�e. Elle a encore baiss� de 1,17% pour cl�turer � 89,52 dollars lundi, portant ses pertes depuis d�but janvier � 11,5%.
Niveaux historiques
Le titre Google �volue en revanche sur ses plus hauts niveaux historiques: il a doubl� de valeur depuis mi-juillet 2012 et m�me tripl� depuis le printemps 2009 o� la Bourse am�ricaine avait touch� son point bas.
Le record absolu � la cl�ture (1.180,97 dollars) remonte au 31 janvier, o� Google avait pris 4% au lendemain de l'annonce d'une hausse de 20% de son b�n�fice net en 2013, � 12,9 milliards de dollars, pour un chiffre d'affaires de presque 60 milliards.
afp/jvia
Leader de la recherche en ligne
Google est depuis plusieurs ann�es d�j� un poids lourd dans la recherche en ligne, o� son moteur Google Search est dominant, ainsi que dans la publicit� en ligne dont il s'est adjug� un tiers du march� mondial l'an dernier.
Son premier concurrent, le r�seau social en ligne Facebook, ne se place qu'� 5,7%, selon des estimations de la soci�t� sp�cialis�e eMarketer.
Logiciels pour mobile
Si Google a enregistr� un �chec dans la fabrication des appareils eux-m�mes et vient d'annoncer la revente des t�l�phones de Motorola au groupe chinois Lenovo, il est devenu incontournable dans les logiciels.
Son syst�me d'exploitation Android, utilis� par de nombreux fabricants comme le sud-cor�en Samsung, fait fonctionner environ les trois quarts des nouveaux smartphones vendus dans le monde.
archives
