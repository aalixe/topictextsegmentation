TITRE: La Cour des comptes étrille la gestion du patrimoine universitaire - 11 février 2014 - Le Nouvel Observateur
DATE: 2014-02-11
URL: http://tempsreel.nouvelobs.com/education/20140210.OBS5766/la-cour-des-comptes-etrille-la-gestion-du-patrimoine-universitaire.html
PRINCIPAL: 0
TEXT:
Actualité > Education > La Cour des comptes étrille la gestion du patrimoine universitaire
La Cour des comptes étrille la gestion du patrimoine universitaire
Publié le 11-02-2014 à 10h55
Mis à jour à 12h22
La Cour des comptes estime que la chancellerie des universit�s de Paris ne sert � rien et co�te 14 millions par an, laissant d�p�rir un pr�cieux patrimoine immobilier.
(C) SIPA
La Cour des comptes n�y va pas par quatre chemins. Elle pr�conise dans son rapport annuel de supprimer purement et simplement une institution selon elle inutile�: la chancellerie des universit�s de Paris. Cr��e apr�s�l��clatement de l�universit� de Paris apr�s mai 68, cet �tablissement public a pour mission de g�rer le patrimoine immobilier des facs parisiennes, qui comprennent notamment les prestigieux locaux de la Sorbonne et leurs salles d'apparat. Co�t de fonctionnement annuel�: 14 millions d�euros. B�n�fice pour la collectivit� selon les magistrats de la Cour�: n�ant.
Ch�teaux en ruine, personnalit�s log�es � bon compte...
La chancellerie se r�v�le, selon l'enqu�te de la Cour, un bien pi�tre gestionnaire immobilier. Qu�il s�agisse des b�timents parisiens, 15.500 m2 "tr�s bien situ�s", c'est un euph�misme, en plein coeur du quartier latin, ou du ch�teau de Richelieu en Indre-et-Loire, l�gu� par le Comte du m�me nom en 1930 pour loger des professeurs de la Sorbonne ou des coll�gues �trangers, du domaine de Ferri�res offert lui en 1975 par Guy de Rothschild pour cr�er un centre de recherche et de documentation qui n�a jamais vu le jour, ou encore de la Villa Finaly, l�gu�e dans les ann�es 1950 pour cr�er un centre d��tudes et d�accueil pour les �tudiants. En 2012, il lui a m�me fallu restituer le domaine de Ferri�res, si mal g�r� qu'il mena�ait de tomber en ruine.
Lecteurs � prix d'or
Et la Cour n�est pas plus indulgente pour ses autres attributions, notamment l�attribution de bourses et prix � des �tudiants dont le montant ne cesse de diminuer chaque ann�e. Elle �pingle au passage quelques "personnalit�s", secr�taires d'Etat, inspecteurs �ou anciens responsables de la chancellerie, log�s � bon compte dans les appartements du quartier latin. Quant � la biblioth�que litt�raire Jacques Doucet, g�r�e par la chancellerie, son co�t annuel d�passe 500.000 euros, pour 329 lecteurs inscrits�! Qui dit mieux�?
V�ronique Radier - Le Nouvel Observateur
Partager
