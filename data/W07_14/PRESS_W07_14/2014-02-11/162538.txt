TITRE: L'OREAL : L'Or�al va racheter 8% de son capital � Nestl� pour E6,5 mds, infos et conseils valeur FR0000120321, OR - Les Echos Bourse
DATE: 2014-02-11
URL: http://bourse.lesechos.fr/infos-conseils-boursiers/infos-conseils-valeurs/infos/l-oreal-va-racheter-8-de-son-capital-a-nestle-pour-e6-5-mds-950166.php
PRINCIPAL: 162528
TEXT:
L'OREAL : L'Or�al va racheter 8% de son capital � Nestl� pour E6,5 mds
11/02/14 � 07:25
- Reuters 0 Commentaire(s)
LOr�al va racheter 8% de son capital � Nestl� pour E6,5 mds | Cr�dits photo : � L'or�al
PARIS, 11 f�vrier (Reuters) - Les conseils d'administration de Nestl� et de L'Or�al ont approuv� un projet de rachat par le groupe fran�ais de cosm�tiques de 48,5 millions de ses propres actions, soit 8% de son capital, au groupe suisse, pour un montant total de 6,5 milliards d'euros, annonce mardi L'Or�al dans un communiqu�.
Ce rachat sera financ� pour partie par la cession par L'Or�al � Nestl� de sa participation de 50% dans le laboratoire pharmaceutique suisse de dermatologie Galderma, d�tenu � parit� par L'Or�al et Nestl� , pour un montant de 3,1 milliards d'euros de valeur d'entreprise (2,6 milliards d'euros de valeur des fonds propres). L'Or�al versera �galement en compl�ment 3,4 milliards d'euros en num�raire.
Toutes les actions rachet�es par L'Or�al seront annul�es et � l'issue de cette op�ration, la participation de Nestl� au capital de L'Or�al sera r�duite de 29,4% � 23,29%, celle de la famille Bettencourt Meyers, l'actionnaire historique du groupe, passant de 30,6% � 33,31% du capital.
Le nombre de repr�sentants de Nestl� sera ramen� de trois � deux administrateurs au conseil de L'Or�al.
L'op�ration sera relutive de plus de 5% sur le r�sultat r�current par action de L'Or�al sur la base d'une ann�e pleine.
Ce rachat sera financ� exclusivement avec les disponibilit�s de L'Or�al et � travers l'�mission de billets de tr�sorerie � court terme et ne n�cessitera donc pas de recourir � la cession de titres Sanofi d�tenus par L'Or�al, pr�cise le groupe.
L'op�ration devrait �tre boucl�e avant la fin du premier semestre 2014. (Jean-Michel B�lot, �dit� par Gw�na�lle Barzic)
Les derni�res infos du secteur Articles personnels
