TITRE: Le virus du Chikungunya gagne du terrain aux Antilles � Top Actus Sant�
DATE: 2014-02-11
URL: http://topactu.fr/sante-et-bien-etre/le-virus-du-chikungunya-gagne-du-terrain-aux-antilles/24354/2014/02/10.html
PRINCIPAL: 161993
TEXT:
Article �crit par La R�daction dans la cat�gorie Sant� et bien-�tre
�
Le virus du Chikungunya continue � se r�pandre sur plusieurs �les des Antilles, et atteint aujourd�hui la Martinique et la Guadeloupe. Depuis d�cembre, plusieurs d�partements d�Outre-mer ont �t� touch�s par l��pid�mie : Saint-Martin et Saint-Barth�l�my en premier, puis la Martinique et la Guadeloupe, au point que le directeur g�n�ral de la sant� a d�cid� de se rendre lundi et mardi dans les zones infect�es.
Si l��le Saint-Martin est la premi�re a avoir �t� touch�e par le virus, et ceci avec quelques cas isol�s d�s d�but d�cembre 2013, c�est �galement aujourd�hui le d�partement le plus touch� mais plus le seul. L�institut national de veille sanitaire (InVS) a en effet signal� ces derniers jours une recrudescence de cas notamment � sur cette �le, mais aussi Saint-Barth�lemy, la Martinique et la Guadeloupe.
Avec 1 025 cas �cliniquement �vocateurs�, 601 �cas probables ou confirme?s� et un de?ce?s enregistr�, L��le Saint-Martin a �t� plac�e en �tat d���pid�mie g�n�ralis�e�. Si � Saint-Barth�l�my, la progression reste en dessous de Saint-Martin, la progression de l��pid�mie restant mod�r�e, elle a tout de m�me enregistr� 215 cas cliniques et 83 cas probables ou confirm�s.
En Martinique, ce sont 1480 cas cliniquement �vocateurs qui ont �t� recens�s, et 518 cas probables ou confirm�s, montrant que la circulation du virus se fait plus intense. En Guadeloupe, 18 communes sont concern�es�, ce qui place ce d�partement en phase de �transmision autochtone mod�r�e. 790 cas cliniquement �vocateurs ont ainsi �t� recens�s et 175 autres cas probables ou confirm�s.
En raison de l�augmentation de la circulation du virus aux Antilles, le directeur g�n�ral de la Sant�, Beno�t Vallet, a d�cid� de se rendre sur place pour faire un point sur la progression de l��pid�mie. Il a principalement comme objectif de prendre toutes les mesures qui permettront de prot�ger la population et lutter contre les moustiques vecteurs.
