TITRE: "Je suis surtout venue pour Sarkozy" - BFMTV.com
DATE: 2014-02-11
URL: http://www.bfmtv.com/politique/je-suis-surtout-venu-sarkozy-707428.html
PRINCIPAL: 162496
TEXT:
> UMP
"Je suis surtout venue pour Sarkozy"
En marge du meeting de Nathalie Kosciusko-Morizet, lundi 10 f�vrier, quatre militants ou sympathisants ont expliqu� � BFMTV.com pourquoi ils soutiennent la candidate. Et ce qu'ils avaient pens� de la visite surprise de Nicolas Sarkozy.
Mis � jour le 12/02/2014 �  9:01
- +
r�agir
Ils habitent Paris et sont venus soutenir Nathalie Kosciusko-Morizet en vue des prochaines �lections municipales dans la capitale. En marge du premier meeting de la candidate UMP , lors duquel Nicolas Sarkozy a fait une apparition tr�s remarqu�e, ils ont donn� leur sentiment � BFMTV.com.
> NICOLAS, 44 ANS
Pourquoi �tre venu soutenir NKM?
On m'a dit aujourd'hui qu'elle tenait un meeting � Paris. Son projet m'int�resse, en tant qu'entrepreneur et cr�ateur de start-up.
Qu'avez-vous pens� de la venue de Nicolas Sarkozy?
Je ne savais pas qu'il serait l�, je croyais que c'�tait une erreur. Quand j'ai entendu les "Nicolas, Nicolas!" j'ai cru que c'�tait pour moi (rires)! Plus s�rieusement, en tant que sympathisant de droite, je suis plut�t pour qu'il revienne. Il n'y a pas d'alternative � droite, � part peut-�tre Bruno Le Maire. Mais entre Fillon, Cop� et Sarkozy, je pr�f�re Sarkozy!
Croyez-vous encore aux chances de NKM pour Paris?
Elle semble avoir perdu. Mais je me souviens que Jacques Chirac, en son temps, avait su renverser la vapeur. En politique, rien n'est jamais perdu.
> CAMILLE ET AURELIEN, 18 ANS
Pourquoi �tre venu soutenir NKM?
Aur�lien: je savais qu'elle tenait un meeting. Et puis j'ai entendu l'information selon laquelle Nicolas Sarkozy allait venir. Ca m'a d�cid�.
Camille: moi, il m'a pr�venu ce matin que Nicolas Sarkozy venait! Je suis surtout venue pour lui.
Qu'avez-vous pens� de la venue de Nicolas Sarkozy?
Camille: il y avait une ambiance g�niale, NKM et Nicolas Sarkozy ont assur� le spectacle!
Aur�lien: c'est une bonne chose, peut-�tre que cela va jouer pour qu'un certain �lectorat de droite se mobilise.
Croyez-vous encore aux chances de NKM pour Paris?
En choeur: oui, on y croit! Et Nicolas Sarkozy peut faire pencher la balance!
> MARC, 61 ANS
Pourquoi �tre venu soutenir NKM?
Parce qu'elle le m�rite, elle est courageuse. Je la crois capable de donner un coup de pied dans la fourmilli�re socialiste, et de donner espoir aux Parisiens, les sortir de la morosit�.
Qu'avez-vous pens� de la venue de Nicolas Sarkozy?
Je ne savais pas qu'il viendrait. Mais j'�tais tr�s content de le revoir et je pense que cela peut �tre un plus pour la campagne. N�anmoins, j'�tais venu avant tout pour soutenir Nathalie Kosciusko-Morizet.
Croyez-vous encore aux chances de NKM pour Paris?
Plus le temps passe, plus je pense qu'elle est capable d'aller chercher la victoire. Elle est dynamique, et surtout, les Parisiens en ont marre!
A lire aussi
