TITRE: Intégration des immigrés : Ayrault présente un plan a minima - 11 février 2014 - Le Nouvel Observateur
DATE: 2014-02-11
URL: http://tempsreel.nouvelobs.com/societe/20140211.OBS5924/integration-des-immigres-ayrault-presente-un-plan-a-minima.html
PRINCIPAL: 164927
TEXT:
Publié le 11-02-2014 à 20h18
Mis à jour à 22h58
La "feuille de route" pour l'int�gration�des immigr�s et la lutte contre les discriminations�ne comprend aucune "annonce spectaculaire" pour �viter les controverses.
Jean-Marc Ayrault, le 13 décembre 2013 à Matignon. (BERTRAND GUAY/AFP)
Op�ration d�minage assum�e. Le gouvernement a pr�sent� mardi 11 f�vrier sa "feuille de route" pour l'int�gration�des immigr�s et la lutte contre les discriminations, qui ne comprend aucune "annonce spectaculaire", ni moyen suppl�mentaire, dans l'espoir d'�viter toute controverse.
Une approche "apais�e, sereine"
Une quinzaine de ministres ont adopt� 28 mesures qui seront "consensuelles, je l'esp�re", a d�clar� le chef du gouvernement, Jean-Marc Ayrault , � l'issue d'une r�union � Matignon.
"La politique n'est pas l'art de mettre sous le tapis les probl�mes", a-t-il ajout�, en plaidant pour une approche "apais�e, sereine" .
Exit donc les questions de m�moire, de religion, ou d'ouverture de la fonction publique : "On s'est concentr� sur les questions d'�galit�", explique son entourage.
Alors que les Suisses viennent de voter pour r�tablir des "quotas d'immigr�s" et que 66% des Fran�ais jugent qu'il y a trop d'immigr�s dans le pays, le climat n'est pas favorable � des avanc�es majeures.
A quelques semaines des �lections municipales, le sujet pr�te encore plus � la surench�re. En d�cembre, la classe politique s'est ainsi enflamm�e apr�s la publication sur le site du Premier ministre de rapports d'experts, qui pr�conisaient notamment d'abolir la loi sur le voile � l'�cole.
Dans ce contexte, la principale annonce de la nouvelle "feuille de route" porte sur la cr�ation d'un "d�l�gu� interminist�riel � l'�galit� r�publicaine et � l'int�gration", rattach� au Premier ministre, dont le nom sera communiqu� d'ici deux � trois semaines.
Il s'agit d'une structure "l�g�re" qui n'empi�tera pas sur les comp�tences des diff�rents ministres et aura pour seule mission de "coordonner" leurs actions et d'�valuer les politiques men�es.
Certains au sein du gouvernement, dont le ministre d�l�gu� � la Ville Fran�ois Lamy, plaidaient pour une structure dot�e de r�els moyens. "Si la structure avait pris trop de place, les ministres risquaient de se d�fausser", a justifi� Matignon.
"Rien ne serait pire qu'une vague feuille de route cosm�tique"
Le ministre de l'Int�rieur, Manuel Valls , reste donc comp�tent pour l'int�gration�des "primo-arrivants", pr�sents depuis moins de cinq ans en France.
Il compte mettre l'accent sur l'acquisition du fran�ais, en exigeant un meilleur niveau. En r�ponse � une question, il a indiqu� qu'il conditionnerait la d�livrance des titres de s�jour aux progr�s accomplis, sans donner de d�tails.
Le dispositif devrait figurer dans le prochain projet de loi immigration, attendu d'ici � la fin de l'ann�e.
Quant � la lutte contre les discriminations selon l'origine, le gouvernement pr�voit surtout de renforcer la formation des agents publics (enseignants, conseillers d'orientation, agents de P�le emploi, inspecteurs du travail) sur la question.
Il a �galement demand� aux partenaires sociaux de discuter des CV anonymes et des recours collectifs en cas de discriminations, lors de la prochaine conf�rence sociale avant l'�t�.
"Ne pas partir sur le terrain de l'histoire officielle"
D'autres propositions avaient d�j� �t� annonc�es : lutte contre le d�crochage scolaire, r�forme de l'�ducation prioritaire, cr�ation d'une aide pour les migrants �g�s qui font des aller-retour dans leur pays d'origine, poursuite de la r�novation des foyers de travailleurs migrants, etc.
Dans les derniers jours, le gouvernement a d�cid� de retirer toute une s�rie de mesures qui portaient sur la reconnaissance de l'apport des �trangers � la construction de la France (mise en valeur d'"oubli�s de l'histoire" sur France T�l�vision, d�veloppement des cours d'arabe et de mandarin dans les �tablissements scolaires, etc.).
"On ne voulait pas partir sur le terrain de l'histoire officielle ou sur celui des m�moires concurrentes", des mati�res �minemment sensibles, a expliqu� l'entourage du Premier ministre.
Cette prudence pourrait rassurer certains d�put�s socialistes qui s'inqui�taient lundi de l'ouverture d'un nouveau front sur un sujet de soci�t�, apr�s le retrait de la loi famille.
Mais elle risque de d�cevoir les personnes concern�es - 5,3 millions d'immigr�s et 6,7 millions de Fran�ais d'origine �trang�re. "Rien ne serait pire qu'une vague feuille de route cosm�tique" , soulignait mardi matin l'association France Terre d'Asile (FTA).
Partager
