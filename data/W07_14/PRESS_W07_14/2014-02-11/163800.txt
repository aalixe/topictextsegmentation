TITRE: Le bio bon pour la santé? Lancement d'une étude à long terme - 11 février 2014 - Le Nouvel Observateur
DATE: 2014-02-11
URL: http://tempsreel.nouvelobs.com/societe/20140211.AFP9770/le-bio-bon-pour-la-sante-lancement-d-une-etude-a-long-terme.html
PRINCIPAL: 163798
TEXT:
Actualité > Société > Le bio bon pour la santé? Lancement d'une étude à long terme
Le bio bon pour la santé? Lancement d'une étude à long terme
Publié le 11-02-2014 à 10h25
Mis à jour à 15h46
A+ A-
Manger bio a-t-il réellement un effet bénéfique à long terme sur la santé? Une vaste étude sur plusieurs années concernant 100.000 personnes au moins, dont la moitié adepte du bio, lancée mardi, va tenter de répondre à la question. (c) Afp
Paris (AFP) - Manger bio a-t-il réellement un effet bénéfique à long terme sur la santé? Une vaste étude sur plusieurs années concernant 100.000 personnes au moins, dont la moitié adepte du bio, lancée mardi, va tenter de répondre à la question.
C'est la plus grande étude jamais conduite sur la consommation des aliments bio, relève le Dr Emmanuelle Kesse, épidémiologiste qui pilote cette nouvelle étude appelée "BioNutriNet" dont l'objectif est de mieux comprendre qui sont les consommateurs de produits bio et les liens entre la consommation de ce type d'aliments et la santé.
L'étude prévoit notamment de mesurer sur un sous-groupe de participants les résidus de pesticides, dit-elle à l'AFP.
Les responsables de l'étude, qui se poursuivra au moins 5 ans, lancent un appel afin que le plus grand nombre de consommateurs (réguliers ou occasionnels) ou non consommateurs de produits bio, participent à ce projet.
"Il y a déjà eu des études sur les consommateurs de bio mais généralement sur de petits groupes et durant seulement quelques semaines", ajoute le Dr Kesse.
Résidus de pesticides
Les consommateurs réguliers de produits bio, 7% de la population française, sont plus éduqués et physiquement plus actifs que les non-consommateurs, mais disposent en revanche de revenus généralement comparables à ceux qui déclarent ne pas s'intéresser au bio, d'après une première étude ponctuelle sur la cohorte NutriNetSanté (échantillon de 54.300 adultes français) publiée en octobre 2013 dans la revue PloS One.
En revanche, les non-consommateurs qui eux invoquent un coût trop cher pour ne pas en consommer, ont effectivement de moindres revenus, selon ce travail.
Si les rations caloriques journalières des consommateurs de bio sont comparables à celles des autres participants, en revanche leurs apports en vitamines, minéraux, omega-3 et fibres sont supérieurs.
Les mangeurs bio sont moins souvent en surpoids ou obèses, montrait également l'étude.
Le nouveau projet cherche à aller plus loin : il s'agit de mieux préciser ce qui détermine la consommation ou non des aliments bio ainsi que ses effets sur l'état nutritionnel (vitamines et minéraux) et toxicologique (résidus de pesticides), mesurés sur un sous-groupe de 300 personnes, grâce à des marqueurs sanguins et urinaires.
Comme tous les Nutrinautes (ceux qui participent à la grande enquête Nutrinet-santé), les participants répondront à divers questionnaires sur leur alimentation, activité physique, poids, taille, antécédents de santé personnels et familiaux, avec une interface sécurisée assurant la confidentialité de leurs réponses.
Chaque mois, ils recevront par mél un questionnaire complémentaire afin de mieux connaître en détail les motivations de l'achat ou non de produits bio, les lieux et le coût de ces achats ainsi qu'au fil du temps de l'état de santé des participants.
A terme, BioNutriNet permettra d'évaluer un éventuel effet protecteur vis-à-vis de malades chroniques (cancers, maladies cardiovasculaires, obésité, diabète, etc.).
Le financement spécifique de BioNutriNet (700.000 euros) est assuré par l'Agence Nationale pour la Recherche.
pour accéder au site de l'étude
Partager
