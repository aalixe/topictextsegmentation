TITRE: Des passants d�couvrent une jambe humaine flottant sur la Seine | Insolite
DATE: 2014-02-11
URL: http://www.lapresse.ca/actualites/insolite/201402/11/01-4737843-des-passants-decouvrent-une-jambe-humaine-flottant-sur-la-seine.php
PRINCIPAL: 165034
TEXT:
>�Des passants d�couvrent une jambe humaine flottant sur la Seine�
Des passants d�couvrent une jambe humaine flottant sur la Seine
Personnes disparues
Agence France-Presse
Une jambe humaine en �tat de d�composition avanc�e d�rivant sur la Seine a �t� d�couverte mardi apr�s-midi par des passants pr�s de Paris, a-t-on appris de sources polici�res.
Peu avant 16H00, trois passants ont aper�u une jambe flottant sur la Seine � la hauteur de la commune de Saint-Cloud et ont pr�venu la police.
�Les policiers du commissariat de Saint-Cloud ont fait appel � la brigade fluviale de la pr�fecture de police de Paris dans le but de rechercher le reste du corps, mais en vain�, a expliqu� une source.
Selon les premiers �l�ments de l'enqu�te, le membre appartient � une personne � la peau blanche, dont le sexe et l'�ge ne sont pas encore connus.
Crime, accident ou suicide, les policiers du commissariat de Saint-Cloud saisis de l'enqu�te n'excluent pour l'heure aucune piste. �Les h�lices des bateaux et p�niches ont pu mutiler le corps�, pr�cise une autre source.
Des pr�l�vements ADN vont �tre r�alis�s sur le membre aux fins d'identification. Les enqu�teurs vont �galement effectuer des recherches afin d'�tablir si cette d�couverte est li�e � un cas de disparition de personne dans le secteur.
Partager
