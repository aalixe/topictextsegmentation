TITRE: Handicap: un résultat 'accablant' pour Paris
DATE: 2014-02-11
URL: http://www.lefigaro.fr/flash-actu/2014/02/11/97001-20140211FILWWW00284-handicap-un-resultat-accablant-pour-paris.php
PRINCIPAL: 164275
TEXT:
Réagir à cet article
Publicité
SainteCarla FièreDEtreDeDroite
Maintenant que je suis plutôt vieille, je me rends compte de la difficulté pour les Personnes Handicapées (ou Âgées, ou bientôt Invalides) dans Paris (et autres villes, d'ailleurs).
Mais peut-être NKM devrait nous expliquer pourquoi son Gouvernement (2007-2012) n'a rien (ou presque) fait à ce sujet ? Les Promesses Repoussées, les Échéances remisent à jamais, etc.
Avant d'attaquer les autres, NKM devrait vérifier ses arrières !
Le 11/02/2014 à 21:50
A. Leonor
Ce n'est pas faux dans certains quartiers, comme le 15ème, par exemple.
En revanche, dans le 13ème arrondissement la circulation des fauteuils roulants est en général assez aisée, à la suite d'un gros effort accompli en une dizaine d'années.
Le 11/02/2014 à 21:22
