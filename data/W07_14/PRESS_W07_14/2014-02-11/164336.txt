TITRE: Le Cr�dit Mutuel Ark�a lance une application bancaire pour 'Google Glass' !
DATE: 2014-02-11
URL: http://www.boursier.com/actualites/news/le-credit-mutuel-arkea-lance-une-application-bancaire-pour-google-glass-565941.html
PRINCIPAL: 164332
TEXT:
Cr�dit photo ��Reuters
(Boursier.com) � Le Cr�dit Mutuel Ark�a annonce la cr�ation de la premi�re application bancaire pour lunettes connect�es en France. Une innovation majeure d�velopp�e en collaboration avec AMA Studios.
"Le Cr�dit Mutuel Ark�a place, depuis toujours, l'innovation au coeur de ses m�tiers. Le groupe fut l'un des pr�curseurs de la banque � distance, notamment avec l'apparition du Minitel puis des supports mobiles tels que les Smartphones... autant de r�ponses pour accompagner la mobilit� de ses clients" rappelle l'�tablissement. "Avec le lancement, in�dit en France, d'une application bancaire pour Google Glass, le Cr�dit Mutuel Ark�a confirme sa fibre novatrice".
Alors que les lunettes connect�es pourraient �tre commercialis�es dans le courant de l'ann�e en France, l'application est d�j� disponible pour les clients des trois f�d�rations du Cr�dit Mutuel Ark�a (Cr�dit Mutuel de Bretagne, Cr�dit Mutuel du Sud-Ouest, Cr�dit Mutuel Massif Central). Pour activer et tester d�s � pr�sent ce nouveau service depuis la France ou l'�tranger, les personnes �quip�es de Google Glass sont invit�es � prendre contact avec la banque : innovation@arkea.com.
Parmi les fonctionnalit�s propos�es par le nouvel applicatif, les utilisateurs pourront notamment consulter leurs comptes, g�olocaliser le distributeur de billets ou l'agence bancaire la plus proche, ou encore contacter leur conseiller.
Les op�rations de connexion et d'authentification, r�alis�es via le t�l�phone mobile de l'utilisateur, sont s�curis�es...
Claude Leguilloux � �2014, Boursier.com
