TITRE: Mark Zuckerberg : le moteur de recherche de Facebook dominera celui de Google
DATE: 2014-02-11
URL: http://www.ya-graphic.com/2014/02/mark-zuckerberg-le-moteur-de-recherche-de-facebook-dominera-celui-de-google/
PRINCIPAL: 163159
TEXT:
Mark Zuckerberg : le moteur de recherche de Facebook dominera celui de Google Examin� par le
f�v 11
. Comment Mark Zuckerberg voit l�avenir de Facebook, le r�seau social qui compte 1,2 milliard d�utilisateurs dans une seule plateforme�? R�cemment le patron du r� Comment Mark Zuckerberg voit l�avenir de Facebook, le r�seau social qui compte 1,2 milliard d�utilisateurs dans une seule plateforme�? R�cemment le patron du r� Rating: 0
Vous �tes ici: Accueil � Actualit�s � Mark Zuckerberg : le moteur de recherche de Facebook dominera celui de Google
Mark Zuckerberg : le moteur de recherche de Facebook dominera celui de Google
Post� par: Yassine A. Post� le: 11 f�vrier 2014 Dans: Actualit�s
Comment Mark Zuckerberg voit l�avenir de Facebook , le r�seau social qui compte 1,2 milliard d�utilisateurs dans une seule plateforme�? R�cemment le patron du r�seau social le plus populaire au monde a partag� sa vision dans un entretien avec des analystes de �Wall Street.
Les b�n�fices�de Facebook au quatri�me trimestre 2013 avaient �t� pr�sent�s lors d�une conf�rence qui s�est tenue le 29 janvier 2014. C��tait l�occasion pour Mark Zuckerberg d�annoncer des chiffres positifs mais aussi de pr�senter quelques perspectives de Facebook dans les 5 � 10 prochaines ann�es. Le PDG a �voqu� la cr�ation d�un moteur de recherche qui dominerait la plupart des moteurs de recherche dont celui de Google.
Le moteur de recherche de Facebook
Facebook veut devenir le meilleur espace o� les gens peuvent partager du contenu de qualit�. Le r�seau social poss�de � lui seul un index de donn�es beaucoup plus volumineux que celui des moteurs de recherche du web � celui de Google entre autres. Il y a en effet plus d�un billion de statuts Facebook, de photos et autres donn�es qui ont �t� partag�es depuis les 10 derni�res ann�es. C�est ce qu�affirment les employ�s de Facebook qui ont d�j� travaill� pour des moteurs de recherche du web.
L�intelligence artificielle de Facebook met ces donn�es � disposition du public gr�ce au Graph Search et au moteur de recherche du r�seau social. Notez que le Graph Search n�est encore qu�en version anglaise, Mark Zuckerberg a annonc� qu�il devait �tre internationalis� sans toutefois pr�ciser de date.
��Nous sommes une soci�t� mobile�� affirme Mark Zuckerberg m�me s�il reconna�t que le Graph Search n�a pas encore �t� d�ploy� dans les appareils mobiles. Le moteur de recherche sera d�ploy� dans ces appareils pour que les gens puissent trouver des r�ponses pr�cises � leurs questions, notamment gr�ce � la reconnaissance vocale. Mark Zuckerberg a pr�cis� qu�aucun moteur de recherche ne pourra rivaliser avec les r�ponses propos�es par le moteur de recherche de Facebook.
Le moteur de recherche de Facebook ne sera pas limit� au web
Quand Mark Zuckerberg �voque les moteurs de recherche actuels il pr�cise que ces moteurs sont limit�s comme celui de Google � limit� aux navigateurs web. En effet il ne dit pas �� search engine�� mais � web search engine �, indique� Business Insider . Vous aurez compris que Facebook a l�ambition de devenir le moteur de recherche le plus grand et le plus complexe jamais cr��.
