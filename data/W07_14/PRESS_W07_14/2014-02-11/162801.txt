TITRE: Cannes - Montpellier : Le direct et les groupes
DATE: 2014-02-11
URL: http://www.foot-national.com/foot-cannes-montpellier-le-direct-et-les-groupes-54595.html
PRINCIPAL: 162800
TEXT:
Cannes - Montpellier : Le direct et les groupes
Cannes - Montpellier : Le direct et les groupes
Pays ? France
A 18h30, Cannes et Montpellier inaugurent les huiti�mes de finale de la Coupe de France. Les deux �quipes se sont d�j� illustr�es lors de cette �dition. L'AS Cannes a fait tomber Saint-Etienne en 32e de finale. Montpellier s'est pay� le luxe d'�liminer le Paris Saint-Germain au Parc des Princes. Voici les groupes des joueurs appel�s par Jean-Marc Pilorget et Rolland Courbis.
Le groupe de Cannes :�Beuve, Gavanon - Cerielo, Marignale, Cl�ment, Rouabah, M'Boup, Me�t�, Chmielinski, Darnet, Soly, Siki, Lenzini, Zokiri, Jama�, Camara, Soumah, Uzamudunka.
Le groupe de Montpellier :�Pionnier, Jourdren - Bocaly, Hilton, El Kaoutari, Deplagne, Jebbour, Assoumin, Marveaux, Mounier, Martin, Cabella, Sanson, Stambouli, Niang, Tinhan, A�t-Fana, Camara, Deza, Monta�o.
