TITRE: Zemmour, I>T�l� et Alain Soral: cherchez l�intru?    | Imm�dias - Lexpress
DATE: 2014-02-11
URL: http://blogs.lexpress.fr/media/2014/02/07/zemmour-itele-et-alain-soral-cherchez-lintru/
PRINCIPAL: 0
TEXT:
Zemmour, I>T�l� et Alain Soral: cherchez l�intru?
le 7 f�vrier 2014 16H08 | par
Renaud Revel
Heureusement que les rubriques � Zapping � des chaines sont� l� pour pointer du doigt ce qui nous a � parfois �chapp�.� Et qui m�rite le d�tour. Surtout quand le sujet porte sur la derni�re fac�tie en date d�un Eric Zemmour pris en flagrant d�lit de manipulation et de bidonnage. Le 1er f�vrier dernier, en effet, l��minent chroniqueur de RTL et de I>T�l� s�est livr� sur le plateau de cette derni�re � un num�ro de prestidigitation id�ologique qui m�aurait sans doute valu un avertissement sal� de ma direction,� si je m��tais livr� dans les colonnes de� l�Express � une telle op�ration.
Durant ce d�bat qui l�oppose� � Nicolas Domenach, (cet excellent� journaliste de Marianne dont on se demande bien ce qu�il fait encore aux cot�s de cet olibrius), le chroniqueur a cru bon brandir � l�antenne un document accablant, � l�entendre, visant� Vincent Peillon : ce ministre �tait accus�, preuve � l�appui,� de livrer l��cole publique � une Cinqui�me colonne compos�e, pour faire court,� d�homosexuels et de transsexuels : vous savez, ces missionnaires de� la ��th�orie du genre�� infiltr�s dans l�Education nationale, o� comme chacun sait l�enseignement de la masturbation est inscrit� au fronton des �coles maternelles. S�ensuivit des salmigondis inaudibles et path�tiques� dans la bouche� de celui qui s�agitait � l�antenne, en brandissant son document : preuve de l�ignominie d�un ministre de la R�publique enfin d�busqu�.
Or cette pi�ce � charge, preuve absolue et d�finitive de la d�viance du ministre en question et de son administration, n��tait� autre que la triste photocopie d�un brulot pondu par quelques dangereux illumin�s exer�ant leurs talents sur le site �galit� et r�conciliation d�Alain Soral : celui dont les soutiens se sont d�ailleurs gondol�s sur la toile, en d�couvrant Eric Zemmour faire la publicit� de leur litt�rature.
On pourrait s�arr�ter� l�. Et consid�rer que cette nouvelle sortie de route de cet agit� du bocal, dont j�ai renonc� depuis longtemps � comprendre le cheminement de la pens�e, n�est qu�un �pisode de plus dans le parcours chaotique d�un gar�on d�cid�ment �gar� dans ce si�cle. Et que les ann�es 30 r�clament : quand les militants de la Ligue des Croix de feu de Fran�ois de la Rocque d�filaient dans les rues de Paris.
Ce qui est plus grave chez Zemmour, c�est que surfant sur les tr�s naus�eux discours d�une frange de la soci�t� fran�aise, homophobe et j�en passe, -celle que l�on a vu battre le pav� de Paris, le week-end dernier-, ce dernier n�h�site pas � puiser ses arguments dans les recoins les moins bien fr�quent�s d�Internet. L� o� Soral et consorts tiennent salon et fabriquent des �l�ments de langage: du pr�t-�-servir aussit�t ron�otyp� par quelques perroquets de service.
Celui-l� m�me en l�occurrence auquel des grands m�dias comme I>T�l� ou RTL offrent une tribune. C�est leur probl�me.
Tweet
0  trackback
Voici la liste de liens se r�f�rant � cette note : Zemmour, I>T�l� et Alain Soral: cherchez l�intru? .
URL de trackback de cette note : http://blogs.lexpress.fr/media/2014/02/07/zemmour-itele-et-alain-soral-cherchez-lintru/trackback/
Commentaires (31)
VIDAL �-�
7 f�vrier 2014�18 h 01 min
Monsieur DOMENACH, vecteur de la gauche bien pensante, na�ve et nocive, ne p�se pas lourd face � Monsieur ZEMMOUR, ne vous en d�plaise.
Je ne peux que vous inviter � forcer plus encore vos charges caricaturales et grotesques � l�endroit de ce dernier.
L�Histoire jugera :
L�actuel Gouvernement et chacun de ses membres.
La Presse �crite aux ordres et d�opinion (� part Valeurs Actuelles faisant office de r�sistante).
Les m�dias audiovisuel, convenus et pitoyablement complaisants (il n�y a qu�� revoir la derni�re conf�rence de presse du Pr�sident de la R�publique pour n��prouver qu�un sentiment de honte devant la ti�deur et la fadeur des questions pos�es).
Bien entendu, � vous lire Monsieur REVEL, je crois comprendre que la v�rit�, l�humanit�, la mesure, la g�n�rosit�, la justice, l��quit�, les valeurs, ne sont que dans votre camp dont je ne suis pas.
L�autre camp �tant naus�abond, etc�
Pitoyable, condamnable et totalement subjectif.
Mieux vaut en rire pour ne pas en pleurer.
