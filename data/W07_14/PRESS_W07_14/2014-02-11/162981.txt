TITRE: Nec annonce un �cran 24" UHD au gamut �tendu
DATE: 2014-02-11
URL: http://www.lesnumeriques.com/moniteur-ecran-lcd/multisync-lcd-ea244uhd-bk-p19434/nec-annonce-24-uhd-en-gamut-etendu-n33159.html
PRINCIPAL: 162980
TEXT:
Nous soutenir, navigation sans publicit� Premium : 2�/mois + Conseil personnalis� Premium+ : 60�/an
Nec annonce un �cran 24" UHD au gamut �tendu
Connectivit� fournie et grande d�finition
�
Publi� le: 10 f�vrier 2014 12:28
Par Valentin Lormeau
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Envoyez-moi un mail lorsque ce produit sera en vente :
SIGNALER UN PROBLEME �
Plus de propositions Moins de propositions
Embarquant une dalle IPS, le�LCD-EA244UHD-BK propose donc une d�finition �quivalente � celle de 4 �crans en Full�HD (1920�x�1080�px). Pour le reste, c'est relativement classique, avec un contraste annonc� � 1000:1 et une luminosit� de 350�cd/m�, ainsi que des angles de vision ouverts � 178�. On ne conna�t pas en revanche le temps de r�ponse, mais il devrait comme d'habitude �tre annonc� aux alentours de 5 ou 6�ms.
La connectique est fournie, avec deux prises HDMI, compatibles MHL, deux prises DVI-D, deux prises DisplayPort et un hub avec 3 ports USB 3.0. De m�me, on peut compter sur la pr�sence d'une paire de haut-parleurs 1�W. Pas de prix annonc�, mais l'�cran devrait �tre disponible aux alentours de fin mai. En esp�rant que des applications tireront parti de l'UHD d'ici l�, ce qui n'est pas gagn� pour l'instant sur Windows 8.
