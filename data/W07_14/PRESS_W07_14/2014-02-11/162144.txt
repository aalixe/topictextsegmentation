TITRE: Google contre Cnil : le Conseil d��tat rejette un recours de Google
DATE: 2014-02-11
URL: http://www.les-infostrateges.com/actu/14021764/google-contre-cnil-le-conseil-detat-rejette-un-recours-de-google
PRINCIPAL: 0
TEXT:
Publi�e le 11 Feb 2014Tags : CNIL , Droit au respect de la vie priv�e , E-r�putation , Google .
Par Didier FROCHOT
Un ultime rebondissement dans le dernier contentieux entre Google par la Cnil vient apporter un peu d�eau au moulin de l�e-r�putation (encore nomm�e web-r�putation, cyber-r�putation ou r�putation num�rique).
Le 3 janvier 2014, la formation contentieuse de la CNIL a sanctionn� la soci�t� Google pour plusieurs manquements aux r�gles de protection des donn�es personnelles consacr�es par la loi Informatique et Libert�s (notre actualit� du 16 janvier ). Elle a ainsi prononc� une sanction p�cuniaire de 150.000 � � son encontre � la plus forte sanction financi�re jamais prononc�e par la Cnil � et ordonn� l�insertion sur le support www.google.fr d�un communiqu� faisant mention de sa d�cision.
Demande de suspension de la publication de la d�cision
Le 14 janvier 2014, la soci�t� Google a sollicit� la suspension partielle de la d�lib�ration de sanction rendue � son encontre par la CNIL le 3 janvier. Elle a en effet sollicit� la suspension de la mesure d�insertion devant le juge des r�f�r�s du Conseil d��tat. Ce dernier a rejet� cette demande par ordonnance en date du 7 f�vrier 2014.
Source : Communiqu� de la Cnil en date du 7 f�vrier 2014
Publication de la d�cision pendant le week-end dernier
C�est pourquoi, les internautes du week-end des 8 et 9 f�vrier auront pu voir appara�tre pendant 48h, conform�ment � la d�cision de la Cnil, l�avis de condamnation de Google. La d�cision avait impos� une dur�e de publication de 48h, sans pr�ciser de jour particulier dans la semaine. Il est �vident que Google a choisi les deux journ�es du week-end, o� l�utilisation d�internet est la plus r�duite�
Encore l�e-r�putation�
Par del� les faits, il est significatif de lire l�argument principal invoqu� par Google pour sa demande de suspension de publication de la d�cision l�ayant condamn�e : "la publication ordonn�e est de nature � cr�er pour elle un pr�judice d�image et de r�putation irr�parable".
Quel aveu ! La publication d�une d�cision de justice parfaitement fond�e en droit causerait donc un pr�judice irr�parable � l�honorable soci�t�
Faites ce que je demande, pas ce que je fais�
Lorsqu�elle se trouve dans l�autre camp, la soci�t� Google Inc. (USA) en personne se cache d�ordinaire derri�re sa filiale fran�aise, la seule soumise au droit fran�ais, par d�finition irresponsable des agissements de la maison m�re, notamment des nuisances provoqu�es par le moteur de recherche (notamment par le biais des recherches associ�es : par exemple "Jean Tartempion escroc") refuse syst�matiquement d�accueillir quelque demande de retrait de propos n�gatifs contre des personnes, physiques ou morales, que ce soit pour les produits Google stricto sensu (le moteur, le r�seau social, Street View, etc.), que pour les blogs qu�elle g�re (Blogspot et Blogger), se drapant dans le respect du droit am�ricain, au m�pris du droit international priv�.
Esp�rons que cette petite d�faite judiciaire impos�e par la France am�nera les dirigeants de Google � moins d�autisme juridique�
En savoir plus
