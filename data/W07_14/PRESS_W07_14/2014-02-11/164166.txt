TITRE: Scarlett Johansson va recevoir un césar d'honneur
DATE: 2014-02-11
URL: http://www.lefigaro.fr/cinema/2014/02/11/03002-20140211ARTFIG00235-scarlett-johansson-va-recevoir-un-cesar-d-honneur.php
PRINCIPAL: 0
TEXT:
Publié
le 11/02/2014 à 16:25
Scarlett Johansson est à l'affiche de Her, de Spike Jonze nommé aux oscars dans la catégorie meilleur film. Crédits photo : Alessandra Tarantino/ASSOCIATED PRESS
L'académie française du cinéma compte remettre une récompense spéciale à l'actrice lors de la cérémonie du 28 février.
Publicité
Les Français sont grands seigneurs. Alors que la star de Lost in Translation a récemment jugé les Parisiens «impolis et grossiers», Scarlett Johansson va recevoir un césar d'honneur lors de la cérémonie du 28 février prochain, qui récompense le meilleur du cinéma français et international. À 29 ans, là voilà déjà récompensée pour l'ensemble de sa carrière. Une consécration.
L'actrice a été révélée à l'âge de 14 ans auprès de Robert Redford dans L'Homme qui murmurait à l'oreille des chevaux, rappelle le président de l'Acade?mie des arts et techniques du ciné?ma dans un communiqué . Son parcours n'est ensuite qu'une série de succès, que ce soit devant la caméra de Sofia Coppola (Lost in Translation), de celle de Brian De Palma (Le Dahlia Noir) ou celle de de Woody Allen (Match Point, Vicky Cristina Barcelona). Son dernier film en date, Her, de Spike Jonze, est nommé cette année dans la catégorie meilleur film aux oscars. Elle prête sa voix à une intelligence artificielle qui vit une romance avec un personnage introverti joué par Joaquin Phoenix .
Résidente parisienne depuis peu
Touche-à-tout au charme puissant, la jeune comédienne incarne également la mystérieuse Veuve Noire dans les blockbusters Disney/Marvel depuis 2010 (The Avengers, Iron Man, Captain America). Grâce à cette carrière presque parfaite, elle a obtenu son étoile sur Hollywood Boulevard à Los Angeles en 2012.
Résidente parisienne depuis peu, elle recevra sa première distinction du cinéma français dans quelques semaines. Après Kevin Costner et Kate Winslet , c'est donc à Scarlett Johansson que reviendra le césar d'honneur remis sur la scène du théâtre du Châtelet.
