TITRE: Syrie: l'�vacuation de civils de Homs suspendue pour mardi  - BFMTV.com
DATE: 2014-02-11
URL: http://www.bfmtv.com/international/syrie-levacuation-civils-homs-suspendue-mardi-708030.html
PRINCIPAL: 164080
TEXT:
r�agir
L' �vacuation des civils assi�g�s dans la ville syrienne de Homs a �t� suspendue pour la journ�e de mardi pour des "raisons logistiques" et devra reprendre mercredi, a d�clar� le gouverneur Talal Barazi. L'op�ration "n'a pas eu lieu aujourd'hui en raison de (difficult�s) logistiques et techniques. Demain matin, nous poursuivrons l'acheminement des aides alimentaires ainsi que l'�vacuation de civils" des quartiers assi�g�s par l'arm�e � Homs, a pr�cis� le gouverneur, joint par t�l�phone.
�� �
L'op�ration d�butera demain matin "� partir de 10 heures locales" (8 heures GMT), a-t-il ajout�, estimant qu'il �tait possible de "prolonger la tr�ve" si cela s'av�rait n�cessaire.
�� �
Selon le gouverneur, "un groupe de civils se pr�pare � sortir demain". "Les cinq quartiers (d'o� les civils sont �vacu�s) sont �loign�s les uns des autres. La situation g�ographique est difficile, nous sommes en train d'assurer les passages ad�quats en supprimant certaines barri�res de sable", a expliqu� le gouverneur.
Depuis vendredi, l'ONU a �vacu� quelque 1.200 personnes en grande majorit� des enfants, des femmes et des hommes �g�s, des quartiers assi�g�s depuis plus de 600 jours par l'arm�e.
Les dossiers de BFMTV
