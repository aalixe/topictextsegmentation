TITRE: PSG, Motta :"Ibrahimovic est sinc�re et tr�s simple" - Goal.com
DATE: 2014-02-11
URL: http://www.goal.com/fr/news/29/ligue-1/2014/02/10/4610214/psg-motta-ibrahimovic-est-sinc%25C3%25A8re-et-tr%25C3%25A8s-simple
PRINCIPAL: 162592
TEXT:
0
10 f�vr. 2014 22:33:00
Thiago Motta, le milieu de terrain du PSG, n'a pas tari d'�loges sur son co�quipier Zlatan Ibrahimovic.
� S�il est bon, l��quipe est bonne. Il est tr�s important en tant que joueur, et il peut apporter beaucoup � l��quipe quand il est en forme. Je le connais depuis de nombreuses ann�es, mais pas beaucoup en dehors du terrain. Nous �tions ensemble un peu de temps � l�Inter, il est s�rieux, sinc�re, et tr�s simple et direct quand il parle. Nous avons un peu un caract�re similaire, je m�entends bien avec lui �, a confi� Thiago Motta au quotidien su�dois Aftonbladet.
Relatifs
