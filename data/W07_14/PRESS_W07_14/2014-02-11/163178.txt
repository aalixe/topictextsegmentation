TITRE: The Mask ou Careto : nouvelle campagne d�espionnage de "certains" Etats ?
DATE: 2014-02-11
URL: http://www.zdnet.fr/actualites/the-mask-ou-careto-nouvelle-campagne-d-espionnage-d-un-etat-39797746.htm
PRINCIPAL: 163095
TEXT:
RSS
The Mask ou Careto : nouvelle campagne d�espionnage de "certains" Etats ?
S�curit� : Les chercheurs de Kaspersky pensent avoir mis � jour une campagne de cyber-espionnage ayant vis� des cibles strat�giques (381 victimes dans 31 pays) et baptis�e � The Mask �. Pour l��diteur, cette campagne pourrait �tre commandit�e par � certains Etats �.
Par Christophe Auffray |
Mardi 11 F�vrier 2014
�
� The Mask, l�une des campagnes de cyber-espionnage les plus avanc�es jamais d�couverte � ce jour �. L�annonce ressemblerait presque � la bande annonce d�un prochain blockbuster am�ricain. Il s�agit plus simplement, mais pas sobrement, de la d�couverte de ce qui selon les experts de Kaspersky s�apparente � une campagne de cyberattaques sophistiqu�es.
D�apr�s les �l�ments collect�s par l��diteur de s�curit�, des op�rations� de cyber-espionnage actives se seraient d�roul�es depuis au moins 2007 et jusqu�en janvier 2014, p�riode depuis laquelle les serveurs de contr�le de Careto sont d�sactiv�s.
Ambassades, labos et activistes cibl�s
Et si Kaspersky parle de cyber-espionnage c�est en raison des cibles bien particuli�res vis�es par ces attaquants, dont la langue, fait inhabituel, serait l�espagnol. L��diteur a r�pertori� plus de 380 � victimes uniques � r�partis sur plus de 1.000 adresses IP et dans 31 pays diff�rents.
� Les principales cibles sont des administrations, des repr�sentations diplomatiques et des� ambassades, des compagnies p�troli�res, gazi�res et �nerg�tiques, des laboratoires de recherche et des activistes � pr�cise Kaspersky.
La nature des �l�ments d�rob�s alimente �galement la th�se du cyber-espionnage. Les auteurs s�emparaient de documents de travail, mais aussi de cl�s de chiffrement, de cl�s SSH, de configurations VPN ou encore de fichiers RDP.
L��diteur qualifie en outre cette op�ration de cyber-espionnage � d�atypique par la complexit� et l�universalit� des outils utilis�s par les criminels �. Les pirates ont notamment eu recours � des programmes malveillants efficaces aussi sur Mac OS X et Linux, � et potentiellement des versions pour Android et iPad/iPhone (iOS). �
Des infections gr�ce � du spear-phishing et de faux sites
Et si l��diteur Russe s�est int�ress� � � The Mask � c�est notamment car ses propres produits de s�curit� �taient vis�s. Pour �viter d��tre d�tect�s, les attaquants exploitaient une faille des logiciels de s�curit� corrig�e plusieurs ann�es auparavant.
Quant au mode d�infection , il est lui � premi�re vue relativement classique. Il repose sur des emails de phishing cibl�s ou spear-phishing. Dans des messages �taient ins�r�s des liens vers un site Web malveillant h�bergeant diff�rents exploits � con�us pour infecter les visiteurs, en fonction de la configuration du syst�me. �
Une fois le visiteur infect�, il �tait redirig� vers le site sain vers lequel le lien int�gr� dans l�email �tait cens� le conduire. Le malware install�, il permettait "d�intercepter tous les canaux de communication et de collecter les informations vitales du syst�me infect�."
Un exploit Zero-Day vendu par VUPEN ?
� Il est extr�mement difficile � d�tecter en raison d�un rootkit furtif. Careto est un syst�me hautement modulaire, qui supporte des plugins et des dossiers de configuration. Cela lui permet d�ex�cuter de nombreuses fonctions � pr�cise encore Kaspersky.
L�analyse r�alis�e par l��diteur l�am�ne � penser que � cette campagne pourrait �tre commandit�e par certains Etats �. Pour �tayer cette hypoth�se, les chercheurs mettent notamment en avant � un tr�s haut degr� de professionnalisme dans les proc�dures op�rationnelles � du groupe de pirates.
Un autre �l�ment pourrait encore renforcer cette hypoth�se. Dans leurs conclusions, mais pas dans le communiqu� de presse, les chercheurs indiquent que les attaques ont utilis� au moins un exploit O-Day.
Particularit� : celui-ci aurait �t� vendu � des gouvernements par une soci�t� fran�aise, VUPEN, qui en 2013 faisait parler d'elle pour la vente d'exploits � la NSA. Une accusation � peine voil�e tr�s mal accueillie par le fondateur de Vupen Chaouki Bekrar.
Our official statement about #Mask : the exploit is not ours, probably it was found by diffing the patch released by Adobe after #Pwn2Own
