TITRE: Intemp�ries. Sept bless�s au Portugal, dont un grave
DATE: 2014-02-11
URL: http://www.ouest-france.fr/intemperies-sept-blesses-au-portugal-dont-un-grave-1921491
PRINCIPAL: 161768
TEXT:
Intemp�ries. Sept bless�s au Portugal, dont un grave
Portugal -
11h42
Les intemp�ries ont aussi frapp� le Portugal. Ils ont fait sept bless�s, dont un grave. 2 250 incidents ont �t� d�compt�s.�|�Epa/MaxPPP
Facebook
Achetez votre journal num�rique
Les intemp�ries ont aussi frapp� le Portugal. Ils ont fait sept bless�s, dont un grave. 2 250 incidents ont �t� d�compt�s.
"Nous avons enregistr� un bless� grave et deux l�gers � Vieira de Leiria (centre) quand leur v�hicule a heurt� un arbre qui �tait tomb� sur la route", a pr�cis� Marco Martins, commandant national adjoint des op�rations de secours de la Protection civile.
2250 incidents d�compt�s
Trois autres bless�s "sans aucune gravit�" ont �t� signal�s � Palmela (sud) apr�s la chute d'un arbre sur leur voiture. Une septi�me personne a �t� bless�e par la chute d'un objet m�tallique � Setubal (sud).�
Au total, les services de secours ont �t� appel�s � intervenir � 2 250 reprises, en raison de chutes d'arbres et d'autres structures, de glissements de terrain ou d'inondations.�
Le derby de Lisbonne annul�
L'ensemble de la c�te portugaise avait �t� plac�e dimanche en alerte rouge, en raison des fortes pluies, de rafales de vent pouvant atteindre 130 km/h et de vagues de 10 m�tres de haut.�
Le derby entre les deux clubs de football rivaux de Lisbonne, le Benfica et le Sporting, a �t� annul� in extremis au moment o� des d�bris s'�taient d�tach�s du toit du stade de la Luz, endommag� par le vent. Les 50 000 spectateurs ont d� �tre �vacu�s et la rencontre a �t� report�e � mardi.
Tags :
