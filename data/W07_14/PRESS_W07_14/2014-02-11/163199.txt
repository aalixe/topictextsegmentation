TITRE: Mort de l'actrice Shirley Temple | Site mobile Le Point
DATE: 2014-02-11
URL: http://www.lepoint.fr/cinema/mort-de-l-actrice-shirley-temple-11-02-2014-1790496_35.php
PRINCIPAL: 0
TEXT:
11/02/14 à 12h22
Mort de l'actrice Shirley Temple
VIDÉO. L'inoubliable enfant-star, icône de l'Amérique des années 1930 et 1940, s'est éteinte à l'âge de 85 ans, selon la BBC.
L'actrice Shirley Temple, ici enfant, sur une photo non datée. AFP
Le Point.fr
L'actrice Shirley Temple est décédée à l'âge de 85 ans. Première enfant-star à avoir connu une renommée internationale, elle fut une véritable icône dans l'Amérique des années 30 et 40. Précoce, elle commence à 3 ans, puis enchaîne les rôles : plus de 40 durant les années 30. En 1935, elle reçoit un oscar "spécial jeunesse" : elle a 6 ans (!) et figure toujours pour cette raison dans le livre Guinness des records.
À 20 ans, Shirley Temple se sent trop vieille pour Hollywood. Elle renonce au cinéma et commence une carrière politique au Parti républicain, puis devient diplomate. Richard Nixon en fait la déléguée des États-Unis à l'ONU. Elle sera ambassadrice au Ghana entre 1974 et 1976, puis en Tchécoslovaquie de 1989 à 1992, alors que le mur de Berlin tombe.
REGARDEZ Shirley Temple enfant, interprétant "When I Grow Up" :
