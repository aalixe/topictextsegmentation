TITRE: Cannes s'offre Montpellier (1-0) ! - Goal.com
DATE: 2014-02-11
URL: http://www.goal.com/fr/news/1727/coupe-de-france/2014/02/11/4612620/cannes-soffre-montpellier-1-0
PRINCIPAL: 164861
TEXT:
0
11 f�vr. 2014 21:08:00
Montpellier s'est inclin� � Cannes (1-0), ce mardi, en Huiti�me de finale de la Coupe de France.
Apr�s avoir �limin� Saint-Etienne, l'AS Cannes s'est offert un autre pensionnaire de Ligue 1 en faisant chuter le Montpellier HSC , ce mardi, pour valider son tickert pour les Quarts de finale de la Coupe de France. Malgr� trois divisions d'�cart, les hommes de Rolland Courbis, en panne de r�alisme, ne sont pas parvenus � prendre l'avantage. Sup�rieurs dans la ma�trise, les Montpelli�rains ont but� sur la d�fense cannoise. En prolongations, les locaux sont finalement parvenus � faire la diff�rence juste avant les tirs aux buts sur une frappe crois�e de Zobiri (1-0, 119e) ! Le but du K.O. pour le MHSC, qui avait pourtant �limin� le PSG au tour pr�c�dent. Le charme de la Coupe a encore parl�.
Relatifs
