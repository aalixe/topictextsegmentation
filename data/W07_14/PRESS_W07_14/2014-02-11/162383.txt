TITRE: Les loyers ont progress� de 0,6% en 2013, d'apr�s Century 21
DATE: 2014-02-11
URL: http://www.boursier.com/actualites/economie/les-loyers-ont-progresse-de-0-6-en-2013-d-apres-century-21-22967.html
PRINCIPAL: 0
TEXT:
Les loyers ont progress� de 0,6% en 2013, d'apr�s Century 21
Abonnez-vous pour
Cr�dit photo ��Reuters
(Boursier.com) � Les loyers ont progress� de seulement 0,6% en 2013 (12,05 euros le m�tre carr�) dans le�r�seau d'agences immobili�res Century 21 (filiale de Nexity avec 900 agences en France. Un chiffre qui est globalement en ligne avec le niveau de l'inflation (+0,7%). Le loyer des studios g�r�s par Century 21 (qui repr�sentent 25,4% des nouvelles locations) a progress� de 0,6%. Celui des 2 pi�ces, qui repr�sentent pr�s de 40% du march�, ont �volu� de +0,8%. Les 3 pi�ces ont augment� de 0,4%, soit une baisse relative � l'inflation de 0,3%. La hausse du loyer des 4 pi�ces atteint par contre +3,2% et celle du les loyers des maisons s'�l�ve � 3,9%.
A Paris, Century 21 mesure un prix moyen des loyers au m�tre carr� en retrait de 0,7%, portant ainsi � -1,4% la baisse nette relative � l'inflation (de 26,8 euros � 26,6 euros le m�tre carr�). Le r�seau d'agences immobili�res observe aussi ce ph�nom�ne de mod�ration des loyers en Ile-de-France et dans les grandes villes comme Lyon et Marseille.
D�sint�r�t pour l'investissement locatif
Globalement, Century 21 souligne que le march� s'autor�gule, les locations nouvelles pr�sentant des loyers plus mod�r�s, tandis que sur les baux en cours, l'application de l'indice de r�f�rence des loyers IRL n'ayant pas �t� effectu�e.
Century 21 continue par ailleurs d'observer que les cadres sup�rieurs et les professions lib�rales se d�sint�ressent de l'investissement locatif ancien, ainsi que les commer�ants et artisans. Pour la premi�re fois depuis 2011, la part des employ�s et ouvriers parmi les investisseurs est sup�rieure � celle des cadres moyens. Dans le r�seau Century 21, les employ�s et ouvriers repr�senteraient d�sormais 18,5% des nouveaux bailleurs...
Olivier Cheilan � �2014, Boursier.com
