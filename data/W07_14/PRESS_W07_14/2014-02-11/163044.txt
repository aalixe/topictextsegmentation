TITRE: L1 / LYON - Lyon : Garde a insist� pour Gourcuff
DATE: 2014-02-11
URL: http://www.football365.fr/france/infos-clubs/lyon/lyon-garde-a-insiste-pour-gourcuff-1102168.shtml
PRINCIPAL: 163041
TEXT:
L1 / LYON - Publi� le 10/02/2014 � 18h31
1
Lyon : Garde a insist� pour Gourcuff
Yoann Gourcuff a effectu� son retour � la comp�tition face � Nantes apr�s avoir �t� touch� aux adducteurs contre Evian.  Malgr� des douleurs persistantes, R�mi Garde, l'entra�neur de l'OL, a tenu � le faire jouer.
R�mi Garde a jou� avec le feu. Face � Nantes, un match capital pour une place europ�enne, l�entra�neur lyonnais a pris le risque de titulariser Yoann Gourcuff. Le milieu de terrain international, auteur de prestations convaincantes depuis plusieurs mois, a �t�, pour le coup, tr�s d�cevant face aux Canaris malgr� la victoire de l�OL (2-1) . Mais �tait-il vraiment en �tat de montrer la pleine mesure de son talent ?
Une heure de jeu et puis s'en va
A priori non comme l�a confirm� son entra�neur. � Je savais qu�il n��tait pas totalement pr�t, a avou� R�mi Garde dans les colonnes du Progr�s. Il n�avait pas beaucoup de rythme dans les jambes, mais malgr� tout quand l��quipe avait le ballon, il a �t� tr�s pr�cieux. � G�n� par des douleurs aux adducteurs, l�ancien Milanais est finalement sorti � l�heure de jeu.
R�dig� par R�daction Football365 Suivre @toto
Plus d'Actualit�s
