TITRE: Abus de faiblesse de Catherine Breillat | Vanity Fair
DATE: 2014-02-11
URL: http://www.vanityfair.fr/culture/cinema/articles/3-raisons-de-voir-abus-de-faiblesse-de-catherine-breillat/12992
PRINCIPAL: 162762
TEXT:
MIS � JOUR LE 25/02/2014 � 15:29 | PUBLI� LE 11/02/2014 � 08:22
3 raisons de voir...
�Abus de faiblesse�, de Catherine Breillat
Chaque semaine, Vanity Fair fait le tour des salles de cin�ma pour vous conseiller un film. En trois points, ni plus ni moins.
Par �lisa James
UN FILM QUI A VALEUR D�EXORCISME
Catherine Breillat, sulfureuse et passionnante r�alisatrice de Romance ou d'Anatomie de l'enfer, revient sur l�attraction qu�exer�a sur elle l�arnaqueur professionnel Christophe Rocancourt qui, engag� pour un projet de film avort�, lui extorqua 800.000 euros, la laissant sur la paille. Breillat adapte le roman �ponyme qu�elle a tir� de cette histoire et se projette dans le r�le de Maud (Isabelle Huppert), cin�aste victime d�un AVC � comme elle - qui rep�re Vilko (Kool Shen), une nuit � la t�l�vision dans un talk-show. La force d�attraction de Vilko fait le reste : entre eux, s�instaure un curieux rapport compos� de s�duction, de manipulation et de d�pendance.�
NEVER EXPLAIN, NEVER COMPLAIN
�
Breillat cueille Maud d�s son AVC, dans sa chambre d�abord, puis � l�h�pital, o�, bouche d�form�e et corps insensible, elle lutte contre l�h�mipl�gie : sc�nes casse-gueules film�es avec une froideur clinique dont Isabelle Huppert se sort avec maestria. Puis Breillat entre dans le sujet � la possession et la d�possession de Maud par Vilko - en �tablissant leur relation faite de vampirisation et de perversit�. Breillat ne se donne pas le beau r�le (Maud, capable de r�veiller son premier assistant la nuit, est �go�ste), �ne r�gle pas ses comptes, ne s�appuie pas non plus sur la b�quille de la psychologie : elle se contente de montrer : le m�pris de classe de Maud, le culot et l�arrogance de Vilko qui s�offre le luxe de s�approcher d�elle avec un post-it marqu� escroc sur le front...?Toute la puissance du film tient dans cet axiome simple : Breillat n�expliquera jamais pourquoi Vilko fascine tant Maud, femme intelligente � demi-morte (un grand nombre de sc�ne la montrent � moiti� endormie au lit). Ce myst�re, servi par une mise en sc�ne tenue, rend ce film, qui oscille entre le drame et la farce, � la fois puissant et singulier.�
�
LE CHOC DE DEUX CORPS
�
Celui, �maci� et fragile d�Isabelle Huppert (au sommet de son art), absolument r�gnante. Mais aussi celui d�un rappeur : Kool Shen qui, apr�s JoeyStarr, fait ses d�buts au cin�ma. Qu�il porte Maud dans ses bras ou escalade la biblioth�que, il impose dans un premier r�le difficile sa pr�sence v�n�neuse. Mais reste un poil trop monocorde pour d�cha�ner l�enthousiasme.�
�
