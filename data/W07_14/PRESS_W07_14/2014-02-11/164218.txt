TITRE: Mercato � Chelsea : Le projet du PSG ne laisse pas Hazard insensible
DATE: 2014-02-11
URL: http://www.foot-sur7.fr/130198-mercato-chelsea-le-projet-du-psg-ne-laisse-pas-hazard-insensible
PRINCIPAL: 164215
TEXT:
Mercato � Chelsea : Le projet du PSG ne laisse pas Hazard insensible
Auteur : Gary SLM / Publi� le : 11 f�vrier 2014
Hazard rejoindra-t-il le PSG cet �t� comme cela avait �t� annonc� par plusieurs m�dias lors du r�cent mercato hivernal ?
Cette question, le journal Le Parisien, par son journaliste Laurent Perrin, y a apport� une r�ponse un peu sp�ciale.
Pour ne pas se mouiller, le confr�re confie en ce qui concerne la venue d� �den Hazard ��C�est r�ellement possible d'un point de vue financier.��
Toujours d�apr�s lui, ��Hazard garde un oeil sur Paris, le projet ne le laisse pas insensible.��
La v�rit� : Hazard est encore tr�s loin du PSG
Seulement, ni Jos� Mourinho ni Roman Abramovitch n�accepteront de laisser partir �den Hazard de Chelsea.� Laurent Perrin rajoute alors ��Hazard prend une telle envergure � Chelsea que Mourinho ne le l�chera jamais. Selon moi, il a 1% de chances de venir � Paris d�s cet �t�. 99% de chances de porter le maillot parisien un jour.��
Cette r�ponse, elle peut aussi �tre appliqu�e � Lionel Messi, Cristiano Ronaldo et aux prochaines stars du ballon rond, car sur le plan financier, le PSG peut effectivement s�offrir tout ce qui sera � vendre.
En clair, Hazard n�est pas pr�s de jouer au Paris Saint-Germain...!
5 0 vote(s)
