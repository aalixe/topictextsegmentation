TITRE: Corbeil-Essonnes : Serge Dassault demande la lev�e de sa propre immunit� parlementaire � metronews
DATE: 2014-02-11
URL: http://www.metronews.fr/paris/corbeil-essonnes-serge-dassault-demande-la-levee-de-sa-propre-immunite-parlementaire/mnbj!NfNMXGX4k9fcI/
PRINCIPAL: 162219
TEXT:
Mis � jour  : 11-02-2014 06:44
- Cr�� : 10-02-2014 20:53
Dassault demande la lev�e de sa propre immunit� parlementaire
ESSONNE - Le s�nateur UMP Serge Dassault a demand� lui-m�me lundi la lev�e de son immunit� parlementaire. Il a pris les devants alors que le bureau du S�nat devait � nouveau se prononcer mercredi. Il pourrait �tre plac� en garde � vue.
�
Le s�nateur UMP Serge Dassault affirme n'avoir rien � se reprocher. Photo :�JDD/GILLES BASSIGNAC/SIPA
Plut�t agir que subir. Le s�nateur UMP Serge Dassault a demand� lundi la lev�e de sa propre immunit� parlementaire, demand�e � plusieurs reprises par la justice dans une enqu�te sur l'achat pr�sum� de votes lors des municipales � Corbeil-Essonnes, et alors que le bureau du S�nat s'appr�tait � prendre une d�cision dans ce sens mercredi.
"Je demande la lev�e de mon immunit� parlementaire", a annonc� � l'AFP l'industriel et propri�taire du Figaro, expliquant vouloir ainsi d�montrer par cette d�marche exceptionnelle qu'il n'avait "rien � (se) reprocher".� Mercredi, le bureau du S�nat, qui doit se prononcer sur une nouvelle demande de lev�e de l'immunit� parlementaire de Serge Dassault, devrait cette fois y r�pondre favorablement, selon plusieurs sources parlementaires.
"Pr�t � affronter cette �preuve"
Avec la d�cision de Serge Dassault, tout suspense est lev�.�Cela permettra aux magistrats du p�le financier de Paris, Serge Tournaire et Guillaume Da�eff, de placer le s�nateur de 88 ans en garde � vue.�"M�me si cette lev�e d'immunit� provoque mon placement en garde � vue, je suis pr�t � affronter cette �preuve", �crit l'ancien maire de Corbeil dans un communiqu�.�"Je pourrai de ce fait avoir acc�s � la proc�dure" et "pouvoir me d�fendre contre ces accusations". "Je pourrai d�montrer ma totale innocence de ces soi-disant achats de votes, accusations invent�es de toutes pi�ces par certains de mes adversaires politiques", ajoute-t-il.
Par deux fois, l'ancien maire de Corbeil-Essonnes (Essonne) avait �chapp� de justesse�� la lev�e de son immunit�.�La derni�re, le 8�janvier, avait d�clench� un toll� , si bien que le pr�sident de la Haute assembl�e, Jean-Pierre Bel (PS), avait d�cid� de changer le mode de vote : vote � main lev�e au lieu de vote � bulletin secret.
Fran�ois Milho
