TITRE: Bond de 41% de la consommation d'or en Chine en 2013 | La-Croix.com
DATE: 2014-02-11
URL: http://www.la-croix.com/Actualite/Economie-Entreprises/Economie/Bond-de-41-de-la-consommation-d-or-en-Chine-en-2013-2014-02-11-1104644
PRINCIPAL: 163739
TEXT:
Bond de 41% de la consommation d'or en Chine en 2013
11/2/14 - 10 H 30
- Mis � jour le 11/2/14 - 11 H 40
Mots-cl�s :
Dans la vitrine d'un orf�vre, � Hefei, en Chine le 18 d�cembre 2010
AFP/Archives
Dans la vitrine d'un orf�vre, � Hefei, en Chine le 18 d�cembre 2010
Avec cet article
� Les jeunes indiennes actives de la classe moyenne changent l�Inde et sa soci�t� �
La consommation d'or en Chine a bondi de 41,36% en 2013, selon des chiffres du secteur montrant que le pays a probablement ravi � l'Inde la premi�re place mondiale, a rapport� la presse mardi.
L'an dernier la demande chinoise a atteint 1.176 tonnes du m�tal pr�cieux, selon l'institut supervisant le commerce de l'or dans la deuxi�me �conomie mondiale.
Cette forte hausse a �t� notamment aliment�e par les achats des femmes chinoises souhaitant profiter de la baisse enregistr�e par le cours de l'or pour faire des affaires, a expliqu� l'agence officielle Chine nouvelle.
Les mesures gouvernementales pour limiter les importations d'or en Inde ont �galement jou�.
Dans le but de r�duire l'�norme d�ficit ext�rieur du pays, le gouvernement indien a en effet pris plusieurs mesures pour limiter les entr�es d'or dans le pays, provoquant une chute de la consommation indienne de m�tal jaune (-52% entre le deuxi�me et le troisi�me trimestre, selon le Conseil mondial de l'or).
La forte augmentation de la demande chinoise a donc en partie compens� la fuite des investisseurs et l'affaiblissement de la demande indienne.
AFP
