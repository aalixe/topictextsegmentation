TITRE: Atos : Des �quipes associ�es au projet Curiosity sur Mars - Trading Sat
DATE: 2014-02-11
URL: http://www.tradingsat.com/atos-FR0000051732/actualites/atos-atos-des-equipes-associees-au-projet-curiosity-sur-mars-504633.html
PRINCIPAL: 163335
TEXT:
Atos : Des �quipes associ�es au projet Curiosity sur Mars
mardi 11 f�vrier 2014 � 11h13
Tweet
(CercleFinance.com) - Atos a annonc� mardi participer aux c�t�s du Centre national d'�tudes spatiales (CNES) � la programmation d'instruments pour Curiosity, le robot de la Nasa qui explore la surface de Mars.
Sur les 10 �quipements scientifiques du 'rover', deux sont r�alis�s sous ma�trise d'ouvrage du CNES, � savoir le ChemCam - qui analyse la composition des roches par tir laser - et Sam , un ensemble d'instruments pour l'�tude d'�chantillons des roches.
Les experts d' Atos , int�gr�s aux �quipes du CNES, sont charg�s d'assurer quotidiennement la programmation et la surveillance des instruments en vol en fonction des plans d'exploration d�finis par les scientifiques du projet.
Curiosity explore le sol de la plan�te rouge depuis 18 mois afin de trouver des preuves d'environnements pr�sents et pass�s favorables � la vie, avec d�j� une moisson de d�couvertes � son actif.
L'outil ChemCam a par exemple permis de mettre en �vidence des veines de gypse sur le site de Yellowknife Bay, une pr�sence permettant de d�montrer que de l'eau liquide a circul� sur Mars dans le pass�.
En ce qui concerne Sam , les donn�es g�n�r�es ont permis de d�tecter la pr�sence de mati�re organique (carbone, hydrog�ne, oxyg�ne, azote, phosphore, soufre) n�cessaire � la vie et � l'habitabilit� de la plan�te Mars.
Atos rappelle avoir particip� au cours des vingt derni�res ann�es � plus de 50 programmes spatiaux et collabor� � de nombreux projets pour le CNES et les principaux acteurs industriels du secteur a�ronautique (ESA, Airbus , Thales Alenia Space).
Copyright (c) 2014 CercleFinance.com. Tous droits r�serv�s.
Vous suivez cette action ?Recevez toutes ses infos en temps r�el :
