TITRE: Barom�tre de l'accessibilit� 2013 : Caen prend la 3e place - France 3 Basse-Normandie
DATE: 2014-02-11
URL: http://basse-normandie.france3.fr/2014/02/11/barometre-de-l-accessibilite-2013-caen-prend-la-3e-place-413123.html
PRINCIPAL: 163679
TEXT:
Les d�tails de l'enqu�te de l'APF sur l'accessibilit� : questionnaire, bar�me de notation et analyse compl�te
Quel chemin jusqu'� l'�ch�ance de 2015 ?
Ce barom�tre a pour but d'�valuer le chemin restant � parcourir pour atteindre les objectifs fix�s pour 2015 par la loi handicap de 2005. Pr�s de 10 millions de citoyens sont concern�s par le handicap, rappelle l'association en soulignant qu'il faut y ajouter toutes les personnes � mobilit� r�duite (personnes��g�es, femmes enceintes, parents avec poussettes, bless�s temporaires). Si l'association salue dans son rapport une hausse de la moyenne nationale, elle la juge tr�s nettement insuffisante :�"Une moyenne de 14,14 � quelques mois de l��ch�ance finale est un grave �chec de la politique de mise en accessibilit� de la France" .
Ainsi, elle souligne qu'"un tiers des chefs-lieux d�partementaux n'ont m�me pas la moyenne pour l'accessibilit� de leurs �quipements municipaux."� L'association estime ensuite qu'� peine plus de la moiti� des �coles et seulement 42% des r�seaux de bus sont accessibles. Les commerces de proximit�, ainsi que les cabinets m�dicaux et param�dicaux sont �galement loin du compte.
Un enjeu pour les �lections municipales de 2014
L'APF profite de la publication de ce barom�tre et de constat qu'elle juge "accablant" pour interpeller les candidats aux prochaines municipales. Acc�s aux r�unions publiques, aux administrations mais aussi au logement pour les personnes en handicap, elle a dress� une liste de points essentiels selon elle sur lesquels elle invite les candidats � se prononcer.
