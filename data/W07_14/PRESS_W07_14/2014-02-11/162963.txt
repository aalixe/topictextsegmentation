TITRE: Le Bitcoin est maintenant ill�gal en Russie
DATE: 2014-02-11
URL: http://www.journaldugeek.com/2014/02/11/le-bitcoin-est-maintenant-illegal-en-russie/
PRINCIPAL: 162962
TEXT:
totoPasBeau
12 f�v, 2014, 04:07 #23
@lemmings87, Selon moi, la chute de cette semaine est surtout due aux probl�mes que rencontrent les gens sur la plateforme de mt gox. Plateforme ayant toujours des probl�mes de transferts de btc et de cash et qui pourtant �changeait encore jusqu�� il y a quelques jours des bitcoins � pr�s de 150$ de plus que les autres plateformes de changes (bitstamp/btc-e). Personnellement, ce genre de probl�me �tait � pr�voir avec mt gox (suffit de voir les histoires qui ont d�j� eu lieu). Les gens ont voulu jouer�
Russie ou pas, ce n�est pas �a qui devrait plomber le bitcoin. On l�a vue avec la chine, le cour a paniqu� mais s�est vite relev�. Les chinois s�en servent m�me toujours.
Qu�il en d�plaise � certains qui qualifient le btc de ponzi/march� uniquement sp�culatif, on peut faire et acqu�rir bien des choses avec. Certes, on est loin d�une utilisation quotidienne par et pour tout le monde. Mais il ne suffit aux gens que d�y croire, un peu de temps et du courage. Cette monnaie a certes des ��probl�mes�� (que certains altcoins proposent de combler) mais elle pr�sente bien des avantages face aux monnaies fiduciaires classiques. Monnaie globale, pas de presse � billets qui tournent continuellement et qui plombent les valeurs (regardez la couronne islandaise ou encore le $US dont pr�s de 85 Milliards de dollars sont imprim�s � partir de rien par mois gr�ce aux quantitative easing. �a ne peut pas �tre bon pour un pays), frais de transferts infimes face aux frais bancaires etc.
Bref, comme dis un peu partout, le btc et autres altcoins (tel que les ltc, doge, meow, aur etc) ne sont peut-�tre pas de ��v�ritables�� monnaies (quoique), et ont leurs propres probl�mes/limites. Mais elles n�en restent pas moins des alternatives int�ressantes � toutes ces monnaies communes qu�on imprime � tout va pour essayer (sur le papier) de tenir droit (comme la tour de Pise cqfd) une �conomie bas�e sur la dette et l�appauvrissement (r�el) continue des populations concern�es.
Bref, il est toujours int�ressant de se renseigner sur les alternatives existantes � toutes choses. Le btc en est une, et d�autres sont des alternatives au btc. Se renseigner ne coute rien. Mais ce n�est pas en lisent deux trois avis vulgaris�s (qui peuvent n�anmoins �tres int�ressants au passage) que l�on peut cerner ce qu�une chose est. Les altcoins ne sont pas tous pourris, et l�or n�est pas une valeur si refuge que �a.
My two cents (ou 0.00003btc)
0
