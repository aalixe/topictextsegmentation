TITRE: Stade Toulousain -  XV de France : le clash ! - 11/02/2014 - LaD�p�che.fr
DATE: 2014-02-11
URL: http://www.ladepeche.fr/article/2014/02/11/1815544-stade-toulousain-xv-de-france-le-clash.html
PRINCIPAL: 162030
TEXT:
Stade Toulousain -  XV de France : le clash !
Publi� le 11/02/2014 � 03:52
,
Mis � jour le 11/02/2014 � 08:25
| 17
top 14. Clubs - Bleus. Toulouse part en guerre contre le staff de l'�quipe de France qui veut pr�server quatre joueurs ce week-end.
Philippe Saint-Andr� r�ve de concertation. / Photo AFP
En voil� une crise comme seul le rugby sait nous les fabriquer. M�me si Philippe Saint-Andr� a essay� de d�samorcer la bombe en communiquant a minima et en �voquant � plusieurs reprises le mot �concertation�.
Derri�re l'annexe de la convention
Au bout du fil de la dynamite, on trouve le paragraphe (b) de l�article 3.1.3 de l�annexe de la convention sign�e mi-d�cembre entre la F�d�ration et la Ligue. Il stipule que �les joueurs seront remis � la disposition de leur club pour les 19e et 21e journ�es du Top 14� et que �une concertation aura lieu entre l�encadrement technique du club et celui de l��quipe de France. Les joueurs seront autoris�s � jouer sauf avis contraire du staff du XV de France motiv� par la situation individuelle du ou des joueurs concern�s.� Derri�re le flou de cette phrase, on devine que c�est le staff du XV de France qui a la dernier mot au bout de la concertation�
Hier matin, Philippe Saint-Andr� a reconnu vouloir �conserver les joueurs qui ont d�but� les matches contre l�Angleterre et l�Italie. Ils sont donc douze dans ce cas : 4 Toulousains (Nyanga, Picamoles, Doussain, Huget), 2 Clermontois (Domingo, Fofana), 1 Montpelli�rain (Mas), 1 Toulonnais (Bastareaud), 1 Castrais (Dulin), 2 du Stade Fran�ais (Pap� et Plisson) et 1 du Racing-M�tro (Le Roux).
Maestri n'a jou�  que trois minutes de moins  que Doussain
Philippe Saint-Andr� a donc choisi le crit�re des deux titularisations. Au passage, nous remarquerons que ce n�est pas forc�ment objectif puisque Jean-Marc Doussain, deux fois titulaire, a jou� 119 minutes quand Yoann Maestri, titulaire une fois, a jou� 116 minutes, soit � peine trois minutes de moins. Mais selon le s�lectionneur national, le premier devrait �tre �conserv� - entendez par-l� pr�serv� - mais pas le second qui pourrait donc jouer � Biarritz, ce week-end, si Guy Nov�s le souhaite.
Mais ce qui r�volte aujourd�hui Ren� Bouscatel, le pr�sident du Stade Toulousain, ce sont ces deux nouveaux doublons (la 21�me journ�e du Top 14 est �galement concern�e) et ce nouvel imp�t � payer � l��quipe de France.
Il est libre Ren�
Le pr�sident a clairement choisi d�en faire un dossier de pr�sident ne souhaitant pas m�ler Guy Nov�s � cette pol�mique : �On n�a rien dit au staff sportif qui n�a pas d�l�gation pour d�cider si les salari�s du club doivent travailler samedi prochain en jouant ou pas. L�employeur, c�est la SASP et non le staff sportif qui n�a aucune d�l�gation pour le faire.�
Ren� Bouscatel rappelle dans l�entretien ci-dessous qu�il a d�missionn� de la Ligue au mois de d�cembre parce qu�il ne voulait pas signer cette convention. Aujourd�hui, libre de ses mouvements, il part en guerre. Esp�rant le soutien d�autres clubs. Qui a d�but� hier du c�t� de Clermont avec le manager Jean-Marc Lhermet : �Ce n�est pas forc�ment ce qu�on avait compris. Le choix des joueurs n�a pas vraiment fait l�objet d�une concertation. Globalement, on n�a pas discut� de qui pouvait jouer ou non. On nous a dit : �On veut garder Domingo et Fofana. Qu�est-ce que vous en pensez ? Il se trouve qu�avec notre effectif, ce n�est pas probl�matique en ce moment. Mais si �a avait �t� probl�matique, on n�aurait pas l�ch� nos joueurs.�
Exactement ce que le Stade Toulousain entend faire. D�s lors, Philippe Saint-Andr� qui souhaitait �une concertation entre gentlemen� va maintenant devoir mettre le casque lourd s�il veut aller au bout de ses id�es.
Philippe Lauga
