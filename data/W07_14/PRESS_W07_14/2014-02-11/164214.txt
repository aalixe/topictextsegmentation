TITRE: Coupe de France : Lille-Caen, Cannes-Montpellier et Angers-CA Bastia mardi - RTL.fr
DATE: 2014-02-11
URL: http://www.rtl.fr/actualites/sport/football/article/coupe-de-france-lille-caen-cannes-montpellier-et-angers-ca-bastia-mardi-7769642789
PRINCIPAL: 164212
TEXT:
Le troph�e de la Coupe de France
Cr�dit : AFP/J.Saget
PR�SENTATION - Les 8es de finale de la Coupe de France, sans le PSG ni l'OM, d�butent mardi 11 f�vrier avec trois rencontres (18h30, 20h et 20h45).
Lille et Montpellier sont les deux premiers des sept clubs de Ligue 1 encore en lice en Coupe de France sur le pont mardi 11 f�vrier, respectivement contre Caen (L2) et � Cannes (CFA). Le troisi�me match de ce mardi opposera deux clubs de Ligue 2, Angers le 2e qui re�oit le dernier, le CA Bastia, avec donc l'assurance de voir un club de L2 disputer les quarts de finale.�
Le seul choc entre �quipes de l'�lite aura lieu mercredi, Nice recevant Monaco � l'Allianz-Riviera. Les autres rencontres opposeront Guingamp � l'�le Rousse (CFA2), Rennes � Auxerre (L2) et Moulins (CFA) � S�te (CFA2), tandis que jeudi Lyon recevra Lens� (L2) en cl�ture de ce tour.
Lille et Montpellier veulent confirmer son renouveau
Apr�s sa premi�re victoire de l�ann�e en championnat face � Sochaux (2-0) , Lille doit confirmer son renouveau en 2014. Pour ce faire les hommes de Ren� Girard affrontent Caen (20h45), tout juste d�fait � Metz et actuel 6e de Ligue 2. M�me si pr�server la 3e place en championnat reste l�objectif prioritaire des Dogues, la Coupe de France, ch�re aux clubs populaires dont le Losc se r�clament, reste �galement un moyen d�acc�der � l�Europe.
Pour Montpellier, la priorit� aussi va au Championnat, car il s'agit pour les H�raultais � la peine cette saison de se maintenir dans l'�lite. Si l'arriv�e de Rolland Courbis a co�ncid� avec un d�but d'embellie, les Montpelli�rains �tant 14e � sept longueurs du premier rel�gable, la prudence devra �tre de mise chez des Cannois chasseurs de t�te qui ont notamment fait tomber Saint-Etienne en 32e de finale.
La r�daction vous recommande
