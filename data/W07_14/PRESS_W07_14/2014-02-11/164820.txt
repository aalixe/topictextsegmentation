TITRE: France/Monde | Découverte d�??une jambe humaine flottant sur la Seine
DATE: 2014-02-11
URL: http://www.dna.fr/actualite/2014/02/11/decouverte-d-une-jambe-humaine-flottant-sur-la-seine
PRINCIPAL: 164817
TEXT:
Découverte d�??une jambe humaine flottant sur la Seine
- Publié le 11/02/2014
HAUTS-DE-SEINE Découverte d�??une jambe humaine flottant sur la Seine
Une jambe humaine en état de décomposition avancée dérivant sur la Seine a été découverte mardi après-midi à Saint-Cloud (Hauts-de-Seine) par des passants, a-t-on appris de sources policières, confirmant une information de BFMTV.
Peu avant 16 h, trois passants ont aperçu une jambe humaine flottant sur la Seine, à hauteur du 10-25 quai du président Carnot et ont prévenu la police.
«Les policiers du commissariat de Saint-Cloud ont fait appel à la brigade fluviale de la préfecture de police de Paris dans le but de rechercher le reste du corps, mais en vain», a expliqué une source.
Selon les premiers éléments de l�??enquête, le membre appartient à une personne à la peau blanche, dont le sexe et l�??âge ne sont pas encore connus.
Crime, accident ou suicide... les policiers du commissariat de Saint-Cloud saisis de l�??enquête n�??excluent pour l�??heure aucune piste. «Les hélices des bateaux et péniches ont pu mutiler le corps», précise une autre source.
Des prélèvements ADN vont être réalisés sur le membre aux fins d�??identification. Les enquêteurs vont également effectuer des recherches afin d�??établir si cette découverte est liée à un cas de disparition de personne sur le secteur.
AFP
Poster un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
