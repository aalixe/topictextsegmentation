TITRE: �P�kin Express� : plus de 50 membres de l'�mission interpell�s en Inde -  En bref - T�l�vision - T�l�rama.fr
DATE: 2014-02-11
URL: http://television.telerama.fr/television/pekin-express-plus-de-50-membres-de-l-emission-interpelles-en-inde,108468.php
PRINCIPAL: 161938
TEXT:
-
Mis � jour le 10/02/2014 � 14h26
A chaque �dition, sa m�saventure. Apr�s l'accident du pr�sentateur St�phane Rotenberg aux Philippines en 2012 et la tentative de braquage de deux candidats l'ann�e derni�re � Cuba, le jeu P�kin Express (M6) a encore une fois connu un p�pin lors de son tournage.
L'incident est survenu ce week-end dans l'Etat du Bengale occidental, en Inde, alors que la cha�ne M6 y r�alise la dixi�me saison du jeu d'aventures, qui doit relier la Birmanie au Sri Lanka. Intrigu�e par la pr�sence de 22 t�l�phones satellitaires dans cette r�gion sensible, proche des fronti�res du Bangladesh, du N�pal et du Bhoutan et connue pour abriter des groupes rebelles, la police indienne a men� une op�ration dans l'h�tel o� se trouvaient leurs propri�taires. Selon le Times of India , elle a arr�t� 51 personnes, avant de les interroger. Toutes se sont r�v�l�es �tre des membres de l'�mission. Aujourd'hui, les � dangereux rebelles � ont �t� rel�ch�s mais trois techniciens semblent toujours d�tenus. Cette petite m�saventure pourrait contraindre la production � modifier le parcours de ce P�kin Express.
.
