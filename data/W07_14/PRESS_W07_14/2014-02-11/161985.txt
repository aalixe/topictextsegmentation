TITRE: Fil Info | Deux magasins sauvés, quatre condamnés
DATE: 2014-02-11
URL: http://www.lalsace.fr/actualite/2014/02/10/deux-magasins-sauves-quatre-condamnes
PRINCIPAL: 161983
TEXT:
Deux magasins sauvés, quatre condamnés
événement
- Publié le 10/02/2014
LIBRAIRIES CHAPITRE Deux magasins sauvés, quatre condamnés
Les librairies Chapitre de Colmar, place de la Cathédrale, et Saint-Louis ont été sauvées ce lundi soir. Le tribunal de commerce de Paris a validé la proposition de reprise de ces deux enseignes. Les quatre autres librairies haut-rhinoises ferment définitivement.
A Saint-Louis, le repreneur conservera les 13 salariés en CDI et compte reconstituer au plus vite les stocks qui avaient fondu ces dernières semaines, mais également créer un espace jeunesse.
A Colmar, la librairie Ruc de la place de la Cathédrale gardera les deux activités librairie et papeterie, avec doublement volume librairie, selon le nouveau propriétaire. Là aussi, l�??intégralité du personnel, à savoir 13 salariés, est conservé. 
Ce n�??est pas le cas dans les autres librairies Chapitre. Ainsi sont condamnées les enseignes d�??Altkirch, de Colmar, rue des Têtes, Guebwiller et Mulhouse. En terme humain, cela fait 34 employés sur le carreau. 
Plus de précisions dans nos éditions de demain
Vos commentaires
