TITRE: Rennes : Avec Pitroipa, Sans Alessandrini - Coupe de France - Football - Sport.fr
DATE: 2014-02-11
URL: http://www.sport.fr/football/coupe-de-france-rennes-avec-pitroipa-sans-alessandrini-339450.shtm
PRINCIPAL: 164379
TEXT:
Rennes : Avec Pitroipa, Sans Alessandrini
Mardi 11 f�vrier 2014 - 18:22
Le Stade Rennais , qui se d�place � Auxerre mercredi (18h45) � l?occasion des huiti�mes de finale de Coupe de France , sera priv� de Romain Alessandrini , bless� � la cheville. Philippe Montanier devra �galement toujours se passer de Jean-Armel Kana-Biyik (cuisse), Romain Danz� (pied), Vincent Pajot (pubalgie) et Julien F�ret (cuisse). Pour pallier ces absences, l?entra�neur breton a convoqu� Jonathan Pitroipa et Adrien Hunou.
Le groupe rennais :
Costil, Ndiaye - Moreira, Armand, Emerson, M'Bengue, Boye - Bakayoko, Doucour�, Makoun, Hunou, Konradsen, Kadir - Oliveira, Toivonen, Ntep, Grosicki, Pitroipa
