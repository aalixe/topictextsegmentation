TITRE: www.lyonne.fr - A la Une - AUXERRE (89000) - Auxerre mauvaise �l�ve en terme d'accessibilit� aux handicap�s
DATE: 2014-02-11
URL: http://www.lyonne.fr/yonne/actualite/2014/02/11/auxerre-mauvaise-eleve-en-terme-d-accessibilite-aux-handicapes_1868886.html
PRINCIPAL: 164440
TEXT:
(cliquez sur le code s'il n'est pas lisible)
Tous les articles
Fusion des r�gions : composez votre propre sc�nario !
Lu 237 fois
Afin de r�duire le "mille-feuille territorial" et diminuer ainsi les d�penses, le premier ministre Manuel Valls a annonc�, le 8 avril dans son discours de politique g�n�rale, son intention de diviser par deux le nombre de r�gions d'ici au 1er janvier 2017. A quelle r�gion aimeriez-vous...
De nombreuses corbeilles sont disponibles pour d�poser ses d�chets ponctuels
Lu 167 fois
Sur les trottoirs ou autres lieux publics, on peut d�poser ses (petites) ordures, gr�ce aux corbeilles que la ville a install�es un peu partout.
Daniel Dutuel a su garder de l'assurance
Lu 581 fois 1
Ce milieu de terrain est de la premi�re g�n�ration dor�e du centre de formation. Apr�s le foot, il s�est lanc� dans les assurances de footballeurs
D�sormais pr�s de la maison de quartier
Lu 24 fois �
Apr�s deux ann�es de bons et loyaux services, la place du Cadran, qui avait accueilli la f�te des plantes, c�de sa place au parc naturel urbain situ� � proximit� de la maison de quartier.
Une histoire espagnole
Lu 293 fois �
A la d�couverte des R�publicains Espagnol. L'association M�moire histoire des r�publicains espagnols de l'Yonne propose une exposition au p�le Rive droite, jusqu'au 18 avril. Demain aura lieu un diaporama-conf�rence, � De la R�publique � l'exil dans...
Les lyc�ens de Saint-Joseph sur la base navale de Brest
Lu 17 fois �
Durant trois jours, en fin de semaine, 40 �l�ves de Saint-Joseph (fili�re s�curit�) ont �t� immerg�s dans la Marine nationale, � l'initiative du capitaine de fr�gate et adjoint d�partemental pour la marine, Louis Philippe Cosme. Le professeur coordonnateur,...
Au centre commercial Fontaines des Clairions, d�s le 21 avril
Lu 119 fois �
Le centre commercial Fontaines des Clairions cherche son �g�rie. Shooting photos et d�fil� sont organis�s pour la d�nicher � partir du 21 avril. En mode � vrais gens �.
L'acc�s aux quais modifi�
Lu 1530 fois
D�but�es hier,�les modifications de circulation dans le quartier du Pont se poursuivront jusqu�� jeudi soir. Elles devraient am�liorer l�acc�s au quai de la R�publique.
45e �dition d�Auxerre-V�zelay, dimanche 27�avril
Lu 348 fois
Six parcours et autant de d�parts diff�rents (contre quatre l�an pass�) sont propos�s, pour rejoindre la colline �ternelle � V�zelay. Pour toutes les g�n�rations.
� Les puces tous les mois, c�est sympa �
Lu 646 fois
L�association organisera dimanche 13 avril, les premi�res puces des quais de l�Yonne. Un nouveau rendez-vous qui sera reconduit tous les mois jusqu�� la fin ao�t.
Dans sa qu�te du maintien, l'AJA a un programme cors�
Lu 2028 fois
� sept journ�es de la fin de la saison, l�AJA est 15 e avec trois points d�avance sur la zone rouge. Si la mission maintien semble bien engag�e, les Aja�stes n�ont toutefois aucune marge de man�uvre en raison d�un calendrier plus d�licat que pour ses cinq concurrents directs.
Paysages et nature s�exposent � l�espace Expression
Lu 4 fois �
L'espace Expression, place des Cordeliers, accueille jusqu'au 15 avril les peintures de Michel Autheman. Issu de l'�cole des Beaux-Arts de Paris et de l'�cole d'architecture de Paris, Michel Autheman utilise la peinture � l'huile comme principal...
