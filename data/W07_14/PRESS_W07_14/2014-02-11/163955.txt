TITRE: Cyber-espionnage: un r�seau global d�couvert par des experts russes | Sciences et espace | RIA Novosti
DATE: 2014-02-11
URL: http://fr.ria.ru/science/20140211/200447813.html
PRINCIPAL: 163861
TEXT:
Edward Snowden d�ment �tre un espion � la solde de la Russie (journal)
La soci�t� russe Kaspersky Lab, �diteur de logiciels de s�curit� informatique, a r�v�l� l'existence d'un r�seau global de cyber-espionnage op�rationnel depuis 2007, qui serait sponsoris� par un Etat non nomm�.
Selon les experts, le r�seau baptis� "The Mask" ou "Careto" ("masque" ou "visage laid" en espagnol argotique) a d�j� effectu� des attaques cibl�es contre au moins 380 soci�t�s publiques, missions diplomatiques, soci�t�s �nerg�tiques et p�trogazi�res, organisations scientifiques et militants politiques de 31 pays dont les Etats-Unis, la France, l'Allemagne, la Belgique, la Suisse et la Chine.�
Le r�seau de cyberespionnage utilisait plusieurs logiciels malveillants sophistiqu�s con�us pour diff�rents syst�mes d'exploitation, y compris pour Windows, Mac OS X, Linux et probablement pour Android et iOS, pour voler des informations dans les syst�mes infect�s, notamment des documents sensibles, des cl�s d'encodage, etc.
"Plusieurs raisons nous font penser que ce serait un r�seau sponsoris� par un Etat. Nous avons not� le haut degr� de professionnalisme du groupe qui a organis� l'attaque - il a surveill� le fonctionnement de l'infrastructure, arr�t� des op�rations en cas de besoin, s'est cach� en utilisant des r�gles de limitation d'acc�s et a effac� le contenu des fichiers journaux au lieu de les supprimer", a d�clar� Costin Raiu, expert de Kaspersky Lab. Selon lui, ce niveau de s�curit� n'est pas typique des groupes cybercriminels ordinaires.
Les experts de Kaspersky ont �tabli que les concepteurs du virus parlaient espagnol, ce qui n'�tait jamais arriv� lors d'attaques d'une telle envergure.
Les serveurs de commande du r�seau The Mask ont cess� de fonctionner en janvier 2014, apr�s le lancement de l'enqu�te par Kaspersky Lab.
Add to blog
