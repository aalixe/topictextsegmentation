TITRE: JO: Mattel vole et la France compte jusqu'� trois - France-Monde - La Voix du Nord
DATE: 2014-02-11
URL: http://www.lavoixdunord.fr/france-monde/jo-mattel-vole-et-la-france-compte-jusqu-a-trois-ia0b0n1913262
PRINCIPAL: 165022
TEXT:
- A +
Coline Mattel est entr�e mardi dans l'histoire du saut � skis en d�crochant le bronze du premier concours olympique f�minin et a apport� une troisi�me m�daille � la d�l�gation fran�aise aux JO de Sotchi.
Deux vols ma�tris�s (99,5 m puis  97,5 m). Mais une petite faute technique � la r�ception du second saut. Voil� ce qui s�pare le bronze de Coline Mattel, 18 ans, de la m�daille d'or "historique" d�croch�e par l'Allemande Carina Vogt, 22 ans, devant l'Autrichienne Daniela Iraschko-Stolz, 30 ans.
Les trois jeunes femmes ont �crit mardi soir, dans la douceur de Rosa Khoutor, une nouvelle page du sport f�minin. Et Coline Mattel a entretenu la flamme bleue, allum�e lundi par les deux m�dailles, d'or (Martin Fourcade) et de bronze (Jean-Guillaume Beatrix) en biathlon. En attendant peut-�tre Jason Lamy Chappuis (combin� nordique) mercredi.
T�te de gondole de cette nouvelle discipline olympique, Sara Takanashi, victorieuse de 10 des 13 concours de Coupe du monde cet hiver, n'a termin� qu'� la quatri�me place. Et elle n'aura m�me pas l'occasion de se rattraper, car les dames, qui s'affrontent sur le petit tremplin (90 m), ne peuvent pas encore se mesurer sur le grand tremplin (120 m). Voil� donc une nouvelle bataille pour l'�galit� � mener!
"Ipod" en or
Le concours de saut � skis f�minin n'a pas �t� affect� par la neige lourde et molle, fruit du radoucissement des temp�ratures, qui a perturb� de nombreuses �preuves, et notamment les disciplines acrobatiques.
Ainsi, en snowboard half-pipe, l'Am�ricain Shaun White, ic�ne de la discipline et double champion olympique de half-pipe (2006, 2010), a largement particip� au mouvement de protestation des concurrents, m�contents de la qualit� du demi-cylindre de 234 m�tres de long, dont la structure a �t� d�t�rior�e par la douceur ambiante.
hite est tomb� � deux reprises lors de son premier passage de la finale. Il a �galement �t� d�s�quilibr� lors de son second passage, avant de terminer � la quatri�me place, derri�re le nouveau champion olympique, le Suisse Iouri Podladtchikov, surnomm� "Ipod", et deux japonais, Ayumu Hirano, 15 ans, qui a d�croch� la m�daille d'argent, devant son compatriote Taku Hiraoka, 18 ans.
Seul v�ritable ambassadeur mondial de son sport, White visait un troisi�me titre d'affil�e dans une m�me �preuve individuelle, ce que seuls six sportifs sont parvenus � faire aux JO d'hiver.
Bescond rate la cible
La neige molle et collan
Shaun Wte a �galement contribu� � plomber l'ambiance autour du ski slopestyle, o� le premier titre remport� par la Canadienne Dara Howell est pass� au second plan en raison des nombreuses chutes spectaculaires et l'�vacuation sur une civi�re de sa compatriote Yuki Tsubota. Vraisemblablement tromp�e par le "jour blanc", elle est violemment tomb�e sur le dos � la r�ception d'un saut.
Les fondeurs norv�giens semblent porter peu d'attention aux conditions de "jeu". Samedi, sur le coton blanc g�n�r� par un froid piquant, Marit Bjoergen avait offert un premier titre � son pays en ski de fond (skiathlon). Trois jours plus tard, sur une bouillie printani�re, Maiken Caspersen Falla s'est impos�e chez les dames devant sa compatriote Ingvild Flugstad Oestberg. Chez les messieurs, Ola Vigen Hattestad l'a emport� au terme de courses ponctu�es par de nombreuses chutes, f�lures et autres bris de skis.
La biathl�te Tara Berger a compl�t� la r�colte norv�gienne quelques heures plus tard, en terminant deuxi�me de la poursuite (10 km) remport�e par la Bi�lorusse Darya Domracheva.
Partie en cinqui�me position, gr�ce � un bon parcours mercredi lors de l'�preuve du sprint, la Franc-Comtoise Ana�s Bescond, qui r�vait d'accrocher une premi�re m�daille, n'a termin� que 12e. Elle a notamment �t� victime de deux rat�s au tir. Le prix d'une m�daille.
R�agir � l'article
