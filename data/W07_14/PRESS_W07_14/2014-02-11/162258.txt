TITRE: 77 jours pour obtenir un rendez-vous chez l'ophtalmo - BFMTV.com
DATE: 2014-02-11
URL: http://www.bfmtv.com/societe/deux-mois-obtenir-un-rendez-vous-chez-lophtalmo-707490.html
PRINCIPAL: 162257
TEXT:
r�agir
C'est un probl�me que les myopes, presbytes et autres Fran�ais souffrant des yeux, connaissent bien. Les d�lais pour obtenir un rendez-vous chez un ophtalmo sont souvent longs. En moyenne, il faut 77 jours pour obtenir un rendez-vous chez l'ophtalmologue, et ce d�lai peut atteindre 7 mois dans certains d�partements, voire un an chez certains praticiens, selon une enqu�te aupr�s de plus de 2.600 d'entre eux.
Pire: 15% des ophtalmos ne sont plus en mesure d'accepter de nouveaux patients, selon cette enqu�te Yssup Research, rendue publique mardi, qui confirme les difficult�s d'acc�s � ces sp�cialistes.
Des d�lais variables selon les villes
� O� on attend le moins. C'est � Paris, dans les Hauts-de-Seine (92), les Alpes-Maritimes (06) et  les Bouches-du-Rh�ne (13) que les d�lais d'attente pour un rendez-vous  sont les plus courts (de 24 � 40 jours en moyenne). Parmi  les dix plus grandes villes, Paris, Marseille et Bordeaux sont  celles  o� l'attente est la moins longue avec un d�lai moyen pour  obtenir un  rendez-vous inf�rieur � un mois (respectivement 24,7 jours,  24,8 et 27,4  ).
La Seine-Saint-Denis fait partie des dix d�partements o� les d�lais   sont les plus courts avec 54,2 jours d'attente mais on s'y heurte � 16%   de refus de rendez-vous contre 2% � Paris.
�� �
� O� on attend le plus. A  l'inverse, dans la Loire (42), dans le Finist�re (29), en Is�re (38) et  en Seine-Maritime (76), les d�lais restent tr�s longs: sup�rieurs � 152  jours d'attente. A Rennes et Toulouse, l'attente d�passe les 3 mois.
�� �
� La Loire mal lotie. Elle bat tous les records: elle est en  t�te des dix d�partements o� le d�lai d'obtention d'un rendez-vous est  le plus long (205,3 jours en moyenne) apr�s prise de contact avec au  moins 20 sp�cialistes, et elle d�tient le taux d'impossibilit� de  prendre rendez-vous le plus �lev� (65%).
�� �
� L'impossibilit� de prendre rendez-vous dans l'ann�e (ou refus de nouveau patient) touche �galement la Meurthe-et-Moselle (59%), la Moselle (46%), l'Is�re (38%)  l'Ile-et-Vilaine (36%), l'Essonne (34%), la Seine-Maritime et le  Finist�re (31% chacun) le Bas-Rhin et le Nord (30% chacun).
�� �
Cette enqu�te  nationale, r�alis�e aupr�s de 2.643 ophtalmologues entre le 15 octobre  2013 et le 30 janvier 2014 par Yssup Research (groupe Publicis Health  Care), conforte un r�cent rapport parlementaire qui souligne le "manque  criant de professionnels", notamment de secteur 1, dont souffrent  certaines r�gions, et des d�lais d'attente qui peuvent parfois atteindre  18 mois.
