TITRE: Pour Peillon, Cop� est "porte-parole de groupes extr�mistes"
DATE: 2014-02-11
URL: http://www.francetvinfo.fr/societe/education/polemique-sur-le-genre/pour-peillon-cope-est-porte-parole-de-groupes-extremistes_527777.html
PRINCIPAL: 165060
TEXT:
Pour Peillon, Cop� est "porte-parole de groupes extr�mistes"
Le ministre de l'Education nationale le d�clare dans un entretien accord� � "Lib�ration".
Le ministre de l'Education nationale, Vincent Peillon, � l'Assembl�e nationale, � Paris, le 11 f�vrier 2014. (ERIC FEFERBERG / AFP)
, publi� le
11/02/2014 | 22:30
La pol�mique sur le livre Tous � poils n'en finit pas. Le ministre de l'Education nationale d�clare que le pr�sident de l'UMP, Jean-Fran�ois Cop�, se fait le "porte-parole de groupes extr�mistes", dans un entretien � Lib�ration , dat� du 12 f�vrier.�"Sans doute ces dirigeants font-ils des calculs politiciens � courte vue", poursuit-il, estimant que "leur outil de propagande est le mensonge".
Quelques heures auparavant, lors des questions d'actualit� au gouvernement, Vincent Peillon a lanc� un avertissement aux d�put�s UMP :�"Attention aux rumeurs que vous r�pandez."
(LCP)
Fin janvier,�Najat�Vallaud-Belkacem, la porte-parole du gouvernement, a lanc� une critique similaire � celle de Vincent Peillon. La ministre des Droits des femmes� avait estim� que Jean-Fran�ois�Cop� se rangeait "du c�t� des extr�mismes contre la R�publique et ses valeurs" alors que le pr�sident de l'UMP avait dit�"comprendre l'inqui�tude des familles" apr�s les rumeurs sur l'enseignement d'une pr�tendue�"th�orie du genre" dans les �coles primaires.
