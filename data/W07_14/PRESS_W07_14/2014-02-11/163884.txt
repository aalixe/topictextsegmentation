TITRE: Décès d'un vigile dans une décharge de l'Orne
DATE: 2014-02-11
URL: http://www.lefigaro.fr/flash-actu/2014/02/11/97001-20140211FILWWW00227-deces-d-un-vigile-dans-une-decharge-de-l-orne.php
PRINCIPAL: 0
TEXT:
le 11/02/2014 à 15:55
Publicité
Un vigile du centre de déchets très contesté de Guy Dauphin Environnement (GDE) situé à Nonant-le-Pin, près du haras du Pin (Orne), est décédé sur le site la nuit dernière, selon le parquet qui privilégie la thèse de l'accident.
"Compte tenu des éléments que j'ai, je privilégie plutôt une thèse accidentelle", a déclaré le procureur de la République d'Argentan, Hugues de Phily, qui n'exclut aucune hypothèse même s'il n'a pour l'heure "aucun élément" allant dans le sens d'un scénario criminel. L'homme, âgé de 44 ans, a apparemment "glissé" dans le bassin de traitement des eaux où son corps a été retrouvé, selon le magistrat.
M. de Phily a précisé que "des traces sur la main de l'homme laissent penser qu'il a pu essayer de s'en sortir", raison pour laquelle la thèse de l'accident est privilégiée sur celle du suicide. L'enquête devra toutefois confirmer ces hypothèses et en particulier l'autopsie, dont les résultats devraient être connus dans les prochains jours, a souligné le parquet. D'après les premières constatations, il n'est pas facile de sortir du bassin car il est "tapissé d'une espèce de bâche", a précisé le procureur, soulignant aussi que l'eau était froide.
Le corps du vigile a été retrouvé entre trois et quatre heures du matin par ses collègues "surpris de ne pas le voir revenir d'une ronde", selon le parquet. Les enquêteurs vont aussi exploiter les vidéos de surveillance et analyser des prélèvements réalisés sur place, notamment dans le bassin.
Le centre de déchets de Guy Dauphin Environnement, ouvert le 22 octobre avec le feu vert de l'Etat, est bloqué depuis le 24 par des opposants qui redoutent que ce centre ne pollue les terres environnantes de réputation mondiale pour l'élevage équin. Cette affaire a connu de multiples épisodes judiciaires ces derniers mois. Le prochain est prévu jeudi au tribunal d'Argentan.
