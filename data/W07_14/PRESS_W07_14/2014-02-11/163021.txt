TITRE: JO 2014 / HALF-PIPE - Half-pipe : Shaun White critique la structure
DATE: 2014-02-11
URL: http://www.sport365.fr/jo-2014-sotchi/half-pipe-shaun-white-critique-structure-1102316.shtml
PRINCIPAL: 163020
TEXT:
JO 2014 / HALF-PIPE - Publi� le 11/02/2014 � 10h41
0
Half-pipe : Shaun White critique la structure
Shaun White, star am�ricaine du half-pipe, a vivement critiqu� la qualit� de la structure de neige o� se d�roulera l'�preuve ce mardi.
PANORAMIC
La structure o� se d�roulera l��preuve de half-pipe, ce mardi, est sujette � de vives critiques de la part des concurrents dont celle de Shaun White, champion olympique en titre et grandissime favori de l��preuve. � J'esp�re qu'ils vont trouver un moyen de rendre ce pipe un peu facile � manoeuvrer car c'est clairement loin d'�tre parfait, a-t-il d�clar�. Le milieu est compl�tement mou et lourd. C'est d�j� arriv� ailleurs mais c'est emb�tant quand �a arrive aux jeux Olympiques. � Son compatriote, Danny Davis, lui a embo�t� le pas. � C'est vraiment dommage d'arriver au jeux Olympiques et de ne pas avoir un pipe � la hauteur de la qualit� des concurrents. Les organisateurs ont embauch� les mauvaises personnes pour construire ce pipe. �
