TITRE: Serge Dassault demande la lev�e de son immunit� parlementaire | France | Nice-Matin
DATE: 2014-02-11
URL: http://www.nicematin.com/france/serge-dassault-demande-la-levee-de-son-immunite-parlementaire.1618707.html
PRINCIPAL: 0
TEXT:
Tweet
PARIS (AFP)
Le s�nateur UMP Serge Dassault a demand� lui-m�me lundi la lev�e de son immunit� parlementaire, devan�ant ainsi la d�cision qui semblait in�luctable du bureau de la Haute Assembl�e, saisi d'une nouvelle demande des juges charg�s du dossier d'achat pr�sum�s de voix � Corbeil-Essonnes.
"Je demande la lev�e de mon immunit� parlementaire", a annonc� � l'AFP l'industriel et propri�taire du Figaro, expliquant vouloir ainsi d�montrer par cette d�marche exceptionnelle qu'il n'avait "rien � (se) reprocher".
Mercredi, le bureau du S�nat, qui doit se prononcer sur une nouvelle demande de lev�e de l'immunit� parlementaire de Serge Dassault, devrait cette fois y r�pondre favorablement, selon plusieurs sources parlementaires.
Avec la d�cision de M. Dassault, tout suspense est lev�.
Cela permettra aux magistrats du p�le financier de Paris, Serge Tournaire et Guillaume Da�eff, de placer le s�nateur de 88 ans en garde � vue.
"M�me si cette lev�e d'immunit� provoque mon placement en garde � vue, je suis pr�t � affronter cette �preuve", �crit M. Dassault dans un communiqu�.
"Je pourrai de ce fait avoir acc�s � la proc�dure" et "pouvoir me d�fendre contre ces accusations". "Je pourrai d�montrer ma totale innocence de ces soi-disant achats de votes, accusations invent�es de toutes pi�ces par certains de mes adversaires politiques", ajoute-t-il.
"La chasse au tra�tre" au palais du Luxembourg
Par deux fois, l'ancien maire de Corbeil-Essonnes (Essonne) avait de justesse �chapp� � la lev�e de son immunit�.
La derni�re, le 8 janvier, avait d�clench� un toll�, si bien que le pr�sident de la Haute assembl�e, Jean-Pierre Bel (PS), avait d�cid� de changer le mode de vote: vote � main lev�e au lieu de vote � bulletin secret.
Dans un communiqu�, les avocats de l'avionneur et patron de presse avaient toutefois assur� que Serge Dassault, qui a �t� maire de cette commune jusqu'en 2008, restait � la disposition des juges parisiens s'ils souhaitaient l'entendre.
Ils s'�taient offusqu�s des critiques contre la d�cision du bureau du S�nat, d�plorant "l'instrumentalisation politique d'une affaire judiciaire au m�pris des r�gles fondamentales de notre droit".
Les 26 membres du bureau du S�nat, 14 de gauche et 12 de droite, avaient en effet rejet� la lev�e par 13 voix contre 12, et une abstention. Il avait donc manqu� deux voix de gauche. Et "la chasse au tra�tre" avait g�ch� l'ambiance au Palais du Luxembourg les jours suivants.
Jean-Marc Ayrault "choqu�"
"C'est la pire journ�e que j'aie v�cue au S�nat depuis mon �lection � sa pr�sidence", avait d'ailleurs reconnu Jean-Pierre Bel, membre de droit du bureau.
Le Premier ministre Jean-Marc Ayrault s'�tait lui m�me dit "choqu�" par ce vote. "La justice peut, si elle le souhaite, tr�s vite, c'est-�-dire demain, faire une nouvelle demande (de lev�e d'immunit�) et l�, je crois que le contexte aura chang� car on ne peut continuer avec ce genre de pratiques qui portent atteinte � la d�mocratie", avait-il ajout�.
Dans cette instruction ouverte depuis mars pour achat de votes, corruption, blanchiment et abus de biens sociaux, les magistrats s'int�ressent aux �lections municipales organis�es en 2008, 2009 et 2010 � Corbeil-Essonnes, remport�es par M. Dassault, puis par son bras droit, Jean-Pierre Bechter.
En annulant le scrutin de 2008, le Conseil d'Etat avait tenu pour "�tablis" des dons d'argent aux �lecteurs, sans se prononcer sur leur ampleur et bien que des t�moins se soient r�tract�s.
Mi-septembre, les avocats de M. Dassault avaient estim� que leur client �tait "l'objet, depuis plusieurs ann�es, de demandes pressantes de remise d'argent par divers individus qui avaient �t� inform�s de sa g�n�rosit�". Il lui est arriv� "d'accorder un soutien financier, mais toujours en dehors de toute d�marche �lectorale", avaient-ils ajout�.
Carlos da Silva, d�put� PS de l'Essonne et candidat aux municipales � Corbeil-Essonnes, a qualifi� aupr�s de l'AFP de "basse man?uvre politique et absolument pas d'un gage de transparence ou de l'innocence de Serge Dassault" la d�cision du s�nateur.
Dans l'entourage de Bruno Piriou, opposant historique � Serge Dassault et candidat aux municipales � la t�te d'une liste soutenue par le PCF, on a salu� "une bonne nouvelle pour la d�mocratie". "La justice a gagn�. Serge Dassault aura � rendre des comptes", a-t-on ajout�.
Tweet
Tous droits de reproduction et de repr�sentation r�serv�s. � 2012 Agence France-Presse.
Toutes les informations (texte, photo, vid�o, infographie fixe ou anim�e, contenu sonore ou multim�dia) reproduites dans cette rubrique (ou sur cette page selon le cas) sont prot�g�es par la l�gislation en vigueur sur les droits de propri�t� intellectuelle. �Par cons�quent, toute reproduction, repr�sentation, modification, traduction, exploitation commerciale ou r�utilisation de quelque mani�re que ce soit est interdite sans l�accord pr�alable �crit de l�AFP, � l�exception de l�usage non commercial personnel. �L�AFP ne pourra �tre tenue pour responsable des retards, erreurs, omissions qui ne peuvent �tre exclus dans le domaine des informations de presse, ni des cons�quences des actions ou transactions effectu�es sur la base de ces informations.
AFP et son logo sont des marques d�pos�es.
"Source AFP" � 2014 AFP
France
Le d�lai de 15 jours au-del� duquel il n'est plus possible de contribuer � l'article est d�sormais atteint.
Vous pouvez poursuivre votre discussion dans les forums de discussion du site. Si aucun d�bat ne correspond � votre discussion, vous pouvez solliciter le mod�rateur pour la cr�ation d'un nouveau forum : moderateur@nicematin.com
