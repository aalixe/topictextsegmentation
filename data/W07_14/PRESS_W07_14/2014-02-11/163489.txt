TITRE: Syrie: Les civils �vacu�s de Homs sont interrog�s par Damas -   News: Standard - lematin.ch
DATE: 2014-02-11
URL: http://www.lematin.ch/monde/Les-civils-evacues-de-Homs-sont-interroges-par-Damas/story/12162379
PRINCIPAL: 0
TEXT:
Les civils �vacu�s de Homs sont interrog�s par Damas
Syrie
�
Les hommes �vacu�s de Homs sont interrog�s par les forces de s�curit� syriennes, sous surveillance du HCR (Haut-commissariat des r�fugi�s), a-t-on appris mardi � Gen�ve lors d'un briefing de l'ONU.
Mis � jour le 11.02.2014 1 Commentaire
Des hommes �vacu�s de Homs sont interrog�s par les autorit�s syriennes.
Galerie photo
Plusieurs sites syriens class�s au patrimoine mondial de l'humanit� ont �t� endommag�s par la guerre civile qui ravage le pays depuis bient�t trois ans.
Galerie photo
Le deuxi�me round de la conf�rence de Gen�ve 2 sur la Syrie va reprendre  lundi 10 f�vrier. Objectif: mettre fin � 2 ans et demi de guerre civile  en cr�ant une autorit� de transition.
Articles en relation
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
Quelque 1132 personnes ont �t� �vacu�es en quatre jours de la vieille ville de Homs. Beaucoup disent avoir souffert de la faim et des bombardements incessants.
�336 hommes, �g�s de plus de 15 ans et de moins de 55 ans, ont �t� interpell�s apr�s leur �vacuation pour �tre interrog�s�, a d�clar� Melissa Fleming, porte-parole du HCR. Sur ce nombre, 42 ont �t� rel�ch�s. Les autres sont toujours entre les mains des autorit�s, qui les interrogent dans une �cole, situ�e non loin de Homs.
�Le HCR est pr�sent dans l'enceinte de l'�cole, mais n'assiste pas aux interrogatoires�, a ajout� la porte-parole. A l'issue des interrogatoires, le HCR a des entretiens avec les hommes, qui demandent souvent le regroupement avec leur famille, car ils ont quitt� la vieille ville avec leur femme et enfants.
Selon l'OCHA, l'Office des nations unies pour les op�rations d'urgence, quelque 1132 personnes ont �t� �vacu�es en quatre jours de la vieille ville de Homs, apr�s un si�ge de 600 jours.
Aucune indication n'a �t� donn�e quant au nombre de personnes qui se trouvent toujours dans l'enceinte de la vieille ville. L'Unicef a indiqu� que 400 enfants, faisant partie du groupe des 1132 �vacu�s ont �t� vaccin�s contre la polio. Il y a eu aussi 20 femmes enceintes �vacu�es.
Tr�ve prolong�e
La plupart des personnes �vacu�es ont t�moign� avoir souffert du �froid, de la faim, de l'eau impropre et des bombardements incessants�, a indiqu� la porte-parole de l'Unicef.
R�gime et rebelles ont conclu une tr�ve par l'interm�diaire de l'ONU. Cette tr�ve a �t� prolong�e jusqu'� mercredi soir pour permettre davantage d'�vacuations et l'acheminement de l'aide � ceux qui ont choisi de rester dans ces quartiers ruin�s par la guerre qui ravage la Syrie. (ats/afp/Newsnet)
Cr��: 11.02.2014, 14h14
