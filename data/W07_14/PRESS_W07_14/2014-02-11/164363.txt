TITRE: Snowboard - JO (H) - Longo et Baisamy sortis
DATE: 2014-02-11
URL: http://www.lequipe.fr/Snowboard/Actualites/Arthur-longo-elimine/440438
PRINCIPAL: 0
TEXT:
a+ a- imprimer RSS
Arthur Longo (photo) et Johann Baisamy ont tous deux �t� victimes de chute lors de leur passage en demi-finales. (AFP)
Johann Baisamy et Arthur Longo ne disputeront pas la finale du halfpipe (18h30), o� l'Am�ricain Shaun White, d�j� sacr� � Turin et Vancouver, sera l'immense favori. Les deux Fran�ais n'ont pu franchir le cap de la demi-finale, seulement 9e et 11e � l'issue de deux runs o� ils ont chut�. �Je suis d��u mais j'ai tout donn�, avoue le premier. J'y ai cru jusqu'au bout. Je me suis fait secouer avant la fin de mon deuxi�me run, au moment o� j'allais faire un double. Je suis content de ne pas m'�tre fait mal. Le pipe �tait compliqu� et vraiment dangereux aujourd'hui. Il y a quinze centim�tres de soupe dans la transition...� Longo �tait tout aussi d�pit� : �Je suis pass� � c�t�. Ce matin, �'aurait pu passer. Je n'aimais pas ce pipe. Les conditions n'avantagent personne mais surtout pas moi qui cherche la hauteur et la fluidit�. Ces dix-huit mois pass�s � travailler avec l'�quipe ont pourtant �t� une exp�rience tr�s enrichissante�.
My.A. � Rosa Khutor
