TITRE: Faits divers : kwassa, viol, chute et bangu� - Divers - Mayottehebdo.com | Toute l'actualit� de Mayotte
DATE: 2014-02-11
URL: http://www.mayottehebdo.com/index.php?option%3Dcom_content%26view%3Darticle%26id%3D11375:faits-divers-kwassa-viol-chute-et-bangue%26catid%3D100:divers%26Itemid%3D74
PRINCIPAL: 163802
TEXT:
Divers
Hier matin au large de Bou�ni, dans le sud-ouest de Mayotte , une embarcation de fortune de type kwassa-kwassa a chavir� et 7 personnes sont d�c�d�es.
Quinze autres personnes ont pu �tre sauv�es. Selon les premi�res d�clarations des naufrag�s, il n'y aurait pas d�autres personnes port�es disparues.
Il s'agissait de personnes en situation irr�guli�re tentant la travers�e � bord d'un bateau de type Kwassa-kwassa.
La brigade nautique de la police aux fronti�res, dans le cadre d'une mission de lutte contre l'immigration clandestine, �tait pr�sente sur les lieux, aux alentours de 8H40. Les conditions m�t�o �taient rendues difficiles avec une forte pluie et peu de visibilit�. Ayant entendu des cris et des appels au secours, les fonctionnaires se sont imm�diatement port�s au secours des naufrag�s. Mais il �tait malheureusement d�j� trop tard pour 7 d'entre eux, d�c�d�s par noyade.
Les rescap�s ont �t� conduits � l'h�pital de Mamoudzou pour une �valuation et prise en charge sanitaire. Deux personnes, soup�onn�es d'�tre les passeurs, ont �t� plac�es en garde � vue dans les locaux de la gendarmerie � Mamoudzou.
L'identification des corps pour qu'ils soient remis aux familles se poursuit et les associations locales y participent, selon la pr�fecture. Malgr� tout 15 personnes ont pu �tre sauv�es et transbord�es sur l'intercepteur de la PAF.
�
�
92 passagers dans trois kwassa-kwassa
Lundi matin, la brigade nautique de la gendarmerie a rep�ch� � trois reprises des embarcations bond�es de clandestins, candidats � "l�eldorado mahorais ". Au total, c�est 92 passagers qui ont �t� conduits, apr�s visite m�dicale, au centre de r�tention administratif.
�
Le violeur de Doujani appr�hend�
Apr�s l�enl�vement et le viol commis sur une fillette de neuf ans vendredi � Doujani, le violeur pr�sum� a �t� attrap� dimanche dans le courant de l�apr�s-midi. Quelques �l�ments concordants ont d�j� �t� mis � jour comme le v�hicule qui a servi � transporter la jeune fille jusqu�� Tsararano, le lieu du viol. L�auteur pr�sum� est toujours sous le coup d�un interrogatoire et pourrait �tre transf�r� au centre de d�tention aujourd�hui.
�
Chute d�un cocotier � Ongojou
Dimanche � 11h10, l�h�licopt�re de la gendarmerie est appel� � Ongojou. Un homme a chut� du haut de son cocotier, soit environ 30 m�tres.
La victime �g�e d�une trentaine d�ann�es est h�litreuill�e dans un �tat grave. Deux rotations sont n�cessaires pour �vacuer le mat�riel et le personnel soignant.
�
Revendeurs de Bangu� arr�t�s
Les gendarmes contr�lent quatre individus suspects dimanche � 17h15 au rond-Point du Four � chaux � Labattoir. Ils sont en possession d�un tube de bangu�, de plusieurs doses de stup�fiants et d�une forte somme d�argent en liquide.
Les revendeurs de drogue sont imm�diatement emmen�s au commissariat de Petite-terre et plac�s en garde � vue.
