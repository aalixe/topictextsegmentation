TITRE: Football - MAXIFOOT : Coupe de France : Cannes se paie Montpellier, Lille se fait peur, Angers en quart...
DATE: 2014-02-11
URL: http://www.maxifoot.fr/football/article-20978.htm
PRINCIPAL: 164986
TEXT:
Coupe de France : Cannes se paie Montpellier, Lille se fait peur, Angers en quart...
Par Nicolas Lagavardan - Actu Coupe De France, Mise en ligne: le 11/02/2014 � 21h55
Taille du texte: Email Imprimer Partager:
Trois matchs des 8es de finale de la Coupe de France ont �t� jou�s mardi soir. Cannes a cr�� la sensation en �liminant Montpellier, Lille a gal�r� face � Caen et Angers a sorti CA Bastia. Les r�sultats de la soir�e...
Les Cannois savourent leur qualification
Les 8es de finale de la Coupe de France ont d�but� ce mardi avec trois matchs au menu. Le premier d'entre eux s'est conclu sur une premi�re surprise : l'�limination de Montpellier � Cannes, pensionnaire de CFA (1-0 ap). De son c�t�, Lille a �t� pouss� jusqu'aux tirs au but par Caen (3-3 ap, 6 tab 5). Angers a d� patienter pour battre CA Bastia (4-2 ap)...
Cannes �limine Montpellier !
Cannes poursuit son bonhomme de chemin en Coupe de France. Le club de CFA a franchi l'obstacle des 8es de finale en dominant Montpellier (1-0 ap). Les H�raultais auront donc battu le PSG au tour pr�c�dent pour pas grand-chose... D�cevants, les hommes de Rolland Courbis ont chut� dans la derni�re minute de la prolongation sur une frappe crois�e du droit de Belkacem Zoniri (119e). Ils auraient pourtant pu ouvrir la marque un peu plus t�t si Mbaye Niang n'avait pas exp�di� un penalty sur le poteau (102e)...
Lille s'est fait peur
Le LOSC a bien failli passer � la trappe face � Caen (3-3 ap, 6 tab 5), au terme d'un match � rallonge et � rebondissements. Apr�s avoir �t� pi�g� en contre et rapidement men� 2-0 par le club de Ligue 2 sur un doubl� de Bengali-Fode Koita (14e, 28e), Lille a hauss� son niveau et r�ussi � refaire son retard par Ronny Rodelin (33e) et Jonathan Delaplace (77e), et m�me � prendre l'avantage � la 83e par Nolan Roux.
Mais alors que les Dogues filaient vers la qualification, Rodelin marquait contre son camp � la 90e et poussait les deux �quipes vers une prolongation qui ne changeait pas le sort du match, malgr� de nombreuses occasions. Durant la s�ance des tirs au but, les gardiens ne parvenaient pas � sortir le moindre tir, mais le 12e tireur, le Caennais Mathias Autret mettait un terme aux d�bats en frappant au dessus du but.
Angers a pris son temps
Dans ce match entre le 2e et le 20e de Ligue 2, Angers a d� patienter jusqu'aux derniers instants de la prolongation (4-2) pour valider son ticket pour les quarts de finale. Les hommes de St�phane Moulin ont m�me �t� men�s � deux reprises � la suite d'un doubl� de Sunday Mba (31e, 104e). Mais apr�s une premi�re �galisation de Khaled Ayari (57e), les Corses ont �t� contraints de jouer � dix apr�s l'exclusion de Mamadou Camara (66e). Charles Diers (105e), Mohamed Yattara (113e) et J�r�my Blayac (117e) en ont profit� pour donner la victoire aux Angevins.
Les r�sultats de mardi (en gras les qualifi�s) :
Cannes (CFA) - Montpellier : 1-0 ap
Angers (L2) - CA Bastia (L2) : 4-2 ap
Lille - Caen (L2) : 3-3 ap, 6 tab 5
Le programme :
