TITRE: J�r�me Kerviel. L'ancien trader en cassation, la prison en toile de fond
DATE: 2014-02-11
URL: http://www.ouest-france.fr/jerome-kerviel-lancien-trader-en-cassation-la-prison-en-toile-de-fond-1922390
PRINCIPAL: 0
TEXT:
J�r�me Kerviel. L'ancien trader en cassation, la prison en toile de fond
France -
Achetez votre journal num�rique
La Cour de cassation examine jeudi le pourvoi de J�r�me Kerviel, desormais sous la menace d'une incarc�ration en cas de rejet.
Un peu plus de six ans apr�s la r�v�lation des faits, l'ancien banquier embl�matique des d�rives de la finance continue de se battre et clame son innocence.
�"Je n'ai vol� personne"
Condamn� en premi�re instance puis en appel � la m�me peine, cinq ans d'emprisonnement dont trois ferme et 4,91 milliards d'euros de dommages et int�r�ts, il reconna�t une part de responsabilit� mais r�fute avoir agi en secret. "J'ai fait ce que la banque m'a appris � faire et je n'ai vol� personne", mart�le-t-il encore dans un entretien au site catholique d'information Aleteia.  "On voudrait condamner � vie un ancien trader qui n'a jamais agi que dans l'int�r�t de sa banque, qui r�compensait chaque ann�e en bonus sa seule hi�rarchie pour sa conduite sp�culative inconsid�r�e?", s'est insurg� lundi l'ancienne magistrate et d�put� europ�enne EELV Eva Joly sur le site du Huffington Post.  La banque savait ou aurait d� savoir ce qui se tramait dans cette salle de march�s d'une tour de La D�fense et ne peut donc se pr�tendre victime, plaide J�r�me Kerviel depuis plusieurs ann�es.
Une victime n�gligente ou consentante?
Des d�fauts de contr�le importants ont notamment �t� point�s par le r�gulateur des banques, la Commission bancaire devenue l'Autorit� de contr�le prudentiel (ACP), qui a condamn� Soci�t� G�n�rale � 4 millions d'euros d'amende pour ces manquements.  Mais dans son avis �crit, l'avocat g�n�ral pr�s la Cour de cassation, Yves Le Baut, r�torque qu'une "victime n�gligente n'est pas pour autant une victime consentante". Selon le magistrat, "on ne peut tirer pour cons�quence du d�faut de vigilance" de la banque "son adh�sion � la commission des agissements qui lui ont port� pr�judice".�
Lire aussi
