TITRE: Silverstone RVZ01 : pour monter une Steam Machine ou un PC de jeu de salon
DATE: 2014-02-11
URL: http://www.clubic.com/materiel-informatique/boitier-pc/actualite-618182-silverstone-rvz01-steam-machine-pc-jeu-salon.html
PRINCIPAL: 0
TEXT:
Silverstone RVZ01 : pour monter une Steam Machine ou un PC de jeu de salon
Partager cette actu
Publi�e                 par Romain Heuillard le Mardi 11 Fevrier 2014
Silverstone a mis en vente le Raven RVZ01, un bo�tier permettant de concevoir une Steam Machine ou un ordinateur de jeu de la taille d'une Xbox One.
R�put� pour ses bo�tiers et composants pour ordinateurs de salon et ordinateurs miniatures, le fabricant Silverstone a mis en vente le Raven RVZ01, destin� � la conception d'un ordinateur de jeu de salon.
Mesurant 382 x 350 x 105 mm, le RVZ01 n'est effectivement qu'un peu plus gros que la Xbox One, qui mesure quant � elle 344 x 263 x 80 mm. Il adopte en tout cas un format adapt� � une installation � l'horizontale sous le t�l�viseur ou � la verticale � ses c�t�s.
Il est destin� � accueillir des composants miniatures tels qu'une carte m�re mini-ITX et une alimentation SFX, mais il peut n�anmoins loger une carte graphique haut de gamme � deux emplacements et de 33 cm de longueur, un ventilateur pour CPU de 83 mm de haut et 3 ventilateurs de 120 mm (dont deux fournis) ou m�me un syst�me de refroidissement liquide (watercooling). On dispose �galement d'un emplacement 3,5 pouces, de trois emplacements 2,5 pouces et d'un emplacement pour unit� optique slim. De quoi assembler un ordinateur tr�s haut de gamme, en n'�cartant que le multi-GPU (CrossFireX ou SLI) et les cartes d'extension, tr�s rarement utilis�s.
Ce Silverstone Raven RVZ01 sera mis en vente en France demain � 80 euros, prix auquel s'ajoutera pour rappel celui d'une alimentation SFX.
Vous aimerez aussi
