TITRE: Sotchi 2014 | Pinturault et Mermillod-Blondin prennent leurs marques en vue du super-combin�
DATE: 2014-02-11
URL: http://www.ledauphine.com/skichrono/2014/02/11/pinturault-et-mermillod-blondin-prennent-leurs-marques-en-vue-du-super-combine
PRINCIPAL: 162601
TEXT:
JO DE SOTCHI / SKI ALPIN Pinturault et Mermillod-Blondin prennent leurs marques en vue du super-combiné
Previous Next
Arrivés lundi soir, rondement menée (vers 21 h alors que le staff est arrivé deux heures plus tard), les deux spécialistes du super-combiné, Alexis Pinturault et Thomas Mermillod-Blondin, ont pris leurs marques dès ce matin sur la piste de descente. Une bonne séance d�??entraînement �?? finalement maintenue malgré une météo incertaine -, pour « Pintu », rapide (5e) même s�??il a manqué une porte sur le haut du tracé. « Ca ne m�??a pas fait gagné trop de temps. Ces premiers repères étaient intéressants. J�??ai eu de bonnes sensations. Je vais essayer de corriger quelques trucs à la vidéo. »
TMB (7e chrono) était aussi satisfait de sa manche. « C�??est une piste que j�??aime bien (3e du super-combiné avec le 7e temps de la descente en 2012). Ca reste la même piste. Il y a de gros sauts. Là, il y avait quelques vaguelettes et je me suis fait surprendre. Je vais faire tous les entraînements. J�??ai fait un peu de jus en restant à la maison et en gardant le petit Jack. J�??ai fait deux jours de slalom, avec le club au Grand-Bo et dimanche à Lélex.» Pinturault y a fait du géant. « J�??avais coupé deux jours complets après Saint-Moritz puis refait trois jours de physique. »
Alors que Ted Ligety, lui aussi arrivé la veille en provenance de Zurich, a aussi pris ses marques, Bode Miller, 8e de la descente dimanche, était le seul « gros » à s�??être remis en piste. Avec le meilleur chono. Of course. « C�??était important. Le revêtement a bien changé. »
1er entrainement sur cette belle descente ! :) Je prends mes marques dans cette nouvelle ambiance. :) #espritbleu http://t.co/hmYABQf4Ed
; Alexis Pinturault (@AlexPinturault) February 11, 2014
De Sotchi, E.B.
Par E.B. | Publié le 11/02/2014 à 08:39
Vos commentaires
