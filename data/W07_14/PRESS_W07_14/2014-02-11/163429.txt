TITRE: JO/Snowboard: le half-pipe vis� par les critiques - LExpress.fr
DATE: 2014-02-11
URL: http://www.lexpress.fr/actualites/1/sport/jo-snowboard-le-half-pipe-vise-par-les-critiques_1322652.html
PRINCIPAL: 163423
TEXT:
JO/Snowboard: le half-pipe vis� par les critiques
Par AFP, publi� le
11/02/2014 � 11:40
, mis � jour � 12:00
Rosa Khoutor (Russie) - La qualit� du demi-cylindre de neige o� se d�roule mardi l'�preuve snowboard de half-pipe des jeux Olympiques est l'objet de vives critiques, y compris des favoris comme les Am�ricains Shaun White et Danny Davis.
La qualit� du demi-cylindre de neige o� se d�roule mardi l'�preuve snowboard de half-pipe des jeux Olympiques est l'objet de vives critiques, y compris des favoris comme les Am�ricains Shaun White et Danny Davis.
afp.com/Franck Fife
"C'est vraiment dommage d'arriver au jeux Olympiques et de ne pas avoir un +pipe+ � la hauteur de la qualit� des concurrents", a indiqu� Davis, r�cent vainqueur dans le +SuperPipe+ d'Aspen des X Games, la comp�tition de r�f�rence dans ce milieu.�
Connu pour son franc-parler, Davis avait estim� ce week-end que les organisateurs avaient embauch� "les mauvaises personnes" pour construire cette structure de neige de 234 m de long et 81 m de d�nivel�, dont les parois, initialement jug�es bien trop verticales par certains concurrents, ont �t� adoucies.�
Le double champion olympique de la sp�cialit� Shaun White a confi� lundi soir apr�s le dernier entra�nement que le +pipe+ avait encore besoin de travail.�
"J'esp�re qu'ils vont trouver un moyen de rendre ce pipe un peu facile � manoeuvrer car c'est clairement loin d'�tre parfait", avait soulign� l'Am�ricain.�
"Le milieu (la partie plate de transition entre les deux parois, ndlr) est compl�tement mou et lourd", a ajout� White. Les temp�ratures douces et l'humidit� ambiante ont en effet ramolli la neige ces derniers jours et ont d�t�rior� la structure du half-pipe.�
"C'est d�j� arriv� ailleurs mais c'est emb�tant quand �a arrive aux jeux Olympiques", a dit l'Am�ricain de 27 ans, donn� favori.�
L'Am�ricaine Hannah Teter, double m�daill�e olympique dans cette �preuve, n'a pas pu mettre en place certaines de ses nouvelles figures � l'entra�nement � cause de la mauvaise condition du demi-cylindre. "C'est un peu dangereux, c'est pourri", a-t-elle dit apr�s l'ultime entra�nement.�
Championne olympique en titre, l'Australienne Torah Bright a �galement critiqu� les organisateurs alors que son fr�re et entra�neur Ben a eu des mots durs envers la F�d�ration internationale de ski (Fis), dans des propos � la presse australienne.�
Cette mini-pol�mique autour de la qualit� du half-pipe est revenue aux oreille du Comit� international olympique (CIO).�
Son porte-parole Mark Adams a indiqu� mardi qu'il n'y avait "pas de probl�me avec le half-pipe lui-m�me" mais que, comme pour tous les sports sur neige, c'�tait "plus difficile quand il fait un peu chaud". �
Par
