TITRE: R�sultats recherche lesechos.fr
DATE: 2014-02-11
URL: http://www.lesechos.fr/entreprises-secteurs/finance-marches/actu/afp-00582798-wall-street-en-legere-hausse-a-la-veille-d-une-audition-de-janet-yellen-649273.php
PRINCIPAL: 161877
TEXT:
La Filature, tisser les liens de l'entrepreneuriat
09/04/2014 Entrepreneurs - lesechos.fr
La Fondation Entreprendre lance un nouvel espace d�di� � la cr�ation d'entreprise. Baptis� La Filature, il accueille plusieurs associations qui sensibilisent les jeunes et soutiennent les cr�ateurs.
R�forme territoriale : "On peut dire que Valls a jet� des pav�s dans la mare"
09/04/2014
Roland Ries, maire de Strasbourg et s�nateur du Bas-Rhin, donne son point de vue sur la volont� de simplification du "mille-feuille" administratif et la suppression des d�partements.
Pourquoi Serge Papin (Syst�me U) arr�te Twitter
09/04/2014 Directions Num�rique - Les Echos
Patron actif sur Twitter, Serge Papin annonce qu'il stoppe son activit� sur ce r�seau social au profit de messages sign�s par son groupe, la coop�rative Syst�me U.
Les attaques cibl�es, nouveau dada des cybercriminels
09/04/2014 Directions Num�rique - Les Echos
Men�es par des cyberpirates professionnels, les attaques cibl�es contre les particuliers ou les entreprises ont quasiment doubl� l'an pass�.
Au Japon, place au � show-rooming �
09/04/2014 Directions Num�rique - Les Echos
Un site de vente en ligne de v�tements reverse des commissions aux magasins physiques, o� les clients ont rep�r� ou essay� l'habit command�.
