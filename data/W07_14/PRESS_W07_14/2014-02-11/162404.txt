TITRE: Nokia va pr�senter un Smartphone fonctionnant avec Android - LExpress.fr
DATE: 2014-02-11
URL: http://lexpansion.lexpress.fr/high-tech/nokia-va-presenter-un-smartphone-fonctionnant-avec-android_428258.html
PRINCIPAL: 162398
TEXT:
Nokia va pr�senter un Smartphone fonctionnant avec Android
Par Reuters, publi� le
10/02/2014 � 21:25
, mis � jour � 21:25
Nokia va pr�senter un nouveau smartphone bas de gamme fonctionnant avec le syst�me d'exploitation Android de Google alors que le groupe finlandais est sur le point de c�der sa division t�l�phones portables � Microsoft et son syst�me d'exploitation Windows Phone, rapporte lundi le Wall Street Journal.
Selon le Wall Street Journal, Nokia va pr�senter un nouveau smartphone bas de gamme fonctionnant avec le syst�me d'exploitation Android de Google. Le groupe finlandais est sur le point de c�der sa division t�l�phones portables � Microsoft et son syst�me d'exploitation Windows Phone. (Reuters/Dado Ruvic)
REUTERS
Ce nouveau mod�le sera pr�sent� au Mobile World Congress de Barcelone, le plus grand salon de l'industrie de la t�l�phonie mobile, organis� � la fin du mois, ajoute le quotidien, citant des sources proches du dossier non identifi�es.�
Nokia et Microsoft ont refus� de commenter l'information.�
Le g�ant des logiciels entend finaliser au cours du trimestre en cours le rachat de l'activit� mobiles du groupe finlandais pour 5,4 milliards d'euros.�
Microsoft et Nokia n'ont pas r�ussi � s'imposer sur le march� des smartphones, domin� par Samsung et Apple.�
Selon le cabinet d'�tudes Strategy Analytics, 79% des smartphones vendus l'an dernier fonctionnaient avec Android, 15% avec iOS, le syst�me d'exploitation d'Apple et 4% avec Windows Phone.�
Bill Rigby, avec la contribution de Ritsuko Ando � Helsinki, Benoit Van Overstraeten pour le service fran�ais�
Par
