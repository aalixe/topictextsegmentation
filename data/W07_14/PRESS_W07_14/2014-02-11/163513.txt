TITRE: Maire depuis 1947, il renonce aux municipales
DATE: 2014-02-11
URL: http://www.lefigaro.fr/flash-actu/2014/02/11/97001-20140211FILWWW00123-maire-depuis-1947-il-renonce-aux-municipales.php
PRINCIPAL: 163511
TEXT:
le 11/02/2014 à 11:58
Publicité
Roger Sénié compte 66 années de mandat à son actif. Cette longévité fait de lui le co-doyen des maires de France. Mais, à 93 ans, l'élu ne briguera pas une 12e fois la mairie de La Bastide-de-Bousignac (Ariège), rapporte mardi le quotidien La Dépêche du Midi . "J'ai pris ma décision il y a quelques jours, j'aurai 94 ans dans trois mois, un âge où on peut être fatigué, ma famille et mes proches ont mis le doigt où il fallait", a expliqué l'édile, qui envisageait il y a encore quelques semaines de se représenter dans sa commune de 330 habitants.
Roger Sénié a été élu pour la première fois en 1947. Il se flatte d'avoir toujours été élu au premier tour depuis. Il s'enorgueillit aussi d'être le plus ancien maire à son poste avec son homologue Arthur Richier de Faucon-du-Caire (Alpes-de-Haute-Provence), âgé lui de 92 ans. L'édile, "vieux lutteur gaulliste" selon La Dépêche, était resté très actif lors de son dernier mandat. En mars 2013, il démissionnait avec fracas pour protester contre une perte fiscale de 145.000 euros liée au rattachement de sa commune à la communauté de communes de Mirepoix.
Il est allé jusqu'à la grève de la faim en vain, mais a été conforté par les électeurs en juillet lors d'une nouvelle élection municipale.
Le vieil homme n'assistera pas en spectateur à l'élection des 23 et 30 mars. "Je vais m'en occuper très sérieusement comme pour moi", indique-t-il en précisant qu'il soutiendra à fond une liste de ses amis" qui a de fortes chances d'être conduite par une femme".
Ce ne sera pas selon lui une liste UMP qui s'opposerait à une liste de gauche. "Il y aura des gens de toutes sortes chez nous, mais aussi dans l'autre liste", assure-t-il.
Roger Sénié n'en promet pas moins une lutte âpre. "En face de nous, il y a un transfuge à qui j'avais mis le pied à l'étrier il y a six ans, mais qui nous a abandonnés dans la bataille l'an dernier", lance-t-il.
