TITRE: Tsonga s�est r�veill�, Mathieu a ferraill� - ATP - Tennis -
DATE: 2014-02-11
URL: http://sport24.lefigaro.fr/tennis/atp/actualites/tsonga-s-est-reveille-mathieu-a-ferraille-678404
PRINCIPAL: 0
TEXT:
Tsonga s�est r�veill�, Mathieu a ferraill�
Par La  r�daction, 10-02-2014
Tweeter
Tsonga a domin� Mayer. -  AFP
Les deux Fran�ais se sont qualifi�s pour le 2e tour du tournoi de Rotterdam, lundi. Tous les deux ont eu besoin de trois sets.
Jo-Wilfried Tsonga et Paul-Henri Mathieu se sont qualifi�s pour le 2e tour du tournoi ATP de Rotterdam, lundi, en dominant respectivement en trois sets Florian Mayer et Ivan Dodig. Tsonga eu besoin de 1h27 de jeu pour dominer l'Allemand Mayer (4-6, 6-3, 6-1), qui avait pris un meilleur d�part. Le num�ro 10 mondial, finaliste du tournoi en 2011, a eu besoin d'un jeu pour r�gler la mire, notamment sur son service (80% de r�ussite en premi�re balle). Au tour suivant, le Manceau affrontera le Tch�que Lukas Rosol ou le Croate Marin Cilic.
Paul-Henri Mathieu, 135e joueur mondial, s'est bagarr� pendant 2 h 15 minutes pour se d�faire du Croate Dodig (33e) 4-6, 7-6, 6-4. Le Fran�ais s'est montr� performant sur les balles importantes, en transformant notamment 5 des 6 balles de break qu'il s'�tait offertes, tandis que la machine � aces (21) de Dodig s'est enray�e dans la derni�re manche (4). �PMH� aura la lourde t�che d'affronter le vainqueur du match opposant Ga�l Monfils (23e), gagnant dimanche du 5e titre de sa carri�re � Montpellier, � l'Argentin Juan Martin Del Potro, tenant du titre et num�ro 4 mondial.
R�sultats du 1er tour :
Mathieu (Fra, q)�- Dodig (Cro) 4-6, 7-6 (7-2), 6-4
Berrer (All, q)�- Galung (P-B, wc) 6-4, 2-6, 6-3
Berdych (Rtc, 3)�- Seppi (Ita) 6-3, 6-3
Tsonga (Fra, 5)�- Mayer (All) 4-6, 6-3, 6-1
Kohlschreiber (All) -�Stakhovsky (Ukr, q) 6-2, 7-5
A lire aussi
