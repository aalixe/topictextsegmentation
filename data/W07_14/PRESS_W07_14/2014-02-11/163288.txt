TITRE: La Chine et Ta�wan nouent un dialogue officiel historique  - RTL info
DATE: 2014-02-11
URL: http://www.rtl.be/info/monde/international/1068522/la-chine-et-taiwan-nouent-un-dialogue-officiel-historique
PRINCIPAL: 163284
TEXT:
La Chine et Ta�wan nouent un dialogue officiel historique
Afp | 11 F�vrier 2014 11h39
R�agir (0)
La Chine et Ta�wan ont nou� mardi un dialogue historique entre leurs gouvernements, pour la premi�re fois depuis la fin de la guerre civile en 1949, dans un contexte de d�gel des tensions et d'espoir d'�changes accrus au-dessus du d�troit de Formose.
Ces entretiens d'un niveau in�dit ont pour cadre hautement symbolique Nankin, la ville de l'est que le camp nationaliste de Chiang Kai-shek avait choisie comme capitale.
Wang Yu-chi, officiel ta�wanais charg� des relations avec la Chine continentale, y a rencontr� mardi en d�but d'apr�s-midi son homologue Zhang Zhijun, chef du Bureau chinois des Affaires ta�wanaises.
La pi�ce dant laquelle s'est d�roul�e leur entrevue avait �t� d�cor�e de fa�on neutre, sans drapeau visible ni titre officiel affich� sur la table des discussions, afin de m�nager les sensibilit�s.
Cette visite de M. Wang repr�sente une "perc�e importante", a estim� mardi Chine nouvelle, l'agence de presse officielle du r�gime communiste. Une port�e toutefois � relativiser, les experts n'imaginant pas que les dirigeants des deux territoires se rencontrent dans un avenir proche.
Mais cette premi�re entrevue entre deux officiels gouvernementaux illustre les efforts men�s depuis quelques ann�es de part et d'autre du d�troit de Formose pour panser les plaies de la guerre civile ayant d�bouch� en 1949 sur une partition entre "R�publique de Chine" et "R�publique populaire de Chine".
Cette ann�e-l�, deux millions de Chinois fid�les au chef nationaliste Chiang Kai-shek, d�fait par les hommes de Mao, se r�fugient sur l'�le de Ta�wan.
Depuis, P�kin et Taipei revendiquent s�par�ment leur pleine autorit� sur la Chine.
P�kin consid�re Ta�wan comme lui appartenant et n'a pas renonc� � la r�unification, par la force si n�cessaire. Mais les relations entre les deux entit�s se sont apais�es depuis l'�lection en 2008 du pr�sident Ma Ying-jeou.
Cet artisan de la renaissance du parti Kuomintang (KMT) -- l'ancien ennemi nationaliste -- a �t� r��lu en 2012 et est favorable � des liens avec la Chine continentale.
- Coop�ration �conomique -
Apr�s de timides contacts dans les ann�es 1990, Ta�wan et la Chine communiste ont franchi en 2010 une �tape d�cisive sur la voie du d�gel en signant un accord-cadre de coop�ration �conomique sous l'impulsion de Ma Ying-jeou.
Cet accord, et d'autres gestes d'ouverture comme la reprise des vols a�riens directs, n'ont toutefois �t� n�goci�s que par des organismes semi-officiels, P�kin et Taipei n'ayant toujours aucune relation diplomatique.
"Ce voyage a des implications cruciales pour poursuivre l'institutionnalisation des relations entre les deux rives du d�troit" de Formose, a d�clar� M. Wang � la presse lors de l'annonce de son d�placement fin janvier.
"En tant que premier pr�sident du Conseil des Affaires continentales, j'ai conscience que ma responsabilit� est grande et que le chemin sera long", a-t-il ajout�.
Le diplomate doit notamment �voquer la cr�ation de bureaux de liaison, l'int�gration �conomique r�gionale et l'acc�s aux soins m�dicaux des �tudiants ta�wanais en Chine. La question de la libert� de la presse doit aussi �tre abord�e.
En fonction de ce qu'il produira, le s�jour de M. Wang en Chine pourrait poser le premier jalon d'une rencontre entre Ma Ying-jeou et le pr�sident chinois Xi Jinping, estime George Tsai, politologue � Taipei.
N�anmoins "les deux parties cherchent les pierres sous leurs pieds en traversant la rivi�re", dit-il, citant un proverbe chinois sur la prudence.
Le sommet des dirigeants de l'Apec (Coop�ration �conomique des pays d'Asie-Pacifique) � P�kin en octobre serait � cet �gard une occasion id�ale, selon Wang Yu-chi.
Le pr�sident chinois Xi avait rencontr� l'ancien vice-pr�sident ta�wanais Vincent Siew en marge du dernier sommet de l'Apec en Indon�sie en octobre 2013. La Chine s'oppose toujours � la pr�sence officielle de dirigeants ta�wanais au sein de l'organisation Asie-Pacifique.
Ta�wan aura � coeur durant ces entretiens de faire avancer des dossiers pratiques sur l'�conomie ou la s�curit�. La Chine, elle, vise le retour de Ta�wan dans son giron, observe Jia Qingguo, professeur � l'universit� de P�kin.
La Chine "attache probablement plus d'importance � l'acc�l�ration de l'int�gration �conomique, en pensant � l'unification politique � plus long terme", estime-t-il.
Le chemin reste long et sem� d'emb�ches jusqu'� une normalisation des relations entre P�kin et Taipei mais le rendez-vous de Nankin a une forte "port�e symbolique", selon lui.
"Il accro�t la confiance de part et d'autre", souligne Jia Qingguo.
Galerie - La Chine et Ta�wan nouent un dialogue officiel historique
