TITRE: Caract�ristique iPhone 6 :  l'�cran incassable gr�ce � un verre en saphir ?
DATE: 2014-02-11
URL: http://www.gentside.com/iphone-6/caracteristique-iphone-6-l-039-ecran-incassable-grace-a-un-verre-en-saphir_art59059.html
PRINCIPAL: 163095
TEXT:
Publi� par Philippe Meignan , le
11 f�vrier 2014
Alors que de nombreuses rumeurs r�centes faisaient �tat du test actuel de prototype d'iPhone 6 �quip� en verre de saphir, une nouvelle information ferait �tat de la construction de pas moins de 200 millions d'�cran avec ce mat�riau...Mais � quel fin ?
Alors que les rumeurs faisaient �tat de la production de prototype d'iPhone 6, avec �cran en verre de saphir, actuellement en phase de test de solidit� chez les fournisseurs de la firme de Cupertino, une nouvelle information fait savoir que ce chiffre serait, en fait, tr�s sous �valu�.
En effet, selon les �crits donn�s par Matt Margolis au site 9to5Mac, l'usine Apple de Mesa, situ�e en Arizona, serait actuellement en train de produire jusqu'� 200 millions d'�crans avec ce mat�riau. Ce chiffre repr�senterait la production pour un iPhone mais pour... une ann�e ! Pourquoi commander autant d'�crans de ce type ?
Certains commencent � en avoir une petite id�e : cette production ne concernerait pas que le seul �cran de l'iPhone 6, mais �galement la fa�ade arri�re, rendant, de fait, le smartphone pratiquement incassable.
D'apr�s d'autres documents transmis Matt Margolis, ce chiffre est le r�sultat de 518 unit�s de production pouvant, par ann�e, produire 103 � 115 millions d'�crans en verre de saphir de 5 pouces. � ce chiffre d'unit�s de production se sont jointes 420 suppl�mentaires poussant donc le rendement total � pr�s de 200 millions d'unit�s produites possible.
Un iPhone 6 enti�rement coqu� de fibre de saphir ? Voil� un argument de choc et de charme pour Apple, mais reste � savoir quel impact sur le prix du smartphone cette adjonction aura.
Suivez-nous sur Facebook
Vous �tes d�j� abonn� ? Ne plus afficher
Suivre
