TITRE: S�curit� routi�re. Deux nouvelles vid�os sur les dangers de la route
DATE: 2014-02-11
URL: http://www.ouest-france.fr/securite-routiere-deux-nouvelles-videos-sur-les-dangers-de-la-route-1922440
PRINCIPAL: 163222
TEXT:
S�curit� routi�re. Deux nouvelles vid�os sur les dangers de la route
France -
Achetez votre journal num�rique
Le cin�aste R�mi Bezan�on a sign� deux courts-m�trages pour la S�curit� routi�re, sur les dangers de l'alcool et du t�l�phone au volant.
R�alisateur de Ma vie en l'air, Le Premier Jour du reste de ta vie ou Un heureux �v�nement, R�mi Bezan�on a pr�t� ses talents � la S�curit� routi�re, en tournant deux courts-m�trages, qui seront diffus�s sur Internet et dans les salles obscrues, avant les projections.
Pas d'images choc
Aucune image d'accidents ou de t�le froiss�e dans ces deux film de cinq minutes. Juste de l'�motion, pour sensibiliser le spectateur.
Le Sourire du pompier raconte l'histoire d'un couple (M�lanie Barnier et Baptiste Lecaplain), frapp� par un accident de la route qui a clou� la femme dans un fauteuil roulant.
Je vous aime tr�s fort montre la touchante relation d'un p�re (Ari� Elmaleh) avec sa petite fille, apr�s la mort de sa m�re, victime d'un accident de la route alors qu'elle t�l�phonait.
Lire aussi
