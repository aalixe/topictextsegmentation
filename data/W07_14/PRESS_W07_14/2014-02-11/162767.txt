TITRE: Catherine Breillat ("Abus de faiblesse"): "J��tais la proie id�ale pour Christophe Rocancourt" � metronews
DATE: 2014-02-11
URL: http://www.metronews.fr/culture/catherine-breillat-j-etais-la-proie-ideale-pour-christophe-rocancourt/mnbj!X08kXnbj4hvWY/
PRINCIPAL: 162762
TEXT:
Cr�� : 15-02-2014 20:00
Catherine Breillat : "J��tais la proie id�ale pour Christophe Rocancourt"
INTERVIEW - La cin�aste Catherine Breillat, victime de l�escroc Christophe Rocancourt apr�s un AVC, romance cette histoire dans "Abus de Faiblesse", un film semi-autobiographique. Rencontre avec une force de la nature et une r�alisatrice toujours amoureuse de la perversion.
�
En d�pit du sujet, vous affirmez que ce film n�est pas autobiographique. Vraiment ?
Pas plus que les autres. Je suis certes partie d�un fait divers m�impliquant directement mais je l�ai romanc�. Vilko et Maud sont des versions cin�matographiques et fantasm�es de Rocancourt et moi. Ce film n�est ni un biopic�ni l�adaptation du livre du m�me nom que j�avais �crit alors que je ne comprenais toujours pas ce qu�il m��tait arriv�.
Et aujourd�hui, comment l�expliquez-vous�?
Quand Rocancourt est entr� dans ma vie, je l�ai vu comme un sauveur. Il me sortait, venait me chercher � l�h�pital, me coupait ma viande... Mais ce que je prenais comme une pr�sence r�confortante et bienveillante apr�s mon AVC �tait en r�alit� du harc�lement. Il profitait de mon hypersensibilit� et de mes absences li�es � la prise de m�dicaments pour me manipuler et me faire signer des ch�ques sans que je m�en souvienne. J��tais la proie id�ale pour ce type de pr�dateur.
Comment avez-vous pens� � Kool Shen pour l�incarner�?
Je voulais un corps de rappeur qui puisse prendre d�embl�e possession des lieux, de la vie de mon h�ro�ne. En faisant des recherches sur Joey Starr que je trouvais trop sexuel pour le r�le, j�ai d�couvert Kool Shen. Il respirait l�intelligence et avait le charisme dont j�avais besoin. Quant � Isabelle, c�est une grande amie. J�ai touch� sa corde sensible en lui disant qu�il valait mieux tourner avec moi tant que j��tais vivante.
"Je suis infirme mais j�ai la chance d��tre vivante"
Malgr� le handicap et l�abus de faiblesse, le film n�est ni plombant ni � charge...
La sinistrose de la maladie, tr�s peu pour moi. Je suis infirme mais j�ai la chance d��tre vivante. Je refuse de me plaindre ou d�en faire des caisses. Pour le reste, j��tais plus victime dans la vie qu�� l��cran. Mais le manich�isme au cin�ma n�a aucun int�r�t.
Christophe Rocancourt pourrait-il vous poursuivre en justice pour le film�?
Non car il m�a attaqu�e et a gagn� un euro symbolique pour le livre qui racontait d�j� cette histoire.  Or, en France, on ne peut pas attaquer deux fois pour le m�me motif. Sans y �tre tenue, j�ai en revanche fait lire le sc�nario � Sonia (Rolland, ex femme de Rocancourt)�: je voulais m�assurer que rien ne la blesse. Quand j��tais chez eux, c�est elle qui s�occupait de moi. Elle a �t� ma grande s�ur, mon amie, ma m�re.
Comment allez-vous aujourd�hui�?
Une partie de mon corps ne m�ob�it plus mais je suis redevenue moi. Lui en revanche a tout perdu�: une femme et une petite fille extraordinaires, un projet de film sur sa vie, l�attention des m�dias... Il est retourn� � la case d�part.
Vous lui avez pardonn�?
Cette question ne se pose pas�: pour moi, ce n�est pas une personne.
�
