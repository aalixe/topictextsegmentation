TITRE: Neuf mineurs arr�t�s pour le viol collectif d'une adolescente
DATE: 2014-02-11
URL: http://www.lemonde.fr/societe/article/2014/02/11/neuf-mineurs-arretes-pour-le-viol-collectif-d-une-adolescente-dans-l-essonne_4364469_3224.html
PRINCIPAL: 0
TEXT:
Neuf mineurs arr�t�s pour le viol collectif d'une adolescente
Le Monde |
� Mis � jour le
11.02.2014 � 16h57
Neuf mineurs ont �t� arr�t�s lundi 10 f�vrier dans le cadre d'une enqu�te sur le viol collectif d'une adolescente en mai dernier, a-t-on appris mardi aupr�s du parquet d'Evry, confirmant une information de RTL.
Les faits remontent au 28 mai. La victime, �g�e de 17 ans, aurait �t� viol�e dans un parc public de Sainte-Genevi�ve-des-Bois, dans l'Essonne, par de nombreux individus. Selon le parquet d'Evry, les neuf interpell�s reconnaissent ��tous plus ou moins�� avoir �t� pr�sents sur les lieux mais ��nient avoir eu des relations sexuelles avec la victime��.
��Il s'agit d'une deuxi�me vague d'interpellations dans cette affaire��, a pr�cis� R�mi Crosson du Cormier, procureur adjoint au tribunal d'Evry, ajoutant que les adolescents avaient �t� identifi�s gr�ce � des photos notamment. Dix-neuf mineurs avaient d�j� �t� arr�t�s en juin. Parmi eux, neuf avaient �t� mis en examen et deux avaient reconnu les faits, confondus par leur ADN, retrouv� sur les lieux du drame.
Soci�t�
