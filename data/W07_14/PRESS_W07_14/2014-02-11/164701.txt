TITRE: Sotchi : la vid�o d'une skieuse aux seins nus fait pol�mique au Liban
DATE: 2014-02-11
URL: http://www.francetvinfo.fr/sports/jo/sotchi-la-video-d-une-skieuse-seins-nus-fait-polemique-au-liban_527453.html
PRINCIPAL: 164696
TEXT:
Tweeter
Sotchi : la vid�o d'une skieuse aux seins nus fait pol�mique au Liban
Le making-of d'une s�ance photo r�alis�e par la skieuse Jackie Chamoun en 2010 inqui�te le gouvernement libanais, qui veut "prot�ger la r�putation" du pays.
Capture d'�cran d'une vid�o montrant le making-of d'une s�ance photo de la skieuse libanaise�Jackie Chamoun, en 2010. (YOUTUBE / FRANCETV INFO)
Par Francetv info avec AFP
Mis � jour le
, publi� le
11/02/2014 | 16:50
Rattrap�e par son pass�, en pleine comp�tition. Une skieuse libanaise participant aux JO de Sotchi est � l'origine d'une pol�mique dans son pays, apr�s la diffusion d'une vid�o la montrant seins nus, lors d'une s�ance photo r�alis�e en 2010. Les autorit�s r�clament une enqu�te.
La vid�o, qui est apparue sur YouTube et a �t� reprise largement sur Facebook et Twitter, mardi 11 f�vrier,�montre la sp�cialiste de ski alpin Jackie Chamoun,�poitrine nue et seulement v�tue d'un slip, participant � une s�ance photo organis�e � la c�l�bre station de ski libanaise de Faraya. Lors de la prise de photos�destin�e � un calendrier,�la jeune femme, alors �g�e de 19 ans, cache ses seins avec des skis.
Apr�s une avalanche de commentaires sur le net, partag�s entre critiques et �loges, la slalomeuse libanaise s'est excus�e publiquement sur sa page Facebook , affirmant que la vid�o n'�tait pas destin�e � �tre diffus�e : seules les photos devaient l'�tre. La vid�o "ne devait pas �tre rendue publique", a-t-elle �crit, reconnaissant que "le Liban est un pays conservateur et qu'il ne s'agit pas de l'image qui refl�te notre culture".
"Tout ce que je vous demande, c'est d'arr�ter de la diffuser, car cela m'aidera � me concentrer sur ce qui est important en ce moment : mon entra�nement et la comp�tition", a plaid� la jeune athl�te.
Le gouvernement inquiet pour "la r�putation du Liban"
Le ministre libanais de la Jeunesse et des Sports, Fay�al Karam�, a r�agi vivement, en r�clamant des mesures au Comit� olympique libanais en vue de "prot�ger la r�putation du Liban". Le chef de la d�l�gation olympique libanais � Sotchi, Fadi Kairouz, a indiqu� que le Comit� olympique libanais allait se r�unir, mardi�apr�s-midi, pour prendre une d�cision.
Le�Liban�est consid�r� comme l'un des pays les plus lib�raux du monde arabe en raison de la libert� vestimentaire qui est accord�e dans le pays ; la consommation d'alcool y est aussi autoris�e. Mais une grande partie de la soci�t� reste profond�ment attach�e aux conventions sociales.
