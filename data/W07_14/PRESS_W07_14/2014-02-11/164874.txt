TITRE: Omnisport | JO Sotchi : Une skieuse libanaise au c�ur d�une pol�mique
DATE: 2014-02-11
URL: http://www.le10sport.com/omnisport/jo-sotchi-une-skieuse-libanaise-au-coeur-dune-polemique135682
PRINCIPAL: 164868
TEXT:
Envoyer � un ami
Engag�e lors des Jeux Olympiques de Sotchi, la skieuse libanaise Jackie Chamoun fait beaucoup parler d�elle ce mardi. La raison�? Des photos et une vid�o dans lesquels elle appara�t d�nud�e. D�licat pour une jeune femme issue d�un pays conservateur. Alors que ces photos ont �t� prises dans le cadre d�un calendrier autrichien avec d�autres skieuses, elle s�est exprim�e via sa page Facebook�: ��Je tiens � pr�senter mes excuses � vous tous, je sais que le Liban est un pays conservateur, et cette image ne refl�te pas notre culture. Les photos et la vid�o font partie du making of et n'�taient pas suppos�s devenir publique. Maintenant que je suis aux Jeux Olympiques, ces photos que je n'avais jamais vues auparavant sont partag�es. C'est triste. � Les autorit�s libanaises r�clament une enqu�te.
�
