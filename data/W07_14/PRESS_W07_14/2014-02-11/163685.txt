TITRE: "Le vote suisse pose des problèmes considérables" - 7SUR7.be
DATE: 2014-02-11
URL: http://www.7sur7.be/7s7/fr/1505/Monde/article/detail/1791036/2014/02/10/Le-vote-suisse-pose-des-problemes-considerables.dhtml
PRINCIPAL: 163679
TEXT:
10/02/14 -  18h17��Source: Belga
� belga.
Le vote suisse en faveur d'une limitation de l'immigration pose des "probl�mes consid�rables", a indiqu� lundi le porte-parole de la chanceli�re allemande Angela Merkel.
"Le gouvernement f�d�ral a pris note de ce r�sultat et le respecte mais de notre point de vue, il est cependant clair qu'il pose des probl�mes consid�rables", a d�clar� Steffen Seibert lors d'une conf�rence de presse r�guli�re du gouvernement.
"Les relations �troites qui lient la Suisse � l'Union europ�enne apportent des deux c�t�s aux populations de grands avantages et la libert� de circulation est au coeur de ces liens �troits", a-t-il ajout�. "La libert� de circulation est pour nous un bien de haute valeur, la chanceli�re l'a toujours soulign�, il appartient d�sormais � la Suisse, au gouvernement suisse, apr�s le r�sultat de ce r�f�rendum, de se rapprocher de l'UE et d'expliquer comment elle entend composer avec ce r�sultat", a-t-il poursuivi, pr�cisant: "Les institutions europ�ennes tireront alors toutes les cons�quences politiques et juridiques de ce vote. L'Allemagne est le premier partenaire commercial de la Suisse.
"Notre int�r�t doit �tre de pr�server une relation entre l'UE et la Suisse aussi �troite que possible de mani�re � ce que les deux parties, les deux partenaires soient en position de conserver leur propre comp�titivit�, dans un contexte de comp�tition globale", a encore soulign� M. Seibert.
Les �lecteurs suisses ont dit "oui" dimanche, � une courte majorit� de 50,3%, � une limitation de l'immigration, ce qui est v�cu par la classe politique nationale comme un coup de tonnerre et un d�saveu mais surtout comme un saut dans l'inconnu pour les relations de la Suisse avec l'Europe.
Lire aussi
