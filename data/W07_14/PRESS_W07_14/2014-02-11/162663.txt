TITRE: R�publique dominicaine: 8 ans de prison pour la Fran�aise en possession de coca�ne - L'Express
DATE: 2014-02-11
URL: http://www.lexpress.fr/actualite/societe/fait-divers/republique-dominicaine-8-ans-de-prison-pour-la-francaise-arretee-avec-de-la-cocaine_1322577.html
PRINCIPAL: 162658
TEXT:
Voter (0)
� � � �
La Fran�aise, �g�e de 41 ans, avait �t� interpell�e avec son mari et leur b�b� � l'a�roport de Puerto Plata (nord), le 29 avril 2012. Dans leur bagage de cabine se trouvaient 11 kilos de coca�ne, selon les autorit�s dominicaines.
afp.com/Kenzo Tribouillard
Une m�re de famille fran�aise a �t� condamn�e en appel lundi � huit ans de prison. Elle est incarc�r�e en R�publique dominicaine depuis avril 2012 apr�s avoir �t� arr�t�e avec 11 kilos de coca�ne dans ses bagages.�
Cette peine, prononc�e par la cour d'appel de Santiago, dans le nord du pays, est identique � celle �mise en avril 2013 par la justice dominicaine, qui avait reconnu coupable de trafic de drogue Liana Guillon, m�re de cinq enfants et originaire de Pontoise (Val-d'Oise).�
"Liana Guillon avait sacrifi� pr�s d'une ann�e de sa libert� � 8.500 kilom�tres de ses enfants, pour pouvoir prouver son innocence, qui lui est une fois de plus refus�e aujourd'hui", a regrett� l'un de ses avocats fran�ais, Ma�tre Philippe Valent . "Liana Guillon, ses enfants et sa famille sont plong�s ce soir dans une immense tristesse", a ajout� l'avocat.�
Le couple s'affirme "pi�g�" par des trafiquants
La Fran�aise, �g�e de 41 ans, avait �t� interpell�e avec son mari et leur b�b� � l'a�roport de Puerto Plata (nord), le 29 avril 2012, alors qu'ils s'appr�taient � monter dans l'avion qui devait les ramener � Paris, apr�s avoir pass� leur lune de miel en R�publique dominicaine.�
Dans leur bagage de cabine se trouvaient 11 kilos de coca�ne, selon les autorit�s dominicaines. Le couple, qui dit avoir laiss� ses bagages � l'h�tel dans une consigne non ferm�e et sans surveillance, nie les faits, affirmant avoir �t� "pi�g�" par des trafiquants.�
Le 29 janvier 2013, la justice avait condamn� Liana Guillon � 8 ans de prison. La cour d'appel avait confirm� ce jugement le 18 avril, mais cette d�cision avait ensuite �t� cass�e par la cour supr�me, en raison d'irr�gularit�s dans la proc�dure. Lors de l'ultime audience le 27 janvier, le repr�sentant du parquet avait toutefois �cart� l'hypoth�se d'une substitution de contenu, et demand� au tribunal de confirmer la condamnation de la quadrag�naire.�
Un ultime recours devant la cour supr�me?
"En absence de toute enqu�te, en absence de tout �l�ment de preuve", Liana Guillon "ne peut accepter" la d�cision rendue ce lundi, souligne Me Valent , qui pointe "les irr�gularit�s constat�es par la cour supr�me". Selon l'avocat, la Fran�aise "envisage de poursuivre son combat pour sa dignit�, son honneur et sa libert�, � travers un ultime recours devant la cour supr�me".�
Le mari de Liana, Christophe Guillon, avait �t� remis en libert� par le tribunal de Puerto Plata � l'issue d'une audience pr�liminaire, le 8 octobre 2012. Son �pouse est rest�e incarc�r�e.�
Avant leur arrestation, le Fran�ais, ing�nieur en informatique, et sa femme, employ�e � la s�curit� sociale, vivaient � Pontoise (Val d'Oise), en r�gion parisienne.�
Avec
