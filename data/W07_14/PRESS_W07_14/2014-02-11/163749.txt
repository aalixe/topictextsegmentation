TITRE: Flappy Bird : cinq bons jeux pour assouvir le manque
DATE: 2014-02-11
URL: http://www.01net.com/editorial/613882/flappy-bird-5-bons-jeux-pour-assouvir-le-manque/
PRINCIPAL: 163747
TEXT:
Tweet
Comment vivre sans Flappy Bird�? Voil� la question que se posent de nombreux possesseurs d�iPhone, qui � force de t�l�chargements compulsifs ont plac� un vulgaire clone mal fichu, Fly Birdie, en t�te de l�App Store � la place du jeu de Dong Nguyen. O� va-t-on�?
La suppression de Flappy Bird semble provoquer un tel vide que certains possesseurs d�iPhone n�h�sitent pas � vendre leur pr�cieux mobile avec l'appli install�e pour des sommes incroyables sur eBay. Et le pire, c�est qu�ils trouvent m�me des acheteurs�: hier, le site d�ench�res a retir� une annonce proposant un iPhone dot� du fameux programme... La derni�re ench�re atteignait 100 000 dollars�!
Il est temps de dire stop � cette folie. Pour vous passer de Flappy Bird tout en �vitant les clones rat�s, la r�daction de 01net vous propose cinq bons jeux en �change. Une cure de d�sintox, en somme�!
Piou Piou contre les cactus (Android)
Mais que fait ce dr�le d�oiseau dans le d�sert, et pourquoi doit-il �viter des cactus�? On ne le sait pas. Mais on conna�t en revanche son auteur, Kek, c�l�bre pour son savoureux blog . Kek estime que Nguyen lui a gentiment piqu� quelques id�es, et a du coup regrett� de n��tre pas devenu millionnaire avec Piou Piou, lanc� il y a deux ans d�j�. Le nom du jeu n��tait peut-�tre pas id�al pour cartonner � l�export. Voil� en tout cas l�occasion de red�couvrir ce titre gratuit, sans pub et rigolo�!
Piou Piou contre les cactus (Google Play Store)
Classic Helicopter (Android)
L�adaptation Android de ce vieux jeu en Flash, sur lequel des millions d�internautes d�soeuvr�s ont pass� des heures au milieu des ann�es 2000, est une r�ussite. Minimaliste, difficile mais parfaitement maniable, il reste une r�f�rence en mati�re d��vitement d�obstacles.��
Classic Helicopter (Google Play Store)
Tiny Wings (iPhone, iPad)
Bien Avant Flappy Bird, ce jeu avait fait sensation il y a quelques ann�es sur l�App Store. Et il n�a en rien perdu de son int�r�t. Ici, votre oiseau n�a pas d�obstacle � �viter, mais doit� progresser au plus vite en profitant des collines comme tremplins. Particuli�rement addictif et vraiment bien r�alis�, il m�rite, si vous n�y avez jamais jou�, de d�penser 0,89 euros�!�
Tiny Wings (App Store) �
Iron Pants (iphone, Android)
Voil� une excellente alternative, bien plus rapide, � Flappy Bird, puisqu�ici vous incarnez un super h�ros. Et il vole vite, le bougre, � tel point qu��viter les caisses de bois (?) qui barrent son chemin est un calvaire. On a vite envie de jeter son mobile par la fen�tre, un bon signe. Seul d�faut�: des pubs trop pr�sentes.�
Iron Pants (App Store)
FlappyDoge (web)
La version de Flappy Bird inspir�e du fameux m�me est �videmment incontournable. Surtout qu�elle reprend quasi � l�identique le gameplay aga�ant de l�original. Et puis un Shiba volant, on en r�ve tous. Such doge amaze. Attention tout de m�me � votre productivit� au bureau.
