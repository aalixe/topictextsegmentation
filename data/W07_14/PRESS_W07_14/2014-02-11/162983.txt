TITRE: NEC LCD-EA244UHD-BK : du 4K au programme - Ecrans, moniteurs - CowcotLand
DATE: 2014-02-11
URL: http://www.cowcotland.com/news/40561/nec-lcd-ea244uhd-bk-4k.html
PRINCIPAL: 162980
TEXT:
NEC LCD-EA244UHD-BK : du 4K au programme
NEC LCD-EA244UHD-BK : du 4K au programme
Post� le 10 f�vrier 2014 � 09:47:09 par Jonathan Riemain �-�1667 lectures 7 commentaires
NEC nous d�voile un nouvel �cran, le LCD-EA244UHD-BK, dot� d'une dalle IPS en 4K, oui cette r�solution tend vraiment � devenir tr�s � la mode. L'�cran pr�sente un design tr�s classique, noir avec des formes tr�s sobres.
L'�cran propose donc une r�solution de 3840 x 2160, avec un contraste de 1000:1, une luminosit� de 350 cd/m�, des angles de vision et un r�tro-�clairage Led.
L'�cran embarque deux hauts-parleurs de 1W et un hub Usb 3.0 4 ports (un entrant, trois sortants). On trouve des entr�es vid�o DVI X2, HDMI (compatible MHL) X2. Le prix n'est pas connu.
