TITRE: Taxis: l'intersyndicale appelle à une grève reconductible
DATE: 2014-02-11
URL: http://www.lefigaro.fr/flash-eco/2014/02/11/97002-20140211FILWWW00321-taxis-l-intersyndicale-appelle-a-une-greve-reconductible.php
PRINCIPAL: 0
TEXT:
le 11/02/2014 à 18:32
Publicité
L'intersyndicale des taxis à l'origine de la manifestation de lundi contre la concurrence des voitures de tourisme avec chauffeur (VTC) a appelé aujourd'hui à "une grève reconductible" jusqu'à "l'arrêt des immatriculations de VTC".
A l'issue de trois heures de réunion, les syndicats ont finalement décidé d'encourager les actions spontanées observées dans Paris et aux abords des aéroports depuis lundi soir. L'intersyndicale (CFDT, CGT, FO, CST, FTI, SDCTP et STM) "soutient toute action défendant la profession" et "appelle l'ensemble des chauffeurs à mener ces actions de manière déterminée mais dans le calme et la dignité". Elle "se réserve le droit de mener des actions en tous lieux et à tous moments", poursuit le communiqué.
"On veut maintenir la pression et accompagner cette spontanéité d'actions", "rien ne sera prémédité", les chauffeurs mèneront "des opérations coups de poings, des blocages", a expliqué à l'AFP Karim Asnoun (CGT). Comme mardi, des actions continueront sur les plateformes aéroportuaires, a-t-il précisé.
"Nous reconduisons le mouvement jusqu'à ce que le gouvernement accepte d'arrêter d'immatriculer de nouveaux VTC" et que "la situation soit clarifiée pour chacun", taxis et VTC, a déclaré Nordine Dahmane (FO). Selon ces deux syndicats, des taxis ont décidé de se rassembler Place de la Concorde. Mais selon la CGT, les CRS "sont en train d'intervenir". A Orly, environ 200 taxis bloquaient toujours à 18H00 la desserte de l'aéroport, selon une source aéroportuaire.
Lire aussi :
