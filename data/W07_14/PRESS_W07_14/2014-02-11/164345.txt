TITRE: Ubisoft catapulte les Lapins crétins sur grand écran
DATE: 2014-02-11
URL: http://www.lefigaro.fr/cinema/2014/02/11/03002-20140211ARTFIG00301-ubisoft-catapulte-les-lapins-cretins-sur-grand-ecran.php
PRINCIPAL: 0
TEXT:
Publié
le 11/02/2014 à 18:10
Les Lapins crétins: scouts toujours plein d'idées lumineuses, et surtout prêts à toutes les bêtises possibles.
Ils sont stupides, givrés et dangereux. La firme bretonne qui les a créés il y a huit ans compte bien les adapter au cinéma d'ici 2015. Retour sur la carrière fulgurante de ces créatures aux lubies farfelues made in France.
Publicité
«Quoi de neuf docteur?» comme dirait Bugs Bunny: le cinéma, pardi! C'est sûr, les Lapins crétins ont les dents longues. L'entreprise française Ubisoft dirigée par le breton Yves Guillemot s'allie à Sony Pictures Entertainment pour adapter au grand écran les aventures des rongeurs timbrés. Prometteuse collaboration entre la maison française spécialisée dans le développement et la distribution de jeux vidéos et le géant américain de la production.
Jean-Julien Baronnet, directeur général d'Ubisoft Motion Pictures vante son partenaire dans l'aventure: «Sony Pictures a une formidable expérience pour développer des blockbusters à la nature hybride entre action réelle et images d'animation pour les publics du monde entier». Hannah Minghella, Présidente pour la Production chez Columbia Pictures applaudit ce nouveau rapprochement qui tend à «étendre les marques d'Ubisoft à de nouveaux publics tout en conservant l'intégrité créative de [leurs] marques.»
Et pour s'étendre, les lapins «sales gosses» n'ont pas besoin de mode d'emploi. Un petit rôle modeste de méchants dans le jeu Rayman imaginés par le Français Michel Ancel , dès la fin 2006, et ils arborent leurs deux dents écartées ainsi que leur regard effaré partout. Ils deviennent les vedettes à part entière d'un jeu vidéo, puis les voilà en bande dessinée, en figurines, en jeux mobiles, et à la télévision: la série télé Lapins Crétins: Invasion a rencontré plus de 2 millions de téléspectateurs sur chaque épisode aux États-Unis.
Les cousins des Minions de Moi, moche et méchant
Le Futuroscope lui aussi, leur a fait une place parmi ses attractions: «The Lapins crétins, La Machine à voyager dans le temps». Un peu comme leurs cousins les Minions dans Moi, moche et méchant, ils baragouinent une sorte de dialecte bien à eux. Et font partie de ces nouvelles créatures obsessionnelles - à l'image de Scratt, l'écureuil fou de L'Âge de glace - qui peuplent l'animation de nos jours. Ils aiment bien aussi se taper dessus.
The Lapins crétins, La grosse aventure, Retour vers le Passé, Les Lapins crétins partent en live�Leurs péripéties ont été déclinées comme celles de Martine ou du Petit Nicolas . Le succès de leur adaptation à la télévision est une performance, d'autres personnages de jeux vidéos n'ont pas eu la facilité de cette reconversion: Street Fighter Prince of Persia, Mortal Kombat...
