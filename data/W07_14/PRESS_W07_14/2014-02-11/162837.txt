TITRE: LCP Assembl�e nationale | "On va interdire aux enfants de regarder Kirikou ?" (Marisol Touraine sur "Tous � Poil")
DATE: 2014-02-11
URL: http://www.lcp.fr/videos/reportages/157090--on-va-interdire-les-enfants-de-regarder-kirikou-marisol-touraine-sur-tous-a-poil
PRINCIPAL: 162835
TEXT:
[Vid�o] "On va interdire aux enfants de regarder Kirikou�?" (Marisol Touraine sur "Tous � Poil")
[Vid�o] "On va interdire aux enfants de regarder Kirikou�?" (Marisol Touraine sur "Tous � Poil")
Commenter cette vid�o�:
"On va interdire aux enfants de regarder Kirikou ?" (Marisol Touraine sur "Tous � Poil")
Publi�e le 11/02/2014 � 10:05
A propos de cette video
La ministre de la Sant� r�pond � Jean-Fran�ois Cop� choqu� par ce livre diffus� dans des �coles.
Invit�e de Politique Matin mardi, Marisol Touraine s�est �tonn�e de la r�action de Jean-Fran�ois Cop� au livre pour enfant "Tous � poil" publi� en 2011. "Je ne sais pas quelle va �tre la prochaine �tape�: on va interdire les enfants de regarder Kirikou�?" demande la ministre, pour qui cette "hyst�risation du d�bat public est sans int�r�t".
Un peu plus tard au cours de l�interview, Marisol Touraine r�agit au "retour" de Nicolas Sarkozy lundi soir au meeting de Nathalie Kosciusko-Morizet�: "le temps du cin�ma muet n�a que trop dur�" lance la ministre des Affaires Sociales.
Vous aimerez aussi
