TITRE: Microsoft gagne beaucoup (beaucoup) d�argent gr�ce � Android | PCWorld.fr
DATE: 2014-02-11
URL: http://www.pcworld.fr/telephonie/actualites,android-rapporte-beaucoup-d-argent-a-microsoft,546431,1.htm
PRINCIPAL: 165087
TEXT:
ENCYCLO
Microsoft gagne beaucoup (beaucoup) d�argent gr�ce � Android
Aussi �tonnant que cela puisse para�tre, le plus gros concurrent de Windows Phone est �galement une source de revenus loin d��tre n�gligeable pour Microsoft.
En effet, selon un rapport d�voil� par nos confr�res de ZDNET , Android aurait rapport� � Microsoft 2 milliards de dollars durant l�ann�e fiscale 2013, gr�ce aux accords de licence pass�s avec Google. Se basant sur ces estimations, les analystes de chez Nomura - qui ont r�dig� le rapport - pensent que chaque smartphone sous Android vendu aurait rapport� 5 dollars � Microsoft.
Une somme d�autant plus �norme lorsqu�on la compare avec les chiffres de Windows Phone, qui n�aurait rapport� � MS �que� 347 millions de dollars, pour un chiffre d�affaires total de 3,3 milliards de dollars. Il faut dire que Microsoft d�pense �norm�ment en marketing pour promouvoir - et tenter d�imposer - son OS mobile, alors que les accords de licence avec Google restent de l�argent �facilement gagn�. Bref, pour le moment, la firme de Redmond n�a donc aucun int�r�t, bien au contraire, � voir Android dispara�tre. Et, de toute fa�on, �a n�est pas pr�s d�arriver�
Sur le m�me th�me
