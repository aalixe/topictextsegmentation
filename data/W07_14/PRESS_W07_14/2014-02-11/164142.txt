TITRE: Virgin Atlantic : des Google Glass et smartwatches Sony pour assurer un meilleur service aux clients
DATE: 2014-02-11
URL: http://www.generation-nt.com/virgin-atlantic-google-glass-smartwatches-sony-pour-assurer-meilleur-service-clients-actualite-1849162.html
PRINCIPAL: 164140
TEXT:
Virgin Atlantic : des Google Glass et smartwatches Sony pour assurer un meilleur service aux clients
Le
par Mathieu M. �|� 1 commentaire(s) Source : The Next Web
Dans l'objectif de rendre son service de vol plus glamour et personnalis�, Virgin Atlantic vient de lancer une p�riode test de six semaines visant � la formation de son personnel d'accueil et volant � la manipulation de Google Glass et de smartwatches Sony.
C'est � l'A�roport Heathrow de Londres que le service est actuellement d�ploy�. Pendant six semaines, les clients de la "upper class " profiteront d'un accueil d'h�tesses �quip�es de Google Glass . Les lunettes devraient permettre au personnel d'enregistrer les vols, d'acc�der � des informations personnalis�es sur les clients concernant leurs destinations, habitudes, pr�f�rences, mais aussi des donn�es concernant la m�t�o du lieu de destination, les �v�nements et un ensemble d'indications permettant de mieux informer les clients.
�"Depuis la minute � laquelle le passager sort de sa limousine avec chauffeur au terminal T3 d'Heathrow et est accueilli par son nom, l'�quipe de Virgin Atlantic �quip� d'une technologie portable commencera l'enregistrement. Dans le m�me temps, le personnel sera capable de mettre � jour les donn�es de derni�re minute du vol du client, de les informer de la m�t�o, des �v�nements locaux de leur destination et de traduire n'importe quelle information depuis une langue �trang�re. Dans le futur, cette technologie pourrait �galement indiquer au personnel de Virgin Galactic les pr�f�rences alimentaires du client et tout ce qui permettrait de proposer des services toujours plus personnalis�s."
Virgin indique que le service devrait remplacer un service existant qui confiait d�j� des informations personnalis�es concernant certains clients privil�gi�s. La soci�t� indique que ces donn�es seront affich�es depuis des Google Glass ou la Smartwatch de Sony, mais rien de tel ne devrait �tre propos� pour les clients voyageant dans des classes inf�rieures.
Difficile de savoir combien de Google Glass seront test�s par Virgin Atlantic pendant ces six semaines de test puisqu'elles sont toujours r�serv�es aux d�veloppeurs du programme Explorer ( et toujours factur�es 1500 dollars la paire).
Compl�ment d'information
