TITRE: Google et Foxconn s'associent pour acc�l�rer le d�veloppement de la robotique
DATE: 2014-02-11
URL: http://www.presse-citron.net/google-et-foxconn-sassocient-pour-accelerer-le-developpement-de-la-robotique
PRINCIPAL: 164147
TEXT:
Robots - 11 f�vrier 2014 :: 16:52 :: Par Axel-Cereloz
Google et Foxconn s�associent pour acc�l�rer le d�veloppement de la robotique
Deux g�ant s�associent pour s�apporter l�un � l�autre et tenter de faire progresser plus rapidement le d�veloppement de la robotique industrielle.
print
L�union fait la force. Le constructeur de produits �lectroniques Foxconn, qui compte parmi ses clients Apple ou encore Microsoft, a d�cid� de faire �quipe avec Google, le g�ant du Web. L�information est relay�e par le Wall Street Journal qui explique que Andy Rubin et Terry Gou travailleraient d�sormais mains dans la main. Andy Rubin, c�est le papa d�Android mais aussi la personne qui dirige la nouvelle section robotique de Google. Terry Gou, c�est le patron de Foxconn. Comme dans chaque partenariat, une relation gagnant-gagnant est recherch�e, qu�elle est-elle ici ?
C�t� Foxconn
L�objectif de Foxconn est d�installer de nombreux robots industriels qui pourront remplacer les ouvriers sur des t�ches simples et r�p�titives. Foxconn est une entreprise r�put�e pour les conditions de travail d�plorables qu�elle offre � ses employ�s. Suite � de nombreuses critiques, le constructeur a d�cid� de remplacer une partie de ses ouvriers par des robots � eux, au moins, ne se plaignent pas ! (ce qui semble �tre la logique appliqu�e par Foxconn)-. Le professionnel des pi�ces �lectroniques a donc choisi de miser sur Google en esp�rant que le g�ant du Web l�aidera � d�velopper plus efficacement ses robots industriels.
C�t� Google
De son c�t�, Google a r�cemment d�cid� de se lancer � corps perdu dans la robotique. Et bien �videmment, quand Google d�cide de faire ce genre de chose, il le fait avec les moyens de Google : huit entreprises sp�cialis�es achet�es en l�espace de quelques mois. Foxconn devrait apporter son expertise pour aider Andy Rubin � mieux g�rer le d�veloppement robotique. Il se pourrait m�me, ceci est une supposition, qu�au bout de cet �change, Google fournisse directement des robots � Foxconn.
Win-Win
Si une chose semble s�re, c�est que les deux compagnies vont s�apporter l�une � l�autre et que ce partenariat am�liorera le d�ploiement robotique des deux entreprises. 2014 sera-t-elle l�ann�e de la robotique pour Google ? Rien ne semble pouvoir arr�ter le g�ant du Web.
