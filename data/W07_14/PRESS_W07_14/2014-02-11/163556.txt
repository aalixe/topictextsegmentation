TITRE: Samsung pourrait partager ce que vous �crivez sur votre smartphone avec des d�veloppeurs
DATE: 2014-02-11
URL: http://www.infos-mobiles.com/samsung/samsung-pourrait-partager-ce-que-vous-ecrivez-sur-votre-smartphone-avec-des-developpeurs/50518
PRINCIPAL: 163555
TEXT:
Accueil � Samsung pourrait partager ce que vous �crivez sur votre smartphone avec des d�veloppeurs
Samsung pourrait partager ce que vous �crivez sur votre smartphone avec des d�veloppeurs
No�mie 11 f�vrier 2014 0
Samsung envisagerait de donner la possibilit� aux d�veloppeurs de savoir plus sur ce que les utilisateurs font sur leurs t�l�phones.�Ce serait ouvrir la porte � l�information et sur les sujets lesquels vous �crivez ou des applications que vous avez sur votre t�l�phone.
Les rapports indiquent que Samsung peut travailler sur un syst�me appel� contexte qui permettrait aux utilisateurs d�enregistrer des informations � partir de leurs appareils mobiles et de le partager avec les d�veloppeurs.�Soi-disant, Contexte pourrait stocker ce que l�utilisateur tape dans votre appareil et les informations stock�es par les capteurs et un registre des applications que vous utilisez et Samsung pourraient envoyer tout cela aux d�veloppeurs pour aider � am�liorer leurs applications.
Bien que jusqu�� pr�sent le projet semble loin d��tre mis sur le march�, m�me si cela se produit, l�id�e que l�entreprise a acc�s � ces informations personnelles ressemble vraiment � quelque chose que peu de nombreux utilisateurs seraient pr�ts � autoriser. Ce service aurait un retard, car il est difficile de savoir si ce service permettrait la Cor�e du Sud de vendre plus de t�l�phones, donc pas tout � fait s�r que le service est lanc�.
En outre, ces rapports sugg�rent �galement que les dirigeants de Samsung et Google se sont r�unis r�cemment pour discuter de la gamme de personnalisation que les d�veloppeurs de t�l�phone ont lorsque vous travaillez avec le syst�me d�exploitation Android. Les informations�assurent que cette r�union a eu lieu juste un jour avant que Google a annonc� la vente de Motorola Mobility.
La soci�t� va organiser une manifestation le 24 F�vrier au Mobile World Congress � Barcelone, o� il aurait pr�vu de communiquer au t�l�phone Galaxy S5.�Un dirigeant a d�clar� que le nouveau smartphone serait lanc� en Mars et Avril.
Le prochain smartphone aura une augmentation �cran en laissant�5,24 pouces, qui restera de Super�AMOLED, mais confirmant la r�solution�QHD�(2560 � 1440 pixels) avec une densit� de pixel resterait � 560 ppi.
Le Samsung Galaxy S5 aura une�cam�ra arri�re de 16 m�gapixels�et un front de 3,2 avec une batterie de 3200 mAh et bien s�r apporter Android 4.4 KitKat install� le syst�me d�exploitation,�qui d�butera la nouvelle interface TouchWiz�que nous avons vu ces derniers jours.
