TITRE: Cyber-espionnage: un Etat derri�re un virus informatique ayant s�vi dans 31 pays - Flash actualit� - High-tech- 11/02/2014 - leParisien.fr
DATE: 2014-02-11
URL: http://www.leparisien.fr/flash-actualite-high-tech/cyber-espionnage-un-etat-derriere-un-virus-informatique-ayant-sevi-dans-31-pays-11-02-2014-3579971.php
PRINCIPAL: 162985
TEXT:
R�agir
Des experts en s�curit� informatique ont annonc� lundi avoir d�couvert un virus d�di� au cyber-espionnage qui aurait frapp� gouvernements et entreprises de 31 pays et serait sponsoris� par un Etat.
Les chercheurs de l'entreprise de s�curit� Kaspersky Lab ont pr�cis� que ce logiciel malveillant, connu sous le nom de "The Mask" ou "Careto", a �t� utilis� au moins depuis 2007 et est particuli�rement complexe.
MON ACTIVIT�
Vos amis peuvent maintenant voir cette activit� Supprimer X
Certaines versions sont capables d'infecter des t�l�phones portables et tablettes, y compris ceux disposant des syst�mes d'exploitation d' Apple ou Google .
Ces experts ajoutent que les concepteurs du virus, qui semblent parler espagnol, pourraient avoir eu recours � ce logiciel malveillant pour voler des documents sensibles comme des cl�s d'encodage.
Les principales cibles sont apparemment des gouvernements et missions diplomatiques, des entreprises du secteur �nerg�tique, des organismes de recherche, des soci�t�s de capitaux priv�s ou encore des militants politiques , selon un document de Kaspersky.
"Pour les victimes, une infection par Careto peut �tre catastrophique", assure l'entreprise de s�curit� dans un communiqu�.
"Careto intercepte tous les canaux de communication et recueille les informations les plus essentielles de l'appareil de la victime. Le d�tecter est extr�mement difficile en raison des capacit�s de discr�tion de ce logiciel furtif, de ses fonctionnalit�s int�gr�es et de ses modules additionnels de cyber-espionnage", explique-t-elle.
Une fois qu'un appareil est infect�, les pirates informatiques peuvent intercepter diff�rentes donn�es comme le trafic internet, les frappes sur le clavier, les conversations via Skype, et voler les informations des appareils connect�s.
Le virus �tait actif jusqu'au mois dernier, lorsque ses serveurs de commande ont �t� arr�t�s durant l?enqu�te de Kaspersky, indiquent les chercheurs.
"Nous avons plusieurs raisons de croire qu'il s'agit d'une campagne sponsoris�e par un Etat", souligne un expert de Kaspersky, Costin Raiu.
Selon lui, les concepteurs du virus sont tr�s qualifi�s et ont jusqu'� pr�sent �t� en mesure de rester cach�s. "Un tel degr� de s�curit� op�rationnelle n'est pas normal pour des groupes cyber-criminels", note-t-il.
"Et le fait que les pirates de Careto semblent parler espagnol est peut-�tre l'aspect le plus �trange" dans cette affaire, ajoute Kaspersky.
"Alors que la plupart des attaques (informatiques) connues de nos jours sont remplies de commentaires en chinois, les langues comme l'allemand, le fran�ais ou l'espagnol sont tr�s rares".
Selon l'enqu�te, 380 victimes issues de 31 pays ont �t� victimes du virus, dont les Etats-Unis, la France, l'Allemagne et la Chine.
