TITRE: PSG | Mercato - PSG : Hazard/Pogba, qui sera le plus facile � d�nicher pour le PSG ?
DATE: 2014-02-11
URL: http://www.le10sport.com/football/ligue1/psg/mercato-psg-hazard-pogba-qui-sera-le-plus-facile-a-denicher-pour-le-psg-135590
PRINCIPAL: 0
TEXT:
�
LE PSG PEUT SE PAYER LES DEUX JOUEURS
Financi�rement, le PSG a largement les moyens de se payer Paul Pogba (environ 40-50M d�euros) et Eden Hazard (environ 60M d�euros), tel n�est pas le probl�me. Le PSG doit convaincre le joueur , l�entourage du joueur et les clubs respectifs. L�, �a se complique. D�j�, quelle est vraiment la priorit� du PSG aujourd�hui�? Paris , comme r�v�l� par le 10 Sport , a d�j� pris un contact avec l�entourage de l � international belge de Chelsea . Le PSG a �galement sond� celui de Paul Pogba . Bref, le PSG avance ses pions. Mais sans se pr�cipiter , en se laissant le maximum de portes de sorties.
�
LE PSG A DAVANTAGE BESOIN DE HAZARD�
Pour Paul Pogba , la porte est clairement ouverte pour l��t� prochain, apr�s la Coupe du monde . Pour plusieurs raisons. D�j�, le joueur peut difficilement refuser de venir au PSG si l�occasion se pr�sente. C�est un beau challenge pour un international fran�ais de rejoindre un club fran�ais qui joue la victoire en Ligue des Champions . Et puis, la Juventus Turin a-t-elle les moyens de refuser une grosse offre pour son milieu de terrain� ? Sans doute pas. Mais le PSG a d�j� Thiago Motta , Marco Verratti , Blaise Matuidi et maintenant Yohan Cabaye dans l�entrejeu� L�int�r�t de recruter Paul Pogba d�s l��t� prochain est-il �vident�?
�
MAIS POGBA EST PLUS ABORDABLE�
La situation est clairement diff�rente pour Eden Hazard . Le Belge est exactement le joueur qui manque au PSG . L� international belge est capable d��voluer de partout , � gauche, dans l� axe , en soutien de l� attaquant , � droite. Seulement, Hazard sera bien plus compliqu� � d�nicher que Pogba . ���Cela fait plaisir de lire que le PSG s�int�resse � moi. Mais �a ne me perturbe pas. Je sais tr�s bien ce que j�ai en t�te. O� je vais. Personne ne va me faire changer d�avis�� , explique Hazard ce mardi dans L�Equipe . D�j�, le PSG doit persuader Hazard que Paris , c�est mieux que Chelsea . C�est tout sauf �vident. Et surtout, Paris doit convaincre Chelsea de l�cher son meilleur joueur. Or, les Blues n�ont pas besoin d�argent et Mourinho compte sur le Belge pour construire le Chelsea de demain. Bref, le PSG � a davantage besoin d�un joueur comme Eden Hazard l��t� prochain mais Paul Pogba est beaucoup plus abordable�
�
Rejoignez-nous et venez en d�battre sur notre page Facebook�: http://www.facebook.com/le10sport
Suivez-nous sur Twitter�:� http://www.twitter.com/le10sport
