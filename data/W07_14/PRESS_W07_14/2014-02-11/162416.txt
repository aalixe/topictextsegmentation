TITRE: Jour J pour Shaun White - 11/02/2014 - leParisien.fr
DATE: 2014-02-11
URL: http://www.leparisien.fr/espace-premium/sports/jour-j-pour-shaun-white-11-02-2014-3578435.php
PRINCIPAL: 162414
TEXT:
Jour J pour Shaun White
Publi� le 11 f�vr. 2014, 07h00
Mon activit� Vos amis peuvent maintenant voir cette activit�
Tweeter
Shaun White, l'une des grandes stars des Jeux olympiques, peut s'offrir aujourd'hui une troisi�me m�daille d'or en snowboard half-pipe. L'Am�ricain de 27�ans, sacr� � Turin puis � Vancouver, �tait arriv� � Sotchi avec l'ambition de faire le doubl� avec le slopestyle, une des nouvelles disciplines olympiques.
Mais le parcours ��intimidant��, un petit p�pin au poignet � l'entra�nement et le niveau spectaculairement �lev� de la concurrence ont dissuad� White de s'�lancer en slopestyle afin d'assurer ses arri�res dans sa discipline f�tiche, non sans essuyer quelques critiques au passage, notamment des Canadiens.
L'�chec en half-pipe est maintenant interdit pour le Californien, habitu� � transformer en or tout ce qu'il touche au point d'avoir construit un empire qui brasse des millions de dollars et d�borde largement le cadre du snowboard pour toucher la mode, la musique et la publicit�.
