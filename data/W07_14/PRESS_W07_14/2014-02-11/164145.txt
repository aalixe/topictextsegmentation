TITRE: Google Glass, lunettes connect�es, r�alit� augment�e - L'Express avec L'Expansion
DATE: 2014-02-11
URL: http://lexpansion.lexpress.fr/high-tech/google-glass_428344.html
PRINCIPAL: 164140
TEXT:
Les Google Glass, un vrai danger pour la vie priv�e?
Dernier avatar de la soci�t� de surveillance? Les lunettes � r�alit� augment�e de Google trouvent d�j� des d�tracteurs alors que leur sortie n'est pr�vue que dans plusieurs mois. Les anti Google Glass �mettent des craintes quant au respect de la vie priv�e.
Ce que vous verrez � travers les Google Glass
A quoi ressemblera la vie � travers les lunettes high-tech de Google ? Une vid�o est l� pour en donner une petite id�e. Command�es � la voix, elles permettront de prendre des photos et des vid�os, d'envoyer des messages, et d'afficher des plans de trajets. D�couvrez l'interface.
