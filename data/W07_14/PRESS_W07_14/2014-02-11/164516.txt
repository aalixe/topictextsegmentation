TITRE: Croissance � mod�r�e � mais marge record�pour L'Or�al
DATE: 2014-02-11
URL: http://www.lemonde.fr/economie/article/2014/02/11/croissance-moderee-mais-marge-record-pour-l-oreal_4364105_3234.html
PRINCIPAL: 164514
TEXT:
| Par Nicole Vulser
La croissance est toujours l�, mais plus � mod�r�e �, comme l'admet le groupe. L' Or �al a annonc�, lundi 10 f�vrier, avoir enregistr�, en 2013, une hausse de 2,3 % de son chiffre d'affaires (+ 5 % � p�rim�tre et taux de change constants), � 22,98 milliards d'euros. En 2012, la croissance avait �t� de 10,4 %. Le b�n�fice net part du groupe, lui, est en augmentation de 3,2 %, � 2,95 milliards d'euros.
L'activit� a notamment �t� p�nalis�e par les effets mon�taires comme l'euro fort. Ceux-ci ont eu un impact n�gatif de 3,7 % sur les ventes mondiales du groupe. Ces r�sultats sont toutefois conformes aux attentes des analystes.
� CONFIANCE � POUR 2014
Les ventes du dernier trimestre ont �t� presque �tales, avec une croissance faible de 0,6 % (5,4 % � p�rim�tre et taux de change constants). Les divisions luxe (Lanc�me, Armani, Kiehl's, Yves Saint Laurent Beaut�) et cosm�tique active (Vichy, La Roche Posay) ont, certes, progress� sur les trois derniers mois de 2013 mais les ventes de produits professionnels et de produits grand public (Garnier, L'Or�al Paris , Maybeline) ont, eux, respectivement recul� de 2,1 % et de 2,4 %. Le tassement de la consommation de produits de beaut� s'est fait sentir , fin 2013, en Europe de l'Ouest et de l'Est, en Asie et en Am�rique latine.
En 2013, seuls Body Shop et les produits professionnels ont vu leurs ventes baisser sur l'ann�e. Mais L'Or�al dit avoir � renforc� ses positions mondiales dans toutes les divisions et toutes les zones g�ographiques �.
Mettant en avant une marge d'exploitation record (3,87 milliards d'euros, soit 16,9 % du chiffre d'affaires contre 16,5 % en 2012), le PDG, Jean-Paul Agon, dit aborder 2014 � avec confiance �. Malgr� les incertitudes mon�taires qui continuent de peser , le leader mondial des cosm�tiques pense � surperformer � nouveau le march� � en 2014 et � r�aliser une nouvelle ann�e de croissance et de r�sultats �.
