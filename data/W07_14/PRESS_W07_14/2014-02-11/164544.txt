TITRE: Nouveaux "trésors" artistiques retrouvés chez un octogénaire allemand - 12 février 2014 - Le Nouvel Observateur
DATE: 2014-02-11
URL: http://tempsreel.nouvelobs.com/monde/20140211.AFP9795/tresor-nazi-60-nouvelles-oeuvres-decouvertes-chez-gurlitt.html
PRINCIPAL: 0
TEXT:
Actualité > Culture > Nouveaux "trésors" artistiques retrouvés chez un octogénaire allemand
Nouveaux "trésors" artistiques retrouvés chez un octogénaire allemand
Publié le 11-02-2014 à 14h45
Mis à jour le 12-02-2014 à 13h15
A+ A-
Soixante nouvelles oeuvres, dont des Monet et Renoir, ont été découvertes chez Cornelius Gurlitt, a annoncé mardi le porte-parole de cet octogénaire allemand propriétaire du "trésor nazi" révélé en novembre. (c) Afp
Berlin (AFP) - Plus de 60 œuvres d'art, dont des Monet, Renoir, Picasso, ont été retrouvées dans la propriété autrichienne d'un octogénaire allemand chez qui un "trésor nazi" avait déjà été découvert en Allemagne, a révélé mardi son porte-parole.
Cornelius Gurlitt, fils d'un marchand d'art au passé trouble sous le Troisième Reich, était apparu sur le devant de la scène en novembre dernier quand la presse allemande a annoncé la découverte au printemps 2012 de plus de 1.400 œuvres de maîtres tels que Chagall ou Matisse, dans son appartement de Munich (sud).
Cette fois-ci, les œuvres se trouvaient dans une maison que l'octogénaire possède à Salzbourg dans le nord de l' Autriche . "Il s'agit de plus de 60 pièces, parmi lesquelles des œuvres de Monet, Renoir ou Picasso", a indiqué Stephan Holzinger, un spécialiste de la communication de crise auquel l'avocat munichois Christoph Edel, chargé d'assurer la tutelle de M. Gurlitt, a confié le rôle de porte-parole du vieil homme.
"Des experts, mandatés par Cornelius Gurlitt, examinent s'il pourrait s'agir d’œuvres éventuellement volées sous le nazisme", précise M. Holzinger dans un communiqué, ajoutant que "d'après un premier état des lieux, un tel soupçon ne s'est pas vérifié". Il n'a pas indiqué la valeur de ces pièces.
Selon le porte-parole, les œuvres ont été placées en sûreté pour prévenir toute tentative de vol. Mais plusieurs questions restent en suspens notamment pourquoi décider de révéler aujourd'hui l'existence de ces œuvres, quels experts vont été chargés d'enquêter sur ces œuvres, ces dernières seront-elles rendues publiques sur Internet comme l'ont été les précédentes?
Interrogé par l'AFP, le parquet d'Augsbourg (sud), en charge de l'affaire Gurlitt s'est contenté d'indiquer que les enquêteurs avaient pris connaissance de cette nouvelle information avec intérêt.
Cornelius Gurlitt, 81 ans, soupçonné de fraude fiscale et recel en Allemagne, a été laissé en liberté et fait l'objet d'une enquête judiciaire, conduite sous l'autorité de ce parquet.
1.400 oeuvres retrouvées
Il a été provisoirement placé sous tutelle pour raisons médicales à la suite d'une requête déposée par les médecins d'un hôpital où il était soigné, une procédure qui le prive en partie de la capacité de prendre seul certaines décisions. Malgré cela M. Gurlitt semble décidé à se battre pour garder "ses" toiles, même s'il a fait des déclarations contradictoires à ce sujet.
M. Gurlitt est le fils de Hildebrand Gurlitt, grand galeriste qui s'était constitué une collection dans les années 30 et 40. Un moment menacé par les nazis, notamment parce qu'il avait une grand-mère juive, il avait ensuite servi le régime en écoulant à l'étranger, moyennant devises, des œuvres volées ou saisies.
Parmi les quelque 1.400 œuvres retrouvées dans l'appartement de Munich de Gurlitt, se trouvent des dessins, gravures et peintures dont des Picasso, Matisse, Renoir, Otto Dix, dont la majeure partie pourrait avoir été soit volée ou extorquée à des familles juives, soit saisie dans les musées comme faisant partie de ce que les nazis classaient dans la catégorie "Art dégénéré".
L'affaire a relancé un débat sur la restitution des œuvres d'art volées aux Juifs sous le nazisme, les autorités allemandes ayant notamment été accusées d'avoir tardé à faire toute la lumière sur les possessions de Gurlitt.
L' Allemagne souhaite se doter d'une loi facilitant ces restitutions. Un projet a été élaboré pour notamment abolir la prescription de trente ans au-delà de laquelle la propriété d'une œuvre d'art ne peut plus être contestée, si le détenteur est considéré comme de "mauvaise foi", c'est-à-dire s'il connaissait la provenance de l'objet au moment de son acquisition.
Baptisée "Lex Gurlitt" par les médias, le texte doit être examiné une première fois vendredi par le Bundesrat, la chambre haute du parlement allemand où siègent les représentants des Etats fédérés.
Partager
