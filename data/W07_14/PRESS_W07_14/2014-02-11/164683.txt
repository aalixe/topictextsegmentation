TITRE: Barack Obama menace les entreprises �trang�res qui prospectent en Iran - LExpress.fr
DATE: 2014-02-11
URL: http://lexpansion.lexpress.fr/economie/barack-obama-menace-les-entreprises-etrangeres-qui-prospectent-en-iran_428444.html
PRINCIPAL: 164682
TEXT:
Barack Obama menace les entreprises �trang�res qui prospectent en Iran
Par L'Expansion.com avec AFP, publi� le
11/02/2014 �  19:58
Le pr�sident am�ricain Barack Obama a promis "une pluie de sanctions" sur les entreprises qui ne respecteraient pas l'embargo international sur le commerce avec l'Iran. La France pourrait bien �tre vis�e.�
Obama: les entreprises �trang�res qui prospectent en Iran le font � "leurs risques et p�rils".
REUTERS/Kevin Lamarque
Le pr�sident am�ricain Barack Obama a pr�venu ce mardi que les entreprises �trang�res qui prospectaient en Iran le faisaient � "leurs risques et p�rils", promettant "une pluie de sanctions" sur celles qui ne respecteraient pas l'embargo international. Fran�ois Hollande , en visite d'Etat � Washington , a lui-m�me mis en garde les entreprises fran�aises qui se sont rendues en Iran dans la d�l�gation de 116 repr�sentants men�e par le Medef International qui s'est rendue � T�h�ran d�but f�vrier .�
La suspension des sanctions "temporaire et cibl�e"
Malgr� l'accord d'�tape de Gen�ve , qui a conduit � la suspension le 20 janvier de certaines sanctions et au d�gel de certains avoirs iraniens � l'�tranger, en �change du gel d'une partie des activit�s nucl�aires sensibles en Iran, les sanctions internationales restent en vigueur, tiennent � rappeler les Etats-Unis. Le secr�taire d'Etat John Kerry avait d�j� averti le ministre des Affaires �trang�res fran�ais Laurent Fabius que la d�l�gation commerciale fran�aise "n'aidait pas les choses" car il ne s'agissait pas de "business as usual" . Son administration avait pr�cis� que "T�h�ran n'est pas ouvert aux affaires car la suspension de nos sanctions est assez temporaire, limit�e et cibl�e".�
En 2002, la France �tait le troisi�me importateur en Iran, notamment avec Renault et PSA . Onze ans plus tard, elle n'�tait plus que le septi�me.�
Suivez L'Expansion
