TITRE: Int�gration�: quelles propositions�?   | LCP.fr | LCP Assembl�e nationale
DATE: 2014-02-11
URL: http://www.lcp.fr/actualites/politique/157091-integration-quelles-propositions
PRINCIPAL: 163453
TEXT:
Int�gration�: quelles propositions�?
Int�gration�: quelles propositions�?
Le 11 f�vrier 2014 � 12h30 , mis � jour le 12 f�vrier 2014 � 11h14, par Aur�lie Marcireau
Commenter / partager :��
���
Une r�union gouvernementale s'est tenue hier sur ce th�me. Pas de r�volution dans les proposition du gouvernement. Le sujet est  �  risques � quelques semaines des municipales !
La plupart des ministres se sont retrouv�s � 17H00 � Matignon pour discuter de la nouvelle "feuille de route" du gouvernement sur l�int�gration et la lutte contre les discriminations, un chantier ouvert peu de temps apr�s l��lection de Fran�ois Hollande. Le gouvernement, a annonc� mardi des mesures visant � am�liorer l�int�gration des immigr�s, mais reste prudent pour ne pas ouvrir un nouveau front de contestation � quelques semaines des municipales.
Un contexte difficile
Mais de groupes de travail en rapports d�experts, ce dossier min� arrive sur la table alors que l�opinion publique est plus crisp�e que jamais sur ces questions�: 66% des Fran�ais pensent qu�il y a trop d��trangers en France, selon un sondage r�cent. Et l��ch�ance �lectorale pr�te � la surench�re.
Dans ce contexte, l�ex�cutif avance avec mesure par peur d��tre accus� de favoriser les communautarismes. Le gouvernement a pr�sent� mardi sa ��feuille de route�� pour l�int�gration des immigr�s et la lutte contre les discriminations, qui ne comprend aucune ��annonce spectaculaire��, ni moyen suppl�mentaire, dans l�espoir d��viter toute controverse.
Une quinzaine de ministres ont adopt� 28 mesures qui seront ��consensuelles, je l�esp�re��, a d�clar� le chef du gouvernement, Jean-Marc Ayrault, � l�issue d�une r�union � Matignon. ��La politique n�est pas l�art de mettre sous le tapis les probl�mes��, a-t-il ajout�, tout en plaidant pour une approche ��apais�e, sereine��.
Exit donc les questions de m�moire, de religion, ou d�ouverture de la fonction publique�: ��On s�est concentr� sur les questions d��galit頻, dit son entourage.
Alors que les Suisses viennent de voter pour r�tablir des ��quotas d�immigr�s�� et que 66% des Fran�ais jugent qu�il y a trop d�immigr�s dans le pays, le climat n�est pas favorable � des avanc�es majeures.
A quelques semaines des �lections municipales, le sujet pr�te encore plus � la surench�re. En d�cembre, la classe politique s�est ainsi enflamm�e apr�s la publication sur le site du Premier ministre de rapports d�experts, qui pr�conisaient notamment d�abolir la loi sur le voile � l��cole.
Dans ce contexte, la principale annonce de la nouvelle ��feuille de route�� porte sur la cr�ation d�un ��d�l�gu� interminist�riel � l��galit� r�publicaine et � l�int�gration��, rattach� au Premier ministre, qui sera nomm� d�ici deux � trois semaines.
Il s�agit d�une structure ��l�g�re�� qui n�empi�tera pas sur les comp�tences des diff�rents ministres et aura pour seule mission de ��coordonner�� leurs actions.
Certains au sein du gouvernement, dont le ministre d�l�gu� � la Ville Fran�ois Lamy, plaidaient pour une structure dot�e de r�els moyens. ��Si la structure avait pris trop de place, les ministres risquaient de se d�fausser��, a justifi� Matignon.
��Piteux renoncement��
Le ministre de l�Int�rieur, Manuel Valls, reste donc comp�tent pour l�int�gration des ��primo-arrivants��, pr�sents depuis moins de cinq ans en France. Il compte mettre l�accent sur l�acquisition du fran�ais, en exigeant un meilleur niveau. La d�livrance des titres de s�jour sera conditionn�e aux progr�s accomplis, a-t-il pr�cis� sans donner de d�tails.
Le dispositif devrait figurer dans le prochain projet de loi immigration, attendu d�ici � la fin de l�ann�e.
Quant � la lutte contre les discriminations, le gouvernement pr�voit surtout de renforcer la formation des agents publics (enseignants, conseillers d�orientation, agents de P�le emploi, inspecteurs du travail).
Il a �galement demand� aux partenaires sociaux de discuter des CV anonymes et des recours collectifs en cas de discriminations, lors de la prochaine conf�rence sociale avant l��t�.
Les autres propositions avaient d�j� �t� annonc�es�: lutte contre le d�crochage scolaire, aide pour les migrants �g�s qui font des aller-retour dans leur pays d�origine, poursuite de la r�novation des foyers de travailleurs migrants, etc.
Dans les derniers jours, le gouvernement a d�cid� de retirer les propositions qui portaient sur la reconnaissance de l�apport des �trangers � la construction de la France (mise en valeur d���oubli�s de l�histoire��, d�veloppement des cours d�arabe et de mandarin dans les �tablissements scolaires, etc.).
��On ne voulait pas partir sur le terrain de l�histoire officielle ou sur celui des m�moires concurrentes��, des mati�res �minemment sensibles, a expliqu� l�entourage du Premier ministre.
��L�agitation que l�opposition -et notamment l�UMP- a provoqu�e quand elle a d�nonc� les rapports propos�s il y a quelques semaines a port� ses fruits��, a imm�diatement comment� le d�put� UMP Thierry Mariani. ��Finalement, le pire a �t� �vit�.��
Cette prudence pourrait �galement rassurer certains d�put�s socialistes qui s�inqui�taient lundi de l�ouverture d�un nouveau front sur un sujet de soci�t�, apr�s le retrait de la loi famille.
Mais elle risque de d�cevoir les personnes concern�es - 5,3 millions d�immigr�s et 6,7 millions de Fran�ais d�origine �trang�re. France Terre d�Asile (FTA) a regrett� ��une vague feuille de route cosm�tique�� quand Patrick Loz�s, ancien pr�sident du Conseil repr�sentatif des associations noires (Cran), a �voqu� un ��piteux renoncement��.
(avec AFP)
+ d�Infos�: Retour sur la pol�mique de d�cembre
Mi d�cembre, la publication par Le Figaro du compte rendu d�taill� d�un rapport sur l�int�gration remis il y a un mois au Premier ministre - et sur lequel il ne s��tait pas encore prononc�-, avait mis le feu aux poudres.
Ce rapport en cinq volets proposait une "politique repens�e" de l�int�gration ax�e sur la lutte contre les discriminations et l��galit� des droits.
Les auteurs, des chercheurs et des experts, qui voulaient  "en finir avec les discriminations l�gales", pr�conisaient entre autres mesures la "suppression des dispositions l�gales et r�glementaires scolaires discriminatoires concernant notamment le +voile+".
Le pr�sident de l�UMP, avait attaqu� le gouvernement�:
"Notre R�publique serait en danger si vous c�diez � cette tentation en mettant en oeuvre, ne serait-ce qu�� minima, un rapport dont l�intention est de d�construire (�) cette R�publique", s�est insurg� Jean-Fran�ois Cop�, qui a tenu � "interpeller solennellement" Fran�ois Hollande sur le sujet dans un communiqu�. Jean-Marc Ayrault avait alors r�pondu que son �quipe ne voulait "�videmment pas" r�introduire les signes religieux � l��cole.
"Ce n�est pas parce que je re�ois des rapports que c�est forc�ment la position du gouvernement",  expliquait M.�Ayrault.
A peine descendu de son avion, � 7.000 km de l�, le pr�sident Hollande, en visite en Guyane, avait  enfonc� le clou�: "Ce n�est pas du tout la position du gouvernement", a-t-il assur�. Interrog� sur les critiques de l�UMP, M.�Hollande a r�pondu�: "Je n�entends pas ce bruit, parce que ces rapports n�ont pas de traduction".
