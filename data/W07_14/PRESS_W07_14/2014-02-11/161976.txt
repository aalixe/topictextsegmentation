TITRE: Démantèlement d'un réseau de voleurs de grands crus bordelais - 10 février 2014 - Le Nouvel Observateur
DATE: 2014-02-11
URL: http://tempsreel.nouvelobs.com/societe/20140210.AFP9708/une-bande-de-voleurs-de-grands-crus-bordelais-demantelee.html?xtor%3DRSS-138
PRINCIPAL: 161974
TEXT:
Actualité > Société > Démantèlement d'un réseau de voleurs de grands crus bordelais
Démantèlement d'un réseau de voleurs de grands crus bordelais
Publié le 10-02-2014 à 13h45
Mis à jour à 17h15
A+ A-
Vingt personnes ont été placées en garde à vue lundi dans le Sud-Ouest et en Ile-de-France, dans le cadre d'une enquête sur une série de vols de grands crus dans les châteaux du Bordelais, pour un préjudice dépassant le million d'euros. (c) Afp
Bordeaux (AFP) - Vingt personnes ont été placées en garde à vue lundi dans le Sud-Ouest et en Ile-de-France, dans le cadre d'une enquête sur une série de vols de grands crus dans les châteaux du Bordelais, pour un préjudice dépassant le million d'euros.
Les interpellations ont mobilisé quelque 300 gendarmes en Haute-Garonne, dans le Tarn-et-Garonne (bien Tarn-et-Garonne), les Pyrénées-Atlantiques, l'Ile-de-France et la Gironde, département où se trouvaient les commanditaires de ce réseau "très structuré" et "professionnel", qui sévissait depuis le mois de juin, a-t-on indiqué à la gendarmerie.
L'enquête était menée depuis six mois par le Groupement de gendarmerie de la Gironde et la section de recherches de Bordeaux pour démanteler ce réseau qui agissait notamment sur "commandes", comme en matière d'oeuvres d'art, et comportait des individus pour certains "très connus de la justice", notamment pour des faits de vols à main armée.
Les braqueurs, "très bien renseignés", procédaient toujours selon un mode opératoire similaire, en se servant de véhicules volés et en dispersant des détergents ou de l'eau de Javel pour effacer toute trace après les effractions. Les véhicules, dérobés lors de "home-jacking", étaient généralement retrouvés brûlés quelques heures après les faits.
Cinq hommes, âgés de 18 à 55 ans, sont soupçonnés de former le noyau dur du réseau, c'est-à-dire les "casseurs". Les autres organisaient le recel, avec deux équipes distinctes de receleurs. Sur les vingt interpellés, tous des hommes majeurs, une quinzaine l'ont été en Gironde, où ils se trouvaient lundi en garde à vue.
Les gendarmes, qui agissaient dans le cadre d'une commission rogatoire d'une juge d'instruction du TGI de Bordeaux, ont retrouvé lors des interpellations "plusieurs centaines de bouteilles de vin", des "dizaines de milliers d'euros" en liquide, des véhicules volés, des armes de poing et d'épaule, a précisé lors d'un point presse le colonel Ghislain Réty, commandant du Groupement de gendarmerie.
Jamais de cette ampleur
Souci d'image oblige, le milieu bordelais du vin était resté très discret sur ces vols, une quinzaine entre juin et début février, qui se produisaient environ "tous les quinze jours". Les derniers faits remontaient au 5 février, selon le colonel Réty.
Treize châteaux au total et deux dépôts, où étaient stockées des bouteilles provenant de plusieurs châteaux, ont été visés, a indiqué une source proche de l'enquête. Aucune complicité avec des employés des châteaux visés n'a été établie "pour l'instant", a précisé le commandant du groupement.
Les gardes à vue devraient être maintenues jusqu'à jeudi, les gendarmes ayant la possibilité d'interroger les suspects pendant 96 heures dans cette affaire concernant une association de malfaiteurs, mise en cause pour des vols "en bande organisée".
"C'est rassurant", a déclaré à l'AFP Bernard Farges, président du Conseil interprofessionnel du vin de Bordeaux (CIVB) en espérant que ce démantèlement dissuadera d'autres malfaiteurs.
Selon lui, voler des grands crus peut être tentant, s'agissant de "produits de valeur", ayant une "notoriété", qui peuvent s'écouler facilement en se vendant à l'unité. Des vols similaires ont été répertoriés, mais jamais dans ces proportions, a-t-il estimé.
En juin, la gendarmerie avait déjà identifié une autre bande, sans lien avec les personnes interpellées lundi, accusée d'avoir volé plusieurs centaines de bouteilles et des bijoux, pour un butin estimé à plusieurs milliers d'euros.
Cette bande était soupçonnée d'avoir dérobé, dans un lieu de stockage à la périphérie de Bordeaux, environ 500 bouteilles de vins, dont la moitié de grands crus. Les bouteilles retrouvées par les enquêteurs, d'une valeur estimée à environ 300.000 euros, avaient été restituées à leur propriétaire.
Partager
