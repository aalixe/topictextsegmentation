TITRE: Chypre: reprise des n�gociations en vue d'une r�unification - DH.be
DATE: 2014-02-11
URL: http://www.dhnet.be/dernieres-depeches/afp/chypre-reprise-des-negociations-en-vue-d-une-reunification-52f9f7b73570c16bb1caa551
PRINCIPAL: 163475
TEXT:
Vous �tes ici: Accueil > Derni�res d�p�ches
Chypre: reprise des n�gociations en vue d'une r�unification
Publi� le
11 f�vrier 2014 � 11h07
Nicosie (AFP)
Les dirigeants chypriote-grec Nicos Anastasiades et chypriote-turc Dervis Eroglu reprennent mardi les n�gociations, suspendues depuis pr�s de deux ans, en vue d'une  r�unification de l'�le, a constat� une journaliste de l'AFP.
Les deux responsables sont arriv�s l'un apr�s l'autre sans faire de d�claration peu apr�s 11H30 locales (09H30 GMT) dans les locaux de l'ONU, sur l'ancien a�roport de Nicosie, o� ils ont �t� accueillis par la responsable des Nations unies � Chypre, Lisa Buttenheim.
� 2014 AFP. Tous droits de reproduction et de repr�sentation r�serv�s. Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par l'AFP. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, rediffus�e, traduite, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de l'AFP.
Derni�res d�p�ches
