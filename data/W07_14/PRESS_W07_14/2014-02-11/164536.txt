TITRE: Une GoogleCar prend une photo de son exc�s de vitesse - Informaticien.be
DATE: 2014-02-11
URL: http://www.informaticien.be/news_item-17961-Une_GoogleCar_prend_une_photo_de_son_exces_de_vitesse.html
PRINCIPAL: 0
TEXT:
Une GoogleCar prend une photo de son exc�s de vitesse
Publi� le: 11/02/2014 @ 18:55:05: Par Nic007 �Dans " Google "
Une voiture Google s'est photographi�e elle-m�me en exc�s de vitesse sur une route en Islande. On peut d�couvrir le clich� en cherchant la route 1 � proximit� du hameau de Laugar, o� la limitation de vitesse est inscrite au sol (50 km/h) et un radar indique l'allure des voitures qui y passent (69 km/h pour la Google Car).
Ce n'est pas la premi�re fois qu'une de ses voitures commet un impair, on se rappellera qu'une Google Car avait renvers� un �ne au Botswana, ou qu'un conducteur indon�sien avait pris la fuite apr�s avoir �t� impliqu� dans un accident.
Plus d'actualit�s dans cette cat�gorie
02/04
