TITRE: Le ski de fond fran�ais toujours bredouille � Sotchi
DATE: 2014-02-11
URL: http://www.lemonde.fr/jeux-olympiques/article/2014/02/11/le-ski-de-fond-francais-toujours-bredouille-a-sotchi_4364341_1616891.html
PRINCIPAL: 163798
TEXT:
Le ski de fond fran�ais toujours bredouille � Sotchi
Le Monde |
� Mis � jour le
11.02.2014 � 14h37
Aurore Jean, derni�re repr�sentante tricolore tous sexes confondus dans l'�preuve de sprint de ski de fond, a �t� �limin�e mardi en demi-finale des Jeux olympiques de Sotchi.
Jean, qui avait termin� 24e du sprint il y a quatre ans � Vancouver, a termin� 6e et derni�re de sa finale, alors qu'il aurait fallu terminer dans les deux premi�res�� ou � d�faut avoir l'un des deux meilleurs temps des non-qualifi�es direct�� pour atteindre la finale.�
��J'ESP�RAIS MIEUX MAIS C'EST COMME �A ��
� J'ai tout donn�, c'est parti vraiment vite d�s le d�but, lors du passage de la bosse, a-t-elle comment� juste apr�s la course au micro de France T�l�vision. J'esp�rais mieux mais c'est comme �a aujourd'hui. C'�tait dur, plus �a allait plus la neige devenait de moins en moins glissante. L'an dernier j'avais fait un podium ici et �a me boostait, mais c'�tait le seul podium de ma carri�re donc tout �tait relatif �, a-t-elle ajout�. La Fran�aise d�cidera de son programme � venir en fonction de son �tat physique .
Chez les hommes, aucun Fran�ais n'est parvenu � se qualifier pour les demi-finales du sprint en ski de fond. En course dans la m�me s�rie, Cyril Miranda et Cyril Gaillard ont termin� respectivement 3e et 6e. Renaud Jay, qui avait fini 3e de sa course un peu plus t�t, a lui aussi �t� �limin� au stade des quarts de finale..
Jeux olympiques
