TITRE: Projet de loi "géolocalisation": ce que réclame la CNIL - 11 février 2014 - Challenges
DATE: 2014-02-11
URL: http://www.challenges.fr/entreprise/20140211.CHA0296/projet-de-loi-geolocalisation-ce-que-reclame-la-cnil.html
PRINCIPAL: 163891
TEXT:
Publié le 11-02-2014 à 12h40
Mis à jour à 12h40
Le projet de loi sur la g�olocalisation�devra, selon la Commission nationale informatique et libert�s, �tre "strictement encadr� par la loi".
La Cnil, commission nationale de l'informatique et des libertés (c) Sipa
La Cnil a jug� mardi 11 f�vrier que le projet de loi sur la g�olocalisation, examin� ce jour � l'Assembl�e nationale, devait �tre "strictement encadr� par la loi" et demande de r�duire de 15 � 8 jours la dur�e d'autorisation de cette pratique dans le cadre d'enqu�tes.
L'Asic, principale association fran�aise des acteurs du web, "inqui�te" du contenu de ce projet de loi, avait sollicit� le pr�sident de la Commission des lois de l'Assembl�e nationale, Jean-Jacques Urvoas, pour qu'il use d'un de ses pouvoirs li�s � sa fonction, � savoir demander � la Commission nationale de l'informatique et des libert�s (Cnil) de rendre public son avis sur le texte.
Le projet de loi, qui sera examin� mardi � partir de 15H � l' Assembl�e nationale , clarifie les conditions d'utilisation de la g�olocalisation par les services enqu�teurs, apr�s que la Cour de cassation a r�cemment invalid� des pi�ces de proc�dure recueillies par ce moyen (portables, balises GPS...) dans le cadre d'enqu�tes pr�liminaires.
"La g�olocalisation doit �tre encadr�e par la loi"
Le texte �tablit la possibilit� pour le parquet d'autoriser la g�olocalisation pour une dur�e de 15 jours dans le cadre d'enqu�tes pr�liminaires. Au-del�, il appartiendra � un juge des libert�s et de la d�tention d'autoriser la prorogation du dispositif, pour une dur�e d'un mois renouvelable.
Il rend �galement possible l'utilisation de la g�olocalisation pour des infractions punies d'au moins trois ans d'emprisonnement alors que les s�nateurs avaient port� ce d�lai � cinq ans pour les d�lits d'atteinte aux biens.
Dans son avis remis en d�cembre au gouvernement et donc rendu public mardi, la Cnil estime que, "compte tenu de son caract�re intrusif, la g�olocalisation en temps r�el doit �tre strictement encadr�e par la loi, soumise � un contr�le a priori, ou exceptionnellement a posteriori, de l'autorit� judiciaire et mise en oeuvre conform�ment aux dispositions du code de proc�dure p�nale".
Quant � l'autorisation d'une g�olocalisation d'une dur�e de quinze jours, la Cnil souligne que "la dur�e de la flagrance, telle que pr�vue � l'article 53 du Code de proc�dure p�nal, est de huit jours renouvelable une fois sur d�cision du procureur de la R�publique", et demande donc que le projet de loi "soit modifi� en ce sens".
(avec AFP)
