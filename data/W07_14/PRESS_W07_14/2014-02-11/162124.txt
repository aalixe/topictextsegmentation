TITRE: VIDÉO. Hollande aux États-Unis... en mai 2012 | Site mobile Le Point
DATE: 2014-02-11
URL: http://www.lepoint.fr/monde/video-hollande-aux-etats-unis-en-mai-2012-11-02-2014-1790397_24.php
PRINCIPAL: 162096
TEXT:
11/02/14 à 06h02
VIDÉO. Hollande aux États-Unis... en mai 2012
Quelques jours seulement après son élection, François Hollande avait été invité par Barack Obama. Une bonne entente placée sous le signe de l'humour.
© Jewel Samad / AFP
Par Sophie Combot
La première visite du président François Hollande aux États-Unis, le 18 mai 2012, avait eu lieu quelques jours seulement après son élection. Barack Obama avait souhaité rencontrer François Hollande avant les sommets du G8 et de l'Otan, pour discuter des liens étroits en matière d'économie et de sécurité qui unissent les deux pays. C'est avec beaucoup d'humour que le président américain avait accueilli le nouveau président français.
REGARDEZ la première visite du président Hollande :
"C'est avec grand plaisir que j'accueille le président Hollande aux États-Unis", assurait Barack Obama. En effet, la dernière visite d'État d'un président français aux États-Unis remontait à 1996. Il s'agissait alors de Jacques Chirac, Nicolas Sarkozy n'ayant pas eu cet honneur. À l'époque, Obama avait suggéré à Hollande de ne plus rouler en scooter à Paris : "Je le sais, car, ici, les services de protection ne m'autorisent pas à le faire." Une mise en garde prémonitoire ?
REGARDEZ la vidéo de la mise en garde de Barack Obama sur l'utilisation du scooter :
