TITRE: Hollande re�u par Obama pour une visite de trois jours
DATE: 2014-02-11
URL: http://www.francetvinfo.fr/monde/ameriques/francois-hollande-est-arrive-aux-etats-unis-pour-une-visite-de-trois-jours_526851.html
PRINCIPAL: 161753
TEXT:
Hollande re�u par Obama pour une visite de trois jours
Les deux dirigeants ont r�alis� lundi une excursion de quelques heures sur le domaine de�Monticello�(Virginie).
Barack Obama re�oit Fran�ois Hollande lors de son arriv�e aux Etats-Unis, � la base militaire a�rienne d'Andrews, le 10 f�vrier 2014. (LARRY DOWNING / REUTERS)
Par Francetv info avec AFP
Mis � jour le
, publi� le
10/02/2014 | 21:14
Fran�ois Hollande est arriv� aux Etats-Unis, lundi 10 f�vrier, pour une visite d'Etat de trois jours. Il a atterri�peu avant 14h30 (20h30 heure de Paris) sur la base a�rienne d'Andrews, � l'est de�Washington. Barack Obama a re�u le pr�sident fran�ais en grande pompe.
(FRANCE TELEVISIONS)
Le chef d'Etat am�ricain a accueilli son h�te lors d'une c�r�monie officielle. Les deux dirigeants ont pris ensuite l'avion Air Force�One�pour une excursion de quelques heures sur le domaine de�Monticello�(Virginie).
Fran�ois Hollande se prom�ne avec Barack Obama et�Leslie Greene Bowman, la pr�sidente de la fondation�Thomas Jefferson, � Monticello (Etats-Unis), le 10 f�vrier 2014. (JEWEL SAMAD / AFP)
C�r�monie solennelle mardi matin
Mardi matin, le cadre sera plus solennel. Sur la pelouse de la Maison Blanche, l'arriv�e de Fran�ois Hollande sera ponctu�e de 21 coups de canon, des hymnes nationaux et d'un passage en revue de troupes.
Les deux chefs d'Etat s'entretiendront dans le bureau ovale avant une conf�rence de presse. Ils aborderont de nombreux sujets internationaux comme la Syrie , l'Iran, l'Ukraine , le Sahel et la Libye. Certains commentateurs estiment d'ailleurs que la visite de Hollande est une forme de "remerciement" d'Obama�pour des dossiers comme le Mali ou encore la Centrafrique .�Les relations �conomiques entre la France et les Etats-unis devraient �galement �tre �voqu�es.
