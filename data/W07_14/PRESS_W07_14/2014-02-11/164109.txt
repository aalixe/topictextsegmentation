TITRE: Bettencourt: la Cour de cassation examine la validation de l'enquête | Site mobile Le Point
DATE: 2014-02-11
URL: http://www.lepoint.fr/societe/bettencourt-la-cour-de-cassation-examine-la-validation-de-l-enquete-11-02-2014-1790448_23.php
PRINCIPAL: 164061
TEXT:
11/02/14 à 09h47
Bettencourt: la Cour de cassation examine la validation de l'enquête
La Cour de cassation examine mardi après-midi la validation de l'enquête dans le volet abus de faiblesse de l'affaire Bettencourt et se penche par ailleurs sur le statut des agendas de Nicolas Sarkozy , qui a bénéficié d'un non-lieu.
La défense de la plupart des personnes renvoyées devant le tribunal correctionnel a formé un pourvoi contre l'arrêt de la chambre de l'instruction de la cour d'appel de Bordeaux , qui a validé le 24 septembre dernier la quasi-totalité de l'instruction menée par le juge Jean-Michel Gentil et ses deux collègues Cécile Ramonatxo et Valérie Noël.
Devant la Cour de cassation, les débats porteront notamment sur l'impartialité de l'expertise médicale de l'héritière de L'Oréal, au coeur de cette affaire, selon laquelle Liliane Bettencourt se trouvait en état de vulnérabilité depuis 2006 et pouvait donc être victime d'abus de faiblesse.
Cette expertise avait été au coeur d'une polémique, car réalisée par un professeur de médecine légale, le Dr Sophie Gromb, suffisamment proche du juge Gentil pour être témoin à son mariage. La révélation de ces liens avait conduit plusieurs des mis en examen à demander, en vain, le dépaysement du dossier.
La validité des expertises psychologiques concernant François-Marie Banier, son compagnon Martin d'Orgeval et l'ancien homme de confiance de Liliane Bettencourt doit également être débattue.
Fait plutôt rare, la Cour de cassation avait accepté d'ordonner l'examen immédiat des pourvois, c'est-à-dire sans attendre que le dossier soit jugé sur le fond.
Il revient à la chambre criminelle de juger de la bonne application du droit par les magistrats de la cour d'appel -- et non pas de statuer non sur les charges retenues contre les mis en examen et le fond du dossier. Elle devrait mettre sa décision en délibéré.
Si elle rejetait les pourvois ou n'annulait que certains éléments du dossier sans juger nécessaire que la justice les examine à nouveau, la Cour de cassation lèverait ainsi le dernier obstacle à la tenue du procès.
Les agendas présidentiels, "pas des biens ordinaires"?
Les pourvois ont été formés par le photographe François-Marie Banier et son compagnon Martin d'Orgeval, l'ancien homme de confiance de Liliane Bettencourt Patrice de Maistre, l'avocat Pascal Wilhelm qui lui avait succédé, mais aussi l'homme d'affaires Stéphane Courbit et l'ex-gestionnaire de l'île seychelloise d'Arros Carlos Cassina Vejarano.
L'ancien ministre du Budget Eric Woerth s'est lui aussi pourvu en cassation, mais la problématique de l'abus de faiblesse "ne le concerne pas", a expliqué à l'AFP son avocat, Jean-Yves Le Borgne.
L'ancien trésorier de campagne de Nicolas Sarkozy est en effet poursuivi pour recel. Il est soupçonné d'avoir reçu lors de deux rendez-vous, début 2007, en connaissant leur origine illicite, des sommes d'au moins 50.000 euros que lui aurait remises Patrice de Maistre. Au lendemain de son renvoi, il s'était dit "écoeuré", mais "serein" car n'ayant "rien à se reprocher".
Même s'il a bénéficié d'un non-lieu et n'est pas renvoyé devant le tribunal, l'ancien président de la République Nicolas Sarkozy a lui maintenu son pourvoi. Il conteste le refus de la justice d'annuler la saisie des agendas qu'il utilisait pendant son mandat à l'Elysée.
Pour Nicolas Sarkozy, ce refus est contraire à l'article 67 de la Constitution disposant que le chef de l'Etat "ne peut, durant son mandat (...) faire l'objet d'une action, d'un acte d'information, d'instruction ou de poursuite".
"En sa qualité d'ancien président de la République, il considère qu'il a un intérêt, indifféremment du non-lieu qu'il a obtenu, à défendre que les objets attachés à la fonction présidentielle ne sont pas des biens ordinaires, saisissables à la seule demande d'un juge d'instruction, sans protection particulière", avait expliqué son avocat à la Cour de cassation, Patrice Spinosi.
