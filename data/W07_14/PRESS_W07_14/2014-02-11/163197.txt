TITRE: Kool Shen dans "Abus de faiblesse" : "Se voir pendant une heure et demie, c'est tr�s compliqu�" dans Laissez-vous tenter le 11-02-2014 sur RTL.
DATE: 2014-02-11
URL: http://www.rtl.fr/emission/laissez-vous-tenter/billet/kool-shen-dans-abus-de-faiblesse-se-voir-pendant-une-heure-et-demie-c-est-tres-complique-7769640739
PRINCIPAL: 163196
TEXT:
Par Michel Cohen-Solal | Publi� le 11/02/2014 � 11h53 | Laissez-vous tenter
Le rappeur fran�ais Kool Shen en septembre 2008 � Paris
Cr�dit : OLIVIER LABAN-MATTEI / AFP
RENCONTRE - Le rappeur Kool Shen est l'un des acteurs de la semaine, � l'affiche d�s demain, au c�t� d'Isabelle Huppert, du dernier film de Catherine Breillat "Abus de faiblesse".
Apr�s Joey Starr, c'est au tour de son complice de sc�ne de faire l'acteur. Kool Shen sera d�s mercredi 12 f�vrier sur grand �cran dans le film de Catherine Breillat Abus de faiblesse, aux c�t�s d'Isabelle Huppert. L'ancien de NTM s'en sort bien.
Kool Shen interpr�te Christophe Rocancourt : "Catherine Breillat ne voulait pas que je lui ressemble, que j'ai des tics de lui. Le seul tic qu'elle ne voulait pas c'est que mon accent banlieusard ressorte trop"
Cr�dit : Michel Cohen-Solal
Son personnage est cens� rappeler celui de Christophe Rocancourt , condamn� � 16 mois de prison dont 8 fermes, ainsi qu'� 578.000 euros de dommages et int�r�ts pour abus de faiblesse au pr�judice de Catherine Breillat, qui souffrait � l'�poque d'une h�mipl�gie. Une histoire qu'elle a racont�e dans un livre.
Abus de Faiblesse - Bande-annonce
Le rap et le m�tier d'acteur : deux domaines diff�rents
"Je n'ai pas lu le bouquin, admet Kool Shen. Je connaissais Rocancourt un peu comme tout le monde, apr�s ses p�rip�ties am�ricaines et canadiennes. Je l'ai rencontr� une fois quand on est remont� sur sc�ne avec NTM � Bercy, mais je ne connais pas plus son histoire". D'ailleurs, la r�alisatrice ne lui a donn� aucune autre directive que d'effacer au maximum son "accent banlieusard". Elle ne voulait pas qu'il prenne des tics de son personnage.
On rentre dans un autre domaine o� l'on doit interpr�ter un personnage
Kool Shen
Catherine Breillat l'a choisi pour "son corps de rappeur". Et c'est vrai, � l'�cran il a une pr�sence presque f�line. "Je pense que �a ne peut pas �tre un d�savantage" d'avoir fait du rap, "maintenant c'est quelque chose de tr�s diff�rent", souligne-t-il. En tant que rappeur, "c'est moi qui �crit mes propres textes, mes propres �motions. L� on rentre dans un autre domaine o� l'on doit interpr�ter un personnage", explique Kool Shen.
Un premier jour de tournage difficile
M�me s'il a tourn� plus de 30 clips, le premier jour �tait stressant pour l'interpr�te de Laisse pas trainer ton fils. Se voir � l'�cran a �t�, pour lui, une petite souffrance. "Se voir pendant une heure et demie, c'est tr�s compliqu�. J'ai d�j� du mal � m'entendre en interview", confie-t-il.
"Je me souviens aussi m'�tre �cout� les premi�res fois quand j'allais en studio c'�tait tr�s p�nible et au bout de 15 ans on commence � s'habituer � sa voix et � ce que l'on est capable de faire et de ne pas faire. Je ne sais pas si c'est tr�s nombriliste mais j'�tais plus focalis� sur moi que sur le film. Donc c'est un peu de souffrance", conclut Kool Shen.
Il vient de finir le tournage d'une s�rie pour Arte et surtout le tournage au Cambodge, du prochain Erick Zonka o� il interpr�te le r�le d'un colonel pendant la guerre d'Indochine.
'Abus de faiblesse" de Catherine Breillat avec Kool Shen et Isabelle Huppert
Cr�dit : Rezo Films
La r�daction vous recommande
