TITRE: Etudiant : un compte et une carte de cr�dit pour un forfait annuel de 20�
DATE: 2014-02-11
URL: http://www.capcampus.com/comparez-les-banques-1742/etudiant-un-compte-et-une-carte-de-credit-pour-un-forfait-annuel-de-20euros-a29012.htm
PRINCIPAL: 163713
TEXT:
La R�daction de Capcampus
Etudiant : un compte et une carte de cr�dit pour un forfait annuel de 20�
D�couvrez le concept Compte NICKEL, id�al pour g�rer son budget �tudiant avec impossibilit� de d�passer son budget
Zoom
Apr�s six mois de phase de test et l'ouverture de plus de mille comptes Nickel, l'offre est distribu�e sur l'ensemble du territoire � partir de ce jour. Soixante buralistes agr��s par l'ACPR sont d�j� �quip�s de la borne Nickel et distribuent effectivement Compte-Nickel en M�tropole et dans les Antilles fran�aises depuis quelques jours. L'objectif est d'�quiper vingt nouveaux buralistes chaque semaine � partir du mois de mars 2014. La carte des buralistes �quip�s est dor�navant mise � jour en temps r�el sur le site www.compte-nickel.fr/ou-le-prendre.
Un compte pour tous
Compte-Nickel est un service de compte de paiement ouvert a? tous, sans conditions de revenus, de d�p�ts ou de patrimoine, et sans possibilit�? de d�couvert ni de cr�dit. Cette philosophie encourage une pratique responsable et p�dagogique de l'argent. Compte-Nickel est adapte? aux attentes de nombreux publics et peut convenir a? tous ceux pour qui les offres bancaires traditionnelles sont trop �toff�es ou on�reuses par rapport a? leurs besoins r�els ainsi qu'a? tous ceux qui sont en marge du syst�me, y compris les interdits bancaires.
Compte-Nickel repr�sente une solution simple pour g�rer son argent au jour le jour.
Il s'ouvre en 5 minutes, en toute s�curit�??, chez le buraliste. Une pi�ce d'identit�et un num�ro de t�l�phone mobile suffisent. La Borne Nickel permet de constituer instantan�ment le dossier client en utilisant les technologies de d�mat�rialisation les plus r�centes. Le buraliste associe une Mastercard au dossier client ainsi constitue? et l'active en temps r�el sur son terminal de paiement �lectronique. Cette approche in�dite permet d'obtenir imm�diatement un compte avec un relev�? d'identit�? bancaire (RIB) et une carte de paiement utilisable partout.
Un RIB, une Mastercard et un suivi en temps r�el sur mobile et sur Internet
Avec Compte-Nickel, chaque client poss�de un relev� d'identit� bancaire (RIB), il peut domicilier ses revenus, recevoir et �mettre des virements sur son compte, enregistrer des pr�l�vements en toute fiabilit�. Il peut d�poser des esp�ces chez le buraliste, payer avec sa Mastercard chez les commer�ants en France et � l'international, faire ses achats en toute s�curit� sur Internet. Il peut retirer des esp�ces chez le buraliste ou dans tous les distributeurs de billets. Il peut suivre toutes ses op�rations en temps r�el sur son t�l�phone mobile ou sur son espace Internet.
Une relation clients facilit�e
Le buraliste est le premier point de contact du client pour l'ouverture de son compte. Une fois le compte cr�e??, le client acc�de ades services de gestion de ses op�rations con�us pour une grande simplicit�? d'usage :
Suivi via mobile : envoi de mails et/ou de SMS selon les alertes programm�es et en tout �tat de cause pour tout pr�l�vement � venir si le solde du compte n'est pas suffisant ;
Site Internet fixe et mobile d�di�? dote? de toutes les fonctionnalit�s : suivi, relev�s, RIB, virements en temps r�el (a? la seconde de valeur) ;
Service client Internet de derni�re g�n�ration ; Service SOS par t�l�phone ouvert de 8h00 a? 22h00, 6 jours/7.
Publi� le 11 f�vrier 2014
R�AGISSEZ, COMMENTEZ, PARTAGEZ !
