TITRE: L'accessibilit� des villes fran�aises aux handicap�s s'am�liore, mais reste insuffisante
DATE: 2014-02-11
URL: http://www.francetvinfo.fr/societe/l-accessibilite-des-villes-francaises-aux-handicapes-s-ameliore-mais-reste-insuffisante_527529.html
PRINCIPAL: 0
TEXT:
Tweeter
L'accessibilit� des villes fran�aises aux handicap�s s'am�liore, mais reste insuffisante
L'Association des paralys�s de France d�nonce un constat�"accablant" :��� peine plus de la moiti� des �coles et seulement 42% des r�seaux de bus accessibles.
Des personnes en fauteuil roulant � Nantes (Loire-Atlantique), le 20 octobre 2009. (  MAXPPP)
Par Francetv info avec AFP
Mis � jour le
, publi� le
11/02/2014 | 18:39
L'Association des paralys�s de France (APF) a publi�, mardi 11 f�vrier, la 5e �dition de son�barom�tre sur l'accessibilit�des villes aux handicap�s.�Un total de 95�chefs-lieux d�partementaux de France m�tropolitaine ont r�pondu. Paris n'a pas donn� suite au questionnaire de l'APF.
L'APF�d�nonce un constat�"accablant"�:�� peine plus de la moiti� des �coles et seulement 42% des r�seaux de bus sont accessibles aux handicap�s.�Elle souligne qu'"un tiers des chefs-lieux d�partementaux n'ont m�me pas la moyenne pour l'accessibilit� de leurs �quipements municipaux".
Mais, de fa�on globale, la note moyenne nationale (sur 20) n'a cess� de s'am�liorer en cinq ans, passant de 10,6 en 2009 � 13,04 en 2012 et 14,14 en 2013. Et aucune ville n'est sous la moyenne en 2013. Autre point positif�:�l'accessibilit� des centres commerciaux progresse, ainsi que celle des bureaux de poste, des piscines et des cin�mas.
Grenoble, Nantes et Caen sur le podium
Grenoble�conserve la t�te du classement avec une moyenne de 18,7/20, suivie de�Nantes�avec 18/20, tandis que�Caen�prend la 3e place avec 17,6/20.�En queue de peloton, on trouve Digne-les-Bains avec 10,2/20, pr�c�d�e de peu par�Alen�on�et�Chaumont�(10,3).
Certaines villes ont connu "des �volutions significatives", reconna�t l'APF,�qui loue le dynamisme de�Poitiers�et de Mont-de-Marsan�(+4,5 points de moyenne chacune),�Dijon�(+3,9),�N�mes�(+3,6 points),�Besan�on�(+2,9), Laval et Saint-Etienne�(+2,7). Paris se classe en 64e position (sur 96), sur la base de sa note de l'ann�e pr�c�dente (13,2/20).
