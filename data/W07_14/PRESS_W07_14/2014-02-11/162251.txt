TITRE: L'Oreal : Communiqu� : "Op�ration strat�gique approuv�e par les Conseils d'Administration  de Nestl� et de L'Or�al"
DATE: 2014-02-11
URL: http://www.euroinvestor.fr/actualites/2014/02/11/loreal-communique-operation-strategique-approuvee-par-les-conseils-dadministration-de-nestle-et-de-loreal/12696200
PRINCIPAL: 162245
TEXT:
10 f�vr.: InvenSense et STMicroelectronics annoncent le r�glement de leurs ..
11 f�vr.: Michelin : COMPAGNIE GENERALE DES ETABLISSEMENTS MICHELIN Informa..
�
L'Oreal : Communiqu� : "Op�ration strat�gique approuv�e par les Conseils d'Administration  de Nestl� et de L'Or�al"
Related content
L'Oreal : Communiqu� : "Avec l'acquisition de Magic Hol..
07 avr.�-�
L'OREAL : D�claration du nombre total de droits de vote..
24 mars�-�
L'Or�al : Communiqu� : � Signature du contrat de cessio..
�
Op�ration strat�gique approuv�e par les Conseils d'Administration
de Nestl� et de L'Or�al
Paris et Vevey, le 11 f�vrier 2014 - R�unis le 10 f�vrier 2014, les Conseils d'Administration de Nestl� et de L'Or�al ont respectivement approuv�, � l'unanimit� des votants, un projet d'op�ration strat�gique pour les deux entreprises consistant en�un rachat par L'Or�al de 48,5 millions de ses propres actions (soit 8% de son capital) � Nestl�, ce rachat �tant financ� :
Pour partie par la cession par L'Or�al � Nestl� de sa participation de 50% dans le laboratoire pharmaceutique suisse de dermatologie Galderma (d�tenu � parit� par L'Or�al et Nestl�) pour un montant de 3,1 milliards d'euros de valeur d'entreprise (2,6 milliards d'euros de valeur des fonds propres), r�mun�r�e par Nestl� en actions L'Or�al (soit 21,2 millions d'actions)�
Pour le solde en num�raire � hauteur de 27,3 millions d'actions L'Or�al d�tenues par Nestl� pour un montant de 3,4 milliards d'euros�
Le prix unitaire de l'action L'Or�al retenu pour cette op�ration est celui de la moyenne des cours de cl�ture entre le lundi 11 novembre 2013 et le lundi 10 f�vrier 2014, soit 124,48 euros.
Toutes les actions rachet�es par L'Or�al seront annul�es et � l'issue de cette op�ration, la participation de Nestl� au capital de L'Or�al sera r�duite de 29,4% � 23,29%, celle de la famille Bettencourt Meyers passant de 30,6% � 33,31% du capital. Afin de refl�ter l'�volution de la participation de Nestl� dans la gouvernance de L'Or�al, le nombre de repr�sentants de Nestl� au Conseil d'Administration de L'Or�al sera ajust� de 3 � 2, et les dispositions applicables en mati�re de plafonnement des participations pr�vues au pacte d'actionnaires entre Nestl� et la famille Bettencourt Meyers s'appliqueront � ces nouveaux niveaux de d�tention.
L'op�ration aura un effet relutif de plus de 5% en ann�e pleine sur le BNPA courant de L'Or�al. Ce rachat sera financ� exclusivement avec les disponibilit�s de L'Or�al et � travers l'�mission de billets de tr�sorerie � court terme et ne n�cessitera donc pas de recourir � la cession de titres Sanofi.
Cette op�ration sera soumise aux proc�dures de consultation des instances repr�sentatives du personnel au sein de Galderma et de L'Or�al, et sera �galement subordonn�e � l'obtention des autorisations des autorit�s de la concurrence comp�tentes. Elle pourrait �tre r�alis�e avant la fin du premier semestre 2014.
M. Peter BRABECK-LETMATHE, Pr�sident de Nestl� SA, a d�clar�:
�� Avec l'acquisition envisag�e de 50% de Galderma, Nestl� poursuit son d�veloppement strat�gique dans la direction de Nutrition, Health and Wellness en �largissant ses activit�s aux soins m�dicaux de la peau.
A cette fin, Nestl� cr�era un nouveau p�le autour d'une nouvelle entit� : Nestl� Skin Health SA. Galderma en sera la base fondatrice et son management en assurera la direction.
Galderma, une fois devenue filiale � 100% de Nestl�, aura tous les moyens n�cessaires � son d�veloppement qui b�n�ficiera � l'entreprise, � ses salari�s et toutes les parties prenantes.
A la suite de l'all�gement de sa participation dans l'Or�al, Nestl� continuera d'apporter son appui au d�veloppement de l'entreprise � laquelle il est associ� depuis 40 ans.
Dans cet esprit, Nestl� restera de concert avec la famille Bettencourt Meyers et les accords existants tels qu'adapt�s � la nouvelle situation continueront � s'appliquer. �
�
M. Jean-Paul AGON, Pr�sident Directeur G�n�ral de L'Or�al SA, a d�clar�:
� Cette op�ration constituera une �tape strat�gique tr�s positive pour L'Or�al, ses collaborateurs et ses actionnaires.
L'Or�al se consacrera enti�rement � son activit� cosm�tique dans le cadre de sa mission � la beaut� pour tous �, de sa strat�gie d'universalisation et de son objectif de conqu�rir 1 milliard de nouveaux consommateurs.
L'Or�al b�n�ficiera de la participation tr�s significative de la famille fondatrice Bettencourt Meyers qui se trouvera encore renforc�e et dont l'engagement envers l'entreprise est � la fois historique et entier.
Par ailleurs, Nestl� qui a toujours �t� un actionnaire loyal et constructif continuera d'apporter son soutien actif.
Enfin, tous les actionnaires de L'Or�al b�n�ficieront de cette op�ration gr�ce � la relution du BNPA r�sultant du rachat et de l'annulation des actions L'Or�al d�tenues par Nestl�. �
Conf�rence de presse Nestl� & L'Or�al
11 f�vrier 2014 � 8h00 (heure de Paris)
�
Mr. Jean-Paul Agon et Peter Brabeck-Letmathe
seront heureux de recevoir la presse
au si�ge de L'Or�al, 41 rue Martre, Clichy
�
www.loreal-finance.com/fr/resultats-annuels
A propos de L'Or�al
Depuis plus de 105 ans, L'Or�al est d�di� au m�tier de la beaut�. Avec un portefeuille unique de 28 marques internationales, diverses et compl�mentaires, le Groupe a r�alis� en 2013 un chiffre d'affaires consolid� de 23 milliards d'euros et compte 77 500 collaborateurs dans le monde. Leader mondial de la beaut�, L'Or�al est pr�sent dans tous les circuits de distribution : le march� de la grande consommation, les grands magasins, les pharmacies et drugstores, les salons de coiffure, le travel retail et les boutiques de marque.
L'Or�al s'appuie sur l'excellence de sa Recherche et Innovation et ses 4 000 chercheurs pour r�pondre � toutes les aspirations de beaut� dans le monde et l'objectif du Groupe de conqu�rir un milliard de nouveaux consommateurs dans les ann�es � venir. A travers son programme � Sharing beauty with all � L'Or�al a pris des engagements ambitieux en mati�re de d�veloppement durable tout au long de sa cha�ne de valeur, � horizon 2020. �
�
A propos de Nestl�
Nestl� est le leader mondial de nutrition, sant� et bien-�tre. L'entreprise a pour ambition d'am�liorer la qualit� de vie des consommateurs en leur proposant des aliments et boissons plus sains et plus savoureux pour toutes les �tapes de la vie et tous les moments de la journ�e. Avec la cr�ation r�cente de Nestl� Health Science, Nestl� a renforc� sa direction strat�gique en se donnant la capacit� de d�velopper des solutions nutritionnelles fond�es sur la science pour aider � pr�venir et traiter diff�rents probl�mes de sant�.
Nestl�, fond�e il y a bient�t 150 ans � Vevey, en Suisse, o� elle a toujours son si�ge, emploie 339�000 personnes dans plus de 150 pays et compte 465 usines.
Avec un chiffre d'affaires de CHF 92,2 milliards en 2012, Nestl� dispose d'un portefeuille de marques sans pareil, avec plus de 2�000 marques globales et locales. 70% du chiffre d'affaires provient de marques g�n�rant plus d'un milliard de francs suisses de ventes.
A propos de Galderma
Galderma est une soci�t� suisse cr��e en 1981 sp�cialis�e dans l'offre de solutions m�dicales innovantes en dermatologie pour chacun tout au long de la vie, et s'engageant pleinement, � travers le monde, aupr�s des professionnels de sant�. La soci�t� emploie plus de
5 000 employ�s dans 31 filiales et dispose d'un r�seau mondial d'agents exclusifs. Son portefeuille de produits complet, disponible dans 70 pays, traite des maladies dermatologiques telles que l'acn�, la rosac�e, l'onychomycose, le psoriasis et les dermatoses cortico-sensibles, les d�sordres pigmentaires, le cancer de la peau et des solutions m�dicales pour le vieillissement cutan�.
�
