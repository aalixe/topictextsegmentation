TITRE: Accessibilit� des villes aux handicap�s : constat "accablant" malgr� des progr�s (AFP) - Business Immo
DATE: 2014-02-11
URL: http://www.businessimmo.com/contents/40627/accessibilite-des-villes-aux-handicapes-constat-accablant-malgre-des-progres-apf
PRINCIPAL: 162520
TEXT:
Accessibilit� des villes aux handicap�s : constat "accablant" malgr� des progr�s (AFP)
11.02.2014
(AFP) - Les villes ont fait des progr�s dans l'accessibilit� aux personnes handicap�es, mais le constat reste "accablant" avec � peine plus de la moiti� des �coles et seulement 42 % des r�seaux de bus accessibles, estime l'Association des Paralys�s de France (APF) mardi en publiant son barom�tre annuel.
Pour lire cet article, choisissez l'une des options suivantes :
Vous n'avez pas encore de compte
