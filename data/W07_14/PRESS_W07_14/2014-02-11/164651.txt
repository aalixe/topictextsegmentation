TITRE: Monsanto Company : UE : Pas de majorit� pour bloquer un nouveau ma�s OGM, infos et conseils valeur US61166W1018 - Les Echos Bourse
DATE: 2014-02-11
URL: http://bourse.lesechos.fr/infos-conseils-boursiers/infos-conseils-valeurs/infos/ue-pas-de-majorite-pour-bloquer-un-nouveau-mais-ogm-950421.php
PRINCIPAL: 164634
TEXT:
Monsanto Company : UE : Pas de majorit� pour bloquer un nouveau ma�s OGM
11/02/14 � 17:59
L'absence de consensus ouvre la voie � un feu vert de la CE
Dix-neuf Etats, dont la France, contre l'autorisation
Paris n'a pas l'intention d'autoriser cette nouvelle vari�t�   (Actualis� avec pr�cisions, MON810)
BRUXELLES/PARIS, 11 f�vrier (Reuters) - Les gouvernements de l' Union europ�enne ne sont pas parvenus mardi � d�gager une majorit� pour bloquer l'autorisation de cultiver un nouveau type de ma�s g�n�tiquement modifi� (OGM), ouvrant la voie � son approbation par la Commission europ�enne .
Dans le cadre du vote, 19 Etats, dont la France, se sont oppos�s � l'approbation du ma�s transg�nique Pioneer TC 1507, un produit fabriqu� conjointement par les groupes am�ricains DuPont et Dow Chemical qui r�siste aux insectes.
Cinq Etats ont vot� pour et quatre se sont abstenus. Mais le vote n'a pas permis de d�gager une majorit� qualifi�e permettant un rejet d�finitif de ce nouvel OGM, ce qui place d�sormais la balle dans le camp de la Commission europ�enne .
"Nous allons regarder s'il y a des possibilit�s de recours contre cette d�cision" a-t-on imm�diatement fait savoir dans l'entourage du ministre fran�ais de l'Agriculture St�phane Le Foll.
Le Parlement europ�en avait appel� le mois dernier au rejet de la demande d'autorisation par 385 voix contre 201 et 30 absentions. ( )
"Le Parlement europ�en s'est clairement mobilis�, les Etats membres dans leur grande majorit� ont port� une voix unie et donc une Europe qui se pr�occupe de sant�, d'environnement et d'alimentation durable est l� et s'est clairement exprim�e", ajoute-on au minist�re fran�ais de l'Agriculture, o� l'on r�affirme que "la France n'a pas l'intention d'autoriser ces vari�t�s sur (son) territoire".
La seule semence transg�nique actuellement cultiv�e en Europe est le ma�s MON810 du semencier am�ricain Monsanto , approuv�e en 1998.
Les Etats membres ont la possibilit� d'interdire sur leur territoire la culture d'une semence OGM autoris�e par l'UE s'ils font la preuve d'une dangerosit� qui n'a pas �t� reconnue � l'�chelon europ�en.
PAS DE DANGER, SELON LA COMMISSION
Le commissaire europ�en � la Sant� Tonio Borg avait rappel� avant la r�union du conseil des ministres des Affaires europ�ennes que de nombreuses recherches avaient montr� que le ma�s TC 1507 n'�tait pas dangereux.
En France, le S�nat examinera lundi prochain une proposition de loi socialiste qui vise � restaurer l'interdiction du MON810 annul�e par le Conseil d'Etat, de mani�re � emp�cher les semis de printemps.
La plus haute juridiction administrative avait annul� le 1er ao�t un arr�t� gouvernemental de mars 2012 interdisant en France la culture du MON810, mais le gouvernement s'�tait engag� � maintenir le moratoire en vigueur. ( )
L'arr�t� de 2012 �tait notamment contest� par l'Association g�n�rale des producteurs de ma�s. Il faisait suite � deux arr�t�s similaires du minist�re de l'Agriculture, en 2007 et 2008, suspendant puis interdisant la mise en culture du MON810.
Ces deux arr�t�s avaient d�j� �t� suspendus en 2011 par le Conseil d'Etat, qui avait alors sollicit� la Cour de justice de l' Union europ�enne sur le fondement juridique d'une telle interdiction au regard du droit communautaire.
Dans son avis, le Conseil d'Etat affirmait que le MON810, le seul ma�s OGM autoris� � la culture dans l'UE, n'�tait "pas susceptible de soulever davantage de pr�occupations pour l'environnement que le ma�s conventionnel".
Apr�s le S�nat, l'Assembl�e examinera � son tour le 10 avril la proposition de loi socialiste. (Barbara Lewis et Sybille de La Hamaide, Marc Joanny et Emile Picy pour le service fran�ais, �dit� par Yves Clarisse)
Laisser un commentaire
