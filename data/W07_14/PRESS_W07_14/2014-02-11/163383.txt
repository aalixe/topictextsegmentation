TITRE: Silverstone d�voile le RVZ01.
DATE: 2014-02-11
URL: http://www.59hardware.net/actualit%25C3%25A9s/bo%25C3%25AEtiers/silverstone-d%25C3%25A9voile-le-rvz01.-2014021115641.html
PRINCIPAL: 163382
TEXT:
Silverstone d�voile le RVZ01.
Mardi, 11 F�vrier 2014 10:33 Laurent Dessinguez
Silverstone, que l�on conna�t aussi comme fabricant d�alimentations et de solutions de refroidissement, pr�sente aujourd�hui un nouveau bo�tier, le RVZ01.
Ce dernier a �t� con�u en vue d��tre compatible avec les cartes-m�res DTX et mini ITX, car ne mesurant que 382 mm de longueur sur 105 mm de hauteur sur 350 mm (14 litres de volume) et ne pesant 3.71 kg. M�lant � la fois plastique et acier, il ne comprend qu�une unique baie pour disque optique slim, une baie interne de 3.5 pouces et jusqu�� trois baies internes 2.5 pouces.
Du fait de sa taille r�duite, il faudra veiller � choisir les composants internes avec soin, notamment car la hauteur du ventirad est limit�e � 83, tandis que la carte graphique ne devra mesurer que 33 cm de long et d�un peu moins de 15 cm (14.93 cm) de large et l�alimentation sera obligatoirement au format SFX.
Le RVZ01 comporte deux ventilateurs de 12 cm, le premier dans la partie sup�rieure du bo�tier et le deuxi�me dans sa partie inf�rieure, � noter qu�il existe un troisi�me emplacement pour ventilateur de 120 mm en bas de la tour.
Le RVZ01 sera disponible d�s le 12 f�vrier (soit demain) au prix de 68 euros HT, soit un prix compris entre 90 et 100 euros.
