TITRE: D�c�s du r�alisateur Gabriel Axel
DATE: 2014-02-11
URL: http://fr.canoe.ca/divertissement/celebrites/nouvelles/2014/02/10/21459866-relaxnews.html
PRINCIPAL: 161877
TEXT:
D�c�s du r�alisateur Gabriel Axel
Gabriel Axel. Photo AFP�
10-02-2014 | 12h50
Derni�re mise � jour: 10-02-2014 | 12h57
Le r�alisateur Gabriel Axel, premier Danois � avoir remport� un Oscar, est mort dimanche � l'�ge de 95 ans, a annonc� lundi la t�l�vision publique DR, o� la fille du cin�aste est journaliste.
Il s'est �teint � son domicile de Bagsvaerd pr�s de Copenhague.
M. Axel �tait surtout connu pour Le festin de Babette, adaptation fid�le d'une longue nouvelle de la Danoise Karen Blixen. Il avait remport� l'Oscar du meilleur film �tranger en 1988.
Le film, o� se c�toient des acteurs danois, fran�ais et su�dois, avait failli ne jamais voir le jour, l'histoire de deux vieilles filles retir�es dans la campagne scandinave semblant peu prometteuse aux yeux des producteurs.
En 2010, le cardinal Jorge Bergoglio, futur pape Fran�ois, avait confi� dans un livre d'entretiens avec des journalistes argentins que ce film �tait son pr�f�r�.
�
