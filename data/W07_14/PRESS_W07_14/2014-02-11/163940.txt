TITRE: ENCADRE-Cour des comptes-Porte-avions fant�me et trains gratuits | Reuters
DATE: 2014-02-11
URL: http://fr.reuters.com/article/frEuroRpt/idFRL5N0LF2Q720140211
PRINCIPAL: 163822
TEXT:
ENCADRE-Cour des comptes-Porte-avions fant�me et trains gratuits
mardi 11 f�vrier 2014 10h21
�
[ - ] Texte [ + ]
PARIS, 11 f�vrier (Reuters) - Le versement "en pure perte" par la France de 196 millions d'euros au Royaume-Uni pour acc�der aux �tudes britanniques sur la construction d'un second porte-avions fant�me et les facilit�s de circulation offertes sur le r�seau SNCF sont �pingl�s dans le rapport de la Cour des comptes fran�aise.
Voici quelques remarques et recommandations de ce rapport  pr�sent� mardi.
* Second porte-avions :
La coop�ration franco-britannique pour la construction de porte-avions s'est traduite par un co�t de 196 millions d'euros pay�s par la France � la Grande-Bretagne, "une d�pense assum�e en pure perte par la France".
Le m�morandum de 2006 sur ce projet, suspendu en 2008 et abandonn� en 2013, "ne reposait pas sur une r�elle coop�ration mais sur un acc�s payant de la France aux r�sultats des �tudes britanniques", remarque la Cour des comptes.
Or, souligne-t-elle, plusieurs raisons laissaient penser qu'aucun projet industriel commun ne pouvait �tre mis en oeuvre, avant m�me la signature de ce m�morandum.
