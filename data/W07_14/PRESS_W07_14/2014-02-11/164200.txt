TITRE: La Belle et la B�te, une oeuvre aux multiples facettes
DATE: 2014-02-11
URL: http://blog.mothershaker.com/2014/02/la-belle-et-la-bete/
PRINCIPAL: 164196
TEXT:
Tweet
Plus que jamais, les adaptations ont la c�te ! Apr�s Frankenstein , c�est au tour de La Belle et la B�te de revenir le 12 f�vrier dans les salles obscures, incarn�es cette fois par L�a Seydoux et Vincent Cassel. Un classique qui ne cesse d�attirer les producteurs�
�
Une version fran�aise de La Belle et la B�te� avant une adaptation am�ricaine ?
Bande annonce de La Belle et la B�te, par Christophe Gans
Depuis le chef d��uvre de Cocteau, on en a vu des adaptations de La Belle et la B�te ! Probablement parce que l�histoire de ce conte est simplissime, que la morale peut �tre comprise par un enfant de 5 ans [�a tombe bien, c'est souvent le public vis� !] et que les r�alisateurs ont pris l�habitude d�en faire de libres adaptations. De la com�die dramatique qui captive les adultes au Disney pour enfants, en passant par le teen movie Sortil�ge avec la chanteuse pour ados Vanessa Hudgens� chacun y trouve � boire et � manger!
Le 12 f�vrier, ce sont donc deux stars du cin�ma hexagonal qui s�essayent � l�interpr�tation de ce classique: L�a Seydoux et Vincent Cassel. Le verdict des critiques est mi-figue, mi-raisin entre ceux qui y voient un bon blockbuster g�n�reux et les autres qui consid�rent que le film n�est pas � la hauteur de ses ambitions. Si cette version ne vous tente pas, peut-�tre que celle du r�alisateur mexicain Guillermo del Toro vous plaira ! Enfin, si elle voit le jour� Car le projet v�g�te depuis plusieurs ann�es et � part la pr�sence d�Emma Watson, le casting tarde � se faire conna�tre.
Une com�die musicale made in Broadway
La Belle et la B�te au th��tre Mogador � Paris
Des gros morceaux de Disney m�lang�s � des paillettes sorties de Broadway, le tout mis en sauce dans un magnifique th��tre parisien, vous obtiendrez un succ�s garanti ! Acclam�e par plus de 35 millions de spectateurs dans 21 pays, la com�die musicale La Belle et la B�te se joue � Paris depuis octobre 2013 au th��tre Mogador. L�id�al pour une sortie en famille.
Ce n�est pas la premi�re fois que La Belle et la B�te monte sur les planches puisque plusieurs op�ras et m�me un ballet ont repris l�histoire.
Dans les livres, sur sc�ne, au cin�ma, quelle est ta version pr�f�r�e de La Belle et la B�te ? Donne ton avis � la Mother sur Facebook , Twitter ou en commentaire de cet article.
The following two tabs change content below.
Derniers articles
Mother Shaker
Mother Shaker, le blog est un cocktail d�actus et de d�couvertes d�di� � tous ceux qui aiment l�art sous toutes ses formes.
