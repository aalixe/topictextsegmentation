TITRE: Kaspersky d�voile une vaste campagne de piratage informatique | Le Courrier strat�gique
DATE: 2014-02-11
URL: http://courrierstrategique.com/4452-kaspersky-devoile-une-vaste-campagne-de-piratage-informatique.html
PRINCIPAL: 164514
TEXT:
Post� par Andre� Touabovitch dans Coulisses le 11 f�vrier 2014 5:07   / Aucun commentaire
Des chercheurs de l�entreprise de s�curit� informatique Kaspersky Lab ont annonc� lundi avoir d�couvert un virus informatique d�di� au cyberespionnage qui aurait frapp� les gouvernements et entreprises de 31 pays depuis 2007.
Ce virus, connu sous le nom de � The Mask � ou � Careto � est particuli�rement complexe. Certaines de ses versions sont capables d�infecter des t�l�phones portables et des tablettes, y compris ceux qui disposent des syst�mes d�exploitation d�Apple ou Google.
Selon le communiqu� de Kaspersky Lab, le virus intercepte tous les canaux de communication et recueille les informations les plus essentielles de l�appareil de la victime.Une fois un appareil infect�, les pirates informatiques peuvent intercepter diff�rentes donn�es telles que le trafic internet, les frappes sur le clavier, les conversations via Skype et m�me voler les informations des appareils connect�s.
Le virus serait extr�mement difficile � d�tecter en raison de ses capacit�s de discr�tion, de ses fonctionnalit�s int�gr�es et de ses modules additionnels de cyberespionnage. L�enqu�te de Kaspersky a r�v�l� que les serveurs de commande du virus ont �t� arr�t�s le mois dernier, ce qui suppose que le virus �tait actif jusqu�� cette date.
Fait assez singulier dans les affaires de cyberespionnage selon les chercheurs, les concepteurs du virus parleraient espagnol. Cette campagne de piratage qui a permis le vol de documents sensibles tels que des cl�s d�encodage aurait �t� sponsoris�e par un Etat. Les principales cibles sont apparemment des gouvernements et missions diplomatiques, des entreprises du secteur �nerg�tique, des organismes de recherche, des soci�t�s de capitaux priv�es ainsi que des militants politiques. Il y aurait eu 380 victimes issues de 31 pays dont les Etats-Unis, la France, l�Allemagne et la Chine.
�
