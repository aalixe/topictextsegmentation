TITRE: Relations diplomatiques: La Chine et Ta�wan renouent le dialogue  -   Monde - lematin.ch
DATE: 2014-02-11
URL: http://www.lematin.ch/monde/La-Chine-et-Taiwan-nouent-un-dialogue-officiel-historique/story/17705517
PRINCIPAL: 162880
TEXT:
La Chine et Ta�wan renouent le dialogue
Relations diplomatiques
�
La Chine et Ta�wan ont renou� mardi un dialogue historique entre leurs gouvernements, une premi�re depuis la fin de la guerre civile en 1949.
Mis � jour le 11.02.2014
Poign�e de main historique entre Wang Yu-chi (g) et Zhang Zhijun.
Image: AFP
Vous avez vu une erreur? Merci de nous en informer.
Faute de frappe / orthographe
Veuillez SVP entrez une adresse e-mail valide
Partager & Commenter
Votre adresse e-mail*
Votre email a �t� envoy�.
La rencontre entre la Chine et Ta�wan intervient dans un contexte de d�gel des tensions et d'espoir d'�changes accrus au-dessus du d�troit de Formose.
Wang Yu-chi, l'officiel ta�wanais charg� des relations avec la Chine continentale, et son homologue Zhang Zhijun, chef du Bureau chinois des Affaires ta�wanaises, se sont salu�s par leurs titres officiels au d�but des discussions, a rapport� la t�l�vision de Hong Kong Phoenix.
Evoquant sa rencontre avec Zhang Zhijun, Wang Yu-chi a parl� d'une �occasion inimaginable dans le pass�. �Etre en mesure de s'asseoir et de discuter est vraiment une occasion de choix, car les deux parties �taient nagu�re pratiquement en guerre�, a-t-il fait remarquer, cit� par Chine nouvelle, l'agence officielle de P�kin.
�Un peu plus d'imagination�
De son c�t�, Zhang Zhijun a d�clar� � son interlocuteur ta�wanais que les deux parties devraient avoir �un peu plus d'imagination� concernant l'avenir des relations entre P�kin et l'�le nationaliste.
Ces entretiens d'un niveau in�dit ont pour cadre hautement symbolique Nankin, ville de l'est de la Chine que le camp nationaliste de Chiang Ka�-chek avait choisie comme capitale. Les pourparlers doivent se tenir jusqu'� vendredi.
La pi�ce dans laquelle s'est d�roul�e leur entrevue �tait d�cor�e de fa�on neutre, sans drapeau visible, ni titre officiel affich� sur la table des discussions. Cela afin de m�nager les sensibilit�s.
Libert� de la presse
Le diplomate ta�wanais avait annonc� en janvier qu'il �voquerait la cr�ation de bureaux de liaison, l'int�gration �conomique r�gionale et l'acc�s aux soins m�dicaux des �tudiants ta�wanais en Chine. La question de la libert� de la presse doit aussi �tre abord�e.
La Chine, elle, vise le retour de Ta�wan dans son giron, observe Jia Qingguo, professeur � l'universit� de P�kin. La Chine �attache probablement plus d'importance � l'acc�l�ration de l'int�gration �conomique, en pensant � l'unification politique � plus long terme�, estime-t-il.
Perc�e importante
Cette visite de Wang Yu-chi repr�sente une �perc�e importante�, a estim� mardi Chine nouvelle. Une port�e toutefois � relativiser, les experts n'imaginant pas que les dirigeants des deux territoires se rencontrent dans un avenir proche.
Cette premi�re entrevue entre deux officiels gouvernementaux illustre les efforts men�s depuis quelques ann�es de part et d'autre du d�troit de Formose pour panser les plaies de la guerre civile ayant d�bouch� en 1949 sur une partition de la Chine.
Relations apais�es
Cette ann�e-l�, deux millions de Chinois fid�les au chef nationaliste Chiang Ka�-chek, d�fait par les hommes de Mao, se r�fugient sur l'�le de Ta�wan. Depuis, P�kin et Taipei revendiquent s�par�ment leur pleine autorit� sur la Chine.
P�kin consid�re Ta�wan comme lui appartenant et n'a pas renonc� � la r�unification, par la force si n�cessaire. Mais les relations entre les deux entit�s se sont apais�es.
Aucune relation diplomatique
Apr�s de timides contacts dans les ann�es 1990, Ta�wan et la Chine communiste ont franchi en 2010 une �tape d�cisive sur la voie du d�gel en signant un accord-cadre de coop�ration �conomique sous l'impulsion du pr�sident ta�wanais Ma Ying-jeou.
Cet accord, et d'autres gestes d'ouverture comme la reprise des vols a�riens directs, n'ont toutefois �t� n�goci�s que par des organismes semi-officiels. P�kin et Taipei n'ont toujours aucune relation diplomatique. (ats/afp/Newsnet)
Cr��: 11.02.2014, 09h39
