TITRE: Gattaz, mauvais compagnon aux Etats-Unis - Lib�ration
DATE: 2014-02-11
URL: http://www.liberation.fr/economie/2014/02/11/gattaz-mauvais-compagnon-aux-etats-unis_979530
PRINCIPAL: 164937
TEXT:
Pierre Gattaz � Paris le 5 f�vrier.  (Photo Eric Piermont. AFP)
LES GENS
Le patron du Medef s�est attir� hier une salve de critiques apr�s les propos fielleux qu�il a tenus � Washington sur le �pacte de responsabilit�. En marge de la visite officielle de Fran�ois Hollande (lire pages 6-7), Pierre Gattaz s�est l�ch� devant quelques journalistes, jugeant �insupportable� la notion de �contreparties� :�Il faut arr�ter de g�rer par la contrainte [�]. On est toujours dans l�incitation n�gative en France [�]. Il faut coincer le gars et le punir.�
Gattaz �a tir� dans le dos de tous les Fran�ais�, s�est illico indign� le leader du Parti de gauche, Jean-Luc M�lenchon : �On �vite de se battre entre Fran�ais � l��tranger, surtout sur des questions qui concernent l��conomie, devant les Am�ricains.� Plus magnanime, Jean-Marc Ayrault a mis cette sortie sur le compte du �d�calage horaire [qui] peut parfois causer des probl�mes��
les plus partag�s
