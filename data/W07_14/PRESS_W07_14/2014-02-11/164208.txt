TITRE: Sotchi 2014. Longo et Baisamy �limin�s en demi-finale du half-pipe - France 3 Alpes
DATE: 2014-02-11
URL: http://alpes.france3.fr/2014/02/11/sochi-longo-et-baisamy-qualifies-pour-les-demi-finales-du-half-pipe-malgre-la-meteo-trop-douce-413259.html
PRINCIPAL: 164201
TEXT:
+  petit
Le grenoblois Arthur Longo et Johann Baisamy d'Avoriaz ont �t� �limin�s en demi finale du�snowboard half-pipe, ce mardi apr�s-midi � Sotchi.�
Ultra�favori l'am�ricain Shaun White est dores et d�j� qualifi� pour la finale qui doit se d�rouler dans la foul�e, de m�me que�le Japonais Ayumu Hirano.
La m�t�o leur a jouer des tours.�En effet, la temp�rature est excessivement douce. Trop douce car la neige est tr�s molle, ce que les sportifs ont du mal � g�rer.�
En demi-finales, les deux Tricolores ont chut� tous les deux dans leurs deux manches respectives pour terminer la comp�tition sur un go�t d'inachev�, puisque leur objectif initial �tait de faire partie des douze finalistes.
Longo a termin� tr�s loin de son potentiel, � une obscure 17e place. "Bien s�r que je suis d��u, a dit le rider des Deux Alpes. Les conditions ne m'ont pas avantag�, moi qui cherche la hauteur et la fluidit�. C'est un peu comme � Vancouver (19e en 2010, NDLR), j'aurais aim� mieux r�ussir".
"Je suis d��u, mais en m�me temps, j'ai tout donn� et je n'ai pas vraiment de regrets, a dit Johann�Baisamy, 16e au final. Les conditions �taient difficiles dans le pipe � cause de la neige molle qui cr�e des bosses et des trous.Il fallait en faire abstraction".
Une v�ritable "Soupe"
La neige ramollie, une v�ritable "soupe" par endroits, a jou� un r�le important lors de l'�preuve de ski slopestyle f�minin mardi. Les qualifications ont �t� marqu�es par de nombreuses chutes.
La hausse des temp�ratures n'a pas arrang� les choses, avec cette fois en ligne de mire le "pipe", ce demi-cylindre de neige, long de 234 m�tres, utilis� en half-pipe.
"Le milieu (la partie plate de transition entre les deux parois, ndlr) est compl�tement mou et lourd", a regrett� White.
Ce mardi vers 13h00, il faisait 11,8�C au Parc olympique � Sotchi, au niveau de la mer, o� se d�roulent les �preuves de glace.
En montagne, le thermom�tre a grimp� jusqu'� +2,9�C sur le site de ski de fond, � +7�C dans l'aire d'arriv�e du ski alpin et � +8,2�C dans l'enceinte de la piste de bobsleigh et de luge.
Les organisateurs ont activ� leur plan d'urgence pr�vu en cas de situation de ce type, c'est � dire�d'utiliser de la neige stock�e depuis l'hiver dernier.�
�
