TITRE: OGM : à quel jeu joue l'Allemagne ? - Europe1.fr - International
DATE: 2014-02-11
URL: http://www.europe1.fr/International/OGM-a-quel-jeu-joue-l-Allemagne-1798413/
PRINCIPAL: 164634
TEXT:
OGM : � quel jeu joue l'Allemagne ?
Par Ga�tan Supertino avec Marie-Laure Combes
Publi� le 11 f�vrier 2014 � 19h21 Mis � jour le 11 f�vrier 2014 � 19h22
Tweet
Un nouveau ma�s OGM, le TC1507, va �tre autoris� � la culture dans l'UE malgr� l'opposition de 19 pays et du Parlement europ�en � REUTERS
NI OUI NI NON - En s'abstenant, Berlin ouvre la voie � la culture d'un ma�s transg�nique en Europe. Culture interdite sur son propre territoire.
Abstention. L'Allemagne n'est pas si �cologiste que �a. Faute de "majorit� qualifi�e" entre les Etats membres de l'Union europ�enne, un deuxi�me ma�s OGM devrait bient�t �tre cultiv� sur le Vieux-continent, apr�s celui de Monsanto. Les 27 ne sont pas parvenus mardi � se mettre d'accord pour interdire le ma�s TC1507 de l'Am�ricain Pionner, � l'issue d'un d�bat public entre les ministres des Affaires europ�ennes, � Bruxelles. La faute notamment � Berlin, qui ne s'est pas prononc�e. R�sultat : la d�cision revient � la Commission europ�enne, qui s'est d�j� prononc�e en faveur de la culture de la c�r�ale controvers�e.
Comment Berlin a fait pencher la balance. Pour interdire la culture de cet OGM , il aurait fallu d�gager une "majorit� qualifi�e" au sein du Conseil europ�en. Mais l'Allemagne, en choisissant de s'abstenir, a fait capoter ce projet souhait� par Paris. Les abstentions de Berlin (29 voix), de la Belgique (12 voix), du Portugal (12 voix) et de la R�publique Tch�que (12 voix) ont en effet manqu� pour atteindre la majorit� de 260 voix.
Math�matiquement, les voix de l'Allemagne n'auraient certes pas suffi. Mais le choix de Berlin "aurait pu changer la donne", regrette un diplomate europ�en cit� par La Tribune . Car l'enjeu �tait de "rallier les autres abstentionnistes et de convaincre la Roumanie (14 voix) pr�te � changer son vote de pour � contre", selon ce diplomate.
>> Comment expliquer ce manque d'engagement de l'Allemagne, qui interdit pourtant la culture d'OGM sur son territoire ? �l�ments de r�ponses.
Pas de consensus en Allemagne. "Le gouvernement (allemand) a d�cid� de s'abstenir lors du vote sur l'autorisation de ce ma�s OGM TC1507. C'est la proc�dure habituelle quand il y a des avis divergents entre les minist�res au sein du gouvernement sur un dossier ou sur un sujet", avait expliqu� le porte-parole du gouvernement d'Angela Merkel, Steffen Seibert, le 5 f�vrier. En effet, au sein de la coalition allemande au pouvoir, les avis sont loin d'�tre les m�mes.
