TITRE: Ebooks : nouvel �chec d'Apple pour se s�parer de son observateur
DATE: 2014-02-11
URL: http://pro.clubic.com/entreprises/apple/actualite-618226-ebooks-nouvel-echec-apple-separer-observateur.html
PRINCIPAL: 0
TEXT:
Ebooks : nouvel �chec d'Apple pour se s�parer de son observateur
Publi� par Audrey Oeillet le mardi 11 f�vrier 2014
�
La Cour d'appel f�d�rale am�ricaine a rejet� une nouvelle demande d'Apple concernant son observateur dans l'affaire d'entente sur les prix des livres �lectroniques. Michael Bromwich reste en place, et l'entreprise doit se montrer coop�rative.
Les recours en justice d'Apple dans la fameuse affaire des ebooks se suivent et se ressemblent : la firme de Cupertino ne supporte pas son observateur Michael Bromwich depuis le d�part, mais elle va tout de m�me devoir travailler avec lui encore un an et demi. La Cour d'appel des �tats-Unis pour le deuxi�me circuit a en effet rejet� la derni�re d'Apple en date.
Michael Bromwich, l'avocat nomm� par la juge Denise Cote au poste d'observateur en octobre 2013, reste donc en place au sein d'Apple pour s'assurer que l'entreprise respecte bien les lois antitrusts en mati�re de livres �lectroniques. ��La d�cision d'aujourd'hui montre tr�s clairement qu'Apple doit maintenant coop�rer avec le contr�leur nomm� par le tribunal�� a comment� une porte-parole du minist�re de la justice am�ricain. Car, jusque-l�, Apple semble avoir tout fait pour freiner le travail de Bromwich, notamment en l'emp�chant d'interroger certains hauts cadres de l'entreprise.
N�anmoins, la Cour d'appel a pr�cis� que l'observateur n'a pas tous les pouvoirs : sa mission est d'observer et d'�valuer les pratiques de l'entreprise, et pas de prendre des mesures en cas de manquement. Il devra donc informer la juge Denise Cote de tout manquement. Par ailleurs, Michael Bromwich devrait voir son champ d'action limit� concernant les rencontres avec les dirigeants de l'entreprise.
Mais ce recadrage, s'il satisfait le minist�re de la justice am�ricain, ne plait toujours pas � Apple : outre le caract�re, jug� trop intrusif, de l'observateur, la soci�t� ne dig�re pas le fait de devoir r�mun�rer ce dernier 1100 dollars de l'heure . Une nouvelle proc�dure d'appel est en cours.
Pendant ce temps, le proc�s visant � d�dommager les consommateurs am�ricains ayant pris part au sein de multiples actions collectives, toujours li�es � l'affaire des livres �lectroniques, se pr�pare. Il devrait avoir lieu en mai : selon les derniers documents de justice, les plaignants pourraient r�clamer environ 840 millions de dollars � Apple.
Vous aimerez aussi
