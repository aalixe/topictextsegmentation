TITRE: Espace : une �toile vieille de 13,6 milliards d'ann�es
DATE: 2014-02-11
URL: http://www.linformatique.org/espace-une-etoile-vieille-de-136-milliards-dannees/
PRINCIPAL: 0
TEXT:
Accueil � Technologie � Espace : une �toile vieille de 13,6 milliards d�ann�es
Espace : une �toile vieille de 13,6 milliards d�ann�es
Publi� par : Emilie Dubois 11 f�vrier 2014 dans Technologie
Voter pour ce post
Selon des astronomes australiens, l��toile SMSS J0313000.36-670839.3 serait l��toile la plus ancienne jamais observ�e par l�homme, elle serait �g�e de 13,6 milliards d�ann�es.
Hormis son matricule barbare, l��toile SMSS J0313000.36-670839.3 est pourtant bien particuli�re. Observ�e par des astronomes australiens, cette �tole se serait form� environ 200 millions d�ann�es apr�s le Big Bang, ce qui lui donnerait un �ge de 13,6 milliards d�ann�es.
Alors que les pr�c�dentes �toiles les plus anciennes �taient �g�es de � seulement � 13,2 milliards d�ann�es, SMSS J0313000.36-670839.3 devient donc la doyenne des �toiles observ�es.
Concr�tement, SMSS J0313000.36-670839.3 est situ�e dans la Voie Lact�e, � quelque 6 000 ann�es-lumi�re de la Terre.
Se r�f�rant � sa quantit� de fer, qui est moins d�un millioni�me de celle de notre Soleil, les astronomes �taient leur affirmation que ce serait l��toile la plus ancienne jamais d�couverte � ce jour.
Le 10 f�vrier 2014, le Space Telescope Science Institute publie cette image de la plus vieille �toile jamais observ�e.
Share the post "Espace : une �toile vieille de 13,6 milliards d�ann�es"
