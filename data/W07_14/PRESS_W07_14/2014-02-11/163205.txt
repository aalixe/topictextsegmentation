TITRE: Leonardo DiCaprio: "Je ferais tout ce que Scorsese veut � l'�cran" - L'Express
DATE: 2014-02-11
URL: http://www.lexpress.fr/culture/cinema/leonardo-dicaprio-je-ferais-toute-ce-que-scorsese-veut-a-l-ecran_1322542.html
PRINCIPAL: 0
TEXT:
Voter (0)
� � � �
Leonardo DiCaprio, en lice pour l'Oscar du meilleur acteur pour son r�le de trader drogu� et d�bauch� dans "Le loup de Wall Street", a "les m�mes go�ts" que Martin Scorsese.
afp.com/Kevin Winter
Leonardo DiCaprio , en lice pour l'Oscar du meilleur acteur pour son r�le de trader drogu� et d�bauch� dans Le loup de Wall Street , a "les m�mes go�ts" que Martin Scorsese. Il serait m�me pr�t � faire � l'�cran "tout ce que veut" le r�alisateur. �
Interrog� par la presse lors du traditionnel d�jeuner des nomm�s, � Beverly Hills, l'acteur a tress� des lauriers � son cin�aste f�tiche, reconnaissant que leur dernier opus pr�sente � l'�cran "beaucoup de comportements abjects".�
"Nous voulions que ce soit un film �difiant et nous souhaitions montrer de fa�on exacte cette partie sombre de notre culture", a-t-il d�clar�. "Et c'�tait une r�action � ce qui s'est pass� en 2008", avec la crise financi�re.�
"Ses films n'ont pas d'�ge"
Le loup de Wall Street est en lice pour 5 Oscars , dont ceux de meilleur film, meilleur acteur et meilleur r�alisateur.�
Martin Scorsese "n'a jamais �t� un r�alisateur didactique qui impose au public les ramifications des actions" d�crites � l'�cran, dit-il. "Il ne juge pas ses personnages, il les montre tels qu'ils sont (...). C'est pour cela que ses films n'ont pas d'�ge".�
L'acteur a rendu hommage "� l'un des premiers cin�astes � m'avoir sid�r� quand j'�tais jeune. Il a inspir� toute ma g�n�ration".�
Martin Scorsese est mon cin�aste pr�f�r�
"Je suppose que notre relation s'explique par le fait que nous avons les m�mes go�ts", a ajout� l'acteur, dont c'est la cinqui�me collaboration avec Scorsese. "Je ferais tout ce qu'il veut � l'�cran".�
Jonah Hill , qui joue l'associ� du personnage interpr�t� par Leonardo DiCaprio, et qui est en lice pour l'Oscar du second r�le masculin, s'est montr� tout aussi admiratif.�
"Martin Scorsese est mon cin�aste pr�f�r�", a-t-il dit. "'Les Affranchis' est le film qui m'a donn� envie de faire du cin�ma et d'y consacrer ma vie. Je repeindrais sa maison s'il me le demandait".�
Avec
