TITRE: "Flappy Bird" retiré du web : panique ou coup marketing ? 4 hypothèses - le Plus
DATE: 2014-02-11
URL: http://leplus.nouvelobs.com/contribution/1144991-flappy-bird-retire-du-web-panique-ou-coup-marketing-4-hypotheses.html
PRINCIPAL: 164158
TEXT:
4
Recevoir les alertes
Le concept de "Flappy Bird" ? Faire voler un oiseau entre des tuyaux, sans jamais qu'il les touche. (DR)
�
Mais quelle mouche a donc piqu� Dong Nguyen, le d�veloppeur de "Flappy Bird" ? Pourquoi a-t-il donc choisi de supprimer son application � succ�s des plateformes de vente que sont Google Play et l�App Store�?
�
C�est par un tweet, publi� le 8 f�vrier 2014, qu�il nous a fait part de sa d�cision. Un choix bien myst�rieux qu�on peine � comprendre pour plusieurs raisons. Qu�est-ce qu�il a pouss� � se retirer, et � dire qu�il " ha�ssait ce jeu d�sormais "�? Voici mes quatre hypoth�ses.
�
I am sorry 'Flappy Bird' users, 22 hours from now, I will take 'Flappy Bird' down. I cannot take this anymore.
� Dong Nguyen (@dongatory) 8 F�vrier 2014
�
1. Il a vraiment p�t� les plombs
�
� en croire sa timeline Twitter, Dong Nguyen n�est pas vraiment l�arch�type de la vantardise. On sent quelqu�un d�humble et modeste sur qui un tsunami vient de s�abattre. Enfin un tsunami, entendons-nous. Il ne lui est rien arriv� de grave, bien au contraire.
�
Il y a un an, il a d�velopp� son jeu et 10 mois plus tard, on peut dire qu�il a tir� les bons num�ros. Bons graphismes, bon gameplay, essoufflement de Candy Crush � Il est arriv� au bon moment. Succ�s incroyable, surchauffe, craquage.
�
2. C�est un coup marketing
�
Il a �t� contact� par un studio. Ils vont affiner le d�veloppement et revendre "Flappy Bird" d�ici deux mois � 80 cents. Le temps de laisser mariner tous les fans du jeu, et de g�n�rer davantage de bruit autour de l�appli. Pari gagnant, quand on sait que des smartphones �quip�s du jeu se vendent d�j� sur eBay � plusieurs milliers de dollars �
�
3. Il a eu peur
�
Si "Flappy Birds" fait autant parler de lui, c�est aussi � cause des nombreuses accusations de plagiat dont l�application est la cible. On pense � ce Fran�ais, Kek, qui a d�velopp� "Piou Piou" il y a cinq ans sur Facebook et deux ans sur mobile � Les deux jeux sont extr�mement semblables.
�
On comprendrait que Dong Nguyen soit parti avec la caisse avant que les ennuis ne lui tombent vraiment dessus. La caisse, qui grossit de 50.000 dollars par jour gr�ce aux revenus publicitaires, d�ailleurs.
�
4. Il va revenir avec une version 2
�
Quid d�un "Flappy Bird" ? J�y crois moyen mais il suffit du soutien d�un studio, d�une campagne de communication � la Candy Crush et le tour sera jou�.
�
Ce que je retiens de cette actualit�, c�est que Dong Nguyen aura r�ussi une chose�: cr�er un effet collector. Il suffit d�aller jeter un �il du c�t� d�eBay et du montant auquel se vendent les iPhone sur lesquels "Flappy Bird" est install� C�est magique de voir comment le web s�est emball� pour cette appli, et l�extr�mit� des comportements que cela engendre. Une esp�ce de spirale sans fin, � mi-chemin entre la fascination et le bullshit.
�
Avec son gameplay addictif, ce petit oiseau qu�il faut faire passer entre des tuyaux qui semblent tomber du ciel, il a r�ussi � coinc� les gens devant leurs �crans de smartphone (en les faisant hurler de rage, de haine et de d�sespoir, il faut l�avouer)..
�
�
Maintenant, on n�a plus qu�� regarder les copycats du jeu prolif�rer sur le web� Ils sont d�j� des centaines disponibles aussi bien sur les navigateurs que sur smartphone. Alors, "Flappy Bird" a vraiment disparu�? Quels que soient les cas, pas vraiment.
�
Propos recueillis par Henri Rouillier .
�
