TITRE: Half-pipe : le pari compl�tement perdu de�Shaun�White - Lib�ration
DATE: 2014-02-11
URL: http://www.liberation.fr/sports/2014/02/11/half-pipe-le-pari-completement-perdu-de-shaun-white_979560
PRINCIPAL: 165002
TEXT:
Lire sur le readerMode zen
HISTOIRE
L�ex-�Flying Tomato� - rapport � sa tignasse rousse, qu�il a coup�e - s�est viand� hier en half-pipe. Apr�s une premi�re manche d�sastreuse (deux chutes), Shaun White n�a pas r�ussi � r�tablir la situation dans la seconde, malgr� un meilleur parcours, terni par un d�s�quilibre. Le snowboarder finit quatri�me, alors qu�il visait une troisi�me m�daille d�or olympique d�affil�e dans l��preuve du�demi-tube, apr�s ses victoires � Turin et Vancouver, ce�qu�aucun Am�ricain n�a r�ussi aux Jeux d�hiver.
A 27�ans, il�voit sa campagne de Sotchi tourner au fiasco, lui�qui avait par surprise d�clar� forfait pour l��preuve du�slopestyle (descente � obstacles), samedi, histoire de mieux se concentrer sur le half-pipe - et par peur de perdre face au niveau �lev� de la concurrence, ont rican� les critiques. White avait invoqu� aussi un risque de blessure. Le pari perdu du Californien qui brasse des millions de dollars � travers le sport, la mode, la musique et la pub a profit� au Suisse n� � Moscou Iouri Podladtchikov, surnomm� �Ipod�, sacr� champion � 25�ans devant deux Japonais, Ayumu Hirano, 15�ans (argent) et Taku Hiraoka, 18�ans (bronze).
