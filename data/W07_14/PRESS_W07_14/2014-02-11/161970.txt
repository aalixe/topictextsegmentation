TITRE: Plafond de la dette : les r�publicains am�ricains envisagent un vote mercredi
DATE: 2014-02-11
URL: http://www.lemonde.fr/ameriques/article/2014/02/11/plafond-de-la-dette-les-republicains-americains-envisagent-un-vote-mercredi_4363897_3222.html
PRINCIPAL: 0
TEXT:
Plafond de la dette : les r�publicains am�ricains envisagent un vote mercredi
Le Monde |
� Mis � jour le
11.02.2014 � 09h13
La Chambre des repr�sentants am�ricaine pourrait voter mercredi 12 f�vrier sur le plafond de la dette, selon plusieurs �lus r�publicains qui s'exprimaient lundi, alors que les Etats-Unis risquent de se retrouver en d�faut de paiement � partir du 27�f�vrier.
Le groupe r�publicain de la chambre s'est vu pr�senter lundi, lors d'une r�union de groupe � huis-clos, une proposition de loi suspendant jusqu'� mars�2015 la limite l�gale de la dette, selon plusieurs repr�sentants qui se sont exprim�s devant la presse.
Cette mesure permettrait au Tr�sor de continuer � emprunter jusqu'� la fin du mois de mars�2015, a expliqu� le repr�sentant, Matt Salmon. L'autorit� actuelle expire le 27�f�vrier, et les deux chambres du Congr�s (Chambre des repr�sentants et S�nat) doivent imp�rativement voter un texte commun d'ici l� pour la renouveler .
En �change, les r�publicains exigeraient une concession, selon le plan �voqu�: l'annulation d'une mesure budg�taire vot�e en d�cembre et qui ralentit le rythme de croissance des pensions militaires pour certains retrait�s. D'autres options seraient aussi envisag�es.
� VOTE DIFFICILE DANS TOUS LES CAS��
Les chefs de file n'ont pas confirm� officiellement qu'un vote aurait lieu mercredi et semblaient vouloir d'abord jauger le soutien dont le plan dispose au sein du groupe r�publicain. ��Nous allons essayer de trouver une solution cette semaine��, a seulement d�clar� Eric Cantor, chef de la majorit� r�publicaine � la Chambre des repr�sentants.
Les r�publicains semblaient tr�s divis�s entre mod�r�s et conservateurs, rappelant les d�chirements de la derni�re bataille sur le plafond de la dette, en octobre dernier. ��Ils n'ont pas les voix, c'est �vident��, a pr�dit Mo Brooks, en soulignant que de nombreux conservateurs voulaient des mesures de r�duction du d�ficit. ��Le vote sera difficile dans tous les cas��, a pr�dit Jason Chaffetz.
Les d�mocrates ont affirm� qu'ils ne voteraient que sur une loi ��propre�� de suspension du plafond de la dette, c'est-�-dire sans contrepartie. Mais le calendrier laisse peu de temps au Congr�s. Mercredi, la Chambre s'ajourne jusqu'au 25�f�vrier, deux jours avant l'ultimatum du Tr�sor.
Economie am�ricaine
