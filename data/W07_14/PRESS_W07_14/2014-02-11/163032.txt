TITRE: JO 2014 / CIO - CIO : L'Inde r�int�gr�e comme membre
DATE: 2014-02-11
URL: http://www.sport365.fr/jo-2014-sotchi/cio-l-inde-reintegree-comme-membre-1102302.shtml
PRINCIPAL: 163030
TEXT:
JO 2014 / CIO - Publi� le 11/02/2014 � 09h40
0
CIO : L'Inde r�int�gr�e comme membre
L'Inde a �t� r�int�gr�e comme membre du Comit� international olympique avec � effet imm�diat � pour les jeux Olympiques de Sotchi, a annonc� le CIO mardi.
Le Comit� international olympique (CIO) a annonc� avoir r�int�gr� l�Inde comme membre avec � effet imm�diat � aux jeux Olympiques de Sotchi. Cela signifie que les sportifs indiens pourront d�sormais courir sous leurs drapeaux, a annonc� le CIO mardi. Le CIO avait suspendu le Comit� olympique indien le 5 d�cembre 2012 en raison de diff�rentes ing�rences gouvernementales dans les affaires sportives et des probl�mes de gouvernance sur fond de corruption.
