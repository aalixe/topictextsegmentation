TITRE: Nantes. Les surveillants de prison de la r�gion d�noncent la surpopulation | Presse Oc�an
DATE: 2014-02-11
URL: http://www.presseocean.fr/actualite/nantes-les-surveillants-de-prison-de-la-region-denoncent-la-surpopulation-11-02-2014-96690
PRINCIPAL: 163322
TEXT:
A la maison d'arr�t, ils �taient d�j� une soixantaine ce mardi matin, � 9h.
Photo PO
Ils manifestent depuis 7 h ce matin, devant la maison d'arr�t de Carquefou.
Des surveillants de prison, venu de toute la r�gion, lancent un "cri d'alarme" et veulent alerter la Chancellerie.
Ils d�noncent notamment la surpopulation p�nale et le manque d'�ffectifs.
"Il n'y a jamais eu une telle tension ", constatent Alexis Grandhaie et Samuel Gauthier, de la CGT p�nitentiaire, qui se disent par ailleurs "tr�s d��us" par la politique p�nale men�e par le gouvernement.
Les agents demandent��galement la suspension de l'article 57 qui interdit les fouilles syst�matiques.
Dans la maison d'arr�t de Nantes, pourtant r�cente, une cinquantaine de matelas ont d�j� �t� pos�s au sol par palier le manque de place.
Lire Presse Oc�an demain mercredo 12 f�vrier
