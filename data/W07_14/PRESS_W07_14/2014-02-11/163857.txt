TITRE: Conf�rence Gen�ve-2:  �Le peuple syrien les implore d'arr�ter ce cauchemar� -   Monde - tdg.ch
DATE: 2014-02-11
URL: http://www.tdg.ch/monde/-Le-peuple-syrien-les-implore-d-arreter-ce-cauchemar/story/10271924
PRINCIPAL: 163855
TEXT:
Votre email a �t� envoy�.
Lakhdar Brahimi a d�clar� qu'il n'est pas satisfait et a demand� d'acc�l�rer le rythme.
�Je ne peux pas mettre un pistolet sur leur tempe�, a affirm� le m�diateur, en r�ponse � une question d'un journaliste lui demandant s'il ne peut pas imposer une solution.
�J'ai un pistolet, mais je ne voudrais pas m'en servir�, a-t-il ajout�. �Le peuple syrien les implore d'arr�ter ce cauchemar. Ils doivent entendre cet appel, le plus t�t sera le mieux�, a affirm� le diplomate alg�rien. �Nous devons faire des progr�s et rapidement. Je demande � toutes les parties d'acc�l�rer le rythme�, a-t-il dit.
D�but laborieux
�Le d�but de cette semaine est aussi laborieux� que lors du premier round, a confi� Lakhdar Brahimi. �Il n'y a pas beaucoup de progr�s, je vais m'efforcer autant que possible de r�ellement lancer le processus�, a-t-il d�clar�. �Mais pour que le processus d�marre, il faut la coop�ration des deux parties et beaucoup de soutien des parties qui ne sont pas pr�sentes�, a soulign� le m�diateur.
Il a confirm� qu'une r�union trilat�rale aura lieu vendredi � Gen�ve avec la secr�taire d'Etat am�ricaine adjointe aux affaires politiques Wendy Sherman et le vice-ministre russe des Affaires �trang�res Gennady Gatilov, tout en la qualifiant de �routine�. Puis, a-t-il indiqu�, il se rendra � New York pour pr�senter son rapport au secr�taire g�n�ral de l'ONU et au Conseil de s�curit�.
�Un succ�s� � Homs
Il a par ailleurs estim� que l'op�ration d'�vacuation des civils � Homs a �t� �un succ�s�, apr�s six mois de tractations, mais que beaucoup d'autres villes assi�g�es ont besoin d'aide. �Cette op�ration a �t� un succ�s, mais elle a �t� extr�mement risqu�e�, a dit Lakhdar Brahimi.
Le m�diateur a confirm� avoir propos� aux deux parties de discuter en parall�le de l'arr�t des violences et de la formation d'un gouvernement de transition. Il n'a pas voulu dire s'il rencontrera mercredi les deux d�l�gations s�par�ment ou ensemble. (ats/afp/Newsnet)
Cr��: 11.02.2014, 15h29
