TITRE: Deux mois d'attente pour aller chez l'ophtalmo - Europe1.fr - Santé
DATE: 2014-02-11
URL: http://www.europe1.fr/Sante/Deux-mois-d-attente-pour-aller-chez-l-ophtalmo-1797423/
PRINCIPAL: 162370
TEXT:
Deux mois d'attente pour aller chez l'ophtalmo
Par Benoist Pasteau avec AFP
Publi� le 11 f�vrier 2014 � 07h54 Mis � jour le 11 f�vrier 2014 � 07h54
Tweet
Photo d'illustration. � MAXPPP
SANT� - Une enqu�te sur l'attente pour obtenir un rendez-vous chez l'ophtalmo met en exergue les tr�s (trop) longs d�lais.
Soixante-dix sept jours, c'est le d�lai moyen en France pour obtenir un rendez-vous chez l'ophtalmologue, et ce d�lai peut atteindre 7 mois dans certains d�partements, voire un an chez certains praticiens, selon une enqu�te aupr�s de plus de 2.600 d'entre eux. 15% des ophtalmos ne sont plus en mesure d'accepter de nouveaux patients, selon cette enqu�te Yssup Research, rendue publique mardi, qui confirme les difficult�s d'acc�s � ces sp�cialistes.
A Paris, c'est rapide ! C'est � Paris, dans les Hauts-de-Seine (92), les Alpes-Maritimes (06) et les Bouches-du-Rh�ne (13) que les d�lais d'attente pour un rendez-vous sont les plus courts (de 24 � 40 jours en moyenne). Parmi les dix plus grandes villes, Paris, Marseille et Bordeaux sont celles o� l'attente est la moins longue avec un d�lai moyen pour obtenir un rendez-vous inf�rieur � un mois (respectivement 24,7 jours, 24,8 et 27,4 ).
Les d�partements les plus "lents" sont... A l'inverse, dans la Loire (42), dans le Finist�re (29), en Is�re (38) et en Seine-Maritime (76), les d�lais restent tr�s longs: sup�rieurs � 152 jours d'attente. A Rennes et Toulouse, l'attente d�passe les 3 mois. Mal lotie, la Loire bat tous les records: elle est en t�te des dix d�partements o� le d�lai d'obtention d'un rendez-vous est le plus long (205,3 jours en moyenne) apr�s prise de contact avec au moins 20 sp�cialistes, et elle d�tient le taux d'impossibilit� de prendre rendez-vous le plus �lev� (65%).
"Manque criant de professionnels". Cette enqu�te nationale, r�alis�e aupr�s de 2.643 ophtalmologues entre le 15 octobre 2013 et le 30 janvier 2014 par Yssup Research (groupe Publicis Health Care), conforte un r�cent rapport parlementaire qui souligne le "manque criant de professionnels", notamment de secteur 1, dont souffrent certaines r�gions, et des d�lais d'attente qui peuvent parfois atteindre 18 mois.
> Suivez l'info Europe 1 en continu sur , et r�agissez sur
2 Commentaires
Par occitanes � 10:07 le 11/02/2014
CONSULTATIONS MEDECINS
Des amies (gien, montargis, briare) doivent changer de d�partement pour aller chez le dentiste, opthalmo. Comme elles ont chang� de r�gion, les m�decins disent qu'ils ne prennent plus de nouveaux patients. Donc elles font 70 km pour se faire soigner; C'est dans le loiret. Et vous?
Par Laeclemar � 08:29 le 11/02/2014
???
Deux mois ? alors c'est que l'ophtalmo est un mauvais avec pas beaucoup de patient car en g�n�ral c'est plut�t trois � cinq mois m�me en r�gion parisienne... j'en sais quelque chose je prend les RDV
Tous les commentaires
D�sir au gouvernement, une "exfiltration calamiteuse" ?
Politique      09/04/2014 - 22:19:13
R�ACTIONS - Le d�sormais ex-patron du PS a �t� nomm� secr�taire d��tat aux Affaires europ�ennes. Les critiques fusent � droite mais aussi � gauche.
Christian Eckert�: le franc-parler de gauche au Budget
Politique      09/04/2014 - 21:27:18
PORTRAIT - Le nouveau secr�taire d'Etat au Budget est un ardent d�fenseur de la relance par la demande, mais sa mission est de serrer les vis.
Cambad�lis, "un homme de coups" � la t�te du PS ?
Politique      09/04/2014 - 21:13:17
PORTRAIT - Ancien trotskiste et ex-lieutenant de DSK, le th�oricien de la "gauche plurielle" devrait succ�der � Harlem D�sir.
Un gouvernement resserr� oui, mais sur le fil...
Politique      09/04/2014 - 18:07:37
INFOGRAPHIE - Le gouvernement Valls compte 30 membres. Depuis le d�but de la Ve R�publique, les gouvernements fran�ais en ont compt� en moyenne 35,5.
R�forme territoriale�: "ce sera encore plus de d�penses"
Politique      09/04/2014 - 17:25:53
VOTRE CHOIX D�ACTU DU 9 AVRIL � Le Premier ministre a propos� une r�duction du nombre de r�gions et la disparition des conseils d�partementaux.
R�ouverture du zoo de Vincennes : �a s'active
Culture      09/04/2014 - 17:25:46
J-2 - Le compte � rebours a commenc�. Samedi, le nouveau zoo de Vincennes ouvrira ses portes, apr�s six ans de travaux.
Politique      09/04/2014 - 14:39:14
PORTRAIT - Homme de r�seau et expert de l'Europe, Jean-Pierre Jouyet vient d'�tre nomm� secr�taire g�n�ral de l'Elys�e.
International      09/04/2014 - 14:27:16
APPEL - Ils ont lanc� une p�tition pour d�noncer "l'immobilisme du gouvernement" et exiger "une action politique forte".
007�: quel m�chant de James Bond �tes-vous�? �
Cin�ma      09/04/2014 - 14:00:32
TOP 5 - Chiwetel Ejiofor, l�acteur de 12 years a slave, est pressenti pour incarner le nouveau m�chant de James Bond. Et parmi les ennemis de 007, lequel est votre pr�f�r� ?
Proc�s Agnelet�: "ce qui tue, plus que la v�rit�, c�est le secret"
France      09/04/2014 - 12:54:32
A L�AUDIENCE - La cour d�assises a entendu les deux fils et l�ex-femme de Maurice Agnelet. Une famille bris�e.
Le flash
