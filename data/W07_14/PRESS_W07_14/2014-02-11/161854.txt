TITRE: Les manifestations se poursuivent en Bosnie-Herz�govine � International � 98,5 fm Montr�al
DATE: 2014-02-11
URL: http://www.985fm.ca/international/nouvelles/les-manifestations-se-poursuivent-en-bosnie-herzeg-299566.html
PRINCIPAL: 161849
TEXT:
Les manifestations se poursuivent en Bosnie-Herz�govine
Publi� par Associated Press le lundi 10 f�vrier 2014 � 17h57.
SARAJEVO, Bosnie-Herz�govine - Des milliers de personnes sont descendues dans les rues d'une dizaine de villes de Bosnie-Herz�govine, lundi, pour r�clamer le remplacement des politiciens par des experts impartiaux qui pourraient s'attaquer plus efficacement � un taux de ch�mage qui atteint 40 pour cent et � une corruption end�mique.
Il s'agissait de la sixi�me journ�e cons�cutive des pires manifestations � frapper ce pays balkanique depuis la fin de la guerre 1991-1995, qui a fait quelque 100 000 morts.
�Mon p�re, ma m�re et mon fr�re sont sans emploi, a dit une enseignante de 34 ans qui gagne 10 $ US par jour en tant que serveuse. J'en ai assez!�
Deux personnes �g�es agitaient une banni�re sur laquelle on pouvait lire que le salaire mensuel d'un politicien �quivaut � quatre ann�es de rentes de retraite moyennes. Les manifestants ont accus� des politiciens grassement pay�s d'�tre obs�d�s par les querelles ethniques.
�Ils vivent dans un monde diff�rent, ils sont compl�tement d�connect�s du peuple�, a dit Anes Podic, un ing�nieur informatique sans emploi stable.
Des manifestants se rassemblent quotidiennement pr�s du si�ge de la pr�sidence dans la capitale, Sarajevo, et dans une dizaine d'autres villes. Ils ont incendi� la pr�sidence et d'autres �difices vendredi et un graffiti pr�venait, �Celui qui s�me la faim r�colte la col�re�.
Les gouvernements r�gionaux de cinq villes, dont Sarajevo, ont d�missionn� bien avant les �lections pr�vues en octobre.
L'entente de paix qui a mis fin � la guerre a donn� naissance � un syst�me politique complexe qui voit quelque 4 millions de Bosniens �tre gouvern�s par plus de 150 minist�res. La corruption est end�mique tandis que les imp�ts �lev�s minent le pouvoir d'achat. Un Bosnien sur cinq vit sous le seuil de la pauvret�.
Une politologue au ch�mage, Svjetlana Nedimovic, a accus� l'Union europ�enne (UE) � dont les 28 ministres des Affaires �trang�res ont discut� de la situation en Bosnie lundi �, d'avoir tourn� le dos � son pays malgr� le soutien de l'organisation aux manifestants en Ukraine.
�Nous avons essay� les �lections, les manifestations pacifistes. Rien n'a march�. Tous ceux qui nous ont enseign� la d�mocratie se retirent�, a-t-elle d�plor�.
� Bruxelles, la chef de la diplomatie europ�enne, Catherine Ashton, a exhort� les autorit�s � garantir le droit des manifestants de protester pacifiquement et � r�pondre aux demandes de la population.
