TITRE: Rapport annuel de la Cour des comptes : la Chancellerie des universit�s de Paris vou�e � dispara�tre ? -  Educpros
DATE: 2014-02-11
URL: http://www.letudiant.fr/educpros/actualite/rapport-annuel-2014-de-la-cour-des-comptes-la-chancellerie-des-universites-de-paris-vouee-a-disparaitre.html
PRINCIPAL: 163703
TEXT:
Bonjour� Voici un article int�ressant : http://www.letudiant.fr/educpros/actualite/rapport-annuel-2014-de-la-cour-des-comptes-la-chancellerie-des-universites-de-paris-vouee-a-disparaitre.html �
0 commentaire
Place de la Sorbonne � Paris  � S.Blitman
Supprimer la Chancellerie des universit�s de Paris : c'est ce que demande la Cour des comptes dans son rapport annuel, rendu public le 11 f�vrier 2014, qui d�nonce une gestion d�faillante, � l'heure o� l'�tat cherche � faire des �conomies.
"Un op�rateur de l��tat inutile." La Cour des comptes ne m�che pas ses mots pour qualifier la Chancellerie des universit�s. Un chapitre de son rapport annuel est consacr� � l'organisme sous tutelle du minist�re de l'Enseignement sup�rieur, qui rassemble 60 agents et dont le budget de fonctionnement s'�l�ve � 14 millions d'euros.
Lire aussi
Fran�ois Weil (recteur de Paris et chancelier des universit�s) : "La chancellerie ne sera pas supprim�e" 12.02.2014
� l'issue d'un contr�le men� en 2012-2013, les magistrats de la rue Cambon d�noncent les "nombreuses carences" de la gestion de ce "patrimoine indivis des 13 universit�s". Et r�clament purement et simplement la suppression de la plus importante des 30 Chancelleries des universit�s � apr�s avoir demand� en vain, plusieurs fois par le pass�, celle des plus petites.
"Un gestionnaire de patrimoine inefficace"
Le rapport souligne les "d�faillances de la gestion immobili�re" de la Chancellerie des universit�s de Paris, propri�taire de plus de 15.000 m2 de locaux dans la capitale.
Le parc locatif parisien, dont la valeur a �t� estim�e � 123 millions d'euros en 2011, rapportait, en 2012, 3,22 millions. Mais la Cour note que "les loyers pratiqu�s par la Chancellerie sont, en moyenne, � un niveau inf�rieur � la valeur basse du march� pour les appartements de type F1 ou F2 et l�g�rement au-dessus de ce niveau pour les appartements de type F3 ou F4 et plus". Soit un manque � gagner de 14 � 34�%, selon que l'on prend en compte la fourchette haute ou basse des loyers.
Au-del� du volet financier, "la proc�dure d�affectation des logements appara�t peu transparente". Ancien secr�taire d��tat � l�Enseignement sup�rieur, inspectrice g�n�rale de l�administration de l'�ducation nationale et de la Recherche, fils d�un recteur aujourd�hui d�c�d� En d�finitive, "une dizaine de locataires de ces appartements ont un lien avec les minist�res charg�s de l��ducation nationale ou de l�Enseignement sup�rieur et ont pu acc�der au parc locatif de la Chancellerie gr�ce � leurs fonctions".
Traitement privil�gi� du recteur de Paris
Par ailleurs, le texte d�nonce une prise en charge injustifi�e des frais li�s � la fonction de recteur, qui pr�side le conseil d'administration de la Chancellerie : outre la "gestion complaisante [de son] logement de fonction", "les avantages en nature dont ont b�n�fici� les recteurs depuis 2000 n�ont pas fait l�objet des d�clarations sociales et fiscales r�glementaires".
Enfin, alors que r�glementairement, "seuls les frais li�s aux activit�s du recteur dans ses fonctions de chancelier des universit�s devraient �tre pris en charge par le budget de l��tablissement public", "c�est l�ensemble des frais de repr�sentation du recteur qui est financ� par la Chancellerie, soit un budget annuel d�environ 100.000��, comprenant la r�mun�ration d�un ma�tre d�h�tel".
Autant de d�rogations et irr�gularit�s auxquelles la rue Cambon entend mettre un terme.
Seule activit� � trouver gr�ce aux yeux des magistrats, l'attribution de bourses aux �tudiants, financ�es essentiellement par des legs. Une petite cinquantaine de prix ont ainsi �t� d�cern�s en 2011 et 2012, pour un montant de 430.000��. Insuffisant, cependant, pour peser dans la balance, d'autant plus que, souligne le rapport, "le positionnement de la Chancellerie est devenu anachronique tant par rapport � l��tat que vis-�-vis des universit�s dont l�autonomie est aujourd�hui renforc�e". D'o� la volont� r�it�r�e de la Cour de supprimer l'institution cr��e il y a plus de quarante ans.
�
La villa Finaly, un joyau qui gr�ve le budget de la Chancellerie
Patrimoine culturel prestigieux, la villa Finaly � Florence appartient aujourd'hui � la Chancellerie des universit�s de Paris, qui la met � disposition des professeurs pour l'organisation de congr�s et des chercheurs pour leurs s�jours d'�tudes.
Mais la Cour des comptes rel�ve qu'en 2012, "le taux d�occupation de la villa ne d�passe pas 24�%". Il en r�sulte un budget de fonctionnement "structurellement d�ficitaire", "bien que les treize universit�s franciliennes propri�taires de la villa apportent chacune une cotisation annuelle de 7.500��". Et le rapport de calculer que "de 2007 � 2011, le d�ficit cumul� s�est �lev� � 1,84 M�, soit une moyenne annuelle de 367.070 �".
Le CNDP �galement menac� de disparition
"Production �ditoriale d�pass�e", "distribution surdimensionn�e", "gestion co�teuse"... Le CNDP (Centre national de la documentation p�dagogique) est aussi dans le viseur de la Cour des comptes, qui pr�ne une r�organisation d'ampleur ou, � d�faut, sa suppression.
