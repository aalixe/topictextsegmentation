TITRE: Pourquoi Docteur
DATE: 2014-02-11
URL: http://www.pourquoidocteur.fr/Pesticides---une-etude-alarmante-sur-leur-toxicite-5370.html
PRINCIPAL: 0
TEXT:
Jusqu'� 1000 fois plu toxiques que pr�vu
Pesticides : une �tude alarmante sur leur toxicit�
Par : C�cile Coumau
Publi� le 11 F�vrier 2014
Les dangers des pesticides sont revus � la hausse. Selon une �tude du controvers� Pr Seralini, ils seraient jusqu'� 1000 fois plus toxiques qu'estim� pr�c�demment.�
Les dangers des pesticides ont-ils �t� sous-�valu�s�? C�est ce qu�affirme une nouvelle �tude men�e par le Pr Gilles-Eric S�ralini, le chercheur qui avait men� l��tude controvers�e sur les effets des OGM. Et il frappe fort � nouveau. Selon ses travaux men�s avec ses coll�gues de l�universit� de Caen et le Comit� de recherche et d�information ind�pendantes sur le g�nie g�n�tique (Criigen) , publi�s dans la revue BioMed Research International , les pesticides seraient ��deux � mille fois plus toxiques�� que ce qui a �t� annonc� jusqu�� maintenant.
Si le Pr Gilles-Eric S�ralini parvient � des r�sultats aussi alarmants, c�est parce qu�il a �valu� la toxicit� des pesticides non pas sur la seule substance active mais sur les formulations compl�tes des pesticides commercialis�s. Autrement dit, avec les adjuvants, ��qui sont souvent gard�s confidentiels�et appel�s inertes par les fabricants �, pr�cisent les auteurs de l��tude.
Le "Roundup" au top de la toxicit�
La toxicit� de 9 pesticides a donc �t� test�e, en comparant les principes actifs et leurs formulations, sur trois lign�es cellulaires humaines. Parmi les herbicides , les insecticides et les fongicides, ce sont les derniers � les fongicides � qui se sont r�v�l�s les plus toxiques, avec des concentrations pourtant de 300-600 fois plus faibles que les dilutions agricoles.
��Malgr� sa r�putation relativement b�nigne��, �crivent les chercheurs, le Roundup �tait de loin le plus toxique parmi les herbicides et les insecticides test�s. Et surtout, 8 formulations sur 9 �taient plusieurs centaines de fois plus toxique que leur principe actif.
La dose journali�re admissible � revoir
Ces r�sultats font dire au Pr S�ralini et � ses coll�gues que la dose journali�re admissible pour les pesticides doit �tre revue car cette norme est calcul�e � partir de la toxicit� du principe actif seul.
Reste maintenant � savoir quel accueil va recevoir ce nouveau pav� dans la mare du Pr S�ralini. Sa pr�c�dente �tude sur les rats avait finalement �t� retir�e de la revue o� elle avait �t� publi�e. Elle estimait que ��les r�sultats pr�sent�s, s�ils ne sont pas incorrects, ne permettent pas de conclure��. Par ailleurs, l'Autorit� europ�enne de s�curit� alimentaire (Efsa) avait conclu en octobre 2013 que ��malgr� un important volume de donn�es, des conclusions fermes ne peuvent �tre �tablies pour la majorit� des effets sanitaires consid�r�s.�� L'Efsa indiquait n'avoir identifi� que deux pathologies dont le risque de survenue �tait statistiquement associ� � l'exposition aux pesticides: les leuc�mies infantiles et la maladie de Parkinson.
Quelques mois auparavant, en juin 2013, l�Inserm avait en revanche estim� qu�il existait � une association positive entre exposition professionnelle � des pesticides et certaines pathologies chez l�adulte��. Et la liste �tait longue�: cancer de la prostate, leuc�mies, maladies neurod�g�n�ratives, Parkinson��
Sur le m�me th�me
