TITRE: Mali, Burundi, Hassan Rohani, Bosco Ntaganda... Le tour du monde en 5 images � metronews
DATE: 2014-02-11
URL: http://www.metronews.fr/info/mali-burundi-hassan-rohani-bosco-ntaganda-le-tour-du-monde-en-5-images/mnbk!6yLDjOWl5D7g/
PRINCIPAL: 162658
TEXT:
Mis � jour  : 11-02-2014 09:41
- Cr�� : 11-02-2014 08:20
Le tour du monde en 5 images : intemp�ries meurtri�res au Burundi
INTERNATIONAL - Enl�vement d'une �quipe humanitaire du CICR au Mali, inondations au Burundi, 35e anniversaire de la r�volution islamique en Iran... Metronews vous livre un tour rapide de l'actualit� internationale.
Tweet
�
Mali - L'enl�vement d'une �quipe du Comit� international de la Croix-Rouge (CICR) au Mali a �t� revendiqu� mardi par un responsable du Mouvement pour l'unicit� et le djihad en Afrique de l'Ouest (Mujao). "Nous avons pris (...) un (v�hicule) 4X4 des 'ennemis de l'islam' avec leurs complices", a en effet d�clar� un responsable connu du Mujao, dans un entretien t�l�phonique avec un journaliste de l'AFP. "Ils sont en vie et en bonne sant�", a-t-il dit, sans plus de d�tails. Les cinq personnes qui auraient �t� enlev�es sont quatre membres du CICR et un v�t�rinaire d'une autre organisation humanitaire, tous des Maliens.
Burundi - Les pluies diluviennes qui touchent actuellement le Burundi causent des d�g�ts consid�rables. Au moins 60 personnes sont mortes dans la nuit de dimanche � lundi dans la capitale Bujumbura. C'est le plus lourd bilan li� � des intemp�ries jamais enregistr� dans la capitale de ce pays d'Afrique centrale. "Ce sont surtout les enfants qui sont victimes", a pr�cis� le porte-parole de la Croix-Rouge burundaise, Alexis Manirakiza. Les autorit�s craignent que le bilan ne s'alourdisse encore fortement.
Iran - Les autorit�s iraniennes sont en liesse mardi, c�l�brant le 35e anniversaire de la r�volution islamique . Un �v�nement qui intervient dans un contexte marqu� par des avanc�es dans les n�gociations nucl�aires avec les grandes puissances. La journ�e du 11 f�vrier, le 22 Bahman dans le calendrier iranien, est l'occasion de d�fil�s dans tout le pays et le pr�sident Hassan Rohani doit faire un discours � T�h�ran.
RDC - On l'appelle tristement Terminator. L'ex-chef de guerre congolais Bosco Ntaganda compara�t depuis lundi devant la Cour p�nale internationale afin de d�terminer s'il doit �tre jug� notamment pour des viols, meurtres et pillages commis dans l'est de la R�publique d�mocratique du Congo. L'homme a eu un "r�le central" dans les crimes "ethniques" commis dans l'est du pays en 2002 et 2003, notamment le viol d'enfants soldats, a accus� lundi la procureure de la Cour p�nale internationale. R�put� sans piti�, Bosco Ntaganda est �galement soup�onn� d'avoir lui-m�me viol� et r�duit en esclavage sexuel des jeunes filles de moins de 15 ans.
Br�sil - Les manifestations contre la vie ch�re au Br�sil depuis jeudi ont fait un premier mort. Un cameraman br�silien bless� � Rio a �t� diagnostiqu� lundi en �tat de "mort c�r�brale", alors qu'une nouvelle manifestation avait lieu dans la soir�e dans la deuxi�me plus grande ville du pays. Il avait �t� frapp� jeudi en pleine t�te par une fus�e d'artifice allum�e par un manifestant en direction de la police. Les manifestations ont eu lieu � l'appel du mouvement "Passe Livre", contre une nouvelle hausse du prix du ticket d'autobus.
Ozal Emier
