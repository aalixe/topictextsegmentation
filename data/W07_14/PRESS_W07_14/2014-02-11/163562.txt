TITRE: Cyber-espionnage : un �tat se cache derri�re le virus Careto.
DATE: 2014-02-11
URL: http://www.59hardware.net/actualit%25C3%25A9s/soci%25C3%25A9t%25C3%25A9-et-business/cyber-espionnage-:-un-%25C3%25A9tat-se-cache-derri%25C3%25A8re-le-virus-careto.-2014021115643.html
PRINCIPAL: 163557
TEXT:
Cyber-espionnage : un �tat se cache derri�re le virus Careto.
Mardi, 11 F�vrier 2014 13:53 Laurent Dessinguez
Les experts de la s�curit� informatique de la soci�t� Kaspersky ont d�cel� plusieurs �l�ments pour le moins �tranges dans le virus Careto, qui les am�neraient � penser qu�il y aurait un Etat � l�origine d�attaques qui ont frapp� gouvernements et entreprises de 31 nations.
Le virus Flame dans ses oeuvres.
En l�occurrence, Careto (masque en fran�ais) est un logiciel malveillant capable non seulement d�infecter ordinateurs, tablettes et t�l�phones portables mais �galement d�intercepter� toutes sortes de donn�es�: trafic internet, tout ce qui est tap� au clavier, conversations Skype, mots de passe, cl�s VPN etc. etc.
Plusieurs structures auraient �t� touch�es, structures �tatiques, missions diplomatiques, entreprises priv�es, organismes de recherche, soci�t� de capitaux priv�s et m�me des individus comme des militants politiques par exemple, le tout dans 31 pays �
D�apr�s Kaspersky, les serveurs de commande auraient �t� coup�s pendant l�enqu�te qu�il a lanc�e, ceci entre autres choses d�notant l��uvre de professionnels, tout comme le fait qu�ils soient rest�s invisibles jusqu�� ce jour.
Costin Raiu, expert pour le compte de Kaspersky d�ajouter�: ��Nous avons plusieurs raisons de croire qu'il s'agit d'une campagne sponsoris�e par un Etat�� � ��Un tel degr� de s�curit� op�rationnelle n'est pas normal pour des groupes cyber-criminels��.
Et le fait que Careto soit vraisemblablement le fait d�hispanophones n�est pas le d�tail le moins �trange de l�affaire�: ��Alors que la plupart des attaques (informatiques) connues de nos jours sont remplies de commentaires en chinois, les langues comme l'allemand, le fran�ais ou l'espagnol sont tr�s rares��.
