TITRE: Comme Shirley Temple, ces enfants stars au cin�ma - Cin�ma - MYTF1News
DATE: 2014-02-11
URL: http://lci.tf1.fr/cinema/photo/comme-shirley-temple-ces-enfants-stars-au-cinema-8363792.html
PRINCIPAL: 0
TEXT:
?
Cin�ma
Shirley Temple s'est �teinte lundi 10 f�vrier � l'�ge de 85 ans. D�couverte � 3 ans, l'actrice �tait la premi�re enfant star du cin�ma. Depuis, bien d'autres ont suivi...
?
Image en cours
?
Le 10 f�vrier 2014, le monde apprenait la disparition de l'actrice Shirley Temple � l'�ge de 85 ans. Premi�re vraie enfant-star du cin�ma am�ricain, l'Am�ricaine avait �t� propuls�e sur le devant de la sc�ne dans les ann�es 30.� D�couverte � l'�ge de trois ans, elle signe un contrat d'exclusivit� avec la Twentieth Century Fox qui la fera acc�der au rang de superstar mondiale. Le visage malicieux, une blondeur dor�e et boucl�e, la petite fille avait tout pour plaire.
Si avant elle le monde ignorait ce qu'�tait le ph�nom�ne " enfant star", depuis l'�re Temple, Hollywood a pris l'habitude de d�celer t�t le potentiel t�l�g�nique de jeunes acteurs et actrices. Aujourd'hui, plusieurs noms tr�s connus du cin�ma international ont entam� leur carri�re alors qu'ils n'�taient encore que des enfants. Drew Barrymore en est un exemple frappant. Issue d'une famille de cin�ma, l'actrice a d�but� � l'�ge de six ans. Le public a pu d�couvrir sa bouille dans le film de son parrain Steven Spielberg, E.T - L'Extraterrestre. D'autres ont suivi : Kirsten Dunst (d�couverte dans Entretien avec un vampire), Macaulay Culkin (r�v�l� dans Maman j'ai rat� l'avion), etc. Plus r�cemment, c'est gr�ce aux huit films de la saga Harry Potter que le trio Daniel Radcliffe - Emma Watson - Rupert Grint a connu la gloire alors que les trois com�diens avaient � peine dix ans � la sortie du premier volet.
Quelque peu malgr� elle, Shirley Temple est donc devenue une ic�ne comme Hollywood en a rarement connu. Une ic�ne � l'origine d'un v�ritable ph�nom�ne dans le cin�ma am�ricain et international. Aujourd'hui, l'enfant star fait partie int�grante du syst�me.� MYTF1News revient sur ces enfants-acteurs devenus des stars d�s le plus jeune �ge.
