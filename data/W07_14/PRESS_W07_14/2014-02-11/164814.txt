TITRE: Nouveaux "tr�sors" artistiques retrouv�s chez un octog�naire allemand - DH.be
DATE: 2014-02-11
URL: http://www.dhnet.be/dernieres-depeches/afp/nouveaux-tresors-artistiques-retrouves-chez-un-octogenaire-allemand-52fa60a63570c16bb1cb0880
PRINCIPAL: 164813
TEXT:
Vous �tes ici: Accueil > Derni�res d�p�ches
Nouveaux "tr�sors" artistiques retrouv�s chez un octog�naire allemand
Publi� le
11 f�vrier 2014 � 18h28
Berlin (AFP)
Plus de 60 ?uvres d'art, dont des Monet, Renoir, Picasso, ont �t� retrouv�es dans la propri�t� autrichienne d'un octog�naire allemand chez qui un "tr�sor nazi" avait d�j� �t� d�couvert en Allemagne, a r�v�l� mardi son porte-parole.
Cornelius Gurlitt, fils d'un marchand d'art au pass� trouble sous le Troisi�me Reich, �tait apparu sur le devant de la sc�ne en novembre dernier quand la presse allemande a annonc� la d�couverte au printemps 2012 de plus de 1.400 ?uvres de ma�tres tels que Chagall ou Matisse, dans son appartement de Munich (sud).
Cette fois-ci, les ?uvres se trouvaient dans une maison que l'octog�naire poss�de � Salzbourg dans le nord de l'Autriche. "Il s'agit de plus de 60 pi�ces, parmi lesquelles des ?uvres de Monet, Renoir ou Picasso", a indiqu� Stephan Holzinger, un sp�cialiste de la communication de crise auquel l'avocat munichois Christoph Edel, charg� d'assurer la tutelle de M. Gurlitt, a confi� le r�le de porte-parole du vieil homme.
"Des experts, mandat�s par Cornelius Gurlitt, examinent s'il pourrait s'agir d??uvres �ventuellement vol�es sous le nazisme", pr�cise M. Holzinger dans un communiqu�, ajoutant que "d'apr�s un premier �tat des lieux, un tel soup�on ne s'est pas v�rifi�". Il n'a pas indiqu� la valeur de ces pi�ces.
Selon le porte-parole, les ?uvres ont �t� plac�es en s�ret� pour pr�venir toute tentative de vol. Mais plusieurs questions restent en suspens notamment pourquoi d�cider de r�v�ler aujourd'hui l'existence de ces ?uvres, quels experts vont �t� charg�s d'enqu�ter sur ces ?uvres, ces derni�res seront-elles rendues publiques sur Internet comme l'ont �t� les pr�c�dentes?
Interrog� par l'AFP, le parquet d'Augsbourg (sud), en charge de l'affaire Gurlitt s'est content� d'indiquer que les enqu�teurs avaient pris connaissance de cette nouvelle information avec int�r�t.
Cornelius Gurlitt, 81 ans, soup�onn� de fraude fiscale et recel en Allemagne, a �t� laiss� en libert� et fait l'objet d'une enqu�te judiciaire, conduite sous l'autorit� de ce parquet.
1.400 oeuvres retrouv�es
Il a �t� provisoirement plac� sous tutelle pour raisons m�dicales � la suite d'une requ�te d�pos�e par les m�decins d'un h�pital o� il �tait soign�, une proc�dure qui le prive en partie de la capacit� de prendre seul certaines d�cisions. Malgr� cela M. Gurlitt semble d�cid� � se battre pour garder "ses" toiles, m�me s'il a fait des d�clarations contradictoires � ce sujet.
M. Gurlitt est le fils de Hildebrand Gurlitt, grand galeriste qui s'�tait constitu� une collection dans les ann�es 30 et 40. Un moment menac� par les nazis, notamment parce qu'il avait une grand-m�re juive, il avait ensuite servi le r�gime en �coulant � l'�tranger, moyennant devises, des ?uvres vol�es ou saisies.
Parmi les quelque 1.400 ?uvres retrouv�es dans l'appartement de Munich de Gurlitt, se trouvent des dessins, gravures et peintures dont des Picasso, Matisse, Renoir, Otto Dix, dont la majeure partie pourrait avoir �t� soit vol�e ou extorqu�e � des familles juives, soit saisie dans les mus�es comme faisant partie de ce que les nazis classaient dans la cat�gorie "Art d�g�n�r�".
L'affaire a relanc� un d�bat sur la restitution des ?uvres d'art vol�es aux Juifs sous le nazisme, les autorit�s allemandes ayant notamment �t� accus�es d'avoir tard� � faire toute la lumi�re sur les possessions de Gurlitt.
L'Allemagne souhaite se doter d'une loi facilitant ces restitutions. Un projet a �t� �labor� pour notamment abolir la prescription de trente ans au-del� de laquelle la propri�t� d'une ?uvre d'art ne peut plus �tre contest�e, si le d�tenteur est consid�r� comme de "mauvaise foi", c'est-�-dire s'il connaissait la provenance de l'objet au moment de son acquisition.
Baptis�e "Lex Gurlitt" par les m�dias, le texte doit �tre examin� une premi�re fois vendredi par le Bundesrat, la chambre haute du parlement allemand o� si�gent les repr�sentants des Etats f�d�r�s.
� 2014 AFP. Tous droits de reproduction et de repr�sentation r�serv�s. Toutes les informations reproduites dans cette rubrique (d�p�ches, photos, logos) sont prot�g�es par des droits de propri�t� intellectuelle d�tenus par l'AFP. Par cons�quent, aucune de ces informations ne peut �tre reproduite, modifi�e, rediffus�e, traduite, exploit�e commercialement ou r�utilis�e de quelque mani�re que ce soit sans l'accord pr�alable �crit de l'AFP.
Derni�res d�p�ches
