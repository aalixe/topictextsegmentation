TITRE: Alg�rie/crash de l'avion militaire: le dernier bilan s'�tablit � 76 morts et un rescap� - china radio international
DATE: 2014-02-11
URL: http://french.cri.cn/621/2014/02/12/441s368152.htm
PRINCIPAL: 0
TEXT:
Alg�rie/crash de l'avion militaire: le dernier bilan s'�tablit � 76 morts et un rescap�
��2014-02-12 05:26:15��xinhua
Le dernier bilan provisoire du crash de l'avion militaire alg�rien survenu dans la journ�e de mardi � l'est du pays fait �tat de 76 morts, dont quatre femmes et un rescap�, ont indiqu� des m�dias officiels.
Citant une source locale de la protection civile, la Radio alg�rienne a indiqu� que les recherches se poursuivent dans des conditions difficiles dans un p�rim�tre de moins en moins accessible aux membres de l'Arm�e et de la protection civile d�p�ch�s sur les lieux du crash.
Un pr�c�dent bilan faisait �tat de la mort de 52 personnes.
L'avion de type Hercule C-130 qui s'�tait �cras� sur le mont Fertas dans la localit� d'Ain Kercha, relevant de la province d'Oum El Bouaghi (470 km au sud-est d'Alger), et qui assurait une liaison entre Tamanrasset (extr�me sud du pays) et Constantine (400 km au sud-est de la capitale), transportait "77 personnes, dont les membres de l'�quipage", a pr�cis� une source militaire � l'agence officielle APS.
Selon le responsable de la communication � la 5�me r�gion militaire (RM), le colonel Lahmadi Bouguern, le crash se serait produit quand l'avion avait entam� les man�uvres d'approche de l'a�roport de Constantine.
