TITRE: �Abus de faiblesse� : Rocancourt attaque le film pol�mique de Breillat - Actualit� Cin�ma - 11/02/2014 - leParisien.fr
DATE: 2014-02-11
URL: http://www.leparisien.fr/cinema/actualite-cinema/abus-de-faiblesse-rocancourt-attaque-le-film-polemique-de-breillat-11-02-2014-3580869.php
PRINCIPAL: 0
TEXT:
�Abus de faiblesse� : Rocancourt attaque le film pol�mique de Breillat
�
Tweeter
Le film de Catherine Breillat, �Abus de faiblesse�, sort mercredi en salle. Il �voque la relation entre une r�alisatrice et un jeune escroc. Christophe Rocancourt, condamn� pour avoir abus� de la r�alisatrice, attaque le film en justice.� | LE PARISIEN/MATTHIEU DE MARTIGNAC
R�agir
Toute ressemblance avec des personnes existantes serait purement fortuite.... Cette petite phrase ne figure pas au g�n�rique de �Abus de faiblesse�, le film de Catherine Breillat qui sort ce mercredi dans les salles. Pourtant, la r�alisatrice �g�e de 64 ans, qui a d�j� racont� cette histoire dans un livre qui portait le m�me titre (publi� en 2009) en a fait une adaptation � l��cran, sans doute tr�s proche de ce qu�elle a v�cu lors de sa relation avec le fameux Christophe Rocancourt .
Vos amis peuvent maintenant voir cette activit� Supprimer X
SUR LE M�ME SUJET
� Abus de faiblesse � : Kool Shen, ce n'est pas une escroquerie**
�
Ce mardi, ce dernier, qui avait gagn� le surnom de �l'escroc des stars� apr�s plusieurs condamnations judiciaires, a annonc� qu'il allait engager des poursuites pour atteinte � la vie priv�e contre le film.��Christophe Rocancourt est un produit qui se vend, mais la question qui se pose est celle de la protection de l'image. C'est comme s'il �tait condamn� � perp�tuit� � �tre le symbole de l'escroc. C'est une atteinte intol�rable � ce qui constitue sa personne m�me�, a assur� l' avocat de celui qui avait condamn� en 2012 pour abus de faiblesse sur la r�alisatrice .
�Ce film n�est pas un biopic, ni une adaptation de mon livre �crit avec Jean-Fran�ois Kerv�an. Et il n�est pas plus autobiographique que mes autres longs m�trages�, insiste, interrog�e par le Parisien, Catherine Breillat que l�on sent terroris�e � l�id�e qu�on puisse penser qu�elle exploite sa propre histoire.
VIDEO. La bande-annonce d'�Abus de faiblesse�
Alors, �Abus de faiblesse�, fiction ou r�alit� ? Retour sur une longue histoire... R�alisatrice de � Parfait amour� �Romance�, �Une vieille ma�tresse� -des films souvent controvers�s en raison de sc�nes hard, Breillat est victime d�un accident vasculaire c�r�bral le 5 avril 2005.
Deux ans plus tard, alors qu�elle souffre encore d�h�mipl�gie et de crises d��pilepsie, la cin�aste entre en contact avec Christophe Rocancourt, qui a d�j� purg� cinq ans de prison aux Etats-Unis pour avoir escroqu� le tout-Hollywood. Il est d�ailleurs question � l��poque que le producteur Thomas Langmann fasse un biopic de ses aventures � Los Angeles.
De son c�t�, la r�alisatrice fascin�e par cet arnaqueur qui partage alors la vie de Sonia Rolland lui propose un r�le dans un film intitul� �Bad Love�, qu�elle doit tourner avec Naomi Campbell. Son producteur Jean-Fran�ois Lepetit (�Trois hommes et un couffin�) pas vraiment tr�s chaud sur un tel choix, lui d�conseille de l�engager.��
Mais Catherine persiste et lui confie �galement l��criture d�un sc�nario intitul� �La vie amoureuse de Christophe Rocancourt�. A la signature de ce contrat, elle lui remet 25000 euros. L�arnaqueur aurait flairer le bon filon. En l�espace d�un an et demi, la r�alisatrice lui fait douze autres ch�ques d�un montant total de 703000 euros. Breillat r�agit et l'attaque en justice, l'accusant d�avoir profit� de son handicap. Christophe Rocancourt est condamn� en f�vrier 2012 � 16 mois de prison dont huit ferme, ainsi qu�� 578000 euros de dommages et int�r�ts pour abus de faiblesse au pr�judice de la r�alisatrice.
Reste qu�� l��cran, on d�couvre Isabelle Huppert incarnant Maud, une r�alisatrice qui se r�veille un matin avec un corps � moiti� mort. D�termin�e, elle se bat pour r�cup�rer de la vie dans ses membres. Et surtout faire un film avec un jeune homme, Vilko (Kool Shen) qui l�entoure de plus en plus et sait se rendre indispensable en se proclamant son ami pour toujours.
Entre ces deux personnages, la situation devient �trange. Maud r�siste quand il lui r�clame de nouveaux ch�ques. Breillat a mis plus de deux ans � �crire ce sc�nario alors que, la plupart du temps, elle arrive � une premi�re version au bout de trois semaines. �Mais il y a une raison �vidente derri�re tout cela : j�avais peur de faire ce film�.
Au final, �Abus de faibless� est un film quasiment clinique sur une relation d�un r�alisme cruel. La performance du tandem Isabelle Hupper/Kool Shen est au diapason. L�ex-rappeur de NTM est parfait. Une carri�re d�acteur s�ouvre � lui. Rocancourt se reconna�tra-t-il en lui ?�
�
