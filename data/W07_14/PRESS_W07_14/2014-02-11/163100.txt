TITRE: Chypre: les n�gociateurs oeuvrent pour une solution rapide - LExpress.fr
DATE: 2014-02-11
URL: http://www.lexpress.fr/actualites/1/monde/chypre-reprise-des-pourparlers-de-paix-apres-deux-ans-d-interruption_1322528.html
PRINCIPAL: 163098
TEXT:
Chypre: les n�gociateurs oeuvrent pour une solution rapide
Par AFP, publi� le
11/02/2014 � 07:28
, mis � jour � 17:49
Nicosie - Les dirigeants chypriote-grec et chypriote-turc ont affirm� leur volont� de parvenir � un r�glement "aussi vite que possible" pour une r�unification de l'�le m�diterran�enne, � la reprise mardi des n�gociations favoris�e par la perspective d'une exploitation des r�serves gazi�res.
Les n�gociations entre Chypriotes-grecs et Chypriotes-turcs en vue d'une r�unification de l'�le ont repris mardi � Nicosie apr�s deux ans de suspension et avec pour catalyseur la future exploitation des r�serves �nerg�tiques au large de l'�le.
afp.com/Stavros Ioannides
Le pr�sident de la R�publique de Chypre Nicos Anastasiades (chypriote-grec) et le dirigeant de la R�publique turque de Chypre Nord (RTCN, autoproclam�e) Dervis Eroglu se sont rencontr�s pendant une heure et demie dans les locaux de l'ONU sur l'a�roport d�saffect� de Nicosie, en pr�sence de la responsable des Nations unies � Chypre, Lisa Buttenheim.�
"J'esp�re qu'aujourd'hui est le d�but de la fin d'une situation ind�sirable et inacceptable qui a maintenu notre �le et notre peuple divis�s pendant quarante ans", a dit M. Anastasiades au terme des discussions, les premi�res depuis deux ans.�
A Ankara, le Premier ministre turc Recep Tayyip Erdogan a estim� que l'on "se dirige vers un nouveau processus � Chypre et si Dieu le veut, il n'y aura pas de marche arri�re et le probl�me chypriote sera r�gl�".�
Chypre est coup�e en deux depuis l'invasion turque de 1974, en r�action � une tentative de coup d'Etat qui visait � la rattacher � la Gr�ce.�
Trente ans plus tard, le pays �tait entr� divis� dans l'Union europ�enne, apr�s l'�chec d'un plan de r�unification pilot� par l'ONU et soumis � referendum qui avait �t� largement approuv� par les Chypriotes-turcs mais rejet� par les Chypriotes-grecs.�
Des discussions poussives avaient repris en 2008, puis suspendues en 2012. Apr�s des mois d'�pres pourparlers, les deux dirigeants se sont mis d'accord vendredi sur une d�claration conjointe pr�par�e par l'ONU pour fixer le cadre des pourparlers.�
"Les dirigeants auront pour objectif de parvenir � une solution aussi vite que possible et d'organiser ensuite des referendums distincts", selon une d�claration commune lue devant la presse par Mme Buttenheim.�
Elle a pr�cis� que la prochaine rencontre, au niveau des n�gociateurs, �tait pr�vue "cette semaine".�
La d�claration commune r�sume le cadre des n�gociations sur une r�unification rendue plus n�cessaire que jamais par la perspective d'exploitation des richesses gazi�res de l'�le et une r�cession historique dans la R�publique de Chypre.�
Nouvelle donne avec le gaz�
Elle r�affirme qu'un r�glement, s'il est approuv� par referendum, consistera en "une f�d�ration bi-communautaire et bi-zonale", dans laquelle Chypre "sera une entit� l�gale unifi�e sur le plan international, avec une souverainet� unique".�
La Constitution pr�voira "que la f�d�ration unifi�e de Chypre soit compos�e de deux Etats membres de statut �gal" et interdira "une union partielle ou totale avec un quelconque autre pays".�
Confront� � une forte opposition aux n�gociations parmi les Chypriotes-grecs, M. Anastasiades a soulign� que l'accord sur la feuille de route "n'�tait que le d�but d'efforts laborieux pour parvenir aux objectifs souhait�s", ajoutant qu'il esp�rait un r�glement "sans gagnants ni perdants".�
L'UE s'est f�licit�e du fait que la d�claration commune "�tablit une base solide pour la reprise des n�gociations en vue d'un r�glement global juste et durable".�
Le communiqu� commun a �galement �t� salu� par Londres et Paris, alors que le secr�taire g�n�ral de l'ONU Ban Ki-moon a promis "un engagement r�solu" aux efforts en cours. �
Son repr�sentant � Chypre, Alexander Downer, a exhort� les deux parties � admettre qu'"il est dans leur int�r�t de r�gler leurs diff�rends, parvenir � un accord sur une vision commune de l'avenir entre les deux communaut�s et conclure les n�gociations pour r�aliser cette vision".�
Apr�s des ann�es de n�gociations infructueuses, la r�cente d�couverte de gisements gaziers au large de l'�le comme pr�s des c�tes isra�liennes a chang� la donne.�
Pour Hubert Faustmann, professeur d'histoire et de sciences politiques � l'Universit� de Nicosie, il s'agit de "la plus grande opportunit� pour la paix depuis 2004".�
"Washington a mis beaucoup de poids dans ce dernier effort de paix parce que le gaz et le p�trole changent le jeu (...). C'est une situation gagnant-gagnant pour tout le monde", estime-t-il.�
Isra�l, qui r�fl�chit � exporter son gaz via un gazoduc passant dans les eaux chypriotes et la Turquie, ou � investir dans une usine de liqu�faction de gaz sur l'�le, "ne donnera pas son gaz � Chypre � moins qu'il y ait une solution", note M. Faustmann.�
Par
