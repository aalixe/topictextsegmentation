TITRE: Mark�Zuckerberg�en�t�te�du�palmar�s�2013�des�50�donateurs�les�plus�g�n�reux
DATE: 2014-02-11
URL: http://french.peopledaily.com.cn/Economie/8533006.html
PRINCIPAL: 162721
TEXT:
Mark Zuckerberg en t�te du palmar�s 2013 des 50 donateurs les plus g�n�reux
(     le Quotidien du Peuple en ligne   )
11.02.2014 � 15h37
Le patron de Facebook, Mark Zuckerberg, a �t� �lu le plus important philanthrope aux Etats-Unis en 2013 par le journal The Chronicle of Philanthropy, pour un don de pr�s d'un milliard de dollars vers� � une association.
Mark Zuckerberg et sa femme Priscilla Chan figurent en t�te du classement annuel des 50 donateurs les plus g�n�reux aux USA en 2013 r�alis� par The Chronicle of Philanthropy.
Le couple a offert un don de plusieurs millions d'actions du r�seau social Facebook, valoris�es actuellement � hauteur de 992 millions de dollars, � une association californienne oeuvrant dans le milieu de la sant� et de l'�ducation.
Articles recommand�s:
