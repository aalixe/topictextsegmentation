TITRE: L'actrice Shirley Temple est morte
DATE: 2014-02-11
URL: http://www.ozap.com/actu/l-actrice-shirley-temple-est-morte/451621
PRINCIPAL: 163199
TEXT:
L'actrice Shirley Temple est morte
12H05 Le 11/02/14 News 3
publi� par Charles Decant
L'actrice Shirley Temple est morte
La BBC annonce ce matin la disparition de l'actrice Shirley Temple, � l'�ge de 85 ans. La premi�re enfant star d'Hollywood avait fait ses premiers pas au cin�ma � trois ans.
Shirley Temple est d�c�d�e
Tweet
Retrouvez aussi l'actu m�dias sur notre page Facebook
Enfant star dans les ann�es 30, Shirley Temple est d�c�d�e lundi � l'�ge de 85 ans, annonce la famille de l'actrice ce matin, selon la BBC. "Entour�e par sa famille et ceux qui s'occupaient d'elle", selon leurs dires, elle s'est �teinte dans sa maison de Woodside en Californie, de cause naturelle. "Nous saluons les choses incroyables qu'elle a accomplies dans sa vie en tant qu'actrice, diplomate et en tant que m�re, grand-m�re et arri�re-grand-m�re", a indiqu� la famille.
C'est en 1932, � l'�ge de 3 ans que Shirley Temple fait ses premiers pas au cin�ma et deux ans plus tard, apr�s avoir d�j� multipli� les projets, elle rencontre un succ�s cons�quent dans le film "Shirley aviatrice", concoct� pour mettre en avant ses talents. L'ann�e suivante, elle obtient m�me un Oscar de la jeunesse pour son travail tout au long de l'ann�e 1934. Elle encha�ne ensuite les longs m�trages tout au long des ann�es 30, puis sa notori�t� faiblit au fil des ann�es, la poussant � prendre sa retraite � l'�ge de 22 ans !
Elle attend 1958, et donc ses 30 ans, pour reprendre le chemin des studios pour la t�l�vision, cette fois, et pr�sente une anthologie baptis�e "Shirley Temple's Storybook", qui conna�t deux saisons et au cours de laquelle l'actrice narre de nombreux contes de f�es, utilisant ses talents d'actrice dans trois des 16 �pisodes de la premi�re saison. La s�rie conna�t une deuxi�me saison mais est annul�e par la cha�ne NBC en 1961. Suite � la fin de cette aventure, la com�dienne multiplie les apparitions dans des �missions et tourne un pilote de com�die, qui n'arrivera jamais � l'antenne.
Mais Shirley Temple s'est �galement illustr�e en tant que diplomate. Apr�s avoir tent� d'int�grer le Congr�s am�ricain en 1967, sans succ�s, elle est d�sign�e ambassadrice des Etats-Unis au Ghana en 1974, puis en Tch�coslovaquie en 1989.
Toutes les news : actus TV
Qu'en pensez vous ?
