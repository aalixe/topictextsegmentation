TITRE: Les Lapins Cr�tins au cin�ma
DATE: 2014-02-11
URL: http://www.gamekult.com/actu/les-lapins-cretins-au-cinema-A129095.html
PRINCIPAL: 163777
TEXT:
Les Lapins Cr�tins au cin�ma
Les Lapins Cr�tins au cin�ma
A propos de..., par TRUNKS -
Mardi 11/02/2014 � 14h 25
Tags : R�flexion
L'invasion Lapins Cr�tins continue. Apr�s les jeux vid�o, les multiples jouets, la s�ri� TV, ou encore la r�cente attraction ouverte au Futuroscope, les bestioles stupides d'Ubisoft auront bient�t droit � une adaptation au cin�ma.
Le film se fera chez Sony Pictures et devrait �videmment mettre en sc�ne les Lapins dans des situations loufoques et improbables. La date de sortie du long m�trage dans les salles n'a pas encore �t� annonc�e.
TRUNKS (Fr�d�ric Luu), Journaliste
