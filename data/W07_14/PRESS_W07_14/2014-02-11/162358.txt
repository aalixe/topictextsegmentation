TITRE: Youssouf Fofana, ex-chef du "gang des barbares", a agress� un surveillant de prison
DATE: 2014-02-11
URL: http://www.francetvinfo.fr/faits-divers/youssouf-fofana-ex-chef-du-gang-des-barbares-a-agresse-un-surveillant-de-prison_526885.html
PRINCIPAL: 162355
TEXT:
Tweeter
Youssouf Fofana, ex-chef du "gang des barbares", a agress� un surveillant de prison
Il a bless� un membre du personnel "� la main avec une brosse � dents aiguis�e", �crit "Le Parisien", lundi.
Sur une vid�o post�e sur YouTube le 26 novembre 2011, Youssouf�Fofana fait l'apologie d'Al-Qa�da. (YOUTUBE / FRANCETV INFO)
, publi� le
10/02/2014 | 23:26
Youssouf Fofana, ancien chef�du "Gang des barbares", a agress� un surveillant de sa prison de�Cond�-sur-Sarthe, affirme Le Parisien , lundi 10 f�vrier. Condamn� en 2009�� perp�tuit� avec une peine de s�ret� de 22 ans pour le meurtre d'Ilan�Halimi, il a bless� un membre du personnel "� la main avec une brosse � dents aiguis�e", pr�cise le journal.
L'incident est survenu lorsque Youssouf Fofana �tait � l'ext�rieur de sa cellule�pour t�l�phoner � son amie incarc�r�e a Rennes. Jusqu'ici, il avait l'autorisation de l'appeler une fois par semaine.
En 2013, en plus de sa peine de 2009,�Youssouf�Fofana a �cop� de sept ans de prison pour�apologie du terrorisme . Il avait post�sur internet, en 2011, des vid�os tourn�es ill�galement dans sa cellule�au centre p�nitentiaire de�Clairvaux�(Aube). Lunettes noires, foulard sur la t�te, il appelait � "la r�volte des Africains".��Il s'y revendiquait notamment comme le "symbolique troph�e de guerre des sionistes de�New�York".��Il y faisait �galement "l'apologie d'Al-Qa�da�et des combattants de la cause d'Allah'".
