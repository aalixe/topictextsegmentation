TITRE: Municipales � Nice: Abdellatif Kechiche soutient Christian Estrosi - L'Express
DATE: 2014-02-11
URL: http://www.lexpress.fr/actualite/politique/municipales-a-nice-abdellatif-kechiche-soutient-christian-estrosi_1322575.html
PRINCIPAL: 0
TEXT:
Abdellatif Kechiche fait partie du comit� de soutien de Christian Estrosi.
Regis Duvignau/REUTERS
Abdellatif Kechiche , r�alisateur laur�at de la Palme d'or pour La Vie d'Ad�le, fait partie du comit� de soutien de Christian Estrosi aux municipales de Nice, maire UMP qui a vot� contre le mariage homosexuel.�
Le cin�aste s'est expliqu� de ce choix dans les colonnes de Nice Matin . Celui qui a grandi dans la cit� des Moulins � Nice estime d'abord que l'actuel d�put�-maire est le seul "capable de faire battre le Front National � Nice".�
De plus, Kechiche estime que Christian Estrosi a toujours bien g�r� sa ville et a beaucoup oeuvrer pour l'am�lioration de "la vie dans les quartiers d�favoris�s", sachant que pour une mairie, "l'essentiel c'est la gestion de la ville".�
Bernadette Chirac , Serge Klarsfeld , l'�crivain Marek Halter ou encore les champions de natation Yannick Agnel , Camille Muffat et les footballeurs ni�ois Didier Digard et Eric Bauth�ac font �galement partie de ce comit� de soutien.�
Suivez L'Express
