TITRE: Google Glass : le Cr�dit Mutuel-Ark�a prend date
DATE: 2014-02-11
URL: http://www.cbanque.com/actu/43480/google-glass-le-credit-mutuel-arkea-prend-date
PRINCIPAL: 0
TEXT:
Cr�dit Mutuel (pr�sentation de la banque)
� martinmatthews - Fotolia.com
Dans un communiqu�, Ark�a annonce le lancement d?une application bancaire adapt�e � Google Glass, les lunettes connect�es d�velopp�es par le g�ant de l?internet. Un bon coup de pub pour la banque brestoise.
Du minitel� � Google Glass. Dans son communiqu� diffus� ce mardi, Ark�a, une des deux principales conf�d�rations du Cr�dit Mutuel (1), rappelle sa ��fibre novatrice�� au moment d�annoncer sa derni�re nouveaut� en date�: une application bancaire, d�velopp�e en collaboration avec l��diteur de logiciel AMA Studios et destin�e aux lunettes high tech d�velopp�es par le moteur de recherche californien.
Depuis 2012, Google teste en effet un nouveau type d�appareil mobile, prenant la forme d�une paire de lunettes qui embarque notamment une cam�ra, un micro et des mini-�crans, le tout connect� en permanence � internet. Le projet est entr� dans une phase active depuis d�but 2013, date de la commercialisation (au prix de 1.499 $, tout de m�me) d�une premi�re s�rie de Google Glass, destin�e � quelques ��geeks�� particuli�rement motiv�s. L�objet pourrait toutefois �tre disponible pour le grand public d�s cette ann�e, m�me si la date de lancement n�a pas encore �t� communiqu�e par Google. En attendant, les Google Glass sont encore tr�s rares en France�: quelques dizaines, tout au plus. Leurs possesseurs sont invit�s � se rapprocher d�Ark�a (2) s�ils veulent tester l�application.
Des fonctionnalit�s classiques
Pourquoi mobiliser des ressources pour d�velopper une application destin�e � aussi peu d�usagers�? ��Issues de nos �laboratoires d�id�es� internes, ces initiatives t�moignent de notre volont� de d�velopper des services novateurs pour tous nos clients��, justifie Ronan Le Moal, le directeur g�n�ral d�Ark�a. Pour autant, les fonctionnalit�s de l�application, d�taill�es dans le communiqu�, apparaissent comme assez classiques�: consultation de comptes, g�olocalisation des distributeurs ou des agences les plus proches ou encore prise de contact avec des conseillers.
S�agit-il alors plut�t d�un simple coup de pub�? Tr�s attendues, les Google Glass (et leurs concurrents � venir) sont, pour certains, la nouvelle fronti�re de l��lectronique grand public. Il est donc de bon aloi d�afficher d�embl�e son int�r�t pour cette nouvelle technologie. Ark�a n�est d�ailleurs pas la seule � s�y int�resser�: outre Google, qui a adapt� ses principaux services (Now, Maps, Gmail, Google+, etc.) au nouvel outil, des g�ants d�internet, comme Facebook, Twitter ou encore Evernote, proposent d�j� des versions de leurs applications adapt�es � Glass.
(1) Bas�e � Brest, Ark�a regroupe les Cr�dits Mutuels de Bretagne, du Sud-Ouest et du Massif Central, soit 3,2�millions de clients et 9.000 salari�s.
(2) En �crivant � innovation@arkea.com
