TITRE: Les abonn�s Free Mobile peuvent d�sormais appeler sans surco�t depuis les Pays-Bas !
DATE: 2014-02-11
URL: http://www.boursier.com/actualites/economie/les-abonnes-free-mobile-peuvent-desormais-appeler-sans-surcout-depuis-les-pays-bas-22977.html
PRINCIPAL: 163751
TEXT:
Cr�dit photo ��Reuters
(Boursier.com) � Alors que SFR a annonc� hier le lancement d'un forfait "Europe" en s�rie limit�e , Free Mobile inclut quasiment chaque semaine une nouvelle destination � son offre d'itin�rance !
Cette fois, il s'agit des Pays-Bas, apr�s l'Allemagne, l'Italie, le Portugal, la Guyane et les Antilles Fran�aises. Les abonn�s de la filiale d'Iliad aux forfaits � 19,99 euros ou 15,99 euros par mois ayant au moins 60 jours d'anciennet� pourront appeler depuis ce pays, 35 jours par ann�e civile, sans surco�t (appels, SMS, MMS illimit�s, Internet 3G jusqu'� 3Go)...
D�marche europ�enne
Free Mobile se targue d'avoir �t� "le premier op�rateur mobile de r�seau, d�s avril 2013, � rendre accessible le roaming (...) � un forfait � moins de 20 euros par mois, sans engagement". Orange et Bouygues Telecom ont �galement ripost� aux offres du quatri�me op�rateur. Ces derniers prennent ainsi de l'avance sur Bruxelles. Afin d'unifier le march� mobile, la Commission europ�enne souhaite en effet mettre fin aux frais d'itin�rance d�s le mois de juillet 2014. La mesure doit cependant encore �tre vot�e par le Parlement et les 28 Etats membres...
Marianne Davril � �2014, Boursier.com
