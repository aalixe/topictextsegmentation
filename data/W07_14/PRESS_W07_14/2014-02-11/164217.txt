TITRE: Thorgan Hazard se dirige vers la Premier League - 7SUR7.be
DATE: 2014-02-11
URL: http://www.7sur7.be/7s7/fr/2756/Zulte-Waregem/article/detail/1791446/2014/02/11/Thorgan-Hazard-se-dirige-vers-la-Premier-League.dhtml
PRINCIPAL: 164215
TEXT:
11/02/14 -  10h42��Source: Het Laatste Nieuws
� belga.
Thorgan Hazard devrait quitter la Jupiler Pro League � la fin de la saison. Direction la Premier League? Sans doute, mais pas � Chelsea.
Chelsea r�fl�chit � la possibilit� de pr�ter Thorgan Hazard � un club de Premier League, d�s la saison prochaine. Le club anglais, propri�taire du joueur, est satisfait de l'�volution du Soulier d'Or � Zulte-Waregem, surtout parce qu'il arrive � �tre constant dans ses prestations. Mais pour le moment, des doutes subsistent quant � sa capacit� de pouvoir faire son trou en �quipe A chez les Blues. Pas illogique.
Jos� Mourinho avait d�j� indiqu� que "Little Hazard" pourrait faire partie de son noyau d�s le mois de juillet. Chelsea n'a toutefois pas encore pris de d�cision � ce sujet. On se dirige vers un pr�t dans une autre formation anglaise, histoire de permettre � Thorgan de se familiariser avec le jeu anglais et d'effectuer une �tape interm�diaire entre Zulte et Chelsea. Hazard, lui, sait tr�s bien qu'il serait compliqu� de s'imposer � Stamford Bridge et il privil�gie logiquement cette option car il veut jouer.
Parmi les clubs cit�s, on �voque Middlesbrough (D2), qui avait manifest� son int�r�t l'�t� dernier. Stoke City serait aussi sur le coup.
Lire aussi
