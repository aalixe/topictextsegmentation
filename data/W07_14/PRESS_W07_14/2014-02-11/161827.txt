TITRE: Petro signe � Limoges  - Pro A - Basket -
DATE: 2014-02-11
URL: http://sport24.lefigaro.fr/basket/pro-a/actualites/petro-signe-a-limoges-678323
PRINCIPAL: 161821
TEXT:
Tweeter
Johan Petro rentre en France, � Limoges -  Panoramic
Exil� en NBA depuis 2005, avec un bref passage par la Chine en d�but de saison, Johan Petro revient en France. Le pivot champion d�Europe avec les Bleus l��t� dernier annonce sa signature � Limoges.
Un champion d�Europe de plus en Pro A. Si le championnat de France a perdu l�un des h�ros de Ljubljana il y a quelques semaines en la personne d�Alexis Ajin�a, reparti en NBA aux Pelicans de La Nouvelle Orl�ans, il en r�cup�re un autre ce lundi�: Johan Petro. Le natif de Paris annonce en effet lui-m�me sa signature au CSP Limoges sur Twitter. Rappelons que Petro avait choisi de rejoindre la Chine apr�s l�EuroBasket. En l�occurrence, il avait sign� aux Zhejiang Guangsha Lions. Une exp�rience dict�e, entre autres, par son d�sir de garder une porte ouverte afin de terminer la saison en NBA mais qui avait tourn� court. Il a finalement �t� lib�r� r�cemment avec la formation de CBA apr�s plusieurs semaines de flou artistique.
Retour en #ProA sous les couleurs du @CSPLimoges ! Un pur bonheur !
� Johan Petro (@Johan_Petro) 10 F�vrier 2014
473 matches en NBA
Johan Petro, 28 ans, c�est 473 matches de saison r�guli�re en NBA entre 2005 et 2013, avec 139 titularisations et 4,7 points, 3,9 rebonds de moyenne, en 15,4 minutes. Il a �galement disput� 22 matches de play-offs en trois campagnes (2,3 pts, 1,8 rbs/match). Draft� par Seattle � la 25e position du premier tour, il a jou� pour les Sonics, puis le Thunder apr�s le d�m�nagement de la franchise � Oklahoma City (2005-2008). Le tout avant d�atterrir � Denver (2009-2010), New Jersey (2010-2012) et Atlanta (2012-2013).
LIMOGES! LIMOGES! LIMOGES.!!!!!!
� Johan Petro (@Johan_Petro) 10 F�vrier 2014
La Leaders Cup pour commencer�?
Petro avait profit� des forfaits de Joakim Noah, Kevin S�raphin et autre Ian Mahinmi pour s�inviter chez les Bleus l��t� dernier, avec d�honn�tes performances en sortie de banc, derri�re Alexis Ajin�a. Il retrouve donc la Pro A neuf ans apr�s avoir quitt� Pau-Orthez, le grand rival du CSP. Son nom avait �t� �voqu� � Strasbourg ces derni�res semaines, mais la Sig a finalement jet� son d�volu sur l�international australien David Andersen. Charge � Johan Petro de permettre � Limoges, actuel co-leader du championnat, de continuer � r�ver au titre. Il pourra probablement se faire les dents d�s le week-end prochain � Marne-la-Vall�e, dans le cadre de la Leaders Cup. L��quipe du pr�sident Fr�d�ric Forte y affrontera Le Mans en quarts de finale, puis Strasbourg ou Nancy lors des demies en cas de qualification.
Club Incontournable est mythique de la pro A peut pas esp�rer mieux comme retour.!! pic.twitter.com/kmnNRrhUIM
� Johan Petro (@Johan_Petro) 10 F�vrier 2014
Williams � Disneyland Paris quand m�me ?
D�apr�s les informations du Populaire du Centre, l�arriv�e de Johan Petro ne signifie pas qu�Eric Williams est d�j� parti. En l�occurrence, le quotidien local croit savoir que �le CSP, qui a donn� carte blanche � (Fr�jus)�Zerbo, parti en C�te d'Ivoire tout en esp�rant son retour jeudi, pourrait encore avoir besoin de lui pour la Leaders Cup�. Eric Williams est au club depuis le 7 novembre dernier. Il tourne � 3,1 points, 2,5 rebonds et 2,3 d��valuation en 10,6 minutes.
A lire aussi
