TITRE: Cinq activit�s surprenantes pour la Saint-Valentin
DATE: 2014-02-11
URL: http://fr.canoe.ca/hommes/seduction/archives/2014/02/20140210-105542.html
PRINCIPAL: 162991
TEXT:
Cinq activit�s surprenantes pour la Saint-Valentin
Cuisiner en couple est l'occasion id�ale de passer du temps ensemble. �Photo Fotolia
Derni�res nouvelles
Sarah-�milie Nault
10-02-2014 | 10h55
Si vous n'avez pas encore choisi quoi faire � la Saint-Valentin, le temps presse! Afin d'�viter de n'avoir rien pr�vu et de la d�cevoir, voici cinq id�es d'activit�s qui la surprendront.
Improviser une escapade amoureuse
La Saint-Valentin est un vendredi cette ann�e: pourquoi ne pas prendre cette journ�e de cong� et faire de la f�te une longue fin de semaine d'amoureux � l'ext�rieur de la ville (ou dans une autre ville)? Mettez le cap sur la campagne, dans un chalet jusqu'au dimanche, ou sur une ville que vous aviez envie de d�couvrir depuis longtemps (am�ricaine, canadienne ou qu�b�coise). Un petit voyage impromptu, m�me pas tr�s loin de chez soi, fait toujours du bien � l'�me, au corps et au couple.
Cuisiner
C'est si agr�able de cuisiner, pourquoi s'en priver � la Saint-Valentin? Le 14 f�vrier, trois options s'offrent � vous: cuisiner un d�licieux repas surprise, afin d'impressionner et de g�ter votre douce, lui offrir un cours de cuisine en couple � Atelier & Saveurs ou vous faire plaisir mutuellement en cuisinant � deux, � la maison, sur fond de bonne musique, verre de vin � la main. Des moments de bonheur simples et assur�ment d�licieux.
Se faire dorloter � deux
Aller au spa, c'est bien, mais aller au spa en campagne et en amoureux, c'est encore mieux! Pour c�l�brer, profitez de l'une des deux offres sp�ciales Saint-Valentin du Balnea . En plus de l'acc�s aux bains, le forfait Rom�o et Juliette comprend un repas d'amoureux en formule table d'h�te, deux verres de bulles et une bo�te de chocolats. Besoin d'un peu plus de folies? �Surclassez� ce forfait en troquant les bulles pour une bouteille de Veuve Clicquot et en y ajoutant un soin gourmand (massage � l'huile chocolat�e ou wrapsody int�gral corps et cheveux au beurre de karit� et exfoliation). Afin vous faire dorloter � deux, partagez les 6 mains expertes du forfait Loverdose comprenant aussi un rituel hindou, un soin capillaire indien et un mini-soin du visage.
Danser lors de la soir�e Frenche ou meurs
Les soir�es Frenche ou Meurs ont bien choisit leur date de grand retour: le 14 f�vrier! Participez-y pour sa jolie ambiance comparable � celle des �p'tites danses de sous-sol d'�glise�, l'enthousiasme de l'adolescence retrouv� le temps d'une soir�e, les bisous et la certitude de terminer la soir�e en dansant un bon vieux slow coll� avec sa douce (ou pour en rencontrer une, si vous �tes c�libataire). (Le vendredi 14 f�vrier au Belmont , 4483 boulevard Saint-Laurent).
Escalader des parois glac�es
Cette activit� extr�me est tout sauf commune pour f�ter la Saint-Valentin et c'est tout � fait pour cette raison que cela surprendra et amusera votre douce moiti�. Le cours d'initiation , fait sur-mesure pour les d�butants, apportera un peu d'adr�naline � votre journ�e, en plus de vous permettre de pratiquer et de partager une activit� nouvelle, originale et remplie de d�fis en cette f�te des amoureux.
Aussi sur Canoe.ca:
