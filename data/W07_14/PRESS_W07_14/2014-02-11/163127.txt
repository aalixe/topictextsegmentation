TITRE: Levallois-Perret: les Balkany refusent d'appliquer la r�forme des rythmes scolaires - L'Express
DATE: 2014-02-11
URL: http://www.lexpress.fr/actualite/politique/levallois-perret-les-balkany-refusent-d-appliquer-la-reforme-des-rythmes-scolaires_1322549.html
PRINCIPAL: 163126
TEXT:
Les Balkany refusent d'appliquer la r�forme des rythmes scolaires � Levallois-Perret (92).
REUTERS/Charles Platiau
Il y avait d�j� eu le d�put�-maire de Nice Christian Estrosi fin novembre. C'est maintenant au tour d' Isabelle Balkany de refuser d'appliquer la r�forme des rythmes scolaires.�
Si Christian Estrosi avait choisi de s'exprimer sur BFMTV, l'adjointe au maire de Levallois-Perret (92) a quant � elle profit� de son conseil municipal. Lundi soir, Isabelle Balkany, premier adjoint d�l�gu� � la Vie scolaire, � la Jeunesse et � la Communication, a pr�sent� une d�lib�ration fixant les horaires de la prochaine rentr�e scolaire. Le tout sans �voquer des cours le mercredi matin, obligatoires avec l'entr�e en vigueur de la semaine de quatre jours et demi, devant �tre g�n�ralis�e en 2014. En guerre depuis des mois contre la r�forme de Vincent Peillon , Isabelle Balkany entend bien organiser son temps scolaire comme elle le souhaite.�
Des d�bats houleux
Et le moins que l'on puisse dire, c'est que ce dernier conseil municipal de Patrick Balkany avant les �lections �tait attendu par les Levalloisiens, venus en nombre.
Les #Levalloisiens sont venus nombreux pr assister au denier Conseil Municipal de @Patrick_Balkany � #Levallois pic.twitter.com/xhScNvucD9
-- Thomas Lsg (@Thomas_Lsg19) 10 F�vrier 2014
�
Les d�bats ont �t� houleux, voire th��traux � Levallois-Perret, o� la d�mesure est souvent de mise .
D�bat houleux au conseil municipal de Levallois : les Balkany font voter le maintien de la semaine de 4 jours � l'�cole #rythmesscolaires
-- Ilan (@i_car) 10 F�vrier 2014
�
Quel spectacle que ce conseil municipal de Levallois, o� le public applaudit ou hue les interventions toutes les 5 min... Grandiose.
-- Ilan (@i_car) 10 F�vrier 2014
�
Les Balkany n'en finissent plus de faire parler d'eux, sous le coup de deux informations judiciaires pour blanchiment de fraude fiscale et d�tournement de fonds public. Patrick Balkany avait alors craqu� et confisqu� une cam�ra de BFMTV fin janvier.�
