TITRE: Vincent Cassel ("La Belle et la B�te") : "Enfant, je ne r�vais pas d'�tre prince" � metronews
DATE: 2014-02-11
URL: http://www.metronews.fr/culture/vincent-cassel-la-belle-et-la-bete-enfant-je-ne-revais-pas-d-etre-prince/mnbk!UCLXz6CxDqXEE/
PRINCIPAL: 164973
TEXT:
Cr�� : 12-02-2014 11:50
Vincent Cassel : "Enfant, je ne r�vais pas d'�tre prince"
INTERVIEW � Le prince maudit de Christophe Gans, c'est lui. Vincent Cassel est le h�ros de "La Belle et la B�te", en salles ce mercredi. Un r�le taill� sur mesure pour un com�dien fier de d�fendre les couleurs de cette superproduction made in France.
Tweet
�
Vincent Cassel, mardi dans les locaux de Metronews. Un prince � la cool. Photo :�BENJAMIN GIRETTE POUR METRONEWS
Le r�alisateur Christophe Gans estime que personne en France n'aurait pu jouer la B�te � part vous. Est-ce que vous �tes d'accord ?
C'est tr�s gentil de sa part mais il exag�re quand m�me un petit peu. Les acteurs sont toujours rempla�ables, quoi qu'on dise. Quand il m'a propos� le r�le de la B�te, j'ai trouv� que c'�tait une bonne id�e et qu'il y avait une certaine logique dans sa d�marche. Apr�s, c'est facile de dire que personne ne pouvait le jouer � part moi quand les choses sont faites (rires). Le projet aurait �galement abouti sans moi.
Il dit aussi que le personnage vous ressemble. A la fois sombre, dr�le, flamboyant... Y a-t-il beaucoup de vous dans les r�les que vous tenez ?
Un acteur laisse toujours transpara�tre un peu de sa personnalit� dans un r�le. Surtout s'il fait bien son travail. Cela ne veut pas dire non plus que je suis un braqueur de banque ou un violeur (rires). Disons que je dois avoir des couleurs qui correspondaient bien � ce personnage.
Avez-vous h�sit� � rejoindre ce projet ? Il y a tout de m�me eu de nombreuses versions de La Belle et la B�te...
Brandir les r�f�rences, c'est un peu bidon en fait (rires). Le film de Cocteau avec Jean Marais est bien �videmment un chef-d��uvre, avec des images qui restent. Mais il est aussi d�suet et indigeste. J'ai essay� de le faire voir � mes enfants et ils ont tr�s vite d�croch�. En raison du rythme, tr�s lent, et d'une th��tralit� un peu trop prononc�e. Aujourd'hui, notre �il est habitu� � autre chose. Pour l'appr�cier, il faut �tre un cin�phile ou avoir v�cu � cette �poque.
"Je suis heureux de faire partie d'une �uvre � l'ambition mondiale"
R�viez-vous d'�tre prince quand vous �tiez enfant ?
Non, pas du tout. Le prince est souvent chiant. Ce n'est pas le plus rigolo. Il est souvent montr� comme un �tre parfait, pond�r�. Le mari chiant, quoi. Alors que dans notre film, il est question de la r�demption d'un homme qui a tout perdu par avidit�... Et �a n'a rien � voir.
Quel regard portez-vous sur L�a Seydoux, la Belle � l'�cran ?
Je la connaissais uniquement en tant qu'actrice. J'�tais spectateur. Quand son nom est arriv� sur le projet, je l'ai trouv� id�ale. C'est une femme mais elle a encore ce c�t� presque poupon, ce visage tout frais, tout rond. Comme Belle qui est finalement une jeune femme qui devient une femme. Et puis c'est tout de m�me l'une des seules actrices que nous ayons qui passe de Christophe Honor� � Mission impossible en passant par Ridley Scott et Abdellatif Kechiche.
Quand on tourne en motion capture, a-t-on l'impression que l'interpr�tation nous �chappe ?
Plus que dans n'importe quel film, ici, quand le tournage s'arr�te, les choses ne sont plus entre nos mains. Ce que la B�te devient � l'�cran ne d�pend pas de moi. J'ai fonctionn� � la confiance avec Christophe Gans. Il avait une v�ritable vision. On a travaill� avec une superbe �quipe sur les effets sp�ciaux. Et puis, vous savez, c'est un tr�s bon exercice pour l'�go que d'incarner la B�te. Quand je vois le r�sultat, je suis heureux de faire partie d'une �uvre � l'ambition mondiale et j'esp�re qu'elle passera la barre pour que d'autres suivent derri�re. Si La Belle et la B�te se viande, on va repartir sur des films intimistes pour dix ans...
"Il faut continuer � inspirer le d�sir en restant inatteignable"
Croyez-vous que certains aimeraient vous voir �chouer ?
Dans ce pays, on aime que les gens se plantent. Pour se rassurer, je pense. Mais on est aussi tr�s patriotes. On r�le et on est fiers d'�tre des r�leurs. Le coq est l'embl�me de la France car c'est le seul animal qui continue � chanter lorsqu'il a les deux pieds dans la merde. Si ce film marche, ce sera une bonne occasion de chanter (sourire).
Depuis votre C�sar pour Mesrine, les choses ont-elles chang� dans votre vie et votre carri�re ?
Non, �a m'a fait plaisir sur le moment d'�tre reconnu dans mon pays. C'�tait un beau moment mais �a n'a rien chang� car je ne voulais pas que �a change. Quand on est rassur�, on n'est jamais au meilleur. Le moment o� on vous dit bravo c'est bien, mais pas b�n�fique. Les r�compenses n'augmentent pas les propositions. C'est votre capacit� � faire r�ver un metteur en sc�ne qui le fait. Il faut continuer � inspirer le d�sir en restant inatteignable. Finalement, un acteur est un dragueur. Et on agace les gens quand on est l� tout le temps. Les gens veulent votre mort quand on vous voit trop, genre : "Encore ce connard !" (rires).
Propos recueillis par J�r�me Vermelin et Mehdi Oma�s
