TITRE: Un an ferme pour l'ancien patron de l'UIMM - 11/02/2014 - leParisien.fr
DATE: 2014-02-11
URL: http://www.leparisien.fr/espace-premium/actu/un-an-ferme-pour-l-ancien-patron-de-l-uimm-11-02-2014-3578811.php
PRINCIPAL: 162253
TEXT:
Faits divers
Un an ferme pour l'ancien patron de l'UIMM
Reconnu coupable d'abus de confiance et de travail dissimul�, Denis Gautier-Sauvagnac devra �galement payer 375 000 � d'amende.
Publi� le 11 f�vr. 2014, 07h00
Paris, le 14 octobre 2013. Denis Gautier-Sauvagnac avait expliqu� � la barre que la caisse noire de la puissante f�d�ration patronale de la m�tallurgie servait � une meilleure r�gulation de la vie sociale. (IP3 Press/Maxppp/Christophe Morin.)
Mon activit� Vos amis peuvent maintenant voir cette activit�
Tweeter
Le tribunal correctionnel de Paris a condamn� hier l'ex-patron Denis Gautier-Sauvagnac � trois ans de prison, dont deux avec sursis, dans l'affaire de la caisse noire de l'Union des industries et m�tiers de la m�tallurgie (UIMM), la puissante f�d�ration patronale de la m�tallurgie.
Les juges ont aussi condamn� Dominique de Calan, d�l�gu� g�n�ral de l'UIMM, � un an avec sursis et 150 000 � d'amende, la chef comptable Dominique Renaud � huit mois avec sursis et l'ex-directeur administratif Bernard Adam � deux mois avec sursis. L'UIMM en tant que personne morale a �t� condamn�e � 150 000 � d'amende. Enfin, un ex-cadre, Jacques Gagliardi, qui avait b�n�fici� de 100 000 F (15 000 �) par an pendant dix ans apr�s son d�part � la retraite, a �cop� de six mois avec sursis. Les quatre autres pr�venus ont �t� relax�s.
Dans ses attendus, le tribunal a notamment rejet� les arguments de Denis Gautier-Sauvagnac qui avait expliqu� � la barre que cette caisse noire de l'UIMM servait � une meilleure r�gulation de la vie sociale et avait d�sign� les syndicats comme les vrais b�n�ficiaires de ces enveloppes.
M e Jean-Yves Le Borgne, son avocat, a d�nonc� � une peine d�mesur�e, un jugement d'une s�v�rit� sans pareille � et annonc� son intention de faire appel. L'UIMM a elle aussi d�cid� de faire appel.
VIDEO. Proc�s de l'UIMM : l'ex-patron Denis Gautier-Sauvagnac condamn� � un an ferme
