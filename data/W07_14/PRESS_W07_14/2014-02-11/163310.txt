TITRE: Lyon, 4e ville la plus accessible de France / Ville de lyon / Politique / Lyon / Journal / Lyon Capitale - le journal de l'actualit� de Lyon et du Grand Lyon.
DATE: 2014-02-11
URL: http://www.lyoncapitale.fr/Journal/Lyon/Politique/Ville-de-lyon/Lyon-4e-ville-la-plus-accessible-de-France
PRINCIPAL: 163278
TEXT:
Lyon, 4e ville la plus accessible de France
Par          Fabien Fournier
Publi� le�11/02/2014� �12:27
R�agissez
L'Association des paralys�s de France (APF) publie son 5e barom�tre de l�accessibilit� des villes de France. En t�te, toujours Grenoble qui se voit d�cern�e la note de 18,7/20, suivie de Nantes et de Caen. Lyon est 4e, avec une note de 17,5/20 contre 16,5/20 en 2012. A �t� mesur�e l'accessibilit� dans les centres m�dicaux, les lignes de bus, les�piscines, les mairies, les cin�mas, les centres commerciaux, les stades, les stationnements...�
Au niveau national, l'enqu�te d�plore qu'�peine plus de la moiti� des �coles et 42% des r�seaux de bus sont accessibles aux personnes en situation de handicap.�
� lire �galement
