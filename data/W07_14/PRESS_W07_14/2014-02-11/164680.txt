TITRE: M�lenchon: Gattaz &quot;a tir� dans le dos&quot; des Fran�ais | La-Croix.com
DATE: 2014-02-11
URL: http://www.la-croix.com/Actualite/France/Melenchon-Gattaz-a-tire-dans-le-dos-des-Francais-2014-02-11-1104960
PRINCIPAL: 164678
TEXT:
Le Conseil constitutionnel censure la loi Florange
Pierre Gattaz "nous a tir� dans le dos, � nous tous les Fran�ais", a estim� mardi Jean-Luc M�lenchon, apr�s les propos critiques du patron du Medef sur le pacte de responsabilit�, lors de la visite d'Etat de Fran�ois Hollande aux Etats-Unis.
"Son comportement dans de telles circonstances est inacceptable. On ne va pas � l'�tranger profiter d'un voyage pr�sidentiel pour critiquer un pr�sident de la R�publique quel qu'il soit", a encore d�clar� le co-pr�sident du Parti de gauche (PG) � des journalistes � Bergerac (Dordogne).
"On �vite de se battre entre Fran�ais � l'�tranger, surtout sur des questions qui concernent l'�conomie devant les Am�ricains. Il s'est comport� comme quelqu'un qui nous a tir� dans le dos, � nous tous les Fran�ais", a-t-il ajout� en marge d'un d�placement � Bergerac.
Jean-Luc M�lenchon a en outre estim� que la participation de M. Gattaz au voyage est tout � fait inhabituelle et "lui donne une importance qu'il n'a pas" car il n'est que le "petit repr�sentant d'une association patronale sans r�elle repr�sentativit�".
Pierre Gattaz a d�clar� mardi depuis Washington, o� il accompagne le chef de l'Etat, ne pas vouloir entendre parler de "contreparties" dans le cadre du pacte de responsabilit�, en demandant qu'on arr�te en France "de g�rer par la contrainte".
Estimant que le discours exigeant des "contreparties �tait "insupportable", il aussi ajout� ne pas comprendre "le mot contreparties": "Il faut d�finir un projet commun sur la cr�ation d'un million d'emploi en 5 ans -ce qui est le projet du Medef- (...) Nous avons des engagements r�ciproques (...) L'objectif est de cr�er de l'emploi, de la croissance et un terreau attractif qui restaure de la confiance", a-t-il ajout�.
AFP
