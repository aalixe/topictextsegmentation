TITRE: La rumeur d�un Nokia sous Android se confirme | Viuz
DATE: 2014-02-11
URL: http://www.viuz.com/2014/02/11/la-rumeur-dun-nokia-sous-android-se-confirme/
PRINCIPAL: 162398
TEXT:
La rumeur d�un Nokia sous Android se confirme
11 f�vrier 2014
A quelques semaines de son int�gration compl�te chez Microsoft, Nokia s�appr�terait � commercialiser un smartphone sur Android qui devrait �tre d�voil� lors du Mobile World Congress � Barcelone du 24 au 27 f�vrier 2014.
Selon le Wall Street Journal , ce t�l�phone issu d�une version modifi�e d�Android pourrait �tre destin� aux segments low cost des lignes de mobiles Nokia.
Destin� � �tre distribu� dans les pays �mergents, il offrirait en priorit� des services Nokia et Microsoft (comme Nokia Here et MixRadio) sans acc�s aux services Google ou � Google Play, l�appstore d�Android.
Mi Janvier, Vizileak avait donn� corps � la rumeur en d�voilant les premi�res photos du Nokia sur Android
Twitter rach�te Cover, un App Launcher mobile contextuel
8 avril 2014
Twitter vient d�annoncer le rachat de Cover, un �cran d�accueil mobile personnalisable permettant de lancer ses Apps en contexte et�
Les Apps : 86% du temps mobile
2 avril 2014
Six ans apr�s leur apparition les Applications constituent 86% de l�usage mobile soit 2H19 par jours sur 2h42 d�usage mobile�.
Le march� des services mobiles g�olocalis�s estim� � 5 milliards d�Euros en 2018.
1 avril 2014
D�apr�s une �tude r�alis�e par Berg Insight le march� des services mobiles g�olocalis�s port� par la publicit� sur les r�seaux�
Qui influence les achats locaux sur mobile ?
31 mars 2014
Local Corporation sp�cialiste de la publicit� g�olocalis�e aux Etats-Unis vient de publier une �tude men�e aupr�s de 1200 propri�taires de�
Au-del� du gaming : Facebook rach�te Oculus VR, sp�cialiste de la r�alit� virtuelle pour 2 milliards de dollars
26 mars 2014
Premi�re acquisition dans le hardware pour Mark Zuckerberg. Facebook vient d�annoncer le rachat d�Oculus VR , cr�ateur du casque de�
