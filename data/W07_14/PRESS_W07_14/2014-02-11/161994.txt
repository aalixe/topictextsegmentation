TITRE: Jos� Mourinho compare Chelsea � Manchester City | Walfoot
DATE: 2014-02-11
URL: http://www.walfoot.be/fra/news/lis/2014-02-11/jose-mourinho-compare-chelsea--manchester-city
PRINCIPAL: 161993
TEXT:
Jos� Mourinho compare Chelsea � Manchester City
11 f�vrier 2014 00:19
Tweet
Le technicien portugais n'a pas sa langue dans sa poche.
Au coeur d'une lutte m�diatique avec Manuel Pellegrini, Jos� Mourinho s'est d�fendu que Chelsea soit le club le plus riche.
"Je n'ai pas � me r�p�ter mais tout le monde ici sait ce qu'est r�ellement Manchester City. Pellegrini a parl� de nos d�penses. C'est un entra�neur fantastique, je respecte �a et au del� de �a, il a une formation universitaire d'ing�nieur", a expliqu� Mourinho.
"Je ne pense pas qu'un ing�nieur ait besoin d'une calculette pour savoir que Mata vendu 44,5 millions d'euros et de Bruyne 21,5 millions d'euros, cela fait 66 millions de recettes. Et Matic achet� 25 millions d'euros et Salah 13 millions d'euros, cela fait 38 millions de d�penses. 66-38=28, donc Chelsea a g�n�r� 28 millions d'euros cet hiver".
"Nous on construit une �quipe pour la d�cennie et eux ils ont une �quipe pour gagner maintenant ou dans les 3-4 ans � venir", a-t-il conclu.
