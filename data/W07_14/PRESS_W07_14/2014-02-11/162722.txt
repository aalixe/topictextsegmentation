TITRE: Mark Zuckerberg, philanthrope de l�ann�e pour son engagement en faveur des enfants
DATE: 2014-02-11
URL: http://www.vousnousils.fr/2014/02/11/mark-zuckerberg-philanthrope-de-lannee-pour-son-engagement-en-faveur-des-enfants-552343
PRINCIPAL: 162721
TEXT:
Le co-fondateur de Facebook Mark Zuckerberg, et sa femme Priscilla Chan, p�diatre, sont en t�te du der�nier clas�se�ment du jour�nal The Chronicle of Philanthropy des 50 plus g�n�reux dona�teurs am�ri�cains .
Don d'un mil�liard de dol�lars en�2013
En 2013, le couple a offert � la Fondation de la Silicon Valley 18 mil�lions de leurs actions per�son�nelles Facebook, d'une valeur de pr�s d'un mil�liard de dol�lars. Les pro�jets finan�c�s ont pour cible "les enfants".
En 2012 d�j�, Mark Zuckerberg avait fait don de 500 mil�lions de dol�lars � des pro�jets li�s � l'�ducation et � la sant�. D�s 2010, le patron le mieux pay� des Etats-Unis avait cr�� avec un apport de 100 mil�lions de dol�lars la fon�da�tion Startup:Education , dont la mis�sion est d'adopter "une approche de start-up envers la cr�a�tion d'un meilleur sys�t�me �duca�tif pour tous les �l�ves".
Les 50 dona�teurs salu�s par le Chronicle of Philanthropy ont vers� au total 7,7 mil�liards de dol�lars (5,64 mil�liards d'euros) l'an dernier.
Sur le m�me sujet
