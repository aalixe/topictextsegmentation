TITRE: Nantes, 2e ville la plus accessible - 20minutes.fr
DATE: 2014-02-11
URL: http://www.20minutes.fr/nantes/1295498-nantes-2e-ville-plus-accessible
PRINCIPAL: 0
TEXT:
Nantes, 2e ville la plus accessible Archives F. Elsner / 20 Minutes
Handicap
L'Association des paralys�s de France (APF) publie ce mardi son cinqui�me barom�tre des villes les plus accessibles aux personnes en situation de handicap. Pour la deuxi�me ann�e cons�cutive, Nantes se classe deuxi�me derri�re Grenoble. ��Notre d�marche d'accessibilit� universelle, qui veut que les am�nagements profitent � toute la soci�t�, est d�sormais bien comprise par la ville, estime Gr�goire Charmois, d�l�gu� d�partemental de l'association. Maintenant, il faut transformer l'essai et voir davantage de choses concr�tes, notamment en mati�re d'accessibilit� de la voirie.�� Exemplaire sur les transports en commun, Nantes a poursuivi ses efforts sur l'accessibilit� des commerces de proximit� en r�unissant leurs propri�taires, les usagers et les techniciens de la ville pour r�fl�chir ensemble aux possibilit�s d'am�nagements.
Les candidats interrog�s
Ce mardi, � l'occasion de la publication du barom�tre, la d�l�gation d�partementale de l'APF interpellera trois des candidates aux �lections municipales de Nantes�: Laurence Garnier (UMP), Johanna Rolland (PS) et Pascale Chiron (EELV - Les Verts) seront interrog�s sur leurs propositions en mati�re d'accessibilit�. De 14�h�30 � 17�h, l'APF se rassemblera place du Commerce pour sensibiliser la population.
La ville de Nantes fait figure de bon �l�ve. Lors des trois premiers barom�tres organis�s par l'APF, en 2010, 2011 et 2012, elle avait remport� la premi�re place. D. P.
