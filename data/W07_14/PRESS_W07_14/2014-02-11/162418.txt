TITRE: Ligue 1: Andorre se rapproche de Toulouse - 20minutes.fr
DATE: 2014-02-11
URL: http://www.20minutes.fr/sport/1295254-ligue-1-andorre-se-rapproche-de-toulouse
PRINCIPAL: 0
TEXT:
L'attaquant du TFC Wissam Ben Yedder a inscrit un tripl� contre Sochaux en Ligue 1 (5-1), le 30 novembre 2013 au Stadium de Toulouse.
�
F. Scheiber / 20 Minutes
FOOTBALL - La F�d�ration andorrane de football et le TFC n�gocient un partenariat�
Qu'on se le dise, Andorre aime le sport toulousain. Partenaire officiel des rugbymen du Stade , la petite principaut� pyr�n�enne de 76 000 habitants noue d�sormais des liens avec les footballeurs du TFC. Toutefois, les deux cas diff�rent. Si le premier contrat reste essentiellement commercial, l�accord en pr�paration se veut purement sportif.
�Les n�gociations ont d�but� il y a trois semaines, il y a eu une nouvelle r�union entre dirigeants vendredi � Toulouse, et le document doit �tre sign� le 5�mars, en Andorre�, d�taille David Rodrigo, le directeur sportif de la F�d�ration andorrane de football (FAF) . Au menu, tout d�abord:�un �change de techniciens.��Des Andorrans doivent venir une semaine � Toulouse pour assister aux entra�nements des diff�rentes �quipes du club, reprend le dirigeant. A l�inverse, des entra�neurs toulousains viendront voir nos s�lections nationales.�
198e place au classement FIFA
De jeunes joueurs de la Principaut� pourront aussi venir tenter leur chance dans la Ville rose.��Nos meilleurs joueurs �g�s de 12 � 19 ans effectueront un stage au TFC , explique Rodrigo. Si l�essai est concluant, certains pourront int�grer les �quipes de jeunes du club.��Les n�gociations ne sont pas encore achev�es, d�o� la discr�tion des dirigeants toulousains, qui pr�f�rent attendre la signature finale pour communiquer.
�L�important dans un accord, c�est que les deux parties en b�n�ficient�, assure-t-on du c�t� de la FAF. C�t� andorran en tout cas, ce partenariat ne pourra qu��tre positif. Avec environ 3 000 licenci�s ( selon la FIFA ), le pays ne peut �videmment pas lutter avec des nations bien plus peupl�es. La s�lection occupe la 198e place au classement mondial sur 209 pays, � �galit� avec les Comores et juste devant l�Erythr�e. Si la majeure partie des internationaux �volue au pays, dans des clubs amateurs, le d�fenseur Marc Vales Gonzalez joue au Real Madrid, dans l��quipe C.
Nicolas Stival
