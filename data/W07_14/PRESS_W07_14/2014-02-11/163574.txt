TITRE: Shirley Temple, l'enfant ch�rie de Hollywood, est morte � 85 ans
DATE: 2014-02-11
URL: http://www.francetvinfo.fr/culture/cinema/l-actrice-americaine-shirley-temple-est-morte_527177.html
PRINCIPAL: 163461
TEXT:
Tweeter
Shirley Temple, l'enfant ch�rie de Hollywood, est morte � 85 ans
Enfant star dans les ann�es 30, la com�dienne est d�c�d�e � son domicile, en�Californie. Elle a �galement �t� ambassadrice.
Photo non dat�e de Shirley Temple. (AFP)
Par Francetv info avec AFP et Reuters
Mis � jour le
, publi� le
11/02/2014 | 11:48
Elle a connu la gloire au cin�ma d�s son enfance, avant de devenir diplomate par la suite. La com�dienne am�ricaine�Shirley Temple est morte � son domicile californien,�lundi 10 f�vrier, � l'�ge de 85 ans, a annonc� mardi un porte-parole de sa famille.
�
L'actrice Shirley Temple dans le film "The Poor Little Rich Girl", en 1936. (KOBAL / AFP)
"Nous saluons sa vie et ses remarquables r�ussites en tant qu'actrice, en tant que diplomate, et surtout en tant que m�re, grand-m�re, arri�re-grand-m�re", a indiqu� la famille dans un communiqu�.�
L'enfant ch�rie de l'Am�rique
Enfant star d'Hollywood, Shirley Temple a obtenu son premier r�le dans un film � l'�ge de 3 ans, rappelle la BBC �(en anglais).�Enchantant l'Am�rique des ann�es 30 avec son visage d'ange et ses cheveux boucl�s, elle avait remport� un Oscar sp�cial en 1935, alors qu'elle n'avait que 6 ans, pour sa performance dans Shirley aviatrice, explique Vanity Fair . A ce jour, elle est toujours la plus jeune � avoir re�u la prestigieuse r�compense.
Shirley Temple a jou� dans une quarantaine de films, la plupart avant l'�ge de 12 ans, notamment Boucles d'or�(Curly Top) en 1935, ou Petite princesse (The Little Princess) en�1939�ou encore�Le Massacre de Fort Apache en�1948.
En 1936, �g�e de seulement 7 ans, la petite fille gagnait pr�s de 50 000 dollars, soit plus de 580 000 euros actuels, indique le Guardian �(en anglais). La m�me ann�e, un barman de Beverly Hills�cr�a un cocktail sans alcool iconique, qui porte depuis son nom.�
Une seconde carri�re dans la diplomatie
La com�dienne avait pris une retraite du grand �cran tr�s pr�coce, en 1950, � seulement 21 ans, mais apr�s�une quarantaine de films. Elle�avait alors embrass� une carri�re de diplomate.
En 1974, le pr�sident Gerald Ford l'avait nomm�e ambassadrice au Ghana, et deux ans plus tard il en avait fait�la premi�re femme chef de protocole du d�partement d'�tat des �tats-Unis.�
En 1989, le pr�sident George Bush p�re lui avait offert le poste d'ambassadrice � Prague, poste sensible en Europe de l'Est, normalement d�volu � des diplomates de carri�re. Elle y avait v�cu la R�volution de velours, la chute du r�gime communiste entre 1989 et 1992.
R�publicaine, elle avait aussi tent� l'aventure comme candidate � la Chambre des repr�sentants en 1967, sans succ�s. Elle avait ensuite�fait partie de la d�l�gation am�ricaine � l'ONU.
