TITRE: Argent : En France, les jeunes �pargnent, les vieux d�pensent - Economie Matin
DATE: 2014-02-11
URL: http://www.economiematin.fr/ecoquick/item/8544-epargne-france-jeunesse-argent-patrimoine
PRINCIPAL: 164117
TEXT:
E-mail
57 % des jeunes de moins de 25 ans veulent �pargner davantage en 2014 qu'en 2013. ShutterStock.com
Le monde � l'envers ! L'image d'Epinal du jeune flambeur en prend un coup, sans doute � cause de la conjoncture �conomique, et de la difficile insertion des jeunes sur le march� du travail. D'apr�s un sondage CSA pour le Cercle des �pargnants, plus de la moiti� des jeunes de moins de 25 ans souhaitent �pargner un maximum en 2014.
�
Crise �conomique, ch�mage, prix de l'immobilier�Les raisons qui peuvent expliquer un tel r�sultat sont nombreuses.
57 % des moins de 25 ans veulent �pargner en 2014�
Force est de constater qu'en mati�re de gestion d'argent, la jeunesse fran�aise s'assagit. D'apr�s un sondage CSA pour le Cercle des �pargnants, 57 % des 18-24 ans souhaitent en effet �pargner davantage en 2014 qu'en 2013. Les 25-34 ans sont 44 % dans ce cas. Le principe de pr�caution gagne donc les jeunes g�n�rations.
25 % des plus de 50 ans veulent puiser dans leur �pargne pour vivre�
A l'inverse, leurs a�n�s veulent revivre le mythe des ann�es folles. D'apr�s le sondage CSA, 25 % des 50-64 ans ont l'intention de puiser dans leur �pargne en 2014 pour se maintenir � un niveau de vie raisonnable, tout comme 30 % des 50-64 ans.
35 % des Fran�ais veulent soigner leur �pargne�
De mani�re g�n�rale, 35 % des Fran�ais semblent vouloir faire attention � leur �pargne en 2014, contre 23 % en 2010. Et 22 % des personnes interrog�es sont bien d�cid�es � puiser dans leur �pargne.
En 2010, le patrimoine m�dia des moins de 30 ans �tait de 3 600 euros�
En terme de chiffres ,le patrimoine financier m�dian des 60-69 ans �tait de 16 700 euros en 2010, celui des plus de 70 ans de 14 600 euros. A l'inverse, les moins de 30 ans pouvaient t�moigner d'un patrimoine financier m�dian de 3 600 euros.
�
L'�tude du Cercle des �pargnants d�montre que les jeunes prennent conscience de la difficult� des conditions de vie auxquelles ils sont soumis, une chose que n'ont pas connue leurs parents. L'assagissement est donc de mise pour ces jeunes, l� o� on s'attendait plut�t � les voir d�penser une plus grande partie de leurs revenus.
�
Fiche technique : �
Sondage con�u par le Centre d'�tudes et de connaissances sur l'opinion publique (Cecop) pour le Cercle des �pargnants et r�alis� par l'institut CSA par t�l�phone les 7 et 8 janvier 2014 aupr�s d'un �chantillon repr�sentatif de 1.009 personnes �g�es de 18 ans et plus, selon la m�thode des quotas.
Jean-Baptiste Le Roux
Jean-Baptiste Le Roux est journaliste. Il travaille �galement pour Radio Notre Dame o� il s'occupe du site Internet. Il a travaill� pour Jalons, Causeur et Valeurs Actuelles avec Basile de Koch avant de rejoindre Economie Matin, � sa cr�ation, en mai 2012. Il est dipl�m� de l'Institut europ�en de journalisme et membre de l'Association des Journalistes de D�fense.
Tagg� sous
