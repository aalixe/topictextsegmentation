TITRE: Une attaque � la grenade fait 13 morts dans un cin�ma du Pakistan � International � 98,5 fm Montr�al
DATE: 2014-02-11
URL: http://www.985fm.ca/international/nouvelles/une-attaque-a-la-grenade-fait-13-morts-dans-un-cin-299807.html
PRINCIPAL: 164634
TEXT:
Une attaque � la grenade fait 13 morts dans un cin�ma du Pakistan
Publi� par Associated Press le mardi 11 f�vrier 2014 � 13h35. Modifi� par 98,5 Sports � 13h54.
PESHAWAR, Pakistan - Au moins 13 personnes ont �t� tu�es quand des assaillants ont lanc� des grenades dans une salle de cin�ma bond� du nord-ouest du Pakistan, mardi.
L'attaque perp�tr�e dans la ville de Peshawar n'a pas �t� imm�diatement revendiqu�e. Environ 80 personnes se trouvaient dans la salle au moment de l'attentat.
Peshawar se trouve pr�s des r�gions limitrophes de la fronti�re avec l'Afghanistan, o� se terrent les talibans pakistanais et des groupes associ�s � Al-Qa�da.
La police a ouvert une enqu�te.
