TITRE: Omnisport | JO Sotchi - Halfpipe : Shaun White s�est manqu� !
DATE: 2014-02-11
URL: http://www.le10sport.com/omnisport/jo-sotchi-halfpipe-shaun-white-sest-manque-135681
PRINCIPAL: 164868
TEXT:
Envoyer � un ami
Tr�s attendu et grand favori de l��preuve de halfpipe des Jeux Olympiques de Sotchi, l�Am�ricain Shaun White n�a pas r�ussi le tripl�. Le rider a chut� lors de son premier run, a commis quelques erreurs lors du second, ce qui lui a valu une quatri�me place. Il a �t� devanc� par le Suisse Iouri Podladtchikov et les Japonais Ayumu Hirano et Taku Hiraoka.
