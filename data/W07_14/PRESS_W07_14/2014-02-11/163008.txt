TITRE: St�phane Rotenberg : "On n'a pas le droit de bouger donc forc�ment il y a une situation d�sagr�able" dans Laissez-vous tenter le 11-02-2014 sur RTL.
DATE: 2014-02-11
URL: http://www.rtl.fr/emission/laissez-vous-tenter/billet/stephane-rotenberg-on-n-a-pas-le-droit-de-bouger-donc-forcement-il-y-a-une-situation-desagreable-7769635551
PRINCIPAL: 163007
TEXT:
Accueil > Emissions > Laissez-vous tenter > Blog > T�l�vision > St�phane Rotenberg : "On n'a pas le droit de bouger donc forc�ment il y a une situation d�sagr�able"
Laissez-vous Tenter
Laurent Bazin & Le Service Culture
Du lundi au vendredi de 09h � 09h30
S'inscrire au podcast
St�phane Rotenberg : "On n'a pas le droit de bouger donc forc�ment il y a une situation d�sagr�able"
Par Laurent Marsick | Publi� le 11/02/2014 � 10h29 | Laissez-vous tenter
St�phane Rotenberg : "Le risque z�ro, c'est l'enfer !"
Cr�dit : dailymotion
Le pr�sentateur de "P�kin Express" raconte la situation dans laquelle lui et son �quipe sont bloqu�s. Ils ont �t� assign�s � r�sidence par les autorit�s indiennes depuis 3 jours et n'ont plus leurs passeports.
L'�quipe de P�kin Express, en ce moment en tournage en Inde, a �t� arr�t�e et assign�e � r�sidence parce qu'ils utilisaient des t�l�phones satellites. La d�tention de ces objets est tr�s encadr�e en Inde, essentiellement pour des raisons de s�curit�, le pays devant faire face � des rebelles mao�stes ou s�paratistes dans le Cachemire.
St�phane Rotenberg est toujours coinc� � son h�tel et donne des nouvelles de l'�quipe.
St�phane Rotenberg : "On est bloqu�s il y a des gens devant la grille de l'h�tel avec des armes, on n'a pas le droit de bouger, on n'a pas nos passeports donc forc�ment il y a une situation d�sagr�able"
Cr�dit : Laurent Marsick
"On est bloqu�s il y a des gens devant la grille de l'h�tel avec des armes, on n'a pas le droit de bouger, on n'a pas nos passeports donc forc�ment il y a une situation d�sagr�able, d�crit le pr�sentateur. On pense aux 3 gars de l'�quipe qui sont dans la grande ville serr�s par la police mais qui sont libres, surveill�s ailleurs. Voil� c'est �trange, mais on attend que �a se r�gle."
Les trois gars de l'�quipe, ce sont ceux de la s�curit� et une personne de la production. Si �a ne se d�bloque pas, c'est tout simplement parce qu'il y a visiblement un probl�me administratif avec ces 22 t�l�phones satellites.
"Grosso modo c'est un peu flou, ils nous parlent d'espionnage, ils nous parlent de choses. Ils ne sont pas d�sagr�ables mais de temps en temps �a se tend et de temps en temps �a se d�tend, c'est tr�s variable", continue St�phane Rotenberg.
Pourquoi des t�l�phones satellites ?
Pour des raisons de s�curit�. "On ne les utilise normalement jamais et l� c'est un cas particulier parce qu'on est dans des zones tr�s recul�es o� les GSM peuvent ne pas passer, on avait quelques t�l�phones satellites, 20 sur toute une �quipe, au cas o� il y ait un p�pin", explique l'animateur de M6.
Ils ne pensaient pas du tout que �a allait provoquer une telle r�action, il s'agit, pour eux, d'une simple s�curit� suppl�mentaire. "On n'a pas cach� nos t�l�phones sous nos sacs, on les a donn�s lorsqu'ils nous les ont demand�s. On est un peu au milieu d'une situation pour l'instant qu'on ne ma�trise pas tr�s bien", poursuit-il.
Quelle suite ?
Chaque ambassade (il y a plusieurs nationalit�s dans l'�quipe) suit tout �a de pr�s mais il faut attendre qu'un juge local prenne une d�cision. Pour la suite, l'option optimiste serait que �a se d�bloque vite, et apr�s l'Inde direction le Sri Lanka avec probablement une �tape suppl�mentaire.
Je continue � penser que demain matin je fais mon check out � l'h�tel et je pars, mais �a fait 3 jours que je pense �a
St�phane Rotenberg
"On va essayer de rattraper le train", esp�re St�phane Rotenberg, c'est � dire l'�tape 8. Les �quipes qui sont d�j� dans le pays d'apr�s sont en train d'�crire une �tape suppl�mentaire. "Chaque jour, on imagine d'autres choses, je continue � penser que demain matin je fais mon check out � l'h�tel et je pars, mais �a fait 3 jours que je pense �a. C'�tait une tr�s belle saison, tr�s surprenante, maintenant on attend de repartir pour que �a continue", conclut-il.
Cette immobilisation a sans doute co�t� de l'argent � la production.
La r�daction vous recommande
