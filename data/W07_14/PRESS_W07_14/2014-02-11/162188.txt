TITRE: Valence | Baromètre accessibilité : Valence dégringole, Privas remonte
DATE: 2014-02-11
URL: http://www.ledauphine.com/drome/2014/02/10/barometre-accessibilite-valence-degringole-privas-remonte
PRINCIPAL: 162184
TEXT:
DROME/ARDECHE Baromètre accessibilité : Valence dégringole, Privas remonte
Previous Next
�? moins d�??un an de l�??application de la loi accessibilité (1er janvier 2015), le baromètre de l�??Association des paralysés de France (APF) sur les 96 chefs-lieux départementaux a une valeur toute particulière cette année. Les résultats sont rendus publics aujourd�??hui. Premier constat, global : « Avis de tempête, le retard pris dans la mise en accessibilité des villes-préfectures est plus que préoccupant ». Du coup, l�??APF joue la carte des futures échéances électorales et « demande que cet enjeu primordial soit inscrit dans les programmes des candidats aux élections municipales à venir ».
Pour les villes préfectures de Drôme et d�??Ardèche, le constat est bien différent.
D�??un côté Valence se classe à la 83e place sur 96, dégringolant de 7 places en un an et de 10 places en 5 ans !
Le problème se cristallise sur l�??accès aux commerces de proximité. « Du coup les grandes surfaces ont la cote ! », commente Pascal Astier, chargé de mission APF-Drôme. Qui souligne également les problèmes d�??accès aux cabinets médicaux et paramédicaux. « Je choisis mon médecin, mon kiné en fonction de son accessibilité », commente-t-il.
« Constat accablant, mais finalement pas surprenant », pour Serge Gouchet, bénévole à l�??APF Drôme. « La commission d�??accessibilité a mis du temps à se mettre en place à Valence, elle compte des personnes certes compétentes, mais elle n�??est pas entendue. Nous ne ressentons pas de véritable politique globale volontariste sur le sujet de l�??accessibilité. Or cette question concerne pourtant 40 % de la population si on considère les personnes handicapées, celles à mobilité réduite, les personnes âgées, les personnes temporairement blessées, les parents-poussettes ».
Quant à la ville de Privas, son résultat est nettement meilleur puisque sa position progresse, passant de la 70e place l�??an dernier à la 58e place cette fois.
Comme pour les éditions précédentes, Grenoble, Nantes et Caen restent en tête pour leur bonne prise en compte de l�??accessibilité.
Par Frédérique FAYS | Publié le 11/02/2014 à 06:04
Vos commentaires
Connectez-vous pour laisser un commentaire
Se connecter
Pour accéder à votre espace personnel, vous devez fournir un identifiant et un mot de passe.
Email
