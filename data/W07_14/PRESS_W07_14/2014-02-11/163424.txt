TITRE: Halfpipe (M): Longo et Baisamy en demie - JO 2014 - Sports.fr
DATE: 2014-02-11
URL: http://www.sports.fr/jo-2014/snowboard/scans/halfpipe-m-longo-et-baisamy-en-demie-1008392/
PRINCIPAL: 163423
TEXT:
11 février 2014 � 12h58
Mis à jour le
11 février 2014 � 12h58
Arthur Longo et Johann Baisamy se sont qualifiés pour les demi-finales de halfpipe masculin, ce mardi matin à l'issue des qualifications de la première série. Les riders français, respectivement 4e et 9e après leurs deux runs, devront donc réaliser deux nouveaux passages pour se hisser en finale, programmée à 18h30.
Un temps deuxième et directement qualifié avec un score de 79,25 points, Arthur Longo a finalement été dépassé par les Suisses Christian Haller et David Habluetzel.
Le Japonnais Ayumu Hirano a largement pris la tête de la série, grâce à un premier run qui lui a permis de récolter auprès des juges la note de 92,25. La star de la discipline, l'Américain Shaun White, fait son entrée en lice dans la seconde série.
