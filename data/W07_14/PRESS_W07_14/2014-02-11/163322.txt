TITRE: VIDEO. Municipales : � 93 ans, le maire renonce � briguer un 12e mandat - Municipales 2014
DATE: 2014-02-11
URL: http://www.leparisien.fr/municipales-2014/video-municipales-a-93-ans-le-maire-renonce-a-briguer-un-12e-mandat-11-02-2014-3580367.php
PRINCIPAL: 0
TEXT:
Municipales � Saint-Genis-Laval : � 100 ans, elle se pr�sente sur une liste FN
Toujours pl�biscit� par ses administr�s d�s le premier tour, Roger S�ni�est fier d'�tre le doyen des maires avec son homologue Arthur Richier de Faucon-du-Caire (Alpes-de-Haute-Provence), �g� lui de 92 ans. �Comme le relate � La D�p�che du Midi�, qui r�v�le l'information , ce��vieux lutteur gaulliste� est rest� tr�s actif lors de son dernier mandat.
En mars 2013, il d�missionnait avec fracas pour protester contre une perte fiscale de 145.000 euros li�e au rattachement de sa commune � la communaut� de communes de Mirepoix.�Il est all� jusqu'� la gr�ve de la faim. En vain. Il a toutefois �t� confort� par les �lecteurs en juillet lors d'une nouvelle �lection municipale.
C'est donc en spectateur que le vieil homme assistera pour la premi�re fois depuis 67 ans � l'�lection municipale des 23 et 30 mars prochains. �Je vais m'en occuper tr�s s�rieusement�, indique-t-il toutefois, promettant de�soutenir pleinement une liste des ses amis�qui �a de fortes chances d'�tre conduite par une femme�. En face, il y aura une seconde liste.�Roger S�ni� promet d�j� une bataille acharn�e �En face de nous, il y a un transfuge � qui j'avais mis le pied � l'�trier il y a six ans, mais qui nous a abandonn�s dans la bataille l'an dernier�, lance cet �ternel battant.
VIDEO. Quand Roger S�ni� voulait rempiler
�
