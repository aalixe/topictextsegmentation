TITRE: Chelsea: si Hazard part... Mourinho partirait aussi!  - L'Avenir Mobile
DATE: 2014-02-11
URL: http://www.lavenir.net/Sports/cnt/DMF20140211_015
PRINCIPAL: 164379
TEXT:
Chelsea: si Hazard part... Mourinho partirait aussi!
11-02-2014 14:33
-
Jos� Mourinho aurait notamment conditionn� son avenir � Chelsea � celui d'Eden Hazard, selon une source interne � Chelsea.
+ Cliquez sur ce lien pour rejoindre notre page Facebook et recevoir nos meilleures infos sport
Eden Hazard , auteur de son premier tripl� en Premier League face � Newcastle , a accord� une large interview � L'�quipe. On y apprend notamment que Jos� Mourinho aurait li� son avenir � Chelsea � deux joueurs, dont le Diable rouge. Une information nourrie par une "source interne" aux Blues.
"Jos� Mourinho a conditionn� son propre avenir � Chelsea � la pr�sence d'Hazard (23 ans) et Oscar (22 ans). Avec Hazard, Roman Abramovitch (NDLR, le proprio des Blues) est persuad� d'avoir recrut� le meilleur joueur du monde des ann�es � venir et il n'�coutera aucune offre �ventuelle, m�me astronomique. Il n'a d'ailleurs jamais laiss� partir un joueur pour de l'argent."
Sur la piste le menant au PSG, �voqu�e lors du dernier mercato hivernal , Hazard a confi� au quotidien sportif fran�ais qu'il ne se voyait pas revenir en Ligue 1. Un championnat pas (plus) assez visible pour ses ambitions.
BROB
