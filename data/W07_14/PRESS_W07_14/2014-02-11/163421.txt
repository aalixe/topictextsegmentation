TITRE: Un appartement-piscine de cannettes de bi�re d�couvert en Bretagne - Terrafemina
DATE: 2014-02-11
URL: http://www.terrafemina.com/societe/buzz/articles/37859-un-appartement-piscine-de-cannettes-de-biere-decouvert-en-bretagne.html
PRINCIPAL: 163411
TEXT:
Un appartement-piscine de cannettes de bi�re d�couvert en Bretagne
Par
Publi� le 11 f�vrier 2014
Un appartement-piscine de cannettes de bi�re d�couvert en Bretagne
��Capture d'�cran
�
�
Un couple de propri�taires d'un appartement dans les C�tes-d'Armor, sans nouvelles du locataire, a eu le malheur de retrouver leur bien enti�rement rempli de cadavres de bouteilles de bi�re.
Il aimait visiblement la bi�re . Le locataire d'un appartement de Ploeuc-sur-Li� (C�tes-d'Armor) a rempli le domicile de milliers de cadavres de bouteilles de bi�re, transformant les lieux en une v�ritable d�charge.
Le couple de propri�taires, sans nouvelles du locataire des lieux depuis un an, avaient tent� d'acc�der � leur bien, mais la porte d'entr�e de l'appartement ne s'ouvrait pas de plus de 30 centim�tres. Apr�s avoir obtenu un droit d'acc�s au logement, les propri�taires, sous contr�le d'huissiers, ont alors d�couvert l'�tendue des d�g�ts.
� On ne pensait pas que c'�tait � ce point-l� �
� On ne pensait pas que c'�tait � ce point-l�. On pouvait imaginer que la porte �tait bloqu�e mais pas pour ces raisons-l� �, a expliqu� Sophie Le Guilloux-Rousseau, l'une des propri�taires des lieux, � Ouest France. Le locataire, visiblement peu � cheval sur l'hygi�ne et grand amateur de houblon, a empil� les bouteilles sur environ un m�tre de haut dans certains coins du salon et de la chambre.
La propri�taire a finalement crois� l'auteur des d�g�ts , dimanche 9 f�vrier. � J'ai beau �tre en col�re r�volt�e. C'est une personne qui a besoin d'aide, clairement �, indique-t-elle. Et de pr�ciser : � il reconna�t qu'il est en tort, que c'est de sa faute, mais il n'a pas d'explications, il est penaud �.
En attendant et malgr� des impay�s de loyers, l'homme (qui n'occupe plus les lieux) ne pourra pas �tre expuls� avant le 15 mars, date � laquelle s'ach�ve la tr�ve hivernale.
