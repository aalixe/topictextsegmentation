#!/usr/bin/env python
# coding: utf-8

# # Packages

# In[256]:


print('#1 Packages')


# In[257]:


# Miscelaneous
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import listdir
from string import punctuation
import re

# SKLearn
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, accuracy_score

# Keras
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Embedding, GRU, concatenate, Input, Bidirectional, LSTM, Concatenate, Dropout
from keras.optimizers import Adam, SGD
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, LearningRateScheduler
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

# Gensim
from gensim.models import Word2Vec

# Mac Bug
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

# Nltk
from nltk.tokenize import TweetTokenizer


# # Miscelaneous

# In[258]:


print('#2 Miscelaneous')


# In[259]:


path = "../../data/Data/"
path_train = path
path_word2vec = "../preprocess/saves/wholedataset24112019/word2vec_200.model"
files = [f for f in listdir(path_train)]


# # Standardization, Computing Max Seq Lentgh & Tokenizer

# In[260]:


print('#3 Standardization, Computing Max Seq Lentgh & Tokenizer')


# In[261]:


def get_stop(file):
    f = open(file, 'r', encoding='latin_1')
    return list(filter(lambda x: x != "valeur", map(lambda x : x[:-1], f.readlines())))

stopWords = set(get_stop("../preprocess/fr-stopword.txt"))
MIN_LENGTH = 2

def standardization(text):
    text = re.sub(r"[0-9]+", "", text)
    text = re.sub(r'http\S+', "", text)
    text = text.lower()
    text = text.translate(str.maketrans(punctuation, ' ' * len(punctuation)))
    text = TweetTokenizer().tokenize(text)
    text = [i for i in text if (i not in stopWords) and (i not in punctuation ) and (len(i) > MIN_LENGTH)]
    return text


# In[262]:


standardization("Bonjour, et bienvenue sur ce journal! :). Va sur cette url: https://www.google.fr! Jacque Chirace est mort. La grève des transport continue")


# # Data Loading

# In[263]:


df = pd.DataFrame()
doc = 0
for idx in range(len(files)):
    tmp = pd.read_csv(path_train + files[idx], sep='\t', names=['start', 'end', 'loc', 'text', 'topic'])
    tmp['doc'] = doc
    doc += 1
    df = df.append(tmp)
print(df.head())
print(df.shape)


# In[264]:


# Tokenizer
all_speech = standardization(' '.join(df.text))
max_seq_length = df['text'].apply(lambda text: len(standardization(text))).max()
tokenizer = Tokenizer()
tokenizer.fit_on_texts(all_speech)
word_index = tokenizer.word_index

# Number Words
nb_words = len(word_index) + 1
nb_words, max_seq_length


# # Preprocessing

# In[265]:


df['seq'] = df['text'].apply(lambda text: standardization(text))
df['seq'] = tokenizer.texts_to_sequences(df['seq'])
df['seq'] = df['seq'].apply(lambda x: pad_sequences([x], maxlen=max_seq_length)[0])
print(df.head())
print(df.shape)


# In[266]:


nb_docs = df['doc'].max()
val = int(0.75 * nb_docs)
test = int(0.9 * nb_docs)
df_train = df[df['doc'] <= val]
df_val = df[(df['doc'] > val) & (df['doc'] <= test)]
df_test  = df[df['doc'] > test]
print(df_train.head())
print(df_train.shape)
print(df_val.head())
print(df_val.shape)
print(df_test.head())
print(df_test.shape)


# # Embedding Matrix

# In[267]:


print('#4 Embedding Matrix')


# In[268]:


# Word2Vec
EMBEDDING_DIM = 200
word2vec = Word2Vec.load(path_word2vec)


# In[269]:


# Default Vector
default_vector = (np.random.rand(EMBEDDING_DIM) * 2.0) - 1
default_vector /= np.linalg.norm(default_vector)

# Embeddings Matrix
embedding_matrix = np.zeros((nb_words, EMBEDDING_DIM))
cpt = 0
for word, i in word_index.items():
    if word in word2vec.wv.vocab:
        embedding_matrix[i] = word2vec.wv[word]
        cpt += 1
    else:
        embedding_matrix[i] = default_vector
print('Embedding Matrix is full at {}%'.format(100 * cpt / len(embedding_matrix)))


# # Generator

# In[270]:


print('#5 Generator')


# In[271]:


class DataGenerator:
    def __init__(self, df, batch_size, max_seq_length, window=5, stride=1):
        self.df = df
        self.batch_size = batch_size
        self.max_seq_length = max_seq_length
        self.window = window
        self.stride = stride
        self.nb_files = df['doc'].max()
        self.step = 0
        for file_id in range(self.nb_files):
            current_df = self.df[df['doc'] == file_id]
            for idx in range(0, len(current_df) - self.window, self.stride):
                self.step += 1
        self.step = self.step // batch_size + (self.step % batch_size > 0)
        
    def getFitGenerator(self):
        X = [[] for _ in range(self.window)]
        Y = []
        while True:
            for file_id in range(self.nb_files):
                current_df = self.df[self.df['doc'] == file_id]
                for idx in range(0, len(current_df) - self.window, self.stride):
                    for xseq in range(self.window):
                        X[xseq].append(current_df.iloc[idx + xseq]['seq'])
                    Y.append(len(self.df[idx: idx + self.window]['topic'].unique()) > 1)
                    if len(Y) == self.batch_size:
                        for i in range(len(X)):
                            X[i] = np.array(X[i])
                        yield X, np.array(Y).astype(float)
                        X = [[] for _ in range(self.window)]
                        Y = []
                        
    def getPredictGenerator(self):
        while True:
            X = [[] for _ in range(self.window)]
            for file_id in range(self.nb_files):
                current_df = self.df[self.df['doc'] == file_id]
                for idx in range(0, len(current_df) - self.window, self.stride):
                    for xseq in range(self.window):
                        X[xseq].append(current_df.iloc[idx + xseq]['seq'])
                    if len(X[0]) == self.batch_size:
                        for i in range(self.window):
                            X[i] = np.array(X[i])
                        yield X
                        X = [[] for _ in range(self.window)]
            if len(X):
                for i in range(len(X)):
                    X[i] = np.array(X[i])
                yield X
            
    def getTrue(self):
        Y = []
        for file_id in range(self.nb_files):
            current_df = self.df[self.df['doc'] == file_id]
            for idx in range(0, len(current_df) - self.window, self.stride):
                Y.append(len(self.df[idx: idx + self.window]['topic'].unique()) > 1)
        return np.array(Y).astype(float)
    
    def testGenerator(self):
        for i, j in self.getFitGenerator():
            for x in range(len(i[0])): # 0 -> 15
                if not j[x]:
                    pass
                for l in range(len(i)): # 0 -> 2
                    print('\nPhrase #{} ->'.format(l))
                    for k in i[l][x]:
                        for key in word_index:
                            if word_index[key] == k:
                                print(key, end=' ')
                                break
                print("\n|")
                print("|------> {}".format(j[x]))
                return
            print('\n -> End of the batch\n')
            return


# In[272]:


gen = DataGenerator(df, 5, max_seq_length)
gen.testGenerator()


# In[280]:


def getModel(df_train, df_val, nb_words, embedding_dim, max_seq_length, window=7, load_weights=False):
    if load_weights:
        return load_model("model.h5"), None
    else:
        # Data
        TRAINING_GENERATOR = DataGenerator(df_train, 32, max_seq_length, window=window)
        VALIDATION_GENERATOR = DataGenerator(df_val, 32, max_seq_length, window=window)
        STEPS_PER_EPOCH = TRAINING_GENERATOR.step
        VALIDATION_STEPS = VALIDATION_GENERATOR.step
        LEARNING_RATE = 0.05
        LR_PATIENCE = 2
        PATIENCE = 3
        EPOCHS = 32
        
        # Inputs
        inputs = []
        for i in range(window):
            inputs.append(Input(shape=(max_seq_length,), name='input_layer_turn_{}'.format(i)))

        # Embedding
        embedding = Embedding(nb_words, EMBEDDING_DIM, weights=[embedding_matrix], 
                              input_length=max_seq_length, trainable=False, name='embedding_layer')  
        embs = []
        for i in range(window):
            embs.append(embedding(inputs[i]))

        # LSTMS
        lstm  = Bidirectional(LSTM(64, dropout=0.25))
        lstms = []
        for i in range(window):
            lstms.append(lstm(embs[i]))

        # Input
        inp = Concatenate(axis=-1)(lstms)
        mi1 = Dense(256, activation='relu')(inp)
        dro1 = Dropout(0.2)(mi1)
        mi2 = Dense(128, activation='relu')(dro1)
        dro2 = Dropout(0.2)(mi2)
        out = Dense(1, activation='sigmoid')(dro2)

        # Model
        model = Model(inputs, out)

        # Callbacks, Optimizers & Compile
        optimizer = SGD(lr=LEARNING_RATE, decay=1e-6, momentum=0.9, nesterov=True)
        model.compile(
            loss='mean_squared_error',
            optimizer=optimizer
        )
        Callbacks = [ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=LR_PATIENCE, verbose=True),
                     EarlyStopping(patience=PATIENCE, monitor='val_loss')]

        # Summary
        print(model.summary())

        # Training
        history = model.fit(TRAINING_GENERATOR.getFitGenerator(),
                            validation_data=VALIDATION_GENERATOR.getFitGenerator(),
                            epochs=EPOCHS,
                            steps_per_epoch=STEPS_PER_EPOCH,
                            validation_steps=VALIDATION_STEPS,
                            callbacks=Callbacks,
                            verbose=True)

        # Saving
        model.save("model.h5")
        return model, history


# # Fit

# In[281]:


print("#6 Fit")


# In[282]:


model, history = getModel(df_train, df_val, nb_words, EMBEDDING_DIM, max_seq_length, window=3, load_weights=False)
model.summary()


# In[284]:


if history is not None:
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model Loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train','val'], loc='upper left')
    plt.show()


# # Testing

# In[249]:


TEST_GENERATOR = DataGenerator(df_test, 32, max_seq_length, window=3)
print(model.evaluate(TEST_GENERATOR.getFitGenerator(), steps=TEST_GENERATOR.step))


# In[250]:


TEST_GENERATOR = DataGenerator(df_test, 32, max_seq_length, window=3)
y_true = TEST_GENERATOR.getTrue()
y_pred = model.predict(TEST_GENERATOR.getPredictGenerator(), steps=TEST_GENERATOR.step)
print(y_true.shape, y_pred.shape)


# In[251]:


def smooth(x):
    res = np.zeros((x.shape))
    kernel = [0.25, 0.5, 0.25]
    for i in range(len(res)):
        total = 0
        for j in range(len(kernel)):
            if i + j - 1 > 0 and i + j - 1 < len(x):
                total += x[i + j - 1] * kernel[j]
        res[i] = total
    return res


# In[252]:


plt.figure(figsize=(20, 50))
dec = 500
for i in range(20):
    plt.subplot(10, 2, i + 1)
    plt.plot(y_true[dec + i * 100: dec + (i + 1) * 100])
    plt.plot(y_pred[dec + i * 100: dec + (i + 1) * 100])

