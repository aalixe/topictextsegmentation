import os
import re
from lxml import etree
import pandas as pd

class Data:
    folders = ['W05_15/', 'W07_14/']
    path = '../../data/'
    def __init__(self):
        self.tvNews = []
        for folder in self.folders:
            folderPath = self.path + folder
            
            # Annot Files
            folderAnnot = folderPath + 'ANNOT_' + folder
            filesAnnot = os.listdir(folderAnnot)
            
            # Files name
            regex = re.compile('.*\.trs')
            removeExt = lambda x: x.split('.trs')[0]
            filesName = list(map(removeExt, list(filter(regex.match, filesAnnot))))
            
            for fileName in filesName:
                paths = self.buildPaths(folder, fileName)
                if self.pathCheck(paths):
                    self.tvNews.append(TvNews(paths, fileName))
                
    def buildPaths(self, folder, file):
        annot = self.path + folder + 'ANNOT_' + folder + file
        tv = self.path + folder + 'TV_' + folder + file
        return [annot + '.trs', annot + '.assgn', tv + '.ctm', tv + '.stm']
    
    def pathCheck(self, paths):
        for path in paths:
            if not os.path.isfile(path):
                return False
        return True
    
    def generateFiles(self):
        X = '{}'.format(self.path + 'Data/')
        os.makedirs(os.path.dirname(X), exist_ok=True)
        for tvNew in self.tvNews:
            tvNew.generateFile(self.path + 'Data/')
            
class TvNews:
    def __init__(self, paths, fileName):
        trs, assgn, ctm, stm = paths
        
        # Date
        self.date = fileName[-19:]
        
        # Name
        self.name = fileName[:-19]
        
        # Segments
        trs_xml = etree.parse(trs)
        self.segments = []
        ctm_df = pd.read_csv(ctm, sep=' ', names=['tv_name', 'channel', 'start', 'time', 'word', 'trust'])
        stm_df = self.readStm(stm)
        assgn_df = self.readAssgn(assgn)
        OUT = []
        for topic in trs_xml.xpath('//Trans/Topics/Topic'):
            if 'OUT' in topic.attrib['desc']:
                OUT.append(topic.attrib['id'])
        for topic in trs_xml.xpath('//Trans/Episode/Section'):
            if 'topic' not in topic.attrib or topic.attrib['topic'] in OUT:
                continue
            start = float(topic.attrib['startTime'])
            end = float(topic.attrib['endTime'])
            word_df = ctm_df[(ctm_df.start > start) & (ctm_df.start < end)]
            breath_group_df = stm_df[(stm_df.start > start) & (stm_df.start < end)]
            press_df = assgn_df[(assgn_df.start == start) & (assgn_df.end == end)]
            self.segments.append(Segment(paths, topic, start, end, word_df, breath_group_df, press_df))
           
    def readStm(self, filename):
        with open(filename, 'r') as f:
            lines = f.read().replace('\r', '').split('\n')[:-1]
        ret = pd.DataFrame(columns=['tv_name', 'channel', 'locutor', 'start', 'end', 'gender', 'words'])
        for i, line in enumerate(lines):
            splitted = line.split(' ')
            tv_name, channel, locutor, start, end, gender = splitted[:6]
            words = splitted[6:]
            ret.loc[i] = [tv_name, int(channel), int(locutor[1:]), float(start), float(end), gender, words]
        return ret
    
    def readAssgn(self,filename):
        with open(filename, 'r') as f:
            text = f.read().replace('\r', '').split('\n')[:-1]
        ret = pd.DataFrame(columns=['start', 'end', 'high', 'medium', 'low'])
        for line in text:
            splitted = line.split(';')
            assert(len(splitted) == 4)
            d = {}
            d['start'], d['end'] = splitted[0].split(':')
            d['high'] = splitted[1].split(' ')
            d['medium'] = splitted[2].split(' ')
            d['low'] = splitted[3].split(' ')
            ret.append(d, ignore_index=True)
        return ret
        
    def generateFile(self, path):
        # Generating X
        filename = '{}{}{}.txt'.format(path, self.name, self.date)
        file = open(filename, 'w+')
        for i, segment in enumerate(self.segments):
            for bg in segment.breathgroups:
                speech = ' '.join(bg.words)
                if len(speech) > 1:
                    file.write('{}\t{}\t{}\t{}\t{}\n'.format(bg.start, bg.end, bg.locutor, ' '.join(bg.words), i))
        file.close()
        
class Segment:
    def __init__(self, paths, topic, start, end, word_df, breath_group_df, press_df):
        trs, assgn, ctm, stm = paths
        
        # Times
        self.start = start
        self.end = end
        
        # Title
        self.title = ""
        for  turn in topic.xpath('Turn'):
            for sync in turn.xpath('Sync'):
                self.title += sync.tail + '. '
        self.title = self.title.replace('\n', '').replace('\t', '')
        
        # Words
        self.words = []
        for _, row in word_df.iterrows():
            self.words.append(Word(row))
        
        # Breathgroup
        self.breathgroups = []
        for _, row in breath_group_df.iterrows():
            self.breathgroups.append(BreathGroup(row))
        
        # Articles
        articles_high = press_df['high']
        articles_medium = press_df['medium']
        articles_low = press_df['low']
        
    def printText(self):
        for word in self.text:
            print(word.word, end=' ')

class Word:
    def __init__(self, row):
        self.trust = row['trust']
        self.word = row['word']
        self.start = row['start']
        end = row['start'] + row['time']
        
class BreathGroup:
    def __init__(self, row):
        #self.channel = row['channel']
        self.locutor = row['locutor']
        self.start = row['start']
        self.end = row['end']
        #self.gender = row['gender']
        self.words = row['words']
        
class Article:
    def __init__(self, filename):
        
        title = ""
        date = ""
        ext = ""
        category = 0
        website = 0
    
class Locutor:
    def __init__(self):
        gender = True
        loc_id = 0