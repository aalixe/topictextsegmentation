import argparse
from datetime import datetime
from scrappers import Scraper
import os.path
from os import mkdir

from scrappers.le_monde import LeMonde
from scrappers.l_express import LExpress
from scrappers.ving_minutes import VingtMinutes
from scrappers.bfm import BFM
from scrappers.francetv import FranceTV
from scrappers.franceinter import FranceInter
from scrappers.le_parisien import LeParisien


def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


if __name__ == "__main__":

    scrappers = []
    for name, value in globals().copy().items():
        if type(value) is type and issubclass(value, Scraper) and value != Scraper:
            scrappers.append(name)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "source",
        help="Source website to parse from.",
        choices=scrappers,
        type=str,
        default="LeMonde",
    )
    parser.add_argument(
        "-s",
        "--startdate",
        help="The Start Date - format YYYY-MM-DD",
        required=True,
        type=valid_date,
    )

    parser.add_argument(
        "-e",
        "--enddate",
        help="The end Date - format YYYY-MM-DD",
        required=True,
        type=valid_date,
    )

    args = parser.parse_args()

    scrapper = globals()[args.source]()

    # Creates the directory and the csv if its not already done
    if not os.path.exists(scrapper.path):
        mkdir(scrapper.path)
        with open(scrapper.path + "articles.csv", "w+") as f:
            f.write("date;path;title;url\n")

    scrapper.run(args.startdate, args.enddate)
