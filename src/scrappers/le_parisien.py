from . import Article, Scraper
from bs4 import BeautifulSoup
import datetime as dt
from urllib.request import Request
from urllib.request import urlopen


class LeParisien(Scraper):
    def __init__(self):
        Scraper.__init__(self)
        self.path = "../data/Data_Press/LeParisien/"

    def parse_list(self, date):
        urls = []
        url_api = "http://www.leparisien.fr/archives/{}".format(
            date.strftime("%Y/%d-%m-%Y/")
        )
        request = Request(url_api, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")
        for url in soup.find(class_="pageContent container").find_all(
            class_="story-preview"
        ):
            urls.append("https:" + url.find("a", href=True)["href"])
        return urls

    def parse_article(self, url, date):
        request = Request(url, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")
        title = soup.find("h1", class_="title_xl").text

        date = date.strftime("%d/%m/%Y")
        text = ""
        for p in soup.find(class_="article-section").find_all("p"):
            text += p.text

        return Article(url, title, date, text)
