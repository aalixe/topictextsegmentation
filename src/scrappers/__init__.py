import datetime as dt
import pandas as pd
import re
import uuid


class Article:
    def __init__(self, url, title, date, text):
        self.id = uuid.uuid1()
        self.url = url
        self.title = title
        self.date = date
        self.text = text
        self.clear_data()

    def __repr__(self):
        return self.title + "\n" + self.text

    def __add__(self, dataframe):
        return dataframe.append(
            {
                "url": self.url,
                "path": "{}.txt".format(self.id),
                "date": self.date,
                "title": self.title,
            },
            ignore_index=True,
        )

    def __gt__(self, path):
        file = open("{}{}.txt".format(path, self.id), "w+")
        file.write(str(self))
        file.close()

    def clear_data(self):
        #  self.text = re.sub("\s+", " ", self.text)
        self.title = re.sub("\s+", " ", self.title).replace(";", ",")
        self.url = re.sub("\s+", " ", self.url).replace(";", ",")
        self.text = re.sub("\s+", " ", self.text)


class Scraper:
    # Return path of the articles.csv
    def __init__(self):
        self.path = None

    def get_csv(self):
        return "{}articles.csv".format(self.path)

    # Save articles in articles.csv & .txt
    def save_articles(self, articles):
        df = pd.DataFrame(columns=["date", "path", "title", "url"])
        for article in articles:
            df = article + df
        with open(self.get_csv(), "a") as f:
            try:
                f.write(df.to_csv(index=False, sep=";", header=False))
                for article in articles:
                    self.path < article
            except Exception as error:
                print(error)

    # Remove urls already present in articles.csv
    def filter_urls(self, urls):
        df = pd.read_csv(self.get_csv(), sep=";")
        urls = pd.Series(urls)
        urls = list(urls[~urls.isin(df.url)])
        return urls

    # Dates python generator to iterate through start to end
    def dates_generator(self, start, end):
        for n in range(int((end - start).days)):
            yield start + dt.timedelta(n)

    def log(self, msg, error=False):
        with open(
            "{}{}.txt".format(self.path, "error" if error else "log"), "a"
        ) as file:
            file.write(
                "[{}] {}\n".format(dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S"), msg)
            )

    def run(self, start_date=dt.date(2019, 10, 24), end_date=dt.date(2019, 10, 25)):
        self.log("{} initialized".format(self.__class__.__name__))
        for date in self.dates_generator(start_date, end_date):
            try:
                urls = self.filter_urls(self.parse_list(date))
            except Exception as e:
                self.log(
                    "parse_list() Failed for {}: {}".format(
                        date.strftime("%d/%m/%Y"), e
                    ),
                    error=True,
                )
                urls = []
            articles = []
            for url in urls:
                try:
                    articles.append(self.parse_article(url, date))
                except Exception as e:
                    self.log(
                        "parse_article() Failed for {}: {}".format(url, e), error=True
                    )
            self.log(
                "{}: {}/{} success".format(
                    date.strftime("%d/%m/%Y"), len(articles), len(urls)
                )
            )
            self.save_articles(articles)
