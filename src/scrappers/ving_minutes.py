from . import Article, Scraper
from bs4 import BeautifulSoup
from urllib.request import Request
from urllib.request import urlopen
import datetime as dt

"""
    Work In Progress
"""


class VingtMinutes(Scraper):
    def __init__(self):
        Scraper.__init__(self)
        self.path = "../data/Data_Press/20Minutes/"

    def parse_list(self, date):
        urls = []
        url_api = "https://www.20minutes.fr/archives/{}".format(
            date.strftime("%Y/%m-%d")
        )
        request = Request(url_api, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")
        for url in soup.find("ul", class_="spreadlist").find_all("a", href=True):
            urls.append("https://www.20minutes.fr" + url["href"])
        return urls

    def parse_article(self, url, date):
        # Request Url
        request = Request(url, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")

        # Find Title
        title = soup.find("h1", class_="nodeheader-title").text

        # Setting date
        date = date.strftime("%d/%m/%Y")

        # Find Corpus Text
        corpus = soup.find(class_="lt-endor-body content")
        text = ""

        try:
            text += corpus.find(class_="summary").text
        except:
            pass
        for p in corpus.find_all("p"):
            text += p.text

        return Article(url, title, date, text)
