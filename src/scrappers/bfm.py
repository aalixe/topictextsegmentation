from . import Article, Scraper
from bs4 import BeautifulSoup
import datetime as dt
from urllib.request import Request
from urllib.request import urlopen

"""
    Fully functional scraper (Take this as example)
"""


class BFM(Scraper):
    def __init__(self):
        Scraper.__init__(self)
        self.path = "../data/Data_Press/BFM/"

    def parse_list(self, date):
        urls = []

        # Parsing Urls for this page
        url_api = "https://www.bfmtv.com/archives/{}/".format(date.strftime("%Y/%m/%d"))
        request = Request(url_api, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")
        for url in soup.find_all("a", class_="color-force", href=True):
            if not url["title"].startswith("VIDEO"):
                urls.append(url["href"])
        return urls

    def parse_article(self, url, date):
        # Request Url
        request = Request(url, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")

        # Find Title
        title = soup.find(class_="titre-article").text

        # Setting date
        date = date.strftime("%d/%m/%Y")

        # Find Corpus Text
        text = ""
        if len(soup.find(class_="links-color bloc").find_all("p")):
            for p in soup.find(class_="links-color bloc").find_all("p"):
                text += p.text
        else:
            text = soup.find(class_="links-color bloc").text

        return Article(url, title, date, text)
