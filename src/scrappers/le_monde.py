from . import Article, Scraper
from bs4 import BeautifulSoup
import datetime as dt
from urllib.request import Request
from urllib.request import urlopen

"""
    Fully functional scraper (Take this as example)
"""


class LeMonde(Scraper):
    def __init__(self):
        Scraper.__init__(self)
        self.path = "../data/Data_Press/LeMonde/"

    def parse_list(self, date):
        urls = []

        # Parsing Urls for this page
        url_api = "https://www.lemonde.fr/archives-du-monde/{}/".format(
            date.strftime("%d-%m-%Y")
        )
        request = Request(url_api, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")
        pages = soup.find_all(class_="river__pagination--page", href=True)
        for url in soup.find_all(class_="teaser__link", href=True):
            urls.append(url["href"])

        # Parsing Urls for other pages
        if len(pages) > 1:
            for page in pages[1:]:
                url_api = "https://www.lemonde.fr{}".format(page["href"])
                request = Request(url_api, headers={"User-Agent": "Mozilla/5.0"})
                soup = BeautifulSoup(urlopen(request), features="lxml")
                pages = soup.find_all(class_="river__pagination--page", href=True)
                for url in soup.find_all(class_="teaser__link", href=True):
                    urls.append(url["href"])

        return urls

    def parse_article(self, url, date):
        # Request Url
        request = Request(url, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")

        # Find Title
        title = soup.find(class_="zone--article").find(class_="article__title").text

        # Setting date
        date = date.strftime("%d/%m/%Y")

        # Find Corpus Text
        text = ""
        try:
            text += soup.find(class_="article__desc").text
        except:
            pass
        for p in soup.find(class_="article__content").find_all(
            class_="article__paragraph"
        ):
            text += p.text

        return Article(url, title, date, text)
