from . import Article, Scraper
from bs4 import BeautifulSoup
import datetime as dt
from urllib.request import Request
from urllib.request import urlopen

"""
    Fully functional scraper (Take this as example)
"""


class FranceInter(Scraper):
    def __init__(self):
        Scraper.__init__(self)
        self.path = "../data/Data_Press/FranceInter/"

    def parse_list(self, date):
        urls = []

        # Parsing Urls for this page
        url_api = "https://www.franceinter.fr/archives/{}".format(
            date.strftime("%Y/%m-%d")
        )
        request = Request(url_api, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")
        for url in soup.find("ul", class_="simple-list").find_all("a", href=True):
            urls.append("https://franceinter.fr/" + url["href"])
        return urls

    def parse_article(self, url, date):
        # Request Url
        request = Request(url, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")

        # Find Title
        title = soup.find("h1").text

        # Setting date
        date = date.strftime("%d/%m/%Y")

        # Find Corpus Text
        text = ""
        for p in soup.find("article", class_="content-body").find_all("p"):
            text += p.text

        if len(text) < 100:
            raise Exception("Not an article")

        return Article(url, title, date, text)
