from . import Article, Scraper
from bs4 import BeautifulSoup
import datetime as dt
from urllib.request import Request
from urllib.request import urlopen
import locale

locale.setlocale(locale.LC_ALL, "fr_FR.UTF-8")

"""
    Fully functional scraper (Take this as example)
"""


class FranceTV(Scraper):
    def __init__(self):
        Scraper.__init__(self)
        self.path = "../data/Data_Press/FranceTV/"

    def parse_list(self, date):
        urls = []

        # Parsing Urls for this page
        url_api = "https://www.francetvinfo.fr/archives/{}/".format(
            date.strftime("%Y/%d-%B-%Y.html").replace("é", "e").replace("û", "u")
        )
        request = Request(url_api, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")
        for url in soup.find("ul", class_="contents").find_all("a", href=True):
            urls.append("https://www.francetvinfo.fr" + url["href"])
        return urls

    def parse_article(self, url, date):
        # Request Url
        request = Request(url, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")

        # Find Title
        title = soup.find("h1").text

        # Setting date
        date = date.strftime("%d/%m/%Y")

        # Find Corpus Text
        text = ""
        for p in soup.find(class_="text").find_all("p"):
            text += p.text
        if len(text) < 50:
            for p in soup.find(class_="text").find_all("div"):
                text += p.text

        return Article(url, title, date, text)
