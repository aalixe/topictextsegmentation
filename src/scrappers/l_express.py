from . import Article, Scraper
from bs4 import BeautifulSoup
import datetime as dt
from urllib.error import HTTPError
from urllib.request import Request
from urllib.request import urlopen


def sanitize_url(url):
    if url[:2] == "//":
        return "https:" + url
    return url


class LExpress(Scraper):
    def __init__(self):
        Scraper.__init__(self)
        self.path = "../data/Data_Press/LExpress/"

    def parse_list(self, date):
        urls = []
        i = 1

        # Parsing Urls for this page
        while True:

            url_api = "https://www.lexpress.fr/archives/{}".format(
                date.strftime("%Y/%m/%d")
            )
            url_api += "?p=%d" % i
            request = Request(url_api, headers={"User-Agent": "Mozilla/5.0"})
            try:
                opened = urlopen(request)
                soup = BeautifulSoup(opened, features="lxml")

                for group in soup.find_all("div", attrs={"class": "group"}):
                    link = group.find_all("a")[0]
                    urls.append(sanitize_url(link["href"]))
            except HTTPError:
                break
            i += 1
        return urls

    def parse_article(self, url, date):
        # Request Url
        request = Request(url, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(urlopen(request), features="lxml")

        # Find Title
        title = soup.find(class_="title_alpha").text

        # Setting date
        date = date.strftime("%d/%m/%Y")

        # Find Corpus Text
        text = ""

        try:
            text += soup.find(class_="chapo title_gamma").text + "\n"
        except:
            pass
        for p in soup.find(class_="article_container").find_all("p"):
            text += p.text

        return Article(url, title, date, text)
