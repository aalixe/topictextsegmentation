from topic_segmenter import TopicSegmenter

import numpy as np

# TODO: x=all texts shape=(nb_examples, max_sentence_length, max_sentence_number)
x = [['Bonjour', 'voici', 'les', 'titres', 'de', 'ce', 'soir'], ['Jean-Marie', 'Le', 'Pen', 'passe', 'au', 'second', 'tour', 'des', 'présidentiels'],
     ['Jean-Marie', 'Le', 'Pen', 'passe', 'au', 'second', 'tour', 'des', 'présidentiels'], ['Bonjour', 'voici', 'les', 'titres', 'de', 'ce', 'soir'],
     ['Jean-Marie', 'Le', 'Pen', 'passe', 'au', 'second', 'tour', 'des', 'présidentiels'],['Jean-Marie', 'Le', 'Pen', 'passe', 'au', 'second', 'tour', 'des', 'présidentiels'],
     ['Bonjour', 'voici', 'les', 'titres', 'de', 'ce', 'soir'], ['Jean-Marie', 'Le', 'Pen', 'passe', 'au', 'second', 'tour', 'des', 'présidentiels']]

# Set vocabulary as a set
vocab = set()
for i in range(len(x)):
  for j in range(len(x[i])):
    vocab.add(x[i][j])

# Set vocabulary as dict (word to index)
l = ['<EOS>'] + list(vocab)
vocab = dict()
for i, e in enumerate(l):
  vocab[e] = i

# Transform x from list of words to list of indexes
for i in range(len(x)):
  for j in range(len(x[i])):
    x[i][j] = vocab[x[i][j]]

# Dimensions
nb_examples = 1
max_sentence_length = max(map(len, x)) + 1
max_sentence_number = len(x)

# Pad X_train with <EOS>
X_train = np.array([xi+[0]*(max_sentence_length-len(xi)) for xi in x]).reshape(nb_examples, max_sentence_number, max_sentence_length)

# TODO: y={0;1} for each sentence shape=(nb_examples, max_sentence_number)
# Pad y_train with 0s
y_train = np.array([0,1,0,1,0,0,1,1]).reshape(nb_examples, max_sentence_number)

# Training
topic_segmenter = TopicSegmenter('/TopicTextSegmentation/src/preprocess/saves/20minutes20112019/word2vec_200.model')
topic_segmenter.set_model(max_sentence_length, max_sentence_number)
epochs = 10
batch_size = 64
validation_split = 0
history = topic_segmenter.train_model(X_train, y_train, epochs, batch_size, validation_split, 'topic_segmenter.h5')
