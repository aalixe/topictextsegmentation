from attention_with_context import AttentionWithContext

from gensim.models import Word2Vec

from keras.models import Model
from keras.layers import Input, LSTM, Dense, Bidirectional, TimeDistributed, Embedding, Dropout
from keras import regularizers
from keras.callbacks import ModelCheckpoint
import keras.backend as K

import numpy as np

class TopicSegmenter():
  def __init__(self, word2vec_path):
    self.hyperparameters =\
    {
      'l2_regulizer': None,
      'rnn_word': LSTM,
      'rnn_units_word': 256,
      'merge_mode_word': 'sum',
      'dense_units_word': 200,
      'rnn_sentence': LSTM,
      'rnn_units_sentence': 256,
      'merge_mode_sentence': 'sum',
      'dense_units_sentence': 200,
      'dropout_sentence': 1,
      'optimizer': 'adam',
      'metrics': ['accuracy'],
      'loss': 'binary_crossentropy'
    }

    word2Vec = Word2Vec.load(word2vec_path)
    vocab_size = len(word2Vec.wv.vocab)

    embedding_matrix = np.zeros((vocab_size+1, word2Vec.vector_size))
    for i, word in enumerate(word2Vec.wv.vocab):
      embedding_matrix[i+1] = word2Vec[word]
      
    self.embedding = Embedding(vocab_size,
                               word2Vec.vector_size,
                               weights=[embedding_matrix],
                               trainable=False,
                               name='embedding_word')

  def set_hyperparameters(self, **kwargs):
    for key in kwargs.keys():
      self.hyperparameters[key] = kwargs[key]

  def show_hyperparameters(self):
    for key, value in self.hyperparameters.items():
      print('%-20s %s' % (key, value))

  def set_model(self, max_sentence_length, max_sentence_number):
    kernel_regularizer = None if self.hyperparameters['l2_regulizer'] is None\
                    else regularizers.l2(self.hyperparameters['l2_regulizer'])

    # Word model
    word_input = Input(shape=(max_sentence_length,),
                       dtype='float64',
                       name='input_word')
    word_sequences = self.embedding(word_input)
    word_lstm = Bidirectional(
      self.hyperparameters['rnn_word'](self.hyperparameters['rnn_units_word'],
                                  return_sequences=True,
                                  kernel_regularizer=kernel_regularizer),
      self.hyperparameters['merge_mode_word'],
      name='bilstm_word')(word_sequences)
    word_dense = TimeDistributed(
      Dense(self.hyperparameters['dense_units_word'],
            kernel_regularizer=kernel_regularizer,
            name='dense_word'), name='distributed_dense_word')(word_lstm)
    word_attention = AttentionWithContext(name='attention_word')(word_dense)
    wordEncoder = Model(word_input, word_attention)

    # Sentence model
    sentence_input = Input(shape=(max_sentence_number, max_sentence_length),
                           dtype='float64',
                           name='input_sentence')
    sentence_encoder = TimeDistributed(wordEncoder,
                                       name='distributed_word_encoder'
                                       )(sentence_input)
    sentence_lstm = Bidirectional(
      self.hyperparameters['rnn_sentence'](self.hyperparameters['rnn_units_sentence'],
                                           return_sequences=True,
                                           kernel_regularizer=kernel_regularizer),
      self.hyperparameters['merge_mode_sentence'],
      name='bilstm_sentence')(sentence_encoder)
    sentence_dense = TimeDistributed(
      Dense(self.hyperparameters['dense_units_sentence'],
            kernel_regularizer=kernel_regularizer,
            name='dense_sentence'),
            name='distributed_dense_sentence')(sentence_lstm)
    sentence_attention = Dropout(self.hyperparameters['dropout_sentence'],
                                 name='dropout_sentence')(
      AttentionWithContext(name='attention_sentence')(sentence_dense))

    # Final model
    output = Dense(max_sentence_number,
                   activation='sigmoid',
                   name='final_dense')(sentence_attention)
    self.model = Model(sentence_input, output, name='topic_segmenter')
    
    self.model.compile(loss=self.hyperparameters['loss'],
                       optimizer=self.hyperparameters['optimizer'],
                       metrics=self.hyperparameters['metrics'])
    
    self.model.summary()

  def train_model(self, X_train, y_train, epochs, batch_size, validation_split,
                  path, verbose=1, checkpoint_path=None):
    checkpoint = None
    if checkpoint_path is not None:
      checkpoint = ModelCheckpoint(checkpoint_path,
                                   verbose=0,
                                   monitor='val_loss',
                                   save_best_only=True,
                                   mode='auto')

    self.history = self.model.fit(X_train, y_train,
                                  epochs=epochs,
                                  batch_size=batch_size,
                                  validation_split=validation_split,
                                  verbose=verbose,
                                  callbacks=[checkpoint] if checkpoint is not None else None)

    self.model.save(path)

    return self.history

  def load_model(self, path):
    self.model.load_model(path)

  def predict(self, X):
    return self.model.predict(X)
