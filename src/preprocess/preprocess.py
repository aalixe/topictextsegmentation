import re
import spacy
import numpy as np
import pandas as pd
from string import punctuation
from gensim.models import Word2Vec
from tensorflow.python.keras.preprocessing.text import text_to_word_sequence


# USAGE:
#  constructor : Pipeline(minimum_word_length, stopwords list)
#
#  Default constructor has a default minimum_word_length of 2
#  and the Spacy french stopwords list.
#  Please use the default constructor Pipeline() if this behavior suits you
#
#  Preprocessed your documents with this 2 lines:
#
#  pipe = Pipeline()
#  preprocessed_corpus = pipe.preprocess_corpus(corpus, is_open)
#
#  corpus can be wether be a list of documents (is_open = True) or a list of filepath (is_open = False)


# Pipeline for preprocessing
class Pipeline:

    # lemmatisation without NER and tagging
    def lemma(self, w):
        return self.nlp(str(w))[0].lemma_.lower()

    # Is the word a stopwords
    def stop(self, w):
        return len(w) > self.min_length and w not in self.stops

    # deaccentuation
    # Shouldn't change a thing, but here it is if one day we need it
    def deaccent(self, w):
        return unidecode.unidecode(w)

    # Words are tokenized
    # Then we remove stopwords
    # Then we lemmatised the words
    def pipe_on_words(self, text):
        # text_to_word_sequence
        # split sur : " "
        # enleve les !"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n
        # on peut changer les param au besoin
        tokens = np.array(text_to_word_sequence(text))
        # On retire les stopwords

        if len(tokens) > 0:
            stopped = self.fstop(tokens)
            filtered = tokens[stopped]
            # lemmatisé
            if len(filtered) > 0:
                lemmatised = self.flem(filtered)
                return lemmatised
            return []
        return []

    # Replacement on the text thanks to regex (re)
    def pipe_on_globals(self, text):
        for rep, val in self.replacements.items():
            text = re.sub(rep, val, text)
        return text

    # Read stopwords file and return list
    # improvement : use numpy  loadtxt for more efficiency
    def get_stop(self, file):
        # stops = np.loadtxt()
        f = open(file, "r", encoding="latin_1")
        stopwords = list(map(lambda x: x[:-1], f.readlines()))
        f.close()
        return stopwords

    # Pipeline for one text (str)
    def pipe(self, text):
        return self.pipe_on_words(self.pipe_on_globals(text))

    # Pipeline for file with given filename
    def pipe_file(self, filename, enc="utf-8"):
        f = open(filename, "r", encoding=enc)
        txt = f.read()
        f.close()
        return self.pipe_on_words(self.pipe_on_globals(txt))

    # Call preprocess on a corpus
    # docs, Array of filename or Array of filepath
    def preprocess_corpus(self, docs, isopen=True):
        if isopen:
            return self.process(docs)
        return self.processOpen(docs)

    # Init function
    # min_length, minimal length for word keeping (len(word) < min_len are deleted)
    # stopfile, custom stop file to import. Default is None and Spacy's one is used
    def __init__(self, min_length=2, stopfile=None):
        # minimum length of a word
        self.min_length = min_length

        # load spacy french news core
        try:
            self.nlp = spacy.load("fr_core_news_sm")
        except:
            print("Could'nt load Spacy fr core, please be sure to have Spacy installed")

        # stopwords list
        self.stops = None
        # We can use a custom stopword list, feel free to overload get_stop() func
        if stopfile is not None:
            self.stops = set(self.get_stop(stopfile))
        else:
            # 600 words stop set from Spacy
            self.stops = spacy.lang.fr.stop_words.STOP_WORDS

        # replacements list
        self.replacements = {
            # unicode troubles
            r"\\u2019": "'",
            r"\\u002c": "",
            # contractions
            r"d['’]": "de ",
            r"l['’]": "le ",
            r"c['’]": "ce ",
            r"qu['’]": "que ",
            r"n['’]": "ne ",
            r"s['’]": "se ",  # 's'il vous plait'  --> se il vous plait
            # apostorophe
            r"C[’']": "",
            r"[’']": " ",
            r"[Ll][’']": "",
            # Remove url
            r"(http|https)?:\/\/[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}(/\S*)?": " ",
            # email
            r"@\w+": r" ",
            # hashtag
            r"#\w+": r" ",
            # 3 pti points
            r"[.]+": " ",
        }

        # Vectorised func
        self.process = np.vectorize(self.pipe)
        self.processOpen = np.vectorize(self.pipe_file)
        self.flem = np.vectorize(self.lemma)
        self.fdeac = np.vectorize(self.deaccent)
        self.fstop = np.vectorize(self.stop)
