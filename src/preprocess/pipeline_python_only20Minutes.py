#!/usr/bin/env python
# coding: utf-8

# In[1]:


from datetime import datetime
from gensim.models import Word2Vec
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import TweetTokenizer
import os
import pandas as pd
from string import punctuation
import re
import sys

nltk.download('stopwords')


# In[2]:


data_path = '../../data/Data_Press'
os.listdir(data_path)


# # Clean data

# In[3]:


tknzr = TweetTokenizer()
stopWords = set(stopwords.words('french'))


# In[4]:


# There are a lot of missing replacemnts to add
replacements = {
    r"d['’]": "de ",   
    r"C[’']": "",
    r"[’']": " ",
}


# In[5]:


def standardization(text):
    text = re.sub(r"\\u2019", "'", text)
    text = re.sub(r"\\u002c", ",", text)
    text = re.sub(r"[0-9]+", "", text)
    text = re.sub(r"@\w+", r' ',text)
    text = re.sub(r"#\w+", r' ',text)
    text = re.sub(r"[.]+"," ",text)
    text = re.sub(r"[0-9]+", "", text)
    text = text.lower()
    text = tknzr.tokenize(text)
    text = [replacements[t] if t in replacements else t for t in text]
    text = [i for i in text if (i not in stopWords) and (i not in punctuation )]
    return text


# ## Article Class
# Articles have one line with their title and another for the text

# In[6]:


class Article:
    def __init__(self, path):
        self.title = ""
        self.content = ""
        self.load(path)
        
    def load(self, path):
        f = open(path, 'r')
        self.title = f.readline()
        self.content = f.readline()
        
    def standardize(self):
        self.content = standardization(self.content)


# In[7]:


path = '../../data/Data_Press/20Minutes/'
art_path = os.path.join(path, os.listdir(path)[0])

art = Article(art_path)


# In[8]:


art.content


# In[9]:


art.standardize()


# In[10]:


art.content


# In[11]:


class MySentences(object):
    def __init__(self, dirname):
        self.dirname = dirname
 
    def __iter__(self):
        for fold_path in ["20Minutes"]:#os.listdir(self.dirname):
            print(str(datetime.now()) + ': Retrieving ' + fold_path)
            fold_path = os.path.join(self.dirname, fold_path)
            df = pd.read_csv(os.path.join(fold_path, 'articles.csv'), sep=';')
            text_contents = []
            for i, file_path in enumerate(df["path"]):
                if i % (len(df["path"]) // 10) == 0:
                    print(str(datetime.now()) + ': ' + str(i * 100 // (len(df["path"]))) + '%')
                    sys.stdout.flush()
                article = Article(os.path.join(fold_path, file_path))
                article.standardize()
                yield article.content


# # Retrieve all articles

# In[12]:


sentences = MySentences(data_path)


# # Building word2vec

# In[13]:


NEGATIVE_SAMPLING = 5


# In[14]:


model_200 = Word2Vec(sentences=sentences, size=200, window=10, min_count=1, workers=4, negative=NEGATIVE_SAMPLING)


# In[15]:


#model_200.wv.most_similar('lama')


# In[16]:


#model_200.save('saves/word2vec_200.model')


# In[17]:


print('Starting second word2vec')


# In[18]:


model_300 = Word2Vec(sentences=sentences, size=300, window=10, min_count=1, workers=4, negative=NEGATIVE_SAMPLING)


# In[19]:


model_300.wv.most_similar('animal')


# In[20]:


#model_300.save('saves/word2vec_300.model')

