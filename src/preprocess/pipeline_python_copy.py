#!/usr/bin/env python
# coding: utf-8

# # Imports

# In[1]:


import os
import re
import sys
import spacy
import numpy as np
import pandas as pd
from datetime import datetime
from string import punctuation
from gensim.models import Word2Vec
from tensorflow.python.keras.preprocessing.text import text_to_word_sequence

from preprocess import Pipeline


# # Load data

# In[2]:


data_path = '../../data/Data_Press'
os.listdir(data_path)


# # Clean data

# Use custom stopwords list

# In[3]:


MIN_LENGTH = 2


# ## Article Class
# Articles have one line with their title and another for the text

# In[4]:


class Article:
    def __init__(self, path):
        self.title = ""
        self.content = ""
        self.load(path)
        
    def load(self, path):
        f = open(path, 'r')
        self.title = f.readline()
        self.content = f.readline()
        f.close()
        
    def standardize(self, preproc):
        self.content = preproc.pipe(self.content)


# In[5]:


path = '../../data/Data_Press/20Minutes/'
art_path = os.path.join(path, os.listdir(path)[0])

art = Article(art_path)


# In[6]:


art.content


# In[7]:


class MySentences(object):
    def __init__(self, dirname):
        self.dirname = dirname
        self.pretreat = Pipeline(MIN_LENGTH)
 
    def __iter__(self):
        for fold_path in os.listdir(self.dirname):
            print(str(datetime.now()) + ': Retrieving ' + fold_path)
            fold_path = os.path.join(self.dirname, fold_path)
            df = pd.read_csv(os.path.join(fold_path, 'articles.csv'), sep=';')
            text_contents = []
            for i, file_path in enumerate(df["path"]):
                if i % (len(df["path"]) // 10) == 0:
                    print(str(datetime.now()) + ': ' + str(i * 100 // (len(df["path"]))) + '%')
                    sys.stdout.flush()
                article = Article(os.path.join(fold_path, file_path))
                article.standardize(self.pretreat)
                yield article.content


# # Retrieve all articles

# In[8]:


sentences = MySentences(data_path)


# # Building word2vec

# In[9]:


NEGATIVE_SAMPLING = 5


# In[10]:


model_200 = Word2Vec(sentences=sentences, size=200, window=10, min_count=1, workers=4, negative=NEGATIVE_SAMPLING)


# In[ ]:


model_200.wv.most_similar('barack')


# In[ ]:


model_200.save('saves/word2vec_200.model')


# In[ ]:


print('Starting second word2vec')


# In[ ]:


model_300 = Word2Vec(sentences=sentences, size=300, window=10, min_count=1, workers=1, negative=NEGATIVE_SAMPLING)


# In[ ]:


model_300.wv.most_similar('barack')


# In[ ]:


model_300.save('saves/word2vec_300.model')


# In[ ]:




