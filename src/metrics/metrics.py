class Segment():
  def __init__(self, start, end):
    self.start = start
    self.end = end
  
  def __repr__(self):
    return '(' + str(self.start) + ';' + str(self.end) + ')'

def cov_2_segments(segment1, segment2):
  if segment1.start > segment2.end or segment2.start > segment1.end:
    return 0.0, 0.0, 0.0
  
  union = Segment(segment1.start, segment1.end)
  
  if segment2.start > segment1.start:
    union.start = segment2.start
  if segment2.end < segment1.end:
    union.end = segment2.end
  
  if union.end - union.start == 0:
    return 0.0, 0.0, 0.0
  
  cov_s1_in_s2 = (union.end - union.start) / (segment1.end - segment1.start)
  cov_s2_in_s1 = (union.end - union.start) / (segment2.end - segment2.start)
  cov_s1_s2 = 2 * cov_s1_in_s2 * cov_s2_in_s1 / (cov_s1_in_s2 + cov_s2_in_s1)
  
  return (cov_s1_s2, cov_s1_in_s2, cov_s2_in_s1)

def correct_ratio(segments_a, segments_b, threshold):
  nb_correct_a = 0.0
  weighted_correct_a = 0.0
  for segment_a in segments_a:
    max_cov_a_in_b = 0.0
    associated_cov_a_b = 0.0
    for segment_b in segments_b:
      cov_a_b, cov_a_in_b, _ = cov_2_segments(segment_a, segment_b)
      
      if max_cov_a_in_b  < cov_a_in_b:
        max_cov_a_in_b = cov_a_in_b
        associated_cov_a_b = cov_a_b
        
    if associated_cov_a_b > threshold:
      nb_correct_a += 1.0
      weighted_correct_a += segment_a.end - segment_a.start
  
  return (nb_correct_a / len(segments_a),
    weighted_correct_a / sum([s.end - s.start for s in segments_a]))
  
def covN_covD(segments_ref, segments_hyp, threshold):
  recallN, recallD = correct_ratio(segments_ref, segments_hyp, threshold)
  precisionN, precisionD = correct_ratio(segments_hyp, segments_ref, threshold)
  f1scoreN = 2 * recallN * precisionN / (recallN + precisionN)
  f1scoreD = 2 * recallD * precisionD / (recallD + precisionD)
  
  return (recallN, precisionN, f1scoreN, recallD, precisionD, f1scoreD)
