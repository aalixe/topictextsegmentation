# Topic Text Segmentation

## Questions

C'est quoi les Sync dans les .trs?

## Installation

Pour utiliser les paquets requis, il est conseillé de les installer via le `requirements.txt`.

```
$ pip install -r requirements.txt
```

Les scrapers peuvent être appelé via la ligne de commande. Pour avoir toute les precisions, il faut lancer `__init__.py`
depuis src.

```
$ cd src/
$ python3 __init__.py --help

usage: __init__.py [-h] -s STARTDATE -e ENDDATE
                   {LeMonde,LExpress,VingtMinutes,BFM,FranceTV,FranceInter,LeParisien}

positional arguments:
  {LeMonde,LExpress,VingtMinutes,BFM,FranceTV,FranceInter,LeParisien}
                        Source website to parse from.

optional arguments:
  -h, --help            show this help message and exit
  -s STARTDATE, --startdate STARTDATE
                        The Start Date - format YYYY-MM-DD
  -e ENDDATE, --enddate ENDDATE
                        The end Date - format YYYY-MM-DD
```

Par exemple, pour récolter tous les articles du 20 minutes du 19 décembre 2015 :

```
$ python3 __init__.py -s 2015-12-19 -e 2015-12-20 VingtMinutes
```

Les articles récoltés sont ensuite stockés dans `data/Data_Press`.
